﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Form;
using System.Collections.Generic;
using System.Linq;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IFollowupFormBusiness
    {
        OrderItemFollowupForm Get(int formId, string guid);

        OrderItemFollowupForm Get(int formId);

        OperationStatus OrderFollowUpForms(Order order);

        bool HasFollowupForm(Order order);

        IQueryable<OrderItemFollowupForm> GetOrderItemFollowupForms(long orderIsts,
            OrderItemStatusReasons itemStatusReasons, long orderItemId = 0);

        OrderItemFollowupForm CreateOrderItemFollowUpForm(ClubFormTemplate form, OrderItem orderItem);

        OperationStatus Update(OrderItemFollowupForm form);

        SupplementalFormDetailViewModel GetSupplementalForm(int formId);

        OperationStatus ChangeFollowUpFormMode(int formId);

        bool AssignedTemplateToPrograms(int id, int clubId);

        IQueryable<ClubFormTemplate> GetProgramClubFormTemplateFromOrderItem(List<OrderItem> orderItems);

        OrderItemFollowupForm CreateOrderItemMultiRegisterFollowUpForm(JbForm form, ClubFormTemplate clubFormTemplate, OrderItem orderItem, int formId);

        string GetFormProgramsName(long clubFormTemplateId, List<long> programIds);

        string GetNamesOfProgramsThatAssignedTemplate(int clubFormTemplateId, int clubId);

        
    }
}
