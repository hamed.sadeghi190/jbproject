﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolReconciliationInfoReportViewModel
    {
        public string SchoolName { get; set; }

        public string IsCashPayments { get; set; }

        public string IsDonations { get; set; }

        public string IsPTAFees { get; set; }

        public string IsPTAScholarships { get; set; }

        public DateTime? AgreementExpirationDate { get; set; }
    }
}