﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('DefaultReportRunController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();


        if ($stateParams.reportName == 'RegistrationForm' || $stateParams.reportName == 'FollowupForm') {
            $scope.MC.openBlade({ url: "Dashboard/Blade/RegistrationDataReports", order: 3, menu: "SEASONS" });
        }
        else if ($stateParams.reportName == 'Balance' || $stateParams.reportName == 'Finance' || $stateParams.reportName == 'ChargeDiscount' || $stateParams.reportName == 'Installment' || $stateParams.reportName == 'Coupon' || $stateParams.reportName == 'Provider Financial' || $stateParams.reportName == 'Coordinator Financial') {
            $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReport", order: 3, menu: "SEASONS" });
        }

        $scope.MC.stateParams = $stateParams;


        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var reportName = $scope.MC.stateParams.reportName;

        if ($stateParams.reportType == 'Parent') {

            $http.get(baseUrl + '/GetAllSeasonProgramsCustomReport', { params: { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: reportName } })
                .success(function (data, status, headers, config) {
                    $scope.Model = data;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch('Model.AllPrograms', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });

        } else {

            $http.get(baseUrl + '/GetAllSeasonPrograms', { params: { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: reportName } })
                .success(function (data, status, headers, config) {
                    $scope.Model = data;

                });
        }

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };

        $scope.runReport = function () {
            $scope.Model.ReportId = $scope.MC.stateParams.reportId;
            $scope.Model.SeasonDomain = $scope.MC.stateParams.seasonDomain;
            $scope.programs = $scope.Model._SelectedPrograms;
            $scope.Model.ReportName = $scope.MC.stateParams.reportName;
            $scope.isAllPrograms = $scope.Model.IsAllPrograms;
            $scope.date = $scope.Model.DateProgram;
            customReportService.setModelInStorage($scope.Model);

            switch (reportName) {
                case "Capacity":
                    {
                        $state.go('CapacityReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }

                case "Installment":
                    {
                        $state.go('InstallmentReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }

                case "FollowupForm":
                    {
                        $state.go('FollowupReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }

                case "Balance":
                    {
                        $state.go('BalanceReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }

                case "ChargeDiscount":
                    {
                        $state.go('ChargesReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "PreRegistration":
                    {
                        $state.go('PreRegistrationRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "InstructorCheckin":
                    {
                        $state.go('CheckinReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "SignOutSheet":
                    {
                        $state.go('SignOutSheetReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "DismissalInformation":
                    {
                        $state.go('DismissalInformationReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "StudentAttendance":
                    {
                        $state.go('StudentAttendanceReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "StudentSummaryAttendance":
                    {
                        $state.go('StudentSummaryAttendanceReport', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "CustomAttendance":
                    {
                        $state.go('CustomAttendanceReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "Roster":
                    {
                        $state.go('RosterReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "AdvanceRoster":
                    {
                        $state.go('AdvanceClassRosterReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "SubscriptionRoster":
                    {
                        $state.go('SubscriptionRosterReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "SignOut":
                    {
                        $state.go('SubscriptionSingOutReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "SubscriptionInternal":
                    {
                        $state.go('SubscriptionInternalReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "SubscriptionAttendance":
                    {
                        $state.go('SubscriptionAttendanceReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "InstructorRoster":
                    {
                        $state.go('InstructorRosterReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "Provider Financial":
                    {
                        $state.go('ProviderFinancialRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "Coordinator Financial":
                    {
                        $state.go('CoordinatorFinancialRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "ParentEmailsList":
                    {
                        $state.go('ParentEmailsListReport', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "RegistrationForm":
                    {
                        cfpLoadingBar.start();
                        $http.post(baseUrl + '/RegistrationFormReportRun', { model: $scope.Model })
                            .success(function (data, status, headers, config) {
                                var location = window.location.href;
                                cfpLoadingBar.complete();
                                window.location.href = baseUrl + '/Downloadreport?fileName=' + data.Data;
                                window.location.href = location;
                            });
                        break;
                    }

                /* Use default case for "Custom Reports" */
                default:
                    $state.go('ReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportId: $scope.MC.stateParams.reportId, reportName: $scope.MC.stateParams.reportName, reportType: $scope.MC.stateParams.reportType });
            }
        }

        $scope.CheckValidation = function () {

            //var obj = { key: "model.DateProgram"};

            $scope.runReport();
        }


    }]);
})();