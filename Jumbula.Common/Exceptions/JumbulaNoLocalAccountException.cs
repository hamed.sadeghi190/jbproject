﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaNoLocalAccountException : JumbulaAccountException
    {
        public JumbulaNoLocalAccountException(Exception e)
            : base(e)
        {
        }
    }
}
