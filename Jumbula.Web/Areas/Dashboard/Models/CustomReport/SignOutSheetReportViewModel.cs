﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models.CustomReport
{
    public class SignOutSheetReportViewModel
    {
        public string ParticipantFirstName { get; set; }

        public string ParticipantLastName { get; set; }

        public string ParentFirstName { get; set; }

        public string ParentLastName { get; set; }

        public string ParentPrimaryPhone { get; set; }

        public string ParentAuthorizedAdult { get; set; }

        public string ParentAuthorizedAdultPhone { get; set; }
    }
}