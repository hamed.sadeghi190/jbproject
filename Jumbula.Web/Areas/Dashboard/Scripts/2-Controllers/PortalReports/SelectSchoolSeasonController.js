﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SelectSchoolSeasonController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'select-county-view';
            window.app.globalObjects.initializeView();

            $scope.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            $scope.MC.stateParams = $stateParams;

            $scope.reportName = "";

            if ($scope.stateParams.reportType) {
                switch ($scope.stateParams.reportType) {
                    case "ProviderConatact":
                        {
                            $scope.reportName = "Provider contact info";
                            break;
                        }
                    case "RoomAssignment":
                        {
                            $scope.reportName = "Room assignment";
                            break;
                        }
                    case "ADM24Data":
                        {
                            $scope.reportName = "ADM24 Data";
                            break;
                        }
                    case "InstructorAssignments":
                        {
                            $scope.reportName = "Instructor assignments";
                            break;
                        }
                }
            }

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.AllSchools = [];
            $scope.AllSeasons = [];
            $scope.Model = {};
            var timer = false;

            $scope.$watch('AllSchools', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $scope.$watch('AllSeasons', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get('Dashboard/PortalReport/GetAllSchool').success(function (data, status, headers, config) {
                $scope.AllSchools = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.fillSeasonData = function (schoolId) {
                $http.get('Dashboard/PortalReport/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {
                    $scope.AllSeasons = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }

            var checkValidation = function (schoolId, seasonId) {
                if (is.undefined(schoolId)) {
                    jbToast.error("Please select a school.");
                    return false;
                }
                else if (is.undefined(seasonId)) {
                    jbToast.error("Please select a season.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {
                if (checkValidation($scope.Model.SchoolId, $scope.Model.SeasonId)) {
                    customReportService.saveSchoolSeasonId($scope.Model.SchoolId, $scope.Model.SeasonId);
                    switch ($scope.stateParams.reportType) {
                        case "ProviderConatact":
                            {
                                $state.go('ProviderContactInfo', { seasonId: $scope.Model.SeasonId });
                                break;
                            }
                        case "RoomAssignment":
                            {
                                $state.go('RoomAssignment', { seasonId: $scope.Model.SeasonId });
                                break;
                            }
                        case "ADM24Data":
                            {
                                $state.go('ADM24Data', { seasonId: $scope.Model.SeasonId });
                                break;
                            }
                        case "InstructorAssignments":
                            {
                                $state.go('InstructorAssignmentsReports', { seasonId: $scope.Model.SeasonId });
                                break;
                            }
                    }
                }
            }
        }
    ]);

})();