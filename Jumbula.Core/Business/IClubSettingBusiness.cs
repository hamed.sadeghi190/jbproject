﻿using Jumbula.Common.Enums;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.ClubSetting;

namespace Jumbula.Core.Business
{
    public interface IClubSettingBusiness
    {
        AutoChargePolicySettingViewModel GetAutoChargePolicy(int clubId);
        AutoChargePolicy ReadAutoChargePolicy(Club club);
        OperationStatus SaveAutoChargePolicy(AutoChargePolicySettingViewModel model, int clubId);

        ProgramsPolicyViewModel GetProgramsPolicy(int clubId);

        OperationStatus SaveSubscriptionProgramPolicy(ProgramsPolicyViewModel model, int clubId);

        OperationStatus SaveGeneralProgramsPolicy(ProgramsPolicyViewModel model, int clubId);

        bool GetCancellationFeePolicy(int clubId);

        bool GetTransferFeePolicy(int clubId);

        bool GetSubscriptionCancellationPolicy(int clubId);

        ProrationMethod GetProrationMethod(int clubId);

        bool GetSubscriptionChangeDesiredPolicy(int clubId);

        TransferOrderPolicy GetSubscriptionTransferPolicy(int clubId);
        

        DelinquentPolicySettingViewModel GetDelinquentPolicy(int clubId);
        OperationStatus SaveDelinquentPolicy(DelinquentPolicySettingViewModel model, int clubId);
        DelinquentPolicy ReadDelinquentPolicy(int clubId);
        bool GetRefundAutomatically(int clubId);

        
        DuplicateEnrollmentPolicySettingViewModel GetDuplicateEnrollmentPolicy(int clubId);
        OperationStatus SaveDuplicateEnrollmentPolicy(DuplicateEnrollmentPolicySettingViewModel model, int clubId);

        EmailsSettingViewModel GetEmailsContents(int clubId);
        ReportsSettingViewModel GetReportsContents(int clubId);
        CatalogsSettingViewModel GetCatalogContents(int clubId);
        OperationStatus SaveEmailsContents(EmailsSettingViewModel model, int clubId);
        OperationStatus SaveReportsContents(ReportsSettingViewModel model, int clubId);
        OperationStatus SaveCatalogContents(CatalogsSettingViewModel model, int clubId);
    }
}
