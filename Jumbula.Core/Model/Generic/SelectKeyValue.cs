﻿namespace Jumbula.Core.Model.Generic
{
    //[Obsolete("TODO we should delete this type")]
    public class SelectKeyValue<T>
    {
        public string Group { get; set; }

        public string Text { get; set; }

        public T Value { get; set; }
    }
}
