﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemConfirmDebitForClubController', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', 'cancelOrderService', 'DebitforClubService', function ($scope, $http, jbToast, $log, $stateParams, $state, cancelOrderService, DebitforClubService) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.Model = cancelOrderService.get();
        $scope.Model = DebitforClubService.get();


        $scope.SubmitAddDebit = function () {

            $http.post($scope.baseUrl + '/CreateDebit', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        jbToast.success("Your operation was successful.", "");
                        $state.go('SystemClubBillingHistory', { clubId: $stateParams.clubId });

                    }
                    else {

                        $scope.MC.handleErrors($scope, data);
                        jbToast.error("You failed operations.");
                    }
                });
        };
        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    $state.go("AddDebitForClub", { clubId: $stateParams.clubId });
                }
            }
            else {
                $state.go("SystemClubBillingHistory", { clubId: $stateParams.clubId });
            }
        }

    }]);
})();