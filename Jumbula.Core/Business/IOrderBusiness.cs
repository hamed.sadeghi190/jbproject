﻿using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;
using Jumbula.Core.Model.Order;

namespace Jumbula.Core.Business
{
    public interface IOrderBusiness
    {
        int CartItems(int userId, int clubId);
        Order Get(long orderId);
        Order Get(string confirmationId);
        Order Get(long orderId, params Expression<Func<Order, object>>[] includes);
        Order GetOrderInCart(int userId, int clubId);
        IQueryable<Order> GetList();
        IQueryable<Order> GetAllOrders();
        IQueryable<OrderChargeDiscount> GetChargeDiscounts(long orderItemId);
        OperationStatus Create(Order order);
        OperationStatus DeleteSessions(List<OrderSession> sessions);
        OperationStatus Update(Order order);
        Order GetOrdersByConfirmationID(string confirmationID);
        JbForm GetLastOrderRegForm(int userId, int? playerId, long programId);
        JbForm GetLastOrderRegForm(int userId, int? playerId);
        IQueryable<Order> GetClubUserAllOrders(int userId, int clubId);
        Order GetLastUserOrder(int userId, int clubId);

        Order GetLastUserOrder(string userName, List<int> clubIds);

        OperationStatus Delete(long orderId);

        decimal CalculateOrderPayableAmount(Order order);
        OperationStatus BaseCreate(Order order);
        IQueryable<OrderItem> GetClubUserOrderItems(int userId, int playerId, int clubId);

        void SetPaymentTransaction(Order order, PaymentDetail paymentDetail, TransactionStatus transactionStatus,
            OrderInstallment installment = null, DateTime? date = null, decimal amount = -1, OrderItem orderItem = null,
            long? instId = null, string note = "", string token = "", string checkId = "",
            TransactionCategory tranCategory = TransactionCategory.Sale, HandleMode handleMode = HandleMode.Online);

        OperationStatus Transfer(long sourceOrderItemId, OrderItem targetOrderItem, int userId, string transferMemo);
        void PayOrderByCredit(int? userId, Cart cart, string orderHistoryDescription, DateTime date, decimal amount);

        Order GetLastOfflineOrderForClub(int clubId, string clubDomain = "");
        bool DoesOfflineOrderExist(int clubId, string clubDomain = "");

        OperationStatus Update(Order order, List<OrderItem> newOrderItems);

        IQueryable<Order> GetUserOrders(int userId);

        void UpdateLiveTestMode(Order order);

        void OrderCompleted(Order order, string orderHistoryDescription, int? userId, int CurrentUserId);

        int? GetOrderClubIdFromItems(List<OrderItem> orderItems);
        IQueryable<Order> GetAllOrdersNeedCreditCard();

        Order GetLastUserOrder(int userId);
        bool IsTakePaymentOrderIsAutoCharge(Order order);

        bool IsUserOwnsTheOrder(int currentUserId, int orderUserId, string clubDomain, string outSourceClubDomain);

        bool UserHasAnyOrderOnClub(int userId, int clubId);
        bool UserHasAnyOrderOnPartnerClub(int userId, int partnerId);
        string GenerateConfirmationId();
        OperationStatus SeparateOrderItemFromCartOrder(Order order, long separatedOrderItemId);
        RefundResultModel Refund(OrderItemsViewModel model);
    }
}
