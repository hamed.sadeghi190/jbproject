﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('accountSettingsPlanController', ['$scope', '$http', 'jbToast', '$state','$window', function ($scope, $http, jbToast, $state,$window) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();
        $scope.baseUrl = window.location.origin + "/Dashboard/AccountSetting";
        $scope.MC.openBlade({ url: "Dashboard/Blade/AccountSettings", order: 2, menu: "SETTINGS" });
        $scope.selectedItem = '';
        $('#plansContainer .plan-box').click(function () {
            $('#plansContainer .plan-box').removeClass('active');
            $(this).addClass('active');
            $scope.selectedItem = $(this).attr('id');
          
        })

        $scope.changePlan = function () {

            if ($scope.selectedItem == 'payAsYouGo')
            {
                $http.post($scope.baseUrl + '/ChangePlan')
              .success(function (data, status, headers, config) {
                  if (data.Status) {
                      if (data.Message == "NeedCredit") {
                          $state.go('AccountSettingManageBilling', { type: 'PayAsYouGo' });
                      }
                      else {
                          jbToast.success("Your plan changed successfully.", "");
                          $state.go('AccountSettings');
                      }
                  }
                  else {
                      jbToast.error(data.Message);
                  }
              });


                
            }
            else if ($scope.selectedItem == 'subscription') {
                $state.go('SubscriptionSetting');
            }
            else if ($scope.selectedItem == 'enterprise') {
                $http.post($scope.baseUrl + '/SendEnterpriseEmail')
              .success(function (data, status, headers, config) {
                  if (data.Status) {
                      jbToast.success("We will contact you for pricing.", "");
                      if (data.Message == "NeedCredit") {
                          $state.go('AccountSettingManageBilling', { type: 'Add' });
                      }
                      else {
                         
                          $state.go('AccountSettings');
                      }
                  }
                  else {
                      jbToast.error(data.Message);
                  }
              });
            }
            else {
                jbToast.error("Please select your plan");
            }
          
        };
    }])

})();