﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('orderItemController', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }
        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.Model = {};
        $scope.Model.Id = $stateParams.orderItemId;

        $scope.ShowForms = false;
        $scope.RemoveBtn_ListForms = false;

        $scope.ShowListForms = function (mode) {

            if (mode) {
                $scope.RemoveBtn_ListForms = true;
                $scope.ShowForms = true;
            } else {
                $scope.ShowForms = false;
            }


        }

        $scope.ShowAnotherOrderList = false;
        $scope.ShowAnotherOrder = function (mode) {

            if (mode) {
                $scope.ShowAnotherOrderList = true;
            } else {
                $scope.ShowAnotherOrderList = false;
            }


        }

        $scope.CancelOrderList = function () {
            $scope.ShowAnotherOrderList = false;
            $scope.Model.SelectedOrders = null;
        }

        $scope.SetFormToAnotherOrder = function () {

            if ($scope.Model.SelectedOrders == null) {
                jbToast.error("Please select orders.");
            } else {

                jbConfirmation
                    .yes(function () {

                        $http.post(window.location.origin + '/Dashboard/Order/SetNewFormToOrders?orderItemId=' + $scope.Model.Id + "&selectedOrders=" + $scope.Model.SelectedOrders)
                            .success(function (data, status, headers, config) {
                                if (data.Status) {
                                    $scope.ShowAnotherOrderList = false;
                                    $scope.Model.SelectedOrders = null;
                                    jbToast.success("Reassigned registration form successfuly.");
                                }
                                else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });

                    })
                    .title('Reassign')
                    .message('Are you sure you want to replace the registration data of the selected orders with this data?')
                    .confirm();
            }
        }

        $scope.ChangeForm = function () {

            $state.go('OrderItem', ({ seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $scope.Model.Id, changeFormId: $scope.Model.SelectedForm }));

        }

        $scope.IsChessEditModel = false;

        $scope.EditChessInformation = function () {
            $scope.IsChessEditModel = true;
        }

        $scope.cancelListForms = function () {
            $scope.Model.UpdateForm = false;
            $scope.Model.Editable = false;
            $scope.ShowForms = false;
            $scope.RemoveBtn_ListForms = false;
            $http.get(window.location.origin + '/Dashboard/Order/GetOrderItem/?orderItemId=' + $scope.Model.Id + "&editable=" + false).success(function (data, status, headers, config) {
                $scope.Model = data;
                $scope.Model.Editable = false;
                $state.go('OrderItem', ({ seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $scope.Model.Id, changeFormId: $scope.Model.SelectedForm }));
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $scope.loadModel = function (editable) {
            if (editable) {
                $scope.MC.stateParams.changeFormId = 0;
            }

            $http.get(window.location.origin + '/Dashboard/Order/GetOrderItem/?orderItemId=' + $scope.Model.Id + "&changeFormId=" + $scope.MC.stateParams.changeFormId + "&editable=" + editable).success(function (data, status, headers, config) {
                $scope.Model = data;
                $scope.Model.Editable = editable;  

                if ($scope.MC.stateParams.changeFormId > 0) {
                    $scope.ShowForms = true;
                } else {
                    $scope.ShowForms = false;
                }

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $scope.sendReminder = function (installmentId) {

            $http.post(window.location.origin + '/Schedule/InstallmentReminderById/?installmentId=' + installmentId)
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        jbToast.success("The reminder sent successfully.");
                        $scope.mainGridOptions.dataSource.read();
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.punchCardSessionsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: window.location.origin + '/Dashboard/Order/GetPunchCardSessions/?orderItemId=' + $scope.Model.Id,
                        type: "POST",
                    },

                },
            schema: {
                total: "total",
                data: "data"
            },
            serverSorting: false,
            batch: true,
            resizable: false,
        });

        $scope.punchCardSessionsGridOptions = {
            dataSource: $scope.punchCardSessionsDataSource,
            resizable: false,
            columns: [
                {
                    field: "Title",
                    title: "Day",
                    width: "100px",
                },
                {
                    title: "Date",
                    template: "#=StrDateTime# #if(HasTwoSessionInADay){# <br/> #=StrDateTime2# #}#",
                },
                {
                    title: "Program",
                    template: "#=BeforeAfter# #if(HasTwoSessionInADay){# <br/> #=BeforeAfter2# #}#",
                },
                {
                    field: "StrDateCreated",
                    title: "Assigned date",
                },
                {
                    title: "Actions",
                    width: "70px",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
						            <li> ...\
                                        <ul>\
                                          #if(Status == 'UnAssigned'){#\
                                            <li class='k-item k-state-default k-first' role='menuitem'>\
                                                <span ng-click='assign(#=Id#)' class='k-link jbi-angle-right'>Assign</span>\
                                            </li>\
                                          #}\
                                          else{#\
                                            <li class='k-item k-state-default k-first' role='menuitem'>\
                                                <span ng-click='unassign(#=Id#)' class='k-link jbi-angle-right'>Unassign</span>\
                                            </li>\
                                            <li class='k-item k-state-default k-first' role='menuitem'>\
                                                <span ng-click='reassign(#=Id#)' class='k-link jbi-angle-right'>Reassign</span>\
                                            </li>\
                                          #}#\
                                        </ul>\
                                    </li>\
                                </ul>"
                }
            ],
        }

        $scope.assign = function (id) {
            $state.go('OrderItemPunchCardAssignment', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $stateParams.orderItemId, sessionId: id })
        }

        $scope.reassign = function (id) {
            $state.go('OrderItemPunchCardAssignment', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $stateParams.orderItemId, sessionId: id })
        }

        $scope.unassign = function (id) {

            jbConfirmation
                .yes(function () {

                    $http.post('/Dashboard/Order/UnassignPunchCardSession', { orderItemId: $stateParams.orderItemId, sessionId: id })
                        .success(function (data, status, headers, config) {
                            if (data.Status) {
                                jbToast.success('Unassign done successfully.');

                                $scope.punchCardSessionsGridOptions.dataSource.read();
                            }
                            else {
                                jbToast.error(data.Message);
                            }
                        });

                })
                .title('Unassign')
                .message('Are you sure you want to unassign this item?')
                .confirm();


        }

        $scope.UnancelOrder = function () {

            jbConfirmation
                .yes(function () {

                    $http.post('/Dashboard/Order/UncancelOrder', { orderItemId: $stateParams.orderItemId })
                        .success(function (data, status, headers, config) {
                            if (data.Status) {

                                $http.get(window.location.origin + '/Dashboard/Order/GetOrderItem/?orderItemId=' + $scope.Model.Id + "&editable=" + false).success(function (data, status, headers, config) {
                                    $scope.Model = data;
                                    jbToast.success('Uncancel done successfully.');
                                }).error(function (data, status, headers, config) {
                                    jbToast.error("error " + data);
                                });
                            }
                            else {
                                jbToast.error(data.Message);
                            }
                        });

                })
                .title('Uncancel')
                .message('Are you sure you want to uncancel this order?')
                .confirm();
        }

        $scope.orderItemTransactionsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: window.location.origin + '/Dashboard/Order/GetOrderItemTransactions/?orderItemId=' + $scope.Model.Id,
                        type: "POST",
                    },
                },
            schema: {
                total: "total",
                data: "data"
            },
            serverSorting: false,
            batch: true,
            resizable: false,
        });

        $scope.orderItemTransactionsGridOptions = {
            dataSource: $scope.orderItemTransactionsDataSource,
            sortable: true,
            resizable: false,
            dataBound: function () {
                var grid = $('#orderItemTransactions').data('kendoGrid');
                if (grid.dataSource.data().length > 0) {
                    //if (grid.dataSource.data()[0].PaymentPlan == '0') {
                    //    grid.hideColumn(7);
                    //}
                }
            },
            columns: [
                {
                    field: "StrTransactionDate",
                    title: "Date",
                    width: "90%"
                },
                {
                    field: "StrAmount",
                    title: "Amount",
                    width: "80%"
                },
                {
                    field: "StrPaymentMethod",
                    title: "Method",
                    width: "100%"
                },
                {
                    field: "StrCategory",
                    title: "Category",
                    width: "110%"

                },
                {
                    field: "Payer",
                    title: "Agent",
                    width: "80%"

                },
                {
                    field: "StrTransactionStatus",
                    title: "Status",
                    width: "100%",
                },
                {
                    title: "Actions",
                    width: "45%",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
						    <li> ...\
                         #if(HandleMode == 1 && TransactionCategory == 9){#\
                                <ul>\
                                   <li class='k-item k-state-default k-first' role='menuitem'>\
                                           <span ng-click=\"viewDetails('#=ErrorMessage#','#=Note#','#=InvoiceCreateDate#','#=InvoiceDueDate#','#=InvoicePaidDate#','#=TransactionCategory#','#=PaymentMethod#','#=StrTransactionStatus#','#=InvoiceNumber#','#=StrTransactionDatePopup#','#=CardNumber#','#=TransactionId#','#=StrAmount#')\" class=\"k-link jbi-angle-right\">View details</span>\
                                    </li>\
                                   #if(PaymentDetail.PayerType == 2){#\
                                    <li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span ng-click='editPayment(#=ProgramId#,#=PaymentDetailId#)' class='k-link jbi-angle-right'>Edit</span>\
                                    </li>\
                                    <li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span ng-click='deleteTransaction(#=Id#)' class='k-link jbi-angle-right'>Delete</span>\
                                    </li>\
                                      #}#\
                                </ul>\
                     	     #}else{#\
                              <ul>\
                                    <li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span ng-click=\"viewDetails('#=ErrorMessage#','#=Note#','#=InvoiceCreateDate#','#=InvoiceDueDate#','#=InvoicePaidDate#','#=TransactionCategory#','#=PaymentMethod#','#=StrTransactionStatus#','#=InvoiceNumber#','#=StrTransactionDatePopup#','#=CardNumber#','#=TransactionId#','#=StrAmount#')\" class=\"k-link jbi-angle-right\">View details</span>\
                                    </li>\
                                #if(EnableUndoRefund){#\
                                    <li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span ng-click=\"UndoRefundFromFamilyCreditToOrder(#=Id#)\" class=\"k-link jbi-angle-right\">Undo transfer</span>\
                                    </li>\
                           #}#\
                                </ul>\
                            #}#\
                            </li>\
                    </ul>"
                }
            ],
        }

        $scope.showPaymentHistoryActions = false;

        $scope.viewDetails = function (errorMessage, note, invoiceCreateDate, invoiceDueDate, invoicePaidDate, category, paymentMethod, status, invoiceNumber, StrTransactionDatePopup, CardNumber, transactionId, amount) {

            $scope.showPaymentHistoryActions = true;
            $scope.ErrorMessage = errorMessage != "" && errorMessage != 'null' ? errorMessage : "";
            $scope.Memo = note != "" && note != 'null' ? note : "";
            $scope.category = category;
            $scope.paymentMethod = paymentMethod;
            $scope.status = status;
            $scope.StrTransactionDatePopup = StrTransactionDatePopup;
            $scope.InvoiceCreateDate = invoiceCreateDate;
            $scope.InvoiceDueDate = invoiceDueDate;
            $scope.InvoicePaidDate = invoicePaidDate;
            $scope.InvoiceNumber = invoiceNumber;
            $scope.CardNumber = CardNumber;
            $scope.TransactionId = transactionId != 0 ? transactionId : "";
            $scope.Amount = amount;
        }

        $scope.dismissHistoryPopup = function () { $scope.showPaymentHistoryActions = false; }

        $scope.UndoRefundFromFamilyCreditToOrder = function (id) {

            $state.go("UndoFamilyCredit", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId, transacTionId: id });
        }

        $scope.orderItemInstallmentsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: window.location.origin + '/Dashboard/Order/GetOrderItemInstallments/?orderItemId=' + $scope.Model.Id,
                        type: "POST",
                    },

                },
            schema: {
                total: "total",
                data: function (response) {
                    return response.data;
                }
            },
            serverSorting: false,
            batch: true,
            resizable: false,
        });

        $scope.orderItemInstallmentsGridOptions = {
            dataSource: $scope.orderItemInstallmentsDataSource,
            sortable: true,
            resizable: false,
            dataBound: function () {
                var grid = $('#orderItemInstallments').data('kendoGrid');
                if ($scope.Model.ProgramType != 'Subscription' && $scope.Model.ProgramType != 'BeforeAfterCare') {
                    grid.hideColumn(0);
                }
            },
            columns: [
                {
                    field: "Schedule",
                    title: "Schedule",
                    template: "# if(IsDeleted){# <span class='line-through'> #=Schedule# </span> #}else{# #=Schedule# #}#",
                    width: "140%"
                },
                {
                    field: "InstallmentDueDate",
                    title: "Due date",
                    template: "# if(IsDeleted){# <span class='line-through'> #=InstallmentDueDate# </span> #}else{# #=InstallmentDueDate# #}#",
                    width: "145%"
                },
                {
                    field: "StrPaidDate",
                    title: "Paid date",
                    width: "100%"
                },
                {
                    field: "StrAmount",
                    title: "Amount",
                    width: "112%"
                },
                {
                    field: "StrPaidAmount",
                    title: "Paid",
                    width: "110%"
                },
                {
                    field: "StrBalance",
                    title: "Balance",
                    width: "250%"
                },
                {
                    field: "InstallmentStatus",
                    title: "Status",
                    width: "170%"
                },
                {
                    field: "",
                    title: "Actions",
                    width: "70%",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                         #if(Balance >0 && Status!=4){#\
                        <li> ... \
                            <ul>\
                                  <li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span ng-click=\"viewInstallmentDetails('#=Id#','#=InstallmentStatus#')\" class='k-link jbi-angle-right'>View details</span>\
                                    </li>\
                                <li ng-click='sendReminder(#=Id#)' class='k-item k-state-default k-first' role='menuitem'>\
                                    <span class='k-link jbi-email-template'>Send reminder</span>\
                                </li>\
                           #if(Status == 8 && Balance >0){#<li ng-click='UndoSuspension(#=Id#)' class='k-item k-state-default k-first' role='menuitem'>\
                                    <span class='k-link jbi-pencil'>Undo Suspension</span>\
                                </li>\ #}#\
                            </ul>\
                        </li>\
                        #}else{#    <li> ... \
                                   <ul>\
                                    <li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span ng-click=\"viewInstallmentDetails('#=Id#','#=InstallmentStatus#')\" class='k-link jbi-angle-right'>View details</span>\
                                    </li>\
                    #if(RefundAmount > 0) {#<li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span title='Refund' ng-click=\"installmentRefund('#=Id#')\" class='k-link jbi-angle-right'>Refund</span>\
                                    </li>\ #}#\
                                </ul>\
                     </li>\#}#\
                    </ul>"
                }
            ],
        }

        $scope.orderItemAddOnInstallmentsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: window.location.origin + '/Dashboard/Order/GetOrderItemAddOnInstallments/?orderItemId=' + $scope.Model.Id,
                        type: "POST",
                    },

                },
            schema: {
                total: "total",
                data: function (response) {
                    return response.data;
                }
            },
            serverSorting: false,
            batch: true,
            resizable: false,
        });

        $scope.orderItemAddOnInstallmentsGridOptions = {
            dataSource: $scope.orderItemAddOnInstallmentsDataSource,
            sortable: true,
            resizable: false,
            dataBound: function () {
                var grid = $('#orderItemAddOnInstallments').data('kendoGrid');
                //if ($scope.Model.ProgramType != 'Subscription' && $scope.Model.ProgramType != 'BeforeAfterCare') {
                //    grid.hideColumn(0);
                //}
            },
            columns: [
                {
                    field: "InstallmentDueDate",
                    title: "Due date",
                    template: "# if(IsDeleted){# <span class='line-through'> #=InstallmentDueDate# </span> #}else{# #=InstallmentDueDate# #}#",
                    width: "90%"
                },
                {
                    field: "StrPaidDate",
                    title: "Paid date",
                    width: "70%"
                },
                {
                    field: "StrAmount",
                    title: "Amount",
                    width: "70%"
                },
                {
                    field: "StrPaidAmount",
                    title: "Paid",
                    width: "70%"
                },
                {
                    field: "StrBalance",
                    title: "Balance",
                    width: "80%"
                },
                {
                    field: "Category",
                    title: "Category",
                    width: "90%"
                },
                {
                    field: "InstallmentStatus",
                    title: "Status",
                    width: "70%"
                },
                {
                    field: "",
                    title: "Actions",
                    width: "80%",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                         #if(Balance >0 && Status!=4){#\
                        <li> ... \
                            <ul>\
                      <li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span ng-click=\"viewInstallmentDetails('#=Id#','#=InstallmentStatus#')\" class='k-link jbi-angle-right'>View details</span>\
                                    </li>\
                      <li ng-click='sendReminder(#=Id#)' class='k-item k-state-default k-first' role='menuitem'>\
                                    <span class='k-link jbi-email-template'>Send reminder</span>\
                                </li>\
                           #if(Status == 8 && Balance >0){#<li ng-click='UndoSuspension(#=Id#)' class='k-item k-state-default k-first' role='menuitem'>\
                                    <span class='k-link jbi-pencil'>Undo Suspension</span>\
                                </li>\ #}#\
                            </ul>\
                        </li>\
                        #}else{#    <li> ... \
                                   <ul>\
                                    <li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span ng-click=\"viewInstallmentDetails('#=Id#','#=InstallmentStatus#')\" class='k-link jbi-angle-right'>View details</span>\
                                    </li>\
                    #if(RefundAmount > 0) {#<li class='k-item k-state-default k-first' role='menuitem'>\
                                        <span title='Refund' ng-click=\"installmentRefund('#=Id#')\" class='k-link jbi-angle-right'>Refund</span>\
                                    </li>\ #}#\
                                </ul>\
                     </li>\#}#\
                    </ul>"
                }
            ],
        }

        $scope.installmentRefund = function (id) {
            window.comesFromOrderItem = true;
            $state.go("PartiallyRefundOrderItem", { seasonDomain: $stateParams.seasonDomain, programId: $scope.Model.ProgramId, orderItemId: $stateParams.orderItemId, installmentId:id });
        }

        $scope.showInstallmentDetails = false;


        $scope.isInitGrid = false;
        $scope.total = 0;
        $scope.viewInstallmentDetails = function (id, status) {
            $scope.showInstallmentDetails = true;

            $scope.installmentId = id;
            $scope.status = status;

            $scope.GetInstallmentTransactions();

        }

        $scope.GetInstallmentTransactions = function () {

            if (!$scope.isInitGrid) {

                $scope.isInitGrid = true;

                $scope.installmentTransactionsDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport:
                        {
                            read: {
                                url: window.location.origin + '/Dashboard/Order/GetOrderItemTransactions',
                                type: "POST",
                                data: function (data) {
                                    return {
                                        orderItemId: $stateParams.orderItemId,
                                        installmentId: $scope.installmentId,
                                    };
                                },
                            },

                        },
                    schema: {
                        total: function (response) {
                            $scope.total = response.total;

                            return response.TotalCount;
                        },
                        data: "data"
                    },
                    serverSorting: false,
                    batch: true,
                    resizable: false,


                });
                $scope.installmentTransactionsGridOptions = {

                    dataSource: $scope.installmentTransactionsDataSource,
                    sortable: true,
                    resizable: false,
                    columns: [
                        {
                            field: "StrTransactionDate",
                            title: "Date",
                            width: "80px",
                        },
                        {
                            field: "StrAmount",
                            title: "Amount",
                            width: "80px",
                        },
                        {
                            field: "StrPaymentMethod",
                            title: "Method",
                            width: "90px",
                        },
                        {
                            field: "CardNumber",
                            title: "Card number",
                            width: "160px",
                        },
                        {
                            field: "TransactionId",
                            title: "Transaction ID",
                            width: "200px",
                            template: "<span title='#=TransactionId#' style='display: inline-block;max-width: 260px;min-width: 160px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;'>#=TransactionId#</span>"
                        },
                        {
                            field: "StrCategory",
                            title: "Category",
                            width: "90px",
                        },
                        {
                            field: "Payer",
                            title: "Agent",
                            width: "70px",
                        },
                        {
                            field: "StrTransactionStatus",
                            title: "Status",
                            width: "70px",
                        },
                    ],
                }
            }
            else {
                $scope.installmentTransactionsGridOptions.dataSource.data([]);
                $scope.installmentTransactionsGridOptions.dataSource.read();
            }
        }
        $scope.dismissInstallmentPopup = function () { $scope.showInstallmentDetails = false; }

        $scope.UndoSuspension = function (id) {
            jbConfirmation
                .yes(function () {
                    $http.post('/Dashboard/Order/UndoSuspension', { installmentId: id })
                        .success(function (data, status, headers, config) {
                            if (data.Status) {
                                jbToast.success("undo suspension successful");
                                $scope.orderItemInstallmentsGridOptions.dataSource.read();

                            }
                            else {
                                jbToast.error(data.Message);
                            }
                        });
                })
                .confirm();
        }

        $scope.SaveForm = function () {
            if (!$scope.Model.Editable) {
                jbConfirmation
                    .yes(function () {
                        if ($("#orderJbForm").valid()) {
                            var formData = form2js('orderJbForm', '.', true);
                            $http.post('/Dashboard/Order/EditOrderForm', { formData: formData, orderItemId: $scope.Model.Id })
                                .success(function (data, status, headers, config) {
                                    if (data.Status) {
                                        $http.get(window.location.origin + '/Dashboard/Order/GetOrderItem/?orderItemId=' + $scope.Model.Id + "&editable=" + false).success(function (data, status, headers, config) {
                                            $scope.Model = data;
                                            $scope.Model.Editable = false;
                                            $scope.ShowForms = false;
                                            jbToast.success('The registrant information has been updated successfully.');

                                            $state.go('OrderItem', ({ seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $scope.Model.Id, changeFormId: $scope.Model.SelectedForm }));

                                        }).error(function (data, status, headers, config) {
                                            jbToast.error("error " + data);
                                        });
                                    }
                                    else {
                                        jbToast.error(data.Message);
                                    }
                                });
                        }
                        else {
                            jbToast.error("Data invalid or missing,please enter valid form data.");
                        }
                    })
                    .title('Warrning')
                    .message('You need to double check the new form to ensure it contains all the data from the previous order. Once you save the new form, any data that is not in this form will be lost. Are you sure you want to save the new form?')
                    .confirm();
            }
            else {
                if ($("#orderJbForm").valid()) {
                    var formData = form2js('orderJbForm', '.', true);
                    $http.post('/Dashboard/Order/EditOrderForm', {
                        formData: formData, orderItemId: $scope.Model.Id
                    })
                        .success(function (data, status, headers, config) {
                            if (data.Status) {
                                $http.get(window.location.origin + '/Dashboard/Order/GetOrderItem/?orderItemId=' + $scope.Model.Id + "&editable=" + false).success(function (data, status, headers, config) {
                                    $scope.Model = data;
                                    $scope.Model.Editable = false;
                                    jbToast.success('The registrant information has been updated successfully.');
                                }).error(function (data, status, headers, config) {
                                    jbToast.error("error " + data);
                                });
                            }
                            else {
                                jbToast.error(data.Message);
                            }
                        });
                }
                else {
                    jbToast.error("Data invalid or missing,please enter valid form data.");
                }
            }

        };

        $scope.ViewWaiver = function (waiverId) {
            if (waiverId != null) {

                $state.go('WaiverView', { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId, waiverId: waiverId })
            }
        }

        $scope.loadModel(false);

        $scope.goBack = function () {
            if ("goBackToOverView" in window) {
                if (window.goBackToOverView) {
                    window.goBackToOverView = false;
                    window.history.back();
                }
                else {
                    window.goBackToOverView = false;
                    $state.go('Program', ({ seasonDomain: $scope.MC.stateParams.seasonDomain, programId: $scope.Model.ProgramId }));
                }
            }
            if ("goToFamilyOrder" in window) {
                if (window.goToFamilyOrder > 0) {
                    var userId = window.goToFamilyOrder;
                    window.goToFamilyOrder = -1;
                    window.clubId = -1;
                    $state.go('FamilyOrders', { userId: userId });
                }
            }
            else {
                window.goBackToOverView = false;
                $state.go('Program', ({ seasonDomain: $scope.MC.stateParams.seasonDomain, programId: $scope.Model.ProgramId }));
            }
        }

        $scope.editPayment = function (programid, transactionId) {
            $state.go("MakePayment", { seasonDomain: $stateParams.seasonDomain, programId: programid, orderItemId: $stateParams.orderItemId, transactionId: transactionId });
        }
        $scope.makePayment = function (seasonDomain, programId, id) {
            window.comesFromOrderItem = true;
            $state.go("MakePayment", { seasonDomain: seasonDomain, programId: programId, orderItemId: id });
        }
        $scope.refund = function (seasonDomain, programId, id) {
            window.comesFromOrderItem = true;
            $state.go("PartiallyRefundOrderItem", { seasonDomain: seasonDomain, programId: programId, orderItemId: id });
        }
        $scope.deleteTransaction = function (transactionId) {

            jbConfirmation
                .yes(function () {
                    type: "GET",
                        $http.get(window.location.origin + '/Dashboard/Order/RemoveTransaction',
                            { params: { orderItemId: $stateParams.orderItemId, transactionId: transactionId } }).success(function (data, status, headers, config) {
                                if (data.Status == true) {
                                    // to cancel confirm box before reload.
                                    window.onbeforeunload = null;
                                    $http.get(window.location.origin + '/Dashboard/Order/GetOrderItem/?orderItemId=' + $scope.Model.Id + "&editable=" + false).success(function (data, status, headers, config) {
                                        $scope.Model = data;
                                        jbToast.success("Payment deleted successfully.");
                                    }).error(function (data, status, headers, config) {
                                        jbToast.error("error " + data);
                                    });
                                }
                            });
                }).confirm();
        }


        $scope.stopInatallmentsPayment = function () {

            $http.post(window.location.origin + '/Dashboard/Order/StopInatallmentsPayment', { orderItemId: $scope.Model.Id }).success(function (data, status, headers, config) {

                $scope.Model.PaymentPlanStatus = 'Stopped';
                jbToast.success("Payments stopped successfully.");

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $scope.resumeInatallmentsPayment = function (installmentId) {
            $http.post(window.location.origin + '/Dashboard/Order/ResumeInatallmentsPayment', { orderItemId: $scope.Model.Id }).success(function (data, status, headers, config) {
                $scope.Model.PaymentPlanStatus = 'Started';
                jbToast.success("Payments resumed successfully.");
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

      
        $scope.SaveChessInformation = function (chessInfoModel) {
            $scope.chessInfo = JSON.stringify(chessInfoModel);

                    $http.post('/Dashboard/Order/EditChessTournament', { orderItemId: $scope.Model.Id, chessInfo: $scope.chessInfo })
                        .success(function (data, status, headers, config) {
                            if (data.Status) {
                                
                                $http.get(window.location.origin + '/Dashboard/Order/GetOrderItem/?orderItemId=' + $scope.Model.Id + "&editable=" + false).success(function (data, status, headers, config) {
                                    $scope.Model = data;
                                    $scope.IsChessEditModel = false;
                                    jbToast.success("The class information has been updated successfully.");
                                }).error(function (data, status, headers, config) {
                                    jbToast.error("error " + data);
                                });
                            }
                            else {
                                jbToast.error(data.Message);
                            }
                        });
        }

        $scope.cancelEditChessInfo = function () {
            $scope.IsChessEditModel = false;
        }

        $scope.selectSection = function (sections, currentSection) {

            for (var i = 0; i < sections.length; i++) {
                if (sections[i].Name != currentSection.Name) {
                    sections[i].IsChecked = false;
                }
            }

            $scope.Model.ChessTournamentAtttribute.Sections = sections;
        };

        $scope.selectSchedule = function (schedules, schedule) {
            console.log(schedule);

            for (var i = 0; i < schedules.length; i++) {
                if (schedules[i].Id != schedule.Id) {
                    schedules[i].IsChecked = false;
                }
            }

            $scope.Model.ChessTournamentAtttribute.Schedules = schedules;
        };

    }]);
})();