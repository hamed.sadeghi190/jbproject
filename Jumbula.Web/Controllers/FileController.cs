﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    public class FileController : JbBaseController
    {
        public virtual ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public virtual byte[] CheckValidFiles(byte[] file)
        {
            //byte[] data = Encoding.Unicode.GetBytes(file);

            var fileSize = file.Count() / 1024;
            byte[] newFile;

            //if (fileSize > 6144)
            //{
            //     newFile = Jumbula.Common.Helper.FileHelper.Compress(file);
            //}

            //PixelFormat format = new PixelFormat();
            //format = PixelFormat.Format32bppPArgb;
            //var d = Jumbula.Common.Helper.ImageHelper.Compress(file, format);

            newFile = Jumbula.Common.Helper.FileHelper.Compress(file);


            return newFile;
        }

        [HttpPost]
        public virtual ActionResult Upload(FileUploadViewModel model)
        {
            var byteFile = Jumbula.Common.Helper.FileHelper.ConvertStreamToByte(model.File.MainFile.InputStream);

            var file = CheckValidFiles(byteFile);

            //try
            //{
            //    var clubDomain = Request.Url.GetSubDomain();
            //    //bayad baraye har ghesmat case set shavad
            //    var fileAttribute = new StorageMetaData(StorageFileType.ESignature, model.FileName, clubDomain);
            //    var fileName = fileAttribute.FileName;

            //    //upload format 
            //    string[] legalExtention = { ".jpg", ".png", ".jpeg", ".gif", ".doc", ".pdf", ".docx", ".zip", ".rar" };

            //    if (!legalExtention.Contains(Path.GetExtension(fileName.ToLower())))
            //    {
            //        return Json(new { message = Constants.M_Ilegal_Uploaded_Image_Message });
            //    }

            //    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
            //    string contentType = string.Format(Constants.F_Image_MimeType, Path.GetExtension(model.FileName).Substring(1));

            //    //Stream fileStream = new MemoryStream(model.Files.File);

            //    //sm.UploadBlob(fileAttribute.FolderName, fileName, fileStream, contentType);
            //    //fileStream.Close();


            //    //por shavad
            //    //return Json(new { Status = true, FileName = fileName, FolderName = fileAttribute.FolderName });

            //    //return RedirectToAction("", "", new { Status = true, FileName = fileName, FolderName = fileAttribute.FolderName });
            //}
            //catch (Exception ex)
            //{
            //    throw ex;

            //}


            return Json(new { Status = true });
        }

        public virtual ActionResult UploadList(FileUploadViewModel model)
        {
            try
            {
                foreach (var item in model.Files)
                {
                    var byteFile = Jumbula.Common.Helper.FileHelper.ConvertStreamToByte(model.File.MainFile.InputStream);
                    var byteSourceFile = Jumbula.Common.Helper.FileHelper.ConvertStreamToByte(model.File.SourceFile.InputStream);

                    var clubDomain = Request.Url.GetSubDomain();
                    //bayad baraye har ghesmat case set shavad
                    var fileAttribute = new StorageMetaData(StorageFileType.ESignature, item.MainFile.FileName, clubDomain);
                    var fileName = fileAttribute.FileName;
                    var sourceFileName = "large-" + fileAttribute.FileName;

                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                    string contentType = string.Format(Constants.F_Image_MimeType, Path.GetExtension(item.MainFile.FileName).Substring(1));

                    //file
                    Stream fileStream = new MemoryStream(byteFile);
                    sm.UploadBlob(fileAttribute.FolderName, fileName, fileStream, contentType);

                    //Source file
                    Stream sourceFileStream = new MemoryStream(byteSourceFile);
                    sm.UploadBlob(fileAttribute.FolderName, sourceFileName, sourceFileStream, contentType);

                    fileStream.Close();

                }

                //por shavad
                return RedirectToAction("");

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        [HttpPost]
        public virtual ActionResult UploadGallery(HttpPostedFileBase file, string name)
        {
            var byteimage = Jumbula.Common.Helper.FileHelper.ConvertStreamToByte(file.InputStream);
            var club = Ioc.ClubBusiness.Get(ClubSubDomain);
            this.EnsureUserIsAdminForClub(ClubSubDomain);
            //compress file
            var fileSize = byteimage.Count() / 1024;
            byte[] newFile = null;

            if (fileSize > 6144)
            {
                newFile = Jumbula.Common.Helper.FileHelper.Compress(byteimage);
            }
            else
            {
                newFile = byteimage;
            }
            var folderName = name == "Gallery" ? "homeSiteImageGallery" : "homeSiteSlider";
            var contentType = "image/jpeg";
            var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
            var smalImageUrl = string.Empty;
            var largeImageUrl = string.Empty;

            if (name == "Gallery")
            {
                var image = Jumbula.Common.Helper.ImageHelper.ByteArrayToImage(newFile);
                var imageResized = Jumbula.Common.Helper.ImageHelper.ResizeImage(image, 360);
                var byteImageResized = Jumbula.Common.Helper.ImageHelper.ImageToByteArray(imageResized);

                var FileName = Path.GetFileNameWithoutExtension(file.FileName).Replace(' ', '_') + "_" + DateTime.UtcNow.ToString("MMddyyyyHHmm") + Path.GetExtension(file.FileName.ToLower());
                Stream srcFileStream = new MemoryStream(byteImageResized);

                sm.UploadBlob(string.Format(@"{0}\{1}", club.Domain.ToLower(), folderName), FileName, srcFileStream, contentType);
                srcFileStream.Close();
                smalImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain.ToLower() + "/" + folderName, FileName).AbsoluteUri;
                srcFileStream.Close();
            }

            var largeFileName = "large-" + Path.GetFileNameWithoutExtension(file.FileName).Replace(' ', '_') + "_" + DateTime.UtcNow.ToString("MMddyyyyHHmm") + Path.GetExtension(file.FileName.ToLower());
            Stream fileStream = new MemoryStream(newFile);
            sm.UploadBlob(string.Format(@"{0}\{1}", club.Domain.ToLower(), folderName), largeFileName, fileStream, contentType);
            fileStream.Close();

            largeImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain.ToLower() + "/" + folderName, largeFileName).AbsoluteUri;


            var listUrl = new { SmallImage = smalImageUrl, LargeImage = largeImageUrl };


            return JsonNet(listUrl);
        }
        [HttpPost]
        public virtual ActionResult RemoveImage(string firstPath, string secondpath, string location)
        {
            var paths = new List<string>();
            paths.Add(firstPath);
            paths.Add(secondpath);
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
            var club = this.GetCurrentClubBaseInfo();
            var clubDomain = club.Domain;

            try
            {
                foreach (var path in paths)
                {
                    if (path != null)
                    {
                        var fileName = path.Split('/').Last();
                        var sourcePath = StorageManager.PathCombine(clubDomain + location, fileName);
                        var targetPath = StorageManager.PathCombine(clubDomain + location, fileName);

                        if (StorageManager.IsExist(StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain + location, fileName).AbsoluteUri))
                        {
                            storageManager.ReadFile(sourcePath, targetPath);
                            storageManager.DeleteBlob(clubDomain + location, fileName);
                        }
                    }

                }

                return Json(new JResult { Status = true, Message = "", RecordsAffected = 0 });
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = Constants.M_E_CreateEdit, RecordsAffected = 0 });
            }
        }
        [HttpPost]
        public virtual ActionResult RemoveImages(List<JbPImage> images, string location)
        {
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
            var club = this.GetCurrentClubBaseInfo();
            var clubDomain = club.Domain;

            foreach (var image in images)
            {
                var paths = new List<string>();
                paths.Add(image.largeImage);
                paths.Add(image.smallImage);

                foreach (var path in paths)
                {
                    if (path != null)
                    {
                        var fileName = path.Split('/').Last();
                        var sourcePath = StorageManager.PathCombine(clubDomain + location, fileName);
                        var targetPath = StorageManager.PathCombine(clubDomain + location, fileName);

                        if (StorageManager.IsExist(StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain + location, fileName).AbsoluteUri))
                        {
                            storageManager.ReadFile(sourcePath, targetPath);
                            storageManager.DeleteBlob(clubDomain + location, fileName);
                        }
                    }
                }
            }
            return Json(new JResult { Status = true, Message = "", RecordsAffected = 0 });
        }
    }
}