﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class ClientCreditCardBusiness :  IClientCreditCardBusiness
    {
        private readonly IRepository<ClientCreditCard, long> _clientCreditCardRepository;
        public ClientCreditCardBusiness(IRepository<ClientCreditCard, long> clientCreditCardRepository)
        {
            _clientCreditCardRepository = clientCreditCardRepository;
        }
        public  ClientCreditCard Get(long cardId)
        {
            return _clientCreditCardRepository.Get(c => c.Id == cardId && c.IsDeleted == false);
        }
        public  OperationStatus Create(ClientCreditCard card)
        {
            return _clientCreditCardRepository.Create(card);
        }
      
        public IQueryable<ClientCreditCard> GetClientCards(long clientId)
        {
          return _clientCreditCardRepository.GetList().Where(c => c.ClientId == clientId && c.IsDeleted == false);
        }
     

        public OperationStatus Delete(long cardId)
        {
            var card = _clientCreditCardRepository.Get(c => c.Id == cardId);
            card.IsDeleted = true;
            return _clientCreditCardRepository.Update(card);
        }

        public OperationStatus Update(ClientCreditCard client)
        {
            return _clientCreditCardRepository.Update(client);
        }
    }
}
