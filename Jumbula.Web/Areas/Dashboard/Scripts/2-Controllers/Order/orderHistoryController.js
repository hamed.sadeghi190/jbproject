﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('orderHistoryController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }
        $scope.baseUrl = window.location.origin + "/Dashboard/Order/";
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;

        $scope.Model = {};
        $scope.Model.Id = $stateParams.orderId;
        $scope.Model.orderItemId = 0
        $scope.getParameters = '?orderId=' + $scope.Model.Id;
        if ($stateParams.orderItemId)
        {
            $scope.Model.orderItemId = $stateParams.orderItemId;
            $scope.getParameters += '&orderItemId='+$scope.Model.orderItemId;
        }
        
        $scope.orderHistoryDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + 'GetOrderHistory/' + $scope.getParameters,
                        type: "POST",
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            batch: true,
            resizable: true,
        });

        $scope.orderHistoryGridOptions = {
            dataSource: $scope.orderHistoryDataSource,
            sortable: { mode: 'single' },
            resizable: true,
            columns: [
                {
                    field: "Email",
                    title: "User",
                },
                {
                    field: "ActionDate",
                    title: "Date",
                },
                {
                    field: "Description",
                    title: "Description",
                },
                {
                    field: "Action",
                    title: "Action",
                }
            ],
        }
    }]);
})();