﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace Jumbula.Common.Helper
{
    public static class JsonHelper
    {
        public static string JsonSerializer<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static string JsonSerializer(dynamic obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static string JsonComplexSerializer<T>(T obj)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
            return JsonSerializer<T>(obj, settings);
        }

        public static string JsonSerializer<T>(T obj, JsonSerializerSettings settings)
        {
            var jsonString = JsonConvert.SerializeObject(obj, settings);
            return jsonString;
        }

        /// <summary>
        /// JSON Deserialization
        /// </summary>       
        public static T JsonDeserialize<T>(string jsonString)
        {
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public static T JsonComplexDeserialize<T>(string jsonString)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
            return JsonDeserialize<T>(jsonString, settings);
        }

        public static T JsonDeserialize<T>(string jsonString, JsonSerializerSettings settings)
        {
            var obj = JsonConvert.DeserializeObject<T>(jsonString, settings);
            return obj;
        }

        public static string EnumToJson<EnumType>() where EnumType : struct, IConvertible
        {
            return "\"" + typeof(EnumType).Name + "\"" +
                   ":[" +
                   String.Join(",", Enum.GetValues(typeof(EnumType)).Cast<EnumType>().Select(e => "\"" + e.ToString() + "\"")) +
                   "]";
        }
    }
}
