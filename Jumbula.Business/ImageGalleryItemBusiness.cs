﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class ImageGalleryItemBusiness : IImageGalleryItemBusiness
    {
        private readonly IRepository<ImageGalleryItem> _imageGalleryItemRepository;
        public ImageGalleryItemBusiness(IRepository<ImageGalleryItem> imageGalleryItemRepository)
        {
            _imageGalleryItemRepository = imageGalleryItemRepository;
        }
        public ImageGalleryItem Get(int id)
        {
            return _imageGalleryItemRepository.GetList().SingleOrDefault(p => p.Id == id);
        }
        public OperationStatus DeleteImages(List<ImageGalleryItem> images)
        {
            images.ForEach(i =>i.Status= EventStatusCategories.deleted);
            return _imageGalleryItemRepository.Save();
        }
    }
}
