
GO

/****** Object:  Table [dbo].[JbUserRoles]    Script Date: 23/02/1395 10:29:14 �.� ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JbUserRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.JbUserRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[JbUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbUserRoles_dbo.JbRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[JbRoles] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[JbUserRoles] CHECK CONSTRAINT [FK_dbo.JbUserRoles_dbo.JbRoles_RoleId]
GO

ALTER TABLE [dbo].[JbUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbUserRoles_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[JbUserRoles] CHECK CONSTRAINT [FK_dbo.JbUserRoles_dbo.JbUsers_UserId]
GO


