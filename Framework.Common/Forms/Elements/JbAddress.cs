﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jb.Framework.Common.Forms.JsonFormatter;
using Jb.Framework.Common.Forms.Model;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbAddress : JbBaseElement
    {
        public JbAddress()
        {
            AutoCompleteAddress = new PostalAddressAutoCompleteViewModel();
            Width = ElementWidth.Full;
        }

        [NotMapped]
        public bool IsRequired { get; set; }

        [DisplayName("Address type")]
        public AddressType AddressType { get; set; }

        [DisplayName("Address")]
        [JsonIgnore]
        public PostalAddressAutoCompleteViewModel AutoCompleteAddress { get; set; }

        [DisplayName("Address line 1")]
        public string AddressLine1 { get; set; }

        [DisplayName("Address line 2")]
        public string AddressLine2 { get; set; }

        [DisplayName("City")]
        public string City { get; set; }

        [DisplayName("State")]
        public string State { get; set; }

        [DisplayName("Zip code")]
        //[RegularExpression(@"^[0-9]*$", ErrorMessage = "Zip code invalid")]
        public string Zip { get; set; }

        [Display(Name = "Country")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "You must select a country.")]

        private string _country;
        public string Country
        {
            get
            {
                var result = _country;
                switch (this.AddressType)
                {
                    case AddressType.US:
                        {
                            result = Countries.UnitedStates.ToString();
                        }
                        break;
                    case AddressType.Canada:
                        {
                            result = Countries.Canada.ToString();
                        }
                        break;
                    case AddressType.INTERNATIONAL:
                        {
                            result = _country;
                        }
                        break;
                }

                return result;
            }
            set
            {
                _country = value;
            }


        }

        public override object GetValue()
        {
            var res = !string.IsNullOrEmpty(AddressLine1) ? AddressLine1 + "," : string.Empty;
            res += !string.IsNullOrEmpty(AddressLine2) ? AddressLine2 + "," : string.Empty;
            res += !string.IsNullOrEmpty(City) ? City + "," : string.Empty;
            res += !string.IsNullOrEmpty(Zip) ? Zip + "," : string.Empty;
            res += !string.IsNullOrEmpty(State) ? State + "," : string.Empty;
            res += !string.IsNullOrEmpty(Country) ? Country : string.Empty;

            return res;
        }
        public override void SetValue(object value)
        {
            var addressItems = value.ToString().Split(',');
            if (addressItems.Count() == 6)
            {
                AddressLine1 = addressItems[0];
                AddressLine2 = addressItems[1];
                City = addressItems[2];
                Zip = addressItems[3].Trim();
                State = addressItems[4];
                Country = addressItems[5];
            }
        }
    }

    public enum AddressType : byte
    {
        [Description("US address")]
        US = 0,
        [Description("Canadian address")]
        Canada = 2,
        [Description("International address")]
        INTERNATIONAL = 1,

    }

    public enum USStates : byte
    {
        [Description("Alabama")]
        AL,
        [Description("Alaska")]
        AK,
        [Description("American Samoa")]
        AmericanSamoa,
        [Description("Arizona")]
        AZ,
        [Description("Arkansas")]
        AR,
        [Description("Armed Forces Americas")]
        ArmedForcesAmericas,
        [Description("Armed Forces Europe")]
        ArmedForcesEurope,
        [Description("Armed Forces Pacific")]
        ArmedForcesPacific,
        [Description("California")]
        CA,
        [Description("Colorado")]
        CO,
        [Description("Connecticut")]
        CT,
        Delaware,
        [Description("District of Columbia")]
        DistrictofColumbia,
        [Description("Federated States of Micronesia")]
        FederatedStatesOfMicronesia,
        [Description("Florida")]
        FL,
        [Description("Georgia")]
        GA,
        Guam,
        [Description("Hawaii")]
        HI,
        [Description("Idaho")]
        ID,
        [Description("Illinois")]
        IL,
        [Description("Indiana")]
        IN,
        [Description("Iowa")]
        IA,
        [Description("Kansas")]
        KS,
        [Description("Kentucky")]
        KY,
        [Description("Louisiana")]
        LA,
        [Description("Maine")]
        ME,
        [Description("Marshall Islands")]
        MarshallIslands,
        [Description("Maryland")]
        MD,
        [Description("Massachusetts")]
        MA,
        [Description("Michigan")]
        MI,
        [Description("Minnesota")]
        MN,
        [Description("Mississippi")]
        MS,
        [Description("Missouri")]
        MO,
        [Description("Montana")]
        MT,
        [Description("Nebraska")]
        NE,
        [Description("Nevada")]
        NV,
        [Description("New Hampshire")]
        NH,
        [Description("New Jersey")]
        NJ,
        [Description("New Mexico")]
        NM,
        [Description("New York")]
        NY,
        [Description("North Carolina")]
        NC,
        [Description("North Dakota")]
        ND,
        [Description("Northern Mariana Islands")]
        NorthernMarianaIslands,
        [Description("Ohio")]
        OH,
        [Description("Oklahoma")]
        OK,
        [Description("Oregon")]
        OR,
        Palau,
        [Description("Pennsylvania")]
        PA,
        [Description("Puerto Rico")]
        PuertoRico,
        [Description("Rhode Island")]
        RhodeIsland,
        [Description("South Carolina")]
        SC,
        [Description("South Dakota")]
        SD,
        [Description("Tennessee")]
        TN,
        [Description("Texas")]
        TX,
        [Description("Utah")]
        UT,
        [Description("Vermont")]
        VT,
        [Description("Virgin Islands")]
        VirginIslands,
        [Description("Virginia")]
        VA,
        [Description("Washington")]
        WA,
        [Description("West Virginia")]
        WV,
        [Description("Wisconsin")]
        WI,
        [Description("Wyoming")]
        WY
    }

    public enum CanadaStates : byte
    {
        [Description("Alberta")]
        Alberta,
        [Description("British Columbia")]
        BritishColumbia,
        [Description("Manitoba")]
        Manitoba,
        [Description("New Brunswick")]
        NewBrunswick,
        [Description("Newfoundland and Labrador")]
        Newfoundland,
        [Description("Northwest Territories")]
        NorthwestTerritories,
        [Description("Nova Scotia")]
        NovaScotia,
        [Description("Nunavut")]
        Nunavut,
        [Description("Ontario")]
        Ontario,
        [Description("Prince Edward Island")]
        PrinceEdwardIsland,
        [Description("Quebec")]
        Quebec,
        [Description("Saskatchewan")]
        Saskatchewan,
        [Description("Yukon")]
        Yukon,




    }


    public enum States
    {
        Alabama,
        Alaska,
        [Description("American Samoa")]
        AmericanSamoa,
        Arizona,
        Arkansas,
        [Description("Armed Forces Americas")]
        ArmedForcesAmericas,
        [Description("Armed Forces Europe")]
        ArmedForcesEurope,
        [Description("Armed Forces Pacific")]
        ArmedForcesPacific,
        California,
        Colorado,
        Connecticut,
        Delaware,
        [Description("District of Columbia")]
        DistrictofColumbia,
        [Description("Federated States of Micronesia")]
        FederatedStatesOfMicronesia,
        Florida,
        Georgia,
        Guam,
        Hawaii,
        Idaho,
        Illinois,
        Indiana,
        Iowa,
        Kansas,
        Kentucky,
        Louisiana,
        Maine,
        [Description("Marshall Islands")]
        MarshallIslands,
        Maryland,
        Massachusetts,
        Michigan,
        Minnesota,
        Mississippi,
        Missouri,
        Montana,
        Nebraska,
        Nevada,
        [Description("New Hampshire")]
        NewHampshire,
        [Description("New Jersey")]
        NewJersey,
        [Description("New Mexico")]
        NewMexico,
        [Description("New York")]
        NewYork,
        [Description("North Carolina")]
        NorthCarolina,
        [Description("North Dakota")]
        NorthDakota,
        [Description("Northern Mariana Islands")]
        NorthernMarianaIslands,
        Ohio,
        Oklahoma,
        Oregon,
        Palau,
        Pennsylvania,
        [Description("Puerto Rico")]
        PuertoRico,
        [Description("Rhode Island")]
        RhodeIsland,
        [Description("South Carolina")]
        SouthCarolina,
        [Description("South Dakota")]
        SouthDakota,
        Tennessee,
        Texas,
        Utah,
        Vermont,
        [Description("Virgin Islands")]
        VirginIslands,
        Virginia,
        Washington,
        [Description("West Virginia")]
        WestVirginia,
        Wisconsin,
        Wyoming,
    }
    public enum Countries : byte
    {
        Afghanistan,
        Albania,
        Algeria,
        Andorra,
        Angola,
        [Description("Antigua and Barbuda")]
        AntiguaandBarbuda,
        Argentina,
        Armenia,
        Aruba,
        Australia,
        Austria,
        Azerbaijan,
        [Description("Bahamas, The")]
        BahamasThe,
        Bahrain,
        Bangladesh,
        Barbados,
        Belarus,
        Belgium,
        Belize,
        Benin,
        Bhutan,
        Bolivia,
        [Description("Bosnia and Herzegovina")]
        BosniaandHerzegovina,
        Botswana,
        Brazil,
        Brunei,
        Bulgaria,
        [Description("Burkina Faso")]
        BurkinaFaso,
        Burma,
        Burundi,
        Cambodia,
        Cameroon,
        Canada,
        [Description("Cabo Verde")]
        CaboVerde,
        [Description("Central African Republic")]
        CentralAfricanRepublic,
        Chad,
        Chile,
        China,
        Colombia,
        Comoros,
        [Description("Congo, Democratic Republic of the")]
        Congo, DemocraticRepublicofthe,
        [Description("Congo, Republic of the")]
        CongoRepublicofthe,
        [Description("Costa Rica")]
        CostaRica,
        [Description("Cote d'Ivoire")]
        CotedIvoire,
        Croatia,
        Cuba,
        Curacao,
        Cyprus,
        Czechia,
        Denmark,
        Djibouti,
        Dominica,
        [Description("Dominican Republic")]
        DominicanRepublic,
        [Description("East Timor (see Timor-Leste)")]
        EastTimor,
        Ecuador,
        Egypt,
        [Description("El Salvador")]
        ElSalvador,
        [Description("Equatorial Guinea")]
        EquatorialGuinea,
        Eritrea,
        Estonia,
        Ethiopia,
        Fiji,
        Finland,
        France,
        Gabon,
        Gambia, The,
        Georgia,
        Germany,
        Ghana,
        Greece,
        Grenada,
        Guatemala,
        Guinea,
        [Description("Guinea-Bissau")]
        GuineaBissau,
        Guyana,
        Haiti,
        [Description(" Holy See")]
        HolySee,
        Honduras,
        [Description(" Hong Kong")]
        HongKong,
        Hungary,
        Iceland,
        India,
        Indonesia,
        Iran,
        Iraq,
        Ireland,
        Israel,
        Italy,
        Jamaica,
        Japan,
        Jordan,
        Kazakhstan,
        Kenya,
        Kiribati,
        [Description("Korea, North")]
        KoreaNorth,
        [Description("Korea, South")]
        KoreaSouth,
        Kosovo,
        Kuwait,
        Kyrgyzstan,
        Laos,
        Latvia,
        Lebanon,
        Lesotho,
        Liberia,
        Libya,
        Liechtenstein,
        Lithuania,
        Luxembourg,
        Macau,
        Macedonia,
        Madagascar,
        Malawi,
        Malaysia,
        Maldives,
        Mali,
        Malta,
        [Description(" Marshall Islands")]
        MarshallIslands,
        Mauritania,
        Mauritius,
        Mexico,
        Micronesia,
        Moldova,
        Monaco,
        Mongolia,
        Montenegro,
        Morocco,
        Mozambique,
        Namibia,
        Nauru,
        Nepal,
        Netherlands,
        [Description("New Zealand")]
        NewZealand,
        Nicaragua,
        Niger,
        Nigeria,
        [Description("North Korea")]
        NorthKorea,
        Norway,
        Oman,
        Pakistan,
        Palau,
        [Description("Palestinian Territories")]
        PalestinianTerritories,
        Panama,
        [Description("Papua New Guinea")]
        PapuaNewGuinea,
        Paraguay,
        Peru,
        Philippines,
        Poland,
        Portugal,
        Qatar,
        Romania,
        Russia,
        Rwanda,
        [Description("Saint Kitts and Nevis")]
        SaintKittsandNevis,
        [Description("Saint Lucia")]
        SaintLucia,
        [Description("Saint Vincent and the Grenadines")]
        SaintVincentandtheGrenadines,
        Samoa,
        [Description("San Marino")]
        SanMarino,
        [Description("Sao Tome and Principe")]
        SaoTomeandPrincipe,
        [Description("Saudi Arabia")]
        SaudiArabia,
        Senegal,
        Serbia,
        Seychelles,
        [Description("Sierra Leone")]
        SierraLeone,
        Singapore,
        [Description("Sint Maarten")]
        SintMaarten,
        Slovakia,
        Slovenia,
        [Description("Solomon Islands")]
        SolomonIslands,
        Somalia,
        [Description("South Africa")]
        SouthAfrica,
        [Description("South Korea")]
        SouthKorea,
        [Description("South Sudan")]
        SouthSudan,
        Spain,
        [Description("Sri Lanka")]
        SriLanka,
        Sudan,
        Suriname,
        Swaziland,
        Sweden,
        Switzerland,
        Syria,
        Taiwan,
        Tajikistan,
        Tanzania,
        Thailand,
        [Description("Timor-Leste")]
        TimorLeste,
        Togo,
        Tonga,
        [Description(" Trinidad and Tobago")]
        TrinidadandTobago,
        Tunisia,
        Turkey,
        Turkmenistan,
        Tuvalu,
        Uganda,
        Ukraine,
        [Description("United Arab Emirates")]
        UnitedArabEmirates,
        [Description("United Kingdom")]
        UnitedKingdom,
        [Description("United States")]
        UnitedStates,
        Uruguay,
        Uzbekistan,
        Vanuatu,
        Venezuela,
        Vietnam,
        Yemen,
        Zambia,
        Zimbabwe,
    }
}
