﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System;
using System.IO;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class EmailCampaignTemplateBusiness :  IEmailCampaignTemplateBusiness
    {
        private readonly IStorageBusiness _storageBusiness;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        public EmailCampaignTemplateBusiness(IStorageBusiness storageBusiness, IRepository<EmailTemplate> emailTemplateRepository)
        {
            _storageBusiness = storageBusiness;
            _emailTemplateRepository = emailTemplateRepository;
        }

        public EmailTemplate Get(int id)
        {
            return _emailTemplateRepository.Get(id);
        }

        public IQueryable<EmailTemplate> GetAllTemplates(int clubId)
        {
            return
                _emailTemplateRepository
                    .GetList()
                    .Where(t => t.Club_Id == clubId && t.Type == TemplateType.Email)
                    .OrderBy(o => o.TemplateName);
        }

        public OperationStatus UpdateCampaignTemplate(EmailTemplate newTemplate)
        {
            try
            {
                var oldTemplate = Get(newTemplate.Id);
                oldTemplate.Template = newTemplate.Template;
                oldTemplate.TemplateName = newTemplate.TemplateName;
                return _emailTemplateRepository.Save();
            }
            catch
            {
                throw;
            }
        }

        public OperationStatus SaveCampaignTemplate(EmailTemplate template)
        {
            try
            {
                return _emailTemplateRepository.Create(template);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public OperationStatus DeleteCampaignTemplate(int templateId)
        {
            try
            {
                return _emailTemplateRepository.Delete(templateId);
            }
            catch (Exception)
            {
                return new OperationStatus { Status = false };
            }

        }

        public EmailTemplate GetTemplateById(int templateId)
        {
            return Get(templateId);
        }

        public EmailTemplate GetDefaultClubTemplate()
        {
            return _emailTemplateRepository.GetList().SingleOrDefault(n => n.TemplateName == "Default");
        }

        public string UploadImageTemplate(string image, string clubDomain, string folderName)
        {
         
            var result = string.Empty;

            if (!string.IsNullOrEmpty(image) && !image.Contains("http:"))
            {
                string fileName = string.Format("{0}.jpg", Guid.NewGuid());

                image = image.Replace("data:image/jpeg;base64,", string.Empty);

                image = image.Replace("data:image/png;base64,", string.Empty);

                var fileData = Convert.FromBase64String(image);

                var sm = _storageBusiness;

                var fileSize = fileData.Count() / 1024;

                var contentType = "image/jpeg";

                Stream fileStream = new MemoryStream(fileData);

                sm.UploadBlob(string.Format(@"{0}\{1}", clubDomain, folderName), fileName, fileStream, contentType);

                fileStream.Close();
                result = fileName;
            }

            return result;
        }
    }
}
