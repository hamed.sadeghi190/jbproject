﻿namespace Jumbula.Core.Identity
{
    public interface IAdditionalData
    {
        ICurrentUser CurrentUser { get; set; }

        ICurrentOrganization CurrentOrganization { get; set; }
    }
}
