﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('registerantPeopleController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cfpLoadingBar', '$timeout', 'jbToast', function ($scope, $http, $log, $stateParams, $state, cfpLoadingBar, $timeout, jbToast) {
        window.app.globalObjects.pageClass = 'registerant-people-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/People';

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });

        $scope.searchTerm = "";
        $scope.SearchTypes = [{ Text: 'Name', Value: 'Name' }, { Text: 'Family email', Value: 'Email' }, { Text: 'Confirmation ID', Value: 'ConfirmationId' }, { Text: 'Phone', Value: 'Phone' }, { Text: 'Season', Value: 'Season' }];
        $scope.SearchType = 'Name';
        $scope.SeasonIds = [];
        $scope.selectedRows = [];
        $scope.selectedRowNumber = [];


        $scope.Model = {};

        $http.get(baseUrl + '/GetClubSeasonsProvider', { params: { clubId: 0 } }).success(function (data, status, headers, config) {

            $scope.Model.Seasons = data;
            $scope.SeasonIds = [''];

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        $scope.prepareProgramDropDown = function () {
            var timer = false;

            $scope.$watch($scope.Model.Programs, function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $("select[chosen]").trigger("chosen:updated");
                }, 1500);
            });

        }

        $scope.getPrograms = function (seasonIds) {

            if (seasonIds === null || seasonIds.length === 0) {

                $scope.Model.Programs = [];
                $scope.ProgramIds = [''];
                $scope.prepareProgramDropDown();
                return;
            }

            $http.get(baseUrl + '/GetSeasonPrograms', { params: { seasonIds: seasonIds } }).success(function (data) {

                $scope.Model.Programs = data;
                $scope.ProgramIds = [''];
                $scope.prepareProgramDropDown();

            }).error(function (data) {
                jbToast.error("error " + data);
            });

        }


        $http.get(window.location.origin + "/Dashboard/OverView/GetAccountInfo")
            .success(function (data, status, headers, config) {
                $scope.Model.HasPartner = data.HasPartner;

                if (data.HasPartner) {
                    $scope.SeasonNames = [{ Value: "Fall", Title: "Fall" }, { Value: "Winter", Title: "Winter" }, { Value: "Spring", Title: "Spring" }, { Value: "Summer", Title: "Summer" }];
                    $scope.SeasonName = "Fall";
                    $scope.Year = 0;
                    getYears();

                    getAllPartnerSchools();
                }

                prepareGrid();
            });



        var prepareGrid = function () {
            $scope.peopleGridDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/GetRegistrantPeople',
                            type: "POST",
                        },
                        parameterMap: function (data, type) {
                            if (type === "read") {
                                return {
                                    searchType: $scope.SearchType,
                                    term: $scope.searchTerm,
                                    firstName: $scope.firstName,
                                    lastName: $scope.lastName,
                                    seasonIds: $scope.SeasonIds,
                                    skip: data.skip,
                                    take: data.take,
                                    page: data.page,
                                    pageSize: data.pageSize,
                                    sort: data.sort,
                                    year: $scope.Year,
                                    seasonName: $scope.SeasonName,
                                    selectedSchools: $scope.Model._SelectedSchools,
                                    programIds: $scope.ProgramIds
                                }
                            }
                        }
                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverFiltering: true,
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
            });

            $scope.registrantGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Registrants.xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                dataSource: $scope.peopleGridDataSource,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                sortable: true,
                columns: [
                    {
                        template: "<input type='checkbox' class='checkbox' />",
                        width: "30px",
                        headerTemplate: '<input type="checkbox" class="checkAll" />'
                    },
                    {
                        field: "FullName",
                        title: "Full name",
                    }, {
                        field: "Age",
                        title: "Age"
                    },
                    {
                        field: "Grade",
                        title: "Grade",
                    },
                    {
                        field: "TotalAmount",
                        title: "Gross",
                        template: "#=(TotalAmount).formatCurrencyWithPenny(2)#"
                    },
                    {
                        field: "ClubName",
                        title: "School",
                    },
                    {
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				<ul><li ui_sref='RegistrationDetail({userId: #=UserId#,playerId:#=PlayerId#,clubId:#=ClubId#})' class='k-item k-state-default k-first' role='menuitem'>\
						<span class='k-link jbi-pencil'>View details</span>\
				    </li>\
                    <li ui_sref='ParticipantClassSchedule({playerId:#=PlayerId#})' class='k-item k-state-default k-first' role='menuitem'>\
						<span class='k-link jbi-register'>Generate class schedule</span>\
				    </li></ul>\
			        </li>\
		        </ul>",
                        attributes: {
                            class: "grid-actions"
                        }
                    }
                ],
                dataBound: function (e) {
                    // add additional css to grid.
                    $("#registrantGrid tbody td").css('word-wrap', 'break-word');

                    var grid = $("#registrantGrid").data("kendoGrid");

                    grid.hideColumn("ClubName");

                    if ($scope.Model.HasPartner) {
                        grid.showColumn("ClubName");
                    }

                    if ($scope.selectedRows.length !== 0) {
                        $scope.checkSelectedItemsGrid();
                    }

                }
            };
        }

        $(document.body).on('click', '#registrantGrid table .checkbox', selectRow);
        $(document.body).on('click', '#registrantGrid table .checkAll', checkAll);

        $scope.checkSelectedItemsGrid = function () {
            var grid = $("#registrantGrid").data("kendoGrid");


            grid.tbody.find("tr").each(function () {

                var $this = $(this);
                var checkbox = $this.find("td .checkbox");
                var dataItem = grid.dataItem($this);

                var isChecked = false;

                for (var i = 0; i < $scope.selectedRows.length; i++) {

                    var element = $scope.selectedRows[i];

                    if (element.PlayerId === dataItem.PlayerId) {
                        isChecked = true;
                        break;
                    }
                }

                checkbox.prop("checked", isChecked);

                if (isChecked) {
                    $this.addClass("k-state-selected");
                    return;
                }

            });

        }

        function selectRow() {
            var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#registrantGrid").data("kendoGrid"),
                dataItem = grid.dataItem(row);
            
            for (var i = 0; i < grid.dataSource._data.length; i++) {
                var element = grid.dataSource._data[i];
                if (element.PlayerId === dataItem.PlayerId) {
                    
                    $scope.selectedRowNumber.push(i);
                    break;
                }
            }

            if (checked) {
                $scope.selectedRows.push(dataItem);
                //-select the row
                row.addClass("k-state-selected");
            } else {

                var index = -1;
                for (var i = 0; i < $scope.selectedRows.length; i++) {
                    if ($scope.selectedRows[i].PlayerId === dataItem.PlayerId) {
                        index = i;
                        break;
                    }
                }

                if (index > -1) {
                    $scope.selectedRows.splice(index, 1);
                }

                //-remove selection
                row.removeClass("k-state-selected");
            }

        }

        function checkAll() {
            var grid = $("#registrantGrid").data("kendoGrid");
            var state = $("#registrantGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
            var checkbox;
            $scope.selectedRows = [];

            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                checkbox = $this.find("td .checkbox");
                checkbox.prop("checked", state);
                var dataItem = grid.dataItem($this);

                if (state) {
                    $scope.selectedRows.push(dataItem);

                    //-select the row
                    $this.addClass("k-state-selected");
                } else {
                    //-remove selection
                    $this.removeClass("k-state-selected");
                }
            });

        }

        $scope.registrantSearch = function () {
            $scope.registrantGridOptions.dataSource.read();
        }

        $scope.registrationDetailGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/RegistrationDetails?userId=' + $scope.MC.stateParams.userId + '&playerId=' + $scope.MC.stateParams.playerId + '&clubId=' + $scope.MC.stateParams.clubId,
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.registrantDetailSearchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 50,
        });

        $scope.registrationDetailGridOptions = {
            dataSource: $scope.registrationDetailGridDataSource,
            //sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            //groupable: true,
            sortable: true,
            columns: [
                {
                    field: "Name",
                    title: "Program name",
                    template: " #= Name # #if(ItemStatusReason == '1') {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == '2') {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == '5') {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == '4'){# <i class='grid-label label warning'>Canceled</i> #} else if (ItemStatus == '1'){# <i class='grid-label label warning'>Draft</i> #}#"
                },
                {
                    field: "OrderDate",
                    title: "Date",
                    template: "#= kendo.toString(kendo.parseDate(OrderDate), 'MMM/dd/yyyy H:mm') #"
                },
                {
                    field: "PaidAmount",
                    title: "Gross",
                },
                {
                    field: "ConfirmationId",
                    title: "Confirm id",
                }
            ]
        };

        $scope.registrationDetailSearch = function () {
            $scope.registrationDetailGridOptions.dataSource.read();
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#registrantGrid").getKendoGrid().saveAsExcel();
            cfpLoadingBar.complete();
        }

        $scope.search = function () {
            $scope.registrantGridOptions.dataSource.page(1);
            $("#registrantGrid").data("kendoGrid").dataSource.read();
        };

        $scope.clearFilters = function () {
            $scope.SearchType = 'Name';
            $scope.searchTerm = "";
            $scope.firstName = "";
            $scope.lastName = "";
            $scope.SeasonIds = [''];
            $scope.SeasonName = "Fall";
            $scope.Year = new Date().getFullYear().toString();
            $scope.Model._SelectedSchools = [];
            $scope.registrantGridOptions.dataSource.page(1);
            $("#registrantGrid").data("kendoGrid").dataSource.read();
        };

        function getYears() {
            $http.get(baseUrl + '/GetCurrentYearsFrom2015').success(function (data, status, headers, config) {

                $scope.Years = data.Years;
                $scope.Year = new Date().getFullYear().toString();
            }).error(function (data) {
                jbToast.error("error " + data);
            });
        }

        function getAllPartnerSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data) {
                    $scope.AllSchools = data.AllSchools;
                    $scope.Model._SelectedSchools = [''];
                });
        }


        $scope.showImageCropper = function () {

            if ($scope.selectedRows.length === 0) {
                $scope.dismissPopup();
                jbToast.error("Please select a participant");
                return;
            }

            $scope.showParticipantSelectedGrid();

            if (!$scope.showCroppedImage) {
                $scope.showCroppedImage = true;
                return;
            }
        }

        $scope.showParticipantSelectedGrid = function () {

            $scope.showSelectedParticipantSection = true;
            $scope.showImageCroppperSection = false;
            $scope.showPreviewLabelTemplate = false;

            var grid = $("#registrantSelectedGrid").data("kendoGrid");

            $("#registrantSelectedGrid").data('kendoGrid').dataSource.data([]);

            $("#registrantSelectedGrid .k-grid-header").css('display', 'none');

            for (var i = 0; i < $scope.selectedRows.length; i++) {

                grid.dataSource.add({ FullName: $scope.selectedRows[i].FullName });
            }
        }

        $scope.fontSize = 130;

        $scope.saveToPDF = function () {

            $scope.image = $scope.cropper.croppedImage;

            var names = [];
            for (var i = 0; i < $scope.selectedRows.length; i++) {
                names.push($scope.selectedRows[i].FullName);
            }

            cfpLoadingBar.start();
            $http.post(baseUrl + '/GeneratePdfParticipantLabel', {
                names: names,
                image: $scope.image,
                fontSize: $scope.fontSize
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/pdf" });

                    var fileUrl = URL.createObjectURL(blob);
                    a.href = fileUrl;
                    a.download = "Logo.pdf";
                    a.click();
                    cfpLoadingBar.complete();
                    $scope.dismissPopup();
                });
        }

        $scope.changeFontSize = function (fontSize) {
            $(".fullNameSize").css("font-size", fontSize);
        }

        $scope.dismissPopup = function () {
            $scope.showCroppedImage = false;
        }

        $scope.cropper = {};
        $scope.cropper.sourceImage = null;
        $scope.cropper.croppedImage = null;
        $scope.bounds = {};
        $scope.bounds.left = 0;
        $scope.bounds.right = 0;
        $scope.bounds.top = 0;
        $scope.bounds.bottom = 0;
        //image cropper end
        $scope.getSize = function (elem) {
            var element = document.getElementById("cropperLogo");
            var files = element.files;
            var file = files[0];

            var reader = new FileReader();
            reader.onload = loadFinished;
            reader.readAsDataURL(file);

            function loadFinished(event) {
                var data = event.target.result;
                var image = new Image();
                image.src = data;
                image.onload = function () {
                    $scope.bounds = {};
                    $scope.bounds.left = 0;
                    $scope.bounds.right = image.naturalWidth;
                    $scope.bounds.top = image.naturalHeight;
                    $scope.bounds.bottom = 0;
                }
            }

        }


        var selectedGridDataSource = new kendo.data.DataSource({});

        $scope.registrantSelectedGridOptions = {
            dataSource: selectedGridDataSource,
            scrollable: true,
            pageable: false,
            sortable: true,
            columns: [
                {
                    field: "FullName"
                }
            ]
        };


    }]);
})();