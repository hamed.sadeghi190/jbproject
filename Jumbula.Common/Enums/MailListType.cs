﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum MailListType : int
    {
        [Description("Uploaded List")]
        Static = 0,
        [Description("Query")]
        Dynamic = 1
    }
}
