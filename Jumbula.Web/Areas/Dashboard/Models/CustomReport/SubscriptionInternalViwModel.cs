﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SubscriptionInternalViwModel
    {
        public List<SubscriptionParticipantInternalViwModel> Participants { get; set; }
        public string PrograName { get; set; }
        public string Logo { get; set; }
        public string SchoolName { get; set; }
        public string ReportDate { get; set; }
        public int TotalNumberOfParticipant { get; set; }
        public int TotalItemInMonday { get; set; }
        public int TotalItemInTuesday { get; set; }
        public int TotalItemInWednesday { get; set; }
        public int TotalItemInThursday { get; set; }
        public int TotalItemInFriday { get; set; }
        public DateTime? StartDate { get; set;}
        public List<long> ProgramIds { get; set; }
        public string ReportName { get; set; }
        public long ScheduleId { get; set; }
        public string ClassName { get; set; }
        public long ClassId { get; set; }
        public string SeasonName { get; set; }
        public string ClassDays { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public List<string> DatesOfDay { get; set; }
        public string StrTime
        {
            get
            {
                return string.Format("{0}-{1}", StartTime, EndTime);
            }
        }
        public string Instructor { get; set; }
        public string Location { get; set; }
        public string Date { get; set; }

    }
}