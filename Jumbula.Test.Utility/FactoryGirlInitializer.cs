﻿using System;
using System.Collections.Generic;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using TimeZones = Jumbula.Common.Enums.TimeZone;
using Jumbula.Core.Domain.Generic;

namespace Jumbula.Test.Utility
{
    public static class FactoryGirlInitializer
    {
        public static void RegisterFactories()
        {
            FactoryGirl.Define(() => new PostalAddress
            {
                Id = 1,
                AdditionalInfo = null,
                Address = "5639 North Ashland Avenue, Chicago, IL 60660, USA",
                AutoCompletedAddress = "5639 North Ashland Avenue, Chicago, IL 60660, USA",
                City = "Chicago",
                Country = "US",
                County = null,
                Lat = 41.9846556,
                Lng = -87.6696488,
                Route = "North Ashland Avenue",
                State = "Illinois",
                Street = "North Ashland Avenue",
                Street2 = null,
                StreetNumber = "5639",
                TimeZone = TimeZones.PST,
                Zip = "33186",
            });

            FactoryGirl.Define(() => new ClubType
            {
                Id = 1,
                Name = "School",
                ParentId = null,
                EnumType = ClubTypesEnum.School,
                ParentType = null
            });

            // To do
            FactoryGirl.Define(() => new JbAttribute
            {
                Id = 1,
                Club = FactoryGirl.Build<Club>(),
                ClubId = 1,
                IsEnabled = true,
                IsRequired = false,
                Label = "Name",
                Name = AttributeName.Custom,
                ReferenceType = AttributeReferenceType.Club,
                ShowInGrid = true,
                Type = AttributeType.Text
            });

            // To do
            FactoryGirl.Define(() => new JbAttributeOption
            {
                Id = 1,
                Attribute = FactoryGirl.Build<JbAttribute>(),
                AttributeId = 1,
                Value = "Lead Generation"
            });

            // To do
            FactoryGirl.Define(() => new ClubAttributeValue
            {
                Attribute = FactoryGirl.Build<JbAttribute>(),
                AttributeId = 1,
                AttributeOption = FactoryGirl.Build<JbAttributeOption>(),
                AttributeOptionId = 1,
                Club = FactoryGirl.Build<Club>(),
                ClubId = 1,
                Value = "Vincent"
            });

            // To do
            FactoryGirl.Define(() => new Client
            {
                Id = 1,

            });

            // To do
            FactoryGirl.Define(() => new CatalogItem
            {
                Id = 1,

            });

            // To do
            FactoryGirl.Define(() => new Jumbula.Core.Domain.Category
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new Charge
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new ClubFormTemplate
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new ClubLocation
            {
                Id = 1,
                Club = FactoryGirl.Build<Club>(),
                IsDeleted = false,
                Name = "Bergen Academy",
                PostalAddress = FactoryGirl.Build<PostalAddress>(),
            });

            // To do
            FactoryGirl.Define(() => new ClubRevenue
            {
                Id = 1,
                Maximum = 400000,
                Minimum = 300001,
                Price = 70,
            });

            // To do
            FactoryGirl.Define(() => new ClubStaff
            {
                Id = 1,
                Club = FactoryGirl.Build<Club>(),
                ClubId = 1,
                Contact = FactoryGirl.Build<ContactPerson>(),
                ContactId = 1,
                DateOfBackgroundCheck = DateTime.Now.AddDays(-7),
                IsDeleted = false,
                JbUserRole = FactoryGirl.Build<JbUserRole>(),
                UserRoleId = 1,
                Programs = null,
                RoleOnClub = RoleCategory.Admin,
                Status = StaffStatus.Active,
            });

            // To do
            FactoryGirl.Define(() => new ClubWaiver
            {
                Id = 1,

            });

            // To do
            FactoryGirl.Define(() => new Campaign
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new MailList
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new JbESignature
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new Coupon
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new Discount
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new PaymentPlan
            {
                Id = 1,
            });

            // To do
            FactoryGirl.Define(() => new Program
            {
                Id = 1,
                Name = "Programm name",
                Season = FactoryGirl.Build<Season>(),
                Club = FactoryGirl.Build<Club>(),
            });

            // To do
            FactoryGirl.Define(() => new Season
            {
                Id = 1,
                //Charges = new List<Charge> { FactoryGirl.Build<Charge>() },
                Childs = null,
                Club = FactoryGirl.Build<Club>(),
                ClubId = 1,
                Description = null,
                Domain = "Spring2017",
                Files = null,
                HasOutSourcePrograms = false,
                IsSandBox = false,
                IsSettingChanged = false,
                MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                Name = SeasonNames.Spring,
                Note = null,
                OrderItems = null,
                OutSourcePrograms = null,
                Parent = null,
                ParentId = null,
                SettingSerialized = @"{\""SchoolSeasonGeneralSetting\"":null,\""SchoolSeasonDetailSetting\"":null,\""SchoolSeasonscheduleSetting\"":{\""Monday\"":false,\""MondayNumberOfClasses\"":0,\""ListOfMondayDates\"":null,\""Tuesday\"":false,\""TuesdayNumberOfClasses\"":0,\""ListOfTuesdayDates\"":null,\""Wednesday\"":false,\""WednesdayNumberOfClasses\"":0,\""ListOfWednesdayDates\"":null,\""Thursday\"":false,\""ThursdayNumberOfClasses\"":0,\""ListOfThursdayDates\"":null,\""Friday\"":false,\""FridayNumberOfClasses\"":0,\""ListOfFridayDates\"":null,\""ListOfNoClassDates\"":[\""07 Aug 2017\"",\""09 Aug 2017\""],\""Id\"":0},\""SeasonProgramInfoSetting\"":null,\""SeasonFormsSetting\"":{\""Form\"":31022,\""Waivers\"":[],\""Id\"":0},\""SeasonAdditionalSetting\"":null}",
                Status = SeasonStatus.Live,
                Template = FactoryGirl.Build<EmailTemplate>(),
                Title = "Spring 2017",
                TransactionActivities = null,
                Year = 2018
            });

            // To do
            FactoryGirl.Define(() => new JbRole("Admin"));

            // To do
            FactoryGirl.Define(() => new JbUserRole
            {
                Id = 1,
                Role = FactoryGirl.Build<JbRole>(),
                RoleId = 1,
                User = FactoryGirl.Build<JbUser>(),
                UserId = 1
            });

            // To do
            FactoryGirl.Define(() => new JbUser
            {
                UserName = "tester@jumbula.com"
            });

            FactoryGirl.Define(() => new CatalogSetting
            {
                Id = 1
            });

            // To do
            FactoryGirl.Define(() => new EmailTemplate
            {
                Id = 1,
                Club = FactoryGirl.Build<Club>(),
                Club_Id = 1,
                Template = @"{\""Name\"":null,\""NewName\"":\""eeeww\"",\""ChangeTemplate\"":true,\""HasHeader\"":true,\""HasLogo\"":false,\""HasFooter\"":false,\""HasMap\"":false,\""IsLive\"":true,\""HasHeaderText\"":false,\""HasFooterText\"":false,\""HeaderText\"":null,\""HeaderBackground\"":\""https://myschool.jumbula.local/Images/Dashboard/Campaigns/mail-images/chess-cover.jpg\"",\""BodyBackColor\"":null,\""Body\"":\""<body autocorrect='off' contenteditable='true' class=''><p><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>Dear Parent/Guardian:</span><br style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'><br style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>Your child, %CHILDNAME%, has been enrolled in the %CLASSNAME% for the %SEASONNAME% session.</span></p><p><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>We look forward to providing a fun and exciting program for your child.</span>﻿</p></body>\"",\""FooterText\"":null,\""FooterBackground\"":null,\""ClubLogo\"":\""https://myschool.jumbula.local/images/club-nosport.png?v=1689703562\"",\""MapZoom\"":0,\""MapLocation\"":null,\""Locations\"":[{\""Address\"":\""Ewing Township, NJ, USA\"",\""AddressPoint\"":\""40.2599864,-74.7909125\""}],\""CongratulationMessage\"":null,\""CustomMessage\"":null,\""IsRegisterationEmail\"":false,\""HasMoreQuestions\"":false,\""MoreQuestionsPhone\"":null,\""MoreQuestionsEmail\"":null,\""ClubDomain\"":null,\""UseProgramLocation\"":false}",
                TemplateName = "Email template 1",
                Type = TemplateType.Email
            });

            // To do
            FactoryGirl.Define(() => new ClubUser
            {
                Club = FactoryGirl.Build<Club>(),
                ClubId = 1,
                Credit = 0,
                TestCredit = 0,
                User = FactoryGirl.Build<JbUser>(),
                UserId = 1
            });

            FactoryGirl.Define(() => new Club
            {
                Id = 1,
                Domain = "jbclub",
                Name = "Jb Club",
                ClubType = FactoryGirl.Build<ClubType>(),
                TypeId = 1,
                Address = FactoryGirl.Build<PostalAddress>(),
                CatalogSetting = FactoryGirl.Build<CatalogSetting>(),
                Category = FactoryGirl.Build<Jumbula.Core.Domain.Category>(),
                CategoryId = 1,
                Client = FactoryGirl.Build<Client>(),
                ClientId = 1,
                ClubRevenue = FactoryGirl.Build<ClubRevenue>(),
                ClubRevenueId = 1,
                Currency = CurrencyCodes.USD,
                Description = "Chess Achieves Is A 501(C)(3) Non Profit Organization That Advances Community And Economic Development Goals, And Works To Fill The Overall Need For Capitalization Of Chess Organizers, Teachers, And Events In This Sector. We Will Serve As An Educational Resource; Working To Teach Youth Chess, Train Chess Educators, And Host Competitive Chess Events.",
                District = null,
                DistrictId = null,
                DistrictSubsidies = null,
                EventRoasters = null,
                IsDeleted = true,
                IsInActive = false,
                IsNonProfit = false,
                IsSettingChanged = false,
                Logo = "Logo_Large.jpg",
                MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow.AddMinutes(10) },
                Pages = null,
                PartnerClub = null,
                PartnerId = null,
                PartnerCommisionRate = 0,
                RelatedClubs = null,
                SettingSerialized = @"{\""SchoolInfo\"":{\""MinGrade\"":null,\""MaxGrade\"":null,\""PTASite\"":null,\""Note\"":null},\""ProgramInfo\"":{\""SelectedCoordinator\"":0,\""NumberOfSeasons\"":null,\""NumberOfClassesPerSeason\"":null,\""MaxClassesPerDay\"":null,\""DaysClassesMeet\"":null,\""BeforeSchool\"":false,\""NumberOfStudents\"":null,\""NumberOfRegistrationsPerSeason\"":null,\""OpenToNonStudents\"":false,\""TranslationNeeded\"":null,\""FolderDay\"":null,\""Note\"":null},\""OnSiteInfo\"":{\""OnSiteCancellationContactInfo\"":null,\""SchoolStartTime\"":null,\""SchoolDismissalTime\"":null,\""EnrichmentStartTimeAM\"":null,\""EnrichmentStartTimePM\"":null,\""InstructorArrivalTimeAM\"":null,\""InstructorArrivalTimePM\"":null,\""EarlyReleaseDay\"":null,\""EarlyReleaseTime\"":null,\""AvailableSpaces\"":null,\""OnSiteCancellationContactId\"":0,\""ParkingForProviders\"":null,\""TransitionInstructions\"":null,\""AttendanceProcedure\"":null,\""DismissalOption\"":null,\""EnrichmentDismissalOption\"":null,\""CheckInProceduresForInstructors\"":null,\""EnrichmentDismissalInstructions\"":null,\""LatePickUpPolicy\"":null,\""PermitResponsibility\"":null,\""Note\"":null,\""Title\"":null},\""RegistrationInfo\"":{\""Colors\"":null,\""RegistrationStartTime\"":null,\""RegistrationEndTime\"":null,\""LateRegistrationOption\"":null,\""PTARegFee\"":0.0,\""ScholarshipRequirement\"":null,\""ScholarshipCodes\"":null,\""SpecialRosterRequirements\"":null,\""ReconciliationDetail\"":null,\""Note\"":null},\""InstructorBackgroundCheckCertificate\"":null,\""InstructorBackgroundCheckCertificateDate\"":null,\""CertificateOfInsurances\"":null,\""BusinessType\"":0,\""RegistrationPageLogoRedirectUrl\"":{\""RedirectUrlType\"":0,\""RedirectUrl\"":null},\""LogOutRedirectUrl\"":{\""RedirectUrlType\"":0,\""RedirectUrl\"":null},\""BrowseMoreRedirectUrl\"":{\""RedirectUrlType\"":0,\""RedirectUrl\"":null},\""BrowseMoreText\"":null,\""NotificationEmails\"":[],\""StaffResponder\"":4426,\""StaffSignatory\"":0,\""PartnerMessageResponder\"":null,\""LegalName\"":null,\""MemberSince\"":null,\""AgreementExpirationDate\"":null,\""TaxIDFEIN\"":null,\""TaxIDSSN\"":null,\""EMDiscountSchools\"":null,\""Provider1099\"":false,\""IsDiscountApplyOnOriginPrice\"":true,\""AppearanceSetting\"":{\""HideProgramDates\"":false,\""HidePhoneNumber\"":false,\""ContactBoxBackColor\"":\""rgba(36, 154, 181, 1)\"",\""TestimonialsBoxBackColor\"":\""rgba(114, 12, 10, 1)\"",\""TestimonialsBoxColor\"":\""rgba(206, 195, 0, 1)\"",\""TestimonialsTitle\"":null,\""TuitionLabelTextColor\"":\""rgba(45, 135, 205, 1)\"",\""PriceTextColor\"":\""rgba(255, 255, 255, 1)\"",\""PriceBackColor\"":\""rgba(45, 135, 205, 1)\"",\""ERButtonTextColor\"":\""rgba(255, 255, 255, 1)\"",\""ERButtonBackColor\"":\""rgba(243, 79, 152, 1)\"",\""ERButtonTextLabel\"":\""Express Registration\"",\""HideProgramDatesInCart\"":false},\""ListOfHolidayDates\"":[\""\"",\""04 Oct 2017\"",\""11 Oct 2017\""],\""Notifications\"":{\""ContentConfirmationEmail\"":\""<body autocorrect='off' contenteditable='true' class=''><p><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>Dear Parent/Guardian:</span><br style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'><br style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>Your child, %CHILDNAME%, has been enrolled in the %CLASSNAME% for the %SEASONNAME% session.</span></p><p><span style='color: rgb(96, 96, 96); font-family: Helvetica; font-size: 15px; background-color: rgb(255, 255, 255);'>We look forward to providing a fun and exciting program for your child.</span>&#65279;</p></body>\"",\""ContentCancellationEmail\"":\""<p><span style=\\""color:#606060;font-family:Helvetica;font-size:15px;background-color:#ffffff;\\"">Dear Parent/Guardian:</span><br style=\\""color:#606060;font-family:Helvetica;font-size:15px;background-color:#ffffff;\\"" /><br style=\\""color:#606060;font-family:Helvetica;font-size:15px;background-color:#ffffff;\\"" /><span style=\\""color:#606060;font-family:Helvetica;font-size:15px;background-color:#ffffff;\\"">Your child, %CHILDNAME%, has not been enrolled in the %CLASSNAME% for the %SEASONNAME% session due to insufficient number of enrollments.</span></p><p><span style=\\""color:#606060;font-family:Helvetica;font-size:15px;background-color:#ffffff;\\"">We are sorry about any inconvenience and look forward to providing a fun and exciting program for your child in another class.</span><span style=\\""font-family:Helvetica;font-size:15px;background-color:#ffffff;color:#222222;\\""></span></p><div style=\\""color:#222222;font-family:arial, sans-serif;font-size:12.8px;background-color:#ffffff;\\""><span style=\\""font-family:Helvetica;font-size:15px;\\""></span></div>\"",\""ContentAbsentee\"":\""Absentee email ffdfdfd\""},\""ListOfHolidayAMDates\"":null,\""ListOfHolidayPMDates\"":null,\""SalesContactUserId\"":null,\""RevenueShare\"":null,\""StaffingRatio\"":null,\""NCESID\"":null,\""FirstProviderId\"":null,\""SecondProviderId\"":null,\""Agenda\"":{\""IsEnabled\"":false,\""IsExactDay\"":false,\""ExactTime\"":null,\""HourAfterClassEnds\"":0,\""Subject\"":null,\""Wrapper\"":null},\""ShowSchoolLogo\"":false}",
                Site = "https://jumbula.com",
                Subsidies = null,
                TimeZone = TimeZones.PST,
                TransactionActivities = null
            });

            FactoryGirl.Define(() => new ContactPerson
            {
                Id = 1,
                Cell = "6045181234",
                Club = FactoryGirl.Build<Club>(),
                Club_Id = 1,
                Address = FactoryGirl.Build<PostalAddress>(),
                AddressId = 1,
                DoB = new DateTime(1982, 02, 03),
                Email = "mostafa@jumbla.com",
                Employer = "Placer County",
                FirstName = "McIan",
                LastName = "Phan",
                IsPrimary = true,
                IsStaff = false,
                Nickname = "Iany",
                Occupation = "IT managment",
                Phone = "9534086543",
                PhoneExtension = "123",
                Relationship = ParentRelationship.Other,
                StaffRole = null,
                StaffStatus = StaffStatus.Active,
                Title = StaffTitle.ProviderInstructor,
                Work = "9966543970",

            });

            FactoryGirl.Define(() => new OrderItem
            {
                Id = 1,
                ProgramSchedule = FactoryGirl.Build<ProgramSchedule>(),
                ProgramScheduleId = FactoryGirl.Build<ProgramSchedule>().Id,
                Order = FactoryGirl.Build<Order>(),
                TotalAmount = 100,
                EntryFee = 100,
                TransactionActivities = new List<TransactionActivity>() { FactoryGirl.Build<TransactionActivity>() },
                OrderChargeDiscounts = new List<OrderChargeDiscount>() { FactoryGirl.Build<OrderChargeDiscount>() },
            });

            FactoryGirl.Define(() => new Order
            {
                Id = 1,
                ConfirmationId = "95D3-C9-776D",
                Club = FactoryGirl.Build<Club>(),
                User = FactoryGirl.Build<JbUser>(),
            });

            FactoryGirl.Define(() => new OrderChargeDiscount
            {
                Id = 1,
                Name = ""
            });

            FactoryGirl.Define(() => new OrderSession
            {
                Id = 1,
            });

            FactoryGirl.Define(() => new ProgramSession
            {
                Id = 1,
            });

            FactoryGirl.Define(() => new ProgramSchedulePart
            {
                Id = 1,
            });

            FactoryGirl.Define(() => new OrderItemSchedulePart
            {
                Id = 1,
            });

            FactoryGirl.Define(() => new ClassSessionsDateTime
            {
                Id = 1,

                
            });

            FactoryGirl.Define(() => new OrderHistory
            {
                Id = 1,
            });

            FactoryGirl.Define(() => new ScheduleSession
            {
                Id = 1,
            });


            FactoryGirl.Define(() => new ScheduleSubscriptionAttribute
            {

            });

            FactoryGirl.Define(() => new ProgramSchedule
            {
                Id = 1,
                Program = FactoryGirl.Build<Program>(),
                ProgramId = FactoryGirl.Build<Program>().Id
            });

            FactoryGirl.Define(() => new ScheduleAttendance
            {
                Id = 1,
                PlayerProfile = FactoryGirl.Build<PlayerProfile>(),
                PickupSerialized = "{\"FullName\": \"John Doe\",\"LocalDateTime\": \"2017-08-24T20:45:52\",\"ClubDateTime\": \"2017-08-24T21:46:35.7449271\",\"DismissalFromEnrichment\": \"Extended Day\",\"Phone\": \"9534086543\"}",
            });

            FactoryGirl.Define(() => new PlayerProfile
            {
                Id = 1,
                Contact = FactoryGirl.Build<ContactPerson>()
            });

            FactoryGirl.Define(() => new OrderInstallment
            {
                Id = 1,
                Amount = 100,
                OrderItem = FactoryGirl.Build<OrderItem>(),

                TransactionActivities = new List<TransactionActivity>() { FactoryGirl.Build<TransactionActivity>(o => o.IsAutoCharge = true) }
            });

            FactoryGirl.Define(() => new AutoChargePolicy()
            {
                Attempts = new List<AutoChargeAttempt>()
                {
                    FactoryGirl.Build<AutoChargeAttempt>(x =>
                    {
                        x.AttemptOrder = 1;
                        x.Enabled = true;
                    }),
                    FactoryGirl.Build<AutoChargeAttempt>(x =>{x.AttemptOrder = 2;x.IntervalDays = 2;})
                }
                    
            });

            FactoryGirl.Define(() => new TransactionActivity()
            {
                TransactionDate = DateTime.UtcNow,
                PaymentDetail = FactoryGirl.Build<PaymentDetail>()

            });

            FactoryGirl.Define(() => new AutoChargeAttempt());

            FactoryGirl.Define(() => new Core.Domain.Email
            {
                Id = 1,
                CallerType = EmailCallerType.AutoCharge,
                Category = EmailCategory.AutoChargeSuccess,
                DateCreated = DateTime.UtcNow,
                FromEmail = "from@jumbula.com",
                FromDisplayName = "From Jumbula",
                Parameters = @"{""InstallmentId"":1}",
                Priority = EmailSendPriority.Low,
                ReplyTo = "noreplay@jumbula.com",
                ScheduleDate = DateTime.UtcNow,
                Status = EmailSendStatus.Accepted,
                SubCategory = "Parent",
                Subject = Constants.F_CashConfirmEmailSubject,
                Recipients = new List<EmailRecipient> { FactoryGirl.Build<EmailRecipient>() }
            });

            FactoryGirl.Define(() => new EmailRecipient
            {
                Id = 1,
                EmailAddress = "email@jumbula.com",
                DisplayName = "Jumbula To recipient",
                Type = EmailRecipientType.To,
               
            });
            FactoryGirl.Define(() => new EarlyBird
            {

            });

            FactoryGirl.Define(() => new ChargeAttribute
            {

            });

            FactoryGirl.Define(() => new UserPaymentPlan
            {

            });
            FactoryGirl.Define(() => new PaymentDetail
            {
                TransactionMessage = "Your card was declined"
            });
            


        }
    }
}
