﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Areas.Dashboard.Models
{
    public class SysAdminClubItemViewModel: BaseViewModel<Club>
    {
        public SysAdminClubItemViewModel()
        {

            OrganizationTimeZone = DropdownHelpers.SeedTimeZones();
            OrganizationSubTypes = Ioc.ClubBusiness.GetClubTypes();
            SelectedTimeZone = "-1";
            SelectedRole = "0";
            AllCategories = Ioc.CategoryBuisness.GetCategories();
            OrganizationRoles = SeedRoles();
            Provider1099 = false;

        }

        public int CommissionRate { get; set; }
        public string CommissionRateString { get; set; }
        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Last name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete last name.")]
        public string LastName { get; set; }

        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [EmailAddress]
        [Display(Name = "Organization owner username")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string UserName { get; set; }



        [Url]
        [DataType(DataType.Url)]
        [Display(Name = "Club website")]
        public string Site { get; set; }

        [Display(Name = "Phone number")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$",
            ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string Phone { get; set; }

        public string StrPhone
        {
            get
            {
                if (!string.IsNullOrEmpty(Phone))
                {
                    return PhoneNumberHelper.FormatPhoneNumber(Phone);
                }
                return string.Empty;
            }
        }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 3)]
        [RegularExpression("[A-Za-z0-9_-]+",
               ErrorMessage = "{0} cannot contain any spaces or special characters such as *, &, !, etc")]
        public string Domain { get; set; }

        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? MemberSince { get; set; }
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? AgreementExpirationDate { get; set; }

        public string StrAgreementExpirationDate
        {
            get

            {
                if (AgreementExpirationDate.HasValue)
                {
                    return AgreementExpirationDate.Value.ToString(Constants.DefaultDateFormat);
                }
                return string.Empty;
            }
        }
        public string TaxIDFEIN { get; set; }
        public string TaxIDSSN { get; set; }
        public string EMDiscountSchools { get; set; }
        public bool Provider1099 { get; set; }
        public string typeMerge { get; set; }

        public string AdminName
        {
            get
            { return string.Format(Constants.F_FullName, FirstName, LastName); }
        }
        [Required(ErrorMessage = "Address is required.")]
        public string Address { get; set; }
        public string County { get; set; }
        public string Description { get; set; }

        public List<int> SelectedUser { get; set; }
        public List<SelectKeyValue<int>> PartnerAdmins { get; set; }
        public string SelectedRole { get; set; }
        public List<SelectKeyValue<string>> OrganizationRoles { get; set; }

        public string SelectedTimeZone { get; set; }
        public List<SelectKeyValue<string>> OrganizationTimeZone { get; set; }

        public int SelectedSubType { get; set; }
        public List<SelectKeyValue<int>> OrganizationSubTypes { get; set; }
        public int SelectedCategory { get; set; }

        public List<SelectKeyValue<int>> AllCategories { get; set; }


        List<SelectKeyValue<string>> SeedRoles()
        {
            var result = new List<SelectKeyValue<string>>();
            result.Add(new SelectKeyValue<string>() { Text = "Select the organization owner role", Value = "0" });
            foreach (RoleCategory type in Enum.GetValues(typeof(RoleCategory)))
            {
                if (type == RoleCategory.Accountant || type == RoleCategory.Instructor || type == RoleCategory.Owner || type == RoleCategory.Parent || type == RoleCategory.SysAdmin || type == RoleCategory.Support || type == RoleCategory.Partner)
                {
                    continue;
                }
                result.Add(new SelectKeyValue<string>() { Text = type.ToDescription(), Value = ((int)type).ToString() });
            }

            return result;
        }
    }
}