﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('manageWeeklyAgendaController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/WeeklyAgenda", order: 0, menu: "WeeklyAgenda" });
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.Model = { Date: null };

        $http.get('/Dashboard/WeeklyAgenda/GetManage').success(function (data, status, headers, config) {

            $scope.Model = data;
        });

        $scope.weeklyAgendaDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: '/Dashboard/WeeklyAgenda/GetDatePrograms',
                        type: "POST",
                        data: function () {
                            return {
                                term: $scope.recipientSearchTerm,
                            };
                        },
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.recipientSearchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort,
                                date: $scope.Model.Date
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverPaging: true,
            pageSize: 10,
        });

        $scope.weeklyAgendaGridOptions = {
            dataSource: $scope.weeklyAgendaDataSource,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            sortable: true,
            columns: [
                {
                    field: "ProgramTime",
                    title: "Class time",
                    template: "",
                    width: "160px",
                },
                {
                    field: "ProgramName",
                    title: "Class name",
                    width: "200px"
                },
                {
                    field: "Agenda",
                    title: "Agenda",
                },
                {
                    field: "Status",
                    title: "Status",
                    width: "100px"
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template:
                        "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                         #if(ShowActions){#\
                           <li> ... \
				             <ul>\
				                 <li ng-click='editAgenda(#=Id#)'>\
					                 <span class='k-link jbi-remove'>Change agenda</span>\
				                 </li>\
                                #if(Status == 'Stopped'){#\
                                    <li  ng-click='start(#=Id#)'>\
					                  <span class='k-link jbi-remove'>Start</span>\
				                   </li>\
                                #}\
                                else{#\
                                  <li  ng-click='stop(#=Id#)'>\
					                 <span class='k-link jbi-remove'>Stop</span>\
				                 </li>\
                                #}#\
				             </ul>\
			               </li>\
                          #}#\
		                </ul>",
                    attributes: {
                        class: "grid-actions"
                    }
                }
            ]
        };

        $scope.submit = function () {

            $scope.weeklyAgendaGridOptions.dataSource.read();
        };

        $scope.editAgenda = function (id) {

            $http.get('/Dashboard/WeeklyAgenda/GetAgenda?sessionId=' + id).success(function (data, status, headers, config) {

                $scope.Agenda = data;

                $scope.Agenda.ShowEdit = true;
            });


        }

        $scope.cancelEditAgenda = function () {
            $scope.Agenda.ShowEdit = false;
        }

        $scope.start = function (id) {
            jbConfirmation
             .yes(function () {

                 $http.post('/Dashboard/WeeklyAgenda/ScheduleSendAgenda', { sessionId: id })
                 .success(function (data, status, headers, config) {
                     if (data.Status) {
                         $scope.weeklyAgendaGridOptions.dataSource.read();
                         jbToast.success('Started sending agenda successfully.');
                     }
                     else {
                         $scope.MC.handleErrors($scope, data);
                     }
                 })
             })
            .title('Start sending agenda')
            .message('Are you sure you want to start sending agenda?')
            .confirm();
        }

        $scope.stop = function (id) {

            jbConfirmation
             .yes(function () {

                 $http.post('/Dashboard/WeeklyAgenda/StopSendAgenda', { sessionId: id })
                 .success(function (data, status, headers, config) {
                     if (data.Status) {
                         $scope.weeklyAgendaGridOptions.dataSource.read();
                         jbToast.success('Stopped sending agenda successfully.');
                     }
                     else {
                         $scope.MC.handleErrors($scope, data);
                     }
                 })
             })
            .title('Stop sending agenda')
            .message('Are you sure you want to stop sending agenda?')
            .confirm();
        }

        $scope.saveAgenda = function () {

            $http.post('/Dashboard/WeeklyAgenda/EditAgenda', { sessionId: $scope.Agenda.Id, text: $scope.Agenda.Text })
             .success(function (data, status, headers, config) {
                 if (data.Status) {
                     $scope.Agenda.ShowEdit = false;

                     
                     $scope.weeklyAgendaGridOptions.dataSource.read();

                     jbToast.success('Changed agenda successfully.');
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.stopAll = function () {
            jbConfirmation
                 .yes(function () {

                     $http.post('/Dashboard/WeeklyAgenda/StopAll', { date: $scope.Model.Date })
                     .success(function (data, status, headers, config) {
                         if (data.Status) {
                             $scope.weeklyAgendaGridOptions.dataSource.read();
                             jbToast.success('Stopped sending all agendas successfully.');
                         }
                         else {
                             $scope.MC.handleErrors($scope, data);
                         }
                     })
                 })
                .title('Stop sending agendas')
                .message('Are you sure you want to stop sending all agendas?')
                .confirm();
        }


        $scope.startAll = function () {
            jbConfirmation
                .yes(function () {

                    $http.post('/Dashboard/WeeklyAgenda/SchedsuleAll', { date: $scope.Model.Date })
                    .success(function (data, status, headers, config) {
                        if (data.Status) {
                            $scope.weeklyAgendaGridOptions.dataSource.read();
                            jbToast.success('Started sending all agendas successfully.');
                        }
                        else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    })
                })
               .title('Start sending agendas')
               .message('Are you sure you want to start sending all agendas?')
               .confirm();
        }

    }]);
})();

