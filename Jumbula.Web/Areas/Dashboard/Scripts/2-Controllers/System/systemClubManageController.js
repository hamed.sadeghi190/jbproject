﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemClubManageController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'members-manage-view';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SystemClubs", order: 1, menu: "SystemClubs" });

        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/System";
        $scope.pageTitle = "Add Club";
        $scope.clubId = 0;
        $scope.commisionRateRange = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        if ($stateParams.clubId) {
            $scope.clubId = $stateParams.clubId;

        }
        if ($scope.clubId > 0)
        {
            $scope.pageTitle = "Edit Club";
        }

        $scope.Model = {}

        $scope.saveClub = function () {

            $http.post($scope.baseUrl + '/SaveClub', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $state.go('SystemClubs');
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };


    }]);
})();