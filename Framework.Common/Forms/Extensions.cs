﻿using Jb.Framework.Common.Forms.Validation;
using System.Collections.Generic;

namespace Jb.Framework.Common.Forms
{
    public static class Extensions
    {
        public static void AddRange(this IList<BaseElementValidator> list, List<BaseElementValidator> newItems)
        {
            foreach (var item in newItems)
            {
                list.Add(item);
            }
        }
    }
}
