﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.CreditCard;

namespace SportsClub.Models
{
    public class BillingMethodsViewModel
    {
        public List<ParentBillingMethodsItemViewModel> BillingMethodsItem { get; set; }

        public ParentBillingMethodsViewModel BillingMethods { get; set; }
    }
}