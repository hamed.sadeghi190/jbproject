﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum BalanceStatus
    {
        [Description("Paid")]
        Paid,
        [Description("Past due")]
        PastDue,
        [Description("Current")]
        Current
    }
}
