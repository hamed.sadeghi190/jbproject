﻿using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Jumbula.Core.Domain
{
    public class JbUser : IdentityUser<int, JbUserLogin, JbUserRole, JbUserClaim>
    {
        public string TagName { get; set; }

        [MaxLength(500)]
        public string Token { get; set; }

        [MaxLength(100)]
        public string NotBrandedToken { get; set; }

        [StringLength(128)]
        public string LastLoggedInClubDomain { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<JbUser, int> manager, string additionalData)
        {
            // Note the authenticationType must match the one defined in
            // CookieAuthenticationOptions.AuthenticationType 
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            // Add custom user claims here 
            userIdentity.AddClaim(new Claim("AdditionalData", additionalData));

            return userIdentity;
        }
    }

    public class JbRole : IdentityRole<int, JbUserRole>
    {
        public JbRole() : base() { }
        public JbRole(string name) { Name = name; }
    }

    public class JbUserRole : IdentityUserRole<int>
    {

        public int Id { get; set; }

        public override int UserId { get; set; }

        public virtual JbUser User { get; set; }

        public override int RoleId { get; set; }

        public virtual JbRole Role { get; set; }

    }

    public class JbUserLogin : IdentityUserLogin<int>
    {
    }

    public class JbUserClaim : IdentityUserClaim<int>
    {
    }

    public class JbUserStore : UserStore<JbUser, JbRole, int, JbUserLogin, JbUserRole, JbUserClaim>
    {
        public JbUserStore(IdentityDbContext<JbUser, JbRole, int, JbUserLogin, JbUserRole, JbUserClaim> context)
            : base(context)
        {
        }
    }

    public class JbRoleStore : RoleStore<JbRole, int, JbUserRole>
    {
        public JbRoleStore(IdentityDbContext<JbUser, JbRole, int, JbUserLogin, JbUserRole, JbUserClaim> context)
            : base(context)
        {
        }

    }

   
}
