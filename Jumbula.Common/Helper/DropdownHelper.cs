﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace Jumbula.Common.Helper
{
    public static class DropdownHelper
    {
        public static List<SelectListItem> GetEnumList<T>(string defaultItem = null) where T : struct, IComparable, IFormattable, IConvertible
        {
            var values = Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(d => new SelectListItem { Text = EnumHelper.GetEnumDescription(d), Value = d.ToString(CultureInfo.InvariantCulture) })
                .ToList();

            if (!string.IsNullOrEmpty(defaultItem))
            {
                values.Insert(0, new SelectListItem { Text = defaultItem, Value = defaultItem.Trim().Replace(" ", string.Empty) });
            }

            return values;
        }

        public static void AddItemToFirst(this List<SelectListItem> items, DefaultDropdownItem item)
        {
            items.Insert(0, GetSelectListItemFromDefaultItem(item));
        }

        public static void AddItemAtEnd(this List<SelectListItem> items, DefaultDropdownItem item)
        {
            items.Add(GetSelectListItemFromDefaultItem(item));
        }

        public static void AddItemAtEnd(this List<SelectListItem> items, string value, string text, string group)
        {
            items.Add(new SelectListItem { Value = value, Text = text, Group = new SelectListGroup { Name = group } });
        }

        private static SelectListItem GetSelectListItemFromDefaultItem(DefaultDropdownItem item)
        {
            var text = item.ToDescription();
            string value;

            switch (item)
            {
                case DefaultDropdownItem.Empty:
                case DefaultDropdownItem.All:
                    {
                        value = null;
                    }
                    break;
                case DefaultDropdownItem.Other:
                    {
                        value = item.ToString();
                    }
                    break;
                case DefaultDropdownItem.NotListed:
                    {
                        value = item.ToString();
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(item), item, null);
            }

            return new SelectListItem { Text = text, Value = value };
        }
    }
}
