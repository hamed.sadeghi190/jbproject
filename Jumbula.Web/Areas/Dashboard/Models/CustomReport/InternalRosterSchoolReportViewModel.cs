﻿using System;
using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class InternalRosterSchoolReportViewModel
    {
        public string ClassName { get; set; }
        public string GradesOrAges { get; set; }
        public long ClassId { get; set; }
        public int MinimumEnrollments { get; set; }
        public int Capacity { get; set; }
        public int Enrolled { get; set; }
        public string TimePeriod { get; set; }
        public string Room { get; set; }
        public int Pending { get; set; }
        public int WaitList { get; set; }
        public string SchoolName { get; set; }
        public string SeasonName { get; set; }
        public string ClassDays { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public List<string> DatesOfDay { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ProgramDate { get; set; }

        public string StrTime {
            get {
                return string.Format("{0}-{1}", StartTime, EndTime);
            }
        }
        public string Instructor { get; set; }
        public string Location { get; set; }
        public List<InternalRosterReportParticipantsSchoolViewModel> ProgramParticipants { get; set; }

    }

}