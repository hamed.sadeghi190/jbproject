﻿

namespace Jumbula.Common.Enums
{
    public enum RegistrationStep : byte
    {
        Step1 = 1,
        Step2 = 2,
        Step3 = 3,
        Step4 = 4,
        Step5 = 5,
        Step6 = 6
    }

}
