﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('advanceSettingsController', ['$scope', '$http', function ($scope, $http) {
        window.app.globalObjects.pageClass = 'school-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AdvanceSettings", order: 2, menu: "SETTINGS" });
    }]);
})();