﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('advanceSettingsBackgroundCheckController', ['$scope', '$http', 'jbToast', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AdvanceSettings", order: 2, menu: "SETTINGS" });



        $scope.addInput = function (filename, fileurl, date) {

            if (filename) {

                $scope.Model.BackgroundCertificatesNames.push(filename);
                $scope.Model.BackgroundCertificatesURLs.push(fileurl);
                $scope.Model.BackgroundCertificatesDates.push(date);
               
            }
        }

        $scope.removeInput = function (index) {
            $scope.Model.BackgroundCertificatesNames.splice(index, 1);
            $scope.Model.BackgroundCertificatesURLs.splice(index, 1);
            $scope.Model.BackgroundCertificatesDates.splice(index, 1);
        }

        $http.get('Dashboard/AdvanceSetting/EditAdvanceSettingBackgroundCheck').success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.removeFile = function (fileIndex) {
            jbConfirmation
             .yes(function () {
                 var files = $scope.Model.BackgroundCertificatesNames[fileIndex];
                 $http.post('KendoEditorFiles/RemoveUplodedFile', { fileNames: files, location: '/InstructorBackgroundCheck/' })
                 .success(function (data, status, headers, config) {

                     if (data.Status == true) {
                         $scope.removeInput(fileIndex);

                         jbToast.warning("The file has been removed successfully. Make sure you click on the Save button.", "");
                     }
                     else {
                         jbToast.error(data.Message);
                     }
                 })
             })
              .title('Delete File')
              .message('Are you sure you want to delete this file?')
              .confirm();
        }
        $scope.save = function () {



            $http.post('Dashboard/AdvanceSetting/EditAdvanceSettingBackgroundCheck', { model: $scope.Model })
         .success(function (data, status, headers, config) {

             if (data.Status == true) {
                 $scope.errors = null;
                 jbToast.success("Background check certificates updated successfully.", "");
                 $state.go('AdvanceSetting');
             }
             else {
                 $scope.MC.handleErrors($scope, data);
             }
         })
        }

        $scope.KendoUploadOptions = {
            localization: {
                select: "Upload a document",
            },
            success: function (e) {
                if (e.response.Status) {
                    $scope.addInput(e.response.FileName, e.response.URL, e.response.UploadDate);

                    $scope.$apply();
                    jbToast.warning("You have not saved your uploads. If you leave this page, your uploads will not save to your portal. Make sure you click on the Save button ", "");
                }
                else {
                    jbToast.error(e.response.message);
                }
            },
            error: function (e) {
                jbToast.error(e.message);
            }
        }

        $scope.onSelect = function (e) {

            if (e.files[0].extension != ".png" && e.files[0].extension != ".jpg" && e.files[0].extension != ".pdf" && e.files[0].extension != ".doc") {
                e.preventDefault();
                jbToast.error("Please upload an image, a PDF or a word file");

            }

        };
    }])

})();