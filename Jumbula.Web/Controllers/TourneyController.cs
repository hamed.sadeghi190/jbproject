﻿using System;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Web.Mvc.Controllers;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    [Authorize(Roles = "Admin, SysAdmin, Support,Parent, Manager")]

    public class TourneyController : JbBaseController
    {
        #region Anonymous Calls

        [AllowAnonymous]
        public virtual ActionResult DebateTourney()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult DebateReg()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult Display(string domain, string clubDomain, int categoryId)
        {
            if (categoryId.Equals(CategoryModel.ChessId))
            {
                return RedirectToAction("DisplayTourney", "Chess", new { domain = domain, clubDomain = clubDomain });
            }
            else if (categoryId.Equals(CategoryModel.TableTennisId))
            {
                return RedirectToAction("DisplayTourney", "TableTennis", new { domain = domain, clubDomain = clubDomain });
            }
            else
            {
                throw new NotSupportedException(string.Format(Constants.F_SportCategoryNotSupported, ""));
            }
        }

        [AllowAnonymous]
        public virtual ActionResult ViewEntriesTourney(string clubDomain, string domain, int categoryId, TourneyRewardType? type, string eventName)
        {
            if (categoryId.Equals(CategoryModel.ChessId))
            {
                return RedirectToAction("ViewEntriesTourneyChess", "Chess", new { clubDomain = clubDomain, domain = domain, tournetType = type, tourneyName = eventName });
            }
            else if (categoryId.Equals(CategoryModel.TableTennisId))
            {
                return RedirectToAction("ViewEntriesTourneyTableTennis", "TableTennis", new { clubDomain = clubDomain, domain = domain });
            }
            else
            {
                throw new NotSupportedException(string.Format(Constants.F_SubDomainCategoryNotSupported, SubDomainCategories.Tourney));
            }
        }
     
      

        #endregion

        #region Authorized Player Calls

        [Authorize(Roles = "Parent")]
        public virtual ActionResult Register(string domain, string clubDomain, int categoryId)
        {

            if (categoryId.Equals(CategoryModel.ChessId))
            {
                return RedirectToAction("Register", "Chess", new { domain = domain, clubDomain = clubDomain });
            }
            else if (categoryId.Equals(CategoryModel.TableTennisId))
            {
                return RedirectToAction("Register", "TableTennis", new { domain = domain, clubDomain = clubDomain });
            }
            else
            {
                throw new NotSupportedException(string.Format(Constants.F_SportCategoryNotSupported, ""));
            }
        }

        #endregion

        #region Admin Authenticated Calls

        public virtual ActionResult CreateFromStandard(string clubDomain, int categoryId)
        {
            if (categoryId.Equals(CategoryModel.ChessId))
            {
                return RedirectToAction("Create", "Chess");
            }
            else if (categoryId.Equals(CategoryModel.TableTennisId))
            {
                return RedirectToAction("CreateTourneyFromStandard", "TableTennis", new { clubDomain = clubDomain });
            }
            else
            {
                throw new NotSupportedException(string.Format(Constants.F_SportCategoryNotSupported, ""));
            }
        }

        public virtual ActionResult CreateFromTemplate(string clubDomain, string domain, int categoryId)
        {

            if (categoryId.Equals(CategoryModel.ChessId))
            {
                return RedirectToAction("CreateTourneyFromTourney", "Chess", new { domain = domain, clubDomain = clubDomain });
            }
            else
            {
                throw new NotSupportedException(string.Format(Constants.F_SportCategoryNotSupported, ""));
            }
        }

        public virtual ActionResult Edit(string domain, string clubDomain, int categoryId, bool fromTemplate = false)
        {

            if (categoryId.Equals(CategoryModel.ChessId))
            {
                return RedirectToAction("Edit", "Chess", new { domain = domain, clubDomain = clubDomain, fromTemplate = fromTemplate });
            }
            else if (categoryId.Equals(CategoryModel.TableTennisId))
            {
                return RedirectToAction("Edit", "TableTennis", new { domain = domain, clubDomain = clubDomain, fromTemplate = fromTemplate });
            }
            else
            {
                throw new NotSupportedException(string.Format(Constants.F_SportCategoryNotSupported, ""));
            }
        }
        #endregion
    }
}
