using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Infrastructure.Flyer;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.UserService;
using Kendo.Mvc.Extensions;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Areas.Dashboard.Models.CustomReport;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class CustomReportController : DashboardBaseController
    {
        private readonly IProgramBusiness _programBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IReportBusiness _reportBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IPlayerProgramNoteBusiness _playerProgramNoteBusiness;

        public CustomReportController(IProgramBusiness programBusiness, IClubBusiness clubBusiness, ISeasonBusiness seasonBusiness, IReportBusiness reportBusiness, IOrderItemBusiness orderItemBusiness, IPlayerProgramNoteBusiness playerProgramNoteBusiness)
        {
            _programBusiness = programBusiness;
            _clubBusiness = clubBusiness;
            _seasonBusiness = seasonBusiness;
            _reportBusiness = reportBusiness;
            _orderItemBusiness = orderItemBusiness;
            _playerProgramNoteBusiness = playerProgramNoteBusiness;
        }


        public virtual ActionResult Report()
        {
            return View("CreateEditReport");
        }
        public virtual ActionResult RosterReportEdit()
        {
            return View();
        }
        public virtual ActionResult ReportRun()
        {
            return View();
        }

        public virtual ActionResult ReportSelectPrograms()
        {
            return View();
        }

        public virtual ActionResult CapacityReport()
        {
            return View();
        }

        public virtual ActionResult InstallmentReport()
        {
            return View("InstallmentReport");
        }
        public virtual ActionResult ProviderFinancialReport()
        {
            return View();
        }
        public virtual ActionResult CoordinatorFinancialReport()
        {
            return View();
        }
        public virtual ActionResult FollowupFormReport()
        {
            return View("FollowupReport");
        }

        public virtual ActionResult BalanceReport()
        {
            return View();
        }

        public virtual ActionResult ChargesReport()
        {
            return View();
        }

        public virtual ActionResult Finance()
        {
            return View();
        }
        public virtual ActionResult FinanceTransaction()
        {
            return View();
        }

        public virtual ActionResult CouponReport()
        {
            return View();
        }

        public virtual ActionResult ShareAsAttachment()
        {
            return View();
        }

        public virtual ActionResult CheckinReport()
        {
            return View();
        }
        public virtual ActionResult PerRegistrationReport()
        {
            return View();
        }
        public virtual ActionResult SignOutSheetReport()
        {
            return View();
        }

        public virtual ActionResult DismissalInformationReport()
        {
            return View();
        }
        public virtual ActionResult DailyDismissalReport()
        {
            return View();
        }

        public virtual ActionResult ProviderContactInformationReport()
        {
            return View();
        }

        public virtual ActionResult InstructorListReport()
        {
            return View();
        }

        public virtual ActionResult InstructorAssignmentsReport()
        {
            return View();
        }

        public virtual ActionResult ParentEmailsListReport()
        {
            return View();
        }

        public virtual ActionResult RoomAssignmentsAdminReport()
        {
            return View();
        }

        public virtual ActionResult StudentAttendanceReport()
        {
            return View();
        }
        public virtual ActionResult StudentSummaryAttendanceReport()
        {
            return View();
        }

        public virtual ActionResult CustomAttendanceReport()
        {
            return View();
        }

        public virtual ActionResult RosterReport()
        {
            return View();
        }
        public virtual ActionResult AdvanceRosterReport()
        {
            return View();
        }
        public virtual ActionResult CampRosterReport()
        {
            return View();
        }
        public virtual ActionResult SubscriptionInternalReport()
        {
            return View();
        }
        public virtual ActionResult SubscriptionRosterReport()
        {
            return View();
        }
        public virtual ActionResult SubscriptionSingOutReport()
        {
            return View();
        }
        public virtual ActionResult InstructorRosterReport()
        {
            return View();
        }
        public virtual ActionResult SignOutSheetAdvancedReport()
        {
            return View();
        }

        public virtual ActionResult SignOutSheetAdvancedV2Report()
        {
            return View();
        }

        public virtual ActionResult AttendanceSheetAdvancedReport()
        {
            return View();
        }

        public virtual ActionResult InternalRosterSchoolReport()
        {
            return View();
        }

        public virtual ActionResult InternalRosterSchoolReport_v2()
        {
            return View();
        }

        public virtual ActionResult AllergyMedicalConditionReport()
        {
            return View();
        }

        public virtual ActionResult SubscriptionAttendanceReport()
        {
            return View();
        }
        public virtual ActionResult BaseFilters()
        {
            return View("_BaseFilters");
        }

        public virtual JsonResult CreateEditReport(int? id, string seasonDomain, CustomReportType? customType)
        {
            var club = ActiveClub;
            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Id;

            if (!id.HasValue)
            {
                var model = new ProgramReportViewModel(club.Id, seasonId, customType.Value);

                return Json(model, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var reportTemplate = Ioc.ReportBusiness.GetReport(id.Value);

                if (reportTemplate.EventType == EventRoasterType.Roster)
                {
                    var rawJbForm = new ProgramReportViewModel(club.Id, seasonId, CustomReportType.Admin).JbForm;

                    var checkedElementsJbForm = Ioc.CustomReportBusiness.RemoveUncheckedElementsJbForm(reportTemplate.JbForm);

                    reportTemplate.JbForm = MergeCheckedElementsJbForm(checkedElementsJbForm, rawJbForm);
                }

                UpdateMoreInfoSection(reportTemplate.JbForm, club.Id, seasonId);
                var model = new ProgramReportViewModel
                {
                    Description = reportTemplate.Description,
                    Id = reportTemplate.Id,
                    JbForm = reportTemplate.JbForm,
                    Name = reportTemplate.Name,
                    SeasonId = reportTemplate.SeasonId,
                    HasHeader = reportTemplate.HasHeader,
                    ReportType = reportTemplate.EventType,
                    CustomReportType = reportTemplate.CustomType,

                    FollowupForms = reportTemplate.FollowupForms.Select(f =>
                            new ProgramReportFollowupItemViewModel
                            {
                                JbForm = f,
                                Title = f.Title,
                                Id = f.Id,
                                IsSelected = true
                            })
                            .ToList()
                };

                var clubFollowupForms = Ioc.ReportBusiness.GetFolloupFormReports(ActiveClub.Id);

                var reportBusiness = Ioc.ReportBusiness;

                foreach (var item in clubFollowupForms.Where(f => !model.FollowupForms.Any(o => o.Title.Equals(f.Title))))
                {
                    item.Id = 0;

                    model.FollowupForms.Add(new ProgramReportFollowupItemViewModel
                    {
                        JbForm = reportBusiness.GenerateReportFollowupForm(item),
                        Title = item.Title,
                        Id = 0,
                        IsSelected = false
                    });
                }

                if (model.CustomReportType == CustomReportType.Parent)
                {
                    model.FollowupForms = null;
                }

                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        private static JbForm MergeCheckedElementsJbForm(JbForm template, JbForm raw)
        {
            raw.CopyByTitle(template);

            return template;
        }

        [HttpPost]
        public virtual ActionResult CreateEditReport(ProgramReportViewModel model)
        {
            if (model.JbForm.Elements.All(e => !((JbSection)e).Elements.Any(i => ((JbCheckBox)i).Value)))
            {
                ModelState.AddModelError("model.NoSelection", "Please check one or more option(s) in report.");
            }

            ModelState.Remove("model.JbForm.CreatedDate");
            ModelState.Remove("model.JbForm.LastModifiedDate");

            for (int i = 0; i < model.FollowupForms.Count; i++)
            {
                ModelState.Remove(string.Format("model.FollowupForms[{0}].JbForm.CreatedDate", i));
                ModelState.Remove(string.Format("model.FollowupForms[{0}].JbForm.LastModifiedDate", i));
            }

            if (ModelState.IsValid)
            {
                var reporttemp = new EventRoaster
                {
                    Name = model.Name,
                    Description = model.Description,
                    JbForm_Id = model.JbForm.Id,
                    JbForm = model.JbForm,
                    SeasonId = model.SeasonId,
                    ClubDomain = ActiveClub.Domain,
                    LastUpdateDate = DateTime.Now,
                    HasHeader = model.HasHeader,
                    CustomType = model.CustomReportType
                };

                reporttemp.JbForm.JsonElements = JsonHelper.JsonSerializer(model.JbForm.Elements);

                foreach (var item in model.FollowupForms.Where(f => f.IsSelected))
                {
                    item.JbForm.JsonElements = JsonHelper.JsonSerializer(item.JbForm.Elements);

                    item.JbForm.RefEntityName = "Reports";
                    item.JbForm.Title = item.Title;
                }

                reporttemp.JbForm.RefEntityName = "Reports";

                reporttemp.FollowupForms = model.FollowupForms.Where(f => f.IsSelected).Select(f => f.JbForm).ToList();

                if (model.Id == 0)
                {
                    reporttemp.JbForm.CreatedDate = DateTime.UtcNow;
                    Ioc.ReportBusiness.Create(reporttemp);
                    model.Id = reporttemp.Id;
                }
                else
                {
                    reporttemp.JbForm.LastModifiedDate = DateTime.UtcNow;
                    reporttemp.Id = model.Id;
                    reporttemp.EventType = model.ReportType;
                    Ioc.ReportBusiness.Update(reporttemp);
                    Ioc.JbFormBusiness.CreateEdit(reporttemp.JbForm);
                    model.Id = reporttemp.Id;

                }

                return JsonNet(new JResult { Data = model.Id.ToString(), Status = true });
            }

            return JsonFormResponse();
        }

        public virtual ActionResult DeleteReport(int reportId)
        {
            var result = Ioc.ReportBusiness.Delete(reportId);
            return JsonNet(new JResult { Status = result.Status, Data = result.ExceptionMessage });
        }

        public virtual JsonNetResult GetAllReports(string seasonDomain)
        {


            var pageSize = Request.Params["PageSize"] != null ? int.Parse(Request.Params["PageSize"]) : 10;
            var page = Request.Params["Page"] != null ? int.Parse(Request.Params["Page"]) : 1;

            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id).Id;

            // get reports in seasons level.
            var reports = Ioc.ReportBusiness.GetReports(ActiveClub.Domain, seasonId);

            // if we need we can get seasons in club level.
            //var reports = Ioc.ReportBusiness.GetReports(JBMembership.GetActiveClubBaseInfo().Domain);

            var dataSource = reports.Where(r => r.EventType == EventRoasterType.Custom).Select(m => new { Title = m.Name.Replace("'", "&apos;"), Value = m.Id, Type = m.CustomType }).OrderByDescending(m => m.Value).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = reports.Count() });
        }

        public virtual JsonNetResult GetAllSeasonPrograms(string seasonDomain, string reportName)
        {
            var club = ActiveClub;
            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Id;

            var programs = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId);

            if (reportName == "DailyDismissal")
            {
                programs = programs.Where(p => p.TypeCategory == ProgramTypeCategory.Class).OrderByDescending(p => p.Id).Select(p => p);
            }
            else if (reportName == "SubscriptionRoster" || reportName == "SignOut" || reportName == "SubscriptionInternal" || reportName == "SubscriptionAttendance")
            {
                programs = programs.Where(p => p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).OrderByDescending(p => p.Id).Select(p => p);
            }
            else if (reportName == "StudentSummaryAttendance" || reportName == "StudentAttendance")
            {
                programs = programs.Where(p => p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).OrderByDescending(p => p.Id).Select(p => p);
            }
            else
            {
                programs = programs.OrderByDescending(p => p.Id).Select(p => p);
            }

            var allPrograms = new List<SelectKeyValue<string>>();

            if (this.GetCurrentUserRole() != RoleCategory.Instructor)
            {
                allPrograms = programs.Select(p => new SelectKeyValue<string> { Text = (p.OutSourceSeasonId.HasValue && ActiveClub.Id != p.Club.Id) ? p.Name + " (" + p.Club.Name + ")" : p.Name, Value = p.Id.ToString() }).ToList();
            }
            else
            {
                var currentUserId = this.GetCurrentUserId();
                allPrograms = programs.Where(p => p.Instructors.Any() && p.Instructors.Select(i => i.JbUserRole.UserId).Contains(currentUserId)).Select(p => new SelectKeyValue<string> { Text = (p.OutSourceSeasonId.HasValue && ActiveClub.Id != p.Club.Id) ? p.Name + " (" + p.Club.Name + ")" : p.Name, Value = p.Id.ToString() }).ToList();
            }

            var model = new { AllPrograms = allPrograms, IsAllPrograms = "true" };

            return JsonNet(model);
        }

        public virtual JsonNetResult GetAllSeasonProgramsCustomReport(string seasonDomain)
        {
            var club = ActiveClub;
            var customReportBusiness = Ioc.CustomReportBusiness;

            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Id;

            var programs = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId)
                .Where(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.ProgramSchedules.FirstOrDefault() != null)
                .OrderByDescending(p => p.Name)
                .ToList();

            var beforeAfterSchedules = programs
                .Where(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
                .Select(p => p.ProgramSchedules)
                .ToList();

            var multiSchedulePrograms = programs
                .Where(p => p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.ChessTournament)
                .Select(p => p.ProgramSchedules)
                .ToList();

            programs.RemoveAll(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare);
            programs.RemoveAll(p => p.TypeCategory == ProgramTypeCategory.Camp);

            var allPrograms = new List<SelectKeyValue<object>>();

            if (this.GetCurrentUserRole() != RoleCategory.Instructor)
            {
                allPrograms.AddRange(programs.Select(p => new SelectKeyValue<object>()
                {
                    Text = (p.OutSourceSeasonId.HasValue && ActiveClub.Id != p.Club.Id) ? (string.Format("{0} ({1}) ({2})", p.Name, p.Club.Name, customReportBusiness.GetProgramDayTime(p.ProgramSchedules.FirstOrDefault()))) : (string.Format("{0} ({1})", p.Name, customReportBusiness.GetProgramDayTime(p.ProgramSchedules.FirstOrDefault()))),
                    Value = p.ProgramSchedules.FirstOrDefault().Id
                }).ToList());

                foreach (var schedules in beforeAfterSchedules)
                {
                    foreach (var schedule in schedules)
                    {
                        allPrograms.Add(new SelectKeyValue<object>()
                        {
                            Text = (schedule.Program.OutSourceSeasonId.HasValue && ActiveClub.Id != schedule.Program.Club.Id) ? schedule.Program.Name + " (" + schedule.Program.Club.Name + ")" + " - " + schedule.Title : schedule.Program.Name + " - " + schedule.Title,
                            Value = schedule.Id
                        });
                    }
                }

                foreach (var schedules in multiSchedulePrograms)
                {
                    allPrograms.Add(new SelectKeyValue<object>()
                    {
                        Text = string.Format("{0} ({1})", schedules.FirstOrDefault().Program.Name, customReportBusiness.GetProgramDayTime(schedules.FirstOrDefault())),
                        Value = schedules.Select(s => s.Id).ToList()
                    });
                }
            }
            else
            {
                var currentUserId = this.GetCurrentUserId();
                allPrograms.AddRange(programs.Where(p => p.Instructors.Any() && p.Instructors.Select(i => i.JbUserRole.UserId).Contains(currentUserId)).Select(p => new SelectKeyValue<object>()
                {
                    Text = (p.OutSourceSeasonId.HasValue && ActiveClub.Id != p.Club.Id) ? (string.Format("{0} ({1}) ({2})", p.Name, p.Club.Name, customReportBusiness.GetProgramDayTime(p.ProgramSchedules.FirstOrDefault()))) : (string.Format("{0} ({1})", p.Name, customReportBusiness.GetProgramDayTime(p.ProgramSchedules.FirstOrDefault()))),
                    Value = p.ProgramSchedules.FirstOrDefault().Id
                }).ToList());
            }

            allPrograms = allPrograms.OrderBy(a => a.Text).ToList();

            var model = new { AllPrograms = allPrograms, IsAllPrograms = "true" };

            return JsonNet(model);
        }

        public virtual JsonNetResult GetReportHeader(int reportId, long? programId)
        {
            var model = new object();

            if (programId.HasValue)
            {
                model = getReportHeader(reportId, programId.Value);
            }
            else
            {
                model = getReportHeader(reportId);
            }

            return JsonNet(model);
        }

        public virtual JsonNetResult GetCustomReportHeader(int reportId, long? scheduleId)
        {
            var model = new object();

            if (scheduleId.HasValue)
            {
                model = getReportHeaderBySchedule(reportId, scheduleId.Value);
            }
            else
            {
                model = getReportHeader(reportId);
            }

            return JsonNet(model);

        }
        private object getReportHeader(int reportId)
        {
            object result = null;
            var report = Ioc.ReportBusiness.GetReport(reportId);

            result = new
            {
                SeasonName = report.Season.Title,
                ReportName = report.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt")
            };

            return result;
        }

        [HttpPost]
        public virtual ActionResult Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        private object getReportHeader(int reportId, long programId)
        {
            object result = null;

            var programBusiness = Ioc.ProgramBusiness;

            var program = programBusiness.Get(programId);

            var report = Ioc.ReportBusiness.GetReport(reportId);

            if (report.HasHeader)
            {
                result = new
                {
                    SeasonName = report.Season.Title,
                    ProgramName = program.Name,
                    RoomAssignment = program.Room ?? string.Empty,
                    Days = string.Join(", ", programBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList()),
                    Date = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt")
                };
            }

            return result;
        }

        private object getReportHeaderBySchedule(int reportId, long scheduleId)
        {
            object result = null;

            var programBusiness = Ioc.ProgramBusiness;

            var program = programBusiness.GetByScheduleId(scheduleId);

            var report = Ioc.ReportBusiness.GetReport(reportId);

            result = new
            {
                SeasonName = report.Season.Title,
                ProgramName = program.Name,
                RoomAssignment = program.Room ?? string.Empty,
                Days = string.Join(", ", programBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList()),
                Date = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt")
            };

            return result;
        }

        [HttpGet]
        public virtual ActionResult GetAdvanceClassRosterReportDetail(string seasonDomain)
        {
            var season = _seasonBusiness.Get(seasonDomain, ActiveClub.Id);
            var club = _clubBusiness.Get(ActiveClub.Id);

            var reportRoster = _reportBusiness.GetClubRosterReport(club.Domain, season.Id);

            var data = new
            {
                ClubName = club.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString(Constants.DefaultDateTimeFormatAbrMonthWithAMPM),
                ReportId = reportRoster == null ? BuildDefaultRosterReport(club.Domain, season) : reportRoster.Id,
            };

            return JsonNet(data);
        }
        [HttpGet]
        public virtual ActionResult GetRosterReportDetail(long programId)
        {
            var program = Ioc.ProgramBusiness.Get(programId);

            var data = new
            {
                ProgramName = program.Name,
                ClubName = (program.ClubId != ActiveClub.Id && program.OutSourceSeasonId.HasValue) ? string.Format("({0})", program.Club.Name) : string.Empty,
                Room = program.Room != null ? program.Room : string.Empty,
                Days = string.Join(", ", Ioc.ProgramBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList()),
                Date = Ioc.ClubBusiness.GetClubDateTime(program.ClubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt"),//Sep 21, 2016 HH:MM

            };

            return JsonNet(data);
        }
        [HttpGet]
        public virtual ActionResult GetSubscriptionRosterReportDetail(long programId)
        {
            var program = Ioc.ProgramBusiness.Get(programId);

            var data = new
            {
                ProgramName = program.Name,
                ClubName = (program.ClubId != ActiveClub.Id && program.OutSourceSeasonId.HasValue) ? string.Format("({0})", program.Club.Name) : string.Empty,
                Room = program.Room != null ? program.Room : string.Empty,
                Days = string.Join(", ", Ioc.ProgramBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList()),
                Date = Ioc.ClubBusiness.GetClubDateTime(program.ClubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt"),
                StartDate = DateTime.Now.Date,
                EndDate = DateTime.Now.Date.AddDays(7),
            };

            return JsonNet(data);
        }

        [HttpPost]
        public virtual ActionResult RunReport(SelectedPrograms model, bool makeCampaign = false)
        {
            var club = ActiveClub;

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            var filterable = (model.Filters != null) &&
                             (model.Filters.RegistrationStart.HasValue || model.Filters.RegistrationEnd.HasValue ||
                              model.Filters.Status.HasValue || model.Filters.Tuition.HasValue || model.Filters._SelectedPrograms != null);

            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms =
                    Ioc.ProgramBusiness.GetSeasonPrograms(seasonId)
                        .Select(p => p.Id).ToList();
            }

            var eventRoaster = Ioc.ReportBusiness.GetReport(model.ReportId);

            var allOrderItems = Ioc.OrderItemBusiness.GetReportOrderItems(model._SelectedPrograms, OrderItemStatusCategories.showAll);
            allOrderItems = filterable
                ? ApplyBaseFilters(model.Filters, allOrderItems)
                : allOrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.completed);

            var orderItemPaging = allOrderItems.OrderBy(o => o.Id)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();


            if (makeCampaign)
            {
                var recipients =
                    allOrderItems.Select(o => new ReportRecipient
                    {
                        Email = o.Order.User.UserName,
                        Lastname = o.LastName,
                        //FirstName = o.FirstName
                    })
                    .Distinct()
                    .ToList();

                var result = MakeCampaign(recipients, club.Domain, model.ReportName);

                return Json(new { result.Status, Data = result.RecordsAffected, Message = result.ExceptionMessage });
            }

            var finalData = new List<Dictionary<string, object>>();
            var titles = new List<string>();

            switch (eventRoaster.CustomType)
            {
                case CustomReportType.Admin:
                    {
                        var data = GetProgramReportModel(eventRoaster, orderItemPaging);

                        var baseJbForm = Ioc.CustomReportBusiness.RemoveUncheckedElementsJbForm(eventRoaster.JbForm);

                        if (eventRoaster.FollowupForms != null)
                        {
                            foreach (var followup in eventRoaster.FollowupForms)
                            {
                                baseJbForm.Elements.AddRange(followup.Elements);
                            }
                        }

                        finalData = FillCustomReport(data, ref titles, baseJbForm, eventRoaster.EventType);
                    }
                    break;

                case CustomReportType.Parent:
                    {
                        finalData = Ioc.CustomReportBusiness.GetCustomReportParentInfo(eventRoaster, orderItemPaging, ref titles);
                    }
                    break;
            }

            if (filterable)
            {
                page = 1;
            }

            var dataSource = finalData;
            return JsonNet(new { DataSource = dataSource, TotalCount = allOrderItems.Count(), Titles = titles });
        }

        [HttpPost]
        public virtual JsonNetResult RosterReportRun(long programId)
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId);

            if (orderItems == null || !orderItems.Any())
            {
                return JsonNet(new { DataSource = new List<object>(), TotalCount = 0 });
            }

            var formBusiness = Ioc.FormBusiness;

            var data = orderItems.ToList()
                .Select(i => new
                {
                    ParticipantFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName),
                    ParticipantLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName),
                    ParticipantGender = formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.Gender),

                    SchoolGrade = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.Grade),
                    SchoolTeacherName = formBusiness.GetTeacher(i.JbForm),

                    SchoolDismissalFromEnrichment = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }),

                    ParentFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName),
                    ParentLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName),
                    ParentEmail = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Email),
                    ParentPrimaryPhone = formBusiness.GetParentPhone(i.JbForm),
                    ParentAlternatePhone = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.AlternatePhone, new List<string> { "Alternate phone", "Alternative phone" }),

                    Parent2FirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName),
                    Parent2LastName = formBusiness.GetFormValue(i.JbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName),
                    Parent2Email = formBusiness.GetFormValue(i.JbForm, SectionsName.Parent2GuardianSection, ElementsName.Email),
                    Parent2PrimaryPhone = formBusiness.GetParent2Phone(i.JbForm),
                    Parent2AlternatePhone = formBusiness.GetFormValue(i.JbForm, SectionsName.Parent2GuardianSection, ElementsName.AlternatePhone, new List<string> { "Alternate phone", "Alternative phone" }),

                    ParentAuthorizedAdult1 = formBusiness.GetAuthorizedAdult(i.JbForm, null),
                    ParentAuthorizedAdultPhone1 = formBusiness.GetAuthorizedAdultPhone(i.JbForm, null),

                    ParentAuthorizedAdult2 = formBusiness.GetAuthorizedAdult(i.JbForm, SectionsName.AuthorizedPickup2),
                    ParentAuthorizedAdultPhone2 = formBusiness.GetAuthorizedAdultPhone(i.JbForm, SectionsName.AuthorizedPickup2),

                    ParentAuthorizedAdult3 = formBusiness.GetAuthorizedAdult(i.JbForm, SectionsName.AuthorizedPickup3),
                    ParentAuthorizedAdultPhone3 = formBusiness.GetAuthorizedAdultPhone(i.JbForm, SectionsName.AuthorizedPickup3),

                    ParentAuthorizedAdult4 = formBusiness.GetAuthorizedAdult(i.JbForm, SectionsName.AuthorizedPickup4),
                    ParentAuthorizedAdultPhone4 = formBusiness.GetAuthorizedAdultPhone(i.JbForm, SectionsName.AuthorizedPickup4),

                    EmergencyContactFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName),
                    EmergencyContactLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.LastName),
                    EmergencyContactPhone = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.HomePhone, new List<string> { "Emergency contact phone number", "Emergency contact phone", "Phone" }),


                    Medical_AllergiesMedicalInfo_SpecialNeeds = formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, ElementsName.MedicalConditions),
                    Medical_AllergiesAllergies = formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, "Allergies")
                }
                )
                .ToList();

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = data.Count });
        }

        [HttpPost]
        public virtual JsonNetResult SubscriptionRosterReportRun(long programId, DateTime? startDate, DateTime? endDate)
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var data = GetSubscriptionRosterData(startDate, endDate, programId);

            var count = data.Count();

            if (count < 1)
            {
                return JsonNet(new { DataSource = new List<object>(), TotalCount = 0 });
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = count });
        }

        public virtual ActionResult GetSubscriptionSessionsPdf(long programId, DateTime? startDate, DateTime? endDate)
        {

            dynamic model = new System.Dynamic.ExpandoObject();
            var program = Ioc.ProgramBusiness.Get(programId);

            model.Date = Ioc.ClubBusiness.GetClubDateTime(program.ClubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt");//Sep 21, 2016 HH:MM

            var data = GetSubscriptionRosterData(startDate, endDate, programId);

            var count = data.Count();

            model.Data = data;
            var allKeys = data.SelectMany(d => d.Keys).Distinct().ToList();
            model.Keys = allKeys;

            return Pdf(program.Season.Domain + "_" + program.Domain + " (Subscription-roster-report)", "SubscriptionRosterReportPdfExport", model);
        }

        private List<Dictionary<string, string>> GetSubscriptionRosterData(DateTime? startDate, DateTime? endDate, long programId)
        {
            if (!startDate.HasValue)
            {
                startDate = DateTime.Now;
                endDate = startDate.Value.AddDays(7);
            }

            if (!endDate.HasValue)
            {
                endDate = startDate.Value.AddDays(7);
            }

            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId);

            if (orderItems == null || !orderItems.Any())
            {
                return new List<Dictionary<string, string>>();// JsonNet(new { DataSource = new List<object>(), TotalCount = 0 });
            }

            var formBusiness = Ioc.FormBusiness;

            var data = orderItems.ToList()
                .Select(i =>
                    GetSubscriptionSessions(i, startDate.Value, endDate.Value)
                  )
                  .ToList();

            var orderedData = new List<SubscripitonRosterReportModel>();

            orderedData = data.Select(s =>
              new SubscripitonRosterReportModel
              {
                  StudentName = s.StudentName,
                  Grade = s.Grade,
                  TeacherName = s.TeacherName,
                  ParentPhoneNumber = s.ParentPhoneNumber,
                  Sessons = s.Sessons.OrderBy(o => o.SessionDateTime).ToList(),
              }
            )
            .ToList();

            var finalData = new List<Dictionary<string, string>>();

            var headerObject = new SubscripitonRosterReportModel();
            var header = new Dictionary<string, string>();

            finalData.Add(header);

            headerObject.StudentName = "";

            foreach (var item in orderedData)
            {
                var dic = new Dictionary<string, string>();

                dic.Add("Name", item.StudentName);
                dic.Add("Grade", item.Grade);
                dic.Add("Teacher_name", item.TeacherName);
                dic.Add("Parent_phone", item.ParentPhoneNumber);

                foreach (var keyVal in item.Sessons)
                {
                    var key = Regex.Replace(keyVal.SessionTitle, "[^0-9a-zA-Z_]+", "_");

                    dic.Add(key, keyVal.IsPresent ? "Yes" : "");


                    if (!headerObject.Sessons.Select(s => s.SessionDateTime).Contains(keyVal.SessionDateTime))
                    {
                        headerObject.Sessons.Add(new SubscriptionRosterItem { SessionDateTime = keyVal.SessionDateTime });
                    }
                }

                finalData.Add(dic);
            }

            headerObject.Sessons = headerObject.Sessons.OrderBy(o => o.SessionDateTime).ToList();

            header.Add("Name", string.Empty);
            header.Add("Grade", string.Empty);
            header.Add("Teacher_name", string.Empty);
            header.Add("Parent_phone", string.Empty);

            foreach (var item in headerObject.Sessons)
            {
                header.Add(Regex.Replace(item.SessionTitle, "[^0-9a-zA-Z_]+", "_"), "");
            }

            var footerRow = new Dictionary<string, string>();

            footerRow.Add("Name", "Total");

            foreach (var item in header)
            {
                var key = item.Key;

                var counts = finalData.SelectMany(f => f).Where(d => d.Key == key).Count(s => s.Value.Equals("Yes"));

                if (!footerRow.ContainsKey(key))
                {
                    footerRow.Add(key, counts != 0 ? counts.ToString() : string.Empty);
                }
            }

            finalData.Add(footerRow);


            return finalData;
        }

        private SubscripitonRosterReportModel GetSubscriptionSessions(OrderItem orderItem, DateTime startDate, DateTime endDate)
        {
            var result = new SubscripitonRosterReportModel();
            var formBusiness = Ioc.FormBusiness;

            var player = orderItem.Player;

            result.StudentName = player.Contact.FullName;
            result.Grade = formBusiness.GetPlayerGradeAsString(orderItem.JbForm) != null ? formBusiness.GetPlayerGradeAsString(orderItem.JbForm) : string.Empty;
            result.TeacherName = formBusiness.GetApolloTeacher(orderItem.JbForm);
            result.ParentPhoneNumber = formBusiness.GetParentPhone(orderItem.JbForm);

            var orderSessons = orderItem.OrderSessions.Where(o => o.ProgramSession.StartDateTime >= startDate && o.ProgramSession.StartDateTime <= endDate).ToList();


            foreach (var session in orderSessons)
            {
                var sessionItem = new SubscriptionRosterItem()
                {
                    IsPresent = true,
                    SessionDateTime = session.ProgramSession.StartDateTime,
                };

                result.Sessons.Add(sessionItem);
            }

            return result;
        }

        [HttpPost]
        public virtual JsonNetResult InstructorRosterReportRun(long programId)
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId);

            if (orderItems == null || !orderItems.Any())
            {
                return JsonNet(new { DataSource = new List<object>(), TotalCount = 0 });
            }

            var data = orderItems.ToList()
                .Select(i => new
                {
                    ParticipantFirstName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName),
                    ParticipantLastName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName),
                    ParticipantGender = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.Gender),

                    SchoolGrade = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.Grade),

                    ParentFirstName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName),
                    ParentLastName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName),
                    ParentEmail = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Email),
                    ParentPrimaryPhone = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }),

                    EmergencyContactFirstName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName),
                    EmergencyContactLastName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.LastName),
                    EmergencyContactPhone = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.HomePhone, new List<string> { "Emergency contact phone number", "Emergency contact phone", "Phone" }),
                }
                )
                .ToList();

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = data.Count });
        }

        [HttpPost]
        public virtual ActionResult KendoExportPdf(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public virtual JsonNetResult FillBaseFilters(SelectedPrograms model, string reportType = "Admin")
        {
            var club = ActiveClub;

            if (model.IsAllPrograms)
            {
                var season = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id);
                model._SelectedPrograms =
                    Ioc.ProgramBusiness.GetSeasonPrograms(season.Id).Select(p => p.Id).ToList();
            }

            if (!model.IsAllPrograms && reportType == "Parent")
            {
                var programBusiness = Ioc.ProgramBusiness;
                for (int i = 0; i < model._SelectedPrograms.Count; i++)
                {
                    var schedule = model._SelectedPrograms[i];
                    model._SelectedPrograms[i] = Ioc.ProgramBusiness.GetByScheduleId(schedule).Id;
                }
                model._SelectedPrograms = model._SelectedPrograms.Distinct().ToList();
            }

            var tuitionsList = new List<SelectKeyValue<string>>();
            var orderStatusList = new List<SelectKeyValue<string>>();
            Program programDb;

            tuitionsList.Add(new SelectKeyValue<string>
            {
                Text = "All",
                Value = ""
            });
            if (model._SelectedPrograms != null)
            {
                foreach (var programId in model._SelectedPrograms)
                {
                    programDb = Ioc.ProgramBusiness.Get(programId);

                    var tuitions =
                        programDb.ProgramSchedules.Where(s => !s.IsDeleted).SelectMany(
                            programschedule =>
                                programschedule.Charges.Where(
                                    c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee));

                    tuitionsList.AddRange(
                        tuitions.Select(t => new SelectKeyValue<string>
                        {
                            Text = string.Format("{0}", _programBusiness.GetProgramNameCustomReport(t.ProgramSchedule, t.Name)),
                            Value = t.Id.ToString()
                        }));
                }
            }

            orderStatusList.AddRange(
                Enum.GetValues(typeof(OrderItemStatusReasons))
                    .Cast<OrderItemStatusReasons>()
                    .Select(s => new SelectKeyValue<string>
                    {
                        Text = s.ToDescription(),
                        Value = s.ToString()
                    }));

            var allStatus = orderStatusList.Single(o => o.Text == "All");
            orderStatusList.RemoveAll(o => o.Text == "All");
            orderStatusList.Insert(0, allStatus);

            return JsonNet(new { Tuitions = tuitionsList, OrderStatus = orderStatusList });
        }

        [HttpPost]
        public virtual JsonNetResult CapacityReportRun(SelectedPrograms model)
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var club = ActiveClub;

            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();
            }

            var allSchedules = new List<ProgramSchedule>();

            foreach (var programId in model._SelectedPrograms)
            {
                allSchedules.AddRange(Ioc.ProgramBusiness.Get(programId).ProgramSchedules.Where(p => !p.IsDeleted));
            }

            var data = FillCapacityViewModel(allSchedules);

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = data.Count });
        }

        public virtual ActionResult InstallmentReportRun(SelectedPrograms model, string term, string automaticType, bool isExportToExcel = false, bool makeCampaign = false)
        {
            var club = ActiveClub;

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();
            }

            var allOrderItems = ApplyAllFilters(model, ReportType.Installment, term, automaticType);

            if (makeCampaign)
            {
                var recipients =
                    allOrderItems.Select(o => new ReportRecipient
                    {
                        Email = o.Order.User.UserName,
                        Lastname = o.LastName,
                        //FirstName = o.FirstName
                    })
                    .Distinct()
                    .ToList();

                var result = MakeCampaign(recipients, club.Domain, model.ReportName);

                return Json(new { result.Status, Data = result.RecordsAffected, Message = result.ExceptionMessage });
            }

            var allData = allOrderItems;

            var data = FillInstallmentViewModel(allData.OrderBy(o => o.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList());

            if (!isExportToExcel)
            {
                return JsonNet(new { DataSource = data, TotalCount = allData.Count() });
            }
            else
            {
                var workbook = new HSSFWorkbook();

                var sheet = workbook.CreateSheet();

                //(Optional) set the width of the columns
                sheet.AutoSizeColumn(0);
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);
                sheet.AutoSizeColumn(6);
                sheet.AutoSizeColumn(7);
                sheet.AutoSizeColumn(8);
                sheet.AutoSizeColumn(9);
                sheet.AutoSizeColumn(10);

                //Create a header row
                var headerRow = sheet.CreateRow(0);

                //Set the column names in the header row
                headerRow.CreateCell(0).SetCellValue("Program name");
                headerRow.CreateCell(1).SetCellValue("Full name");
                headerRow.CreateCell(2).SetCellValue("Email");
                headerRow.CreateCell(3).SetCellValue("Registration date");
                headerRow.CreateCell(4).SetCellValue("Confirmation");
                headerRow.CreateCell(5).SetCellValue("Installments");
                headerRow.CreateCell(6).SetCellValue("Paid installments");
                headerRow.CreateCell(7).SetCellValue("Installment details");
                headerRow.CreateCell(8).SetCellValue("Total amount");
                headerRow.CreateCell(9).SetCellValue("Paid amount");
                headerRow.CreateCell(10).SetCellValue("Balance");
                headerRow.CreateCell(11).SetCellValue("Preapproval");
                headerRow.CreateCell(12).SetCellValue("Past Due");

                //(Optional) freeze the header row so it is not scrolled
                sheet.CreateFreezePane(0, 1, 0, 1);

                int rowNumber = 1;

                //Populate the sheet with values from the grid data
                foreach (var item in data)
                {
                    //Create a new row
                    var row = sheet.CreateRow(rowNumber++);

                    //Set values for the cells
                    row.CreateCell(0).SetCellValue(item.ProgramName);
                    row.CreateCell(1).SetCellValue(item.FullName);
                    row.CreateCell(2).SetCellValue(item.Email);
                    row.CreateCell(3).SetCellValue(item.RegDate);
                    row.CreateCell(4).SetCellValue(item.ConfirmationId);
                    row.CreateCell(5).SetCellValue(item.InstallmentsNum);
                    row.CreateCell(6).SetCellValue(item.PaidInstallments);

                    var installmentDetails = string.Empty;

                    foreach (var installment in item.Installments)
                    {
                        var amount = installment.Amount;
                        var dueDate = installment.DueDate;
                        var paymentStatus = installment.PaymentStatus;
                        installmentDetails += string.Format("{0} {1} {2}, ", dueDate, amount, paymentStatus);
                    }
                    //var cs = workbook.CreateCellStyle();
                    //cs.WrapText = true;
                    //cs.Alignment = HorizontalAlignment.LEFT;
                    installmentDetails = (installmentDetails.Length > 0)
                        ? installmentDetails.Remove(installmentDetails.Length - 2)
                        : installmentDetails;
                    row.CreateCell(7).SetCellValue(installmentDetails);
                    //row.Cells[5].CellStyle = cs;

                    row.CreateCell(8).SetCellValue((double)item.Total);
                    row.CreateCell(9).SetCellValue((double)item.PaidAmount);
                    row.CreateCell(10).SetCellValue((double)item.Balance);
                    row.CreateCell(11).SetCellValue(item.Preapproval);
                    row.CreateCell(12).SetCellValue(item.PastDue);
                }

                //Write the workbook to a memory stream
                var output = new MemoryStream();
                workbook.Write(output);

                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                string contentType = string.Format("application/vnd.ms-excel");
                var reportName = string.Format(Constants.ReportName, "installment", model.SeasonDomain);

                // this is a trick using stream you have to return position to 0.
                output.Position = 0;
                sm.UploadBlob(club.Domain, reportName, output, contentType);

                string url = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, reportName).AbsoluteUri;

                output.Flush();
                output.Close();

                return JsonNet(new JResult { Data = url, Status = true });
            }
        }


        public virtual ActionResult ProviderFinancialRun(SelectedPrograms model)
        {
            var commisionRate = Ioc.ClubBusiness.Get(ActiveClub.Id).PartnerCommisionRate;
            var programBusiness = Ioc.ProgramBusiness;

            var programs = new List<Program>();
            var coupons = new List<Coupon>();

            var season = Ioc.SeasonBusiness.Get(model.SeasonDomain, ActiveClub.Id);
            long seasonId = season.Id;

            List<int> providerFundedId = new List<int>();
            var providerFinancial = new List<ProviderFinancialReportViewModel>();

            if (model.IsAllPrograms)
            {
                programs = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).ToList();
            }
            else
            {
                programs = Ioc.ProgramBusiness.GetList(model._SelectedPrograms).ToList();
            }

            var providerCoupons = Ioc.CouponBusiness.GetList(seasonId).Where(c => !c.Deleted && c.IsAllProgram).ToList();
            var schoolCoupons = programs.SelectMany(p => p.Season.Coupons).ToList();

            coupons.AddRange(providerCoupons);
            coupons.AddRange(schoolCoupons);

            foreach (var program in programs)
            {
                var providerFundedCouponIds = programBusiness.GetProviderFundedCoupons(program, coupons).Select(c => c.Id).ToList();
                var allOrderitem = program.ProgramSchedules.SelectMany(o => o.OrderItems).Where(o => o.ItemStatus == OrderItemStatusCategories.completed);

                providerFinancial.Add(new ProviderFinancialReportViewModel
                {
                    ClassName = program.Name,
                    SchoolName = program.Season.Club.Name,
                    TotalRegistrations = programBusiness.GetRegisteredCount(program, program.Season.Status == SeasonStatus.Test),
                    TotalPaidEnrollments = programBusiness.TotalPaidRegistration(program, allOrderitem.ToList(), program.Season.Status == SeasonStatus.Test),
                    ClassFee = programBusiness.GetEmClassFee(program),
                    EMRate = (program.CustomFields.CommisionRate.HasValue && program.CustomFields.CommisionRate != 0) ? program.CustomFields.CommisionRate.Value : commisionRate,
                    TotalProviderFundedScholarships = program.ProgramSchedules.SelectMany(o => o.OrderItems).Where(c => c.Order.IsLive == (program.Season.Status == SeasonStatus.Live) && c.ItemStatus == OrderItemStatusCategories.completed && c.OrderChargeDiscounts.Any(ch => !ch.IsDeleted)).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && providerFundedCouponIds.Contains(c.CouponId.Value)).Count(),
                    ProviderPenaltiesDeducted = programBusiness.GetProviderNoShowFines(program) + programBusiness.GetProviderLatePenaltyFines(program)
                });
            }

            return JsonNet(new { DataSource = providerFinancial, Status = true });
        }

        public virtual ActionResult CoordinatorFinancialRun(SelectedPrograms model)
        {
            var club = ActiveClub;
            var programBusiness = Ioc.ProgramBusiness;
            var clubbase = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            var programs = new List<Program>();
            var coupons = new List<Coupon>();
            var season = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id);
            var isTestMode = season.Status == SeasonStatus.Test;
            var orderitem = Ioc.OrderItemBusiness.GetList().Where(o => o.Order.IsLive == !isTestMode);

            long seasonId = 0;
            List<int> providerFundedId = new List<int>();
            var coordinatorFinancial = new List<CoordinatorFinancialReportViewModel>();

            if (model.IsAllPrograms)
            {
                seasonId = season.Id;
                programs = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).ToList();
            }
            else
            {
                seasonId = season.Id;
                programs = Ioc.ProgramBusiness.GetList(model._SelectedPrograms).ToList();
            }
            decimal totalActivity = 0;
            decimal totalEM = 0;
            var totalDonationsReceived = orderitem.Where(c => c.ProgramScheduleId == null && c.Order.ClubId == ActiveClub.Id && c.SeasonId == seasonId && c.ItemStatus == OrderItemStatusCategories.completed) != null ? orderitem.Where(c => c.ProgramScheduleId == null && c.Order.ClubId == ActiveClub.Id && c.SeasonId == seasonId && c.ItemStatus == OrderItemStatusCategories.completed).Sum(o => (decimal?)o.TotalAmount) ?? 0 : 0;
            coupons = Ioc.CouponBusiness.GetList(seasonId).Where(c => !c.Deleted && c.IsAllProgram).ToList();

            foreach (var program in programs)
            {

                var providerFundedCouponIds = programBusiness.GetProviderFundedCoupons(program, coupons).Select(c => c.Id).ToList();
                var PTAPTOFundedCouponIds = programBusiness.GetPTAPTOFundedCoupons(program, coupons).Select(c => c.Id).ToList();
                var CashPaymenttoPTACouponIds = programBusiness.GetCashPaymentCoupons(program, coupons).Select(c => c.Id).ToList();
                var totalProviderFundedScholarships = program.ProgramSchedules.SelectMany(o => o.OrderItems.Where(oi => oi.Order.IsLive != isTestMode)).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.OrderChargeDiscounts.Any(ch => !ch.IsDeleted)).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && providerFundedCouponIds.Contains(c.CouponId.Value)).Count();
                var totalPTAFundedScholarships = program.ProgramSchedules.SelectMany(o => o.OrderItems.Where(oi => oi.Order.IsLive != isTestMode)).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.OrderChargeDiscounts.Any(ch => !ch.IsDeleted)).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && PTAPTOFundedCouponIds.Contains(c.CouponId.Value)).Count();
                var totalCashPaymenttoPTA = program.ProgramSchedules.SelectMany(o => o.OrderItems.Where(oi => oi.Order.IsLive != isTestMode)).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.OrderChargeDiscounts.Any(ch => !ch.IsDeleted)).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && CashPaymenttoPTACouponIds.Contains(c.CouponId.Value)).Count();
                var amountCustomChargeTransfer = program.ProgramSchedules.SelectMany(o => o.OrderItems.Where(oi => oi.Order.IsLive != isTestMode)).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.OrderChargeDiscounts.Any(ch => !ch.IsDeleted) && c.ItemStatusReason == OrderItemStatusReasons.transferIn).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CustomCharge).Sum(c => (decimal?)c.Amount) ?? 0;
                var amountOrderItemTransfer = amountCustomChargeTransfer == 0 ? 0 : program.ProgramSchedules.SelectMany(o => o.OrderItems.Where(oi => oi.Order.IsLive == isTestMode)).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.ItemStatusReason == OrderItemStatusReasons.transferIn).Sum(c => (decimal?)c.TotalAmount) ?? 0;
                var totalOrderItem = amountOrderItemTransfer - amountCustomChargeTransfer;
                decimal transferActivityfee = 0;

                if (program.ProgramSchedules.SelectMany(o => o.OrderItems.Where(oi => oi.Order.IsLive == isTestMode)).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.OrderChargeDiscounts.Any(ch => !ch.IsDeleted) && c.ItemStatusReason == OrderItemStatusReasons.transferIn).Any())
                {
                    transferActivityfee = clubbase.Charges.Sum(c => c.AmountType == ChargeDiscountType.Fixed ? amountCustomChargeTransfer == 0 ? 0 : c.Amount : totalOrderItem * c.Amount / 100);
                }
                var Activityfee = program.ProgramSchedules.SelectMany(o => o.OrderItems.Where(oi => oi.Order.IsLive == isTestMode)).Where(c => c.ItemStatus == OrderItemStatusCategories.completed).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge).Sum(c => (decimal?)c.Amount) ?? 0;

                totalActivity += Activityfee + transferActivityfee;

                totalEM = (totalDonationsReceived + totalActivity) * 5 / 100;

                coordinatorFinancial.Add(new CoordinatorFinancialReportViewModel
                {
                    ClassName = program.Name,
                    ProviderName = program.OutSourceSeasonId.HasValue ? program.OutSourceSeason.Club.Name : program.Club.Name,
                    TotalRegistrations = programBusiness.GetRegisteredCount(program, isTestMode),
                    ClassFee = programBusiness.GetEmClassFee(program),
                    TotalProviderFundedScholarships = totalProviderFundedScholarships,
                    TotalPTAFundedScholarships = totalPTAFundedScholarships,
                    TotalCashPaymenttoPTA = totalCashPaymenttoPTA,
                    TotalPTAActivityFeesAssessed = Activityfee + transferActivityfee,
                });
            }

            return JsonNet(new { DataSource = coordinatorFinancial, TotalDonationsReceived = CurrencyHelper.FormatCurrencyWithPenny(totalDonationsReceived), TotalPTAActivityFees = CurrencyHelper.FormatCurrencyWithPenny(totalActivity), Totalem = CurrencyHelper.FormatCurrencyWithPenny(totalEM) });
        }

        public virtual ActionResult FollowupReportRun(SelectedPrograms model, string term, bool isExportToExcel = false, bool makeCampaign = false)
        {

            var club = ActiveClub;
            var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;


            if (model.IsAllPrograms)
            {
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();
            }

            var allOrderItems = ApplyAllFilters(model, ReportType.FollowupForm, term);

            if (makeCampaign)
            {
                var recipients =
                    allOrderItems.Select(o => new ReportRecipient
                    {
                        Email = o.Order.User.UserName,
                        Lastname = o.LastName,
                        //FirstName = o.FirstName
                    })
                    .Distinct()
                    .ToList();

                var result = MakeCampaign(recipients, club.Domain, model.ReportName);

                return Json(new { result.Status, Data = result.RecordsAffected, Message = result.ExceptionMessage });
            }

            var allData = allOrderItems;

            var data =
                FillFollowupViewModel(allData.OrderBy(o => o.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList());

            if (!isExportToExcel)
            {
                var dataSource = data;
                return JsonNet(new { DataSource = dataSource, TotalCount = allData.Count() });
            }
            else
            {
                var workbook = new HSSFWorkbook();

                var sheet = workbook.CreateSheet();

                //(Optional) set the width of the columns
                sheet.AutoSizeColumn(0);
                sheet.AutoSizeColumn(1);
                sheet.AutoSizeColumn(2);
                sheet.AutoSizeColumn(3);
                sheet.AutoSizeColumn(4);
                sheet.AutoSizeColumn(5);

                //Create a header row
                var headerRow = sheet.CreateRow(0);

                //Set the column names in the header row
                headerRow.CreateCell(0).SetCellValue("Follow-up forms");
                headerRow.CreateCell(1).SetCellValue("Program name");
                headerRow.CreateCell(2).SetCellValue("Full name");
                headerRow.CreateCell(3).SetCellValue("Email");
                headerRow.CreateCell(4).SetCellValue("Regsitration date");
                headerRow.CreateCell(5).SetCellValue("Confirmation");

                //(Optional) freeze the header row so it is not scrolled
                sheet.CreateFreezePane(0, 1, 0, 1);

                int rowNumber = 1;

                //Populate the sheet with values from the grid data
                foreach (var item in data)
                {
                    //Create a new row
                    var row = sheet.CreateRow(rowNumber++);

                    var formDetails = string.Empty;

                    foreach (var form in item.FollowupForms)
                    {
                        var name = form.FormName;
                        var status = form.Status;
                        formDetails += string.Format("{0} {1}, ", name, status);
                    }

                    formDetails = (formDetails.Length > 0)
                        ? formDetails.Remove(formDetails.Length - 2)
                        : formDetails;

                    row.CreateCell(0).SetCellValue(formDetails);

                    //Set values for the cells
                    row.CreateCell(1).SetCellValue(item.ProgramName);
                    row.CreateCell(2).SetCellValue(item.FullName);
                    row.CreateCell(3).SetCellValue(item.Email);
                    row.CreateCell(4).SetCellValue(item.RegDate);
                    row.CreateCell(5).SetCellValue(item.ConfirmationId);
                }

                //Write the workbook to a memory stream
                var output = new MemoryStream();
                workbook.Write(output);

                var reportName = string.Format(Constants.ReportName, "follow-upForm", model.SeasonDomain);

                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                string contentType = string.Format("application/vnd.ms-excel");

                // this is a trick using stream you have to return position to 0.
                output.Position = 0;
                sm.UploadBlob(club.Domain, reportName, output, contentType);

                string url = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, reportName).AbsoluteUri;

                output.Flush();
                output.Close();

                return JsonNet(new JResult { Data = url, Status = true });
            }
        }

        public virtual ActionResult BalanceReportRun(SelectedPrograms model, string term, bool isExportToExcel = false, bool makeCampaign = false)
        {
            var club = ActiveClub;

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();
            }

            var allOrderItems = ApplyAllFilters(model, ReportType.Balance, term);

            if (makeCampaign)
            {
                var recipients =
                    allOrderItems.Select(o => new ReportRecipient
                    {
                        Email = o.Order.User.UserName,
                        Lastname = o.LastName,
                        //FirstName = o.FirstName
                    }).Distinct().ToList();

                var result = MakeCampaign(recipients, club.Domain, model.ReportName);

                return Json(new { result.Status, Data = result.RecordsAffected, Message = result.ExceptionMessage });
            }

            var allData = allOrderItems;

            var data =
                FillBalanceViewModel(allData.OrderBy(o => o.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList(),
                    (model.Filters != null) ? model.Filters.BalanceFilter : BalanceReportFilters.All);

            var dataSource = data;
            return JsonNet(new { DataSource = dataSource, TotalCount = allData.Count() });
        }

        public virtual ActionResult ProgramChargesReportRun(SelectedPrograms model, string term,
            bool makeCampaign = false)
        {
            var club = ActiveClub;

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            var allOrderItems = ApplyAllFilters(model, ReportType.ChargeDiscount, term);

            if (makeCampaign)
            {
                var recipients =
                    allOrderItems.Select(o => new ReportRecipient
                    {
                        Email = o.Order.User.UserName,
                        Lastname = o.LastName,
                        //FirstName = o.FirstName
                    }).Distinct().ToList();

                var result = MakeCampaign(recipients, club.Domain, model.ReportName);

                return Json(new { result.Status, Data = result.RecordsAffected, Message = result.ExceptionMessage });
            }

            var allData = allOrderItems;
            var finalChargeDiscounts = GetAllProgramChargeDiscounts(allData);
            var finalModel = (allData.Any())
                ? FillChargeDiscountModel(allData.OrderBy(o => o.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList(),
                    finalChargeDiscounts)
                : new List<Dictionary<string, string>>();

            var dataSource = finalModel;
            return
                JsonNet(
                    new { DataSource = dataSource, TotalCount = allData.Count() });
        }

        public virtual ActionResult FillFinanceFilters()
        {

            var allTransctionsCategories =
                Enum.GetValues(typeof(TransactionCategory))
                    .Cast<TransactionCategory>()
                    .Select(s => new SelectKeyValue<string>
                    {
                        Text = s.ToDescription(),
                        Value = s.ToString()
                    });

            var allPaymentMethods = new List<SelectKeyValue<string>>
            {
                new SelectKeyValue<string>
                {
                    Text = "All",
                    Value = ""
                }
            };

            allPaymentMethods.AddRange(Enum.GetValues(typeof(PaymentMethod))
                .Cast<PaymentMethod>()
                .Select(s => new SelectKeyValue<string>
                {
                    Text = s.ToDescription(),
                    Value = s.ToString()
                }));
            return JsonNet(new { TransactionsCategories = allTransctionsCategories, PaymentMethods = allPaymentMethods });
        }

        public virtual ActionResult FinanceReportRun(string seasonDomain, string term, FinanceReportFilter filters, bool makeCampaign = false, bool isAllTransactions = false)
        {
            var club = ActiveClub;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

            var pageSize = (Request.Params["PageSize"] != null) ? int.Parse(Request.Params["PageSize"]) : 100000;
            var page = (Request.Params["Page"] != null) ? int.Parse(Request.Params["Page"]) : 1;

            var transactions =
                Ioc.TransactionActivityBusiness.GetAllTransactionBySeasonId(season.Id);

            if (!string.IsNullOrEmpty(term))
            {
                transactions =
                    transactions.Where(
                        p => p.OrderItem != null && p.OrderItem.Order.User.UserName.ToLower().Equals(term.ToLower()));
            }

            if (filters != null && filters.RegistrationStart.HasValue)
            {
                if (filters.RegistrationEnd.HasValue)
                {
                    var startDate = filters.RegistrationStart.Value;
                    var endDate = filters.RegistrationEnd.Value;

                    transactions =
                        transactions.Where(
                            t => DbFunctions.TruncateTime(t.TransactionDate) >=
                                startDate &&
                                DbFunctions.TruncateTime(t.TransactionDate) <= endDate)
                            .Select(t => t);
                }
                else
                {
                    transactions =
                        transactions.Where(
                            t =>
                                DbFunctions.TruncateTime(t.TransactionDate) >= filters.RegistrationStart.Value);
                }
            }
            else if (filters != null && filters.RegistrationEnd.HasValue)
            {
                transactions =
                    transactions.Where(
                        t =>
                            DbFunctions.TruncateTime(t.TransactionDate) <= filters.RegistrationEnd.Value);
            }

            if (filters != null && filters.TransactionFilters != null)
            {
                if (filters.TransactionFilters.TransactionCategory != null)
                {
                    transactions =
                        transactions.Where(
                            t => filters.TransactionFilters.TransactionCategory.Contains(t.TransactionCategory))
                            .Select(t => t);
                }
                if (filters.TransactionFilters.PaymentMethod.HasValue)
                {
                    transactions =
                        transactions.Where(
                            t => t.PaymentDetail.PaymentMethod == filters.TransactionFilters.PaymentMethod.Value)
                            .Select(t => t);
                }
            }
            var groupbyTransactions = new List<IGrouping<long, TransactionActivity>>();
            var modelTransactions = new List<IGrouping<long, TransactionActivity>>();

            if (isAllTransactions)
            {
                var sortTransactions = transactions.OrderBy(t => t.TransactionDate);
                //groupbyTransactions = sortTransactions.GroupBy(t => t.Id).ToList();
                groupbyTransactions = sortTransactions.GroupBy(t => t.Id).ToList().OrderBy(i => i.First().TransactionDate).ToList();
                modelTransactions = groupbyTransactions.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                groupbyTransactions = transactions.GroupBy(t => t.PaymentDetailId).ToList();
                modelTransactions = groupbyTransactions.OrderBy(g => g.Key).Skip((page - 1) * pageSize).Take(pageSize).ToList();
            }

            if (makeCampaign)
            {
                var recipients =
                    groupbyTransactions.SelectMany(p => p.ToList().Select(c => new ReportRecipient
                    {
                        Email = c.Order.User.UserName,
                        Lastname = c.OrderItem.LastName,
                        //FirstName = c.OrderItem.FirstName
                    })).Distinct().ToList();

                var result = MakeCampaign(recipients, club.Domain, "Finance");

                return Json(new { result.Status, Data = result.RecordsAffected, Message = result.ExceptionMessage });
            }

            var model = FillFinanceModel(modelTransactions);
            var dataSource = model;
            return JsonNet(new { DataSource = dataSource, TotalCount = groupbyTransactions.Count() });

        }

        [HttpPost]
        public virtual ActionResult RegistrationFormReportRun(SelectedPrograms model)
        {
            var club = ActiveClub;

            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms =
                    Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();
            }

            var allOrders = Ioc.OrderItemBusiness.GetReportOrderItems(model._SelectedPrograms).OrderBy(o => o.Player.Contact.FirstName).ThenBy(o => o.Player.Contact.LastName).AsNoTracking();

            var data = FillRegistrationForms(allOrders, model.IncludeForms);

            var viewRendered = this.RenderViewToString("_RegistrationFormPdfExportView", data);

            // Convert to pdf

            using (var pdfStream = new MemoryStream())
            {
                var pdfDoc = new Document(PageSize.A4, 20, 20, 20, 20);
                var pdfWriter = PdfWriter.GetInstance(pdfDoc, pdfStream);

                pdfDoc.Open();

                using (var strHtml = new StringReader(viewRendered))
                {
                    XMLWorkerHelper.GetInstance().ParseXHtml(pdfWriter, pdfDoc, strHtml);
                }

                pdfDoc.Close();

                using (var stream = new MemoryStream(pdfStream.GetBuffer()))
                {
                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection,
                        Constants.Path_ClubFilesContainer);
                    sm.UploadBlob(club.Domain, string.Format("Reg ({0}).pdf", model.SeasonDomain), stream, "application/pdf");
                }
            }

            return JsonNet(new JResult { Data = string.Format("Reg ({0}).pdf", model.SeasonDomain), Status = true });
        }

        public virtual ActionResult FillCouponFilter(string seasonDomain)
        {
            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);
            var seasonCoupons =
                Ioc.CouponBusiness.GetList(season.Id).ToList().Select(s => new SelectKeyValue<string>
                {
                    Text = string.Format("{0} ({1})", s.Name, s.Code),
                    Value = s.Id.ToString()
                });
            return JsonNet(seasonCoupons);
        }

        public virtual ActionResult CouponsReportRun(string seasonDomain, string term, ReportFilters filters, bool makeCampaign = false)
        {
            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);
            var club = ActiveClub;

            var orderItems = Ioc.OrderItemBusiness.GetOrderItems(clubId: ActiveClub.Id, seasonId: season.Id, isTestMode: season.Status == SeasonStatus.Test)
                    .Where(o => o.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.CouponId.HasValue));

            if (!string.IsNullOrEmpty(term))
            {
                orderItems = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(term).Success
                    ? orderItems.Where(p => p.Order.User.UserName.ToLower().Equals(term.ToLower()))
                    : orderItems.Where(
                        p => p.FirstName.ToLower().StartsWith(term.ToLower()) || p.LastName.ToLower().StartsWith(term.ToLower()));
            }

            if (filters != null && filters.RegistrationStart.HasValue)
            {
                if (filters.RegistrationEnd.HasValue)
                {
                    var startDate = filters.RegistrationStart.Value;
                    var endDate = filters.RegistrationEnd.Value;

                    orderItems =
                        orderItems.Where(
                            t =>
                                DbFunctions.TruncateTime(t.Order.CompleteDate) >= startDate &&
                                DbFunctions.TruncateTime(t.Order.CompleteDate) <= endDate).Select(t => t);
                }
                else
                {
                    orderItems =
                        orderItems.Where(
                            t =>
                                DbFunctions.TruncateTime(t.Order.CompleteDate) >= filters.RegistrationStart.Value);
                }
            }
            else if (filters != null && filters.RegistrationEnd.HasValue)
            {
                orderItems =
                        orderItems.Where(
                            t =>
                                DbFunctions.TruncateTime(t.Order.CompleteDate) <= filters.RegistrationEnd.Value);
            }

            if (filters != null && filters.CouponFilter > 0)
            {
                orderItems = orderItems.Where(c => c.OrderChargeDiscounts.Any(o => !o.IsDeleted && o.CouponId == filters.CouponFilter));
            }

            if (!makeCampaign)
            {
                var pageSize = int.Parse(Request.Params["PageSize"]);
                var page = int.Parse(Request.Params["Page"]);

                var orderItemsChargeDiscounts =
                    orderItems.SelectMany(
                        o => o.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon));

                if (filters != null && filters.CouponFilter > 0)
                {
                    orderItemsChargeDiscounts = orderItemsChargeDiscounts.Where(c => c.CouponId == filters.CouponFilter);
                }

                var model = FillCouponeViewModel(orderItemsChargeDiscounts.OrderBy(o => o.Id)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .ToList());
                return JsonNet(new { DataSource = model, TotalCount = orderItemsChargeDiscounts.Count() });
            }
            else
            {
                var orderItemsWithCoupons = orderItems;

                var recipients =
                    orderItemsWithCoupons.Select(o => new ReportRecipient
                    {
                        Email = o.Order.User.UserName,
                        Lastname = o.LastName,
                        //FirstName = o.FirstName
                    }).Distinct().ToList();

                var result = MakeCampaign(recipients, club.Domain, "Coupon");

                return Json(new { result.Status, Data = result.RecordsAffected, Message = result.ExceptionMessage });
            }
        }
        public virtual FileResult ExportDefaultReport(ExportMode mode, string seasonDomain, bool isAllPrograms, ReportType reportType, List<long> programs)
        {
            var club = ActiveClub;

            var exp = new ExportFileHelper();

            var expsch = new ExportDocumnetSchema();

            if (isAllPrograms)
            {
                programs = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Programs.Select(p => p.Id).ToList();
            }

            switch (reportType)
            {
                case ReportType.Capacity:
                    {

                        var allSchedules = new List<ProgramSchedule>();

                        foreach (var programId in programs)
                        {
                            allSchedules.AddRange(Ioc.ProgramBusiness.Get(programId)
                                .ProgramSchedules.Where(p => !p.IsDeleted));
                        }

                        var finalModel =
                            FillCapacityViewModel(allSchedules)
                                .Select(
                                    m =>
                                        new
                                        {
                                            m.ProgramName,
                                            m.Status,
                                            m.Open,
                                            m.Filled,
                                            m.Schedule
                                        });

                        expsch = Ioc.CustomReportBusiness.ExportCapacityReport(finalModel.ToList<object>());

                        break;
                    }
            }

            var output = new MemoryStream();
            string memType = string.Empty;
            string fileName = "{0} CapacityReport.{1}";

            if (mode == ExportMode.Pdf)
            {
                output = exp.ExportPdf(expsch);
                memType = Constants.E_Pdf_MimeType;
                fileName = string.Format(seasonDomain, club.Name, Constants.E_PdfFileExt);
            }
            else if (mode == ExportMode.Excel)
            {
                output = exp.ExportExcel(expsch);
                memType = Constants.E_Excel_MimeType;
                fileName = string.Format(fileName, club.Name, Constants.E_ExcelFileExt);
            }

            return File(output.ToArray(), memType, fileName);
        }

        public virtual JsonNetResult FillShareModel(string reportName)
        {
            var model = new ShareAsAttachment();

            if (reportName.Equals("TransactionSubscriptionFinance"))
            {
                var email = _clubBusiness.Get(ActiveClub.Id).ContactPersons.First().Email;
                model = new ShareAsAttachment() { Message = "The transactions subscription report export you requested is ready to go. Just click the link below to start the download.  This file will be available for one week.", Subject = "Transactions Subscription Report", SentTo = email };
            }
            else
                model = new ShareAsAttachment { Message = "This is a report from jumbula.com", Subject = "Report From Jumbula.com" };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult ShareReport(ReportType type, ShareAsAttachment model, SelectedPrograms selectedModel, string term, string automaticType)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Domain);

            this.EnsureUserIsAdminForClub(club.Domain);

            var expsch = new ExportDocumnetSchema();

            var exp = new ExportFileHelper();

            const string fileNameFormat = "{0}({1}_{2}){3}.xls";

            var fileName = string.Empty;

            var titles = new List<string>();

            if (ModelState.IsValid)
            {
                ModelState.Clear();

                if (selectedModel.IsAllPrograms)
                {
                    var seasonId = Ioc.SeasonBusiness.Get(selectedModel.SeasonDomain, club.Id).Id;
                    selectedModel._SelectedPrograms =
                        Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();
                }
                switch (type)
                {
                    case ReportType.Capacity:
                        {
                            fileName = string.Format(fileNameFormat, club.Name, "capacity", selectedModel.SeasonDomain, RandomHelper.GenerateGuid());

                            var allSchedules = new List<ProgramSchedule>();
                            foreach (var programId in selectedModel._SelectedPrograms)
                            {
                                allSchedules.AddRange(Ioc.ProgramBusiness.Get(programId)
                                    .ProgramSchedules.Where(p => !p.IsDeleted));
                            }

                            var finalModel =
                                FillCapacityViewModel(allSchedules)
                                    .Select(
                                        m =>
                                            new
                                            {
                                                m.ProgramName,
                                                m.Status,
                                                m.Open,
                                                m.Filled,
                                                m.Schedule
                                            }).ToList<object>();

                            expsch = Ioc.CustomReportBusiness.ExportCapacityReport(finalModel);
                            break;
                        }
                    case ReportType.Installment:
                        {
                            var allOrderItems = ApplyAllFilters(selectedModel, ReportType.Installment, term, automaticType);
                            var installmentModel =
                                FillInstallmentViewModel(allOrderItems.ToList())
                                    .Select(item => new
                                    {
                                        ParticipantFullName = item.FullName,
                                        item.InstallmentsNum,
                                        item.DoublePaidAmount,
                                        item.DoubleBalance,
                                        item.Email,
                                        item.PaidInstallments,
                                        item.PastDue,
                                        Installments = item.ToInstallmentDetail(),
                                        item.ProgramName,
                                        item.Preapproval,
                                        item.RegDate,
                                        item.ConfirmationId,
                                        item.DoubleTotal
                                    }).ToList<object>();

                            expsch = Ioc.CustomReportBusiness.ExportInstallmentReport(installmentModel);
                            fileName = string.Format(fileNameFormat, club.Name, "installment", selectedModel.SeasonDomain, RandomHelper.GenerateGuid());
                            break;
                        }
                    case ReportType.FollowupForm:
                        {
                            var allOrderItems = ApplyAllFilters(selectedModel, ReportType.FollowupForm, term);

                            var followupModel =
                                FillFollowupViewModel(allOrderItems).Select(p => new
                                {
                                    p.Email,
                                    FollowupForms = p.ToFollowupFromDetails(),
                                    p.FullName,
                                    p.ProgramName,
                                    RegistrationDate = p.RegDate,
                                    p.ConfirmationId
                                }).ToList<object>();

                            expsch = Ioc.CustomReportBusiness.ExportFollowupReport(followupModel);
                            fileName = string.Format(fileNameFormat, club.Name, "follow-up", selectedModel.SeasonDomain, RandomHelper.GenerateGuid());
                            break;
                        }
                    case ReportType.Balance:
                        {
                            var allOrders = ApplyAllFilters(selectedModel, ReportType.Balance, term);

                            var balanceModel =
                                FillBalanceViewModel(allOrders, selectedModel.Filters.BalanceFilter)
                                    .Select(p => new
                                    {
                                        p.Balance,
                                        p.ConfirmationId,
                                        p.PaymentDetail,
                                        p.ProgramName,
                                        p.FullName,
                                        p.Email,
                                        p.RegDate
                                    }).ToList<object>();

                            expsch = Ioc.CustomReportBusiness.ExportBalanceReport(balanceModel);
                            fileName = string.Format(fileNameFormat, club.Name, "balance", selectedModel.SeasonDomain, RandomHelper.GenerateGuid());
                            break;
                        }
                    case ReportType.ChargeDiscount:
                        {
                            //var seasonId = Ioc.SeasonBusiness.Get(selectedModel.SeasonDomain, club.Domain).Id;

                            var allOrderItems = ApplyAllFilters(selectedModel, ReportType.ChargeDiscount, term);

                            var allOrderChargeDiscounts = GetAllProgramChargeDiscounts(allOrderItems);

                            var chargeDiscountsModel = FillChargeDiscountModel(allOrderItems, allOrderChargeDiscounts);

                            expsch = Ioc.CustomReportBusiness.ExportChargeDiscountReport(chargeDiscountsModel);
                            fileName = string.Format(fileNameFormat, club.Name, "chargeDiscount", selectedModel.SeasonDomain, RandomHelper.GenerateGuid());
                            break;
                        }
                    case ReportType.Custom:
                        {
                            var isFilterable = false;
                            if (selectedModel.Filters != null)
                            {
                                isFilterable = selectedModel.Filters.RegistrationStart.HasValue ||
                                               selectedModel.Filters.RegistrationEnd.HasValue ||
                                               selectedModel.Filters.Tuition.HasValue ||
                                               selectedModel.Filters.Status.HasValue;
                            }
                            var eventRoaster = Ioc.ReportBusiness.GetReport(selectedModel.ReportId);

                            fileName = string.Format(fileNameFormat, club.Name,
                                eventRoaster.Name.Replace(" ", "") +
                                DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, ActiveClub.TimeZone)
                                    .ToString("MMM dd, yyyy"), selectedModel.SeasonDomain,
                                RandomHelper.GenerateGuid());

                            var allOrderItems = Ioc.OrderItemBusiness.GetReportOrderItems(selectedModel._SelectedPrograms, OrderItemStatusCategories.showAll);

                            allOrderItems = isFilterable
                                ? ApplyBaseFilters(selectedModel.Filters, allOrderItems)
                                : allOrderItems;

                            var orderItems = allOrderItems.OrderBy(o => o.Id).ToList();

                            var fillModel = GetProgramReportModel(eventRoaster, orderItems);

                            var baseJbForm = Ioc.CustomReportBusiness.RemoveUncheckedElementsJbForm(eventRoaster.JbForm);

                            if (eventRoaster.FollowupForms != null)
                            {
                                foreach (var followup in eventRoaster.FollowupForms)
                                {
                                    baseJbForm.Elements.AddRange(followup.Elements);
                                }
                            }

                            var data = FillCustomReport(fillModel, ref titles, baseJbForm, eventRoaster.EventType);

                            expsch = Ioc.CustomReportBusiness.CustomReportToExcel(data, titles);
                            break;
                        }
                    case ReportType.CustomParent:
                        {
                            var isFilterable = false;
                            if (selectedModel.Filters != null)
                            {
                                isFilterable = selectedModel.Filters.RegistrationStart.HasValue ||
                                               selectedModel.Filters.RegistrationEnd.HasValue ||
                                               selectedModel.Filters.Tuition.HasValue ||
                                               selectedModel.Filters.Status.HasValue;
                            }
                            var eventRoaster = Ioc.ReportBusiness.GetReport(selectedModel.ReportId);

                            fileName = string.Format(fileNameFormat, club.Name,
                                eventRoaster.Name.Replace(" ", "") +
                                DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, ActiveClub.TimeZone)
                                    .ToString("MMM dd, yyyy"), selectedModel.SeasonDomain,
                                RandomHelper.GenerateGuid());

                            var allOrderItems = Ioc.OrderItemBusiness.GetReportOrderItems(selectedModel._SelectedPrograms, OrderItemStatusCategories.showAll);

                            allOrderItems = isFilterable
                                ? ApplyBaseFilters(selectedModel.Filters, allOrderItems)
                                : allOrderItems;

                            var orderItems = allOrderItems.OrderBy(o => o.Id).ToList();

                            var data = Ioc.CustomReportBusiness.GetCustomReportParentInfo(eventRoaster, orderItems, ref titles, false, true);

                            expsch = Ioc.CustomReportBusiness.CustomReportToExcel(data);
                            break;
                        }
                }

                // Save in blob.
                MemoryStream output;

                switch (type)
                {

                    case ReportType.CustomParent:
                        {
                            output = exp.ExportExcelDicData(expsch);
                            break;
                        }
                    case ReportType.Custom:
                    case ReportType.ChargeDiscount:
                        {
                            output = exp.ExportExcelDicData(expsch);
                            break;
                        }
                    default:
                        {
                            output = exp.ExportExcel(expsch);
                            break;
                        }
                }

                using (var stream = new MemoryStream(output.GetBuffer()))
                {

                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection,
                        Constants.Path_ClubFilesContainer);
                    sm.UploadBlob(club.Domain, fileName, stream, "application/xls");

                }

                var reportLink = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri;

                var templateModel = new EmailTemplateModel
                {
                    HasLogo = true,
                    HasHeader = true,
                    ClubLogo = _clubBusiness.GetClubLogoURL(club.Domain, club.Logo),
                    Body = string.Format("{0} <br/><hr/> <a href=" + reportLink + "> Download Report </a>",
                        model.Message),
                    IsRegisterationEmail = true
                };

                var body = this.RenderViewToString(Constants.Path_EmailTemplate, templateModel);

                Ioc.OldEmailBusiness.SendEmail(club.ContactPersons.FirstOrDefault().Email, model.SentTo, model.Subject,
                    body, true);

                return JsonNet(new JResult { Status = true });
            }

            return JsonFormResponse();
        }

        public ActionResult CheckIfShareModelIsValid(ShareAsAttachment model)
        {
            return JsonNet(ModelState.IsValid);
        }

        public virtual ActionResult ShareProgramLessReports(ReportType type, ShareAsAttachment model, string seasonDomain, ReportFilters filters, string term)
        {
            if (ModelState.IsValid)
            {
                ModelState.Clear();

                var club = Ioc.ClubBusiness.Get(ActiveClub.Domain);

                var expsch = new ExportDocumnetSchema();

                var exp = new ExportFileHelper();

                string fileName = string.Empty;

                const string fileNameFormat = "{0}({1}_{2}){3}.xls";

                //var reportLink = string.Empty;

                switch (type)
                {
                    case ReportType.Finance:
                        {
                            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

                            var allOrderItems =
                                Ioc.TransactionActivityBusiness.GetAllTransactionBySeasonId(season.Id);

                            if (!string.IsNullOrEmpty(term))
                            {
                                allOrderItems =
                                    allOrderItems.Where(
                                        p => p.OrderItem != null && p.OrderItem.Order.User.UserName.ToLower().Equals(term.ToLower()));
                            }

                            if (filters != null && filters.RegistrationStart.HasValue)
                            {
                                if (filters.RegistrationEnd.HasValue)
                                {
                                    var startDate = filters.RegistrationStart.Value;
                                    var endDate = filters.RegistrationEnd.Value;

                                    allOrderItems =
                                        allOrderItems.Where(
                                            t => DbFunctions.TruncateTime(t.TransactionDate) >=
                                                 startDate &&
                                                 DbFunctions.TruncateTime(t.TransactionDate) <= endDate)
                                            .Select(t => t);
                                }
                                else
                                {
                                    allOrderItems =
                                        allOrderItems.Where(
                                            t =>
                                                DbFunctions.TruncateTime(t.TransactionDate) >=
                                                filters.RegistrationStart.Value);
                                }
                            }
                            else if (filters != null && filters.RegistrationEnd.HasValue)
                            {
                                allOrderItems =
                                        allOrderItems.Where(
                                            t =>
                                                DbFunctions.TruncateTime(t.Order.CompleteDate) <= filters.RegistrationEnd.Value);
                            }

                            if (filters != null && filters.TransactionFilters != null)
                            {
                                if (filters.TransactionFilters.TransactionCategory.HasValue)
                                {
                                    allOrderItems =
                                        allOrderItems.Where(
                                            t => t.TransactionCategory == filters.TransactionFilters.TransactionCategory.Value)
                                            .Select(t => t);
                                }
                                if (filters.TransactionFilters.PaymentMethod.HasValue)
                                {
                                    allOrderItems =
                                        allOrderItems.Where(
                                            t => t.PaymentDetail.PaymentMethod == filters.TransactionFilters.PaymentMethod.Value)
                                            .Select(t => t);
                                }
                            }

                            var financeModel =
                                FillFinanceModel(allOrderItems.GroupBy(t => t.PaymentDetailId).ToList()).Select(p => new
                                {
                                    p.FullName,
                                    p.UserEmail,
                                    p.PayerEmail,
                                    p.Date,
                                    p.Confirmation,
                                    p.TransactionType,
                                    p.TransactionCategory,
                                    p.Charge,
                                    p.Payment,
                                    p.Balance,
                                    p.PaymentMethod,
                                    p.Note,
                                    p.OrderId,
                                    p.CheckId,
                                    p.PaypalHandler
                                }).ToList<object>();
                            expsch = Ioc.CustomReportBusiness.ExportFinanceReport(financeModel);
                            fileName = string.Format(fileNameFormat, club.Name, "finance", seasonDomain, RandomHelper.GenerateGuid());
                            break;
                        }
                    case ReportType.Coupon:
                        {
                            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);
                            var orderItemsWithCoupons =
                                Ioc.OrderItemBusiness.GetOrderItems(clubId: ActiveClub.Id, seasonId: season.Id, isTestMode: season.Status == SeasonStatus.Test)
                                    .Where(o => o.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.CouponId.HasValue));

                            if (!string.IsNullOrEmpty(term))
                            {
                                orderItemsWithCoupons = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(term).Success
                                    ? orderItemsWithCoupons.Where(p => p.Order.User.UserName.ToLower().Equals(term.ToLower()))
                                    : orderItemsWithCoupons.Where(
                                        p => p.FirstName.ToLower().StartsWith(term.ToLower()) || p.LastName.ToLower().StartsWith(term.ToLower()));
                            }

                            if (filters != null && filters.RegistrationStart.HasValue)
                            {
                                if (filters.RegistrationEnd.HasValue)
                                {
                                    var startDate = filters.RegistrationStart.Value;
                                    var endDate = filters.RegistrationEnd.Value;

                                    orderItemsWithCoupons =
                                        orderItemsWithCoupons.Where(
                                            t =>
                                                DbFunctions.TruncateTime(t.Order.CompleteDate) >= startDate &&
                                                DbFunctions.TruncateTime(t.Order.CompleteDate) <= endDate).Select(t => t);
                                }
                                else
                                {
                                    orderItemsWithCoupons =
                                        orderItemsWithCoupons.Where(
                                            t =>
                                                DbFunctions.TruncateTime(t.Order.CompleteDate) >= filters.RegistrationStart.Value);
                                }
                            }
                            else if (filters != null && filters.RegistrationEnd.HasValue)
                            {
                                orderItemsWithCoupons =
                                        orderItemsWithCoupons.Where(
                                            t =>
                                                DbFunctions.TruncateTime(t.Order.CompleteDate) <= filters.RegistrationEnd.Value);
                            }

                            var orderItemsChargeDiscounts =
                                orderItemsWithCoupons.SelectMany(
                                    o => o.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon));

                            if (filters != null && filters.CouponFilter > 0)
                            {
                                orderItemsChargeDiscounts = orderItemsChargeDiscounts.Where(c => c.CouponId == filters.CouponFilter);
                            }

                            var couponModel = FillCouponeViewModel(orderItemsChargeDiscounts)
                                    .Select(p => new
                                    {
                                        p.FullName,
                                        p.ProgramName,
                                        p.Email,
                                        p.RegDate,
                                        p.ConfirmationId,
                                        p.CouponName,
                                        p.CouponCode,
                                        p.Amount
                                    }).ToList<object>();

                            expsch = Ioc.CustomReportBusiness.ExportCouponReport(couponModel);
                            fileName = string.Format(fileNameFormat, club.Name, "coupon", seasonDomain, RandomHelper.GenerateGuid());
                            break;
                        }
                }

                // Save in blob.
                var output = type == ReportType.Custom ? exp.ExportExcelDicData(expsch) : exp.ExportExcel(expsch);

                using (var stream = new MemoryStream(output.GetBuffer()))
                {
                    fileName = string.Format(fileName, club.Name, RandomHelper.GenerateGuid(),
                        Constants.E_ExcelFileExt);

                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection,
                        Constants.Path_ClubFilesContainer);
                    sm.UploadBlob(club.Domain, fileName, stream, "application/xls");

                }

                var reportLink = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri;

                var templateModel = new EmailTemplateModel
                {
                    HasLogo = true,
                    HasHeader = true,
                    ClubLogo = _clubBusiness.GetClubLogoURL(club.Domain, club.Logo),
                    Body = string.Format("{0} <br/><hr/> <a href=" + reportLink + "> Download Report </a>",
                        model.Message),
                    IsRegisterationEmail = true
                };

                var body = this.RenderViewToString(Constants.Path_EmailTemplate, templateModel);

                Ioc.OldEmailBusiness.SendEmail(club.ContactPersons.FirstOrDefault().Email, model.SentTo, model.Subject,
                    body, true);

                return JsonNet(new JResult { Status = true });
            }
            return JsonFormResponse();
        }

        [HttpPost]
        public virtual ActionResult KendoExport(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            //using (var stream = new MemoryStream(fileContents))
            //{
            //    //memType = Constants.E_Pdf_MimeType;
            //    fileName = string.Format(fileName, ActiveClub.Name, Utilities.GenerateGuid(),
            //        Constants.E_ExcelFileExt);

            //    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection,
            //        Constants.Path_ClubFilesContainer);
            //    sm.UploadBlob(ActiveClub.Domain, fileName, stream, "application/xls");
            //}

            return File(fileContents, contentType, fileName);
        }

        public virtual ActionResult Downloadreport(string fileName)
        {
            byte[] filebytes = null;

            var type = Path.GetExtension(fileName).Remove(0, 1);

            if (StorageManager.IsExist(StorageManager.GetUri(Constants.Path_ClubFilesContainer, ActiveClub.Domain, fileName).AbsoluteUri))
            {
                var blobReader = new WebClient();
                var url = StorageManager.GetUri(Constants.Path_ClubFilesContainer, ActiveClub.Domain, fileName).AbsoluteUri;
                filebytes = blobReader.DownloadData(url);
            }

            return File(filebytes, string.Format("application/{0}", type), fileName);
        }
        [HttpPost]
        public virtual JsonNetResult GetCheckinsReport(SelectedPrograms model)
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);
            var club = ActiveClub;

            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();

            }
            var checkins = Ioc.UserEventBusiness.GetList(model._SelectedPrograms).ToList();

            var result = checkins.Select(c =>
                new CheckinReportViewModel
                {
                    Id = c.Program.Id,
                    ProgramName = c.Program.Name
                }
            ).DistinctBy(c => c.Id)
            .ToList();

            var dataSource = result.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = result.Count });
        }

        [HttpPost]
        public virtual JsonNetResult GetAllCheckinsReport(SelectedPrograms model)
        {
            var club = ActiveClub;

            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();

            }
            var checkins = Ioc.UserEventBusiness.GetList(model._SelectedPrograms).ToList();

            var programBusiness = Ioc.ProgramBusiness;

            var result = checkins.Select(c =>
                new
                {
                    Id = c.Program.Id,
                    ProgramName = c.Program.Name,
                    SessionDate = programBusiness.GetSession(c.Program, c.SessionId.Value) != null ?
                                  programBusiness.GetSession(c.Program, c.SessionId.Value).Start.ToString(Constants.DateTime_Comma) : "-",
                    CheckinTime = c.ClubDateTime != null ? c.ClubDateTime.ToString("h:m tt") : "-",
                    UserName = GetClubStaff(c),
                    Address = c.Address != null && !string.IsNullOrWhiteSpace(c.Address.Address) ? (c.Address.Address.Contains("Street,") == true ?
                              c.Address.Address.Insert(c.Address.Address.IndexOf("Street,") + 7, "\n") : c.Address.Address) : "-",
                    GPS = c.Address.Lat != 0 && c.Address.Lng != 0 ? string.Format("{0}, {1}", c.Address.Lat, c.Address.Lng) : "-",
                    Distance = c.Program.ClubLocation.PostalAddress != null && c.Address != null ?
                               ((int)GoogleMap.CalculateDistanceBetweenTwoPoint(c.Program.ClubLocation.PostalAddress.Lat, c.Program.ClubLocation.PostalAddress.Lng, c.Address.Lat, c.Address.Lng)).ToString() : "-",
                    CheckinDelay = programBusiness.GetSession(c.Program, c.SessionId.Value) != null && c.ClubDateTime.TimeOfDay != null ?
                                   (-((int)(c.ClubDateTime.TimeOfDay - programBusiness.GetSession(c.Program, c.SessionId.Value).Start.TimeOfDay).TotalMinutes)).ToString() : "-"
                }
            )
            .ToList();


            return JsonNet(new { DataSource = result, TotalCount = result.Count });
        }

        public virtual ActionResult GetCustomAttendanceReportDetail(string seasonDomain = "", int programId = 0)
        {
            var clubId = ActiveClub.Id;
            var programBusiness = Ioc.ProgramBusiness;

            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var program = programBusiness.Get(programId);

            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM
                SeasonName = season != null ? season.Title : "-",
                ClassName = program.Name,
                Days = string.Join(", ", programBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList()),
                Room = program.Room != null ? program.Room : string.Empty
            };

            return JsonNet(model);
        }

        [HttpGet]
        public virtual ActionResult GetCustomAttendanceReport(int programId)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var formBusiness = Ioc.FormBusiness;

            var result = Ioc.OrderItemBusiness.GetProgramOrderItems(programId)
                .Where(
                 i =>
                       !i.Order.IsDraft && i.ProgramScheduleId.HasValue &&
                       (i.ItemStatus == OrderItemStatusCategories.completed ||
                       (i.ItemStatus == OrderItemStatusCategories.changed && i.ItemStatusReason != OrderItemStatusReasons.transferOut) &&
                       i.ItemStatusReason != OrderItemStatusReasons.canceled))
                            .ToList()
                            .Select(i => new
                            {
                                ParticipantFullName = string.Format("{0} {1}", formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName), formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName)),
                                StandardDismissal = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.StandardDismissal),
                                SchoolGrade = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.Grade),
                                SchoolTeacherName = formBusiness.GetTeacher(i.JbForm),
                                Medical_AllergiesMedicalInfo_SpecialNeeds = formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, ElementsName.MedicalConditions),
                                Medical_AllergiesAllergies = formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, "Allergies")
                            });


            var dataSource = result.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = result.ToList().Count });

        }

        public virtual ActionResult GetProgramSessions(long programId)
        {
            var sessions = new List<SelectKeyValue<int>>();
            ScheduleSession lastSession = null;

            var programName = string.Empty;

            var program = Ioc.ProgramBusiness.Get(programId);

            if (program != null)
            {
                programName = program.Name;

                var allSessions = Ioc.ProgramBusiness.GetSessions(program).ToList();
                sessions = allSessions
                 .OrderBy(o => o.Start)
                     .ToList()
                     .Select(s => new SelectKeyValue<int>
                     {
                         Value = s.Id,
                         Text = s.Start.ToString("MMM dd, yyyy")
                     })
                     .ToList();

                lastSession = GetLastSession(program, allSessions);
            }

            return Json(new { Sessions = sessions, ProgramName = programName, LastSessionId = lastSession.Id, LastSessionDate = lastSession.Start.ToString("MMM dd, yyyy") }, JsonRequestBehavior.AllowGet);
        }

        private ScheduleSession GetLastSession(Program program, List<ScheduleSession> sessions)
        {
            var club = program.Club;
            var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow);

            var sessinsMinDateTime = sessions.Min(s => s.Start);
            var sessinsMaxDateTime = sessions.Max(s => s.End);

            if (clubDateTime > sessinsMinDateTime && clubDateTime < sessinsMaxDateTime)
            {
                foreach (var session in sessions.OrderByDescending(o => o.Start))
                {
                    if (session.Start < clubDateTime)
                    {
                        return session;
                    }
                }
            }
            else if (clubDateTime < sessinsMinDateTime)
            {
                return sessions.First();
            }

            return sessions.Last();
        }

        [HttpPost]
        public virtual ActionResult SessionAttendanceReportRun(long programId, int? sessionId)
        {
            var result = new List<SessionAttendanceItemViewModel>();

            var programBusiness = Ioc.ProgramBusiness;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            if (programId == 0)
            {
                return JsonNet(new { DataSource = result, TotalCount = 0 });
            }

            if (!sessionId.HasValue)
            {
                var program = programBusiness.Get(programId);

                var sessions = programBusiness.GetSessions(program);

                sessionId = sessions != null && sessions.Any() ? sessions.Last().Id : 0;
            }

            var query = programBusiness.GetSessionAttendance(programId, sessionId.Value);

            result = query.ToList()
                    .Select(s => new SessionAttendanceItemViewModel
                    {
                        Id = s.Id,
                        FullName = s.PlayerProfile.Contact.FullName,
                        Status = s.Status
                    })
                    .OrderBy(o => o.FullName)
                    .ToList();

            var dataSource = result.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = query.Count });
        }

        [HttpPost]
        public virtual ActionResult StudentSummaryAttendanceReportRun(long programId)
        {
            var model = new List<IDictionary<string, Object>>();
            var sessionAttendances = new List<ScheduleAttendance>();

            var programBusiness = Ioc.ProgramBusiness;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            if (programId == 0)
            {
                return JsonNet(new { DataSource = model, TotalCount = 0 });
            }

            var program = programBusiness.Get(programId);
            var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

            var sessions = programBusiness.GetSessions(program).Where(s => s.End.Date <= clubDateTime.Date);

            if (sessions == null && !sessions.Any())
            {
                return JsonNet(new { DataSource = new SessionAttendanceItemViewModel(), TotalCount = 0 });
            }

            foreach (var session in sessions)
            {
                sessionAttendances.AddRange(programBusiness.GetSessionAttendance(programId, session.Id));
            }

            var groupbyPlayers = sessionAttendances.
                GroupBy(s => s.PlayerProfile.Id).
                Select(s => s.FirstOrDefault())
                .OrderBy(s => s.PlayerProfile.Contact.LastName)
                .ThenBy(o => o.PlayerProfile.Contact.FirstName)
                .ToList();

            foreach (var player in groupbyPlayers)
            {
                var newItem = new ExpandoObject() as IDictionary<string, Object>;

                var presentCount = 0;
                var absentCount = 0;
                var lateCount = 0;

                newItem.Add("Participant", string.Format("{0}, {1}", player.PlayerProfile.Contact.LastName, player.PlayerProfile.Contact.FirstName));

                foreach (var item in sessions)
                {
                    var startDate = item.Start;
                    var endDate = item.End;
                    var status = sessionAttendances.SingleOrDefault(a => a.SessionId == item.Id && a.PlayerProfile.Id == player.PlayerProfile.Id) != null ? sessionAttendances.SingleOrDefault(a => a.SessionId == item.Id && a.PlayerProfile.Id == player.PlayerProfile.Id).Status : null;

                    presentCount += status == AttendanceStatus.Present ? 1 : 0;
                    absentCount += status == AttendanceStatus.Absent ? 1 : 0;
                    lateCount += status == AttendanceStatus.Late ? 1 : 0;

                    var title = startDate.ToString(Constants.DateTime_Comma).Replace(" ", "_").Replace("/", "_SLASH_").Replace("(", "_BRAKETOPEN_").Replace(")", "_BRAKETCLOSE_").Replace(",", "_COMMA_").Replace("-", "_SDASH_");

                    var value = status == null ? "?" : status.ToString();

                    newItem.Add(title, value);
                }

                newItem.Add("Show_totals", string.Format("Presents: {0}, Absents: {1}, Lates: {2}", presentCount, absentCount, lateCount));

                model.Add(newItem);
            }

            var dataSource = model;
            var total = groupbyPlayers.Count();

            return JsonNet(new { DataSource = dataSource, TotalCount = total, ProgramName = program.Name });
        }

        [HttpPost]
        public virtual ActionResult SignOutSheetReportRun(long programId, bool printMode = false)
        {
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId);

            if (orderItems == null || !orderItems.Any())
            {
                return Json(new { DataSource = new List<object>(), TotalCount = 0 });
            }

            var data = orderItems.ToList()
                .Select(i => new SignOutSheetReportViewModel()
                {
                    ParticipantFirstName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName),
                    ParticipantLastName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName),
                    ParentFirstName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName),
                    ParentLastName = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName),
                    ParentPrimaryPhone = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }),
                    ParentAuthorizedAdult = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, "Authorized Adult"),
                    ParentAuthorizedAdultPhone = Ioc.FormBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, "Authorized Adult Phone"),
                })
                .ToList();

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();
                var program = Ioc.ProgramBusiness.Get(programId);
                model.Date = Ioc.ClubBusiness.GetClubDateTime(program.ClubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt");//Sep 21, 2016 HH:MM

                model.Data = data;

                return Pdf(program.Season.Domain + "_" + program.Domain + " (Sign-outSheet)", "_SignOutSheetReportPdfExportView", model);

            }
            return Json(new { DataSource = data, TotalCount = data.Count }, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult GetSignOutSheetReportDetail(long programId)
        {
            var program = Ioc.ProgramBusiness.Get(programId);

            var data = new
            {
                ProgramName = program.Name,
                ProgramDomain = program.Domain,
                Date = Ioc.ClubBusiness.GetClubDateTime(program.ClubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt"),//Sep 21, 2016 HH:MM
                SeasonName = program.Season.Title
            };

            return JsonNet(data);
        }

        public virtual ActionResult GetDismissalInformationReportDetail(string seasonDomain = "", int programId = 0)
        {
            var clubId = ActiveClub.Id;
            var programBusiness = Ioc.ProgramBusiness;

            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var program = programBusiness.Get(programId);

            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM
                SeasonName = season != null ? season.Title : "-",
                ClassName = program.Name,
                Days = string.Join(", ", programBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList())
            };

            return JsonNet(model);
        }

        [HttpGet]
        public virtual ActionResult GetDismissalInformationReport(int programId)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var clubId = ActiveClub.Id;
            var formBusiness = Ioc.FormBusiness;

            var result = Ioc.OrderItemBusiness.GetProgramOrderItems(programId)
                .Where(
                 i =>
                       !i.Order.IsDraft && i.ProgramScheduleId.HasValue &&
                       (i.ItemStatus == OrderItemStatusCategories.completed ||
                       (i.ItemStatus == OrderItemStatusCategories.changed && i.ItemStatusReason != OrderItemStatusReasons.transferOut) &&
                       i.ItemStatusReason != OrderItemStatusReasons.canceled))
                            .ToList()
                            .Select(i => new
                            {
                                ParticipantFullName = string.Format("{0} {1}", formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName), formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName)),

                                ParentFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName),
                                ParentLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName),
                                ParentPrimaryPhone = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }),
                                ParentAlternatePhone = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.AlternatePhone, new List<string> { "Alternate phone", "Alternative phone" }),

                                ParentAuthorizedAdult1 = formBusiness.GetAuthorizedAdult(i.JbForm, null),
                                ParentAuthorizedAdultPhone1 = formBusiness.GetAuthorizedAdultPhone(i.JbForm, null),

                                ParentAuthorizedAdult2 = formBusiness.GetAuthorizedAdult(i.JbForm, SectionsName.AuthorizedPickup2),
                                ParentAuthorizedAdultPhone2 = formBusiness.GetAuthorizedAdultPhone(i.JbForm, SectionsName.AuthorizedPickup2),

                                ParentAuthorizedAdult3 = formBusiness.GetAuthorizedAdult(i.JbForm, SectionsName.AuthorizedPickup3),
                                ParentAuthorizedAdultPhone3 = formBusiness.GetAuthorizedAdultPhone(i.JbForm, SectionsName.AuthorizedPickup3),

                                ParentAuthorizedAdult4 = formBusiness.GetAuthorizedAdult(i.JbForm, SectionsName.AuthorizedPickup4),
                                ParentAuthorizedAdultPhone4 = formBusiness.GetAuthorizedAdultPhone(i.JbForm, SectionsName.AuthorizedPickup4),
                                EmergencyContactFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName),
                                EmergencyContactLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.LastName),
                                EmergencyContactPhone = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.HomePhone, new List<string> { "Emergency contact phone number", "Emergency contact phone", "Phone" }),
                                SchoolDismissalFromEnrichment = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }),
                            });


            var dataSource = result.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = result.ToList().Count });

        }

        [HttpGet]
        public virtual JsonNetResult GetAllProgramsDailyDismissalReport(string seasonDomain)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);
            var programs = Ioc.ProgramBusiness
                .GetListBySeasonId(season.Id)
                .Where(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.TypeCategory == ProgramTypeCategory.Class)
                .OrderBy(p => p.Name)
                .ToList();

            var allPrograms = programs.Select(p => new SelectKeyValue<string>
            {
                Text = (p.OutSourceSeasonId.HasValue && club.IsProvider) ? p.Name + " (" + p.Club.Name + ")" :
                (p.OutSourceSeasonId.HasValue && club.IsSchool) ? p.Name + " (" + p.OutSourceSeason.Club.Name + ")" : p.Name,

                Value = p.Id.ToString()
            })
                   .OrderBy(s => s.Text).ToList();

            return JsonNet(allPrograms);
        }

        [HttpGet]
        public virtual JsonNetResult GetSessionsDailyDismissalReport(long programId)
        {
            var program = Ioc.ProgramBusiness.Get(programId);

            var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

            var programSesssions = Ioc.ProgramBusiness.GetSessions(program)
                .Where(s => s.End.Date <= clubDateTime.Date)
                .OrderBy(o => o.Start)
                .Select(s => new SelectKeyValue<long>()
                {
                    Text = s.Start.ToString("ddd, MMM dd, yyyy"),
                    Value = s.Id,
                })
                    .ToList();

            return JsonNet(programSesssions);
        }

        [HttpPost]
        public virtual ActionResult GetDailyDismissalReport(long programId, int sessionId, bool printMode = false)
        {
            if (programId == 0 && sessionId == 0)
            {
                return JsonNet(new { DataSource = new List<object>(), TotalCount = 0 });
            }

            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var formBusiness = Ioc.FormBusiness;
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId).ToList();

            var data = Ioc.ProgramBusiness.GetSessionAttendance(programId, sessionId).ToList().Select(s =>
                  new DailyDismissalReportViewModel
                  {
                      Id = string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId),
                      StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                      DismissalFromEnrichment = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,
                      IsPickedUp = s.IsPickedUp ? "Dismissed" : string.Empty,
                      PickupTime = s.Pickup != null ? s.Pickup.LocalDateTime.ToString("hh:mm tt") : string.Empty,
                      PickupName = s.Pickup != null ? s.Pickup.FullName : string.Empty,
                  }).OrderBy(s => s.StudentName).ToList();

            if (printMode)
            {
                var model = new DailyReportReportPdfExportViewModel();

                var clubId = ActiveClub.Id;
                var clubName = ActiveClub.Name;
                var date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM

                model.Data = data;
                model.ClubName = clubName;
                model.Date = date;

                // Convert to pdf
                var viewRendered = this.RenderViewToString("_DailyDismissalReportPdfExportView", model);

                using (var pdfStream = new MemoryStream())
                {
                    var pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
                    var pdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, pdfStream);

                    pdfDoc.Open();

                    using (var strHtml = new StringReader(viewRendered))
                    {
                        iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(pdfWriter, pdfDoc, strHtml);
                    }

                    pdfDoc.Close();

                    using (var stream = new MemoryStream(pdfStream.GetBuffer()))
                    {
                        byte[] buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        pdfStream.Close();
                        stream.Close();
                        return File(buffer, "application/pdf", "DailyDismissalReport.pdf");
                    }
                }

            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = data.Count });
        }

        public virtual ActionResult GetProviderContactInformationReport(long seasonId, PaginationModel paginationModel, ShareAsAttachment model, bool isShare = false, bool printMode = false)
        {
            var seasonPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Where(p => p.Status != ProgramStatus.Frozen);
            var season = Ioc.SeasonBusiness.Get(seasonId);

            var models = new List<ProviderContactInfoViewModel>();
            var seasonProgramPaging = (!isShare) ?
                seasonPrograms.OrderBy(c => c.Name)
                    .Skip((paginationModel.Page - 1) * paginationModel.PageSize)
                    .Take(paginationModel.PageSize) : seasonPrograms;
            foreach (var program in seasonProgramPaging)
            {
                var providerName = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.Name
                    : string.Empty;

                var fullName = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.ContactPersons.First().FirstName + " " + program.OutSourceSeason.Club.ContactPersons.First().LastName
                    : program.Club.ContactPersons.First().FirstName + " " + program.Club.ContactPersons.First().LastName;

                var contactEmail = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.ContactPersons.First().Email
                    : program.Club.ContactPersons.First().Email;

                var phone = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.ContactPersons.First().Phone
                    : program.Club.ContactPersons.First().Phone;

                ClubStaff instructor1St = null;
                ClubStaff instructor2nd = null;
                if (program.Instructors != null && program.Instructors.Any())
                {
                    instructor1St = program.Instructors.First();
                    if (program.Instructors.Count > 1)
                    {
                        instructor2nd = program.Instructors.Last();
                    }
                }



                models.Add(new ProviderContactInfoViewModel
                {
                    ClassName = program.Name,
                    ProviderName = providerName,
                    FullName = fullName,
                    ContactEmail = contactEmail,
                    Phone = phone,
                    Instructor1St = (instructor1St != null && instructor1St.Contact != null) ? instructor1St.Contact.FullName : "",
                    Phone1StInstructor = (instructor1St != null && instructor1St.Contact != null) ? instructor1St.Contact.Phone : "",
                    Instructor2nd = (instructor2nd != null && instructor2nd.Contact != null) ? instructor2nd.Contact.FullName : "",
                    Phone2ndInstructor = (instructor2nd != null && instructor2nd.Contact != null) ? instructor2nd.Contact.Phone : "",
                });
            }
            models = models.OrderBy(p => p.ClassName).ToList();
            if (isShare)
            {
                // Save in blob.
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
                var exp = new ExportFileHelper();

                var expsch = Ioc.CustomReportBusiness.ExportProviderContactInfo(models.Select(m => new
                {
                    ClassName = m.ClassName,
                    ProviderName = m.ProviderName,
                    FullName = m.FullName,
                    ContactEmail = m.ContactEmail,
                    Phone = m.Phone,
                    Instructor1St = m.Instructor1St,
                    Phone1StInstructor = m.Phone1StInstructor,
                    Instructor2nd = m.Instructor2nd,
                    Phone2ndInstructor = m.Phone2ndInstructor,

                }).ToList<object>());

                MemoryStream output = exp.ExportExcel(expsch);
                var objcetName = GetReoprtNameWithSeason(seasonId, season.ClubId).Data;
                var fileName = (objcetName != null ? objcetName.ToString() : "") + " (Provider contact info).xls";
                using (var stream = new MemoryStream(output.GetBuffer()))
                {
                    //memType = Constants.E_Pdf_MimeType;
                    //fileName = string.Format(fileName, club.Name, Utilities.GenerateGuid(),
                    //    Constants.E_ExcelFileExt);

                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection,
                        Constants.Path_ClubFilesContainer);
                    sm.UploadBlob(club.Domain, fileName, stream, "application/xls");

                }

                var reportLink = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri;

                var templateModel = new EmailTemplateModel
                {
                    HasLogo = true,
                    HasHeader = true,
                    ClubLogo = _clubBusiness.GetClubLogoURL(club.Domain, club.Logo),
                    Body = string.Format("{0} <br/><hr/> <a href=" + reportLink + "> Download Report </a>", model.Message),
                    IsRegisterationEmail = true
                };

                var body = this.RenderViewToString(Constants.Path_EmailTemplate, templateModel);

                Ioc.OldEmailBusiness.SendEmail(club.ContactPersons.First().Email, model.SentTo, model.Subject,
                    body, true);
                return JsonNet(new JResult { Status = true });
            }

            if (printMode)
            {
                dynamic printModel = new System.Dynamic.ExpandoObject();
                var ClubName = ActiveClub.Name;
                printModel.Date = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                printModel.ClubName = ClubName;
                printModel.Data = models;

                return Pdf(ClubName + " (ProviderContactInformation)", "_ProviderContactInformationReportPdfExportView", printModel);

            }


            return JsonNet(new { DataSource = models, TotalCount = seasonPrograms.Count() });
        }

        public virtual ActionResult GetProviderContactInformationReportDetail(string seasonDomain)
        {
            var clubId = ActiveClub.Id;
            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id).Id;

            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM
                SeasonId = seasonId
            };

            return JsonNet(model);
        }

        public virtual JsonNetResult GetReoprtNameWithSeason(long seasonId, int schoolId)
        {
            var seasonName = Ioc.SeasonBusiness.Get(seasonId).Title.Replace('.', '_');
            var schoolName = Ioc.ClubBusiness.Get(schoolId).Name.Replace('.', '_');
            var reportTitle = string.Format("{0}_{1}", seasonName, schoolName);
            return JsonNet(reportTitle);
        }

        [HttpPost]
        public virtual JsonNetResult GetCheckinsReportDetails(long programId)
        {
            var programBusiness = Ioc.ProgramBusiness;

            var checkins = Ioc.UserEventBusiness.GetList().Where(c => c.ProgramId == programId).ToList();

            var result = checkins.Select(c =>
                new CheckinReportViewModel
                {
                    SessionDate = programBusiness.GetSession(c.Program, c.SessionId.Value) != null ?
                                  programBusiness.GetSession(c.Program, c.SessionId.Value).Start.ToString(Constants.DateTime_Comma) : "-",
                    CheckinTime = c.ClubDateTime != null ? c.ClubDateTime.ToString("h:m tt") : "-",
                    UserName = GetClubStaff(c),
                    Address = c.Address != null && !string.IsNullOrWhiteSpace(c.Address.Address) ? (c.Address.Address.Contains("Street,") == true ?
                              c.Address.Address.Insert(c.Address.Address.IndexOf("Street,") + 7, "\n") : c.Address.Address) : "-",
                    GPS = c.Address.Lat != 0 && c.Address.Lng != 0 ? string.Format("{0}, {1}", c.Address.Lat, c.Address.Lng) : "-",
                    Distance = c.Program.ClubLocation.PostalAddress != null && c.Address != null ?
                               ((int)GoogleMap.CalculateDistanceBetweenTwoPoint(c.Program.ClubLocation.PostalAddress.Lat, c.Program.ClubLocation.PostalAddress.Lng, c.Address.Lat, c.Address.Lng)).ToString() : "-",
                    CheckinDelay = programBusiness.GetSession(c.Program, c.SessionId.Value) != null && c.ClubDateTime.TimeOfDay != null ?
                                   (-((int)(c.ClubDateTime.TimeOfDay - programBusiness.GetSession(c.Program, c.SessionId.Value).Start.TimeOfDay).TotalMinutes)).ToString() : "-"
                }
            )
            .ToList();

            return JsonNet(new { DataSource = result, TotalCount = result.Count });
        }

        private string GetClubStaff(UserEvent userEvent)
        {
            return userEvent.Program.OutSourceSeason != null ? (userEvent.Program.OutSourceSeason.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == userEvent.UserId) != null ?
                   userEvent.Program.OutSourceSeason.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == userEvent.UserId).Contact.FullName : "-")
                   : (userEvent.Program.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == userEvent.UserId) != null ?
                   userEvent.Program.Club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == userEvent.UserId).Contact.FullName : "-");
        }

        [HttpGet]
        public virtual ActionResult GetInstructorListReportInfo(string seasonDomain, bool printMode = false)
        {
            var clubId = ActiveClub.Id;

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            var instructors = season.Programs.Where(c => c.Status != ProgramStatus.Deleted).SelectMany(i => i.Instructors).Distinct().ToList();
            var outSourcePrograms = season.OutSourcePrograms;

            instructors.AddRange(outSourcePrograms.Where(c => c.Status != ProgramStatus.Deleted).SelectMany(p => p.Instructors).Distinct().ToList());

            var data = instructors.Distinct().Select(i => new InstructorListReportViewModel()
            {
                Name = i.Contact.FullName,
                CellPhone = i.Contact.Phone != null ? i.Contact.Phone : "-",
                DateOfBackgroundCheck = i.DateOfBackgroundCheck != null ? string.Format("{0:MMM dd, yyyy}", i.DateOfBackgroundCheck) : "-",
                Status = i.Status.ToString()
            });

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();

                model.Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var clubName = this.GetCurrentClubName();
                model.ClubName = clubName;
                model.SeasonName = season.Title;

                return Pdf(clubName + " (InstructorList)", "_InstructorListReportPdfExportView", model);
            }

            return Json(new { DataSource = data, TotalCount = instructors.Count }, JsonRequestBehavior.AllowGet);

        }

        public virtual ActionResult GetInstructorListReportDetail(string seasonDomain = "")
        {
            var clubId = ActiveClub.Id;

            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM
                SeasonName = season != null ? season.Title : "-"
            };

            return JsonNet(model);
        }


        public virtual ActionResult GetInstructorAssignmentsReportDetail(string seasonDomain = "")
        {
            var clubId = ActiveClub.Id;

            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM
                SeasonName = season != null ? season.Title : "-"
            };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult GetInstructorAssignmentsReportInfo(string seasonDomain, bool printMode = false)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var clubId = ActiveClub.Id;
            var seasonBusiness = Ioc.SeasonBusiness;

            var season = seasonBusiness.Get(seasonDomain, clubId);

            var instructors = season.Programs.Where(c => c.Status != ProgramStatus.Deleted).SelectMany(i => i.Instructors).Distinct().ToList();
            var outSourcePrograms = season.OutSourcePrograms;

            instructors.AddRange(outSourcePrograms.Where(c => c.Status != ProgramStatus.Deleted).SelectMany(p => p.Instructors).Distinct().ToList());

            var data = new List<InstructorAssignmentsReportViewModel>();
            foreach (var instructor in instructors.Distinct())
            {
                var programs = instructor.Programs;
                var classAssignemnts = new List<string>();

                foreach (var program in programs)
                {
                    if (program.Status != ProgramStatus.Deleted && (program.Season.Id == season.Id || program.OutSourceSeasonId == season.Id))
                    {
                        if (program.OutSourceSeason != null)
                        {
                            classAssignemnts.Add(program.Name + " (" + program.Club.Name + ")");
                        }
                        else
                        {
                            classAssignemnts.Add(program.Name);
                        }
                    }
                }
                data.Add(new InstructorAssignmentsReportViewModel()
                {
                    Name = instructor.Contact.FullName,
                    Status = instructor.Status.ToString(),
                    DateOfBackgroundCheck = instructor.DateOfBackgroundCheck != null ? string.Format("{0:MMM dd, yyyy}", instructor.DateOfBackgroundCheck) : "-",
                    ClassAssignments = string.Join(", ", classAssignemnts)
                });
            }

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();

                model.Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = data;
                var clubName = Ioc.ClubBusiness.Get(clubId).Name;
                model.ClubName = clubName;
                model.SeasonName = season.Title;

                return Pdf(clubName + " (InstructorAssignments)", "_InstructorAssignmentsReportPdfExportView", model);
            }

            var dataSource = data.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return Json(new { DataSource = dataSource, TotalCount = data.ToList().Count });

        }

        [HttpGet]
        public virtual JsonNetResult GetProgramsAndOrdersReportHeader(string seasonDomain)
        {
            var clubId = ActiveClub.Id;
            var seasonBusiness = Ioc.SeasonBusiness;
            var season = seasonBusiness.Get(seasonDomain, clubId);

            var model = new
            {
                SeasonName = season.Title,
                Date = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt")
            };

            return JsonNet(model);
        }

        [HttpGet]
        public virtual JsonNetResult GetStaffListReportHeader()
        {
            var model = new
            {
                Date = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt")
            };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual JsonNetResult GetParentEmailsListReportDetail(SelectedPrograms model)
        {
            var programNames = new List<string>();
            var programBusiness = Ioc.ProgramBusiness;
            var clubId = ActiveClub.Id;

            var season = Ioc.SeasonBusiness.Get(model.SeasonDomain, clubId);
            var seasonId = season.Id;

            if (model.IsAllPrograms)
            {
                programNames = programBusiness.GetSeasonPrograms(seasonId).Select(p => p.Name).ToList();
            }
            else
            {
                programNames.Add(programBusiness.Get(model._SelectedPrograms.First()).Name);
            }

            var result = new
            {
                Date = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString("MMM dd, yyyy hh:MM tt"),
                programsSelected = model.IsAllPrograms ? string.Format("{0}, ...", programNames.First()) : programNames.First(),
                SeasonName = season.Title,
                ClubName = ActiveClub.Name
            };

            return JsonNet(result);
        }

        [HttpPost]
        public virtual ActionResult GetParentEmailsListReportInfo(SelectedPrograms model, string programNames, bool printMode = false)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var clubId = ActiveClub.Id;

            var season = Ioc.SeasonBusiness.Get(model.SeasonDomain, clubId);
            if (model.IsAllPrograms)
            {
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(season.Id).Where(p => p.Status != ProgramStatus.Deleted).Select(p => p.Id).ToList();
            }

            var formBusiness = Ioc.FormBusiness;

            var result = Ioc.OrderItemBusiness.GetReportOrderItems(model._SelectedPrograms)
                .Where(
                 i =>
                       !i.Order.IsDraft && i.ProgramScheduleId.HasValue &&
                       (i.ItemStatus == OrderItemStatusCategories.completed ||
                       (i.ItemStatus == OrderItemStatusCategories.changed && i.ItemStatusReason != OrderItemStatusReasons.transferOut) &&
                       i.ItemStatusReason != OrderItemStatusReasons.canceled) && i.Order.ClubId == ActiveClub.Id)
                            .ToList()
                            .Select(i => new ParentEmailsListReportViewModel()
                            {
                                ProgramName = Ioc.ProgramBusiness.GetList().Where(p => p.ProgramSchedules.Select(s => s.Id).Contains(i.ProgramScheduleId.Value)).First().Name,
                                ParticipantFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName),
                                ParticipantLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName),
                                ParentEmail = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Email),
                            });


            if (printMode)
            {
                dynamic modelExport = new System.Dynamic.ExpandoObject();

                modelExport.Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                modelExport.Data = result;
                var clubName = ActiveClub.Name;
                modelExport.ClubName = clubName;
                modelExport.SeasonName = season.Title;
                modelExport.ProgramNames = programNames;

                return Pdf(clubName + " (ParentEmailsList)", "_ParentEmailsListReportPdfExportView", modelExport);
            }

            var dataSource = result.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = result.ToList().Count });
        }
        public virtual JsonNetResult GetAllPrograms(string seasonDomain = "")
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var programs = Ioc.ProgramBusiness.GetListBySeasonId(season.Id).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4);

            var Allprograms = programs.Select(p => new SelectKeyValue<long>() { Text = p.Name, Value = p.Id })
                    .OrderBy(s => s.Text).ToList();
            return JsonNet(Allprograms);
        }

        public virtual JsonNetResult GetAllProgramsSignOutSheetReport(string seasonDomain = "")
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var programs = Ioc.ProgramBusiness.GetListBySeasonId(season.Id)
                .Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted)
                .SelectMany(p => p.ProgramSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both && !s.IsDeleted))
                .ToList();

            var Allprograms = programs.Select(p => new SelectKeyValue<long>()
            {
                Text = !string.IsNullOrEmpty(p.Title)
                        ? $"{p.Program.Name} - {p.Title}"
                        : p.Program.Name,
                Value = p.Id
            })
             .OrderBy(s => s.Text).ToList();

            return JsonNet(Allprograms);
        }

        public virtual JsonNetResult GetSubscriptionSeasonPrograms(string seasonDomain = "")
        {
            var clubId = ActiveClub.Id;
            List<ProgramTypeCategory> programtypeCategory = new List<ProgramTypeCategory>() { ProgramTypeCategory.Subscription, ProgramTypeCategory.BeforeAfterCare };

            var allprograms = Ioc.NewReportBusiness.GetSeasonProgramsTextValue(clubId, seasonDomain, programtypeCategory);

            return JsonNet(allprograms);
        }

        public virtual JsonNetResult GetDate()
        {
            var model = new schoolProgramsCheckInOutReportViewModel();
            model.DateProgram = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow);

            return JsonNet(model.DateProgram);
        }

        public JsonNetResult GetReportDate()
        {
            var dateTime = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).ToString(Constants.DefaultDateTimeFormatAbrMonthWithAMPM);

            return JsonNet(new { Date = dateTime });
        }

        [HttpGet]
        public virtual FileResult GenerateAttendanceSheetAdvancedPdf(string seasonDomain, long? programId, DateTime date)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var showSchoolLogo = true;
            var school = Ioc.ClubBusiness.Get(clubId);
            var partner = new Club();

            if (school.PartnerId.HasValue)
            {
                partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);
            }
            else
            {
                partner = Ioc.ClubBusiness.Get(school.Id);
            }

            if (school.Setting != null)
            {
                showSchoolLogo = school.Setting.ShowSchoolLogo;
            }

            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelAttendanceSheetAdvancedReport(season.Id, school.Id, programId, date);

            var PdfModel = new SignOutSheetPdfSchoolViewModel()
            {
                SchoolLogo = showSchoolLogo ? _clubBusiness.GetClubLogoURL(school.Domain, school.Logo) : null,
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model,
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "AttendanceSheetAdvancedPdfSchoolReports", PdfModel);

            return File(file, "application/pdf", "attendance sheet advanced_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        public virtual ActionResult ProgramsListAttendanceSheetAdvancedReport(string seasonDomain, long? programId, DateTime date)
        {
            var clubId = ActiveClub.Id;
            var programBusiness = Ioc.ProgramBusiness;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var programs = programBusiness.GetListBySeasonId(season.Id).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4);
            var model = new List<SchoolProgramsSignOutReportViewModel>();

            model = CreateModelAttendanceSheetAdvancedReport(season.Id, clubId, programId, date);

            return JsonNet(model);
        }
        private List<SchoolProgramsSignOutReportViewModel> CreateModelAttendanceSheetAdvancedReport(long seasonId, int schoolId, long? programId, DateTime date)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            var club = _clubBusiness.Get(schoolId);
            var reportDate = $"{Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString(Constants.DefaultDateTimeFormatAbrMonthWithAMPM)} ({club.TimeZone.ToString()})";
            date = date.Date;
            var tommorow = date.AddDays(1);
            if (programId != null)
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Id == programId).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4).ToList();
            }
            var model = new List<SchoolProgramsSignOutReportViewModel>();

            model = programs != null ? programs.ToList().Select(p => new SchoolProgramsSignOutReportViewModel()
            {
                ClassName = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                ClassId = p.Id,
                ClassDays = programBusiness.GetClassDays(p, date) != null || programBusiness.GetClassDays(p, date).Any() ? programBusiness.FormatDays(programBusiness.GetClassDays(p, date).Select(pr => pr.DayOfWeek).ToList()) : "",
                StartTime = programBusiness.GetClassDays(p, date).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p, date).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(p, date).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p, date).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                DatesOfDay = GetListDates(p, schoolId, date),
                Location = p.Room,
                SchoolName = p.Season.Club.Name,
                SeasonName = p.Season.Domain,
                ProgramParticipants = GetAllParticipant(p, date).Count > 0 ? GetAllParticipant(p, date) : null,
                Instructor = programBusiness.GetInstractors(p),
                ReportDate = reportDate
            }).OrderBy(n => n.ClassName).ToList() : null;



            return model;
        }
        private List<string> GetListDates(Program program, int schoolId, DateTime selectedDate)
        {
            var programBusiness = Ioc.ProgramBusiness;

            var schedule = programBusiness.GetScheduleByDate(program, selectedDate);

            var classDays = programBusiness.GetClassDays(program, selectedDate);

            var result = classDays.Select(pr => pr.DayOfWeek).Count() > 1 ?
            programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, schedule.StartDate, schedule.EndDate), schedule.EndDate).Where(d => d.DayOfWeek >= classDays.FirstOrDefault().DayOfWeek && d.DayOfWeek <= classDays.LastOrDefault().DayOfWeek).OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList().Count > 5 ?
            programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, schedule.StartDate, schedule.EndDate), schedule.EndDate).Where(d => d.DayOfWeek >= classDays.FirstOrDefault().DayOfWeek && d.DayOfWeek <= classDays.LastOrDefault().DayOfWeek).OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList().GetRange(0, 5) :
            programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, schedule.StartDate, schedule.EndDate), schedule.EndDate).Where(d => d.DayOfWeek >= classDays.FirstOrDefault().DayOfWeek && d.DayOfWeek <= classDays.LastOrDefault().DayOfWeek).OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList() :
            programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, schedule.StartDate, schedule.EndDate), schedule.EndDate).Where(d => d.DayOfWeek == classDays.FirstOrDefault().DayOfWeek).ToList().OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList().Count > 5 ?
            programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, schedule.StartDate, schedule.EndDate), schedule.EndDate).Where(d => d.DayOfWeek == classDays.FirstOrDefault().DayOfWeek).ToList().OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList().GetRange(0, 5) :
            programBusiness.GetDatesBetweenTwoIntervals(schoolId, GetDate(selectedDate, schedule.StartDate, schedule.EndDate), schedule.EndDate).Where(d => d.DayOfWeek == classDays.FirstOrDefault().DayOfWeek).ToList().OrderBy(da => da.Date).ToList().Select(ds => ds.Date.ToString("MM/dd")).ToList();
            return result;
        }
        private DateTime GetDate(DateTime SelectedDate, DateTime StartDate, DateTime EndDate)
        {
            if (SelectedDate < StartDate)
            {
                SelectedDate = StartDate;
            }
            if (SelectedDate > EndDate)
            {
                SelectedDate = EndDate;
            }
            return SelectedDate;
        }
        private List<ProgramParticipantListSchoolViewModel> GetAllParticipant(Program program, DateTime date)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = new List<OrderItem>();
            var model = new List<ProgramParticipantListSchoolViewModel>();
            var num = 1;

            date = date.Date;
            var tommorow = date.AddDays(1);

            if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || program.TypeCategory == ProgramTypeCategory.Camp || program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                programOrderItems.AddRange(orderItemBusiness.GetProgramOrderItems(program.Id, OrderItemStatusCategories.showAll).Where(o => o.OrderSessions.Any(s => !s.ProgramSession.IsDeleted && s.ProgramSession.StartDateTime >= date && s.ProgramSession.StartDateTime < tommorow)).ToList());
            }
            else
            {
                programOrderItems.AddRange(orderItemBusiness.GetProgramOrderItems(program.Id).ToList().Where(o => o.ProgramSchedule.Sessions.ToList().Any(s => !s.IsDeleted && s.Start >= date && s.Start < tommorow)).ToList());
            }

            model = programOrderItems.ToList().Select(o => new ProgramParticipantListSchoolViewModel()
            {
                ParticipantName = string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)),
                TeacherName = o.JbForm != null ? formBusiness.GetTeacher(o.JbForm) == "" ? "-" : formBusiness.GetTeacher(o.JbForm) : "",
                Phone = formBusiness.GetParentPhone(o.JbForm) == "" ? "-" : formBusiness.GetParentPhone(o.JbForm),
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                Grade = formBusiness.GetPlayerGradeAsString(o.JbForm) != null ? formBusiness.GetPlayerGradeAsString(o.JbForm) == "Kindergarten" ? "K" : formBusiness.GetPlayerGradeAsString(o.JbForm) : "-",
                DoB = o.JbForm != null ? !formBusiness.GetParticipantDoB(o.JbForm).HasValue ? "-" : formBusiness.GetParticipantDoB(o.JbForm).Value.ToString(Constants.DateTime_Comma) : ""
            }).OrderBy(n => n.ParticipantName).ToList();

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }
        [HttpGet]
        public virtual FileContentResult GenerateAttendanceSheetAdvancedExcel(string seasonDomain, long? programId, DateTime date)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = CreateModelAttendanceSheetAdvancedReport(season.Id, clubId, programId, date);

            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();


            //Define Style
            ICellStyle alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.VerticalAlignment = VerticalAlignment.Center;

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var borderTopStyle = workbook.CreateCellStyle();
            borderTopStyle.BorderTop = BorderStyle.Thin;
            borderTopStyle.Alignment = HorizontalAlignment.Center;
            borderTopStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleLeftAlignment = workbook.CreateCellStyle();
            greyStyleLeftAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleLeftAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleLeftAlignment.Alignment = HorizontalAlignment.Left;
            greyStyleLeftAlignment.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleCenterAlignment = workbook.CreateCellStyle();
            greyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            greyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;
            //greyStyleCenterAlignment.BorderBottom = BorderStyle.MEDIUM;


            sheet.SetDefaultColumnStyle(0, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(1, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(2, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(3, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(4, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(5, alignmentCenterStyle);


            var rowNumber = 0;
            var itemNumber = 0;

            foreach (var item in model)
            {
                //**** Header ****////
                var headerRow = sheet.CreateRow(rowNumber);

                if (itemNumber != 0)
                {
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                }
                itemNumber++;

                // set default column size and color
                headerRow.CreateCell(0).SetCellValue(" ");
                headerRow.CreateCell(1).SetCellValue(" ");
                headerRow.CreateCell(2).SetCellValue(" ");
                headerRow.CreateCell(3).SetCellValue(" ");
                headerRow.CreateCell(4).SetCellValue(" ");
                headerRow.CreateCell(5).SetCellValue(" ");
                headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(4).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 50 * 256);
                headerRow.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 10 * 256);
                headerRow.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                headerRow.GetCell(5).Sheet.SetColumnWidth(5, 10 * 256);

                //Set the column names in the header row
                headerRow.CreateCell(1).SetCellValue(item.ClassName);
                headerRow.CreateCell(6).SetCellValue(item.SchoolName);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 50 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
                headerRow.GetCell(1).CellStyle = greyBoldStyle;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Days of week");
                headerRow.CreateCell(2).SetCellValue(item.ClassDays);
                headerRow.CreateCell(6).SetCellValue("Check - In/Out Sheet");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Time");
                headerRow.CreateCell(2).SetCellValue(item.StrTime);
                headerRow.CreateCell(6).SetCellValue(item.SeasonName);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Instructor");
                headerRow.CreateCell(2).SetCellValue(item.Instructor);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Location");
                headerRow.CreateCell(2).SetCellValue(item.Location);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue("Report date");
                headerRow.CreateCell(2).SetCellValue(item.ReportDate);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;

                //line space between header and content
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);


                if (item.ProgramParticipants == null)
                {
                    continue;
                }

                //*** Content ***//
                // static header
                rowNumber++;
                var row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("#");
                row.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));

                row.CreateCell(1).SetCellValue("Student name");
                row.CreateCell(2).SetCellValue("Teacher name");
                row.CreateCell(3).SetCellValue("Grade");
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                var lastCell = 4;
                for (int i = 0; i < item.DatesOfDay.Count(); i++)
                {
                    row.CreateCell(lastCell + i).SetCellValue(item.DatesOfDay[i]);
                    row.GetCell(lastCell + i).CellStyle = greyStyleCenterAlignment;
                    row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber, lastCell + i, lastCell + i + 1));
                    lastCell += 1;
                }


                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                row.CreateCell(1).SetCellValue("Student phone");
                row.CreateCell(2).SetCellValue("Transport home");
                row.CreateCell(3).SetCellValue("Date of birth");
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                lastCell = 4;
                for (int i = 0; i < item.DatesOfDay.Count(); i++)
                {
                    row.CreateCell(lastCell + i).SetCellValue("IN");
                    row.GetCell(lastCell + i).CellStyle = greyStyleCenterAlignment;
                    row.CreateCell(lastCell + i + 1).SetCellValue("OUT");
                    row.GetCell(lastCell + i + 1).CellStyle = greyStyleCenterAlignment;
                    lastCell += 1;
                }


                //Content
                foreach (var participant in item.ProgramParticipants)
                {
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(participant.Number);
                    row.GetCell(0).CellStyle = alignmentCenterStyle;
                    row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));
                    row.GetCell(0).CellStyle = borderTopStyle;

                    row.CreateCell(1).SetCellValue(participant.ParticipantName);
                    row.CreateCell(2).SetCellValue(participant.TeacherName);
                    row.CreateCell(3).SetCellValue(participant.Grade);
                    //row.GetCell(3).CellStyle = alignmentCenterStyle;
                    //row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 3, 3));
                    row.GetCell(1).CellStyle = borderTopStyle;
                    row.GetCell(2).CellStyle = borderTopStyle;
                    row.GetCell(3).CellStyle = borderTopStyle;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(1).SetCellValue(participant.Phone);
                    row.CreateCell(2).SetCellValue(participant.TransportHome);
                    row.CreateCell(3).SetCellValue(participant.DoB);
                    lastCell = 4;
                    for (int i = 0; i < item.DatesOfDay.Count(); i++)
                    {
                        row.CreateCell(lastCell + i).SetCellValue("");
                        row.CreateCell(lastCell + i + 1).SetCellValue("");

                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber - 1, rowNumber, lastCell, lastCell));
                        lastCell++;
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber - 1, rowNumber, lastCell, lastCell));
                        lastCell++;
                    }
                }


                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);

            }


            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel",//MIME type of Excel files
             "ReportPrograms.xls");
        }

        [HttpGet]
        public virtual FileResult GenerateSignOutSheetPdf(string seasonDomain, long? scheduleId, DateTime date, string sortBy)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var school = Ioc.ClubBusiness.Get(clubId);
            var partner = new Club();
            var showSchoolLogo = true;

            if (school.PartnerId.HasValue)
            {
                partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);
            }
            else
            {
                partner = Ioc.ClubBusiness.Get(school.Id);
            }

            if (school.Setting != null)
            {
                showSchoolLogo = school.Setting.ShowSchoolLogo;
            }

            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelSignOutSheetReport(season.Id, school.Id, scheduleId, date, sortBy);

            var PdfModel = new SignOutSheetPdfSchoolViewModel()
            {
                SchoolLogo = showSchoolLogo ? _clubBusiness.GetClubLogoURL(school.Domain, school.Logo) : null,
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model,
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "SignOutSheetPdfSchoolReports", PdfModel);

            return File(file, "application/pdf", "Sign-Out sheet_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        public virtual ActionResult ProgramsListSignOutSheetReport(string seasonDomain, long? scheduleId, DateTime date, string sortBy)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var model = new List<SchoolProgramsSignOutReportViewModel>();

            model = CreateModelSignOutSheetReport(season.Id, clubId, scheduleId, date, sortBy);

            return JsonNet(model);
        }
        private List<ProgramParticipantListSchoolViewModel> GetAllParticipantSignOutSheet(Program program, DateTime date)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = new List<OrderItem>();
            var model = new List<ProgramParticipantListSchoolViewModel>();
            var num = 1;

            date = date.Date;
            var tommorow = date.AddDays(1);

            if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || program.TypeCategory == ProgramTypeCategory.Camp || program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                programOrderItems.AddRange(orderItemBusiness.GetProgramOrderItems(program.Id).Where(o => o.OrderSessions.Any(s => !s.ProgramSession.IsDeleted && s.ProgramSession.StartDateTime >= date && s.ProgramSession.StartDateTime < tommorow)).ToList());
            }
            else
            {
                programOrderItems.AddRange(orderItemBusiness.GetProgramOrderItems(program.Id).ToList().Where(o => o.ProgramSchedule.Sessions.ToList().Any(s => !s.IsDeleted && s.Start >= date && s.Start < tommorow)).ToList());
            }

            model = programOrderItems.ToList().Select(o => new ProgramParticipantListSchoolViewModel()
            {
                ParticipantName = string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)),
                TeacherName = o.JbForm != null ? formBusiness.GetTeacher(o.JbForm) == "" ? "-" : formBusiness.GetTeacher(o.JbForm) : "",
                Phone = "-",
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                Grade = formBusiness.GetPlayerGradeAsString(o.JbForm) != null ? formBusiness.GetPlayerGradeAsString(o.JbForm) == "Kindergarten" ? "K" : formBusiness.GetPlayerGradeAsString(o.JbForm) : "-",
                DoB = o.JbForm != null ? !formBusiness.GetParticipantDoB(o.JbForm).HasValue ? "-" : formBusiness.GetParticipantDoB(o.JbForm).Value.ToString(Constants.DefaultDateFormat) : ""
            }).OrderBy(n => n.ParticipantName).ToList();

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        private List<ProgramParticipantListSchoolViewModel> GetAllParticipantSignOutSheet(ProgramSchedule programSchedule, DateTime date)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var orderSessionBusiness = Ioc.OrderSessionBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = new List<OrderItem>();
            var model = new List<ProgramParticipantListSchoolViewModel>();
            var num = 1;

            date = date.Date;
            var tommorow = date.AddDays(1);

            if (programSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || programSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription)
            {

                programOrderItems.AddRange(Ioc.NewReportBusiness.GetReportOrderItemsWithEffectiveDate(programSchedule.Id, date));

            }
            else if (programSchedule.Program.TypeCategory == ProgramTypeCategory.Camp)
            {
                programOrderItems.AddRange(orderSessionBusiness.GetList().Where(o => o.ProgramSession.Charge.ProgramScheduleId.Value == programSchedule.Id &&
                       ((o.ProgramSession.Charge.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == o.OrderItem.Order.IsLive) &&
                       o.OrderItem.ItemStatus == OrderItemStatusCategories.completed &&
                       !o.ProgramSession.IsDeleted &&
                       o.ProgramSession.StartDateTime >= date && o.ProgramSession.StartDateTime < tommorow)
                   .Select(o => o.OrderItem)
                   .ToList());
            }
            else
            {
                programOrderItems.AddRange(orderItemBusiness.GetReportOrderItemsBySchedule(programSchedule.Id).ToList().Where(o => o.ProgramSchedule.Sessions.ToList().Any(s => !s.IsDeleted && s.Start >= date && s.Start < tommorow)).ToList());
            }

            model = programOrderItems.ToList().Select(o => new ProgramParticipantListSchoolViewModel()
            {
                ParticipantName = string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)),
                TeacherName = o.JbForm != null ? formBusiness.GetTeacher(o.JbForm) == "" ? "-" : formBusiness.GetTeacher(o.JbForm) : "",
                Phone = "-",
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                Grade = formBusiness.GetPlayerGradeAsString(o.JbForm) != null ? formBusiness.GetPlayerGradeAsString(o.JbForm, true) : "",
                GradeOrder = formBusiness.GetPlayerGrade(o.JbForm) != null ? formBusiness.GetPlayerGrade(o.JbForm).GetOrder().HasValue ? formBusiness.GetPlayerGrade(o.JbForm).GetOrder().Value : 0 : 0,
                DoB = o.JbForm != null ? !formBusiness.GetParticipantDoB(o.JbForm).HasValue ? "-" : formBusiness.GetParticipantDoB(o.JbForm).Value.ToString(Constants.DefaultDateFormat) : "",
                Mode = o.Mode,
                Classroom = o.JbForm != null ? formBusiness.GetPlayerHomeRoom(o.JbForm) == "" ? "" : formBusiness.GetPlayerHomeRoom(o.JbForm) : "",
            }).OrderBy(n => n.ParticipantName).ToList();

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }
        private List<ProgramParticipantListSchoolViewModel> GetPunchcardParticipantSignOutSheet(Program program, DateTime date)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var orderSessionBusiness = Ioc.OrderSessionBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = new List<OrderItem>();
            var model = new List<ProgramParticipantListSchoolViewModel>();
            var num = 1;

            programOrderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(program.Id).Where(o => o.Mode == OrderItemMode.PunchCard).DistinctBy(o => o.PlayerId).ToList();

            model = programOrderItems.ToList().Select(o => new ProgramParticipantListSchoolViewModel()
            {
                ParticipantName =
                    o.OrderSessions.Count(s => s.ProgramSession.StartDateTime.Date == date) == 2 ?
                         string.Format("{0}, {1} ***", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)) :
                    o.OrderSessions.Any(s => s.ProgramSession.StartDateTime.Date == date && s.ProgramSession.ProgramSchedule.ScheduleMode == TimeOfClassFormation.AM) ?
                        string.Format("{0}, {1} *", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)) :
                    o.OrderSessions.Any(s => s.ProgramSession.StartDateTime.Date == date && s.ProgramSession.ProgramSchedule.ScheduleMode == TimeOfClassFormation.PM) ?
                        string.Format("{0}, {1} **", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)) :
                    string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)),
                TeacherName = o.JbForm != null ? formBusiness.GetTeacher(o.JbForm) == "" ? "-" : formBusiness.GetTeacher(o.JbForm) : "",
                Phone = "-",
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                Grade = formBusiness.GetPlayerGradeAsString(o.JbForm) != null ? formBusiness.GetPlayerGradeAsString(o.JbForm, true) : "",
                GradeOrder = formBusiness.GetPlayerGrade(o.JbForm) != null ? formBusiness.GetPlayerGrade(o.JbForm).GetOrder().HasValue ? formBusiness.GetPlayerGrade(o.JbForm).GetOrder().Value : 0 : 0,
                DoB = o.JbForm != null ? !formBusiness.GetParticipantDoB(o.JbForm).HasValue ? "-" : formBusiness.GetParticipantDoB(o.JbForm).Value.ToString(Constants.DefaultDateFormat) : "",
                Mode = o.Mode,
                Classroom = o.JbForm != null ? formBusiness.GetPlayerHomeRoom(o.JbForm) == "" ? "" : formBusiness.GetPlayerHomeRoom(o.JbForm) : "",
            }).OrderBy(n => n.ParticipantName).ToList();

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        private List<SchoolProgramsSignOutReportViewModel> CreateModelSignOutSheetReport(long seasonId, int schoolId, long? scheduleId, DateTime date, string sortBy)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            var club = _clubBusiness.Get(schoolId);
            var reportDate = $"{Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString(Constants.DefaultDateTimeFormatAbrMonthWithAMPM)} ({club.TimeZone.ToString()})";
            date = date.Date;
            var tommorow = date.AddDays(1);
            if (scheduleId != null)
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted && p.ProgramSchedules.Select(s => s.Id).Contains(scheduleId.Value)).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted).ToList();
            }
            var model = new List<SchoolProgramsSignOutReportViewModel>();

            if (scheduleId.HasValue)
            {
                var schedule = programs.SelectMany(c => c.ProgramSchedules).SingleOrDefault(c => c.Id == scheduleId.Value);

                var allOrderItems = GetAllParticipantSignOutSheet(schedule, date);

                if (schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
                {
                    var punchCardOrderItems = GetPunchcardParticipantSignOutSheet(schedule.Program, date);

                    var comboSchedule = schedule.Program.ProgramSchedules.SingleOrDefault(p => p.ScheduleMode == TimeOfClassFormation.Both);

                    var allComboOrderItems = new List<ProgramParticipantListSchoolViewModel>();

                    if (comboSchedule != null)
                    {
                        allComboOrderItems = GetAllParticipantSignOutSheet(comboSchedule, date);

                        allOrderItems.AddRange(allComboOrderItems);
                    }

                    var beforeAfterOrderItemsModel = SetViewModelSignOutSheetReport(schedule, date, allOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}", reportDate);
                    model.Add(beforeAfterOrderItemsModel);

                    var dropinOrderItemsModel = SetViewModelSignOutSheetReport(schedule, date, allOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).ToList(), $"{schedule.Program.Name} - {schedule.Title} - Drop-in", reportDate);
                    model.Add(dropinOrderItemsModel);

                    var punchCardOrderItemsModel = SetViewModelSignOutSheetReport(schedule.Program, date, punchCardOrderItems, $"{schedule.Program.Name} - Punchcard", reportDate);
                    model.Add(punchCardOrderItemsModel);

                    if (model.Any())
                    {
                        model = sortDataSignOutSheetReport(model, sortBy);
                        foreach (var item in model)
                        {
                            if (item.ProgramParticipants != null)
                            {
                                setRowNumberSignOutSheetReport(item.ProgramParticipants);
                            }
                        }
                    }

                    return model;
                }

                var orderItemsModel = SetViewModelSignOutSheetReport(schedule, date, allOrderItems.ToList(), !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Program.Name} - {schedule.Title}" : schedule.Program.Name, reportDate);

                model.Add(orderItemsModel);

                if (model.Any())
                {
                    model = sortDataSignOutSheetReport(model, sortBy);
                    foreach (var item in model)
                    {
                        if (item.ProgramParticipants != null)
                        {
                            setRowNumberSignOutSheetReport(item.ProgramParticipants);
                        }
                    }
                }

                return model;
            }


            var beforeAfterPrograms = programs.Where(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).ToList();

            foreach (var program in beforeAfterPrograms)
            {
                var comboSchedule = program.ProgramSchedules.SingleOrDefault(p => p.ScheduleMode == TimeOfClassFormation.Both);

                var allComboOrderItems = new List<ProgramParticipantListSchoolViewModel>();

                if (comboSchedule != null)
                {
                    allComboOrderItems = GetAllParticipantSignOutSheet(comboSchedule, date);
                }

                foreach (var schedule in program.ProgramSchedules)
                {
                    if (schedule.ScheduleMode == TimeOfClassFormation.AM)
                    {
                        var allBeforeOrderItems = GetAllParticipantSignOutSheet(schedule, date);
                        allBeforeOrderItems.AddRange(allComboOrderItems);

                        var beforeAfterOrderItemsModel = SetViewModelSignOutSheetReport(schedule, date, allBeforeOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}", reportDate);
                        model.Add(beforeAfterOrderItemsModel);

                        var dropinOrderItemsModel = SetViewModelSignOutSheetReport(schedule, date, allBeforeOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).ToList(), $"{schedule.Program.Name} - {schedule.Title} - Drop-in", reportDate);
                        model.Add(dropinOrderItemsModel);
                    }
                    else if (schedule.ScheduleMode == TimeOfClassFormation.PM)
                    {
                        var allAfterOrderItems = GetAllParticipantSignOutSheet(schedule, date);
                        allAfterOrderItems.AddRange(allComboOrderItems);

                        var beforeAfterOrderItemsModel = SetViewModelSignOutSheetReport(schedule, date, allAfterOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}", reportDate);
                        model.Add(beforeAfterOrderItemsModel);

                        var dropinOrderItemsModel = SetViewModelSignOutSheetReport(schedule, date, allAfterOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).ToList(), $"{schedule.Program.Name} - {schedule.Title} - Drop-in", reportDate);
                        model.Add(dropinOrderItemsModel);
                    }


                }

                var punchCardOrderItems = GetPunchcardParticipantSignOutSheet(program, date);
                var punchCardOrderItemsModel = SetViewModelSignOutSheetReport(program, date, punchCardOrderItems, $"{program.Name} - Punchcard", reportDate);
                model.Add(punchCardOrderItemsModel);
            }

            programs.RemoveAll(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare);

            foreach (var program in programs)
            {
                foreach (var schedule in program.ProgramSchedules)
                {
                    var allOrderItems = GetAllParticipantSignOutSheet(schedule, date);

                    var orderItemsModel = SetViewModelSignOutSheetReport(schedule, date, allOrderItems.ToList(), !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Program.Name} - {schedule.Title}" : schedule.Program.Name, reportDate);

                    model.Add(orderItemsModel);
                }
            }

            if (model.Any())
            {
                model = sortDataSignOutSheetReport(model, sortBy);
                foreach (var item in model)
                {
                    if (item.ProgramParticipants != null)
                    {
                        setRowNumberSignOutSheetReport(item.ProgramParticipants);
                    }
                }
            }

            model = model.OrderBy(n => n.ClassName).ToList();

            return model;
        }

        private static void setRowNumberSignOutSheetReport(List<ProgramParticipantListSchoolViewModel> participants)
        {
            var num = 1;
            foreach (var participant in participants)
            {
                participant.Number = num;
                num++;
            }
        }

        private static List<SchoolProgramsSignOutReportViewModel> sortDataSignOutSheetReport(List<SchoolProgramsSignOutReportViewModel> model, string sortBy)
        {
            if (sortBy == "Participant")
            {
                for (int i = 0; i < model.Count; i++)
                {
                    if (model.ElementAt(i).ProgramParticipants != null)
                    {
                        model.ElementAt(i).ProgramParticipants = model.ElementAt(i).ProgramParticipants.OrderBy(p => p.ParticipantName.Replace("*", "")).ToList();
                    }
                }
            }
            else if (sortBy == "Grade")
            {
                for (int i = 0; i < model.Count; i++)
                {
                    if (model.ElementAt(i).ProgramParticipants != null)
                    {
                        model.ElementAt(i).ProgramParticipants = model.ElementAt(i).ProgramParticipants.OrderBy(p => p.GradeOrder).ToList();
                    }
                }
            }

            return model;
        }

        private static SchoolProgramsSignOutReportViewModel SetViewModelSignOutSheetReport(ProgramSchedule schedule, DateTime date, List<ProgramParticipantListSchoolViewModel> participants, string programName, string reportDate)
        {
            var programBusiness = Ioc.ProgramBusiness;

            return new SchoolProgramsSignOutReportViewModel()
            {
                ClassName = programName,
                ClassId = schedule.Id,
                ClassDays = string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(programBusiness.GetClassDays(schedule).Select(pr => pr.DayOfWeek).ToList())),
                StartTime = programBusiness.GetClassDays(schedule).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(schedule).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(schedule).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(schedule).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                Location = schedule.Program.Room,
                SchoolName = schedule.Program.Season.Club.Name,
                SeasonName = schedule.Program.Season.Domain,
                ProgramParticipants = participants.Count() > 0 ? participants.ToList() : null,
                Instructor = programBusiness.GetInstractors(schedule.Program),
                Date = date.ToString(Constants.DateTime_Comma),
                ReportDate = reportDate
            };
        }

        private static SchoolProgramsSignOutReportViewModel SetViewModelSignOutSheetReport(Program program, DateTime date, List<ProgramParticipantListSchoolViewModel> participants, string programName, string reportDate)
        {
            var programBusiness = Ioc.ProgramBusiness;

            return new SchoolProgramsSignOutReportViewModel()
            {
                ClassName = programName,
                ClassId = program.Id,
                ClassDays = string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(programBusiness.GetClassDays(program).Select(pr => pr.DayOfWeek).Distinct().ToList())),
                StartTime = programBusiness.GetClassDays(program).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(program).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(program).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(program).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                Location = program.Room,
                SchoolName = program.Season.Club.Name,
                SeasonName = program.Season.Domain,
                ProgramParticipants = participants.Count() > 0 ? participants.ToList() : null,
                Instructor = programBusiness.GetInstractors(program),
                Date = date.ToString(Constants.DateTime_Comma),
                ReportDate = reportDate
            };
        }

        [HttpGet]
        public virtual FileContentResult GenerateSignOutSheetExcel(string seasonDomain, long? scheduleId, DateTime date, string sortBy)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = CreateModelSignOutSheetReport(season.Id, clubId, scheduleId, date, sortBy);

            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();


            //Define Style
            ICellStyle alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.VerticalAlignment = VerticalAlignment.Center;

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var borderTopStyle = workbook.CreateCellStyle();
            borderTopStyle.BorderTop = BorderStyle.Thin;
            borderTopStyle.Alignment = HorizontalAlignment.Center;
            borderTopStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleLeftAlignment = workbook.CreateCellStyle();
            greyStyleLeftAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleLeftAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleLeftAlignment.Alignment = HorizontalAlignment.Left;
            greyStyleLeftAlignment.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleCenterAlignment = workbook.CreateCellStyle();
            greyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            greyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;
            //greyStyleCenterAlignment.BorderBottom = BorderStyle.MEDIUM;


            sheet.SetDefaultColumnStyle(0, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(1, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(2, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(3, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(4, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(5, alignmentCenterStyle);


            var rowNumber = 0;
            var itemNumber = 0;

            foreach (var item in model)
            {
                if (item.ProgramParticipants == null)
                {
                    continue;
                }

                //**** Header ****////
                var headerRow = sheet.CreateRow(rowNumber);

                if (itemNumber != 0)
                {
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                }
                itemNumber++;

                // set default column size and color
                headerRow.CreateCell(0).SetCellValue(" ");
                headerRow.CreateCell(1).SetCellValue(" ");
                headerRow.CreateCell(2).SetCellValue(" ");
                headerRow.CreateCell(3).SetCellValue(" ");
                headerRow.CreateCell(4).SetCellValue(" ");
                headerRow.CreateCell(5).SetCellValue(" ");
                headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(4).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 30 * 256);
                headerRow.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 10 * 256);
                headerRow.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                headerRow.GetCell(5).Sheet.SetColumnWidth(5, 10 * 256);

                //Set the column names in the header row
                headerRow.CreateCell(1).SetCellValue(item.ClassName);
                headerRow.CreateCell(6).SetCellValue(item.SchoolName);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 40 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
                headerRow.GetCell(1).CellStyle = greyBoldStyle;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Days of week: {item.ClassDays}");
                headerRow.CreateCell(6).SetCellValue("Check - In/Out Sheet");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Time: {item.StrTime}");
                headerRow.CreateCell(6).SetCellValue(item.SeasonName);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Instructor: {item.Instructor}");
                headerRow.CreateCell(6).SetCellValue(item.Date);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Location: {item.Location}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Report date: {item.ReportDate}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;

                //line space between header and content
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);



                //*** Content ***//
                // static header
                rowNumber++;
                var row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("#");
                row.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));

                row.CreateCell(1).SetCellValue("Participant");
                row.CreateCell(2).SetCellValue("Teacher name");
                row.CreateCell(3).SetCellValue("Grade");
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                var lastCell = 4;
                for (int i = 0; i < 2; i++)
                {
                    row.CreateCell(lastCell + i).SetCellValue("");
                    row.GetCell(lastCell + i).CellStyle = greyStyleCenterAlignment;
                    row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber, lastCell + i, lastCell + i + 1));
                    lastCell += 1;
                }


                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                row.CreateCell(1).SetCellValue("Phone");
                row.CreateCell(2).SetCellValue("Transport home");
                row.CreateCell(3).SetCellValue("DOB");
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                lastCell = 4;
                for (int i = 0; i < 2; i++)
                {
                    if (i == 0)
                    {
                        row.CreateCell(lastCell + i).SetCellValue("Time in");
                        row.GetCell(lastCell + i).Sheet.SetColumnWidth(lastCell + i, 10 * 256);
                    }
                    else
                    {
                        row.CreateCell(lastCell + i).SetCellValue("Time out");
                        row.GetCell(lastCell + i).Sheet.SetColumnWidth(lastCell + i, 10 * 256);
                    }
                    row.GetCell(lastCell + i).CellStyle = greyStyleCenterAlignment;
                    row.CreateCell(lastCell + i + 1).SetCellValue("Signature");
                    row.GetCell(lastCell + i + 1).CellStyle = greyStyleCenterAlignment;
                    row.GetCell(lastCell + i + 1).Sheet.SetColumnWidth(lastCell + i + 1, 20 * 256);
                    lastCell += 1;
                }


                //Content
                if (item.ProgramParticipants != null)
                {
                    foreach (var participant in item.ProgramParticipants)
                    {
                        rowNumber++;
                        row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(participant.Number);
                        row.GetCell(0).CellStyle = alignmentCenterStyle;
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));
                        row.GetCell(0).CellStyle = borderTopStyle;

                        row.CreateCell(1).SetCellValue(participant.ParticipantName);
                        row.CreateCell(2).SetCellValue(participant.TeacherName);
                        row.CreateCell(3).SetCellValue(participant.Grade);
                        row.GetCell(1).CellStyle = borderTopStyle;
                        row.GetCell(2).CellStyle = borderTopStyle;
                        row.GetCell(3).CellStyle = borderTopStyle;

                        rowNumber++;
                        row = sheet.CreateRow(rowNumber);
                        row.CreateCell(1).SetCellValue(participant.Phone);
                        row.CreateCell(2).SetCellValue(participant.TransportHome);
                        row.CreateCell(3).SetCellValue(participant.DoB);
                        lastCell = 4;
                        for (int i = 0; i < 2; i++)
                        {
                            row.CreateCell(lastCell + i).SetCellValue("");
                            row.CreateCell(lastCell + i + 1).SetCellValue("");

                            row.Sheet.AddMergedRegion(
                                new NPOI.SS.Util.CellRangeAddress(rowNumber - 1, rowNumber, lastCell, lastCell));
                            lastCell++;
                            row.Sheet.AddMergedRegion(
                                new NPOI.SS.Util.CellRangeAddress(rowNumber - 1, rowNumber, lastCell, lastCell));
                            lastCell++;
                        }
                    }
                }
                else
                {
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(string.Empty);
                    row.GetCell(0).CellStyle = alignmentCenterStyle;
                    row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));
                    row.GetCell(0).CellStyle = borderTopStyle;

                    row.CreateCell(1).SetCellValue(string.Empty);
                    row.CreateCell(2).SetCellValue(string.Empty);
                    row.CreateCell(3).SetCellValue(string.Empty);
                    row.GetCell(1).CellStyle = borderTopStyle;
                    row.GetCell(2).CellStyle = borderTopStyle;
                    row.GetCell(3).CellStyle = borderTopStyle;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(1).SetCellValue(string.Empty);
                    row.CreateCell(2).SetCellValue(string.Empty);
                    row.CreateCell(3).SetCellValue(string.Empty);
                    lastCell = 4;
                    for (int i = 0; i < 2; i++)
                    {
                        row.CreateCell(lastCell + i).SetCellValue("");
                        row.CreateCell(lastCell + i + 1).SetCellValue("");

                        row.Sheet.AddMergedRegion(
                            new NPOI.SS.Util.CellRangeAddress(rowNumber - 1, rowNumber, lastCell, lastCell));
                        lastCell++;
                        row.Sheet.AddMergedRegion(
                            new NPOI.SS.Util.CellRangeAddress(rowNumber - 1, rowNumber, lastCell, lastCell));
                        lastCell++;
                    }
                }

                if (item.ClassName.Contains("Punchcard") && item.ProgramParticipants != null)
                {
                    rowNumber++;
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("* Today attending the first session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("** Today attending the secnod session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("*** Today attending the both session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;
                }

                if (item.ClassName.Contains("Punchcard") && item.ProgramParticipants != null)
                {
                    rowNumber++;
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("* Today attending the first session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("** Today attending the secnod session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("*** Today attending the both session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;
                }


                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);

            }


            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel",//MIME type of Excel files
             "ReportPrograms.xls");
        }


        [HttpGet]
        public virtual FileResult GenerateSignOutSheetV2Pdf(string seasonDomain, long? scheduleId, DateTime date, string sortBy)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var school = Ioc.ClubBusiness.Get(clubId);
            var partner = new Club();
            var showSchoolLogo = true;

            if (school.PartnerId.HasValue)
            {
                partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);
            }
            else
            {
                partner = Ioc.ClubBusiness.Get(school.Id);
            }

            if (school.Setting != null)
            {
                showSchoolLogo = school.Setting.ShowSchoolLogo;
            }

            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelSignOutSheetReport(season.Id, school.Id, scheduleId, date, sortBy);

            var PdfModel = new SignOutSheetPdfSchoolViewModel()
            {
                SchoolLogo = showSchoolLogo ? _clubBusiness.GetClubLogoURL(school.Domain, school.Logo) : null,
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "SignOutSheetPdfSchoolV2Reports", PdfModel);

            return File(file, "application/pdf", "Sign-Out sheet_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        [HttpGet]
        public virtual FileContentResult GenerateSignOutSheetV2Excel(string seasonDomain, long? scheduleId, DateTime date, string sortBy)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = CreateModelSignOutSheetReport(season.Id, clubId, scheduleId, date, sortBy);

            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();


            //Define Style
            ICellStyle alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.VerticalAlignment = VerticalAlignment.Center;

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var borderTopStyle = workbook.CreateCellStyle();
            borderTopStyle.BorderTop = BorderStyle.Thin;
            borderTopStyle.Alignment = HorizontalAlignment.Center;
            borderTopStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleLeftAlignment = workbook.CreateCellStyle();
            greyStyleLeftAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleLeftAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleLeftAlignment.Alignment = HorizontalAlignment.Left;
            greyStyleLeftAlignment.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleCenterAlignment = workbook.CreateCellStyle();
            greyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            greyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;
            //greyStyleCenterAlignment.BorderBottom = BorderStyle.MEDIUM;


            sheet.SetDefaultColumnStyle(0, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(1, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(2, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(3, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(4, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(5, alignmentCenterStyle);


            var rowNumber = 0;
            var itemNumber = 0;

            foreach (var item in model)
            {
                if (item.ProgramParticipants == null)
                {
                    continue;
                }

                //**** Header ****////
                var headerRow = sheet.CreateRow(rowNumber);

                if (itemNumber != 0)
                {
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                }
                itemNumber++;

                // set default column size and color
                headerRow.CreateCell(0).SetCellValue(" ");
                headerRow.CreateCell(1).SetCellValue(" ");
                headerRow.CreateCell(2).SetCellValue(" ");
                headerRow.CreateCell(3).SetCellValue(" ");
                headerRow.CreateCell(4).SetCellValue(" ");
                headerRow.CreateCell(5).SetCellValue(" ");
                headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(4).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 30 * 256);
                headerRow.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 10 * 256);
                headerRow.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                headerRow.GetCell(5).Sheet.SetColumnWidth(5, 10 * 256);

                //Set the column names in the header row
                headerRow.CreateCell(1).SetCellValue(item.ClassName);
                headerRow.CreateCell(6).SetCellValue(item.SchoolName);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 40 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
                headerRow.GetCell(1).CellStyle = greyBoldStyle;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Days of week: {item.ClassDays}");
                headerRow.CreateCell(6).SetCellValue("Check - In/Out Sheet");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Time: {item.StrTime}");
                headerRow.CreateCell(6).SetCellValue(item.Date);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Report date: {item.ReportDate}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;

                //line space between header and content
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);



                //*** Content ***//
                // static header
                rowNumber++;
                var row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("#");
                row.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);

                row.CreateCell(1).SetCellValue("Participant");
                row.CreateCell(2).SetCellValue("Classroom");
                row.CreateCell(3).SetCellValue("Grade");
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                var lastCell = 4;

                for (int i = 0; i < 2; i++)
                {
                    if (i == 0)
                    {
                        row.CreateCell(lastCell + i).SetCellValue("Time in");
                        row.GetCell(lastCell + i).Sheet.SetColumnWidth(lastCell + i, 10 * 256);
                    }
                    else
                    {
                        row.CreateCell(lastCell + i).SetCellValue("Time out");
                        row.GetCell(lastCell + i).Sheet.SetColumnWidth(lastCell + i, 10 * 256);
                    }
                    row.GetCell(lastCell + i).CellStyle = greyStyleCenterAlignment;
                    row.CreateCell(lastCell + i + 1).SetCellValue("Signature");
                    row.GetCell(lastCell + i + 1).CellStyle = greyStyleCenterAlignment;
                    row.GetCell(lastCell + i + 1).Sheet.SetColumnWidth(lastCell + i + 1, 20 * 256);
                    lastCell += 1;
                }


                //Content
                if (item.ProgramParticipants != null)
                {
                    foreach (var participant in item.ProgramParticipants)
                    {
                        rowNumber++;
                        row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(participant.Number);
                        row.GetCell(0).CellStyle = alignmentCenterStyle;
                        row.GetCell(0).CellStyle = borderTopStyle;

                        row.CreateCell(1).SetCellValue(participant.ParticipantName);
                        row.CreateCell(2).SetCellValue(participant.Classroom);
                        row.CreateCell(3).SetCellValue(participant.Grade);
                        row.GetCell(1).CellStyle = borderTopStyle;
                        row.GetCell(2).CellStyle = borderTopStyle;
                        row.GetCell(3).CellStyle = borderTopStyle;

                        lastCell = 4;
                        for (int i = 0; i < 2; i++)
                        {
                            row.CreateCell(lastCell + i).SetCellValue("");
                            row.CreateCell(lastCell + i + 1).SetCellValue("");
                        }
                    }
                }
                else
                {
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(string.Empty);
                    row.GetCell(0).CellStyle = alignmentCenterStyle;
                    row.GetCell(0).CellStyle = borderTopStyle;

                    row.CreateCell(1).SetCellValue(string.Empty);
                    row.CreateCell(2).SetCellValue(string.Empty);
                    row.CreateCell(3).SetCellValue(string.Empty);
                    row.GetCell(1).CellStyle = borderTopStyle;
                    row.GetCell(2).CellStyle = borderTopStyle;
                    row.GetCell(3).CellStyle = borderTopStyle;

                    lastCell = 4;
                    for (int i = 0; i < 2; i++)
                    {
                        row.CreateCell(lastCell + i).SetCellValue("");
                        row.CreateCell(lastCell + i + 1).SetCellValue("");
                    }
                }

                if (item.ClassName.Contains("Punchcard") && item.ProgramParticipants != null)
                {
                    rowNumber++;
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("* Today attending the first session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("** Today attending the secnod session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("*** Today attending the both session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;
                }

                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);

            }


            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel",//MIME type of Excel files
             "ReportPrograms.xls");
        }



        public virtual ActionResult ProgramsListSignOutSheetReportV3(string seasonDomain, List<long> scheduleIds, DateTime date, string sortBy)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var model = new List<SchoolProgramsSignOutReportViewModel>();

            model = CreateModelSignOutSheetReportV3(season.Id, clubId, scheduleIds, date, sortBy);

            return JsonNet(model);
        }

        private List<SchoolProgramsSignOutReportViewModel> CreateModelSignOutSheetReportV3(long seasonId, int schoolId, List<long> scheduleIds, DateTime date, string sortBy)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var club = _clubBusiness.Get(schoolId);
            var reportDate = $"{Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString(Constants.DefaultDateTimeFormatAbrMonthWithAMPM)} ({club.TimeZone.ToString()})";
            date = date.Date;
            var tommorow = date.AddDays(1);

            var model = new List<SchoolProgramsSignOutReportViewModel>();

            var allOrderItems = new List<ProgramParticipantListSchoolViewModel>();
            var orderItemsModel = new SchoolProgramsSignOutReportViewModel();

            var programs = Ioc.ProgramBusiness.GetList().Where(p => p.ProgramSchedules.Any(s => scheduleIds.Contains(s.Id)));

            var schedules = programs.SelectMany(p => p.ProgramSchedules).Where(s => scheduleIds.Contains(s.Id));

            foreach (var schedule in schedules)
            {
                allOrderItems.AddRange(GetAllParticipantSignOutSheet(schedule, date));

                orderItemsModel = SetViewModelSignOutSheetReportV3(schedule.Program, date, allOrderItems.ToList(), string.Join(", ", programs.Select(p => p.Name).ToList()), reportDate);
            }

            model.Add(orderItemsModel);

            if (model.Any())
            {
                model = sortDataSignOutSheetReport(model, sortBy);
                foreach (var item in model)
                {
                    if (item.ProgramParticipants != null)
                    {
                        setRowNumberSignOutSheetReport(item.ProgramParticipants);
                    }
                }
            }

            model = model.OrderBy(n => n.ClassName).ToList();

            return model;
        }

        private static SchoolProgramsSignOutReportViewModel SetViewModelSignOutSheetReportV3(Program program, DateTime date, List<ProgramParticipantListSchoolViewModel> participants, string programName, string reportDate)
        {
            var programBusiness = Ioc.ProgramBusiness;

            return new SchoolProgramsSignOutReportViewModel()
            {
                ClassName = programName,
                SchoolName = program.Season.Club.Name,
                ProgramParticipants = participants.Count() > 0 ? participants.ToList() : null,
                Date = date.ToString(Constants.DateTime_Comma),
                ReportDate = reportDate
            };
        }

        [HttpPost]
        public virtual FileResult GenerateSignOutSheetV3Pdf(string seasonDomain, List<long> scheduleIds, DateTime date, string sortBy)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var school = Ioc.ClubBusiness.Get(clubId);
            var partner = new Club();
            var showSchoolLogo = true;

            if (school.PartnerId.HasValue)
            {
                partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);
            }
            else
            {
                partner = Ioc.ClubBusiness.Get(school.Id);
            }

            if (school.Setting != null)
            {
                showSchoolLogo = school.Setting.ShowSchoolLogo;
            }

            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelSignOutSheetReportV3(season.Id, school.Id, scheduleIds, date, sortBy);

            var PdfModel = new SignOutSheetPdfSchoolViewModel()
            {
                SchoolLogo = showSchoolLogo ? _clubBusiness.GetClubLogoURL(school.Domain, school.Logo) : null,
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "SignOutSheetPdfSchoolV3Reports", PdfModel);

            return File(file, "application/pdf", "Sign-Out sheet_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        public virtual FileContentResult GenerateSignOutSheetV3Excel(string seasonDomain, List<long> scheduleIds, DateTime date, string sortBy)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = CreateModelSignOutSheetReportV3(season.Id, clubId, scheduleIds, date, sortBy);

            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();


            //Define Style
            ICellStyle alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.VerticalAlignment = VerticalAlignment.Center;

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var borderTopStyle = workbook.CreateCellStyle();
            borderTopStyle.BorderTop = BorderStyle.Thin;
            borderTopStyle.Alignment = HorizontalAlignment.Center;
            borderTopStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleLeftAlignment = workbook.CreateCellStyle();
            greyStyleLeftAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleLeftAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleLeftAlignment.Alignment = HorizontalAlignment.Left;
            greyStyleLeftAlignment.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleCenterAlignment = workbook.CreateCellStyle();
            greyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            greyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;
            //greyStyleCenterAlignment.BorderBottom = BorderStyle.MEDIUM;


            sheet.SetDefaultColumnStyle(0, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(1, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(2, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(3, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(4, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(5, alignmentCenterStyle);


            var rowNumber = 0;
            var itemNumber = 0;

            foreach (var item in model)
            {
                if (item.ProgramParticipants == null)
                {
                    //Set empty page

                    //**** Header ****////
                    var headerRow2 = sheet.CreateRow(rowNumber);

                    if (itemNumber != 0)
                    {
                        rowNumber++;
                        headerRow2 = sheet.CreateRow(rowNumber);
                        rowNumber++;
                        headerRow2 = sheet.CreateRow(rowNumber);
                        rowNumber++;
                        headerRow2 = sheet.CreateRow(rowNumber);
                        rowNumber++;
                        headerRow2 = sheet.CreateRow(rowNumber);
                        rowNumber++;
                        headerRow2 = sheet.CreateRow(rowNumber);
                    }
                    itemNumber++;

                    // set default column size and color
                    headerRow2.CreateCell(0).SetCellValue(" ");
                    headerRow2.CreateCell(1).SetCellValue(" ");
                    headerRow2.CreateCell(2).SetCellValue(" ");
                    headerRow2.CreateCell(3).SetCellValue(" ");
                    headerRow2.CreateCell(4).SetCellValue(" ");
                    headerRow2.CreateCell(5).SetCellValue(" ");
                    headerRow2.GetCell(0).CellStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(1).CellStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(2).CellStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(3).CellStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(4).CellStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(5).CellStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                    headerRow2.GetCell(1).Sheet.SetColumnWidth(1, 30 * 256);
                    headerRow2.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                    headerRow2.GetCell(3).Sheet.SetColumnWidth(3, 10 * 256);
                    headerRow2.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                    headerRow2.GetCell(5).Sheet.SetColumnWidth(5, 10 * 256);

                    //Set the column names in the header row
                    headerRow2.CreateCell(1).SetCellValue("Unexpected drop-in");
                    headerRow2.CreateCell(6).SetCellValue(item.SchoolName);
                    headerRow2.RowStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(1).Sheet.SetColumnWidth(1, 40 * 256);
                    headerRow2.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
                    headerRow2.GetCell(1).CellStyle = greyBoldStyle;
                    headerRow2.GetCell(6).CellStyle = greyBoldStyle;

                    rowNumber++;
                    headerRow2 = sheet.CreateRow(rowNumber);
                    headerRow2.CreateCell(1).SetCellValue($"Report date: {item.ReportDate}");
                    headerRow2.CreateCell(6).SetCellValue("Check - In/Out Sheet");
                    headerRow2.RowStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(1).CellStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(6).CellStyle = greyBoldStyle;


                    rowNumber++;
                    headerRow2 = sheet.CreateRow(rowNumber);
                    headerRow2.CreateCell(1).SetCellValue(string.Empty);
                    headerRow2.CreateCell(6).SetCellValue(item.Date);
                    headerRow2.RowStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(1).CellStyle = greyStyleLeftAlignment;
                    headerRow2.GetCell(6).CellStyle = greyBoldStyle;

                    //line space between header and content
                    rowNumber++;
                    headerRow2 = sheet.CreateRow(rowNumber);



                    //*** Content ***//
                    // static header
                    rowNumber++;
                    var row2 = sheet.CreateRow(rowNumber);
                    row2.CreateCell(0).SetCellValue("#");
                    row2.GetCell(0).CellStyle = greyStyleCenterAlignment;
                    row2.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);

                    row2.CreateCell(1).SetCellValue("Participant");
                    row2.CreateCell(2).SetCellValue("Parent name");
                    row2.CreateCell(3).SetCellValue("Grade");
                    row2.CreateCell(4).SetCellValue("Phone");
                    row2.CreateCell(5).SetCellValue("Email");
                    row2.CreateCell(6).SetCellValue("Signature");
                    row2.CreateCell(7).SetCellValue("Time out");
                    row2.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                    row2.GetCell(5).Sheet.SetColumnWidth(5, 20 * 256);
                    row2.GetCell(6).Sheet.SetColumnWidth(6, 10 * 256);
                    row2.GetCell(1).CellStyle = greyStyleCenterAlignment;
                    row2.GetCell(2).CellStyle = greyStyleCenterAlignment;
                    row2.GetCell(3).CellStyle = greyStyleCenterAlignment;
                    row2.GetCell(4).CellStyle = greyStyleCenterAlignment;
                    row2.GetCell(5).CellStyle = greyStyleCenterAlignment;
                    row2.GetCell(6).CellStyle = greyStyleCenterAlignment;

                    break;
                }

                //**** Header ****////
                var headerRow = sheet.CreateRow(rowNumber);

                if (itemNumber != 0)
                {
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                }
                itemNumber++;

                // set default column size and color
                headerRow.CreateCell(0).SetCellValue(" ");
                headerRow.CreateCell(1).SetCellValue(" ");
                headerRow.CreateCell(2).SetCellValue(" ");
                headerRow.CreateCell(3).SetCellValue(" ");
                headerRow.CreateCell(4).SetCellValue(" ");
                headerRow.CreateCell(5).SetCellValue(" ");
                headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(4).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 30 * 256);
                headerRow.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 10 * 256);
                headerRow.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                headerRow.GetCell(5).Sheet.SetColumnWidth(5, 10 * 256);

                //Set the column names in the header row
                headerRow.CreateCell(1).SetCellValue(item.ClassName);
                headerRow.CreateCell(6).SetCellValue(item.SchoolName);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 40 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
                headerRow.GetCell(1).CellStyle = greyBoldStyle;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Report date: {item.ReportDate}");
                headerRow.CreateCell(6).SetCellValue("Check - In/Out Sheet");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue(string.Empty);
                headerRow.CreateCell(6).SetCellValue(item.Date);
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyBoldStyle;

                //line space between header and content
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);



                //*** Content ***//
                // static header
                rowNumber++;
                var row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("#");
                row.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);

                row.CreateCell(1).SetCellValue("Participant");
                row.CreateCell(2).SetCellValue("Classroom");
                row.CreateCell(3).SetCellValue("Grade");
                row.CreateCell(4).SetCellValue("Present");
                row.CreateCell(5).SetCellValue("Signature");
                row.CreateCell(6).SetCellValue("Time out");
                row.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                row.GetCell(5).Sheet.SetColumnWidth(5, 20 * 256);
                row.GetCell(6).Sheet.SetColumnWidth(6, 10 * 256);
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                row.GetCell(4).CellStyle = greyStyleCenterAlignment;
                row.GetCell(5).CellStyle = greyStyleCenterAlignment;
                row.GetCell(6).CellStyle = greyStyleCenterAlignment;

                var lastCell = 6;


                //Content
                if (item.ProgramParticipants != null)
                {
                    foreach (var participant in item.ProgramParticipants)
                    {
                        rowNumber++;
                        row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(participant.Number);
                        row.GetCell(0).CellStyle = alignmentCenterStyle;
                        row.GetCell(0).CellStyle = borderTopStyle;

                        row.CreateCell(1).SetCellValue(participant.ParticipantName);
                        row.CreateCell(2).SetCellValue(participant.Classroom);
                        row.CreateCell(3).SetCellValue(participant.Grade);
                        row.GetCell(1).CellStyle = borderTopStyle;
                        row.GetCell(2).CellStyle = borderTopStyle;
                        row.GetCell(3).CellStyle = borderTopStyle;

                        lastCell = 4;
                        for (int i = 0; i < 2; i++)
                        {
                            row.CreateCell(lastCell + i).SetCellValue("");
                            row.CreateCell(lastCell + i + 1).SetCellValue("");
                        }
                    }
                }
                else
                {
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(string.Empty);
                    row.GetCell(0).CellStyle = alignmentCenterStyle;
                    row.GetCell(0).CellStyle = borderTopStyle;

                    row.CreateCell(1).SetCellValue(string.Empty);
                    row.CreateCell(2).SetCellValue(string.Empty);
                    row.CreateCell(3).SetCellValue(string.Empty);
                    row.GetCell(1).CellStyle = borderTopStyle;
                    row.GetCell(2).CellStyle = borderTopStyle;
                    row.GetCell(3).CellStyle = borderTopStyle;

                    lastCell = 4;
                    for (int i = 0; i < 2; i++)
                    {
                        row.CreateCell(lastCell + i).SetCellValue("");
                        row.CreateCell(lastCell + i + 1).SetCellValue("");
                    }
                }






                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);




                //Set empty page

                //**** Header ****////
                var headerRow1 = sheet.CreateRow(rowNumber);

                if (itemNumber != 0)
                {
                    rowNumber++;
                    headerRow1 = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow1 = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow1 = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow1 = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow1 = sheet.CreateRow(rowNumber);
                }
                itemNumber++;

                // set default column size and color
                headerRow1.CreateCell(0).SetCellValue(" ");
                headerRow1.CreateCell(1).SetCellValue(" ");
                headerRow1.CreateCell(2).SetCellValue(" ");
                headerRow1.CreateCell(3).SetCellValue(" ");
                headerRow1.CreateCell(4).SetCellValue(" ");
                headerRow1.CreateCell(5).SetCellValue(" ");
                headerRow1.GetCell(0).CellStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(4).CellStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                headerRow1.GetCell(1).Sheet.SetColumnWidth(1, 30 * 256);
                headerRow1.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                headerRow1.GetCell(3).Sheet.SetColumnWidth(3, 10 * 256);
                headerRow1.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                headerRow1.GetCell(5).Sheet.SetColumnWidth(5, 10 * 256);

                //Set the column names in the header row
                headerRow1.CreateCell(1).SetCellValue("Unexpected drop-in");
                headerRow1.CreateCell(6).SetCellValue(item.SchoolName);
                headerRow1.RowStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(1).Sheet.SetColumnWidth(1, 40 * 256);
                headerRow1.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
                headerRow1.GetCell(1).CellStyle = greyBoldStyle;
                headerRow1.GetCell(6).CellStyle = greyBoldStyle;

                rowNumber++;
                headerRow1 = sheet.CreateRow(rowNumber);
                headerRow1.CreateCell(1).SetCellValue($"Report date: {item.ReportDate}");
                headerRow1.CreateCell(6).SetCellValue("Check - In/Out Sheet");
                headerRow1.RowStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(6).CellStyle = greyBoldStyle;


                rowNumber++;
                headerRow1 = sheet.CreateRow(rowNumber);
                headerRow1.CreateCell(1).SetCellValue(string.Empty);
                headerRow1.CreateCell(6).SetCellValue(item.Date);
                headerRow1.RowStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow1.GetCell(6).CellStyle = greyBoldStyle;

                //line space between header and content
                rowNumber++;
                headerRow1 = sheet.CreateRow(rowNumber);



                //*** Content ***//
                // static header
                rowNumber++;
                var row1 = sheet.CreateRow(rowNumber);
                row1.CreateCell(0).SetCellValue("#");
                row1.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row1.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);

                row1.CreateCell(1).SetCellValue("Participant");
                row1.CreateCell(2).SetCellValue("Parent name");
                row1.CreateCell(3).SetCellValue("Grade");
                row1.CreateCell(4).SetCellValue("Phone");
                row1.CreateCell(5).SetCellValue("Email");
                row1.CreateCell(6).SetCellValue("Signature");
                row1.CreateCell(7).SetCellValue("Time out");
                row1.GetCell(4).Sheet.SetColumnWidth(4, 10 * 256);
                row1.GetCell(5).Sheet.SetColumnWidth(5, 20 * 256);
                row1.GetCell(6).Sheet.SetColumnWidth(6, 10 * 256);
                row1.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row1.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row1.GetCell(3).CellStyle = greyStyleCenterAlignment;
                row1.GetCell(4).CellStyle = greyStyleCenterAlignment;
                row1.GetCell(5).CellStyle = greyStyleCenterAlignment;
                row1.GetCell(6).CellStyle = greyStyleCenterAlignment;



            }


            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel",//MIME type of Excel files
             "ReportPrograms.xls");
        }



        public virtual ActionResult ProgramsListSignOutSheetReportV4(string seasonDomain, long? scheduleId, DateTime date, string sortBy)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var model = new List<SchoolProgramsSignOutReportViewModel>();

            model = CreateModelSignOutSheetReportV4(season.Id, clubId, scheduleId, date, sortBy);

            return JsonNet(model);
        }

        private List<SchoolProgramsSignOutReportViewModel> CreateModelSignOutSheetReportV4(long seasonId, int schoolId, long? scheduleId, DateTime date, string sortBy)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            var club = _clubBusiness.Get(schoolId);
            var reportDate = $"{Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString(Constants.DefaultDateTimeFormatAbrMonthWithAMPM)} ({club.TimeZone.ToString()})";
            date = date.Date;
            var tommorow = date.AddDays(1);
            if (scheduleId != null)
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted && p.ProgramSchedules.Select(s => s.Id).Contains(scheduleId.Value)).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(seasonId).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted).ToList();
            }
            var model = new List<SchoolProgramsSignOutReportViewModel>();

            if (scheduleId.HasValue)
            {
                var schedule = programs.SelectMany(c => c.ProgramSchedules).SingleOrDefault(c => c.Id == scheduleId.Value);

                var allOrderItems = GetAllParticipantSignOutSheetV4(schedule, date);

                if (schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
                {
                    var punchCardOrderItems = GetPunchcardParticipantSignOutSheetV4(schedule.Program, date);

                    var comboSchedule = schedule.Program.ProgramSchedules.SingleOrDefault(p => p.ScheduleMode == TimeOfClassFormation.Both);

                    var allComboOrderItems = new List<ProgramParticipantListSchoolViewModel>();

                    if (comboSchedule != null)
                    {
                        allComboOrderItems = GetAllParticipantSignOutSheetV4(comboSchedule, date);

                        allOrderItems.AddRange(allComboOrderItems);
                    }

                    var beforeAfterOrderItemsModel = SetViewModelSignOutSheetReportV4(schedule, date, allOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}", reportDate);
                    model.Add(beforeAfterOrderItemsModel);

                    var dropinOrderItemsModel = SetViewModelSignOutSheetReportV4(schedule, date, allOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).ToList(), $"{schedule.Program.Name} - {schedule.Title} - Drop-in", reportDate);
                    model.Add(dropinOrderItemsModel);

                    var punchCardOrderItemsModel = SetViewModelSignOutSheetReportV4(schedule.Program, date, punchCardOrderItems, $"{schedule.Program.Name} - Punchcard", reportDate);
                    model.Add(punchCardOrderItemsModel);

                    if (model.Any())
                    {
                        model = sortDataSignOutSheetReport(model, sortBy);
                        foreach (var item in model)
                        {
                            if (item.ProgramParticipants != null)
                            {
                                setRowNumberSignOutSheetReport(item.ProgramParticipants);
                            }
                        }
                    }

                    return model;
                }

                var orderItemsModel = SetViewModelSignOutSheetReportV4(schedule, date, allOrderItems.ToList(), !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Program.Name} - {schedule.Title}" : schedule.Program.Name, reportDate);

                model.Add(orderItemsModel);

                if (model.Any())
                {
                    model = sortDataSignOutSheetReport(model, sortBy);
                    foreach (var item in model)
                    {
                        if (item.ProgramParticipants != null)
                        {
                            setRowNumberSignOutSheetReport(item.ProgramParticipants);
                        }
                    }
                }

                return model;
            }


            var beforeAfterPrograms = programs.Where(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).ToList();

            foreach (var program in beforeAfterPrograms)
            {
                var comboSchedule = program.ProgramSchedules.SingleOrDefault(p => p.ScheduleMode == TimeOfClassFormation.Both);

                var allComboOrderItems = new List<ProgramParticipantListSchoolViewModel>();

                if (comboSchedule != null)
                {
                    allComboOrderItems = GetAllParticipantSignOutSheetV4(comboSchedule, date);
                }

                foreach (var schedule in program.ProgramSchedules)
                {
                    if (schedule.ScheduleMode == TimeOfClassFormation.AM)
                    {
                        var allBeforeOrderItems = GetAllParticipantSignOutSheetV4(schedule, date);
                        allBeforeOrderItems.AddRange(allComboOrderItems);

                        var beforeAfterOrderItemsModel = SetViewModelSignOutSheetReportV4(schedule, date, allBeforeOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}", reportDate);
                        model.Add(beforeAfterOrderItemsModel);

                        var dropinOrderItemsModel = SetViewModelSignOutSheetReportV4(schedule, date, allBeforeOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).ToList(), $"{schedule.Program.Name} - {schedule.Title} - Drop-in", reportDate);
                        model.Add(dropinOrderItemsModel);
                    }
                    else if (schedule.ScheduleMode == TimeOfClassFormation.PM)
                    {
                        var allAfterOrderItems = GetAllParticipantSignOutSheetV4(schedule, date);
                        allAfterOrderItems.AddRange(allComboOrderItems);

                        var beforeAfterOrderItemsModel = SetViewModelSignOutSheetReportV4(schedule, date, allAfterOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}", reportDate);
                        model.Add(beforeAfterOrderItemsModel);

                        var dropinOrderItemsModel = SetViewModelSignOutSheetReportV4(schedule, date, allAfterOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).ToList(), $"{schedule.Program.Name} - {schedule.Title} - Drop-in", reportDate);
                        model.Add(dropinOrderItemsModel);
                    }


                }

                var punchCardOrderItems = GetPunchcardParticipantSignOutSheetV4(program, date);
                var punchCardOrderItemsModel = SetViewModelSignOutSheetReportV4(program, date, punchCardOrderItems, $"{program.Name} - Punchcard", reportDate);
                model.Add(punchCardOrderItemsModel);
            }

            programs.RemoveAll(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare);

            foreach (var program in programs)
            {
                foreach (var schedule in program.ProgramSchedules)
                {
                    var allOrderItems = GetAllParticipantSignOutSheetV4(schedule, date);

                    var orderItemsModel = SetViewModelSignOutSheetReportV4(schedule, date, allOrderItems.ToList(), !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Program.Name} - {schedule.Title}" : schedule.Program.Name, reportDate);

                    model.Add(orderItemsModel);
                }
            }

            if (model.Any())
            {
                model = sortDataSignOutSheetReport(model, sortBy);
                foreach (var item in model)
                {
                    if (item.ProgramParticipants != null)
                    {
                        setRowNumberSignOutSheetReport(item.ProgramParticipants);
                    }
                }
            }

            model = model.OrderBy(n => n.ClassName).ToList();

            return model;
        }

        private List<ProgramParticipantListSchoolViewModel> GetPunchcardParticipantSignOutSheetV4(Program program, DateTime date)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var orderSessionBusiness = Ioc.OrderSessionBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = new List<OrderItem>();
            var model = new List<ProgramParticipantListSchoolViewModel>();
            var num = 1;

            programOrderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(program.Id).Where(o => o.Mode == OrderItemMode.PunchCard).ToList();

            model = programOrderItems.ToList().Select(o => new ProgramParticipantListSchoolViewModel()
            {
                ParticipantName =
                    o.OrderSessions.Count(s => s.ProgramSession.StartDateTime.Date == date) == 2 ?
                         string.Format("{0}, {1} ***", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)) :
                    o.OrderSessions.Any(s => s.ProgramSession.StartDateTime.Date == date && s.ProgramSession.ProgramSchedule.ScheduleMode == TimeOfClassFormation.AM) ?
                        string.Format("{0}, {1} *", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)) :
                    o.OrderSessions.Any(s => s.ProgramSession.StartDateTime.Date == date && s.ProgramSession.ProgramSchedule.ScheduleMode == TimeOfClassFormation.PM) ?
                        string.Format("{0}, {1} **", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)) :
                    string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)),
                TeacherName = o.JbForm != null ? formBusiness.GetTeacher(o.JbForm) == "" ? "-" : formBusiness.GetTeacher(o.JbForm) : "",
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                AuthorizedAdultNames = o.JbForm != null ? formBusiness.GetAuthorizedAdultNames(o.JbForm).Any(a => a != " ") ? string.Join(", ", formBusiness.GetAuthorizedAdultNames(o.JbForm).Where(a => a != " ")) : "-" : "",
                GradeOrder = formBusiness.GetPlayerGrade(o.JbForm) != null ? formBusiness.GetPlayerGrade(o.JbForm).GetOrder().HasValue ? formBusiness.GetPlayerGrade(o.JbForm).GetOrder().Value : 0 : 0,
                Mode = o.Mode,
                MorningProgramArrival = o.JbForm != null ? formBusiness.GetMorningProgramArrival(o.JbForm) == null ? "n/a" : formBusiness.GetMorningProgramArrival(o.JbForm) : "",
            }).OrderBy(n => n.ParticipantName).ToList();

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        private List<ProgramParticipantListSchoolViewModel> GetAllParticipantSignOutSheetV4(ProgramSchedule programSchedule, DateTime date)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var orderSessionBusiness = Ioc.OrderSessionBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = new List<OrderItem>();
            var model = new List<ProgramParticipantListSchoolViewModel>();
            var num = 1;

            date = date.Date;
            var tommorow = date.AddDays(1);

            if (programSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || programSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription)
            {

                programOrderItems.AddRange(Ioc.NewReportBusiness.GetReportOrderItemsWithEffectiveDate(programSchedule.Id, date));

            }
            else if (programSchedule.Program.TypeCategory == ProgramTypeCategory.Camp)
            {
                programOrderItems.AddRange(orderSessionBusiness.GetList().Where(o => o.ProgramSession.Charge.ProgramScheduleId.Value == programSchedule.Id &&
                       ((o.ProgramSession.Charge.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == o.OrderItem.Order.IsLive) &&
                       o.OrderItem.ItemStatus == OrderItemStatusCategories.completed &&
                       !o.ProgramSession.IsDeleted &&
                       o.ProgramSession.StartDateTime >= date && o.ProgramSession.StartDateTime < tommorow)
                   .Select(o => o.OrderItem)
                   .ToList());
            }
            else
            {
                programOrderItems.AddRange(orderItemBusiness.GetReportOrderItemsBySchedule(programSchedule.Id).ToList().Where(o => o.ProgramSchedule.Sessions.ToList().Any(s => !s.IsDeleted && s.Start >= date && s.Start < tommorow)).ToList());
            }
            
            model = programOrderItems.ToList().Select(o => new ProgramParticipantListSchoolViewModel()
            {
                ParticipantName = string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm) == "" ? "-" : formBusiness.GetPlayerFirstName(o.JbForm)),
                TeacherName = o.JbForm != null ? formBusiness.GetTeacher(o.JbForm) == "" ? "-" : formBusiness.GetTeacher(o.JbForm) : "",
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                AuthorizedAdultNames = o.JbForm != null ? formBusiness.GetAuthorizedAdultNames(o.JbForm).Any(a => a != " ") ? string.Join(", ", formBusiness.GetAuthorizedAdultNames(o.JbForm).Where(a => a != " ")) : "-" : "",
                GradeOrder = formBusiness.GetPlayerGrade(o.JbForm) != null ? formBusiness.GetPlayerGrade(o.JbForm).GetOrder().HasValue ? formBusiness.GetPlayerGrade(o.JbForm).GetOrder().Value : 0 : 0,
                Mode = o.Mode,
                MorningProgramArrival = o.JbForm != null ? formBusiness.GetMorningProgramArrival(o.JbForm) == null ? "n/a" : formBusiness.GetMorningProgramArrival(o.JbForm) : "",
            }).OrderBy(n => n.ParticipantName).ToList();

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        private List<SignOutSheetReportAdvanceClubStaffViewModel> GetStaffsSignOutSheetV4(Club club)
        {
            var result = club.ClubStaffs.Where(c => !c.IsDeleted && c.Status == StaffStatus.Active && (c.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString() || c.JbUserRole.Role.Name == RoleCategory.OnsiteSupportManager.ToString()))
                .Select(c => new SignOutSheetReportAdvanceClubStaffViewModel()
                {
                    FullName = $"{c.Contact.LastName}, {c.Contact.FirstName}",
                    Role = c.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString() ? RoleCategory.OnsiteCoordinator.ToDescription() : RoleCategory.OnsiteSupportManager.ToDescription(),
                    Email = c.JbUserRole.User.Email,
                    Phone = PhoneNumberHelper.FormatPhoneNumber(c.Contact.Phone)
                })
                .ToList();

            return result;
        }

        private SchoolProgramsSignOutReportViewModel SetViewModelSignOutSheetReportV4(ProgramSchedule schedule, DateTime date, List<ProgramParticipantListSchoolViewModel> participants, string programName, string reportDate)
        {
            var programBusiness = Ioc.ProgramBusiness;

            return new SchoolProgramsSignOutReportViewModel()
            {
                ClassName = programName,
                ClassId = schedule.Id,
                ClassDays = string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(programBusiness.GetClassDays(schedule).Select(pr => pr.DayOfWeek).ToList())),
                StartTime = programBusiness.GetClassDays(schedule).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(schedule).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(schedule).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(schedule).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                SchoolName = schedule.Program.Season.Club.Name,
                ProgramParticipants = participants.Count() > 0 ? participants.ToList() : null,
                Date = date.ToString(Constants.DateTime_Comma),
                ReportDate = reportDate,
                Pending = _orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == schedule.Program.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.initiated && o.Order.IsLive).Count(),
                WaitList = schedule.Program.WaitLists.Count(w => w.IsLive == (w.Program.Season.Status == SeasonStatus.Live)),
                Enrolled = _orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == schedule.Program.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive).Count(),
                MinimumEnrollments = (schedule.Program.TypeCategory == ProgramTypeCategory.Class) && schedule.Program.ProgramSchedules.Any(s => !s.IsDeleted) ? (schedule.Program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0,
                Capacity = (schedule.Program.TypeCategory == ProgramTypeCategory.Class) && schedule.Program.ProgramSchedules.Any(s => !s.IsDeleted) ? (schedule.Program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.Capacity : 0,
                GradesOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(schedule.Program)) ? programBusiness.GenerateGradeInfoLabelForFlyer(schedule.Program) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(schedule.Program)) ? programBusiness.GenerateAgeInfoLabel(schedule.Program) : null,
                SeasonName = schedule.Program.Season.Domain,
                Instructor = programBusiness.GetInstractors(schedule.Program, true),
                Location = schedule.Program.Room,
                Staffs = GetStaffsSignOutSheetV4(schedule.Program.Club)
            };
        }

        private SchoolProgramsSignOutReportViewModel SetViewModelSignOutSheetReportV4(Program program, DateTime date, List<ProgramParticipantListSchoolViewModel> participants, string programName, string reportDate)
        {
            var programBusiness = Ioc.ProgramBusiness;

            return new SchoolProgramsSignOutReportViewModel()
            {
                ClassName = programName,
                ClassId = program.Id,
                ClassDays = string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(programBusiness.GetClassDays(program).Select(pr => pr.DayOfWeek).Distinct().ToList())),
                StartTime = programBusiness.GetClassDays(program).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(program).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(program).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(program).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                SchoolName = program.Season.Club.Name,
                ProgramParticipants = participants.Count() > 0 ? participants.ToList() : null,
                Date = date.ToString(Constants.DateTime_Comma),
                ReportDate = reportDate,
                Pending = _orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == program.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.initiated && o.Order.IsLive).Count(),
                WaitList = program.WaitLists.Count(w => w.IsLive == (w.Program.Season.Status == SeasonStatus.Live)),
                Enrolled = _orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == program.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive).Count(),
                MinimumEnrollments = (program.TypeCategory == ProgramTypeCategory.Class) && program.ProgramSchedules.Any(s => !s.IsDeleted) ? (program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0,
                Capacity = (program.TypeCategory == ProgramTypeCategory.Class) && program.ProgramSchedules.Any(s => !s.IsDeleted) ? (program.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.Capacity : 0,
                GradesOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(program)) ? programBusiness.GenerateGradeInfoLabelForFlyer(program) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(program)) ? programBusiness.GenerateAgeInfoLabel(program) : null,
                SeasonName = program.Season.Domain,
                Instructor =  programBusiness.GetInstractors(program, true),
                Location = program.Room,
                Staffs = GetStaffsSignOutSheetV4(program.Club)
            };
        }

        [HttpGet]
        public virtual FileResult GenerateSignOutSheetV4Pdf(string seasonDomain, long? scheduleId, DateTime date, string sortBy)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var school = Ioc.ClubBusiness.Get(clubId);
            var partner = new Club();
            var showSchoolLogo = true;

            if (school.PartnerId.HasValue)
            {
                partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);
            }
            else
            {
                partner = Ioc.ClubBusiness.Get(school.Id);
            }

            if (school.Setting != null)
            {
                showSchoolLogo = school.Setting.ShowSchoolLogo;
            }

            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelSignOutSheetReportV4(season.Id, school.Id, scheduleId, date, sortBy);

            var PdfModel = new SignOutSheetPdfSchoolViewModel()
            {
                SchoolLogo = showSchoolLogo ? _clubBusiness.GetClubLogoURL(school.Domain, school.Logo) : null,
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "SignOutSheetPdfSchoolV4Reports", PdfModel);

            return File(file, "application/pdf", "Sign-Out sheet_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        [HttpGet]
        public virtual FileContentResult GenerateSignOutSheetV4Excel(string seasonDomain, long? scheduleId, DateTime date, string sortBy)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = CreateModelSignOutSheetReportV4(season.Id, clubId, scheduleId, date, sortBy);

            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();


            //Define Style
            ICellStyle alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.VerticalAlignment = VerticalAlignment.Center;

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var borderTopStyle = workbook.CreateCellStyle();
            borderTopStyle.BorderTop = BorderStyle.Thin;
            borderTopStyle.Alignment = HorizontalAlignment.Center;
            borderTopStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleLeftAlignment = workbook.CreateCellStyle();
            greyStyleLeftAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleLeftAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleLeftAlignment.Alignment = HorizontalAlignment.Left;
            greyStyleLeftAlignment.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleCenterAlignment = workbook.CreateCellStyle();
            greyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            greyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;
            //greyStyleCenterAlignment.BorderBottom = BorderStyle.MEDIUM;


            sheet.SetDefaultColumnStyle(0, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(1, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(2, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(3, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(4, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(5, alignmentCenterStyle);


            var rowNumber = 0;
            var itemNumber = 0;

            foreach (var item in model)
            {
                if (item.ProgramParticipants == null)
                {
                    continue;
                }

                //**** Header ****////
                var headerRow = sheet.CreateRow(rowNumber);

                if (itemNumber != 0)
                {
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                }
                itemNumber++;

                // set default column size and color
                headerRow.CreateCell(0).SetCellValue(" ");
                headerRow.CreateCell(1).SetCellValue(" ");
                headerRow.CreateCell(2).SetCellValue(" ");
                headerRow.CreateCell(3).SetCellValue(" ");
                headerRow.CreateCell(4).SetCellValue(" ");
                headerRow.CreateCell(5).SetCellValue(" ");
                headerRow.CreateCell(6).SetCellValue(" ");
                headerRow.CreateCell(7).SetCellValue(" ");
                headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(4).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(7).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 20 * 256);
                headerRow.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
                headerRow.GetCell(4).Sheet.SetColumnWidth(4, 20 * 256);
                headerRow.GetCell(5).Sheet.SetColumnWidth(5, 10 * 256);
                headerRow.GetCell(6).Sheet.SetColumnWidth(6, 40 * 256);
                headerRow.GetCell(7).Sheet.SetColumnWidth(7, 10 * 256);

                //Set the column names in the header row
                //Set the column names in the header row
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Class: {item.ClassName}");
                headerRow.CreateCell(3).SetCellValue($"Time period: {item.SeasonName}");
                headerRow.CreateCell(5).SetCellValue($"Minimum enrollments: {item.MinimumEnrollments}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Grades/Ages: {item.GradesOrAges}");
                headerRow.CreateCell(3).SetCellValue($"Primary instructor: {item.Instructor}");
                headerRow.CreateCell(5).SetCellValue($"Capacity: {item.Capacity}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"School: {item.SchoolName}");
                headerRow.CreateCell(3).SetCellValue($"Facility/Room: {item.Location}");
                headerRow.CreateCell(5).SetCellValue($"Enrolled: {item.Enrolled}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Days: {item.ClassDays}");
                headerRow.CreateCell(5).SetCellValue($"Pending: {item.Pending}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Time: {item.StrTime}");
                headerRow.CreateCell(5).SetCellValue($"WaitList: {item.WaitList}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                //line space between header and content
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);



                //*** Content ***//
                // static header
                rowNumber++;
                var row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("#");
                row.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);

                row.CreateCell(1).SetCellValue("Participant");
                row.CreateCell(2).SetCellValue("Teacher");
                row.CreateCell(3).SetCellValue("Dismissal");
                row.CreateCell(4).SetCellValue("Morning arrival");
                row.CreateCell(5).SetCellValue("Sign in");
                row.CreateCell(6).SetCellValue("Authorized pick up");
                row.CreateCell(7).SetCellValue("Signature");
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                row.GetCell(4).CellStyle = greyStyleCenterAlignment;
                row.GetCell(5).CellStyle = greyStyleCenterAlignment;
                row.GetCell(6).CellStyle = greyStyleCenterAlignment;
                row.GetCell(7).CellStyle = greyStyleCenterAlignment;



                //Content
                if (item.ProgramParticipants != null)
                {
                    foreach (var participant in item.ProgramParticipants)
                    {
                        rowNumber++;
                        row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(participant.Number);
                        row.GetCell(0).CellStyle = alignmentCenterStyle;
                        row.GetCell(0).CellStyle = borderTopStyle;

                        row.CreateCell(1).SetCellValue(participant.ParticipantName);
                        row.CreateCell(2).SetCellValue(participant.TeacherName);
                        row.CreateCell(3).SetCellValue(participant.TransportHome);
                        row.CreateCell(4).SetCellValue(participant.MorningProgramArrival);
                        row.CreateCell(5).SetCellValue(string.Empty);
                        row.CreateCell(6).SetCellValue(participant.AuthorizedAdultNames);
                        row.CreateCell(7).SetCellValue(string.Empty);
                        row.GetCell(1).CellStyle = borderTopStyle;
                        row.GetCell(2).CellStyle = borderTopStyle;
                        row.GetCell(3).CellStyle = borderTopStyle;
                        row.GetCell(4).CellStyle = borderTopStyle;
                        row.GetCell(5).CellStyle = borderTopStyle;
                        row.GetCell(6).CellStyle = borderTopStyle;
                        row.GetCell(7).CellStyle = borderTopStyle;
                    }
                }
                else
                {
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(string.Empty);
                    row.GetCell(0).CellStyle = alignmentCenterStyle;
                    row.GetCell(0).CellStyle = borderTopStyle;

                    row.CreateCell(1).SetCellValue(string.Empty);
                    row.CreateCell(2).SetCellValue(string.Empty);
                    row.CreateCell(3).SetCellValue(string.Empty);
                    row.CreateCell(4).SetCellValue(string.Empty);
                    row.CreateCell(5).SetCellValue(string.Empty);
                    row.CreateCell(6).SetCellValue(string.Empty);
                    row.CreateCell(7).SetCellValue(string.Empty);
                    row.GetCell(1).CellStyle = borderTopStyle;
                    row.GetCell(2).CellStyle = borderTopStyle;
                    row.GetCell(3).CellStyle = borderTopStyle;
                    row.GetCell(4).CellStyle = borderTopStyle;
                    row.GetCell(5).CellStyle = borderTopStyle;
                    row.GetCell(6).CellStyle = borderTopStyle;
                    row.GetCell(7).CellStyle = borderTopStyle;

                }

                if (item.Staffs != null && item.ProgramParticipants != null)
                {
                    rowNumber++;
                    foreach (var staff in item.Staffs)
                    {
                        rowNumber++;
                        row = sheet.CreateRow(rowNumber);
                        row.RowStyle = greyStyleLeftAlignment;

                        var fullName = staff.FullName != null ? $" - {staff.FullName}" : staff.FullName;
                        var phone = staff.Phone != null ? $" - {staff.Phone}" : staff.Phone;
                        var email = staff.Email != null ? $" - {staff.Email}" : staff.Email;

                        row.CreateCell(1).SetCellValue($"{staff.Role} {fullName} {phone} {email}");
                        row.GetCell(1).CellStyle = greyStyleLeftAlignment;
                    }
                }

                if (item.ClassName.Contains("Punchcard") && item.ProgramParticipants != null)
                {
                    rowNumber++;
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("* Today attending the first session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("** Today attending the secnod session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("*** Today attending the both session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;
                }

                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);

            }


            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel",//MIME type of Excel files
             "Sign-out sheet.xls");
        }


        [HttpGet]
        public virtual ActionResult GetRoomAssignmentsReportHeader(string seasonDomain = "")
        {
            var clubId = ActiveClub.Id;

            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = new
            {
                ClubName = ActiveClub.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt"),//Sep 21, 2016 HH:MM
                SeasonName = season != null ? season.Title : "-"
            };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult GetRoomAssignments(string seasonDomain, bool printMode = false)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }

            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var programs = Ioc.ProgramBusiness.GetSeasonPrograms(season.Id);

            var models = new List<RoomAssignmentViewModel>();
            var spaceReqString = string.Empty;
            var additionalComments = string.Empty;

            var programPaging = new List<Program>();

            if (printMode)
            {
                programPaging = programs.OrderBy(c => c.Name).ToList();
            }
            else
            {
                programPaging = programs.OrderBy(c => c.Name)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize).ToList();
            }

            foreach (var program in programPaging)
            {
                if (program.CatalogId.HasValue)
                {
                    var catalog = Ioc.CatalogBusiness.Get(program.CatalogId.Value);
                    var spaceReq = catalog.CatalogItemOptions.Where(o => o.Type == CatalogOptionType.SpaceRequrement).ToList();
                    spaceReqString = spaceReq.Aggregate("",
                        (current, space) => current + string.Format(" {0},", space.Title));

                    spaceReqString = (!string.IsNullOrEmpty(spaceReqString)) ? spaceReqString.Remove(spaceReqString.Length - 1) : string.Empty;

                    additionalComments = catalog.Detail;
                }

                var classDuration = string.Empty;

                foreach (var programSchedule in program.ProgramSchedules)
                {
                    var scheduleDays = ((ScheduleAttribute)programSchedule.Attributes).Days;
                    foreach (var scheduleDay in scheduleDays)
                    {
                        classDuration += string.Format(" {0} ({1} - {2}),", scheduleDay.DayOfWeek, new DateTime(scheduleDay.StartTime.Value.Ticks).ToString(@"hh:mm tt"), new DateTime(scheduleDay.EndTime.Value.Ticks).ToString(@"hh:mm tt"));
                    }
                }

                classDuration = (!string.IsNullOrEmpty(classDuration)) ? classDuration.Remove(classDuration.Length - 1) : string.Empty;

                var schedule = program.ProgramSchedules.First();
                var max = schedule.Attributes.Capacity;
                var min = schedule.Attributes.MinimumEnrollment;
                var days = ((ScheduleAttribute)schedule.Attributes).Days.Select(d => d.DayOfWeek);
                var day = days.Aggregate(string.Empty, (current, item) => current + string.Format(" {0},", item));
                day = day.Remove(day.Length - 1);
                var room = program.Room;

                var classDate = Ioc.ProgramBusiness.GetClassDates(program.ProgramSchedules.First());
                //var sessions = Ioc.ProgramBusiness.GetSessions(program);
                var sessionsString = classDate.Aggregate(string.Empty,
                    (current, session) => current + string.Format(" {0},", session.SessionDate.ToString(@"MMM dd")));

                sessionsString = sessionsString.Remove(sessionsString.Length - 1);

                var newRow = new RoomAssignmentViewModel
                {
                    ClassName = program.Name,
                    SpaceReq = spaceReqString,
                    Duration = classDuration,
                    Day = day,
                    Sessions = sessionsString,
                    Min = min,
                    Max = max,
                    Room = room,
                    AdditionalComments = additionalComments
                };
                models.Add(newRow);
            }

            models = models.OrderBy(c => c.ClassName).ToList();

            if (printMode)
            {
                dynamic model = new System.Dynamic.ExpandoObject();

                model.Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                model.Data = models;
                var clubName = Ioc.ClubBusiness.Get(clubId).Name;
                model.ClubName = clubName;
                model.SeasonName = season.Title;

                return Pdf(clubName + " (Room assignments)", "_RoomAssignmentsAdminReportPdfExportView", model);
            }

            return JsonNet(new { DataSource = models, TotalCount = programs.Count() });
        }

        [HttpPost]
        public virtual JsonNetResult GetAllInfoPreRegistrationReport(SelectedPrograms model)
        {
            var club = ActiveClub;
            var orderItemBusiness = Ioc.OrderItemBusiness;
            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();

            }

            var allPerregistrationItem = Ioc.OrderItemBusiness.GetList().Where(p => model._SelectedPrograms.ToList().Contains(p.ProgramSchedule.Program.Id))
                                        .Where(i => i.ItemStatus != OrderItemStatusCategories.deleted && (i.PreRegistrationStatus == PreRegistrationStatus.Initiated || i.PreRegistrationStatus == PreRegistrationStatus.Added && i.ItemStatus == OrderItemStatusCategories.initiated)).ToList();

            var result = allPerregistrationItem.Select(item =>
              new PerRegistrationReportViewModel
              {
                  Id = item.ProgramSchedule.Program.Id,
                  ProgramName = item.ProgramSchedule.Program.Name,
                  ParticipantName = item.FirstName + " " + item.LastName,
                  UserName = item.Order.User.UserName,
                  OrderDate = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), item.Order.CompleteDate),
                  ProviderName = item.ProgramSchedule.Program.OutSourceSeasonId.HasValue ? item.ProgramSchedule.Program.OutSourceSeason.Club.Name : item.ProgramSchedule.Program.Club.Name,
                  Capacity = (item.ProgramSchedule.Attributes as ScheduleAttribute).Capacity != 0 ? (item.ProgramSchedule.Attributes as ScheduleAttribute).Capacity.ToString() : "-",
                  Count = Ioc.OrderItemBusiness.GetList().Where(i => i.ProgramSchedule.ProgramId == item.ProgramSchedule.ProgramId && i.ItemStatus != OrderItemStatusCategories.deleted && (i.PreRegistrationStatus == PreRegistrationStatus.Initiated || i.PreRegistrationStatus == PreRegistrationStatus.Added && i.ItemStatus == OrderItemStatusCategories.initiated)).ToList().Count,
              }
          )
          .ToList();


            return JsonNet(new { DataSource = result, TotalCount = result.Count });
        }

        [HttpPost]
        public virtual JsonNetResult GetPreRegistrationsReport(SelectedPrograms model)
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);
            var club = ActiveClub;


            if (model.IsAllPrograms)
            {
                var seasonId = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id).Id;
                model._SelectedPrograms = Ioc.ProgramBusiness.GetSeasonPrograms(seasonId).Select(p => p.Id).ToList();

            }

            var allPerregistrationItem = Ioc.OrderItemBusiness.GetList().Where(p => model._SelectedPrograms.ToList().Contains(p.ProgramSchedule.Program.Id))
                                        .Where(i => i.ItemStatus != OrderItemStatusCategories.deleted && (i.PreRegistrationStatus == PreRegistrationStatus.Initiated || i.PreRegistrationStatus == PreRegistrationStatus.Added && i.ItemStatus == OrderItemStatusCategories.initiated)).ToList();

            var result = allPerregistrationItem.Select(i =>
               new PerRegistrationReportViewModel
               {
                   Id = i.ProgramSchedule.Program.Id,
                   ProgramName = i.ProgramSchedule.Program.Name,
                   ProviderName = i.ProgramSchedule.Program.OutSourceSeasonId.HasValue ? i.ProgramSchedule.Program.OutSourceSeason.Club.Name : i.ProgramSchedule.Program.Club.Name,
                   Count = Ioc.OrderItemBusiness.GetList().Where(item => item.ProgramSchedule.ProgramId == i.ProgramSchedule.ProgramId && item.ItemStatus != OrderItemStatusCategories.deleted && (item.PreRegistrationStatus == PreRegistrationStatus.Initiated || item.PreRegistrationStatus == PreRegistrationStatus.Added && item.ItemStatus == OrderItemStatusCategories.initiated)).ToList().Count,
                   Capacity = (i.ProgramSchedule.Attributes as ScheduleAttribute).Capacity != 0 ? (i.ProgramSchedule.Attributes as ScheduleAttribute).Capacity.ToString() : "-",
               }
           ).DistinctBy(c => c.Id)
           .ToList();

            var dataSource = result.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = result.Count });
        }
        [HttpPost]
        public virtual JsonNetResult GetPreRegistrationReportDetails(long programId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programPerRegistrationItems = Ioc.OrderItemBusiness.GetList().Where(i => i.ProgramSchedule.Program.Id == programId && (i.PreRegistrationStatus == PreRegistrationStatus.Initiated || i.PreRegistrationStatus == PreRegistrationStatus.Added && i.ItemStatus == OrderItemStatusCategories.initiated) && i.ItemStatus != OrderItemStatusCategories.deleted).ToList();

            var result = programPerRegistrationItems.Select(c =>
                         new PerRegistrationReportViewModel
                         {
                             ParticipantName = c.FirstName + " " + c.LastName,
                             UserName = c.Order.User.UserName,
                             OrderDate = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), c.Order.CompleteDate),
                             ProviderName = c.ProgramSchedule.Program.OutSourceSeasonId.HasValue ? c.ProgramSchedule.Program.OutSourceSeason.Club.Name : c.ProgramSchedule.Program.Club.Name,
                         }
                         ).ToList();

            return JsonNet(new { DataSource = result, TotalCount = result.Count });
        }
        [HttpGet]
        public virtual ActionResult GetSubscriptionSignOutReportDetail(long programId, DateTime date)
        {
            var program = Ioc.ProgramBusiness.Get(programId);

            var data = new
            {
                ProgramName = program.Name,
                ClubName = (program.ClubId != ActiveClub.Id && program.OutSourceSeasonId.HasValue) ? string.Format("({0})", program.Club.Name) : string.Empty,
                ClassDate = date,
                StrClassDate = date != null ? date.ToString("MMM dd, yyyy") : "-",
            };

            return JsonNet(data);
        }
        [HttpPost]
        public virtual JsonNetResult RunSignOutReport(PaginationModel paginationModel, long programId, DateTime date, bool printMode = false)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }


            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId);

            if (orderItems == null || !orderItems.Any())
            {
                return JsonNet(new { DataSource = new List<object>(), TotalCount = 0 });
            }


            var formBusiness = Ioc.FormBusiness;
            var participantsClass = orderItems.ToList().Where(i => i.OrderSessions.ToList().Select(o => o.ProgramSession.StartDateTime.Date).ToList().Any(s => s == date.Date)).ToList();

            var model = participantsClass.Select(c => new SingOutReportViewModel()
            {
                ParticipantName = c.LastName + ", " + c.FirstName,
                Grade = formBusiness.GetPlayerGradeAsString(c.JbForm) != null ? formBusiness.GetPlayerGradeAsString(c.JbForm) : string.Empty,
                TuitionName = c.EntryFeeName,
            }).ToList();

            model = model.OrderBy(n => n.ParticipantName).ToList();
            var dataSource = model.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            return JsonNet(new { DataSource = dataSource, TotalCount = model.Count() });
        }
        [HttpPost]
        public virtual FileResult GenerateSignReportPdf(long programId, DateTime startDate)
        {
            var model = new SingOutReportViewModel();
            var program = Ioc.ProgramBusiness.Get(programId);
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId);
            var club = program.Club;

            var formBusiness = Ioc.FormBusiness;
            var participantsClass = orderItems.ToList().Where(i => i.OrderSessions.ToList().Select(o => o.ProgramSession.StartDateTime.Date).ToList().Any(s => s == startDate.Date)).ToList();

            var clubLogo = "";
            var partner = club.PartnerClub;

            var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);

            if (clubInfo.logo)
            {
                clubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo);
            }
            else
            {
                clubLogo = _clubBusiness.GetClubLogoURL(club.Domain, club.Logo);
            }


            model.Date = startDate != null ? startDate.ToString("MMM dd, yyyy") : "-";
            model.Day = startDate.DayOfWeek.ToDescription();
            model.ReportName = "SignOut";
            model.SchoolName = club.Name;
            model.Logo = clubLogo;

            if (orderItems != null)
            {
                model.participants = participantsClass.ToList().Select(i => GetParticipantInfo(i)).OrderBy(c => c.ParticipantName).ToList();
                var num = 0;
                foreach (var item in model.participants)
                {
                    item.Number = num + 1;
                    num++;
                }
            }


            var flyerGenerator = new FlyerGenerator();
            byte[] file = null;
            file = flyerGenerator.GenerateFromView(this.ControllerContext, "_SignOutReportPdf", model);
            return File(file, "application/pdf", "SignOut.Pdf");
        }
        [HttpGet]
        public virtual ActionResult GetSubscriptionInternalReportDetail(List<long> programIds, DateTime? startDate)
        {
            var model = new SubscriptionInternalViwModel();

            model.StartDate = startDate;
            model.ProgramIds = programIds;
            model.ReportName = "Internal";
            model = GetInformationForSubscriptionReport(model, String.Empty);

            return JsonNet(model);
        }

        [HttpPost]
        public virtual FileResult GenerateInernalReportPdf(List<long> programIds, DateTime? startDate)
        {
            var model = new SubscriptionInternalViwModel();


            model.StartDate = startDate;
            model.ProgramIds = programIds;
            model.ReportName = "Internal";
            model = GetInformationForSubscriptionReport(model, string.Empty);

            var flyerGenerator = new FlyerGenerator();
            byte[] file = null;
            file = flyerGenerator.GenerateFromView(this.ControllerContext, "_InternalReportPdf", model);
            return File(file, "application/pdf", "Internal roster.Pdf");
        }

        [HttpGet]
        public virtual ActionResult GetSubscriptionAttendanceReportDetail(List<long> programIds, DateTime? startDate, string sortBy)
        {
            var model = new SubscriptionInternalViwModel();

            model.StartDate = startDate.Value.Date;
            model.ProgramIds = programIds;
            model.ReportName = "Attendance";
            model = GetInformationForSubscriptionReport(model, sortBy);
            return JsonNet(model);
        }

        [HttpPost]
        public virtual FileResult GenerateAttendanceReportPdf(List<long> programIds, DateTime? startDate, string sortBy)
        {
            var model = new SubscriptionInternalViwModel();

            model.StartDate = startDate;
            model.ProgramIds = programIds;
            model.ReportName = "Attendance";
            model = GetInformationForSubscriptionReport(model, sortBy);

            var flyerGenerator = new FlyerGenerator();
            byte[] file = null;
            file = flyerGenerator.GenerateFromView(this.ControllerContext, "_AttendanceReportPdf", model);
            return File(file, "application/pdf", "Attendance.Pdf");
        }

        public SubscriptionInternalViwModel GetInformationForSubscriptionReport(SubscriptionInternalViwModel model, string sortBy)
        {
            var program = Ioc.ProgramBusiness.Get(model.ProgramIds.FirstOrDefault());
            var orderBusiness = Ioc.OrderItemBusiness;

            DateTime endDate;
            endDate = model.StartDate.Value.AddDays(7);

            var orderItems = new List<OrderItem>();
            foreach (var id in model.ProgramIds)
            {
                orderItems.AddRange(orderBusiness.GetSubscriptionReportOrderItems(id, model.StartDate.Value.Date).ToList());
            }

            var club = program.Club;
            var clubLogo = "";
            var partner = club.PartnerClub;

            var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);

            if (clubInfo.logo)
            {
                clubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo);
            }
            else
            {
                clubLogo = _clubBusiness.GetClubLogoURL(club.Domain, club.Logo);
            }

            model.Logo = clubLogo;
            model.SchoolName = club.Name;
            model.ReportDate = DateTime.UtcNow.ToString("MMM dd, yyyy");

            var num = 1;
            model.Participants = orderItems.ToList().Select(i => GetSubscriptionParticipantInformation(i, model.StartDate.Value, endDate, model.ReportName)).ToList();
            model.Participants.RemoveAll(p => p == null);
            model.Participants = model.Participants.Count > 0 ? model.Participants.OrderBy(p => p.StudentName).ToList() : null;
            if (model.Participants != null)
            {
                model.TotalNumberOfParticipant = model.Participants.Where(i => i == null).ToList().Count != model.Participants.Count ? model.Participants.Count : 0;

                foreach (var participant in model.Participants)
                {
                    participant.Number = num;
                    num++;
                }

                model.TotalItemInMonday = model.Participants.Where(p => p.Monday == true).ToList().Count;
                model.TotalItemInTuesday = model.Participants.Where(p => p.Tuesday == true).ToList().Count;
                model.TotalItemInWednesday = model.Participants.Where(p => p.Wednesday == true).ToList().Count;
                model.TotalItemInThursday = model.Participants.Where(p => p.Thursday == true).ToList().Count;
                model.TotalItemInFriday = model.Participants.Where(p => p.Friday == true).ToList().Count;
            }
            else
            {
                model.TotalNumberOfParticipant = 0;
            }

            if (model.Participants != null)
            {
                if (sortBy == "Participant")
                {
                    model.Participants = model.Participants.OrderBy(p => p.StudentName).ToList();
                }
                else if (sortBy == "Grade")
                {
                    model.Participants = model.Participants.OrderBy(p => p.GradeOrder).ToList();
                }

                setRowNumberSubscriptionAttendanceReport(model.Participants);
            }

            return model;
        }

        private static void setRowNumberSubscriptionAttendanceReport(List<SubscriptionParticipantInternalViwModel> participants)
        {
            var num = 1;
            foreach (var participant in participants)
            {
                participant.Number = num;
                num++;
            }
        }

        public List<SubscriptionInternalViwModel> GetInformationForSubscriptionAttendanceReport(long seasonId, long? scheduleId, DateTime date)
        {
            var model = new List<SubscriptionInternalViwModel>();
            var programs = new List<Program>();

            if (scheduleId.HasValue)
            {
                programs = _programBusiness.GetListBySeasonId(seasonId).Where(p => (p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted && p.ProgramSchedules.Select(s => s.Id).Contains(scheduleId.Value)).ToList();
            }
            else
            {
                programs = _programBusiness.GetListBySeasonId(seasonId).Where(p => (p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted).ToList();
            }

            var firstProgram = programs.FirstOrDefault();
            var club = firstProgram.Club;
            var reportDate = $"{Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString(Constants.DefaultDateTimeFormatAbrMonthWithAMPM)} ({club.TimeZone.ToString()})";

            var orderBusiness = Ioc.OrderItemBusiness;

            DateTime endDate;
            endDate = date.AddDays(7);

            var programIds = programs.Select(p => p.Id).ToList();

            var orderItems = new List<OrderItem>();
            foreach (var id in programIds)
            {
                orderItems.AddRange(orderBusiness.GetProgramOrderItems(id, OrderItemStatusCategories.showAll).ToList());
            }

            var clubLogo = "";
            var partner = club.PartnerClub;

            var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);

            if (clubInfo.logo)
            {
                clubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo);
            }
            else
            {
                clubLogo = _clubBusiness.GetClubLogoURL(club.Domain, club.Logo);
            }

            var allParticipants = orderItems.ToList().Select(i => GetSubscriptionParticipantInformation(i, date.Date, endDate.Date, "Athendance")).ToList();
            allParticipants.RemoveAll(p => p == null);
            allParticipants = allParticipants.Count > 0 ? allParticipants.OrderBy(p => p.StudentName).ToList() : new List<SubscriptionParticipantInternalViwModel>();


            if (scheduleId.HasValue && allParticipants != null)
            {
                var schedule = programs.SelectMany(c => c.ProgramSchedules).SingleOrDefault(c => c.Id == scheduleId.Value);
                if (schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
                {
                    var beforeAfterOrderItemsModel = SetViewModelSubscriptionAttendanceReport(date, allParticipants.Where(a => a.orderMode == OrderItemMode.Noraml && (a.ScheduleMode == schedule.ScheduleMode || a.ScheduleMode == TimeOfClassFormation.Both)).ToList(), $"{schedule.Program.Name} - {schedule.Title}", reportDate, clubLogo, schedule);
                    model.Add(beforeAfterOrderItemsModel);

                    var dropInOrderItems = GetDropInPunchcarParticipantForAttendanceReport(schedule, allParticipants, true);
                    var dropinOrderItemsModel = SetViewModelSubscriptionAttendanceReport(date, dropInOrderItems, $"{schedule.Program.Name} - {schedule.Title} - Drop-in", reportDate, clubLogo, schedule);
                    model.Add(dropinOrderItemsModel);

                    var punchCardOrderItems = GetDropInPunchcarParticipantForAttendanceReport(schedule, allParticipants, false);
                    var punchCardOrderItemsModel = SetViewModelSubscriptionAttendanceReport(date, punchCardOrderItems, $"{schedule.Program.Name} - Punchcard", reportDate, clubLogo, null, schedule.Program);
                    model.Add(punchCardOrderItemsModel);

                    return model;
                }

                var orderItemsModel = SetViewModelSubscriptionAttendanceReport(date, allParticipants.ToList(), !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Program.Name} - {schedule.Title}" : schedule.Program.Name, reportDate, clubLogo, schedule);

                model.Add(orderItemsModel);

                return model;
            }

            foreach (var program in programs)
            {
                var comboSchedule = program.ProgramSchedules.SingleOrDefault(p => p.ScheduleMode == TimeOfClassFormation.Both);

                var allComboOrderItems = new List<SubscriptionParticipantInternalViwModel>();

                if (comboSchedule != null)
                {
                    allComboOrderItems = allParticipants.Where(p => p.ScheduleMode == TimeOfClassFormation.Both && p.ProgramId == program.Id).ToList();
                }

                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                {
                    if (schedule.ScheduleMode == TimeOfClassFormation.AM || schedule.ScheduleMode == TimeOfClassFormation.PM)
                    {
                        var allBeforeAfterOrderItems = allParticipants.Where(p => p.ScheduleMode == schedule.ScheduleMode && p.ProgramId == program.Id).ToList();
                        allBeforeAfterOrderItems.AddRange(allComboOrderItems);

                        var beforeAfterOrderItemsModel = SetViewModelSubscriptionAttendanceReport(date, allBeforeAfterOrderItems.Where(a => a.orderMode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}", reportDate, clubLogo, schedule);
                        model.Add(beforeAfterOrderItemsModel);

                        var dropInOrderItems = GetDropInPunchcarParticipantForAttendanceReport(schedule, allParticipants, true);
                        var dropinOrderItemsModel = SetViewModelSubscriptionAttendanceReport(date, dropInOrderItems, $"{schedule.Program.Name} - {schedule.Title} - Drop-in", reportDate, clubLogo, schedule);
                        model.Add(dropinOrderItemsModel);

                        var punchCardOrderItems = GetDropInPunchcarParticipantForAttendanceReport(schedule, allParticipants, false);
                        var punchCardOrderItemsModel = SetViewModelSubscriptionAttendanceReport(date, punchCardOrderItems, $"{program.Name} - Punchcard", reportDate, clubLogo, null, program);
                        model.Add(punchCardOrderItemsModel);
                    }
                    else if (schedule.ScheduleMode == TimeOfClassFormation.None)
                    {
                        var allOrderItems = allParticipants.Where(p => p.ScheduleMode == TimeOfClassFormation.None && p.ProgramId == program.Id).ToList();
                        var orderItemsModel = SetViewModelSubscriptionAttendanceReport(date, allOrderItems, $"{program.Name}", reportDate, clubLogo, schedule);
                        model.Add(orderItemsModel);
                    }
                }
            }

            return model;
        }

        private SubscriptionInternalViwModel SetViewModelSubscriptionAttendanceReport(DateTime date, List<SubscriptionParticipantInternalViwModel> participants, string programName, string reportDate, string logo, ProgramSchedule schedule = null, Program program = null)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var num = 1;

            var model = new SubscriptionInternalViwModel()
            {
                ClassName = programName,
                SchoolName = program != null ? program.Season.Club.Name : schedule.Program.Season.Club.Name,
                Participants = participants.Count() > 0 ? participants.ToList() : null,
                ReportDate = reportDate,
                Logo = logo
            };

            if (participants != null)
            {
                model.TotalNumberOfParticipant = participants.Where(i => i == null).ToList().Count != participants.Count ? participants.Count : 0;

                foreach (var participant in participants)
                {
                    participant.Number = num;
                    num++;
                }

                model.TotalItemInMonday = participants.Where(p => p.Monday == true).ToList().Count;
                model.TotalItemInTuesday = participants.Where(p => p.Tuesday == true).ToList().Count;
                model.TotalItemInWednesday = participants.Where(p => p.Wednesday == true).ToList().Count;
                model.TotalItemInThursday = participants.Where(p => p.Thursday == true).ToList().Count;
                model.TotalItemInFriday = participants.Where(p => p.Friday == true).ToList().Count;
            }
            else
            {
                model.TotalNumberOfParticipant = 0;
            }

            return model;
        }
        public virtual JsonNetResult GetAllCampClubs(string seasonDomain)
        {
            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id).Id;
            var programs = Ioc.ProgramBusiness.GetListBySeasonId(seasonId).Where(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage >= ProgramPageStep.Step4 && p.TypeCategory == ProgramTypeCategory.Camp).ToList();

            var Allprograms = programs.Select(p => new SelectKeyValue<long>() { Text = p.Name, Value = p.Id })
                    .OrderBy(s => s.Text).ToList();
            return JsonNet(Allprograms);
        }

        private List<SubscriptionParticipantInternalViwModel> GetDropInPunchcarParticipantForAttendanceReport(ProgramSchedule schedule, List<SubscriptionParticipantInternalViwModel> allParticipants, bool isDropIn)
        {
            var participants = new List<SubscriptionParticipantInternalViwModel>();
            var scheduleSessions = schedule.ProgramSessions.Where(p => !p.IsDeleted);

            participants = isDropIn ? allParticipants.Where(a => a.orderMode == OrderItemMode.DropIn).ToList() : allParticipants.Where(p => p.orderMode == OrderItemMode.PunchCard && p.ProgramId == schedule.ProgramId).ToList();
            var participantSessionIds = participants.SelectMany(c => c.sessionId).ToList();

            participants = participants.Where(o => scheduleSessions.Select(p => p.Id).ToList().Any(item => o.sessionId.Contains(item))).ToList();

            return participants;
        }
        [HttpPost]
        public virtual ActionResult GetCampRosterReport(long programId, long? scheduleId, decimal? entryFeeId)
        {
            var pageSize = 0;
            if (Request.Params["PageSize"] != null)
            {
                pageSize = int.Parse(Request.Params["PageSize"]);
            }
            var page = 0;
            if (Request.Params["Page"] != null)
            {
                page = int.Parse(Request.Params["Page"]);
            }
            var model = new List<IDictionary<string, Object>>();
            var total = 0;

            var titles = new List<string>();

            model = CreateModelCampRosterReport(programId, page, pageSize, ref total, ref titles, scheduleId, entryFeeId);

            return JsonNet(new { DataSource = model, TotalCount = total, Titles = titles });
        }

        private List<IDictionary<string, Object>> CreateModelCampRosterReport(long programId, int page, int pageSize, ref int total, ref List<string> titles, long? scheduleId, decimal? entryFeeId)
        {
            var query = Ioc.OrderItemBusiness.GetProgramOrderItems(programId, OrderItemStatusCategories.completed);

            if (scheduleId.HasValue && scheduleId.Value > 0)
            {
                query = query.Where(item => item.ProgramScheduleId == scheduleId);
            }

            if (entryFeeId.HasValue && entryFeeId.Value > 0)
            {
                query = query.Where(oitem => oitem.OrderChargeDiscounts.Any(cd => !cd.IsDeleted && cd.ChargeId == entryFeeId));
            }

            var orderItemPaging = query
                .GroupBy(o => o.PlayerId.Value)
                .Select(o => o.FirstOrDefault())
                .OrderBy(o => o.Player.Contact.LastName)
                .ThenBy(o => o.Player.Contact.FirstName)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var playerProfileBusiness = Ioc.PlayerProfileBusiness;

            var userIds = orderItemPaging.Select(o => o.Order.UserId).ToList();

            var allParticipants = playerProfileBusiness.GetAllParticipantsProfile(userIds);
            var allParents = playerProfileBusiness.GetAllParentsProfile(userIds);
            var allEmergencyContacts = playerProfileBusiness.GetAllEmergencyFamily(userIds);
            var allAuthorizePickups = playerProfileBusiness.GetAllAuthorizePickupFamily(userIds);

            var model = new List<IDictionary<string, Object>>();

            var schedules = new List<ProgramSchedule>();
            if (scheduleId.HasValue && scheduleId.Value > 0)
            {
                schedules.Add(Ioc.ProgramBusiness.GetSchedule(scheduleId.Value));
            }
            else
            {
                schedules.AddRange(Ioc.ProgramBusiness.Get(programId).ProgramSchedules);
            }

            foreach (var item in orderItemPaging)
            {
                var participant = allParticipants.FirstOrDefault(a => a.Id == item.PlayerId.Value);
                var parent1 = allParents.Where(a => a.UserId == item.Order.UserId).Count() >= 1 ? allParents.Where(a => a.UserId == item.Order.UserId).ElementAt(0) : null;
                var parent2 = allParents.Where(a => a.UserId == item.Order.UserId).Count() >= 2 ? allParents.Where(a => a.UserId == item.Order.UserId).ElementAt(1) : null;
                var emergency = allEmergencyContacts.FirstOrDefault(a => a.Family.UserId == item.Order.UserId);
                var authorize1 = allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId) != null && allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).Count() >= 1 ? allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).ElementAt(0) : null;
                var authorize2 = allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId) != null && allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).Count() >= 2 ? allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).ElementAt(1) : null;

                var newItem = new ExpandoObject() as IDictionary<string, Object>;

                newItem.Add("Participant", participant != null && participant.Contact != null ? string.Format("{0}, {1}", participant.Contact.LastName, participant.Contact.FirstName) : string.Empty);

                titles.Add("Participant");

                for (int i = 0; i < schedules.Count; i++)
                {
                    var schedule = schedules.ElementAt(i);
                    if (item.Player.OrderItems.Any(o => o.ProgramScheduleId.Value == schedule.Id))
                    {
                        var title = !string.IsNullOrEmpty(schedule.Title) ? string.Format("{0} ({1}-{2})", schedule.Title, schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat))
                            : string.Format("{0}-{1}", schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat));

                        titles.Add(title);

                        title = string.Format("_{0}", schedule.Id);

                        var value = string.Join(", ", item.Player.OrderItems.Where(o => o.ProgramScheduleId.Value == schedule.Id).Select(o => o.EntryFeeName));

                        newItem.Add(title, value);
                    }
                    else
                    {
                        var title = !string.IsNullOrEmpty(schedule.Title) ? string.Format("{0} ({1}-{2})", schedule.Title, schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat))
                            : string.Format("{0}-{1}", schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat));

                        titles.Add(title);

                        title = string.Format("_{0}", schedule.Id);

                        var value = string.Empty;

                        newItem.Add(title, value);
                    }
                }

                newItem.Add("Gender", participant != null && participant.Contact != null ? (participant.Gender == GenderCategories.Male || participant.Gender == GenderCategories.Female || participant.Gender == GenderCategories.GenderNeutral) ? participant.Gender.ToDescription() : string.Empty : string.Empty);
                newItem.Add("Grade", participant != null && participant.Info != null ? participant.Info.Grade != 0 ? participant.Info.Grade.ToDescription() : string.Empty : string.Empty);
                newItem.Add("Parent_1", parent1 != null && parent1.Contact != null ? string.Format("{0}, {1}", parent1.Contact.LastName, parent1.Contact.FirstName) : string.Empty);
                newItem.Add("Phone_1", parent1 != null && parent1.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(parent1.Contact.Phone) : string.Empty);
                newItem.Add("Email_1", parent1 != null && parent1.Contact != null ? parent1.Contact.Email : string.Empty);
                newItem.Add("Parent_2", parent2 != null && parent2.Contact != null ? string.Format("{0}, {1}", parent2.Contact.LastName, parent2.Contact.FirstName) : string.Empty);
                newItem.Add("Phone_2", parent2 != null && parent2.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(parent2.Contact.Phone) : string.Empty);
                newItem.Add("Email_2", parent2 != null && parent2.Contact != null ? parent2.Contact.Email : string.Empty);
                newItem.Add("Emergency_contact", emergency != null && emergency.Contact != null ? string.Format("{0}, {1}", emergency.Contact.LastName, emergency.Contact.FirstName) : string.Empty);
                newItem.Add("Emergency_contact_phone", emergency != null && emergency.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(emergency.Contact.Phone) : string.Empty);
                newItem.Add("Authorize_adult_1", authorize1 != null && authorize1.Contact != null ? string.Format("{0}, {1}", authorize1.Contact.LastName, authorize1.Contact.FirstName) : string.Empty);
                newItem.Add("Authorize_adult_1_phone", authorize1 != null && authorize1.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorize1.Contact.Phone) : string.Empty);
                newItem.Add("Authorize_adult_2", authorize2 != null && authorize2.Contact != null ? string.Format("{0}, {1}", authorize2.Contact.LastName, authorize2.Contact.FirstName) : string.Empty);
                newItem.Add("Authorize_adult_2_phone", authorize2 != null && authorize2.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorize2.Contact.Phone) : string.Empty);
                newItem.Add("Medical_condition_SSLASH_special_needs", participant != null && participant.Info != null ? participant.Info.SpecialNeeds : string.Empty);
                newItem.Add("Allergies", participant != null && participant.Info != null ? participant.Info.Allergies : string.Empty);

                titles.Add("Gender");
                titles.Add("Grade");
                titles.Add("Parent 1");
                titles.Add("Phone 1");
                titles.Add("Email 1");
                titles.Add("Parent 2");
                titles.Add("Phone 2");
                titles.Add("Email 2");
                titles.Add("Emergency contact");
                titles.Add("Phone");
                titles.Add("Authorize adult 1");
                titles.Add("Phone");
                titles.Add("Authorize adult 2");
                titles.Add("Phone");
                titles.Add("Medical condition/special needs");
                titles.Add("Allergies");

                model.Add(newItem);
            }

            total = query.GroupBy(o => o.PlayerId.Value).Count();

            return model;
        }

        private ParticipantInfo GetParticipantInfo(OrderItem orderItem)
        {
            var result = new ParticipantInfo();
            var formBusiness = Ioc.FormBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var program = orderItem.ProgramSchedule.Program;

            result.ParticipantName = string.Format("{0}, {1}", formBusiness.GetFormValue(orderItem.JbForm, SectionsName.ParticipantSection, ElementsName.LastName), formBusiness.GetFormValue(orderItem.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName));
            result.Grade = formBusiness.GetPlayerGradeAsString(orderItem.JbForm) != null ? formBusiness.GetPlayerGradeAsString(orderItem.JbForm) : string.Empty;
            result.TuitionName = orderItem.EntryFeeName;

            return result;
        }

        [HttpGet]
        public virtual ActionResult GetInternalRosterSchoolReport(string seasonDomain, long? programId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var programs = programBusiness.GetListBySeasonId(season.Id).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4);
            var model = new List<InternalRosterSchoolReportViewModel>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            model = CreateModelInternalRosterSchoolReport(seasonDomain, programId);

            return JsonNet(model);
        }

        [HttpGet]
        public virtual FileResult GenerateInternalRosterSchoolReportPdf(string seasonDomain, long? programId)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var school = Ioc.ClubBusiness.Get(clubId);
            var partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);


            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelInternalRosterSchoolReport(seasonDomain, programId);

            var PdfModel = new InternalRosterReportsSchoolPdfViewModel()
            {
                SchoolLogo = _clubBusiness.GetClubLogoURL(school.Domain, school.Logo),
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model,
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "InternalRosterSchoolReportsPdf", PdfModel);

            return File(file, "application/pdf", "Internal Roster_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        private List<InternalRosterSchoolReportViewModel> CreateModelInternalRosterSchoolReport(string seasonDomain, long? programId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            if (programId != null)
            {
                programs = programBusiness.GetListBySeasonId(season.Id).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Id == programId).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(season.Id).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4).ToList();
            }
            var model = new List<InternalRosterSchoolReportViewModel>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            model = programs != null ? programs.ToList().Select(p => new InternalRosterSchoolReportViewModel()
            {
                ClassName = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                GradesOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(p)) ? programBusiness.GenerateGradeInfoLabelForFlyer(p) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(p)) ? programBusiness.GenerateAgeInfoLabel(p) : null,
                ClassId = p.Id,
                Pending = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == p.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.initiated && o.Order.IsLive).Count(),
                WaitList = p.WaitLists.Count(w => w.IsLive == (w.Program.Season.Status == SeasonStatus.Live)),
                Enrolled = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == p.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive).Count(),
                MinimumEnrollments = (p.TypeCategory == ProgramTypeCategory.Class) && p.ProgramSchedules.Any(s => !s.IsDeleted) ? (p.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0,
                Capacity = (p.TypeCategory == ProgramTypeCategory.Class) && p.ProgramSchedules.Any(s => !s.IsDeleted) ? (p.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.Capacity : 0,
                ClassDays = programBusiness.GetClassDays(p) != null || programBusiness.GetClassDays(p).Any() ? programBusiness.FormatDays(programBusiness.GetClassDays(p).Select(pr => pr.DayOfWeek).ToList()) : "",
                StartTime = programBusiness.GetClassDays(p).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(p).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                StartDate = p.ProgramSchedules.FirstOrDefault().StartDate,
                EndDate = p.ProgramSchedules.FirstOrDefault().StartDate.AddDays(28),
                ProgramDate = string.Format("{0} - {1}", p.ProgramSchedules.FirstOrDefault().StartDate.ToString("MM/dd/yyyy"), p.ProgramSchedules.FirstOrDefault().EndDate.ToString("MM/dd/yyyy")),
                Location = p.Room,
                SchoolName = p.Season.Club.Name,
                SeasonName = p.Season.Domain,
                ProgramParticipants = GetAllParticipantForInternalRosterReport(p).Count > 0 ? GetAllParticipantForInternalRosterReport(p) : null,
                Instructor = programBusiness.GetInstractors(p),

            }).OrderBy(n => n.ClassName).ToList() : null;

            return model;
        }

        private List<InternalRosterReportParticipantsSchoolViewModel> GetAllParticipantForInternalRosterReport(Program program)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == program.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive);
            var model = new List<InternalRosterReportParticipantsSchoolViewModel>();
            var num = 1;

            model = programOrderItems.ToList().Select(o => new InternalRosterReportParticipantsSchoolViewModel()
            {
                ParticipantName = string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm)),
                Parent1Name = o.JbForm != null ? formBusiness.GetParent1Name(o.JbForm) == "" ? "-" : formBusiness.GetParent1Name(o.JbForm) : "",
                AuthorizedPickup1Name = o.JbForm != null ? formBusiness.GetAuthorizedPickup1Name(o.JbForm) == "" ? "-" : formBusiness.GetAuthorizedPickup1Name(o.JbForm) : "",
                EmergencyContactName = o.JbForm != null ? formBusiness.GetEmergencyContactName(o.JbForm) == "" ? "-" : formBusiness.GetEmergencyContactName(o.JbForm) : "",
                Parent1Phone = o.JbForm != null ? formBusiness.GetParent1Phone(o.JbForm) == "" ? "-" : formBusiness.GetParent1Phone(o.JbForm) : "",
                AuthorizedPickup1Phone = o.JbForm != null ? formBusiness.GetAuthorizedPickup1Phone(o.JbForm) == "" ? "-" : formBusiness.GetAuthorizedPickup1Phone(o.JbForm) : "",
                EmergencyContactPhone = o.JbForm != null ? formBusiness.GetEmergencyContactPhone(o.JbForm) == "" ? "-" : formBusiness.GetEmergencyContactPhone(o.JbForm) : "",
                Parent1Email = o.JbForm != null ? formBusiness.GetParent1Email(o.JbForm) == "" ? "-" : formBusiness.GetParent1Email(o.JbForm) : "",
                AuthorizedPickup1Email = o.JbForm != null ? formBusiness.GetAuthorizedPickup1Email(o.JbForm) == "" ? "-" : formBusiness.GetAuthorizedPickup1Email(o.JbForm) : "",
                EmergencyContactEmail = o.JbForm != null ? formBusiness.GetEmergencyContactEmail(o.JbForm) == "" ? "-" : formBusiness.GetEmergencyContactEmail(o.JbForm) : "",
                Allergies = o.JbForm != null ? formBusiness.GetAllergies(o.JbForm) : "",
                MedicalConditions = o.JbForm != null ? formBusiness.GetMedicalConditions(o.JbForm) : "",
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                GradeOrAge = formBusiness.GetPlayerGradeOrAgeAsString(o.JbForm) != null ? formBusiness.GetPlayerGradeOrAgeAsString(o.JbForm) == "Kindergarten" ? "K" : formBusiness.GetPlayerGradeOrAgeAsString(o.JbForm) : string.Empty,
            }).OrderBy(n => n.ParticipantName).ToList();
            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        [HttpGet]
        public virtual ActionResult GetInternalRosterSchoolReport_v2(string seasonDomain, long? scheduleId)
        {
            var model = new List<InternalRosterSchoolReportViewModel>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            model = CreateModelInternalRosterSchoolReport_v2(seasonDomain, scheduleId);

            return JsonNet(model);
        }

        private List<InternalRosterSchoolReportViewModel> CreateModelInternalRosterSchoolReport_v2(string seasonDomain, long? scheduleId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            if (scheduleId != null)
            {
                programs = programBusiness.GetListBySeasonId(season.Id).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted && p.ProgramSchedules.Select(s => s.Id).Contains(scheduleId.Value)).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(season.Id).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted).ToList();
            }
            var model = new List<InternalRosterSchoolReportViewModel>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            var allComboOrderItem = new List<InternalRosterReportParticipantsSchoolViewModel>();

            if (scheduleId.HasValue)
            {

                var schedule = programs.SelectMany(c => c.ProgramSchedules).SingleOrDefault(c => c.Id == scheduleId.Value);

                var allOrderItems = GetAllParticipantForInternalRosterReport_v2(schedule);
                var programOrderItems = GetAllParticipantForInternalRosterReport_v2(schedule.Program);

                if (schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
                {
                    var comboSchedule = schedule.Program.ProgramSchedules.SingleOrDefault(p => p.ScheduleMode == TimeOfClassFormation.Both);

                    var allComboOrderItems = new List<InternalRosterReportParticipantsSchoolViewModel>();

                    if (comboSchedule != null)
                    {
                        allComboOrderItems = GetAllParticipantForInternalRosterReport_v2(comboSchedule);

                        allOrderItems.AddRange(allComboOrderItems);
                    }

                    if (allOrderItems.Any())
                    {
                        allOrderItems = allOrderItems.OrderBy(a => a.ParticipantName).ToList();
                        var beforeAfterOrderItemsModel = SetViewModelInternalRosterReport_v2(schedule, allOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}");
                        model.Add(beforeAfterOrderItemsModel);
                    }

                    var dropinOrderItems = programOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).OrderBy(a => a.ParticipantName).ToList();
                    var dropinOrderItemsModel = SetViewModelInternalRosterReport_v2(schedule, dropinOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).ToList(), $"{schedule.Program.Name} - Drop-in");
                    model.Add(dropinOrderItemsModel);

                    var punchcardOrderItems = programOrderItems.Where(a => a.Mode == OrderItemMode.PunchCard).OrderBy(a => a.ParticipantName).ToList();
                    var punchcardOrderItemsModel = SetViewModelInternalRosterReport_v2(schedule, punchcardOrderItems.Where(a => a.Mode == OrderItemMode.PunchCard).ToList(), $"{schedule.Program.Name} - Punchcard");
                    model.Add(punchcardOrderItemsModel);

                    if (model.Any())
                    {
                        allOrderItems = allOrderItems.OrderBy(a => a.ParticipantName).ToList();
                        foreach (var item in model)
                        {
                            if (item.ProgramParticipants != null)
                            {
                                setRowNumberInternalRosterReport_v2(item.ProgramParticipants);
                            }
                        }
                    }

                    return model;
                }

                if (allOrderItems.Any())
                {
                    allOrderItems = allOrderItems.OrderBy(a => a.ParticipantName).ToList();
                    var orderItemsModel = SetViewModelInternalRosterReport_v2(schedule, allOrderItems.ToList(), !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Program.Name} - {schedule.Title}" : schedule.Program.Name);

                    model.Add(orderItemsModel);
                }

                if (model.Any())
                {
                    foreach (var item in model)
                    {
                        if (item.ProgramParticipants != null)
                        {
                            setRowNumberInternalRosterReport_v2(item.ProgramParticipants);
                        }
                    }
                }

                return model;
            }


            var beforeAfterPrograms = programs.Where(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).ToList();

            foreach (var program in beforeAfterPrograms)
            {
                var programOrderItems = GetAllParticipantForInternalRosterReport_v2(program);

                var comboSchedule = program.ProgramSchedules.SingleOrDefault(p => p.ScheduleMode == TimeOfClassFormation.Both);

                var allComboOrderItems = new List<InternalRosterReportParticipantsSchoolViewModel>();

                if (comboSchedule != null)
                {
                    allComboOrderItems = GetAllParticipantForInternalRosterReport_v2(comboSchedule);
                }

                foreach (var schedule in program.ProgramSchedules)
                {
                    if (schedule.ScheduleMode == TimeOfClassFormation.AM)
                    {
                        var allBeforeOrderItems = GetAllParticipantForInternalRosterReport_v2(schedule);
                        allBeforeOrderItems.AddRange(allComboOrderItems);

                        if (allBeforeOrderItems.Any())
                        {
                            allBeforeOrderItems = allBeforeOrderItems.OrderBy(a => a.ParticipantName).ToList();
                            var beforeAfterOrderItemsModel = SetViewModelInternalRosterReport_v2(schedule, allBeforeOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}");
                            model.Add(beforeAfterOrderItemsModel);
                        }


                    }
                    else if (schedule.ScheduleMode == TimeOfClassFormation.PM)
                    {
                        var allAfterOrderItems = GetAllParticipantForInternalRosterReport_v2(schedule);
                        allAfterOrderItems.AddRange(allComboOrderItems);

                        if (allAfterOrderItems.Any())
                        {
                            allAfterOrderItems = allAfterOrderItems.OrderBy(a => a.ParticipantName).ToList();
                            var beforeAfterOrderItemsModel = SetViewModelInternalRosterReport_v2(schedule, allAfterOrderItems.Where(a => a.Mode == OrderItemMode.Noraml).ToList(), $"{schedule.Program.Name} - {schedule.Title}");
                            model.Add(beforeAfterOrderItemsModel);
                        }



                    }
                }

                var dropinOrderItems = programOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).OrderBy(a => a.ParticipantName).ToList();
                var dropinOrderItemsModel = SetViewModelInternalRosterReport_v2(program, dropinOrderItems.Where(a => a.Mode == OrderItemMode.DropIn).ToList(), $"{program.Name} - Drop-in");
                model.Add(dropinOrderItemsModel);

                var punchcardOrderItems = programOrderItems.Where(a => a.Mode == OrderItemMode.PunchCard).OrderBy(a => a.ParticipantName).ToList();
                var punchcardOrderItemsModel = SetViewModelInternalRosterReport_v2(program, punchcardOrderItems.Where(a => a.Mode == OrderItemMode.PunchCard).ToList(), $"{program.Name} - Punchcard");
                model.Add(punchcardOrderItemsModel);

            }

            programs.RemoveAll(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare);

            foreach (var program in programs)
            {
                foreach (var schedule in program.ProgramSchedules)
                {
                    var allOrderItems = GetAllParticipantForInternalRosterReport_v2(schedule);

                    if (allOrderItems.Any())
                    {
                        allOrderItems = allOrderItems.OrderBy(a => a.ParticipantName).ToList();

                        var orderItemsModel = SetViewModelInternalRosterReport_v2(schedule, allOrderItems.ToList(), !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Program.Name} - {schedule.Title}" : schedule.Program.Name);

                        model.Add(orderItemsModel);
                    }
                }
            }

            if (model.Any())
            {
                foreach (var item in model)
                {
                    if (item.ProgramParticipants != null)
                    {
                        setRowNumberInternalRosterReport_v2(item.ProgramParticipants);
                    }
                }
            }

            model = model.OrderBy(m => m.ClassName).ToList();

            return model;
        }

        private List<InternalRosterReportParticipantsSchoolViewModel> GetAllParticipantForInternalRosterReport_v2(ProgramSchedule programSchedule)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var orderSessionBusiness = Ioc.OrderSessionBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var playerProfileBusiness = Ioc.PlayerProfileBusiness;
            var num = 1;

            var programOrderItems = new List<OrderItem>();

            if (programSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || programSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                programOrderItems.AddRange(Ioc.NewReportBusiness.GetReportOrderItemsWithEffectiveDate(programSchedule.Id).Distinct());
            }
            else if (programSchedule.Program.TypeCategory == ProgramTypeCategory.Camp)
            {
                programOrderItems.AddRange(orderItemBusiness.GetReportOrderItemsBySchedule(programSchedule.Id).ToList());
            }
            else
            {
                programOrderItems.AddRange(orderItemBusiness.GetReportOrderItemsBySchedule(programSchedule.Id).ToList());
            }

            var userIds = programOrderItems.Select(a => a.Order.UserId).ToList();

            var allParents = playerProfileBusiness.GetAllParentsProfile(userIds);
            var allEmergencyContacts = playerProfileBusiness.GetAllEmergencyFamily(userIds);
            var allAuthorizePickups = playerProfileBusiness.GetAllAuthorizePickupFamily(userIds);

            var model = new List<InternalRosterReportParticipantsSchoolViewModel>();


            foreach (var orderItem in programOrderItems)
            {
                var parents = allParents.Where(a => a.UserId == orderItem.Player.UserId);
                var emergencyContacts = allEmergencyContacts.Where(e => e.Family.UserId == orderItem.Player.UserId).ToList();
                var authorizedPickups = allAuthorizePickups.Where(e => e.Family.UserId == orderItem.Player.UserId).ToList();

                model.Add(new InternalRosterReportParticipantsSchoolViewModel()
                {
                    ParticipantName = orderItem.Player?.Contact != null ? $"{orderItem.Player.Contact.LastName}, {orderItem.Player.Contact.FirstName}" : "",
                    PermissionPhotography = orderItem.Player?.Info != null && orderItem.Player.Info.HasPermissionPhotography.HasValue ? orderItem.Player.Info.HasPermissionPhotography.Value ? "Yes" : "No" : "",
                    Allergies = orderItem.Player?.Info != null ? orderItem.Player.Info.Allergies : "",
                    MedicalConditions = orderItem.Player?.Info != null ? orderItem.Player.Info.SpecialNeeds : "",
                    GradeOrAge = orderItem.Player?.Info?.Grade != null ? orderItem.Player.Info.Grade.ToAbbreviation() : orderItem.Player.Contact?.DoB != null ? DateTimeHelper.CalculateAge(orderItem.Player.Contact.DoB.Value).ToString() : "",
                    Mode = orderItem.Mode,

                    Parent1Name = parents.Count() > 0 && parents.First().Contact != null ? $"{parents.First().Contact.LastName}, {parents.First().Contact.FirstName}" : "-",
                    Parent1Phone = parents.Count() > 0 && parents.First().Contact != null && !string.IsNullOrEmpty(parents.First().Contact.Phone) ? PhoneNumberHelper.FormatPhoneNumber(parents.First().Contact.Phone) : "-",
                    Parent1Email = parents.Count() > 0 && parents.First().Contact != null ? parents.First().Contact.Email : "-",

                    Parent2Name = parents.Count() > 1 && parents.ElementAt(1).Contact != null ? $"{parents.ElementAt(1).Contact.LastName}, {parents.ElementAt(1).Contact.FirstName}" : "-",
                    Parent2Phone = parents.Count() > 1 && parents.ElementAt(1).Contact != null && !string.IsNullOrEmpty(parents.ElementAt(1).Contact.Phone) ? PhoneNumberHelper.FormatPhoneNumber(parents.ElementAt(1).Contact.Phone) : "-",

                    AuthorizedPickup1Name = authorizedPickups.Count() > 0 && authorizedPickups.First().Contact != null ? $"{authorizedPickups.First().Contact.LastName}, {authorizedPickups.First().Contact.FirstName}" : "-",
                    AuthorizedPickup1Phone = authorizedPickups.Count() > 0 && authorizedPickups.First().Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorizedPickups.First().Contact.Phone) : "-",
                    AuthorizedPickup1Email = authorizedPickups.Count() > 0 && authorizedPickups.First().Contact != null ? authorizedPickups.First().Contact.Email : "-",
                    AuthorizedPickup2Name = authorizedPickups.Count() > 1 && authorizedPickups.ElementAt(1).Contact != null ? $"{authorizedPickups.ElementAt(1).Contact.LastName}, {authorizedPickups.ElementAt(1).Contact.FirstName}" : "-",
                    AuthorizedPickup3Name = authorizedPickups.Count() > 2 && authorizedPickups.ElementAt(2).Contact != null ? $"{authorizedPickups.ElementAt(2).Contact.LastName}, {authorizedPickups.ElementAt(2).Contact.FirstName}" : "-",

                    EmergencyContactName = authorizedPickups.Count() > 0 && authorizedPickups.Any(a => a.IsEmergencyContact) && authorizedPickups.First(a => a.IsEmergencyContact).Contact != null ? $"{authorizedPickups.First(a => a.IsEmergencyContact).Contact.LastName}, {authorizedPickups.First(a => a.IsEmergencyContact).Contact.FirstName}" :
                        emergencyContacts.Count() > 0 && emergencyContacts.First().Contact != null ? $"{emergencyContacts.First().Contact.LastName}, {emergencyContacts.First().Contact.FirstName}" : "-",
                    EmergencyContactPhone = authorizedPickups.Count() > 0 && authorizedPickups.Any(a => a.IsEmergencyContact) && authorizedPickups.First(a => a.IsEmergencyContact).Contact != null && !string.IsNullOrEmpty(authorizedPickups.First(a => a.IsEmergencyContact).Contact.Phone) ? PhoneNumberHelper.FormatPhoneNumber(authorizedPickups.First(a => a.IsEmergencyContact).Contact.Phone) :
                        emergencyContacts.Count() > 0 && emergencyContacts.First().Contact != null && !string.IsNullOrEmpty(emergencyContacts.First().Contact.Phone) ? PhoneNumberHelper.FormatPhoneNumber(emergencyContacts.First().Contact.Phone) : "-",
                    EmergencyContactEmail = authorizedPickups.Count() > 0 && authorizedPickups.Any(a => a.IsEmergencyContact) && authorizedPickups.First(a => a.IsEmergencyContact).Contact != null ? authorizedPickups.First(a => a.IsEmergencyContact).Contact.Email :
                        emergencyContacts.Count() > 0 && emergencyContacts.First().Contact != null ? emergencyContacts.First().Contact.Email : "-"
                });
            }

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        private List<InternalRosterReportParticipantsSchoolViewModel> GetAllParticipantForInternalRosterReport_v2(Program program)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var orderSessionBusiness = Ioc.OrderSessionBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var playerProfileBusiness = Ioc.PlayerProfileBusiness;
            var num = 1;

            var programOrderItems = orderItemBusiness.GetProgramOrderItems(program.Id).ToList();

            var userIds = programOrderItems.Select(a => a.Order.UserId).ToList();

            var allParents = playerProfileBusiness.GetAllParentsProfile(userIds);
            var allEmergencyContacts = playerProfileBusiness.GetAllEmergencyFamily(userIds);
            var allAuthorizePickups = playerProfileBusiness.GetAllAuthorizePickupFamily(userIds);

            var model = new List<InternalRosterReportParticipantsSchoolViewModel>();


            foreach (var orderItem in programOrderItems)
            {
                var parents = allParents.Where(a => a.UserId == orderItem.Player.UserId);
                var emergencyContacts = allEmergencyContacts.Where(e => e.Family.UserId == orderItem.Player.UserId).ToList();
                var authorizedPickups = allAuthorizePickups.Where(e => e.Family.UserId == orderItem.Player.UserId).ToList();

                model.Add(new InternalRosterReportParticipantsSchoolViewModel()
                {
                    ParticipantName = orderItem.Player?.Contact != null ? $"{orderItem.Player.Contact.LastName}, {orderItem.Player.Contact.FirstName}" : "",
                    PermissionPhotography = orderItem.Player?.Info != null && orderItem.Player.Info.HasPermissionPhotography.HasValue ? orderItem.Player.Info.HasPermissionPhotography.Value ? "Yes" : "No" : "",
                    Allergies = orderItem.Player?.Info != null ? orderItem.Player.Info.Allergies : "",
                    MedicalConditions = orderItem.Player?.Info != null ? orderItem.Player.Info.SpecialNeeds : "",
                    GradeOrAge = orderItem.Player?.Info?.Grade != null ? orderItem.Player.Info.Grade.ToAbbreviation() : orderItem.Player.Contact?.DoB != null ? DateTimeHelper.CalculateAge(orderItem.Player.Contact.DoB.Value).ToString() : "",
                    Mode = orderItem.Mode,

                    Parent1Name = parents.Count() > 0 && parents.First().Contact != null ? $"{parents.First().Contact.LastName}, {parents.First().Contact.FirstName}" : "-",
                    Parent1Phone = parents.Count() > 0 && parents.First().Contact != null && !string.IsNullOrEmpty(parents.First().Contact.Phone) ? PhoneNumberHelper.FormatPhoneNumber(parents.First().Contact.Phone) : "-",
                    Parent1Email = parents.Count() > 0 && parents.First().Contact != null ? parents.First().Contact.Email : "-",

                    Parent2Name = parents.Count() > 1 && parents.ElementAt(1).Contact != null ? $"{parents.ElementAt(1).Contact.LastName}, {parents.ElementAt(1).Contact.FirstName}" : "-",
                    Parent2Phone = parents.Count() > 1 && parents.ElementAt(1).Contact != null && !string.IsNullOrEmpty(parents.ElementAt(1).Contact.Phone) ? PhoneNumberHelper.FormatPhoneNumber(parents.ElementAt(1).Contact.Phone) : "-",

                    AuthorizedPickup1Name = authorizedPickups.Count() > 0 && authorizedPickups.First().Contact != null ? $"{authorizedPickups.First().Contact.LastName}, {authorizedPickups.First().Contact.FirstName}" : "-",
                    AuthorizedPickup1Phone = authorizedPickups.Count() > 0 && authorizedPickups.First().Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorizedPickups.First().Contact.Phone) : "-",
                    AuthorizedPickup1Email = authorizedPickups.Count() > 0 && authorizedPickups.First().Contact != null ? authorizedPickups.First().Contact.Email : "-",
                    AuthorizedPickup2Name = authorizedPickups.Count() > 1 && authorizedPickups.ElementAt(1).Contact != null ? $"{authorizedPickups.ElementAt(1).Contact.LastName}, {authorizedPickups.ElementAt(1).Contact.FirstName}" : "-",
                    AuthorizedPickup3Name = authorizedPickups.Count() > 2 && authorizedPickups.ElementAt(2).Contact != null ? $"{authorizedPickups.ElementAt(2).Contact.LastName}, {authorizedPickups.ElementAt(2).Contact.FirstName}" : "-",

                    EmergencyContactName = authorizedPickups.Count() > 0 && authorizedPickups.Any(a => a.IsEmergencyContact) && authorizedPickups.First(a => a.IsEmergencyContact).Contact != null ? $"{authorizedPickups.First(a => a.IsEmergencyContact).Contact.LastName}, {authorizedPickups.First(a => a.IsEmergencyContact).Contact.FirstName}" :
                        emergencyContacts.Count() > 0 && emergencyContacts.First().Contact != null ? $"{emergencyContacts.First().Contact.LastName}, {emergencyContacts.First().Contact.FirstName}" : "-",
                    EmergencyContactPhone = authorizedPickups.Count() > 0 && authorizedPickups.Any(a => a.IsEmergencyContact) && authorizedPickups.First(a => a.IsEmergencyContact).Contact != null && !string.IsNullOrEmpty(authorizedPickups.First(a => a.IsEmergencyContact).Contact.Phone) ? PhoneNumberHelper.FormatPhoneNumber(authorizedPickups.First(a => a.IsEmergencyContact).Contact.Phone) :
                        emergencyContacts.Count() > 0 && emergencyContacts.First().Contact != null && !string.IsNullOrEmpty(emergencyContacts.First().Contact.Phone) ? PhoneNumberHelper.FormatPhoneNumber(emergencyContacts.First().Contact.Phone) : "-",
                    EmergencyContactEmail = authorizedPickups.Count() > 0 && authorizedPickups.Any(a => a.IsEmergencyContact) && authorizedPickups.First(a => a.IsEmergencyContact).Contact != null ? authorizedPickups.First(a => a.IsEmergencyContact).Contact.Email :
                        emergencyContacts.Count() > 0 && emergencyContacts.First().Contact != null ? emergencyContacts.First().Contact.Email : "-"
                });
            }

            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        private static void setRowNumberInternalRosterReport_v2(List<InternalRosterReportParticipantsSchoolViewModel> participants)
        {
            var num = 1;
            foreach (var participant in participants)
            {
                participant.Number = num;
                num++;
            }
        }

        private InternalRosterSchoolReportViewModel SetViewModelInternalRosterReport_v2(ProgramSchedule schedule, List<InternalRosterReportParticipantsSchoolViewModel> allOrderItems, string programName)
        {

            return new InternalRosterSchoolReportViewModel()
            {
                ClassName = programName,
                ClassId = schedule.Id,
                StartDate = schedule.StartDate,
                EndDate = schedule.StartDate.AddDays(28),
                SchoolName = schedule.Program.Season.Club.Name,
                ProgramParticipants = allOrderItems.Count > 0 ? allOrderItems : null
            };
        }

        private InternalRosterSchoolReportViewModel SetViewModelInternalRosterReport_v2(Program program, List<InternalRosterReportParticipantsSchoolViewModel> allOrderItems, string programName)
        {

            return new InternalRosterSchoolReportViewModel()
            {
                ClassName = programName,
                ProgramParticipants = allOrderItems.Count > 0 ? allOrderItems : null
            };
        }

        [HttpGet]
        public virtual FileResult GenerateInternalRosterSchoolReportPdf_v2(string seasonDomain, long? scheduleId)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var partner = new Club();
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var showSchoolLogo = true;

            var school = Ioc.ClubBusiness.Get(clubId);

            if (school.PartnerId.HasValue)
            {
                partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);
            }
            else
            {
                partner = Ioc.ClubBusiness.Get(school.Id);
            }

            if (school.Setting != null)
            {
                showSchoolLogo = school.Setting.ShowSchoolLogo;
            }

            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelInternalRosterSchoolReport_v2(seasonDomain, scheduleId);

            var PdfModel = new InternalRosterReportsSchoolPdfViewModel()
            {
                SchoolLogo = showSchoolLogo ? _clubBusiness.GetClubLogoURL(school.Domain, school.Logo) : null,
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model,
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "InternalRosterSchoolReportsPdf_v2", PdfModel);

            return File(file, "application/pdf", "Internal Roster_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }


        [HttpGet]
        public virtual ActionResult GetInternalRosterSchoolReportV3(string seasonDomain, long? programId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var programs = programBusiness.GetListBySeasonId(season.Id).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4);
            var model = new List<InternalRosterSchoolReportViewModel>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            model = CreateModelInternalRosterSchoolReportV3(seasonDomain, programId);

            return JsonNet(model);
        }

        private List<InternalRosterSchoolReportViewModel> CreateModelInternalRosterSchoolReportV3(string seasonDomain, long? programId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            if (programId != null)
            {
                programs = programBusiness.GetListBySeasonId(season.Id).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Id == programId).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(season.Id).Where(p => p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4).ToList();
            }
            var model = new List<InternalRosterSchoolReportViewModel>();
            var orderItemBusiness = Ioc.OrderItemBusiness;

            model = programs != null ? programs.ToList().Select(p => new InternalRosterSchoolReportViewModel()
            {
                ClassName = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                GradesOrAges = !string.IsNullOrEmpty(programBusiness.GenerateGradeInfoLabelForFlyer(p)) ? programBusiness.GenerateGradeInfoLabelForFlyer(p) : !string.IsNullOrEmpty(programBusiness.GenerateAgeInfoLabel(p)) ? programBusiness.GenerateAgeInfoLabel(p) : null,
                ClassId = p.Id,
                Pending = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == p.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.initiated && o.Order.IsLive).Count(),
                WaitList = p.WaitLists.Count(w => w.IsLive == (w.Program.Season.Status == SeasonStatus.Live)),
                Enrolled = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == p.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive).Count(),
                MinimumEnrollments = (p.TypeCategory == ProgramTypeCategory.Class) && p.ProgramSchedules.Any(s => !s.IsDeleted) ? (p.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0,
                Capacity = (p.TypeCategory == ProgramTypeCategory.Class) && p.ProgramSchedules.Any(s => !s.IsDeleted) ? (p.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.Capacity : 0,
                ClassDays = programBusiness.GetClassDays(p) != null || programBusiness.GetClassDays(p).Any() ? programBusiness.FormatDays(programBusiness.GetClassDays(p).Select(pr => pr.DayOfWeek).ToList()) : "",
                StartTime = programBusiness.GetClassDays(p).FirstOrDefault().StartTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault().StartTime.Value).ToString("h:mm tt") : "",
                EndTime = programBusiness.GetClassDays(p).FirstOrDefault().EndTime.HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(p).FirstOrDefault().EndTime.Value).ToString("h:mm tt") : "",
                StartDate = p.ProgramSchedules.FirstOrDefault().StartDate,
                EndDate = p.ProgramSchedules.FirstOrDefault().StartDate.AddDays(28),
                ProgramDate = string.Format("{0} - {1}", p.ProgramSchedules.FirstOrDefault().StartDate.ToString("MM/dd/yyyy"), p.ProgramSchedules.FirstOrDefault().EndDate.ToString("MM/dd/yyyy")),
                Location = p.Room,
                SchoolName = p.Season.Club.Name,
                SeasonName = p.Season.Domain,
                ProgramParticipants = GetAllParticipantForInternalRosterReportV3(p).Count > 0 ? GetAllParticipantForInternalRosterReportV3(p) : null,
                Instructor = programBusiness.GetInstractors(p, true),

            }).OrderBy(n => n.ClassName).ToList() : null;

            return model;
        }

        private List<InternalRosterReportParticipantsSchoolViewModel> GetAllParticipantForInternalRosterReportV3(Program program)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var formBusiness = Ioc.FormBusiness;
            var programOrderItems = orderItemBusiness.GetList().Where(o => o.ProgramSchedule.ProgramId == program.Id && o.ProgramScheduleId.HasValue && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive);
            var model = new List<InternalRosterReportParticipantsSchoolViewModel>();
            var num = 1;

            model = programOrderItems.ToList().Select(o => new InternalRosterReportParticipantsSchoolViewModel()
            {
                ParticipantName = string.Format("{0}, {1}", formBusiness.GetPlayerLastName(o.JbForm), formBusiness.GetPlayerFirstName(o.JbForm)),
                Parent1Name = o.JbForm != null ? formBusiness.GetParent1Name(o.JbForm) == "" ? "-" : formBusiness.GetParent1Name(o.JbForm) : "",
                Parent1Phone = o.JbForm != null ? formBusiness.GetParent1Phone(o.JbForm) == "" ? "-" : formBusiness.GetParent1Phone(o.JbForm) : "",
                Parent1Email = o.JbForm != null ? formBusiness.GetParent1Email(o.JbForm) == "" ? "-" : formBusiness.GetParent1Email(o.JbForm) : "",
                Parent2Name = o.JbForm != null ? formBusiness.GetParent2Name(o.JbForm) == "" ? "-" : formBusiness.GetParent2Name(o.JbForm) : "",
                Parent2Phone = o.JbForm != null ? formBusiness.GetParent2Phone(o.JbForm) == "" ? "-" : formBusiness.GetParent2Phone(o.JbForm) : "",
                Parent2Email = o.JbForm != null ? formBusiness.GetParent2Email(o.JbForm) == "" ? "-" : formBusiness.GetParent2Email(o.JbForm) : "",
                Allergies = o.JbForm != null ? formBusiness.GetAllergies(o.JbForm) : "",
                MedicalConditions = o.JbForm != null ? formBusiness.GetMedicalConditions(o.JbForm) : "",
                TransportHome = o.JbForm != null ? formBusiness.GetDismissalFromEnrichment(o.JbForm) == "" ? "-" : formBusiness.GetDismissalFromEnrichment(o.JbForm) : "",
                TeacherName = o.JbForm != null ? formBusiness.GetTeacher(o.JbForm) == "" ? "-" : formBusiness.GetTeacher(o.JbForm) : "",
                Treatments = o.JbForm != null ? formBusiness.GetPlayerSelfAdministerMedicationDescription(o.JbForm) == "" ? "-" : formBusiness.GetPlayerSelfAdministerMedicationDescription(o.JbForm) : "",
            }).OrderBy(n => n.ParticipantName).ToList();
            foreach (var participant in model)
            {
                participant.Number = num;
                num++;
            }

            return model;
        }

        [HttpGet]
        public virtual FileResult GenerateInternalRosterSchoolReportPdfV3(string seasonDomain, long? programId)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var school = Ioc.ClubBusiness.Get(clubId);
            var partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);


            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelInternalRosterSchoolReportV3(seasonDomain, programId);

            var PdfModel = new InternalRosterReportsSchoolPdfViewModel()
            {
                SchoolLogo = _clubBusiness.GetClubLogoURL(school.Domain, school.Logo),
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model,
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "InternalRosterSchoolReportsPdf_v3", PdfModel);

            return File(file, "application/pdf", "Internal Roster_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        [HttpGet]
        public virtual FileContentResult GenerateInternalRosterReportExcelV3(string seasonDomain, long? programId)
        {
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var model = CreateModelInternalRosterSchoolReportV3(seasonDomain, programId);

            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();


            //Define Style
            ICellStyle alignmentCenterStyle = workbook.CreateCellStyle();
            alignmentCenterStyle.Alignment = HorizontalAlignment.Center;
            alignmentCenterStyle.VerticalAlignment = VerticalAlignment.Center;

            var boldFont = workbook.CreateFont();
            boldFont.Boldweight = (short)FontBoldWeight.Bold;

            var borderTopStyle = workbook.CreateCellStyle();
            borderTopStyle.BorderTop = BorderStyle.Thin;
            borderTopStyle.Alignment = HorizontalAlignment.Center;
            borderTopStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyBoldStyle = workbook.CreateCellStyle();
            greyBoldStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyBoldStyle.FillPattern = FillPattern.SolidForeground;
            greyBoldStyle.SetFont(boldFont);
            greyBoldStyle.Alignment = HorizontalAlignment.Left;
            greyBoldStyle.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleLeftAlignment = workbook.CreateCellStyle();
            greyStyleLeftAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleLeftAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleLeftAlignment.Alignment = HorizontalAlignment.Left;
            greyStyleLeftAlignment.VerticalAlignment = VerticalAlignment.Center;

            var greyStyleCenterAlignment = workbook.CreateCellStyle();
            greyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            greyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            greyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            greyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;

            var borderTopGreyStyleCenterAlignment = workbook.CreateCellStyle();
            borderTopGreyStyleCenterAlignment.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
            borderTopGreyStyleCenterAlignment.FillPattern = FillPattern.SolidForeground;
            borderTopGreyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            borderTopGreyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;
            borderTopGreyStyleCenterAlignment.BorderTop = BorderStyle.Thin;
            borderTopGreyStyleCenterAlignment.Alignment = HorizontalAlignment.Center;
            borderTopGreyStyleCenterAlignment.VerticalAlignment = VerticalAlignment.Center;
            //greyStyleCenterAlignment.BorderBottom = BorderStyle.MEDIUM;


            sheet.SetDefaultColumnStyle(0, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(1, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(2, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(3, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(4, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(5, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(6, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(7, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(8, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(9, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(10, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(11, alignmentCenterStyle);
            sheet.SetDefaultColumnStyle(12, alignmentCenterStyle);


            var rowNumber = 0;
            var itemNumber = 0;

            foreach (var item in model)
            {
                if (item.ProgramParticipants == null)
                {
                    continue;
                }

                //**** Header ****////
                var headerRow = sheet.CreateRow(rowNumber);

                if (itemNumber != 0)
                {
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                    rowNumber++;
                    headerRow = sheet.CreateRow(rowNumber);
                }
                itemNumber++;

                // set default column size and color
                headerRow.CreateCell(0).SetCellValue(" ");
                headerRow.CreateCell(1).SetCellValue(" ");
                headerRow.CreateCell(2).SetCellValue(" ");
                headerRow.CreateCell(3).SetCellValue(" ");
                headerRow.CreateCell(4).SetCellValue(" ");
                headerRow.CreateCell(5).SetCellValue(" ");
                headerRow.CreateCell(6).SetCellValue(" ");
                headerRow.CreateCell(7).SetCellValue(" ");
                headerRow.CreateCell(8).SetCellValue(" ");
                headerRow.CreateCell(9).SetCellValue(" ");
                headerRow.CreateCell(10).SetCellValue(" ");
                headerRow.GetCell(0).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(2).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(4).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(6).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(7).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(8).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(9).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(10).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                headerRow.GetCell(1).Sheet.SetColumnWidth(1, 20 * 256);
                headerRow.GetCell(2).Sheet.SetColumnWidth(2, 20 * 256);
                headerRow.GetCell(3).Sheet.SetColumnWidth(3, 20 * 256);
                headerRow.GetCell(4).Sheet.SetColumnWidth(4, 20 * 256);
                headerRow.GetCell(5).Sheet.SetColumnWidth(5, 30 * 256);
                headerRow.GetCell(6).Sheet.SetColumnWidth(6, 30 * 256);
                headerRow.GetCell(7).Sheet.SetColumnWidth(7, 30 * 256);
                headerRow.GetCell(8).Sheet.SetColumnWidth(8, 20 * 256);
                headerRow.GetCell(9).Sheet.SetColumnWidth(9, 20 * 256);
                headerRow.GetCell(10).Sheet.SetColumnWidth(10, 20 * 256);

                //Set the column names in the header row
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Class: {item.ClassName}");
                headerRow.CreateCell(3).SetCellValue($"Time period: {item.SeasonName}");
                headerRow.CreateCell(5).SetCellValue($"Minimum enrollments: {item.MinimumEnrollments}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Grades/Ages: {item.GradesOrAges}");
                headerRow.CreateCell(3).SetCellValue($"Primary instructor: {item.Instructor}");
                headerRow.CreateCell(5).SetCellValue($"Capacity: {item.Capacity}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"School: {item.SchoolName}");
                headerRow.CreateCell(3).SetCellValue($"Facility/Room: {item.Location}");
                headerRow.CreateCell(5).SetCellValue($"Enrolled: {item.Enrolled}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(3).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Days: {item.ClassDays}");
                headerRow.CreateCell(5).SetCellValue($"Pending: {item.Pending}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);
                headerRow.CreateCell(1).SetCellValue($"Time: {item.StrTime}");
                headerRow.CreateCell(5).SetCellValue($"WaitList: {item.WaitList}");
                headerRow.RowStyle = greyStyleLeftAlignment;
                headerRow.GetCell(1).CellStyle = greyStyleLeftAlignment;
                headerRow.GetCell(5).CellStyle = greyStyleLeftAlignment;

                //line space between header and content
                rowNumber++;
                headerRow = sheet.CreateRow(rowNumber);



                //*** Content ***//
                // static header
                rowNumber++;
                var row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("#");
                row.CreateCell(1).SetCellValue("Participant");
                row.CreateCell(2).SetCellValue("Contact name");
                row.CreateCell(3).SetCellValue("Phone");
                row.CreateCell(4).SetCellValue("Email");
                row.CreateCell(5).SetCellValue("Allergies");
                row.CreateCell(6).SetCellValue("Medical conditions");
                row.CreateCell(7).SetCellValue("Treatments");
                row.CreateCell(8).SetCellValue("Teacher");
                row.CreateCell(9).SetCellValue("Dismissal");
                row.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                row.GetCell(4).CellStyle = greyStyleCenterAlignment;
                row.GetCell(5).CellStyle = greyStyleCenterAlignment;
                row.GetCell(6).CellStyle = greyStyleCenterAlignment;
                row.GetCell(7).CellStyle = greyStyleCenterAlignment;
                row.GetCell(8).CellStyle = greyStyleCenterAlignment;
                row.GetCell(9).CellStyle = greyStyleCenterAlignment;

                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                row.CreateCell(0).SetCellValue("#");
                row.GetCell(0).CellStyle = greyStyleCenterAlignment;
                row.GetCell(0).Sheet.SetColumnWidth(0, 10 * 256);
                row.CreateCell(1).SetCellValue("Participant");
                row.GetCell(1).CellStyle = greyStyleCenterAlignment;
                row.GetCell(1).Sheet.SetColumnWidth(0, 10 * 256);
                row.CreateCell(2).SetCellValue("Parent 1");
                row.CreateCell(3).SetCellValue("Parent 1");
                row.CreateCell(4).SetCellValue("Parent 1");
                row.CreateCell(5).SetCellValue("Allergies");
                row.CreateCell(6).SetCellValue("Medical conditions");
                row.CreateCell(7).SetCellValue("Treatments");
                row.CreateCell(8).SetCellValue("Teacher");
                row.CreateCell(9).SetCellValue("Dismissal");
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 1, 1));
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 5, 5));
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 6, 6));
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 7, 7));
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 8, 8));
                row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 9, 9));

                row.GetCell(0).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(1).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(2).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(3).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(4).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(5).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(6).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(7).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(8).CellStyle = borderTopGreyStyleCenterAlignment;
                row.GetCell(9).CellStyle = borderTopGreyStyleCenterAlignment;

                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                row.CreateCell(2).SetCellValue("Parent 2");
                row.CreateCell(3).SetCellValue("Parent 2");
                row.CreateCell(4).SetCellValue("Parent 2");
                row.GetCell(2).CellStyle = greyStyleCenterAlignment;
                row.GetCell(3).CellStyle = greyStyleCenterAlignment;
                row.GetCell(4).CellStyle = greyStyleCenterAlignment;

                //Content
                if (item.ProgramParticipants != null)
                {
                    foreach (var participant in item.ProgramParticipants)
                    {

                        rowNumber++;
                        row = sheet.CreateRow(rowNumber);
                        row.CreateCell(0).SetCellValue(participant.Number);
                        row.GetCell(0).CellStyle = alignmentCenterStyle;
                        row.GetCell(0).CellStyle = borderTopStyle;

                        row.CreateCell(1).SetCellValue(participant.ParticipantName);
                        row.CreateCell(2).SetCellValue(participant.Parent1Name);
                        row.CreateCell(3).SetCellValue(participant.Parent1Phone);
                        row.CreateCell(4).SetCellValue(participant.Parent1Email);
                        row.CreateCell(5).SetCellValue(participant.Allergies);
                        row.CreateCell(6).SetCellValue(participant.MedicalConditions);
                        row.CreateCell(7).SetCellValue(participant.Treatments);
                        row.CreateCell(8).SetCellValue(participant.TeacherName);
                        row.CreateCell(9).SetCellValue(participant.TransportHome);

                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 0, 0));
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 1, 1));
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 5, 5));
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 6, 6));
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 7, 7));
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 8, 8));
                        row.Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowNumber, rowNumber + 1, 9, 9));

                        row.GetCell(1).CellStyle = borderTopStyle;
                        row.GetCell(2).CellStyle = borderTopStyle;
                        row.GetCell(3).CellStyle = borderTopStyle;
                        row.GetCell(4).CellStyle = borderTopStyle;
                        row.GetCell(5).CellStyle = borderTopStyle;
                        row.GetCell(6).CellStyle = borderTopStyle;
                        row.GetCell(7).CellStyle = borderTopStyle;
                        row.GetCell(8).CellStyle = borderTopStyle;
                        row.GetCell(9).CellStyle = borderTopStyle;

                        rowNumber++;
                        row = sheet.CreateRow(rowNumber);
                        row.CreateCell(2).SetCellValue(participant.Parent2Name);
                        row.CreateCell(3).SetCellValue(participant.Parent2Phone);
                        row.CreateCell(4).SetCellValue(participant.Parent2Email);



                    }
                }
                else
                {
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.CreateCell(0).SetCellValue(string.Empty);
                    row.GetCell(0).CellStyle = alignmentCenterStyle;
                    row.GetCell(0).CellStyle = borderTopStyle;

                    row.CreateCell(1).SetCellValue(string.Empty);
                    row.CreateCell(2).SetCellValue(string.Empty);
                    row.CreateCell(3).SetCellValue(string.Empty);
                    row.CreateCell(4).SetCellValue(string.Empty);
                    row.CreateCell(5).SetCellValue(string.Empty);
                    row.CreateCell(6).SetCellValue(string.Empty);
                    row.GetCell(1).CellStyle = borderTopStyle;
                    row.GetCell(2).CellStyle = borderTopStyle;
                    row.GetCell(3).CellStyle = borderTopStyle;
                    row.GetCell(4).CellStyle = borderTopStyle;

                }

                if (item.ClassName.Contains("Punchcard") && item.ProgramParticipants != null)
                {
                    rowNumber++;
                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("* Today attending the first session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("** Today attending the secnod session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;

                    rowNumber++;
                    row = sheet.CreateRow(rowNumber);
                    row.RowStyle = greyStyleLeftAlignment;
                    row.CreateCell(1).SetCellValue("*** Today attending the both session");
                    row.GetCell(1).CellStyle = greyStyleLeftAlignment;
                }

                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);
                rowNumber++;
                row = sheet.CreateRow(rowNumber);

            }


            //Write the Workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel",//MIME type of Excel files
             "Internal roster.xls");
        }


        public virtual ActionResult GetAllergyMedicalConditionReport(string seasonDomain, long? scheduleId)
        {
            var model = CreateModelAllergyMedicalConditionReport(seasonDomain, scheduleId);

            return JsonNet(model);
        }

        private List<AllergyMedicalConditionReportViewModel> CreateModelAllergyMedicalConditionReport(string seasonDomain, long? scheduleId)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var programs = new List<Program>();
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);

            var medicalConditionModel = new List<AllergyMedicalConditionReportParticipantViewModel>();
            var allergyModel = new List<AllergyMedicalConditionReportParticipantViewModel>();
            var selfAdministeredModel = new List<AllergyMedicalConditionReportParticipantViewModel>();
            var nutAllergyModel = new List<AllergyMedicalConditionReportParticipantViewModel>();
            var model = new List<AllergyMedicalConditionReportViewModel>();
            var allOrderItem = new List<AllergyMedicalConditionReportParticipantViewModel>();

            if (scheduleId != null)
            {
                programs = programBusiness.GetListBySeasonId(season.Id).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted && p.ProgramSchedules.Select(s => s.Id).Contains(scheduleId.Value)).ToList();
            }
            else
            {
                programs = programBusiness.GetListBySeasonId(season.Id).Where(p => (p.TypeCategory != ProgramTypeCategory.SeminarTour && p.TypeCategory != ProgramTypeCategory.ChessTournament && p.TypeCategory != ProgramTypeCategory.Calendar) && p.Status == ProgramStatus.Open && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Status != ProgramStatus.Deleted).ToList();
            }

            if (scheduleId.HasValue)
            {
                var schedule = programs.SelectMany(c => c.ProgramSchedules).SingleOrDefault(c => c.Id == scheduleId.Value);

                allOrderItem.AddRange(GetAllParticipantForAllergyMedicalConditionReport(schedule));
            }
            else
            {
                var programSchedules = programs.SelectMany(p => p.ProgramSchedules.Where(s => !s.IsDeleted)).ToList();

                foreach (var schedule in programSchedules)
                {
                    allOrderItem.AddRange(GetAllParticipantForAllergyMedicalConditionReport(schedule));
                }
            }

            var medicalConditionOrderItems = allOrderItem.Where(a => a.HasMedicalCondition)
                .GroupBy(a => a.PlayerId)
                .Select(a => new AllergyMedicalConditionReportParticipantViewModel
                {
                    Number = a.FirstOrDefault().Number,
                    Grade = a.FirstOrDefault().Grade,
                    ParticipantName = a.FirstOrDefault().ParticipantName,
                    ProgramName = string.Join(", ", a.Select(p => p.ProgramName).Distinct().ToList()),
                    Description = a.FirstOrDefault().DescriptionMedical
                }).ToList();

            medicalConditionOrderItems = medicalConditionOrderItems.OrderBy(m => m.ParticipantName).ToList();
            setRowNumberAllergyMedicalConditionReport(medicalConditionOrderItems);
            medicalConditionModel.AddRange(medicalConditionOrderItems);


            var allergyOrderItems = allOrderItem.Where(a => a.HasAllergies).GroupBy(a => a.PlayerId)
                .Select(a => new AllergyMedicalConditionReportParticipantViewModel
                {
                    Number = a.FirstOrDefault().Number,
                    Grade = a.FirstOrDefault().Grade,
                    ParticipantName = a.FirstOrDefault().ParticipantName,
                    ProgramName = string.Join(", ", a.Select(p => p.ProgramName).Distinct().ToList()),
                    Description = a.FirstOrDefault().DescriptionAllergies
                }).ToList();

            allergyOrderItems = allergyOrderItems.OrderBy(m => m.ParticipantName).ToList();
            setRowNumberAllergyMedicalConditionReport(allergyOrderItems);
            allergyModel.AddRange(allergyOrderItems);


            var selfAdministeredOrderItems = allOrderItem.Where(a => a.HasSelfAdministered).GroupBy(a => a.PlayerId)
                .Select(a => new AllergyMedicalConditionReportParticipantViewModel
                {
                    Number = a.FirstOrDefault().Number,
                    Grade = a.FirstOrDefault().Grade,
                    ParticipantName = a.FirstOrDefault().ParticipantName,
                    ProgramName = string.Join(", ", a.Select(p => p.ProgramName).Distinct().ToList()),
                    Description = a.FirstOrDefault().DescriptionSelfAdministered
                }).ToList();

            selfAdministeredOrderItems = selfAdministeredOrderItems.OrderBy(m => m.ParticipantName).ToList();
            setRowNumberAllergyMedicalConditionReport(selfAdministeredOrderItems);
            selfAdministeredModel.AddRange(selfAdministeredOrderItems);


            var nutAllergyOrderItems = allOrderItem.Where(a => a.HasNutAllergy).GroupBy(a => a.PlayerId)
                .Select(a => new AllergyMedicalConditionReportParticipantViewModel
                {
                    Number = a.FirstOrDefault().Number,
                    Grade = a.FirstOrDefault().Grade,
                    ParticipantName = a.FirstOrDefault().ParticipantName,
                    ProgramName = string.Join(", ", a.Select(p => p.ProgramName).Distinct().ToList()),
                    Description = a.FirstOrDefault().DescriptionNutAllergy
                }).ToList();

            nutAllergyOrderItems = nutAllergyOrderItems.OrderBy(m => m.ParticipantName).ToList();
            setRowNumberAllergyMedicalConditionReport(nutAllergyOrderItems);
            nutAllergyModel.AddRange(nutAllergyOrderItems);


            model.Add(new AllergyMedicalConditionReportViewModel { SectionName = "Medical conditions and special needs", SectionParticipant = medicalConditionModel });
            model.Add(new AllergyMedicalConditionReportViewModel { SectionName = "Allergies and dietary restrictions", SectionParticipant = allergyModel });
            model.Add(new AllergyMedicalConditionReportViewModel { SectionName = "Self-administered medication", SectionParticipant = selfAdministeredModel });
            model.Add(new AllergyMedicalConditionReportViewModel { SectionName = "Nut allergy", SectionParticipant = nutAllergyModel });

            return model;
        }

        private static void setRowNumberAllergyMedicalConditionReport(List<AllergyMedicalConditionReportParticipantViewModel> participants)
        {
            var num = 1;
            foreach (var participant in participants)
            {
                participant.Number = num;
                num++;
            }
        }

        private List<AllergyMedicalConditionReportParticipantViewModel> GetAllParticipantForAllergyMedicalConditionReport(ProgramSchedule programSchedule)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;
            var formBusiness = Ioc.FormBusiness;

            var programOrderItems = orderItemBusiness.GetReportOrderItemsBySchedule(programSchedule.Id);

            var userIds = programOrderItems.Select(a => a.Order.UserId).ToList();

            var model = new List<AllergyMedicalConditionReportParticipantViewModel>();

            foreach (var orderItem in programOrderItems)
            {
                model.Add(new AllergyMedicalConditionReportParticipantViewModel()
                {
                    ParticipantName = orderItem.Player?.Contact != null ? $"{orderItem.Player.Contact.LastName}, {orderItem.Player.Contact.FirstName}" : "",
                    Grade = orderItem.Player?.Info != null && orderItem.Player.Info.Grade != 0 ? orderItem.Player.Info.Grade.ToAbbreviation() : "",
                    ProgramName = orderItem.ProgramSchedule.Program.Name,
                    HasAllergies = orderItem.Player?.Info != null && orderItem.Player.Info.HasAllergies.HasValue ? orderItem.Player.Info.HasAllergies.Value : false,
                    HasMedicalCondition = orderItem.Player?.Info != null && orderItem.Player.Info.HasSpecialNeeds.HasValue ? orderItem.Player.Info.HasSpecialNeeds.Value : false,
                    HasNutAllergy = orderItem.Player?.Info != null && orderItem.Player.Info.NutAllergy.HasValue ? orderItem.Player.Info.NutAllergy.Value : false,
                    HasSelfAdministered = orderItem.Player?.Info != null && orderItem.Player.Info.SelfAdministerMedication.HasValue ? orderItem.Player.Info.SelfAdministerMedication.Value : false,
                    DescriptionAllergies = orderItem.Player?.Info != null ? orderItem.Player.Info.Allergies : "",
                    DescriptionMedical = orderItem.Player?.Info != null ? orderItem.Player.Info.SpecialNeeds : "",
                    DescriptionSelfAdministered = orderItem.Player?.Info != null ? orderItem.Player.Info.SelfAdministerMedicationDescription : "",
                    DescriptionNutAllergy = orderItem.Player?.Info != null ? orderItem.Player.Info.NutAllergyDescription : "",
                    PlayerId = orderItem.PlayerId.HasValue ? orderItem.PlayerId.Value : 0
                });
            }

            return model;
        }

        public virtual FileResult GenerateAllergyMedicalConditionReportPdf(string seasonDomain, long? scheduleId)
        {
            var pdfGenerator = new FlyerGenerator();
            var seasonBusiness = Ioc.SeasonBusiness;
            var clubId = ActiveClub.Id;
            var season = Ioc.SeasonBusiness.Get(seasonDomain, clubId);
            var school = Ioc.ClubBusiness.Get(clubId);
            var partner = new Club();
            var showSchoolLogo = true;

            if (school.PartnerId.HasValue)
            {
                partner = Ioc.ClubBusiness.Get(school.PartnerId.Value);
            }
            else
            {
                partner = Ioc.ClubBusiness.Get(school.Id);
            }

            if (school.Setting != null)
            {
                showSchoolLogo = school.Setting.ShowSchoolLogo;
            }

            var seasonName = seasonBusiness.Get(season.Id).Name;

            var model = CreateModelAllergyMedicalConditionReport(seasonDomain, scheduleId);

            var PdfModel = new AllergyMedicalConditionReportPdfViewModel()
            {
                SchoolLogo = showSchoolLogo ? _clubBusiness.GetClubLogoURL(school.Domain, school.Logo) : null,
                ClubLogo = _clubBusiness.GetClubLogoURL(partner.Domain, partner.Logo),
                Programs = model,
            };

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "_AllergyMedicalConditionReportPdf", PdfModel);

            return File(file, "application/pdf", "Medical information_" + school.Name + " " + seasonName.ToDescription() + ".Pdf");
        }

        public JsonNetResult GetParticipantNoteReportHeader(string seasonDomain)
        {
            var season = _seasonBusiness.Get(seasonDomain, ActiveClub.Id);
            var club = _clubBusiness.Get(ActiveClub.Id);

            var data = new
            {
                ClubName = club.Name,
                Date = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).ToString(Constants.DefaultDateTimeFormatAbrMonthWithAMPM),
                SeasonName = season.Title
            };

            return JsonNet(data);
        }

        private SubscriptionParticipantInternalViwModel GetSubscriptionParticipantInformation(OrderItem orderItem, DateTime startDate, DateTime endDate, string reportName)
        {
            var result = new SubscriptionParticipantInternalViwModel();
            var formBusiness = Ioc.FormBusiness;
            var programBusiness = Ioc.ProgramBusiness;
            var program = orderItem.ProgramSchedule.Program;

            var orderSessions = orderItem.OrderSessions.Where(o => o.ProgramSession.StartDateTime >= startDate && o.ProgramSession.StartDateTime <= endDate).ToList();

            if (orderSessions.Count > 0)
            {
                foreach (var session in orderSessions)
                {
                    if (session.ProgramSession.StartDateTime.DayOfWeek.ToString() == "Monday")
                    {
                        result.Monday = true;
                    }
                    if (session.ProgramSession.StartDateTime.DayOfWeek.ToString() == "Tuesday")
                    {
                        result.Tuesday = true;
                    }
                    if (session.ProgramSession.StartDateTime.DayOfWeek.ToString() == "Wednesday")
                    {
                        result.Wednesday = true;
                    }
                    if (session.ProgramSession.StartDateTime.DayOfWeek.ToString() == "Thursday")
                    {
                        result.Thursday = true;
                    }
                    if (session.ProgramSession.StartDateTime.DayOfWeek.ToString() == "Friday")
                    {
                        result.Friday = true;
                    }

                }

                if (orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
                {
                    var gender = orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.Gender.ToString()).GetValue();
                    if (gender != null)
                    {
                        result.Gender = gender.ToString();
                    }
                }

                result.StrStartTime = programBusiness.GetClassDays(program).Where(o => o.StartTime.HasValue).Min(pr => pr.StartTime).HasValue ? DateTime.Today.Add(programBusiness.GetClassDays(program).Where(o => o.StartTime.HasValue).Min(pr => pr.StartTime.Value)).ToString("h:mm tt") : null;
                result.StrEndTime = programBusiness.GetClassDays(program).Where(o => o.EndTime.HasValue).Max(pr => pr.EndTime) != null ? DateTime.Today.Add(programBusiness.GetClassDays(program).Where(o => o.EndTime.HasValue).Max(pr => pr.EndTime.Value)).ToString("h:mm tt") : null;
                result.ProgramName = program.Name;

                var player = orderItem.Player;
                result.StudentName = string.Format("{0}, {1}", formBusiness.GetFormValue(orderItem.JbForm, SectionsName.ParticipantSection, ElementsName.LastName), formBusiness.GetFormValue(orderItem.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName));
                result.Grade = formBusiness.GetPlayerGradeAsString(orderItem.JbForm) != null ? formBusiness.GetPlayerGradeAsString(orderItem.JbForm) : string.Empty;
                result.GradeOrder = formBusiness.GetPlayerGrade(orderItem.JbForm) != null ? formBusiness.GetPlayerGrade(orderItem.JbForm).GetOrder().HasValue ? formBusiness.GetPlayerGrade(orderItem.JbForm).GetOrder().Value : 0 : 0;
                result.TeacherName = formBusiness.GetApolloTeacher(orderItem.JbForm);
                result.TuitionName = orderItem.EntryFeeName;
                result.FirstParentPhone = formBusiness.GetParentPhone(orderItem.JbForm);
                if (reportName == "Internal")
                {
                    result.FirstParentName = formBusiness.GetParent1Name(orderItem.JbForm);
                    result.FirstParentEmail = formBusiness.GetFormValue(orderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.Email);
                    result.FirstParentRelationShip = formBusiness.GetParent1RelationShip(orderItem.JbForm);
                    result.FirstParentHome = formBusiness.Parent1HomePhone(orderItem.JbForm);


                    result.SecoundParentName = formBusiness.GetParent2Name(orderItem.JbForm);
                    result.SecoundParentEmail = formBusiness.GetFormValue(orderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.Email);
                    result.SecoundParentHome = result.FirstParentHome = formBusiness.Parent2HomePhone(orderItem.JbForm);
                    result.SecoundParentPhone = formBusiness.GetParentPhone(orderItem.JbForm);
                    result.SecoundParentRelationShip = formBusiness.GetParent2RelationShip(orderItem.JbForm);

                    //Emergency contact
                    result.FirstEmergencyName = formBusiness.GetEmergencyContactName(orderItem.JbForm);
                    result.FirstEmergencyPhone = formBusiness.GetEmergencyContactPhone(orderItem.JbForm)/*GetFormValue(orderItem.JbForm, "Emergency Contact 1", "Phone")*/;
                    result.FirstEmergencyRelationShip = formBusiness.GetEmergency1RelationShip(orderItem.JbForm)/*GetFormValue(orderItem.JbForm, "Emergency Contact 1", "Relationship (to the student)")*/;

                    result.SecoundEmergencyName = formBusiness.GetEmergencyContact2Name(orderItem.JbForm);
                    result.SecoundEmergencyPhone = formBusiness.GetEmergencyContact2Phone(orderItem.JbForm);
                    result.SecoundEmergencyRelationShip = formBusiness.GetEmergency2RelationShip(orderItem.JbForm);
                    //Athuraize pick up
                    result.FirstAuthuraizePikeUpName = formBusiness.GetAuthorizedPickup1Name(orderItem.JbForm);
                    result.FirstAuthuraizePikeUpPhone = formBusiness.GetAuthorizedPickup1Phone(orderItem.JbForm);
                    result.FirstAuthuraizePikeUpRelationShip = formBusiness.GetAuthuraizePikeUp1RelationShip(orderItem.JbForm);

                    result.SecoundAuthuraizePikeUpName = formBusiness.GetAuthorizedPickup2Name(orderItem.JbForm);
                    result.SecoundAuthuraizePikeUpPhone = formBusiness.GetAuthorizedPickup1Phone(orderItem.JbForm);
                    result.SecoundAuthuraizePikeUpRelationShip = formBusiness.GetAuthuraizePikeUp2RelationShip(orderItem.JbForm);
                    //Medical info
                    result.HealthMedical = formBusiness.GetMedicalConditions(orderItem.JbForm);
                    result.Allergies = formBusiness.GetAllergies(orderItem.JbForm);
                    result.Deitary = formBusiness.GetDeitary(orderItem.JbForm);
                    result.Medication = formBusiness.GetMedication(orderItem.JbForm);
                    result.Other = formBusiness.GetOtherInformation(orderItem.JbForm);
                }
                return result;
            }
            else
            {
                return null;
            }

            return result;
        }

        [HttpGet]
        public virtual JsonNetResult GetAllParticipantNoteReport(string seasonDomain, long? scheduleId)
        {
            var notes = new List<PlayerProgramNote>();

            if (scheduleId.HasValue)
            {
                notes = _playerProgramNoteBusiness.GetList(n => n.Program.ProgramSchedules.Any(p => p.Id == scheduleId.Value) && !n.IsDeleted).ToList();
            }
            else
            {
                notes = _playerProgramNoteBusiness.GetList(n => n.Program.Season.ClubId == ActiveClub.Id &&  n.Program.Season.Domain == seasonDomain && !n.IsDeleted).ToList();
            }

            var model = notes.Select(n => new
            {
                Id = n.ProfileId,
                ProgramName = n.Program.Name,
                FullName = n.PlayerProfile.Contact.FullName,
                Note = n.Note,
                Title = n.Title,
                Date = n.MetaData.DateUpdated.ToString(Constants.DateTime_Comma),
                Owner = n.ClubStaff.Contact.FullName
            })
            .ToList();

            return JsonNet(new { DataSource = model, TotalCount = model.Count });
        }

        [HttpGet]
        public virtual JsonNetResult GetParticipantNoteReport(string seasonDomain, long? scheduleId)
        {
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);
            var notes = new List<PlayerProgramNote>();

            if (scheduleId.HasValue)
            {
                notes = _playerProgramNoteBusiness.GetList(n => n.Program.ProgramSchedules.Any(p => p.Id == scheduleId.Value) && !n.IsDeleted).ToList();
            }
            else
            {
                notes = _playerProgramNoteBusiness.GetList(n => n.Program.Season.ClubId == ActiveClub.Id && n.Program.Season.Domain == seasonDomain && !n.IsDeleted).ToList();
            }

            var result = notes.OrderBy(n => n.PlayerProfile.Contact.FirstName).Skip((page - 1) * pageSize).Take(pageSize).Select(n => new
            {
                Id = n.ProfileId,
                FullName = n.PlayerProfile.Contact.FullName,
            })
            .DistinctBy(n => n.Id)
            .ToList();

            return JsonNet(new { DataSource = result, TotalCount = notes.Count });
        }

        [HttpGet]
        public virtual JsonNetResult GetParticipantNoteReportDetail(int profileId, int? scheduleId)
        {
            var notes = _playerProgramNoteBusiness.GetList(n => n.ProfileId == profileId && !n.IsDeleted).ToList();

            if (scheduleId.HasValue)
            {
                notes = notes.Where(n => n.Program.ProgramSchedules.Select(p => p.Id).ToList().Contains(scheduleId.Value)).ToList();
            }

            var result = notes.OrderBy(n => n.Program.Name).Select(n => new
            {
                ProgramName = n.Program.Name,
                Note = n.Note.Replace("\\n", "<br/>"),
                Title = n.Title,
                Date = n.MetaData.DateUpdated.ToString(Constants.DateTime_Comma),
                Owner = n.ClubStaff.Contact.FullName
            })
            .ToList();

            return JsonNet(new { DataSource = result, TotalCount = result.Count });
        }

        [HttpGet]
        public virtual JsonNetResult GetLiveSeasonsClub(string seasonDomain)
        {
            var seasons = _seasonBusiness.GetLiveSeasonsClub(ActiveClub.Id, seasonDomain);

            return JsonNet(seasons);
        }

        [HttpGet]
        public virtual JsonNetResult CopyCustomReportToSeason(int sourceReportId, List<long> destinationSeasonIds)
        {
            var result = _reportBusiness.CopyCustomReportToSeason(sourceReportId, destinationSeasonIds);

            return JsonNet(result);
        }

        #region Private Methods
        private IEnumerable<RunProgramReportViewModel> GetProgramReportModel(EventRoaster eventRoaster, IEnumerable<OrderItem> allOrderItems)
        {
            var reportmodel = new List<RunProgramReportViewModel>();
            var ordersList = new List<List<OrderItem>>();



            foreach (var group in allOrderItems.GroupBy(o => o.ProgramSchedule.ProgramId))
            {
                var orderItems = group.ToList();
                var program = Ioc.ProgramBusiness.Get(group.Key);
                var programName = program.Name;

                if (orderItems.Any())
                {
                    ordersList.Add(orderItems);
                }
                var roomNumber = program.Room;
                var instructor = string.Empty;
                var dayOfWeek = string.Join(", ", Ioc.ProgramBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList());
                if (program.Instructors != null && program.Instructors.Any(c => c.Contact != null))
                {
                    instructor = string.Join(", ", program.Instructors.Select(c => string.Format(Constants.F_FullName, c.Contact.FirstName, c.Contact.LastName)));
                }
                var schoolName = program.Club.Name;

                if (ordersList.Any())
                {
                    reportmodel.Add(new RunProgramReportViewModel(programName, eventRoaster, ordersList, instructor, roomNumber, dayOfWeek, schoolName));
                }

                ordersList.Clear();
            }

            return reportmodel;
        }

        private List<Dictionary<string, object>> FillCustomReport(IEnumerable<RunProgramReportViewModel> data, ref List<string> titles, JbForm baseJbForm, EventRoasterType reportType = EventRoasterType.Custom)
        {
            var finalData = new List<Dictionary<string, object>>();

            var fieldNames = new Dictionary<string, string>();

            Dictionary<string, object> newRow;
            #region
            foreach (var element in data)
            {
                foreach (var jbForm in element.DataTable)
                {
                    if (reportType == EventRoasterType.Roster)
                    {
                        newRow = new Dictionary<string, object> { { "Program", element.EventTitle } };
                        titles.Add("Program");
                    }
                    else
                    {
                        newRow = new Dictionary<string, object> { { "Program", element.EventTitle } };
                        titles.Add("Program");
                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "ScheduleStartEnd"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var item =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Single(
                                e => e.Name == "ScheduleStartEnd");
                        if (!fieldNames.Any(f => f.Key == item.Name))
                        {
                            fieldNames.Add(item.Name, "ScheduleStartEnd");
                        }

                        var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "ScheduleStartEnd");
                        var itemValue = item.GetValue();
                        newRow.Add(itemName, (itemValue != null) ? itemValue.ToString() : "-");
                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "TuitionName"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var item =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Single(
                                e => e.Name == "TuitionName");
                        if (!fieldNames.Any(f => f.Key == item.Name))
                        {
                            fieldNames.Add(item.Name, "TuitionName");
                        }

                        var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "TuitionName");
                        var itemValue = item.GetValue();
                        newRow.Add(itemName, (itemValue != null) ? itemValue.ToString() : "-");
                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "Instructor"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var item =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Single(
                                e => e.Name == "Instructor");
                        if (!fieldNames.Any(f => f.Key == item.Name))
                        {
                            fieldNames.Add(item.Name, "Instructor");
                        }

                        var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "Instructor");
                        var itemValue = item.GetValue();
                        newRow.Add(itemName, (itemValue != null) ? itemValue.ToString() : "-");
                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "Balance"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var item =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Single(
                                e => e.Name == "Balance");
                        if (!fieldNames.Any(f => f.Key == item.Name))
                        {
                            fieldNames.Add(item.Name, "Balance");
                        }

                        var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "Balance");
                        var itemValue = item.GetValue();
                        var firstValue = itemValue.ToString().Substring(0, 1);
                        itemValue = itemValue.ToString().Substring(itemValue.ToString().LastIndexOf("$") + 1);

                        var correctValue = firstValue == "-" && itemValue.ToString() != "-" ? "-" + itemValue.ToString() : itemValue.ToString();
                        newRow.Add(itemName, correctValue != null && correctValue != "-" ? Convert.ToDecimal(correctValue) : 0);
                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "OrderAmount"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var item =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Single(
                                e => e.Name == "OrderAmount");
                        if (!fieldNames.Any(f => f.Key == item.Name))
                        {
                            fieldNames.Add(item.Name, "OrderAmount");
                        }

                        var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "OrderAmount");
                        var itemValue = item.GetValue();
                        itemValue = itemValue.ToString().Substring(itemValue.ToString().LastIndexOf("$") + 1);
                        newRow.Add(itemName, (itemValue != null) ? Convert.ToDecimal(itemValue.ToString()) : 0);
                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "OrderPaidAmount"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var item =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Single(
                                e => e.Name == "OrderPaidAmount");
                        if (!fieldNames.Any(f => f.Key == item.Name))
                        {
                            fieldNames.Add(item.Name, "OrderPaidAmount");
                        }

                        var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "OrderPaidAmount");
                        var itemValue = item.GetValue();
                        itemValue = itemValue.ToString().Substring(itemValue.ToString().LastIndexOf("$") + 1);
                        newRow.Add(itemName, (itemValue != null) ? Convert.ToDecimal(itemValue.ToString()) : 0);
                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "Charges"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var items =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Where(
                                e => e.Name == "Charges");

                        for (int i = 0; i < items.Count(); i++)
                        {
                            var item = items.ElementAt(i);
                            if (!fieldNames.Any(f => f.Key == "Charges" + i))
                            {
                                fieldNames.Add("Charges" + i, "Charges" + i);
                            }

                            var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "Charges" + i);
                            var itemValue = item.GetValue();
                            itemValue = itemValue.ToString().Substring(itemValue.ToString().LastIndexOf("$") + 1);

                            var title = titles.Last();
                            titles.RemoveAt(titles.Count() - 1);

                            var isDuplicateTitle = false;
                            if (newRow.Any() && newRow != null)
                            {
                                for (int j = 0; j < newRow.Count; j++)
                                {
                                    var row = newRow.ElementAt(j);

                                    if (row.Key == itemName)
                                    {
                                        if (row.Value != "" && itemValue != "-")
                                        {
                                            newRow[itemName] = Convert.ToDecimal(row.Value) + Convert.ToDecimal(itemValue.ToString());
                                        }
                                        isDuplicateTitle = true;
                                    }
                                }
                            }

                            if (!isDuplicateTitle)
                            {
                                if (itemValue.ToString() == "-")
                                {
                                    newRow.Add(itemName, "");
                                }
                                else
                                {
                                    itemValue = Convert.ToDecimal(itemValue.ToString());
                                    newRow.Add(itemName, itemValue);
                                }
                                titles.Add(title);
                            }
                        }

                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "Tuition"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var item =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Single(
                                e => e.Name == "Tuition");
                        if (!fieldNames.Any(f => f.Key == item.Name))
                        {
                            fieldNames.Add(item.Name, "Tuition");
                        }

                        var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "Tuition");
                        var itemValue = item.GetValue();
                        itemValue = itemValue.ToString().Substring(itemValue.ToString().LastIndexOf("$") + 1);
                        newRow.Add(itemName, (itemValue != null) ? Convert.ToDecimal(itemValue.ToString()) : 0);
                    }

                    if (jbForm.Elements.Any(e => e.Name == "MoreInfoSection") && ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Any(e => e.Name == "DiscountTotal"))
                    {
                        var moreInfoSection = jbForm.Elements.Single(e => e is JbSection && e.Name == "MoreInfoSection");
                        var item =
                            ((JbSection)(jbForm.Elements.Single(e => e.Name == "MoreInfoSection"))).Elements.Single(
                                e => e.Name == "DiscountTotal");
                        if (!fieldNames.Any(f => f.Key == item.Name))
                        {
                            fieldNames.Add(item.Name, "DiscountTotal");
                        }

                        var itemName = GeneratePropertyName(item, moreInfoSection, ref titles, fieldNames, "DiscountTotal");
                        var itemValue = item.GetValue();
                        itemValue = itemValue.ToString().Substring(itemValue.ToString().LastIndexOf("$") + 1);
                        newRow.Add(itemName, (itemValue != null) ? Convert.ToDecimal(itemValue.ToString()) : 0);
                    }

                    for (int j = 0; j < baseJbForm.Elements.Count; j++)
                    {
                        var baseSection = baseJbForm.Elements.ElementAt(j);

                        var section = jbForm.Elements.FirstOrDefault(e => e.Title == baseSection.Title);

                        var isExistSection = true;

                        if (section == null)
                        {
                            isExistSection = false;
                            section = baseSection;
                        }

                        if (baseSection is JbSection)
                        {
                            for (int i = 0; i < ((JbSection)baseSection).Elements.Count; i++)
                            {
                                var baseItem = ((JbSection)baseSection).Elements.ElementAt(i);

                                var item = ((JbSection)section).Elements.FirstOrDefault(e => e.Title.ToLower() == baseItem.Title.ToLower());

                                if (item == null || !isExistSection)
                                {
                                    if (!fieldNames.Any(f => f.Key == ("Another_" + j + "_" + i)))
                                    {
                                        fieldNames.Add("Another_" + j + "_" + i, "Another_" + j + "_" + i);
                                    }
                                    var itemName = GeneratePropertyName(baseItem, section, ref titles, fieldNames, "Another_" + j + "_" + i);

                                    newRow.Add(itemName, string.Empty);
                                    continue;
                                }

                                if (item.GetType() != typeof(JbHidden))
                                {
                                    if (item.Name == "Charges" || item.Name == "Tuition" || item.Name == "TuitionName" || item.Name == "ScheduleStartEnd" || item.Name == "Instructor" || item.Name == "Balance" || item.Name == "OrderAmount" || item.Name == "OrderPaidAmount" || item.Name == "DiscountTotal")
                                    {
                                        continue;
                                    }

                                    if (!fieldNames.Any(f => f.Key == ("Another_" + j + "_" + i)))
                                    {
                                        fieldNames.Add("Another_" + j + "_" + i, "Another_" + j + "_" + i);
                                    }
                                    var itemName = GeneratePropertyName(item, section, ref titles, fieldNames, "Another_" + j + "_" + i);

                                    var title = titles.Last();
                                    titles.RemoveAt(titles.Count() - 1);

                                    if (newRow.All(c => c.Key != itemName/*.Replace(" ", string.Empty)*/))
                                    {
                                        if (item.GetType() == typeof(JbTextBox) && item.Name.ToLower().StartsWith("address") || item.GetType() == typeof(JbAddress))
                                        {
                                            var mainAddressLine = string.Format("{0}_Address_Line", itemName);
                                            var city = string.Format("{0}_City", itemName);
                                            var state = string.Format("{0}_State", itemName);
                                            var zipcode = string.Format("{0}_Zip_code", itemName);


                                            //var country = string.Format("{0}_Country", itemName);

                                            if (item.GetType() == typeof(JbAddress))
                                            {
                                                var line1 = (!string.IsNullOrEmpty(((JbAddress)item).AddressLine1))
                                                    ? ((JbAddress)item).AddressLine1
                                                    : string.Empty;
                                                var line2 = (!string.IsNullOrEmpty(((JbAddress)item).AddressLine2))
                                                    ? ((JbAddress)item).AddressLine2
                                                    : string.Empty;

                                                var addressLine =
                                                    string.Format("{0}#{1}", line1, line2)
                                                        //.Replace(" ", string.Empty)
                                                        .Replace("#", " ");

                                                newRow.Add(mainAddressLine,
                                                    (!string.IsNullOrEmpty(addressLine))
                                                        ? addressLine
                                                        : string.Empty);
                                                newRow.Add(city, ((JbAddress)item).City);
                                                newRow.Add(state, ((JbAddress)item).State);
                                                newRow.Add(zipcode, ((JbAddress)item).Zip);

                                                titles.Add(string.Format("{0} Address Line", title));
                                                titles.Add(string.Format("{0} City", title));
                                                titles.Add(string.Format("{0} State", title));
                                                titles.Add(string.Format("{0} Zip code", title));
                                                //newRow.Add(country, ((JbAddress)item).Country);
                                            }
                                            else
                                            {
                                                var addressValue = !string.IsNullOrEmpty(((JbTextBox)item).Value) ? ((JbTextBox)item).Value.Split(',') : null;
                                                if (addressValue != null && !string.IsNullOrEmpty(addressValue[0]))
                                                {
                                                    var len = addressValue.Length;

                                                    var addressLine = "";
                                                    var addressCity = "";
                                                    var addressState = "";
                                                    var addressZipCode = "";

                                                    if (len > 1)
                                                    {
                                                        try
                                                        {
                                                            addressLine = addressValue[0] + ", " + addressValue[1];
                                                            addressCity = addressValue[2];
                                                            addressState = addressValue[3];
                                                            addressZipCode = addressValue[4];
                                                        }
                                                        catch { }
                                                    }
                                                    else if (len == 1)
                                                    {
                                                        addressLine = addressValue[0];
                                                    }

                                                    newRow.Add(mainAddressLine, addressLine);
                                                    newRow.Add(city, addressCity);
                                                    newRow.Add(state, addressState);
                                                    newRow.Add(zipcode, addressZipCode);

                                                    titles.Add(string.Format("{0} Address Line", title));
                                                    titles.Add(string.Format("{0} City", title));
                                                    titles.Add(string.Format("{0} State", title));
                                                    titles.Add(string.Format("{0} Zip code", title));
                                                }
                                                else
                                                {
                                                    newRow.Add(mainAddressLine, string.Empty);
                                                    newRow.Add(city, string.Empty);
                                                    newRow.Add(state, string.Empty);
                                                    newRow.Add(zipcode, string.Empty);

                                                    titles.Add(string.Format("{0} Address Line", title));
                                                    titles.Add(string.Format("{0} City", title));
                                                    titles.Add(string.Format("{0} State", title));
                                                    titles.Add(string.Format("{0} Zip code", title));
                                                    //newRow.Add(country, string.Empty);
                                                }
                                            }
                                        }
                                        if (item.GetType() == typeof(USCFInfo) || item.Title.ToLower().Contains("uscf"))
                                        {
                                            var uscf = item.GetValue() != null ? item.GetValue().ToString() : "";
                                            var values = uscf.Split(',');

                                            var uscfIdName = string.Format("_{0}_{1}_UscfId", j, i);
                                            //var uscfRatingQuick = string.Format("{0}_UscfRatingQuick", itemName);
                                            var uscfRatingRegular = string.Format("_{0}_{1}_UscfRatingRegular", j, i);
                                            var uscfExpirationDate = string.Format("_{0}_{1}_UscfExpirationDate", j, i);

                                            if (values.Any() && !string.IsNullOrEmpty(values[0]))
                                            {
                                                newRow.Add(uscfIdName, values[0].Split(':') != null ? values[0].Split(':')[1] : string.Empty);
                                                //newRow.Add(uscfRatingQuick, values[1].Split(':')[1]);
                                                newRow.Add(uscfRatingRegular, values[2].Split(':') != null ? values[2].Split(':')[1] : string.Empty);
                                                newRow.Add(uscfExpirationDate, values[3].Split(':') != null ? values[3].Split(':')[1] : string.Empty);

                                                titles.Add(string.Format("{0} UscfId", title));
                                                titles.Add(string.Format("{0} UscfRatingRegular", title));
                                                titles.Add(string.Format("{0} UscfExpirationDate", title));
                                            }
                                            else
                                            {
                                                newRow.Add(uscfIdName, string.Empty);
                                                //newRow.Add(uscfRatingQuick, string.Empty);
                                                newRow.Add(uscfRatingRegular, string.Empty);
                                                newRow.Add(uscfExpirationDate, string.Empty);

                                                titles.Add(string.Format("{0} UscfId", title));
                                                titles.Add(string.Format("{0} UscfRatingRegular", title));
                                                titles.Add(string.Format("{0} UscfExpirationDate", title));
                                            }

                                        }
                                        else
                                        {
                                            newRow.Add(itemName/*.Replace(" ", string.Empty)*/,
                                                (item.GetValue() != null) ? item.GetValue().ToString() : string.Empty);

                                            titles.Add(title);
                                        }

                                    }
                                }
                            }
                        }
                    }

                    finalData.Add(newRow);
                }
            }
            #endregion
            return finalData;
        }

        private List<CapacityReportViewModel> FillCapacityViewModel(IEnumerable<ProgramSchedule> schedules)
        {
            var dataSource = new List<CapacityReportViewModel>();

            foreach (var schedule in schedules)
            {
                var programBusiness = Ioc.ProgramBusiness;
                var programDb = programBusiness.Get(schedule.ProgramId);

                var isTestMode = programDb.Season.Status == SeasonStatus.Test;

                var scheduleCharges =
                    schedule.Charges.Where(
                        charge => !charge.IsDeleted && charge.Category == ChargeDiscountCategory.EntryFee);


                const string unlimited = "Unlimited";
                var programRegStatus = Ioc.ProgramBusiness.GetRegStatus(programDb).ToDescription();

                if (schedule.Attributes.Capacity > 0)
                {

                    var programName = (programDb.TypeCategory == ProgramTypeCategory.Camp)
                        ? _programBusiness.GetProgramName(schedule)
                        : programDb.Name;
                    var filled = scheduleCharges.Sum(s => Ioc.OrderItemBusiness.RegisteredCount(s.Id, s.ProgramSchedule.Program.Season.Status == SeasonStatus.Test));
                    var open = -1;

                    open = schedule.Attributes.Capacity - filled;
                    var status = filled >= schedule.Attributes.Capacity
                        ? "Full"
                        : programRegStatus;

                    dataSource.Add(new CapacityReportViewModel
                    {
                        ProgramName = programName,
                        Status = status,
                        Open =
                            (schedule.Attributes.Capacity) > 0
                                ? ((open >= 0) ? open.ToString() : "0")
                                : unlimited,
                        Capacity =
                            (schedule.Attributes.Capacity) > 0
                                ? schedule.Attributes.Capacity.ToString()
                                : unlimited,
                        MinimumEnrollment =
                            schedule.Program.TypeCategory == ProgramTypeCategory.Class
                                ? schedule.Attributes.MinimumEnrollment.ToString()
                                : "",
                        Filled = filled,
                        Schedule = _programBusiness.GetScheduleStartEndDate(schedule),
                        EnableWaitList = schedule.Program.Season.Setting.SeasonAdditionalSetting != null && schedule.Program.Season.Setting.SeasonAdditionalSetting.IsWaitListAvailable == true ? "Yes" :
                                         (schedule.Program.IsWaitListAvailable == true ? "Yes" : "No"),
                        WaitList = schedule.Program.WaitLists.Any(w => w.IsLive != isTestMode) ? programDb.TypeCategory != ProgramTypeCategory.Camp ? schedule.Program.WaitLists.Count(w => w.IsLive != isTestMode) : schedule.Program.WaitLists.Where(c => c.ScheduleId == schedule.Id).Count(w => w.IsLive != isTestMode) : 0,
                        DayOfWeek = string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(programBusiness.GetClassDays(schedule).Select(s => s.DayOfWeek).ToList()))
                    });
                }
                else
                {
                    foreach (var charge in scheduleCharges)
                    {
                        var filled = Ioc.OrderItemBusiness.RegisteredCount(charge.Id, charge.ProgramSchedule.Program.Season.Status == SeasonStatus.Test);
                        var open = -1;
                        var status = programRegStatus;

                        if (charge.Capacity > 0)
                        {
                            open = (charge.Capacity - filled >= 0) ? charge.Capacity - filled : 0;
                            status = filled >= charge.Capacity
                                ? "Full"
                                : programRegStatus;
                        }

                        dataSource.Add(new CapacityReportViewModel
                        {
                            ProgramName =
                                (programDb.TypeCategory == ProgramTypeCategory.Camp)
                                    ? _programBusiness.GetProgramName(schedule, charge.Name)
                                    : _programBusiness.GetProgramName(programDb.Name, charge.Name),
                            Status = status,
                            Open = (open >= 0) ? open.ToString() : unlimited,
                            Capacity = charge.Capacity > 0 ? charge.Capacity.ToString() : unlimited,
                            MinimumEnrollment = charge.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Class ? charge.ProgramSchedule.Attributes.MinimumEnrollment.ToString() : "",
                            Filled = filled,
                            Schedule = _programBusiness.GetScheduleStartEndDate(schedule),
                            EnableWaitList = schedule.Program.Season.Setting != null && schedule.Program.Season.Setting.SeasonAdditionalSetting != null && schedule.Program.Season.Setting.SeasonAdditionalSetting.IsWaitListAvailable == true ? "Yes" :
                                             (schedule.Program.IsWaitListAvailable == true ? "Yes" : "No"),
                            WaitList = schedule.Program.WaitLists.Any(w => w.IsLive != isTestMode) ? programDb.TypeCategory != ProgramTypeCategory.Camp ? schedule.Program.WaitLists.Count(w => w.IsLive != isTestMode) : schedule.Program.WaitLists.Where(c => c.ScheduleId == schedule.Id).Count(w => w.IsLive != isTestMode) : 0,
                            DayOfWeek = string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(programBusiness.GetClassDays(schedule).Select(s => s.DayOfWeek).ToList()))
                        });
                    }
                }
            }
            return dataSource;
        }

        private IEnumerable<InstallmentReportViewModel> FillInstallmentViewModel(IEnumerable<OrderItem> orderItems)
        {
            var dataSource = new List<InstallmentReportViewModel>();

            var programOrderItems = orderItems.ToList();
            var accountService = Ioc.AccountingBusiness;
            foreach (var item in programOrderItems)
            {
                var currency = item.Order.Club.Currency;
                var programDb = Ioc.ProgramBusiness.Get(item.ProgramSchedule.ProgramId);
                var tuition = item.GetOrderChargeDiscounts().FirstOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee);
                var programName = (programDb.TypeCategory == ProgramTypeCategory.Camp)
                    ? _programBusiness.GetProgramName(item.ProgramSchedule, (tuition != null) ? tuition.Name : string.Empty)
                    : _programBusiness.GetProgramName(programDb.Name, (tuition != null) ? tuition.Name : string.Empty);
                var pastDue =
                    item.Installments.OrderBy(i => i.InstallmentDate)
                        .Count(i => i.InstallmentDate.Date < DateTime.UtcNow.Date &&
                                    i.Status == OrderStatusCategories.initiated);

                var installments = new List<InstallmentItemReportViewModel>();

                foreach (var installmentItem in item.Installments.Where(i => i.Type == InstallmentType.Noraml))
                {
                    var installment = new InstallmentItemReportViewModel();

                    installment.StrAmount = CurrencyHelper.FormatCurrencyWithPenny(installmentItem.Amount, currency);
                    installment.PaymentStatus =
                        installmentItem.Status == OrderStatusCategories.completed
                            ? "Paid"
                            : (installmentItem.Status == OrderStatusCategories.pending) ? "Cash/Check" : "-";

                    if (item.Installments.First().Id == installmentItem.Id && item.ProgramTypeCategory != ProgramTypeCategory.Subscription)
                    {
                        installment.PaymentStatus += " (Deposit)";
                    }

                    installment.DueDate =
                        (installmentItem.PaidDate.HasValue)
                            ? DateTimeHelper.ConvertUtcDateTimeToLocal(installmentItem.PaidDate.Value, ActiveClub.TimeZone).ToString(Constants.DateTime_Comma)
                            : DateTimeHelper.ConvertUtcDateTimeToLocal(installmentItem.InstallmentDate, ActiveClub.TimeZone).ToString(Constants.DateTime_Comma);

                    installments.Add(installment);
                }


                var installmentNum = item.Installments.Count(i => i.Type == InstallmentType.Noraml);
                var paidInstallments = item.Installments.Count(i => i.Status == OrderStatusCategories.completed && i.Type == InstallmentType.Noraml);
                var total = item.ProgramTypeCategory != ProgramTypeCategory.Subscription ? item.TotalAmount : item.Installments.Where(i => i.Type == InstallmentType.Noraml).Sum(i => i.Amount);
                var paidAmount = item.ProgramTypeCategory != ProgramTypeCategory.Subscription ? item.PaidAmount : item.Installments.Where(i => i.Status == OrderStatusCategories.completed && i.Type == InstallmentType.Noraml).Sum(i => i.PaidAmount);

                var balance = accountService.OrderItemBalance(item);
                var hasPreapproval = Ioc.PlayerProfileBusiness.HasPreapprovalkey(item.Player.Id) ? "Yes" : "-";

                dataSource.Add(new InstallmentReportViewModel
                {
                    ProgramName = programName,
                    StrBalance = CurrencyHelper.FormatCurrencyWithPenny(balance, currency),
                    Balance = balance,
                    DoubleBalance = (double)balance,
                    InstallmentsNum = installmentNum,
                    Installments = installments,
                    PaidInstallments = paidInstallments,
                    FullName = string.Format("{0} {1}", item.FirstName, item.LastName),
                    Preapproval = hasPreapproval,
                    PastDue = (pastDue > 0) ? "Yes" : "-",
                    Email = item.Order.User.UserName,
                    Phone = item.Player.Contact.Phone,
                    Total = total,
                    DoubleTotal = (double)total,
                    StrTotal = CurrencyHelper.FormatCurrencyWithPenny(total, currency),
                    PaidAmount = paidAmount.HasValue ? paidAmount.Value : 0,
                    DoublePaidAmount = paidAmount.HasValue ? (double)paidAmount.Value : 0,
                    StrPaidAmount = CurrencyHelper.FormatCurrencyWithPenny(paidAmount, currency),
                    RegDate =
                        DateTimeHelper.ConvertUtcDateTimeToLocal(item.Order.CompleteDate, ActiveClub.TimeZone)
                            .ToString(Constants.DateTime_Comma),
                    ConfirmationId = item.Order.ConfirmationId,
                    OrderItemId = item.Id,
                    OrderId = item.Order_Id,
                    ItemStatusReason = (int)item.ItemStatusReason
                });
            }

            return dataSource.OrderByDescending(o => o.OrderDate).ToList();
        }

        private List<RegistrationFormViewModel> FillRegistrationForms(IEnumerable<OrderItem> orderItems, IncludeForms forms)
        {
            var data = new List<RegistrationFormViewModel>();

            var programOrderItems = orderItems.ToList();

            data.AddRange(programOrderItems.Select(o => new RegistrationFormViewModel
            {
                Currency = ActiveClub.Currency,
                ProgramName =
                    (o.ProgramTypeCategory == ProgramTypeCategory.Camp)
                        ? _programBusiness.GetProgramName(o.ProgramSchedule,
                            (o.OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) !=
                             null)
                                ? o.OrderChargeDiscounts.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Name
                                : string.Empty)
                        : _programBusiness.GetProgramName(o.ProgramSchedule.Program.Name,
                            (o.OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) !=
                             null)
                                ? o.OrderChargeDiscounts.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Name
                                : string.Empty),
                ParticipantName =
                    (o.Player != null) ? o.Player.Contact.FullName : string.Format("{0} {1}", o.FirstName, o.LastName),
                StartDate = DateTimeHelper.ConvertUtcDateTimeToLocal(o.ProgramSchedule.StartDate, ActiveClub.TimeZone),
                EndDate = DateTimeHelper.ConvertUtcDateTimeToLocal(o.ProgramSchedule.EndDate, ActiveClub.TimeZone),
                PostalAddress = o.ProgramSchedule.Program.ClubLocation.PostalAddress,
                Age = (o.Player != null && o.Player.Contact != null && o.Player.Contact.Age != null) ? DateTimeHelper.CalculateAge(o.Player.Contact.DoB.Value, DateTime.UtcNow) : 0,
                RegConfirmation = o.Order.ConfirmationId,
                RegistrationDate = DateTimeHelper.ConvertUtcDateTimeToLocal(o.Order.CompleteDate, ActiveClub.TimeZone),
                Charges = o.GetOrderChargeDiscounts().Select(c => new Charges
                {
                    Price = c.Amount,
                    Category = c.Category,
                    Description = c.Description,
                    Label = c.Name
                }).ToList(),
                PaidAmount = o.PaidAmount,
                Followups = (forms != null && forms.Followups) ? o.FollowupForms.Select(f => f).ToList() : null,
                Waivers = (forms != null && forms.Waivers) ? o.OrderItemWaivers.ToList() : null,
                RegForm = (forms != null && forms.Registrations) ? o.JbForm : null
            }));

            return data;
        }

        private IEnumerable<FollowupReportViewModel> FillFollowupViewModel(IEnumerable<OrderItem> orderItems)
        {
            var dataSource = new List<FollowupReportViewModel>();

            var programOrderItems = orderItems.ToList();

            dataSource.AddRange(programOrderItems.Select(orderItem => new FollowupReportViewModel
            {
                ProgramName =
                    (orderItem.ProgramTypeCategory == ProgramTypeCategory.Camp)
                        ? _programBusiness.GetProgramName(orderItem.ProgramSchedule,
                            (orderItem.OrderChargeDiscounts.FirstOrDefault(
                                c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) != null)
                                ? orderItem.OrderChargeDiscounts.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee)
                                    .Name
                                : string.Empty)
                        : _programBusiness.GetProgramName(orderItem.ProgramSchedule.Program.Name,
                            (orderItem.GetOrderChargeDiscounts().FirstOrDefault(
                                c => c.Category == ChargeDiscountCategory.EntryFee) != null)
                                ? orderItem.GetOrderChargeDiscounts().First(c => c.Category == ChargeDiscountCategory.EntryFee)
                                    .Name
                                : string.Empty),
                FullName = orderItem.FullName,
                FollowupForms = orderItem.FollowupForms.Select(f => new UserFollowupForm
                {
                    FormName = f.FormName,
                    Status = (f.Status == FollowupStatus.Completed) ? "Filled" : "-",
                    FormId = f.Id

                }).ToList(),
                Email = orderItem.Order.User.UserName,
                ConfirmationId = orderItem.Order.ConfirmationId,
                OrderId = orderItem.Order_Id,
                OrderItemId = orderItem.Id,
                RegDate = DateTimeHelper.ConvertUtcDateTimeToLocal(orderItem.Order.CompleteDate, ActiveClub.TimeZone).ToString(Constants.DateTime_Comma),
                ItemStatusReason = (int)orderItem.ItemStatusReason
            }));

            return dataSource;
        }

        private List<BalanceReportViewModel> FillBalanceViewModel(IEnumerable<OrderItem> orderItems, BalanceReportFilters filters)
        {
            var dataSource = new List<BalanceReportViewModel>();
            var service = Ioc.AccountingBusiness;

            var programOrderItems = new List<OrderItem>();

            programOrderItems = orderItems.ToList();

            foreach (var group in programOrderItems.GroupBy(i => i.ProgramSchedule.ProgramId))
            {
                dataSource.AddRange(@group.ToList()
                .Where(orderItem => orderItem.TotalAmount > orderItem.PaidAmount)
                .Select(o => new BalanceReportViewModel
                {
                    ProgramName = _programBusiness.GetProgramName(o),
                    FullName = o.FullName,
                    Email = o.Order.User.UserName,
                    PaymentDetail = (o.PaymentPlanId.HasValue)
                        ? "Installment"
                        : "Cash/Check",
                    ConfirmationId = o.Order.ConfirmationId,
                    RegDate = DateTimeHelper.ConvertUtcDateTimeToLocal(o.Order.CompleteDate, ActiveClub.TimeZone).ToString(Constants.DateTime_Comma),
                    OrderItemId = o.Id,
                    OrderId = o.Order_Id,
                    Balance =
                        (filters == BalanceReportFilters.DueDate &&
                         Ioc.InstallmentBusiness.HasOrderItemPastDueDate(o, DateTime.UtcNow))
                            ? CurrencyHelper.FormatCurrencyWithPenny(service.OrderItemDueAmount(o, DateTime.UtcNow), ActiveClub.Currency)
                            : CurrencyHelper.FormatCurrencyWithPenny(service.OrderItemBalance(o), ActiveClub.Currency),
                    ItemStatusReason = (int)o.ItemStatusReason
                }));
            }

            return dataSource;
        }

        private List<Dictionary<string, string>> FillChargeDiscountModel(IEnumerable<OrderItem> orderItems, List<OrderChargeDiscount> allcChargeDiscounts)
        {
            var programOrderItems = orderItems.ToList();
            var defaultModel = programOrderItems.ToList().Select(orderItem => new ProgramChargesModel
            {
                Charges = orderItem.GetOrderChargeDiscounts().Select(c => new OrderChargeDiscount
                {
                    Amount = c.Amount,
                    Name = c.Name,
                    Category = c.Category,
                    Subcategory = c.Subcategory,
                    //Description = c.Description,
                    Id = c.OrderItem.ProgramScheduleId.Value,
                }).ToList(),
                Email = orderItem.Order.User.UserName,
                FullName = orderItem.FullName,
                ConfirmationId = orderItem.Order.ConfirmationId,
                OrderItemId = orderItem.Id,
                ProgramName =
                    (orderItem.ProgramTypeCategory == ProgramTypeCategory.Camp)
                        ? _programBusiness.GetProgramName(orderItem.ProgramSchedule,
                            (orderItem.GetOrderChargeDiscounts().FirstOrDefault(
                                c => c.Category == ChargeDiscountCategory.EntryFee) != null)
                                ? orderItem.GetOrderChargeDiscounts().First(
                                    c => c.Category == ChargeDiscountCategory.EntryFee)
                                    .Name
                                : string.Empty)
                        : _programBusiness.GetProgramName(orderItem.ProgramSchedule.Program.Name,
                            (orderItem.GetOrderChargeDiscounts().FirstOrDefault(
                                c => c.Category == ChargeDiscountCategory.EntryFee) != null)
                                ? orderItem.GetOrderChargeDiscounts().First(
                                    c => c.Category == ChargeDiscountCategory.EntryFee)
                                    .Name
                                : string.Empty),
                RegDate = DateTimeHelper.ConvertUtcDateTimeToLocal(orderItem.Order.CompleteDate, ActiveClub.TimeZone),
                TotalPrice = orderItem.TotalAmount,
                StartEndDate =
                    string.Format("{0}_{1}",
                        DateTimeHelper.ConvertUtcDateTimeToLocal(orderItem.ProgramSchedule.StartDate, ActiveClub.TimeZone)
                            .ToString(Constants.DateTime_Comma),
                        DateTimeHelper.ConvertUtcDateTimeToLocal(orderItem.ProgramSchedule.EndDate, ActiveClub.TimeZone)
                            .ToString(Constants.DateTime_Comma)),
                OrderId = orderItem.Order_Id,
                ItemStatusReason = (int)orderItem.ItemStatusReason
            }).ToList();

            var finalModel = new List<Dictionary<string, string>>();

            var orderItemBusiness = Ioc.OrderItemBusiness;

            foreach (var item in defaultModel)
            {
                var orderItem = orderItemBusiness.GetList().SingleOrDefault(o => o.Id == item.OrderItemId);
                var newRow = new Dictionary<string, string>
                {
                    {"Program", item.ProgramName},
                    {"Full_Name", item.FullName},
                    {"Email", item.Email},
                    {"Registration_date", item.RegDate.ToString(Constants.DateTime_Comma)},
                    {"Confirmation", item.ConfirmationId},
                    {"OrderItemId", item.OrderItemId.ToString()},
                    {"OrderId", item.OrderId.ToString()},
                    {"ItemStatusReason", item.ItemStatusReason.ToString()}
                };

                //var usedCharge = new List<OrderChargeDiscount>();
                foreach (var charge in allcChargeDiscounts)
                {
                    // I set the program schedule Id in charge.Id
                    var chargeProgramScheduleId = charge.Id;

                    var chargefName = Regex.Replace(charge.Name, "[^0-9a-zA-Z_]+", "_");
                    var descrip = (charge.Category > 0)
                        ? string.Format("_s_{0}_p_", Regex.Replace(charge.Description, "[^0-9a-zA-Z_]+", "_"))
                        : string.Empty;
                    var chargeName = string.Format("{0}_{1}", descrip, chargefName);

                    if (
                        item.Charges.Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge)
                            .Any(
                                c => c.Name.ToLower().Trim() == charge.Name.ToLower().Trim() &&
                                     c.Amount == charge.Amount &&
                                     c.Category == charge.Category &&
                                     chargeProgramScheduleId == c.Id))
                    {

                        var itemAmount = item.Charges.Where(
                            c =>
                                c.Name.ToLower().Trim() == charge.Name.ToLower().Trim() &&
                                c.Amount == charge.Amount &&
                                c.Category == charge.Category).Sum(c => c.Amount
                            );
                        newRow.Add(chargeName, CurrencyHelper.FormatCurrencyWithPenny(itemAmount, ActiveClub.Currency, true));
                    }
                    else if (item.Charges.Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount)
                        .Any(
                            c => c.Category < 0 &&
                                 c.Name.ToLower().Trim() == charge.Name.ToLower().Trim() &&
                                 //c.Amount == charge.Amount &&
                                 c.Category == charge.Category))
                    {
                        var itemAmount = item.Charges.Where(
                            c =>
                                c.Name.ToLower().Trim() == charge.Name.ToLower().Trim() &&
                                //c.Amount == charge.Amount &&
                                c.Category == charge.Category).Sum(c => c.Amount
                            );
                        newRow.Add(chargeName, CurrencyHelper.FormatCurrencyWithPenny(itemAmount, ActiveClub.Currency, true));
                    }
                    else
                    {
                        newRow.Add(chargeName, "-");
                    }
                }
                newRow.Add("Total", CurrencyHelper.FormatCurrencyWithPenny(item.TotalPrice, ActiveClub.Currency, true));
                newRow.Add("Paid_amount", CurrencyHelper.FormatCurrencyWithPenny(orderItem.PaidAmount, ActiveClub.Currency, true));
                newRow.Add("Balance", CurrencyHelper.FormatCurrencyWithPenny(Ioc.AccountingBusiness.OrderItemBalance(orderItem), ActiveClub.Currency, true));
                finalModel.Add(newRow);
            }

            return finalModel;
        }

        private List<CouponReportViewModel> FillCouponeViewModel(IEnumerable<OrderChargeDiscount> allCoupons)
        {
            //var isFiltered = (couponFilter > 0);
            var model = new List<CouponReportViewModel>();

            model.AddRange(allCoupons.ToList().Select(coupon => new CouponReportViewModel
            {
                CouponName = coupon.Name,
                CouponCode = coupon.Coupon.Code,
                Amount = CurrencyHelper.FormatCurrencyWithPenny(coupon.Amount, ActiveClub.Currency, true),
                CouponId = coupon.CouponId.Value,
                OrderItemId = coupon.OrderItemId.Value,
                FullName = coupon.OrderItem.FullName,
                ConfirmationId = coupon.OrderItem.Order.ConfirmationId,
                Email = coupon.OrderItem.Order.User.UserName,
                ProgramName =
                    (coupon.OrderItem.ProgramTypeCategory == ProgramTypeCategory.Camp)
                        ? _programBusiness.GetProgramName(coupon.OrderItem.ProgramSchedule,
                            (coupon.OrderItem.OrderChargeDiscounts.FirstOrDefault(
                                c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) != null)
                                ? coupon.OrderItem.OrderChargeDiscounts.First(
                                    c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Name
                                : string.Empty)
                        : _programBusiness.GetProgramName(coupon.OrderItem.ProgramSchedule.Program.Name,
                            (coupon.OrderItem.OrderChargeDiscounts.FirstOrDefault(
                                c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) != null)
                                ? coupon.OrderItem.OrderChargeDiscounts.First(
                                    c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Name
                                : string.Empty),
                OrderId = coupon.OrderItem.Order_Id,
                RegDate =
                    DateTimeHelper.ConvertUtcDateTimeToLocal(coupon.OrderItem.Order.CompleteDate, ActiveClub.TimeZone)
                        .ToString(Constants.DateTime_Comma)
            }));
            return model;
        }

        private List<FinanceViewModel> FillFinanceModel(IEnumerable<IGrouping<long, TransactionActivity>> transactions)
        {
            var model = (from @group in transactions
                         let activity = @group.ToList()
                         select new FinanceViewModel
                         {
                             Note = activity.First().Note,
                             Confirmation = activity.First().Order.ConfirmationId,
                             Charge =
                                 (activity.First().TransactionCategory != TransactionCategory.Refund &&
                                  activity.First().TransactionCategory != TransactionCategory.Cancellation)
                                     ? activity.Sum(c => c.Amount)
                                     : 0,
                             Payment =
                                 (activity.First().TransactionCategory == TransactionCategory.Refund ||
                                  activity.First().TransactionCategory == TransactionCategory.Cancellation)
                                     ? activity.Sum(c => c.Amount)
                                     : 0,
                             Date =
                                 DateTimeHelper.ConvertUtcDateTimeToLocal(activity.First().TransactionDate, ActiveClub.TimeZone)
                                     .ToString(Constants.DateTime_Comma),
                             OrderId = (activity.First().OrderId.HasValue) ? activity.First().OrderId.Value : 0,
                             PaymentMethod = activity.First().PaymentDetail.PaymentMethod,
                             TransactionType = activity.First().TransactionType,
                             TransactionCategory = activity.First().TransactionCategory,
                             CheckId =
                                 (!string.IsNullOrEmpty(activity.First().PaymentDetail.TransactionId))
                                     ? activity.First().PaymentDetail.TransactionId
                                     : (!string.IsNullOrEmpty(activity.First().CheckId)) ? activity.First().CheckId : "-",
                             UserEmail = activity.First().Order.User.UserName,
                             PayerEmail = activity.First().PaymentDetail.PayerEmail,
                             PaypalHandler =
                                 (activity.First().PaymentDetail.PaymentMethod == PaymentMethod.Paypal &&
                                  activity.First().PaymentDetail.PaypalIPN.HasValue())
                                     ? "Jumbula"
                                     : (activity.First().PaymentDetail.PaymentMethod == PaymentMethod.Paypal &&
                                        !activity.First().PaymentDetail.PaypalIPN.HasValue())
                                         ? "Organization"
                                         : "-",
                             FullName =
                                 Regex.Replace(
                                     activity.Where(a => a.OrderItemId.HasValue && a.OrderItem != null)
                                         .Aggregate(string.Empty,
                                             (current, transactionActivity) =>
                                                 current + string.Format("{0}, ", transactionActivity.OrderItem.FullName))
                                         .Trim(), "\\,$", ""),
                             ProgramName = Regex.Replace(
                                     activity.Where(a => a.OrderItemId.HasValue && a.OrderItem != null)
                                         .Aggregate(string.Empty,
                                             (current, transactionActivity) =>
                                                 current + string.Format("{0}, ", transactionActivity.OrderItem.ProgramSchedule != null ? transactionActivity.OrderItem.ProgramSchedule.Program.Name : "Donation"))
                                         .Trim(), "\\,$", ""),
                         }).ToList();

            decimal amount = 0;
            foreach (var item in model)
            {
                amount = (item.TransactionCategory == TransactionCategory.Refund ||
                          item.TransactionCategory == TransactionCategory.Cancellation)
                    ? amount - item.Payment
                    : amount + item.Charge;
                item.Balance = amount;
            }

            return model;
        }

        private static IQueryable<OrderItem> ApplyBaseFilters(ReportFilters filters, IQueryable<OrderItem> filterData)
        {
            IQueryable<OrderItem> filteredOrderItems = filterData;

            var isInFilter = false;

            if (filters._SelectedPrograms != null)
            {
                filteredOrderItems = Ioc.OrderItemBusiness.GetCustomReportOrderItems(filters._SelectedPrograms, OrderItemStatusCategories.showAll);
            }

            if (filters.RegistrationStart.HasValue)
            {
                if (filters.RegistrationEnd.HasValue)
                {
                    var startDate = filters.RegistrationStart.Value;
                    var endDate = filters.RegistrationEnd.Value;
                    filteredOrderItems =
                        filterData
                            .Where(
                                item =>
                                    DbFunctions.TruncateTime(item.Order.CompleteDate) >=
                                    startDate &&
                                    DbFunctions.TruncateTime(item.Order.CompleteDate) <=
                                    endDate)
                            .Select(m => m);
                }
                else
                {
                    filteredOrderItems =
                        filterData.Select(order => order)
                            .Where(
                                item =>
                                    DbFunctions.TruncateTime(item.Order.CompleteDate) >=
                                    filters.RegistrationStart.Value)
                            .Select(m => m);
                }

                isInFilter = true;
            }
            else if (filters.RegistrationEnd.HasValue)
            {
                filteredOrderItems =
                        filterData.Select(order => order)
                            .Where(
                                item =>
                                    DbFunctions.TruncateTime(item.Order.CompleteDate) <=
                                    filters.RegistrationEnd.Value)
                            .Select(m => m);
            }

            if (filters.Tuition.HasValue && filteredOrderItems.Any())
            {
                filteredOrderItems =
                    filteredOrderItems.SelectMany(
                        orderItem =>
                            orderItem.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee),
                        (orderItem, charge) => new { orderItem, charge })
                        .Where(@t => @t.charge.ChargeId == filters.Tuition.Value)
                        .Select(@t => @t.orderItem);
            }
            else if (filters.Tuition.HasValue && !isInFilter)
            {
                filteredOrderItems =
                    filterData.SelectMany(
                        orderItem =>
                            orderItem.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee),
                        (orderItem, charge) => new { orderItem, charge })
                        .Where(@t => @t.charge.ChargeId == filters.Tuition.Value)
                        .Select(@t => @t.orderItem);
                isInFilter = true;
            }
            if (filters.Status.HasValue && filteredOrderItems.Any())
            {
                if (filters.Status.Value == OrderItemStatusReasons.regular)
                {
                    filteredOrderItems =
                    filteredOrderItems.Select(m => m)
                        .Where(
                            item =>
                                item.ItemStatus == OrderItemStatusCategories.completed)
                        .Select(p => p);
                }
                else if (filters.Status.Value == OrderItemStatusReasons.showAll)
                {
                    filteredOrderItems =
                        filteredOrderItems.Select(m => m)
                            .Where(
                                item =>
                                    item.ItemStatus == OrderItemStatusCategories.completed ||
                                    item.ItemStatus == OrderItemStatusCategories.changed)
                            .Select(p => p);
                }
                else
                {
                    filteredOrderItems =
                    filteredOrderItems.Select(m => m)
                        .Where(
                            item =>
                                item.ItemStatusReason == filters.Status)
                        .Select(p => p);
                }
            }
            else if (filters.Status.HasValue && !isInFilter)
            {
                if (filters.Status.Value == OrderItemStatusReasons.regular)
                {
                    filteredOrderItems =
                    filterData.Select(item => item)
                        .Where(
                            n => n.ItemStatus == OrderItemStatusCategories.completed)
                        .Select(p => p);
                }
                else if (filters.Status.Value == OrderItemStatusReasons.showAll)
                {
                    filteredOrderItems =
                        filterData.Select(item => item)
                            .Where(
                                item =>
                                    item.ItemStatus == OrderItemStatusCategories.completed ||
                                    item.ItemStatus == OrderItemStatusCategories.changed)
                            .Select(p => p);
                }
                else
                {
                    filteredOrderItems =
                    filterData.Select(item => item)
                        .Where(
                            n => n.ItemStatusReason == filters.Status)
                        .Select(p => p);
                }
            }

            return filteredOrderItems;
        }

        private static IQueryable<OrderItem> ApplyAllFilters(SelectedPrograms model, ReportType type, string term, string automaticType = "All")
        {
            var filterable = (model.Filters != null) &&
                             (model.Filters.RegistrationStart.HasValue || model.Filters.RegistrationEnd.HasValue ||
                              model.Filters.Status.HasValue || model.Filters.Tuition.HasValue);
            // Add AsNoTracking
            var allOrderItems =
                Ioc.OrderItemBusiness.GetReportOrderItems(model._SelectedPrograms, OrderItemStatusCategories.changed)
                    .AsNoTracking();

            switch (type)
            {
                case ReportType.Installment:
                    {
                        allOrderItems =
                            allOrderItems.Where(o => o.PaymentPlanType == PaymentPlanType.Installment).Select(o => o);
                        break;
                    }
                case ReportType.FollowupForm:
                    {
                        allOrderItems = allOrderItems.Where(p => p.FollowupForms.Any()).Select(o => o);

                        if (model.Filters != null)
                            switch (model.Filters.FollowupFilter)
                            {
                                case FollowupFormFilter.Completed:
                                    {
                                        allOrderItems =
                                            allOrderItems.Where(
                                                o =>
                                                    o.FollowupForms.Count > 0 &&
                                                    o.FollowupForms.All(f => f.Status == FollowupStatus.Completed))
                                                .Select(p => p);
                                        break;
                                    }
                                case FollowupFormFilter.Uncompleted:
                                    {
                                        allOrderItems =
                                            allOrderItems.Where(
                                                o =>
                                                    o.FollowupForms.Count > 0 &&
                                                    o.FollowupForms.Any(f => f.Status == FollowupStatus.NotFill))
                                                .Select(p => p);
                                        break;
                                    }
                            }

                        break;
                    }
                case ReportType.Balance:
                    {
                        allOrderItems = allOrderItems.Where(o => o.TotalAmount > o.PaidAmount).Select(o => o);

                        allOrderItems = model.Filters != null && (model.Filters.BalanceFilter == BalanceReportFilters.DueDate)
                            ? allOrderItems.Where(p => p.Order.PaymentPlanType == PaymentPlanType.Installment)
                            : allOrderItems;
                        break;
                    }
                case ReportType.ChargeDiscount:
                    {
                        break;
                    }
            }

            if (!string.IsNullOrEmpty(term))
            {
                allOrderItems = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Match(term).Success
                    ? allOrderItems.Where(p => p.Order.User.UserName.ToLower().Equals(term.ToLower()))
                    : allOrderItems.Where(
                        p => p.FirstName.ToLower().StartsWith(term.ToLower()) || p.LastName.ToLower().StartsWith(term.ToLower()));
            }

            if (automaticType == "AutoCharge")
            {
                allOrderItems = allOrderItems.Where(i => i.Order.IsAutoCharge);
            }
            if (automaticType == "NoAutoCharge")
            {
                allOrderItems = allOrderItems.Where(i => !i.Order.IsAutoCharge);
            }


            var allData = (filterable)
                ? ApplyBaseFilters(model.Filters, allOrderItems)
                : allOrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.completed);

            return allData;
        }

        private static string GeneratePropertyName(IJbBaseElement item, IJbBaseElement section, ref List<string> titles, Dictionary<string, string> fieldNames, string field)
        {
            var itemName = item.Title;
            //var itemName = item.Title;
            itemName = (itemName.Contains("_"))
                ? itemName.Substring(0, itemName.IndexOf('_'))
                : itemName;
            var sectionName = (section.Title.Contains("_"))
                ? section.Name.Substring(0, section.Title.IndexOf('_'))
                : Regex.Replace(section.Title, "Information", string.Empty,
                    RegexOptions.IgnoreCase);

            if (sectionName == "Parent/Guardian ")
            {
                sectionName = "Parent";
            }
            else if (sectionName == "Emergency Contact ")
            {
                sectionName = "Emergency";
            }
            else if (sectionName == "Parent 2/Guardian ")
            {
                sectionName = "Parent2";
            }
            else if (sectionName == "Medical/Allergy ")
            {
                sectionName = "";
            }
            else if (sectionName == "Parent 2th/Guardian ")
            {
                sectionName = "Parent2";
            }
            else if (sectionName == "Authorized Pickup 1 / Permission to Release ")
            {
                sectionName = "Authorized Pickup 1";
            }
            else if (sectionName == "Authorized Pickup 2 / Permission to Release ")
            {
                sectionName = "Authorized Pickup 2";
            }
            else if (sectionName == "Authorized Pickup 3 / Permission to Release ")
            {
                sectionName = "Authorized Pickup 3";
            }
            else if (sectionName == "Authorized Pickup 4 / Permission to Release ")
            {
                sectionName = "Authorized Pickup 4";
            }

            titles.Add(string.Format("{0}{1}{2}", sectionName, !string.IsNullOrWhiteSpace(sectionName) ? "sDash" : string.Empty, itemName));

            var fieldName = fieldNames.SingleOrDefault(f => f.Key == field).Value;

            itemName = string.Format("{0}", fieldName);

            itemName = string.Format("_{0}", itemName);

            return itemName;
        }

        private static List<OrderChargeDiscount> GetAllProgramChargeDiscounts(IQueryable<OrderItem> allData)
        {
            var allOrderChargeDiscounts = new List<OrderChargeDiscount>();

            foreach (var group in allData.GroupBy(o => o.ProgramScheduleId))
            {
                var description = string.Format("({0}_{1})",
                    group.First().ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma),
                    group.First().ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma));
                allOrderChargeDiscounts.AddRange(group.ToList()
                    .SelectMany(o => o.GetOrderChargeDiscounts())
                    .DistinctBy(c => new { c.Category, c.Name }).Select(c => new OrderChargeDiscount
                    {
                        Amount = c.Amount,
                        Category = c.Category,
                        Id = group.Key.Value,
                        Name = c.Name,
                        Description = description
                    }).ToList());
            }

            var finalChargeDiscounts = new List<OrderChargeDiscount>();
            foreach (var scheduleChargeDiscount in allOrderChargeDiscounts.GroupBy(c => c.Id))
            {
                finalChargeDiscounts.AddRange(
                scheduleChargeDiscount.Where(c => c.Category == ChargeDiscountCategory.EntryFee));

                finalChargeDiscounts.AddRange(
                    scheduleChargeDiscount.Where(
                        c =>
                            c.Category != ChargeDiscountCategory.EntryFee && c.Category > 0 &&
                            finalChargeDiscounts.All(d => d.Name != c.Name || d.Id != c.Id)));
            }

            finalChargeDiscounts.AddRange(
                    allOrderChargeDiscounts.Where(c => c.Category < 0 && finalChargeDiscounts.All(d => d.Name != c.Name)));

            return finalChargeDiscounts;
        }

        private static void UpdateMoreInfoSection(JbForm oldForm, int clubId, long seasonId)
        {
            var oldMoreInfoSection =
                (JbSection)oldForm.Elements.SingleOrDefault(s => s is JbSection && ((JbSection)s).Name == "MoreInfoSection");

            var moreInfoSection = Ioc.CustomReportBusiness.CreateMoreInfoSection();
            var allChargeInSeason = GetAllSeasonCharges(seasonId, clubId).ToList();
            if (allChargeInSeason.Any())
            {
                var charges = allChargeInSeason.Select(c => new JbCheckBox
                {
                    Name = "Charges",
                    Title = (!string.IsNullOrEmpty(c.Name)) ? c.Name.Trim() : c.Description.Trim()
                }).ToList();
                moreInfoSection.Elements.AddRange(charges);
            }
            if (oldMoreInfoSection != null)
            {
                foreach (var element in moreInfoSection.Elements)
                {
                    if (
                        oldMoreInfoSection.Elements.All(
                            e =>
                                (e.Name != "Charges" && e.Name != element.Name) ||
                                (e.Name == "Charges" && e.Title != element.Title)))
                    {
                        oldMoreInfoSection.Elements.Add(new JbCheckBox
                        {
                            Name = element.Name,
                            Title = element.Title
                        });
                    }
                }
            }

        }

        private static IEnumerable<OrderChargeDiscount> GetAllSeasonCharges(long seasonId, int clubId)
        {
            var season = Ioc.SeasonBusiness.Get(seasonId);
            var allOrdersCharges =
                Ioc.OrderItemBusiness.GetOrderItems(clubId: clubId, seasonId: seasonId, isTestMode: season.Status == SeasonStatus.Test)
                    .SelectMany(oi => oi.OrderChargeDiscounts)
                    .Where(c => !c.IsDeleted && c.Category > 0 && c.Category != ChargeDiscountCategory.EntryFee)
                    .DistinctBy(c => new { c.Name, c.Category });
            return allOrdersCharges;
        }

        private int BuildDefaultRosterReport(string clubDomain, Season programSeason)
        {
            var club = _clubBusiness.Get(clubDomain);

            var jbForm = new ProgramReportViewModel(club.Id, programSeason.Id, CustomReportType.Admin).JbForm;

            Ioc.ReportBusiness.setDefaultColumn(jbForm);


            Ioc.JbFormBusiness.CreateEdit(jbForm);

            EventRoaster report = null;

            report = new EventRoaster()
            {
                JbForm_Id = jbForm.Id,
                JbForm = jbForm,
                ClubDomain = clubDomain,
                EventType = EventRoasterType.Roster,
                HasHeader = false,
                Name = "Roster",
                LastUpdateDate = DateTime.UtcNow,
                SeasonId = programSeason.Id,
                Season = programSeason,

                FollowupForms = new List<JbForm>(),
            };
            Ioc.ReportBusiness.Create(report);

            return report.Id;
        }




        private static OperationStatus MakeCampaign(IEnumerable<ReportRecipient> recipients, string clubDomain, string reportName)
        {
            var result = Ioc.CampaignBusiness.MakeCampaignOnReport(recipients.DistinctBy(c => c.Email).ToList(), clubDomain, reportName);
            return result;
        }

        #endregion
    }
}