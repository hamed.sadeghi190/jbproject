﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jb.Framework.Common.Forms.Validation;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Jumbula.Web.Controllers
{
    public class FormBuilderController : JbBaseController
    {
        private readonly ICustomElementManager _customElementManager;

        public FormBuilderController(ICustomElementManager customElementManager)
        {
            _customElementManager = customElementManager;
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual ActionResult DisplayForm(int id)
        {
            var jbForm = Ioc.JbFormBusiness.Get(id);
            jbForm.CurrentMode = AccessMode.ReadOnly;
            return View(jbForm);
        }

        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual ActionResult Edit(int id, string entityName, int entityId)
        {
            JbForm jbForm;
            if (id > 0)
            {
                jbForm = Ioc.JbFormBusiness.Get(id);
            }
            else
            {
                jbForm = new JbForm() { Title = "", Id = 0, RefEntityName = entityName, RefEntityId = entityId };

                //////-------Get last json template from club service---------                              
                ////if (entityName.Equals("Event", StringComparison.CurrentCultureIgnoreCase))
                ////{
                ////    var activeClub = JBMembership.GetActiveClubBaseInfo();

                ////    if (Ioc.IEventService.GetAll()
                ////                                .Any(e => e.ClubDomain == activeClub.Domain && e.JbForm_Id != null))
                ////    {
                ////        jbForm = Ioc.IEventService.GetLastForm(activeClub.Domain);
                ////        if (jbForm != null)
                ////        {
                ////            jbForm.Id = 0;
                ////            jbForm.Title = string.Empty;
                ////            jbForm.RefEntityId = entityId;
                ////            jbForm.RefEntityName = entityName;
                ////        }
                ////    }

                ////}
                if (jbForm == null || !jbForm.Elements.Any())
                    DefaulSectionGenerator(jbForm);
            }
            jbForm.ChangeElementsMode(AccessMode.Edit, AccessMode.Design);
            ViewBag.JsonForm = JsonHelper.JsonSerializer(jbForm).Replace("\\\"", "").Replace("\\n", "");
            ViewBag.DefaultTemplate = TemplateService();

            return View("_Edit", jbForm);
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual string DefaultFormTemplate()
        {

            return TemplateService();
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual string JbElamentTypes()
        {
            var model = new List<IJbBaseElement>()
            {
                new JbTextBox(){CurrentMode = AccessMode.Design},
                new JbTextArea(){CurrentMode = AccessMode.Design},
                new JbDropDown(){CurrentMode = AccessMode.Design},
                new JbAddress(){CurrentMode = AccessMode.Design},
                new JbPhone(){CurrentMode = AccessMode.Design},
                new JbEmail(){CurrentMode = AccessMode.Design},
                new JbNumber(){CurrentMode = AccessMode.Design},
                new JbCheckBox(){CurrentMode = AccessMode.Design},
                new JbParagraph(){CurrentMode = AccessMode.Design},
                new JbRadioList(){CurrentMode = AccessMode.Design},
                new JbSignature(){CurrentMode = AccessMode.Design},
                new JbTextEditor(){CurrentMode = AccessMode.Design},
                //new JbDatePicker(),
            };
            var elements = _customElementManager.GetAllCustomeElement();
            if (elements != null && elements.Any())
            {
                elements.ForEach(e => e.CurrentMode = AccessMode.Design);
                model.AddRange(elements);
            }

            return JsonHelper.JsonSerializer<List<IJbBaseElement>>(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual JsonResult Build(string formJson)
        {
            formJson = formJson.Replace("&quot;", "\"");
            var jbForm = JsonHelper.JsonDeserialize<JbForm>(formJson);
            jbForm.ChangeElementsMode(AccessMode.Design, AccessMode.Edit);
            var refType = Type.GetType(string.Format("{0}.{1},{2}", Constants.SolutionConstants.DomainClassesNamespace, jbForm.RefEntityName, Constants.SolutionConstants.CoreProjectAssemblyName), true);

            jbForm.RefEntityName = refType.Name;
            Ioc.JbFormBusiness.CreateEdit(jbForm);
            return Json(new { Status = "Success", FormId = jbForm.Id });
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual JsonResult Edit(string formJson)
        {
         

            formJson = formJson.Replace("&quot;", "\"");
            var jbForm = JsonHelper.JsonDeserialize<JbForm>(formJson);

          
            var result = CheckGradeDuplication(jbForm);
            if (!result)
            {
                return Json(new { Status = false, Message = "You cannot select the  grade in both the participants and school sections." });
            }

            jbForm.ChangeElementsMode(AccessMode.Design, AccessMode.Edit);
            var refType = Type.GetType(string.Format("{0}.{1},{2}", Constants.SolutionConstants.DomainClassesNamespace, jbForm.RefEntityName, Constants.SolutionConstants.CoreProjectAssemblyName), true);

            jbForm.RefEntityName = refType.Name;
            Ioc.JbFormBusiness.Update(jbForm);

            return Json(new { Status = true, FormId = jbForm.Id, EntityId = jbForm.RefEntityId });
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual JsonResult Create(string formJson)
        {

            formJson = formJson.Replace("&quot;", "\"");
            var jbForm = JsonHelper.JsonDeserialize<JbForm>(formJson);
            jbForm.ChangeElementsMode(AccessMode.Design, AccessMode.Edit);

            jbForm.RefEntityName = "Program";

            var refType = Type.GetType(string.Format("{0}.{1},{2}", Constants.SolutionConstants.DomainClassesNamespace, jbForm.RefEntityName, Constants.SolutionConstants.CoreProjectAssemblyName), true);

            jbForm.Id = 0;

            jbForm.RefEntityName = refType.Name;
            Ioc.JbFormBusiness.CreateEdit(jbForm);
            return Json(new { Status = "Success", FormId = jbForm.Id, EntityId = jbForm.RefEntityId });
        }

        public virtual ActionResult AddTemplate(int jbFormId, string templateName, FormType formType, DefaultFormType? defaultFormType)
        {
            var userId = this.GetCurrentUserId();

            var club = Ioc.ClubBusiness.Get(base.ClubSubDomain);

            if (string.IsNullOrEmpty(templateName))
            {
                ModelState.AddModelError("model.FormName", "Form name is required.");
            }
            else
            {
                if (club.ClubFormTemplates.Any(f => f.FormType == formType && f.Title.ToLower().Equals(templateName.ToLower())))
                {
                    ModelState.AddModelError("model.FormName", "A form with this name is exist.");
                }
            }
           var jbForm = Ioc.JbFormBusiness.Get(jbFormId);
            var result = CheckGradeDuplication(jbForm);
            if (!result)
            {
                return Json(new { Status = false, Message = "You cannot select the  grade in both the participants and school sections." });
            }
            if (ModelState.IsValid)
            {
                Ioc.ClubBusiness.AddFormTemplate(club.Id, jbFormId, templateName, formType, defaultFormType);

                int templateId = Ioc.ClubBusiness.GetFormTemplates(club.Id, formType).SingleOrDefault(s => s.JbForm_Id == jbFormId).Id;

                return Json(new { Status = true, templateId = templateId });
            }

            return JsonFormResponse();
        }


        private bool CheckGradeDuplication(JbForm jbform)
        {
          var formElements=  Ioc.FormBusiness.GetFormElements(jbform,ElementsName.Grade.ToDescription());

            if (formElements!=null && formElements.Count>1)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        [HttpPost]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual JsonResult Save(JbForm jbForm)
        {
            //formJson = formJson.Replace("&quot;", "\"");
            //var jbForm = JsonHelper.JsonDeserialize<JbForm>(formJson);
            jbForm.ChangeElementsMode(AccessMode.Design, AccessMode.Edit);
            var refType = Type.GetType(string.Format("{0}.{1},{2}", Constants.SolutionConstants.DomainClassesNamespace, jbForm.RefEntityName, Constants.SolutionConstants.CoreProjectAssemblyName), true);

            jbForm.RefEntityName = refType.Name;
            Ioc.JbFormBusiness.CreateEdit(jbForm);
            return Json(new { Status = "Success", FormId = 0 });
        }

        string TemplateService()
        {
            var elemets = _customElementManager.GetAllCustomeElement();
            var res = "{";
            res += JsonHelper.EnumToJson<VisibilityMode>() + ",";
            res += "\"elementTypes\":[";
            res += JsonHelper.JsonSerializer(new JbTextBox()
            {
                Title = "Question",
                IsRequired = true,
                CurrentMode = AccessMode.Design,
                VisibleMode = VisibilityMode.Both,
                Validations =
                     new List<BaseElementValidator>
                            {
                                {
                                    new BaseElementValidator()
                                        {
                                            Type = ElementValidationType.Required,
                                            IsChecked = true,
                                            Message = "This text field is required"
                                        }
                                },
                            }

            }) + ",";
            res += JsonHelper.JsonSerializer(new JbTextArea()
            {
                Title = "Question",
                IsRequired = true,
                CurrentMode = AccessMode.Design,
                Width = ElementWidth.Full,
                VisibleMode = VisibilityMode.Both,
                Validations =
                    new List<BaseElementValidator>
                            {
                                {
                                    new BaseElementValidator()
                                        {
                                            Type = ElementValidationType.Required,
                                            IsChecked = true,
                                            Message = "This text field is required"
                                        }
                                },
                            }

            }) + ",";
            res += JsonHelper.JsonSerializer(new JbNumber()
            {
                Title = "Question",
                IsRequired = true,
                CurrentMode = AccessMode.Design,
                VisibleMode = VisibilityMode.Both,
                Validations =
                    new List<BaseElementValidator>
                            {
                                {

                                    new BaseElementValidator()
                                        {
                                            Type = ElementValidationType.Required,
                                            IsChecked = true,
                                            Message = "Number is required"
                                        }
                                },
                            }

            }) + ",";
            res += JsonHelper.JsonSerializer(new JbEmail()
            {
                Title = "Question",
                IsRequired = true,
                CurrentMode = AccessMode.Design,
                VisibleMode = VisibilityMode.Both,
                PlaceHolder = "e.g. example@gmail.com",
                Size = ElementSize.Medium,
                Validations =
                    new List<BaseElementValidator>
                            {
                                {
                                    new BaseElementValidator()
                                        {
                                            Type = ElementValidationType.Required,
                                            IsChecked = true,
                                            Message = "Email is required"
                                        }
                                },
                                 {
                                    new BaseElementValidator()
                                        {
                                            Type = ElementValidationType.Email,
                                            IsChecked = true,
                                            Message = "Email address is invaid"
                                        }
                                },
                            }

            }) + ",";
            res += JsonHelper.JsonSerializer(new JbCheckBox()
            {
                Title = "Question",
                CurrentMode = AccessMode.Design,
                Width = ElementWidth.Full,
                VisibleMode = VisibilityMode.Both,
            }) + ",";
            res += JsonHelper.JsonSerializer(new JbAddress()
            {
                Title = "Question",
                CurrentMode = AccessMode.Design,
                Width = ElementWidth.Full,
                VisibleMode = VisibilityMode.Both,
            }) + ",";
            res += JsonHelper.JsonSerializer(new JbPhone()
            {
                Title = "Question",
                IsRequired = true,
                CurrentMode = AccessMode.Design,
                VisibleMode = VisibilityMode.Both,
                Validations =
                   new List<BaseElementValidator>
                            {
                                {

                                    new BaseElementValidator()
                                        {
                                            Type = ElementValidationType.Required,
                                            IsChecked = true,
                                            Message = "phone is required"
                                        }
                                },
                            }
            }) + ",";
            res += JsonHelper.JsonSerializer(new JbDropDown()
            {
                Title = "Question",
                IsRequired = true,
                CurrentMode = AccessMode.Design,
                VisibleMode = VisibilityMode.Both,
                Validations = new List<BaseElementValidator> { { new BaseElementValidator() { Type = ElementValidationType.Required, IsChecked = true, Message = "This field is required" } }, },
                Items = new List<JbElementItem>() { { new JbElementItem() { Text = "First", Value = "01" } }, { new JbElementItem() { Text = "Second", Value = "02" } }, { new JbElementItem() { Text = "Third", Value = "03" } } }
            }) + ",";
            res += JsonHelper.JsonSerializer(new JbRadioList()
            {
                Title = "Question",
                IsRequired = true,
                CurrentMode = AccessMode.Design,
                VisibleMode = VisibilityMode.Both,
                Width= ElementWidth.Full,
                Validations = new List<BaseElementValidator> { { new BaseElementValidator() { Type = ElementValidationType.Required, IsChecked = true, Message = "This field is required" } }, },
                Items = new List<JbElementItem>() { { new JbElementItem() { Text = "First", Value = "First" } }, { new JbElementItem() { Text = "Second", Value = "Second" } }, { new JbElementItem() { Text = "Third", Value = "Third" } } }
            }) + ",";
            res += JsonHelper.JsonSerializer(new JbParagraph()
            {
                Title = "Question",
                CurrentMode = AccessMode.Design,
                Width = ElementWidth.Full,
                VisibleMode = VisibilityMode.Both

            }) + ",";
            res += JsonHelper.JsonSerializer(new JbSection()
            {
                Title = "Question",
                CurrentMode = AccessMode.Design,
                VisibleMode = VisibilityMode.Both,
            }) + ",";
            foreach (var customElement in elemets)
            {
                customElement.CurrentMode = AccessMode.Design;
                customElement.VisibleMode = VisibilityMode.Both;
                var cc = JsonHelper.JsonSerializer(customElement);
                res += cc;
            }
            res += "]";
            res += "}";
            return res;
        }

        public virtual ActionResult AngularEditorTemplate()
        {
            return PartialView("AngularEditorTemplate");
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual string GetDefaultJbForm(DefaultFormType defaultFormType, bool elements = false)
        {
            var jbForm = new JbForm();


            switch (defaultFormType)
            {
                case DefaultFormType.Primary:
                    {
                        jbForm = Ioc.FormBusiness.GetDefaultRegistrationForm();
                    }
                    break;
                case DefaultFormType.School:
                    {
                        jbForm = Ioc.FormBusiness.GetDefaultSchoolRegistrationForm();
                    }
                    break;
                case DefaultFormType.TeamTournament:
                    break;
                default:
                    break;
            }

            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new StringEnumConverter { CamelCaseText = false });

            var res = elements == false ? JsonHelper.JsonSerializer(jbForm, settings) : jbForm.JsonElements;

            return res;
        }

        [HttpGet]
        [Authorize(Roles = "Admin, Support, Manager")]
        public virtual JsonResult GetFormConstants()
        {
            //Get grade
            var grades = typeof(SchoolGradeType).GetWithOrder().Select(s =>
            new
            {
                Title = s.Text,
                Value = ((int)EnumHelper.ParseEnum<SchoolGradeType>(s.Value)).ToString(),
                Visible = s.Text != "PreSchool 3" || s.Text != "PreSchool 4"
            }).ToList();

            //Get gender
            var genders = Enum.GetValues(typeof(GenderCategories)).Cast<GenderCategories>().Where(g => g != GenderCategories.All && g != GenderCategories.None).Select(v => new
            {
                Title = EnumHelper.ToDescription((v)),
                Value = v.ToString(),
                Visible = v != GenderCategories.GenderNeutral
            }).ToList();

            //Get parentRelationships
            var parentRelationships = Enum.GetValues(typeof(ParentRelationship)).Cast<ParentRelationship>().Select(v => new
            {
                Title = EnumHelper.ToDescription((v)),
                Value = ((int)v).ToString(),
                Visible = true
            }).ToList();

            //Get relationships
            var relationships = Enum.GetValues(typeof(Relationship)).Cast<Relationship>().Select(v => new
            {
                Title = EnumHelper.ToDescription((v)),
                Value = ((int)v).ToString(),
                Visible = true
            }).ToList();

            //Get hearAboutUs
            var hearAboutUs = Enum.GetValues(typeof(HearAboutUs)).Cast<HearAboutUs>().Select(v => new
            {
                Title = EnumHelper.ToDescription((v)),
                Value = ((int)v).ToString(),
                Visible = true
            }).ToList();

            //Get SelfAdministeredMedication
            var selfAdministeredMedication = Enum.GetValues(typeof(SelfAdministeredMedication)).Cast<SelfAdministeredMedication>().Select(v => new
            {
                Title = EnumHelper.ToDescription((v)),
                Value = ((int)v).ToString(),
                Visible = true
            }).ToList();

            //Get NutAllergy
            var nutAllergy = Enum.GetValues(typeof(NutAllergy)).Cast<NutAllergy>().Select(v => new
            {
                Title = EnumHelper.ToDescription((v)),
                Value = ((int)v).ToString(),
                Visible = true
            }).ToList();


            //Get PermissionPhotography
            var permissionPhotography = Enum.GetValues(typeof(PermissionPhotography)).Cast<PermissionPhotography>().Select(v => new JbElementItem
            {
                Text = EnumHelper.ToDescription((v)),
                Value = ((int)v).ToString()
            }).ToList();

            var result = new
            {
                Grades = grades,
                Genders = genders,
                ParentRelationships = parentRelationships,
                Relationships = relationships,
                HearAboutUs = hearAboutUs,
                SelfAdministeredMedication = selfAdministeredMedication,
                NutAllergy = nutAllergy,
                PermissionPhotography = permissionPhotography
            };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        void DefaulSectionGenerator(JbForm jbForm)
        {
            if (jbForm == null)
                jbForm = new JbForm();
            Ioc.PlayerProfileBusiness.ProfileSectionsGenerator(jbForm);
        }
    }
}
