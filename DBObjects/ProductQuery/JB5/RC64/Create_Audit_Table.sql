

CREATE TABLE [dbo].[JbAudits](
	[Id][uniqueidentifier] NOT NULL DEFAULT ('00000000-0000-0000-0000-000000000000'),
	[DateTime] [datetime] NOT NULL DEFAULT (getdate()),
	[Zone] [nvarchar](max) NULL,
	[Url] [nvarchar](max) NOT NULL,
	[IpAddress] [nvarchar](max) NOT NULL,
	[ClubDomain] [nvarchar](max) NULL,
	[UserName] [nvarchar](max) NULL,
	[Action] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Browser] [nvarchar](max) NULL,
	[Device] [nvarchar](max) NULL,
	[OwnerKey] [nvarchar](max) NULL,
	[StatusCode] int NULL,
	[Status] [bit] NOT NULL DEFAULT ((0))
    CONSTRAINT PK_JbAudits PRIMARY KEY (Id)
 ) 

GO


