﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.Email;
using Newtonsoft.Json.Linq;
using Stripe;

namespace Jumbula.Web.Controllers
{
    [AllowAnonymous]
    public class StripeWebhookController : JbBaseController
    {
 
        public virtual ActionResult StripeWebhookListner()
        {

            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);

            // string json = @"{""id"":""evt_18vEBqC06xbBji0pCbw3MctL"",""object"":""event"",""api_version"":""2015-10-16"",""created"":1474239314,""data"":{""object"":{""id"":""ch_18vEBpC06xbBji0pSjMD4Kdp"",""object"":""charge"",""amount"":16620,""amount_refunded"":0,""application_fee"":""fee_9DfWnzdY4M4YaU"",""balance_transaction"":""txn_18vEBqC06xbBji0plnNi5FnQ"",""captured"":true,""created"":1474239313,""currency"":""usd"",""customer"":null,""description"":""Received payment for confirmation: 14B3-B0-F19C."",""destination"":null,""dispute"":null,""failure_code"":null,""failure_message"":null,""fraud_details"":{},""invoice"":null,""livemode"":true,""metadata"":{""ActionCaller"":""1"",""Confirmation id"":""14B3-B0-F19C"",""1_Participant"":""Auron Strickland"",""1_Program"":""GameMaker Studio (Oct 03, 2016 - Dec 05, 2016), 8 - week (60 minutes)""},""order"":null,""paid"":true,""receipt_email"":null,""receipt_number"":null,""refunded"":false,""refunds"":{""object"":""list"",""data"":[],""has_more"":false,""total_count"":0,""url"":""/v1/charges/ch_18vEBpC06xbBji0pSjMD4Kdp/refunds""},""shipping"":null,""source"":{""id"":""card_18vEBpC06xbBji0pYaxUS3nf"",""object"":""card"",""address_city"":""Arlington"",""address_country"":""US"",""address_line1"":""1125 N. Kenilworth St"",""address_line1_check"":""pass"",""address_line2"":""Apt 3"",""address_state"":""VA"",""address_zip"":""22205"",""address_zip_check"":""pass"",""brand"":""Visa"",""country"":""US"",""customer"":null,""cvc_check"":""pass"",""dynamic_last4"":null,""exp_month"":8,""exp_year"":2017,""fingerprint"":""VpeIgjIQe33o1wdf"",""funding"":""credit"",""last4"":""0620"",""metadata"":{},""name"":""Latonia Strickland"",""tokenization_method"":null},""source_transfer"":null,""statement_descriptor"":null,""status"":""succeeded""}},""livemode"":true,""pending_webhooks"":1,""request"":""req_9DfWkppWxaYdap"",""type"":""charge.succeeded"",""user_id"":""acct_18XYcrC06xbBji0p""}";
            string json =new StreamReader(req).ReadToEnd();

            JToken dataObj = null;
            string type;
            try
            {

                JObject obj = JObject.Parse(json);
                type = (string)obj.SelectToken("type");

                dataObj = obj.SelectToken("data.object");
               
                var eventId = (string)obj.SelectToken("id");
                var stripeWebhook = new StripeWebhook()
                {
                    Created = (string)obj.SelectToken("created"),
                    Data =  dataObj != null ? dataObj.ToString() : "",
                    EventId = eventId,
                    Type = type,
                    Webhook = json,
                    livemode = (bool)obj.SelectToken("livemode")
                };
                Ioc.StripeWebhookBusiness.Create(stripeWebhook);
               

            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, ex.Message + "Unable to parse incoming event");
            }

            if (dataObj == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Incoming event empty");

            switch (type)
            {
                case "charge.pending":
                    // ACH payment After creating the charge, you will receive a charge.pending notification
                    break;
                case "charge.succeeded":
                    {
                        // ACHYou will receive a charge.succeeded notification once the charge has transitioned to succeeded and the funds are available in your balance.
                        UpdateCharge(dataObj.ToString());
                        break;
                    }
                case "charge.failed":
                    {
                        StripeCharge stripeCharge = Mapper<StripeCharge>.MapFromJson(dataObj.ToString());
                        string confirmationId;
                        if (stripeCharge.Metadata.ContainsKey("Confirmation id"))
                        {
                            confirmationId = stripeCharge.Metadata["Confirmation id"];
                            var order = Ioc.OrderBusiness.Get(confirmationId);
                            if (order != null && order.TransactionActivities != null && order.TransactionActivities.Any(c => c.PaymentDetail.PayKey == stripeCharge.Id && c.PaymentDetail.Status != PaymentDetailStatus.INCOMPLETE))
                            {
                                foreach (var tran in order.TransactionActivities.Where(c => c.PaymentDetail.PayKey == stripeCharge.Id && c.PaymentDetail.Status != PaymentDetailStatus.INCOMPLETE))
                                {
                                    tran.TransactionStatus = TransactionStatus.Failure;
                                    tran.PaymentDetail.Status = PaymentDetailStatus.INCOMPLETE;
                                }

                                Ioc.OrderBusiness.Update(order);
                                EmailService.Get(this.ControllerContext).SendEcheckClearedNotification(true, order.Id, (decimal)(stripeCharge.Amount / 100.00));
                            }
                            
                        }
                        break;
                    }
                case "account.updated":
                    // Occurs whenever an account status or property has changed.
                    break;
                case "account.application.deauthorized":
                    // Occurs whenever a user deauthorizes an application. Sent to the related application only.
                    break;
                case "application_fee.created":
                    // Occurs whenever an application fee is created on a charge.

                    break;
                case "application_fee.refunded":
                    // Occurs whenever an application fee is refunded, whether from refunding a charge or from refunding the application fee directly, including partial refunds.

                    break;
                case "charge.captured":
                    // Occurs whenever a previously uncaptured charge is captured.
                    break;
                case "charge.refunded":
                    ChargeRefunded(dataObj.ToString());
                    break;

                case "charge.updated":
                    // Occurs whenever a charge description or metadata is updated.
                    break;
                case "customer.created":
                    // Occurs whenever a new customer is created.
                    break;
                case "customer.deleted":
                    // Occurs whenever a customer is deleted.
                    break;
                case "customer.updated":
                    // Occurs whenever any property of a customer changes.
                    break;



            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        private void ChargeRefunded(string dataObj)
        {
            StripeCharge stripeCharge = Mapper<StripeCharge>.MapFromJson(dataObj);
           
                            if(stripeCharge.AmountRefunded==0)
                            {
                                EmailService.Get(this.ControllerContext).SendErrorMessaged(dataObj);
                            }
                          
           
        }

        private void UpdateCharge(string dataObj)
        {
            try {
                StripeCharge stripeCharge = Mapper<StripeCharge>.MapFromJson(dataObj);

                var payAction = JumbulaSubSystem.Order;
                if (stripeCharge.Metadata.ContainsKey("ActionCaller"))
                {
                    payAction = ((JumbulaSubSystem)(Convert.ToInt32(stripeCharge.Metadata.ContainsKey("ActionCaller"))));
                }
                switch (payAction)
                {
                    case JumbulaSubSystem.Order:
                        {
                            if (stripeCharge.Metadata.ContainsKey("Confirmation id"))
                            {
                                var confirmationId = stripeCharge.Metadata["Confirmation id"];
                                var order = Ioc.OrderBusiness.Get(confirmationId);
                                if (order != null && order.TransactionActivities != null)
                                {
                                    foreach (OrderItem item in order.RegularOrderItems)
                                    {
                                        item.PaidAmount += item.TransactionActivities.Where(i => i.PaymentDetail.PaymentMethod == PaymentMethod.Echeck && i.TransactionStatus == TransactionStatus.Pending && i.PaymentDetail.PayKey == stripeCharge.Id).Sum(t => t.Amount);

                                        if (item.PaymentPlanType == PaymentPlanType.Installment && item.Installments.Any())
                                        {
                                            var firstInst = item.Installments.FirstOrDefault();
                                            if (firstInst != null)
                                            {
                                                item.PaidAmount = firstInst.Amount;
                                                firstInst.PaidAmount = firstInst.Amount;
                                            }
                                        }
                                    }

                                    if (order.TransactionActivities.Any(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Echeck)  && order.TransactionActivities.Any(c => c.PaymentDetail.PayKey == stripeCharge.Id))
                                    {
                                        foreach (var item in order.RegularOrderItems)
                                        {
                                            foreach (var tran in item.TransactionActivities.Where(c => c.TransactionStatus == TransactionStatus.Pending && c.PaymentDetail.PayKey == stripeCharge.Id))
                                            {
                                                tran.TransactionStatus = TransactionStatus.Success;
                                                tran.PaymentDetail.Status = PaymentDetailStatus.COMPLETED;
                                            }
                                        }
                                       
                                        Ioc.OrderBusiness.Update(order);

                                        EmailService.Get(this.ControllerContext).SendEcheckClearedNotification(false, order.Id, (decimal)(stripeCharge.Amount / 100.00));

                                    }
                                }
                            }
                         
                            break;
                        }
                    case JumbulaSubSystem.Installment:
                    case JumbulaSubSystem.Invoice:
                    case JumbulaSubSystem.MakeaPayment:
                        break;
                }
               
            }
            catch(Exception ex)
            {
                LogHelper.LogException(ex);
            }

        }
    }
}