﻿using Jumbula.Common.Enums;
using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models.Register
{
    public class RegisterChargeDiscountModel : BaseViewModel<Charge, long>
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public ChargeDiscountCategory Category { get; set; }
        public decimal Amount { get; set; }
        public decimal ProratePrice { get; set; }
        public decimal EarlyBirdPrice { get; set; }
        public DateTime EarlyBirdExpireDate { get; set; }
        public string Description { get; set; }
        public bool IsFull { get; set; }
        public bool HasEarlyBird { get; set; }
        public bool HasProrate { get; set; }
        public bool HasNoSession { get; set; }
        public bool IsMandatory { get; set; }
        public int Capacity { get; set; }
        public int NumberOfRegistration { get; set; }
        public int LeftSessions { get; set; }
        public bool EditMode { get; set; }
        public int TotalSessions { get; set; }
        public decimal ChangedPrice { get; set; }
        public bool IsPriceChanged { get; set; }
        public string DropInLable { get; set; }
        public int CapacityLeft
        {
            get
            {
                if (Capacity > 0)
                {
                    return (Capacity - NumberOfRegistration) > 0 ? (Capacity - NumberOfRegistration) : 0;
                }
                return -1;
            }
        }
    }
}