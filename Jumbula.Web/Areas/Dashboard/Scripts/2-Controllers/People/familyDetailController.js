﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('familyDetailController', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', function ($scope, $http, jbToast, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'family-detail-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/People';

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });
        $scope.pageTitle = "";
        $http.get(baseUrl + '/FamilyDetails/?userId=' + $scope.MC.stateParams.userId + '&clubId=' + $scope.MC.stateParams.clubId)
            .success(function (data, status, headers, config) {
                $scope.Model = data;
                if ( $scope.Model.TagName != "")
                {
                    $scope.pageTitle = $scope.Model.TagName;
                }
                else
                {
                    $scope.pageTitle = $scope.Model.UserName;
                }
            });

        $scope.parentsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetParentsList/?userId=' + $scope.MC.stateParams.userId,
                        type: "POST",
                    },

                },
            schema: {
                total: "total",
                data: "data"
            },
            serverSorting: false,
            batch: true,
            resizable: false,
        });
        $scope.parentsGridOptions = {
            dataSource: $scope.parentsDataSource,
            sortable: true,
            resizable: false,
            columns: [
                {
                    field: "FullName",
                    title: "Full name",
                },
                {
                    field: "DOB",
                    title: "Date of birth",
                },
                {
                    field: "Gender",
                    title: "Gender",
                },
                {
                    field: "EmailAddress",
                    title: "Email",
                },
                {
                    field: "Phone",
                    title: "Phone",
                }
            ],
        }
        $scope.participantsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetParticipantList/?userId=' + $scope.MC.stateParams.userId,
                        type: "POST",
                    },

                },
            schema: {
                total: "total",
                data: "data"
            },
            serverSorting: false,
            batch: true,
            resizable: false,
        });

        $scope.participantsGridOptions = {
            dataSource: $scope.participantsDataSource,
            sortable: true,
            resizable: false,
            columns: [
                {
                    field: "FullName",
                    title: "Full name",
                },
                {
                    field: "DOB",
                    title: "Date of birth",
                },
                {
                    field: "Gender",
                    title: "Gender",
                },
                {
                    field: "Graduated",
                    title: "Status",
                },
                {
                    field: "TotalOrders",
                    title: "Number of orders",
                },
                  {
                      field: "Balance",
                      title: 'Balance',
                      template: "<span>#if(Balance >0) {# <p class='fg-red'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance <0) {# <p class='fg-green'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance ==0) {# <p>#=Balance#</p> #}#"
                  },
                  {
                      title: "Actions",
                      width: "70px",
                      headerTemplate: "<span class='actions-title'>Actions</span>",
                      template: '<ul kendo-menu style="display: inline-block;position:absolute;overflow: visible" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    #if(data.Graduated != "graduated"){#\
                                    <li ng-click="Graduate(#=PlayerId#)" >\
                                          <span class="jbi-user-graduate k-link ">Set status as graduated</span>\
                                    </li>\
                                    #}#\
                                   #if(data.Graduated == "graduated"){#\
                                   <li ng-click="UnGraduate(#=PlayerId#)" >\
                                          <span class="jbi-user-male k-link">Set status as not graduated</span>\
                                    </li>\
                                   #}#\
                            </ul>\
                        </li>\
                    </ul>'
                  }
               
            ],
        }

        $scope.Graduate = function (playerId) {

            $http.post(baseUrl + '/SaveEducationalStatus', { playerId: playerId })
                     .success(function (data, status, headers, config) {

                         if (data.Status == true) {

                             jbToast.success("Participant status updated successfully.", "");

                             $("#familyParticipants").data("kendoGrid").dataSource.read();
                         }
                         else {
                             $scope.MC.handleErrors($scope, data);
                         }
                     });
        }

        $scope.UnGraduate = function (playerId) {

            $http.post(baseUrl + '/CancelGraduate', { playerId: playerId })
                    .success(function (data, status, headers, config) {

                        if (data.Status == true) {

                            jbToast.success("Participant status updated successfully.", "");

                            $("#familyParticipants").data("kendoGrid").dataSource.read();
                        }
                        else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
        }

        $scope.goBack=function()
        {
            $state.go('Families');
        }
    }]);
})();