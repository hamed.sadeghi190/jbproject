﻿using System;
using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface ISubscriptionBusiness
    {
        List<ProgramSubscriptionItem> GenerateSubscriptionSchedules(DateTime startDate, DateTime endDate, decimal amount, int dueDate);

        bool IsSubscriptionSchedulesPreviouslyEdited(long programId);

        List<ProgramSubscriptionItem> GetAppliedSubscriptionItems(int clubId, List<ProgramSubscriptionItem> subscriptionItems, DateTime? desiredStartDate);

        decimal GetFirstSubscriptionAmount(List<ProgramSubscriptionItem> subscriptionItems, OrderItem orderItem);

        decimal GetTotalSubscriptionAmount(List<ProgramSubscriptionItem> subscriptionItems, OrderItem orderItem);

        decimal GetFirstSubscriptionAmount(ProgramSchedule programSchedule, DateTime? desiredStartDate, long selectedChargeId = 0);

        List<ProgramSubscriptionItem> GetProgramSubscriptionItems(OrderItem orderItem);

        decimal CalculateSubscriptionPayableAmount(OrderItem orderItem);

        decimal CalclulateSurcharge(OrderItem orderItem, Charge surcharge);

        void CalculateOrderInstallment(OrderItem orderItem, List<OrderInstallment> installmentList);

        void CalculateInstallment(OrderItem orderItem, List<OrderInstallment> installmentList);
        decimal GetTotalSubscriptionAmount(ProgramSchedule programSchedule, DateTime? desiredStartDate, long selectedChargeId = 0);

        Charge GetSelectedCharge(ProgramSchedule programSchedule, long chargeId);

        Charge GetSelectedCharge(OrderItem orderItem);

        ProgramSubscriptionItem GetAppliedSubscriptionItem(ProgramSchedule programSchedule, DateTime? desiredStartDate);

        Charge GetApplicationFee(Program program);

        ScheduleSubscriptionAttribute GetSubscriptionAtribute(ProgramSchedule programSchedule);

        ProgramSchedulePart GetSubscriptionItemFromDesiredStartDate(Program program, DateTime desiredStartDate);

        void ManipulateCartOrderItem(OrderItem orderItem);

        decimal CalculateChargeForAppliedItems(OrderItem orderItem, OrderChargeDiscount charge);

        decimal CalculateDiscountForAppliedItems(OrderItem orderItem, OrderChargeDiscount charge);

        void SetInstallmentsStatus(OrderItem orderItem, DateTime date);

        bool IsInstallmentItemIsForPay(OrderItem orderItem, OrderInstallment orderInstallment, TransactionStatus transactionStatus);

        decimal GetFullPayDiscount(ProgramSchedule programSchedule, decimal totalAmount);

        int GetAppliedSubscriptionItemsCount(OrderItem orderItem);

        List<ProgramSession> GetDropInProgramSessions(Program program, List<long> sessionIds);
        decimal CalculateItemAmount(ProgramSubscriptionItem subscriptionItem, OrderItem orderItem);

        int MigrateSessions();
        //ProgramSubscriptionItem GetMonthDesiredStartDate(OrderItem orderItem, DateTime desiredStartdate);

        bool IsFull(OrderItem orderItem);
        ScheduleAfterBeforeCareAttribute GetBeforAfterCareAtribute(ProgramSchedule programSchedule);
        ProgramSchedulePart GetAppliedProgramPart(ProgramSchedule programSchedule, DateTime? desiredStartDate);
        Charge GetCharge(long id);
        decimal GetTotalBeforeAfterCareAmount(ProgramSchedule programSchedule, DateTime? desiredStartDate, List<DayOfWeek> selectedDays, long selectedChargeId = 0);
        int GetPassScheduleDueDate(OrderItem orderItem);
        ProgramSchedulePart GetMonthDesiredStartDate(long programId, DateTime desiredStartdate);
        decimal CalculateDiscountForShowInView(OrderItem orderItem, OrderChargeDiscount discount, decimal totalDiscountAmount);
        bool IsProrateProgramPart(ProgramSchedulePart appliedProgramPart, ProgramSchedule schedule, DateTime? desiredStartDate);
        decimal CalculateBeforeAfterCareProrateAmount(ProgramSchedulePart prorateItem, DateTime desiredStartDate, Charge charge, List<DayOfWeek> days);
        dynamic GetOrderItemAdditionalInformation(OrderItem orderItem);
        decimal CalculateCurrentScheduleAmount(Program program, DateTime desiredStartDate, int selectedChargeId, List<DayOfWeek> days);
        decimal CalculateSubscriptionCoupon(OrderItem orderItem, Coupon coupon);
        List<ProgramSchedulePart> GetAppliedProgramParts(List<ProgramSchedulePart> programScheduleParts, ProgramSchedule schedule, DateTime? desiredStartDate);

    }
}
