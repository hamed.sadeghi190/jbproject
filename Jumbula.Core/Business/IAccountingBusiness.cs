﻿using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Core.Business
{
    public interface IAccountingBusiness
    {
        decimal OrderItemBalance(OrderItem orderItem);
        decimal OrderItemsBalance(List<OrderItem> orderItems);
        decimal OrderItemsBalance(IQueryable<OrderItem> orderItems);
        decimal GetClientBalance(Client client);
        decimal OrderBalance(Order order);
        decimal OrderItemDueAmount(OrderItem orderItem, DateTime? date = null);
        decimal OrderDueAmount(Order order, DateTime? date = null);
    }
}
