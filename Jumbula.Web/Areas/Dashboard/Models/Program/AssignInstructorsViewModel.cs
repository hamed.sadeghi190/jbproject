﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AssignInstructorsViewModel
    {
        public List<int> SelectedInstructors { get; set; }

        public List<SelectKeyValue<int>> AllInstructors { get; set; }
    }
}