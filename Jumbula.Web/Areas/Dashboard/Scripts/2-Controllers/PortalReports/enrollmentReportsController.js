﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('enrollmentReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });
        $scope.baseUrl = window.location.origin + "/Dashboard/PortalReport/";

        $scope.Model = {};

        $http.get($scope.baseUrl + '/GetEnrollmentInfo').success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.runReport = function () {
            if ($scope.Model.Name == "SelectSeason" || $scope.Model.Year == 0) {
                if ($scope.Model.Name == "SelectSeason") {
                    jbToast.error("Plaese select season");
                }
                if ($scope.Model.Year == 0) {
                    jbToast.error("Plaese select year");
                }
            }
            else {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = '/Dashboard/PortalReport/GenerateEnrollmentExcel?name=' + $scope.Model.Name + '&year=' + $scope.Model.Year + '';
                a.download = "SchoolsEnrollment.xlsx";
                a.click();
            }
           
        };

      

    }]);
})();