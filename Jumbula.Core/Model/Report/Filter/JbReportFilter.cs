﻿using System.Collections.Generic;

namespace Jumbula.Core.Model.Report.Filter
{
    public class JbReportFilter
    {
        public JbReportFilter()
        {
            Elements = new List<IJbReportFilterElement>();
        }

        public List<IJbReportFilterElement> Elements { get; set; }
    }
}
