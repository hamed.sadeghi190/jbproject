﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaTokenExpiredException : JumbulaAccountException
    {
        public JumbulaTokenExpiredException(Exception e)
            : base(e)
        {
        }
    }
}
