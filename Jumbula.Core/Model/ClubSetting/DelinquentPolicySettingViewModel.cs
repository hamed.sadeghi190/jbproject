﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Constants;
using Jumbula.Common.Helper;

namespace Jumbula.Core.Model.ClubSetting
{
    public class DelinquentPolicySettingViewModel
    {
        private const string AutoChargeFailAttemptTitle = "{0} attempt";

        private static readonly Dictionary<string, string> GetManualPaymentDayOptions = new Dictionary<string, string>
        {
            {"1", "1 day"}, {"2", "2 days"}, {"3", "3 days"}, {"4", "4 days"}, {"5", "5 days"}, {"6", "6 days"},
            {"7", "7 days"}, {"8", "8 days"}, {"9", "9 days"}, {"10", "10 days"}, {"11", "11 days"}, {"12", "12 days"},
            {"13", "13 days"}, {"14", "14 days"}, {"15", "15 days"}, {"16", "16 days"}, {"17", "17 days"},
            {"18", "18 days"}, {"19", "19 days"}, {"20", "20 days"}, {"21", "21 days"}, {"22", "22 days"},
            {"23", "23 days"}, {"24", "24 days"}, {"25", "25 days"}, {"26", "26 days"}, {"27", "27 days"},
            {"28", "28 days"}, {"29", "29 days"}, {"30", "30 days"}
        };

        public List<SelectListItem> AutoChargeOptions
        {
            get
            {
                var result = new List<SelectListItem>();

                for (var i = 1; i <= Common.Constants.ClubSetting.CountOfAttempts; i++)
                    result.Add(new SelectListItem { Value = i.ToString(), Text = string.Format(AutoChargeFailAttemptTitle, NumberHelper.NumberToAdjective(i)).FirstCharToUpper() });

                return result;
            }
        }

        public List<SelectListItem> ManualPaymentDayOptions => GetManualPaymentDayOptions.Select(x => new SelectListItem { Text = x.Value.ToString(), Value = x.Key.ToString() }).ToList();
        public bool AutoChargeFailAttempted { get; set; } = false;
        public string AutoChargeFailAttemptNumber { get; set; } = 1.ToString();
        public bool ManualInstallmentDelayed { get; set; } = false;
        public string ManualInstallmentDelayDays { get; set; } = 1.ToString();
        public bool CalculateFamilyBalance { get; set; } = false;
        public DateTime? FamilyBalanceNoLimitDate { get; set; } = null;

        [RegularExpression(Validation.PriceRegex, ErrorMessage = Validation.PriceRegexErrorMessage)]
        [Display(Name = "Minimum unpaid balance")]
        public decimal FamilyBalanceMinPrice { get; set; } = decimal.Zero;
        public bool AllowEnrollOnDelinquent { get; set; } = true;
        public string NotAllowEnrollTemplate { get; set; } = Delinquent.NotAllowEnrollTemplate;
        public string AllowEnrollTemplate { get; set; } = Delinquent.AllowEnrollTemplate;
        public bool ExpelChildOnDelinquent { get; set; } = false;
        public string ExpelChildTemplate { get; set; } = Delinquent.ExpelChildTemplate;
    }
}