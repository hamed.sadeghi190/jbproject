﻿using Jb.Framework.Common;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class FieldSettingBusiness : IFieldSettingBusiness
    {
        private readonly IRepository<JbFieldSetting> _fieldSettingRepository;
        private readonly IEntitiesContext _dataContext;

        public FieldSettingBusiness(IRepository<JbFieldSetting> fieldSettingRepository, IEntitiesContext dataContext)
        {
            _fieldSettingRepository = fieldSettingRepository;
            _dataContext = dataContext;
        }

        public List<JbFieldSetting> GetDefaultFieldSettings(JbClass jbClass)
        {
            ICustomizableEntity entity = null;

            Type type = Type.GetType(jbClass.ToDescription());
            entity = (ICustomizableEntity)Activator.CreateInstance(type);

            return entity.GetDefaultSetting();
        }

        public List<JbFieldSetting> GetFieldSettings(JbClass jbClass, int clubId)
        {
            var result = new List<JbFieldSetting>();
            var defaultSettings = GetDefaultFieldSettings(jbClass);
            var customizedSettings = _fieldSettingRepository.GetList().Where(s => s.ClubId == clubId && s.Class == jbClass).ToList();

            var fieldsWhichNotInCustomized = defaultSettings.Where(d => !customizedSettings.Select(s => s.FieldName).Contains(d.FieldName)).ToList();

            result.AddRange(customizedSettings);
            result.AddRange(fieldsWhichNotInCustomized);

            return result;
        }

        public bool GetFieldActiveStatus(List<JbFieldSetting> fieldSettings, string fieldName)
        {
            return fieldSettings.Any(f => f.FieldName == fieldName) && fieldSettings.Single(f => f.FieldName == fieldName).IsEnabled;
        }

        public bool GetFieldShowInGridStatus(List<JbFieldSetting> fieldSettings, string fieldName)
        {
            return fieldSettings.Any(f => f.FieldName == fieldName) && fieldSettings.Single(f => f.FieldName == fieldName).ShowInGrid;
        }

        public JbFieldSetting GetFieldSettings(JbClass jbClass, int clubId, string fieldName)
        {
            var result = GetFieldSettings(jbClass, clubId).Where(s => s.FieldName == fieldName).LastOrDefault();

            return result;
        }

        public OperationStatus SaveSettings(List<JbFieldSetting> fieldSettings)
        {

            foreach (var item in fieldSettings.Where(d => d.Id == 0).ToList())
            {
                _dataContext.Set<JbFieldSetting>().Add(item);
            }


            var fieldSettingIds = fieldSettings.Where(f => f.Id != 0).Select(f => f.Id).ToList();

            var changedSettings = _fieldSettingRepository.GetList(s => fieldSettingIds.Contains(s.Id)).ToList();

            foreach (var item in changedSettings)
            {
                var changed = fieldSettings.SingleOrDefault(f => f.Id == item.Id);

                if (changed != null)
                {
                    item.IsEnabled = changed.IsEnabled;
                    item.ShowInGrid = changed.ShowInGrid;
                }
            }

            return _fieldSettingRepository.Save();
        }
    }
}
