﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('couponAddController', ['$scope', '$http', '$log', '$stateParams', '$state', 'couponAddEditService', function ($scope, $http, $log, $stateParams, $state, couponAddEditService) {
        window.app.globalObjects.pageClass = 'seasons-view step1';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });

        couponAddEditService($scope, $http, $stateParams);
    }]);
})();