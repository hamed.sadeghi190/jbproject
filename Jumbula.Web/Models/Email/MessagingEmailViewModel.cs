﻿namespace Jumbula.Web.Models.Email
{
    public class MessagingEmailViewModel
    {
        public string Reciver{ get; set; }
        public string Sender { get; set; }
        public string Message { get; set; }
        public string MessageUrl { get; set; }
        public string LogoUrl { get; set; }
        public string ClubName { get; set; }
        public string ProgramName { get; set; }
    }
}