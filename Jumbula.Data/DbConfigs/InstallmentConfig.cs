﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class InstallmentConfig : EntityTypeConfiguration<OrderInstallment>
    {
        public InstallmentConfig()
        {
            HasMany(p => p.TransactionActivities).WithOptional(c => c.Installment).HasForeignKey(c => c.InstallmentId).WillCascadeOnDelete(true);
            HasMany(p => p.PendingPreapprovalTransactions).WithOptional(c => c.Installment).HasForeignKey(c => c.InstallmentId).WillCascadeOnDelete(true);
        }
    }
}
