﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programAddEditStep4Controller', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function($scope, $http, $log, $stateParams, $state,  jbToast) {
        $scope.baseUrl = window.location.origin + "/Dashboard/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.commisionRateRange = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

        $scope.seasonName = $stateParams.seasonDomain;

        $http.get($scope.baseUrl + '/ProgramCreateEditStep4', { params: { programType: $stateParams.programType, seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId } }).success(function (data, status, headers, config) {
            $scope.Model = data;

            if ($scope.Model.LastCreatedPage.split("Step")[1] < 3) {
                jbToast.warning("Please setup your program step by step.")
                $state.go('ProgramAddEditStep' + (Number($scope.Model.LastCreatedPage.split("Step")[1]) + 1), { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
            }
        }).error(function() {
            jbToast.error("Something went wrong!");
        });

        $scope.saveStep4 = function () {

            $http.post($scope.baseUrl + '/ProgramCreateEditStep4', { model: $scope.Model })
               .success(function (data, status, headers, config) {
                   if (data.Status == true) {
                       $scope.agreementConfirmed = true;
                   }
                   else {
                       $scope.MC.handleErrors($scope, data);
                   }
               });
        };


    }]);
})();