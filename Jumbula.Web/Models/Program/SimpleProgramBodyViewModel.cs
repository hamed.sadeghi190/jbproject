﻿
using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Domain.Generic;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Models
{
    public class SimpleProgramBodyViewModel : BaseViewModel<Program, long>, IProgramBodyViewModel
    {
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public Club Club { get; set; }
        public string Domain { get; set; }
        public bool EnablePriceHidden { get; set; }
        public string WeeklyEmail { get; set; }
        public string MaterialsNeeded { get; set; }
        public string Name { get; set; }
        public string MinEnrollment { get; set; }
        public string MaxEnrollment { get; set; }
        public string Description { get; set; }
        public string RoomAssignment { get; set; }
        public DateTime? StartDate { get; set; }

        public bool DisableCapacityRestriction { get; set; }
        public List<ClassSessionsDateTime> ClassDates { get; set; }
        public string strClassDates { get

            {
                return GenerateClassDateStrings();
            }

        }

     
        public DateTime? EndDate { get; set; }

        public PostalAddress Address { get; set; }

        public string LocationName { get; set; }

        public bool HideGoogleMapInClassPage { get; set; }
        public string ClubLogo { get; set; }

        public Jumbula.Common.Enums.TimeZone ClubTimeZone { get; set; }
        public CurrencyCodes Currency { get; set; }
        public ProgramBodySchedule Schedule { get; set; }

        private string GenerateClassDateStrings()
        {
           if(ClassDates!=null && ClassDates.Any())
            {
                var fisrtItem = true;
                var result = string.Empty;
                foreach(var item in ClassDates)
                {
                    if(!fisrtItem)
                    {
                        result += ", ";
                    }
                    result += item.SessionDate.ToString(Constants.DefaultDateFormat);
                    if(item.TimeChanges)
                    {
                        result += "*";
                    }
                    fisrtItem = false;
                }
                return result;
            }
            return string.Empty;
        }
    }
}