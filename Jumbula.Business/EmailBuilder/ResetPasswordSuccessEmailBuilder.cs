﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using System.Collections.Generic;
using System.Net.Mail;

namespace Jumbula.Business.EmailBuilder
{
    public class ResetPasswordSuccessEmailBuilder : BaseEmailBuilder<ResetPasswordSuccessParameterModel>
    {
        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private readonly IUserProfileBusiness _userProfileBusiness;
        private string _userName;
        private Club _club;
        private RequestType _requestType;
        #endregion

        #region Constants

        #endregion

        #region Constructors
        public ResetPasswordSuccessEmailBuilder(IClubBusiness clubBusiness, IUserProfileBusiness userProfileBusiness)
        {
            _clubBusiness = clubBusiness;
            _userProfileBusiness = userProfileBusiness;
        }
        #endregion

        #region Properties
        public override EmailCategory Category => EmailCategory.ResetPasswordSuccess;

        public override string TemplateName => "ResetPasswordSuccessEmail";

        public override EmailCallerType CallerType => EmailCallerType.User;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.Normal;

        public override bool ShouldSendToSupport => true;

        #endregion

        #region Public Methods
        public override void Initiate(ResetPasswordSuccessParameterModel parameters)
        {
            _userName = parameters.UserName;
            _requestType = parameters.RequestType;
            _club = parameters.ClubId.HasValue ? _clubBusiness.Get(parameters.ClubId.Value) : null;
        }

        public override void InitiatePreview(ResetPasswordSuccessParameterModel parameters)
        {
        }

        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            ResetPasswordSuccessBodyModel result;

            if (_requestType == RequestType.Branded)
            {
                result = new ResetPasswordSuccessBodyModel
                {
                    RequestType = _requestType,
                    HomePageUrl = _club.Site,
                    LogoUrl = _clubBusiness.GetClubLogo(_club),
                    HomeTitle = _club.Name,
                    UserFullName = _userName
                };

            }
            else
            {
                result = new ResetPasswordSuccessBodyModel
                {
                    RequestType = _requestType,
                    HomePageUrl = Constants.JumbulaDotCom_Url,
                    LogoUrl = $"{Constants.JumbulaDotCom_Url}Images/jb-logo.png",
                    HomeTitle = Constants.W_Jumbula,
                    UserFullName = _userName
                };
            }

            return result;
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            var result = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);

            if (_requestType == RequestType.Branded)
                result = new MailAddress(Constants.W_NoReplyEmailAddress, _club.Name);

            return result;
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return _requestType == RequestType.Branded ? _club.Name : Constants.W_Jumbula;
        }

        public override string GetReplyTo(string subCategory = null)
        {
            return Constants.W_NoReplyEmailAddress;
        }

        public override string GetSubject(string subCategory = null)
        {
            var result = Constants.F_ResetPasswordSuccessSubject;

            if (_requestType == RequestType.Branded)
                result = $"{_club.Name} password reset notification";

            return result;
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            return new List<MailAddress> { new MailAddress(_userName) };
        }

        public override List<CommunicationHistory> GetHistories()
        {
            var user = _userProfileBusiness.Get(_userName);

            return new List<CommunicationHistory>()
            {
                new CommunicationHistory{ RelatedEntity = nameof(JbUser), RelatedEntityId = user.Id},
            };
        }
        #endregion
    }
}
