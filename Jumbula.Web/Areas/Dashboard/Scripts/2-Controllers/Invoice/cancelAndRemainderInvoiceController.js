﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('cancelAndRemainderInvoiceController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', function ($scope, $http, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/Invoice";


        $scope.Model = {};

        $http.get($scope.baseUrl + '/CancelAndReminderInvoice?invoiceId=' + $stateParams.invoiceId).success(function (data, status, headers, config) {

            $scope.Model = data;
        });


        $scope.pageMode;

        if ($state.current.name == 'CancelationInvoice') {

            $scope.pageMode = 'Cancel'
        }
        else {
            $scope.pageMode = 'Reminder'
        }


        $scope.Cancelation = function () {
            $('#CancelInvoice').prop("disabled", true);
            $http.post($scope.baseUrl + '/SubmitCancelationInvoice', { model: $scope.Model }).success(function (data, status, headers, config) {

                if (data.Status == true) {
                    $state.go('InvoiceDetail', { invoiceId: $stateParams.invoiceId }, 'CancelationInvoice', { invoiceId: $stateParams.invoiceId }, function () { });
                    jbToast.success("You have canceled this invoice.");
                } else {
                    $scope.MC.handleErrors($scope, data);
                }

            })
        };

        $scope.Reminder = function () {
            $('#ReminderInvoice').prop("disabled", true);
            $http.post($scope.baseUrl + '/SubmitReminderInvoice', { model: $scope.Model }).success(function (data, status, headers, config) {
                if (data.Status == true) {

                    $state.go('InvoiceDetail', { invoiceId: $stateParams.invoiceId }, 'ReminderInvoice', { invoiceId: $stateParams.invoiceId }, function () { });
                    jbToast.success("You have sent invoice reminder.");
                } else {
                    $('#ReminderInvoice').prop("disabled", false);
                    $scope.MC.handleErrors($scope, data);
                }

            })
        };


        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("Invoices", null);
                }
            }
            else {
                $state.go("Invoices", null);
            }
        }

    }])

})();