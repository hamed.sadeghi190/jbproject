﻿namespace SportsClub.Areas.Dashboard.Models
{
    public class ProviderContactInfoViewModel
    {
        public string ClassName { get; set; }
        public string ProviderName { get; set; }
        public string FullName { get; set; }
        public string ContactEmail { get; set; }
        public string Phone { get; set; }
        public string Instructor1St { get; set; }
        public string Phone1StInstructor { get; set; }
        public string Instructor2nd { get; set; }
        public string Phone2ndInstructor { get; set; }
    }

    public class RoomAssignmentViewModel
    {
        public string ClassName { get; set; }
        public string SpaceReq { get; set; }
        public string Duration { get; set; }
        public string Day { get; set; }
        public string Sessions { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public string Room { get; set; }
        public string AdditionalComments { get; set; }
    }
}