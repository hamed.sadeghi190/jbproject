﻿using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class NotificationSettingElements : BaseElements
    {
        public NotificationSettingElements(IWebDriver driver)
            : base(driver)
        {

        }
        #region Header

        public IWebElement BackLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@class='icon-arrow-left']"));
            }
        }
        public IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//div[@class='title']/h1"));
            }
        }

        #endregion Header

        public IWebElement ConfirmationEmailBody
        {
            get
            {
                return Driver.FindElement(By.Id("ConfirmationEmailBody"));
            }
        }
        public IWebElement SaveConfirmationBtn
        {
            get
            {
                return Driver.FindElement(By.Id("SaveConfirmationEmailBody"));
            }
        }
        public IWebElement CancellationEmailBody
        {
            get
            {
                return Driver.FindElement(By.Id("CancellationEmailBody"));
            }
        }
        public IWebElement SaveCancellationBtn
        {
            get
            {
                return Driver.FindElement(By.Id("SaveCancellationEmailBody"));
            }
        }
        public IWebElement MessageboxTitle
        {
            get
            {
                return Driver.FindElement(By.ClassName("toast-title"));
            }
        }
        public IWebElement MessageboxBody
        {
            get
            {
                return Driver.FindElement(By.ClassName("toast-message"));
            }
        }

    }
}
