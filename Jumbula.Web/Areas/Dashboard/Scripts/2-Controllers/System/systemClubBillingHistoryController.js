﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemClubBillingHistoryController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();


        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.Model = {};

        $scope.clubId = $stateParams.clubId;

        $http.get($scope.baseUrl + '/GetClubName?ClubId=' + $stateParams.clubId).success(function (data, status, headers, config) {
            $scope.Model = data;
        });

        $scope.mainGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + '/GetAllClubBillingHistories/?clubId=' + $scope.clubId,
                        cache: false,
                        type: "POST"

                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: '',
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: null
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            sortable: false,
            serverFiltering: true,
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: false,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "Date",
                    title: "Date"
                },
                {
                    field: "Description",
                    title: "Description",
                    template: "#if(Credit >0) {# <a ui_sref='SystemBillingHistoryReceipt({clubId: clubId, billingId: #=Id#})'  class='block'> #= Description # </a> #} else {# <p>#=Description#</p> #}#"
                }
            , {
                field: "StrDebit",
                title: "Debit",
            },
             {
                 field: "StrCredit",
                 title: "Credit",
             },
             {
                 field: "BillingId",
                 title: "Billing Id",
             },
             {
                 field: "Balance",
                 title: 'Balance',
                 template: "<span>#if(Balance >0) {# <p class='fg-red'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance <0) {# <p class='fg-green'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance ==0) {# <p>#=Balance#</p> #}#"
             },
             {
                 title: "Actions",
                 width: "70px",
                 headerTemplate: "<span class='actions-title'>Actions</span>",
                 template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                                  #if(Credit>0 && Balance>=0){#\
                                      <li> ...\
                                         <ul>\
                                              <li ui-sref="SystemClubRefund({clubId: clubId, billingId: #=Id#})" >\
                                                         <span>Refund</span>\
                                                </li>\
                                          </ul>\
                                     </li>\
                                   #}#\
                              </ul>'
             }],
        };

        $scope.Takepayment = function (clubId, billingId) {
            window.comesFromOrderItem = true;
            $state.go("SystemPaymentWithCredit", { clubId: $stateParams.clubId, billingId: $stateParams.billingId });
        }
        $scope.chargeWithCredit = function (clubId, billingId) {
            window.comesFromOrderItem = true;
            $state.go("SystemChargeClub", { clubId: $stateParams.clubId, billingId: $stateParams.billingId });
        }
        $scope.AddDebit = function (clubId, billingId) {
            window.comesFromOrderItem = true;
            $state.go("AddDebitForClub", { clubId: $stateParams.clubId, billingId: $stateParams.billingId });
        }

    }]);
})();