﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class OrderItemWaiverBusiness:  IOrderItemWaiverBusiness
    {
        private readonly IRepository<OrderItemWaiver> _orderItemWaiverRepository;
        public OrderItemWaiverBusiness(IRepository<OrderItemWaiver> orderItemWaiverRepository)
        {
            _orderItemWaiverRepository = orderItemWaiverRepository;
        }

        public OperationStatus Create(OrderItemWaiver orderItemWaivar)
        {
            return _orderItemWaiverRepository.Create(orderItemWaivar);

        }

        public IQueryable<OrderItemWaiver> GetList()
        {
            return _orderItemWaiverRepository.GetList();
        }
        public OrderItemWaiver Get(int WaiverId)
        {
            return _orderItemWaiverRepository.GetList().SingleOrDefault(w => w.Id == WaiverId);
        }

    }
}
