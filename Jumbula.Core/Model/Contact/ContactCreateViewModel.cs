﻿
using System.ComponentModel.DataAnnotations;


namespace Jumbula.Core.Model.Contact
{
    public class ContactCreateViewModel
    {
        [Display(Name = "First name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string LastName { get; set; }

        [Display(Name = "Email address")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }

        public bool ShowEmail { get; set; }

        [Display(Name = "Phone number")]
        [RegularExpression("[0-9]+$",
             ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string Phone { get; set; }

        public bool ShowPhone { get; set; }

        [StringLength(64)]
        public string PhoneExtension { get; set; }
    }
}
