﻿using System;

namespace Jumbula.Core.Model.Report
{
    public class MemberListReportBuilderViewModel
    {
        public string Name { get; set; }

        public string ContactName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? AgreementExpirationDate { get; set; }
    }
}
