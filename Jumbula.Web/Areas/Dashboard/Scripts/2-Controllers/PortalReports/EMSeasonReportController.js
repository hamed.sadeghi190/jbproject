﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('EMSeasonReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReports", order: 1, menu: "PortalReports" });

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            $scope.stateParams = $stateParams;
            $scope.MC.stateParams = $stateParams;
            $scope.Model = {};

            $scope.reportName = "";
            var nameModel = customReportService.getModelFromStrorage();
            var seasonId = nameModel.seasonId;
            var schoolId = nameModel.schoolId;

            if (is.not.undefined(nameModel) && nameModel != null) {


                $http.get('Dashboard/PortalReport/GetReoprtNameWithSeason', { params: { schoolId: schoolId, seasonId: seasonId } }).success(function (data, status, headers, config) {
                    $scope.reportName = data;


                }).error(function (data, status, headers, config) {

                    jbToast.error(data.Message);
                });
            }

            $scope.ProgramsReportGridOptions = {

                excel: {
                    allPages: true,
                    fileName: "EM Season Reconciliation Report.xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();
                },
                dataSource: {
                    type: "json",
                    transport: {
                        read: {
                            url: '/Dashboard/PortalReport/SelectEMSeasonList?seasonId=' + seasonId + '&schoolId=' + schoolId,
                        }
                    },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },

                    batch: true,
                    serverSorting: false,
                    resizable: true,
                },
                scrollable: true,
                sortable: true,
                editable: false,

                columns: [

                       {
                           field: "SchoolSeasonname",
                           title: "Season name",
                           width: "260px",
                       },
                       {
                           field: "TotalRegistrations",
                           title: "Total # of registrations",
                           width: "200px",
                       },
                       {
                           field: "TotalPaidRegistrations",
                           title: "Total # of fully paid registrations",
                           width: "250px",

                       },
                        {
                            field: "TotalPaymentsCollected",
                            title: "Total Amt of payments collected",
                            width: "300px",
                            format: "{0:c2}",
                        },
                        {
                            field: "ProvFundedScholarship",
                            title: "Total Amt of provider-funded scholarships",
                            width: "450px",
                            format: "{0:c2}",
                        },
                        {
                            field: "TotalCashPayments",
                            title: "Total amt of cash payments to PTA/PTO",
                            width: "330px",
                            format: "{0:c2}",
                        },
                         {
                             field: "TotalPTAfundedScholarship",
                             title: "Total Amt of PTA/PTO-funded scholarships",
                             width: "350px",
                             format: "{0:c2}",
                         },
                         {
                             field: "TotalGrossIncome",
                             title: "Total gross income",
                             width: "200px",
                             format: "{0:c2}",
                         },
                        {
                            field: "Totalfees",
                            title: "Total 4% fees",
                            width: "200px",
                            format: "{0:c2}",
                        },
                        {
                            field: "Totalproviderdeductions",
                            title: "Total provider deductions",
                            width: "300px",
                            format: "{0:c2}",
                        },
                        {
                            field: "TotalLateRegistrationfees",
                            title: "Total late registration fees",
                            width: "300px",
                            format: "{0:c2}",
                        },
                         {
                             field: "TotalCancellationFees",
                             title: "Total cancellation fees",
                             width: "250px",
                             format: "{0:c2}",
                         },
                         {
                             field: "TotalPTAdonationsreceived",
                             title: "Total PTA donations received",
                             width: "230px",
                             format: "{0:c2}",
                         },
                         {
                             field: "TotalPTAActivityFeesReceived",
                             title: "Total PTA activity fees received",
                             width: "300px",
                             format: "{0:c2}",
                         },
                          
                          {
                              field: "Totalproviderpayments",
                              title: "Total provider payments",
                              width: "200px",
                              format: "{0:c2}",
                          },
                           {
                               field: "TotalPTApayments",
                               title: "Total PTA payments",
                               width: "200px",
                               format: "{0:c2}",
                           },
                           {
                               field: "Totaljumbulafee",
                               title: "Total jumbula fees",
                               width: "200px",
                               format: "{0:c2}",
                           },
                           {
                               field: "TotalStripeFees",
                               title: "Total stripe fees",
                               width: "200px",
                               format: "{0:c2}",
                           },
                             {
                                 field: "TotalNetEMIncome",
                                 title: "Total net EM income",
                                 width: "200px",
                                 format: "{0:c2}",
                             }],


            };

            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                $("#ProgramsReportGrid").getKendoGrid().saveAsExcel();
            }
        }
    ]);


})();