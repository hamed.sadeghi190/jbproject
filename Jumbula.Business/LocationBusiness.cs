﻿using Jb.Framework.Common;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System.Linq;

namespace Jumbula.Business
{
    public class LocationBusiness : ILocationBusiness
    {
        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private readonly IEntitiesContext _dataContext;
        private readonly IRepository<ClubLocation> _clubLocationRepository;
        #endregion

        #region Constractors
        public LocationBusiness(IClubBusiness clubBusiness , IEntitiesContext dataContext, IRepository<ClubLocation> clubLocationRepository)
        {
            _clubBusiness = clubBusiness;
            _dataContext = dataContext;
            _clubLocationRepository = clubLocationRepository;
        }
        #endregion

        #region Methode

        public ClubLocation GetClubLocation(int id)
        {
            return _dataContext.Set<ClubLocation>().SingleOrDefault(c => c.Id == id);
        }
        public bool IsLocationAssignToAnotherProgram(int clubId, long? programId, long locationId)
        {
            bool result;
            long id = programId ?? 0;
            var club = _clubBusiness.Get(clubId);

            var allClubPrograms = club.Seasons.SelectMany(s => s.Programs.Where(p => p.Status != ProgramStatus.Deleted)).AsQueryable();

            var programs = allClubPrograms.Where(p => p.ClubLocationId == locationId);
            var count = programs.Count();

            if (count > 1)
            {
                result = true;
            }
            else
            {
                if (count == 0 || count == 1 && id > 0)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }

            return result;
        }

        public OperationStatus DeleteLocation(int id)
        {
            var clubLocation = _dataContext.Set<ClubLocation>().SingleOrDefault(c => c.Id == id);

            if (clubLocation != null)
            {
                clubLocation.IsDeleted = true;  
            }

            return UpdateLocation(clubLocation);
        }
        public OperationStatus UpdateLocation(ClubLocation clubLocation)
        {
            var oldClubLocation = _dataContext.Set<ClubLocation>().SingleOrDefault(c => c.Id == clubLocation.Id);

            if (oldClubLocation != null)
            {
                oldClubLocation.Name = clubLocation.Name;
                oldClubLocation.PostalAddress = clubLocation.PostalAddress;
            }

            return _clubLocationRepository.Save();
        }

        public IQueryable<ClubLocation> GetClubLocationsByClubId(int clubId)
        {
            return _clubBusiness.Get(clubId).ClubLocations.Where(l => !l.IsDeleted).AsQueryable();
        }

        public OperationStatus CreateLocation(ClubLocation clubLocation)
        {
            _dataContext.Set<ClubLocation>().Add(clubLocation);

            return _clubLocationRepository.Save();
        }
        #endregion
    }
}
