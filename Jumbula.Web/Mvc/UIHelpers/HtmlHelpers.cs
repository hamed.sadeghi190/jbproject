﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.WebLogic.UserService;

namespace Jumbula.Web.Mvc.UIHelpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString File(this HtmlHelper html, string name, string accept = "")
        {
            var tb = new TagBuilder("input");

            if (!String.IsNullOrEmpty(accept))
            {
                tb.Attributes.Add("accept", accept);
            }

            tb.Attributes.Add("name", name);
            tb.GenerateId(name);

            tb.Attributes.Add("type", "file");

            return MvcHtmlString.Create(tb.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, string accept = "")
        {
            string name = GetFullPropertyName(expression);
            return html.File(name, accept);
        }

        public static IHtmlString MetaAcceptLanguage<t>(this HtmlHelper<t> html)
        {
            var clubDomain = JbUserService.GetCurrentClubDomain();

            if (string.IsNullOrEmpty(clubDomain))
            {
                return new MvcHtmlString(string.Empty); 
            }

            var acceptLanguage = Ioc.ClubBusiness.GetClubCurrency(clubDomain);

            return new HtmlString($"<meta name=\"accept-language\" content=\"{acceptLanguage.ToDescription()}\">");
        }

        public static string FormatCurrency(this HtmlHelper helper, int money)
        {
            var clubCurrency = Ioc.ClubBusiness.GetClubCurrency(JbUserService.GetCurrentClubDomain());

            return CurrencyHelper.FormatCurrencyWithoutPenny(money, clubCurrency);
        }

        public static string FormatCurrency(this HtmlHelper helper, decimal money)
        {
            var clubCurrency = Ioc.ClubBusiness.GetClubCurrency(JbUserService.GetCurrentClubDomain());

            return CurrencyHelper.FormatCurrencyWithoutPenny(money, clubCurrency);
        }

        public static string FormatCurrencyWithPenny(this HtmlHelper helper, decimal money)
        {
            var clubCurrency = Ioc.ClubBusiness.GetClubCurrency(JbUserService.GetCurrentClubDomain());

            return CurrencyHelper.FormatCurrencyWithPenny(money, clubCurrency);
        }

        public static string FormatCurrencyWithPenny(this HtmlHelper helper, decimal? money)
        {
            if (!money.HasValue)
                return string.Empty;

            var clubCurrency = Ioc.ClubBusiness.GetClubCurrency(JbUserService.GetCurrentClubDomain());

            return CurrencyHelper.FormatCurrencyWithPenny(money, clubCurrency);
        }


        public static MvcHtmlString DisplayLocation(this HtmlHelper helper, PostalAddress address, string locationName, string room)
        {
            List<string> list = new List<string>();
            string result = String.Empty;

            // Add room to result if it is not empty
            if (!String.IsNullOrEmpty(room))
            {
                list.Add(String.Format("Room: {0}", room));
            }

            // Add location name to result if it is not empty
            if (!String.IsNullOrEmpty(locationName))
            {
                list.Add(locationName);
            }

            // Add address to result if it is not empty
            if (address != null && !String.IsNullOrEmpty(address.Address))
            {
                list.Add(address.Address);
            }

            // return location name + room + address separated by cama
            result = String.Format("<address class='pull-left hidden-phone'>{0}</address>", String.Join(", ", list));

            return MvcHtmlString.Create(result);
        }


        public static string SubDomainFileContent(this HtmlHelper helper, string domain, string subDomain, string fileName)
        {
            var uri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, domain, subDomain, fileName);
            var mvc = MvcHtmlString.Create(uri.AbsoluteUri);
            return mvc.ToString();
        }

        public static string SubDomainFileContent(this HtmlHelper helper, string domain, string fileName)
        {
            var uri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, domain, fileName);
            var mvc = MvcHtmlString.Create(uri.AbsoluteUri);
            return mvc.ToString();
        }
        public static MvcHtmlString CheckBoxWithLabel(this HtmlHelper htmlHelper, string name)
        {
            MvcHtmlString checkbox = InputExtensions.CheckBox(htmlHelper, name);
            MvcHtmlString lable = LabelExtensions.Label(htmlHelper, name, new { @for = name });
            string[] tag = Regex.Split(checkbox.ToString(), "/>");
            //string result = tag[0] + "/>" + lable.ToString() + tag[1] + "/>";
            return MvcHtmlString.Create(String.Format("{0}/>{1}/>", tag[0], lable.ToString() + tag[1]));
        }

        public static MvcHtmlString CheckBoxWithLabel(this HtmlHelper htmlHelper, string name, string value, bool isChecked, string typeId, object htmlAttributes)
        {
            MvcHtmlString checkbox = InputExtensions.CheckBox(htmlHelper, name, isChecked, htmlAttributes);
            MvcHtmlString lable = LabelExtensions.Label(htmlHelper, name, value, new { @for = typeId });
            string[] tag = Regex.Split(checkbox.ToString(), "/>");
            //string result = tag[0] + "/>" + lable.ToString() + tag[1] + "/>";
            return MvcHtmlString.Create(String.Format("{0}/>{1}/>", tag[0], lable.ToString() + tag[1]));
        }

        public static MvcHtmlString GenerateHiddenFields<TModel>(this HtmlHelper<TModel> Html, IJbBaseElement model)
        {
            var result = String.Empty;
            result += Html.Hidden("Name", model.Name).ToHtmlString();
            result += Html.Hidden("Title", model.Title).ToHtmlString();
            result += Html.Hidden("HelpText", model.HelpText).ToHtmlString();
            result += Html.Hidden("ElementId", model.ElementId).ToHtmlString();
            result += Html.Hidden("CurrentMode", model.CurrentMode).ToHtmlString();
            result += Html.Hidden("VisibleMode", model.VisibleMode).ToHtmlString();
            result += Html.Hidden("ModelType", model.GetType()).ToHtmlString();

            if(model.IsConditional)
            {
                result += Html.Hidden("IsConditional", model.IsConditional).ToHtmlString();
                result += Html.Hidden("RelatedElementId", model.RelatedElementId).ToHtmlString();
                result += Html.Hidden("RelatedElementValue", model.RelatedElementValue).ToHtmlString();
            }

            result = model.AccessRole.Aggregate(result, (current, accRole) => current + Html.HiddenFor(modelItem => accRole).ToHtmlString());

            return MvcHtmlString.Create(result);
        }

        public static Dictionary<string, object> AddAttr(this Dictionary<string, object> attrArray, string key, object value)//this HtmlHelper<TModel> Html,
        {
            var temp = attrArray.ToArray().ToList();
            temp.Add(new KeyValuePair<string, object>(key, value));
            return temp.ToDictionary(d => d.Key, d => d.Value);
        }
        public static MvcHtmlString Ang<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            //return new MvcHtmlString(typeof(TModel).Name.Replace("ViewModel", string.Empty) + "." + html.IdFor(expression));
            return new MvcHtmlString("Model." + html.IdFor(expression));
        }

        public static MvcHtmlString NgValFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return NgValFor(htmlHelper, expression, null /* validationMessage */, new RouteValueDictionary());
        }

        public static MvcHtmlString NgValFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage)
        {
            return NgValFor(htmlHelper, expression, validationMessage, new RouteValueDictionary());
        }

        public static MvcHtmlString NgValFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage, object htmlAttributes)
        {
            return NgValFor(htmlHelper, expression, validationMessage, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public static MvcHtmlString NgValFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage, IDictionary<string, object> htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, new ViewDataDictionary<TModel>());
            var name = ExpressionHelper.GetExpressionText(expression);
            var validations = ModelValidatorProviders.Providers.GetValidators(
                    metadata ?? ModelMetadata.FromStringExpression(name, new ViewDataDictionary()),
                    new ControllerContext())
                .SelectMany(v => v.GetClientValidationRules()).ToArray();

            var validatorMessages = validations.ToDictionary(k => k.ValidationType, v => v.ErrorMessage);

            string result = "";

            result += GetNgValDirectiveString(validatorMessages);
            result += GetValidatorDirectivesString(validations);

            return new MvcHtmlString(result);
        }

        private static string GetValidatorDirectivesString(IEnumerable<ModelClientValidationRule> validations)
        {
            var result = "";
            foreach (var val in validations)
            {
                result += " " + ConvertMvcClientValidatorToAngularValidatorString(val);
            }
            return result;
        }

        private static string ConvertMvcClientValidatorToAngularValidatorString(ModelClientValidationRule val)
        {
            switch (val.ValidationType.ToLower())
            {
                case "required":
                    return "required";
                case "range":
                    return String.Format("ng-minlength=\"{0}\" ng-maxlength=\"{1}\"", val.ValidationParameters["min"], val.ValidationParameters["max"]);
                case "regex":
                    return String.Format("ng-pattern=\"{0}\"", val.ValidationParameters["pattern"]);
                case "length":
                    string lengthRes = "";
                    if (val.ValidationParameters.ContainsKey("min"))
                        lengthRes += String.Format("ng-minlength=\"{0}\" ", val.ValidationParameters["min"]);
                    if (val.ValidationParameters.ContainsKey("max"))
                        lengthRes += String.Format("ng-maxlength=\"{0}\"", val.ValidationParameters["max"]);
                    return lengthRes.TrimEnd();
                default:
                    return String.Format("{0}=\"{1}\"", val.ValidationType, Json.Encode(val.ValidationParameters));
            }
        }

        private static string GetNgValDirectiveString(Dictionary<string, string> validatorMessages)
        {
            return String.Format("ngval='{0}'", Json.Encode(validatorMessages));
        }

        public static MvcHtmlString AngValidationFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            string modelName = "model." + html.IdFor(expression);

            string result = String.Format(@"<div ng-repeat= {0}err in errors.formErrors['{1}'].Messages{2}> <span>{{{{err}}}}</span> <br /> </div>", "\"", modelName, "\"");

            return new MvcHtmlString(result);
        }

        public static MvcHtmlString AngValidation(this HtmlHelper html, string name)
        {
            string modelName = "model." + name;

            string result = String.Format("<p ng-repeat={0}err in errors.formErrors['{1}'].Messages{2}> <span class='error font italic'>{{{{err}}}}</span> <br /> </p>", "\"", modelName, "\"");

            return new MvcHtmlString(result);
        }

        public static MvcHtmlString AngValidationClassFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            string modelName = "model." + html.IdFor(expression);

            string result = String.Format("ng-class=\"{{'error-state':MC.isEmpty(errors.formErrors['{0}'].Messages)}}\"", modelName);

            return new MvcHtmlString(result);
        }

        public static MvcHtmlString AngValidationClass(this HtmlHelper html, string name)
        {
            string modelName = "model." + name;

            string result = String.Format("ng-class=\"{{'error-state':MC.isEmpty(errors.formErrors['{0}'].Messages)}}\"", modelName);

            return new MvcHtmlString(result);
        }

        public static MvcHtmlString AngTooltipValidationFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            string modelName = "model." + html.IdFor(expression);

            string result = String.Format("data-toggle=\"tooltip\" validation-error-tooltip data-placement=\"top\" data-original-title=\"{{{{MC.joinStringsWithBreak(errors.formErrors['{0}'].Messages)}}}}\" jb-tooltip data-html=\"true\" ", modelName);

            return new MvcHtmlString(result);
        }
        public static MvcHtmlString AngTooltipValidation(this HtmlHelper html, string name)
        {
            string modelName = "model." + name;

            string result = String.Format("data-toggle=\"tooltip\" validation-error-tooltip data-placement=\"top\" data-original-title=\"{{{{MC.joinStringsWithBreak(errors.formErrors['{0}'].Messages)}}}}\" jb-tooltip data-html=\"true\" ", modelName);

            return new MvcHtmlString(result);
        }

        private static string GetFullPropertyName<T, TProperty>(Expression<Func<T, TProperty>> exp)
        {
            MemberExpression memberExp;

            if (!TryFindMemberExpression(exp.Body, out memberExp))
                return String.Empty;

            var memberNames = new Stack<string>();

            do
            {
                memberNames.Push(memberExp.Member.Name);
            }
            while (TryFindMemberExpression(memberExp.Expression, out memberExp));

            return String.Join(".", memberNames.ToArray());
        }

        private static bool TryFindMemberExpression(Expression exp, out MemberExpression memberExp)
        {
            memberExp = exp as MemberExpression;

            if (memberExp != null)
                return true;

            if (IsConversion(exp) && exp is UnaryExpression)
            {
                memberExp = ((UnaryExpression)exp).Operand as MemberExpression;

                if (memberExp != null)
                    return true;
            }

            return false;
        }

        private static bool IsConversion(Expression exp)
        {
            return (exp.NodeType == ExpressionType.Convert || exp.NodeType == ExpressionType.ConvertChecked);
        }
    }
}