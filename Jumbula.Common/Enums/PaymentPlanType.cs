﻿

namespace Jumbula.Common.Enums
{
    public enum PaymentPlanType
    {
        FullPaid = 0,
        Installment = 1,
    }
}
