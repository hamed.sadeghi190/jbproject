﻿using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface IClientPaymentMethodBusiness
    {
        ClientPaymentMethod Get(long? clientId);

        ClientPaymentMethod GetByClub(int clubId);
    }
}
