﻿(function () {
    'use strict';

    angular.module('jbTools').service("familyOrdersService", [function () {

        var _model;

        function setModel(Data) {
            _model = Data;
        }

        function getModel() {
            return _model;
        }

        return {
            set: setModel,
            get: getModel
        }
    }]);
})();