﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('manageStaffController', [
		'$scope', '$http', 'jbToast', '$state', '$stateParams', function ($scope, $http, jbToast, $state, $stateParams) {
		    window.app.globalObjects.pageClass = 'staff-manage-view';
		    window.app.globalObjects.initializeView();

		    $scope.MC.stateParams = $stateParams;


		    if ($stateParams.staffId) {

		        $http.get('Dashboard/Staff/GetStaff', { params: { id: $stateParams.staffId } })
					.success(function (data, status, headers, config) {
					    $scope.Model = data;
					});

		    } else {

		        $http.get('Dashboard/Staff/GetStaff')
					.success(function (data, status, headers, config) {
					    $scope.Model = data;

					    if ($stateParams.staffRole) {

					        $scope.Model.RoleCategory = $stateParams.staffRole;
					    }
					});
		    }

		    $scope.addEdit = function () {


		        if ($scope.Model.PageMode == 'Create') {
		            $scope.add();
		        }
		        else {
		            $scope.edit();
		        }
		    }

		    $scope.add = function () {

		        $http.post('Dashboard/Staff/InviteStaff', { model: $scope.Model })
					.success(function (data, status, headers, config) {

					    if (data.Status) {

					        $scope.errors = null;

					        jbToast.success("Email sent to staff successfully.", "");

					        $state.go('ManageStaffs');
					    } else {
					        $scope.MC.handleErrors($scope, data);
					    }
					});
		    }

		    $scope.edit = function () {

		        $http.post('Dashboard/Staff/EditStaff', { model: $scope.Model })
					.success(function (data, status, headers, config) {

					    if (data.Status) {

					        $scope.errors = null;

					        jbToast.success("Staff editted successfully.", "");

					        $state.go('ManageStaffs');
					    } else {
					        $scope.MC.handleErrors($scope, data);
					    }
					});
		    }
		}
    ]);

})();