﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('viewCatalogController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

		var catalogId = $stateParams.catalogId;

        $http.get('Dashboard/Catalog/GetCreateEdit', { params: { catalogId: catalogId } }).success(function (data, status, headers, config)
            {
                $scope.Model = data;

				 $scope.catalogImage = $scope.Model.Image;

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });

        $scope.init = function () {
            $('[data-role=dropdown]').dropdown();
        }
    }]);
})();

