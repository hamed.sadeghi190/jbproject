﻿using Jumbula.Common.Enums;
using Jumbula.Core.Identity;

namespace Jumbula.Core.Domain
{
    public class ClubBaseInfoModel : ICurrentOrganization
    {
        public int Id { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }

        public Jumbula.Common.Enums.TimeZone? TimeZone { get; set; }

        public CurrencyCodes Currency { get; set; }

        public ClubType ClubType { get; set; }

        public string Site { get; set; }

        public string Logo { get; set; }

        public string PartnerLogo { get; set; }

        public int? PartnerId { get; set; }
        public bool HasPartner
        {
            get
            {
                return PartnerId.HasValue;
            }
        }

        public bool IsSchool
        {
            get
            {

                if (ClubType != null && (ClubType.EnumType == ClubTypesEnum.School) || (ClubType.ParentType != null && ClubType.ParentType.EnumType == ClubTypesEnum.School))
                {
                    return true;
                }
                return false;
            }
        }

        public string PartnerSite { get; set; }

    }
}
