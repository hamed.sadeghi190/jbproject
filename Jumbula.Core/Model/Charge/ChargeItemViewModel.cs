﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Core.Model.Charge
{
    public class ChargeItemViewModel
    {
        [MaxLength(256, ErrorMessage = "{0} cannot be longer than 256 characters.")]
        public string Name { get; set; }

        public decimal Amount { get; set; }

        public decimal? EarlyBirdAmount { get; set; }

        public bool HasEarlyBird { get; set; }

        public CurrencyCodes Currency { get; set; }
        public string EarlyBirdExpiryDate { get; set; }
        
        public string StrAmount => CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency);

        public string StrEarlyBirdAmount => CurrencyHelper.FormatCurrencyWithPenny(EarlyBirdAmount, Currency);

        public int? Capacity { get; set; }
        public string CapacityLeft { get; set; }
    }
}
