﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('editCancelDropInOrderController', ['$scope', '$http', 'jbToast', '$state', '$stateParams', 'jbConfirmation', 'paymentHistoryService', function ($scope, $http, jbToast, $state, $stateParams, jbConfirmation, paymentHistoryService) {
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var counter = function () {
            setTimeout(function () {
                var options = {
                    useEasing: true,
                    useGrouping: true,
                    separator: ',',
                    decimal: '.',
                    prefix: symbol,
                    suffix: ''
                };
                var remainingStart = Number($("#remaining").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    newAmountStart = Number($("#newAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    differenceStart = Number($("#difference").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    paidStart = Number($("#paidAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    orderAmountStart = Number($("#orderAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0;


                var remainingEnd = Number($scope.calculateBalance()) || 0,
                    newAmountEnd = Number($scope.calculateTotalAmount()) || 0,
                    differenceEnd = Number($scope.calculateDifference()) || 0,
                    paidEnd = Number($scope.Model.PaidAmount) || 0,
                    orderAmountEnd = Number($scope.Model.OrderAmount) || 0;

                new countUp("remaining", remainingStart, remainingEnd, 2, .5, options).start();
                new countUp("newAmount", newAmountStart, newAmountEnd, 2, .5, options).start();
                new countUp("difference", differenceStart, differenceEnd, 2, .5, options).start();
                new countUp("paidAmount", paidStart, paidEnd, 2, .5, options).start();
                new countUp("orderAmount", orderAmountStart, orderAmountEnd, 2, .5, options).start();
            }, 200);
        };

        $scope.Number = Number;
        var returnedByBack = $state.returnByBack();
        $scope.Model = {};
        if (returnedByBack) {
            $scope.Model = paymentHistoryService.get();
            counter();
            paymentHistoryService.set(null);
        }
        if (!returnedByBack || $scope.Model == null) {
            if ($stateParams.orderId) {

                $http.get('Dashboard/Order/GetEdit', { params: { orderItemId: $stateParams.orderId } })
                .success(function (data, status, headers, config) {

                    $scope.Model = data;

                    $scope.Model.RefundOrPay = 'true';

                    $scope.Model.PageStep = 'OrderDetail';
                })
            }
        }


        $scope.$watch("Model",
            function (newValue, oldValue) {
                if (newValue != oldValue) {
                    counter();
                }
            }, true
        );

        $scope.edit = function () {

            $scope.submit();

        }

        $scope.submit = function () {

            $http.post('Dashboard/Order/CancelEditDropIn', { model: $scope.Model })
               .success(function (data, status, headers, config) {

                   if (data.Status == true) {

                       $scope.errors = null;

                       var message = '';

                       if ($scope.cancelMode('Program')) {

                           message = 'The program is cancelled successfully.'
                       } else {
                           message = 'The program is edited successfully.';
                       }

                       jbToast.success(message);
                       if ("goToFamilyOrder" in window) {
                           if (window.goToFamilyOrder > 0) {
                               var userId = window.goToFamilyOrder;
                               window.goToFamilyOrder = -1;
                               $state.go('FamilyOrders', { userId: userId });
                           }
                       }
                       else {
                           $state.go('OrderItem', { seasonDomain: $stateParams.seasonDomain, orderItemId: $scope.Model.Id });
                       }
                   }
                   else {
                       $scope.MC.handleErrors($scope, data);
                   }
               })
        }

        $scope.currentProgramSessionId = 0;

        $scope.editSessionItem = function (session) {

            $scope.currentProgramSessionId = session.ProgramSessionId;

            for (var m = 0; m < $scope.Model.OrderSessions.length; m++) {

                if ($scope.Model.OrderSessions[m].Id == session.Id) {

                    $scope.Model.OrderSessions[m].EditMode = true;

                    $http.get('Dashboard/Order/GetCalendarForItem', { params: { orderItemId: $stateParams.orderId } })
                .success(function (data, status, headers, config) {

                    $scope.SchedulerModel = data;

                    $scope.calendarSchedules = [];

                    $scope.calendarDataSource = null;

                    $scope.calendarDataSource = new kendo.data.SchedulerDataSource({
                        batch: true,
                        transport: {
                            read: {
                                url: window.location.origin + '/Dashboard/Order/GetCalendarItems?programId=' + $scope.Model.ProgramId + '&orderItemId=' + $stateParams.orderId + '&programSessionId=' + session.ProgramSessionId + '&oldSessionId=' + session.Id,
                                dataType: "json"
                            },
                            parameterMap: function (options, operation) {
                                if (operation !== "read" && options.models) {
                                    return { models: kendo.stringify(options.models) };
                                }
                            }
                        },

                        schema: {
                            model: {
                                id: "taskId",
                                fields: {
                                    taskId: {
                                        from: "TaskId",
                                        type: "number",
                                    },
                                    title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                                    start: { type: "date", from: "Start" },
                                    end: { type: "date", from: "End" },
                                    startTimezone: { from: "StartTimezone" },
                                    endTimezone: { from: "EndTimezone" },
                                    description: { from: "Description" },
                                    recurrenceId: { from: "RecurrenceId" },
                                    recurrenceRule: { from: "RecurrenceRule" },
                                    recurrenceException: { from: "RecurrenceException" },
                                    ownerId: { type: "number", from: "OwnerId" },
                                    isAllDay: { type: "boolean", from: "IsAllDay" }
                                }
                            }
                        },
                    });

                    $("#sessionScheduler").html('');

                    $("#sessionScheduler").kendoScheduler({
                        date: new Date($scope.SchedulerModel.StartProgramDate),
                        startTime: new Date($scope.SchedulerModel.EndProgramDate),
                        height: 250,
                        width: 680,
                        editable: false,
                        dataBound: function (e) {

                            this.element.find(".CheckClick").bind("click", function (e) {

                                var calendar = $("#sessionScheduler").data("kendoScheduler");

                                var dueDate = $(this).data('dodate');
                                var time = $(this).data('time');
                                var id = $(this).data('id');
                                $scope.onCheckboxChange(dueDate, time, id, session);

                            });
                        },
                        views: [
                          "month",
                          {
                              type: "month", selected: true
                          },
                        ],
                        eventTemplate: "<div class='customAllDayTemplate' style='background-color:#:Color#'><input class='CheckClick' # if(IsOrderSession) {# disabled #} # # if(IsChecked) {# checked #} # type='checkbox' data-id='#:Id#' data-dodate='#:DoDate#' data-time=#=kendo.toString(start,'h:mmtt').replace('AM', 'am').replace('PM', 'pm')#-#=kendo.toString(end,'h:mmtt').replace('AM', 'am').replace('PM', 'pm')#></div>",
                        timezone: "Etc/UTC",
                        dataSource: $scope.calendarDataSource,
                        resources: [
                                {
                                    field: "ownerId",
                                    title: "Owner",
                                    dataSource: $scope.calendarSchedules,
                                }
                        ]
                    });

                });

                } else {
                    $scope.Model.OrderSessions[m].EditMode = false;
                }
            }

        }

        $scope.onCheckboxChange = function (date, time, id, session) {

            for (var j = 0; j < $scope.calendarDataSource._data.length; j++) {

                if ($scope.calendarDataSource._data[j].Id == id) {

                    $scope.calendarDataSource._data[j].IsChecked = true;
                    session.ProgramSessionId = id;
                }
                else {

                    if ($scope.calendarDataSource._data[j].IsOrderSession == true) {
                        continue;
                    }
                    else {
                        $scope.calendarDataSource._data[j].IsChecked = false;
                    }
                }
            }

            date = new Date(date).format('mm/dd/yyyy');
            for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                if ($scope.Model.OrderSessions[i].Id == session.Id) {

                    $scope.Model.OrderSessions[i].UpdatedDate = date;
                    $scope.Model.OrderSessions[i].NewTime = time;
                }
            }
            $("#sessionScheduler").data("kendoScheduler").refresh();
        }

        //Save new session
        $scope.saveSession = function (session) {
            session.EditMode = false;

            if ($scope.Model.ProgramType == 'Subscription') {
                if (session.UpdatedDate != null) {
                    session.NewDate = session.UpdatedDate;
                    session.Time = session.NewTime;
                }

            } else {
                $scope.AddAmount = false;
                $scope.DateHaveEqualTime = false;
                $scope.CurrentSessionHaveEqualTime = false;
                $scope.RemoveAmount = false;
                var sessionDate = new Date(session.Date)
                var newSessionDate = new Date(session.UpdatedDate)

                if (session.UpdatedDate != null) {
  
                    session.NewDate = session.UpdatedDate;
                    session.Time = session.NewTime;

                    if (newSessionDate.getTime() == sessionDate.getTime()) {
                        for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                            var currentDate = new Date($scope.Model.OrderSessions[i].Date)

                            if (sessionDate.getTime() === currentDate.getTime() && (session.Id != $scope.Model.OrderSessions[i].Id)) {
                                $scope.RemoveAmount = true;
                                break;
                            }                       
                        }
                    }

                    for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                        var currentDate = new Date($scope.Model.OrderSessions[i].Date)

                        if (newSessionDate.getTime() === currentDate.getTime() && $scope.Model.OrderSessions[i].Id != session.Id) {

                            $scope.DateHaveEqualTime = true;

                            if ($scope.Model.OrderSessions[i].IsDeleted == true) {

                                $scope.AddAmount = true;
                            }
                        }
                    }

                    for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                        var currentDate = new Date($scope.Model.OrderSessions[i].Date)

                        if (sessionDate.getTime() === currentDate.getTime() && (session.Id != $scope.Model.OrderSessions[i].Id)) {

                            $scope.CurrentSessionHaveEqualTime = true;
                        }

                    }


                    if (!$scope.DateHaveEqualTime && $scope.CurrentSessionHaveEqualTime) {

                        $scope.AddAmount = true;
                    }

                    if ($scope.DateHaveEqualTime && !$scope.CurrentSessionHaveEqualTime)
                    {
                        $scope.RemoveAmount = true;
                    }

                    if ($scope.RemoveAmount) {

                        for (var c = 0; c < $scope.Model.OrderChargeDiscounts.length; c++) {

                            if ($scope.Model.OrderChargeDiscounts[c].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[c].Category == 'EntryFee') {
                                $scope.Model.OrderChargeDiscounts[c].Amount -= session.Amount;
                            }
                        }
                    }

                    if ($scope.AddAmount) {

                        for (var c = 0; c < $scope.Model.OrderChargeDiscounts.length; c++) {

                            if ($scope.Model.OrderChargeDiscounts[c].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[c].Category == 'EntryFee') {
                                $scope.Model.OrderChargeDiscounts[c].Amount += session.Amount;
                            }
                        }
                    }
                }
            }
        }


        $scope.cancelEditSessionItem = function (session) {

            session.EditMode = false;
            session.UpdatedDate = null;
            session.ProgramSessionId = $scope.currentProgramSessionId;
        }

        //cancel session
        $scope.deletedSessionsAmounts = 0;
        $scope.removeSessionItem = function (session) {
            var message = 'Are you sure you want to cancel this session?';
            jbConfirmation.message(message).yes(function () {

                if ($scope.Model.OrderSessions && $scope.Model.ProgramType == 'Subscription') {
                    for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {
                        if ($scope.Model.OrderSessions[i].Id == session.Id) {

                            $scope.Model.OrderSessions[i].IsDeleted = true;
                            $scope.deletedSessionsAmounts += session.Amount;
                        }
                    }

                    for (var c = 0; c < $scope.Model.OrderChargeDiscounts.length; c++) {

                        if ($scope.Model.OrderChargeDiscounts[c].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[c].Category == 'EntryFee') {
                            $scope.Model.OrderChargeDiscounts[c].Amount -= session.Amount;
                        }
                    }
                } else {

                    var sessionDate = new Date(session.Date);
                    $scope.RemoveAmount = false;
                    $scope.DateHaveEqualTime = false;


                    for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {
                        if ($scope.Model.OrderSessions[i].Id == session.Id) {

                            $scope.Model.OrderSessions[i].IsDeleted = true;
                        }
                    }

                    for (var j = 0; j < $scope.Model.OrderSessions.length; j++) {

                        var currentSessionDate = new Date($scope.Model.OrderSessions[j].Date);

                        if ($scope.Model.OrderSessions[j].Id != session.Id) {

                            if (currentSessionDate.getTime() === sessionDate.getTime()) {

                                $scope.DateHaveEqualTime = true;

                                if ($scope.Model.OrderSessions[j].IsDeleted == true) {

                                    $scope.deletedSessionsAmounts += session.Amount;
                                    $scope.RemoveAmount = true;
                                }
                            }
                        }
                    }
                    //if one have one time
                    if (!$scope.DateHaveEqualTime) {

                        $scope.deletedSessionsAmounts += session.Amount;
                        $scope.RemoveAmount = true;
                    }

                    if ($scope.RemoveAmount) {

                        for (var c = 0; c < $scope.Model.OrderChargeDiscounts.length; c++) {

                            if ($scope.Model.OrderChargeDiscounts[c].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[c].Category == 'EntryFee') {
                                $scope.Model.OrderChargeDiscounts[c].Amount -= session.Amount;
                            }
                        }
                    }
                }
            })
            .confirm();
        }

        //undo session
        $scope.activeSession = function (session) {

            $scope.AddAmount = false;
            $scope.DateHaveEqualTime = false;

            var message = 'Are you sure you want to undo this session?';
            jbConfirmation.message(message).yes(function () {

                if ($scope.Model.ProgramType == 'Subscription') {

                    for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                        if ($scope.Model.OrderSessions[i].Id == session.Id) {
                            $scope.Model.OrderSessions[i].IsDeleted = false;
                        }
                    }

                    for (var c = 0; c < $scope.Model.OrderChargeDiscounts.length; c++) {

                        if ($scope.Model.OrderChargeDiscounts[c].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[c].Category == 'EntryFee') {
                            $scope.Model.OrderChargeDiscounts[c].Amount += session.Amount;
                        }
                    }
                } else {

                    var sessionDate = new Date(session.Date);

                    for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                        if ($scope.Model.OrderSessions[i].Id == session.Id) {
                            $scope.Model.OrderSessions[i].IsDeleted = false;
                        }
                    }

                    for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                        var currentDate = new Date($scope.Model.OrderSessions[i].Date);

                        if ($scope.Model.OrderSessions[i].Id != session.Id) {

                            if (sessionDate.getTime() === currentDate.getTime()) {

                                $scope.DateHaveEqualTime = true;
                                if ($scope.Model.OrderSessions[i].IsDeleted == true) {

                                    $scope.AddAmount = true;
                                }
                            }
                        }
                    }

                    if ($scope.AddAmount || !$scope.DateHaveEqualTime) {

                        for (var c = 0; c < $scope.Model.OrderChargeDiscounts.length; c++) {

                            if ($scope.Model.OrderChargeDiscounts[c].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[c].Category == 'EntryFee') {
                                $scope.Model.OrderChargeDiscounts[c].Amount += session.Amount;
                            }
                        }
                    }
                }
            }).confirm();

        }


        $scope.addChargeDiscount = function (chargeDiscountId) {

            if (is.existy($scope.Model.SelectedChargeDiscount) || is.not.empty($scope.Model.SelectedChargeDiscount)) {

                var selectedCDTemp = $scope.getChargeDiscount(chargeDiscountId);

                if (!$scope.hasTuition() && selectedCDTemp.Category != 'EntryFee') {
                    jbToast.error('You should select tuition option first.');
                }
                else {
                    for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                        if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {
                            $scope.Model.OrderChargeDiscounts[i].Checked = true;
                        }
                    }
                }
            }
        }

        $scope.IsCancelEntireOrder = false;
        $scope.IsCancelOrder = false;

        $scope.ActiveOrder = function (chargeDiscountId) {
            $scope.Model.CancellationFee = null;
            $scope.IsCancelOrder = false;
            for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {
                    $scope.Model.OrderChargeDiscounts[i].Checked = true;
                }
            }
            for (var j = 0; j < $scope.Model.OrderSessions.length; j++) {

                if ($scope.Model.OrderSessions[j].IsDeleted == true) {
                    $scope.Model.OrderSessions[j].IsDeleted = false;

                }

            }

            for (var c = 0; c < $scope.Model.OrderChargeDiscounts.length; c++) {
                if ($scope.Model.OrderChargeDiscounts[c].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[c].Category == 'EntryFee') {
                    $scope.Model.OrderChargeDiscounts[c].Amount += $scope.deletedSessionsAmounts;
                }
            }

        }

        $scope.IsCancelOrder

        $scope.removeChargeDiscount = function (chargeDiscountId, removeType) {

            var message = 'Are you sure you want to proceed with this action?';

            if ($scope.getChargeDiscount(chargeDiscountId).Category == 'EntryFee') {
                message = 'Are you sure you want to cancel order?';
            }
            else {
                message = 'Are you sure you want to delete this item?';
            }

            jbConfirmation.message(message).yes(function () {

                if ($scope.Model.OrderChargeDiscounts) {

                    for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {
                        if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {

                            if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee') {

                                for (var l = 0; l < $scope.Model.OrderChargeDiscounts.length; l++) {
                                    $scope.Model.OrderChargeDiscounts[l].Checked = false;
                                }
                            }
                            else {
                                $scope.Model.OrderChargeDiscounts[i].Checked = false;
                            }
                        }
                    }
                    if ($scope.Model.OrderSessions != null && removeType == 'AllOrder') {
                        $scope.IsCancelOrder = true;
                        $scope.Model.EditFee = null;
                        for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                            $scope.Model.OrderSessions[i].IsDeleted = true;

                        }
                    }

                }

            })
            .confirm();
        }

        $scope.CancelEntireOrder = function () {
            var message = 'Are you sure you want to cancel order?';
            jbConfirmation.message(message).yes(function () {

                if ($scope.Model.OrderChargeDiscounts) {
                    $scope.IsCancelEntireOrder = true;

                    for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                        if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee') {

                            for (var l = 0; l < $scope.Model.OrderChargeDiscounts.length; l++) {
                                $scope.Model.OrderChargeDiscounts[l].Checked = false;
                            }
                        }
                        else {
                            $scope.Model.OrderChargeDiscounts[i].Checked = false;
                        }
                    }
                    if ($scope.Model.OrderSessions != null) {
                        $scope.IsCancelOrder = true;
                        for (var i = 0; i < $scope.Model.OrderSessions.length; i++) {

                            $scope.Model.OrderSessions[i].IsDeleted = true;

                        }
                    }
                }
            })
             .confirm();
        }

        $scope.ActiveEntireOrder = function () {
            $scope.IsCancelOrder = false;
            $scope.IsCancelEntireOrder = false;
            for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee') {
                    $scope.Model.OrderChargeDiscounts[i].Checked = true;
                }
            }
            for (var j = 0; j < $scope.Model.OrderSessions.length; j++) {

                if ($scope.Model.OrderSessions[j].IsDeleted == true) {
                    $scope.Model.OrderSessions[j].IsDeleted = false;

                }

            }

            for (var c = 0; c < $scope.Model.OrderChargeDiscounts.length; c++) {
                if ($scope.Model.OrderChargeDiscounts[c].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[c].Category == 'EntryFee') {
                    $scope.Model.OrderChargeDiscounts[c].Amount += $scope.deletedSessionsAmounts;
                }
            }
        }

        $scope.editItem = function (chargeDiscount) {
            chargeDiscount._Amount = chargeDiscount.Amount;

            chargeDiscount.EditMode = true;
        }

        $scope.cancelEditItem = function (chargeDiscount) {
            chargeDiscount.Amount = chargeDiscount._Amount;

            chargeDiscount.EditMode = false;
        }

        $scope.hasTuition = function () {

            if ($scope.Model.OrderChargeDiscounts) {
                for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                    if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        return true;
                    }
                }
            }

            return false;
        }

        $scope.cancelMode = function (type) {
            if (type == 'Donation/Cancel') {

                return false;

            } else if (type == 'Donation/Edit') {

                return true;

            }
            else {
                return !$scope.hasTuition();
            }

        }

        $scope.getEntryFee = function () {

            for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee') {
                    return $scope.Model.OrderChargeDiscounts[i];
                }
            }
        }

        $scope.getChargeDiscount = function (chargeDiscountId) {

            for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {
                    return $scope.Model.OrderChargeDiscounts[i];
                }
            }

            return null;
        }

        $scope.calculateDifference = function () {
            return $scope.calculateTotalAmount() - $scope.Model.OrderAmount;
        }

        $scope.calculateTotalAmount = function (session) {

            var charges = 0;

            var discounts = 0;

            var cancellationFee = 0;
            var editFee = 0;

            var sessionAmount = 0;

            var result = 0;

            if ($scope.Model.EditFee) {
                editFee = parseFloat($scope.Model.EditFee);
            }

            if ($scope.Model.CancellationFee) {
                cancellationFee = parseFloat($scope.Model.CancellationFee);
            }

            if ($scope.Model && $scope.Model.OrderChargeDiscounts) {
                for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                    if ($scope.Model.OrderChargeDiscounts[i].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        charges += parseFloat($scope.Model.OrderChargeDiscounts[i].Amount)
                    }

                    if ($scope.Model.OrderChargeDiscounts[i].SubCategory == 'Discount' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        discounts += parseFloat($scope.Model.OrderChargeDiscounts[i].Amount)
                    }
                }
                result = charges + editFee + cancellationFee  - discounts;
                return result;
            }


            return editFee + cancellationFee;
        }

        $scope.calculateBalance = function () {

            if ($scope.Model) {
                return $scope.calculateTotalAmount() - $scope.Model.PaidAmount;
            }
            else {
                return 0;
            }
        }

        $scope.sortByTuition = function (item) {
            var result = 0;

            switch (item.Category) {
                case 'EntryFee':
                    {
                        result = 0;
                        break;
                    }
                case 'CustomCharge':
                    {
                        result = 1000;
                        break;
                    }
                case 'CustomDiscount':
                    {
                        result = 2000;
                        break;
                    }
                default:
                    {
                        result = 100;
                        break;
                    }
            }

            if (!item.Checked) {
                result += 10000;
            }

            return result;
        }

        $scope.continue = function () {
            if ($scope.Model.PageStep == 'OrderDetail') {
                $scope.Model.PageStep = 'Review';

            }
        }
        $scope.back = function () {

            if ($scope.Model.PageStep == 'Review') {
                $scope.Model.PageStep = 'OrderDetail';
            }
            else {
                history.back();
            }
        }

        $scope.validateDate = function (date) {

            if (new Date(date) == 'Invalid Date') {
                return false;
            }

            return true;
        }

        $scope.saveInstallment = function (installmentItem) {

            var amount = installmentItem.Amount;
            var dueDate = installmentItem.DueDate;

            var today = new Date();
            var dueDate = new Date(dueDate);

            if (dueDate == 0 || dueDate == '' || dueDate == null || dueDate == 'undefined') {
                jbToast.error('You should enter Due date.');
            }
            else if (!$scope.validateDate(dueDate)) {
                jbToast.error('The format of date is invalid.');
            }
            else if (amount == 0 || amount == '' || amount == null || amount == 'undefined') {
                jbToast.error('You should enter amount.');
            }
            else if (isNaN(amount)) {
                jbToast.error('The format of amount is invalid.');
            }
            else {
                installmentItem.EditMode = false;
            }
        }

        $scope.ShowCancellationFeeBox = false;

        $scope.DisplayeCancellationFeeBox = function (displaye) {

            if (displaye) {
                $scope.ShowCancellationFeeBox = true;
            } else {
                $scope.ShowCancellationFeeBox = false;
                $scope.Model.EditFee = null;
                $scope.Model.CancellationFee =null;
            }
        }

        $scope.formatDate = function (date) {

            if (date != null && date != 'undefined') {
                return new Date(date).format('mm/dd/yyyy');
            }
        }
    }])
})();