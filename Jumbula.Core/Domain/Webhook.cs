﻿using System.Collections.Generic;

namespace Jumbula.Core.Domain
{
    public class Webhook
    {
        public string Uid { get; set; }
        public string Status { get; set; }
        public string Event_Id { get; set; }
        public string Reason { get; set; }
        public string Purchase { get; set; }
        public string Event { get; set; }
        public string Email { get; set; }
        public long Timestamp { get; set; }
        public string SmtpId { get; set; }
        public string Type { get; set; }
        public List<string> Category { get; set; }
        public string Id { get; set; }
        public int CampaignId { get; set; }
        public int EmailId { get; set; }
    }
}
