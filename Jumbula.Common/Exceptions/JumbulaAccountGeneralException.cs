﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaAccountGeneralException : JumbulaAccountException
    {
        public JumbulaAccountGeneralException(Exception e)
            : base(e)
        {
        }
    }
}
