﻿using System;
using System.Linq;
using SportsClub.Db;
using SportsClub.Domain;
using System.Web;

namespace SportsClub.Models.Cart
{
    // Use the session to store objects, not Db
    //
    public class CartDb : ICartDb, IDisposable
    {
        private const string CartSessionKey = "CartKey";
        private const string EditCartSessionKey = "EditedCartKey";

        public Cart Get(HttpContextBase context)
        {
            var cart = (Cart)context.Session[CartSessionKey];

            if (cart == null)
            {
                throw new JumbulaCartSessionExpiredException(new Exception());
            }

            return cart;
        }

       public Cart GetEditCart(HttpContextBase context, bool InitialIfNull)
        {
            var cart = (Cart)context.Session[EditCartSessionKey];

            if (cart == null)
            {
                context.Session[CartSessionKey] = new Cart();
                cart = (Cart)context.Session[CartSessionKey];
            }

            return cart;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="item"></param>
        /// <param name="collapase">
        /// If true, all items with the same ItemId will end up in the same item and the quantity accounts for the new item
        /// </param>
       //public void Add(HttpContextBase context, CartOrderViewModel order, bool collapase)
       // {
       //     Cart cart;

       //     try
       //     {
       //         cart = Get(context);

       //         if (cart.Mode == CartMode.CancelOrder)
       //         {
       //             throw new NotSupportedException();
       //         }

       //       if (collapase == false) // simply add the order
       //         {
       //             cart.Orders.Add(order);
       //         }

       //         // If order contains an item with the same ItemId, increment the count
       //         // Otherwise create a new item

       //         else
       //         {
       //             cart.Orders.Add(order);
       //         }
       //     }

       //     catch (JumbulaCartSessionExpiredException) // cart does not exist yet or has expired
       //     {
       //         var newCart = new Cart();

       //         //// ChangeSetID Reset
       //         //newCart.ChnageSetID = Guid.NewGuid().ToString();

       //         newCart.Orders.Add(order);
       //         context.Session[CartSessionKey] = newCart;
       //     }
       // }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="item"></param>
        /// <param name="collapase">
        /// If true, all items with the same ItemId will end up in the same item and the quantity accounts for the new item
        /// </param>
       //public void AddEditItem(HttpContextBase context, CartOrderViewModel order, bool collapase)
       // {
       //     Cart cart;

       //     try
       //     {
       //         cart = (Cart)context.Session[EditCartSessionKey];

       //         // empty every time we add new item
       //         if (cart.Orders.Count > 0)
       //         {
       //             cart.Orders.Clear();
       //         }

       //         if (collapase == false) // simply add the order
       //         {
       //             cart.Orders.Add(order);
       //         }
       //         else
       //         {
       //             cart.Orders.Add(order);
       //         }
       //     }

       //     catch (JumbulaCartSessionExpiredException) // cart does not exist yet or has expired
       //     {
       //         var newCart = new Cart();

       //         //// ChangeSetID Reset
       //         //newCart.ChnageSetID = Guid.NewGuid().ToString();

       //         newCart.Orders.Add(order);
       //         context.Session[EditCartSessionKey] = newCart;
       //     }
       // }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="itemId"></param>
        /// <param name="dateCreatedTicks">
        /// The cart can hold registration items with the same ItemId for more than one person.
        /// This paramenter is used to identify the correct item to delete.
        /// </param>
        public void Remove(HttpContextBase context, string orderID)
        {
            try
            {
                Cart cart = Get(context);

                // ChangeSetID Reset
                //cart.ChnageSetID = Guid.NewGuid().ToString();

                // Does order already exist?
                var order = cart.Orders.SingleOrDefault(p => p.OrderId == orderID);
                if (order != null)
                {
                    cart.Orders.Remove(order);
                    var couponDiscount = order.Discounts.SingleOrDefault(c => c.Category == ChargeDiscountCategories.Coupon);
                    if (couponDiscount != null)
                        cart.Coupon.Amount -= couponDiscount.Amount;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Empty(HttpContextBase context)
        {
            var cart = (Cart)context.Session[CartSessionKey];

           if (cart != null)
            {
                cart.Orders.Clear();
                cart.Mode = CartMode.None;
                cart.Coupon = new CartCoupon();
            }
        }

        public string CurrentDomain(HttpContext context)
        {
            var cart = (Cart)context.Session[CartSessionKey]; 
            string currentDomain = string.Empty;

            if (cart != null && cart.Orders.Any()) 
            {
                // get cueent domain
                currentDomain = cart.Orders.First().Domain;
            }

            return currentDomain;
        }
        public string SibilingForUser(HttpContextBase context, string domain, string subDomain)
        {
            try
            {
                var cart = (Cart)context.Session[CartSessionKey];
                if (cart != null && cart.Orders!=null && cart.Orders.Count>0)
                {
                    var orders = cart.Orders.Where<CartOrderViewModel>(c => c.Domain == domain && c.SubDomain == subDomain);
                    if (orders != null && orders.Count()>0)
                    {
                        
                        switch (orders.FirstOrDefault().SubDomainCategory)
                        {
                            case SubDomainCategories.NewRegular:
                                {
                                    var item = orders.SelectMany(c => c.OrderItems).FirstOrDefault(x => ((OrderItem)x).RegType != RegistrationType.Dropin);
                                    if (item!=null)
                                    {
                                        return string.Format(Constants.F_FullName, item.FirstName, item.LastName);
                                    }
                                    return null;
                                    
                                }
                            default:
                                return null;
                        }
                    }
                }
                
                 return null;
               
              
            }
            catch
            {
                throw;
            }
        }
        public void Dispose()
        {
        }
    }
}

