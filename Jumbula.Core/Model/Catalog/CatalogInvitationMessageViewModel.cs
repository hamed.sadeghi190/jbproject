﻿using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Core.Model.Catalog
{
    public class CatalogInvitationMessageViewModel
    {
        public string ProgramName { get; set; }

        public string Grades { get; set; }

        public string WeekDays { get; set; }

        public string Times { get; set; }

        public string ClubName { get; set; }

        public string ProviderClubName { get; set; }

        public string SeasonTitle { get; set; }

        public string ProviderSeasonTitle { get; set; }

        public string SpecialRequests { get; set; }
    }
}
