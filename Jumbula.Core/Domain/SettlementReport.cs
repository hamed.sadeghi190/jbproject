﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public enum SettlementReportStatus
    {
        Paid = 1,
        Closed = 2,
        Deleted =3
    }

    public class SettlementReport
    {
        public int Id { get; set; }
        
        public SettlementReportStatus Status { get; set; }
        
        [Required]
        [MaxLength(128)]
        public string Domain { get; set; }
        
        public DateTime StartDate { get; set; }
        
        public DateTime EndDate { get; set; }

        //sum of order amount in settlement range
        [Range(0, int.MaxValue)]
        public decimal GrossProceeds { get; set; }

        //sum of jumbula fee in settlement 
        [Range(0, int.MaxValue)]
        public decimal ProcessingFee { get; set; }

        //sum of net in settlement 
        [Range(0, int.MaxValue)]
        public decimal NetProceeds { get; set; }

        public virtual ICollection<SettlementReportOrders> SettlementReportOrders { get; set; }
       
    }

    public class SettlementReportOrders
    {
        public int Id { get; set; }
        [ForeignKey("Order_Id")]
        public virtual Order Order { get; set; }

        [ForeignKey("SettlementReport_Id")]
        public virtual SettlementReport SettlementReport { get; set; }

        public long Order_Id { get; set; }
        public int SettlementReport_Id { get; set; }
    }

}
