﻿(function () {
    'use strict';

    angular.module('dashboardApp').filter('unsafe',['$sce', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    }]);

})();