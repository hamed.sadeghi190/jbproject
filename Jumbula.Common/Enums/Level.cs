﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum Level
    {
        [Description("Select level")]
        None,

        [Description("Beginner")]
        Beginner,

        [Description("Intermediate")]
        Intermediate,

        [Description("Advance")]
        Advance
    }
}
