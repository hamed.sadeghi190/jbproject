﻿using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class RegistrationDetailsViewModel : BaseViewModel<Order,long>
    {
        public string ConfirmationId { get; set; }

        public DateTime OrderDate { get; set; }

        public string Name { get; set; }

        public decimal PaidAmount { get; set; }

        public long OrderId { get; set; }

        public int ItemStatusReason { get; set; }

        public int ItemStatus { get; set; }

    }
}