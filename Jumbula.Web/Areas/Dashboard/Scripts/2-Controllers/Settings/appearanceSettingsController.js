﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('appearanceSettingsController', ['$scope', '$http', '$state', 'jbToast', function ($scope, $http, $state, jbToast) {
        window.app.globalObjects.pageClass = 'general-settings-view';
        window.app.globalObjects.initializeView();


        $http.get('Dashboard/Setting/EditClubAppearanceSettings').success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.save = function () {

            $http.post('Dashboard/Setting/EditClubAppearanceSettings', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status) {

                     $scope.errors = null;

                     jbToast.success('Appearance settings updated successfully.');
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.openColorPicker = function (e) {
            $(e.currentTarget).find("input").data("kendoColorPicker").open();
        }

    }])
})();