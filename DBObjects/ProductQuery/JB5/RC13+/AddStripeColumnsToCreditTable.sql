alter TABLE [dbo].[Clients] add [IsStripeTestMode] [bit] NOT NULL CONSTRAINT [DF__Clients__IsStrip__5031C87B] DEFAULT ((0)) 
alter TABLE [dbo].[Clients] add [StripeCustomerId] [nvarchar](max) NULL
alter TABLE [dbo].[Clients] add [StripeToken] [nvarchar](max) NULL