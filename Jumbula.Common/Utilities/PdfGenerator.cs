﻿
using iTextSharp.text;
using Jumbula.Common.Helper;
using System.IO;
using System.Web.Mvc;

namespace Jumbula.Common.Utilities
{
    public class PdfGenerator
    {
        public PdfGenerator()
        {
            Size = PageSize.A4;
            MarginTop = 0;
            MarginBottom = 0;
            MarginLeft = 10;
            MarginRight = 10;

        }

        public Rectangle Size { get; set; }

        public float MarginTop { get; set; }
        public float MarginBottom { get; set; }

        public float MarginLeft { get; set; }
        public float MarginRight { get; set; }

        public byte[] GenerateFromView(ControllerContext controllerContext, string viewName, object model)
        {
            var html = ViewHelper.RenderViewToString(controllerContext, viewName, model);

             return GenerateFromHtml(html);
        }

        public byte[] GenerateFromHtml(string html, string flyerType = "")
        {
            return CreatePDF(html);
        }

        private byte[] CreatePDF(string html, string flyerType = "")
        {
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();

            htmlToPdf.Margins.Bottom = 5;
            htmlToPdf.Margins.Top = 5;
            htmlToPdf.Margins.Left = 5;
            htmlToPdf.Margins.Right = 5;
            htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
            htmlToPdf.Zoom = (float)(1.273);
            htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;

            var pdfBytes = htmlToPdf.GeneratePdf(html);

            return pdfBytes;
        }

        private Document CreateDocument()
        {
            return new Document(Size, MarginLeft, MarginRight, MarginTop, MarginBottom);
        }

        private byte[] ConverMemoryStreamToByte(MemoryStream memoryStream)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = memoryStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}