﻿/// <reference path="../PaymentPlan/paymentPlanManageController.js" />
(function () {
    'use strict';

    angular.module('dashboardApp').controller('accountSettingsController', ['$scope', '$http', 'jbToast', function ($scope, $http, jbToast) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AccountSettings", order: 2, menu: "SETTINGS" });
        $scope.baseUrl = window.location.origin + "/Dashboard/AccountSetting";

        $http.get($scope.baseUrl + '/GetAccountOverview').success(function (data, status, headers, config) {
            $scope.Model = data;
        });

    }]);
})();