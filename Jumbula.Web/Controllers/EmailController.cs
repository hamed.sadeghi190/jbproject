﻿using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    public class EmailController : JbBaseController
    {
        #region Fields

        private readonly IEmailBusiness _emailBusiness;

        #endregion


        #region Constructor

        public EmailController(IEmailBusiness emailBusiness)
        {
            _emailBusiness = emailBusiness;
        }

        #endregion


        public virtual ActionResult _RegistrationEmailTemplateDesigner()
        {
            return View();
        }

        [HttpGet]
        public virtual JsonNetResult CreateEditTemplate(string seasonDomain)
        {
            var club = Ioc.ClubBusiness.Get(ClubSubDomain);

            EmailTemplateModel model;
            EmailTemplate template;

            int templateId = 0;

            template = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Template;

            if (template != null)
            {

                model = JsonConvert.DeserializeObject<EmailTemplateModel>(template.Template);
                templateId = template.Id;
            }
            else
            {
                model = new EmailTemplateModel { Name = "" };
            }


            if (!string.IsNullOrEmpty(model.HeaderBackground) && !model.HeaderBackground.Contains("http:") && !model.HeaderBackground.Contains("https:"))
            {
                model.HeaderBackground = GetImageUrl(model.HeaderBackground, model.ClubDomain, "emailTemplate");
            }
            if (!string.IsNullOrEmpty(model.FooterBackground) && !model.FooterBackground.Contains("http:") && !model.FooterBackground.Contains("https:"))
            {
                model.FooterBackground = GetImageUrl(model.FooterBackground, model.ClubDomain, "emailTemplate");
            }

            if (model.HasHeaderText && string.IsNullOrEmpty(model.HeaderText))
            {
                model.HeaderText = club.Name;
            }

            model.ClubLogo = Ioc.ClubBusiness.GetClubLogo(club);

            model.Locations.Clear();
            foreach (var location in club.ClubLocations)
            {
                model.Locations.Add(new ClubAddressViewModel
                {
                    Address = location.PostalAddress.Address,
                    AddressPoint = location.PostalAddress.Lat.ToString(CultureInfo.InvariantCulture) + "," + location.PostalAddress.Lng.ToString(CultureInfo.InvariantCulture)
                });
            }

            return JsonNet(new { Data = model, templateId });
        }

        [HttpPost]
        public virtual JsonNetResult SaveUpdateRegistrationEmail(EmailTemplateModel model, string seasonDomain, int? templateId)
        {

            var club = Ioc.ClubBusiness.Get(ClubSubDomain);
            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Id;
            var templateName = $"{seasonDomain}Template";
            OperationStatus result;

            model.ClubDomain = club.Domain;

            if (!string.IsNullOrEmpty(model.HeaderBackground) && !model.HeaderBackground.Contains("http:") && !model.HeaderBackground.Contains("https:"))
            {
                var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.HeaderBackground, club.Domain, "emailTemplate");
                model.HeaderBackground = headerPath;
            }
            if (!string.IsNullOrEmpty(model.FooterBackground) && !model.FooterBackground.Contains("http:") && !model.FooterBackground.Contains("https:"))
            {
                var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.FooterBackground, club.Domain, "emailTemplate");
                model.FooterBackground = headerPath;
            }

            model.Locations.Clear();

            var newTemp = new EmailTemplate()
            {
                Template = JsonConvert.SerializeObject(model),
                TemplateName = templateName,
                Type = TemplateType.Registration,
                Club_Id = club.Id
            };

            if (templateId.HasValue && templateId.Value > 0)
            {
                newTemp.Id = templateId.Value;
                result = Ioc.SeasonBusiness.UpdateEmailTemplate(newTemp, seasonId);
            }
            else
            {
                result = Ioc.SeasonBusiness.SaveEmailTemplate(newTemp, seasonId);
            }

            return JsonNet(new JResult { Status = result.Status, Data = newTemp.Id.ToString() });
        }

        public string GetImageUrl(string fileName, string clubDomain, string folderName)
        {
            string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, $@"{clubDomain}\{folderName}", fileName).AbsoluteUri;

            imageUri = string.Concat(imageUri, "?", "rnd=", Guid.NewGuid());

            if (string.IsNullOrEmpty(fileName))
            {
                imageUri = string.Empty;
            }

            return imageUri;
        }

        public ActionResult QueueInstallmentSuccessEmail()
        {
            var successEmailParameters = new AutoChargeSuccessEmailParameterModel
            {
                InstallmentId = 433876,
                TransactionActivityId = 588672
            };

             _emailBusiness.Send(EmailCategory.AutoChargeSuccess, successEmailParameters);

 
            return Content("OK");
        }

        public ActionResult QueueInstallmentFailEmail()
        {
            var failEmailParameters = new AutoChargeFailEmailParameterModel
            {
                InstallmentId = 433876,
                TransactionActivityId = 588672,
                AttemptNumber = 1
            };

            _emailBusiness.Send(EmailCategory.AutoChargeFail, failEmailParameters);


            return Content("OK");
        }

        public ActionResult QueuePasswordChangeSuccessEmail()
        {
            var resetPasswordSuccessParameter = new ResetPasswordSuccessParameterModel
            {
                ClubId = this.GetCurrentClubId(),
                RequestType = RequestType.Branded,
                UserName = "mostafa@jumbula.com"
            };

            _emailBusiness.Send(EmailCategory.ResetPasswordSuccess, resetPasswordSuccessParameter);

            return Content("OK");
        }

        public ActionResult QueueSummaryEmail()
        {
            var summaryEmailParameters = new AutoChargeSupportSummaryEmailParameterModel()
            {
                SuccessInstallments = new List<long>
                {
                    423869, 423868, 423867, 423866
                },
                FailedInstallments = new List<Tuple<long, long>>
                {
                    new Tuple<long, long>(413814, 578668),
                    new Tuple<long, long>(433876, 588672),
                    new Tuple<long, long>(433858, 588671),
                    new Tuple<long, long>(413804, 578667),
                },
                SystemFailedInstallments = new List<Tuple<long, long>>
                {
                    new Tuple<long, long>(413814, 578668),
                    new Tuple<long, long>(433876, 588672),
                    new Tuple<long, long>(433858, 588671),
                    new Tuple<long, long>(413804, 578667),
                },
                NotAttemptedInstallments = new List<long>()
                {
                    413814, 433876, 433858, 413804
                },
                OtherErrorInstallments = new List<long>
                {
                    413814, 433876, 433858, 413804
                }
            };

        _emailBusiness.Send(EmailCategory.AutoChargeSupportSummary, summaryEmailParameters);


            return Content("OK");
        }

        public ActionResult SendQueuedEmails(EmailCategory? emailType = null)
        {

            _emailBusiness.SendAllQueued(emailType);


            return Content("OK");
        }

        [HttpGet]
        public ActionResult PreviewSuccessEmail()
        {
            var successEmailParameter = new AutoChargeSuccessEmailParameterModel
            {
                ClubId = this.GetCurrentClubId(),
                SuccessMessage = "Email successfully charged.",
            };

            var body = _emailBusiness.GetPreview(EmailCategory.AutoChargeSuccess, successEmailParameter, "Admin");

            return Content(body);
        }
    }
}