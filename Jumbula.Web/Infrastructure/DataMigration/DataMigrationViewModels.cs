﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Domain;

namespace Jumbula.Web.Infrastructure.DataMigration
{
    public class DataMigrationHomeViewModel
    {
        public List<DataMigrationItemViewModel> MigrationItems { get; set; }
    }

    public class DataManipulationHomeViewModel
    {
        public List<DataManipulationItemViewModel> MigrationItems { get; set; }
    }

    public class DataMigrationItemViewModel
    {
        public DataMigrationName Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "Last run")]
        public DateTime? LastRun { get; set; }
        [Display(Name = "Number of runs")]
        public int NumberOfRuns { get; set; }
    }

    public class DataManipulationItemViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        [Display(Name = "Last run")]
        public DateTime? LastRun { get; set; }
        [Display(Name = "Number of runs")]
        public int NumberOfRuns { get; set; }
    }

    public class DataMigrationResultItemViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Time of run")]
        public DateTime RunDateTime { get; set; }

    }

    public class DataMigrationRunViewModel
    {
        public int Id { get; set; }
        public DataMigrationName Name { get; set; }
        public RunPageMode PageMode { get; set; }
        public string Description { get; set; }

        public List<DataMigrationResultItemViewModel> ResultItems { get; set; }
        public DataMigrationUploadViewModel Upload { get; set; }

        public string Message { get; set; }
    }

    public class DataManipulationRunViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Title { get; set; }
        public RunPageMode PageMode { get; set; }
        public string Description { get; set; }

        public bool HasSource { get; set; }

        public List<DataMigrationResultItemViewModel> ResultItems { get; set; }
        public DataMigrationUploadViewModel Upload { get; set; }

        public string Message { get; set; }

        int? _startPoint;

        public int? StartPoint
        {
            get
            {
                return _startPoint.HasValue ? _startPoint.Value : 1;
            }
            set
            {
                _startPoint = value;
            }
        }

        public int? EndPoint { get; set; }
    }

    public class DataManipulationSettingViewModel
    {
        public string Title { get; set; }
        public int? StartPoint { get; set; }

        public int? EndPoint { get; set; }

        public string ConnectionString { get; set; }

        public int NumbersInCycle { get; set; }

        public bool HasSourceFile { get; set; }

        public string InclusiveIds { get; set; }
        public string ExceptIds { get; set; }

    }

    public class DataManipulationReportViewModel
    {
        public DataManipulationReportFilterMode FilterMode { get; set; }
    }

    public enum DataManipulationReportFilterMode
    {
        AllLogs,
        Errors,
        Warnings
    }

    public enum RunPageMode : byte
    {
        Uplaod,
        Run,
        Success,
        Fail
    }

    public class DataMigrationUploadViewModel
    {

    }

}