﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.UserService;
using Serilog;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{


    public class ScheduleController : JbBaseController
    {

        [AllowAnonymous]
        public void AutoChargeClientBilling(int month = 0, int day = 0)
        {
            var today = DateTime.UtcNow;
            if (month > 0)
            {
                today = new DateTime(today.Year, month, day > 0 ? day : today.Day);
            }
            var periodDate = today.ToString(Constants.DateTime_Comma) + " - " + today.AddMonths(1).ToString(Constants.DateTime_Comma);
            var clientDb = Ioc.ClientBusiness;
            var clients = Ioc.ClientBusiness.GetList().Where(c => c.IsDeleted == false && c.BillingPeriodDay == today.Day && c.LastMonthBillingPaid < today.Month).ToList();
            var debitDesc = "Jumbula Online for Business " + periodDate;
            var stripeService = new JbStripeService(string.Empty, false);
            var accountService = Ioc.AccountingBusiness;
            var clubBusiness = Ioc.ClubBusiness;

            foreach (var item in clients)
            {
                var billingId = Ioc.ClientBusiness.GenerateBillingId();
                if (item.BillingHistories == null)
                {
                    item.BillingHistories = new List<ClientBillingHistory>();
                }
                if (!item.BillingHistories.Any(c => c.BillingDate.Month == today.Month && c.BillingDate.Year == today.Year))
                {
                    item.BillingHistories.Add(new ClientBillingHistory()
                    {
                        BillingDate = today,
                        BillingId = billingId,
                        ClientId = item.Id,
                        Debit = item.PricePlan.MonthlyCharges,
                        Description = (item.BillingNotes != null && item.BillingNotes.Any()) ? string.Join(", ", item.BillingNotes.Select(c => c.Description).ToList()) : debitDesc,
                        Credit = 0

                    });
                    clientDb.Update(item);
                }
                else
                {
                    billingId = item.BillingHistories.First(c => c.BillingDate.Month == today.Month && c.BillingDate.Year == today.Year).BillingId;
                }

                if (accountService.GetClientBalance(item) > 0)
                {
                    var club = clubBusiness.GetClubByClientId(item.Id);
                    var billingStatus = stripeService.ChargeClient(item, club.Name, today, billingId);

                    var text = "monthly charge";
                    EmailService.Get(this.ControllerContext).SendClientBillingNotification(billingId, item.Id, club, billingStatus, periodDate, Constants.F_ClientMonthlyBilling, text, Constants.F_ClientMonthlyBillingEmailDescription);

                }
            }
        }

        [AllowAnonymous]
        public void InstallmentReminder(DateTime? installmentDate = null)
        {
            if (!installmentDate.HasValue)
            {
                installmentDate = DateTime.UtcNow.AddDays(7);
            }
            var emailService = EmailService.Get(this.ControllerContext);
            var orderInstallments = Ioc.InstallmentBusiness.GetAllInstallmentsWithNoReminder(installmentDate.Value.Date).ToList();
            if (orderInstallments != null && orderInstallments.Count() > 0)
            {

                var playerInstallments = orderInstallments.GroupBy(c => new { c.OrderItem.PlayerId, c.OrderItem.Order.ClubId }).ToList();
                foreach (var playerItems in playerInstallments)
                {
                    var itemGroupByDate = playerItems.GroupBy(c => c.InstallmentDate.ToString(Constants.DefaultDateFormat));
                    foreach (var dateItem in itemGroupByDate)
                    {
                        var token = Guid.NewGuid().ToString("N");
                        emailService.SendInstallmentReminder(dateItem.ToList(), token);
                        foreach (var item in dateItem.ToList())
                        {
                            item.Token = token;
                            item.NoticeSent = true;
                            Ioc.InstallmentBusiness.Update(item);
                        }
                    }


                }
            }
        }

        [AllowAnonymous]
        public void InstallmentReminderByToken(string token)
        {

            var orderInstallments = Ioc.InstallmentBusiness.GetListByToken(token).ToList();
            if (orderInstallments != null && orderInstallments.Count > 0)
            {
                EmailService.Get(this.ControllerContext).SendInstallmentReminderByToken(orderInstallments, token);
            }
        }

        [AllowAnonymous]
        public virtual JsonResult InstallmentReminderById(long installmentId)
        {

            var orderInstallment = Ioc.InstallmentBusiness.Get(installmentId);
            if (orderInstallment != null)
            {
                var token = Guid.NewGuid().ToString("N");
                var list = new List<OrderInstallment>();
                list.Add(orderInstallment);
                orderInstallment.Token = token;
                orderInstallment.NoticeSent = true;
                Ioc.InstallmentBusiness.Update(orderInstallment);
                EmailService.Get(this.ControllerContext).SendInstallmentReminderManually(list, token);

                return Json(new JResult { Status = true, Message = "", RecordsAffected = 1 });
            }

            return Json(new JResult { Status = false, Message = "Data is invalid", RecordsAffected = 0 });
        }

        [AllowAnonymous]
        public void InstallmentAutoChargeReminder(DateTime? installmentDate = null)
        {
            if (!installmentDate.HasValue)
            {
                installmentDate = DateTime.UtcNow.AddDays(7);
            }
            var emailService = EmailService.Get(ControllerContext);
            var orderInstallments = Ioc.InstallmentBusiness.GetAllAutoChargeInstallmentsWithNoReminder(installmentDate.Value.Date).ToList();

            if (!orderInstallments.Any()) return;

            var playerInstallments = orderInstallments.GroupBy(c => new { c.OrderItem.PlayerId, c.OrderItem.Order.ClubId }).ToList();
            foreach (var playerItems in playerInstallments)
            {
                var clubId = playerItems.Key.ClubId;
                var itemGroupByDate = playerItems.GroupBy(c => c.InstallmentDate.ToString(Constants.DefaultDateFormat));
                foreach (var dateItem in itemGroupByDate)
                {
                    if (dateItem.Sum(c => c.Balance) <= 0) continue;
                    emailService.SendInstallmentAutoChargeReminder(dateItem.ToList(), clubId);
                    foreach (var item in dateItem.ToList())
                    {
                        try
                        {
                            item.NoticeSent = true;
                            Ioc.InstallmentBusiness.Update(item);
                        }
                        catch (Exception ex)
                        {
                            var itemString = item != null ? JsonHelper.JsonSerializer(item) : " dateItem is null";
                            LogHelper.LogException(ex, itemString);
                        }
                    }
                }
            }
        }

        [AllowAnonymous]
        public virtual ActionResult UpdateInstallmentsDate()
        {
            var updatedtInstallments = new List<OrderInstallment>();
            var installmentDate = DateTime.Parse("2018-12-31 00:00:00.000");

            var partner = Ioc.ClubBusiness.Get("rightatschool");
            var partnerClubs = Ioc.ClubBusiness.GetList().Where(c => c.PartnerId == partner.Id);

            var clubsProgram = Ioc.ProgramBusiness.GetList().Where(p => p.TypeCategory == ProgramTypeCategory.Camp && partnerClubs.Select(c => c.Id).Contains(p.ClubId));

            var programschedules = clubsProgram.SelectMany(p => p.ProgramSchedules);

            var orderitems = Ioc.OrderItemBusiness.GetList().Where(i => (i.ItemStatus == OrderItemStatusCategories.completed || i.ItemStatus == OrderItemStatusCategories.changed) && programschedules.Select(s => s.Id).Contains(i.ProgramScheduleId.Value));

            var installments = orderitems.SelectMany(i => i.Installments).Where(i => i.Amount != 0 && i.PaidAmount != i.Amount && i.InstallmentDate == installmentDate);

            updatedtInstallments.AddRange(installments);

            var result = Ioc.InstallmentBusiness.UpdateInstallments(updatedtInstallments);

            if (result.Status)
            {
                EmailService.Get(ControllerContext).SendUpdateRasInstallment(result.Message);
                return JsonNet("Successful");
            }

            return JsonNet("Failed");
        }

        [AllowAnonymous]
        public ActionResult StartAutoCharge(DateTime? dueDate = null)
        {
            dueDate = (dueDate ?? DateTime.UtcNow);
            Ioc.AutoChargeBusiness.Run(dueDate.Value);
            return Content("Scheduler finished Or Duplicate run rejected.");
        }

        [AllowAnonymous]
        public async Task<ActionResult> AutoChargeBridge(DateTime? dueDate = null)
        {
            dueDate = (dueDate ?? DateTime.UtcNow);

            var baseUrl = UrlHelpers.GetBaseUrl(this.GetCurrentClubDomain());
            var relativePath = Url.Action("StartAutoCharge", new { dueDate });

            var url = baseUrl + relativePath;

            try
            {
                var timeout = new TimeSpan(0, 0, 20);
                await RequestHelper.GetRequest(url, timeout);
            }
            catch (Exception e)
            {
                Log.Error(e, "AutoChargeBridge");
            }
            return Content("ok");
        }

    }
}
