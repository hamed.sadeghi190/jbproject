

SET IDENTITY_INSERT [dbo].[JbUsers]  ON
GO

-- Insert Users from userprofile table
DECLARE @userCount INT 

select @userCount = COUNT(UserId) from UserProfile


WHILE 1 = 1
BEGIN

   INSERT INTO [dbo].[JbUsers]
           (
            [Id],
		    [Email],
            [EmailConfirmed]
           ,[PasswordHash]
           ,[SecurityStamp]
           ,[PhoneNumber]
           ,[PhoneNumberConfirmed]
           ,[TwoFactorEnabled]
           ,[LockoutEndDateUtc]
           ,[LockoutEnabled]
           ,[AccessFailedCount]
           ,[UserName]
		   ,[TagName]
		   ,[Credit])
    SELECT TOP(@userCount) 
        s.UserId
        ,s.UserName
        ,1
        ,''
		,''
		,Null
		,0
		,0
		,Null
		,0
		,0
		, s.UserName
		, s.TagName
		, s.Credit

    FROM [dbo].[UserProfile] s
    WHERE NOT EXISTS ( 
        SELECT 1
        FROM [dbo].[JbUsers]
        WHERE id = s.UserId
    )

    IF @@ROWCOUNT < @userCount BREAK
    
END

SET IDENTITY_INSERT [dbo].[JbUsers]  OFF
GO

-------------------

Update [dbo].[JbUsers] set EmailConfirmed = (select IsConfirmed from [dbo].[webpages_Membership] where JbUsers.Id = [dbo].[webpages_Membership].UserId)
Update [dbo].[JbUsers] set PasswordHash = (select [Password] from [dbo].[webpages_Membership] where JbUsers.Id = [dbo].[webpages_Membership].UserId)
