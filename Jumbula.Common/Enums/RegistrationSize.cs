﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum RegistrationSize
    {
        [Description("0-100")]
        Size1,
        [Description("101-200")]
        Size2,
        [Description("201-300")]
        Size3,
        [Description("301-400")]
        Size4,
        [Description("401-500")]
        Size5,
        [Description("501 or higher")]
        Size6
    }

}
