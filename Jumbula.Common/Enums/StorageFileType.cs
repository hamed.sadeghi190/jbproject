﻿

namespace Jumbula.Common.Enums
{
    public enum StorageFileType : byte
    {
        ESignature,
        Flyer
    }
}
