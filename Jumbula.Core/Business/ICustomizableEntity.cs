﻿using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface ICustomizableEntity
    {
        List<JbFieldSetting> GetDefaultSetting();
    }
}
