﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
    class SchoolTest : BaseTest<SchoolElements>
    {
        public SchoolTest(IWebDriver webDriver)
            :base(webDriver)
        { 
        }
        
        public void View()
        {
            Assert.AreEqual("Schools", Elements.PageTitle.Text);

            #region AssertRow1
            Assert.AreEqual("Ashland Elementary PTO", Elements.Row1Name.Text);
            Assert.AreEqual("jumbula-ashland", Elements.Row1Domain.Text);
            Assert.AreEqual("Ashland School", Elements.Row1Admin.Text);
            Assert.AreEqual("123-456-7890", Elements.Row1Phone.Text);
            Assert.AreEqual("Ashlandschool@jumbula.com", Elements.Row1Email.Text);
            Assert.AreEqual("11/30/2017", Elements.Row1ExpirationDate.Text);
            Assert.AreEqual("Suffolk County", Elements.Row1County.Text);
            #endregion AssertRow1

            #region AssertRow2
            Assert.AreEqual("Ashlawn Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("jumbula-ashlawn", Elements.Row2Domain.Text);
            Assert.AreEqual("Ashlawn School", Elements.Row2Admin.Text);
            Assert.AreEqual("825-682-5486", Elements.Row2Phone.Text);
            Assert.AreEqual("Ashlawnschool@jumbula.com", Elements.Row2Email.Text);
            Assert.AreEqual("12/30/2017", Elements.Row2ExpirationDate.Text);
            Assert.AreEqual("Middlesex County", Elements.Row2County.Text);
            #endregion AssertRow2

            #region AssertRow3
            Assert.AreEqual("Claremont Immersion School PTA", Elements.Row3Name.Text);
            Assert.AreEqual("jumbula-claremont", Elements.Row3Domain.Text);
            Assert.AreEqual("Claremont School", Elements.Row3Admin.Text);
            Assert.AreEqual("344-536-4561", Elements.Row3Phone.Text);
            Assert.AreEqual("Claremontschool@jumbula.com", Elements.Row3Email.Text);
            Assert.AreEqual("01/30/2018", Elements.Row3ExpirationDate.Text);
            Assert.AreEqual("Rosario", Elements.Row3County.Text);
            #endregion AssertRow3

            #region AssertRow4
            Assert.AreEqual("Discovery Elementary PTA", Elements.Row4Name.Text);
            Assert.AreEqual("jumbula-discovery", Elements.Row4Domain.Text);
            Assert.AreEqual("Discovery School", Elements.Row4Admin.Text);
            Assert.AreEqual("973-285-6184", Elements.Row4Phone.Text);
            Assert.AreEqual("Discoveryschool@jumbula.com", Elements.Row4Email.Text);
            Assert.AreEqual("", Elements.Row4ExpirationDate.Text);
            Assert.AreEqual("Rosario", Elements.Row4County.Text);
            #endregion AssertRow4

            #region AssertRow5
            Assert.AreEqual("Hayfield Elementary PTA", Elements.Row5Name.Text);
            Assert.AreEqual("jumbula-hayfield", Elements.Row5Domain.Text);
            Assert.AreEqual("Hayfield school", Elements.Row5Admin.Text);
            Assert.AreEqual("762-185-6285", Elements.Row5Phone.Text);
            Assert.AreEqual("Hayfieldschool@jumbula.com", Elements.Row5Email.Text);
            Assert.AreEqual("02/28/2018", Elements.Row5ExpirationDate.Text);
            Assert.AreEqual("Suffolk County", Elements.Row5County.Text);
            #endregion AssertRow5

            #region AssertRow6
            Assert.AreEqual("John F. Pattie, Sr. Elementary PTCO", Elements.Row6Name.Text);
            Assert.AreEqual("jumbula-pattie", Elements.Row6Domain.Text);
            Assert.AreEqual("John School", Elements.Row6Admin.Text);
            Assert.AreEqual("735804261", Elements.Row6Phone.Text);
            Assert.AreEqual("Johnschool@jumbula.com", Elements.Row6Email.Text);
            Assert.AreEqual("03/30/2018", Elements.Row6ExpirationDate.Text);
            Assert.AreEqual("Rosario", Elements.Row6County.Text);
            #endregion AssertRow6

            Assert.IsFalse(Elements.Row7Exist);

      
        }
        public void Filter()
        {
            // Active and Inactive
            Elements.AllOption.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Ashland Elementary PTO", Elements.Row1Name.Text);
            Assert.AreEqual("Ashlawn Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("Barrett Elementary PTA", Elements.Row3Name.Text);
            Assert.AreEqual("Claremont Immersion School PTA", Elements.Row4Name.Text);
            Assert.AreEqual("Discovery Elementary PTA", Elements.Row5Name.Text);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.Row6Name.Text);
            Assert.AreEqual("Jamestown Elementary PTA", Elements.Row7Name.Text);
            Assert.AreEqual("John F. Pattie, Sr. Elementary PTCO", Elements.Row8Name.Text);
            Assert.AreEqual("McKinley Elementary PTA", Elements.Row9Name.Text);
            Assert.AreEqual("Oakton Elementary PTA", Elements.Row10Name.Text);

            // Inactive
            Elements.InactiveOption.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Barrett Elementary PTA", Elements.Row1Name.Text);
            Assert.AreEqual("Jamestown Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("McKinley Elementary PTA", Elements.Row3Name.Text);
            Assert.AreEqual("Oakton Elementary PTA", Elements.Row4Name.Text);
            Assert.IsFalse(Elements.Row5Exist);

            //Filter by Name and Active and Inactive
            Elements.SearchTerm.SendKeys("Elementary PTA");
            Elements.AllOption.Click();
            ExtraAPI.Wait(10000);

            Assert.AreEqual("Ashlawn Elementary PTA", Elements.Row1Name.Text);
            Assert.AreEqual("Barrett Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("Discovery Elementary PTA", Elements.Row3Name.Text);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.Row4Name.Text);
            Assert.AreEqual("Jamestown Elementary PTA", Elements.Row5Name.Text);
            Assert.AreEqual("McKinley Elementary PTA", Elements.Row6Name.Text);
            Assert.AreEqual("Oakton Elementary PTA", Elements.Row7Name.Text);
            Assert.IsFalse(Elements.Row8Exist);

            //Filter by Name and Active
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Elements.SearchTerm.SendKeys("Elementary PTA");
            Elements.SearchNameButton.Click();
            ExtraAPI.Wait(10000);

            Assert.AreEqual("Ashlawn Elementary PTA", Elements.Row1Name.Text);
            Assert.AreEqual("Discovery Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.Row3Name.Text);
            Assert.IsFalse(Elements.Row4Exist);

            //Filter by Name and Inctive
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Elements.SearchTerm.SendKeys("Elementary PTA");
            Elements.InactiveOption.Click();
            ExtraAPI.Wait(10000);

            Assert.AreEqual("Barrett Elementary PTA", Elements.Row1Name.Text);
            Assert.AreEqual("Jamestown Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("McKinley Elementary PTA", Elements.Row3Name.Text);
            Assert.AreEqual("Oakton Elementary PTA", Elements.Row4Name.Text);
            Assert.IsFalse(Elements.Row5Exist);

            //Filter by Domain and Active and Inactive
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Elements.DomainOption.Click();
            ExtraAPI.Wait(2000);
            Elements.SearchTerm.SendKeys("jumbula-ashlawn");
            Elements.AllOption.Click();
            ExtraAPI.Wait(10000);

            Assert.AreEqual("Ashlawn Elementary PTA", Elements.Row1Name.Text);
            Assert.IsFalse(Elements.Row2Exist);

            //Filter by Domain and Inactive
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Elements.DomainOption.Click();
            ExtraAPI.Wait(2000);
            Elements.SearchTerm.SendKeys("jumbula-ashlawn");
            Elements.InactiveOption.Click();
            ExtraAPI.Wait(10000);

            Assert.IsFalse(Elements.Row1Exist);

            // Clear Filter
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Ashland Elementary PTO", Elements.Row1Name.Text);
            Assert.AreEqual("Ashlawn Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("Claremont Immersion School PTA", Elements.Row3Name.Text);
            Assert.AreEqual("Discovery Elementary PTA", Elements.Row4Name.Text);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.Row5Name.Text);
            Assert.AreEqual("John F. Pattie, Sr. Elementary PTCO", Elements.Row6Name.Text);
            Assert.IsFalse(Elements.Row7Exist);
            Assert.AreEqual("string:Active", Elements.ActiveTypeFilter.GetAttribute("value"));
            Assert.AreEqual("string:Name", Elements.FilterType.GetAttribute("value"));

            //Filter by Domain and Active
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Elements.DomainOption.Click();
            ExtraAPI.Wait(2000);
            Elements.SearchTerm.SendKeys("jumbula-ashlawn");
            Elements.SearchNameButton.Click();
            ExtraAPI.Wait(10000);

            Assert.AreEqual("Ashlawn Elementary PTA", Elements.Row1Name.Text);
            Assert.IsFalse(Elements.Row2Exist);

            // Filter by Expiration date -> start date only
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Elements.ExpirationDateOption.Click();
            ExtraAPI.Wait(2000);
            Elements.StartDateFilter.SendKeys("30 Jan 2018");
            ExtraAPI.Wait(2000);
            Elements.SearchDateButton.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Claremont Immersion School PTA", Elements.Row1Name.Text);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("John F. Pattie, Sr. Elementary PTCO", Elements.Row3Name.Text);
            Assert.IsFalse(Elements.Row4Exist);
            
            // Filter by Expiration date -> end date only
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Elements.ExpirationDateOption.Click();
            ExtraAPI.Wait(2000);
            Elements.EndDateFilter.SendKeys("30 Jan 2018");
            ExtraAPI.Wait(2000);
            Elements.SearchDateButton.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Ashland Elementary PTO", Elements.Row1Name.Text);
            Assert.AreEqual("Ashlawn Elementary PTA", Elements.Row2Name.Text);
            Assert.AreEqual("Claremont Immersion School PTA", Elements.Row3Name.Text);
            Assert.IsFalse(Elements.Row4Exist);

            // Filter by Expiration date -> start and end date 
            Elements.ClearFilter.Click();
            ExtraAPI.Wait(10000);
            Elements.ExpirationDateOption.Click();
            ExtraAPI.Wait(2000);
            Elements.StartDateFilter.SendKeys("30 Jan 2018");
            ExtraAPI.Wait(2000);
            Elements.EndDateFilter.SendKeys("28 Feb 2018");
            ExtraAPI.Wait(2000);
            Elements.SearchDateButton.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Claremont Immersion School PTA", Elements.Row1Name.Text);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.Row2Name.Text);
            Assert.IsFalse(Elements.Row3Exist);
        }
    }
}
