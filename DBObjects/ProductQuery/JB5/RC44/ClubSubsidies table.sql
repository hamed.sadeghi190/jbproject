
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ClubSubsidies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClubId] [int] NOT NULL,
	[SubsidyId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ClubSubsidies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClubSubsidies]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubSubsidies_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClubSubsidies] CHECK CONSTRAINT [FK_dbo.ClubSubsidies_dbo.Clubs_ClubId]
GO

ALTER TABLE [dbo].[ClubSubsidies]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubSubsidies_dbo.Subsidies_SubsidyId] FOREIGN KEY([SubsidyId])
REFERENCES [dbo].[Subsidies] ([Id])
GO

ALTER TABLE [dbo].[ClubSubsidies] CHECK CONSTRAINT [FK_dbo.ClubSubsidies_dbo.Subsidies_SubsidyId]
GO


