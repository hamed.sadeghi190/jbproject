﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;
using Newtonsoft.Json;

namespace Jumbula.Core.Domain
{
    public class Message : BaseEntity
    {
        public Message()
        {
            UserMessages = new HashSet<UserMessage>();
        }

        public string Text { get; set; }

        public DateTime CreatedDate { get; set; }

        public int SenderId { get; set; }

        [ForeignKey("SenderId")]
        public virtual JbUser Sender { get; set; }

        public int ClubId { get; set; }

        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }

        public int? MeassageHeaderId { get; set; }

        [ForeignKey("MeassageHeaderId")]
        public virtual MessageHeader MessageHeader { get; set; }

        public virtual ICollection<UserMessage> UserMessages { get; set; }
    }


    public class MessageHeader : BaseEntity
    {
        public string ChatId { get; set; }

        public string Subject { get; set; }

        public MessageType Type { get; set; }

        public  long? ProgramId { get; set; }

        [ForeignKey("ProgramId")]
        public virtual Program  Program { get; set; }


        [Column("Attribute")]
        public string AttributeSerialized { get; set; }

        MessageAttributeBase _attribute;

        [NotMapped]
        public MessageAttributeBase Attribute
        {
            get
            {
                switch (this.Type)
                {
                    case MessageType.Ordinary:
                        {
                            throw new NotImplementedException();
                        }
                    case MessageType.ScheduleDraftSession:
                        {
                            return string.IsNullOrEmpty(AttributeSerialized) ? new MessageScheduleDraftSessionAttribute() : JsonConvert.DeserializeObject<MessageScheduleDraftSessionAttribute>(AttributeSerialized);
                        }
                    case MessageType.ESignature:
                        {
                            return string.IsNullOrEmpty(AttributeSerialized) ? new MessageEsignatureAttribute() : JsonConvert.DeserializeObject<MessageEsignatureAttribute>(AttributeSerialized);
                        }
                    default:
                        {
                            throw new NotSupportedException();
                        }
                }
            }
            set
            {
                _attribute = value;
            }
        }


        public virtual ICollection<Message> Messages { get; set; }
    }

    public abstract class MessageAttributeBase
    {

    }

    public class MessageScheduleDraftSessionAttribute : MessageAttributeBase
    {
        public ScheduleDraftSessionStatus Status { get; set; }
        public long ProgramId { get; set; }
    }

    public class MessageEsignatureAttribute : MessageAttributeBase
    {
        public SignatureStatus Status { get; set; }
    }

    public enum ScheduleDraftSessionStatus : byte
    {
        Draft,
        Modified,
        Declined,
        Accepted
    }

    public class UserMessage : BaseEntity
    {
        public int ReceiverId { get; set; }
        [ForeignKey("ReceiverId")]
        public virtual JbUser Receiver { get; set; }

        public int MessageId { get; set; }

        [ForeignKey("MessageId")]
        public virtual Message Message { get; set; }

        public bool IsRead { get; set; }

        public int ClubId { get; set; }

        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }
    }

  

    public enum MessageType : byte
    {
        Ordinary,
        ScheduleDraftSession,
        ESignature
    }

  
}
