﻿
using System;

namespace SportsClub.Areas.Dashboard.Models
{
    public class InvoiceMessageViewModel
    {
      
        public string Username { get; set; }
        public string SeasonDomain { get; set; }
        public long ProgramId { get; set; }
        public long OrderItemId { get; set; }
        public int UserId { get; set; }
        public string ConfirmationId { get; set; }
        public string FullName { get; set; }
        public string Message { get; set; }

        public bool IsSuceed { get; set; }

        public long InvoiceId { get; set; }
        public int SelectDueDate { get; set; }
        public DateTime DueDate { get; set; }

        public string StrDueDate
        {
            get
            {
                return DueDate.ToString("MMM dd, yyyy");
            }
        }

    }

}