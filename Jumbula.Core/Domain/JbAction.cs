﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;

namespace Jumbula.Core.Domain
{
    public class ActionRole : BaseEntity
    {
        public JbAction Action { get; set; }

        public List<RoleCategory> Roles { get; set; }
    }

    public enum JbAction
    {
        AdminDashboard_View,
        Overview_View,
        Home_View,
        //Season
        Season_View,
        Season_Add,
        Season_Edit,
        Season_Archive,
        Season_Delete,
        Season_AssignRoom,
        Season_AssignInstructor,
        Season_SwitchMode,
        Season_SchoolAdvancedSettings_View,
        ProgramsAndOrders_View,
        Report_View,
        Setup_View,

        //Program
        Program_View,
        Program_Draft_View,
        Program_List_View,
        Program_Add,
        Program_Edit,
        Program_Delete,
        Program_Freeze,
        Program_Copy,
        Program_RespondInvitation,
        Program_TotalRevenue_View,
        Program_AssignInstructors,
        Program_AssignRoom,

        Orders_ConfirmationId_View,
        Orders_Balance_View,
        Orders_EmailParticipant,

        Coupon_View,
        Coupon_Add,
        Coupon_Upload,

        Discount_View,
        Discount_Add,

        Charges_View,
        Charges_Add,
        ApplicationFee_Add,

        PaymentPlan_View,
        PaymentPlan_Add,

        ReigstrationEmailTemplate_View,

        Forms_View,
        FollowUpForms_View,

        GaTrackingId_View,

        Campaign_View,
        CampaignRecipients_View,
        People_View,
        CampaignClassConfirmation_View,

        //Contact
        [Description("Contact view")]
        Contact_View,
        [JbZone(JbZone.AdminDashboard)]
        [Description("Contact add")]
        Contact_Add,
        [JbZone(JbZone.AdminDashboard)]
        [Description("Contact edit")]
        Contact_Edit,
        [JbZone(JbZone.AdminDashboard)]
        [Description("Contact delete")]
        Contact_Delete,
        [Description("Contact setting view")]
        ContactSettings_View,
        [JbZone(JbZone.AdminDashboard)]
        [Description("Contact setting save")]
        ContactSettings_Save,

        //Staff
        Staff_View,
        Staff_Add,
        Staff_Add_From_Partner,
        Staff_Edit,
        Staff_Delete,
        Staff_Freeze,
        Staff_ResendInvitation,
        Staff_Active,

        //Invoice
        invoice_View,
        Invoice_Add,
        //Catalog
        Catalogs_View,
        Catalogs_ViewItem,
        CatalogItem_Add,
        CatalogItem_Archive,
        CatalogItem_Hide,
        CatalogItem_Edit,
        CatalogItem_Delete,
        CatalogItem_View,
        CatalogItem_Invite,
        Catalog_ViewAll,
        Catalog_Search,
        Flyer_View,
        //ESignature
        ESignature_View,
        ESignature_Add,
        ESignature_Setting,
        //Catalog settings
        CatalogSettings_View,
        CatalogSettings_Save,
        Help_View,
        Campaign_Email_View,
        Campaign_Email_Send,
        Campaign_Template_Add,
        Campaign_EmailTemplates_View,
        Families_View,
        PeopleRegistrations_View,
        Parents_View,
        //ParentsInvoices_View,

        //Reports
        Reports_RegistrationData,
        Reports_Finance,
        Reports_Rosters,
        Reports_Capacities,
        Reports_CapacitiesGraph,
        Reports_Custom,
        Reports_MobileApp,
        Reports_Checkin,
        Reports_SignOutSheet,
        Reports_DismissalInformation,
        Reports_DailyDismissal,
        Reports_ProviderContactInformation,
        Reports_InstructorList,
        Reports_InstructorAssignments,
        Reports_ParentEmailsList,
        Reports_RoomAssignmentsAdmin,
        Reports_StudentAttendance,
        Reports_StudentSummaryAttendance,
        Reports_CustomAttendance,
        Reports_Rosters_Admin,
        Reports_Rosters_Instructor,
        Reports_Finance_School_Provider,
        Reports_Finance_Coupon_Coordinator,
        Reports_Finance_Provider,
        TotalAmount_View,
        Reports_Subscription,
        Reports_Medical,
        Reports_Operations,
        Reports_SignOut,
        Reports_InternalUse,
        Reports_Attendance,
        Reports_AttendanceSheetAdvanced,
        //System reports
        Reports_SystemFinance,
        Reports_SystemFinance_Revenue,
        Reports_SystemFinance_Transaction,
        Reports_SystemFinance_RevenuMatrix,
        Reports_SystemFinance_Installments,
        Reports_System_AutoCharge,
        Reports_System_ClientCreditCards,
        Reports_System_Installments,
        ConfigureRosterReport_View,

        //Portal reports
        PortalReport_View,
        PortalReport_Catalog,
        PortalReport_UpdateReport,
        PortalReport_CustomReport,
        PortalReport_CustomReportClub,
        PortalReport_Title1Discount,
        PortalReport_FiarFaxCounty,
        PortalReport_Pricing,
        PortalReport_FullClassList,
        PortalReport_CatalogOverviewActivity,
        PortalReport_CatalogList,
        PortalReport_SchoolMemberList,
        PortalReport_Discount,
        ProtalReport_Planning,
        ProtalReport_Form,
        PortalReport_Enrollments,
        PortalReport_Rosters,
        ProtalReport_ProviderAddressList,
        ProtalReport_SchoolAddressList,
        ProtalReport_MemberList,
        ProtalReport_InstructorList,
        ProtalReport_InsuranceCertificateCheck,
        ProtalReport_SeasonSurveyDates,
        ProtalReport_InstructorAssignments,
        ProtalReport_FullSeasonClassList,
        ProtalReport_Followup,
        ProtalReport_InstructorCheckin,
        ProtalReport_ProviderContact,
        ProtalReport_RoomAssignment,
        ProtalReport_Finance,
        PortalReport_Donations,
        PortalReport_ParentMailingList,
        PortalReport_FamilyCreditCard,
        PortalReport_BulkDependentCareReceipt,
        PortalReport_Graduate,
        PortalReport_OnsitePersonCheckin,
        Reports_PerRegistration,
        PortalReport_SchoolInvoices,
        PortalReport_Installments,
        Reports_CampRoster,
        PortalReport_ClientCreditCards,

        //Weekly agenda
        WeeklyAgenda_View,
        WeeklyAgenda_Settings_View,
        WeeklyAgenda_Settings_Save,

        //Settings
        Settings_View,
        Settings_General_View,
        Settings_General_Save,
        Settings_PaymentSettings_View,
        Settings_PaymentSettings_Save,
        Settings_Checkout_View,
        Settings_Checkout_Save,
        Settings_Appearance_View,
        Settings_Appearance_Save,
        Settings_Account_View,
        Settings_Partner_View,
        Settings_Partner_Save,
        Settings_Account_Save,
        Settings_School_View,
        Settings_Advance_View,
        Settings_MobileApp_View,
        Settings_Notifications_View,
        Settings_School_Save,
        Settings_JbHomeSite,
        Settings_Holidays_View,

        //Account setting
        AccountSetting_Overview_View,
        AccountSetting_Overview_Save,
        AccountSetting_Billing_View,
        AccountSetting_Billing_History_View,
        AccountSetting_Billing_Add,
        AccountSetting_Billing_Save,
        AccountSetting_Billing_Delete,
        AccountSetting_Billing_Activate,
        AccountSetting_Insurance_View,
        AccountSetting_Insurance_Save,

        AccountSetting_ChangePlan_View,
        AccountSetting_ChangePlan_Save,
        Account_Subscription_Save,
        Account_Subscription_View,
        Account_Overview_View,

        // Advance setting
        AdvanceSetting_BackgroundCheck_View,
        AdvanceSetting_BackgroundCheck_Save,
        LogoSetting_View,
        //School setting
        SchoolSetting_Info_View,
        SchoolSetting_Info_Save,
        SchoolSetting_ProgramInfo_View,
        SchoolSetting_ProgramInfo_Save,
        SchoolSetting_OnSiteInfo_View,
        SchoolSetting_OnSiteInfo_Save,
        SchoolSetting_RegistrationInfo_View,
        SchoolSetting_RegistrationInfo_Save,
        SchoolSetting_AutoChargePolicy_View,
        SchoolSetting_AutoChargePolicy_Save,
        SchoolSetting_DelinquentPolicy_View,
        SchoolSetting_DelinquentPolicy_Save,
        SchoolSetting_ProgramPolicy_View,
        SchoolSetting_DuplicateEnrollment_View,
        SchoolSetting_DuplicateEnrollment_Save,
        Season_SchoolSetting_PaymentPlan,

        //School setting
        Season_SchoolSetting_GeneralInfo_View,
        Season_SchoolSetting_GeneralInfo_Save,
        Season_SchoolSetting_DetailInfo_View,
        Season_SchoolSetting_DetailInfo_Save,
        Season_SchoolSetting_ScheduleInfo_View,
        Season_SchoolSetting_ScheduleInfo_Save,
        Season_SchoolSetting_RegistrationInfo_View,
        Season_SchoolSetting_RegistrationInfo_Save,

        ViewJumbulaHomeSettings,

        //Member
        Member_View,
        Member_Add,
        Subsidies_View,
        Subsidy_Add,
        Districts_View,
        District_Add,

        //Donation
        DonationForm_View,
        DonationForm_Save,

        // Overview
        SeasonOverview_View,
        TotalSale_View,
        TotalRegistration_View,
        DailySale_View,
        CapacityReport_View,
        AttendanceReports_View,
        SaleDistribution_View,
        Tour_View,
        ProgramCalendar_View,
        ProgramCalendar_Action,

        // Orders
        Orders_View,
        Offline_Register,
        OrderDetail_View,
        Order_Edit,
        Order_Transfer,
        Order_TakePayment,
        Order_Refund,
        Order_History,

        // Sessions
        ProgramSession_List_View,
        ProgramSession_Edit,
        ProgramSession_Delete,
        ProgramSession_Attendance_View,

        // Waitlist
        Waitlist_View,

        // System
        Clubs_View,
        Clubs_Add,
        Toolbox_view,

        // Messaging
        Messaging_View,

        // Parent
        ParentDashboard_View,

        // People
        People_Delinquents_View,
        People_Delinquents_SendMail,
        People_Delinquents_ViewHistory,
        People_ViewHistory_AddHistory,
        People_ViewHistory_EditHistory,
        People_ViewHistory_DeleteHistory,
        People_ViewHistory_ViewEmail,

        // Family
        [Description("Credit card update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_Creditcard,
        [Description("Credit card delete")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Delete_Creditcard,
        [Description("Insurance add")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Insurance_Add,
        [Description("Email update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_Email,
        [Description("Password update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_Password,
        [Description("Participant update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_Participant,
        [Description("Insurance update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_Insurance,
        [Description("Emergency contact update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_EmergencyContact,
        [Description("Authorized pickup update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_AuthorizedPickup,
        [Description("Player delete")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Delete_Player,
        [Description("Family contact delete")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Delete_Contact,
        [Description("Parent update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_Parent,
        [Description("Follow-up form fill")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Complete_FollowupForm,
        [Description("Registration form update")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Change_RegistrationForm,
        [Description("Waitlist delete")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Delete_Waitlist,
        [Description("Add invoice to cart")]
        [JbZone(JbZone.ParentDashboard)]
        Family_Add_InvoiceToCart,
        FamilyAddItemToCart,
        [Description("Login to parent dashboard")]
        [JbZone(JbZone.ParentDashboard)]
        Family_View_Admin,

        [Description("Login")]
        [JbZone(JbZone.ParentDashboard)]
        Login,
        [Description("Logout")]
        [JbZone(JbZone.ParentDashboard)]
        Logout,
        [Description("Signup")]
        [JbZone(JbZone.ParentDashboard)]
        Signup,

        //ActivityLog
        [Description("Activity log view")]
        ActivityLog_View
    }

    public class JbZoneAttribute : Attribute
    {
        internal JbZoneAttribute(JbZone zone)
        {
            Zone = zone;
        }

        public JbZone Zone { get; private set; }

        public static JbZone? GetZone(JbAction action)
        {
            JbZone? result = null;

            var attribute = action.GetAttribute<JbZoneAttribute>();

            if (attribute != null)
            {
                result = attribute.Zone;
            }

            return result;
        }

    }
}

