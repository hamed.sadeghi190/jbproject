﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class EditClubUserRoleViewModel : BaseViewModel<Club>
    {
        public EditClubUserRoleViewModel()
        {
            JumbulaRoles = SeedRoles();
        }
        [EmailAddress]
        [Display(Name = "New username")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string NewUserName { get; set; }

     public string ClubName { get; set; }
        public int ClubId { get; set; }
        public int SelectedUserId { get; set; }

     
        public List<UserViewModel> ClubUsers { get; set; }

        public List<SelectKeyValue<string>> JumbulaRoles { get; set; }

       public string SelectedRole { get; set; }

        List<SelectKeyValue<string>> SeedRoles()
        {
            var result = new List<SelectKeyValue<string>>();
    
            foreach (RoleCategory type in Enum.GetValues(typeof(RoleCategory)))
            {
                if (type == RoleCategory.Accountant || type == RoleCategory.Instructor || type == RoleCategory.Owner || type == RoleCategory.Parent || type == RoleCategory.SysAdmin || type == RoleCategory.Support || type == RoleCategory.Partner)
                {
                    continue;
                }
                result.Add(new SelectKeyValue<string>() { Text = type.ToDescription(), Value = (type).ToString() });
            }

            return result;
        }
    }

}