
CREATE TABLE [dbo].[OrderItemWaivers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[OrderItemId] [bigint] NOT NULL,
	[Signature] [nvarchar](max) NULL,
	[Agreed] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.OrderItemWaivers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[OrderItemWaivers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderItemWaivers_dbo.OrderItems_OrderItemId] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[OrderItems] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OrderItemWaivers] CHECK CONSTRAINT [FK_dbo.OrderItemWaivers_dbo.OrderItems_OrderItemId]
GO

ALTER TABLE [dbo].[ClubWaivers] ADD [IsRequired] [bit] Not NULL default 1
go
ALTER TABLE [dbo].[ClubWaivers] ADD [WaiverConfirmationType] [tinyint] Not NULL default 0
