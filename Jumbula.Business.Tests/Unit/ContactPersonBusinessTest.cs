﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Contact;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class ContactPersonBusinessTest
    {
        #region Fields

        private Mock<IContactRepository> _mockContactRepository;
        private Mock<IClubRepository> _mockClubRepository;
        private Mock<IClubBusiness> _mockClubBusiness;
        private Mock<IFieldSettingBusiness> _mockFieldSettingBusiness;

        #endregion

        #region Constants
        private const int PageIndex = 0;
        private const int PageSize = 10;
        private const int Total = 100;
        private const int Id = 10;
        #endregion

        #region Test methods
        [TestMethod]
        public void GetPartnerContacts_WithoutMemberIdIncludePartner_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockClubRepository.Setup(x => x.GetMembersOfPartner(Id, true)).Returns(CreateClubs().AsQueryable);
            _mockContactRepository.Setup(x => x.GetClubContacts(new List<int> { Id, Id + 1, Id + 2 }, It.IsAny<Expression<Func<ContactPerson, bool>>>(), string.Empty, null, PageIndex, PageSize)).Returns(CreateContactPerson);

            // Act
            var result = service.GetPartnerContacts(Id, new ContactFilterViewModel(), null, PageIndex, PageSize);

            // Assert
            result.Count.Should().Be(3);
            result.ElementAt(0).CellPhone.Should().Be("604-518-1234");
            result.ElementAt(0).DoB.Should().Be(new DateTime(1982, 02, 03));
            result.ElementAt(0).Email.Should().Be("mostafa@jumbla.com");
            result.ElementAt(0).FirstName.Should().Be("McIan");
            result.ElementAt(0).LastName.Should().Be("Phan");
            result.ElementAt(0).IsLimitedForChange.Should().BeTrue();
            result.ElementAt(0).IsPrimary.Should().BeTrue();
            result.ElementAt(0).Nickname.Should().Be("Iany");
            result.ElementAt(0).IsStaff.Should().BeFalse();
            result.ElementAt(0).OrganizationName.Should().Be("Jb Club");
            result.ElementAt(0).Phone.Should().Be("953-408-6543");
            result.ElementAt(0).PhoneExtension.Should().Be("123");
            result.ElementAt(0).StaffStatus.Should().Be("Active");
            result.ElementAt(0).Title.Should().Be("Instructor");
            result.ElementAt(0).WorkPhone.Should().Be("996-654-3970");
        }

        [TestMethod]
        public void GetPartnerContacts_StaffRoleIsNotEmpty_ShouldReturnValidTitle()
        {
            // Arrange
            var service = CreateService();
            _mockClubRepository.Setup(x => x.GetMembersOfPartner(Id, true)).Returns(CreateClubs().Where(p => p.Id == Id + 1).AsQueryable);
            _mockContactRepository.Setup(x => x.GetClubContacts(new List<int> { Id + 1 }, It.IsAny<Expression<Func<ContactPerson, bool>>>(), null, null, PageIndex, PageSize)).Returns(CreateContactPerson);

            // Act
            var result = service.GetPartnerContacts(Id, new ContactFilterViewModel(), null, PageIndex, PageSize);

            // Assert
            result.Count.Should().Be(3);
            result.ElementAt(2).Title.Should().Be("Instructor/Manager");
        }

        [TestMethod]
        public void GetPartnerContacts_ContactPersonIsNotStaffNorPrimary_ShouldReturnValidIsLimitedForChange()
        {
            // Arrange
            var service = CreateService();
            _mockClubRepository.Setup(x => x.GetMembersOfPartner(Id, true)).Returns(CreateClubs().Where(p => p.Id == Id + 1).AsQueryable);
            _mockContactRepository.Setup(x => x.GetClubContacts(new List<int> { Id + 1 }, It.IsAny<Expression<Func<ContactPerson, bool>>>(), null, null, PageIndex, PageSize)).Returns(CreateContactPerson);

            // Act
            var result = service.GetPartnerContacts(Id, new ContactFilterViewModel(), null, PageIndex, PageSize);

            // Assert
            result.Count.Should().Be(3);
            result.ElementAt(1).IsLimitedForChange.Should().BeFalse();
        }

        [TestMethod]
        public void GetPartnerContacts_NoMembersOfPartner_ShouldNotReturnAnyResult()
        {
            // Arrange
            var service = CreateService();
            _mockClubRepository.Setup(x => x.GetMembersOfPartner(Id, true)).Returns(CreateClubs().Where(p => p.Id == Id + 1).AsQueryable);
            _mockContactRepository.Setup(x => x.GetClubContacts(new List<int> { Id + 1 }, It.IsAny<Expression<Func<ContactPerson, bool>>>(), null, null, PageIndex, PageSize)).Returns(CreateEmptyContactPerson);

            // Act
            var result = service.GetPartnerContacts(Id, new ContactFilterViewModel(), null, PageIndex, PageSize);

            // Assert
            result.Count.Should().Be(0);
        }

        #endregion

        #region Private methods
        private ContactPersonBusiness CreateService()
        {
            _mockContactRepository = new Mock<IContactRepository>();
            _mockClubRepository = new Mock<IClubRepository>();
            _mockClubBusiness = new Mock<IClubBusiness>();
            _mockFieldSettingBusiness = new Mock<IFieldSettingBusiness>();

            return new ContactPersonBusiness(_mockContactRepository.Object, _mockClubRepository.Object, _mockFieldSettingBusiness.Object, _mockClubBusiness.Object);
        }

        private static IEnumerable<Club> CreateClubs()
        {
            var club1 = FactoryGirl.Build<Club>(p => p.Id = Id);
            club1.ClubStaffs = CreateClubStaff().ToList();
            var club2 = FactoryGirl.Build<Club>(p => p.Id = Id + 1);
            club2.ClubStaffs = CreateClubStaff().ToList();
            var club3 = FactoryGirl.Build<Club>(p => p.Id = Id + 2);
            club3.ClubStaffs = CreateClubStaff().ToList();
            return new List<Club> { club1, club2, club3 };
        }

        private static PaginatedList<ContactPerson> CreateContactPerson
        {
            get
            {
                var contactPerson1 = FactoryGirl.Build<ContactPerson>(p => p.Id = Id);
                var contactPerson2 = FactoryGirl.Build<ContactPerson>(p =>
                {
                    p.Id = Id + 1;
                    p.IsStaff = false;
                    p.IsPrimary = false;
                });
                var contactPerson3 = FactoryGirl.Build<ContactPerson>(p =>
                {
                    p.Id = Id + 2;
                    p.StaffRole = "Manager";
                });
                var list = new List<ContactPerson> { contactPerson1, contactPerson2, contactPerson3 };
                return new PaginatedList<ContactPerson>(list, PageIndex, PageSize, Total);
            }
        }

        private static PaginatedList<ContactPerson> CreateEmptyContactPerson => new PaginatedList<ContactPerson>(new List<ContactPerson>(), PageIndex, PageSize, Total);

        private static IEnumerable<ClubStaff> CreateClubStaff()
        {
            var clubStaff1 = FactoryGirl.Build<ClubStaff>(p => p.Id = Id);
            var clubStaff2 = FactoryGirl.Build<ClubStaff>(p => p.Id = Id + 1);
            var clubStaff3 = FactoryGirl.Build<ClubStaff>(p =>
            {
                p.Id = Id + 2;
                p.IsDeleted = true;
            });
            return new List<ClubStaff> { clubStaff1, clubStaff2, clubStaff3 };
        }
        #endregion
    }
}
