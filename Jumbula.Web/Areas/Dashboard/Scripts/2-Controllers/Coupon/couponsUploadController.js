﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('couponsUploadController', ['$scope', '$http', 'cfpLoadingBar', '$stateParams', '$state', 'jbToast', 'jbRandom', 'programService', 'formService', 'jbConfirmation', 'couponUploadService', function ($scope, $http, cfpLoadingBar, $stateParams, $state, jbToast, jbRandom, programService, formService, jbConfirmation, couponUploadService) {
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.Urls = [];

        $scope.Model = null;

        $stateParams.templateId = 0;
        $stateParams.formType = "CouponsUpload";

        $scope.JbForm = {};
        $scope.JbForm.EditMode = false;

        if ($scope.MC.IsFormUploadRename) {
            $scope.JbForm.ConfirmMode = true;
        }

        $scope.JbElement = {}
        var baseUrl = window.location.origin + "/Dashboard/Coupon";

        couponUploadService($scope, $http, $stateParams);
   

        $scope.OnUploaderSuccess = function (e) {
            if (e.response.Status == true && e.response.Data == 'Add') {

                $scope.uploadedList = true;
            
                $scope.Model.CouponsList = e.response.uploadedList;
            
                } 
        }

        $scope.onUpload = function (e) {
            var files = e.files;
            $.each(files, function (index, value) {
                if (value.extension !== '.csv') {
                    jbToast.error("Please upload a .csv file.");
                    e.preventDefault();
                }
            });
        }

        $scope.removeOldList = function () {
            $http.post(baseUrl + '/DeleteUploadedList')
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.showListName = false;
                        $scope.uploadedList = false;
                        $scope.Model.ListName = "";
                    }
                });
        }


        $scope.goBack = function () {
            alert("back");
        };

        var back = function () {
            $scope.MC.IsFormUploadRename = false;
            $state.go('Coupons', { programId: $scope.MC.stateParams.programId }, 'ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
        }
    }]);
})();