﻿
using Jb.Framework.Common;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class SeasonFileBusiness : ISeasonFileBusiness
    {
        private readonly IRepository<SeasonFile> _seasonFileRepository;
        public SeasonFileBusiness(IRepository<SeasonFile> seasonFileRepository)
        {
            _seasonFileRepository = seasonFileRepository;
        }
     
        public OperationStatus Create(long seasonId,int FileId)
        {
            SeasonFile seasonFiles = new SeasonFile();
            seasonFiles.SeasonId = seasonId;
            seasonFiles.FileId = FileId;
            return _seasonFileRepository.Create(seasonFiles);
        }

       
    }
}
