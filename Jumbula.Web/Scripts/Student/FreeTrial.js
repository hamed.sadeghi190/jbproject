﻿$('input').attr('autocomplete', 'off');

function AjaxFailure() {
    $(this).removeClass('btn-spinner');
}

//$('.btn.btn-jb[type="submit"]').click(function () {
  //  $(this).addClass('btn-spinner');
//});

function lblInputSetFocus() {
    $('.jb-lbl-input').each(function () {
        if ($(this).val() == "" || $(this).val() == null) {
            $(this).parent('.form-group').removeClass('focused');
        } else {
            $(this).parent('.form-group').addClass('focused');
        }
    });
};
lblInputSetFocus();

$('.jb-lbl-input').focus(function () {
    if ($(this).val() == "") {
        $(this).parent('.form-group').addClass('focused');
    }
    $(this).parent().find('.input-tooltip').fadeIn();
});
$('.jb-lbl-input').focusout(function () {
    if ($(this).val() == "") {
        $(this).parent('.form-group').removeClass('focused');
        //$(this).parent().find('.domain-url').removeAttr('style');
    }
    $(this).parent().find('.input-tooltip').fadeOut();
});


//$('#organizationName').focus(function () {
//    $('#domainURL').parent('.form-group').addClass('focused');
//}).focusout(function () {
//    if ($('#domainURL').val() == "" || $('#domainURL').val() == null) {
//        $('#domainURL').parent('.form-group').removeClass('focused');
//    }
//});

//$('#organizationName').keyup(function () {
//    var $organizationName = $(this).val();
//    $('#domainURL').val($organizationName.replace(/[^A-Z0-9_-]/ig, "_"));
//    checkDomain();
//});

$('#domainURL').keyup(function () {
    var $domainURL = $(this).val();
    $(this).val($domainURL.replace(/[^A-Z0-9-]/ig, "-"));
    checkDomain();
});

$('.form-group .jb-label, .form-group .domain-url').click(function (e) {
    e.preventDefault();
    $(this).parent().find('.jb-lbl-input').focus();
})

$('#domainURL').keyup(function () {
    checkDomain();
});

$('.form-group.website input.jb-lbl-input').on('focus', function () {
    $(this).attr('placeholder', 'e.g. http://www.example.com');
}).on('focusout', function () {
    $(this).removeAttr('placeholder');
});

function checkDomain() {
    var $domainUrl = $('#domainURL').val();
    $.ajax({
        type: 'POST',
        url: 'IsClubDomainValid?domain=' + $domainUrl,
        success: function (data) {
            if (data.status) {
                $('.form-group.domain').removeClass('invalid-input').addClass('valid-input');

            } else {  
                $('.form-group.domain').removeClass('valid-input').addClass('invalid-input');
            }
        }
    });
}

var progressBar = $('.progress .progress-bar');

$('#password').focusin(function () {
    $('#passwordGroup .password-strength-info').fadeIn();
}).focusout(function () {
    $('#passwordGroup .password-strength-info').fadeOut();
}).keyup(function () {
    var pswd = $(this).val();
    //validate the length
    if (pswd.length < 8) {
        $('#length').removeClass('valid').addClass('invalid');
    } else {
        $('#length').removeClass('invalid').addClass('valid');
    }
    //validate symbols
    if (pswd.match(/[^A-Z0-9]/ig)) {
        $('#symbol').removeClass('invalid').addClass('valid');
    } else {
        $('#symbol').removeClass('valid').addClass('invalid');
    }
    //validate Uppercase Letters
    if (pswd.match(/[A-Z]/)) {
        $('#uppercase').removeClass('invalid').addClass('valid');
    } else {
        $('#uppercase').removeClass('valid').addClass('invalid');
    }
    //validate Lowercase Letters
    if (pswd.match(/[a-z]/)) {
        $('#lowercase').removeClass('invalid').addClass('valid');
    } else {
        $('#lowercase').removeClass('valid').addClass('invalid');
    }
    //validate number
    if (pswd.match(/\d/)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');     
    }

    var validSize = $('.password-strength-info > li.valid').size();
    progressWidth = validSize * 20;
    progressBar.css({ 'width': progressWidth + '%' });
    if (progressWidth <= 80) {
        progressBar.css({ 'background-color': '#ff0000' });
        if ($('#passwordGroup .password-strength-info').css('display') == "none")
            $('#passwordGroup .password-strength-info').fadeIn();
    } else {
        progressBar.css({ 'background-color': '#55c700' });
        $('#passwordGroup .password-strength-info').fadeOut();
    }
});


// Set current year for copyright on footer-content
var currentYear = new Date().getFullYear();
$('#freeTrialContainer .footer-content .copyright .year').html(currentYear);