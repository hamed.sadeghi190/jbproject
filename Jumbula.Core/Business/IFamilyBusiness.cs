﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IFamilyBusiness
    {
        Family GetFamily(int userId);
        OperationStatus Update(Family entity);
        OperationStatus Create(Family entity);
        Family AddUserFamily(int userId);

        IQueryable<Family> GetList();
    }
}
