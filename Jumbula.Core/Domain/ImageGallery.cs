﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class ImageGallery : BaseEntity
    {
        public ImageGallery()
        {
            Images = new HashSet<ImageGalleryItem>();
        }

        [Required]
        public string Name { get; set; }

        public string Domain { get; set; }

        [Required]
        public string ClubDomain { get; set; }

        [Required]
        public ImageGalleryType Type { get; set; }

        [Required]
        public EventStatusCategories Status { get; set; }

        [Required]
        public MetaData MetaData { get; set; }
        public virtual ICollection<ImageGalleryItem> Images { get; set; }

    }
}
