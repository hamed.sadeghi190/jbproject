﻿using System.Xml.Serialization;
using SportsClub.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SportsClub.Infrastructure;
using SportsClub.Db;
using System.IO;
using System.Web.Mvc;
using SportsClub.Infrastructure.JBMembership;
using SportsClub.Infrastructure.Helper;
using SportsClub.Models.FormModels;
using Jb.Framework.Common.Model;

namespace SportsClub.Models
{
    public enum FinishDateType
    {
        EndDate,
        NumberOfSession,
        UnSpecifiedEndDate,
    }


    public class CoursePageViewModel
    {
        public bool IsAdmin { get; set; }
        public CourseViewModel Course { get; set; }
        public GoogleMapsViewModel Map { get; set; }
        public CourseInfoPageViewModel Info { get; set; }
        public ICollection<CoursePageImageGalleryItemViewModel> Gallery { get; set; }
        public int ProfileId { get; set; }
        public bool LoggedIn { get; set; }
        public RoleCategories CurrentUserRole { get; set; }
        public EventStatusCategories EventStaus { get; set; }
        public PageMetaDataViewModel MetaData { get; set; }
        public Level Level { get; set; }
        public Genders Gender { get; set; }
        
        public bool IsOutsourcerEvent { get; set; }
        public string OutsourcerClubDomain { get; set; }
        public string OutsourcerClubName { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                // Get cart
                var cartDb = Ioc.ICartDbService;

                // Get domain of orders in the cart(if there is no order in cart return empty)
                string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                if (!string.IsNullOrEmpty(currentDomain))
                {
                    return !(currentDomain == this.Course.ClubDomain);
                }

                return false;
            }
        }

        public string MinAgeMessage
        {
            get
            {
                if (Course.EventSearchField.MinAge.HasValue)
                {

                    if (Course.EventSearchField.MaxAge.HasValue)
                    {
                        return string.Format(Constants.Age_Restriction_Message, Course.EventSearchField.MinAge, Course.EventSearchField.MaxAge);
                    }

                    return string.Format(Constants.Min_Age_Restriction_Message, Course.EventSearchField.MinAge);
                }
                if (Course.EventSearchField.MaxAge.HasValue)
                {
                    return string.Format(Constants.Max_Age_Restriction_Message, Course.EventSearchField.MaxAge);
                }
                return "";
            }
        }

        public string GradeMessage
        {
            get
            {
                if (Course.EventSearchField.MinGrade != null)
                {

                    if (Course.EventSearchField.MaxGrade != null)
                    {
                        return string.Format(Constants.Grade_Restriction_Message, Course.EventSearchField.MinGrade.Value.ToDescription(),
                                             Course.EventSearchField.MaxGrade.ToDescription());
                    }
                    return string.Format(Constants.Min_Grade_Restriction_Message, Course.EventSearchField.MinGrade.Value.ToDescription());
                    //
                }
                if (Course.EventSearchField.MaxGrade != null)
                {
                    return string.Format(Constants.Max_Grade_Abbreviation, Course.EventSearchField.MaxGrade.Value.ToDescription());
                }
                return "";
            }
        }

        public string LeftDays
        {
            get
            {
                string result = string.Empty;

                DateTime expireDate = Course.RegistrationPeriod.RegisterEndDate.Value.AddDays(1);

                TimeSpan elapsedTime = expireDate - Utilities.GetCurrentLocalDateTime(Course.TimeZone);

                // If remainig time is more than 3 days
                if (elapsedTime.TotalHours >= 72)
                {
                    result = string.Empty;
                }
                // If remainig time is between 3 and 1 day
                else if (elapsedTime.TotalHours >= 24 && elapsedTime.TotalHours < 72)
                {
                    result = string.Format("{0} days left", elapsedTime.Days + 1);
                }
                // If remainig time is less than 1 day
                else if (elapsedTime.TotalHours < 24)
                {
                    if (elapsedTime.TotalHours >= 1)
                    {
                        result = string.Format("{0}:{1} hours left", elapsedTime.Hours, elapsedTime.Minutes);
                    }
                    // If remainig time is less than 1 houre
                    else if (elapsedTime.Minutes > 0 && elapsedTime.Minutes < 60)
                    {
                        result = string.Format("{0} min left", elapsedTime.Minutes);
                    }
                }

                return result;
            }
        }

        public DateTime? RegisterStartDate { get; set; }

        public CoursePageViewModel()
        {

        }

        public int? CapacityLeft { get; set; }

        public CoursePageViewModel(Course course)
        {
            var club = Ioc.IClubDbService.GetBasicInfo(course.ClubDomain);
            IsAdmin = WebMatrix.WebData.WebSecurity.CurrentUserId == club.UserId;

            Course = new CourseViewModel(course);

            EventStaus = course.EventStatus;
            Gender = Course.EventSearchField.Gender.HasValue ? (Genders)Course.EventSearchField.Gender : Genders.BoyAndGirl;
            Level = Course.EventSearchField.Level.HasValue ? (Level)Course.EventSearchField.Level : Level.None;
            RegisterStartDate = course.RegistrationPeriod.RegisterStartDate.Value;

            // check if we have capacity 
            if (course.Capacity.HasValue)
            {
                CapacityLeft = course.Capacity.Value - Ioc.ICourseDbService.CompletedOrderCount(course.Domain, course.ClubDomain);
            }

            Map = new GoogleMapsViewModel()
            {
                Name = course.ClubLocation.Name,
                PostalAddress = Course.PostalAddress,
                Room = course.Room
            };
            string logoUri = string.Empty;
            if (club != null)
            {
                logoUri = Utilities.GetClubLogoURL(club.Domain, club.Logo, course.CategoryId);
            }

            // check if club has affiliator
            bool hasAffilator = false;
            string affiliatorLogoUri = string.Empty;
            string affiliatorDomain = string.Empty;
            string affiliatorName = string.Empty;
            if (club.AffiliatorID.HasValue)
            {
                hasAffilator = true;
                IClubDb clubDb = Infrastructure.Helper.Ioc.IClubDbService;
                var affiliatorClub = clubDb.GetBasicInfoOfAffiliatorClubByAffiliatorID(club.AffiliatorID.Value);

                if (affiliatorClub != null)
                {
                    affiliatorLogoUri = Utilities.GetClubLogoURL(affiliatorClub.Domain, affiliatorClub.Logo, affiliatorClub.CategoryId);
                    affiliatorDomain = affiliatorClub.Domain;
                    affiliatorName = affiliatorClub.Name;
                }
            }

            Info = new CourseInfoPageViewModel()
            {
                Name = course.Name,
                NameLong = course.NameLong,
                Domain = course.Domain,
                ClubDomain = course.ClubDomain,
                ClubName = club.Name,
                PostalAddress = course.ClubLocation.PostalAddress,
                StartDate = Course.StartDate,
                FinishDate = Course.FinishDate,
                LeftSessions = Course.NumOfSessionLeft,
                LocationName = course.ClubLocation.Name,
                PdfFileName = course.FileName,
                LogoUri = logoUri,
                //IsOnProgress = Course.IsOnProgress,
                CourseType = Course.CourseType,
                //Expired = Utilities.IsEventExpired(Course.FinishDate.Value, course.TimeZone) || Course.NumOfSessionLeft <= 0,
                HasAffiliator = hasAffilator,
                AffiliatorLogoUri = affiliatorLogoUri,
                AffiliatorDomain = affiliatorDomain,
                AffiliatorName = affiliatorName,
                Room = course.Room,
                WeeksCount = course.WeeksCount,
                EventRegStatus = Utilities.GetEventRegStatus(course.RegistrationPeriod.RegisterStartDate.Value, course.RegistrationPeriod.RegisterEndDate.Value, course.TimeZone),
                EventRunningStatus = Utilities.GetEventRunningStatus(course.RecurrentStartDate.Value, course.RecurrentEndDate, course.TimeZone)
            };
            

            // Meta tag
            MetaData = new PageMetaDataViewModel()
            {
                PageTitle =
                            this.Info.Name +
                            Constants.C_Space +
                            "by" +
                            Constants.C_Space +
                            this.Info.ClubName +
                            Constants.S_SpaceDashSpace +
                            CategoryModel.GetCategoryName(course.CategoryId) +
                            Constants.C_Space + SubDomainCategories.Course,

                MetaKeywords = string.Join(Constants.S_CommaSpace, new string[] 
                            { 
                                this.Info.Name,
                                this.Info.ClubName,
                                CategoryModel.GetCategoryName(course.CategoryId) + Constants.C_Space + SubDomainCategories.Course, 
                                Utilities.StringNullHandle(course.ClubLocation.PostalAddress.City,string.Empty), 
                                Utilities.GetAbbreviationState(course.ClubLocation.PostalAddress.State) 
                            }),

                MetaDesc = course.Description,
            };

            // load image gallery
            Gallery = new List<CoursePageImageGalleryItemViewModel>();

            string galleryPath = string.Format("{1}/{0}", course.Domain, course.ClubDomain);

            //StorageManager sm = new SportsClub.Infrastructure.StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
            //Uri[] images = sm.ListDirectoryBlobFiles(galleryPath, Constants.Path_ClubFilesImagesDir);
            //Uri[] thumbs = sm.ListDirectoryBlobFiles(galleryPath, Constants.Path_ClubFilesThumnailsDir);
            List<Domain.ImageGalleryItem> images = Ioc.IImageGalleryDbService.GetImageGalleryEvent(course.Domain, course.ClubDomain);
            Gallery = new List<CoursePageImageGalleryItemViewModel>();
            foreach (var item in images)
            {
                Gallery.Add(new CoursePageImageGalleryItemViewModel()
               {
                   OriginUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, course.ClubDomain, item.Path).AbsoluteUri,
                   ThumbnailUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, course.ClubDomain, course.Domain + "/ImageGallery/" + Path.GetFileNameWithoutExtension(item.Path) + "_Thumbnail" + Path.GetExtension(item.Path)).AbsoluteUri,
                   ImageTitle = item.Title,
                   ImageDescription = item.Description
               });
            }

            // Realize event is outsourcer or not
            IsOutsourcerEvent = !string.IsNullOrEmpty(course.OutSourcerClubDomain);

            if (IsOutsourcerEvent)
            {
                OutsourcerClubDomain = course.OutSourcerClubDomain;

                //Get outsourcer club domain from db
                OutsourcerClubName = Ioc.IClubDbService.GetClubName(OutsourcerClubDomain);
            }
        }
    }

    public class CourseInfoPageViewModel
    {
        public string Name { get; set; }
        public string NameLong { get; set; }
        public string Domain { get; set; }
        public string ClubDomain { get; set; }
        public string ClubName { get; set; }
        public int? LeftSessions { get; set; }
        public string LocationName { get; set; }
        public PostalAddress PostalAddress { get; set; }
        public string PdfFileName { get; set; }
        public string LogoUri { get; set; }
        public int? WeeksCount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public CourseType CourseType { get; set; }
        // affiliator 
        public bool HasAffiliator { get; set; }

        public string AffiliatorLogoUri { get; set; }

        public string AffiliatorDomain { get; set; }

        public string AffiliatorName { get; set; }

        public string Room { get; set; }

        public EventRegStatus EventRegStatus { get; set; }

        public EventRunningStatus EventRunningStatus { get; set; }
    }

    public class CoursePageImageGalleryItemViewModel
    {
        public string OriginUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string ImageTitle { get; set; }
        public string ImageDescription { get; set; }
    }

    public class CourseViewModel
    {
        //public IList<CustomFieldViewModel> TemplateFields { get; set; }

        public string ItemId { get; set; }

        public string Domain { get; set; }
        public string ClubDomain { get; set; }

        public int? CoachId { get; set; }

        public int? AssistantCoachId { get; set; }

        [Required]
        [DisplayName("Class name")]
        [MaxLength(100, ErrorMessage = "{0} is too long")]
        [MinLength(3, ErrorMessage = "Enter a complete {0}")]
        public string Name { get; set; }

        [DisplayName("Class name (continued)")]
        [MaxLength(255, ErrorMessage = "{0} is too long")]
        public string NameLong { get; set; }

        [DisplayName("Capacity")]
        //[Range(1, Constants.Course_Capacity_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? Capacity { get; set; }

        public int ReservedCapacity { get; set; }

        [Required]
        [DisplayName(" Head instructor")]
        [MaxLength(255, ErrorMessage = "{0} is too long")]
        public string HeadCoach { get; set; }

        [DisplayName("Assistant instructors")]
        [MaxLength(255, ErrorMessage = "{0} is too long")]
        public string AssistantCoaches { get; set; }

        [AllowHtml]
        [DisplayName("Class description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public ClubLocation ClubLocation { get; set; }

        //public ICollection<CourseRecurrenceTimeViewModel> RecurrenceTime { get; set; }
        public MeetUpTimes RecurrenceTime { get; set; }
        [DisplayName("All sessions price")]
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be a positive number")]
        public decimal? FullPrice { get; set; }

        [DisplayName("Do you have per session (drop-in) price?")]
        //[Range(1, int.MaxValue, ErrorMessage = "{0} must be a positive number")]
        public decimal? SessionPrice { get; set; }

        [DisplayName("Allow late registration and prorate the price?")]
        public bool HasProRatingPrice { get; set; }


        [DisplayName("Number of weeks")]
        public int? WeeksCount { get; set; }

        [DisplayName("Early bird discount")]
        public ChargeDiscountViewModel EarlyBirdDiscount { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Early bird discount end date")]
        public DateTime? EarlyBirdEndDate { get; set; }

        [DisplayName("Sibling discount")]
        public ChargeDiscountViewModel SiblingDiscount { get; set; }

        [DisplayName("Club membership discount")]
        public ChargeDiscountViewModel MembershipDiscount { get; set; }

        public DonationViewModel Donation { get; set; }

        public PolicyViewModel Refund { get; set; }
        public PolicyViewModel Waiver { get; set; }
        public PolicyViewModel MedicalRelease { get; set; }

        [DisplayName("Skill level")]
        public OptionalRegDataViewModel SkillLevel { get; set; }

        [DisplayName("School")]
        public OptionalRegDataViewModel School { get; set; }

        [DisplayName("Parent/gaurdian information")]
        public OptionalRegDataViewModel ParentGuardian { get; set; }

        [DisplayName("Emergency contact information")]
        public OptionalRegDataViewModel EmergencyContact { get; set; }

        [DisplayName("Insurance information")]
        public OptionalRegDataViewModel Insurance { get; set; }

        [DisplayName("Doctor contact information")]
        public OptionalRegDataViewModel DoctorContact { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        [AllowHtml]
        [DisplayName("Notes")]
        [DataType(DataType.MultilineText)]
        [StringLength(1024, ErrorMessage = "{0} is too long")]
        public string AdditionalNotes { get; set; }

        [DisplayName("Disable jumbula online regisration")]
        public bool DisableOnlineRegistration { get; set; }

        [DisplayName("External registration url")]
        public string ExternalRegUrl { get; set; }

        [Required(ErrorMessage = "You must check the box indicating that you will review the generated class and registration page.")]
        [DisplayName("I understand that I should double check the system generated class and registration page.")]
        public bool AcceptLicenseAgreement { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Start date")]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Finish date")]
        public DateTime? FinishDate { get; set; }

        [Required]
        [DisplayName("Time zone")]
        public Domain.TimeZone TimeZone { get; set; }

        public string FileName { get; set; }

        public FinishDateType FinishDateType { get; set; }

        public bool IsCreatedMode { get; set; }

        public EventStatusCategories EventStatus { get; set; }

        public MetaData MetaData { get; set; }


        public bool HasSessionPrice { get; set; }

        public int? NumOfSessionLeft { get; set; }

        public bool IsOnProgress { get; set; }

        public ICollection<byte[]> Images { get; set; }

        public decimal ProRatingPrice { get; set; }

        public int TotalSessions { get; set; }

        public string ClubName { get; set; }

        public string LogoUri { get; set; }

        public bool IsLoggedIn { get; set; }

        // Form Wizard Step
        [DefaultValue(EventPageStep.BasicInformation)]
        public EventPageStep CurrentStep { get; set; }

        [DisplayName("Class category")]
        public DisplayCategoryViewModel Category { get; set; }

        [UIHint("PaymentPlanViewModel")]
        public PaymentPlanViewModel PaymentPlan { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public int PostalAdderssId { get; set; }

        public int ClubLocationId { get; set; }

        [DisplayName("Upload an existing flyer")]
        public FlyerType FlyerType { get; set; }

        public EventSearchField EventSearchField { get; set; }

        [DisplayName("Check if you want to save a copy of this event as template.")]
        public bool AsTemplate { get; set; }

        public bool FromTemplate { get; set; }

        [DisplayName("Room")]
        public string Room { get; set; }

        public CourseType CourseType { get; set; }

        public EventSearchTags EventSearchTags { get; set; }

        [DisplayName("Template name")]
        public string TemplateName { get; set; }

        public SelectList Ages { get; set; }

        public SelectList CapacityList { get; set; }

        public DisplayClubLocationViewModel DisplayClubLocationViewModel { get; set; }

        [DisplayName("Outsourcer")]
        public string OutSourcerClubDomain { get; set; }

        public IEnumerable<OutSourcerList> OutSourcers { get; set; }

        public SelectList OutSourcerSelectList { get; set; }

        public int ClubId { get; set; }

        public bool IsVendor { get; set; }

        [DisplayName("Is this an outsourcer event for a school or club?")]
        public EventSponserType IsVendorEvent { get; set; }

        public int OutSourcerClubLocationId { get; set; }

        public bool CaptureRestriction { get; set; }

        public AssignFormViewModel AssignForm { get; set; }

        public List<CustomDiscountViewModel> CustomDiscounts { get; set; }

        public CourseViewModel()
        {
            Donation = new DonationViewModel();
            //TemplateFields = new List<CustomFieldViewModel>();
            AssignForm = new AssignFormViewModel();
            ClubLocation = new ClubLocation();
            ProRatingPrice = 0;
            PostalAddress = new PostalAddress();
            Images = new HashSet<byte[]>();
            SeedRegDataOptions();
            FinishDateType = FinishDateType.NumberOfSession;
            SeedCourseRecurrenceTime(null);
            EventSearchField = new EventSearchField();
            EventSearchTags = new EventSearchTags();
            SeedAges();
            SeedCapacity();
            OutSourcers = new List<OutSourcerList>();
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel();
            PaymentPlan = new PaymentPlanViewModel();
            CustomDiscounts = new List<CustomDiscountViewModel>();
            RegistrationPeriod = new RegistrationPeriodViewModel();
        }

        // this constructor is for create mode
        public CourseViewModel(string clubDomain, Domain.TimeZone timeZone, EventPageStep pageStep,
                               CourseType courseType, bool isCreateMode, int clubId, bool isVendor)
            : this()
        {
            ClubDomain = clubDomain;
            TimeZone = timeZone;
            CurrentStep = pageStep;
            CourseType = courseType;
            IsCreatedMode = isCreateMode;
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(clubDomain);
            SeedCourseRecurrenceTime(null);
            SeedPolicies();
            SeedChargeDiscounts();
            SeedCategories(clubCategoryId: JBMembership.GetActiveClubBaseInfo().CategoryId);
            SeedClubLocation();
            ClubId = clubId;
            IsVendor = isVendor;
            SeedOUtSourcers();
            PaymentPlan = new PaymentPlanViewModel(clubDomain);
            RegistrationPeriod.RegisterStartDate = Utilities.GetCurrentLocalDateTime(timeZone);

            // initial AssignForm
            var forms = Ioc.IFormsDbService.GetForms(clubDomain, EventStatusCategories.open);
            AssignForm = new AssignFormViewModel() { AllForms = new SelectList(forms, "Id", "Name") };

            AssignForm.Forms = forms.Select(x => new FormListViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

        }


        public CourseViewModel(string clubDomain, bool isCreatedMode)
        {
            // TemplateFields = new List<CustomFieldViewModel>();
            ClubLocation = new ClubLocation();
            ProRatingPrice = 0;

            Images = new HashSet<byte[]>();

            FinishDateType = FinishDateType.NumberOfSession;
            ClubDomain = clubDomain;
            IsCreatedMode = isCreatedMode;
            EventSearchTags = new EventSearchTags();
        }

        // this constructor is for edit mode
        public CourseViewModel(Course course, bool fromTemplate = false, int clubId = 0, bool isVendor = false)
            : this()
        {
            // set event for a special outsourcer
            ClubId = clubId;
            IsVendor = isVendor;
            IsVendorEvent = string.IsNullOrEmpty(course.OutSourcerClubDomain) ? EventSponserType.Vendor : EventSponserType.OutSourcer;
            OutSourcerClubDomain = course.OutSourcerClubDomain;
            SeedOUtSourcers();
            SeedCategories(course.CategoryId);

            RegistrationPeriod  = new RegistrationPeriodViewModel(course.RegistrationPeriod.RegisterStartDate.Value, course.RegistrationPeriod.RegisterEndDate.Value); 

            // initial AssignForm
            var forms = Ioc.IFormsDbService.GetForms(course.ClubDomain, EventStatusCategories.open);
            AssignForm = new AssignFormViewModel() { AllForms = new SelectList(forms, "Id", "Name") };
            foreach (var item in course.EventForms)
            {
                AssignForm.SelectedForms.Add(item.FormId);
            }

            AssignForm.Forms = forms.Select(x => new FormListViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            EventSearchField = new EventSearchField
            {
                Gender = course.EventSearchField.Gender,
                Level = course.EventSearchField.Level,
                MaxAge = course.EventSearchField.MaxAge,
                MinAge = course.EventSearchField.MinAge,
                MaxGrade = course.EventSearchField.MaxGrade == SchoolGradeType.College || course.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool ? SchoolGradeType.None : course.EventSearchField.MaxGrade,
                MinGrade = course.EventSearchField.MinGrade == SchoolGradeType.College || course.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool ? SchoolGradeType.None : course.EventSearchField.MinGrade,

            };

            PostalAddress = course.ClubLocation.PostalAddress;
            // TemplateFields = new List<CustomFieldViewModel>();
            FlyerType = course.FlyerType;
            FileName = course.FileName;
            Room = course.Room;
            AsTemplate = !fromTemplate && course.AsTemplate;
            FromTemplate = fromTemplate;
            CourseType = course.CourseType;
            EventSearchTags = course.EventSearchTags;
            TemplateName = course.TemplateName;
            CaptureRestriction = course.CaptureRestriction;

            CoachId = course.CoachId;
            AssistantCoachId = course.AssistantCoachId;

            SeedCourseRecurrenceTime(course.RecurrenceTime);

            SeedPolicies();
            SeedChargeDiscounts();
            SeedRegDataOptions();
            ItemId = course.ItemId;
            Domain = course.Domain;
            ClubDomain = course.ClubDomain;
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(course.ClubDomain)
                {
                    SelectedClubLocationId = course.ClubLocation.Id
                };

            FileName = course.FileName;
            Name = course.Name;
            NameLong = course.NameLong;
            Capacity = course.Capacity;
            ReservedCapacity = course.ReservedCapacity;

            TimeZone = course.TimeZone;
            HeadCoach = course.HeadCoach;
            AssistantCoaches = course.AssistantCoaches;

            Description = course.Description;
            ClubLocation = course.ClubLocation;

            StartDate = course.RecurrentStartDate;

            Donation.Checked = course.Donation.Checked;
            Donation.Title = course.Donation.Title;
            Donation.Description = course.Donation.Description;

            // if weeksCount exsists
            if (course.WeeksCount.HasValue)
            {
                WeeksCount = course.WeeksCount.Value;
                FinishDate = course.RecurrentEndDate.Value;
                FinishDateType = Models.FinishDateType.NumberOfSession;
            }
            else if (course.RecurrentEndDate.HasValue)
            {
                FinishDate = course.RecurrentEndDate.Value;
                FinishDateType = Models.FinishDateType.EndDate;
            }
            else
            {
                FinishDate = null;
                FinishDateType = FinishDateType.UnSpecifiedEndDate;
            }

            CourseType = course.CourseType;
            FullPrice = course.FullPrice;
            SessionPrice = course.SessionPrice;
            DisableOnlineRegistration = course.DisableOnlineRegistration;
            ExternalRegUrl = course.ExternalRegUrl;
            MetaData = course.MetaData;
            EventStatus = course.EventStatus;
            HasSessionPrice = course.HasSessionPrice;
            HasProRatingPrice = course.HasProRatingPrice;

            PaymentPlan = new PaymentPlanViewModel(course.PaymentPlans.FirstOrDefault(), course.ClubDomain);

            TotalSessions = Ioc.ICourseDbService.CalculateTotalSessions(StartDate.Value, FinishDate, course.RecurrenceTime);
            NumOfSessionLeft = Ioc.ICourseDbService.CalculateLeftSessions(StartDate.Value, FinishDate, course.RecurrenceTime);

            if (HasProRatingPrice && NumOfSessionLeft.HasValue)
            {
                ProRatingPrice = Math.Floor((FullPrice.Value * NumOfSessionLeft.Value) / TotalSessions);
            }

            foreach (ChargeDiscount discount in course.ChargeDiscounts)
            {
                switch (discount.Category)
                {
                    case ChargeDiscountCategories.EarlyBird:
                        {
                        EarlyBirdDiscount.IsChecked = true;
                        EarlyBirdDiscount.Amount = (int)discount.Amount;
                        break;
                        }
                    case ChargeDiscountCategories.Sibling:
                        {
                        SiblingDiscount.IsChecked = true;
                        SiblingDiscount.Amount = (int)discount.Amount;
                        SiblingDiscount.ChargeDiscountType = discount.ChargeDiscountType;
                        break;
                        }
                    case ChargeDiscountCategories.ClubMembership:
                        {
                        MembershipDiscount.IsChecked = true;
                        MembershipDiscount.Amount = (int)discount.Amount;
                        MembershipDiscount.ChargeDiscountType = discount.ChargeDiscountType;
                        break;
                }
                    case ChargeDiscountCategories.Custom:
                        {
                            CustomDiscounts.Add(new CustomDiscountViewModel 
                            { 
                                Discount = new ChargeDiscountViewModel 
                                {
                                    IsChecked = true, 
                                    Amount = (int)discount.Amount,
                                    ChargeDiscountType = discount.ChargeDiscountType,
                                    Desc = discount.Desc
            }
                            });
                            break;
                        }

                }
            }

            if (course.ChargeDiscounts.Any(c => c.Category == ChargeDiscountCategories.EarlyBird))
            {
                EarlyBirdEndDate = course.EarlybirdEndDate.Value;
            }

            foreach (Policy policy in course.Policies)
            {
                switch (policy.Category)
                {
                    case PolicyCategories.MedicalRelease:
                        MedicalRelease.Set(policy);
                        break;

                    case PolicyCategories.Refund:
                        Refund.Set(policy);
                        break;

                    case PolicyCategories.Waiver:
                        Waiver.Set(policy);
                        break;
                }
            }

            AdditionalNotes = course.AdditionalNotes;



            foreach (OptionalRegData regdata in course.RegData)
            {
                switch (regdata.Category)
                {
                    case OptionalRegDataCategories.Doctor:
                        DoctorContact.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.EmergencyContact:
                        EmergencyContact.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.Insurance:
                        Insurance.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.ParentGuardian:
                        ParentGuardian.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.School:
                        School.IsChecked = true;
                        break;

                    case OptionalRegDataCategories.SkillLevel:
                        SkillLevel.IsChecked = true;
                        break;
                }
            }
        }

        public Course ToCourse()
        {
            ClubDomain = ClubDomain;
            ItemId = Utilities.GenerateItemId();
            EventStatus = EventStatusCategories.open;
            DateTime now = DateTime.Now;
            MetaData = new MetaData { DateCreated = now, DateUpdated = now };


            List<OptionalRegData> regdata = new List<OptionalRegData>();

            if (SkillLevel.IsChecked)
            {
                regdata.Add(new OptionalRegData
                {
                    Category = OptionalRegDataCategories.SkillLevel
                });
            }

            if (School.IsChecked)
            {
                regdata.Add(new OptionalRegData()
                {
                    Category = OptionalRegDataCategories.School
                });
            }

            if (ParentGuardian.IsChecked)
            {
                regdata.Add(new OptionalRegData()
                {
                    Category = OptionalRegDataCategories.ParentGuardian
                });
            }

            if (EmergencyContact.IsChecked)
            {
                regdata.Add(new OptionalRegData()
                {
                    Category = OptionalRegDataCategories.EmergencyContact
                });
            }

            if (EmergencyContact.IsChecked)
            {
                regdata.Add(new OptionalRegData()
                {
                    Category = OptionalRegDataCategories.EmergencyContact
                });
            }

            if (Insurance.IsChecked)
            {
                regdata.Add(new OptionalRegData()
                {
                    Category = OptionalRegDataCategories.Insurance
                });
            }

            if (DoctorContact.IsChecked)
            {
                regdata.Add(new OptionalRegData()
                {
                    Category = OptionalRegDataCategories.Doctor
                });
            }

            List<ChargeDiscount> ChargeDiscounts = new List<ChargeDiscount>();

            if (EarlyBirdDiscount.IsChecked)
            {
                ChargeDiscounts.Add(new ChargeDiscount()
                {
                    Category = ChargeDiscountCategories.EarlyBird,
                    Amount = EarlyBirdDiscount.Amount.HasValue ? EarlyBirdDiscount.Amount.Value : 0,
                    Desc = EarlyBirdDiscount.Desc
                });
            }

            if (SiblingDiscount.IsChecked)
            {
                ChargeDiscounts.Add(new ChargeDiscount()
                {
                    Category = ChargeDiscountCategories.Sibling,
                    Amount = SiblingDiscount.Amount.HasValue ? SiblingDiscount.Amount.Value : 0,
                    Desc = SiblingDiscount.Desc,
                    ChargeDiscountType = SiblingDiscount.ChargeDiscountType,
                });
            }

            if (MembershipDiscount.IsChecked)
            {
                ChargeDiscounts.Add(new ChargeDiscount()
                {
                    Category = ChargeDiscountCategories.ClubMembership,
                    Amount = MembershipDiscount.Amount.HasValue ? MembershipDiscount.Amount.Value : 0,
                    Desc = MembershipDiscount.Desc,
                    ChargeDiscountType = MembershipDiscount.ChargeDiscountType,

                });
            }

            foreach (var customDiscount in CustomDiscounts)
            {
                ChargeDiscounts.Add(new ChargeDiscount()
                {
                    Category = ChargeDiscountCategories.Custom,
                    Amount = customDiscount.Discount.Amount.HasValue ? customDiscount.Discount.Amount.Value : 0,
                    Desc = customDiscount.Discount.Desc,
                    ChargeDiscountType = customDiscount.Discount.ChargeDiscountType,
                });
            }

            List<SportsClub.Domain.Policy> Policies = new List<SportsClub.Domain.Policy>();

            if (Refund.IsChecked)
            {
                Policies.Add(new Policy()
                {
                    Category = PolicyCategories.Refund,
                    Field1 = Refund.Field1.HasValue ? Refund.Field1.Value : -1,
                    Field2 = Refund.Field2.HasValue ? Refund.Field2.Value : -1,
                    Description = Refund.Desc
                });
            }

            if (Waiver.IsChecked)
            {
                Policies.Add(new Policy()
                {
                    Category = PolicyCategories.Waiver,
                    Field1 = Waiver.Field1.HasValue ? Waiver.Field1.Value : -1,
                    Field2 = Waiver.Field2.HasValue ? Waiver.Field2.Value : -1,
                    Description = Waiver.Desc

                });
            }

            if (MedicalRelease.IsChecked)
            {
                Policies.Add(new Policy()
                {
                    Category = PolicyCategories.MedicalRelease,
                    Field1 = MedicalRelease.Field1.HasValue ? MedicalRelease.Field1.Value : -1,
                    Field2 = MedicalRelease.Field2.HasValue ? MedicalRelease.Field2.Value : -1,
                    Description = MedicalRelease.Desc

                });
            }

            // recurrencetime
            ICollection<CourseRecurrenceTime> courseRecurrenceTimeList = new HashSet<CourseRecurrenceTime>();
            foreach (var item in RecurrenceTime.RecurrenceTimes)
            {
                if (item.IsChecked)
                {
                    courseRecurrenceTimeList.Add(new CourseRecurrenceTime
                    {
                        Day = item.Day,
                        StartTime = item.StartTime,
                        FinishTime = item.FinishTime
                    });
                }
            }

            // if weeksCount set finishdate as well
            if (FinishDateType == FinishDateType.NumberOfSession)
            {
                SeedFinishDate(); //startDate.AddDays((int)(WeeksCount * 7));
            }
            else if (FinishDateType == FinishDateType.UnSpecifiedEndDate)
            {
                FinishDate = null;
                HasProRatingPrice = false;
            }

            // convet coursse to xml it is beter to conver to derived class
            var customodelMetaData = new Models.FormModels.FormViewModel();


            //AA To Do Add form to course 
            var customFileds = new Models.FormModels.FormViewModel();
            //foreach (var item in TemplateFields)
            //{
            //    customFileds.Add(new Question()
            //    {
            //        SectionId = 1,
            //        Type = item.FieldType,
            //        Title = item.Title,
            //        Validations = new List<QuestionValidator>() { new QuestionValidator() { Type = item.ValidationType } }
            //    });
            //}

            customodelMetaData = customFileds;
            var customTempalte = SerializeHelper.Serialize(customodelMetaData).ToString();

            ClubLocation = Ioc.IClubLocationDb.GetClubLocation(DisplayClubLocationViewModel.SelectedClubLocationId.Value);

            DateTime? endDateOfEarlyBird = null;
            if (EarlyBirdDiscount.IsChecked)
            {
                endDateOfEarlyBird = EarlyBirdEndDate.Value;
            }

            // add all selected form to eventforms table
            var eventForms = AssignForm.SelectedForms.Select(item => new EventForms() { FormId = item }).ToList();

            Course course = new Course()
            {
                CustomTemplateMetaData = customTempalte,

                ItemId = ItemId,

                ClubDomain = ClubDomain,

                Name = Name,
                NameLong = NameLong,
                EventSearchField = new EventSearchField
                {
                    MinAge = EventSearchField.MinAge,
                    MaxAge = EventSearchField.MaxAge,
                    MinGrade = EventSearchField.MinGrade.HasValue && EventSearchField.MinGrade.Value != SchoolGradeType.None ? EventSearchField.MinGrade : null,
                    MaxGrade = EventSearchField.MaxGrade.HasValue && EventSearchField.MaxGrade.Value != SchoolGradeType.None ? EventSearchField.MaxGrade : null,
                    Level = EventSearchField.Level.HasValue && EventSearchField.Level.Value != Level.None ? EventSearchField.Level : null,
                    Gender = EventSearchField.Gender.HasValue ? EventSearchField.Gender : null,

                },

                HeadCoach = HeadCoach,
                AssistantCoaches = AssistantCoaches,
                Capacity = Capacity,
                Description = Utilities.GetDescription(Description),
                EventForms = eventForms,
                TimeZone = TimeZone,
                RecurrentStartDate = StartDate,
                RecurrentEndDate = FinishDate,
                WeeksCount = FinishDateType == FinishDateType.NumberOfSession ? WeeksCount : null,

                RecurrenceTime = courseRecurrenceTimeList,

                ChargeDiscounts = ChargeDiscounts,
                EarlybirdEndDate = endDateOfEarlyBird,

                Policies = Policies,

                AdditionalNotes = Utilities.GetDescription(AdditionalNotes),

                RegData = regdata,

                DisableOnlineRegistration = DisableOnlineRegistration,
                ExternalRegUrl = DisableOnlineRegistration ? ExternalRegUrl : string.Empty,

                FullPrice = FullPrice,
                SessionPrice = SessionPrice,
                HasSessionPrice = HasSessionPrice,
                HasProRatingPrice = HasProRatingPrice,
                Domain = (IsCreatedMode || FromTemplate) ? Utilities.GenerateSubDomainName(ClubDomain, Name) : Domain,

                EventStatus = EventStatus,
                MetaData = MetaData,
                CategoryId = Category.CategoryId,

                AsTemplate = AsTemplate,
                ClubLocation = ClubLocation,
                FlyerType = FlyerType,
                Room = Room,
                CourseType = CourseType,
                EventSearchTags = EventSearchTags,
                TemplateName = TemplateName,
                OutSourcerClubDomain = IsVendorEvent == EventSponserType.OutSourcer ? OutSourcerClubDomain : null,
                CaptureRestriction = CaptureRestriction,

                RegistrationPeriod = new RegistrationPeriod()
                    {
                        RegisterStartDate = RegistrationPeriod.RegisterStartDate,
                        RegisterEndDate = RegistrationPeriod.RegisterEndDate
                    }
            };

            course.CoachId = CoachId;
            course.AssistantCoachId = AssistantCoachId;

            course.ReservedCapacity = ReservedCapacity;


            // Donation
            course.Donation.Checked = Donation.Checked;
            if (!Donation.Checked)
            {
                // if donation is unchecked set description and title null
                course.Donation.Description = null;
                course.Donation.Title = null;
            }
            else
            {
                // if donation is checked set description and title to model value
                course.Donation.Description = Donation.Description;
                course.Donation.Title = Donation.Title;
            }

            course.TimeZone = TimeZoneHelper.GetTimeZone(ClubLocation.PostalAddress.Lat, ClubLocation.PostalAddress.Lng);

            if (PaymentPlan.IsChecked)
            {
                course.PaymentPlans.Add(PaymentPlan.ToPaymentPlan());
            }

            return course;
        }

        private void SeedOUtSourcers()
        {
            var outSourcers = Ioc.IClubVendorDb.GetSchoolsForClub(ClubId);
            OutSourcers = outSourcers.Select(m => new OutSourcerList
            {
                OutSourcerClubDomain = m.OutSourcerClubDomain,
                OutSourcerClubName = m.OutSourcerLocation.PostalAddress.Address + "(" + m.OutSourcerClubName + ")",
                OutSourcerLocation = m.OutSourcerLocation,

            }).ToList();
            OutSourcerSelectList = new SelectList(OutSourcers, "OutSourcerClubDomain", "OutSourcerClubName");

        }

        private void SeedFinishDate()
        {
            int numWeeks = 0;
            TotalSessions = 0;
            DateTime tmpStart = (DateTime)StartDate;
            DayOfWeek dayofWeekend = tmpStart.AddDays(-1).DayOfWeek;
            while (numWeeks < WeeksCount)
            {
                foreach (CourseRecurrenceTimeViewModel rec in RecurrenceTime.RecurrenceTimes)
                {
                    if (tmpStart.DayOfWeek.ToString().Equals(rec.Day.ToString()) && rec.IsChecked)
                    {
                        TotalSessions++;
                        break;
                    }
                }
                tmpStart = tmpStart.AddDays(1);
                if (tmpStart.DayOfWeek.Equals(dayofWeekend))
                {
                    numWeeks++;
                }
            }
            FinishDate = tmpStart;
        }

        private void SeedPolicies()
        {
            IStandardPolicyDb policyDb = Ioc.IStandardPolicyDbService;
            StandardPolicy[] policies = policyDb.Get();

            // Note: PolicyCategories start @ index 1, must reduce by one

            Refund = new PolicyViewModel() { Category = PolicyCategories.Refund, Name = policies[(int)PolicyCategories.Refund - 1].Name, Desc = policies[(int)PolicyCategories.Refund - 1].Description, IsChecked = false, Field1 = 14, Field2 = 0 };
            Waiver = new PolicyViewModel() { Category = PolicyCategories.Waiver, Name = policies[(int)PolicyCategories.Waiver - 1].Name, Desc = policies[(int)PolicyCategories.Waiver - 1].Description, IsChecked = false };
            MedicalRelease = new PolicyViewModel() { Category = PolicyCategories.MedicalRelease, Name = policies[(int)PolicyCategories.MedicalRelease - 1].Name, Desc = policies[(int)PolicyCategories.MedicalRelease - 1].Description, IsChecked = false };
        }

        private void SeedChargeDiscounts()
        {
            EarlyBirdDiscount = new ChargeDiscountViewModel(false, ChargeDiscountCategories.EarlyBird, null, Constants.M_Camp_CD_EarlyBird, ChargeDiscountType.Fixed);
            SiblingDiscount = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Sibling, null, Constants.M_Camp_CD_Sibling, ChargeDiscountType.Fixed);
            MembershipDiscount = new ChargeDiscountViewModel(false, ChargeDiscountCategories.ClubMembership, null, Constants.M_Camp_CD_ClubMembership, ChargeDiscountType.Fixed);
        }

        private void SeedRegDataOptions()
        {
            School = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.School, IsChecked = true };
            EmergencyContact = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.EmergencyContact, IsChecked = true };
            Insurance = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.Insurance, IsChecked = true };
            ParentGuardian = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.ParentGuardian, IsChecked = true };
            SkillLevel = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.SkillLevel, IsChecked = true };
            DoctorContact = new OptionalRegDataViewModel { Category = OptionalRegDataCategories.Doctor, IsChecked = true };
        }

        private void SeedCourseRecurrenceTime(ICollection<CourseRecurrenceTime> list)
        {
            //RecurrenceTime = new HashSet<CourseRecurrenceTimeViewModel>();
            RecurrenceTime = new MeetUpTimes();
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Sunday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Monday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Tuesday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Wednesday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Thursday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Friday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Saturday });

            // for the edit mode
            if (list != null)
            {
                foreach (var item in list)
                {
                    var recDb = RecurrenceTime.RecurrenceTimes.Single(p => p.Day == item.Day);
                    if (recDb != null)
                    {
                        RecurrenceTime.RecurrenceTimes.Remove(recDb);
                        RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = item.Day, FinishTime = item.FinishTime, StartTime = item.StartTime, IsChecked = true });
                    }
                }
            }

            RecurrenceTime.RecurrenceTimes = RecurrenceTime.RecurrenceTimes.OrderBy(c => c.Day).ToList();
        }

        private void SeedCategories(int categoryId = 0, int clubCategoryId = 0)
        {
            Category = new DisplayCategoryViewModel();
            // Load categories from db with a condition that check template existence in course category
            Category = new DisplayCategoryViewModel()
            {
                Categories = Ioc.ICategoryDbService.GetCategories(SubDomainCategories.Course),
                CategoryId = categoryId,
                ClubCategoryId = clubCategoryId
            };
        }

        public void SeedClubLocation()
        {
            var clubLocations = Ioc.IClubLocationDb.GetClubLocations(ClubDomain);
            DisplayClubLocationViewModel.ClubLocations = clubLocations;
        }


        private void SeedCapacity()
        {
            var capacityList = new List<SelectListItem>();
            for (int i = 1; i <= 1000; i++)
            {
                capacityList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            CapacityList = new SelectList(capacityList, "Value", "Text");
        }

        private void SeedAges()
        {
            var ageList = new List<SelectListItem>();
            for (int i = 1; i <= 120; i++)
            {
                ageList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            Ages = new SelectList(ageList, "Value", "Text");
        }


        public bool HasSession(DateTime start, DateTime end, List<CourseRecurrenceTimeViewModel> RecurrenceTime)
        {
            int totalSessions = 0;
            bool isOnProgress = start.CompareTo(DateTime.Now) < 0;

            DateTime tmpDate = DateTime.Now;
            bool isCounting = false;
            var recurences = RecurrenceTime.Where(c => c.IsChecked).ToList();
            while (!(start.CompareTo(end) > 0))
            {
                if (isOnProgress && start.Date.Equals(tmpDate.Date))
                {
                    isCounting = true;
                }

                foreach (CourseRecurrenceTimeViewModel recTime in recurences)
                {
                    if (start.DayOfWeek.ToString().Equals(recTime.Day.ToString()))
                    {
                        totalSessions++;

                        break;
                    }
                }

                start = start.AddDays(1);
                if (isCounting)
                {
                    tmpDate = tmpDate.AddDays(1);
                }
            }

            return totalSessions > 0;
        }
    }

    public class CourseRecurrenceTimeViewModel
    {
        [Required]
        public bool IsChecked { get; set; }

        public ScheduleDayOfWeek Day { get; set; }

        //Timespan not saved to xml correctly i ignored
        [XmlIgnore]
        [Display(Name = "From")]
        [DataType(DataType.Time)]
        public TimeSpan? StartTime { get; set; }

        //Timespan not saved to xml correctly i ignored
        [XmlIgnore]
        [Display(Name = "To")]
        [DataType(DataType.Time)]
        public TimeSpan? FinishTime { get; set; }

        //insted Of Saving StartTime Timespan whe save StartTimeTicks long
        public long StartTimeTicks
        {
            get
            {
                if (StartTime != null) return ((TimeSpan)StartTime).Ticks;
                return 0;
            }
            set
            {
                if (value != 0)
                {
                    StartTime = TimeSpan.FromTicks((long)value);
                }
                else
                {

                    StartTime = null;
                }

            }
        }

        //insted Of Saving FinishTime Timespan whe save FinishTimeTicks long
        public long FinishTimeTicks
        {
            get
            {
                if (FinishTime != null) return ((TimeSpan)FinishTime).Ticks;
                return 0;

            }
            set
            {
                if (value != 0)
                {
                    FinishTime = TimeSpan.FromTicks((long)value);
                }
                else
                {
                    FinishTime = null;
                }

            }
        }

        public CourseRecurrenceTimeViewModel()
        {
        }

        //public CourseRecurrenceTime ToCourceRecurrenceTime()
        //{
        //    return new CourseRecurrenceTime
        //    {
        //        Day = Day,
        //        StartTime = StartTime,
        //        FinishTime = FinishTime
        //    };
        //}
    }

    public class TimeSlotViewModel
    {
        public int Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public TimeSpan StartTime { get; set; }

        [Required]
        public TimeSpan FinishTime { get; set; }

    }

    public class CourseRegistrationViewModel
    {
        public string Name { get; set; }

        public string Domain { get; set; }

        public string ClubDomain { get; set; }

        public bool HasSessionPrice { get; set; }

        public int orderId { get; set; }

        public CoursePageViewModel Course { get; set; }

        public int ProfileId { get; set; }

        public PlayerProfileComplitionFormContactViewModel Contact { get; set; }
        public PlayerProfileComplitionFormSkillViewModel Skill { get; set; }
        public PlayerProfileComplitionFormDoctorContactViewModel Doctor { get; set; }
        public PlayerProfileSchoolViewModel School { get; set; }
        public PlayerProfileComplitionFormEmergencyContactViewModel EmergencyContact { get; set; }
        public DonationViewModel Donation { get; set; }

        public List<ChargeDiscountViewModel> MandetoryCharges { get; set; }
        public List<OptionalChargeDiscountViewModel> OptionalCharges { get; set; }
        public List<ChargeDiscountViewModel> CustomDiscounts { get; set; }


        public IEnumerable<SelectListItem> AllTeachers { get; set; }
        [Display(Name = "Teacher Name")]
        public int? TeacherId { get; set; }
        public OraganizationTypes OraganizationType { get; set; }

        public PlayerProfile Player { get; set; }

        public string PrimaryPlayerEmail { get; set; }

        public DateTime? EarlybirdEndDate { get; set; }
        public bool IsEearlybirdExpired { get { return DateTime.Now > EarlybirdEndDate; } }
        public ChargeDiscountViewModel EarlybirdDiscount { get; set; }

        [DisplayName("Sibling discount")]
        public SiblingViewModel SiblingDiscount { get; set; }

        [DisplayName("Club membership discount")]
        public MembershipViewModel MembershipDiscount { get; set; }

        public PolicyViewModel Refund { get; set; }
        public PolicyViewModel Waiver { get; set; }
        public PolicyViewModel MedicalRelease { get; set; }

        [Required]
        public RegistrationType RegistrationType { get; set; }

        public string OutSourcerClubDomain { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                var cartDb = Ioc.ICartDbService;
                string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                if (!string.IsNullOrEmpty(currentDomain))
                {
                    return !(currentDomain == this.ClubDomain);
                }

                return false;
            }
        }

        public decimal? FullPrice { get; set; }
        public decimal? SessionPrice { get; set; }
     
        public int? NumOfSessionLeft { get; set; }
        public int TotalSessions { get; set; }
        public int CategoryId { get; set; }
        public string PlayerImageUrl { get; set; }
     
        public bool IsAdultLoggedInUser { get; set; }
        public bool ParentRequired { get; set; }

       
        [Display(Name = "Registering for")]
        public int SelectedProfileId { get; set; }

        public IEnumerable<SelectListItem> PlayerLists { get; set; }

        public Nullable<DateTime> DropInSessionDate { get; set; }

        public Models.FormModels.FormViewModel JumbulaAdminForm { get; set; }

        public Models.FormModels.FormViewModel ClubAdminForm { get; set; }

        public AddPlayerProfileViewModel Parent { get; set; }

        public bool IsRestrictedAge
        {
            get
            {
                
                if (Course.Course.CaptureRestriction)
                {
                   
                    int age = Utilities.CalculateAge(Player.Contact.DoB.Value, Course.Course.StartDate);

                    if ((Course.Course.EventSearchField.MinAge.HasValue && age < Course.Course.EventSearchField.MinAge) || (Course.Course.EventSearchField.MaxAge.HasValue && age > Course.Course.EventSearchField.MaxAge))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public bool IsRestrictedGrade { get; set; }

        public string AgeRestrictionMessage
        {
            get
            {
                if (Course.Course.EventSearchField.MinAge.HasValue)
                {
                    if (Course.Course.EventSearchField.MaxAge.HasValue)
                    {
                        return string.Format(Constants.Age_Restriction_Message, Course.Course.EventSearchField.MinAge, Course.Course.EventSearchField.MaxAge);
                    }

                    return string.Format(Constants.Min_Age_Restriction_Message, Course.Course.EventSearchField.MinAge);
                }

                if (Course.Course.EventSearchField.MaxAge.HasValue)
                {
                    return string.Format(Constants.Max_Age_Restriction_Message, Course.Course.EventSearchField.MaxAge);
                }

                return string.Empty;
            }
        }

        public PaymentPlanViewModel PaymentPlan { get; set; }

        public bool IsRestrictedUser { get; set; }

        public string GradeRestrictionMessage
        {
            get
            {
                if (Course.Course.EventSearchField.MinGrade.HasValue)
                {

                    if (Course.Course.EventSearchField.MaxGrade.HasValue)
                    {
                        return string.Format(Constants.Grade_Restriction_Message, Course.Course.EventSearchField.MinGrade.Value.ToDescription(), Course.Course.EventSearchField.MaxGrade.Value.ToDescription());
                    }
                    return string.Format(Constants.Min_Grade_Restriction_Message, Course.Course.EventSearchField.MinGrade.Value.ToDescription());
                }
                if (Course.Course.EventSearchField.MaxGrade.HasValue)
                {
                    return string.Format(Constants.Max_Grade_Restriction_Message, Course.Course.EventSearchField.MaxGrade.Value.ToDescription());
                }

                return string.Empty;
            }
        }

      
        public bool CheckGradeRestriction(SchoolGradeType? eventMinGrade, SchoolGradeType? eventMaxGrade, SchoolGradeType? playerGrade, bool isAdult)
        {
          
            if (Course.Course.CaptureRestriction && playerGrade.HasValue && playerGrade != SchoolGradeType.None && !isAdult)
            {
                if ((eventMinGrade.HasValue && eventMinGrade != SchoolGradeType.None && playerGrade < eventMinGrade) || (eventMinGrade.HasValue && eventMaxGrade != SchoolGradeType.None && playerGrade > eventMaxGrade))
                {
                    return true;
                }
            }

            return false;
        }

      
        public CourseRegistrationViewModel()
        {
            init();
            JumbulaAdminForm = new Models.FormModels.FormViewModel();
            ClubAdminForm = new Models.FormModels.FormViewModel();
            Donation = new DonationViewModel();
            PaymentPlan = new PaymentPlanViewModel();
            MandetoryCharges = new List<ChargeDiscountViewModel>();
            OptionalCharges = new List<OptionalChargeDiscountViewModel>();
        }

        // auto property assignment mode
        public CourseRegistrationViewModel(Course course, bool newMode = true)
            : this()
        {
            // indecate logged in user is adult or not
            var loggedUserContact = Ioc.IPlayerProfileDbService.GetByUserId(TheMembership.GetCurrentUserId())
                                            .Single(x => x.Type == PlayerProfileType.Primary)
                                            .Contact;
            IsAdultLoggedInUser = Utilities.CalculateAge(loggedUserContact.DoB.Value) >= Constants.PlayerProfile_Min_Age;

            // load custom question
            var categoryQuestionMetadata = Ioc.ICategoryDbService.GetTemplate(SubDomainCategories.Course, course.CategoryId).CustomFields;
            if (!string.IsNullOrEmpty(categoryQuestionMetadata))
            {
                JumbulaAdminForm = SerializeHelper.Deserialize<Models.FormModels.FormViewModel>(categoryQuestionMetadata);
            }

            var adminQuestionMetadata = Ioc.ICourseDbService.GetEventQuestionMetadata(course.Domain, course.ClubDomain);
            if (!string.IsNullOrEmpty(adminQuestionMetadata))
            {
                ClubAdminForm = SerializeHelper.Deserialize<Models.FormModels.FormViewModel>(adminQuestionMetadata);
            }

            //
            Donation = new DonationViewModel(course.Donation);

            // load mandetory charges
            MandetoryCharges = course.MandetoryCharges.Select(p => new ChargeDiscountViewModel(p, true)).ToList();

            // 
            OptionalCharges = course.OptionalCharges.Select(p => new OptionalChargeDiscountViewModel(p, true)).ToList();

            CustomDiscounts = course.ChargeDiscounts.Where(c => c.Category == ChargeDiscountCategories.Custom).Select(p => new ChargeDiscountViewModel(p, false)).ToList();

            Name = course.Name;
            HasSessionPrice = course.HasSessionPrice;

            TotalSessions = Ioc.ICourseDbService.CalculateTotalSessions((DateTime)course.RecurrentStartDate.Value, course.RecurrentEndDate, course.RecurrenceTime);
            NumOfSessionLeft = Ioc.ICourseDbService.CalculateLeftSessions((DateTime)course.RecurrentStartDate.Value, course.RecurrentEndDate, course.RecurrenceTime);


            SeedFullPrice(course.HasProRatingPrice, (decimal)course.FullPrice);
            SessionPrice = course.SessionPrice;
            EarlybirdEndDate = course.EarlybirdEndDate;

            this.Course = new CoursePageViewModel(course);

            var currentUser = TheMembership.GetCurrentUserName();
            PaymentPlan = new PaymentPlanViewModel(course.PaymentPlans.FirstOrDefault(), course.ClubDomain);
            IsRestrictedUser = PaymentPlan.PaymentPlanType == PaymentPlanType.Installment && (PaymentPlan.RestrictedType == RestrictedType.AllUser || PaymentPlan.SelectedUsers.Contains(currentUser));

            if (newMode)
            {
                ChargeDiscount temp = course.ChargeDiscounts.FirstOrDefault(d => d.Category == ChargeDiscountCategories.Sibling);

                if (temp != null)
                {
                    SiblingDiscount = new SiblingViewModel()
                    {
                        Amount = (int)temp.Amount,
                        Category = ChargeDiscountCategories.Sibling,
                        ChargeDiscountType = temp.ChargeDiscountType,
                        Id=temp.Id
                    };
                }

                temp = course.ChargeDiscounts.FirstOrDefault(d => d.Category == ChargeDiscountCategories.ClubMembership);

                if (temp != null)
                {
                    MembershipDiscount = new MembershipViewModel()
                    {
                        Amount = (int)temp.Amount,
                        Category = ChargeDiscountCategories.ClubMembership,
                        ChargeDiscountType = temp.ChargeDiscountType,
                        Id = temp.Id

                    };
                }
            }

            // check the expiration date of eb discount
            if (EarlybirdEndDate.HasValue && EarlybirdEndDate.Value >= DateTime.Today)
            {
                ChargeDiscount temp = course.ChargeDiscounts.FirstOrDefault(d => d.Category == ChargeDiscountCategories.EarlyBird);

                if (temp != null)
                {
                    EarlybirdDiscount = new ChargeDiscountViewModel()
                    {
                        Amount = (int)temp.Amount,
                        Category = ChargeDiscountCategories.EarlyBird,
                        IsChecked = true,
                        Id=temp.Id
                    };
                }
            }

            SeedPolicies();

            foreach (Policy item in course.Policies)
            {
                switch (item.Category)
                {
                    case PolicyCategories.Refund:
                        Refund.Set(item);
                        break;

                    case PolicyCategories.Waiver:
                        Waiver.Set(item);
                        break;

                    case PolicyCategories.MedicalRelease:
                        MedicalRelease.Set(item);
                        break;

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_PolicyCategoryNotSupported, item.Category));
                }
            }
        }

        void init()
        {
            //SkillLevel = null;
            //School = null;
            //ParentGuardian = null;
            //EmergencyContact = null;
            //Insurance = null;
            //DoctorContact = null;

            MembershipDiscount = null;
            SiblingDiscount = null;
        }

        private void SeedPolicies()
        {
            IStandardPolicyDb policyDb = Ioc.IStandardPolicyDbService;
            StandardPolicy[] policies = policyDb.Get();

            // Note: PolicyCategories start @ index 1, must reduce by one

            Refund = new PolicyViewModel() { Category = PolicyCategories.Refund, Name = policies[(int)PolicyCategories.Refund - 1].Name, Desc = policies[(int)PolicyCategories.Refund - 1].Description, IsChecked = false };
            Waiver = new PolicyViewModel() { Category = PolicyCategories.Waiver, Name = policies[(int)PolicyCategories.Waiver - 1].Name, Desc = policies[(int)PolicyCategories.Waiver - 1].Description, IsChecked = false };
            MedicalRelease = new PolicyViewModel() { Category = PolicyCategories.MedicalRelease, Name = policies[(int)PolicyCategories.MedicalRelease - 1].Name, Desc = policies[(int)PolicyCategories.MedicalRelease - 1].Description, IsChecked = false };
        }

        private void SeedChargeDiscounts()
        {
            EarlybirdDiscount = new ChargeDiscountViewModel(false, ChargeDiscountCategories.EarlyBird, null, Constants.M_Camp_CD_EarlyBird, ChargeDiscountType.Fixed);
            SiblingDiscount = new SiblingViewModel(new ChargeDiscountViewModel(false, ChargeDiscountCategories.Sibling, null, Constants.M_Camp_CD_Sibling, ChargeDiscountType.Fixed), false);
            MembershipDiscount = new MembershipViewModel(new ChargeDiscountViewModel(false, ChargeDiscountCategories.ClubMembership, null, Constants.M_Camp_CD_ClubMembership, ChargeDiscountType.Fixed), false);
        }

        private void SeedFullPrice(bool HasProRate, decimal price)
        {
            if (HasProRate && NumOfSessionLeft.HasValue)
            {
                FullPrice = Math.Round((price * NumOfSessionLeft.Value) / TotalSessions);
            }
            else
            {
                FullPrice = price;
            }
        }
    }

    public class CourseRegistrationContactPersonViewModel
    {
        [Required]
        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Home phone")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876)")]
        public string Phone { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Cell phone")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876)")]
        public string Cell { get; set; }

        [Phone]
        [DisplayName("Work phone")]
        [StringLength(64)]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876)")]
        public string Work { get; set; }

        public CourseRegistrationContactPersonViewModel()
        {
        }

        public CourseRegistrationContactPersonViewModel(ContactPerson model)
        {
            FirstName = model.FirstName;
            LastName = model.LastName;
            Email = model.Email;
            Phone = model.Phone;
            Cell = model.Cell;
            Work = model.Work;
        }

        public ContactPerson ToContactPerson()
        {
            return new ContactPerson { FirstName = FirstName, LastName = LastName, Email = Email, Phone = Phone, Cell = Cell, Work = Work };
        }
    }

    [Serializable]
    public class MeetUpTimes
    {
        public MeetUpTimes()
        {
            RecurrenceTimes = new List<CourseRecurrenceTimeViewModel>();
        }

        public List<CourseRecurrenceTimeViewModel> RecurrenceTimes { get; set; }

        [MaxLength(ErrorMessage = "Description should be 100 character maximum")]
        public string Description { get; set; }
    }
}