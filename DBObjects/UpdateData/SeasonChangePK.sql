
drop index IX_SeasonId
on  [dbo].[Events]

drop index IX_SeasonId
on  [dbo].[EventRoasters]

drop index IX_SeasonId
on  [dbo].[Coupons]

alter table  [dbo].[EventRoasters]
drop  [FK_dbo.EventRoasters_dbo.Seasons_SeasonId]

alter table  [dbo].[Coupons]
drop  [FK_dbo.Coupons_dbo.Seasons_SeasonId]

alter table  [dbo].[Events]
drop  [FK_dbo.Events_dbo.Seasons_SeasonId]

alter table [dbo].[Seasons]
drop CONSTRAINT  [PK_dbo.Seasons]