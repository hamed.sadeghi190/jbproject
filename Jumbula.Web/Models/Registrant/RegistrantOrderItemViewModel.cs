﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using MailChimp.Ecomm;


namespace SportsClub.Models
{
    public class RegistrantOrderItemViewModel
    {
        public long Id { get; set; }

        public string AttendeeName { get; set; }

        public string ProegramName { get; set; }

        public DateTime OrderDate { get; set; }

        public string OrderAmount { get; set; }
        public string PaidAmount { get; set; }
        public string EventDomain { get; set; }

        public string TuitionName { get; set; }

        public bool IsDonation { get; set; }
        public DateTime StartDate { get; set; }

        public OrderItemStatusReasons ItemStatusReasons { get; set; }

        public Int16 OrderItemStatus => (Int16)ItemStatusReasons;

        public string Confirmation { get; set; }

        public CurrencyCodes Currency { get; set; }
        public string ClubDomain { get; set; }

        public List<OrderInstallment> Installments { get; set; }


    }
}