﻿using System;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using Jumbula.Core.Model.Payment;
using Stripe;

namespace Jumbula.Core.Business
{
    public interface IPaymentBusiness
    {
        JbPaymentResult ChargeInstallmentNew(OrderInstallment installment,Club club,DateTime? dueDate);

        RefundResultModel Refund(TransactionActivity transactionActivity, List<OrderInstallment> orderInstallments, OrderItem orderItem, string clubDomain, decimal refundAmount, bool isTestMode);

        PaymentDetailModel GetPaymentDetails(string clubDomain, string payKey, IEnumerable<TransactionActivity> oldRefunded, bool isPaypalTransaction, bool isTestMode);
    }
}
