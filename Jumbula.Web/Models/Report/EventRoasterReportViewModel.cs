﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Jb.Framework.Web.Model;
using SportsClub.Domain;
using SportsClub.Infrastructure.Helper;
using SportsClub.Infrastructure.Lucene;
using Jb.Framework.Common.Forms;
using SportsClub.Db;

namespace SportsClub.Models
{
    public class EventRoasterReportViewModel : BaseViewModel<EventRoaster>
    {
        #region Field&Properties

        private ILuceneHelper _luceneHelper;

        public int Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public long? SeasonId { get; set; }

        public bool ForOldEvent { get; set; }

        public EventRoasterType Type { get; set; }

        public IEnumerable<SelectListItem> AllEvents { get; set; }
        private List<SportsClub.Domain.Event> Events { get; set; }

        [Required]
        public List<string> SelectedEvents { get; set; }
        public JbForm JbForm { get; set; }

        #endregion

        #region Constructor
        public EventRoasterReportViewModel()
        {
            JbForm = new JbForm();
            ForOldEvent = false;
        }
        private EventRoasterReportViewModel(EventRoasterType roasterType)
            : this()
        {
            Type = roasterType;
        }
        public EventRoasterReportViewModel(string clubDomain, long? seasonId)
            : this(EventRoasterType.PerSeason)
        {
            SeasonId = seasonId;
            SeedData(clubDomain);
            GenerateJbForm();
        }
        public EventRoasterReportViewModel(string clubDomain, string eventDomain, long? seasonId)
            : this(EventRoasterType.PerEvent)
        {
            SeedData(clubDomain, eventDomain);
            GenerateJbForm();
        }
        public EventRoasterReportViewModel(string domain, bool oldEvents)
            : this(EventRoasterType.OldEvents)
        {
            ForOldEvent = oldEvents;
            SeedData(domain);
            GenerateJbForm();
        }
        //public EventRoasterReportViewModel(EventRoaster eventRoaster, string domain, int? seasonId)
        //    : this(domain, seasonId)
        //{

        //}
        #endregion

        private void SeedData(string domain)
        {
            var events = Ioc.ILuceneHelperService.GetAllEvents(domain, false).ToList();
            if (ForOldEvent)
            {
                var dbEvents = Ioc.IEventService.GetAll().Where(e => e.EventType != EventTypeCategory.Camp).Select(e => e.Domain).ToList();
                AllEvents = events.Where(e => !dbEvents.Contains(e.Domain)).Select(e =>
                                            new System.Web.Mvc.SelectListItem()
                                                {
                                                    Value = e.Domain,
                                                    Text = e.Name
                                                }).ToList();
            }
            else
            {
                Events = Ioc.IEventService.GetAll()
                                             .Where(e => e.EventStatus != EventStatus.Deleted && events.Any(v => v.Domain == e.Domain) && e.JbForm_Id.HasValue).ToList();
                AllEvents = Events.Select(e =>
                                               new System.Web.Mvc.SelectListItem()
                                               {
                                                   Value = e.Domain,
                                                   Text = e.Name
                                               }).ToList();//&& e.JbForm_Id.HasValue
            }
        }

        private void SeedData(string clubdomain, string eventDomain)
        {
            var eventdb = Ioc.IEventService.Get(clubdomain, eventDomain, false);
            Events = new List<Domain.Event>();
            Events.Add(eventdb);
        }
        private void GenerateJbForm()
        {
            var newform = new JbForm();
            if (ForOldEvent)
            {
                new PlayerProfileDb().ProfileSectionsGenerator_ForOldEvent(newform);

                var otherInfo = new JbSection() { Name = "Other information", Title = "Other information" };
                otherInfo.Elements.Add(new JbTextArea() { Name = "Prior or current musical training, instrument, name of teacher", Title = "Prior or current musical training, instrument, name of teacher" });
                otherInfo.Elements.Add(new JbTextBox() { Name = "Comments", Title = "Comments" });
                newform.Elements.Add(otherInfo);
            }
            else
            {
                var jbForms = Events.Where(e => e.JbForm_Id.HasValue).Select(e => e.JbForm).ToList();
                foreach (var form in jbForms)
                {
                    try
                    {
                        form.CopyByTitle(newform);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            var lastSection = new JbSection();
            foreach (var element in newform.Elements)
            {
                if (element is JbSection)
                {
                    lastSection = new JbSection() { Name = element.Name, Title = element.Title };
                    JbForm.Elements.Add(lastSection);
                    foreach (var element2 in (element as JbSection).Elements.Where(e => !(e is JbHidden)))
                    {
                        var checkbox = new JbCheckBox() { ElementId = element2.ElementId, Name = element2.Name, Title = element2.Title };
                        lastSection.Elements.Add(checkbox);
                    }
                }
                else
                {
                    var checkbox = new JbCheckBox();
                    Jb.Framework.Helper.AutoMapper.Map(element, ref checkbox);
                    (lastSection as JbSection).Elements.Add(checkbox);
                }
            }
        }

        //private void CopyAny_Temp(JbForm sourceForm, JbForm destForm)
        //{
        //    foreach (var rootElement in sourceForm.Elements)
        //    {
        //        if (rootElement is JbSection)
        //        {
        //            if (destForm.Elements.Any(e => e != null && e.Equals(rootElement)))
        //            {
        //                var section = destForm.Elements.First(e => e != null && e.Equals(rootElement)) as JbSection;
        //                foreach (var selemnt in (rootElement as JbSection).Elements)
        //                {
        //                    var element = section.Elements.SingleOrDefault(e => e != null && e.Equals(rootElement));
        //                    if (element == null)
        //                        section.Elements.Add(selemnt);
        //                    else
        //                        element = selemnt;
        //                }
        //            }
        //            else
        //                destForm.Elements.Add(rootElement);
        //        }
        //    }
        //}
    }
}