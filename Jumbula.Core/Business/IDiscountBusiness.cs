﻿using Jb.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IDiscountBusiness
    {
        IQueryable<Discount> GetList(long seasonId, bool isDeleted = false);
        IQueryable<Discount> GetList(List<long> seasonIds, bool isDeleted);
        Discount Get(long discountId);
        OperationStatus Create(Discount discount);
        OperationStatus Update(Discount discount);
        OperationStatus Delete(long discountId);
        void CalculateDiscount(Order order, DiscountMode discountMode);
        IEnumerable<Discount> GetProgramsDiscounts(IEnumerable<Program> selectedProgram);
        IEnumerable<Discount> GetProgramsDiscounts(Program program);
        List<string> GetAllSeasonDiscountCoupon(long seasonId);
        List<string> GetAllSeasonPartnerDiscountCoupon(int partnerId);
        List<string> GetAllSeasonClubDiscountCoupon(int clubId);
        OperationStatus CopyInSeason(long discountId, long seasonId, int clubId, string newSeasonDomain);
    }
}
