﻿(function () {
    'use strict';

    angular.module('jbTools').service("seasonSettingsService", [function () {

        var _model;

        var isCopySeason = false;

        function setFormsAndWaiversModel(data) {
            _model = data;
        }

        function getFormsAndWaiversModel() {
            return _model;
        }

        return {
            setFormsAndWaivers: setFormsAndWaiversModel,
            getFormsAndWaivers: getFormsAndWaiversModel,
        }
    }]);
})();