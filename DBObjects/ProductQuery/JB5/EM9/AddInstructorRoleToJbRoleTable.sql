-- Add instructor role to role table if not exist

   IF NOT EXISTS (SELECT * FROM JbRoles 
                   WHERE [Name] = 'Instructor')
   BEGIN
      INSERT INTO JbRoles([Name])
       VALUES ('Instructor')
   END
