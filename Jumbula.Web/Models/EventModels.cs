﻿using System.CodeDom;
using Jb.Framework.Common.Forms;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using NPOI.SS.Formula.Functions;
using SportsClub.Db;
using SportsClub.Domain;
using SportsClub.Infrastructure;
using SportsClub.Infrastructure.Helper;
using SportsClub.Infrastructure.JBMembership;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsClub.Models.EventModels;
using WebMatrix.WebData;
using SportsClub.Filters;
using SportsClub.Models.FormModels;
using System.Diagnostics;
using Jb.Framework.Web.Model;

namespace SportsClub.Models
{
    public class EventCreateEditModel : BaseViewModel<Domain.Event>
    {
        public int Id { get; set; }

        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        [DisplayName("Event category")]
        public DisplayCategoryViewModel Category { get; set; }

        [Required]
        [DisplayName("Event name")]
        [MaxLength(100, ErrorMessage = "{0} is too long")]
        [MinLength(3, ErrorMessage = "Enter a complete {0}")]
        public string Name { get; set; }

        [AllowHtml]
        [DisplayName("Class description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string Note { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public EventTypeCategory EventType { get; set; }

        public EventStatus EventStatus { get; set; }

        public Domain.TimeZone TimeZone { get; set; }

        public int ClubId { get; set; }

        public int CategoryId { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        public EventSearchTags EventSearchTags { get; set; }

        public EventSearchField EventSearchField { get; set; }

        public string FileName { get; set; }

        public string OutSourcerClubDomain { get; set; }

        public FlyerType FlyerType { get; set; }

        public Domain.Calendar Calendar { get; set; }

        public EventSchedule EventSchedule { get; set; }

        [DefaultValue(EventPageStep.BasicInformation)]
        public EventPageStep CurrentStep { get; set; }

        public DisplayClubLocationViewModel ClubLocation { get; set; }
        public int ClubLocationId { get; set; }

        public IEnumerable<OutSourcerList> OutSourcers { get; set; }
        public SelectList OutSourcerSelectList { get; set; }
        public int OutSourcerClubLocationId { get; set; }

        public PageMode PageMode { get; set; }

        public SaveType SaveType { get; set; }

        public bool IsCreated { get; set; }

        public SelectList Ages { get; set; }

        [DisplayName("Is this an outsourcer event for a school or club?")]
        public EventSponserType EventSponserType { get; set; }

        public bool CaptureRestriction { get; set; }

        [Required(ErrorMessage = "You must check the box indicating that you will review the generated class and registration page.")]
        [DisplayName("I understand that I should double check the system generated class and registration page.")]
        public bool AcceptLicenseAgreement { get; set; }

        public bool HasSchedule { get; set; }
        public JbForm JbForm { get; set; }


        public Domain.Event ToEvent()
        {
            var result = new Domain.Event();

            result.CategoryId = CategoryId;
            result.ClubDomain = ClubDomain;
            result.Domain = Domain;
            result.ClubLocationId = ClubLocationId;
            result.Description = Description;
            result.EventStatus = EventStatus;
            result.EventType = EventType;
            result.Name = Name;
            result.Note = Note;
            result.TimeZone = TimeZone;
            result.JbForm = this.JbForm;
            var eventAttribute = new CalendarAttribute()
            {
                Calendar = Calendar
            };

            result.EventAttributesSerialized = JsonConvert.SerializeObject(eventAttribute, Formatting.None);

            result.EventMetaData = new EventMetaData()
            {
                DateCreated = DateCreated,

                EventSearchTags = new EventSearchTags(),
                FileName = FileName,
                FlyerType = FlyerType,
                CaptureRestriction = CaptureRestriction,

                RegistrationPeriod = new RegistrationPeriod()
                {
                    RegisterStartDate = RegistrationPeriod.RegisterStartDate,
                    RegisterEndDate = RegistrationPeriod.RegisterEndDate
                },

                EventSearchField = EventSearchField = new EventSearchField
                {
                    MinAge = EventSearchField.MinAge,
                    MaxAge = EventSearchField.MaxAge,
                    MinGrade = EventSearchField.MinGrade.HasValue && EventSearchField.MinGrade.Value != SchoolGradeType.None ? EventSearchField.MinGrade : null,
                    MaxGrade = EventSearchField.MaxGrade.HasValue && EventSearchField.MaxGrade.Value != SchoolGradeType.None ? EventSearchField.MaxGrade : null,
                    Level = EventSearchField.Level.HasValue && EventSearchField.Level.Value != Level.None ? EventSearchField.Level : null,
                    Gender = EventSearchField.Gender.HasValue ? EventSearchField.Gender : null,
                },
            };

            if (PageMode == Models.PageMode.Edit)
            {
                DateUpdated = DateTime.Now;
            }

            result.CategoryId = Category.CategoryId;

            return result;
        }

        #region Constructors
        public EventCreateEditModel()
        {
            //ClubLocation = new ClubLocation();
            EventSearchField = new EventSearchField();
            RegistrationPeriod = new RegistrationPeriodViewModel();
            OutSourcers = new List<OutSourcerList>();
            SeedOutSourcers();
            SeedAges();
        }

        public EventCreateEditModel(string clubDomain, EventTypeCategory eventType, PageMode pageMode, SaveType saveType, bool isVendor, int clubCategoryId)
            : this()
        {
            ClubLocation = new DisplayClubLocationViewModel(clubDomain);
            SeedCategories(clubCategoryId: clubCategoryId);
            ClubLocation.SeedClubLocation();
            PageMode = pageMode;
            SaveType = saveType;
            EventSponserType = isVendor ? SportsClub.Domain.EventSponserType.Vendor : SportsClub.Domain.EventSponserType.OutSourcer;
        }

        public EventCreateEditModel(Domain.Event newEvent)
            : this()
        {
            this.Calendar = new Domain.Calendar();
            this.CategoryId = newEvent.CategoryId;
            this.ClubDomain = newEvent.ClubDomain;
            this.DateCreated = newEvent.EventMetaData.DateCreated;
            this.DateUpdated = newEvent.EventMetaData.DateUpdated;
            this.Description = newEvent.Description;
            this.Domain = newEvent.Domain;
            this.EventSchedule = new EventSchedule();
            this.EventSearchField = newEvent.EventMetaData.EventSearchField;
            this.EventSearchTags = newEvent.EventMetaData.EventSearchTags;
            //this.EventSponserType 
            this.EventStatus = newEvent.EventStatus;
            this.EventType = newEvent.EventType;
            this.FileName = newEvent.EventMetaData.FileName;
            this.FlyerType = newEvent.EventMetaData.FlyerType;
            this.HasSchedule = true;
            this.Name = newEvent.Name;
            this.Note = newEvent.Note;
            this.PageMode = Models.PageMode.Edit;
            this.RegistrationPeriod = new RegistrationPeriodViewModel(newEvent.EventMetaData.RegistrationPeriod.RegisterStartDate.Value, newEvent.EventMetaData.RegistrationPeriod.RegisterEndDate.Value);
            this.TimeZone = newEvent.TimeZone;
            this.CaptureRestriction = newEvent.EventMetaData.CaptureRestriction;
            this.JbForm = newEvent.JbForm;
            this.CategoryId = newEvent.CategoryId;
            this.ClubLocation = new DisplayClubLocationViewModel(newEvent.ClubDomain)
            {
                SelectedClubLocationId = newEvent.ClubLocationId,
            };

            this.ClubLocation.SeedClubLocation();

            SeedCategories(categoryId: newEvent.CategoryId);
        }

        #endregion

        #region Internals

        private void SeedCategories(int categoryId = 0, int clubCategoryId = 0)
        {
            Category = new DisplayCategoryViewModel();
            // Load categories from db with a condition that check template existence in course category
            Category = new DisplayCategoryViewModel()
            {
                Categories = Ioc.ICategoryDbService.GetCategories(SubDomainCategories.Course),
                CategoryId = categoryId,
                ClubCategoryId = clubCategoryId
            };
        }

        private void SeedAges()
        {
            var ageList = new List<SelectListItem>();
            for (int i = 1; i <= 120; i++)
            {
                ageList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            Ages = new SelectList(ageList, "Value", "Text");
        }

        private void SeedOutSourcers()
        {
            var outSourcers = Ioc.IClubVendorDb.GetSchoolsForClub(ClubId);
            OutSourcers = outSourcers.Select(m => new OutSourcerList
            {
                OutSourcerClubDomain = m.OutSourcerClubDomain,
                OutSourcerClubName = m.OutSourcerLocation.PostalAddress.Address + "(" + m.OutSourcerClubName + ")",
                OutSourcerLocation = m.OutSourcerLocation,

            }).ToList();
            OutSourcerSelectList = new SelectList(OutSourcers, "OutSourcerClubDomain", "OutSourcerClubName");
        }

        #endregion
    }

    public class EventBaseCreateEditModel : BaseViewModel<Domain.Event>
    {
        public int Id { get; set; }

        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        public EventTypeCategory EventType { get; set; }

        public EventStatus EventStatus { get; set; }

        public WizardStep WizardStep { get; set; }
        public JbForm JbForm { get; set; }

        public T ConvertToChild<T>() where T : EventBaseCreateEditModel, new()
        {
            EventBaseCreateEditModel model = new T();

            model.ClubDomain = this.ClubDomain;
            model.Domain = this.Domain;
            model.EventStatus = this.EventStatus;
            model.EventType = this.EventType;
            model.Id = this.Id;
            model.WizardStep = this.WizardStep;

            return (T)model;
        }

        #region Constructors
        public EventBaseCreateEditModel()
        {
            ClubDomain = JBMembership.GetActiveClubBaseInfo().Domain;
        }

        public EventBaseCreateEditModel(string domain)
            : this()
        {
            Domain = domain;
        }
        #endregion
    }

    public class EventBasicInformationEditModel : EventBaseCreateEditModel
    {
        [DisplayName("Event category")]
        public DisplayCategoryViewModel Category { get; set; }

        [Required]
        [DisplayName("Event name")]
        [MaxLength(100, ErrorMessage = "{0} is too long")]
        [MinLength(3, ErrorMessage = "Enter a complete {0}")]
        public string Name { get; set; }

        [MaxLength(255, ErrorMessage = "{0} is too long")]
        public string HeadInstructor { get; set; }

        public List<string> AssistantInstructors { get; set; }

        public string HeadInstructorLabel { get; set; }

        public string AssistantInstructorsLabel { get; set; }

        public List<SelectListItem> AllInstructors { get; set; }

        [AllowHtml]
        [DisplayName("Class description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string Note { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public Domain.TimeZone TimeZone { get; set; }

        public int ClubId { get; set; }

        public int CategoryId { get; set; }

        public EventSearchTags EventSearchTags { get; set; }

        public EventSearchField EventSearchField { get; set; }

        public string FileName { get; set; }

        public string OutSourcerClubDomain { get; set; }

        public FlyerType FlyerType { get; set; }

        public DisplayClubLocationViewModel ClubLocation { get; set; }
        public int ClubLocationId { get; set; }

        public IEnumerable<OutSourcerList> OutSourcers { get; set; }
        public SelectList OutSourcerSelectList { get; set; }
        public int OutSourcerClubLocationId { get; set; }

        public bool CaptureRestriction { get; set; }

        public SelectList Ages { get; set; }

        [DisplayName("Is this an outsourcer event for a school or club?")]
        public EventSponserType EventSponserType { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        public Domain.Event ToEvent()
        {
            var result = new Domain.Event();

            result.CategoryId = CategoryId;
            result.ClubDomain = ClubDomain;
            result.Domain = Domain;
            result.ClubLocationId = ClubLocationId;
            result.Description = Description;
            result.EventStatus = EventStatus;
            result.EventType = EventType;
            result.Name = Name;
            result.Note = Note;
            result.TimeZone = TimeZone;

            result.EventMetaData = new EventMetaData()
            {
                DateCreated = DateCreated,

                EventSearchTags = new EventSearchTags(),
                FileName = FileName,
                FlyerType = FlyerType,
                CaptureRestriction = CaptureRestriction,

                RegistrationPeriod = new SportsClub.Domain.RegistrationPeriod()
                {
                    RegisterStartDate = RegistrationPeriod.RegisterStartDate,
                    RegisterEndDate = RegistrationPeriod.RegisterEndDate
                },

                EventSearchField = EventSearchField = new EventSearchField
                {
                    MinAge = EventSearchField.MinAge,
                    MaxAge = EventSearchField.MaxAge,
                    MinGrade = EventSearchField.MinGrade.HasValue && EventSearchField.MinGrade.Value != SchoolGradeType.None ? EventSearchField.MinGrade : null,
                    MaxGrade = EventSearchField.MaxGrade.HasValue && EventSearchField.MaxGrade.Value != SchoolGradeType.None ? EventSearchField.MaxGrade : null,
                    Level = EventSearchField.Level.HasValue ? EventSearchField.Level : Level.None,
                    Gender = EventSearchField.Gender.HasValue ? EventSearchField.Gender : null,
                },
            };

            if (WizardStep.PageMode == Models.PageMode.Edit)
            {
                DateUpdated = DateTime.Now;
            }

            result.Instructor = new Instructor()
            {
                HeadInstructor = this.HeadInstructor,
                AssistantInstructors = this.AssistantInstructors,
                HeadInstructorLabel = this.HeadInstructorLabel,
                AssistantInstructorLabel = this.AssistantInstructorsLabel
            };

            result.CategoryId = Category.CategoryId;

            if (WizardStep.PageMode == PageMode.Create && Utilities.CompareEnumValues<EventPageStep>(EventPageStep.BasicInformation, WizardStep.LastCreatedPage) != -1)
            {
                result.LastCreatedPage = SportsClub.Domain.EventPageStep.BasicInformation;
            }
            else
            {
                result.LastCreatedPage = WizardStep.LastCreatedPage;
            }

            return result;
        }

        #region Constructors
        public EventBasicInformationEditModel()
            : base()
        {
            RegistrationPeriod = new RegistrationPeriodViewModel();
            //ClubLocation = new ClubLocation();
            EventSearchField = new EventSearchField();
            OutSourcers = new List<OutSourcerList>();

            AssistantInstructors = new List<string>();

            SeedOutSourcers();
            SeedAges();
            SeedInstructors();

            HeadInstructorLabel = Constants.Default_Head_Instructor_Label;
            AssistantInstructorsLabel = Constants.Default_Assistant_Instructor_Label;
        }

        public EventBasicInformationEditModel(string domain)
            : base(domain)
        {

        }

        public EventBasicInformationEditModel(EventTypeCategory eventType, PageMode pageMode, SaveType saveType, bool isVendor, int clubCategoryId)
            : this()
        {
            ClubLocation = new DisplayClubLocationViewModel(ClubDomain);
            SeedCategories(clubCategoryId: clubCategoryId);
            ClubLocation.SeedClubLocation();
            EventType = eventType;
            EventSponserType = isVendor ? SportsClub.Domain.EventSponserType.Vendor : SportsClub.Domain.EventSponserType.OutSourcer;
        }

        public EventBasicInformationEditModel(Domain.Event newEvent)
            : this()
        {

            this.CategoryId = newEvent.CategoryId;
            this.ClubDomain = newEvent.ClubDomain;
            this.DateCreated = newEvent.EventMetaData.DateCreated;
            this.DateUpdated = newEvent.EventMetaData.DateUpdated;
            this.Description = newEvent.Description;
            this.Domain = newEvent.Domain;

            this.EventSearchField = newEvent.EventMetaData.EventSearchField;
            this.EventSearchTags = newEvent.EventMetaData.EventSearchTags;
            //this.EventSponserType 
            this.EventStatus = newEvent.EventStatus;
            this.EventType = newEvent.EventType;
            this.FileName = newEvent.EventMetaData.FileName;
            this.FlyerType = newEvent.EventMetaData.FlyerType;

            this.Name = newEvent.Name;
            this.Note = newEvent.Note;

            this.JbForm = newEvent.JbForm;

            this.TimeZone = newEvent.TimeZone;
            this.CaptureRestriction = newEvent.EventMetaData.CaptureRestriction;
            this.CategoryId = newEvent.CategoryId;
            this.ClubLocation = new DisplayClubLocationViewModel(newEvent.ClubDomain)
            {
                SelectedClubLocationId = newEvent.ClubLocationId,
            };

            var instructor = newEvent.Instructor;

            this.HeadInstructor = instructor.HeadInstructor;
            this.AssistantInstructors = instructor.AssistantInstructors;

            if (string.IsNullOrEmpty(instructor.HeadInstructorLabel))
            {
                this.HeadInstructorLabel = Constants.Default_Head_Instructor_Label;
            }
            else
            {
                this.HeadInstructorLabel = instructor.HeadInstructorLabel;
            }

            if (string.IsNullOrEmpty(instructor.AssistantInstructorLabel))
            {
                this.AssistantInstructorsLabel = Constants.Default_Assistant_Instructor_Label;
            }
            else
            {
                this.AssistantInstructorsLabel = instructor.AssistantInstructorLabel;
            }

            if (AssistantInstructors != null)
            {
                foreach (var item in AssistantInstructors)
                {
                    if (!this.AllInstructors.Any(a => a.Text.Equals(item)))
                    {
                        AllInstructors.Add(new SelectListItem { Value = item, Text = item });
                    }
                }
            }

            this.RegistrationPeriod = new RegistrationPeriodViewModel(newEvent.EventMetaData.RegistrationPeriod.RegisterStartDate.Value, newEvent.EventMetaData.RegistrationPeriod.RegisterEndDate.Value);

            this.ClubLocation.SeedClubLocation();

            SeedCategories(categoryId: newEvent.CategoryId);


        }

        #endregion

        #region Internals

        private void SeedInstructors()
        {
            AllInstructors = Ioc.ICoachDbService.All(ClubDomain).ToList()
                .Select(n =>
                           new SelectListItem() { Value = n.FullName, Text = n.FullName })
                        .ToList();
        }

        private void SeedCategories(int categoryId = 0, int clubCategoryId = 0)
        {
            Category = new DisplayCategoryViewModel();
            // Load categories from db with a condition that check template existence in course category
            Category = new DisplayCategoryViewModel()
            {
                Categories = Ioc.ICategoryDbService.GetCategories(SubDomainCategories.Course),
                CategoryId = categoryId,
                ClubCategoryId = clubCategoryId
            };
        }

        private void SeedAges()
        {
            var ageList = new List<SelectListItem>();
            for (int i = 1; i <= 120; i++)
            {
                ageList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            Ages = new SelectList(ageList, "Value", "Text");
        }

        private void SeedOutSourcers()
        {
            var outSourcers = Ioc.IClubVendorDb.GetSchoolsForClub(ClubId);
            OutSourcers = outSourcers.Select(m => new OutSourcerList
            {
                OutSourcerClubDomain = m.OutSourcerClubDomain,
                OutSourcerClubName = m.OutSourcerLocation.PostalAddress.Address + "(" + m.OutSourcerClubName + ")",
                OutSourcerLocation = m.OutSourcerLocation,

            }).ToList();
            OutSourcerSelectList = new SelectList(OutSourcers, "OutSourcerClubDomain", "OutSourcerClubName");
        }

        #endregion
    }

    public class EventCalendarRegSettingsEditModel : EventBaseCreateEditModel
    {
        public Domain.Calendar Calendar { get; set; }

        public EventSchedule EventSchedule { get; set; }

        public EventScheduleCalendar EventCalendar { get; set; }

        [MustBeTrue(ErrorMessage = "There is no any schedule defined.")]
        public bool HasSchedule { get; set; }

        public Domain.Event ToEvent()
        {
            Domain.Event eventDb = new Domain.Event();

            eventDb.ClubDomain = ClubDomain;
            eventDb.Domain = Domain;

            if (WizardStep.PageMode == PageMode.Create && Utilities.CompareEnumValues<EventPageStep>(EventPageStep.RegSettings, WizardStep.LastCreatedPage) != -1)
            {
                eventDb.LastCreatedPage = SportsClub.Domain.EventPageStep.RegSettings;
            }
            else
            {
                eventDb.LastCreatedPage = WizardStep.LastCreatedPage;
            }

            return eventDb;
        }

        #region Constructors
        public EventCalendarRegSettingsEditModel()
            : base()
        {

        }

        public EventCalendarRegSettingsEditModel(string domain)
            : base(domain)
        {
        }
        #endregion
    }

    public class EventPackRegSettingsEditModel : EventBaseCreateEditModel
    {
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Start date")]
        [Required]
        public DateTime? StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Finish date")]
        [Required]
        public DateTime? FinishDate { get; set; }

        [DisplayName("Full price")]
        [Required]
        public decimal? Price { get; set; }

        public string PriceTitle { get; set; }

        [DisplayName("Capacity")]
        public int? Capacity { get; set; }

        public SelectList CapacityList { get; set; }

        public PackAttribute ToPackAttribute()
        {
            PackAttribute result = new PackAttribute();

            result.Capacity = this.Capacity;
            result.FinishDate = this.FinishDate.Value;
            result.Price = this.Price.Value;
            result.StartDate = this.StartDate.Value;
            result.PriceTitle = this.PriceTitle;

            return result;
        }

        #region Internals

        public void SeedCapacity()
        {
            var capacityList = new List<SelectListItem>();
            for (int i = 1; i <= 1000; i++)
            {
                capacityList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            CapacityList = new SelectList(capacityList, "Value", "Text");
        }

        #endregion

        #region Constructors

        public EventPackRegSettingsEditModel()
        {

        }

        public EventPackRegSettingsEditModel(string domain)
            : base(domain)
        {
            SeedCapacity();

            EventType = EventTypeCategory.Pack;

            PriceTitle = "Full price";
        }

        public EventPackRegSettingsEditModel(Domain.Event eventDb)
            : base(eventDb.Domain)
        {
            var attributes = JsonConvert.DeserializeObject<PackAttribute>(eventDb.EventAttributesSerialized);

            this.Capacity = attributes.Capacity;
            this.FinishDate = attributes.FinishDate;
            this.StartDate = attributes.StartDate;
            this.Price = attributes.Price;
            this.Capacity = attributes.Capacity;
            this.PriceTitle = attributes.PriceTitle;
            this.EventType = eventDb.EventType;


            SeedCapacity();
        }
        #endregion
    }

    public class EventAdditionalInfoEditModel : EventBaseCreateEditModel
    {
        public AssignFormViewModel AssignForm { get; set; }

        #region Constructors

        public EventAdditionalInfoEditModel()
            : base()
        {
            AssignForm = new AssignFormViewModel();
        }

        public EventAdditionalInfoEditModel(string domain)
            : base(domain)
        {
            // initial AssignForm
            var forms = Ioc.IFormsDbService.GetForms(this.ClubDomain, EventStatusCategories.open);
            AssignForm = new AssignFormViewModel() { AllForms = new SelectList(forms, "Id", "Name") };

            AssignForm.Forms = forms.Select(x => new FormListViewModel
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
        }

        #endregion
    }

    public class EventLastPageEditModel : EventBaseCreateEditModel
    {
        [Display(Name = "I understand that I should double check the system generated class and registration page.")]
        public bool IsFinished { get; set; }

        #region Constructors
        public EventLastPageEditModel()
            : base()
        {
        }

        public EventLastPageEditModel(string domain)
            : base(domain)
        {

        }
        #endregion
    }

    public class EventDisplayBase
    {
        public string Domain { get; set; }

        public string ClubDomain { get; set; }

        public EventStatus EventStatus { get; set; }

        public EventRegStatus EventRegStatus { get; set; }

        public EventRunningStatus EventRunningStatus { get; set; }

        public EventTypeCategory EventType { get; set; }



        public EventDisplayBase()
        {
        }

        public EventDisplayBase(string clubDomain, string domain)
        {
            Domain = domain;
            ClubDomain = clubDomain;
        }

        public EventDisplayBase(string clubDomain, string domain, RegistrationPeriod registrationPeriod, Domain.TimeZone timeZone, DateTime startDate, DateTime? endDate)
            : this(clubDomain, domain)
        {
            EventRegStatus = Utilities.GetEventRegStatus(registrationPeriod.RegisterStartDate.Value,
                registrationPeriod.RegisterEndDate.Value, timeZone);
            EventRunningStatus = Utilities.GetEventRunningStatus(startDate, endDate, timeZone);
        }
    }

    public class EventPageViewModel : EventDisplayBase
    {
        public string Name { get; set; }

        public RoleCategories? CurrentUserRole { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        public Domain.TimeZone TimeZone { get; set; }

        public string LogoUri { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public string LocationName { get; set; }

        public int CategoryId { get; set; }

        public GoogleMapsViewModel Map { get; set; }

        public EventDisplayBodyBase EventDisplayBody { get; set; }

        public EventRightSideBase EventRightSide { get; set; }

        public string LeftDays { get; set; }

        public PageMetaDataViewModel MetaData { get; set; }

        public string ClubName { get; set; }

        public string SeasonDomain { get; set; }

        public bool IsEligibleUser { get; set; }
        public RegistrationMode RegistrationMode { get; set; }

        public string PdfFileName { get; set; }
        public int? CapacityLeft { get; set; }

        public OrderMode OrderMode { get; set; }
        public bool IsNotCurrentCartDomain
        {
            get
            {
                // Get cart
                var cartDb = Ioc.ICartDbService;

                // Get domain of orders in the cart(if there is no order in cart return empty)
                string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                if (!string.IsNullOrEmpty(currentDomain))
                {
                    return !(currentDomain == this.ClubDomain);
                }

                return false;
            }
        }

        public bool LoggedIn { get; set; }

        public EventPageViewModel()
        {
            //var eventId = DependencyResolverHelper.IEventService.Get(ClubDomain, Domain).Id;
            //IsEligibleUser = DependencyResolverHelper.IRestrictedDb.IsEligibleUser(TheMembership.GetCurrentUserId(), eventId);
        }

        public EventPageViewModel(Domain.Event newEvent)
            : this()
        {
            var club = Ioc.IClubDbService.GetBasicInfo(newEvent.ClubDomain);

            this.LeftDays = leftDays(newEvent);

            this.ClubDomain = newEvent.ClubDomain;
            this.Domain = newEvent.Domain;
            this.EventStatus = newEvent.EventStatus;
            this.EventType = newEvent.EventType;
            this.Name = newEvent.Name;
            this.TimeZone = newEvent.TimeZone;
            this.RegistrationPeriod = new RegistrationPeriodViewModel(newEvent.EventMetaData.RegistrationPeriod.RegisterStartDate.Value, newEvent.EventMetaData.RegistrationPeriod.RegisterEndDate.Value);
            this.PostalAddress = newEvent.ClubLocation.PostalAddress;
            this.CategoryId = newEvent.CategoryId;
            this.PdfFileName = newEvent.EventMetaData.FileName;
            this.LoggedIn = WebSecurity.IsAuthenticated;
            this.ClubName = club.Name;
            this.RegistrationMode = newEvent.RegistrationMode;
            this.SeasonDomain = newEvent.Season.Domain;

            if (club != null)
            {
                LogoUri = Utilities.GetClubLogoURL(club.Domain, club.Logo, newEvent.CategoryId);
            }

            Map = new GoogleMapsViewModel()
            {
                Name = newEvent.ClubLocation.Name,
                PostalAddress = newEvent.ClubLocation.PostalAddress,
            };

            MetaData = new PageMetaDataViewModel()
            {
                PageTitle =
                            this.Name +
                            Constants.C_Space +
                            "by" +
                            Constants.C_Space +
                            this.ClubName +
                            Constants.S_SpaceDashSpace +
                            CategoryModel.GetCategoryName(newEvent.CategoryId) +
                            Constants.C_Space + this.EventType,

                MetaKeywords = string.Join(Constants.S_CommaSpace, new string[] 
                            { 
                                this.Name,
                                this.ClubName,
                                CategoryModel.GetCategoryName(newEvent.CategoryId) + Constants.C_Space + this.EventType, 
                                Utilities.StringNullHandle(newEvent.ClubLocation.PostalAddress.City,string.Empty), 
                                Utilities.GetAbbreviationState(newEvent.ClubLocation.PostalAddress.State) 
                            }),

                MetaDesc = newEvent.Description,
            };

            switch (EventType)
            {
                case EventTypeCategory.NewRegular:
                    {
                        var attributes = JsonConvert.DeserializeObject<EventCourseRegSettings>(newEvent.EventAttributesSerialized);

                        this.EventDisplayBody = new EventDisplayBodyCourse(ClubDomain, Domain)
                        {
                            RecurrenceTime = SeedCourseRecurrenceTime(attributes.RecurrenceTime),
                            StartDate = attributes.RecurrentStartDate,
                            FinishDate = attributes.RecurrentEndDate,
                            WeeksCount = attributes.WeeksCount,
                        };

                        if (!string.IsNullOrEmpty(newEvent.Description))
                        {
                            ((EventDisplayBodyCourse)EventDisplayBody).Description = newEvent.Description;
                        }

                        var eventRightSide = new EventDisplayRightSideCourse(newEvent.Instructor)
                        {
                            Capacity = attributes.Capacity,
                            Gender = newEvent.EventMetaData.EventSearchField.Gender,
                            Level = newEvent.EventMetaData.EventSearchField.Level,
                            FullPrice = attributes.FullPrice,
                            MinAge = newEvent.EventMetaData.EventSearchField.MaxAge,
                            MaxGrade = newEvent.EventMetaData.EventSearchField.MaxGrade,
                            MinGrade = newEvent.EventMetaData.EventSearchField.MinGrade,
                            MaxAge = newEvent.EventMetaData.EventSearchField.MaxAge,
                            StartDate = attributes.RecurrentStartDate,
                            FinishDate = attributes.RecurrentEndDate,
                            WeeksCount = attributes.WeeksCount,
                            HasProRatingPrice = attributes.HasProRatingPrice,
                            SessionPrice = attributes.SessionPrice,
                            LeftSessions = Ioc.ICourseDbService.CalculateLeftSessions((DateTime)attributes.RecurrentStartDate, attributes.RecurrentEndDate, attributes.RecurrenceTime),
                        };
                        if (eventRightSide.Capacity.HasValue)
                        {
                            CapacityLeft = eventRightSide.Capacity.Value - Ioc.ICourseDbService.CompletedOrderCount(newEvent.Domain, newEvent.ClubDomain);
                        }
                        if (attributes.FinishDateType != FinishDateType.UnSpecifiedEndDate)
                        {
                            eventRightSide.TotalSession =
                            Ioc.ICourseDbService.CalculateTotalSessions(attributes.RecurrentStartDate.Value,
                                attributes.RecurrentEndDate, attributes.RecurrenceTime);
                            if (eventRightSide.LeftSessions != null)
                            {
                                eventRightSide.ProRatingPrice =
                                    Math.Round((decimal)((attributes.FullPrice.Value *
                                                          eventRightSide.LeftSessions) /
                                                          eventRightSide.TotalSession));
                            }
                        }
                        this.EventRightSide = eventRightSide;
                    }
                    break;
                case EventTypeCategory.Calendar:
                    {
                        this.EventDisplayBody = new EventDisplayBodyCalendar(newEvent);

                        this.EventRightSide = new EventDisplayRightSideCalendar(newEvent);
                    }
                    break;
                case EventTypeCategory.Pack:
                    {
                        this.EventDisplayBody = new EventDisplayBodyPack(newEvent);

                        this.EventRightSide = new EventDisplayRightSidePack(newEvent);
                    }
                    break;
            }
        }

        #region Course Event Methods

        private MeetUpTimes SeedCourseRecurrenceTime(ICollection<CourseRecurrenceTime> list)
        {
            //RecurrenceTime = new HashSet<CourseRecurrenceTimeViewModel>();
            var RecurrenceTime = new MeetUpTimes();
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Sunday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Monday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Tuesday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Wednesday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Thursday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Friday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Saturday });

            if (list != null)
            {
                foreach (var item in list)
                {
                    var recDb = RecurrenceTime.RecurrenceTimes.Single(p => p.Day == item.Day);
                    if (recDb != null)
                    {
                        RecurrenceTime.RecurrenceTimes.Remove(recDb);
                        RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = item.Day, FinishTime = item.FinishTime, StartTime = item.StartTime, IsChecked = true });
                    }
                }
            }

            RecurrenceTime.RecurrenceTimes = RecurrenceTime.RecurrenceTimes.OrderBy(c => c.Day).ToList();
            return RecurrenceTime;
        }

        #endregion

        [Obsolete("This method must remove later")]
        private string leftDays(Domain.Event newEvent)
        {

            string result = string.Empty;

            DateTime expireDate = newEvent.EventMetaData.RegistrationPeriod.RegisterEndDate.Value.AddDays(1);

            TimeSpan elapsedTime = expireDate - Utilities.GetCurrentLocalDateTime(newEvent.TimeZone);

            // If remainig time is more than 3 days
            if (elapsedTime.TotalHours >= 72)
            {
                result = string.Empty;
            }
            // If remainig time is between 3 and 1 day
            else if (elapsedTime.TotalHours >= 24 && elapsedTime.TotalHours < 72)
            {
                result = string.Format("{0} days left", elapsedTime.Days + 1);
            }
            // If remainig time is less than 1 day
            else if (elapsedTime.TotalHours < 24)
            {
                if (elapsedTime.TotalHours >= 1)
                {
                    result = string.Format("{0}:{1} hours left", elapsedTime.Hours, elapsedTime.Minutes);
                }
                // If remainig time is less than 1 houre
                else if (elapsedTime.Minutes > 0 && elapsedTime.Minutes < 60)
                {
                    result = string.Format("{0} min left", elapsedTime.Minutes);
                }
            }

            return result;
        }

    }

    public class EventDisplayBodyBase : EventDisplayBase
    {
        public string LogoUri { get; set; }

        public GoogleMapsViewModel Map { get; set; }

        public virtual ClubLocation ClubLocation { get; set; }

        public EventDisplayBodyBase()
        {

        }

        public EventDisplayBodyBase(string clubdomain, string domain)
            : base(clubdomain, domain)
        {

        }
    }

    public class EventRightSideBase : EventDisplayBase
    {
        public int? MinAge { get; set; }

        public int? MaxAge { get; set; }

        public SchoolGradeType? MinGrade { get; set; }

        public SchoolGradeType? MaxGrade { get; set; }

        public Level? Level { get; set; }

        public Genders? Gender { get; set; }

        public string GradeMessage
        {
            get
            {
                if (MinGrade != null)
                {

                    if (MaxGrade != null)
                    {
                        return string.Format(Constants.Grade_Restriction_Message, MinGrade.Value.ToDescription(),
                                             MaxGrade.ToDescription());
                    }
                    return string.Format(Constants.Min_Grade_Restriction_Message, MinGrade.Value.ToDescription());
                    //
                }
                if (MaxGrade != null)
                {
                    return string.Format(Constants.Max_Grade_Abbreviation, MaxGrade.Value.ToDescription());
                }
                return "";
            }
        }

        public string MinAgeMessage
        {
            get
            {
                if (MinAge.HasValue)
                {

                    if (MaxAge.HasValue)
                    {
                        return string.Format(Constants.Age_Restriction_Message, MinAge, MaxAge);
                    }

                    return string.Format(Constants.Min_Age_Restriction_Message, MinAge);
                }
                if (MaxAge.HasValue)
                {
                    return string.Format(Constants.Max_Age_Restriction_Message, MaxAge);
                }
                return "";
            }
        }

        public DateTime? StartDate { get; set; }

        public DateTime? FinishDate { get; set; }

        public string HeadInstructor { get; set; }

        public List<string> AssistantInstructors { get; set; }

        public string HeadInstructorLabel { get; set; }

        public string AssistantInstructorLabel { get; set; }

        public EventRightSideBase()
        {

        }

        public EventRightSideBase(string clubDomain, string domain)
        {
            Domain = domain;
            ClubDomain = clubDomain;
        }

        public EventRightSideBase(string clubDomain, string domain, RegistrationPeriod registrationPeriod,
            Domain.TimeZone timeZone, DateTime startDate, DateTime? endDate)
            : base(clubDomain, domain, registrationPeriod, timeZone, startDate, endDate)
        {

        }

        public EventRightSideBase(Domain.Instructor instructor)
        {
            HeadInstructor = instructor.HeadInstructor;
            AssistantInstructors = instructor.AssistantInstructors;
            HeadInstructorLabel = instructor.HeadInstructorLabel;
            AssistantInstructorLabel = instructor.AssistantInstructorLabel;
        }
    }

    public class EventDisplayRightSideCourse : EventRightSideBase
    {
        public decimal? FullPrice { get; set; }

        public bool HasProRatingPrice { get; set; }

        //public int? NumOfSessionLeft { get; set; }

        public decimal ProRatingPrice { get; set; }

        public int? LeftSessions { get; set; }

        public int? Capacity { get; set; }

        public int? WeeksCount { get; set; }

        public decimal? SessionPrice { get; set; }

        public int TotalSession { get; set; }

        public string GradeMessage
        {
            get
            {
                if (base.MinGrade != null)
                {

                    if (base.MaxGrade != null)
                    {
                        return string.Format(Constants.Grade_Restriction_Message, base.MinGrade.Value.ToDescription(),
                                             base.MaxGrade.ToDescription());
                    }
                    return string.Format(Constants.Min_Grade_Restriction_Message, base.MinGrade.Value.ToDescription());
                    //
                }
                if (base.MaxGrade != null)
                {
                    return string.Format(Constants.Max_Grade_Abbreviation, base.MaxGrade.Value.ToDescription());
                }
                return "";
            }
        }

        public string MinAgeMessage
        {
            get
            {
                if (base.MinAge.HasValue)
                {

                    if (base.MaxAge.HasValue)
                    {
                        return string.Format(Constants.Age_Restriction_Message, base.MinAge, base.MaxAge);
                    }

                    return string.Format(Constants.Min_Age_Restriction_Message, base.MinAge);
                }
                if (base.MaxAge.HasValue)
                {
                    return string.Format(Constants.Max_Age_Restriction_Message, base.MaxAge);
                }
                return "";
            }
        }

        public EventDisplayRightSideCourse(string clubDomain, string domain)
            : base(clubDomain, domain)
        {

        }

        public EventDisplayRightSideCourse(string clubDomain, string domain, RegistrationPeriod registrationPeriod,
            Domain.TimeZone timeZone, DateTime startDate, DateTime? endDate)
            : base(clubDomain, domain, registrationPeriod, timeZone, startDate, endDate)
        {

        }

        public EventDisplayRightSideCourse(Domain.Instructor instructor)
            : base(instructor)
        {

        }
    }

    public class EventDisplayBodyCourse : EventDisplayBodyBase
    {
        public int? WeeksCount { get; set; }

        public DateTime? FinishDate { get; set; }

        public DateTime? StartDate { get; set; }

        public MeetUpTimes RecurrenceTime { get; set; }

        public string Description { get; set; }

        public EventDisplayBodyCourse(string clubDomain, string domain)
            : base(clubDomain, domain)
        {

        }
    }

    public class EventDisplayRightSidePack : EventRightSideBase
    {
        public decimal Price { get; set; }

        public string PriceTitle { get; set; }

        public decimal? Capacity { get; set; }

        public EventDisplayRightSidePack(Domain.Event eventDb)
            : base(eventDb.ClubDomain, eventDb.Domain)
        {
            Gender = eventDb.EventMetaData.EventSearchField.Gender;
            Level = eventDb.EventMetaData.EventSearchField.Level;

            MinAge = eventDb.EventMetaData.EventSearchField.MaxAge;
            MaxGrade = eventDb.EventMetaData.EventSearchField.MaxGrade;
            MinGrade = eventDb.EventMetaData.EventSearchField.MinGrade;
            MaxAge = eventDb.EventMetaData.EventSearchField.MaxAge;

            var eventAttribute = (PackAttribute)eventDb.EventAttributes;

            StartDate = eventAttribute.StartDate;
            FinishDate = eventAttribute.FinishDate;
            Price = eventAttribute.Price;
            PriceTitle = eventAttribute.PriceTitle;
            Capacity = eventAttribute.Capacity;

            var instructor = eventDb.Instructor;

            HeadInstructor = instructor.HeadInstructor;
            AssistantInstructors = instructor.AssistantInstructors;
            HeadInstructorLabel = instructor.HeadInstructorLabel;
            AssistantInstructorLabel = instructor.AssistantInstructorLabel;
        }
    }

    public class EventDisplayBodyPack : EventDisplayBodyBase
    {

        public DateTime? FinishDate { get; set; }

        public DateTime? StartDate { get; set; }

        public string Description { get; set; }

        public EventDisplayBodyPack(Domain.Event eventDb)
            : base(eventDb.ClubDomain, eventDb.Domain)
        {
            var eventAttribute = (PackAttribute)eventDb.EventAttributes;

            StartDate = eventAttribute.StartDate;
            FinishDate = eventAttribute.FinishDate;
            Description = eventDb.Description;

        }
    }

    public class EventRegistrationViewModel : EventDisplayBase
    {
        public string Name { get; set; }

        public int? ProfileId { get; set; }

        public RoleCategories? CurrentUserRole { get; set; }

        public CourseRegistrationType RegistrationType { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        public Domain.TimeZone TimeZone { get; set; }

        public string SeasonDomain { get; set; }

        public PlayerProfileComplitionFormContactViewModel Contact { get; set; }

        public PlayerProfileComplitionFormSkillViewModel Skill { get; set; }

        public PlayerProfileComplitionFormDoctorContactViewModel Doctor { get; set; }

        public PlayerProfileSchoolViewModel School { get; set; }

        public PlayerProfileComplitionFormEmergencyContactViewModel EmergencyContact { get; set; }

        public PlayerProfile Player { get; set; }

        public OraganizationTypes OraganizationType { get; set; }

        public bool ParentRequired { get; set; }

        public AddPlayerProfileViewModel Parent { get; set; }

        public IEnumerable<SelectListItem> AllTeachers { get; set; }

        public bool IsRestrictedAge { get; set; }

        public PolicyViewModel Refund { get; set; }
        public PolicyViewModel Waiver { get; set; }
        public PolicyViewModel MedicalRelease { get; set; }

        public EventPageViewModel EventInfo { get; set; }

        public EventScheduleBase EventSchedule { get; set; }

        public List<SelectedItem> SelectedItems { get; set; }

        public EventRegostratinFormViewModel Form { get; set; }

        public PaymentPlanViewModel PaymentPlan { get; set; }

        public List<CustomDiscountViewModel> CustomDiscounts { get; set; }

        public EventChargeDiscountEditModel MultiSessionDiscount { get; set; }

        public bool HasMultisession { get; set; }

        public SiblingViewModel SiblingDiscount { get; set; }

        public bool IsRestrictedUser { get; set; }

        public int CategoryId { get; set; }

        public bool LoggedIn { get; set; }

        public int? SelectedProfileId { get; set; }

        public string PlayerImageUrl { get; set; }

        public OrderMode OrderMode { get; set; }

        public bool IsEligibleUser { get; set; }
        public bool IsAdultLoggedInUser { get; set; }

        public RegistrationMode RegistrationMode { get; set; }

        public DonationViewModel Donation { get; set; }

        public IEnumerable<SelectListItem> PlayerLists { get; set; }
        public List<ChargeDiscountViewModel> MandetoryCharges { get; set; }
        public List<OptionalChargeDiscountViewModel> OptionalCharges { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                var cartDb = Ioc.ICartDbService;

                string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                if (!string.IsNullOrEmpty(currentDomain))
                {
                    return !(currentDomain == this.ClubDomain);
                }

                return false;
            }
        }

        public EventRegistrationViewModel()
        {
            SelectedItems = new List<SelectedItem>();

            EventInfo = new EventPageViewModel();

            // Refund = new PolicyViewModel();

            // Waiver = new PolicyViewModel();

            // MedicalRelease = new PolicyViewModel();

            PaymentPlan = new PaymentPlanViewModel();

            MandetoryCharges = new List<ChargeDiscountViewModel>();

            OptionalCharges = new List<OptionalChargeDiscountViewModel>();

            Donation = new DonationViewModel();

        }

        public EventRegistrationViewModel(Domain.Event newEvent, OrderMode orderMode)
            : this()
        {

            EventType = newEvent.EventType;

            OrderMode = orderMode;

            HasMultisession = newEvent.ChargeDiscounts.Any(m => m.Category == ChargeDiscountCategories.MultiSession);

            EventInfo = new EventPageViewModel(newEvent);

            Form = new EventRegostratinFormViewModel(newEvent.JbForm);
           
            var currentUser = TheMembership.GetCurrentUserName();

            PaymentPlan = new PaymentPlanViewModel(newEvent.PaymentPlans.FirstOrDefault(), newEvent.ClubDomain);
     
            CustomDiscounts = new List<CustomDiscountViewModel>();

            MultiSessionDiscount = new EventChargeDiscountEditModel();

            IsRestrictedUser = PaymentPlan.PaymentPlanType == PaymentPlanType.Installment && (PaymentPlan.RestrictedType == RestrictedType.AllUser || PaymentPlan.SelectedUsers.Contains(currentUser));

            // SeedPolicies();
            MandetoryCharges = newEvent.ChargeDiscounts.Where(c => c.Category == ChargeDiscountCategories.Mandetory).Select(p => new ChargeDiscountViewModel(p, true)).ToList();
            OptionalCharges = newEvent.ChargeDiscounts.Where(c => c.Category == ChargeDiscountCategories.Optional).Select(p => new OptionalChargeDiscountViewModel(p, true)).ToList();

            //foreach (Policy item in newEvent.Policies)
            //{
            //    switch (item.Category)
            //    {
            //        case PolicyCategories.Refund:
            //            Refund.Set(item);
            //            break;

            //        case PolicyCategories.Waiver:
            //            Waiver.Set(item);
            //            break;

            //        case PolicyCategories.MedicalRelease:
            //            MedicalRelease.Set(item);
            //            break;

            //        default:
            //            throw new NotSupportedException(string.Format(Constants.F_PolicyCategoryNotSupported, item.Category));
            //    }
            //}

            foreach (var item in newEvent.ChargeDiscounts)
            {
                switch (item.Category)
                {
                    case ChargeDiscountCategories.Custom:
                        {
                            var custom = new CustomDiscountViewModel();
                            custom.Discount.Amount = (int)item.Amount;
                            custom.Discount.Category = item.Category;
                            custom.Discount.ChargeDiscountType = item.ChargeDiscountType;
                            custom.Discount.Desc = item.Desc;
                            custom.Discount.Id = item.Id;
                            CustomDiscounts.Add(custom);
                            break;
                        }
                    case ChargeDiscountCategories.Sibling:
                        {
                            SiblingDiscount = new SiblingViewModel(item);

                            break;
                        }
                    case ChargeDiscountCategories.MultiSession:
                        {
                            this.MultiSessionDiscount = new EventChargeDiscountEditModel(item, false);
                            break;
                        }
                }
            }

            if (newEvent.ChargeDiscounts.Any(m => m.Category == ChargeDiscountCategories.Donation))
            {
                var donation = newEvent.ChargeDiscounts.Single(m => m.Category == ChargeDiscountCategories.Donation);
                Donation = new DonationViewModel(new Donation
                {
                    Checked = true,
                    Description = donation.Detail,
                    Title = donation.Desc
                });
            }

            RegistrationMode = newEvent.RegistrationMode;
        }

        //private void SeedPolicies()
        //{
        //    IStandardPolicyDb policyDb = DependencyResolverHelper.IStandardPolicyDbService;
        //    StandardPolicy[] policies = policyDb.Get();
        //    Refund = new PolicyViewModel() { Category = PolicyCategories.Refund, Name = policies[(int)PolicyCategories.Refund - 1].Name, Desc = policies[(int)PolicyCategories.Refund - 1].Description, IsChecked = false };
        //    Waiver = new PolicyViewModel() { Category = PolicyCategories.Waiver, Name = policies[(int)PolicyCategories.Waiver - 1].Name, Desc = policies[(int)PolicyCategories.Waiver - 1].Description, IsChecked = false };
        //    MedicalRelease = new PolicyViewModel() { Category = PolicyCategories.MedicalRelease, Name = policies[(int)PolicyCategories.MedicalRelease - 1].Name, Desc = policies[(int)PolicyCategories.MedicalRelease - 1].Description, IsChecked = false };
        //}

    }

    public class EventCourseScheduleViewModel : EventScheduleBase
    {
        public bool HasEarlybird { get; set; }

        public decimal? FullPrice { get; set; }

        public bool HasProRatingPrice { get; set; }

        public bool HasSessionPrice { get; set; }

        public CourseRegistrationType RegistrationType { get; set; }

        public Nullable<DateTime> DropInSessionDate { get; set; }

        public decimal ProRatingPrice { get; set; }

        public int? LeftSessions { get; set; }

        public int? Capacity { get; set; }

        public int? WeeksCount { get; set; }

        public DateTime? FinishDate { get; set; }

        public DateTime? StartDate { get; set; }

        public decimal? SessionPrice { get; set; }

        public int TotalSession { get; set; }

        public EventCourseScheduleViewModel()
        {

        }

        public EventCourseScheduleViewModel(string clubDomain, string domain)
        {
            Domain = domain;
            ClubDomain = clubDomain;
        }

        public EventCourseScheduleViewModel(Domain.Event eventDb)
            : base(eventDb.ClubDomain, eventDb.Domain, eventDb.EventMetaData.RegistrationPeriod, eventDb.TimeZone, eventDb.EventSchedule.StartDate.Value, eventDb.EventSchedule.EndDate)
        {

            var attributes = JsonConvert.DeserializeObject<EventCourseRegSettings>(eventDb.EventAttributesSerialized);
            var schedules = eventDb.EventSchedule;
            Domain = eventDb.Domain;
            ClubDomain = eventDb.ClubDomain;

            #region Earlybird

            if (attributes.EarlybirdDiscounts != null && attributes.EarlybirdDiscounts.Count > 0)
            {
                var clubTimeZone = Ioc.IClubDbService.Get(eventDb.ClubDomain, true).TimeZone;

                var localTimeNow = Utilities.GetCurrentLocalDateTime(clubTimeZone.Value);

                foreach (var earlybirdDiscount in attributes.EarlybirdDiscounts.OrderBy(m => m.EarlybirdDate))
                {
                    if (localTimeNow.Date <= earlybirdDiscount.EarlybirdDate.Date)
                    {
                        this.FullPrice = earlybirdDiscount.Price;
                        this.HasEarlybird = true;
                        break;
                    }
                }
            }

            #endregion

            if (!this.FullPrice.HasValue)
            {
                this.FullPrice = attributes.FullPrice;
                this.HasEarlybird = false;
            }
            //this.FullPrice = attributes.FullPrice;
            this.HasSessionPrice = attributes.HasSessionPrice;
            this.HasProRatingPrice = attributes.HasProRatingPrice;
            this.SessionPrice = attributes.SessionPrice;
            this.WeeksCount = attributes.WeeksCount;
            this.StartDate = attributes.RecurrentStartDate;
            this.RegistrationType = new CourseRegistrationType();

            if (this.EventRunningStatus == EventRunningStatus.InProgress && this.HasProRatingPrice)
            {
                RegistrationType = CourseRegistrationType.ProRating;
            }

            if (attributes.FinishDateType != FinishDateType.UnSpecifiedEndDate)
            {
                if (attributes.FinishDateType == FinishDateType.NumberOfSession)
                {
                    SeedFinishDate(schedules.EventScheduleAttributeList);
                }
                else if (attributes.FinishDateType == FinishDateType.UnSpecifiedEndDate)
                {
                    FinishDate = null;
                    HasProRatingPrice = false;
                }

                this.LeftSessions = Ioc.ICourseDbService.CalculateLeftSessions((DateTime)attributes.RecurrentStartDate,
                attributes.RecurrentEndDate, attributes.RecurrenceTime);

                this.TotalSession =
                Ioc.ICourseDbService.CalculateTotalSessions(attributes.RecurrentStartDate.Value,
                    attributes.RecurrentEndDate, attributes.RecurrenceTime);

                var totalSession = this.TotalSession;

                this.ProRatingPrice = Math.Round((decimal)((attributes.FullPrice.Value * this.LeftSessions) / totalSession));
            }

            EventRegStatus = Utilities.GetEventRegStatus(eventDb.EventMetaData.RegistrationPeriod.RegisterStartDate.Value, eventDb.EventMetaData.RegistrationPeriod.RegisterEndDate.Value, eventDb.TimeZone);

            EventRunningStatus = Utilities.GetEventRunningStatus(eventDb.EventSchedule.StartDate.Value,
                                            eventDb.EventSchedule.EndDate, eventDb.TimeZone);
            EventStatus = eventDb.EventStatus;
            ClubDomain = eventDb.ClubDomain;
            Domain = eventDb.Domain;
            Name = eventDb.Name;
 
        }

        private void SeedFinishDate(List<EventScheduleAttribute> schedules)
        {
            int numWeeks = 0;
            TotalSession = 0;
            DateTime tmpStart = (DateTime)StartDate;
            DayOfWeek dayofWeekend = tmpStart.AddDays(-1).DayOfWeek;
            while (numWeeks < WeeksCount)
            {

                foreach (var rec in schedules)
                {
                    TotalSession++;
                    break;
                }

                tmpStart = tmpStart.AddDays(1);

                if (tmpStart.DayOfWeek.Equals(dayofWeekend))
                {
                    numWeeks++;
                }
            }
            FinishDate = tmpStart;
        }
    }

    public class EventPackScheduleViewModel : EventScheduleBase
    {
        public decimal Price { get; set; }

        public string PriceTitle { get; set; }

        public EventPackScheduleViewModel()
        {

        }

        public EventPackScheduleViewModel(Domain.Event eventDb)
            : base(eventDb.ClubDomain, eventDb.Domain)
        {
            var eventAttribute = (PackAttribute)eventDb.EventAttributes;

            Price = eventAttribute.Price;
            PriceTitle = eventAttribute.PriceTitle;

            StartDate = eventAttribute.StartDate;
            EndDate = eventAttribute.FinishDate;

            EventRegStatus = Utilities.GetEventRegStatus(eventDb.EventMetaData.RegistrationPeriod.RegisterStartDate.Value,
               eventDb.EventMetaData.RegistrationPeriod.RegisterEndDate.Value, eventDb.TimeZone);
            EventRunningStatus = Utilities.GetEventRunningStatus(eventAttribute.StartDate, eventAttribute.FinishDate, eventDb.TimeZone);
        }
    }

    public class EventRegostratinFormViewModel : BaseViewModel<Domain.Event>
    {
        public JbForm JbForm { get; set; }

        public EventRegostratinFormViewModel()
        {
            this.JbForm = new JbForm();
        }

        public EventRegostratinFormViewModel(JbForm jbForm)
        {
            this.JbForm = jbForm;
        }
    }

    public class SelectedItem
    {
        public int Id { get; set; }

        public decimal Price { get; set; }
    }

    public class EventScheduleCreateEditModel : IValidatableObject
    {
        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        [Display(Name = "Schedule Description")]
        public string ScheduleDescription { get; set; }

        [Display(Name = "Begins on")]
        [Required]
        public DateTime? ScheduleStartDate { get; set; }


        public DateTime? ScheduleEndDate { get; set; }

        public ContinueType ContinueType { get; set; }

        public int? Occourances { get; set; }

        [DayIsChecked(ErrorMessage = "You must select at least one day.")]
        public List<EventScheduleDayViewModel> Days { get; set; }

        public SelectList CapacityList { get; set; }

        public PageMode PageMode { get; set; }

        public Domain.Calendar ToCalendar()
        {
            var calendar = new Domain.Calendar()
            {
                ContinueType = this.ContinueType,
                Description = ScheduleDescription,
                EndDate = ScheduleEndDate.Value,
                Occourances = Occourances.HasValue ? Occourances.Value : 0,

                StartDate = ScheduleStartDate.Value,
                Days = Days.Select(d => new EventScheduleDay
                                        {
                                            Capacity = d.Capacity,
                                            DayOfWeek = d.DayOfWeek,
                                            EndTime = d.EndTime,
                                            IsChecked = d.IsChecked,
                                            Price = d.Price,
                                            StartTime = d.StartTime,
                                            EbChecked = d.EbDiscount.IsChecked,
                                            EbPrice = d.EbDiscount.Price,
                                            EbPriorDay = d.EbDiscount.PriorDay
                                        }
                                   )
                                  .ToList(),
            };

            return calendar;
        }

        #region Validation
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            switch (ContinueType)
            {
                case ContinueType.Until:
                    {
                        if (!ScheduleEndDate.HasValue)
                        {
                            yield return new ValidationResult("You must enter schedule end date", new[] { "ScheduleEndDate" });
                        }

                        if (ScheduleStartDate.HasValue && ScheduleEndDate.HasValue && ScheduleStartDate.Value >= ScheduleEndDate.Value)
                        {
                            yield return new ValidationResult("End date must be later than the start date.", new[] { "ScheduleEndDate" });
                        }
                    }
                    break;
                case ContinueType.For:
                    {
                        if (!Occourances.HasValue)
                        {
                            yield return new ValidationResult("You must enter occourances", new[] { "Occourances" });
                        }
                    }
                    break;
            }
        }

        #endregion

        #region Constructors
        public EventScheduleCreateEditModel()
        {
            Days = new List<EventScheduleDayViewModel>()
            {
                new EventScheduleDayViewModel{DayOfWeek = DayOfWeek.Sunday},
                new EventScheduleDayViewModel{DayOfWeek = DayOfWeek.Monday},
                new EventScheduleDayViewModel{DayOfWeek = DayOfWeek.Tuesday},
                new EventScheduleDayViewModel{DayOfWeek = DayOfWeek.Wednesday},
                new EventScheduleDayViewModel{DayOfWeek = DayOfWeek.Thursday},
                new EventScheduleDayViewModel{DayOfWeek = DayOfWeek.Friday},
                new EventScheduleDayViewModel{DayOfWeek = DayOfWeek.Saturday},
            };

            CapacityList = Utilities.DropDownCapacity(1000);
        }

        public EventScheduleCreateEditModel(PageMode pageMode, string clubDomain, string domain)
            : this()
        {
            PageMode = pageMode;
            ClubDomain = clubDomain;
            Domain = domain;
            Occourances = 1;

            foreach (var item in Days)
            {
                item.EbDiscount = new CalendarEbDiscount();
            }
        }

        public EventScheduleCreateEditModel(Domain.Calendar calendar)
        {
            this.ContinueType = calendar.ContinueType;
            this.ScheduleDescription = calendar.Description;
            this.ScheduleEndDate = calendar.EndDate;
            this.ScheduleStartDate = calendar.StartDate;
            this.Occourances = calendar.Occourances;
            this.PageMode = Models.PageMode.Edit;

            this.Days = calendar.Days.Select(d => new EventScheduleDayViewModel(d)).ToList();

            this.CapacityList = Utilities.DropDownCapacity(1000);

        }

        #endregion
    }

    public class EventScheduleDayViewModel : IValidatableObject
    {
        public DayOfWeek DayOfWeek { get; set; }

        public bool IsChecked { get; set; }

        [Display(Name = "Start time")]
        [DataType(DataType.Time)]
        public TimeSpan? StartTime { get; set; }

        [Display(Name = "End time")]
        [DataType(DataType.Time)]
        public TimeSpan? EndTime { get; set; }

        [Display(Name = "Price")]
        public int Price { get; set; }

        [Display(Name = "Capacity")]
        public int? Capacity { get; set; }

        public CalendarEbDiscount EbDiscount { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (IsChecked && !StartTime.HasValue)
            {
                yield return new ValidationResult("You must enter start time", new[] { "StartTime" });
            }

            if (IsChecked && !EndTime.HasValue)
            {
                yield return new ValidationResult("You must enter end time", new[] { "EndTime" });
            }

            if (IsChecked && EndTime.HasValue && StartTime.HasValue && StartTime >= EndTime)
            {
                yield return new ValidationResult("End time must be later than the start time.", new[] { "EndTime" });
            }

            if (IsChecked && Price == 0)
            {
                yield return new ValidationResult("You must enter price", new[] { "Price" });
            }

            if (EbDiscount.IsChecked && EbDiscount.Price == 0)
            {
                yield return new ValidationResult("You must enter early bird price", new[] { "EbDiscount.Price" });
            }

            if (EbDiscount.IsChecked && EbDiscount.Price >= Price)
            {
                yield return new ValidationResult("Early bird price must less than price", new[] { "EbDiscount.Price" });
            }
        }

        public EventScheduleDayViewModel()
        {

        }

        public EventScheduleDayViewModel(EventScheduleDay eventSchedule)
        {
            DayOfWeek = eventSchedule.DayOfWeek;
            IsChecked = eventSchedule.IsChecked;
            StartTime = eventSchedule.StartTime;
            EndTime = eventSchedule.EndTime;
            Price = eventSchedule.Price;
            Capacity = eventSchedule.Capacity;
            EbDiscount = new CalendarEbDiscount
            {
                IsChecked = eventSchedule.EbChecked,
                Price = eventSchedule.EbPrice,
                PriorDay = eventSchedule.EbPriorDay
            };
        }
    }

    public class EventScheduleBase : EventDisplayBase
    {
        public string Name { get; set; }

        [Display(Name = "Start date")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End date")]
        public DateTime EndDate { get; set; }

        public EventScheduleBase()
        {

        }

        public EventScheduleBase(string clubDomain, string domain)
            : base(clubDomain, domain)
        {

        }

        public EventScheduleBase(string clubDomain, string domain, RegistrationPeriod registrationPeriod, Domain.TimeZone timeZone, DateTime startDate, DateTime? endDate)
            : base(clubDomain, domain, registrationPeriod, timeZone, startDate, endDate)
        {

        }

    }

    public class EventScheduleCalendar : EventScheduleBase
    {
        public bool HasSchedule { get; set; }

        public bool HasPack { get; set; }

        public decimal PackPrice { get; set; }

        public string PackTitle { get; set; }

        public CourseRegistrationType RegistrationType { get; set; }

        public List<CalendarRegisterDayOfWeek> Schedule { get; set; }

        public EventScheduleCalendar()
        {
        }

        public EventScheduleCalendar(string clubDomain, string domain, PageMode pageMode = PageMode.Edit)
        {
            DateTime? calendarStartDate = null;

            var calendar = new Domain.Calendar();

            if (!string.IsNullOrEmpty(domain))
            {
                calendar = Ioc.IEventService.GetEventCalendar(clubDomain, domain);
            }

            if (calendar != null)
            {
                calendarStartDate = calendar.StartDate;
                HasSchedule = (pageMode == PageMode.Edit);
            }

            ClubDomain = clubDomain;
            Domain = domain;
            StartDate = calendarStartDate.HasValue ? calendarStartDate.Value : DateTime.Now;
        }

        public EventScheduleCalendar(Domain.Event eventDb)
        {
            ClubDomain = eventDb.ClubDomain;
            Domain = eventDb.Domain;

            var ea = JsonConvert.DeserializeObject<CalendarAttribute>(eventDb.EventAttributesSerialized);

            StartDate = ea.Calendar.StartDate;
            EndDate = ea.Calendar.EndDate;
            HasPack = ea.HasPack;
            PackPrice = ea.PackPrice;
            PackTitle = ea.PackTitle;

            Schedule = ea.Calendar.Days.Where(d => d.IsChecked).Select(d =>
                new CalendarRegisterDayOfWeek
                {
                    Day = d.DayOfWeek,
                    StartTime = d.StartTime.Value,
                    EndTime = d.EndTime.Value
                })
                .ToList();
        }
    }

    public class CalendarRegisterDayOfWeek
    {
        public DayOfWeek Day { get; set; }

        public TimeSpan StartTime { get; set; }

        public TimeSpan EndTime { get; set; }
    }

    public class RegisterEventSectionModel
    {
        public int ItemId { get; set; }

        public string Description { get; set; }

        public DateTime Start { get; set; }

        public DateTime? End { get; set; }

        public decimal Price
        {
            get
            {
                return SelectedCharges.Any(s => s.Category == ChargeDiscountCategories.EF) ? SelectedCharges.Single(s => s.Category == ChargeDiscountCategories.EF).Amount : 0;
            }
        }

        public List<ChargeDiscount> SelectedCharges { get; set; }

        #region Constructors

        public RegisterEventSectionModel()
        {
            SelectedCharges = new List<ChargeDiscount>();
        }

        #endregion
    }

    public class ProgramViewModel : ISchedulerEvent
    {
        public int TaskID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        private DateTime start;
        public DateTime Start { get; set; }

        private DateTime end;
        public DateTime End { get; set; }

        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }

        public string EndTimezone { get; set; }
        public string StartTimezone { get; set; }

        public int Price { get; set; }
        public int? Capacity { get; set; }

        public bool Enabled { get; set; }

        public bool EnableEb { get; set; }
        public int EbPrice { get; set; }

        public SelectList CapacityList { get; set; }

        public ProgramViewModel()
        {
            CapacityList = Utilities.DropDownCapacity(1000);
        }

        public ProgramViewModel(EventScheduleAttribute eventSchedule, DateTime localDateTime)
        {
            TaskID = eventSchedule.Id;
            Title = eventSchedule.Title;
            Start = eventSchedule.Start.Value;
            End = eventSchedule.End.Value;
            Description = string.Empty;
            IsAllDay = false;
            RecurrenceRule = string.Empty;
            RecurrenceException = string.Empty;
            RecurrenceID = null;

            Capacity = eventSchedule.Capacity;
            Enabled = eventSchedule.Start.Value.Date >= localDateTime.Date;

            EnableEb = (eventSchedule.Start.Value.Date.AddDays(-eventSchedule.EbPriorDay) >= localDateTime.Date) && eventSchedule.EbChecked;

            Price = EnableEb ? eventSchedule.EbPrice : eventSchedule.Price;
        }
    }

    public class EventDisplayBodyCalendar : EventDisplayBodyBase
    {
        public string Description { get; set; }

        public EventDisplayBodyCalendar()
        {

        }

        public EventDisplayBodyCalendar(Domain.Event eventDb)
        {
            ClubDomain = eventDb.ClubDomain;
            Domain = eventDb.Domain;
            Description = eventDb.Description;
        }
    }

    public class EventDisplayRightSideCalendar : EventRightSideBase
    {
        public decimal MinPrice { get; set; }

        public decimal MaxPrice { get; set; }

        public string Price
        {
            get
            {
                if (MinPrice != MaxPrice)
                {
                    return string.Format("{0}-{1}", Utilities.FormatCurrencyWithoutPenny(MinPrice), Utilities.FormatCurrencyWithoutPenny(MaxPrice));
                }

                return Utilities.FormatCurrencyWithoutPenny(MinPrice);
            }
        }

        public EventDisplayRightSideCalendar()
        {

        }

        public EventDisplayRightSideCalendar(Domain.Event eventDb)
            : base(eventDb.ClubDomain, eventDb.Domain)
        {

            Gender = eventDb.EventMetaData.EventSearchField.Gender;
            Level = eventDb.EventMetaData.EventSearchField.Level;

            MinAge = eventDb.EventMetaData.EventSearchField.MaxAge;
            MaxGrade = eventDb.EventMetaData.EventSearchField.MaxGrade;
            MinGrade = eventDb.EventMetaData.EventSearchField.MinGrade;
            MaxAge = eventDb.EventMetaData.EventSearchField.MaxAge;

            StartDate = eventDb.EventSchedule.StartDate;
            FinishDate = eventDb.EventSchedule.EndDate;

            var eventAttribute = eventDb.EventSchedule.EventScheduleAttributeList;

            if (eventAttribute != null && eventAttribute.Count > 0)
            {
                MinPrice = eventAttribute.Min(p => p.Price);
                MaxPrice = eventAttribute.Max(p => p.Price);
            }

            var instructor = eventDb.Instructor;

            HeadInstructor = instructor.HeadInstructor;
            AssistantInstructors = instructor.AssistantInstructors;
            HeadInstructorLabel = instructor.HeadInstructorLabel;
            AssistantInstructorLabel = instructor.AssistantInstructorLabel;
        }
    }

    public class CalendarEbDiscount
    {

        [DisplayName("Early bird pricing")]
        public bool IsChecked { get; set; }

        [DisplayName("Prior")]
        public int PriorDay { get; set; }

        public List<SelectListItem> PriorDays { get; set; }

        [DisplayName("of session date price")]
        public int Price { get; set; }

        public CalendarEbDiscount()
        {
            PriorDays = new List<SelectListItem>();

            for (int i = 1; i < 11; i++)
            {
                PriorDays.Add(new SelectListItem { Value = i.ToString(), Text = i.ToString() });
            }
        }
    }
}