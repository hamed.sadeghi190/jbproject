﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('organizationSettingsController', ['$scope', '$http', '$state', 'jbToast', function ($scope, $http, $state, jbToast) {
        window.app.globalObjects.pageClass = 'general-settings-view';
        window.app.globalObjects.initializeView();

        
        //image cropper start
      
        $scope.cropper = {};
        $scope.cropper.sourceImage = null;
        $scope.cropper.croppedImage = null;
        $scope.bounds = {};
        $scope.bounds.left = 0;
        $scope.bounds.right = 0;
        $scope.bounds.top = 0;
        $scope.bounds.bottom = 0;
        //image cropper end
        $scope.getSize = function (elem) {
            var element = document.getElementById("cropperLogo");
            var files = element.files;
            var file = files[0];

            var reader = new FileReader();
            reader.onload = loadFinished;
            reader.readAsDataURL(file);

            function loadFinished(event) {
                var data = event.target.result;
                var image = new Image();
                image.src = data;
                image.onload = function () {
                    $scope.bounds = {};
                    $scope.bounds.left = 0;
                    $scope.bounds.right = image.naturalWidth;
                    $scope.bounds.top =image.naturalHeight;
                    $scope.bounds.bottom = 0;
                }
            }

        }
        $http.get('Dashboard/Setting/EditGeneralSetting').success(function (data, status, headers, config) {
            $scope.Model = data;
            $scope.cropper.croppedImage = $scope.Model.Logo;
            
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.save = function (settingsSection) {
            $scope.Model.Logo = $scope.cropper.croppedImage;
            
            $http.post('Dashboard/Setting/EditGeneralSetting', { model: $scope.Model, settingsSection: settingsSection })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {

                     $scope.errors = null;

                     jbToast.success(data.Message, "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.addEmailItem = function () {

            if (validateEmail($scope.newEmail)) {

                $scope.Model.AllCCEmails.push({ Text: $scope.newEmail, Value: $scope.newEmail });
                $scope.Model.SelectedCCEmails.push($scope.newEmail);

                $scope.newEmail = '';
            }
            else {

                alert('Email is not valid.');
            }
        };
              
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

    }])
})();