﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using System.ComponentModel.DataAnnotations;


namespace Jumbula.Core.Model.Order
{
    public class ConfirmPaymentAmountViewModel 
    {
        public long Id { get; set; }
        public CurrencyCodes Currency { get; set; }
        public string EncodedReturnUrl { get; set; }

        [Required(ErrorMessage = "You should enter {0}.")]
        [Display(Name = "amount")]
        public decimal PayableAmount { get; set; }
        public DateTime DueDate { get; set; }
        public string FullName { get; set; }
        public string ProgramName { get; set; }
        public decimal CancellationFee { get; set; }
        public string StrCancellationFee => CurrencyHelper.FormatCurrencyWithPenny(CancellationFee, Currency);
        public decimal Balance { get; set; }
        public bool IsAutoChargeNotPastDue { get; set; }
        public string ReturnUrl { get; set; }
        public string Token { get; set; }
        public string StrDueDate => DateTimeHelper.FormatShortDateSeperetedByCamma(DueDate);

        public string StrBalance => CurrencyHelper.FormatCurrencyWithPenny(Balance, Currency);
        public AmountPayType AmountPayType { get; set; }
        public PaymentPlanType PaymentType { get; set; }

    }

}

