﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('waiversController', ['$scope', '$http', '$stateParams', '$state', 'waiverService', 'jbConfirmation', 'jbToast', function ($scope, $http, $stateParams, $state, waiverService, jbConfirmation, jbToast) {
        //window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.initial = function () {
            app.globalObjects.initializeView();
        };

        $scope.Model = {};

        $scope.waiverDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
            {
                read: {
                    url: window.location.origin + '/Dashboard/Club/GetWaiversPaging',
                    type: "POST",
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            //term: $scope.recipientSearchTerm,
                            skip: data.skip,
                            take: data.take,
                            page: data.page,
                            pageSize: data.pageSize,
                            sort: data.sort
                        }
                    }
                }
            },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: false,
            serverPaging: false
        });

        $scope.waiverGridOptions = {
            dataSource: $scope.waiverDataSource,
            sortable: false,
            columns: [
                {
                    field: "Name",
                    title: "Name",
                    template: "<strong class='grid-row-title'>#= Name #</strong>"
                },
                {
                    field: "IsRequired",
                    title: "Mandatory",
                    width: "160px",
                },
                {
                    field: "DateCreated",
                    title: "Created",
                    width: "160px",
                    template: "#= kendo.toString(kendo.parseDate(DateCreated), 'MMM dd, yyyy') #",
                },
                {
                    field: "",
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                  <li ng-click='Edit(#:Id#)'>\
                                        <span class='k-link jbi-pencil'>Edit</span>\
                                  </li>\
                                  <li ng-click='DeleteWaiver(#:Id#)'>\
                                        <span class='k-link jbi-remove'>Delete</span>\
                                  </li>\
                                  <li  class='MoveUp''>\
                                        <span class='k-link jbi-arrow-top'>Move up</span>\
                                  </li>\
                                  <li  class='MoveDown''>\
                                        <span class='k-link jbi-arrow-bottom'>Move down</span>\
                                  </li>\
                            </ul>\
                        </li>\
                    </ul>"
                }
            ],
            scrollable: false,
            dataBound: function (e) {

                var grid = $("#waiverGrid").data("kendoGrid");

                $(".MoveUp").each(function (e) {
                    if (e == 0) {
                        $(this).hide();
                    }
                });

                $(".MoveDown").each(function (e) {
                    if (e == grid.dataSource.data().length - 1) {
                        $(this).hide();
                    }
                });
                

                //Drag and drop
                grid.table.kendoSortable({
                    filter: ">tbody >tr",
                    hint: function (element) { //customize the hint
                        var table = $('<table style="width: 600px;" class="k-grid k-widget"></table>'),
                            hint;

                        table.append(element.clone()); //append the dragged element
                        table.css("opacity", 0.7);

                        return table; //return the hint element
                    },
                    cursor: "move",
                    placeholder: function (element) {
                        return $('<tr colspan="4" class="placeholder"></tr>');
                    },
                    change: function (e) {
                        var skip = grid.dataSource.skip(),
                            oldIndex = e.oldIndex + skip,
                            newIndex = e.newIndex + skip,
                            data = grid.dataSource.data(),
                            dataItem = grid.dataSource.getByUid(e.item.data("uid"));

                        grid.dataSource.remove(dataItem);
                        grid.dataSource.insert(newIndex, dataItem);

                        $scope.SaveOrders(grid.dataSource._data);
                    }
                });



                $('.MoveDown').on("click", function (e) {
                    var grid = $("#waiverGrid").data("kendoGrid");

                    var selectedItem = $(this).closest("tr");
                    var selectedUid = selectedItem.attr("data-uid");
                    var itemIndex = selectedItem.index();
                    var dataItem = grid.dataItem(selectedItem);

                    var newIndex = itemIndex + 1;
                    var content = $(".k-grid-content");

                    var offset = selectedItem.offset().top;

                    if (newIndex < grid.dataSource.view().length) {
                        grid.dataSource.remove(dataItem);
                        grid.dataSource.insert(newIndex, dataItem);

                        //grid.select("[data-uid=" + selectedUid + "]");

                        $scope.SaveOrders(grid.dataSource._data);
                    }

                });

                $('.MoveUp').on("click", function (e) {
                    var grid = $("#waiverGrid").data("kendoGrid");

                    var selectedItem = $(this).closest("tr");
                    var selectedUid = selectedItem.attr("data-uid");
                    var itemIndex = selectedItem.index();
                    var dataItem = grid.dataItem(selectedItem);

                    var newIndex = itemIndex - 1;
                    var content = $(".k-grid-content");
                    var offset = selectedItem.offset().top;

                    if (newIndex <= 0) {
                        newIndex = 0;
                    }

                    grid.dataSource.remove(dataItem);
                    grid.dataSource.insert(newIndex, dataItem);

                   // grid.select("[data-uid=" + selectedUid + "]");

                    $scope.SaveOrders(grid.dataSource._data);
                    
                });

            },
        };



        $scope.Edit = function (Id, Name, Text) {

            $scope.Model.Id = Id;

            $http.get('/Dashboard/Club/GetWaiver', { params: { waiverId: Id } }).success(function (data, status, headers, config) {
                //console.log(data)
                $scope.EditMode = true;
                $scope.AddMode = false;
                $scope.Model.Name = data.Name;
                $scope.Model.Text = data.Text.replace(/\n/g, '<br/>');
                $scope.Model.WaiverConfirmationType = data.WaiverConfirmationType;
                $scope.Model.IsRequired = data.IsRequired
            })

        }

        $scope.SaveWaiver = function () {

            $http.post('/Dashboard/Club/EditWaiver', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    if (data.Status) {
                        $scope.waiverGridOptions.dataSource.read();
                        $scope.EditMode = false;
                        $scope.AddMode = false;
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.DeleteWaiver = function (id) {
            jbConfirmation
                .yes(function () {
                    id = typeof id !== 'undefined' ? id : $scope.Model.Id;
                    $http.post('/Dashboard/Club/DeleteWaiver', { id: id })
                        .success(function (data, status, headers, config) {
                            $scope.waiverGridOptions.dataSource.read();
                            $scope.EditMode = false;
                            $scope.AddMode = false;
                        });
                })
                .confirm();
        }

        $scope.AddWaiver = function () {

            $http.post('/Dashboard/Club/CreateWaiver', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    if (data.Status) {
                        $scope.waiverGridOptions.dataSource.read();
                        $scope.AddMode = false;
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.Save = function () {

            $scope.Model.AllWaivers = [];

            $http.get('Dashboard/club/GetWaivers').success(function (data, status, headers, config) {

                $scope.Model.AllWaivers = data;

                waiverService.setModel($scope.Model);

                $state.back();
            })
        }

        $scope.ActiveAgree = function () {

            $scope.Model.WaiverConfirmationType = 'Agree'

        }

        $scope.SaveOrders = function (dataSorted) {

            $http.post('/Dashboard/Club/UpdateWaiverOrder', { model: dataSorted })
                .success(function (data, status, headers, config) {

                    if (data.Status) {
                        //jbToast.success("Save orders successfully.");
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });

        }

    }]);
})();