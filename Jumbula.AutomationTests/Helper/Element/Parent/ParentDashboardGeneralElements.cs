﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element.Parent
{
    class ParentDashboardGeneralElements : BaseElements
    {
        public ParentDashboardGeneralElements(IWebDriver driver)
            :base(driver)
        {

        }
        public IWebElement UsernameLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainNavbarCollapse']/div/div/ul/li[6]/a"));
            }
        }
        public IWebElement Username
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainNavbarCollapse']/div/div/ul/li[6]/ul/li[1]"));
            }
        }
        
        
        public List<IWebElement> Domains
        {
            get
            {
                var domains= Driver.FindElements(By.XPath("//*[@id='mainNavbarCollapse']/div/div/ul/li[6]/ul/li")).ToList();
                domains.RemoveAt(0);
                domains.RemoveAt(0);
                domains.RemoveAt(0);
                domains.RemoveAt(0);
                domains.RemoveAt(0);
                return domains;
            }
        }
        public IWebElement GetDomain(string domain)
        {
            var domains = Domains.ToList();
            return domains.First(d => d.Text.StartsWith(domain));
        }

        public IWebElement InvoiceLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[contains(text(), 'Invoices')]"));
            }
        }
    }
}
