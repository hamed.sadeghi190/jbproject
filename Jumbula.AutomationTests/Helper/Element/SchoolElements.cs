﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class SchoolElements : BaseElements
    {
        public SchoolElements(IWebDriver driver)
            : base(driver)
        {

        }
        #region Header
        public IWebElement BackLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@class='icon-arrow-left']"));
            }
        }
        public IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//div[@class='title']/h1"));
            }
        }

        public IWebElement AddSchoolLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@title='Add school']"));
            }
        }
        #endregion Header

        #region Filter
        public IWebElement FilterType
        {
            get
            {
                return Driver.FindElement(By.XPath("//select[@ng-model='filter.FilterTypes']"));
            }
        }
        public IWebElement NameOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Name']"));
            }
        }
        public IWebElement DomainOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Domain']"));
            }
        }
        public IWebElement ExpirationDateOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Expiration date']"));
            }
        }
        public IWebElement SearchTerm
        {
            get
            {
                return Driver.FindElement(By.Id("searchClubName"));
            }
        }
        public IWebElement StartDateFilter
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@id='filter.startDate']"));
            }
        }
        public IWebElement EndDateFilter
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@id='filter.endDate']"));
            }
        }
        public IWebElement SearchNameButton
        {
            get
            {
                return Driver.FindElement(By.Id ("btn-searchName"));
            }
        }

        public IWebElement SearchDateButton
        {
            get
            {
                return Driver.FindElement(By.Id("btn-searchDate"));
            }
        }
        public IWebElement ActiveTypeFilter
        {
            get
            {
                return Driver.FindElement(By.XPath("//select[@ng-model='filter.activeType']"));
            }
        }
        public IWebElement AllOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='All']"));
            }
        }
        public IWebElement ActiveOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Active']"));
            }
        }
        public IWebElement InactiveOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Inactive']"));
            }
        }
        public IWebElement ClearFilter
        {
            get
            {
                IList<IWebElement> btns = Driver.FindElements(By.TagName("button"));
                return btns.First(btn => btn.Text == "Clear filters");
            }
        }

        #endregion Filter

        #region GridRow1
        public bool Row1Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row1Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[1]/span"));
            }
        }
        
        public IWebElement Row1Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[2]/span"));
            }
        }
        public IWebElement Row1Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[3]/span"));
            }
        }
        public IWebElement Row1Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[4]/span"));
            }
        }
        public IWebElement Row1Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[5]/span"));
            }
        }
        public IWebElement Row1ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[6]/span"));
            }
        }
        public IWebElement Row1CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[7]/span"));
            }
        }
        public IWebElement Row1County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[8]/span"));
            }
        }
        public IWebElement Row1Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/span"));
            }
        }
        public IWebElement Row1LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row1EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row1DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row1ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row1EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[1]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow1

        #region GridRow2
        public bool Row2Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row2Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[1]/span"));
            }
        }
        
        public IWebElement Row2Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[2]/span"));
            }
        }
        public IWebElement Row2Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[3]/span"));
            }
        }
        public IWebElement Row2Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[4]/span"));
            }
        }
        public IWebElement Row2Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[5]/span"));
            }
        }
        public IWebElement Row2ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[6]/span"));
            }
        }
        public IWebElement Row2CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[7]/span"));
            }
        }
        public IWebElement Row2County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[8]/span"));
            }
        }
        public IWebElement Row2Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/span"));
            }
        }
        public IWebElement Row2LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row2EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row2DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row2ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row2EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[2]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow2

        #region GridRow3
        public bool Row3Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row3Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[1]/span"));
            }
        }
        
        public IWebElement Row3Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[2]/span"));
            }
        }
        public IWebElement Row3Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[3]/span"));
            }
        }
        public IWebElement Row3Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[4]/span"));
            }
        }
        public IWebElement Row3Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[5]/span"));
            }
        }
        public IWebElement Row3ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[6]/span"));
            }
        }
        public IWebElement Row3CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[7]/span"));
            }
        }
        public IWebElement Row3County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[8]/span"));
            }
        }
        public IWebElement Row3Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/span"));
            }
        }
        public IWebElement Row3LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row3EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row3DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row3ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row3EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[3]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow3

        #region GridRow4
        public bool Row4Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row4Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[1]/span"));
            }
        }
        
        public IWebElement Row4Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[2]/span"));
            }
        }
        public IWebElement Row4Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[3]/span"));
            }
        }
        public IWebElement Row4Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[4]/span"));
            }
        }
        public IWebElement Row4Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[5]/span"));
            }
        }
        public IWebElement Row4ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[6]/span"));
            }
        }
        public IWebElement Row4CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[7]/span"));
            }
        }
        public IWebElement Row4County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[8]/span"));
            }
        }
        public IWebElement Row4Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[9]/span"));
            }
        }
        public IWebElement Row4LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row4EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row4DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row4ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row4EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[4]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow4

        #region GridRow5
        public bool Row5Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row5Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[1]/span"));
            }
        }
        
        public IWebElement Row5Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[2]/span"));
            }
        }
        public IWebElement Row5Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[3]/span"));
            }
        }
        public IWebElement Row5Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[4]/span"));
            }
        }
        public IWebElement Row5Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[5]/span"));
            }
        }
        public IWebElement Row5ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[6]/span"));
            }
        }
        public IWebElement Row5CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[7]/span"));
            }
        }
        public IWebElement Row5County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[8]/span"));
            }
        }
        public IWebElement Row5Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[9]/span"));
            }
        }
        public IWebElement Row5LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row5EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row5DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row5ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row5EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[5]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow5

        #region GridRow6
        public bool Row6Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row6Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[1]/span"));
            }
        }
        
        public IWebElement Row6Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[2]/span"));
            }
        }
        public IWebElement Row6Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[3]/span"));
            }
        }
        public IWebElement Row6Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[4]/span"));
            }
        }
        public IWebElement Row6Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[5]/span"));
            }
        }
        public IWebElement Row6ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[6]/span"));
            }
        }
        public IWebElement Row6CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[7]/span"));
            }
        }
        public IWebElement Row6County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[8]/span"));
            }
        }
        public IWebElement Row6Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[9]/span"));
            }
        }
        public IWebElement Row6LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row6EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row6DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row6ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row6EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[6]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow6

        #region GridRow7
        public bool Row7Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row7Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[1]/span"));
            }
        }
        public IWebElement Row7Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[2]/span"));
            }
        }
        public IWebElement Row7Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[3]/span"));
            }
        }
        public IWebElement Row7Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[4]/span"));
            }
        }
        public IWebElement Row7Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[5]/span"));
            }
        }
        public IWebElement Row7ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[6]/span"));
            }
        }
        public IWebElement Row7CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[7]/span"));
            }
        }
        public IWebElement Row7County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[8]/span"));
            }
        }
        public IWebElement Row7Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[9]/span"));
            }
        }
        public IWebElement Row7LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row7EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row7DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row7ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row7EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[7]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow7

        #region GridRow8
        public bool Row8Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row8Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[1]/span"));
            }
        }
        public IWebElement Row8Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[2]/span"));
            }
        }
        public IWebElement Row8Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[3]/span"));
            }
        }
        public IWebElement Row8Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[4]/span"));
            }
        }
        public IWebElement Row8Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[5]/span"));
            }
        }
        public IWebElement Row8ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[6]/span"));
            }
        }
        public IWebElement Row8CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[7]/span"));
            }
        }
        public IWebElement Row8County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[8]/span"));
            }
        }
        public IWebElement Row8Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[9]/span"));
            }
        }
        public IWebElement Row8LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row8EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row8DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row8ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row8EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[8]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow8

        #region GridRow9
        public bool Row9Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row9Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[1]/span"));
            }
        }
        public IWebElement Row9Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[2]/span"));
            }
        }
        public IWebElement Row9Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[3]/span"));
            }
        }
        public IWebElement Row9Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[4]/span"));
            }
        }
        public IWebElement Row9Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[5]/span"));
            }
        }
        public IWebElement Row9ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[6]/span"));
            }
        }
        public IWebElement Row9CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[7]/span"));
            }
        }
        public IWebElement Row9County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[8]/span"));
            }
        }
        public IWebElement Row9Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[9]/span"));
            }
        }
        public IWebElement Row9LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row9EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row9DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row9ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row9EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[9]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow9

        #region GridRow10
        public bool Row10Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row10Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[1]/span"));
            }
        }
        public IWebElement Row10Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[2]/span"));
            }
        }
        public IWebElement Row10Admin
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[3]/span"));
            }
        }
        public IWebElement Row10Phone
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[4]/span"));
            }
        }
        public IWebElement Row10Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[5]/span"));
            }
        }
        public IWebElement Row10ExpirationDate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[6]/span"));
            }
        }
        public IWebElement Row10CommRate
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[7]/span"));
            }
        }
        public IWebElement Row10County
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[8]/span"));
            }
        }
        public IWebElement Row10Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[9]/span"));
            }
        }
        public IWebElement Row10LoginAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[9]/ul/li/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row10EditAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[9]/ul/li/div/ul/li[2]/span"));
            }
        }
        public IWebElement Row10DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[9]/ul/li/div/ul/li[4]/span"));
            }
        }
        public IWebElement Row10ResetPasswordAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[9]/ul/li/div/ul/li[5]/span"));
            }
        }
        public IWebElement Row10EditUserAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainGrid']/div[2]/table/tbody/tr[10]/td[9]/ul/li/div/ul/li[7]/span"));
            }
        }
        #endregion GridRow10

    }
}
