﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum OrderStatusCategories
    {
        [Description("Not initiated")]
        notInitiated = 0,
        [Description("Initiated")]
        initiated = 1,
        [Description("Pending")]
        pending = 2,
        [Description("Completed")]
        completed = 3,
        [Description("Canceled")]
        canceled = 4,
        [Description("Refunded")]
        refunded = 5,
        [Description("Unknown")]
        unknown = 7,
        [Description("Error")]
        error = 8,
        [Description("Transferred")]
        transferred = 9,
        [Description("Deleted")]
        deleted = 10
    }

}
