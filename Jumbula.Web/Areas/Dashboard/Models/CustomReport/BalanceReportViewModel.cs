﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class BalanceReportViewModel
    {
        public string ProgramName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string Balance { get; set; }

        public string ConfirmationId { get; set; }

        public string PaymentDetail { get; set; }

        public string RegDate { get; set; }

        public long OrderItemId { get; set; }

        public long OrderId { get; set; }

        public int ItemStatusReason { get; set; }

    }

    public enum BalanceReportFilters
    {
        All = 0,
        DueDate = 1
    }

}