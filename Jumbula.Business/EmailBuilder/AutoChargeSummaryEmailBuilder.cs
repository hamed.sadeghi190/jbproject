﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Jumbula.Core.Business;
using Jumbula.Core.Data;

namespace Jumbula.Business.EmailBuilder
{
    public class AutoChargeSummaryEmailBuilder : BaseEmailBuilder<AutoChargeSummaryEmailParameterModel>
    {
        #region Constants

        private const string EmailTemplateFileName = "AutoChargeSummary";
        private const string SubjectThemplate = "Autocharge summary for {0}";

        #endregion


        private readonly IRepository<TransactionActivity, long> _transactionActivityRepository;
        private readonly IRepository<Club> _clubRepository;
        private readonly IClubBusiness _clubBusiness;

        private List<Tuple<long, long>> _successInstallments = new List<Tuple<long, long>>();
        private List<Tuple<long, long>> _failedInstallments = new List<Tuple<long, long>>();
        private int _clubId;
        private Club _club;
        private DateTime _date;
        private string _subject;

        private List<string> _clubOwnerEmails;
        public override EmailCategory Category => EmailCategory.AutoChargeSummary;

        public override EmailCallerType CallerType => EmailCallerType.AutoCharge;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.Low;

        public override string TemplateName => EmailTemplateFileName;


        #region Constructors
        public AutoChargeSummaryEmailBuilder(IClubBusiness clubBusiness, IRepository<TransactionActivity, long> transactionActivityRepository, IRepository<Club> clubRepository)
        {
            _transactionActivityRepository = transactionActivityRepository;
            _clubRepository = clubRepository;
            _clubBusiness = clubBusiness;
        }
        #endregion

        public override void Initiate(AutoChargeSummaryEmailParameterModel parameters)
        {

            _successInstallments = parameters.SuccessInstallments;
            _failedInstallments = parameters.FailedInstallments;

            _date = parameters.Date;
            _clubId = parameters.ClubId;
            _subject = string.Format(SubjectThemplate, _date.Date.ToShortDateString());
            _club = _clubRepository.Get(_clubId);
            _clubOwnerEmails = _clubBusiness.ClubOwnerEmailList(_club);
        }

        public override void InitiatePreview(AutoChargeSummaryEmailParameterModel parameters)
        {
        }

        public override string GetSubject(string subCategory = null)
        {
            return _subject;
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            return new MailAddress(Common.Constants.Email.JumbulaNoReplyEmailAddress);
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return $"{EmailCallerType.AutoCharge}";
        }

        public override string GetReplyTo(string subCategory = null)
        {
            return Common.Constants.Email.JumbulaNoReplyEmailAddress;
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            return _clubOwnerEmails.Select(e => new MailAddress(e)).ToList();
        }

        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            var successTransactionActivityIds = _successInstallments.Select(s => s.Item2).Distinct().ToList();

            var successTransactions = _transactionActivityRepository.GetList(t => successTransactionActivityIds.Contains(t.Id)).ToList();

            var failTransactionActivityIds = _failedInstallments.Select(s => s.Item2).Distinct().ToList();

            var failTransactions = _transactionActivityRepository.GetList(t => failTransactionActivityIds.Contains(t.Id)).ToList();


            var successItems = new List<AutoChargeSummaryBodyItemModel>();
            var failedItems = new List<AutoChargeSummaryBodyItemModel>();
            decimal successAmount = 0;
            decimal failAmount = 0;

            foreach (var transaction in successTransactions)
            {
                var newSucessItem = new AutoChargeSummaryBodyItemModel()
                {
                    Price = transaction.Amount,
                    PaymentDate = transaction.TransactionDate,
                    ConfirmationId = transaction.Order.ConfirmationId,
                    PlayerFullName = transaction.OrderItem.Player.Contact.FullName,
                    Currency = transaction.OrderItem.Order.Club.Currency
                };
                successAmount += transaction.Amount;
                successItems.Add(newSucessItem);

            }

            foreach (var failTransaction in failTransactions)
            {
                var newFailItem = new AutoChargeSummaryBodyItemModel()
                {
                    Price = failTransaction.Amount,
                    PaymentDate = failTransaction.TransactionDate,
                    ConfirmationId = failTransaction.Order.ConfirmationId,
                    PlayerFullName = failTransaction.OrderItem.Player.Contact.FullName,
                    Currency = failTransaction.OrderItem.Order.Club.Currency
                };
                failAmount += failTransaction.Amount;
                failedItems.Add(newFailItem);
            }

            return new AutoChargeSummaryBodyModel
            {
                ClubName = _club.Name,
                Date = _date,
                Subject = _subject,
                SuccessItems = successItems,
                SuccessCount = successTransactions.Count,
                SuccessTotalAmount = successAmount,
                FailedItems = failedItems,
                FailTotalAmount = failAmount,
                FailCount = failTransactions.Count,
            };
        }

        public override List<CommunicationHistory> GetHistories()
        {
            return new List<CommunicationHistory>();
        }

    }
}
