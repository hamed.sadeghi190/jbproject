﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaJumAdminNotOwnerException : JumbulaAccountException
    {
        public JumbulaJumAdminNotOwnerException(Exception e)
            : base(e)
        {
        }
    }
}
