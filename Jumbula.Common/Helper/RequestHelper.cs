﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Common.Helper
{
    public static class RequestHelper
    {
        public static async Task GetRequest(string url)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            var client = new HttpClient();

            await client.GetAsync(url);
        }
        public static async Task GetRequest(string url,TimeSpan timeOut)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            var client = new HttpClient {Timeout = timeOut};
            await client.GetAsync(url);
        }
        public static async Task<HttpResponseMessage> PostRequest(string url, string body, TimeSpan timeOut)
        {
            var stringContent = new StringContent(body, Encoding.UTF8);
            var client = new HttpClient() { Timeout = timeOut };
            return await client.PostAsync(url, stringContent);
        }

        public static string HttpPost(string url, string data,TimeSpan timeout)
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.UserAgent = "Jumbula";
            request.KeepAlive = false;
            request.Method = "POST";
            request.Timeout = (int)timeout.TotalMilliseconds;
            var postBytes = Encoding.ASCII.GetBytes(data);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = postBytes.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            var response = (HttpWebResponse)request.GetResponse();
            var sr = new StreamReader(response.GetResponseStream());

            return sr.ReadToEnd();
        }

        public static MemoryStream GetStreamFromUrl(string url)
        {
            byte[] imageData;
            using (var wc = new WebClient())
            {
                imageData = wc.DownloadData(url);
            }
            var ms = new MemoryStream(imageData);

            return ms;
        }
    }
}
