﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class SeasonFile : BaseEntity
    {
        public SeasonFile()
        {
        }

        [ForeignKey("SeasonId")]
        public virtual Season Season { get; set; }
        public long SeasonId { get; set; }


        [ForeignKey("FileId")]
        public virtual JbFile File { get; set; }
        public int FileId { get; set; }

    }
   
}
