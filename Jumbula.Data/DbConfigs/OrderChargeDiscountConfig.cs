﻿using System.Data.Entity.ModelConfiguration;
using Jumbula.Core.Domain;

namespace Jumbula.Data.DbConfigs
{
    public class OrderChargeDiscountConfig : EntityTypeConfiguration<OrderChargeDiscount>
    {
        public OrderChargeDiscountConfig()
        {
            HasOptional(r => r.Charge).WithMany().HasForeignKey(r => r.ChargeId);
            HasOptional(r => r.Discount).WithMany().HasForeignKey(r => r.DiscountId);
            HasOptional(r => r.Coupon).WithMany().HasForeignKey(r => r.CouponId);
        }
    }
}
