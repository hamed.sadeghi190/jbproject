
/****** Object:  Table [dbo].[EmailAttachments]    Script Date: 1/22/2019 3:37:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailAttachments](
	[Id] [INT] IDENTITY(1,1) NOT NULL,
	[EmailId] [INT] NOT NULL,
	[FileName] [NVARCHAR](255) NULL,
	[FileUrl] [NVARCHAR](1000) NULL,
	[FileSize] [FLOAT] NOT NULL,
 CONSTRAINT [PK_dbo.EmailAttachments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[EmailAttachments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EmailAttachments_dbo.Emails_EmailId] FOREIGN KEY([EmailId])
REFERENCES [dbo].[Emails] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EmailAttachments] CHECK CONSTRAINT [FK_dbo.EmailAttachments_dbo.Emails_EmailId]
GO


