﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class SimpleProgramRightSideViewModel : BaseViewModel<Program, long>, IProgramRightSideViewModel, IProgramModel
    {

        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }

        public ProgramStatus Status { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public ProgramTypeCategory TypeCategory { get; set; }

        public List<Charge> Prices { get; set; }

        public decimal EarlybridPrice { get; set; }

        public decimal ProratePrice { get; set; }

        public bool HasEarlybird { get; set; }

        public bool IsProrate { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string ClubLogo { get; set; }

        public Genders Gender { get; set; }

    }
}