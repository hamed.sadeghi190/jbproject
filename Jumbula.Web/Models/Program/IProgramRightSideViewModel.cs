﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public interface IProgramRightSideViewModel
    {
        List<Charge> Prices { get; set; }

        decimal EarlybridPrice { get; set; }

        decimal ProratePrice { get; set; }

        bool HasEarlybird { get; set; }

        bool IsProrate { get; set; }

        DateTime? StartDate { get; set; }

        DateTime? EndDate { get; set; }
    }
}