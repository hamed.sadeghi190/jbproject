drop table [dbo].[webpages_UsersInRoles]
go

drop table [dbo].[webpages_OAuthMembership]
go

drop table [dbo].[webpages_Membership]
go

drop table [dbo].[webpages_Roles]
go

alter table [dbo].[UserProfile] drop constraint [DF__UserProfi__Credi__3F073C79]
go

drop table [dbo].[PaymentPlanUserProfiles]
go

drop table [dbo].[UserProfile]
go
