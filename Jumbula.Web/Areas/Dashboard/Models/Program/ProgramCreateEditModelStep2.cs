﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramCreateEditModelStep2 : BaseViewModel<Program, long>,  IProgramWizardModel
    {
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }

        [Required(ErrorMessage = "Program name is required.")]
        public string Name { get; set; }

        public ProgramStatus Status { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public ProgramTypeCategory TypeCategory { get; set; }

        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }

        public ProgramPageStep PageStep { get; set; }

        public List<ProgramScheduleViewModel> Schedules { get; set; }

        public ProgramPageStep LastCreatedPage { get; set; }

        public SaveType SaveType { get; set; }

        public List<SelectKeyValue<string>> Capacities { get; set; }

        public List<SelectKeyValue<int>> MinimumEnrollments { get; set; }

        public List<SelectKeyValue<string>> Sessions { get; set; }

        public List<SelectKeyValue<string>> Ages { get; set; }

    }
}