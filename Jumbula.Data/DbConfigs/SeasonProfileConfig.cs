﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class SeasonProfileConfig : EntityTypeConfiguration<Season>
    {
        public SeasonProfileConfig()
        {
            HasRequired(s => s.Club).WithMany(c => c.Seasons).HasForeignKey(s => s.ClubId);
            HasMany(p => p.Coupons).WithOptional(c => c.Season).HasForeignKey(c => c.SeasonId);
            //HasMany(p => p.Pages).WithOptional(c => c.Season).HasForeignKey(c => c.SeasonId);
            HasMany(p => p.OrderItems).WithRequired(c => c.Season).HasForeignKey(c => c.SeasonId);
            HasMany(p => p.Discounts).WithRequired(c => c.Season).HasForeignKey(c => c.SeasonId).WillCascadeOnDelete(false);
            HasMany(p => p.PaymentPlans).WithRequired(c => c.Season).HasForeignKey(c => c.SeasonId).WillCascadeOnDelete(false);
            HasMany(p => p.TransactionActivities).WithOptional(c => c.Season).HasForeignKey(c => c.SeasonId).WillCascadeOnDelete(false);
        }
    }
}
