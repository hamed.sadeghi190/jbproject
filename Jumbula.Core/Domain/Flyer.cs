﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class Flyer
    {
        public string FileName { get; set; }

        public FlyerType FlyerType { get; set; }
    }
}
