﻿using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Catalog;
using System;
using System.Linq;

namespace Jumbula.Business
{
    public class CatalogBusiness : ICatalogBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IRepository<CatalogItem> _catalogItemRepository;

        public CatalogBusiness(IClubBusiness clubBusiness, IRepository<CatalogItem> catalogItemRepository)
        {
            _clubBusiness = clubBusiness;
            _catalogItemRepository = catalogItemRepository;
        }


        public CatalogItem Get(int id)
        {
            return _catalogItemRepository.GetList().SingleOrDefault(p => p.Id == id);
        }

        public IQueryable<CatalogItem> GetList()
        {
            return _catalogItemRepository.GetList().Where(c => c.Status != CatalogStatus.Deleted);
        }

        public IQueryable<CatalogItem> GetList(int clubId)
        {
            return this.GetList().Where(c => c.ClubId == clubId);
        }

        public IQueryable<CatalogItem> GetList(string clubDomain)
        {
            return this.GetList().Where(c => c.Club.Domain.Equals(clubDomain, StringComparison.OrdinalIgnoreCase));
        }


        public OperationStatus Create(CatalogItem entity)
        {
            return _catalogItemRepository.Create(entity);
        }

        public OperationStatus Update(CatalogItem entity)
        {
            return _catalogItemRepository.Update(entity);
        }

        public OperationStatus Delete(int id)
        {
            var catalog = this.Get(id);

            catalog.Status = CatalogStatus.Deleted;

            return _catalogItemRepository.Update(catalog);
        }

        public OperationStatus Hide(int id)
        {
            var catalog = this.Get(id);

            catalog.Status = CatalogStatus.Hidden;

            return _catalogItemRepository.Update(catalog);
        }

        public OperationStatus Archive(int id)
        {
            var catalog = this.Get(id);

            catalog.Status = CatalogStatus.Archived;

            return _catalogItemRepository.Update(catalog);
        }

        public OperationStatus Active(int id)
        {
            var catalog = this.Get(id);

            catalog.Status = CatalogStatus.Active;

            return _catalogItemRepository.Update(catalog);
        }

        public OperationStatus UpdateSettings(CatalogSetting catalogSetting, int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            club.CatalogSetting = catalogSetting;

            var res = _clubBusiness.Update(club);

            return res;
        }


        public CatalogInvitationMessageViewModel BuildInviteMessageBody(Program program, Season outsourceSeason, DateTime? startTime, DateTime? endTime)
        {
            var programSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted);
            var programScheduleAttribute = programSchedule.Attributes as ScheduleAttribute;

            var model = new CatalogInvitationMessageViewModel();

            model.ProgramName = string.Format("Class: {0}", program.Name);

            var classSchedule = program.ProgramSchedules.First(f => !f.IsDeleted);

            switch (classSchedule.AttendeeRestriction.RestrictionType)
            {
                case Common.Enums.RestrictionType.None:
                    break;
                case Common.Enums.RestrictionType.Age:
                    model.Grades = string.Format("Ages: {0} - {1}", programSchedule.AttendeeRestriction.MinAge.HasValue ? programSchedule.AttendeeRestriction.MinAge.Value.ToString() : string.Empty, programSchedule.AttendeeRestriction.MaxAge.HasValue ? programSchedule.AttendeeRestriction.MaxAge.Value.ToString() : string.Empty);
                    break;
                case Common.Enums.RestrictionType.Grade:
                    model.Grades = string.Format("Grades: {0} - {1}", programSchedule.AttendeeRestriction.MinGrade.Value.ToDescription(), programSchedule.AttendeeRestriction.MaxGrade.Value.ToDescription());
                    break;
                default:
                    break;
            }

            if (programScheduleAttribute.WeekDaysMode == WeekDaysMode.Any)
            {
                model.WeekDays = "any day";
            }
            else
            {
                model.WeekDays = string.Join(", ", programScheduleAttribute.Days.Select(d => d.DayOfWeek.ToDescription()));
            }

            if (startTime.HasValue || endTime.HasValue)
            {
                model.Times = string.Format("Times: {0} - {1}", startTime.HasValue ? DateTimeHelper.FormatTimeSpanAmPm(startTime.Value.TimeOfDay) : string.Empty, endTime.HasValue ? DateTimeHelper.FormatTimeSpanAmPm(endTime.Value.TimeOfDay) : string.Empty);
            }

            model.ProviderClubName = string.Format("Provider: {0}", outsourceSeason.Club.Name);

            model.ProviderSeasonTitle = string.Format("Provider season: {0}", outsourceSeason.Title);

            model.ClubName = string.Format("School: {0}", program.Club.Name);

            model.SeasonTitle = string.Format("School season: {0}", program.Season.Title);

            if (!string.IsNullOrEmpty(program.CustomFields.SpecialRequests))
            {
                model.SpecialRequests = string.Format("Special requests: {0}", program.CustomFields.SpecialRequests);
            }

            return model;
        }
    }
}
