﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class PricePlanBusiness : IPricePlanBusiness
    {
        private IRepository<PricePlan> _pricePlanRepository;
        private IEntitiesContext _dataContext;

        public PricePlanBusiness(IRepository<PricePlan> pricePlanRepository, IEntitiesContext dataContext)
        {
            _pricePlanRepository = pricePlanRepository;
            _dataContext = dataContext;
        }

        public PricePlan GetByRevenueIdAndType(int revenueId, PricePlanType planType, decimal revenuePrice)
        {
            if (_pricePlanRepository.GetList().Any(c => c.IsDeleted == false && c.Type == planType && c.ClubRevenueId.Value == revenueId && c.MonthlyCharges == revenuePrice))
            {
                return _pricePlanRepository.GetList().FirstOrDefault(c => c.IsDeleted == false && c.Type == planType && c.ClubRevenueId.Value == revenueId && c.MonthlyCharges == revenuePrice);
            }
            return null;
        }
        public PricePlan Get(int pricePlanId)
        {
            return _pricePlanRepository.Get(c => c.Id == pricePlanId && c.IsDeleted != true);
        }
        public PricePlan GetbyType(PricePlanType planType)
        {
            if (_pricePlanRepository.GetList().Any(c => c.IsDeleted == false && c.Type == planType))
            {
                return GetList().FirstOrDefault(c => c.IsDeleted == false && c.Type == planType);
            }
            return null;
        }
        public IQueryable<PricePlan> GetList()
        {
            return _pricePlanRepository.GetList().Where(c => c.IsDeleted != true);
        }

        public OperationStatus Delete(int pricePlanId)
        {
            var pricePlan = _pricePlanRepository.Get(c => c.Id == pricePlanId);
            pricePlan.IsDeleted = true;
            return Update(pricePlan);
        }
        public IQueryable<ClubRevenue> GetAllRevenues()
        {
            return _dataContext.Set<ClubRevenue>().AsQueryable();
        }
        public PricePlan GetFreeTrialPlan()
        {
            var plan = GetbyType(PricePlanType.FreeTrial);
            if (plan == null)
            {
                plan = new PricePlan()
                {
                    Deposit = 0,
                    Description = "Free trial",
                    MonthlyCharges = 0,
                    TransactionFee = 0,
                    Type = PricePlanType.FreeTrial

                };
                Create(plan);
            }
            return GetbyType(PricePlanType.FreeTrial);
        }

        public PricePlan GetPayAsYouGoPlan()
        {
            var plan = GetbyType(PricePlanType.PayAsYouGo);
            if (plan == null)
            {
                plan = new PricePlan()
                {
                    Deposit = 0,
                    Description = "Pay as you go",
                    MonthlyCharges = 5,
                    TransactionFee = 1,
                    Type = PricePlanType.PayAsYouGo

                };
                Create(plan);
            }
            return GetbyType(PricePlanType.PayAsYouGo);
        }
        public PricePlan GetSubscriptionPlan(int revenueId)
        {
            var revenue = GetAllRevenues().FirstOrDefault(c => c.Id == revenueId);
            var plan = GetByRevenueIdAndType(revenueId, PricePlanType.Subscription, revenue.Price);

            if (plan == null)
            {

                if (revenue != null)
                {
                    plan = new PricePlan()
                    {
                        ClubRevenueId = revenue.Id,
                        ClubRevenue = revenue,
                        Deposit = 0,
                        Description = revenue.Description,
                        MonthlyCharges = revenue.Price,
                        TransactionFee = 0,
                        Type = PricePlanType.Subscription

                    };

                    Create(plan);
                }
            }

            return GetByRevenueIdAndType(revenueId, PricePlanType.Subscription, revenue.Price);
        }

        public SelectList GetClubRevenueList()
        {

            return new SelectList(GetAllRevenues().ToList().Take(10), dataValueField: "Id", dataTextField: "FreeTrialDesc", selectedValue: "");
        }

        public OperationStatus Create(PricePlan client)
        {
            return _pricePlanRepository.Create(client);
        }

        public OperationStatus Update(PricePlan client)
        {
            return _pricePlanRepository.Update(client);
        }
    }
}
