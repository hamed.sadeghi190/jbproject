﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IPaypalTrackingBusiness
    {
        OperationStatus Create(PaypalTracking paypalTracking);
        OperationStatus Update(PaypalTracking paypalTracking);
        string GenerateTrackingId(JumbulaSubSystem caller, long callerId,string token);

        IQueryable<PaypalTracking> GetUnProcessedItems();
    }
}
