﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class ClubAttributeValue
    {
        [ForeignKey("ClubId")]

        public virtual Club Club { get; set; }

        [Column(Order = 0), Key]
        public int ClubId { get; set; }

        [ForeignKey("AttributeId")]
        public virtual JbAttribute Attribute { get; set; }

        [Column(Order = 1), Key]
        public int AttributeId { get; set; }

        public string Value { get; set; }

        public int? AttributeOptionId { get; set; }

        [ForeignKey("AttributeOptionId")]
        public virtual JbAttributeOption AttributeOption { get; set; }
    }
}
