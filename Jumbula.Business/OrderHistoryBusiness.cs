﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class OrderHistoryBusiness : IOrderHistoryBusiness
    {
        private readonly IRepository<OrderHistory> _orderHistoryRepository;
        private readonly IEntitiesContext _dataContext;

        public OrderHistoryBusiness(IRepository<OrderHistory> orderHistoryRepository, IEntitiesContext dataContext)
        {
            _orderHistoryRepository = orderHistoryRepository;
            _dataContext = dataContext;
        }

        public OperationStatus AddOrderHistory(OrderAction action, string description, long orderId, int userId, long? orderItemId = null)
        {
            var orderHistory = new OrderHistory()
            {
                Action = action,
                ActionDate = DateTime.UtcNow,
                Description = description,
                OrderId = orderId,
                OrderItemId = orderItemId,
                UserId = userId
            };

            return _orderHistoryRepository.Create(orderHistory);
        }

        public OperationStatus Create(OrderHistory orderHistory)
        {
            try
            {
                _dataContext.Set<OrderHistory>().Add(orderHistory);

                _dataContext.SaveChanges();

                return new OperationStatus { Status = true };
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IQueryable<OrderHistory> GetAllHistoryByOrderId(long orderId)
        {

            return _orderHistoryRepository.GetList().Where(o => o.OrderId == orderId);
        }

        public IQueryable<OrderHistory> GetAllHistoryByOrderItemId(long orderId, long orderItemId)
        {
            return _orderHistoryRepository.GetList().Where(o => o.OrderId == orderId && (o.OrderItemId.HasValue == false || o.OrderItemId.Value == orderItemId));
        }

        public IQueryable<OrderHistory> GetCancelHistory(long orderId, long orderItemId)
        {
            return GetAllHistoryByOrderItemId(orderId, orderItemId).Where(h => h.Action == OrderAction.Canceled);
        } 
    }
}
