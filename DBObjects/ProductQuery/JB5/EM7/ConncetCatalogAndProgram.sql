

Alter TABLE [dbo].[Programs] add [CatalogId] [int] NULL
 
GO

ALTER TABLE [dbo].[Programs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Programs_dbo.CatalogItems_CatalogId] FOREIGN KEY([CatalogId])
REFERENCES [dbo].[CatalogItems] ([Id])
GO

ALTER TABLE [dbo].[Programs] CHECK CONSTRAINT [FK_dbo.Programs_dbo.CatalogItems_CatalogId]
