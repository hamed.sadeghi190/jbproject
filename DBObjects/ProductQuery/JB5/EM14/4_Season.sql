alter TABLE [dbo].[Seasons] add [IsSandBox] [bit]  NULL
go
update [dbo].[Seasons] set IsSandBox=0
go
alter TABLE [dbo].[Seasons] alter column  [IsSandBox] [bit] NOt NULL
go

alter TABLE [dbo].[Charges] add [AmountType] [int]  NULL
go
update [dbo].[Charges] set [AmountType]=0
go
alter TABLE [dbo].[Charges] alter column [AmountType] [int] NOT  NULL
go
alter table [dbo].[Charges] add [ClubId] [int] NULL
 go

ALTER TABLE [dbo].[Charges]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Charges_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
GO

ALTER TABLE [dbo].[Charges] CHECK CONSTRAINT [FK_dbo.Charges_dbo.Clubs_ClubId]
GO

alter TABLE [dbo].[Clients] add [ConvenienceFeeType] [int]  NULL
go
update [dbo].[Clients] set [ConvenienceFeeType]=1
go
alter TABLE [dbo].[Clients] alter column [ConvenienceFeeType] [int] NOT NULL
go
alter TABLE [dbo].[Clients] add 	[ConvenienceFeeLabel] [nvarchar](max) NULL
go
alter TABLE [dbo].[Clients] add [EnableCartDonation] [bit]  NULL
go 
update [dbo].[Clients] set [EnableCartDonation]=[EnableDonation]
go
alter TABLE [dbo].[Clients] alter column [EnableCartDonation] [bit] NOT NULL
go
alter table charges alter column [ProgramScheduleId] [bigint]  NULL