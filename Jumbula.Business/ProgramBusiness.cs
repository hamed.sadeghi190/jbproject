﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Domain.Generic;
using Jumbula.Core.Model.Charge;
using Jumbula.Core.Model.Generic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Expressions;
using TimeZones = Jumbula.Common.Enums.TimeZone;
using Jumbula.Core.Model.Program;
using Force.DeepCloner;
using OrderChargeDiscount = Jumbula.Core.Domain.OrderChargeDiscount;

namespace Jumbula.Business
{
    public class ProgramBusiness : IProgramBusiness
    {
        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private ISubscriptionBusiness _subscriptionBusiness;
        private readonly IAttributeBusiness _attributeBusiness;
        private IOrderBusiness _orderBusiness;
        private IOrderItemBusiness _orderItemBusiness;
        private IOrderSessionBusiness _orderSessionBusiness;
        private IProgramSessionBusiness _programSessionBusiness;
        private ISeasonBusiness _seasonBusiness;
        private readonly IImageGalleryBusiness _imageGalleryBusiness;
        private readonly ICategoryBusiness _categoryBusiness;
        private readonly IRepository<Program, long> _programRepository;
        private readonly IEntitiesContext _dataContext;
        private readonly ILocationBusiness _locationBusiness;
        private readonly IRepository<OrderChargeDiscount, long> _orderChargeDiscountBusiness;
        #endregion

        #region Constractors
        public ProgramBusiness(IClubBusiness clubBusiness,
            IAttributeBusiness attributeBusiness,
            IImageGalleryBusiness imageGalleryBusiness,
            ICategoryBusiness categoryBusiness,
            IRepository<Program, long> programRepository,
            IEntitiesContext dataContext,
            ILocationBusiness locationBusiness,
            IRepository<OrderChargeDiscount, long> orderChargeDiscountBusiness)
        {
            _clubBusiness = clubBusiness;

            _attributeBusiness = attributeBusiness;
            _imageGalleryBusiness = imageGalleryBusiness;
            _categoryBusiness = categoryBusiness;
            _programRepository = programRepository;
            _dataContext = dataContext;
            _locationBusiness = locationBusiness;
            _orderChargeDiscountBusiness = orderChargeDiscountBusiness;
        }
        #endregion
        public ISubscriptionBusiness SubscriptionBusiness
        {
            get { return _subscriptionBusiness; }
            set { _subscriptionBusiness = value; }
        }

        public ISeasonBusiness SeasonBusiness
        {
            get { return _seasonBusiness; }
            set { _seasonBusiness = value; }
        }

        public IOrderBusiness OrderBusiness
        {
            get { return _orderBusiness; }
            set { _orderBusiness = value; }
        }

        public IOrderItemBusiness OrderItemBusiness
        {
            get { return _orderItemBusiness; }
            set { _orderItemBusiness = value; }
        }

        public IOrderSessionBusiness OrderSessionBusiness
        {
            get { return _orderSessionBusiness; }
            set { _orderSessionBusiness = value; }
        }

        public IProgramSessionBusiness ProgramSessionBusiness
        {
            get { return _programSessionBusiness; }
            set { _programSessionBusiness = value; }
        }


        #region ProgramName
        public string FormatTuitionOption(string tutionName, decimal amount, DateTime startDate, DateTime endDate)
        {
            return
                $"{tutionName} ({startDate.ToString(Constants.DefaultDateFormat)}-{endDate.ToString(Constants.DefaultDateFormat)}) - {amount.ToString("C2")}";
        }

        public string GetEmailProgramDateWithScheduleTitle(ProgramSchedule schedule)
        {
            var name = schedule.Title != null ? $"{schedule.Title} ({schedule.StartDate.ToString(Constants.DateTime_Comma)} - {schedule.EndDate.ToString(Constants.DateTime_Comma)})"
                : $"{schedule.StartDate.ToString(Constants.DateTime_Comma)} - {schedule.EndDate.ToString(Constants.DateTime_Comma)}";

            return name;
        }

        public string GetProgramScheduleTitleAndDate(OrderItem orderItem, string chessSchedule = "")
        {
            var result = string.Empty;

            switch (orderItem.ProgramSchedule.Program.TypeCategory)
            {
                case ProgramTypeCategory.BeforeAfterCare:

                    if (orderItem.Mode != OrderItemMode.DropIn && orderItem.Mode != OrderItemMode.PunchCard)
                    {
                        result = $"{(!string.IsNullOrEmpty(orderItem.ProgramSchedule.Title) ? $"{orderItem.ProgramSchedule.Title} " : string.Empty)}";
                        break;
                    }
                    else
                    {
                        result = $"{DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}";
                        break;
                    }
                case ProgramTypeCategory.Camp:
                    result =
                        $"{(!string.IsNullOrEmpty(orderItem.ProgramSchedule.Title) ? $"{orderItem.ProgramSchedule.Title} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)})" : $"{DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}")}";
                    break;
                case ProgramTypeCategory.ChessTournament:
                    result =
                    $"{(!string.IsNullOrEmpty(orderItem.ProgramSchedule.Title) ? $"{orderItem.ProgramSchedule.Title},{chessSchedule} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.Start.Value)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.End.Value)})" : $"{chessSchedule} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.Start.Value)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.End.Value)})")}";
                    break;
                case ProgramTypeCategory.Class:
                case ProgramTypeCategory.SeminarTour:
                case ProgramTypeCategory.Subscription:
                    result =
                        $"{DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}";
                    break;
                case ProgramTypeCategory.Calendar:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return result;
        }
        public string GetProgramNameWithScheduleTitleAndDate(OrderItem orderItem, string tutionName, string chessSchedule = "")
        {
            var result = string.Empty;

            switch (orderItem.ProgramSchedule.Program.TypeCategory)
            {
                case ProgramTypeCategory.BeforeAfterCare:

                    if (orderItem.Mode != OrderItemMode.DropIn && orderItem.Mode != OrderItemMode.PunchCard)
                    {
                        result =
                           $"{(!string.IsNullOrEmpty(orderItem.ProgramSchedule.Title) ? $"{orderItem.ProgramSchedule.Program.Name}, {orderItem.ProgramSchedule.Title} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}), {tutionName}" : $"{orderItem.ProgramSchedule.Program.Name} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}), {tutionName}")}";
                        break;
                    }
                    else
                    {
                        result =
                       $"{(!string.IsNullOrEmpty(orderItem.ProgramSchedule.Title) ? $"{orderItem.ProgramSchedule.Program.Name} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {orderItem.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma)}), {tutionName}" : $"{orderItem.ProgramSchedule.Program.Name} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}), {tutionName}")}";
                        break;
                    }
                case ProgramTypeCategory.Camp:
                    result =
                        $"{(!string.IsNullOrEmpty(orderItem.ProgramSchedule.Title) ? $"{orderItem.ProgramSchedule.Program.Name}, {orderItem.ProgramSchedule.Title} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}), {tutionName}" : $"{orderItem.ProgramSchedule.Program.Name} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}), {tutionName}")}";
                    break;
                case ProgramTypeCategory.Class:
                case ProgramTypeCategory.SeminarTour:
                case ProgramTypeCategory.Subscription:
                    result =
                       $"{orderItem.ProgramSchedule.Program.Name} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.StartDate)} - { DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.ProgramSchedule.EndDate)}), {tutionName}";

                    break;
                case ProgramTypeCategory.Calendar:
                    break;
                case ProgramTypeCategory.ChessTournament:
                    result =
                      $"{($"{orderItem.ProgramSchedule.Program.Name}, {chessSchedule} ({DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.Start.Value)} - {DateTimeHelper.FormatShortDateSeperetedByCamma(orderItem.End.Value)}), {tutionName}")}";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result;
        }

        public string GetProgramDateWithScheduleTitleAndSchedulePart(ProgramSchedule schedule, ProgramSchedulePart programSchedulePart)
        {
            var result = string.Empty;

            switch (schedule.Program.TypeCategory)
            {
                case ProgramTypeCategory.BeforeAfterCare:
                case ProgramTypeCategory.Subscription:
                    result = programSchedulePart != null ? $"{(!string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Title} ({programSchedulePart.StartDate.ToString(Constants.DateTime_Comma)},{programSchedulePart.EndDate.ToString(Constants.DateTime_Comma)}) " : $"{programSchedulePart.StartDate.ToString(Constants.DateTime_Comma)},{programSchedulePart.EndDate.ToString(Constants.DateTime_Comma)}")}" : $"{schedule.Title}";
                    break;
                case ProgramTypeCategory.Camp:
                    result =
                        $"{(!string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Title} ({schedule.StartDate.ToString(Constants.DateTime_Comma)},{schedule.EndDate.ToString(Constants.DateTime_Comma)}) " : $"{schedule.StartDate.ToString(Constants.DateTime_Comma)},{schedule.EndDate.ToString(Constants.DateTime_Comma)}")}";
                    break;
                case ProgramTypeCategory.Class:
                    break;
                case ProgramTypeCategory.SeminarTour:
                    break;
                case ProgramTypeCategory.Calendar:
                    break;
                case ProgramTypeCategory.ChessTournament:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result;
        }

        public string GetScheduleStartEndDate(ProgramSchedule schedule)
        {
            return
                $"{schedule.StartDate.ToString(Constants.DateTime_Comma)} - {schedule.EndDate.ToString(Constants.DateTime_Comma)}";
        }

        public string GetEmailProgramNameOnlyTuition(ProgramSchedule schedule, string tutionName)
        {
            return $"{schedule.Program.Name} - {tutionName}";
        }

        public string GetProgramNameCustomReport(ProgramSchedule schedule, string tutionName)
        {
            string result;
            if (schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                result = $"{schedule.Program.Name} - {schedule.Title} ({tutionName})";
            }
            else if (schedule.Program.TypeCategory == ProgramTypeCategory.Camp)
            {
                result =
                    $"{(!string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Program.Name}, {schedule.Title} /" : $"{schedule.Program.Name},")} {schedule.StartDate.ToString(Constants.DateTime_Comma)} - {schedule.EndDate.ToString(Constants.DateTime_Comma)} ({tutionName})";
            }
            else
            {
                result = $"{schedule.Program.Name} ({tutionName})";
            }

            return result;
        }
        public string GetEmailProgramDate(ProgramSchedule schedule)
        {

            return
                $"{schedule.StartDate.ToString(Constants.DateTime_Comma)} - {schedule.EndDate.ToString(Constants.DateTime_Comma)}";
        }

        public string GetEmailProgramName(ProgramSchedule schedule, string tutionName)
        {
            return $"{schedule.Program.Name} - {tutionName}";
        }

        public string GetProgramNameOnlyTuition(ProgramSchedule schedule, string tutionName)
        {
            return $"{schedule.Program.Name} ({tutionName})";
        }

        public string GetProgramName(OrderItem orderItem)
        {
            var entryFeeName = orderItem.OrderChargeDiscounts.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee) != null ? orderItem.OrderChargeDiscounts.First(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Name : string.Empty;

            var result = orderItem.ProgramTypeCategory == ProgramTypeCategory.Camp ? GetProgramName(orderItem.ProgramSchedule, entryFeeName) : GetProgramName(orderItem.ProgramSchedule.Program.Name, entryFeeName);

            return result;
        }

        public string GetProgramName(ProgramSchedule schedule)
        {
            return
                $"{schedule.Program.Name} ({schedule.StartDate.ToString(Constants.DateTime_Comma)} - {schedule.EndDate.ToString(Constants.DateTime_Comma)})";
        }

        public string GetProgramName(string programName, DateTime startDate, DateTime endDate, string tutionName)
        {
            return
                $"{programName} ({startDate.ToString(Constants.DateTime_Comma)} - {endDate.ToString(Constants.DateTime_Comma)}), {tutionName}";
        }

        public string GetProgramName(string programName, string tutionName)
        {
            return $"{programName} ({tutionName})";
        }

        public string GetProgramName(ProgramSchedule schedule, string tutionName)
        {
            return
                $"{schedule.Program.Name} ({schedule.StartDate.ToString(Constants.DateTime_Comma)} - {schedule.EndDate.ToString(Constants.DateTime_Comma)}), {tutionName}";
        }
        #endregion

        public string GradeRestrictionMessage(SchoolGradeType? minGrade, SchoolGradeType? maxGrade, bool applyAtProgramStart)
        {
            string message = string.Empty;

            if (minGrade.HasValue && maxGrade.HasValue)
            {
                if (minGrade.Value == maxGrade.Value)
                {
                    message = applyAtProgramStart ? $"Participants must be entering grades {minGrade.Value.ToDescription().ToLower()}."
                        : $"Participants must currently be in {minGrade.Value.ToDescription().ToLower()} grade.";
                }
                else
                {
                    message = applyAtProgramStart ? $"Participants must be entering grades {minGrade.Value.ToDescription().ToLower()} to {maxGrade.Value.ToDescription()}."
                        : $"Participants must currently be in grades {minGrade.Value.ToDescription().ToLower()} to {maxGrade.Value.ToDescription()}.";

                }
            }
            else if (minGrade.HasValue == false && maxGrade.HasValue)
            {
                message = applyAtProgramStart ? $"Participants must be entering {maxGrade.Value.ToDescription().ToLower()} grade or lower."
                    : $"Participants must currently be in {maxGrade.Value.ToDescription().ToLower()} grade or lower.";

            }
            else if (minGrade.HasValue)
            {
                message = applyAtProgramStart ? $"Participants must be entering {minGrade.Value.ToDescription().ToLower()} grade or higher."
                    : $"Participants must currently be in {minGrade.Value.ToDescription().ToLower()} grade or higher.";
            }
            return message;
        }

        public bool HasGradeRestriction(ProgramSchedule programSchedule)
        {
            return programSchedule.AttendeeRestriction.RestrictionType == RestrictionType.Grade;
        }

        public bool HasAgeRestriction(ProgramSchedule programSchedule)
        {
            return programSchedule.AttendeeRestriction.RestrictionType == RestrictionType.Age;
        }

        public string AgeRestrictionMessage(int? minAge, int? maxAge, bool applyAtProgramStart)
        {
            string message = string.Empty;

            if (minAge.HasValue && maxAge.HasValue)
            {
                if (minAge.Value == maxAge.Value)
                {
                    message = applyAtProgramStart ? $"Participants must be {minAge.Value.ToString()} years old when the program starts."
                        : $"Participants must currently be {minAge.Value.ToString()} years old.";
                }
                else
                {
                    message = applyAtProgramStart ? $"Participants must be {minAge.Value.ToString()} to {maxAge.Value.ToString()} when the program starts."
                        : $"Participants must currently be {minAge.Value.ToString()} to {maxAge.Value.ToString()} years old.";

                }
            }
            else if (minAge.HasValue == false && maxAge.HasValue)
            {
                message = applyAtProgramStart ? $"Participants must be {maxAge.Value.ToString()} years or younger when the program starts."
                    : $"Participants must currently be {maxAge.Value.ToString()} years or younger.";

            }
            else if (minAge.HasValue)
            {
                message = applyAtProgramStart ? $"Participants must be {minAge.Value.ToString()} years or older when the program starts."
                    : $"Participants must currently be {minAge.Value.ToString()} years or older.";
            }

            return message;
        }

        IQueryable<Program> IProgramBusiness.GetList()
        {
            return _programRepository.GetList(p => p.Status != ProgramStatus.Deleted);
        }

        IQueryable<Program> IProgramBusiness.GetList(IEnumerable<long> programs)
        {
            return _programRepository.GetList().Where(p => programs.Contains(p.Id));
        }

        public void CheckProgramCapacity(IEnumerable<OrderItem> orderItems)
        {
            var isTestMode = !orderItems.First().Order.IsLive;

            foreach (var item in orderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.notInitiated && o.ProgramScheduleId.HasValue && o.ProgramSchedule.Program.TypeCategory != ProgramTypeCategory.Subscription).GroupBy(c => c.ProgramSchedule))
            {
                var chargeWithCapacity = item.Key.Charges.Where(c => c.Capacity <= 0 && !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Select(c => c.Name);
                foreach (var charge in item.ToList().GroupBy(c => c.EntryFeeName))
                {
                    var orderCharge = charge.First().OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee);
                    var chargeItem = item.Key.Charges.FirstOrDefault(c => c.Id == orderCharge.ChargeId);

                    var capacityLeft = CapacityLeft(item.Key, orderCharge.ChargeId, isTestMode);

                    var itemcount = 0;
                    if (chargeItem != null && chargeItem.Capacity > 0)
                    {
                        itemcount = charge.Count();
                        if (capacityLeft.HasValue && capacityLeft.Value < itemcount)
                        {
                            if (!item.Key.Program.HasRegisterPIN)
                            {
                                charge.OrderByDescending(c => c.Id).Take(itemcount - capacityLeft.Value).ToList().ForEach(c => c.IsProgramFull = true);
                            }
                            else
                            {
                                if (item.Key.Program.DisableCapacityRestriction)
                                {
                                    foreach (var orderItem in charge)
                                    {
                                        if (orderItem.PassedPin != orderItem.ProgramSchedule.Program.RegisterPIN)
                                        {
                                            orderItem.IsProgramFull = true;
                                        }
                                    }
                                }
                                else
                                {
                                    charge.OrderByDescending(c => c.Id).Take(itemcount - capacityLeft.Value).ToList().ForEach(c => c.IsProgramFull = true);
                                }
                            }
                        }

                    }
                    else if (item.Key.Attributes.Capacity > 0)
                    {
                        if (!item.Key.Program.DisableCapacityRestriction)
                        {
                            itemcount = item.Where(c => chargeWithCapacity.Contains(c.EntryFeeName)).Count();
                            if (capacityLeft.HasValue && capacityLeft.Value < itemcount)
                            {
                                item.Where(c => chargeWithCapacity.Contains(c.EntryFeeName))
                                    .OrderByDescending(c => c.Id).Take(itemcount - capacityLeft.Value).ToList()
                                    .ForEach(c => c.IsProgramFull = true);
                            }
                        }
                        else
                        {
                            foreach (var orderItem in charge)
                            {
                                if (orderItem.PassedPin != orderItem.ProgramSchedule.Program.RegisterPIN)
                                {
                                    orderItem.IsProgramFull = true;
                                }
                            }
                        }
                    }
                }
            }

            #region Subscription
            var subscriptionBusiness = _subscriptionBusiness;

            foreach (var item in orderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.notInitiated && o.ProgramScheduleId.HasValue && o.ProgramSchedule != null && (o.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription || o.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)))
            {
                item.IsProgramFull = false;
            }
            #endregion

        }

        public List<ClassSessionsDateTime> GetClassDates(ProgramSchedule programSchedule, TimeOfClassFormation timeMode = TimeOfClassFormation.None)
        {
            var club = programSchedule.Program.Club;
            List<string> noClassDates = GetNoClassDates(programSchedule.Program);

            List<ClassSessionsDateTime> classdates = null;
            if (programSchedule.Sessions != null && programSchedule.Sessions.Any())
            {
                classdates = new List<ClassSessionsDateTime>();

                foreach (var session in programSchedule.Sessions.Where(c => c.IsDeleted == false))
                {
                    var dayOfWeek = session.Start.DayOfWeek;
                    var newItem = new ClassSessionsDateTime()
                    {
                        Id = session.Id,
                        SessionDate = session.Start,
                        TimeChanges = false,
                        StartTime = session.Start,
                        EndTime = session.End
                    };
                    var scheduleDay = ((ScheduleAttribute)programSchedule.Attributes).Days.FirstOrDefault(c => c.DayOfWeek == dayOfWeek);
                    if (scheduleDay != null)
                    {
                        if (scheduleDay.StartTime.HasValue && scheduleDay.StartTime.Value != session.Start.TimeOfDay)
                        {
                            newItem.TimeChanges = true;
                        }
                        if (scheduleDay.EndTime.HasValue && scheduleDay.EndTime.Value != session.End.TimeOfDay)
                        {
                            newItem.TimeChanges = true;
                        }
                    }
                    classdates.Add(newItem);
                }
            }

            if (classdates != null && classdates.Any())
            {
                classdates = classdates.OrderBy(c => c.SessionDate).ToList();
                if (noClassDates != null && noClassDates.Any())
                {
                    foreach (var item in noClassDates)
                    {
                        var deletedItem = classdates.FirstOrDefault(c => !string.IsNullOrEmpty(item) && c.SessionDate.Date.Equals(DateTime.Parse(item)));
                        if (deletedItem != null)
                        {
                            classdates.Remove(deletedItem);
                        }
                    }
                }
            }

            classdates = timeMode != TimeOfClassFormation.None ? ApplyHolidaysToClassDates(club, classdates, timeMode) : classdates;

            return classdates;
        }

        public List<ClassSessionsDateTime> ApplyHolidaysToClassDates(Club club, List<ClassSessionsDateTime> classdates, TimeOfClassFormation timeMode)
        {
            List<string> holidayDates = club.Setting.ListOfHolidayDates;
            List<string> holidayAMDates = club.Setting.ListOfHolidayAMDates;
            List<string> holidayPMDates = club.Setting.ListOfHolidayPMDates;

            if (classdates != null && classdates.Any())
            {
                classdates = classdates.OrderBy(c => c.SessionDate).ToList();

                if (holidayDates != null && holidayDates.Any())
                {
                    classdates.RemoveAll(c => holidayDates.Where(d => d != String.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.SessionDate.Date));
                }

                if (holidayAMDates != null && holidayAMDates.Any())
                {
                    classdates.RemoveAll(c => holidayAMDates.Where(d => d != String.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.SessionDate.Date) && DateTimeHelper.IsTimeAm(c.StartTime.TimeOfDay));
                }

                if (holidayPMDates != null && holidayPMDates.Any())
                {
                    classdates.RemoveAll(c => holidayPMDates.Where(d => d != String.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.SessionDate.Date) && !DateTimeHelper.IsTimeAm(c.StartTime.TimeOfDay));
                }
            }

            return classdates;
        }

        public List<ScheduleSession> GetClassHolidayDates(Program program)
        {
            List<string> holidayDates = program.Club.Setting.ListOfHolidayDates;
            List<string> holidayAMDates = program.Club.Setting.ListOfHolidayAMDates;
            List<string> holidayPMDates = program.Club.Setting.ListOfHolidayPMDates;
            var allClassSessions = GetAllClassSessions(program);

            var classHalidays = new List<ScheduleSession>();

            if (allClassSessions != null && allClassSessions.Any())
            {

                if (holidayDates != null && holidayDates.Any())
                {
                    classHalidays.AddRange(allClassSessions.Where(c => holidayDates.Where(d => d != String.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.Start.Date)));
                }

                if (holidayAMDates != null && holidayAMDates.Any())
                {
                    classHalidays.AddRange(allClassSessions.Where(c => holidayAMDates.Where(d => d != String.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.Start.Date) && DateTimeHelper.IsTimeAm(c.Start.TimeOfDay)));
                }

                if (holidayPMDates != null && holidayPMDates.Any())
                {
                    classHalidays.AddRange(allClassSessions.Where(c => holidayPMDates.Where(d => d != String.Empty).Select(d => DateTime.Parse(d)).ToList().Contains(c.Start.Date) && !DateTimeHelper.IsTimeAm(c.Start.TimeOfDay)));
                }
            }

            return classHalidays;
        }

        public TimeOfClassFormation GetClassScheduleTime(Program program)
        {
            var timeProgram = new TimeOfClassFormation();
            var allDays = GetClassDays(program);
            var amTimes = new List<TimeSpan>();
            var pmTimes = new List<TimeSpan>();

            foreach (var day in allDays)
            {
                if (day.StartTime.HasValue)
                {
                    if (DateTimeHelper.IsTimeAm(day.StartTime.Value))
                    {
                        amTimes.Add(day.StartTime.Value);
                    }
                    else
                    {
                        pmTimes.Add(day.StartTime.Value);
                    }
                }
            }

            if (amTimes.Count > 0 && pmTimes.Count > 0 || amTimes.Count == 0 && pmTimes.Count == 0)
            {
                timeProgram = TimeOfClassFormation.Both;
            }
            else if (amTimes.Count > 0)
            {
                timeProgram = TimeOfClassFormation.AM;
            }
            else
            {
                timeProgram = TimeOfClassFormation.PM;
            }

            return timeProgram;
        }

        public List<string> GetNoClassDates(Program program)
        {
            var result = new List<string>();

            if (program.Club.ClubType != null && (program.Club.ClubType.EnumType == ClubTypesEnum.School) || (program.Club.ClubType.ParentType != null && program.Club.ClubType.ParentType.EnumType == ClubTypesEnum.School))
            {
                if (((SeasonSchoolSetting)program.Season.Setting).SchoolSeasonscheduleSetting != null)
                {
                    result = ((SeasonSchoolSetting)program.Season.Setting).SchoolSeasonscheduleSetting.ListOfNoClassDates;
                }
            }

            return result;
        }

        public List<ProgramScheduleDay> GetClassDays(Program program)
        {
            List<ProgramScheduleDay> result = new List<ProgramScheduleDay>();

            if (program.TypeCategory == ProgramTypeCategory.ChessTournament || program.TypeCategory == ProgramTypeCategory.Calendar || program.TypeCategory == ProgramTypeCategory.SeminarTour)
            {
                return result;
            }
            else if (program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                {
                    var scheduleDays = ((ScheduleSubscriptionAttribute)(schedule.Attributes)).Days;

                    result.AddRange(scheduleDays);
                }
            }
            else if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both))
                {
                    var scheduleDays = ((ScheduleAfterBeforeCareAttribute)(schedule.Attributes)).Days;

                    result.AddRange(scheduleDays);
                }
            }
            else
            {
                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                {
                    var scheduleDays = ((ScheduleAttribute)(schedule.Attributes)).Days;

                    result.AddRange(scheduleDays);
                }
            }

            return result;
        }

        public List<ProgramScheduleDay> GetClassDays(Program program, DateTime date)
        {
            List<ProgramScheduleDay> result = new List<ProgramScheduleDay>();

            if (program.TypeCategory == ProgramTypeCategory.ChessTournament || program.TypeCategory == ProgramTypeCategory.Calendar || program.TypeCategory == ProgramTypeCategory.SeminarTour)
            {
                return result;
            }
            else if (program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                {
                    var scheduleDays = ((ScheduleSubscriptionAttribute)(schedule.Attributes)).Days;

                    result.AddRange(scheduleDays);
                }
            }
            else if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both))
                {
                    var scheduleDays = ((ScheduleAfterBeforeCareAttribute)(schedule.Attributes)).Days;

                    result.AddRange(scheduleDays);
                    break;
                }
            }
            else if (program.TypeCategory == ProgramTypeCategory.Camp)
            {
                var status = false;

                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                {
                    if (date >= schedule.StartDate && date <= schedule.EndDate)
                    {
                        status = true;
                        var scheduleDays = ((ScheduleAttribute)(schedule.Attributes)).Days;

                        result.AddRange(scheduleDays);
                        break;
                    }
                }

                if (!status)
                {
                    var scheduleDays = ((ScheduleAttribute)(program.ProgramSchedules.FirstOrDefault().Attributes)).Days;

                    result.AddRange(scheduleDays);
                }
            }
            else
            {
                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                {
                    var scheduleDays = ((ScheduleAttribute)(schedule.Attributes)).Days;

                    result.AddRange(scheduleDays);
                }
            }

            return result;
        }

        public List<ProgramScheduleDay> GetClassDays(ProgramSchedule programSchedule)
        {
            List<ProgramScheduleDay> result = new List<ProgramScheduleDay>();

            if (programSchedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament || programSchedule.Program.TypeCategory == ProgramTypeCategory.Calendar || programSchedule.Program.TypeCategory == ProgramTypeCategory.SeminarTour)
            {
                return result;
            }
            else if (programSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription)
            {

                var scheduleDays = ((ScheduleSubscriptionAttribute)(programSchedule.Attributes)).Days;

                result.AddRange(scheduleDays);

            }
            else if (programSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                var scheduleDays = ((ScheduleAfterBeforeCareAttribute)(programSchedule.Attributes)).Days;

                result.AddRange(scheduleDays);
            }
            else if (programSchedule.Program.TypeCategory == ProgramTypeCategory.Camp)
            {

                var scheduleDays = ((ScheduleAttribute)(programSchedule.Attributes)).Days;

                result.AddRange(scheduleDays);
            }
            else
            {
                var scheduleDays = ((ScheduleAttribute)(programSchedule.Attributes)).Days;

                result.AddRange(scheduleDays);
            }

            return result;
        }


        public IQueryable<Program> GetList(JbPSeasonCalendar programTypeCategory)
        {
            var result = _programRepository.GetList(p =>
                (
                    (programTypeCategory.IncludeClasses && p.TypeCategory == ProgramTypeCategory.Class) ||
                    (programTypeCategory.IncludeCamp && p.TypeCategory == ProgramTypeCategory.Camp) ||
                    (programTypeCategory.IncludeCalendar && p.TypeCategory == ProgramTypeCategory.Calendar) ||
                    (programTypeCategory.IncludeSeminarTour && p.TypeCategory == ProgramTypeCategory.SeminarTour) ||
                    (programTypeCategory.IncludeChessTournament && p.TypeCategory == ProgramTypeCategory.ChessTournament)) &&
                    (p.Status != ProgramStatus.Deleted && p.SeasonId == programTypeCategory.SeasonId && p.LastCreatedPage >= ProgramPageStep.Step4)
                );

            return result;
        }

        public IQueryable<Program> GetList(JbPSeasonGrid programTypeCategory)
        {
            var result = _programRepository.GetList(p =>
                (
                    (programTypeCategory.IncludeBeforeAfter && p.TypeCategory == ProgramTypeCategory.BeforeAfterCare) ||
                    (programTypeCategory.IncludeClasses && p.TypeCategory == ProgramTypeCategory.Class) ||
                    (programTypeCategory.IncludeCamp && p.TypeCategory == ProgramTypeCategory.Camp) ||
                    (programTypeCategory.IncludeCalendar && p.TypeCategory == ProgramTypeCategory.Calendar) ||
                    (programTypeCategory.IncludeSeminarTour && p.TypeCategory == ProgramTypeCategory.SeminarTour) ||
                    (programTypeCategory.IncludeChessTournament && p.TypeCategory == ProgramTypeCategory.ChessTournament)) &&
                    (p.Status != ProgramStatus.Deleted && p.SeasonId == programTypeCategory.SeasonId && p.LastCreatedPage >= ProgramPageStep.Step4)
                );

            return result;
        }

        public Program Get(long id)
        {
            return _programRepository.GetList().SingleOrDefault(p => p.Id == id);
        }

        public Program Get(long id, params Expression<Func<Program, object>>[] includes)
        {
            return _programRepository.GetList(includes).SingleOrDefault(p => p.Id == id);
        }

        public Program Get(int clubId, string seasonDomain, string domain)
        {
            return GetList(clubId).Include(p => p.ClubFormTemplates).SingleOrDefault(p => p.Domain.ToLower().Equals(domain.ToLower()) &&
                                                                                   p.Season.Domain.ToLower().Equals(seasonDomain.ToLower()) &&
                                                                                   p.Status != ProgramStatus.Deleted);
        }

        public Program GetByScheduleId(long scheduleId)
        {
            Program result = null;

            var schedule = _dataContext.Set<ProgramSchedule>().SingleOrDefault(s => s.Id == scheduleId);
            result = schedule != null ? schedule.Program : null;

            return result;
        }

        public IQueryable<Program> GetListWithOutSources(int clubId)
        {
            var query = _programRepository.GetList(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage == ProgramPageStep.Step4 && (p.OutSourceSeasonId.HasValue && p.OutSourceSeason.ClubId == clubId || p.Season.ClubId == clubId));

            return query;
        }

        public IQueryable<Program> GetList(int clubId)
        {
            return _programRepository.GetList(p => p.Status != ProgramStatus.Deleted &&
                                p.Season.ClubId == clubId);
        }
        public IQueryable<Program> GetList(List<int> clubIds)
        {
            return _programRepository.GetList(p => p.Status != ProgramStatus.Deleted &&
                               clubIds.Contains(p.Season.ClubId));
        }
        public IQueryable<Program> GetListBySeasonId(long seasonId)
        {
            return _programRepository.GetList(p => p.Status != ProgramStatus.Deleted &&
                               (p.SeasonId == seasonId || (p.OutSourceSeasonId.HasValue && p.OutSourceSeasonId.Value == seasonId)));
        }

        public IQueryable<Program> GetListBySeasonId(List<long> seasonIds)
        {
            return _programRepository.GetList(p => p.Status != ProgramStatus.Deleted && p.LastCreatedPage == ProgramPageStep.Step4 &&
                                                   (seasonIds.Contains(p.SeasonId) || (seasonIds.Contains(p.OutSourceSeasonId.Value))));
        }

        public IQueryable<Program> GetList(int clubId, string seasonDomain = "", string programName = "", DateTime? startDateCreated = null, DateTime? endDateCreated = null, SaveType? saveType = null, ProgramTypeCategory? typeCategory = null, int? instructorId = null)
        {
            var club = _clubBusiness.Get(clubId);
            Season season = null;

            if (!String.IsNullOrEmpty(seasonDomain))
            {
                season = club.Seasons.Single(s => s.Status != SeasonStatus.Deleted && s.Domain.Equals(seasonDomain, StringComparison.OrdinalIgnoreCase));
            }

            var result = _programRepository.GetList(p => p.Status != ProgramStatus.Deleted && p.Season.Status != SeasonStatus.Deleted &&
                               (p.Season.ClubId == clubId || (p.OutSourceSeason != null && p.OutSourceSeason.ClubId == clubId)));

            if (season != null)
            {
                result = result.Where(p => p.Season.Id == season.Id || (p.OutSourceSeason != null && p.OutSourceSeason.Id == season.Id));
            }

            if (!String.IsNullOrEmpty(programName))
            {
                result = result.Where(r => r.Name.ToLower().Contains(programName.ToLower()));
            }

            if (startDateCreated.HasValue)
            {
                result = result.Where(r => r.MetaData.DateCreated >= startDateCreated.Value);
            }

            if (endDateCreated.HasValue)
            {
                result = result.Where(r => r.MetaData.DateCreated <= endDateCreated.Value);
            }

            if (saveType.HasValue)
            {
                if (saveType == SaveType.Publish)
                {
                    result = result.Where(r => r.LastCreatedPage >= ProgramPageStep.Step4);
                }
                else
                {
                    result = result.Where(r => r.LastCreatedPage < ProgramPageStep.Step4);
                }
            }

            if (typeCategory.HasValue)
            {
                result = result.Where(r => r.TypeCategory == typeCategory);
            }

            if (instructorId.HasValue)
            {
                result = result.Where(r => r.Instructors.Any(c => c.Id == instructorId.Value));
            }

            result = result.OrderByDescending(o => o.MetaData.DateUpdated);

            return result;
        }

        public IQueryable<Program> GetInstructorPrograms(int instructorId)
        {
            return _programRepository.GetList().Where(p => p.Instructors.Any(c => c.Id == instructorId));
        }

        public IQueryable<Program> GetInstructorPrograms(int instructorId, int clubId)
        {
            return GetList(clubId, null, null, null, null, null, null, instructorId);
        }

        public OperationStatus Create(Program program)
        {
            try
            {
                program.MetaData = new MetaData
                {
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow
                };

                _dataContext.Set<Program>().Add(program);

                _dataContext.SaveChanges();

                return new OperationStatus { Status = true };
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public OperationStatus Update(Program program)
        {
            var originalProgram = _dataContext.Set<Program>().SingleOrDefault(p => p.Id == program.Id);

            originalProgram = program;

            originalProgram.MetaData.DateUpdated = DateTime.UtcNow;
            if (program.IsCustomFieldChanged)
            {
                originalProgram.CustomFieldsSerialized = JsonConvert.SerializeObject(program.CustomFields);
            }

            return _programRepository.Save();
        }

        public OperationStatus Update(Program program, List<ProgramSchedule> schedules, List<SelectKeyValue<int>> numberOfClassDays = null)
        {
            var originalProgram = _dataContext.Set<Program>().SingleOrDefault(p => p.Id == program.Id);

            originalProgram = program;

            originalProgram.MetaData.DateUpdated = DateTime.UtcNow;

            foreach (var item in originalProgram.ProgramSchedules)
            {
                var originalSchedule = _dataContext.Set<ProgramSchedule>().SingleOrDefault(i => i.Id == item.Id);
                if (!schedules.Any(s => s.Id == item.Id))
                {
                    item.IsDeleted = true;

                    if (item.OrderItems.Any(o => o.ItemStatus == OrderItemStatusCategories.initiated))
                    {
                        item.OrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.initiated).ToList().ForEach(i => i.ItemStatus = OrderItemStatusCategories.deleted);
                    }

                    foreach (var charge in originalSchedule.Charges)
                    {
                        var originalCharge = _dataContext.Set<Charge>().SingleOrDefault(i => i.Id == charge.Id);

                        if (originalSchedule.Charges.Any(s => s.Id == charge.Id))
                        {
                            charge.IsDeleted = true;

                            if (program.TypeCategory == ProgramTypeCategory.Camp)
                            {
                                charge.ProgramSessions.ToList().ForEach(c => c.IsDeleted = true);
                                var chargeDiscounts = originalSchedule.OrderItems.SelectMany(o => o.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.ChargeId == charge.Id);
                                {
                                    var orderItems = chargeDiscounts.Select(c => c.OrderItem).Where(i => i.ItemStatus == OrderItemStatusCategories.initiated);
                                    orderItems.ToList().ForEach(i => i.ItemStatus = OrderItemStatusCategories.deleted);
                                }
                            }
                        }
                    }
                }
            }

            foreach (var schedule in schedules)
            {
                var originalSchedule = _dataContext.Set<ProgramSchedule>().SingleOrDefault(i => i.Id == schedule.Id);

                if (originalSchedule != null)
                {
                    foreach (var item in originalSchedule.Charges)
                    {
                        if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare && item.Category == ChargeDiscountCategory.EntryFee)
                        {
                            //delete entryFee
                            if (numberOfClassDays != null)
                            {
                                var chargeWeekDay = ((BeforeAfterCareChargeAttribute)item.Attributes).NumberOfClassDay;

                                if (numberOfClassDays.Any(n => n.Value == 1 && chargeWeekDay.Contains(n.Text)))
                                {
                                    continue;
                                }
                                else
                                {
                                    item.IsDeleted = true;
                                }
                            }
                        }
                        else
                        {
                            if (!schedule.Charges.Any(s => s.Id == item.Id))
                            {
                                item.IsDeleted = true;

                                if (program.TypeCategory == ProgramTypeCategory.Camp)
                                {
                                    item.ProgramSessions.ToList().ForEach(c => c.IsDeleted = true);

                                    var chargeDiscounts = originalSchedule.OrderItems.SelectMany(o => o.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.ChargeId == item.Id);
                                    {
                                        var orderItems = chargeDiscounts.Select(c => c.OrderItem).Where(i => i.ItemStatus == OrderItemStatusCategories.initiated);
                                        orderItems.ToList().ForEach(i => i.ItemStatus = OrderItemStatusCategories.deleted);
                                    }
                                }
                            }
                        }

                    }
                }

                if (originalSchedule == null)
                {
                    originalProgram.ProgramSchedules.Add(schedule);
                }
                else
                {
                    foreach (var charge in schedule.Charges)
                    {
                        var originalCharge = _dataContext.Set<Charge>().SingleOrDefault(i => i.Id == charge.Id);

                        if (originalCharge == null)
                        {
                            originalSchedule.Charges.Add(charge);
                        }
                        else
                        {
                            charge.ProgramScheduleId = originalCharge.ProgramScheduleId;

                            _dataContext.Entry<Charge>(originalCharge).CurrentValues.SetValues(charge);
                        }
                    }

                    _dataContext.Entry<ProgramSchedule>(originalSchedule).CurrentValues.SetValues(schedule);
                }
            }

            var result = _programRepository.Save();

            return result;
        }
        public OperationStatus UpdateCharges(Program program, List<ProgramSchedule> schedules)
        {
            var originalProgram = _dataContext.Set<Program>().SingleOrDefault(p => p.Id == program.Id);

            originalProgram = program;

            originalProgram.MetaData.DateUpdated = DateTime.UtcNow;

            foreach (var schedule in schedules)
            {
                var originalSchedule = _dataContext.Set<ProgramSchedule>().SingleOrDefault(i => i.Id == schedule.Id);

                if (originalSchedule == null)
                {
                    originalProgram.ProgramSchedules.Add(schedule);
                }
                else
                {
                    foreach (var charge in schedule.Charges.Where(c => !c.IsDeleted))
                    {
                        var originalCharge = _dataContext.Set<Charge>().SingleOrDefault(i => i.Id == charge.Id);

                        if (originalCharge == null)
                        {
                            originalSchedule.Charges.Add(charge);
                        }
                        else
                        {
                            if (charge.ProgramSchedule.IsDeleted)
                            {
                                charge.IsDeleted = true;
                            }
                            //else
                            //{
                            //    if (charge.Category != ChargeDiscountCategory.ApplicationFee)
                            //    {
                            //        originalCharge.IsDeleted = true;
                            //        //originalSchedule.Charges.Add(charge);
                            //    }
                            //}
                            charge.ProgramScheduleId = originalCharge.ProgramScheduleId;

                            _dataContext.Entry(originalCharge).CurrentValues.SetValues(charge);
                        }
                    }

                    _dataContext.Entry(originalSchedule).CurrentValues.SetValues(schedule);
                }
            }

            return _programRepository.Save();
        }

        public OperationStatus CreateScheduleParts(Program program, DateTime startDate, DateTime endDate, string paymentMode, string monthlyType, int dueDateDay, bool SetPartsCharges)
        {
            var result = new OperationStatus();

            var programParts = _dataContext.Set<ProgramSchedulePart>().Where(p => p.ProgramId == program.Id);

            if (programParts != null && programParts.Any())
            {
                foreach (var item in programParts)
                {
                    var partCharges = _dataContext.Set<ProgramSchedulePartCharge>().Where(i => i.SchedulePartId == item.Id);
                    _dataContext.Set<ProgramSchedulePartCharge>().RemoveRange(partCharges);
                }

                programParts.ToList().ForEach(p => p.IsDeleted = true);
            }

            var programScheduleParts = GenerateSchedulePart(program.Id, startDate, endDate, paymentMode, monthlyType, dueDateDay);

            if (SetPartsCharges)
            {
                var programCharges = program.ProgramSchedules.Where(p => !p.IsDeleted).SelectMany(s => s.Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee)).ToList();

                foreach (var part in programScheduleParts)
                {
                    part.Charges = SetSchedulePartCharges(programCharges);
                }
            }

            _dataContext.Set<ProgramSchedulePart>().AddRange(programScheduleParts);

            return _programRepository.Save();
        }

        public List<ProgramSchedulePart> GenerateSchedulePart(long programId, DateTime startDate, DateTime endDate, string paymentMode, string monthlyType, int dueDateDay)
        {
            var date = startDate.Date;
            var programScheduleParts = new List<ProgramSchedulePart>();

            if (paymentMode == "Monthly")
            {
                var monthlyScheduleParts = GenerateMonthlySchedulePart(programId, startDate, endDate, monthlyType, dueDateDay);
                programScheduleParts.AddRange(monthlyScheduleParts);
            }
            else
            {
                while (date < endDate)
                {
                    var part = new ProgramSchedulePart();

                    part.ProgramId = programId;
                    part.StartDate = date;

                    var candidateEndDate = paymentMode == "BiWeekly" ? date.AddDays(13) : date.AddDays(6);

                    if (candidateEndDate > endDate)
                    {
                        part.EndDate = endDate;
                    }
                    else
                    {
                        part.EndDate = candidateEndDate;
                    }

                    #region DueDate

                    DateTime paymentDueDate = new DateTime();

                    if (dueDateDay == 0)
                    {
                        paymentDueDate = part.StartDate;
                    }
                    else
                    {
                        paymentDueDate = part.StartDate.AddDays(-dueDateDay);
                    }
                    #endregion

                    part.DueDate = paymentDueDate;

                    programScheduleParts.Add(part);

                    date = part.EndDate.AddDays(1);
                }
            }

            return programScheduleParts;
        }

        private List<ProgramSchedulePart> GenerateMonthlySchedulePart(long programId, DateTime startDate, DateTime endDate, string monthlyType, int dueDateDay)
        {
            var date = startDate.Date;
            var programScheduleParts = new List<ProgramSchedulePart>();
            var year = DateTime.Now.Year;

            while (date < endDate)
            {
                var schedulePart = new ProgramSchedulePart();

                schedulePart.ProgramId = programId;

                schedulePart.StartDate = date;

                var candidateEndDate = EndDateOfMonthSchedulePart(date, monthlyType);

                if (candidateEndDate > endDate)
                {
                    schedulePart.EndDate = endDate;
                }
                else
                {
                    schedulePart.EndDate = candidateEndDate;
                }

                #region DueDate

                DateTime paymentDueDate = new DateTime();

                if (dueDateDay == 0)
                {
                    paymentDueDate = schedulePart.StartDate;
                }
                else
                {
                    paymentDueDate = schedulePart.StartDate.AddDays(-dueDateDay);
                }
                #endregion

                schedulePart.DueDate = paymentDueDate;

                programScheduleParts.Add(schedulePart);

                date = schedulePart.EndDate.AddDays(1);
            }

            return programScheduleParts;
        }

        private DateTime EndDateOfMonthSchedulePart(DateTime startDate, string monthlyType)
        {
            DateTime endDate;
            if (monthlyType == "Calendar")
            {
                endDate = startDate.AddMonths(1).AddDays(-startDate.AddMonths(1).Day);
            }
            else
            {
                endDate = startDate.AddMonths(1).AddDays(-1);
            }

            return endDate;
        }
        public List<ProgramSchedulePart> GetScheduleParts(long programId)
        {
            return _dataContext.Set<ProgramSchedulePart>().Where(i => i.ProgramId == programId && !i.IsDeleted).ToList();
        }

        public List<ProgramSchedulePart> GenerateSubscriptionScheduleParts(DateTime startDate, DateTime endDate, decimal amount, int dueDate)
        {
            var result = new List<ProgramSchedulePart>();

            var date = startDate.Date;

            while (date < endDate)
            {
                var item = new ProgramSchedulePart();

                item.StartDate = date;

                var candidateEndDate = date.AddDays(29);

                if (candidateEndDate > endDate)
                {
                    item.EndDate = endDate;
                }
                else
                {
                    item.EndDate = candidateEndDate;
                }

                #region DueDate
                int? dueDateMonth = null;
                int? dueDateYear = null;

                var day = date.Day;
                if (day < dueDate)
                {
                    dueDateMonth = date.AddMonths(-1).Month;
                    dueDateYear = date.AddMonths(-1).Year;
                }
                else
                {
                    dueDateMonth = date.Month;
                    dueDateYear = date.Year;
                }

                var dueDateDay = dueDate;

                DateTime paymentDueDate = new DateTime(dueDateYear.Value, dueDateMonth.Value, dueDateDay);
                #endregion

                item.DueDate = paymentDueDate;
                //item.Id = Guid.NewGuid().ToString();

                result.Add(item);

                date = item.EndDate.AddDays(1);
            }

            return result;
        }
        public List<ProgramSchedulePartCharge> GetSchedulePartCharges(long schedulePartId)
        {
            return _dataContext.Set<ProgramSchedulePartCharge>().Where(i => i.SchedulePartId == schedulePartId).ToList();
        }

        public List<ProgramSchedulePartCharge> GetProgramPartCharges(long chargeId)
        {
            return _dataContext.Set<ProgramSchedulePartCharge>().Where(i => i.ChargeId == chargeId).ToList();
        }
        public List<ProgramSchedulePartCharge> GetSchedulePartCharges(Charge charge)
        {
            return _dataContext.Set<ProgramSchedulePartCharge>().Where(i => i.ChargeId == charge.Id).ToList();
        }
        public OperationStatus UpdateScheduleParts(List<ProgramSchedulePart> ProgramScheduleParts)
        {
            var result = new OperationStatus();

            foreach (var schedulePart in ProgramScheduleParts)
            {
                var originalSchedulepart = _dataContext.Set<ProgramSchedulePart>().SingleOrDefault(i => i.Id == schedulePart.Id);

                _dataContext.Entry<ProgramSchedulePart>(originalSchedulepart).CurrentValues.SetValues(schedulePart);
            }

            result.Status = _dataContext.SaveChanges() > 0;

            return _programRepository.Save();

        }
        public OperationStatus CreateSchedulePartCharges(List<Charge> programCharges, ProgramSchedulePart schedulePart)
        {
            var result = new OperationStatus();

            var partCharges = _dataContext.Set<ProgramSchedulePartCharge>().Where(i => i.SchedulePartId == schedulePart.Id);
            _dataContext.Set<ProgramSchedulePartCharge>().RemoveRange(partCharges);

            var schedulePartCharges = new List<ProgramSchedulePartCharge>();


            foreach (var charge in programCharges.ToList())
            {
                var partCharge = new ProgramSchedulePartCharge
                {
                    ChargeId = charge.Id,
                    ChargeAmount = charge.Amount,
                    SchedulePartId = schedulePart.Id
                };

                schedulePartCharges.Add(partCharge);
            }


            _dataContext.Set<ProgramSchedulePartCharge>().AddRange(schedulePartCharges);

            return _programRepository.Save();
        }

        private List<ProgramSchedulePartCharge> SetSchedulePartCharges(List<Charge> programCharges)
        {
            var result = new OperationStatus();
            var schedulePartCharges = new List<ProgramSchedulePartCharge>();

            foreach (var charge in programCharges.ToList())
            {
                var partCharge = new ProgramSchedulePartCharge
                {
                    ChargeId = charge.Id,
                    ChargeAmount = charge.Amount,
                    Charge = charge
                };

                schedulePartCharges.Add(partCharge);
            }

            return schedulePartCharges;
        }
        public bool IsProgramSchedulePartEdited(long programId)
        {
            var program = Get(programId);

            var currentProgramSchedulePart = GetScheduleParts(programId);

            var schedule = program.ProgramSchedules.FirstOrDefault(s => !s.IsDeleted);
            var scheduleAtrribute = (ScheduleAfterBeforeCareAttribute)schedule.Attributes;

            var startDate = schedule.StartDate;
            var endDate = schedule.EndDate;
            var paymentMode = scheduleAtrribute.PaymentScheduleMode;
            var dueDate = scheduleAtrribute.PaymentDueDate;
            var monthlyType = scheduleAtrribute.MonthlyScheduleType;

            var generateProgramPart = GenerateSchedulePart(programId, startDate, endDate, paymentMode, monthlyType, dueDate);



            if (currentProgramSchedulePart.Count == 0)
            {
                return false;
            }

            if (currentProgramSchedulePart.Count() != generateProgramPart.Count())
            {
                return true;
            }

            for (int i = 0; i < currentProgramSchedulePart.Count(); i++)
            {
                if (currentProgramSchedulePart[i].StartDate != generateProgramPart[i].StartDate)
                {
                    return true;
                }

                if (currentProgramSchedulePart[i].EndDate != generateProgramPart[i].EndDate)
                {
                    return true;
                }

                if (currentProgramSchedulePart[i].IsDeleted != generateProgramPart[i].IsDeleted)
                {
                    return true;
                }
                if (currentProgramSchedulePart[i].DueDate != generateProgramPart[i].DueDate)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsProgramSchedulePartEdited(Program program, DateTime newStartDate, DateTime newEndDate, string scheduleMode, string scheduleMonthlyType, int paymentDuedate)
        {
            var result = false;

            var programschedules = program.ProgramSchedules.Where(s => !s.IsDeleted);

            var defaultSchedule = programschedules.First();
            var scheduleAtrribute = (ScheduleAfterBeforeCareAttribute)defaultSchedule.Attributes;

            var hasProgramOrders = _orderSessionBusiness.HasActiveOrder(program);

            var oldStartDate = defaultSchedule.StartDate;
            var oldEndDate = defaultSchedule.EndDate;
            var paymentMode = scheduleAtrribute.PaymentScheduleMode;
            var dueDate = scheduleAtrribute.PaymentDueDate;
            var monthlyType = scheduleAtrribute.MonthlyScheduleType;

            if (oldStartDate != newStartDate && !hasProgramOrders)
            {
                return true;
            }

            if (oldEndDate != newEndDate && !hasProgramOrders)
            {
                return true;
            }

            if (dueDate != paymentDuedate)
            {
                return true;
            }

            if (paymentMode != scheduleMode)
            {
                return true;
            }

            if (paymentMode == "Monthly" && monthlyType != scheduleMonthlyType)
            {
                return true;
            }

            return result;
        }
        public OperationStatus Delete(long id)
        {
            var program = _dataContext.Set<Program>().SingleOrDefault(p => p.Id == id);

            program.Status = ProgramStatus.Deleted;

            return _programRepository.Save();
        }

        public OperationStatus Freeze(long id)
        {
            var program = _dataContext.Set<Program>().SingleOrDefault(p => p.Id == id);

            program.Status = ProgramStatus.Frozen;

            return _programRepository.Save();
        }

        public OperationStatus UnFreeze(long id)
        {
            var program = _dataContext.Set<Program>().SingleOrDefault(p => p.Id == id);

            program.Status = ProgramStatus.Open;

            return _programRepository.Save();
        }

        public OperationStatus FreezeSchedule(long id)
        {
            var programSchedule = _dataContext.Set<ProgramSchedule>().SingleOrDefault(p => p.Id == id);

            programSchedule.IsFreezed = true;

            return _programRepository.Save();
        }

        public OperationStatus UnFreezeSchedule(long id)
        {
            var programSchedule = _dataContext.Set<ProgramSchedule>().SingleOrDefault(p => p.Id == id);

            programSchedule.IsFreezed = false;

            return _programRepository.Save();
        }

        public bool CheckAndAddSecheduleProgramSession(Program program)
        {
            var programNewSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both && (s.ProgramSessions == null || (s.ProgramSessions != null && s.ProgramSessions.All(p => p.IsDeleted)))).ToList();
            var result = programNewSchedules.Any();

            if (result)
            {
                _programSessionBusiness.CreateScheduleSessions(programNewSchedules);
            }

            return result;
        }

        public Program Copy(long id)
        {
            var sourceProgram = _programRepository.GetList().Include(p => p.ClubLocation.PostalAddress).Include(p => p.ClubFormTemplates).Include(w => w.ClubWaivers).AsNoTracking().SingleOrDefault(p => p.Id == id);

            foreach (var schedule in sourceProgram.ProgramSchedules)
            {
                schedule.Program =  sourceProgram;

                foreach (var charge in schedule.Charges)
                {
                    charge.ProgramSchedule = schedule;

                    charge.ProgramSessions = new List<ProgramSession>();
                    var chargeSessions = _dataContext.Set<ProgramSession>().Where(p => p.ChargeId == charge.Id && !p.IsDeleted).AsNoTracking();
                    CollectionHelper.AddEntities(charge.ProgramSessions, chargeSessions.ToList());
                }

                schedule.ProgramSessions = new List<ProgramSession>();
                var sessions = _dataContext.Set<ProgramSession>().Where(p => p.ProgramScheduleId == schedule.Id && !p.IsDeleted).AsNoTracking();
                CollectionHelper.AddEntities(schedule.ProgramSessions, sessions.ToList());

                schedule.OrderItems.Clear();
            }

            var scheduleParts = _dataContext.Set<ProgramSchedulePart>().Where(p => p.ProgramId == sourceProgram.Id).AsNoTracking();
            CollectionHelper.AddEntities(sourceProgram.ScheduleParts, scheduleParts.ToList());

            foreach (var schedulePart in sourceProgram.ScheduleParts)
            {
                var partCharges = _dataContext.Set<ProgramSchedulePartCharge>().Where(p => p.SchedulePartId == schedulePart.Id).AsNoTracking();

                foreach (var partCharge in partCharges)
                {
                    partCharge.Charge = sourceProgram.ProgramSchedules
                                                                  .SelectMany(s => s.Charges)
                                                                  .FirstOrDefault(g => g.Amount == partCharge.Charge.Amount && g.Name == partCharge.Charge.Name && g.Category == partCharge.Charge.Category);
                }

                CollectionHelper.AddEntities(schedulePart.Charges, partCharges.ToList());
            }

            var selectedForms = sourceProgram.ClubFormTemplates.ToArray().ToList().Select(s => s.Id);

            if (selectedForms != null && selectedForms.Any())
            {
                var forms = _clubBusiness.GetFormTemplates(sourceProgram.Season.ClubId, selectedForms.Select(c => c), null);

                sourceProgram.ClubFormTemplates.Clear();

                CollectionHelper.AddEntities<ClubFormTemplate>(sourceProgram.ClubFormTemplates, forms);
            }
            else
            {
                sourceProgram.ClubFormTemplates.Clear();
            }

            var selectedWaivers = sourceProgram.ClubWaivers.ToArray().ToList().Select(s => s.Id);

            if (selectedWaivers != null && selectedWaivers.Any())
            {
                var waivers = _clubBusiness.GetWaivers(selectedWaivers.Select(c => c)).ToList();

                sourceProgram.ClubWaivers.Clear();

                CollectionHelper.AddEntities<ClubWaiver>(sourceProgram.ClubWaivers, waivers);
            }
            else
            {
                sourceProgram.ClubWaivers.Clear();
            }

            sourceProgram.Season = null;
            Create(sourceProgram);

            return sourceProgram;
        }

        public Program CopyInSeason(Program program, long seasonId, bool forwardDates, int? monthNum)
        {
            var clubDomain = program.Club.Domain;
            var sourceProgram =
                _programRepository.GetList()
                    .Include(p => p.ClubLocation.PostalAddress)
                    .Include(p => p.ClubFormTemplates)
                    .Include(w => w.ClubWaivers)
                    .Include(p => p.Categories)
                    .Include(i => i.ImageGallery)
                    .AsNoTracking()
                    .SingleOrDefault(p => p.Id == program.Id);

            foreach (var schedule in sourceProgram.ProgramSchedules)
            {
                schedule.Program = sourceProgram;

                if (forwardDates && monthNum.HasValue)
                {
                    schedule.StartDate = schedule.StartDate.AddMonths(monthNum.Value);
                    schedule.EndDate = schedule.EndDate.AddMonths(monthNum.Value);
                    schedule.RegistrationPeriod.RegisterStartDate =
                        schedule.RegistrationPeriod.RegisterStartDate.Value.AddMonths(monthNum.Value);
                    // check this property value for session pack.
                    schedule.RegistrationPeriod.RegisterEndDate = (schedule.RegistrationPeriod.RegisterEndDate.HasValue)
                        ? schedule.RegistrationPeriod.RegisterEndDate.Value.AddMonths(monthNum.Value)
                        : (DateTime?)null;

                    if (sourceProgram.TypeCategory == ProgramTypeCategory.ChessTournament)
                    {
                        var tournneySchedule = ((TournamentScheduleAttribute)schedule.Attributes);

                        foreach (var tournamentSchedule in tournneySchedule.Schedules)
                        {
                            tournamentSchedule.StartDate = tournamentSchedule.StartDate.Value.AddMonths(monthNum.Value);
                            tournamentSchedule.EndDate = tournamentSchedule.EndDate.Value.AddMonths(monthNum.Value);
                        }
                        schedule.AttributesSerialized = JsonConvert.SerializeObject(tournneySchedule);
                    }
                }

                foreach (var charge in schedule.Charges)
                {
                    charge.ProgramSchedule = schedule;
                    if (forwardDates && charge.HasEarlyBird)
                    {
                        var attrModel = JsonConvert.DeserializeObject<ChargeAttribute>(charge.AttributesSerialized);

                        foreach (var earlyBird in attrModel.EarlyBirds)
                        {
                            earlyBird.ExpireDate =
                                (earlyBird.ExpireDate.HasValue)
                                    ? earlyBird.ExpireDate.Value.AddMonths(monthNum.Value)
                                    : (DateTime?)null;
                        }

                        charge.AttributesSerialized = JsonConvert.SerializeObject(attrModel);
                    }

                    charge.ProgramSessions = new List<ProgramSession>();
                    var chargeSessions = _dataContext.Set<ProgramSession>().Where(p => p.ChargeId == charge.Id && !p.IsDeleted).AsNoTracking();
                    CollectionHelper.AddEntities(charge.ProgramSessions, chargeSessions.ToList());
                }

                schedule.ProgramSessions = new List<ProgramSession>();
                var sessions = _dataContext.Set<ProgramSession>().Where(p => p.ProgramScheduleId == schedule.Id && !p.IsDeleted).AsNoTracking();
                CollectionHelper.AddEntities(schedule.ProgramSessions, sessions.ToList());

                schedule.OrderItems.Clear();
            }

            var scheduleParts = _dataContext.Set<ProgramSchedulePart>().Where(p => p.ProgramId == sourceProgram.Id).AsNoTracking();
            CollectionHelper.AddEntities(sourceProgram.ScheduleParts, scheduleParts.ToList());

            foreach (var schedulePart in sourceProgram.ScheduleParts)
            {
                var partCharges = _dataContext.Set<ProgramSchedulePartCharge>().Where(p => p.SchedulePartId == schedulePart.Id).AsNoTracking();

                foreach (var partCharge in partCharges)
                {
                    partCharge.Charge = sourceProgram.ProgramSchedules
                                                                  .SelectMany(s => s.Charges)
                                                                  .FirstOrDefault(g => g.Amount == partCharge.Charge.Amount && g.Name == partCharge.Charge.Name && g.Category == partCharge.Charge.Category);
                }

                CollectionHelper.AddEntities(schedulePart.Charges, partCharges.ToList());
            }

            var selectedForms = sourceProgram.ClubFormTemplates.ToArray().ToList().Select(s => s.Id);

            if (selectedForms != null && selectedForms.Any())
            {
                var forms = _clubBusiness.GetFormTemplates(sourceProgram.ClubId, selectedForms.Select(c => c), null);

                sourceProgram.ClubFormTemplates.Clear();

                CollectionHelper.AddEntities<ClubFormTemplate>(sourceProgram.ClubFormTemplates, forms);
            }
            else
            {
                sourceProgram.ClubFormTemplates.Clear();
            }

            var selectedWaivers = sourceProgram.ClubWaivers.ToArray().ToList().Select(s => s.Id);

            if (selectedWaivers != null && selectedWaivers.Any())
            {
                var waivers = _clubBusiness.GetWaivers(selectedWaivers.Select(c => c)).ToList();

                sourceProgram.ClubWaivers.Clear();

                CollectionHelper.AddEntities<ClubWaiver>(sourceProgram.ClubWaivers, waivers);
            }
            else
            {
                sourceProgram.ClubWaivers.Clear();
            }

            sourceProgram.SeasonId = seasonId;
            var location = _locationBusiness.GetClubLocationsByClubId(sourceProgram.Season.ClubId).SingleOrDefault(l => l.Id == sourceProgram.ClubLocationId);
            sourceProgram.ClubLocationId = (location != null) ? location.Id : 0;

            sourceProgram.ClubLocation = null;
            sourceProgram.Club = null;
            sourceProgram.Season = null;

            var categories = _categoryBusiness.GetList(sourceProgram.Categories.Select(c => c.Id)).ToList();

            sourceProgram.Categories.Clear();

            foreach (var category in categories)
            {
                sourceProgram.Categories.Add(category);
            }

            if (sourceProgram.ImageGalleryId.HasValue)
            {
                sourceProgram.ImageGallery = null;

                var imageGallery = _imageGalleryBusiness.Get(sourceProgram.ImageGalleryId.Value);

                sourceProgram.ImageGallery = new ImageGallery()
                {

                    Name = sourceProgram.Name,
                    ClubDomain = clubDomain,
                    MetaData = new MetaData() { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                    Type = ImageGalleryType.Program,
                    Status = EventStatusCategories.open,
                    Images = imageGallery.Images.Select(i => new
                    ImageGalleryItem()
                    {
                        Title = i.Title,
                        Path = i.Path,
                        Status = EventStatusCategories.open,
                        Order = 0,
                        MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow }
                    })
                    .ToList()
                };
            }

            var res = Create(sourceProgram);

            return sourceProgram;
        }

        public IQueryable<ClubWaiver> GetWaivers(long id)
        {
            return _programRepository.Get(id).ClubWaivers.AsQueryable();
        }

        public ProgramRegStatus GetRegStatus(Program program)
        {
            ProgramRegStatus result = ProgramRegStatus.NotStarted;

            DateTime registerStartDate = RegisterStartDate(program);
            DateTime registerEndDate = RegisterEndDate(program);

            DateTime localDateTime = DateTimeHelper.GetCurrentLocalDateTime(program.Club.TimeZone);

            if (localDateTime < registerStartDate)
            {
                result = ProgramRegStatus.NotStarted;
            }
            else if (localDateTime >= registerStartDate && localDateTime <= registerEndDate)
            {
                result = ProgramRegStatus.Open;
            }
            else
            {
                result = ProgramRegStatus.Closed;
            }

            return result;
        }

        public ProgramStatusType GetStatus(Program program, bool isTestMode)
        {
            ProgramStatusType result = ProgramStatusType.Available;

            if (program.Status == ProgramStatus.Deleted)
            {
                throw new NotSupportedException("Deleted program not supported for this operation.");
            }

            var regStatus = GetRegStatus(program);

            if (regStatus == ProgramRegStatus.NotStarted)
            {
                return ProgramStatusType.NotStarted;
            }
            else if (regStatus == ProgramRegStatus.Closed)
            {
                return ProgramStatusType.Expired;
            }

            if (IsFull(program, isTestMode))
            {
                return ProgramStatusType.Full;
            }

            if (program.Status == ProgramStatus.Frozen)
            {
                return ProgramStatusType.Frozen;
            }

            // If none of above conditions  happend, the event status will be available
            return result;
        }

        public ProgramRunningStatus GetRunningStatus(Program program)
        {
            ProgramRunningStatus result = ProgramRunningStatus.NotStarted;

            DateTime localDateTime = DateTimeHelper.GetCurrentLocalDateTime(program.ClubLocation.PostalAddress.TimeZone.HasValue ? program.ClubLocation.PostalAddress.TimeZone.Value : TimeZones.UTC);

            DateTime startDate = StartDate(program).Value;
            DateTime endDate = EndDate(program).Value;

            if (localDateTime < startDate)
            {
                result = ProgramRunningStatus.NotStarted;
            }
            else if (localDateTime >= startDate && (localDateTime <= endDate.AddDays(1)))
            {
                result = ProgramRunningStatus.InProgress;
            }
            else
            {
                result = ProgramRunningStatus.Expired;
            }

            return result;
        }

        public ProgramRegStatus GetScheduleRegStatus(ProgramSchedule schedule)
        {
            var result = ProgramRegStatus.NotStarted;

            DateTime localDateTime = DateTimeHelper.GetCurrentLocalDateTime(schedule.Program.Club.TimeZone);

            DateTime startDate = schedule.RegistrationPeriod.RegisterStartDate.Value.AddTicks(schedule.RegistrationPeriod.RegisterStartTime.HasValue ? schedule.RegistrationPeriod.RegisterStartTime.Value.Ticks : 0);

            DateTime endDate = schedule.RegistrationPeriod.RegisterEndDate.Value.AddTicks(schedule.RegistrationPeriod.RegisterEndTime.HasValue ? schedule.RegistrationPeriod.RegisterEndTime.Value.Ticks : TimeSpan.Parse("23:59:59").Ticks);

            if (localDateTime < startDate)
            {
                result = ProgramRegStatus.NotStarted;
            }
            else if (localDateTime >= startDate && (localDateTime <= endDate))
            {
                result = ProgramRegStatus.Open;
            }
            else
            {
                result = ProgramRegStatus.Closed;
            }

            return result;
        }

        public ProgramRegStatus GetScheduleRegStatus(ProgramSchedule programSchedule, TimeZones timeZone)
        {
            ProgramRegStatus result = ProgramRegStatus.NotStarted;

            DateTime localDateTime = DateTimeHelper.GetCurrentLocalDateTime(timeZone);

            if (localDateTime < programSchedule.RegistrationPeriod.RegisterStartDate)
            {
                result = ProgramRegStatus.NotStarted;
            }
            else if (localDateTime >= programSchedule.RegistrationPeriod.RegisterStartDate && localDateTime <= programSchedule.RegistrationPeriod.RegisterEndDate.Value)
            {
                result = ProgramRegStatus.Open;
            }
            else
            {
                result = ProgramRegStatus.Closed;
            }

            return result;
        }

        public IQueryable<Program> GetSeasonPrograms(long seasonId)
        {

            return
                GetListBySeasonId(seasonId)
                    .Where(
                        p => p.LastCreatedPage == ProgramPageStep.Step4);
        }

        public IQueryable<Program> GetClubPrograms(int clubId)
        {

            return
                GetList(clubId)
                    .Where(
                        p => p.LastCreatedPage == ProgramPageStep.Step4);
        }

        public bool IsProgramExpired(DateTime deadline, TimeZones timeZone)
        {
            return DateTimeHelper.GetCurrentLocalDateTime(timeZone) > deadline;
        }

        public bool IsFull(Charge charge, bool isTestMode)
        {

            if (charge.Capacity == 0)
            {
                return false;
            }

            var filled = GetRegisteredCount(charge, isTestMode);

            return filled >= charge.Capacity;
        }

        public bool IsFull(Program program, bool isTestMode, bool checkTuitions = false)
        {

            if (checkTuitions)
            {
                int? programCapacityLeft = CapacityLeft(program, isTestMode);

                if (!programCapacityLeft.HasValue)
                {
                    return false;
                }
                else
                {
                    if (programCapacityLeft <= 0)
                    {
                        return true;
                    }

                    return false;
                }
            }


            foreach (var schedule in program.ProgramSchedules)
            {

                int? scheduleCapacity = 0;

                scheduleCapacity = GetCapacity(schedule);

                if (scheduleCapacity == 0)
                {
                    return false;
                }

                int filled = GetRegisteredCount(schedule, isTestMode);

                if (filled < scheduleCapacity || scheduleCapacity == null)
                {
                    return false;
                }
            }

            return true;
        }

        #region
        public int? GetCapacity(Program program)
        {
            int? result = null;

            foreach (var schedule in program.ProgramSchedules)
            {
                int? scheduleCapacityLeft = GetCapacity(schedule);

                if (scheduleCapacityLeft.HasValue)
                {
                    if (result == null)
                    {
                        result = 0;
                    }

                    result += scheduleCapacityLeft;
                }
                else
                {
                    // null means unlimited
                    return null;
                }
            }

            return result;
        }

        public int? GetCapacity(ProgramSchedule schedule)
        {
            int? result = null;

            int? scheduleCapacity = null;//

            if (schedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament)
            {
                scheduleCapacity = ((TournamentScheduleAttribute)schedule.Attributes).Capacity;
            }
            else if (schedule.Program.TypeCategory == ProgramTypeCategory.Subscription || schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                scheduleCapacity = 0;
            }
            else
            {
                scheduleCapacity = ((ScheduleAttribute)schedule.Attributes).Capacity;
            }

            int? tuitionsCapacity = GetCapacity(schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted).ToList());

            if (!scheduleCapacity.HasValue || (scheduleCapacity.HasValue && scheduleCapacity == 0))
            {
                return tuitionsCapacity;
            }
            else
            {
                if (tuitionsCapacity.HasValue && tuitionsCapacity.Value < scheduleCapacity.Value)
                {
                    return tuitionsCapacity;
                }
                else
                {
                    return scheduleCapacity;
                }
            }

            return result;
        }

        private int? GetCapacity(List<Charge> charges)
        {
            int? result = null;

            foreach (var charge in charges)
            {
                int? chargeCapacity = GetCapacity(charge);

                if (!chargeCapacity.HasValue)
                {
                    return null;
                }
                else
                {
                    if (result == null)
                    {
                        result = 0;
                    }

                    result += chargeCapacity;
                }
            }

            return result;
        }

        public int? GetCapacity(Charge charge)
        {
            if (charge.Capacity == 0)
            {
                return null;
            }
            else
            {
                return charge.Capacity;
            }
        }
        #endregion

        #region GetRegisteredCount
        public int GetRegisteredCount(Program program, bool isTestMode)
        {
            int result = 0;

            foreach (var schedule in program.ProgramSchedules)
            {
                result += GetRegisteredCount(schedule, isTestMode);
            }

            return result;
        }

        public int GetRegisteredCount(ProgramSchedule schedule, bool isTestMode)
        {
            int result = 0;

            if (schedule.OrderItems != null)
            {
                result = schedule.OrderItems.Count(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive != isTestMode);
            }

            return result;
        }

        private int? GetRegisteredCount(List<Charge> charges, bool isTestMode)
        {
            int? result = null;

            foreach (var charge in charges)
            {
                result += GetRegisteredCount(charge, isTestMode);
            }

            return result;
        }

        public int GetRegisteredCount(Charge charge, bool isTestMode)
        {
            int result = 0;

            if (charge.ProgramSchedule.OrderItems != null)
            {
                result = charge.ProgramSchedule.OrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive != isTestMode)
                                                          .Count(o => o.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.ChargeId.HasValue && c.ChargeId.Value == charge.Id && c.Category == ChargeDiscountCategory.EntryFee));
            }

            return result;
        }
        #endregion

        public decimal GetTotalRegistration(Program program, bool isTestMode)
        {
            decimal result = 0;

            result = program.ProgramSchedules.SelectMany(o => o.OrderItems).Where(o => o.Order.IsLive != isTestMode).Sum(o => o.EntryFee);

            return result;
        }

        public int? CapacityLeft(Program program, bool isTestMode)
        {
            int? result = null;

            int? programCapacity = GetCapacity(program);

            if (programCapacity.HasValue)
            {
                int registeredCount = GetRegisteredCount(program, isTestMode);

                result = programCapacity.Value - registeredCount;
            }

            return result;
        }

        public int? CapacityLeft(ProgramSchedule schedule, long? chargeId, bool isTestMode)
        {
            if (schedule == null || (chargeId.HasValue && !schedule.Charges.Any(c => c.Id == chargeId && !c.IsDeleted)))
            {
                return null;
            }

            int result = 0;

            if (chargeId.HasValue)
            {
                var charge = schedule.Charges.FirstOrDefault(c => c.Id == chargeId);
                if (charge.Capacity > 0)
                {
                    return CapacityLeft(charge, isTestMode);
                }
            }
            int scheduleCapacity = schedule.Attributes.Capacity;
            if (scheduleCapacity != 0)
            {
                var allCapacityOfOtherCharges = 0;

                var allRegisteredCountOfChargesWithoutCapacity = 0;
                var chargesWithCapacity = schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted && c.Id != chargeId && c.Capacity > 0);
                var chargesWithoutCapacity = schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted && c.Capacity <= 0);
                if (chargesWithCapacity != null && chargesWithCapacity.Any())
                {
                    allCapacityOfOtherCharges = chargesWithCapacity.Sum(c => c.Capacity);

                }
                if (chargesWithoutCapacity != null && chargesWithoutCapacity.Any())
                {

                    foreach (var item in chargesWithoutCapacity)
                    {
                        allRegisteredCountOfChargesWithoutCapacity += _orderItemBusiness.RegisteredCount(item.Id, isTestMode);
                    }
                }

                return scheduleCapacity - allCapacityOfOtherCharges - allRegisteredCountOfChargesWithoutCapacity;
            }
            return result;
        }

        public int? CapacityLeft(ProgramSchedule schedule, bool isTestMode)
        {
            int result = 0;

            int? scheduleCapacity = GetCapacity(schedule);

            if (scheduleCapacity.HasValue)
            {
                int registeredCount = GetRegisteredCount(schedule, isTestMode);

                result = scheduleCapacity.Value - registeredCount;
            }

            return result;
        }

        public int? CapacityLeft(Charge charge, bool isTestMode)
        {
            int? result = null;

            int? chargeCapacity = GetCapacity(charge);

            if (chargeCapacity.HasValue)
            {
                int registeredCount = GetRegisteredCount(charge, isTestMode);

                result = chargeCapacity.Value - registeredCount;
            }

            return result;
        }

        public bool IsConfirmed(Program program, bool isTestMode)
        {
            var result = true;

            foreach (var schedule in program.ProgramSchedules)
            {
                result = result && IsConfirmed(schedule, isTestMode);
            }

            return result;
        }

        private bool IsConfirmed(ProgramSchedule programSchedule, bool isTestMode)
        {
            int registeredCount = 0;
            int minimumEnrollment = programSchedule.Attributes.MinimumEnrollment;

            registeredCount = GetRegisteredCount(programSchedule, isTestMode);

            return registeredCount >= minimumEnrollment;
        }

        public string CapacityLeftFormated(Charge charge, bool isTestMode)
        {
            string result = String.Empty;

            int? capacityLeft = CapacityLeft(charge, isTestMode);

            if (capacityLeft != null)
            {
                if (capacityLeft <= 0)
                {
                    result = "Full";
                }
                else
                {
                    result = String.Format("{0} spots left", capacityLeft.ToString());
                }
            }

            return result;
        }

        public bool CheckAgeRestriction(ProgramSchedule schedule, int age)
        {
            if (schedule.AttendeeRestriction.RestrictionType == RestrictionType.Age)
            {
                if (schedule.AttendeeRestriction.MinAge > age || schedule.AttendeeRestriction.MaxAge < age)
                {
                    return true;
                }
            }

            return false;
        }

        public bool CheckGradeRestriction(Program program, SchoolGradeType grade)
        {
            var minGrade = GetMinGrade(program);
            var maxGrade = GetMaxGrade(program);

            if (minGrade.HasValue && grade.GetOrder() < minGrade.GetOrder())
            {
                return true;
            }

            if (maxGrade.HasValue && grade.GetOrder() > maxGrade.GetOrder())
            {
                return true;
            }

            return false;
        }

        public string CheckGradeRestriction(ProgramSchedule schedule, object gradeObj)
        {
            if (schedule.AttendeeRestriction.RestrictionType != RestrictionType.Grade) return string.Empty;
            var message = string.Empty;

            if (gradeObj == null)
            {
                return "Grade is required.";
            }

            int? grade = null;

            foreach (SchoolGradeType schoolGrade in Enum.GetValues(typeof(SchoolGradeType)))
            {
                if (!gradeObj.ToString().ToLower()
                    .Equals(EnumHelper.ToDescription(schoolGrade).ToLower())) continue;
                grade = schoolGrade.GetOrder();
                break;
            }

            if (grade == null)
            {
                if (gradeObj.ToString() == "NotSelected")
                    grade = -1;
                else
                    return string.Empty;
            }


            if (schedule.AttendeeRestriction.MinGrade.HasValue && schedule.AttendeeRestriction.MaxGrade.HasValue)
            {
                var mingrage = schedule.AttendeeRestriction.MinGrade.GetOrder();
                var maxgarde = schedule.AttendeeRestriction.MaxGrade.GetOrder();

                if (mingrage == maxgarde)
                {
                    if (maxgarde != grade)
                    {
                        message =
                            $"Participants must be in {EnumHelper.ToDescription(schedule.AttendeeRestriction.MaxGrade.Value).ToLower()} grade to register for this class.";
                    }
                }
                else if (!(mingrage <= grade && maxgarde >= grade))
                {
                    message =
                        $"Participants must be in grades {EnumHelper.ToDescription(schedule.AttendeeRestriction.MinGrade.Value).ToLower()} to {EnumHelper.ToDescription(schedule.AttendeeRestriction.MaxGrade.Value)} to register for this class.";

                }

            }
            else if (schedule.AttendeeRestriction.MinGrade.HasValue == false && schedule.AttendeeRestriction.MaxGrade.HasValue)
            {
                var maxgarde = schedule.AttendeeRestriction.MaxGrade.GetOrder();
                if (grade > maxgarde)
                {
                    message =
                        $"Participants must be in {EnumHelper.ToDescription(schedule.AttendeeRestriction.MaxGrade.Value).ToLower()} grade or lower to register for this class.";
                }

            }
            else if (schedule.AttendeeRestriction.MinGrade.HasValue && schedule.AttendeeRestriction.MaxGrade.HasValue == false)
            {
                var mingrage = schedule.AttendeeRestriction.MinGrade.GetOrder();
                if (grade < mingrage)
                {
                    message =
                        $"Participants must be in {EnumHelper.ToDescription(schedule.AttendeeRestriction.MinGrade.Value).ToLower()} grade or higher to register for this class.";
                }
            }
            return message;

        }

        public bool CheckAgeRestriction(Program program, int age)
        {
            foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
            {
                if (schedule.AttendeeRestriction.RestrictionType == RestrictionType.Age)
                {
                    if (schedule.AttendeeRestriction.MinAge > age || schedule.AttendeeRestriction.MaxAge < age)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public string CheckGenderRestriction(ProgramSchedule schedule, Genders gender)
        {
            if (schedule.AttendeeRestriction.Gender.Value != Genders.NoRestriction)
            {
                if (schedule.AttendeeRestriction.Gender.Value != gender)
                {
                    return String.Format("This schedule is only for {0}.", schedule.AttendeeRestriction.Gender.ToDescription());
                }
            }

            return null;
        }

        public bool CheckGenderRestriction(Program program, Genders gender)
        {
            foreach (var schedule in program.ProgramSchedules)
            {
                if (schedule.AttendeeRestriction.Gender.Value != Genders.NoRestriction && gender != Genders.NoRestriction)
                {
                    if (schedule.AttendeeRestriction.Gender.Value != gender)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool HasGenderRestriction(ProgramSchedule programSchedule)
        {
            return programSchedule.AttendeeRestriction.Gender.HasValue && programSchedule.AttendeeRestriction.Gender.Value != Genders.NoRestriction;
        }

        public Genders GetGender(Program program)
        {


            bool girl = program.ProgramSchedules.Any(p => p.AttendeeRestriction.Gender == Genders.Female);

            bool boy = program.ProgramSchedules.Any(p => p.AttendeeRestriction.Gender == Genders.Male);

            bool boyAndGirl = program.ProgramSchedules.Any(p => p.AttendeeRestriction.Gender == Genders.NoRestriction);

            girl = girl || boyAndGirl;
            boy = boy || boyAndGirl;

            if (girl && boy)
            {
                return Genders.NoRestriction;
            }
            else
            {
                if (girl)
                {
                    return Genders.Female;
                }

                return Genders.Male;
            }

        }

        public string GetStringGender(Program program)
        {


            bool girl = program.ProgramSchedules.Any(p => p.AttendeeRestriction.Gender == Genders.Female);

            bool boy = program.ProgramSchedules.Any(p => p.AttendeeRestriction.Gender == Genders.Male);

            bool boyAndGirl = program.ProgramSchedules.Any(p => p.AttendeeRestriction.Gender == Genders.NoRestriction);

            girl = girl || boyAndGirl;
            boy = boy || boyAndGirl;

            if (girl && boy)
            {
                return "None";
            }
            else
            {
                if (girl)
                {
                    return Genders.Female.ToDescription();
                }

                return Genders.Male.ToDescription();
            }

        }

        public int? GetMinAge(Program program)
        {
            int? minAge = null;

            if (program.ProgramSchedules.Where(w => !w.IsDeleted).Any(w => w.AttendeeRestriction.RestrictionType == RestrictionType.None || w.AttendeeRestriction.RestrictionType == RestrictionType.Grade))
            {
                return null;
            }

            if (program.ProgramSchedules.Where(w => !w.IsDeleted).Any(w => w.AttendeeRestriction.RestrictionType == RestrictionType.Age))
            {
                if (program.ProgramSchedules.Where(w => !w.IsDeleted && w.AttendeeRestriction.RestrictionType == RestrictionType.Age).Any(m => m.AttendeeRestriction.MinAge == null))
                {
                    return minAge;
                }

                minAge = program.ProgramSchedules.Where(w => !w.IsDeleted && w.AttendeeRestriction.RestrictionType == RestrictionType.Age).Min(m => m.AttendeeRestriction.MinAge);
            }

            return minAge;
        }

        public int? GetMaxAge(Program program)
        {
            int? maxAge = null;

            if (program.ProgramSchedules.Where(w => !w.IsDeleted).Any(w => w.AttendeeRestriction.RestrictionType == RestrictionType.None || w.AttendeeRestriction.RestrictionType == RestrictionType.Grade))
            {
                return null;
            }

            if (program.ProgramSchedules.Where(w => !w.IsDeleted).Any(w => w.AttendeeRestriction.RestrictionType == RestrictionType.Age))
            {
                if (program.ProgramSchedules.Where(w => !w.IsDeleted && w.AttendeeRestriction.RestrictionType == RestrictionType.Age).Any(m => m.AttendeeRestriction.MaxAge == null))
                {
                    return maxAge;
                }

                maxAge = program.ProgramSchedules.Where(w => !w.IsDeleted && w.AttendeeRestriction.RestrictionType == RestrictionType.Age).Max(m => m.AttendeeRestriction.MaxAge);
            }

            return maxAge;
        }

        public SchoolGradeType? GetMinGrade(Program program)
        {
            SchoolGradeType? minGrade = null;

            if (program.ProgramSchedules.Where(w => !w.IsDeleted).Any(w => w.AttendeeRestriction.RestrictionType == RestrictionType.None || w.AttendeeRestriction.RestrictionType == RestrictionType.Age))
            {
                return null;
            }

            if (program.ProgramSchedules.Where(w => !w.IsDeleted).Any(w => w.AttendeeRestriction.RestrictionType == RestrictionType.Grade))
            {
                if (program.ProgramSchedules.Where(w => !w.IsDeleted && w.AttendeeRestriction.RestrictionType == RestrictionType.Grade).Any(m => m.AttendeeRestriction.MinGrade == null))
                {
                    return minGrade;
                }

                minGrade = program.ProgramSchedules.Where(w => !w.IsDeleted && w.AttendeeRestriction.RestrictionType == RestrictionType.Grade).Min(m => m.AttendeeRestriction.MinGrade);
            }

            return minGrade;
        }

        public SchoolGradeType? GetMaxGrade(Program program)
        {
            SchoolGradeType? maxGrade = null;

            if (program.ProgramSchedules.Where(w => !w.IsDeleted).Any(w => w.AttendeeRestriction.RestrictionType == RestrictionType.None || w.AttendeeRestriction.RestrictionType == RestrictionType.Age))
            {
                return null;
            }

            if (program.ProgramSchedules.Where(w => !w.IsDeleted).Any(w => w.AttendeeRestriction.RestrictionType == RestrictionType.Grade))
            {
                if (program.ProgramSchedules.Where(w => !w.IsDeleted && w.AttendeeRestriction.RestrictionType == RestrictionType.Grade).Any(m => m.AttendeeRestriction.MaxGrade == null))
                {
                    return maxGrade;
                }

                maxGrade = program.ProgramSchedules.Where(w => !w.IsDeleted && w.AttendeeRestriction.RestrictionType == RestrictionType.Grade).Max(m => m.AttendeeRestriction.MaxGrade);
            }

            return maxGrade;
        }

        public DateTime? StartDate(Program program)
        {
            if (program.ProgramSchedules != null && program.ProgramSchedules.Where(s => !s.IsDeleted).Any())
            {
                var result = program.ProgramSchedules.Where(s => !s.IsDeleted).Min(s => s.StartDate);
                return result;
            }

            return null;
        }

        public DateTime? EndDate(Program program)
        {
            if (program.ProgramSchedules != null && program.ProgramSchedules.Where(s => !s.IsDeleted).Any())
            {
                var result = program.ProgramSchedules.Where(s => !s.IsDeleted).Max(s => s.EndDate);
                return result;
            }

            return null;
        }

        public DateTime RegisterStartDate(Program program)
        {
            return program.ProgramSchedules.Where(s => !s.IsDeleted).Min(s => s.RegistrationPeriod.RegisterStartDate.Value.AddTicks(s.RegistrationPeriod.RegisterStartTime.HasValue ? s.RegistrationPeriod.RegisterStartTime.Value.Ticks : 0));
        }

        public DateTime RegisterEndDate(Program program)
        {
            if (program.TypeCategory == ProgramTypeCategory.Calendar)
            {
                return program.ProgramSchedules.Where(s => !s.IsDeleted).Max(s => s.Sessions.Max(m => m.End));
            }
            else
            {
                if (program.ProgramSchedules.Where(s => !s.IsDeleted).Max(s => s.RegistrationPeriod.RegisterEndDate).HasValue)
                {
                    return program.ProgramSchedules.Where(s => !s.IsDeleted).Max(s => s.RegistrationPeriod.RegisterEndDate.Value.AddTicks(s.RegistrationPeriod.RegisterEndTime.HasValue ? s.RegistrationPeriod.RegisterEndTime.Value.Ticks : TimeSpan.Parse("23:59:59").Ticks));
                }

                return DateTime.UtcNow.AddYears(-20);
            }
        }

        public string ProgramPaymentPriod(ProgramSchedule schedule)
        {
            var defaultScheduleAttribute = (ScheduleAfterBeforeCareAttribute)schedule.Attributes;

            var paymentPriod = defaultScheduleAttribute.PaymentScheduleMode;

            return paymentPriod;
        }

        public List<string> GetScheduleTime(ProgramSchedule schedule)
        {
            var scheduleTime = new List<string>();

            var programTimes = new List<ScheduleTimesViewModel>();

            programTimes.AddRange(GetTimesProgram(schedule));

            if (programTimes.Count >= 2)
            {
                var groupTimes = programTimes.GroupBy(t => t.Day).ToList();
                foreach (var time in groupTimes)
                {
                    if (time.ToList().Count > 1)
                    {
                        scheduleTime.Add(String.Format("{0}: {1} {2} {3}", time.FirstOrDefault().Day, time.FirstOrDefault().MainTime, " ", time.LastOrDefault().MainTime));
                    }
                    else
                    {
                        scheduleTime.Add(String.Format("{0}: {1}", time.FirstOrDefault().Day, time.FirstOrDefault().MainTime));
                    }
                }
            }
            else
            {
                foreach (var time in programTimes)
                {
                    scheduleTime.Add(time.MainTime);
                }
            }

            return scheduleTime;
        }
        public List<ScheduleTimesViewModel> GetTimesProgram(ProgramSchedule schedule)
        {
            var programTimes = new List<ScheduleTimesViewModel>();

            var scheduleAttribute = schedule.Attributes as ScheduleAfterBeforeCareAttribute;

            var days = scheduleAttribute.Days;

            foreach (var day in days)
            {
                if (programTimes.Count == 0)
                {
                    programTimes.Add(new ScheduleTimesViewModel
                    {
                        Day = day.DayOfWeek.ToString(),
                        StartTime = day.StartTime.Value,
                        EndTime = day.EndTime.Value,
                        MainTime = String.Format("{0} - {1}", DateTime.Today.Add(day.StartTime.Value).ToString("h:mm tt"), DateTime.Today.Add(day.EndTime.Value).ToString("h:mm tt")),

                    });
                }
                else
                {
                    var timeCount = 0;
                    foreach (var availabelTime in programTimes)
                    {
                        if (day.StartTime == availabelTime.StartTime && day.EndTime == availabelTime.EndTime)
                        {
                            timeCount++;

                            availabelTime.Day = availabelTime.Day + ", " + day.DayOfWeek.ToString();
                        }
                    }
                    if (timeCount == 0)
                    {
                        programTimes.Add(new ScheduleTimesViewModel
                        {
                            Day = day.DayOfWeek.ToString(),
                            StartTime = day.StartTime.Value,
                            EndTime = day.EndTime.Value,
                            MainTime = String.Format("{0} - {1}", DateTime.Today.Add(day.StartTime.Value).ToString("h:mm tt"), DateTime.Today.Add(day.EndTime.Value).ToString("h:mm tt")),
                        });
                    }
                }
            }

            return programTimes;
        }

        public List<string> GetComboTime(List<ProgramSchedule> schedules)
        {
            var comboTime = new List<string>();

            var programTimes = new List<ScheduleTimesViewModel>();


            foreach (var schedule in schedules)
            {
                programTimes.AddRange(GetTimesProgram(schedule));
            }

            if (programTimes.Count > 2)
            {
                var groupTimes = programTimes.GroupBy(t => t.Day).ToList();
                foreach (var time in groupTimes)
                {
                    if (time.ToList().Count > 1)
                    {
                        comboTime.Add(String.Format("{0}: {1} {2} {3}", time.FirstOrDefault().Day, time.FirstOrDefault().MainTime, " ", time.LastOrDefault().MainTime));
                    }
                    else
                    {
                        comboTime.Add(String.Format("{0}: {1}", time.FirstOrDefault().Day, time.FirstOrDefault().MainTime));
                    }
                }
            }
            else
            {
                foreach (var time in programTimes)
                {
                    comboTime.Add(time.MainTime);
                }
            }

            return comboTime;
        }
        public List<ScheduleSession> GenerateSessions(DateTime StartDate, DateTime? endDate, List<ProgramScheduleDay> days, ContinueType contineuType, int occurances)
        {
            DateTime? scheduleEndDate = null;

            switch (contineuType)
            {
                case ContinueType.Until:
                    {
                        scheduleEndDate = endDate;
                    }
                    break;
                case ContinueType.For:
                    {

                        if (endDate.HasValue)
                        {
                            TimeSpan time = endDate.Value - StartDate;
                            var countDays = time.TotalDays;
                            scheduleEndDate = StartDate.AddDays(countDays);
                        }
                        else
                        {
                            scheduleEndDate = StartDate.AddDays(7 * occurances);
                        }

                    }
                    break;
            }

            var scheduleSections = new List<ScheduleSession>();

            DateTime currentDate = StartDate;

            var random = new Random();

            // Add selected days to schedule list
            while (currentDate <= scheduleEndDate)
            {
                DayOfWeek dayOfWeek = currentDate.DayOfWeek;

                foreach (var item in days)
                {
                    if (dayOfWeek == item.DayOfWeek)
                    {
                        scheduleSections.Add(new
                            ScheduleSession()
                        {
                            Id = random.Next(1000000, 9999999),
                            Start = currentDate.Date.Add(item.StartTime.Value),
                            End = currentDate.Date.Add(item.EndTime.Value),
                        });
                    }
                }

                currentDate = currentDate.AddDays(1);
            }

            return scheduleSections;
        }

        public OperationStatus UpdateSessionItem(ScheduleSession sessionItem, long programId)
        {
            var program = Get(programId);

            var sessions = GetSessions(program).ToList();

            var session = sessions.Single(s => s.Id == sessionItem.Id);

            session.End = sessionItem.End;
            session.Start = sessionItem.Start;

            var result = UpdateSessions(sessions, programId);

            return result;
        }

        public OperationStatus DeleteSessionItem(int sessionItemId, long programId)
        {
            var program = Get(programId);

            var sessions = GetSessions(program).ToList();

            var session = sessions.Single(s => s.Id == sessionItemId);

            sessions.Remove(session);

            var result = UpdateSessions(sessions, programId);

            return result;
        }

        public OperationStatus UpdateSessions(List<ScheduleSession> sessions, long programId)
        {
            var program = Get(programId);

            var programSchedule = program.ProgramSchedules.Where(s => !s.IsDeleted).First();

            programSchedule.SectionsSerialized = JsonHelper.JsonSerializer(sessions);

            var result = Update(program);

            return result;
        }

        public IEnumerable<ScheduleSession> GetSessions(Program program)
        {
            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted).ToList();

            switch (program.TypeCategory)
            {
                case ProgramTypeCategory.Subscription:
                    {
                        var programSchedule = programSchedules.SingleOrDefault();

                        return GetSubscriptionSessions(programSchedule);
                    }
                case ProgramTypeCategory.BeforeAfterCare:
                    {
                        return GetBeforeAfterSessions(programSchedules);
                    }
                case ProgramTypeCategory.Camp:
                    {
                        return GetCampSessions(program);
                    }
                default:
                    {
                        return GetClassSessions(program, programSchedules.ToList());
                    }
            }
        }

        public List<ScheduleSession> GetSessions(ProgramSchedule schedule)
        {
            var result = new List<ScheduleSession>();

            result = schedule.Sessions;

            return result;
        }

        public List<ScheduleSession> GetSessions(IEnumerable<ProgramSchedule> schedules)
        {
            var result = new List<ScheduleSession>();
            foreach (var schedule in schedules)
            {
                switch (schedule.Program.TypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        {
                            var programScheduleMode = GetClassScheduleTime(schedule.Program);
                            var programSessions = GetClassDates(schedule, programScheduleMode).Select(p =>
                                new ScheduleSession()
                                {
                                    Id = p.Id,
                                    Start = p.StartTime,
                                    End = p.EndTime,
                                    ScheduleId = schedule.Id
                                }).ToList();

                            result.AddRange(programSessions);
                        }
                        break;
                    case ProgramTypeCategory.BeforeAfterCare:
                    case ProgramTypeCategory.Subscription:
                        {
                            var programSessions = _programSessionBusiness.GetList(schedule);

                            _programSessionBusiness.ApplyHolidays(schedule.Program, programSessions);

                            result.AddRange(programSessions.OrderBy(p => p.StartDateTime).Select(p => new ScheduleSession()
                            {
                                Id = p.Id,
                                Start = p.StartDateTime,
                                End = p.EndDateTime,
                                ScheduleId = p.ProgramScheduleId.Value
                            }).ToList());
                        }
                        break;
                    case ProgramTypeCategory.Camp:
                        {
                            var programSessions = _programSessionBusiness.GetList().Where(p => p.Charge.ProgramScheduleId.Value == schedule.Id && !p.IsDeleted && !p.Charge.IsDeleted).ToList();

                            _programSessionBusiness.ApplyHolidays(schedule.Program, programSessions);

                            result.AddRange(programSessions.Select(p => new ScheduleSession()
                            {
                                Id = p.Id,
                                Start = p.StartDateTime,
                                End = p.EndDateTime,
                                ScheduleId = p.Charge.ProgramScheduleId.Value
                            }).ToList());
                        }
                        break;
                }
            }

            return result;
        }

        public List<ScheduleSession> GetAllClassSessions(Program program)
        {
            var result = new List<ScheduleSession>();

            result = program.ProgramSchedules.First().Sessions.Where(c => !c.IsDeleted).ToList();

            return result;
        }

        public ScheduleSession GetSession(Program program, int sessionId)
        {
            ScheduleSession result = null;

            var sessions = GetSessions(program);

            if (sessions != null && sessions.Any())
            {
                return sessions.SingleOrDefault(s => s.Id == sessionId);
            }

            return result;
        }

        public ProgramSchedule GetSchedule(long scheduleId)
        {
            return _dataContext.Set<ProgramSchedule>().Find(scheduleId);
        }

        public int GetScheduleCapacity(ProgramSchedule programSchedule)
        {
            var capacity = 0;

            var scheduleAttribute = programSchedule.Attributes;
            capacity = scheduleAttribute.Capacity;

            return capacity;
        }

        public bool IsSessionsEdited(long scheduleId)
        {
            var result = false;

            var currentSchedule = GetSchedule(scheduleId);

            if (currentSchedule == null)
            {
                return false;
            }

            var generatedSessions = GenerateSessions(currentSchedule.StartDate, currentSchedule.EndDate, ((ScheduleAttribute)currentSchedule.Attributes).Days, ((ScheduleAttribute)currentSchedule.Attributes).ContinueType, ((ScheduleAttribute)currentSchedule.Attributes).Occurances);

            var currentSessions = currentSchedule.Sessions;

            if (currentSessions.Count() != generatedSessions.Count())
            {
                return true;
            }

            for (int i = 0; i < currentSessions.Count(); i++)
            {
                if (currentSessions[i].Start != generatedSessions[i].Start)
                {
                    return true;
                }

                if (currentSessions[i].End != generatedSessions[i].End)
                {
                    return true;
                }

                if (currentSessions[i].IsDeleted != generatedSessions[i].IsDeleted)
                {
                    return true;
                }
            }

            return result;
        }

        public List<ScheduleAttendance> GetSessionAttendance(long programId, int sessionId)
        {
            var program = Get(programId);
            switch (program.TypeCategory)
            {
                case ProgramTypeCategory.Class:
                    {
                        var allAttendances = Get(programId).ProgramSchedules.SelectMany(p => p.OrderItems)
                    .Where(o => o.ItemStatus == OrderItemStatusCategories.completed &&
                                o.Order.IsLive == (o.Season.Status == SeasonStatus.Live) ||
                               (o.ItemStatus == OrderItemStatusCategories.changed &&
                               (o.ItemStatusReason == OrderItemStatusReasons.regular || o.ItemStatusReason == OrderItemStatusReasons.transferIn || o.ItemStatusReason == OrderItemStatusReasons.refund)))
                    .Select(p =>
                        new ScheduleAttendance
                        {
                            PlayerProfile = p.Player,
                            ProfileId = p.PlayerId.Value,
                            ScheduleId = p.ProgramScheduleId.Value,
                            Schedule = p.ProgramSchedule,
                            SessionId = sessionId,
                        })
                        .GroupBy(g => g.ProfileId)
                        .Select(g => g.First())
                        .ToList();

                        var allAttendancesProfileId = allAttendances.Select(p => p.ProfileId).ToList();
                        var allAttendancesScheduleId = allAttendances.Select(p => p.ScheduleId).ToList();

                        var scheduleAttendances = _dataContext.Set<ScheduleAttendance>()
                                    .Where(a => allAttendancesProfileId.Contains(a.ProfileId)
                                    && a.SessionId == sessionId
                                    && allAttendancesScheduleId.Contains(a.ScheduleId)).ToList();

                        foreach (var item in allAttendances)
                        {
                            var scheduleAttendance = scheduleAttendances.SingleOrDefault(s => s.ProfileId == item.ProfileId && s.ScheduleId == item.ScheduleId && s.SessionId == item.SessionId);

                            if (scheduleAttendance == null) continue;

                            item.End = scheduleAttendance.End;
                            item.Id = scheduleAttendance.Id;
                            item.Note = scheduleAttendance.Note;
                            item.Start = scheduleAttendance.Start;
                            item.Status = scheduleAttendance.Status;
                            item.ClubStaffId = scheduleAttendance.ClubStaffId;
                            item.IsPickedUp = scheduleAttendance.IsPickedUp;
                            item.PickupSerialized = scheduleAttendance.PickupSerialized;
                            item.AttendanceDate = scheduleAttendance.AttendanceDate;
                            item.PickedUpDate = scheduleAttendance.PickedUpDate;
                        }

                        return allAttendances;
                    }


                case ProgramTypeCategory.Subscription:
                case ProgramTypeCategory.BeforeAfterCare:
                case ProgramTypeCategory.Camp:
                    {
                        var date = _programSessionBusiness.Get(sessionId).StartDateTime.Date;

                        var orderItems = _orderItemBusiness.GetProgramOrderItems(programId, OrderItemStatusCategories.showAll)
                            .ToList()
                            .Where(o => o.ItemStatus == OrderItemStatusCategories.completed || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.CancelEffectiveDate > date) || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.TransferEffectiveDate > date))
                            .ToList();


                        var allAttendances = orderItems.Where(i => i.OrderSessions.ToList().Select(o => o.ProgramSession.Id).ToList().Any(s => s == sessionId))
                            .Select(p => new StudentAttendance
                            {
                                OrderSession = p.OrderSessions.SingleOrDefault(o => o.ProgramSessionId == sessionId)
                            }).ToList();

                        var allAttendancesOrderSessionId = allAttendances.Select(p => p.OrderSession.Id).ToList();

                        var studentAttendances = _dataContext.Set<StudentAttendance>()
                                    .Where(a => allAttendancesOrderSessionId.Contains(a.OrderSessionId)).ToList();

                        foreach (var item in allAttendances)
                        {
                            var studentAttendance = studentAttendances.SingleOrDefault(s => s.OrderSessionId == item.OrderSession.Id);

                            if (studentAttendance == null) continue;

                            item.Id = studentAttendance.Id;
                            item.AttendanceStaffId = studentAttendance.AttendanceStaffId;
                            item.AttendanceStatus = studentAttendance.AttendanceStatus;
                            item.AttendanceDate = studentAttendance.AttendanceDate;
                            item.DismissalInfoSerialized = studentAttendance.DismissalInfoSerialized;
                            item.IsDismissed = studentAttendance.IsDismissed;
                            item.DismissalDate = studentAttendance.DismissalDate;
                        }

                        var result = allAttendances.Select(a => new ScheduleAttendance()
                        {
                            Id = a.Id,
                            ProfileId = a.OrderSession.OrderItem.PlayerId ?? 0,
                            PlayerProfile = a.OrderSession.OrderItem.Player,
                            Schedule = a.OrderSession.OrderItem.ProgramSchedule,
                            ScheduleId = a.OrderSession.OrderItem.ProgramScheduleId ?? 0,
                            SessionId = a.OrderSession.ProgramSessionId ?? 0,
                            ClubStaff = a.AttendanceStaff,
                            IsPickedUp = a.IsDismissed,
                            PickupSerialized = a.DismissalInfoSerialized,
                            Status = a.AttendanceStatus,
                            PickedUpDate = a.DismissalDate,
                            AttendanceDate = a.AttendanceDate,
                            Start = a.OrderSession.ProgramSession.StartDateTime,
                            End = a.OrderSession.ProgramSession.EndDateTime
                        }).ToList();

                        return result;
                    }
            }

            return null;
        }

        public List<ScheduleAttendance> GetSessionAttendance_V2_2_1(long programId, int sessionId, int page, int pageSize, ref int totalCount)
        {
            var program = Get(programId);
            switch (program.TypeCategory)
            {
                case ProgramTypeCategory.Class:
                    {
                        var query = _orderItemBusiness.GetProgramOrderItems(programId)
                        .Where(o => o.ItemStatus == OrderItemStatusCategories.completed &&
                                    o.Order.IsLive == (o.Season.Status == SeasonStatus.Live) ||
                                   (o.ItemStatus == OrderItemStatusCategories.changed &&
                                   (o.ItemStatusReason == OrderItemStatusReasons.regular || o.ItemStatusReason == OrderItemStatusReasons.transferIn || o.ItemStatusReason == OrderItemStatusReasons.refund)))
                        .ToList()
                        .GroupBy(g => g.PlayerId.Value)
                        .Select(g => g.First());

                        totalCount = query.Count();

                        var allAttendances = query.OrderBy(o => o.Player.Contact.LastName).ThenBy(o => o.Player.Contact.FirstName)
                            .Skip((page - 1) * pageSize).Take(pageSize)
                            .Select(p =>
                            new ScheduleAttendance
                            {
                                PlayerProfile = p.Player,
                                ProfileId = p.PlayerId.Value,
                                ScheduleId = p.ProgramScheduleId.Value,
                                Schedule = p.ProgramSchedule,
                                SessionId = sessionId,
                            })
                            .ToList();

                        var allAttendanceProfileIds = allAttendances.Select(p => p.ProfileId).ToList();

                        var allAttendanceScheduleIds = allAttendances.Select(p => p.ScheduleId).ToList();

                        var shceduleAttendances = _dataContext.Set<ScheduleAttendance>()
                                    .Where(a => allAttendanceProfileIds.Contains(a.ProfileId)
                                    && a.SessionId == sessionId
                                    && allAttendanceScheduleIds.Contains(a.ScheduleId)).ToList();

                        foreach (var item in allAttendances)
                        {
                            var studentAttendance = shceduleAttendances.SingleOrDefault(s => s.ProfileId == item.ProfileId && s.ScheduleId == item.ScheduleId && s.SessionId == item.SessionId);

                            if (studentAttendance != null)
                            {
                                item.End = studentAttendance.End;
                                item.Id = studentAttendance.Id;
                                item.Note = studentAttendance.Note;
                                item.Start = studentAttendance.Start;
                                item.Status = studentAttendance.Status;
                                item.ClubStaffId = studentAttendance.ClubStaffId;
                                item.IsPickedUp = studentAttendance.IsPickedUp;
                                item.PickupSerialized = studentAttendance.PickupSerialized;
                                item.AttendanceDate = studentAttendance.AttendanceDate;
                                item.PickedUpDate = studentAttendance.PickedUpDate;
                            }
                        }

                        return allAttendances;
                    }

                case ProgramTypeCategory.Camp:
                    break;

                case ProgramTypeCategory.Subscription:
                    {
                        var query = _orderSessionBusiness.GetList().Where(o => o.ProgramSessionId.HasValue && o.ProgramSessionId == sessionId)
                             .Where(o => o.OrderItem.ItemStatus == OrderItemStatusCategories.completed &&
                                    o.OrderItem.Order.IsLive == (o.OrderItem.Season.Status == SeasonStatus.Live) ||
                                   (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed &&
                                   (o.OrderItem.ItemStatusReason == OrderItemStatusReasons.regular || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.transferIn || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.refund)))
                            .GroupBy(g => g.OrderItemId)
                            .Select(g => g.FirstOrDefault());

                        totalCount = query.Count();

                        var allOrderSessions = query.OrderBy(o => o.OrderItem.Player.Contact.LastName).ThenBy(o => o.OrderItem.Player.Contact.FirstName)
                            .Skip((page - 1) * pageSize).Take(pageSize)
                            .ToList()
                            .Select(p => new StudentAttendance
                            {
                                OrderSession = p
                            })
                            .ToList();

                        var allAttendanceOrderSessionIds = allOrderSessions.Select(p => p.OrderSession.Id).ToList();

                        var studentAttendances = _dataContext.Set<StudentAttendance>()
                                    .Where(a => allAttendanceOrderSessionIds.Contains(a.OrderSessionId)).ToList();

                        foreach (var item in allOrderSessions)
                        {
                            var studentAttendance = studentAttendances.SingleOrDefault(s => s.OrderSessionId == item.OrderSession.Id);

                            if (studentAttendance != null)
                            {
                                item.Id = studentAttendance.Id;
                                item.AttendanceStaffId = studentAttendance.AttendanceStaffId;
                                item.AttendanceStatus = studentAttendance.AttendanceStatus;
                                item.AttendanceDate = studentAttendance.AttendanceDate;
                                item.DismissalInfoSerialized = studentAttendance.DismissalInfoSerialized;
                                item.IsDismissed = studentAttendance.IsDismissed;
                                item.DismissalDate = studentAttendance.DismissalDate;
                            }
                        }

                        var result = allOrderSessions.Select(a => new ScheduleAttendance()
                        {
                            Id = a.Id,
                            ProfileId = a.OrderSession.OrderItem.PlayerId.HasValue ? a.OrderSession.OrderItem.PlayerId.Value : 0,
                            PlayerProfile = a.OrderSession.OrderItem.Player,
                            Schedule = a.OrderSession.OrderItem.ProgramSchedule,
                            ScheduleId = a.OrderSession.OrderItem.ProgramScheduleId.HasValue ? a.OrderSession.OrderItem.ProgramScheduleId.Value : 0,
                            SessionId = a.OrderSession.ProgramSessionId.HasValue ? a.OrderSession.ProgramSessionId.Value : 0,
                            ClubStaff = a.AttendanceStaff,
                            IsPickedUp = a.IsDismissed,
                            PickupSerialized = a.DismissalInfoSerialized,
                            Status = a.AttendanceStatus,
                            PickedUpDate = a.DismissalDate,
                            AttendanceDate = a.AttendanceDate,
                            Start = a.OrderSession.ProgramSession.StartDateTime,
                            End = a.OrderSession.ProgramSession.EndDateTime
                        }).ToList();

                        return result;
                    }
                default:
                    break;
            }

            return null;
        }

        public List<StudentAttendance> GetSessionSubscriptionAttendance(long programId, int sessionId)
        {
            var allOrderSessions = _orderSessionBusiness.GetList().Where(o => o.ProgramSessionId.HasValue && o.ProgramSessionId == sessionId)
                .Where(o => o.OrderItem.ItemStatus == OrderItemStatusCategories.completed &&
                       o.OrderItem.Order.IsLive == (o.OrderItem.Season.Status == SeasonStatus.Live) ||
                      (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed &&
                      (o.OrderItem.ItemStatusReason == OrderItemStatusReasons.regular || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.transferIn || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.refund)))
               .GroupBy(g => g.OrderItemId)
               .Select(g => g.FirstOrDefault())
               .ToList()
               .Select(p => new StudentAttendance
               {
                   OrderSession = p
               })
               .ToList();

            var allAttendanceOrderSessionIds = allOrderSessions.Select(p => p.OrderSession.Id).ToList();

            var studentAttendances = _dataContext.Set<StudentAttendance>()
                        .Where(a => allAttendanceOrderSessionIds.Contains(a.OrderSessionId)).ToList();

            foreach (var item in allOrderSessions)
            {
                var studentAttendance = studentAttendances.SingleOrDefault(s => s.OrderSessionId == item.OrderSession.Id);

                if (studentAttendance != null)
                {
                    item.Id = studentAttendance.Id;
                    item.AttendanceStaffId = studentAttendance.AttendanceStaffId;
                    item.AttendanceStatus = studentAttendance.AttendanceStatus;
                    item.AttendanceDate = studentAttendance.AttendanceDate;
                    item.DismissalInfoSerialized = studentAttendance.DismissalInfoSerialized;
                    item.IsDismissed = studentAttendance.IsDismissed;
                    item.DismissalDate = studentAttendance.DismissalDate;
                }
            }
            return allOrderSessions;
        }

        public List<StudentAttendance> GetSessionSubscriptionAttendance_V2_2_1(long programId, int sessionId, int page, int pageSize, ref int totalCount)
        {
            var query = _orderSessionBusiness.GetList().Where(o => o.ProgramSessionId.HasValue && o.ProgramSessionId == sessionId)
                .Where(o => o.OrderItem.ItemStatus == OrderItemStatusCategories.completed &&
                            (o.OrderItem.Order.IsLive == (o.OrderItem.Season.Status == SeasonStatus.Live) ||
                             (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed &&
                              (o.OrderItem.ItemStatusReason == OrderItemStatusReasons.regular || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.transferIn || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.refund))))
               .GroupBy(g => g.OrderItemId)
               .Select(g => g.FirstOrDefault());

            totalCount = query.Count();

            var allOrderSessions = query.OrderBy(o => o.OrderItem.Player.Contact.LastName).ThenBy(o => o.OrderItem.Player.Contact.FirstName)
                .Skip((page - 1) * pageSize).Take(pageSize)
                .Include(i => i.OrderItem.Player.Contact)
                .ToList()
                .Select(p => new StudentAttendance
                {
                    OrderSession = p
                }).ToList();

            var allAttendanceOrderSessionIds = allOrderSessions.Select(p => p.OrderSession.Id).ToList();

            var studentAttendances = _dataContext.Set<StudentAttendance>()
                        .Where(a => allAttendanceOrderSessionIds.Contains(a.OrderSessionId)).ToList();

            foreach (var item in allOrderSessions)
            {
                var studentAttendance = studentAttendances.SingleOrDefault(s => s.OrderSessionId == item.OrderSession.Id);

                if (studentAttendance != null)
                {
                    item.Id = studentAttendance.Id;
                    item.AttendanceStaffId = studentAttendance.AttendanceStaffId;
                    item.AttendanceStatus = studentAttendance.AttendanceStatus;
                    item.AttendanceDate = studentAttendance.AttendanceDate;
                    item.DismissalInfoSerialized = studentAttendance.DismissalInfoSerialized;
                    item.IsDismissed = studentAttendance.IsDismissed;
                    item.DismissalDate = studentAttendance.DismissalDate;
                }
            }

            return allOrderSessions;
        }

        public List<StudentAttendance> GetBeforeAfterAttendance(long programId, DateTime date, int page, int pageSize, ref int totalCount)
        {
            var orderSessions = new List<OrderSession>();

            var programSchedules = Get(programId).ProgramSchedules.Where(p => p.ScheduleMode != TimeOfClassFormation.Both);

            foreach (var schedule in programSchedules)
            {
                orderSessions.AddRange(_orderSessionBusiness.GetReportOrderSessionsWithEffectiveDate(schedule.Id, date));
            }

            orderSessions = orderSessions.DistinctBy(o => o.OrderItemId).ToList();

            totalCount = orderSessions.Count();

            var allOrderSessions = orderSessions.OrderBy(o => o.OrderItem.Player.Contact.LastName)
                .ThenBy(o => o.OrderItem.Player.Contact.FirstName)
                .Skip((page - 1) * pageSize).Take(pageSize)
                .Select(p => new StudentAttendance
                {
                    OrderSession = p
                })
                .ToList();

            var allAttendanceOrderSessionIds = allOrderSessions.Select(p => p.OrderSession.Id).ToList();

            var studentAttendances = _dataContext.Set<StudentAttendance>()
                        .Where(a => allAttendanceOrderSessionIds.Contains(a.OrderSessionId)).ToList();

            foreach (var item in allOrderSessions)
            {
                var studentAttendance = studentAttendances.SingleOrDefault(s => s.OrderSessionId == item.OrderSession.Id);

                if (studentAttendance != null)
                {
                    item.Id = studentAttendance.Id;
                    item.AttendanceStaffId = studentAttendance.AttendanceStaffId;
                    item.AttendanceStatus = studentAttendance.AttendanceStatus;
                    item.AttendanceDate = studentAttendance.AttendanceDate;
                    item.DismissalInfoSerialized = studentAttendance.DismissalInfoSerialized;
                    item.IsDismissed = studentAttendance.IsDismissed;
                    item.DismissalDate = studentAttendance.DismissalDate;
                }
            }

            return allOrderSessions;
        }

        public List<ScheduleAttendance> GetProgramAttendance(long programId)
        {

            var allAttendances = Get(programId).ProgramSchedules.SelectMany(p => p.OrderItems)
                .Where(o => o.Order.IsLive && o.ItemStatus == OrderItemStatusCategories.completed || (o.ItemStatus == OrderItemStatusCategories.changed && (o.ItemStatusReason == OrderItemStatusReasons.regular || o.ItemStatusReason == OrderItemStatusReasons.transferIn || o.ItemStatusReason == OrderItemStatusReasons.refund)))
                .Select(p =>
                    new ScheduleAttendance
                    {
                        PlayerProfile = p.Player,
                        ProfileId = p.PlayerId.Value,
                        ScheduleId = p.ProgramScheduleId.Value,
                    })
                .GroupBy(g => g.ProfileId)
                .Select(g => g.First())
                .ToList();

            return allAttendances;
        }

        public List<ScheduleAttendance> GetProgramAttendance_V2_2_1(long programId, int page, int pageSize, ref int totalCount)
        {
            var query = _orderItemBusiness.GetProgramOrderItems(programId)
                .Where(o => o.ItemStatus == OrderItemStatusCategories.completed &&
                            o.Order.IsLive == (o.Season.Status == SeasonStatus.Live) ||
                           (o.ItemStatus == OrderItemStatusCategories.changed &&
                           (o.ItemStatusReason == OrderItemStatusReasons.regular || o.ItemStatusReason == OrderItemStatusReasons.transferIn || o.ItemStatusReason == OrderItemStatusReasons.refund)))
                .GroupBy(g => g.PlayerId)
                .Select(g => g.FirstOrDefault());

            totalCount = query.Count();

            var allAttendances = query.OrderBy(o => o.Player.Contact.LastName)
                .ThenBy(o => o.Player.Contact.FirstName)
                .Skip((page - 1) * pageSize).Take(pageSize)
                .ToList()
                .Select(p =>
                    new ScheduleAttendance
                    {
                        PlayerProfile = p.Player,
                        ProfileId = p.PlayerId.Value,
                        ScheduleId = p.ProgramScheduleId.Value,
                    })
                .ToList();

            return allAttendances;
        }

        public List<ScheduleAttendance> GetProgramSubscriptionAttendance(long programId, DateTime? dateTime)
        {
            var orderItems = _orderItemBusiness.GetProgramOrderItems(programId).ToList();

            if (dateTime.HasValue)
            {
                orderItems = orderItems.Where(i => i.OrderSessions.ToList().Select(o => o.ProgramSession.StartDateTime).ToList().Any(s => s.Date == dateTime.Value.Date)).ToList();
            }

            var allAttendances = orderItems.ToList()
                .Select(p =>
                    new ScheduleAttendance
                    {
                        PlayerProfile = p.Player,
                        ProfileId = p.PlayerId.Value,
                        ScheduleId = p.ProgramScheduleId.Value,
                    })
                    .GroupBy(g => g.ProfileId)
                    .Select(g => g.First())
                    .ToList();

            return allAttendances;
        }

        public List<ScheduleAttendance> GetProgramSubscriptionAttendance_V2_2_1(long programId, DateTime? dateTime, int page, int pageSize, ref int totalCount)
        {
            var startDate = dateTime.Value.Date;
            var endDate = dateTime.Value.Date.AddDays(1);

            var query = _orderSessionBusiness.GetList().Where(o => o.OrderItem.ProgramSchedule.ProgramId == programId &&
                                                                   o.ProgramSession.StartDateTime >= startDate && o.ProgramSession.StartDateTime < endDate &&
                                                                   (o.OrderItem.ItemStatus == OrderItemStatusCategories.completed) &&
                                                                   (o.OrderItem.Order.IsLive == (o.OrderItem.Season.Status == SeasonStatus.Live) ||
                                                                    (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed &&
                                                                     (o.OrderItem.ItemStatusReason == OrderItemStatusReasons.regular || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.transferIn || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.refund))))
                .GroupBy(g => g.OrderItemId)
                .Select(g => g.FirstOrDefault());

            totalCount = query.Count();

            var allOrderSessions = query
                .OrderBy(o => o.OrderItem.Player.Contact.LastName)
                .ThenBy(o => o.OrderItem.Player.Contact.FirstName)
                .Skip((page - 1) * pageSize).Take(pageSize)
                .Include(i => i.OrderItem.Player)
                .ToList()
                .Select(p => new ScheduleAttendance
                {
                    PlayerProfile = p.OrderItem.Player,
                    ProfileId = p.OrderItem.PlayerId.Value,
                    ScheduleId = p.OrderItem.ProgramScheduleId.Value,
                }).ToList();

            return allOrderSessions;
        }

        public List<ScheduleAttendance> GetProgramBeforeAfterAttendance_V2_2_1(long programId, DateTime? dateTime, int page, int pageSize, ref int totalCount)
        {
            var orderSessions = new List<OrderSession>();

            var programSchedules = Get(programId).ProgramSchedules.Where(p => p.ScheduleMode != TimeOfClassFormation.Both);

            foreach (var schedule in programSchedules)
            {
                orderSessions.AddRange(_orderSessionBusiness.GetReportOrderSessionsWithEffectiveDate(schedule.Id, dateTime.Value));
            }

            orderSessions = orderSessions.DistinctBy(o => o.OrderItemId).ToList();

            totalCount = orderSessions.Count();

            var allOrderSessions = orderSessions
                .OrderBy(o => o.OrderItem.Player.Contact.LastName)
                .ThenBy(o => o.OrderItem.Player.Contact.FirstName)
                .Skip((page - 1) * pageSize).Take(pageSize)
                .ToList()
                .Select(p => new ScheduleAttendance
                {
                    PlayerProfile = p.OrderItem.Player,
                    ProfileId = p.OrderItem.PlayerId.Value,
                    ScheduleId = p.OrderItem.ProgramScheduleId.Value,
                }).ToList();

            return allOrderSessions;
        }

        public List<ScheduleAttendance> UpdateScheduleAttendance(List<ScheduleAttendance> attendances)
        {
            var classAttendances = attendances.Where(a => a.Schedule.Program.TypeCategory == ProgramTypeCategory.Class).ToList();
            var subscriptionAttendances = attendances.Where(a => a.Schedule.Program.TypeCategory == ProgramTypeCategory.Subscription || a.Schedule.Program.TypeCategory == ProgramTypeCategory.Camp || a.Schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare).ToList();

            {
                var profileIds = classAttendances.Select(s => s.ProfileId).ToList();
                var scheduleIds = classAttendances.Select(s => s.ScheduleId).ToList();
                var sessionIds = classAttendances.Select(s => s.SessionId).ToList();

                var shceduleAttendances = _dataContext.Set<ScheduleAttendance>()
                                        .Where(a => profileIds.Contains(a.ProfileId)
                                        && sessionIds.Contains(a.SessionId)
                                        && scheduleIds.Contains(a.ScheduleId)).ToList();

                foreach (var item in attendances)
                {
                    var studentAttendance = shceduleAttendances.SingleOrDefault(s => s.ProfileId == item.ProfileId && s.ScheduleId == item.ScheduleId && s.SessionId == item.SessionId);

                    if (studentAttendance != null)
                    {
                        item.End = studentAttendance.End;
                        item.Id = studentAttendance.Id;
                        item.Note = studentAttendance.Note;
                        item.Start = studentAttendance.Start;
                        item.Status = studentAttendance.Status;
                        item.ClubStaffId = studentAttendance.ClubStaffId;
                        item.IsPickedUp = studentAttendance.IsPickedUp;
                        item.PickupSerialized = studentAttendance.PickupSerialized;
                        item.AttendanceDate = studentAttendance.AttendanceDate;
                        item.PickedUpDate = studentAttendance.PickedUpDate;
                        item.PlayerProfile = studentAttendance.PlayerProfile;
                    }
                }
            }



            {
                var profileIds = subscriptionAttendances.Select(s => s.ProfileId).ToList();
                var scheduleIds = subscriptionAttendances.Select(s => s.ScheduleId).ToList();
                var sessionIds = subscriptionAttendances.Select(s => s.SessionId).ToList();

                var studentAttendances = _dataContext.Set<StudentAttendance>()
                                        .Where(a => profileIds.Contains(a.OrderSession.OrderItem.PlayerId.Value)
                                        && sessionIds.Contains(a.OrderSession.ProgramSessionId.Value)
                                        && scheduleIds.Contains(a.OrderSession.OrderItem.ProgramScheduleId.Value)).ToList();

                foreach (var item in attendances)
                {
                    var studentAttendance = studentAttendances.SingleOrDefault(a => item.ProfileId == a.OrderSession.OrderItem.PlayerId.Value
                                        && item.SessionId == a.OrderSession.ProgramSessionId.Value
                                        && item.ScheduleId == a.OrderSession.OrderItem.ProgramScheduleId.Value);

                    if (studentAttendance != null)
                    {
                        item.Id = studentAttendance.Id;
                        item.Start = studentAttendance.AttendanceDate ?? DateTime.UtcNow;
                        item.Status = studentAttendance.AttendanceStatus;
                        item.ClubStaffId = studentAttendance.AttendanceStaffId;
                        item.IsPickedUp = studentAttendance.IsDismissed;
                        item.PickupSerialized = studentAttendance.DismissalInfoSerialized;
                        item.AttendanceDate = studentAttendance.AttendanceDate;
                        item.PickedUpDate = studentAttendance.DismissalDate;
                        item.PlayerProfile = studentAttendance.OrderSession.OrderItem.Player;
                    }
                }
            }

            return attendances;
        }

        public List<ScheduleAttendance> GetSessionAttendance(List<int> ids)
        {
            return _dataContext.Set<ScheduleAttendance>().Where(a => ids.Contains(a.Id)).ToList();
        }

        public AttendanceStatus? GetStudentAttendanceStatus(int profileId, long scheduleId, int sessionId)
        {
            var query = _dataContext.Set<ScheduleAttendance>().SingleOrDefault(s => s.ProfileId == profileId && s.ScheduleId == scheduleId && s.SessionId == sessionId);
            var status = query != null ? query.Status : null;

            return status;
        }

        public OperationStatus UpdateAttendances(List<ScheduleAttendance> attendances)
        {
            OperationStatus result = new OperationStatus { Status = false };

            var scheduleIds = attendances.Select(a => a.ScheduleId).Distinct().ToList();

            var programs = _programRepository.GetList().Where(p => p.ProgramSchedules.Any(s => scheduleIds.Contains(s.Id))).Distinct()
                .Include("ProgramSchedules").Include("ProgramSchedules.Charges").Include("ProgramSchedules.Charges.ProgramSessions");

            foreach (var scheduleId in scheduleIds)
            {
                var scheduleAttendance = attendances.Where(a => a.ScheduleId == scheduleId);

                var program = programs.SingleOrDefault(p => p.ProgramSchedules.Select(s => s.Id).Contains(scheduleId));

                var clubDateTime = _clubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

                var sessions = GetSessions(program);

                var session = sessions.SingleOrDefault(s => s.Id == scheduleAttendance.First().SessionId);

                var allScheduleAttendances = _dataContext.Set<ScheduleAttendance>().Where(s => s.ScheduleId == scheduleId).ToList();

                var attendanceIdsForEdit = allScheduleAttendances.Where(s => scheduleAttendance.Select(a => a.SessionId).Contains(s.SessionId) && scheduleAttendance.Select(a => a.ProfileId).Contains(s.ProfileId)).ToList();

                foreach (var item in attendanceIdsForEdit)
                {
                    var attendance = scheduleAttendance.Single(o => o.ProfileId == item.ProfileId && o.ScheduleId == item.ScheduleId && o.SessionId == item.SessionId);
                    item.AttendanceDate = item.Status != attendance.Status ? clubDateTime : item.AttendanceDate;
                    item.Status = attendance.Status;
                    item.ClubStaffId = attendance.ClubStaffId;
                }

                var attendancesForAdd = scheduleAttendance.Where(s => !attendanceIdsForEdit.Select(a => a.SessionId).Contains(s.SessionId) || !attendanceIdsForEdit.Select(a => a.ProfileId).Contains(s.ProfileId)).ToList();

                foreach (var item in attendancesForAdd)
                {
                    item.Start = session.Start;
                    item.End = session.End;
                    item.AttendanceDate = clubDateTime;

                    _dataContext.Set<ScheduleAttendance>().Add(item);
                }

            }

            return _programRepository.Save();
        }

        public OperationStatus UpdateAttendancesPickup(List<ScheduleAttendance> attendances)
        {
            OperationStatus result = new OperationStatus { Status = false };

            var scheduleIds = attendances.Select(a => a.ScheduleId).Distinct().ToList();

            var programs = _programRepository.GetList().Where(p => p.ProgramSchedules.Any(s => scheduleIds.Contains(s.Id))).Distinct()
                .Include("ProgramSchedules").Include("ProgramSchedules.Charges").Include("ProgramSchedules.Charges.ProgramSessions");

            foreach (var scheduleId in scheduleIds)
            {
                var scheduleAttendance = attendances.Where(a => a.ScheduleId == scheduleId);

                var program = programs.SingleOrDefault(p => p.ProgramSchedules.Select(s => s.Id).Contains(scheduleId));

                var clubDateTime = _clubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

                var sessions = GetSessions(program);

                var session = sessions.SingleOrDefault(s => s.Id == scheduleAttendance.First().SessionId);

                var allScheduleAttendances = _dataContext.Set<ScheduleAttendance>().Where(s => s.ScheduleId == scheduleId).ToList();

                var attendanceIdsForEdit = allScheduleAttendances.Where(s => scheduleAttendance.Select(a => a.SessionId).Contains(s.SessionId) && scheduleAttendance.Select(a => a.ProfileId).Contains(s.ProfileId)).ToList();

                foreach (var item in attendanceIdsForEdit)
                {
                    var attendance = scheduleAttendance.Single(o => o.ProfileId == item.ProfileId && o.ScheduleId == item.ScheduleId && o.SessionId == item.SessionId);
                    item.PickedUpDate = item.IsPickedUp != attendance.IsPickedUp ? clubDateTime : item.PickedUpDate;
                    item.ClubStaffId = attendance.ClubStaffId;
                    item.IsPickedUp = attendance.IsPickedUp;
                    item.PickupSerialized = attendance.PickupSerialized;
                }

                var attendancesForAdd = scheduleAttendance.Where(s => !attendanceIdsForEdit.Select(a => a.SessionId).Contains(s.SessionId) || !attendanceIdsForEdit.Select(a => a.ProfileId).Contains(s.ProfileId)).ToList();

                foreach (var item in attendancesForAdd)
                {
                    item.Start = session.Start;
                    item.End = session.End;
                    item.PickedUpDate = clubDateTime;

                    _dataContext.Set<ScheduleAttendance>().Add(item);
                }
            }

            return _programRepository.Save();
        }

        public List<ScheduleAttendance> GetStudentAttendance(long programId, int profileId)
        {
            var program = Get(programId);
            var schedule = program.ProgramSchedules.Last(p => !p.IsDeleted);
            var scheduleAttendance = _dataContext.Set<ScheduleAttendance>().Where(p => p.ProfileId == profileId && p.ScheduleId == schedule.Id).ToList();

            return scheduleAttendance;
        }

        public List<ProgramSessionsItemViewModel> GenerateSessionItems(Program program, SchoolGradeType? schoolGradeType)
        {
            var scheduleSections = new List<ProgramSessionsItemViewModel>();
            var random = new Random();

            foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
            {

                DateTime? scheduleEndDate = null;

                if (program.TypeCategory != ProgramTypeCategory.ChessTournament)
                {
                    var attributeContinueType = schedule.Program.TypeCategory != ProgramTypeCategory.BeforeAfterCare ? ((ScheduleAttribute)schedule.Attributes).ContinueType : ((ScheduleAfterBeforeCareAttribute)schedule.Attributes).ContinueType;
                    switch (attributeContinueType)
                    {
                        case ContinueType.Until:
                            {
                                scheduleEndDate = schedule.EndDate;
                            }
                            break;
                        case ContinueType.For:
                            {
                                scheduleEndDate = schedule.StartDate.AddDays(7 * ((ScheduleAttribute)schedule.Attributes).Occurances);
                            }
                            break;
                    }
                }
                else
                {
                    scheduleEndDate = schedule.EndDate;
                }

                DateTime currentDate = schedule.StartDate;

                Dictionary<string, int> gradeList = new Dictionary<string, int>();

                SchoolGradeType? scheduleMinGrade = schedule.AttendeeRestriction.MinGrade;
                SchoolGradeType? scheduleMaxGrade = schedule.AttendeeRestriction.MaxGrade;

                //set true to show all schedule by default
                bool containtSchedule = true;

                // Add selected days to schedule list
                if (schedule.AttendeeRestriction.RestrictionType == RestrictionType.Grade && schoolGradeType != 0)
                {
                    foreach (var e in Enum.GetValues(typeof(SchoolGradeType)))
                    {
                        //I collect range of min and max
                        if ((schedule.AttendeeRestriction.MinGrade.HasValue && (int)e >= (int)schedule.AttendeeRestriction.MinGrade.Value) && (schedule.AttendeeRestriction.MaxGrade.HasValue && (int)e <= (int)schedule.AttendeeRestriction.MaxGrade.Value))
                        {
                            gradeList.Add(e.ToString(), (int)e);
                        }
                        else if (!schedule.AttendeeRestriction.MinGrade.HasValue && (schedule.AttendeeRestriction.MaxGrade.HasValue && (int)e <= (int)schedule.AttendeeRestriction.MaxGrade.Value))
                        {
                            gradeList.Add(e.ToString(), (int)e);
                        }
                        else if ((schedule.AttendeeRestriction.MinGrade.HasValue && (int)e >= (int)schedule.AttendeeRestriction.MinGrade.Value) && !schedule.AttendeeRestriction.MaxGrade.HasValue)
                        {
                            gradeList.Add(e.ToString(), (int)e);
                        }
                    }
                    containtSchedule = gradeList.ContainsKey(schoolGradeType.ToString());
                }

                if (program.TypeCategory == ProgramTypeCategory.SeminarTour)
                {
                    if (containtSchedule)
                    {
                        scheduleSections.Add(new ProgramSessionsItemViewModel()
                        {
                            Id = random.Next(1000000, 9999999),
                            Start = schedule.StartDate,
                            End = schedule.EndDate,
                            //Day = item.DayOfWeek,
                            Title = program.Name,
                            ScheduleMinGrade = (scheduleMinGrade.HasValue && schedule.AttendeeRestriction.RestrictionType == RestrictionType.Grade) ? scheduleMinGrade.Value : SchoolGradeType.NotInSchool,
                            ScheduleMaxGrade = (scheduleMaxGrade.HasValue && schedule.AttendeeRestriction.RestrictionType == RestrictionType.Grade) ? scheduleMaxGrade.Value : SchoolGradeType.College,
                            //ProgramGradeList = gradeList
                            ProgramStatus = GetEmProgramStatus(program, program.Season.Status == SeasonStatus.Test),
                            CapacityLeft = CapacityLeft(program, program.Season.Status == SeasonStatus.Test),
                            Domain = program.Domain,
                            SeasonDomain = program.SeasonDomain,
                            Room = program.Room,
                        });
                    }
                }
                else if (program.TypeCategory == ProgramTypeCategory.ChessTournament)
                {
                    var tournamentAttribute = (TournamentScheduleAttribute)(schedule.Attributes);

                    if (tournamentAttribute.Schedules != null)
                    {
                        foreach (var tournamentSchedule in tournamentAttribute.Schedules)
                        {
                            if (containtSchedule)
                            {

                                scheduleSections.Add(new ProgramSessionsItemViewModel()
                                {
                                    Id = random.Next(1000000, 9999999),
                                    Start = tournamentSchedule.StartDate.Value,
                                    End = tournamentSchedule.EndDate.Value,
                                    //Day = item.DayOfWeek,
                                    Title = program.Name,
                                    ScheduleMinGrade = (scheduleMinGrade.HasValue && schedule.AttendeeRestriction.RestrictionType == RestrictionType.Grade) ? scheduleMinGrade.Value : SchoolGradeType.NotInSchool,
                                    ScheduleMaxGrade = (scheduleMaxGrade.HasValue && schedule.AttendeeRestriction.RestrictionType == RestrictionType.Grade) ? scheduleMaxGrade.Value : SchoolGradeType.College,
                                    //ProgramGradeList = gradeList
                                    ProgramStatus = GetEmProgramStatus(program, program.Season.Status == SeasonStatus.Test),
                                    CapacityLeft = CapacityLeft(program, program.Season.Status == SeasonStatus.Test),
                                    Domain = program.Domain,
                                    SeasonDomain = program.SeasonDomain,
                                    Room = program.Room,
                                });
                            }
                        }
                    }
                }
                else
                {
                    while (currentDate <= scheduleEndDate)
                    {
                        DayOfWeek dayOfWeek = currentDate.DayOfWeek;

                        var days = GetProgramScheduleDays(program, schedule);

                        foreach (var item in days)
                        {
                            if (containtSchedule)
                            {
                                if (dayOfWeek == item.DayOfWeek)
                                {
                                    scheduleSections.Add(new
                                       ProgramSessionsItemViewModel()
                                    {
                                        Id = random.Next(1000000, 9999999),
                                        Start = currentDate.Date.Add(item.FirstStartTime.Value),
                                        End = currentDate.Date.Add(item.FirstEndTime.Value),
                                        SecondStart = item.SecondStartTime.HasValue ? currentDate.Date.Add(item.SecondStartTime.Value) : (DateTime?)null,
                                        SecondEnd = item.SecondEndTime.HasValue ? currentDate.Date.Add(item.SecondEndTime.Value) : (DateTime?)null,
                                        Day = item.DayOfWeek,
                                        Title = schedule.Program.TypeCategory != ProgramTypeCategory.BeforeAfterCare ? program.Name : $"{program.Name} - {schedule.Title}",
                                        ScheduleMinGrade = (scheduleMinGrade.HasValue && schedule.AttendeeRestriction.RestrictionType == RestrictionType.Grade) ? scheduleMinGrade.Value : SchoolGradeType.NotInSchool,
                                        ScheduleMaxGrade = (scheduleMaxGrade.HasValue && schedule.AttendeeRestriction.RestrictionType == RestrictionType.Grade) ? scheduleMaxGrade.Value : SchoolGradeType.College,
                                        //ProgramGradeList = gradeList
                                        ProgramStatus = GetEmProgramStatus(program, program.Season.Status == SeasonStatus.Test),
                                        CapacityLeft = CapacityLeft(program, program.Season.Status == SeasonStatus.Test),
                                        Domain = program.Domain,
                                        SeasonDomain = program.SeasonDomain,
                                        Room = program.Room,
                                    });
                                }
                            }
                        }

                        currentDate = currentDate.AddDays(1);
                    }
                }
            }

            return scheduleSections;
        }

        public List<ChargeItemViewModel> GetClassFees(Program program, CurrencyCodes currency)
        {
            var result = new List<ChargeItemViewModel>();

            if (program.TypeCategory != ProgramTypeCategory.Camp && program.TypeCategory != ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var sch in program.ProgramSchedules)
                {
                    var charges = sch.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false).ToList();

                    if (charges.Any())
                    {
                        result.AddRange(charges.Select(c => MapChargeItemViewModel(c, program.Season.Status == SeasonStatus.Test, sch.Attributes.ShowCapacityLeft, program.Club, program.Season)).ToList());
                    }
                }
            }

            var jsonResult = JsonConvert.SerializeObject(result);

            return result;
        }

        public List<string> GetProgramChargesCapacities(Program program)
        {
            var result = new List<string>();
            var countCharges = 0;

            if (program.TypeCategory != ProgramTypeCategory.Camp)
            {
                foreach (var schedule in program.ProgramSchedules.ToList())
                {
                    var charges = schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false).ToList();
                    countCharges = charges.Count;
                    foreach (var charge in charges)
                    {
                        var chargeCapacity = GetCapacity(charge);

                        if (charges.Count == 1 && chargeCapacity != null)
                        {
                            result.Add(chargeCapacity?.ToString());

                            return result;
                        }
                        else if (charges.Count > 1)
                        {
                            result.Add(chargeCapacity?.ToString());
                        }
                    }
                }

                var capacity = GetCapacity(program);

                if (capacity.HasValue && capacity > 0)
                {
                    if (countCharges > 1)
                    {
                        result.Add($"Total: {capacity}");
                    }
                    else
                    {
                        result.Add(capacity.ToString());
                    }
                }
            }

            return result;
        }

        public List<KeyValuePair<string, string>> GetProgramChargesSpotsLeft(Program program, bool isTestMode)
        {
            var result = new List<KeyValuePair<string, string>>();
            var capacity = GetCapacity(program);

            if (program.TypeCategory != ProgramTypeCategory.Camp)
            {
                foreach (var schedule in program.ProgramSchedules.ToList())
                {
                    var countEnrollment = 0;
                    var charges = schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false).ToList();

                    foreach (var charge in charges)
                    {
                        countEnrollment += GetRegisteredCount(charge, isTestMode);
                        if (schedule.Attributes.ShowCapacityLeft)
                        {
                            var capacityLeft = CapacityLeft(charge, isTestMode);
                            if (capacityLeft <= schedule.Attributes.ShowCapacityLeftNumber)
                            {
                                result.Add(new KeyValuePair<string, string>(charge.Name.Replace("'", "&#39;"), capacityLeft == 0 ? "Full" : capacityLeft == null ? "" : capacityLeft.ToString()));
                            }
                            else
                            {
                                if (capacityLeft > 0)
                                {
                                    result.Add(new KeyValuePair<string, string>(charge.Name.Replace("'", "&#39;"), "Available"));
                                }
                                else
                                {
                                    result.Add(new KeyValuePair<string, string>(charge.Name.Replace("'", "&#39;"), ""));
                                }
                            }
                        }
                        else if (program.Season.Setting != null && program.Season.Setting.SeasonAdditionalSetting != null && program.Season.Setting.SeasonAdditionalSetting.ShowRemainingSpots)
                        {
                            var capacityLeft = CapacityLeft(charge, isTestMode);

                            if (capacityLeft <= program.Season.Setting.SeasonAdditionalSetting.CapacityLeftNumbers)
                            {
                                result.Add(new KeyValuePair<string, string>(charge.Name.Replace("'", "&#39;"), capacityLeft == 0 ? "Full" : capacityLeft == null ? "" : capacityLeft.ToString()));
                            }
                            else
                            {
                                if (capacityLeft > 0)
                                {
                                    result.Add(new KeyValuePair<string, string>(charge.Name.Replace("'", "&#39;"), "Available"));
                                }
                                else
                                {
                                    result.Add(new KeyValuePair<string, string>(charge.Name.Replace("'", "&#39;"), ""));
                                }
                            }
                        }
                    }

                    if (capacity.HasValue && capacity > 0 && !result.Any(r => r.Value != ""))
                    {
                        var capacityLeft = capacity - countEnrollment;
                        if (schedule.Attributes.ShowCapacityLeft)
                        {
                            if (capacityLeft <= schedule.Attributes.ShowCapacityLeftNumber)
                            {
                                result.Add(new KeyValuePair<string, string>("", capacityLeft == 0 ? "Full" : capacityLeft == null ? "" : capacityLeft.ToString()));
                            }
                            else
                            {
                                result.Add(new KeyValuePair<string, string>("Available", "Available"));
                            }
                        }
                        else if (program.Season.Setting != null && program.Season.Setting.SeasonAdditionalSetting != null && program.Season.Setting.SeasonAdditionalSetting.ShowRemainingSpots)
                        {
                            if (capacityLeft <= program.Season.Setting.SeasonAdditionalSetting.CapacityLeftNumbers)
                            {
                                result.Add(new KeyValuePair<string, string>("", capacityLeft == 0 ? "Full" : capacityLeft == null ? "" : capacityLeft.ToString()));
                            }
                            else
                            {
                                result.Add(new KeyValuePair<string, string>("Available", "Available"));
                            }
                        }
                    }
                }
            }

            return result;
        }

        public JbForm GetJbForm(int id)
        {
            return _dataContext.Set<JbForm>().SingleOrDefault(j => j.Id == id);
        }

        public string GenerateSubDomainName(string clubDomain, string seasonDomain, string name)
        {
            string domain = string.Empty;
            bool success = false;
            var i = 0;

            domain = JumbulaDomainHelper.GenerateRandomDomain(name, i);
            while (IsProgramDomainExist(clubDomain, seasonDomain, domain))
            {
                i++;
                domain = JumbulaDomainHelper.GenerateRandomDomain(name, i);
            }

            if (!IsProgramDomainExist(clubDomain, seasonDomain, domain))
                success = true;

            if (success == false)
                throw new Exception("Dmain generation failed");

            return domain;
        }

        public bool HasOrder(Program program, bool completedOnly = false)
        {
            bool result;

            if (completedOnly)
            {
                result = _orderBusiness.GetList().Any(order => order.OrderStatus != OrderStatusCategories.deleted && order.OrderItems.Any(item => item.ItemStatus != OrderItemStatusCategories.deleted && item.ItemStatus == OrderItemStatusCategories.completed && item.ProgramSchedule.ProgramId == program.Id));
            }
            else
            {
                var orders = _orderBusiness.GetList().Where(order => order.OrderStatus != OrderStatusCategories.deleted && order.OrderItems.Any(item => item.ItemStatus != OrderItemStatusCategories.deleted && item.ItemStatus != OrderItemStatusCategories.initiated && item.ItemStatus != OrderItemStatusCategories.notInitiated && item.ProgramSchedule.ProgramId == program.Id));

                result = orders.Any();

                if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || program.TypeCategory == ProgramTypeCategory.Subscription)
                {
                    result = orders.Any(o => o.OrderItems.Any(i => i.OrderSessions.Any()));
                }
            }

            return result;
        }

        public bool IsProgramDomainExist(string clubDomain, string seasonDomain, string domain)
        {
            return _dataContext.Set<Program>().Any(p => p.Club.Domain.ToLower().Equals(clubDomain.ToLower()) && p.Season.Domain.Equals(seasonDomain.ToLower()) && p.Domain.Equals(domain.ToLower()));
        }

        public bool IsProgramScheduleProrate(ProgramSchedule schedule, Charge charge)
        {
            if (charge.Attributes.IsProrate)
            {
                DateTime now = DateTimeHelper.GetCurrentLocalDateTime(schedule.Program.Club.TimeZone);
                if (now.Date.CompareTo(charge.Attributes.ProrateStartDate.Value.Date) >= 0)
                {
                    int missedSessions = CalculateMissedSession(schedule, charge.Attributes.ProrateStartDate.Value);
                    if (missedSessions > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public decimal CalculateProratePrice(ProgramSchedule schedule, Charge charge)
        {

            if (charge.Attributes.IsProrate && charge.Attributes.ProrateStartDate.HasValue && charge.Attributes.ProrateSessionPrice.HasValue)
            {
                DateTime now = DateTimeHelper.GetCurrentLocalDateTime(schedule.Program.Club.TimeZone);
                if (now.Date.CompareTo(charge.Attributes.ProrateStartDate.Value.Date) >= 0)
                {

                    int missedSessions = CalculateMissedSession(schedule, charge.Attributes.ProrateStartDate.Value);
                    decimal chargePrice = charge.Amount;
                    if (charge.Attributes.EarlyBirds != null)
                    {
                        foreach (var earlyBrid in charge.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                        {
                            if (now.Date <= earlyBrid.ExpireDate.Value.Date)
                            {
                                chargePrice = earlyBrid.Price;
                                break;
                            }
                        }
                    }

                    var leftprice = chargePrice - (missedSessions * charge.Attributes.ProrateSessionPrice.Value);
                    return leftprice > 0 ? leftprice : 0;
                }
            }
            return charge.Amount;
        }
        public int CalculateMissedSession(ProgramSchedule schedule, DateTime prorateDate)
        {
            if (schedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament)
            {
                return 0;
            }

            DateTime day = prorateDate;
            DateTime now = DateTimeHelper.GetCurrentLocalDateTime(schedule.Program.Club.TimeZone);
            var scheduleDays = ((ScheduleAttribute)schedule.Attributes).Days.Select(d => d.DayOfWeek).ToList();

            int missedSessions = 0;

            var allSessions = GetSessions(schedule.Program);

            missedSessions = GetSessions(schedule).Count(s => s.Start < now && s.Start >= day);

            return missedSessions > 0 ? missedSessions : 0;
        }
        public int CalculateTotalSessions(ProgramSchedule schedule)
        {
            if (schedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament || schedule.Program.TypeCategory == ProgramTypeCategory.Subscription || schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                return 0;
            }

            DateTime day = schedule.StartDate;

            var scheduleDays = ((ScheduleAttribute)schedule.Attributes).Days.Select(d => d.DayOfWeek).ToList();

            int totalSessions = 0;

            while (day.Date <= schedule.EndDate.Date)
            {
                if (scheduleDays.Contains(day.DayOfWeek))
                {
                    totalSessions++;
                }

                day = day.AddDays(1);
            }

            return totalSessions;
        }
        public int CalculateLeftSessions(ProgramSchedule schedule)
        {

            if (schedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament || schedule.Program.TypeCategory == ProgramTypeCategory.Subscription || schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                return 0;
            }

            DateTime now = DateTimeHelper.GetCurrentLocalDateTime(schedule.Program.Club.TimeZone);
            var scheduleDays = ((ScheduleAttribute)schedule.Attributes).Days.Select(d => d.DayOfWeek).ToList();

            int leftSessions = 0;

            var allSessions = GetSessions(schedule.Program);

            leftSessions = GetSessions(schedule).Count(s => s.Start > now);

            return leftSessions > 0 ? leftSessions : 0;
        }

        public IQueryable<Charge> GetAllSchedulesCharges(List<long> programIDs)
        {
            var query =
                _programRepository.GetList()
                    .Where(p => p.Status != ProgramStatus.Deleted && programIDs.Contains(p.Id))
                    .SelectMany(p => p.ProgramSchedules).SelectMany(s => s.Charges);

            return query;
        }

        public ProgramStatusType GetProgramStatusType(Program program, bool isTestMode)
        {
            var programRegStatus = GetRegStatus(program);

            if (program.Status == ProgramStatus.Frozen)
            {
                return ProgramStatusType.Frozen;
            }

            switch (programRegStatus)
            {
                case ProgramRegStatus.NotStarted:
                    {
                        return ProgramStatusType.NotStarted;
                    }
                case ProgramRegStatus.Closed:
                    {
                        return ProgramStatusType.Expired;
                    }
                default:
                    break;
            }

            if (IsFull(program, isTestMode))
            {
                return ProgramStatusType.Full;
            }

            return ProgramStatusType.Available;
        }

        public bool IsProgramBelongsToClub(int clubId, long programId)
        {
            var program = this.Get(programId);
            if (program != null)
            {
                if (program.ClubId == clubId)
                {
                    return true;
                }
            }
            return false;
        }

        public OperationStatus InsertRespond(ProgramRespondToInvitation respond)
        {
            try
            {
                _dataContext.Set<ProgramRespondToInvitation>().Add(respond);
                return _programRepository.Save();
            }
            catch
            {
                throw;

            }
        }

        public string GetEmProgramStatus(Program program, bool isTestMode)
        {
            var status = "Scheduled";

            if (GetProgramStatusType(program, isTestMode) == ProgramStatusType.Frozen)
            {
                status = "Canceled";
            }
            else if (IsFull(program, isTestMode))
            {
                status = "Full";
            }
            else if (program.ProgramSchedules.First().Attributes.MinimumEnrollment > 0 && IsConfirmed(program, isTestMode))
            {
                status = "Confirmed";
            }
            return status;
        }

        public string GenerateGradeInfoLabel(Program program)
        {
            var minGrade = GetMinGrade(program);
            var maxGrade = GetMaxGrade(program);

            return GenerateGradeInfoLabel(minGrade, maxGrade);
        }
        public string GenerateGradeInfoLabelForFlyer(Program program)
        {
            var minGrade = GetMinGrade(program);
            var maxGrade = GetMaxGrade(program);

            return GenerateGradeInfoLabelForFlyer(minGrade, maxGrade);
        }
        public string GenerateGradeInfoLabelForReport(Program program)
        {
            var minGrade = GetMinGrade(program);
            var maxGrade = GetMaxGrade(program);

            return GenerateGradeInfoLabelForReport(minGrade, maxGrade);
        }

        public string GenerateAgeInfoLabel(Program program)
        {
            var minAge = GetMinAge(program);
            var maxAge = GetMaxAge(program);

            return GenerateAgeInfoLabel(minAge, maxAge);
        }

        public string GenerateGradeInfoLabelForReport(SchoolGradeType? min, SchoolGradeType? max)
        {

            var finalLabel = String.Empty;

            if (!min.HasValue)
            {
                min = SchoolGradeType.NotInSchool;
            }

            var minValue = min.ToDescription();

            if (minValue == "Kindergarten")
            {
                minValue = "K";
            }

            if (!max.HasValue)
            {
                max = SchoolGradeType.College;
            }

            string minGrade = (min == SchoolGradeType.NotInSchool) ? null : minValue;
            string maxGrade = (max == SchoolGradeType.College) ? null : max.ToDescription();

            if (String.IsNullOrEmpty(minGrade) && String.IsNullOrEmpty(maxGrade))
            {
                finalLabel = String.Empty;
            }
            else if (!String.IsNullOrEmpty(minGrade) && String.IsNullOrEmpty(maxGrade))
            {
                const string label = "Min grade: {0}";
                finalLabel = String.Format(label, minGrade);
            }
            else if (String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade))
            {
                const string label = "Max grade: {0}";
                finalLabel = String.Format(label, maxGrade);
            }
            else if (!String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade) && min != max)
            {
                const string label = "{0} - {1}";
                finalLabel = String.Format(label, minGrade, maxGrade);
            }
            else if (!String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade) && min == max)
            {
                const string label = "{0}";
                finalLabel = String.Format(label, minGrade);
            }
            return finalLabel;
        }
        public string GenerateGradeInfoLabel(SchoolGradeType? min, SchoolGradeType? max)
        {

            var finalLabel = String.Empty;

            if (!min.HasValue)
            {
                min = SchoolGradeType.NotInSchool;
            }

            if (!max.HasValue)
            {
                max = SchoolGradeType.College;
            }

            string minGrade = (min == SchoolGradeType.NotInSchool) ? null : min.ToDescription();
            string maxGrade = (max == SchoolGradeType.College) ? null : max.ToDescription();

            if (String.IsNullOrEmpty(minGrade) && String.IsNullOrEmpty(maxGrade))
            {
                finalLabel = String.Empty;
            }
            else if (!String.IsNullOrEmpty(minGrade) && String.IsNullOrEmpty(maxGrade))
            {
                const string label = "(Min grade: {0})";
                finalLabel = String.Format(label, minGrade);
            }
            else if (String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade))
            {
                const string label = "(Max grade: {0})";
                finalLabel = String.Format(label, maxGrade);
            }
            else if (!String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade) && min != max)
            {
                const string label = "(Grades: {0} - {1})";
                finalLabel = String.Format(label, minGrade, maxGrade);
            }
            else if (!String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade) && min == max)
            {
                const string label = "(Grade: {0})";
                finalLabel = String.Format(label, minGrade);
            }
            return finalLabel;
        }

        public string GenerateGradeInfoLabelForFlyer(SchoolGradeType? min, SchoolGradeType? max)
        {

            var finalLabel = String.Empty;

            if (!min.HasValue)
            {
                min = SchoolGradeType.NotInSchool;
            }
            var minValue = min.ToDescription();
            if (minValue == "Kindergarten")
            {
                minValue = "K";
            }

            if (!max.HasValue)
            {
                max = SchoolGradeType.College;
            }

            string minGrade = (min == SchoolGradeType.NotInSchool) ? null : minValue;
            string maxGrade = (max == SchoolGradeType.College) ? null : max.ToDescription();

            if (String.IsNullOrEmpty(minGrade) && String.IsNullOrEmpty(maxGrade))
            {
                finalLabel = String.Empty;
            }
            else if (!String.IsNullOrEmpty(minGrade) && String.IsNullOrEmpty(maxGrade))
            {
                const string label = "Min grade: {0}";
                finalLabel = String.Format(label, minGrade);
            }
            else if (String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade))
            {
                const string label = "Max grade: {0}";
                finalLabel = String.Format(label, maxGrade);
            }
            else if (!String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade) && min != max)
            {
                const string label = "Grades: {0} - {1}";
                finalLabel = String.Format(label, minGrade, maxGrade);
            }
            else if (!String.IsNullOrEmpty(minGrade) && !String.IsNullOrEmpty(maxGrade) && min == max)
            {
                const string label = "Grade: {0}";
                finalLabel = String.Format(label, minGrade);
            }
            return finalLabel;
        }

        public string GenerateAgeInfoLabel(int? min, int? max)
        {
            var finalLabel = String.Empty;

            var minAge = min;
            var maxAge = max;

            if (minAge == null && maxAge == null)
            {
                finalLabel = String.Empty;
            }
            else if (minAge != null && maxAge == null)
            {
                const string label = "Min age: {0}";
                finalLabel = String.Format(label, minAge);
            }
            else if (minAge == null && maxAge != null)
            {
                const string label = "Max age: {0}";
                finalLabel = String.Format(label, maxAge);
            }
            else if (minAge != null && maxAge != null && min != max)
            {
                const string label = "Ages: {0} - {1}";
                finalLabel = String.Format(label, minAge, maxAge);
            }
            else if (minAge != null && maxAge != null && min == max)
            {
                const string label = "Ages: {0}";
                finalLabel = String.Format(label, minAge);
            }
            return finalLabel;
        }

        public IQueryable<ClubStaff> GetProgramInstructor(int programId)
        {
            return Get(programId).Instructors.AsQueryable();

        }
        public bool HasActiveOrder(ProgramSchedule programSchedule)
        {
            return programSchedule.OrderItems.Any(o => o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive);
        }

        #region EM
        public int? GetEMRate(Program program)
        {

            return (program.CustomFields.CommisionRate.HasValue && program.CustomFields.CommisionRate != 0) ? program.CustomFields.CommisionRate.Value : (program.OutSourceSeason != null ? program.OutSourceSeason.Club.PartnerCommisionRate : 0);
        }

        public decimal GetEmClassFee(Program program)
        {
            return program.ProgramSchedules.First().Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).First().Amount;
        }

        public List<Coupon> GetProviderFundedCoupons(Program program, List<Coupon> coupons)
        {
            var result = new List<Coupon>();

            if (program.Coupons.Any(c => c.Name.StartsWith("Provider-Funded", StringComparison.OrdinalIgnoreCase)))
            {
                result.AddRange(program.Coupons.Where(c => c.Name.StartsWith("Provider-Funded", StringComparison.OrdinalIgnoreCase)));
            }
            if (coupons != null && coupons.Any(c => c.Name.StartsWith("Provider-Funded", StringComparison.OrdinalIgnoreCase)))
            {
                result.AddRange(coupons.Where(c => c.Name.StartsWith("Provider-Funded", StringComparison.OrdinalIgnoreCase)));
            }

            return result;
        }

        public List<Coupon> GetPTAPTOFundedCoupons(Program program, List<Coupon> coupons)
        {
            var result = new List<Coupon>();

            if (program.Coupons != null && program.Coupons.Any(c => c.Name.StartsWith("PTA-funded", StringComparison.OrdinalIgnoreCase)))
            {
                result.AddRange(program.Coupons.Where(c => c.Name.StartsWith("PTA-funded", StringComparison.OrdinalIgnoreCase)));
            }
            if (coupons != null && coupons.Any(c => c.Name.StartsWith("PTA-funded", StringComparison.OrdinalIgnoreCase)))
            {
                result.AddRange(coupons.Where(c => c.Name.StartsWith("PTA-funded", StringComparison.OrdinalIgnoreCase)));
            }
            if (program.Coupons != null && program.Coupons.Any(c => c.Name.StartsWith("PTO-funded", StringComparison.OrdinalIgnoreCase)))
            {
                result.AddRange(program.Coupons.Where(c => c.Name.StartsWith("PTO-funded", StringComparison.OrdinalIgnoreCase)));
            }
            if (coupons != null && coupons.Any(c => c.Name.StartsWith("PTO-funded", StringComparison.OrdinalIgnoreCase)))
            {
                result.AddRange(coupons.Where(c => c.Name.StartsWith("PTO-funded", StringComparison.OrdinalIgnoreCase)));
            }
            return result;
        }

        public List<Coupon> GetCashPaymentCoupons(Program program, List<Coupon> coupons)
        {
            var result = new List<Coupon>();

            if (program.Coupons != null && program.Coupons.Any(c => c.Name.StartsWith("PTA-funded", StringComparison.OrdinalIgnoreCase)))
            {
                result.AddRange(program.Coupons.Where(c => c.Name.StartsWith("PTA-funded", StringComparison.OrdinalIgnoreCase)));
            }
            if (coupons != null && coupons.Any(c => c.Name.StartsWith("PTA-funded", StringComparison.OrdinalIgnoreCase)))
            {
                result.AddRange(coupons.Where(c => c.Name.StartsWith("PTA-funded", StringComparison.OrdinalIgnoreCase)));
            }

            return result;
        }
        public decimal GetTotalAmtProviderFundedScholarships(Program program, List<Coupon> coupons)
        {
            var providerFundedCouponIds = GetProviderFundedCoupons(program, coupons).Select(c => c.Id).ToList();

            return program.ProgramSchedules.SelectMany(o => o.OrderItems).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.OrderChargeDiscounts.Any( ch => !ch.IsDeleted)).SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue && providerFundedCouponIds.Contains(c.CouponId.Value)).Sum(c => Math.Abs(c.Amount));
        }

        public int TotalRegistration(Program program, bool isTestModem)
        {
            return program.ProgramSchedules.SelectMany(o => o.OrderItems).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive != isTestModem).Any() ? program.ProgramSchedules.SelectMany(o => o.OrderItems).Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive != isTestModem).Count() : 0;
        }
        public int TotalPaidRegistration(Program program, List<OrderItem> OrderItemProgram, bool isTestMode)
        {
            return TotalRegistration(program, isTestMode) - OrderItemProgram.Count(i => i.Order.IsLive != isTestMode && i.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon));

        }
        public decimal GetProviderNoShowFines(Program program)
        {
            return (program.CustomFields != null && program.CustomFields.Noshowpenalty.HasValue) ? (program.CustomFields.Noshowpenalty.Value) : 0;
        }

        public decimal GetProviderLatePenaltyFines(Program program)
        {
            return (program.CustomFields != null && program.CustomFields.LateCheckInPenalty.HasValue) ? (program.CustomFields.LateCheckInPenalty.Value) : 0;
        }
        #endregion
        public OperationStatus AssignRooms(Dictionary<long, string> programRooms)
        {
            var programIds = programRooms.Select(r => r.Key).ToList();
            var programs = _programRepository.GetList(p => programIds.Contains(p.Id));

            foreach (var program in programs)
            {
                var room = String.Empty;

                room = programRooms[program.Id];

                program.Room = room;
                program.MetaData.DateUpdated = DateTime.UtcNow;
            }

            return _programRepository.Save();
        }

        public OperationStatus AssignInstructorsPrograms(Dictionary<long, List<int>> programModels, int clubId)
        {
            var programIds = programModels.Select(p => p.Key).ToList();

            var selectedInstructors = programModels.Select(p => p.Value).ToList();

            for (int i = 0; i < programIds.Count(); i++)
            {
                var selectedInstructor = selectedInstructors.ElementAt(i);

                var program = Get(programIds.ElementAt(i));

                program.Instructors.Clear();

                if (selectedInstructor != null && selectedInstructor.Any())
                {
                    var activeClub = clubId;
                    if (program.OutSourceSeasonId != null)
                    {
                        activeClub = program.OutSourceSeason.ClubId;
                    }

                    var instructors = _clubBusiness.GetClubInstructors(activeClub).Where(c => selectedInstructor.Contains(c.Id)).ToList();

                    CollectionHelper.AddEntities<ClubStaff>(program.Instructors, instructors);
                }
            }

            return _programRepository.Save();
        }

        public List<ProgramScheduleDay> GetProgramScheduleDays(ProgramSchedule programSchedule)
        {
            var result = new List<ProgramScheduleDay>();

            switch (programSchedule.Program.TypeCategory)
            {
                case ProgramTypeCategory.Class:
                case ProgramTypeCategory.Camp:
                case ProgramTypeCategory.Calendar:
                    {
                        result = ((ScheduleAttribute)programSchedule.Attributes).Days.Where(d => d.StartTime.HasValue && d.EndTime.HasValue).ToList();
                    }
                    break;
                case ProgramTypeCategory.SeminarTour:
                case ProgramTypeCategory.ChessTournament:
                    {

                    }
                    break;
                case ProgramTypeCategory.Subscription:
                    {
                        var subscriptionAttribute = (ScheduleSubscriptionAttribute)programSchedule.Attributes;
                        result = subscriptionAttribute.Days != null ? subscriptionAttribute.Days : new List<ProgramScheduleDay>();
                    }
                    break;
                default:
                    break;
            }

            return result;
        }

        public void AddImageGalleryToProgram(Program program, List<string> paths, string name, string domain)
        {
            var today = DateTime.UtcNow;
            program.ImageGallery = new ImageGallery();

            program.ImageGallery.MetaData = new MetaData()
            {
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };
            program.ImageGallery.Name = name;
            program.ImageGallery.Status = EventStatusCategories.open;
            program.ImageGallery.Type = ImageGalleryType.Program;
            program.ImageGallery.ClubDomain = domain;
            program.ImageGallery.Images = new List<ImageGalleryItem>();

            foreach (var path in paths)
            {
                program.ImageGallery.Images.Add(new ImageGalleryItem()
                {
                    Title = name,
                    Path = path,
                    Status = EventStatusCategories.open,
                    Order = 0,
                    MetaData = new MetaData { DateCreated = today, DateUpdated = today }
                });
            }
        }

        public DateTime GetEndDateFromSessionWeeks(List<DayOfWeek> weekDays, DateTime startDate, int sessionWeeks)
        {

            DayOfWeek endDayOfWeek = startDate.DayOfWeek;

            DateTime today = startDate;

            for (int i = 0; i < 7; i++)
            {
                foreach (var day in weekDays)
                {
                    if (today.DayOfWeek == day)
                    {
                        endDayOfWeek = day;
                    }
                }

                today = today.AddDays(1);
            }

            DateTime endDate = startDate;

            for (int i = 0; i < sessionWeeks; i++)
            {

                if (i == sessionWeeks - 1)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        if (endDate.DayOfWeek == endDayOfWeek)
                        {
                            break;
                        }
                        endDate = endDate.AddDays(1);
                    }
                }
                else
                {
                    endDate = endDate.AddDays(7);
                }
            }


            return endDate;
        }

        public string GetInstractors(Program program, bool includePhone = false)
        {
            var instructors = String.Empty;
            if (program.Instructors != null && program.Instructors.Any())
            {
                var allInstructors = includePhone ? program.Instructors.Select(i => !string.IsNullOrEmpty(i.Contact.Phone) ? $"{i.Contact.FullName} - {PhoneNumberHelper.FormatPhoneNumber(i.Contact.Phone)}" : i.Contact.FullName).ToList() : program.Instructors.Select(i => i.Contact.FullName).ToList();
                if (allInstructors != null && allInstructors.Any())
                {
                    foreach (var Item in allInstructors)
                    {
                        if (!String.IsNullOrEmpty(instructors))
                        {
                            instructors += ", ";
                        }
                        instructors += Item.ToString();
                    }
                }

            }
            return instructors;
        }

        public List<string> GetInstractorsInHomeSite(Program program)
        {
            var instructors = new List<string>();

            if (program.Instructors != null && program.Instructors.Any())
            {
                var allInstructors = program.Instructors.Select(i => i.Contact.FullName).ToList();
                if (allInstructors != null && allInstructors.Any())
                {
                    foreach (var Item in allInstructors)
                    {
                        instructors.Add(Item?.Replace("'", "&#39;").ToString());
                    }
                }

            }
            return instructors;
        }

        public string FormatDays(List<DayOfWeek> allDaysProgram)
        {
            var _day = String.Empty;
            if (allDaysProgram != null && allDaysProgram.Any())
            {
                foreach (var dayItem in allDaysProgram)
                {
                    if (!String.IsNullOrEmpty(_day))
                    {
                        _day += ", ";
                    }
                    _day += dayItem.ToString().Substring(0, 3);
                }
            }
            return _day;
        }

        public List<DateTime> GetDatesBetweenTwoIntervals(int schoolId, DateTime programStartDate, DateTime programEndDate)
        {
            var classdates = new List<DateTime>();
            var countDate = (programEndDate.Date - programStartDate.Date).TotalDays;
            var school = _clubBusiness.Get(schoolId);
            List<string> holidayDates = school.Setting.ListOfHolidayDates;

            var nextDate = programStartDate;
            for (int i = 0; i <= countDate; i++)
            {
                classdates.Add(nextDate);
                nextDate = nextDate.AddDays(1);
            }

            if (classdates != null && classdates.Any())
            {
                if (holidayDates != null && holidayDates.Any())
                {
                    classdates.RemoveAll(c => holidayDates.Where(d => d != String.Empty && DateTime.TryParse(d, out _)).Select(d => DateTime.Parse(d)).ToList().Contains(c.Date));
                }
            }
            return classdates;
        }
        public string GetClassDaysSeperatedByCamma(Program program)
        {
            var programDays = GetClassDays(program).Select(pr => pr.DayOfWeek).ToList();

            if (!programDays.Any()) return String.Empty;
            var daysAbbreviation = GetDayOfWeekAbbreviation(programDays);

            return String.Join(", ", daysAbbreviation);
        }
        public List<string> GetDayOfWeekAbbreviation(List<DayOfWeek> days)
        {
            return DateTimeHelper.GetDayOfWeekAbbreviation(days);
        }
        public int GetProgramWeeksNumber(Program program)
        {
            var sessions = GetSessions(program);
            var previousDay = new DateTime(1900, 1, 1);
            var weekCount = 0;

            foreach (var session in sessions)
            {
                if (session.Start.Date < previousDay.Date.AddDays(7)) continue;
                weekCount++;
                previousDay = session.Start.Date;
            }

            return weekCount;
        }
        public ProgramAttributeValue GetAttribute(Program program, AttributeName attributeName)
        {
            var attribute = GetAttributes(program).SingleOrDefault(s => s.Attribute.Name == attributeName);

            return attribute;
        }

        public List<ProgramAttributeValue> GetAttributes(long programId)
        {
            var program = Get(programId);

            return GetAttributes(program);
        }

        public List<ProgramAttributeValue> GetAttributes(Program program)
        {

            var programAttributeValues = program.ProgramAttributeValues.ToList();

            var programAttributes = _attributeBusiness.GetList(AttributeReferenceType.Program, program.ClubId);

            var programAttributeValueIds = programAttributeValues.Select(a => a.AttributeId).ToList();

            var attributesWherNotInProgramButExistsInClub = new List<JbAttribute>();

            try
            {
                attributesWherNotInProgramButExistsInClub = programAttributes.ToList();//

                if (programAttributeValueIds.Any())
                {
                    attributesWherNotInProgramButExistsInClub = attributesWherNotInProgramButExistsInClub.Where(p => !programAttributeValueIds.Contains(p.Id)).ToList();
                }
            }
            catch
            {
                return new List<ProgramAttributeValue>();
            }

            var newProgramAttributeValues = attributesWherNotInProgramButExistsInClub != null ? attributesWherNotInProgramButExistsInClub.Select(a =>
                                          new ProgramAttributeValue
                                          {
                                              Attribute = a,
                                              AttributeId = a.Id,
                                              Program = program,
                                              ProgramId = program.Id,
                                              Value = null
                                          })
                                          .ToList() : null;

            programAttributeValues.AddRange(newProgramAttributeValues);

            return programAttributeValues;
        }

        public void SetAttributes(Program program, Dictionary<AttributeName, string> KeyValues)
        {
            var programAtrrubutes = program.ProgramAttributeValues;

            if (programAtrrubutes == null)
            {
                programAtrrubutes = new List<ProgramAttributeValue>();
            }

            var clubAttributes = _attributeBusiness.GetList(AttributeReferenceType.Program, program.ClubId).ToList();//

            foreach (var item in KeyValues)
            {
                var programAttribute = programAtrrubutes.SingleOrDefault(p => p.Attribute.Name == item.Key);

                if (programAttribute != null)
                {
                    programAttribute.Value = item.Value;
                }
                else
                {
                    var clubAttribute = (clubAttributes != null && clubAttributes.Any()) ? clubAttributes.SingleOrDefault(s => s.Name == item.Key) : new JbAttribute();

                    if (clubAttribute != null && clubAttribute.Id != 0)
                    {
                        programAtrrubutes.Add(new ProgramAttributeValue
                        {
                            Attribute = clubAttribute,
                            AttributeId = clubAttribute.Id,
                            Program = program,
                            ProgramId = program.Id,
                            Value = item.Value
                        });
                    }
                }
            }
        }

        public List<Program> GetProgramsToday(int clubId, long seasonId, DateTime? dateTime)
        {
            var result = new List<Program>();

            var clubDateTime = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow);

            dateTime = dateTime.HasValue ? dateTime.Value : clubDateTime;

            var programs = _programRepository.GetList().Where(p => p.SeasonId == seasonId && p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                  && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Season.Status == SeasonStatus.Live && (p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Subscription));

            foreach (var program in programs)
            {
                var sessions = GetSessions(program);

                foreach (var session in sessions)
                {
                    if (dateTime.HasValue ? (dateTime.Value.Date == session.Start.Date) : clubDateTime.Date == session.Start.Date)
                    {
                        result.Add(program);
                        break;
                    }
                }
            }

            return result;
        }

        public IEnumerable<OrderItem> GetProgramOrderitems(long programId)
        {
            var program = Get(programId);
            var programOrderItems = program.ProgramSchedules.SelectMany(s => s.OrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.completed && o.Order.OrderStatus == OrderStatusCategories.completed));

            return programOrderItems.ToList();
        }

        public Charge GetProgramApplicationFee(Program program)
        {
            var apllicationFee = new Charge();

            var scheduleCharges = program.ProgramSchedules.LastOrDefault(s => !s.IsDeleted).Charges;

            if (scheduleCharges != null && scheduleCharges.Any())
            {
                apllicationFee = scheduleCharges.SingleOrDefault(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted);
            }

            return apllicationFee;
        }

        public bool GetFirstInstallmentInRegistrationTime(Program program)
        {
            var result = false;

            var progeamAttribute = _subscriptionBusiness.GetBeforAfterCareAtribute(program.ProgramSchedules.Where(s => !s.IsDeleted).First());
            result = progeamAttribute.GetFirstPaymentAtTheRegisterTime;

            return result;
        }

        public bool IsCapacityFull(List<Charge> charges)
        {
            var result = false;

            var programScheduleId = charges.FirstOrDefault().ProgramScheduleId;

            var totalChargesCapacity = charges.Where(c => !c.IsDeleted).Sum(s => s.Capacity);

            var scheduleHaveUnlimitedCapacity = charges.Any(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted && c.Capacity == 0);

            var allOrdersToSchedule = _orderItemBusiness.GetList().Where(o => o.ItemStatus == OrderItemStatusCategories.completed && o.ProgramScheduleId == programScheduleId && (o.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == o.Order.IsLive).Count();

            result = allOrdersToSchedule >= totalChargesCapacity && scheduleHaveUnlimitedCapacity == false ? true : false;

            return result;

        }

        public decimal GetDropInTotalFee(Program program, List<long> sessionIds, ProgramSchedule schedule = null)
        {
            decimal result = 0;

            switch (program.TypeCategory)
            {
                case ProgramTypeCategory.Subscription:
                    {
                        var subscriptionAttribute = JsonConvert.DeserializeObject<ScheduleSubscriptionAttribute>(program.ProgramSchedules.Last(s => !s.IsDeleted).AttributesSerialized);
                        if (!subscriptionAttribute.EnableDropIn)
                        {
                            return result;
                        }

                        var dropinPrice = subscriptionAttribute.DropInSessionPrice;

                        result = dropinPrice * sessionIds.Count(s => s != 0);
                    }
                    break;
                case ProgramTypeCategory.BeforeAfterCare:
                    {
                        var scheduleAttribute = JsonConvert.DeserializeObject<ScheduleAfterBeforeCareAttribute>(schedule.AttributesSerialized);

                        if (!scheduleAttribute.EnableDropIn)
                        {
                            return result;
                        }

                        var dropinPrice = scheduleAttribute.DropInSessionPrice;
                        var dropInSamePrice = scheduleAttribute.DropInSameSessionPrice;
                        var enabelSamePrice = scheduleAttribute.EnableSameDropInPrice;

                        var todayDate = _clubBusiness.GetClubDateTime(schedule.Program.Club, DateTime.UtcNow);
                        var programSessions = _subscriptionBusiness.GetDropInProgramSessions(schedule.Program, sessionIds);

                        foreach (var session in programSessions)
                        {
                            var amount = enabelSamePrice && session.StartDateTime.Date == todayDate.Date ? dropInSamePrice : dropinPrice;

                            result += amount;
                        }

                    }
                    break;
                case ProgramTypeCategory.Camp:
                    {
                        var programSessions = _programSessionBusiness.GetList(p => sessionIds.Contains(p.Id));

                        foreach (var session in programSessions)
                        {
                            var charge = program.ProgramSchedules.SelectMany(s => s.Charges).FirstOrDefault(c => c.Id == session.ChargeId);

                            var amount = charge.Attributes.DropInPrice;

                            result += amount.Value;
                        }
                    }
                    break;
                default:
                    break;
            }


            return result;
        }

        public ProgramSchedule GetScheduleByDate(Program program, DateTime date)
        {
            var result = new ProgramSchedule();

            if (program.TypeCategory == ProgramTypeCategory.ChessTournament || program.TypeCategory == ProgramTypeCategory.Calendar || program.TypeCategory == ProgramTypeCategory.SeminarTour)
            {
                result = program.ProgramSchedules.FirstOrDefault();
                return result;
            }
            else if (program.TypeCategory == ProgramTypeCategory.Subscription || program.TypeCategory == ProgramTypeCategory.Camp)
            {
                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                {
                    if (date >= schedule.StartDate && date <= schedule.EndDate)
                    {
                        return schedule;
                    }
                }
            }
            else if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both))
                {
                    if (date >= schedule.StartDate && date <= schedule.EndDate)
                    {
                        return schedule;
                    }
                }
            }

            result = program.ProgramSchedules.FirstOrDefault();

            return result;
        }

        public bool IsTeamChessTourney(Program program)
        {
            if (program.TypeCategory != ProgramTypeCategory.ChessTournament) return false;
            var tournamentAttribute = ((TournamentScheduleAttribute)(program.ProgramSchedules.FirstOrDefault()?.Attributes));

            return tournamentAttribute != null && tournamentAttribute.TournamentType == TournamentType.Team;
        }



        private static bool TuitonHasEarlyBird(Charge charge)
        {
            var result = false;
            var now = DateTimeHelper.GetCurrentLocalDateTime(charge.ProgramSchedule.Program.Club.TimeZone);
            var earlyBirds = charge.Attributes.EarlyBirds;

            if (earlyBirds == null) return false;
            foreach (var earlyBird in earlyBirds)
            {
                if (earlyBird.ExpireDate == null || now.Date > earlyBird.ExpireDate.Value.Date) continue;
                result = true;
                break;
            }

            return result;
        }

        private static decimal? GetEarlyBirdAmount(Charge charge)
        {
            decimal? result = null;
            var now = DateTimeHelper.GetCurrentLocalDateTime(charge.ProgramSchedule.Program.Club.TimeZone);
            var earlyBirds = charge.Attributes.EarlyBirds;

            if (earlyBirds == null) return result;
            foreach (var earlyBird in earlyBirds)
            {
                if (earlyBird.ExpireDate == null || now.Date > earlyBird.ExpireDate.Value.Date) continue;
                result = earlyBird.Price;
                break;
            }

            return result;
        }

        private static string GetEarlyBirdExpireDate(Charge charge)
        {
            var result = String.Empty;
            var now = DateTimeHelper.GetCurrentLocalDateTime(charge.ProgramSchedule.Program.Club.TimeZone);
            var earlyBirds = charge.Attributes.EarlyBirds;

            if (earlyBirds == null) return result;
            foreach (var earlyBird in earlyBirds)
            {
                if (earlyBird.ExpireDate == null || now.Date > earlyBird.ExpireDate.Value.Date) continue;
                result = String.Format("{0} at {1}", earlyBird.ExpireDate.Value.ToString(Constants.DateTime_Comma), earlyBird.ExpireDate.Value.ToString(Constants.TimeWithAMPM));
                break;
            }

            return result;
        }

        private ChargeItemViewModel MapChargeItemViewModel(Charge charge, bool isTestMode, bool showCapacityLeft, Club club, Season season)
        {
            var capacityLeft = CapacityLeft(charge, isTestMode);

            var model = new ChargeItemViewModel
            {
                Name = charge.Name?.Replace("'", "&#39;"),
                Amount = CalculateProgramChargeAmount(club, charge).Value,
                HasEarlyBird = TuitonHasEarlyBird(charge),
                EarlyBirdAmount = CalculateProgramChargeAmount(club, charge, charge.Attributes.EarlyBirds, true),
                EarlyBirdExpiryDate = GetEarlyBirdExpireDate(charge),
                Capacity = GetCapacity(charge),
                CapacityLeft = showCapacityLeft ? capacityLeft == 0 ? "Full" : capacityLeft == null ? "" : capacityLeft.ToString() : ""
            };

            return model;
        }

        public decimal? CalculateProgramChargeAmount(Club club, Charge charge, List<EarlyBird> earlyBirds = null, bool isEarlyBird = false)
        {
            decimal? result = charge.Amount;

            if (isEarlyBird)
            {
                if (earlyBirds != null && earlyBirds.Any())
                {
                    result = GetEarlyBirdAmount(charge);
                }
                else { return null; }
            }

            result = CalculateWithSurcharge(club, charge, result);


            return result;
        }

        public decimal? CalculateWithSurcharge(Club club, Charge charge, decimal? programChargeAmount)
        {
            decimal? result = programChargeAmount;
            var showCombinedPriceOnlyForClub = false;

            if (club?.Setting != null)
            {
                showCombinedPriceOnlyForClub = club.Setting.ShowCombinedPriceOnly;
            }

            if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
            {
                if (club.IsSchool)
                {
                    var clubSurcharge = club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);

                    if (!showCombinedPriceOnlyForClub) return result;
                    if (charge.ProgramSchedule.Program.TypeCategory != ProgramTypeCategory.Class) return result;

                    if (clubSurcharge != null)
                    {
                        var clubCharge = clubSurcharge.AmountType == ChargeDiscountType.Fixed
                              ? clubSurcharge.Amount
                              : programChargeAmount != null ? programChargeAmount * clubSurcharge.Amount / 100 : programChargeAmount * clubSurcharge.Amount / 100;

                        result += clubCharge != null ? CurrencyHelper.RoundCurrencyCeilingWithoutPenny(clubCharge.Value) : 0;
                    }

                }
            }

            if (club.PartnerClub?.Charges == null || !club.PartnerClub.Charges.Any(c =>
                    c.IsDeleted == false && c.Category == ChargeDiscountCategory.PartnerSurcharge)) return result;
            {
                var partnerSurcharge = club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);

                if (club.Domain.Equals("em-waplesmill", StringComparison.OrdinalIgnoreCase))
                {
                    if (partnerSurcharge != null) partnerSurcharge.Amount = 6;
                }

                if (!showCombinedPriceOnlyForClub) return result;
                if (charge.ProgramSchedule.Program.TypeCategory != ProgramTypeCategory.Class) return result;

                if (partnerSurcharge != null)
                {
                    var partnerCharge = partnerSurcharge.AmountType == ChargeDiscountType.Fixed ? partnerSurcharge.Amount
                                            : programChargeAmount != null ? programChargeAmount * partnerSurcharge.Amount / 100 : programChargeAmount * partnerSurcharge.Amount / 100;

                    result += partnerCharge != null ? CurrencyHelper.RoundCurrencyCeilingWithoutPenny(partnerCharge.Value) : 0;
                }

            }
            return result;
        }
        public object GetProgramSchedules(long programId)
        {
            var program = Get(programId);
            var hasProgramSchedules = program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || program.TypeCategory == ProgramTypeCategory.Camp;


            var result = program.ProgramSchedules.Select(ps => new SelectListItem() { Text = ps.Title, Value = ps.Id.ToString() })
                     .OrderBy(s => s.Text).ToList();
            result.AddItemToFirst(DefaultDropdownItem.All);

            var model = new { HasProgramSchedules = hasProgramSchedules, ProgramSchedules = result };

            return model;
        }

        public List<SelectListItem> GetKeyValueSeasonPrograms(long seasonId)
        {
            var programs = GetSeasonPrograms(seasonId);

            var result = programs.Select(p => new SelectListItem() { Text = p.Name, Value = p.Id.ToString() })
                   .OrderBy(s => s.Text).ToList();

            result.AddItemToFirst(DefaultDropdownItem.All);

            return result;
        }

        private List<ProgramScheduleDaysViewModel> GetProgramScheduleDays(Program program, ProgramSchedule schedule)
        {
            var days = new List<ProgramScheduleDaysViewModel>();
            var atributeDays = new List<ProgramScheduleDay>();

            if (schedule.ScheduleMode == TimeOfClassFormation.Both)

            {
                var programScheduls = program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both);

                atributeDays.AddRange(((ScheduleAfterBeforeCareAttribute)programScheduls.First(s => s.ScheduleMode == TimeOfClassFormation.AM).Attributes).Days);
                atributeDays.AddRange(((ScheduleAfterBeforeCareAttribute)programScheduls.First(s => s.ScheduleMode == TimeOfClassFormation.PM).Attributes).Days);

                var groupDays = atributeDays.GroupBy(d => d.DayOfWeek);

                foreach (var item in groupDays)
                {
                    var itemDays = item.ToList();

                    var referfe = new ProgramScheduleDaysViewModel
                    {
                        FirstStartTime = itemDays.First().StartTime.Value,
                        FirstEndTime = itemDays.First().EndTime.Value,
                        SecondStartTime = itemDays.Last().StartTime.Value,
                        SecondEndTime = itemDays.Last().EndTime.Value,
                        DayOfWeek = itemDays.First().DayOfWeek,
                    };

                    days.Add(referfe);
                }
            }
            else
            {
                var Scheduledays = schedule.Program.TypeCategory != ProgramTypeCategory.BeforeAfterCare ? ((ScheduleAttribute)schedule.Attributes).Days : ((ScheduleAfterBeforeCareAttribute)schedule.Attributes).Days;
                days = Scheduledays.Select(d => new ProgramScheduleDaysViewModel
                {
                    FirstEndTime = d.EndTime.Value,
                    FirstStartTime = d.StartTime.Value,
                    DayOfWeek = d.DayOfWeek,

                }).ToList();
            }

            return days;
        }

        public List<SelectListItem> GetSeasonProgramItems(long seasonId)
        {
            var result = new List<SelectListItem>();

            result.AddRange(GetListBySeasonId(seasonId).Where(p => p.Status == ProgramStatus.Open).ToList().OrderBy(o => o.Name).Select(m =>
                    new SelectListItem
                    {
                        Text = m.Name,
                        Value = m.Id.ToString()
                    })
                     .ToList());

            return result;
        }

        public List<SelectKeyValue<long>> GetClubProgramItems(int clubId)
        {
            var result = new List<SelectKeyValue<long>>();

            result.AddRange(GetClubPrograms(clubId).ToList().OrderBy(o => o.Name).Select(m =>
                    new SelectKeyValue<long>
                    {
                        Group = m.Season.Title,
                        Text = m.Name,
                        Value = m.Id
                    })
                     .ToList());

            return result;
        }

        public IEnumerable<ProgramSchedulePart> GetProgramDeletedScheduleParts(long programId, DateTime dateTime, bool editEndDate)
        {
            var programScheduleParts = GetScheduleParts(programId);

            var deletedScheduleParts = editEndDate ? programScheduleParts.Where(s => s.StartDate.Date > dateTime.Date) : programScheduleParts.Where(s => s.EndDate.Date < dateTime.Date);

            return deletedScheduleParts;
        }

        public IEnumerable<ProgramCalendarViewModel> GetProgramCalendarItems(List<long> seasonIds, int clubId, string baseUrl, DateTime startDate, DateTime endDate)
        {
            var query = GetListWithOutSources(clubId).Where(p => p.Season.Status == SeasonStatus.Live && p.Status != ProgramStatus.Deleted);

            if (seasonIds != null)
            {
                query = query.Where(p => seasonIds.Contains(p.SeasonId) || seasonIds.Contains(p.OutSourceSeasonId.Value));
            }

            var programs = query.ToList();

            var result = new List<ProgramCalendarViewModel>();

            foreach (var program in programs)
            {

                if (program.TypeCategory == ProgramTypeCategory.ChessTournament || program.TypeCategory == ProgramTypeCategory.SeminarTour)
                {
                    var enrollments = _orderItemBusiness.GetProgramOrderItems(program.Id);

                    result.Add(MapNoneProgramSessionCalendarViewModel(program.ProgramSchedules.First(), enrollments.Count(), baseUrl, clubId));
                }
                else
                {
                    var sessions = GetSessions(program).ToList();

                    if (!sessions.Any())
                    {
                        continue;
                    }

                    if (!sessions.Any(s => s.Start.Date >= startDate && s.Start.Date <= endDate))
                    {
                        continue;
                    }

                    if (program.TypeCategory == ProgramTypeCategory.Class || program.TypeCategory == ProgramTypeCategory.Camp)
                    {
                        var enrollments = _orderItemBusiness.GetProgramOrderItems(program.Id).AsQueryable();

                        var orderChargeDiscounts = new List<OrderChargeDiscount>();

                        if (program.TypeCategory == ProgramTypeCategory.Camp)
                        {
                            var enrollmentIds = enrollments.Select(o => o.Id).ToList();
                            orderChargeDiscounts = _orderChargeDiscountBusiness.GetList().Where(o => enrollmentIds.Contains(o.OrderItemId.Value) && !o.IsDeleted).ToList();
                        }

                        result.AddRange(sessions.Select(session => MapProgramSessionCalendarViewModel(session, null, program, baseUrl, clubId, enrollments, orderChargeDiscounts)));
                    }
                    else
                    {
                        var sessionIds = sessions.Select(s => s.Id).ToList();

                        var programSessions = _programSessionBusiness.GetList(p => sessionIds.Contains(p.Id)).ToList();

                        result.AddRange(sessions.Select(session =>
                            MapProgramSessionCalendarViewModel(session, programSessions, program, baseUrl, clubId)));
                    }
                }
            }

            return result;
        }

        private ProgramCalendarViewModel MapProgramSessionCalendarViewModel(ScheduleSession session, List<ProgramSession> programSessions, Program program, string baseUrl, int currentClubId, IQueryable<OrderItem> enrollments = null, List<OrderChargeDiscount> orderChargeDiscounts = null)
        {
            var tuitionName = string.Empty;
            var countEnrollments = enrollments?.Count() ?? 0;
            var schedule = program.ProgramSchedules.Single(s => s.Id == session.ScheduleId);

            if (countEnrollments == 0 && programSessions != null)
            {
                countEnrollments = _programSessionBusiness.GetNumberOfTakenPlaces(programSessions.Single(s => s.Id == session.Id));
            }

            var capacity = string.Empty;

            var isFull = false;

            
            if (schedule.Program.TypeCategory == ProgramTypeCategory.Class || schedule.Program.TypeCategory == ProgramTypeCategory.SeminarTour || schedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament)
            {
                var charges = schedule.Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).ToList();

                var allCapacity = 0;
                var isUnlimited = false;

                if (charges.All(c => c.Capacity != 0))
                {
                    foreach (var item in charges)
                    {
                        allCapacity += item.Capacity;

                    }

                    isFull = allCapacity != 0 && countEnrollments >= allCapacity;
                }
                else if (charges.All(c => c.Capacity == 0))
                {
                    isUnlimited = false;
                }
                else
                {
                    isUnlimited = true;
                }


                if (schedule.Program.TypeCategory == ProgramTypeCategory.Class && !isFull && !isUnlimited && schedule.Attributes.Capacity > 0)
                {
                    isFull = countEnrollments >= schedule.Attributes.Capacity;

                    allCapacity = schedule.Attributes.Capacity;
                }

                capacity = allCapacity == 0 ? Constants.UnlimitedCapacity : allCapacity.ToString();

            }
            else if (schedule.Program.TypeCategory == ProgramTypeCategory.Subscription || schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                var capacityBeforeAfter = schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare ? _subscriptionBusiness.GetBeforAfterCareAtribute(schedule).Capacity : _subscriptionBusiness.GetSubscriptionAtribute(schedule).Capacity;
                var programSession = _programSessionBusiness.Get(session.Id);

                capacity = capacityBeforeAfter == 0 ? Constants.UnlimitedCapacity : capacityBeforeAfter.ToString() ;

                isFull = _programSessionBusiness.IsFull(programSession, capacityBeforeAfter);
            }
            else if (schedule.Program.TypeCategory == ProgramTypeCategory.Camp)
            {
                var programSession = _programSessionBusiness.Get(session.Id);
                var charge = programSession.Charge;

                countEnrollments = orderChargeDiscounts?.Count(c => c.Category == ChargeDiscountCategory.EntryFee && c.ChargeId == charge.Id) ?? 0; 

                tuitionName = programSession.Charge.Name;

                capacity = charge.Capacity == 0 ? Constants.UnlimitedCapacity : charge.Capacity.ToString();

                isFull = IsFull(charge, schedule.Program.Season.Status == SeasonStatus.Test);
            }

            var title = !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Title} - {schedule.Program.Name}" : schedule.Program.Name;

            if (!string.IsNullOrEmpty(tuitionName))
            {
                title = $"{title} - {tuitionName}";
            }

            return new ProgramCalendarViewModel
            {
                Title = title,
                Start = session.Start,
                End = session.End,
                StrStartTime = session.Start.ToShortTimeString().ToLower().Replace(" ", string.Empty),
                StrEndTime = session.End.ToShortTimeString().ToLower().Replace(" ", string.Empty),
                ProgramSessionId = session.Id,
                IsAllDay = false,
                IsFull = isFull,
                Date = session.Start.Date.ToShortDateString(),
                Enrollment = countEnrollments,
                Capacity = capacity,
                ProgramId = schedule.Program.Id,
                SeasonDomain = schedule.Program.OutSourceSeasonId.HasValue && schedule.Program.OutSourceSeason.ClubId == currentClubId ? schedule.Program.OutSourceSeason.Domain : schedule.Program.SeasonDomain,
                ViewPageUrl = GetProgramUrl(schedule.Program.Club.Domain, schedule.Program.SeasonDomain, schedule.Program.Domain, baseUrl, schedule.Program.OutSourceSeasonId.HasValue && schedule.Program.OutSourceSeason.ClubId == currentClubId),
                HideOutSourceProgramInProvider = schedule.Program.OutSourceSeasonId.HasValue && schedule.Program.OutSourceSeason.ClubId == currentClubId
            };
        }

        private ProgramCalendarViewModel MapNoneProgramSessionCalendarViewModel(ProgramSchedule schedule, int countEnrollments, string baseUrl, int currentClubId)
        {

            return new ProgramCalendarViewModel
            {
                Title = schedule.Program.Name,
                Start = schedule.StartDate,
                End = schedule.EndDate,
                IsAllDay = true,
                Date = schedule.StartDate.Date.ToShortDateString(),
                Enrollment = countEnrollments,
                Capacity = GetCapacityProgram(schedule),
                ProgramId = schedule.Program.Id,
                SeasonDomain = schedule.Program.OutSourceSeasonId.HasValue && schedule.Program.OutSourceSeason.ClubId == currentClubId ? schedule.Program.OutSourceSeason.Domain : schedule.Program.SeasonDomain,
                ViewPageUrl = GetProgramUrl(schedule.Program.Club.Domain, schedule.Program.SeasonDomain, schedule.Program.Domain, baseUrl, schedule.Program.OutSourceSeasonId.HasValue && schedule.Program.OutSourceSeason.ClubId == currentClubId),
                HideOutSourceProgramInProvider = schedule.Program.OutSourceSeasonId.HasValue && schedule.Program.OutSourceSeason.ClubId == currentClubId
            };
        }

        private static string GetCapacityProgram(ProgramSchedule schedule)
        {
            string result;

            if (schedule.Attributes.Capacity > 0)
            {
                result = schedule.Attributes.Capacity.ToString();
            }
            else
            {
                var charge = schedule.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee && c.ProgramScheduleId == schedule.Id);

                result = charge != null && charge.Capacity > 0 ? charge.Capacity.ToString() : Constants.UnlimitedCapacity;
            }

            return result;
        }

        private static string GetProgramUrl(string clubDomain, string seasonDomain, string programDomain, string baseUrl, bool viewOnly = false)
        {
            var url = $"{baseUrl.Replace(baseUrl.Split('.').ElementAt(0).Substring(baseUrl.IndexOf("//", StringComparison.Ordinal) + 2), clubDomain)}/{seasonDomain}/{programDomain}";

            return viewOnly ? $"{url}/{ProgramConstants.View}" : url;
        }

        public string GetProgramPinUniqueKey(Program program, string userName)
        {
            return $"{program.Club.Domain}{program.Season.Domain}{program.Domain}{userName}{ProgramConstants.Pin}".ToLower();
        }

        private IEnumerable<ScheduleSession> GetSubscriptionSessions(ProgramSchedule programSchedule)
        {
            var programSessions = _programSessionBusiness.GetList(programSchedule);

            _programSessionBusiness.ApplyHolidays(programSchedule.Program, programSessions);

            var result = programSessions.Select(p => new ScheduleSession()
            {
                Id = p.Id,
                Start = p.StartDateTime,
                End = p.EndDateTime,
                ScheduleId = p.ProgramScheduleId.Value
            });

            return result.OrderBy(p => p.Start);
        }

        private IEnumerable<ScheduleSession> GetBeforeAfterSessions(List<ProgramSchedule> programSchedules)
        {
            var programSessions = _programSessionBusiness.GetList(programSchedules);

            _programSessionBusiness.ApplyHolidays(programSchedules.First().Program, programSessions);

            var result = programSessions.Select(p => new ScheduleSession()
            {
                Id = p.Id,
                Start = p.StartDateTime,
                End = p.EndDateTime,
                ScheduleId = p.ProgramScheduleId.Value
            });

            return result.OrderBy(p => p.Start);
        }

        private IEnumerable<ScheduleSession> GetCampSessions(Program program)
        {
            var programScheduleIds = program.ProgramSchedules.Where(p => !p.IsDeleted).Select(p => p.Id).ToList();

            var programSessions = _programSessionBusiness.GetList().Where(p => !p.IsDeleted && !p.Charge.IsDeleted && programScheduleIds.Contains(p.Charge.ProgramScheduleId.Value)).ToList();

            _programSessionBusiness.ApplyHolidays(program, programSessions);

            var result = programSessions.Select(p => new ScheduleSession()
            {
                Id = p.Id,
                Start = p.StartDateTime,
                End = p.EndDateTime,
                ScheduleId = p.Charge.ProgramScheduleId.Value
            });

            return result.OrderBy(p => p.Start);
        }

        private IEnumerable<ScheduleSession> GetClassSessions(Program program, List<ProgramSchedule> programSchedules)
        {
            var programScheduleMode = GetClassScheduleTime(program);

            //Get sessions holiday
            if (program.TypeCategory != ProgramTypeCategory.SeminarTour && program.TypeCategory != ProgramTypeCategory.ChessTournament && programSchedules.First().Sessions.Any() && programSchedules.First().Sessions != null)
            {
                return GetClassDates(programSchedules.First(), programScheduleMode)
                    .Select(p => new ScheduleSession()
                    {
                        Id = p.Id,
                        Start = p.StartTime,
                        End = p.EndTime,
                        ScheduleId = program.ProgramSchedules.First().Id
                    })
                    .OrderBy(p => p.Start);
            }

            return new List<ScheduleSession>();
        }

        public IEnumerable<ProgramSchedulePart> GetUpcomingSchedules(List<ProgramSchedulePart> programScheduleParts, DateTime today)
        {
            var upcomingScheduleParts = new List<ProgramSchedulePart>();

            foreach (var part in programScheduleParts.Where(p => !p.IsDeleted))
            {
                if (part.EndDate.Date < today.Date) continue;
                else
                    upcomingScheduleParts.Add(part);
            }

            return upcomingScheduleParts;
        }
    }
}
