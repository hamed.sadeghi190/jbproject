﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CapacityReportViewModel
    {
        public string ProgramName { get; set; }

        public string Open { get; set; }

        public string Status { get; set; }

        public string Capacity { get; set; }

        public string MinimumEnrollment { get; set; }

        public string DayOfWeek { get; set; }

        public int Filled { get; set; }

        public string Schedule { get; set; }

        public string EnableWaitList { get; set; }

        public int WaitList { get; set; }

    }
}