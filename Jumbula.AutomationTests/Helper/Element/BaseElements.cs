﻿using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    public class BaseElements
    {
        public BaseElements(IWebDriver driver)
        {
            _driver = driver;
        }

        IWebDriver _driver;
        protected IWebDriver Driver
        {
            get
            {
                return _driver;
            }
        }

        public IWebElement Grid_GetCellByTitle(string id, int row, string column, string innerTag = "")
        {
            var thElements = Driver.FindElements(By.XPath(string.Format("//*[@id='{0}']//th", id))).ToList();

            for (int i = 0; i < thElements.Count; i++)
            {
                if (thElements[i].Text == column)
                {
                    if (string.IsNullOrEmpty(innerTag))
                    {
                        return Driver.FindElement(By.XPath(string.Format("//*[@id='{0}']//tr[{1}]//td[{2}]", id, row, i + 1)));
                    }
                    else
                    {
                        return Driver.FindElement(By.XPath(string.Format("//*[@id='{0}']//tr[{1}]//td[{2}]//{3}", id, row, i + 1, innerTag)));
                    }
                }

            }
            return null;
        }

        public IWebElement Grid_Actions(string id, int row)
        {
            var thElements = Driver.FindElements(By.XPath(string.Format("//*[@id='{0}']//th", id))).ToList();
            var index = 0;
            for (int i = 0; i < thElements.Count; i++)
            {
                if (thElements[i].Text == "Actions")
                {
                    index = i;
                    break;
                }
            }
            return Driver.FindElement(By.XPath(string.Format("//*[@id='{0}']//tr[{1}]//td[{2}]//span[contains(text(),'...')]", id, row, index + 1)));
        }
       
        public IWebElement Grid_Action_Child(string id, int row, string action)
        {
            var thElements = Driver.FindElements(By.XPath(string.Format("//*[@id='{0}']//th", id))).ToList();
            var index = 0;
            for (int i = 0; i < thElements.Count; i++)
            {
                if (thElements[i].Text == "Actions")
                {
                    index = i;
                    break;
                }
            }
            return Driver.FindElement(By.XPath(string.Format("//*[@id='{0}']//tr[{1}]//td[{2}]//span[contains(text(), '{3}')]", id, row, index + 1, action))); ;
        }
        public IWebElement Grid_Action_Parent(string id, int row, string action)
        {
            var thElements = Driver.FindElements(By.XPath(string.Format("//*[@id='{0}']//th", id)));
            var index = 0;
            for (int i = 0; i < thElements.Count; i++)
            {
                if (thElements[i].Text == "Actions")
                {
                    index = i;
                    break;
                }
            }
            return Driver.FindElement(By.XPath(string.Format("//*[@id='{0}']//tr[{1}]//td[{2}]//preceding-sibling::span[contains(text(), '{3}')]", id, row, index + 1, action)));
        }
        public IWebElement Grid_GetCellByIndex(string id, int row, int column, string innerTag = "")
        {
            if (string.IsNullOrEmpty(innerTag))
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='{0}']//tr[{1}]//td[{2}]", id, row, column)));
            }
            else
            {
                return Driver.FindElement(By.XPath(string.Format("//*[@id='{0}']//tr[{1}]//td[{2}]//{3}", id, row, column, innerTag)));
            }

        }
        public bool Grid_RowExist(string id, int row)
        {
            if (Driver.FindElements(By.XPath(string.Format("//*[@id='{0}']//tbody//tr[{1}]", id, row))).FirstOrDefault() != null)
            {
                return true;
            }
            return false;

        }

    }
}
