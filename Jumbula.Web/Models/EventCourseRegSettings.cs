﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsClub.Domain;
using TimeZone = SportsClub.Domain.TimeZone;

namespace SportsClub.Models
{
    public class EventCourseRegSettings
    {
        public EventCourseRegSettings()
        {
            
        }

        public int CategoryId { get; set; }
        
        [StringLength(128)]
        public string OutSourcerClubDomain { get; set; }
        
        public string NameLong { get; set; }
        
        public int? Capacity { get; set; }
        
        public int ReservedCapacity { get; set; }

        public FinishDateType FinishDateType { get; set; }

        public virtual IList<EventForms> EventForms { get; set; }
        
        public virtual ICollection<CourseRecurrenceTime> RecurrenceTime { get; set; }

        [Required]
        public DateTime? RecurrentStartDate { get; set; }

        public DateTime? RecurrentEndDate { get; set; } 
        
        public int? WeeksCount { get; set; }

        public SelectList OutSourcerSelectList { get; set; }

        public decimal? FullPrice { get; set; }

        public bool HasSessionPrice { get; set; }
        
        public decimal? SessionPrice { get; set; }

        [Required]
        [DefaultValue(false)]

        public bool HasProRatingPrice { get; set; }

        public string ExternalRegUrl { get; set; }

        public virtual ICollection<PaymentPlan> PaymentPlans { get; set; }

        public List<EventEarlybirdDiscount> EarlybirdDiscounts { get; set; }
    }

    public class EventEarlybirdDiscount
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be a positive number")]
        public decimal Price { get; set; }

        [Required]
        public DateTime EarlybirdDate { get; set; }
    }
}