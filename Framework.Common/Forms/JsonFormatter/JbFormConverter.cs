﻿using Jb.Framework.Common.Forms.Binding;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Jb.Framework.Common.Forms.JsonFormatter
{
    public class JbFormConverter : JsonConverter
    {

        public override bool CanConvert(Type objectType)
        {
            return typeof(JbForm).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JbForm form = new JbForm();
            JObject jsonObject = JObject.Load(reader);
            var properties = jsonObject.Properties().ToList();
            foreach (var property in properties)
            {
                if (property.Name.Equals("Type"))
                    continue;
                var proInfo = form.GetType().GetProperty(property.Name);

                if (proInfo.PropertyType == form.Elements.GetType())
                {
                    var jbc = new JbElementConverter();
                    var props = property.Value.ToObject<List<object>>();
                    List<IJbBaseElement> elements = props.Select(prop => jbc.GetJbElement(prop as JObject)).ToList();

                    proInfo.SetValue(form, elements);
                }
                else if (proInfo.PropertyType == form.AccessRole.GetType())
                {
                    var accessRoles = property.Value.ToObject<Dictionary<string, AccessMode>>();
                    proInfo.SetValue(form, accessRoles);
                }
                else
                {
                    //if (typeof(System.Collections.IEnumerable).IsAssignableFrom(proInfo.PropertyType))
                    //    continue;
                    if (proInfo.PropertyType != typeof(BindingConfig))
                        proInfo.SetValue(form, Convert.ChangeType(property.Value, proInfo.PropertyType));
                }

            }
            return form;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var form = value as JbForm;
            writer.WriteStartObject();
            var properties = form.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));

            foreach (var property in properties)
            {
                if (Attribute.IsDefined(property, typeof(JsonIgnoreAttribute)))
                    continue;
                writer.WritePropertyName(property.Name);
                serializer.Serialize(writer, property.GetValue(form));
            }

            writer.WriteEndObject();
        }
    }
}
