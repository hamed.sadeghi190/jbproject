﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CustomReportSelectProgramsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', '$timeout', 'jbToast', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, $timeout, jbToast) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;


        var baseUrl = window.location.origin + "/Dashboard/PortalReport";


        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.Year = 0;
        $scope.Model._SelectedClubs = [];
        $scope.ClubTypes = [{ Value: "school", Title: "Schools" }, { Value: "provider", Title: "Providers" }, { Value: "district", Title: "School districts" }];

        $scope.Model.IsAllPrograms = "true";

        $scope.Model.ClubType = "school";
        $scope.Model.IsAllClubs = "true";

        $scope.updateChosenRadio = function () {
            $scope.Model.IsAllClubs = "true";
        }


        $scope.getClubs = function (clubType) {
            if (clubType === "school")
                getAllSchools();
            else if (clubType === "provider")
                getAllProviders();
            else
                getAllDistricts();
        }

        function getAllSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data) {
                    $scope.AllClubs = data.AllSchools;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllProviders() {
            $http.get(baseUrl + "/GetAllProvider")
                .success(function (data) {
                    $scope.AllClubs = data;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch('AllClubs', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllDistricts() {
            $http.get(baseUrl + '/GetAllPartnerSchoolDistricts')
                .success(function (data) {
                    $scope.AllClubs = data.AllDisctricts;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        $http.get(baseUrl + '/GetFullSeasonClassListReportHeader').success(function (data) {

            $scope.Years = data.Years;
            $scope.Model.AllSeason = data.Seasons;
            
            $scope.Model.Year = new Date().getFullYear().toString();
            $scope.Model.SeasonName = data.Seasons[0].Value;
        }).error(function (data) {
            jbToast.error("error " + data);
        });


        $scope.runReport = function () {
            $scope.Model.ReportId = $scope.MC.stateParams.reportId;
            $scope.programs = $scope.Model._SelectedPrograms;
            $scope.Model.ReportName = $scope.MC.stateParams.reportName;
            $scope.Model.ReportType = $scope.MC.stateParams.reportType;
            $scope.IsAllPrograms = $scope.Model.IsAllPrograms;
            customReportService.setModelInStorage($scope.Model);

            $state.go('CustomReportPortalRun', { reportId: $scope.MC.stateParams.reportId, reportName: $scope.MC.stateParams.reportName, reportType: $scope.MC.stateParams.reportType });
        }

        $scope.CheckValidation = function () {

            $scope.runReport();
        }


    }]);
})();