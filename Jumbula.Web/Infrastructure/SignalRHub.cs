﻿using Microsoft.AspNet.SignalR;

namespace Jumbula.Web.Infrastructure
{
    public class SignalRHub : Hub
    {
        public void RefreshSignalFromParentToAdmins(string userName)
        {
            Clients.User(userName).refreshSignal();
        }
    }
}