﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('InstructorListReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};

        $scope.Model = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/GetInstructorListReportDetail', { params: { seasonDomain: $scope.seasonDomain } }).success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.SeasonName = data.SeasonName;
            $scope.Date = data.Date;
            dataInfo = data;
            manipulateInstructorListGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        var model = $scope.Model;


        var manipulateInstructorListGrid = function () {
            $scope.InstructorListDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetInstructorListReportInfo",
                        dataType: "json",
                        data: function () {
                            return {
                                seasonDomain: $scope.seasonDomain
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                resizable: true,
            });

            $scope.InstructorListGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (InstructorList).xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;
                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });                   
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.SeasonName
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Instructor list - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.InstructorListDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#InstructorListGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },               
                sortable: true,
                columns: [

                    {
                        field: "Name",
                        title: "Instructor name",
                        width: "190px"
                    }, {
                        field: "Status",
                        title: "Status",
                        width: "120px"
                    }, {
                        field: "DateOfBackgroundCheck",
                        title: "BGC date",
                        width: "160px"
                    }, {
                        field: "CellPhone",
                        title: "Cell phone",
                        width: "120px"
                    }, 
                ]
            }
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#InstructorListGrid").getKendoGrid().saveAsExcel();
        }
        $scope.saveToPDF = function () {
            cfpLoadingBar.start();
            $http.get(baseUrl + "/GetInstructorListReportInfo?seasonDomain=" + $scope.seasonDomain + "&printMode=" + true)
            .success(function (data, status, headers, config) {
                cfpLoadingBar.complete();
            });

        }
        $scope.print = function () {
            cfpLoadingBar.start();
            printGrid();
            cfpLoadingBar.complete();
        }




        function printGrid() {
            var gridElement = $('#InstructorListGrid'),
                printableContent = '',
                win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
                doc = win.document.open();

            var htmlStart =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title></title>' +
                '<div> <ul style="list-style:none"> ' + "Instructor list - " + dataInfo.ClubName + ' </li> </ul> </div>' +
                '<div> <ul style="list-style:none"> ' + dataInfo.SeasonName + ' </li> </ul> </div>' +
                '<div> <ul style="list-style:none"> ' + dataInfo.Date + ' </li> </ul> </div>' +
                '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
                '<style>' +
                'html { font: 11pt sans-serif; text-align: left }' +
                '.k-grid { border-top-width: 0; }' +
                '.k-grid, .k-grid-content { height: auto !important; padding: 20px; text-align: left}' +
                '.k-grid-content { overflow: visible !important; padding: 20px;}' +
                '.k-grid .k-grid-header  th { padding-bottom: 20px; }' +
                '.k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
                '</style>' +
                '</head>' +
                '<body>';

            var htmlEnd =
                    '</body>' +
                    '</html>';

            var gridHeader = gridElement.children('.k-grid-header');
            if (gridHeader[0]) {
                var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
                printableContent = gridElement
                    .clone()
                        .children('.k-grid-header').remove()
                    .end()
                        .children('.k-grid-content')
                            .find('table')
                                .first()
                                    .children('tbody').before(thead)
                                .end()
                            .end()
                        .end()
                    .end()[0].outerHTML;
            } else {
                printableContent = gridElement.clone()[0].outerHTML;
            }

            doc.write(htmlStart + printableContent + htmlEnd);
            doc.close();
            win.print();
        }
    }
    ]);

})();