﻿
using Jumbula.Core.Business;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class ImageGalleryBusiness :  IImageGalleryBusiness 
    {
        private readonly IRepository<ImageGallery> _imageGalleryRepository;
        public ImageGalleryBusiness(IRepository<ImageGallery> imageGalleryRepository)
        {
            _imageGalleryRepository = imageGalleryRepository;
        }

        public ImageGallery Get(int id)
        {
            return _imageGalleryRepository.GetList().SingleOrDefault(p => p.Id == id);
        }
    }
}
