﻿
angular.module('pageBuilder').directive('imageTooltip', function () {
    return {
        restrict: 'E',
        scope: {
            imgSrc: '@imgSrc',
            align: '@align',
            vAlign: '@vAlign'
        },
        templateUrl: "/Areas/Dashboard/Scripts/3-Directives/Tooltips/ImageTooltip/ImageTooltip.html",

        link: function (scope, element, attributes) {
            scope.appendImg = false;
       
            scope.toggleImage = function () {               
                element.find('.container-image-tooltip').stop().slideToggle(200);
            }
            scope.tooltipFire = function () {
                scope.appendImg = true;
                scope.toggleImage();
            }
            if (scope.align && (scope.align == 'left' || scope.align == 'center' || scope.align == 'right')) {
                element.addClass('image-tooltip-align-' + scope.align);
            }

            if (scope.vAlign && (scope.vAlign == 'top')) {
                element.addClass('image-tooltip-valign-' + scope.vAlign);
            }

            element.bind('click', function (event) {
                event.stopPropagation();
            });

            $(document).bind('click', function () {  
                element.find('.container-image-tooltip').stop().slideUp(200);
                scope.$apply();
            });
        }
    }
});


