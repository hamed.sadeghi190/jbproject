﻿using Jumbula.Core.Business;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class AttributeBusiness : IAttributeBusiness
    {
        private IClubBusiness _clubBusiness;
        private readonly IRepository<JbAttribute> _attributeRepository;

        public AttributeBusiness(IRepository<JbAttribute> attributeRepository)
        {
            _attributeRepository = attributeRepository;
        }

        public IClubBusiness ClubBusiness
        {
            get => _clubBusiness;
            set => _clubBusiness = value;
        }

        public  IQueryable<JbAttribute> GetList()
        {
            return _attributeRepository.GetList();
        }

        public IQueryable<JbAttribute> GetList(AttributeReferenceType referenceType, int clubId, int? partnerId = null)
        {
            var club = _clubBusiness.Get(clubId);

            return GetList(referenceType, club, partnerId);
        }


        public IQueryable<JbAttribute> GetList(AttributeReferenceType referenceType, Club club, int? partnerId = null)
        {
            int clubId = club != null ? club.Id : 0;
            if (club == null && partnerId.HasValue)
            {
                club = _clubBusiness.Get(partnerId.Value);
                clubId = club.Id;
            }

            var hasPartner = club.PartnerId.HasValue;
            var partner = hasPartner ? club.PartnerClub : null;
            var clubPartnerId = partner != null ? partner.Id : 0;

            var result = _attributeRepository.GetList(a => a.ReferenceType == referenceType && (a.ClubId == clubId || (hasPartner && a.ClubId == clubPartnerId)));

            return result;
        }
    }
}
