﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum OrderItemStatusReasons : byte
    {

        [Description("Completed")]
        regular = 0,
        [Description("Transferred In")]
        transferIn = 1,
        [Description("Transferred Out")]
        transferOut = 2,
        [Description("Canceled")]
        canceled = 4,
        [Description("Refunded")]
        refund = 5,
        [Description("All")]
        showAll = 6,
        [Description("Draft")]
        draft = 7,
    }
}
