﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Model.Order
{
    public class RefundResultModel
    {
        public bool Status { get; set; }
        public string TransactionId { get; set; }
        public decimal RefundedAmount { get; set; }
        public decimal RefundAmount { get; set; }
        public decimal RemainAmount { get; set; }
        public string ErrorMessage { get; set; }
        public string RefundNote { get; set; }
        public string Response { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public PaymentDetailStatus PaymentStatus { get; set; }
    }

    public class PaymentDetailModel
    {
        public decimal Amount { get; set; }

        public decimal AmountRefunded { get; set; }

        public bool Status { get; set; }
    }
}
