﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
    public class ProgramTestScenario : BaseTestScenario
    {
        [Test]
        public void ProgramCreate()
        {

            BaseUrl = DataDriver.SportClubURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginAdmin();

            // Go to create class page
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToCreateClass();

            // Create class
            var programTest = new ProgramTest(WebDriver);
            programTest.CreateClass();
        }
    }
}
