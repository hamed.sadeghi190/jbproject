﻿namespace Jumbula.Common.Enums
{
    public enum ContactCategory
    {
        Phone = 1,
        Note = 2,
        Meeting = 3,
        Email = 4
    }
}
