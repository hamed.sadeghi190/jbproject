﻿using System.Collections.Generic;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System.Linq;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using Jumbula.Core.Model.Order;

namespace Jumbula.Business
{
    public class ChargeBusiness : IChargeBusiness
    {
        private readonly IRepository<Charge, long> _charageRepository;
        private readonly IClubBusiness _clubBusiness;
        private readonly ISeasonBusiness _seasonBusiness;

        public ChargeBusiness(IRepository<Charge, long> charageRepository, IClubBusiness clubBusiness, ISeasonBusiness seasonBusiness)
        {
            _charageRepository = charageRepository;
            _clubBusiness = clubBusiness;
            _seasonBusiness = seasonBusiness;
        }
        public Charge Get(long chargeId)
        {
            return _charageRepository.Get(c => c.Id == chargeId && c.IsDeleted != true);
        }

        public IQueryable<Charge> GetList(long seasonId)
        {
            return _charageRepository.GetList().Where(c => c.SeasonId == seasonId && !c.IsDeleted);
        }

        public OperationStatus Delete(long chargeId)
        {
            var charge = _charageRepository.Get(d => d.Id == chargeId);
            charge.IsDeleted = true;
            return _charageRepository.Update(charge);
        }

        private List<string> GetSeasonChargeNames(List<long> seasonIds, List<long> scheduleIds)
        {
            var allCharges =
                _charageRepository.GetList()
                    .Where(c => (scheduleIds.Contains(c.ProgramScheduleId.Value) || seasonIds.Contains(c.SeasonId.Value)) && c.Category > 0 && c.Category != ChargeDiscountCategory.EntryFee)
                    .Select(c => new { c.IsDeleted, c.Name })
                    .AsEnumerable()
                    .DistinctBy(c => c.Name)
                    .Select(c => c.IsDeleted ? $"{c.Name} (deleted)" : c.Name)
                    .ToList();

            allCharges.AddRange(new List<string>
            {
                "Paid-in-full discount",
                "Custom",
                "Application fee",
                "Payment plan fee",
                "Transfer fee",
                "Cancellation fee",
                "Edit fee",
                "Prorate fee",
                "Custom charge",
                "Surcharge",
                "Convenience fee",
                "Partner surcharge",
                "Partner convenience fee",
            });

            return allCharges.Distinct().ToList();
        }

        public List<string> GetAllSeasonPartnerCharges(int partnerId)
        {
            var seasons = _clubBusiness.GetList().Where(c => c.PartnerId == partnerId).SelectMany(c => c.Seasons).Where(c => c.Status != SeasonStatus.Deleted);

            var seasonIds = seasons.Select(s => s.Id).ToList();

            var scheduleIds = seasons.SelectMany(s => s.Programs).Where(p => p.Status != ProgramStatus.Deleted).SelectMany(p => p.ProgramSchedules).Select(p => p.Id).ToList();

            var allCharges = GetSeasonChargeNames(seasonIds, scheduleIds);

            return allCharges;
        }

        public List<string> GetAllSeasonClubCharges(int clubId)
        {
            var seasons = _seasonBusiness.GetList(clubId).Where(c => c.Status != SeasonStatus.Deleted);

            var seasonIds = seasons.Select(s => s.Id).ToList();

            var scheduleIds = seasons.SelectMany(s => s.Programs).Where(p => p.Status != ProgramStatus.Deleted).SelectMany(p => p.ProgramSchedules).Select(p => p.Id).ToList();

            var allCharges = GetSeasonChargeNames(seasonIds, scheduleIds);

            return allCharges;
        }

        public IEnumerable<Charge> GetProgramSeasonCharges(Program program, ChargeDiscountCategory category)
        {
            List<Charge> programSeasonCharges = new List<Charge>();
            var seasonCharges = _charageRepository.GetList().Where(d => d.Category == category && d.SeasonId == program.SeasonId && !d.IsDeleted);
            var AllseasonCharges = _charageRepository.GetList().Where(d => d.SeasonId == program.SeasonId && !d.IsDeleted);

            programSeasonCharges.AddRange(seasonCharges.Where(c => c.IsAllProgram));
            programSeasonCharges.AddRange(seasonCharges.Where(c => c.Programs.Select(a => a.Id).Contains(program.Id)));

            return programSeasonCharges.Distinct();
        }

        public IEnumerable<Charge> GetProgramClubCharge(Program program, ChargeDiscountCategory category)
        {
            List<Charge> result = new List<Charge>();
            var programClubCharges = GetProgramClubCharges(program);

            result.AddRange(programClubCharges.Where(c => c.Category == category));

            return result;
        }

        public List<Charge> GetProgramClubCharges(Program program)
        {
            List<Charge> programClubCharges = new List<Charge>();

            if (program.Club != null && program.Club.Charges != null)
            {
                programClubCharges.AddRange(program.Club.Charges.Where(c => !c.IsDeleted));
            }

            if (program.Club.PartnerClub != null && program.Club.PartnerClub.Charges != null)
            {
                programClubCharges.AddRange(program.Club.PartnerClub.Charges.Where(c => !c.IsDeleted));
            }

            return programClubCharges;
        }

        public List<Charge> GetProgramCharges(OrderItem orderItem, Program program, long? tutionId)
        {
            var result = new List<Charge>();

            if (tutionId.HasValue) //tranafer
            {
                result.AddRange(program.ProgramSchedules.SelectMany(s => s.Charges).SingleOrDefault(c => c.Id == tutionId).ProgramSchedule.Charges.Where(c => c.Category != ChargeDiscountCategory.EntryFee).Where(c => !c.IsDeleted));
            }
            else //Edit
            {
                result.AddRange(program.ProgramSchedules.SelectMany(s => s.Charges).Where(o => o.ProgramScheduleId == orderItem.ProgramScheduleId && o.Category != ChargeDiscountCategory.EntryFee && !o.IsDeleted));
            }

            return result;
        }

        public List<Charge> GetOtherThanProgramCharges(Program program)
        {
            var result = new List<Charge>();

            if (!(program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || program.TypeCategory == ProgramTypeCategory.Subscription))
            {
                result.AddRange(GetProgramClubCharge(program, ChargeDiscountCategory.PartnerSurcharge));
                result.AddRange(GetProgramClubCharge(program, ChargeDiscountCategory.Surcharge));

                result.AddRange(GetProgramSeasonCharges(program, ChargeDiscountCategory.ApplicationFee));

                result.AddRange(GetInstallmentFee());
                result.AddRange(GetLateRegistationFee());
                result.AddRange(GetLatePickupFee());
            }

            result.AddRange(GetCustomCharge());

            return result;
        }

        public List<Charge> GetInstallmentFee()
        {
            var result = new List<Charge>();

            result.Add(new Charge()
            {
                Category = ChargeDiscountCategory.InstallmentFee,
                Name = ChargeDiscountCategory.InstallmentFee.ToDescription(),
                Id = -100,
            });

            return result;
        }

        public List<Charge> GetLateRegistationFee()
        {
            var result = new List<Charge>();

            result.Add(new Charge()
            {
                Category = ChargeDiscountCategory.LateRegistrationFee,
                Name = ChargeDiscountCategory.LateRegistrationFee.ToDescription(),
                Id = -101
            });

            return result;
        }

        public List<Charge> GetLatePickupFee()
        {
            var result = new List<Charge>();

            result.Add(new Charge()
            {
                Category = ChargeDiscountCategory.LatePickupFee,
                Name = ChargeDiscountCategory.LatePickupFee.ToDescription(),
                Id = -102
            });

            return result;
        }

        public List<Charge> GetCustomCharge()
        {
            var result = new List<Charge>();

            result.Add(new Charge()
            {
                Category = ChargeDiscountCategory.CustomCharge,
                Name = ChargeDiscountCategory.CustomCharge.ToDescription(),
                Id = -103
            });

            return result;
        }

        public string GetChargeType(Charge charge, bool hasPartner)
        {
            var result = $"Service - {charge?.Category.ToDescription()} ";

            if (hasPartner) // not indivisual Clubs
            {
                if (charge?.Category == ChargeDiscountCategory.Surcharge)
                {
                    result = "Service - Member surcharge";
                }

                if (charge?.Category == ChargeDiscountCategory.PartnerSurcharge)
                {
                    result = "Service - Partner surcharge";
                }

            }

            if ((charge?.Category == ChargeDiscountCategory.CustomCharge && charge.Id <= 0) || charge?.Category == ChargeDiscountCategory.LatePickupFee
                || charge?.Category == ChargeDiscountCategory.InstallmentFee || charge?.Category == ChargeDiscountCategory.LateRegistrationFee
                )
            {
                result = "Service";
            }

            return result;

        }

        public decimal GetChargeAmountForEditAndTransfer(Charge charge)
        {
            var result = charge != null ? charge.Amount : 0;

            if (charge != null && ((charge.Category == ChargeDiscountCategory.CustomCharge && charge.Id <= 0) || charge.Category == ChargeDiscountCategory.Surcharge
                || charge.Category == ChargeDiscountCategory.PartnerSurcharge || charge.Category == ChargeDiscountCategory.ApplicationFee))
            {
                result = 0;
            }

            return result;

        }

        public List<ChargeDiscountItemViewModel> GetChargesForEdit(OrderItem orderItem, Program program, List<ChargeDiscountItemViewModel> currentOrderChargeDiscount)
        {
            var result = new List<ChargeDiscountItemViewModel>();
            var programCharges = new List<Charge>();
            var programOtherThanCharges = new List<Charge>();
            var allProgramCharges = new List<Charge>();
            int id = 0;
            programCharges = GetProgramCharges(orderItem, program, null);
            programOtherThanCharges = GetOtherThanProgramCharges(program);

            allProgramCharges.AddRange(programOtherThanCharges);
            allProgramCharges.AddRange(programCharges);

            foreach (var charge in allProgramCharges)
            {
                if (!(charge.Category == ChargeDiscountCategory.LateRegistrationFee || charge.Category == ChargeDiscountCategory.InstallmentFee ||
                    charge.Category == ChargeDiscountCategory.LatePickupFee || charge.Category == ChargeDiscountCategory.CustomDiscount ||
                    (charge.Category == ChargeDiscountCategory.CustomCharge && charge.Id <= 0)))
                {
                    result.Add(
                        new ChargeDiscountItemViewModel
                        {
                            Id = id--,
                            Amount = Math.Abs(GetChargeAmountForEditAndTransfer(charge)),
                            AmountType = ChargeDiscountType.Fixed,
                            Category = charge.Category,
                            Checked = false,
                            Name = charge.Name,
                            Type = GetChargeType(charge, orderItem.Season.Club.PartnerId.HasValue),
                            SubCategory = ChargeDiscountSubcategory.Charge,
                            ChargeId = charge.Id,
                        });
                }
                else
                {
                    result.Add(
                    new ChargeDiscountItemViewModel
                    {
                        Category = charge.Category,
                        Checked = false,
                        Id = id--,
                        Name = charge.Name,
                        Type = GetChargeType(charge, orderItem.Season.Club.PartnerId.HasValue),
                        SubCategory = ChargeDiscountSubcategory.Charge,
                    });
                }
            }

            var customCharge = currentOrderChargeDiscount.FirstOrDefault(c => c.Category == ChargeDiscountCategory.CustomCharge && c.ChargeId == null);

            if (customCharge == null)
                result = result.Where(c => !currentOrderChargeDiscount.Select(d => d.Category).Contains(c.Category) || (!currentOrderChargeDiscount.Select(d => d.Name).Contains(c.Name) && c.Category == ChargeDiscountCategory.CustomCharge)).ToList();
            else
                result = result.Where(c => !currentOrderChargeDiscount.Select(d => d.Category).Contains(c.Category)).ToList();

            return result;
        }

        public List<ChargeDiscountItemViewModel> GetChargesForTransfer(OrderItem sourceOrderItem, Program targetProgram, long? tutionId)
        {
            var result = new List<ChargeDiscountItemViewModel>();
            var programCharges = new List<Charge>();
            var programOtherThanCharges = new List<Charge>();

            programOtherThanCharges = GetOtherThanProgramCharges(targetProgram);
            programCharges = GetProgramCharges(sourceOrderItem, targetProgram, tutionId);

            int id = 0;

            foreach (var charge in programOtherThanCharges) // Get charge other than program
            {
                var isChecked = false;
                var hasChargeId = false;
                decimal sourceChargeAmount = 0;

                if (GetOrderThanProgramCharges(sourceOrderItem, charge, ref isChecked, ref hasChargeId)) // check charge is required for add
                {
                    if (charge.Category == ChargeDiscountCategory.InstallmentFee || charge.Category == ChargeDiscountCategory.LateRegistrationFee
                   || charge.Category == ChargeDiscountCategory.LatePickupFee || (charge.Category == ChargeDiscountCategory.CustomCharge && charge.Id > 0)
                   || charge.Category == ChargeDiscountCategory.CustomDiscount)
                    {
                        if (sourceOrderItem.GetOrderChargeDiscounts().ToList().Any(c => c.Category == charge.Category)) // Get amount of source order
                        {
                            sourceChargeAmount = sourceOrderItem.GetOrderChargeDiscounts().FirstOrDefault(c => c.Category == charge.Category).Amount;
                        }
                    }

                    result.Add(new ChargeDiscountItemViewModel
                    {
                        Checked = isChecked,
                        Amount = sourceChargeAmount != 0 ? Math.Abs(sourceChargeAmount) : Math.Abs(GetChargeAmountForEditAndTransfer(charge)),
                        Name = charge.Name,
                        Type = GetChargeType(charge, sourceOrderItem.Season.Club.PartnerId.HasValue),
                        Id = id--,
                        SubCategory = ChargeDiscountSubcategory.Charge,
                        Category = charge.Category,
                        ChargeId = hasChargeId ? charge.Id : (long?)null,
                    });
                }
            }

            foreach (var orderChargeDiscount in sourceOrderItem.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category != ChargeDiscountCategory.EntryFee && c.Category != ChargeDiscountCategory.CancellationFee && c.Category != ChargeDiscountCategory.EditFee && c.Category != ChargeDiscountCategory.TransferFee && c.Category != ChargeDiscountCategory.ProrateFee))
            {
                if (orderChargeDiscount.Category != ChargeDiscountCategory.DayCare && orderChargeDiscount.Category != ChargeDiscountCategory.Lunch
                    && orderChargeDiscount.Category != ChargeDiscountCategory.CustomCharge &&
                    orderChargeDiscount.Category != ChargeDiscountCategory.CustomDiscount)
                {
                    if (!programOtherThanCharges.Any(c => c.Category == orderChargeDiscount.Category))
                    {
                        result.Add(new ChargeDiscountItemViewModel
                        {
                            Checked = false,
                            Amount = Math.Abs(GetChargeAmountForEditAndTransfer(orderChargeDiscount.Charge)),
                            Name = orderChargeDiscount.Name,
                            Type = GetChargeType(orderChargeDiscount.Charge, sourceOrderItem.Season.Club.PartnerId.HasValue),
                            Id = id--,
                            SubCategory = ChargeDiscountSubcategory.Charge,
                            Category = orderChargeDiscount.Category,
                            ChargeId = orderChargeDiscount.ChargeId,
                        });
                    }
                }

            }


            foreach (var programCharge in programCharges) // Get program charges
            {
                var isChecked = false;
                var hasChargeId = false;

                if (GetProgramCharges(sourceOrderItem, programCharge, ref isChecked, ref hasChargeId))  // check charge is required for add
                {
                    result.Add(new ChargeDiscountItemViewModel
                    {
                        Checked = isChecked,
                        Amount = Math.Abs(GetChargeAmountForEditAndTransfer(programCharge)),
                        Name = programCharge.Name,
                        Id = id--,
                        SubCategory = ChargeDiscountSubcategory.Charge,
                        Category = programCharge.Category,
                        Type = GetChargeType(programCharge, sourceOrderItem.Season.Club.PartnerId.HasValue),
                        ChargeId = hasChargeId ? programCharge.Id : (long?)null,
                    });
                }
            }

            return result;
        }

        public bool GetProgramCharges(OrderItem sourceOrderItem, Charge targetCharge, ref bool isChecked, ref bool hasChargeId)
        {
            var result = false;
            isChecked = false;
            hasChargeId = false;

            if (sourceOrderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == targetCharge.Category
            && c.Category != ChargeDiscountCategory.CancellationFee && c.Category != ChargeDiscountCategory.EditFee && c.Category != ChargeDiscountCategory.TransferFee && c.Category != ChargeDiscountCategory.ProrateFee
            && c.Category != ChargeDiscountCategory.EntryFee
            && c?.Charge?.Name == targetCharge.Name &&
            Math.Abs(c.Amount) == Math.Abs(targetCharge.Amount))) //when chage should Show it and active
            {
                isChecked = true;
                hasChargeId = true;
                result = true;

            }
            else if (!sourceOrderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == targetCharge.Category
            && c.Category != ChargeDiscountCategory.CancellationFee && c.Category != ChargeDiscountCategory.EditFee && c.Category != ChargeDiscountCategory.TransferFee && c.Category != ChargeDiscountCategory.ProrateFee && c.Category != ChargeDiscountCategory.EntryFee)) //when chage should Show it and not active
            {
                hasChargeId = true;
                result = true;
            }
            else if (sourceOrderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == targetCharge.Category
            && c.Category != ChargeDiscountCategory.CancellationFee && c.Category != ChargeDiscountCategory.EditFee && c.Category != ChargeDiscountCategory.TransferFee && c.Category != ChargeDiscountCategory.ProrateFee && c.Category != ChargeDiscountCategory.EntryFee
            && Math.Abs(c.Amount) != Math.Abs(targetCharge.Amount))) //when chage should Show it 
            {
                hasChargeId = true;
                result = true;
            }

            return result;
        }

        public bool GetOrderThanProgramCharges(OrderItem sourceOrderitem, Charge targetCharge, ref bool isChecked, ref bool hasChargeId)
        {
            var result = false;
            isChecked = false;
            hasChargeId = false;

            if (targetCharge.Category == ChargeDiscountCategory.LatePickupFee ||
                   targetCharge.Category == ChargeDiscountCategory.LateRegistrationFee ||
                   targetCharge.Category == ChargeDiscountCategory.InstallmentFee ||
                   (targetCharge.Category == ChargeDiscountCategory.CustomCharge && targetCharge.Id > 0) ||
                   targetCharge.Category == ChargeDiscountCategory.CustomDiscount) // charges not need chargeID
            {
                if (sourceOrderitem.GetOrderChargeDiscounts().ToList().Any(c => c.Category == targetCharge.Category && c?.Charge?.Name.Trim().ToLower() == targetCharge.Name.Trim().ToLower() &&
            Math.Abs(c.Amount) == Math.Abs(targetCharge.Amount))) // if was in source and target
                {
                    isChecked = true;
                }
            }
            else if (!(targetCharge.Category == ChargeDiscountCategory.CustomCharge && targetCharge.Id <= 0))
            {
                hasChargeId = true;
            }

            result = true;

            return result;
        }
    }
}
