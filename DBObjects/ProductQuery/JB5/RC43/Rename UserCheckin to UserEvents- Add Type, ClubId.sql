EXEC sp_rename 'UserCheckins', 'UserEvents'

ALTER TABLE UserEvents
ALTER COLUMN ProgramId bigint null

ALTER TABLE UserEvents
ALTER COLUMN SessionId int null


Alter Table UserEvents Add [Type] tinyint not null default(0)


Alter Table UserEvents Add [ClubId] int null
GO

ALTER TABLE [dbo].[UserEvents]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserEvents_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])