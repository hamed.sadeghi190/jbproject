﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class AttachmentModel
    {
        public string FileName { get; set; }

        public string FileUrl { get; set; }

        public float FileSize { get; set; }
    }
}