﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('checkOutSettingsController', ['$scope', '$http', 'jbToast', '$state', 'uiComponentsService', function ($scope, $http, jbToast, $state, uiComponentsService) {
        window.app.globalObjects.pageClass = 'checkout-settings-view';
        window.app.globalObjects.initializeView();



        $http.get('Dashboard/Setting/EditCheckOutSetting').success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.save = function (settingsSection) {

            $http.post('Dashboard/Setting/EditCheckOutSetting', { model: $scope.Model , settingsSection: settingsSection})
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {

                     $scope.errors = null;
                     
                     jbToast.success("Checkout settings updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.EditDonationForm = function () {
            $http.get('Dashboard/Setting/EditDonationForm').success(function (data, status, headers, config) {
                if (data.Status) {
                    $state.go('Form', { formType: "donation", templateId: data.Data });
                }
                else {
                    jbToast.error(data.Message);
                }
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }
        $scope.toggleTooltip = function (containerId) {
            if (arguments[3])
                uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2], arguments[3]);
            else if (arguments[2])
                uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2]);
            else if (arguments[1])
                uiComponentsService.toggleTooltip(containerId, arguments[1]);
            else
                uiComponentsService.toggleTooltip(containerId);
        }

    }])

})();