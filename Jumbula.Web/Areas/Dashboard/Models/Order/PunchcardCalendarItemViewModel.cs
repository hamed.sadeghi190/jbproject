﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class PunchcardCalendarItemViewModel
    {
        public int TaskId { get; set; }

        public string Title { get; set; }
        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public string StartTimezone { get; set; }

        public string EndTimezone { get; set; }

        public string Description { get; set; }

        public int OwnerId { get; set; }

        public bool IsAllDay { get; set; }

        public bool IsAssigned { get; set; }

        public bool IsSelected { get; set; }

        public bool IsFull { get; set; }

        public string Date { get; set; }

        public int ProgramSessionId { get; set; }
    }
}
