﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace Jumbula.Core.Model.Email
{
    public class EmailViewModel
    {
        public EmailViewModel()
        {
            TemplateNames = new List<SelectKeyValue<string>>();
            ReplyToList = new List<SelectKeyValue<int>>();
            FromList = new List<SelectKeyValue<int>>();
            CcList = new List<SelectKeyValue<int>>();
            BccList = new List<SelectKeyValue<int>>();
            Attachments = new List<AttachmentModel>();
            MailTemplate = new EmailTemplateModel();
        }
        public EmailCategory Category { get; set; }

        [Required(ErrorMessage = Common.Constants.Validation.RequiredMessage)]
        [Display(Name = "Subject")]
        [MaxLength(128)]
        public string Subject { get; set; }

        [Required(ErrorMessage = Common.Constants.Validation.RequiredMessage)]
        [Display(Name = "Body")]
        public string Body { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = Common.Constants.Validation.RequiredMessage)]
        [Display(Name = "Email")]
        [MaxLength(128)]
        public string From { get; set; }

        [Display(Name = "Reply to")]
        [MaxLength(128)]
        public string ReplyTo { get; set; }
        public List<string> Ccs { get; set; }
        public List<string> Bccs { get; set; }
        public List<string> User { get; set; }
        public List<SelectKeyValue<int>> FromList { get; set; }
        public List<SelectKeyValue<int>> ReplyToList { get; set; }
        public List<SelectKeyValue<int>> CcList { get; set; }
        public List<SelectKeyValue<int>> BccList { get; set; }

        public bool HasTemplate { get; set; }

        public List<SelectKeyValue<string>> TemplateNames { get; set; }

        [Display(Name = "Email Template")]
        public int? TemplateId { get; set; } = null;

        public EmailTemplateModel MailTemplate { get; set; }

        public int? UserId { get; set; } = null;

        public bool IsConfirmation { get; set; } = false;
        public bool IsCampaignMode { get; set; } = false;
        public string Parameters { get; set; }

        public List<EmailRecipient> Recipients { get; set; }
        public bool IsReplyToEmailSet { get; set; } = false;
        public bool IsCcListEmailSet { get; set; } = false;
        public bool IsBccListEmailSet { get; set; } = false;
        public List<AttachmentModel> Attachments { get; set; }
    }
}