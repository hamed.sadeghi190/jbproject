﻿using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class ClassSchedulingEmailElements : BaseElements
    {
        public ClassSchedulingEmailElements(IWebDriver driver)
            : base(driver)
        {

        }

        #region Header
        public IWebElement BackLink
        {
            get
            {
                return Driver.FindElement(By.ClassName("icon-arrow-left"));
            }
        }
        public IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//div[@class='title']/[h1]"));
            }
        }

        #endregion Header

        #region Recipients
        public IWebElement ViewRecipientsBtn
        {
            get
            {
                return Driver.FindElement(By.Id("editRecipients"));
            }
        }
        public IWebElement SearchTerm
        {
            get
            {
                return Driver.FindElement(By.Id("searchRecipient"));
            }
        }
        public IWebElement SearchIcon
        {
            get
            {
                return Driver.FindElement(By.Id("searchRecipients"));
            }
        }
        public bool Row1Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='GridRecipient']/div[2]/table/tbody/tr[1]/td/span")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row1Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='GridRecipient']/div[2]/table/tbody/tr[1]/td/span"));
            }
        }
        public bool Row2Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='GridRecipient']/div[2]/table/tbody/tr[2]/td/span")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row2Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='GridRecipient']/div[2]/table/tbody/tr[2]/td/span"));
            }
        }
        public bool Row3Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='GridRecipient']/div[2]/table/tbody/tr[3]/td/span")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row3Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='GridRecipient']/div[2]/table/tbody/tr[3]/td/span"));
            }
        }
        public bool Row4Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='GridRecipient']/div[2]/table/tbody/tr[4]/td/span")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row4Email
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='GridRecipient']/div[2]/table/tbody/tr[4]/td/span"));
            }
        }

        #endregion Recipients

        public IWebElement Subject
        {
            get
            {
                return Driver.FindElement(By.Id("SubjectNotification"));
            }
        }

        public IWebElement PlainTextOption
        {
            get
            {
                return Driver.FindElement(By.Id("PlainText"));
            }
        }

        public IWebElement HtmlOption
        {
            get
            {
                return Driver.FindElement(By.Id("HTML"));
            }
        }

        public IWebElement EmailBody
        {
            get
            {

                var iframe = Driver.FindElement(By.XPath("//*[@id='bodyeditor']/div/table/tbody/tr[2]/td/iframe"));
                Driver.SwitchTo().Frame(iframe);
                var body= Driver.FindElement(By.XPath("//body[@contenteditable='true']"));
                return body;
            }
        }
        public IWebElement TemplateList
        {
            get
            {
                return Driver.FindElement(By.Id("Templates"));
            }
        }
        public IWebElement NewTemplateOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='New Template ...']"));
            }
        }
        public IWebElement ConfirmationTemplateOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Confirmation template']"));
            }
        }

        public IWebElement CancellationTemplateOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Cancellation template']"));
            }
        }

        public IWebElement NewTemplateName
        {
            get
            {
                return Driver.FindElement(By.Id("NewName"));
            }
        }

        public IWebElement SendBtn
        {
            get
            {
                return Driver.FindElement(By.Id("SendNotification"));
            }
        }
        public IWebElement MessageboxTitle
        {
            get
            {
                return Driver.FindElement(By.ClassName("toast-title"));
            }
        }
        public IWebElement MessageboxBody
        {
            get
            {
                return Driver.FindElement(By.ClassName("toast-message"));
            }
        }

    }
}
