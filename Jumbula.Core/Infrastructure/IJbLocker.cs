﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Infrastructure
{
    public interface IJbLocker
    {
        bool TryLock(JbWorker worker);
        void UnLock(JbWorker worker);
    }
}
