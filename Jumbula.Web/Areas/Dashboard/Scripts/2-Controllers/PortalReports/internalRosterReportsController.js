﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('internalRosterReportsController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            var timer = false;

            $scope.Model = {};


            $scope.Model.schoolId = [];
            $scope.Model.seasonId = [];
            $scope.Model.teacher = [];

            $http.get(baseUrl + '/GetAllSchool')
            .success(function (data, status, headers, config) {

                $scope.AllSchools = data;


            });    
            
            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };
            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });
            $scope.$watch('AllSeasons', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $scope.fillSeasonData = function (schoolId) {
                $http.get(baseUrl + '/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                    $scope.AllSeasons = data;

                    $scope.Model.schoolId = schoolId;

                   
              
                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }

            $scope.fillProgramsData = function (seasonId) {
                $http.get(baseUrl + '/GetAllPrograms', { params: { seasonId: seasonId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                    $scope.AllPrograms = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }

            var checkValidation = function (schoolId, seasonId) {
                if (is.undefined(schoolId) || schoolId <= 0) {
                    jbToast.error("Please select a school.");
                    return false;
                }
                else if (is.undefined(seasonId) || seasonId <= 0) {
                    jbToast.error("Please select a season.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {
                if (checkValidation($scope.Model.schoolId, $scope.Model.SeasonId)) {
                    customReportService.saveSchoolSeasonId($scope.Model.schoolId, $scope.Model.SeasonId);
                   
                    $http.get(baseUrl + '/GetInternalRosterReport', { params: { seasonId: $scope.Model.SeasonId, schoolId: $scope.Model.schoolId, programId: $scope.Model.ProgramId } }).success(function (data, status, headers, config) {
                        $scope.ModelPrograms = data;
                 
                    }).error(function (data, status, headers, config) {
                        jbToast.error("error " + data);
                    });
   
                }
            }

            $scope.saveToPdf = function () {

                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = '/Dashboard/PortalReport/GenerateInternalRosterReportPdf?seasonId=' + $scope.Model.SeasonId + '&schoolId=' + $scope.Model.schoolId + '&programId=' + $scope.Model.ProgramId;
                a.download = "parentContact.Pdf";
                a.click();
            };
        }
    ]);
})();