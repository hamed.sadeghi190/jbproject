

alter TABLE [dbo].[Clients] add [BillingPeriodDay] [int] NOT NULL DEFAULT ((1))
go
alter TABLE [dbo].[Clients] add [LastMonthBillingPaid] [int] NOT NULL DEFAULT ((0))
go



CREATE TABLE [dbo].[BillingNotes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.BillingNotes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO







CREATE TABLE [dbo].[ClientBillingHistories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BillingId] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Debit] [decimal](18, 2) NOT NULL,
	[Credit] [decimal](18, 2) NOT NULL,
	[BillingDate] [datetime] NOT NULL,
	[ClientId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.ClientBillingHistories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClientBillingHistories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientBillingHistories_dbo.Clients_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClientBillingHistories] CHECK CONSTRAINT [FK_dbo.ClientBillingHistories_dbo.Clients_ClientId]
GO



CREATE TABLE [dbo].[ClientBillingNotes](
	[BillingNoteId] [bigint] NOT NULL,
	[ClientId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ClientBillingNotes] PRIMARY KEY CLUSTERED 
(
	[BillingNoteId] ASC,
	[ClientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClientBillingNotes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientInvoiceDescriptions_dbo.Clients_InvoiceId] FOREIGN KEY([BillingNoteId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClientBillingNotes] CHECK CONSTRAINT [FK_dbo.ClientInvoiceDescriptions_dbo.Clients_InvoiceId]
GO

ALTER TABLE [dbo].[ClientBillingNotes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientInvoiceDescriptions_dbo.InvoiceDescriptions_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[BillingNotes] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClientBillingNotes] CHECK CONSTRAINT [FK_dbo.ClientInvoiceDescriptions_dbo.InvoiceDescriptions_ClientId]
GO




CREATE TABLE [dbo].[BillingTransactionActivities](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[TransactionType] [tinyint] NOT NULL,
	[TransactionStatus] [int] NOT NULL,
	[PaymentMethod] [int] NOT NULL,
	[TransactionResponse] [nvarchar](max) NULL,
	[TransactionId] [nvarchar](128) NULL,
	[BillingId] [bigint] NOT NULL,
	[CreditCardId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.BillingTransactionActivities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[BillingTransactionActivities]  WITH CHECK ADD  CONSTRAINT [FK_dbo.BillingTransactionActivities_dbo.ClientBillingHistories_BillingId] FOREIGN KEY([BillingId])
REFERENCES [dbo].[ClientBillingHistories] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[BillingTransactionActivities] CHECK CONSTRAINT [FK_dbo.BillingTransactionActivities_dbo.ClientBillingHistories_BillingId]
GO

ALTER TABLE [dbo].[BillingTransactionActivities]  WITH CHECK ADD  CONSTRAINT [FK_dbo.BillingTransactionActivities_dbo.ClientCreditCards_CreditCardId] FOREIGN KEY([CreditCardId])
REFERENCES [dbo].[ClientCreditCards] ([Id])
GO

ALTER TABLE [dbo].[BillingTransactionActivities] CHECK CONSTRAINT [FK_dbo.BillingTransactionActivities_dbo.ClientCreditCards_CreditCardId]
GO



