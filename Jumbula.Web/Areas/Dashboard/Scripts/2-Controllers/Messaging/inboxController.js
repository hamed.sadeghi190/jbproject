﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('inboxController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'catalog-view';//seasons-view programs
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Messaging", order: 0, menu: "MESSAGING" });
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        $scope.initializeBladeSize = function () {

            var bladeWrapper = angular.element(document.querySelector('#bladeWrapper'));

            var bladeWrapperHeight = bladeWrapper.css('height');

            var messageElement = angular.element(document.querySelector('#messageItemsWrapper'));

            messageElement.css('height', bladeWrapperHeight);
        }

        $scope.initializeBladeSize();

        $scope.Start = 0;
        $scope.Take = 20;

        $scope.LastStart = -1;
        $scope.Total = 0;
        var isGetInProcess = false;

        $scope.getChats = function (start, take) {

            if ($scope.Start > $scope.Total) {
                return;
            }

            if (!isGetInProcess && $scope.LastStart != $scope.Start) {
                isGetInProcess = true;
                $scope.LastStart = $scope.Start;

                $http.get('/Message/GetInboxContacts', { params: { start: start, take: take } })
                    .success(function (data, status, headers, config) {
                    $scope.MC.CurrentChat = null;

                    $scope.Total = data.Total;

                    var messages = data.Messages;

                    if ($scope.MC.Chats == null || $scope.MC.Chats.lenght < 1 || $scope.Start == 0) {
                        $scope.MC.Chats = messages;
                    }
                    else {
                        for (var i = 0; i < messages.length; i++) {
                            $scope.MC.Chats.push(messages[i]);
                        }
                    }

                    $scope.Start = start + take;
                    $scope.Take = 10;
                    isGetInProcess = false;

                  })
            }
        }


        $scope.getChats($scope.Start, $scope.Take);

        if ($stateParams.chatId) {
            $scope.MC.getChat($stateParams.chatId);
        }

        $scope.formatDateTime = function (dateTime) {

            var date = convertUTCDateToLocalDate(new Date(dateTime));
            return formatDate(date);
        }

        function convertUTCDateToLocalDate(date) {
            var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);

            var offset = date.getTimezoneOffset() / 60;
            var hours = date.getHours();

            newDate.setHours(hours - offset);

            return newDate;
        }

        var formatDate = function (date) {
            var monthNames = [
              "January", "February", "March",
              "April", "May", "June", "July",
              "August", "September", "October",
              "November", "December"
            ];

            var day = date.getDate();
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return day + ' ' + monthNames[monthIndex] + ' ' + year;
        }

        $scope.formatTime = function (ticks) {

            //ticks are in nanotime; convert to microtime
            var ticksToMicrotime = ticks / 10000;

            //ticks are recorded from 1/1/1; get microtime difference from 1/1/1/ to 1/1/1970
            var epochMicrotimeDiff = 2208988800000;

            //new date is ticks, converted to microtime, minus difference from epoch microtime
            var dateTime = new Date(ticksToMicrotime - epochMicrotimeDiff);

            var result = dateTime.format('h:MM TT', false);

            return result;
        }

        $scope.formatDate = function (ticks) {

            //ticks are in nanotime; convert to microtime
            var ticksToMicrotime = ticks / 10000;

            //ticks are recorded from 1/1/1; get microtime difference from 1/1/1/ to 1/1/1970
            var epochMicrotimeDiff = 2208988800000;

            //new date is ticks, converted to microtime, minus difference from epoch microtime
            var dateTime = new Date(ticksToMicrotime - epochMicrotimeDiff);

            var result = dateTime.format('mmm d', false);

            return result;
        }

        $scope.init = function () {
            $('[data-role=dropdown]').dropdown();
        }

        $scope.loadMore = function () {

            $scope.getChats($scope.Start, $scope.Take);
        };

    }]);
})();