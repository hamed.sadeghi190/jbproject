﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('confirmPartiallyRefundController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cancelOrderService', 'jbToast', function ($scope, $http, $log, $stateParams, $state, cancelOrderService, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/Order";

        $scope.Model = cancelOrderService.get();
        
        $scope.cancelAction = function () {
            cancelOrderService.set($scope.Model);
            $state.back();
        }

        $scope.cancelAndRefundOrder = function () {
            
            $('#submitCancelAndRefundOrder').prop("disabled", true);
            $http.post($scope.baseUrl + '/SubmitRefundOrderItem', { orderViewModel: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status) {
                      
                        jbToast.success('Refund operation done successfully.');

                        $scope.MC.backQueueItems.pop();
                        if ("comesFromOrderItem" in window) {
                            if (window.comesFromOrderItem) {
                                window.comesFromOrderItem = false;
                                $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId });
                            }
                        }
                       else if ("goToFamilyOrder" in window) {
                            if (window.goToFamilyOrder > 0) {
                                var userId = window.goToFamilyOrder;
                                var clubId = window.clubId;
                                window.goToFamilyOrder = -1;
                                window.clubId = -1;
                                $state.go('FamilyOrders', { userId: userId, clubId: clubId});
                            }
                        }
                        else
                            {
                            $state.go('OrderItem', { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId })
                            }
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }

                    $('#submitCancelAndRefundOrder').prop("disabled", false);
                });

        };
    }]);
})();