﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class Category : BaseEntity
    {
        //public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        public string LogoUri { get; set; }

        public bool IsPrimary { get; set; }

        public bool SupportTourney { get; set; }

        public int? ParentId { get; set; }

        public int? Order { get; set; }

        [Column(TypeName = "xml")]
        public string Configuration { get; set; }

        [ForeignKey("ParentId")]
        public virtual Category ParentCategory { set; get; }

        public virtual IList<Category> SubCategories { get; set; }

        public virtual IList<CetgoryTemplate> CategoryTemplates { get; set; }

        public virtual IList<Program> Programs { get; set; }

        public virtual IList<CatalogItem> CatalogItems { get; set; }

    }

    public class CetgoryTemplate
    {
        public int Id { get; set; }

        [Required]
        public SubDomainCategories SubDomainCategory { get; set; }

        [Required]
        public int Cotegory_Id { get; set; }

        [ForeignKey("Cotegory_Id")]
        public virtual Category Category { get; set; }

        [Column(TypeName = "xml")]
        public string CustomFields { get; set; }
    }
}
