﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('InsuranceCertificateCheckReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};
        var isBegin = false;

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.filter.printMode = false;

        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            dataInfo = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        
        $scope.filter.typeId = -1;

        var model = $scope.Model;

        var prepareGrid = function () {

            isBegin = true;

            $scope.InsuranceCertificateCheckDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetInsuranceCertificateCheckReportInfo",
                        dataType: "json",
                        //type: "POST",
                        data: function () {
                            return {
                                startDate: $scope.filter.startDate,
                                endDate: $scope.filter.endDate,                                
                                printMode: $scope.filter.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
            });

            $scope.InsuranceCertificateCheckGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Insurance certificate check).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Insurance certificate check - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.InsuranceCertificateCheckDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#InsuranceCertificateCheckGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [
                    {
                        field: "ProviderName",
                        title: "Provider name",
                        width: "180px"
                    }, {
                        field: "Email",
                        title: "Email",
                        width: "180px"
                    },
                    {
                        field: "Insurance",
                        title: "Insurance list",
                        width: "200px"
                    },
                    {
                        field: "Date",
                        title: "Expiration",
                        width: "200px"
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $scope.filter.printMode = false;
            $("#InsuranceCertificateCheckGrid").getKendoGrid().saveAsExcel();

        }
        $scope.saveToPDF = function () {
            $scope.filter.printMode = true;
            $("#InsuranceCertificateCheckGrid").data("kendoGrid").dataSource.read();

        }
        $scope.print = function () {
            $scope.filter.printMode = true;
            $("#InsuranceCertificateCheckGrid").data("kendoGrid").dataSource.read();
        }

        $scope.search = function () {
            if (!isBegin) {
                prepareGrid();
            } else {
                $scope.filter.printMode = false;
                $("#InsuranceCertificateCheckGrid").data("kendoGrid").dataSource.read();
            }

        };

        $scope.clearFilters = function () {
            $scope.filter.endDate = "";
            $scope.filter.startDate = "";           
            $scope.filter.printMode = false;
            $("#InsuranceCertificateCheckGrid").data("kendoGrid").dataSource.read();
        }




    }
    ]);

})();