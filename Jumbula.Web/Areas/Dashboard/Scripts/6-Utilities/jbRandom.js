﻿(function () {
    'use strict';

    angular.module('jbTools').factory("jbRandom", [function () {
        return {
            Generate: function (length, allowNumbers, allowChars, allowUppercase, allowLowercase, returnAsNumber) {
                var random = "", possible = "";
                if (allowNumbers)
                    possible += "0123456789";
                if (allowChars) {
                    if (allowUppercase)
                        possible += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    if (allowLowercase)
                        possible += "abcdefghijklmnopqrstuvwxyz";
                }
                for (var i = 0; i < length; i++)
                    random += possible.charAt(Math.floor(Math.random() * possible.length));
                if (returnAsNumber && allowNumbers && !allowChars)
                    return parseInt(random);
                return random;
            }
        }
    }]);
})();