﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonReplicateController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', 'jbConfirmation', function ($scope, $http, $stateParams, $state, jbToast, jbConfirmation) {
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + '/Dashboard/Club';
        $scope.clubId = $scope.MC.stateParams.clubId;

        $scope.Model = {};

        if ($stateParams.memberId != 0) {
            $scope.ReplicateMode = 'SingleReplicate';
        }
        else {
            $scope.ReplicateMode = 'MultipleReplicate';
        }

        var seasonDomain = $stateParams.seasonDomain;

        $http.get($scope.baseUrl + '/GetSeasonReplicate', { params: { clubId: $scope.clubId, memberId: $stateParams.memberId } }).success(function (data, status, headers, config) {

            $scope.Model = data;

            if ($scope.ReplicateMode == 'SingleReplicate') {
                $scope.Model.MemberId = $stateParams.memberId;
            }

        });

        $scope.save = function () {

            var message = 'Are you sure you want to replicate the season to all the members?';

            if ($stateParams.memberId != 0) {
                message = 'Are you sure you want to replicate the season to this member?'
            }

            jbConfirmation
               .yes(function () {

                   $http.post($scope.baseUrl + '/SaveSeasonReplicate', { model: $scope.Model }).success(function (data, status, headers, config) {

                       if (data.Status) {
                           jbToast.success("Seasons replicated successfully.");
                       }
                       else {
                           $scope.MC.handleErrors($scope, data);
                       }
                   });

               })
                .title('Replicate season')
                .message(message)
                .confirm();
        }

    }])

})();