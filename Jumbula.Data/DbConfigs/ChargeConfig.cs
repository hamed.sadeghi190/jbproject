﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class ChargeConfig : EntityTypeConfiguration<Charge>
    {
        public ChargeConfig()
        {
            HasMany(model => model.Programs).WithMany(model => model.Charges);
        }
    }
}
