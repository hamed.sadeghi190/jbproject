﻿using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class PlayerInfo : BaseEntity
    {
        public string Allergies { get; set;}
        public string SpecialNeeds { get; set; }
        public bool? SelfAdministerMedication { get; set; }
        public string SelfAdministerMedicationDescription { get; set; }
        public bool? NutAllergy { get; set; }

        [MaxLength(256)]
        public string DoctorName { get; set; }

        [MaxLength(64)]
        public string DoctorPhone { get; set; }

        [MaxLength(128)]
        public string DoctorLocation { get; set; }
        public bool? HasPermissionPhotography { get; set; }

        [MaxLength(256)]
        public string SchoolName { get; set; }

        [MaxLength(64)]
        public string HomeRoom { get; set; }
        public SchoolGradeType Grade { get; set; }

        public bool? HasSpecialNeeds { get; set; }

        public bool? HasAllergies { get; set; }

        [MaxLength(4000)]
        public string NutAllergyDescription { get; set; }

    }
}
