﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage.Table;

namespace SportsClub.Models.Temp_Model
{
    public class NewUsers : TableEntity
    {
        public string FullName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}