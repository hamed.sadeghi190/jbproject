﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Report;
using Jumbula.Core.Model.Report.Filter;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace Jumbula.Business.ReportBuilders
{
    public class CampRosterPortalReportBuilderBusiness : BaseReportBuilder
    {
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IPlayerProfileBusiness _playerProfileBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly INewReportBusiness _reportBusiness;


        private const string ProgramIdParam = "programId";
        private const string ScheduleIdParam = "scheduleId";
        private const string EntryFeeId = "entryFeeId";

        public CampRosterPortalReportBuilderBusiness(IOrderItemBusiness orderItemBusiness, IPlayerProfileBusiness playerProfileBusiness, IProgramBusiness programBusiness, INewReportBusiness reportBusiness)
        {
            //_orderItemBusiness = (IOrderItemBusiness)DependencyResolver.Current.GetService(typeof(IOrderItemBusiness));
            //_playerProfileBusiness = (IPlayerProfileBusiness)DependencyResolver.Current.GetService(typeof(IPlayerProfileBusiness));
            //_programBusiness = (IProgramBusiness)DependencyResolver.Current.GetService(typeof(IProgramBusiness));
            //_reportBusiness = (INewReportBusiness)DependencyResolver.Current.GetService(typeof(INewReportBusiness));
            _orderItemBusiness = orderItemBusiness;
            _playerProfileBusiness = playerProfileBusiness;
            _programBusiness = programBusiness;
            _reportBusiness = reportBusiness;
        }

        public override ReportName ReportName => ReportName.CampRosterPortal;
        public override PdfTemplateName? PdfTemplate => PdfTemplateName.Default;
        public override ExcelTemplateName? ExcelTemplate => ExcelTemplateName.Default;

        public override ReportDataModel BindData(Dictionary<string, object> parameters, ClubBaseInfoModel clubBaseInfo, PaginationModel paginationModel, List<ReportGridSort> sort = null)
        {

            //if (!HasParameter(parameters, ProgramIdParam) || !HasParameter(parameters, ScheduleIdParam) ) // || !HasParameter(parameters, EntryFeeId))
            //{
            //    return new PaginatedList<object>(null, 0, 0, 0);
            //}

            var total = 0;

            var titles = new List<ReportGridColumn>();

            var programId = long.Parse(GetParameter(parameters, ProgramIdParam).ToString());
            var scheduleId = 327427; //long.Parse(GetParameter(parameters, ScheduleIdParam).ToString());
            var entryFeeId = 0; //long.Parse(GetParameter(parameters, EntryFeeId).ToString());

            var dictionaryModel = CreateModelCampRosterReport(programId, paginationModel.Page, paginationModel.PageSize, ref total, ref titles, scheduleId, entryFeeId);

            var result = dictionaryModel.ToPaginatedList<object>(paginationModel.Page, paginationModel.PageSize, total);

            var newTitles = new List<ReportGridColumn>();

            if (dictionaryModel.Any())
            {
                for (int i = 0; i < dictionaryModel[0].Count; i++)
                {
                    newTitles.Add(titles.ElementAt(i));
                }
            }

            titles = newTitles;


            return new ReportDataModel(result, titles);
        }

        public override List<IJbReportFilterElement> GetFilters()
        {
            var jbReportFilter = new JbReportFilter();

            jbReportFilter.Elements.AddRange(new List<IJbReportFilterElement>()
            {
                new JbDropDownReportFilter()
                {
                    Id = 0,
                    Name = ReportFilterElementName.School,
                    Title = ReportFilterElementName.School.ToDescription(),
                    ActionFilter = ReportFilterAction.PrepareData,
                    TargetFilter = ReportFilterElementName.Season,
                    ParameterName = ReportConstants.ClubIdParam,
                    Data = _reportBusiness.GetPartnerSchools(ClubBaseInfo.Id)
                },
                new JbDropDownReportFilter()
                {
                    Id = 1,
                    Name = ReportFilterElementName.Season,
                    Title = ReportFilterElementName.Season.ToDescription(),
                    ActionFilter = ReportFilterAction.NoAction,
                    TargetFilter = ReportFilterElementName.CampProgram,
                    ParameterName = ReportConstants.SeasonIdParam
                },
                new JbDropDownReportFilter()
                {
                    Id = 2,
                    Name = ReportFilterElementName.CampProgram,
                    Title = ReportFilterElementName.CampProgram.ToDescription(),
                    ActionFilter = ReportFilterAction.NoAction,
                    TargetFilter = ReportFilterElementName.Schedule,
                    ParameterName = ReportConstants.ProgramIdParam
                },
                new JbDropDownReportFilter()
                {
                    Id = 3,
                    Name = ReportFilterElementName.Schedule,
                    Title = ReportFilterElementName.Schedule.ToDescription(),
                    ActionFilter = ReportFilterAction.NoAction,
                    TargetFilter = ReportFilterElementName.Tuition,
                    ParameterName = ReportConstants.ScheduleIdParam
                },
                new JbDropDownReportFilter()
                {
                    Id = 4,
                    Name = ReportFilterElementName.Tuition,
                    Title = ReportFilterElementName.Tuition.ToDescription(),
                    ActionFilter = ReportFilterAction.NoAction,
                    ParameterName = ReportConstants.EntryFeeIdParam
                }
            });

            return jbReportFilter.Elements;
        }

        public override List<ReportHeader> GetHeaders()
        {
            return null;
        }

        public override List<ReportGridColumn> GetColumns()
        {
            return null;
        }

        public override PaginatedList<object> GetData()
        {
            return null;
        }

        public override void Initiate()
        {
            
        }

        private List<IDictionary<string, object>> CreateModelCampRosterReport(long programId, int page, int pageSize, ref int total, ref List<ReportGridColumn> titles, long? scheduleId, decimal? entryFeeId)
        {
            var query = _orderItemBusiness.GetProgramOrderItems(programId);

            if (scheduleId.HasValue && scheduleId.Value > 0)
            {
                query = query.Where(item => item.ProgramScheduleId == scheduleId);
            }

            if (entryFeeId.HasValue && entryFeeId.Value > 0)
            {
                query = query.Where(oitem => oitem.OrderChargeDiscounts.Any(cd => !cd.IsDeleted && cd.ChargeId == entryFeeId));
            }

            var orderItemPaging = query
                .GroupBy(o => o.PlayerId.Value)
                .Select(o => o.FirstOrDefault())
                .OrderBy(o => o.Player.Contact.LastName)
                .ThenBy(o => o.Player.Contact.FirstName)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            var playerProfileBusiness = _playerProfileBusiness;

            var userIds = orderItemPaging.Select(o => o.Order.UserId).ToList();

            var allParticipants = playerProfileBusiness.GetAllParticipantsProfile(userIds);
            var allParents = playerProfileBusiness.GetAllParentsProfile(userIds);
            var allEmergencyContacts = playerProfileBusiness.GetAllEmergencyFamily(userIds);
            var allAuthorizePickups = playerProfileBusiness.GetAllAuthorizePickupFamily(userIds);

            var model = new List<IDictionary<string, Object>>();

            var schedules = new List<ProgramSchedule>();
            if (scheduleId.HasValue && scheduleId.Value > 0)
            {
                schedules.Add(_programBusiness.GetSchedule(scheduleId.Value));
            }
            else
            {
                schedules.AddRange(_programBusiness.Get(programId).ProgramSchedules);
            }

            foreach (var item in orderItemPaging)
            {
                var participant = allParticipants.FirstOrDefault(a => item.PlayerId != null && a.Id == item.PlayerId.Value);
                var parent1 = allParents.Count(a => a.UserId == item.Order.UserId) >= 1 ? allParents.Where(a => a.UserId == item.Order.UserId).ElementAt(0) : null;
                var parent2 = allParents.Count(a => a.UserId == item.Order.UserId) >= 2 ? allParents.Where(a => a.UserId == item.Order.UserId).ElementAt(1) : null;
                var emergency = allEmergencyContacts.FirstOrDefault(a => a.Family.UserId == item.Order.UserId);
                var authorize1 = allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId) != null && allAuthorizePickups.Count(a => a.Family.UserId == item.Order.UserId) >= 1 ? allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).ElementAt(0) : null;
                var authorize2 = allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId) != null && allAuthorizePickups.Count(a => a.Family.UserId == item.Order.UserId) >= 2 ? allAuthorizePickups.Where(a => a.Family.UserId == item.Order.UserId).ElementAt(1) : null;

                var newItem = new ExpandoObject() as IDictionary<string, Object>;


                newItem.Add("Participant", participant?.Contact != null ? $"{participant.Contact.LastName}, {participant.Contact.FirstName}" : string.Empty);
                titles.Add(new ReportGridColumn() { Field = "Participant", Title = "Participant", Locked = true});

                for (int i = 0; i < schedules.Count; i++)
                {
                    var schedule = schedules.ElementAt(i);
                    if (item.Player.OrderItems.Any(o => o.ProgramScheduleId != null && o.ProgramScheduleId.Value == schedule.Id))
                    {
                        var Title = !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Title} ({schedule.StartDate.ToString(Constants.DefaultDateFormat)}-{schedule.EndDate.ToString(Constants.DefaultDateFormat)})"
                            : $"{schedule.StartDate.ToString(Constants.DefaultDateFormat)}-{schedule.EndDate.ToString(Constants.DefaultDateFormat)}";

                        titles.Add(new ReportGridColumn() { Field = $"_{schedule.Id}", Title = Title });

                        Title = $"_{schedule.Id}";

                        var value = string.Join(", ", item.Player.OrderItems.Where(o => o.ProgramScheduleId != null && o.ProgramScheduleId.Value == schedule.Id).Select(o => o.EntryFeeName));

                        newItem.Add(Title.Replace(" ", "_"), value);
                    }
                    else
                    {
                        var Title = !string.IsNullOrEmpty(schedule.Title) ? $"{schedule.Title} ({schedule.StartDate.ToString(Constants.DefaultDateFormat)}-{schedule.EndDate.ToString(Constants.DefaultDateFormat)})"
                            : $"{schedule.StartDate.ToString(Constants.DefaultDateFormat)}-{schedule.EndDate.ToString(Constants.DefaultDateFormat)}";

                        titles.Add(new ReportGridColumn() { Field = $"_{schedule.Id}", Title = Title });

                        Title = $"_{schedule.Id}";

                        var value = string.Empty;

                        newItem.Add(Title.Replace(" ", "_"), value);
                    }
                }

                newItem.Add("Gender", participant?.Contact != null ? (participant.Gender == GenderCategories.Male || participant.Gender == GenderCategories.Female || participant.Gender == GenderCategories.GenderNeutral) ? participant.Gender.ToDescription() : string.Empty : string.Empty);
                newItem.Add("Grade", participant?.Info != null ? participant.Info.Grade != 0 ? participant.Info.Grade.ToDescription() : string.Empty : string.Empty);
                newItem.Add("Parent_1", parent1?.Contact != null ? $"{parent1.Contact.LastName}, {parent1.Contact.FirstName}" : string.Empty);
                newItem.Add("Phone_1", parent1?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(parent1.Contact.Phone) : string.Empty);
                newItem.Add("Email_1", parent1?.Contact != null ? parent1.Contact.Email : string.Empty);
                newItem.Add("Parent_2", parent2?.Contact != null ? $"{parent2.Contact.LastName}, {parent2.Contact.FirstName}" : string.Empty);
                newItem.Add("Phone_2", parent2?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(parent2.Contact.Phone) : string.Empty);
                newItem.Add("Email_2", parent2?.Contact != null ? parent2.Contact.Email : string.Empty);
                newItem.Add("Emergency_contact", emergency?.Contact != null ? $"{emergency.Contact.LastName}, {emergency.Contact.FirstName}" : string.Empty);
                newItem.Add("Emergency_contact_phone", emergency?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(emergency.Contact.Phone) : string.Empty);
                newItem.Add("Authorize_adult_1", authorize1?.Contact != null ? $"{authorize1.Contact.LastName}, {authorize1.Contact.FirstName}" : string.Empty);
                newItem.Add("Authorize_adult_1_phone", authorize1?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorize1.Contact.Phone) : string.Empty);
                newItem.Add("Authorize_adult_2", authorize2?.Contact != null ? $"{authorize2.Contact.LastName}, {authorize2.Contact.FirstName}" : string.Empty);
                newItem.Add("Authorize_adult_2_phone", authorize2?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorize2.Contact.Phone) : string.Empty);
                newItem.Add("Medical_condition_SSLASH_special_needs", participant?.Info != null ? participant.Info.SpecialNeeds : string.Empty);
                newItem.Add("Allergies", participant?.Info != null ? participant.Info.Allergies : string.Empty);

                titles.Add(new ReportGridColumn() { Field = "Gender", Title = "Gender" });
                titles.Add(new ReportGridColumn() { Field = "Grade", Title = "Grade" });
                titles.Add(new ReportGridColumn() { Field = "Parent_1", Title = "Parent 1" });
                titles.Add(new ReportGridColumn() { Field = "Phone_1", Title = "Phone 1" });
                titles.Add(new ReportGridColumn() { Field = "Email_1", Title = "Email 1" });

                titles.Add(new ReportGridColumn() { Field = "Parent_2", Title = "Parent 2" });
                titles.Add(new ReportGridColumn() { Field = "Phone_2", Title = "Phone 2" });
                titles.Add(new ReportGridColumn() { Field = "Email_1", Title = "Email 2" });

                titles.Add(new ReportGridColumn() { Field = "Emergency_contact", Title = "Emergency contact" });
                titles.Add(new ReportGridColumn() { Field = "Emergency_contact_phone", Title = "Phone" });

                titles.Add(new ReportGridColumn() { Field = "Authorize_adult_1", Title = "Authorize adult 1" });
                titles.Add(new ReportGridColumn() { Field = "Authorize_adult_1_phone", Title = "Phone" });

                titles.Add(new ReportGridColumn() { Field = "Authorize_adult_2", Title = "Authorize adult 2" });
                titles.Add(new ReportGridColumn() { Field = "Authorize_adult_2_phone", Title = "Phone" });

                titles.Add(new ReportGridColumn() { Field = "Medical_condition_SSLASH_special_needs", Title = "Medical condition/special needs" });
                titles.Add(new ReportGridColumn() { Field = "Allergies", Title = "Allergies" });

                model.Add(newItem);
            }

            total = query.GroupBy(o => o.PlayerId.Value).Count();

            return model;
        }

    }

}
