﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class GridSort
    {
        public string field { get; set; }

        public string dir { get; set; }
    }
}