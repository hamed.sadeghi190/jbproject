﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IPlayerProgramNoteBusiness
    {
        PlayerProgramNote Get(int id);
        IQueryable<PlayerProgramNote> GetList();
        IQueryable<PlayerProgramNote> GetList(Expression<Func<PlayerProgramNote, bool>> predicate);
        IQueryable<PlayerProgramNote> GetList(int programId, int clubStaffId, int profileId);
        OperationStatus Create(PlayerProgramNote playerProgramNote);
        OperationStatus Update(PlayerProgramNote playerProgramNote);
        OperationStatus Delete(List<int> ids);
        OperationStatus Update(List<PlayerProgramNote> notes);

    }
}
