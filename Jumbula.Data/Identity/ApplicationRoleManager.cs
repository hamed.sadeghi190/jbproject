﻿using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Data.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace Jumbula.Data.Identity
{
    public class ApplicationRoleManager : RoleManager<JbRole, int> , IApplicationRoleManager<JbRole>
    {
        public ApplicationRoleManager(IRoleStore<JbRole, int> roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options,
            IOwinContext context)
        {
            return new ApplicationRoleManager(new JbRoleStore(context.Get<JumbulaDataContext>()));
        }
    }

  
}
