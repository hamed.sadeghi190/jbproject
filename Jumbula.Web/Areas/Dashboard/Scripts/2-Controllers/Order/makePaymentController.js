﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('makePaymentController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'makePaymentService', function ($scope, $http, $log, $stateParams, $state, jbToast, makePaymentService) {
            window.app.globalObjects.pageClass = 'seasons-view order';
            window.app.globalObjects.initializeView();
            $scope.MC.stateParams = $stateParams;
            $scope.baseUrl = window.location.origin + "/Dashboard/Order";
            $scope.orderItemId = 0;
            $scope.transactionId = 0;
            $scope.balance = 0;
            $scope.maxAmount = 0;

            $scope.pageTitle = "Take a new payment";
            if ($stateParams.transactionId) {
                $scope.transactionId = $stateParams.transactionId;
            }
            if ($stateParams.orderItemId) {
                $scope.orderItemId = $stateParams.orderItemId;
            }
            var returnedByBack = $state.returnByBack();
            $scope.Model = {};
            if (returnedByBack) {
                $scope.Model = makePaymentService.get();

                if ($scope.Model.isPayInstallment) {
                    var i = 0, len = $scope.Model.Installments.length;
                    for (; i < len; i++) {
                        if ($scope.Model.Installments[i].IsSelected == true) {
                            $scope.balance += $scope.Model.Installments[i].OldBalance;

                        }
                    }
                    $scope.maxAmount = $scope.balance;
                } else {
                    $scope.balance = $scope.Model.OldBalance;
                    $scope.maxAmount = $scope.balance;
                }



            }
            if (!returnedByBack || $scope.Model == null) {
                $http.get($scope.baseUrl + '/GetMakePaymentModel/?orderItemId=' + $stateParams.orderItemId + '&token=' + $scope.transactionId).success(function (data, status, headers, config) {
                    $scope.Model = data;

                    if ($scope.Model.IsEdit) {
                        $scope.pageTitle = "Edit payment";
                    }
                    if ($scope.Model.isPayInstallment) {
                        var i = 0, len = $scope.Model.Installments.length;
                        for (; i < len; i++) {
                            if ($scope.Model.Installments[i].IsSelected == true) {
                                $scope.balance += $scope.Model.Installments[i].OldBalance;

                            }
                        }
                        $scope.maxAmount = $scope.balance;
                    } else {
                        $scope.balance = $scope.Model.OldBalance;
                        $scope.maxAmount = $scope.balance;
                    }


                }).error(function (data, status, headers, config) {
                    $scope.MC.handleErrors($scope, data);
                });
            }

            $scope.calculateBalance = function (instAmount, instSelected) {
                if (instSelected) {
                    $scope.balance += instAmount;
                } else {
                    $scope.balance -= instAmount;
                }
            }


            $scope.changeMaxAmount = function (chargeMethod) {
                if (chargeMethod == '4') {
                    $scope.maxAmount = ($scope.balance >= $scope.Model.AvailableCredit ? ($scope.Model.AvailableCredit).formatCurrencyWithPenny(2) : ($scope.balance).formatCurrencyWithPenny(2));
                    $scope.Model.SelectedPaymentMethod = 13;
                }
                else {
                    $scope.maxAmount = $scope.balance;
                    $scope.Model.SelectedPaymentMethod = 0;
                }
            }

            $scope.cancelAction = function () {
                if ("comesFromOrderItem" in window) {
                    if (window.comesFromOrderItem) {
                        window.comesFromOrderItem = false;
                        $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId });
                    }
                }
                else if ("goToFamilyOrder" in window) {
                    if (window.goToFamilyOrder > 0) {
                        var userId = window.goToFamilyOrder;
                        window.goToFamilyOrder = -1;
                        $state.go('FamilyOrders', { userId: userId });
                    }
                }
                else {
                    $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId });
                }
            }
            $scope.confirmPayment = function () {
                $http.post($scope.baseUrl + '/ValidateMakePayment', { model: $scope.Model })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            if ($scope.Model.SelectedChargeMethod == null || $scope.Model.SelectedChargeMethod =='') {
                                jbToast.error('You must choose the method of payment.');
                            } else {
                                $scope.Model.NewBalance = $scope.Model.OldBalance - $scope.Model.Amount;
                                if ($scope.Model.isPayInstallment) {
                                    var i = 0, len = $scope.Model.Installments.length;
                                    var changeAmount = $scope.Model.Amount;
                                    for (; i < len; i++) {
                                        if ($scope.Model.Installments[i].IsSelected == true) {
                                            var instBalance = $scope.Model.Installments[i].OldBalance;
                                            if (instBalance >= changeAmount) {
                                                $scope.Model.Installments[i].NewBalance = parseInt(instBalance) - parseInt(changeAmount);
                                                changeAmount = 0;
                                            }
                                            else {
                                                $scope.Model.Installments[i].NewBalance = 0;
                                                changeAmount = parseInt(changeAmount) - parseInt(instBalance);
                                            }

                                        }
                                        else {
                                            $scope.Model.Installments[i].NewBalance = $scope.Model.Installments[i].OldBalance;
                                        }
                                    }
                                }
                                makePaymentService.set($scope.Model);
                                $state.forward('ConfirmMakePayment', { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, orderItemId: $stateParams.orderItemId }, 'MakePayment', { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, orderItemId: $stateParams.orderItemId }, null);

                            }

                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            }

            if (returnedByBack) {
                $scope.MC.backQueueItems.pop();
            }
        }
    ]);
})();