﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('paymentPlanManageController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'discounts-view';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/PaymentPlan";
        $scope.pageTitle = "Manage Payment Plan";
        $scope.paymentPlanId = 0;
        if ($stateParams.paymentPlanId) {
            $scope.paymentPlanId = $stateParams.paymentPlanId;
        }

        $scope.installmentDateInputs = [];

        $scope.addInput = function (date) {

            $scope.installmentDateInputs.push({ duedate: date});
        }

        $scope.removeInput = function (index) {
            $scope.installmentDateInputs.splice(index, 1);
        }
        $scope.Model = {}
      


        $http.get($scope.baseUrl + '/CreateEditPaymentPlan?paymentPlanId=' + $scope.paymentPlanId + '&seasonDomain=' + $scope.MC.stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model = data;

            var exNo = $scope.Model.NumOfInstallments;
            if ($scope.Model.ListOfInstallmentDates != null) {
                for (var i = 0; i < exNo; i++) {
                    $scope.addInput($scope.Model.ListOfInstallmentDates[i]);
                }
            }
            else {

                for (var i = 0; i < exNo; i++) {
                    $scope.addInput('');
                }
            }

        });
  
        $scope.updateInstallmentDates = function () {

            var exNo = $scope.Model.NumOfInstallments;
            var NewNo = $scope.Model.SelectedInstallments.Value;
            if (exNo > NewNo)
            {
                for (var i = exNo; i > NewNo; i--)
                {
                    $scope.removeInput(i - 1);
                }
            }
            else {
                for (var i = exNo; i < NewNo; i++) {
                    $scope.addInput('');
                }
            }

            $scope.Model.NumOfInstallments = NewNo;
        }

        $scope.programDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: {
                    url: window.location.origin + "/Dashboard/program/GetListForDropDown?seasonDomain=" + $scope.MC.stateParams.seasonDomain,
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            term: document.activeElement.value
                        }
                    }
                }
            }
        });
        $scope.programOptions = {
            placeholder: "Select programs...",
            dataTextField: "Name",
            dataValueField: "Id",
            autoBind: false,
            dataSource: $scope.programDataSource
        };
        $scope.userDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: {
                    url: window.location.origin + '/Dashboard/People/GetFamiliesForDropDown/',
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            term: document.activeElement.value
                        }
                    }
                }
            },
            schema: {
                data: "Data"
            }
        });

        $scope.userOptions = {
            placeholder: "Select users...",
            dataTextField: "UsernameAndPlayersName",
            dataValueField: "Id",
            autoBind: false,
            dataSource: $scope.userDataSource
        };


        $scope.getProgramsList = function () {
            if ($scope.Model && $scope.Model.ProgramsList) {
                return $scope.Model.ProgramsList;
            } else {
                return [];
            }
        }

        $scope.savePaymentplan = function () {
            $scope.Model.InstallmentDueDates = '';
            for (var i = 0; i < $scope.installmentDateInputs.length; i++) {
                if (i != 0)
                {
                    $scope.Model.InstallmentDueDates += ',';
                }
                $scope.Model.InstallmentDueDates += $scope.installmentDateInputs[i].duedate;
            }
         
                $http.post($scope.baseUrl + '/SavePaymentPlan', { model: $scope.Model })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $state.go('PaymentPlans', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                        }
                        else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
        };

      
    }]);
})();