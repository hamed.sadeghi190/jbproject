﻿using System.Collections.Generic;
using Jumbula.Core.Model.Report.Filter;

namespace Jumbula.Core.Model.Report
{
    public class ReportModel
    {
        public ReportModel(string title, Dictionary<string, object> headers, List<ReportExportElement> exports, ReportGridModel reportGridModel, string filterView, JbReportFilter reportFilter)
        {
            Title = title;
            Headers = headers;
            Filters = reportFilter;
            Exports = exports;
            ReportGridModel = reportGridModel;
            FilterView = filterView;
        }

        public string Title { get; set; }

        public Dictionary<string, object> Headers { get; set; }

        public string FilterView { get; set; }
        public JbReportFilter Filters { get; set; }

        public List<ReportExportElement> Exports { get; set; }

        public ReportGridModel ReportGridModel { get; set; }

    }
}
