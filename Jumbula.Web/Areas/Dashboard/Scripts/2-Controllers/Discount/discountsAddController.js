﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('discountsAddController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'discounts-add';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.MC.openBlade({ url: "Dashboard/Discount/DiscountsAddChooseTypeBlade", order: 3, menu: "SEASONS" });
    }]);
})();