﻿using Jumbula.Common.Enums;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Jumbula.Business.EmailBuilder
{
    public class AutoChargeSupportSummaryEmailBuilder : BaseEmailBuilder<AutoChargeSupportSummaryEmailParameterModel>
    {
        #region Constants

        private const string EmailTemplateFileName = "AutoChargeSupportSummary";
        private const string Subject = "Auto charge summary";

        #endregion

        #region Fields

        private readonly IRepository<TransactionActivity, long> _transactionActivityRepository;

        private List<long> _successInstallments;
        private List<Tuple<long, long>> _failedInstallments;
        private List<Tuple<long, long>> _systemFailedInstallments;
        private List<long> _notAttemptedInstallments;
        private List<long> _otherErrorInstallments;
  

        #endregion

        #region Constructors
        public AutoChargeSupportSummaryEmailBuilder(IRepository<TransactionActivity, long> transactionActivityRepository)
        {
            _transactionActivityRepository = transactionActivityRepository;
        }
        #endregion

        #region Properties
        public override EmailCategory Category => EmailCategory.AutoChargeSupportSummary;

        public override string TemplateName => EmailTemplateFileName;

        public override EmailCallerType CallerType => EmailCallerType.AutoCharge;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.Low;
        #endregion

        #region Public Methods
        public override void Initiate(AutoChargeSupportSummaryEmailParameterModel parameters)
        {
            _successInstallments = parameters.SuccessInstallments;
            _failedInstallments = parameters.FailedInstallments;
            _systemFailedInstallments = parameters.SystemFailedInstallments;
            _notAttemptedInstallments = parameters.NotAttemptedInstallments;
            _otherErrorInstallments = parameters.OtherErrorInstallments;
        }

        public override void InitiatePreview(AutoChargeSupportSummaryEmailParameterModel parameters)
        {
        }

        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            var transactionActivityIds = _failedInstallments.Select(s => s.Item2).ToList();

            transactionActivityIds.AddRange(_systemFailedInstallments.Select(s => s.Item2).ToList());

            transactionActivityIds = transactionActivityIds.Distinct().ToList();

            var installmentTransactionMessages = _transactionActivityRepository.GetList(t => transactionActivityIds.Contains(t.Id)).ToList().Select(s => new Tuple<long, string>(s.InstallmentId.HasValue? s.InstallmentId.Value: 0, s.PaymentDetail.TransactionMessage));

            return new AutoChargeSupportSummaryBodyModel
            {
                SuccessInstallments = _successInstallments,
                FailedInstallments = installmentTransactionMessages.Where(t => _failedInstallments.Select(s => s.Item1).ToList().Contains(t.Item1)).ToList(),
                SystemFailedInstallments = installmentTransactionMessages.Where(t => _systemFailedInstallments.Select(s => s.Item1).ToList().Contains(t.Item1)).ToList(),
                NotAttemptedInstallments = _notAttemptedInstallments,
                OtherErrorInstallments = _otherErrorInstallments,
            };
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            return new MailAddress(Common.Constants.Email.JumbulaNoReplyEmailAddress);
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return $"{EmailCallerType.AutoCharge}";
        }

        public override string GetReplyTo(string subCategory = null)
        {
            return Common.Constants.Email.JumbulaNoReplyEmailAddress;
        }

        public override string GetSubject(string subCategory = null)
        {
            return Subject;
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            return new List<MailAddress> { new MailAddress(Common.Constants.Email.JumbulaSupportEmailAddress) };
        }

        public override List<CommunicationHistory> GetHistories()
        {
            return new List<CommunicationHistory>();
        }
        #endregion
    }
}
