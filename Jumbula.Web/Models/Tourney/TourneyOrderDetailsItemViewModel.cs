﻿
namespace SportsClub.Models
{
    public class TourneyOrderDetailsItemViewModel
    {
        public string Section { get; set; }

        public string Schedule { get; set; }

        public string Byes { get; set; }

        public string Options { get; set; }

        public decimal Fees { get; set; }
    }
}