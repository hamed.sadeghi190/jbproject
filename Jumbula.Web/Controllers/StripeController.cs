﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Exceptions;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Stripe;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    [AllowAnonymous]
    public class StripeController : JbBaseController
    {
        public virtual ActionResult DirectPayment(JumbulaSubSystem actionCaller, bool backToDetail, string token, int creditId = 0)
        {

            try
            {
                StripePaymentModels model = new StripePaymentModels();
                switch (actionCaller)
                {
                    case JumbulaSubSystem.Order:
                        model = OrderPayment(token, 0);
                        break;
                    case JumbulaSubSystem.Installment:
                        model = InstallmentPayment(token);
                        break;
                    case JumbulaSubSystem.Invoice:
                        model = InvoicePayment(token, backToDetail);

                        break;
                    case JumbulaSubSystem.MakeaPayment:
                        model = TakeaPayment(token);
                        break;
                }

                return View(model);


            }
            catch (JumbulaCartSessionExpiredException ex)
            {
                LogHelper.LogException(ex);
            }
            catch (Exception e)
            {
                throw e;
            }
            if (actionCaller == JumbulaSubSystem.Order)
            {
                return RedirectToAction("Index", "Cart");
            }
            else
            {
                return RedirectToAction("MakePayment", "Cart", new { token = token, actionCaller = actionCaller });
            }

        }


        [HttpPost]
        public virtual JsonResult AchPaymentAsync(string token, string accountId, int? invoiceId)
        {
            JbPaymentService paymentService = new JbPaymentService();

            try
            {

                Cart cart = Ioc.CartBusiness.Get(this.GetCurrentUserId(), this.GetCurrentClubId());
                Dictionary<string, string> metaData;
                decimal payableAmount = 0;


                cart.Order.IsDraft = true;

                cart.Order.OrderStatus = OrderStatusCategories.initiated;

                cart.Order.CompleteDate = DateTime.UtcNow;
                var orderDb = Ioc.OrderBusiness;
                cart.Order.PaymentMethod = PaymentMethod.Echeck;
                orderDb.Update(cart.Order);

                JbStripeService stripeService = null;

                Club club;

                if (!invoiceId.HasValue)
                {
                    if (string.IsNullOrEmpty(cart.Order.ConfirmationId))
                    {
                        cart.Order.ConfirmationId = Ioc.OrderBusiness.GenerateConfirmationId();
                    }
                    if (cart == null || cart.Order == null || cart.Order.OrderItems == null || !cart.Order.RegularOrderItems.Any())
                    {
                        return Json(new JResult() { Status = false, Message = "Reload" }, JsonRequestBehavior.AllowGet);
                    }
                    if (cart.Order.OrderMode == OrderMode.Online)
                    {
                        Ioc.ProgramBusiness.CheckProgramCapacity(cart.Order.RegularOrderItems);
                        if (cart.Order.RegularOrderItems.Any(c => c.IsProgramFull))
                            return Json(new JResult() { Status = false, Message = "Reload" }, JsonRequestBehavior.AllowGet);
                    }
                    stripeService = new JbStripeService(cart.Order.Club.Domain, !cart.Order.IsLive);
                    club = cart.Order.Club;
                    metaData = paymentService.GenerateMetaData(JumbulaSubSystem.Order, cart.Order);
                    payableAmount = Ioc.OrderBusiness.CalculateOrderPayableAmount(cart.Order);
                }
                //add for invoice
                else
                {
                    club = Ioc.InvoiceBusiness.Get(invoiceId).Club;
                    var invoice = Ioc.InvoiceBusiness.Get(invoiceId);
                    var paymentdetailId = Ioc.PaymentDetailBusiness.GetByInvoiceId(invoice.Id).Id;
                    var transactions = Ioc.TransactionActivityBusiness.GetList().Where(t => t.PaymentDetailId == paymentdetailId).ToList();
                    stripeService = new JbStripeService(club.Domain, !cart.Order.IsLive);
                    metaData = paymentService.GenerateMetaData(JumbulaSubSystem.Invoice, null, null, transactions);
                    payableAmount = invoice.Amount;
                }




                string bankAccountToken = stripeService.GetPlaidBankAccountTokenAsync(token, accountId);


                var clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);


                var client = club.Client;
                var currency = club.Currency;
                if (club.PartnerId.HasValue && club.IsSchool)
                {
                    client = club.PartnerClub.Client;
                    currency = club.PartnerClub.Currency;
                }

                var secretKey = (clientPaymentMethod.IsCreditCardTestMode || !cart.Order.IsLive) ? WebConfigHelper.Stripe_SecretKey_Test : WebConfigHelper.Stripe_SecretKey_Live;
                var connectedId = (clientPaymentMethod.IsCreditCardTestMode || !cart.Order.IsLive) ? WebConfigHelper.Stripe_TestPayment_ConnectedId : clientPaymentMethod.StripeCustomerId;
                var connectedAccessToken = (clientPaymentMethod.IsCreditCardTestMode || !cart.Order.IsLive) ? WebConfigHelper.Stripe_TestPayment_AccessToken : clientPaymentMethod.StripeAccessToken;
                var chargeToken = bankAccountToken;


                StripeCharge stripeCharge = stripeService.Charge(client.Id, payableAmount, currency, chargeToken, metaData, cart.Order.ConfirmationId, connectedId, connectedAccessToken);
                var ipn = Newtonsoft.Json.JsonConvert.SerializeObject(stripeCharge);
                if (stripeCharge.Status.ToLower().Equals(StripeChargeStatus.succeeded.ToString()) || stripeCharge.Status.ToLower().Equals(StripeChargeStatus.pending.ToString()))
                {
                    if (!invoiceId.HasValue)
                    {
                        var paymentStatus = PaymentDetailStatus.PENDING;
                        var tranStatus = TransactionStatus.Pending;
                        if (stripeCharge.Status.ToLower().Equals(StripeChargeStatus.succeeded.ToString()))
                        {
                            paymentStatus = PaymentDetailStatus.COMPLETED;
                            tranStatus = TransactionStatus.Success;
                        }
                        var errorMsg = stripeService.UpdateOrderBasedOnPayment(this.ControllerContext, cart.Order, stripeCharge, ipn, null, paymentStatus, tranStatus, PaymentMethod.Echeck);
                        if (!string.IsNullOrEmpty(errorMsg))
                        {
                            var paymentDetail = new PaymentDetail(this.GetCurrentUserName(), PaymentDetailStatus.ERROR, bankAccountToken, PaymentMethod.Echeck, currency, ipn, errorMsg, string.Empty, JbUserService.GetCurrentUserRoleType());
                            orderDb.SetPaymentTransaction(cart.Order, paymentDetail, TransactionStatus.Failure);
                            orderDb.Update(cart.Order);
                            return Json(new JResult() { Status = true, Message = Url.Action("PaymentGeneralError", "Cart", new { subject = Constants.M_Payment_Failed, msg = errorMsg, status = false, cid = cart.Order.ConfirmationId }) }, JsonRequestBehavior.AllowGet);
                        }

                        return Json(new JResult() { Status = true, Message = Url.Action("FinalizeConfirmation", "Cart", new { cid = cart.Order.ConfirmationId, paymentMethod = PaymentMethod.Echeck, offlinePayNow = false, PayableAmount = payableAmount }) }, JsonRequestBehavior.AllowGet);
                    }
                    //Add for invoice echeck
                    else
                    {
                        var paymentStatus = PaymentDetailStatus.PENDING;
                        var tranStatus = TransactionStatus.Pending;
                        if (stripeCharge.Status.ToLower().Equals(StripeChargeStatus.succeeded.ToString()))
                        {
                            paymentStatus = PaymentDetailStatus.COMPLETED;
                            tranStatus = TransactionStatus.Success;
                        }
                        var paymentDetailId = Ioc.PaymentDetailBusiness.GetByInvoiceId(invoiceId.Value).Id;
                        var transactions = Ioc.TransactionActivityBusiness.GetList().Where(t => t.PaymentDetailId == paymentDetailId).ToList();
                        var errorMsg = stripeService.UpdateOrderBasedOnPayment(this.ControllerContext, cart.Order, stripeCharge, ipn, null, paymentStatus, tranStatus, PaymentMethod.Echeck, transactions);
                        if (!string.IsNullOrEmpty(errorMsg))
                        {
                            var paymentDetail = new PaymentDetail(this.GetCurrentUserName(), PaymentDetailStatus.ERROR, bankAccountToken, PaymentMethod.Echeck, currency, ipn, errorMsg, string.Empty, JbUserService.GetCurrentUserRoleType());
                            foreach (var transaction in transactions)
                            {
                                transaction.TransactionStatus = TransactionStatus.Failure;
                                Ioc.TransactionActivityBusiness.Update(transaction);
                            }

                            return Json(new JResult() { Status = true, Message = Url.Action("PaymentGeneralError", "Cart", new { subject = Constants.M_Payment_Failed, msg = errorMsg, status = false, cid = cart.Order.ConfirmationId }) }, JsonRequestBehavior.AllowGet);
                        }

                        return Json(new JResult() { Status = true, Message = Url.Action("DisplaySucceedOperation", "Cart", new { paymentMethod = PaymentMethod.Echeck, offlinePayNow = false, PayableAmount = payableAmount }) }, JsonRequestBehavior.AllowGet);
                    }
                    //End code for invoice
                }
            }
            catch (Exception ex)
            {
                return Json(new JResult() { Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new JResult() { Status = false, Message = "Reload" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult DirectPayment(StripePaymentModels model)
        {
            var userId = 0;
            var creditCards = Ioc.UserCreditCardBusiness;
            JbPaymentService paymentService = new JbPaymentService();

            string errorMsg = string.Empty;
            var ipn = model.CreateTokenResponse;
            var username = User.Identity.IsCurrentUserExist() ? this.GetCurrentUserName() : string.Empty;
            List<OrderInstallment> installments = null;
            List<TransactionActivity> transactions = null;
            var confirmationIds = new List<string>();
            var isOrderTestMode = false;
            PaymentGateway _paymentGateway;


            CurrencyCodes currency = CurrencyCodes.USD;
            var stripeService = new JbStripeService(string.Empty, model.IsTestMode);



            try
            {
                Club club = null;
                Dictionary<string, string> metadata = null;
                decimal payableAmount = model.PayableAmount;

                switch (model.ActionCaller)
                {

                    case JumbulaSubSystem.Installment:
                        {
                            installments = Ioc.InstallmentBusiness.GetListByToken(model.ConfirmationId).Where(i => !i.IsDeleted).ToList();
                            payableAmount = installments.Sum(c => c.Balance);
                            metadata = paymentService.GenerateMetaData(JumbulaSubSystem.Installment, null, installments);
                            club = Ioc.ClubBusiness.Get(installments.First().OrderItem.Order.ClubId);
                            username = (!string.IsNullOrEmpty(username)) ? username : installments.First().OrderItem.Order.User.UserName;
                            userId = installments.First().OrderItem.Order.UserId;
                            isOrderTestMode = !installments.First().OrderItem.Order.IsLive;
                            break;
                        }
                    case JumbulaSubSystem.Invoice:
                        {

                            transactions = Ioc.TransactionActivityBusiness.GetInvoicedByToken(model.ConfirmationId).ToList();
                            payableAmount = transactions.Sum(c => c.Amount);
                            metadata = paymentService.GenerateMetaData(JumbulaSubSystem.Invoice, null, null, transactions);
                            club = Ioc.ClubBusiness.Get(transactions.First().ClubId);
                            username = (!string.IsNullOrEmpty(username)) ? username : transactions.First().Order.User.UserName;
                            userId = transactions.First().Order.UserId;
                            isOrderTestMode = !(transactions.First().OrderId.HasValue ? transactions.First().Order.IsLive : transactions.First().OrderItem.Order.IsLive);
                            break;
                        }
                    case JumbulaSubSystem.MakeaPayment:
                        {
                            transactions = Ioc.TransactionActivityBusiness.GetDraftByToken(model.ConfirmationId).ToList();
                            payableAmount = transactions.Sum(c => c.Amount);
                            metadata = paymentService.GenerateMetaData(JumbulaSubSystem.MakeaPayment, null, null, transactions);
                            club = Ioc.ClubBusiness.Get(transactions.First().ClubId);
                            username = (!string.IsNullOrEmpty(username)) ? username : transactions.First().Order.User.UserName;
                            userId = transactions.First().Order.UserId;
                            isOrderTestMode = !(transactions.First().OrderId.HasValue ? transactions.First().Order.IsLive : transactions.First().OrderItem.Order.IsLive);
                            break;
                        }
                }

                if (club != null)
                {
                    var clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                    _paymentGateway = clientPaymentMethod.DefaultPaymentGateway;

                    var clientId = club.ClientId;
                    currency = club.Currency;
                    if (club.PartnerId.HasValue && club.IsSchool)
                    {
                        clientId = club.PartnerClub.ClientId;
                        currency = club.PartnerClub.Currency;
                    }
                    var connectedId = (clientPaymentMethod.IsCreditCardTestMode || isOrderTestMode) ? WebConfigHelper.Stripe_TestPayment_ConnectedId : clientPaymentMethod.StripeCustomerId;
                    var connectedAccessToken = (clientPaymentMethod.IsCreditCardTestMode || isOrderTestMode) ? WebConfigHelper.Stripe_TestPayment_AccessToken : clientPaymentMethod.StripeAccessToken;
                    var secretKey = (clientPaymentMethod.IsCreditCardTestMode || isOrderTestMode) ? WebConfigHelper.Stripe_SecretKey_Test : WebConfigHelper.Stripe_SecretKey_Live;

                    var userCustomerId = string.Empty;
                    var userCustomerResponse = string.Empty;
                    var token = model.Token;
                    StripeCharge stripeCharge;
                    if (model.ActionCaller == JumbulaSubSystem.Invoice)
                    {
                        var transactionsGroup = transactions.GroupBy(t => t.OrderId);
                        foreach (var tran in transactionsGroup)
                        {
                            confirmationIds.Add(tran.First().Order.ConfirmationId);
                        }
                        model.ConfirmatiocIds = confirmationIds;
                        stripeCharge = stripeService.Charge(clientId.Value, payableAmount, currency, token, metadata, model.StrConfirmatiocIds, connectedId, connectedAccessToken);
                    }
                    else
                    {
                        stripeCharge = stripeService.Charge(clientId.Value, payableAmount, currency, token, metadata, model.ConfirmationId, connectedId, connectedAccessToken);
                    }

                    ipn = Newtonsoft.Json.JsonConvert.SerializeObject(stripeCharge);

                    if (stripeCharge.Status.ToLower().Equals(StripeChargeStatus.succeeded.ToString()) && stripeCharge.Paid)
                    {
                        UserCreditCard creditCard = null;
                        int? creditcardId = null;
                        var address = new PostalAddress();
                        var modelCard = new PaymentModels();
                        if (model.PaymentMethod == PaymentMethod.Card) // No select express payment methods available
                        {
                            //var brand = GetBrandCard(model.CardNumber);
                            //Utilities.GetGEO(model.StrAddress, ref address);
                            modelCard = new PaymentModels()
                            {
                                CardNumber = model.CardNumber,
                                Month = model.Month,
                                Year = (model.Year2Char + 2000).ToString(),
                                CardHolderName = model.CardHolderName,
                                Address = model.Address,
                                //StrAddress = model.StrAddress,
                                CVV = model.CVV,
                                UserId = model.UserId,
                            };
                        }
                        try
                        {
                            if (userId > 0 && creditCard == null)
                            {

                                PostalAddress postalAddress = new PostalAddress();
                                //Utilities.GetGEO(model.StrAddress, ref postalAddress);

                                creditCard = paymentService.CreateCreditCard(modelCard, userId, postalAddress, string.Empty, stripeCharge.Id, userCustomerResponse, false, _paymentGateway, false, false);

                            }
                            creditcardId = creditCard.Id;
                        }
                        catch
                        { }
                        switch (model.ActionCaller)
                        {

                            case JumbulaSubSystem.Installment:
                                var date = DateTime.UtcNow;
                                errorMsg = stripeService.UpdateInstallmentBasedOnPayment(installments, stripeCharge, ipn, creditCard.Id, date, JbUserService.GetCurrentUserRoleType());
                                if (string.IsNullOrEmpty(errorMsg))
                                {
                                    EmailService.Get(this.ControllerContext).SendInstallmentPaymentConfirmation(installments.First().Token, date, false);
                                    return RedirectToAction("DisplaySucceedOperation", "Cart");
                                }
                                break;
                            case JumbulaSubSystem.Invoice:
                                errorMsg = SucceedInvoicePayment(transactions, stripeCharge, ipn, creditCard.Id, model.BackToDetail);
                                var invoiceId = transactions.First().PaymentDetail.InvoiceId;
                                if (string.IsNullOrEmpty(errorMsg))
                                {
                                    if (model.BackToDetail == true)
                                    {
                                        var urlToRedirect = string.Format("{0}/Dashboard#/Invoicing/Invoice/Details/{1}", Request.Url.GetLeftPart(UriPartial.Authority), invoiceId);
                                        return Redirect(urlToRedirect);
                                    }
                                    else
                                    {
                                        return RedirectToAction("DisplaySucceedOperation", "Cart");
                                    }

                                }

                                break;
                            case JumbulaSubSystem.MakeaPayment:

                                if (string.IsNullOrEmpty(errorMsg))
                                {
                                    return RedirectToAction("MakePaymentReturnStatus", "Order", new { token = model.ConfirmationId, message = errorMsg, status = string.IsNullOrEmpty(errorMsg) });
                                }
                                break;
                        }
                    }
                    else
                    {
                        errorMsg = stripeCharge.FailureMessage;
                    }
                }

            }
            catch (StripeException stripeException)
            {
                errorMsg = stripeException.Message;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }
            var orderdb = Ioc.OrderBusiness;
            var paymentDetail = new PaymentDetail(username, PaymentDetailStatus.ERROR, model.Token, PaymentMethod.Card, currency, ipn, errorMsg, string.Empty, JbUserService.GetCurrentUserRoleType());
            switch (model.ActionCaller)
            {

                case JumbulaSubSystem.Installment:
                    foreach (var item in installments)
                    {

                        orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, item, DateTime.UtcNow);
                        Ioc.InstallmentBusiness.Update(item);
                    }
                    break;
                case JumbulaSubSystem.Invoice:

                    orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, null, DateTime.UtcNow, transactions.Sum(c => c.Amount), transactions.First().OrderItem, null, "", model.ConfirmationId, "", TransactionCategory.Invoice);
                    Ioc.OrderItemBusiness.Update(transactions.First().OrderItem);
                    break;
                case JumbulaSubSystem.MakeaPayment:

                    orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, null, null, transactions.Sum(c => c.Amount), transactions.First().OrderItem, null, "", "", "", TransactionCategory.TakePayment);
                    Ioc.OrderItemBusiness.Update(transactions.First().OrderItem);
                    return RedirectToAction("MakePaymentReturnStatus", "Order", new { token = model.ConfirmationId, message = errorMsg, status = false });
            }

            return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Payment_Failed, msg = errorMsg, status = false, cid = model.ConfirmationId });


        }
        private string SucceedInvoicePayment(List<TransactionActivity> transactions, StripeCharge stripeCharge, string ipn, int creditcardId, bool? backToDetail = false)
        {
            try
            {
                var club = transactions.First().Club;

                var tranDb = Ioc.TransactionActivityBusiness;
                if (transactions == null || transactions.Count == 0)
                {
                    return "Invalid Token";
                }
                var paidAmount = transactions.Sum(c => c.Amount);
                var orderItem = Ioc.OrderItemBusiness.GetItem(transactions.FirstOrDefault().OrderItemId.Value);
                var invoiceCategory = transactions.Select(t => t.TransactionCategory).First();

                if (Ioc.TransactionActivityBusiness.IsPayKeyExist(stripeCharge.Id))
                {
                    return string.Empty;
                }

                PaymentDetail paymentDetail = new PaymentDetail(stripeCharge.ReceiptEmail, PaymentDetailStatus.COMPLETED, stripeCharge.Id, PaymentMethod.Card, transactions.FirstOrDefault().Club.Currency, ipn, string.Empty, stripeCharge.BalanceTransactionId, JbUserService.GetCurrentUserRoleType(), creditcardId);

                var date = DateTime.UtcNow;
                if (invoiceCategory != TransactionCategory.Invoice)
                {
                    foreach (var tran in transactions)
                    {
                        tran.PaymentDetail.Currency = paymentDetail.Currency;
                        tran.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                        tran.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                        tran.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                        tran.PaymentDetail.Status = paymentDetail.Status;
                        tran.PaymentDetail.PayKey = paymentDetail.PayKey;
                        tran.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                        tran.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                        tran.PaymentDetail.CreditCardId = paymentDetail.CreditCardId;
                        tran.TransactionStatus = TransactionStatus.Success;
                        tran.TransactionDate = date;
                        tranDb.Update(tran);
                        if (tran.InstallmentId.HasValue && tran.InstallmentId.Value > 0)
                        {
                            var installment = Ioc.InstallmentBusiness.Get(tran.InstallmentId.Value);
                            installment.PaidAmount = (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0) + tran.Amount;
                            installment.PaidDate = date;
                            installment.Status = OrderStatusCategories.completed;
                            Ioc.InstallmentBusiness.Update(installment);
                        }
                    }
                    var totalPaidAmount = transactions.Sum(c => c.Amount);
                    orderItem.PaidAmount += totalPaidAmount;
                    Ioc.OrderItemBusiness.Update(orderItem);
                    Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.MadePayment, string.Format(Constants.Order_MakePayment, totalPaidAmount, transactions.FirstOrDefault().Note), orderItem.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, orderItem.Id);
                    EmailService.Get(this.ControllerContext).SendReceivedPaymentEmail(Ioc.OrderItemBusiness.GetItem(transactions.FirstOrDefault().OrderItemId.Value), totalPaidAmount, DateTime.UtcNow, paymentDetail);

                    return string.Empty;

                }
                else
                {
                    var invoice = transactions.First().PaymentDetail.invoice;
                    invoice.PaidAmount = paidAmount;
                    invoice.Status = InvoiceStatus.Paid;
                    Ioc.InvoiceBusiness.Update(invoice);
                    foreach (var transaction in transactions)
                    {
                        transaction.PaymentDetail.Currency = paymentDetail.Currency;
                        transaction.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                        transaction.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                        transaction.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                        transaction.PaymentDetail.Status = paymentDetail.Status;
                        transaction.PaymentDetail.PayKey = paymentDetail.PayKey;
                        transaction.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                        transaction.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                        transaction.PaymentDetail.CreditCardId = paymentDetail.CreditCardId;
                        transaction.TransactionStatus = TransactionStatus.Success;
                        transaction.TransactionDate = date;
                        tranDb.Update(transaction);

                        if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                        {
                            var installment = Ioc.InstallmentBusiness.Get(transaction.InstallmentId.Value);
                            installment.PaidAmount = (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0) + transaction.Amount;
                            installment.PaidDate = date;
                            installment.Status = OrderStatusCategories.completed;
                            Ioc.InstallmentBusiness.Update(installment);

                        }

                    }
                    foreach (var transaction in transactions)
                    {
                        if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                        {
                            var orderItemInInvoice = transaction.OrderItem;
                            var instalment = transaction.Installment;
                            var InstalmentordeItamBalance = transaction.Amount;
                            orderItemInInvoice.PaidAmount += transaction.Amount;
                            Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(Constants.Order_InvoiceHistory, InstalmentordeItamBalance, transaction.Note), orderItemInInvoice.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, orderItemInInvoice.Id);

                        }
                        else
                        {
                            var orderItemInInvoice = transaction.OrderItem;
                            var ordeItamBalance = transaction.Amount;
                            orderItemInInvoice.PaidAmount += transaction.Amount;
                            Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(Constants.Order_InvoiceHistory, ordeItamBalance, transaction.Note), orderItemInInvoice.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, orderItemInInvoice.Id);
                        }

                    }
                    var invoiceId = invoice.Id;
                    var userName = invoice.User.UserName;
                    var today = DateTime.UtcNow;
                    var clubTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, today);
                    if (backToDetail == true)
                    {
                        var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(Constants.Invoice_TakePaymentWithCreditcard, userName, CurrencyHelper.FormatCurrencyWithPenny(paidAmount, club.Currency)) };
                        Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
                    }
                    else
                    {
                        var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(Constants.Invoice_Paided, userName, CurrencyHelper.FormatCurrencyWithPenny(paidAmount, club.Currency)) };
                        Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
                    }


                    var totalPaidAmount = transactions.Sum(c => c.Amount);

                    EmailService.Get(this.ControllerContext).SendReceivedFamilyInvoicePaymentEmail(transactions, totalPaidAmount, DateTime.UtcNow, paymentDetail, invoice);

                    return string.Empty;
                }

            }


            catch (Exception e)
            {
                LogHelper.LogException(e);
                return e.Message;

            }

        }

        public virtual ActionResult StripeConnectReturnUrl()
        {
            if (Request["state"] == null)
            {
                PageNotFound();
            }
            int state = 0;

            if (!int.TryParse(Request["state"].ToString(), out state))
            {
                PageNotFound();
            }

            var club = Ioc.ClubBusiness.Get(state);
            if (club == null)
            {
                PageNotFound();
            }
            var model = new PaymentSettingsViewModel();

            var msg = "1";
            try
            {
                /// Check the user is the owner of the club
                ///Todo : Check state

                if (Request["code"] != null)
                {
                    var code = Request["code"];
                    var ss = new JbStripeService(club.Domain, false);

                    var res = ss.StripeConnect(code);
                    var stripeReturn = JsonConvert.DeserializeObject<SportsClub.Models.Payment.StripeOAuthToken>(res);
                    if (string.IsNullOrEmpty(stripeReturn.Error) && string.IsNullOrEmpty(stripeReturn.ErrorDescription))
                    {
                        club.Client.PaymentMethods.EnableStripePayment = true;
                        club.Client.PaymentMethods.IsCreditCardTestMode = !(stripeReturn.LiveMode);

                        club.Client.PaymentMethods.StripeCustomerId = stripeReturn.StripeUserId;
                        club.Client.PaymentMethods.StripeAccessToken = stripeReturn.AccessToken;
                        club.Client.PaymentMethods.StripeResponse = res;

                        Ioc.ClientBusiness.Update(club.Client);

                    }
                    else
                    {
                        msg = stripeReturn.ErrorDescription;
                    }
                }
                else
                {
                    msg = Request["error_description"] ?? Constants.M_E_CreateEdit;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            var urlFirstPart = Request.Url.Host;
            var urlToRedirect = string.Format("https://{0}.{1}/Dashboard#/Settings/Payment/{2}/", club.Domain, urlFirstPart, msg);

            if (urlFirstPart.Contains("cantabile.azadeh.local"))
            {
                urlToRedirect = "https://cantabile.azadeh.local/Dashboard#/Settings/Payment/1/";
            }

            return RedirectPermanent(urlToRedirect);
        }



        private StripePaymentModels InvoicePayment(string token, bool backToDetail)
        {
            StripePaymentModels model = null;
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    var isOrderTestMode = true;
                    var transactions = Ioc.TransactionActivityBusiness.GetInvoicedByToken(token).ToList();

                    var firstTransaction = transactions.First();
                    if (firstTransaction.OrderId.HasValue)
                    {
                        isOrderTestMode = !firstTransaction.Order.IsLive;
                    }
                    else if (firstTransaction.OrderItemId.HasValue)
                    {
                        isOrderTestMode = !firstTransaction.OrderItem.Order.IsLive;
                    }

                    if (transactions != null && transactions.Count > 0)
                    {
                        var club = Ioc.ClubBusiness.Get(transactions.FirstOrDefault().ClubId);
                        var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                        model = new StripePaymentModels()
                        {
                            Currency = club.Currency,
                            ConfirmationId = token,
                            PayableAmount = transactions.Sum(c => c.Amount),
                            IsTestMode = isOrderTestMode || clinetPaymentMethod.IsCreditCardTestMode,
                            ActionCaller = JumbulaSubSystem.Installment,
                            BackToDetail = backToDetail

                        };


                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return model;
        }
        private StripePaymentModels InstallmentPayment(string token)
        {
            StripePaymentModels model = null;
            try
            {
                if (!string.IsNullOrEmpty(token))
                {

                    var installment = Ioc.InstallmentBusiness.GetListByToken(token).ToList();

                    if (installment != null && installment.Count > 0)
                    {
                        var club = Ioc.ClubBusiness.Get(installment.FirstOrDefault().OrderItem.Order.ClubId);
                        var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);
                        model = new StripePaymentModels()
                        {
                            Currency = club.Currency,
                            ConfirmationId = token,
                            PayableAmount = installment.Sum(c => c.Balance),
                            IsTestMode = !installment.First().OrderItem.Order.IsLive || clinetPaymentMethod.IsCreditCardTestMode,
                            ActionCaller = JumbulaSubSystem.Installment,

                        };


                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return model;
        }

        //private ConfirmationViewModel Confirmation(int creditId)
        //{
        //    var cartDb = Ioc.CartBusiness;
        //    var cart = cartDb.Get(this.GetCurrentUserId());
        //    var model = new ConfirmationViewModel(cart)
        //    {
        //        ClubName = cart.Order.Club.Name,
        //        ConfirmationId = cart.Order.ConfirmationId,
        //        UserId = cart.Order.UserId,
        //        Currency = cart.Order.Club.Currency,
        //        OrderAmount = cart.Order.OrderAmount,
        //        PayableAmount = Ioc.OrderBusiness.CalculateOrderPayableAmount(cart.Order),
        //        OrderInstallments = Ioc.InstallmentBusiness.MergeOrderItemInstallments(cart.Order),
        //        CreditId = creditId,
        //    };
        //    return model;
        //}
        private StripePaymentModels OrderPayment(string token, int creditId)
        {
            StripePaymentModels model = null;
            try
            {
                if (this.IsUserAuthenticated())
                {
                    if (User.Identity.IsCurrentUserExist())
                    {
                        var order = Ioc.OrderBusiness.GetOrdersByConfirmationID(token);
                        var payableAmount = Ioc.OrderBusiness.CalculateOrderPayableAmount(order);
                        var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(order.ClubId);
                        model = new StripePaymentModels()
                        {
                            Currency = order.Club.Currency,
                            ConfirmationId = order.ConfirmationId,
                            UserId = order.UserId,
                            PayableAmount = payableAmount,
                            IsTestMode = (!order.IsLive || clinetPaymentMethod.IsCreditCardTestMode),
                            ActionCaller = JumbulaSubSystem.Order,
                            CreditId = creditId,


                        };

                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return model;
        }
        private StripePaymentModels TakeaPayment(string token)
        {
            StripePaymentModels model = null;


            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    var isOrderTestMode = true;
                    var transactionactivities = Ioc.TransactionActivityBusiness.GetDraftByToken(token).ToList();

                    var firstTransaction = transactionactivities.First();
                    if (firstTransaction.OrderId.HasValue)
                    {
                        isOrderTestMode = !firstTransaction.Order.IsLive;
                    }
                    else if (firstTransaction.OrderItemId.HasValue)
                    {
                        isOrderTestMode = !firstTransaction.OrderItem.Order.IsLive;
                    }

                    if (transactionactivities != null && transactionactivities.Count > 0)
                    {
                        var club = Ioc.ClubBusiness.Get(transactionactivities.First().ClubId);
                        var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                        model = new StripePaymentModels()
                        {
                            Currency = club.Currency,
                            ConfirmationId = token,
                            PayableAmount = transactionactivities.Sum(c => c.Amount),
                            IsTestMode = isOrderTestMode || clinetPaymentMethod.IsCreditCardTestMode,
                            ActionCaller = JumbulaSubSystem.MakeaPayment
                        };
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return model;
        }


    }


}