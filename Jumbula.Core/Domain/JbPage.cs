﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Jumbula.Core.Domain
{
    [JsonConverter(typeof(JbPageConverter))]
    public class JbPage : BaseEntity
    {
        public static readonly string DefaultJbPageButtonBackgroundColor = "#5FAB1E";
        public static readonly string DefaultJbPageButtonColor = "#fff";
        public static readonly string DefaultJbPageButtonSize = "large";
        public static readonly string DefaultJbPageButtonAlignment = "center";
        public static readonly string DefaultJbPageCalenderProgramTextColor = "#000";
        public static readonly string DefaultJbPageCalenderProgramBackgroundColor = "rgb(218,242,204)";


        public JbPage()
        {
            PageMode = JbPMode.Design;
            Header = new JbPHeader { Menu = new List<JbPMenuItem>() };

            Elements = new List<JbPBaseElement>();
            Footer = new JbPFooter();
            Address = new PostalAddress();
            deletedImages = new List<JbPImage>();
        }
        [NotMapped]
        public List<JbPImage> deletedImages { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }

        [JsonIgnore]
        public string JsonBody { get; set; }

        //[JsonIgnore]
        public DateTime CreateDate { get; set; }

        public SaveType SaveType { get; set; }

        public int ClubId { get; set; }

        [ForeignKey("ClubId")]
        [JsonIgnore]
        public virtual Club Club { get; set; }

        #region NotMapped Property
        [NotMapped]
        public JbPMode PageMode { get; set; }
        [NotMapped]
        public JbPHeader Header { get; set; }
        //[NotMapped]
        //public JbPCover Cover { get; set; }

        [NotMapped]
        public List<JbPBaseElement> Elements { get; set; }
        //[NotMapped]
        //public List<JbPTab> Tabs { get; set; }

        [NotMapped]
        public JbPFooter Footer { get; set; }
        [NotMapped]
        public JbPSetting Setting { get; set; }

        [NotMapped]
        public PostalAddress Address { get; set; }

        #endregion

    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPHeader : JbPBaseElement
    {
        public JbPHeader()
        {
            Menu = new List<JbPMenuItem>();
        }
        public string LogoUrl { get; set; }
        public bool ShowLogo { get; set; }
        public string LogoRedirecturl { get; set; }
        //public string BgColor { get; set; }
        //public string Color { get; set; }
        public List<JbPMenuItem> Menu { get; set; }

        public string TitleColor { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPMenuItem : JbPBaseElement
    {
        public string Link { get; set; }
        public string Target { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPFilter : JbPBaseElement
    {
        public JbPFilter()
        {
            Grades = new List<JbPFilterGradeItem>();
            Setting = new JbPGridSetting();
        }

        public JbPFilter(bool initializeElements)
        {
            Grades = new List<JbPFilterGradeItem>();
            Setting = new JbPGridSetting();

            if (initializeElements)
            {
                HasAddress = false;
                HasGrade = false;
                HasLocationName = false;
                HasWeekDay = false;
                HasCategory = false;
                HasStatus = false;
                HasCity = false;
                Grades = GetGradeItems();
                Setting = new JbPGridSetting()
                {
                    TextColor = "#fff",
                    BackgroundColor = "#16a085",
                    ActiveBackgroundColor = "#105245",
                    SubBackgroundColor = "#09836b",

                };
            }
        }

        public bool HasLocationName { get; set; }
        public bool HasWeekDay { get; set; }
        public bool HasCategory { get; set; }
        public bool HasStatus { get; set; }
        public bool HasGrade { get; set; }
        public bool HasAddress { get; set; }
        public bool HasCity { get; set; }
        public JbPGridSetting Setting { get; set; }
        public List<JbPFilterGradeItem> Grades { get; set; }

        private List<JbPFilterGradeItem> GetGradeItems()
        {
            var grades = new List<JbPFilterGradeItem>();
            grades.Add(new JbPFilterGradeItem { Grade = null, Title = "All" });
            grades.AddRange(Enum.GetValues(typeof(SchoolGradeType))
               .Cast<SchoolGradeType>()
               .Select(d => new JbPFilterGradeItem { Grade = d, IsEnabled = false })
               .ToList());

            int count = 1;
            foreach (var item in grades)
            {
                if (count < 10 && item.Grade != SchoolGradeType.PreSchool && item.Grade != SchoolGradeType.NotInSchool)
                {
                    item.IsEnabled = true;
                }
                count++;
            }

            return grades;
        }


    }

    public class JbPFilterGradeItem : JbPBaseElement
    {
        private string _title;
        public override string Title
        {
            get
            {
                return string.IsNullOrWhiteSpace(_title) ? Grade.ToDescription() : _title;
            }
            set
            {
                _title = value;
            }
        }

        public SchoolGradeType? Grade { get; set; }

        public bool IsEnabled { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPCover : JbPBaseElement
    {
        public string Subtitle { get; set; }
        public JbPCoverBGMode BackgroundMode { get; set; }
        public string Background { get; set; }
        public string Paragraph { get; set; }
        public bool Visible { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPAppearance : JbPBaseElement
    {
        public JbPAppearance()
        {

            EnabledAppearance = false;
            TabTextColor = "#01a3f1";
            TabHoverTextColor = "#333";
            TabBackgroundColor = "#fff";
            TabBorderColor = "#01a3f1";
            TabSelectedBackgroundColor = "#01a3f1";
            TabSelectedTextColor = "#fff";
            TabSelectedBorderColor = "#fff";
            TabSelectedHoverTextColor = "#eee";

        }
        public bool EnabledAppearance { get; set; }
        public string TabTextColor { get; set; }
        public string TabHoverTextColor { get; set; }
        public string TabBackgroundColor { get; set; }
        public string TabBorderColor { get; set; }
        public string TabSelectedBackgroundColor { get; set; }
        public string TabSelectedHoverTextColor { get; set; }
        public string TabSelectedTextColor { get; set; }
        public string TabSelectedBorderColor { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPFooter : JbPBaseElement
    {
        public string Email { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public bool Visible { get; set; }
        public string BackgroundColor { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPContact : JbPBaseElement
    {
        /*public string Email { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }*/
        public string Contact { get; set; }
        public string ContactBackgroundColor { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPSection : JbPBaseElement
    {
        public JbPSection()
        {
            TypeTitle = "Tile";
            Columns = new List<JbPSectionColumn>();
        }
        public JbPColumnCount ColumnCount { get; set; }
        public List<JbPSectionColumn> Columns { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPGrid : JbPBaseElement
    {
        public JbPGrid()
        {
            TypeTitle = "List";
            Width = "full-width";
            Align = "align-center";
            ProgramRegBtnLinkTo = JbPageProgramLinkType.ClassPage;
            RegLinks = new List<JbPRegLink>();
            BeforeAfterRegLinks = new List<JbPBeforeAfterCareRegLinks>();
        }

        public bool ShowRestrictions { get; set; }
        public bool ShowLocations { get; set; }
        public bool ShowStartEndDates { get; set; }
        public bool ShowDaysTimes { get; set; }
        public bool ShowRegistrationPeriod { get; set; }
        public bool ShowTuitionsPrices { get; set; }
        public bool ShowAdditionalNotes { get; set; }
        public JbPDayTimeFormats DaysTimesFormat { get; set; }
        public bool HideDayTimesDate { get; set; }
        public bool ShowCapacityLeft { get; set; }
        public string Align { get; set; }
        public string Width { get; set; }
        [DefaultValue(JbPageProgramLinkType.ClassPage)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
        public JbPageProgramLinkType ProgramRegBtnLinkTo { get; set; }
        public List<JbPRegLink> RegLinks { get; set; }
        public List<JbPBeforeAfterCareRegLinks> BeforeAfterRegLinks { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPSeasonCalendar : JbPBaseElement
    {
        public JbPSeasonCalendar()
        {
            TypeTitle = "Calendar";
            IncludeCalendar = true;
            IncludeCamp = true;
            IncludeChessTournament = true;
            IncludeClasses = true;
            IncludeSeminarTour = true;
            ShowGrade = false;
            ShowGradeInformation = false;
        }

        public long SeasonId { get; set; }
        public bool IncludeClasses { get; set; }
        public bool IncludeCamp { get; set; }
        public bool IncludeSeminarTour { get; set; }
        public bool IncludeCalendar { get; set; }
        public bool IncludeChessTournament { get; set; }
        public bool ShowGrade { get; set; }
        public bool ShowGradeInformation { get; set; }

        public SchoolGradeType SchoolGradeType { get; set; }
        public long LocationId { get; set; }
        public string CityName { get; set; }
        public long CampId { get; set; }

    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPSeasonGrid : JbPBaseElement
    {
        public JbPSeasonGrid()
        {
            TypeTitle = "Grid";
            IncludeCalendar = false;
            IncludeCamp = true;
            IncludeChessTournament = false;
            IncludeClasses = true;
            HideFuture = false;
            HideExpired = false;
            HideFrozen = false;
            IncludeSeminarTour = false;
            IncludeBeforeAfter = false;
            ShowGradeInformation = false;
            Filter = new JbPFilter();
            ShowRoomColumn = true;
            ShowInstructorsColumn = false;
            ShowProviderColumn = false;
            ShowCapacityColumn = false;
            ShowLocationColumn = false;
            ShowPriceColumn = true;
            ShowStatusColumn = true;
            ShowRegistrationColumn = false;
            GridLayoutType = GridLayoutType.Weekly;
            EnabledFilterOptions = true;
            SeasonRegBtnTitle =  Constants.DefaultRegistrationButtonTitle;
            SeasonRegBtnBackgroundColor = JbPage.DefaultJbPageButtonBackgroundColor;
            SeasonRegBtnLabelColor = JbPage.DefaultJbPageButtonColor;
            SeasonRegBtnSize = JbPage.DefaultJbPageButtonSize;
            SeasonRegBtnAlignment = JbPage.DefaultJbPageButtonAlignment;
            ProgramRegBtnTitle = Constants.DefaultRegistrationButtonTitle;
            ProgramRegBtnLinkTo = JbPageProgramLinkType.RegisterationPage;
            ProgramRegBtnBackgroundColor = JbPage.DefaultJbPageButtonBackgroundColor;
            ProgramRegBtnLabelColor = JbPage.DefaultJbPageButtonColor;
            CalenderProgramBackgroundColor = JbPage.DefaultJbPageCalenderProgramBackgroundColor;
            CalenderProgramTextColor = JbPage.DefaultJbPageCalenderProgramTextColor;
            ShowRegistrationBtnTop = false;
            ShowRegistrationBtnBottom = false;
            ShowRestrictionsColumn = false;
            ShowRegistrationDatesColumn = false;
            ShowDaysTimesColumn = false;
            ShowDatesColumn = false;
            ShowProgramColumn = true;
            ShowGradeAgeColumn = true;
            ShowCapacityLeftColumn = false;
        }

        public long SeasonId { get; set; }
        public bool IncludeClasses { get; set; }
        public bool HideFrozen { get; set; }
        public bool HideExpired { get; set; }
        public bool HideFuture { get; set; }
        public bool IncludeCamp { get; set; }
        public bool IncludeSeminarTour { get; set; }
        public bool IncludeCalendar { get; set; }
        public bool IncludeChessTournament { get; set; }

        public bool IncludeBeforeAfter { get; set; }
        public bool EnabledPopup { get; set; }
        public bool EnabledRegistrationPopup { get; set; }
        public bool EnabledFilterOptions { get; set; }
        public bool ShowGradeInformation { get; set; }
        public SchoolGradeType SchoolGradeType { get; set; }
        public string SelectedAddress { get; set; }
        public string SelectedGrade { get; set; }
        public List<int> SelectedCategory { get; set; }
        public string SelectedLocationName { get; set; }
        public List<DayOfWeek> SelectedWeekDay { get; set; }
        public List<string> SelectedAMPM { get; set; }
        public List<string> SelectedProgramStatus { get; set; }
        public string CityName { get; set; }
        public long CampId { get; set; }
        public JbPFilter Filter { get; set; }
        public string SeasonRegBtnTitle { get; set; }
        public string SeasonRegBtnAlignment { get; set; }
        public string SeasonRegBtnSize { get; set; }
        public string SeasonRegBtnLabelColor { get; set; }
        public string SeasonRegBtnBackgroundColor { get; set; }
        public string ProgramRegBtnTitle { get; set; }
        public JbPageProgramLinkType ProgramRegBtnLinkTo { get; set; }
        public string ProgramRegBtnLabelColor { get; set; }
        public string ProgramRegBtnBackgroundColor { get; set; }
        public string CalenderProgramTextColor { get; set; }
        public string CalenderProgramBackgroundColor { get; set; }

        public string SeasonDomain { get; set; }
        public bool ShowRegistrationBtnTop { get; set; }
        public bool ShowRegistrationBtnBottom { get; set; }
        public bool ShowRoomColumn { get; set; }
        public bool ShowInstructorsColumn { get; set; }
        public bool ShowProviderColumn { get; set; }
        public bool ShowCapacityColumn { get; set; }
        public bool ShowLocationColumn { get; set; }
        public bool ShowCapacityLeftColumn { get; set; }
        public bool ShowStatusColumn { get; set; }
        public bool ShowProgramColumn { get; set; }
        public bool ShowRestrictionsColumn { get; set; }
        public bool ShowRegistrationDatesColumn { get; set; }
        public bool ShowDaysTimesColumn { get; set; }
        public bool ShowDatesColumn { get; set; }
        public bool ShowRegistrationColumn { get; set; }
        public bool ShowGradeAgeColumn { get; set; }
        public bool ShowPriceColumn { get; set; }
        public GridLayoutType GridLayoutType { get; set; }
        public string ScheduleStartDate { get; set; }
    }



    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPMap : JbPBaseElement
    {
        public string Address { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPTitlePack : JbPBaseElement
    {
        public JbPTitlePack()
        {
            TypeTitle = "Title";
        }
        public string Subtitle { get; set; }
        public string Paragraph { get; set; }
        public string SectiontBackgroundColor { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPDivider : JbPBaseElement
    {
        public JbPDivider()
        {
            TypeTitle = "Title";
        }
        public int DividerHeight { get; set; }
        public int LineThickness { get; set; }
        public string LineColor { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPRegLink : JbPBaseElement
    {
        public JbPRegLink()
        {
            TypeTitle = "Program";
            Tuitions = new List<JbPTuition>();
            Schedules = new List<JbPScheduleViewModel>();
        }
        public long SeasonId { get; set; }
        public long? ProgramId { get; set; }
        public bool IsExternal { get; set; }
        public string LinkUrl { get; set; }
        public string LinkTitle { get; set; }
        public bool ShowAgeRestrictions { get; set; }
        public int? MinAge { get; set; }
        public int? MaxAge { get; set; }
        public bool ShowGradeRestrictions { get; set; }
        public string MinGrade { get; set; }
        public string MaxGrade { get; set; }
        public bool ShowLocation { get; set; }
        public string Location { get; set; }
        public bool ShowRegistrationPeriod { get; set; }
        public RegistrationPeriod RegistrationPeriod { get; set; }
        public bool ShowStartEndDates { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool ShowDaysTimes { get; set; }
        public JbPDayTimeFormats DaysTimesFormat { get; set; }
        public bool HideDayTimesDate { get; set; }
        public List<JbPScheduleViewModel> Schedules { get; set; }
        public bool ShowCapacityLeft { get; set; }
        public bool ShowTuitionsPrices { get; set; }
        public bool ShowGenderRestrictions { get; set; }
        public Genders? GenderRestrictions { get; set; }
        public ProgramRegStatus RegStatus { get; set; }

        public bool IsFreezed { get; set; }

        public bool IsFull { get; set; }

        public bool IsDeleted { get; set; }

        public ProgramTypeCategory ProgramType { get; set; }

        public List<JbPTuition> Tuitions { get; set; }
        public string AdditionalNote { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPRegBtn : JbPBaseElement
    {
        public JbPRegBtn()
        {
            TypeTitle = "Title";
        }
        public int SeasonId { get; set; }
        public string SeasonDomain { get; set; }
        public string Alignment { get; set; }
        public string Size { get; set; }
        public string RegBtnLabelColor { get; set; }
        public string RegBtnBackgroundColor { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPSectionColumn : JbPBaseElement
    {
        public JbPSectionColumn()
        {
            TypeTitle = "Column";
            RegLinks = new List<JbPRegLink>();
            BeforeAfterRegLinks = new List<JbPBeforeAfterCareRegLinks>();
        }
        public List<JbPRegLink> RegLinks { get; set; }

        public List<JbPBeforeAfterCareRegLinks> BeforeAfterRegLinks { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPTab : JbPBaseElement
    {
        public JbPTab()
        {
            TypeTitle = "Tab";
            Elements = new List<JbPBaseElement>();

            Cover = new JbPCover();
            Appearance = new JbPAppearance();
            TabLink = "";
            EnableFilter = false;
            HideForUsers = false;

        }
        public bool EnableFilter { get; set; }
        public List<JbPBaseElement> Elements { get; set; }
        public JbPCover Cover { get; set; }
        public JbPAppearance Appearance { get; set; }
        public bool HideForUsers { get; set; }
        public string TabLink { get; set; }

    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPVideo : JbPBaseElement
    {
        public string UrlVideo { get; set; }
        public bool ChoiceVideoType { get; set; }

        public string CoverVideo { get; set; }

        public string YouTubeID { get; set; }
    }
    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPBeforeAfterCareRegLinks : JbPBaseElement
    {
        public JbPBeforeAfterCareRegLinks()
        {
            IsBeforeAfter = true;
            ColorSetting = new JbPBeforeAfterCareSetting();
            Schedules = new List<JbPScheduleViewModel>();
            PunchcardInformation = new DropInPunchCardInformation();
            DropInInformation = new DropInPunchCardInformation();
            TitleFontSize = 30;
            DateTimeFontSize = 16;
            MainDropInPunchcardDescription = "Description";
            PunchcardDescription = "Punchcard description";
            DropInDescription = "Drop-in description";
            BestValueForBeforeAfter = "Both";
            BestValueForDropInPunchcard = "Punchcard";
            BestValueFontSizeForBeforeAfter = 14;
            BestValueFontSizeForDropInPunchcard = 14;
        }
        public int TitleFontSize { get; set; }
        public int DateTimeFontSize { get; set; }
        public bool IsBeforeAfter { get; set; }
        public bool ShowBeforeAfterDescription { get; set; }
        public bool ShowBestValueForBeforeAfter { get; set; }
        public bool ShowBestValueForDropInPunchcard { get; set; }
        public string BestValueForBeforeAfter { get; set; }
        public int BestValueFontSizeForBeforeAfter { get; set; }
        public int BestValueFontSizeForDropInPunchcard { get; set; }
        public string BestValueForDropInPunchcard { get; set; }
        public long BeforeAfterSeasonId { get; set; }
        public long? BeforeAfterProgramId { get; set; }
        public JbPBeforeAfterCareSetting ColorSetting { get; set; }
        public string Description { get; set; }
        public string MinGrade { get; set; }
        public string MaxGrade { get; set; }
        public string DropInDescription { get; set; }
        public string PunchcardDescription { get; set; }
        public string MainDropInPunchcardDescription { get; set; }
        public bool IsExternal { get; set; }
        public string PaymentPriod { get; set; }
        public bool IsEnabeldDropIn { get; set; }
        public bool IsEnabeldPunchcard { get; set; }
        public string LinkUrl { get; set; }
        public string LinkClassPageUrl { get; set; }
        public bool LinkToClassPage { get; set; }
        public List<JbPScheduleViewModel> Schedules { get; set; }

        public DropInPunchCardInformation DropInInformation { get; set; }

        public DropInPunchCardInformation PunchcardInformation { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPImageGallery : JbPBaseElement
    {
        public JbPImageGallery()
        {
            Images = new List<JbPImage>();
        }

        public List<JbPImage> Images { get; set; }
    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPImageSlider : JbPBaseElement
    {
        public JbPImageSlider()
        {
            Images = new List<JbPImage>();
        }

        public List<JbPImage> Images { get; set; }

    }

    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPImage : JbPBaseElement
    {

        public string smallImage { get; set; }
        public string largeImage { get; set; }
        public string location { get; set; }

    }


    public class JbPBaseElement
    {
        public string ElementId { get; set; }
        public string TypeTitle { get; set; }
        public virtual string Title { get; set; }
        public JbPBaseElement()
        {
            TypeTitle = this.GetType().Name.Replace("JbP", "");
            ElementId = this.GetType().Name + StaticRandom.Instance.Next(10000, 99999);
        }
    }

    public class JbPTuition
    {
        public string Label { get; set; }

        public decimal Price { get; set; }

        public string PaymentPriod { get; set; }
        public int NumberOfClassDays { get; set; }
        public string CapacityLeft { get; set; }
        public long ChargeId { get; set; }
    }
    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPGridSetting : JbPBaseElement
    {
        public string TextColor { get; set; }
        public string BackgroundColor { get; set; }
        public string SubBackgroundColor { get; set; }
        public string ActiveBackgroundColor { get; set; }
    }
    [JsonConverter(typeof(JbPElementConverter))]
    public class JbPBeforeAfterCareSetting : JbPBaseElement
    {
        public JbPBeforeAfterCareSetting()
        {

        }
        public JbPBeforeAfterCareSetting(bool initializeElements)
        {
            if (initializeElements)
            {

                TitleColor = "#1F84A8";
                TimeColor = "#535087";
                HeaderBackgroundColor = "#D5DF26";
                ButtonBackgroundColor = "#1F84A8";
                ButtonTextColor = "#fff";
            }

        }
        public string TitleColor { get; set; }
        public string TimeColor { get; set; }
        public string HeaderBackgroundColor { get; set; }
        public string ButtonBackgroundColor { get; set; }
        public string ButtonTextColor { get; set; }
    }

    public class JbPSetting
    {
        public bool ShowStatusLabels { get; set; }
        public bool ShowLabelFullCapacity { get; set; }
        public bool ShowLabelNotStarted { get; set; }
        public bool ShowLabelExpired { get; set; }
        public bool ShowLabelFrozen { get; set; }
        public bool CustomizeRegLinksColor { get; set; }
        public string RegLiksAvailableColor { get; set; }
        public string RegLiksUnavailableColor { get; set; }
        public string TabBackgroundColor { get; set; }
        public string TabTextColor { get; set; }
        public string TabFontSize { get; set; }
        public string TabFontWeight { get; set; }
        public string TabSelectedBackgroundColor { get; set; }
        public string TabSelectedTextColor { get; set; }
        public string TabHoverTextColor { get; set; }
        public string TabSelectedHoverTextColor { get; set; }
        public string TabBorderColor { get; set; }
        public string TabSelectedBorderColor { get; set; }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum JbPColumnCount
    {
        Two,
        Three
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum JbPMode
    {
        Design,
        Preview,
        View,
        Iframe
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum JbPElementTypes
    {
        JbPCover,
        JbPAppearance,
        JbPHeader,
        JbPFooter,
        JbPSection,
        JbPSectionColumn,
        JbPGrid,
        JbPMenuItem,
        JbPTitlePack,
        JbPRegLink,
        JbPage,
        JbPTab,
        JbPSeasonCalendar,
        JbPSeasonGrid,
        JbPDivider,
        JbPRegBtn,
        JbPContact,
        JbPVideo,
        JbPBeforeAfterCareProgram,
        JbPImageGallery,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum JbPDayTimeFormats
    {
        DaysTimes,
        DaysOnly,
        TimesOnly,
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum JbPCoverBGMode
    {
        Gallery,
        Upload,
        Url,
        Solid,
        None
    }

    public class JbPScheduleViewModel
    {

        public string Title { get; set; }

        public long Id { get; set; }
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public JbPScheduleDayViewModel[] Days { get; set; }

        public string strDays
        {
            get
            {
                var allSchedules = string.Empty;
                var Schedules = new List<string>();
                if (Days != null && Days.Any())
                {
                    foreach (var item in Days)
                    {
                        Schedules.Add(string.Format("{0} ({1} - {2})", item.DayOfWeek, item.StartTime, item.EndTime));
                        allSchedules = string.Join("<br/> ", Schedules);
                    }
                    return allSchedules;
                }
                return string.Empty;
            }
        }
        public string Description { get; set; }
        public List<JbPTuition> Tuitions { get; set; }

        public List<ScheduleTimesViewModel> ScheduleDaysTime { get; set; }

        public List<string> StrScheduleDaysTime { get; set; }
        public TimeOfClassFormation Mode { get; set; }
    }

    public class JbPScheduleDayViewModel
    {
        public DayOfWeek DayOfWeek { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }

    public class DropInPunchCardInformation
    {
        public string Title { get; set; }
        public string Amount { get; set; }
        public string FeeLable { get; set; }
        public List<ScheduleTimesViewModel> Times { get; set; }
        public List<string> StrTimes { get; set; }
        public string LinkUrl { get; set; }
        public string LinkClassPageUrl { get; set; }
    }

    public class ScheduleTimesViewModel
    {
        public string Day { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string MainTime { get; set; }
    }

    public enum GridLayoutType : byte
    {
        Weekly,
        List,
        Calendar,
        Agenda
    }

    public enum JbPageProgramLinkType : byte
    {
        RegisterationPage,
        ClassPage
    }
}
