﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SportsClub.Models
{
    public class ParentChangeEmailViewModel
    {
        [Required(ErrorMessage = "The Username field is required.")]
        [EmailAddress]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "The Username must be a valid e-mail address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The current password field is required.")]
        [StringLength(100, ErrorMessage = "The current password must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string CurrentPassword { get; set; }

    }

  
}