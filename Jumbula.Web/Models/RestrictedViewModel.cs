﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;

namespace SportsClub.Models
{
    public class RestrictedViewModel
    {

        public RestrictedViewModel()
        {
            // ...
        }

        public RestrictedViewModel(string domain)
        {
            var members = Ioc.ClubBusiness.Get(domain).Users.ToList();

            _clubMembers = new List<JbUser>();
            foreach (var item in members)
            {
                _clubMembers.Add(new JbUser()
                {
                    Id = item.UserId,
                    UserName = item.User.UserName
                });
            }

            MemberItems = _clubMembers.Select(u => new SelectListItem() { Text = u.UserName, Value = u.Id.ToString() }).ToList();
        }

        private List<JbUser> _clubMembers { get; set; }

        public bool IsRestricted { get; set; }

        public List<string> Users { get; set; }

        public List<SelectListItem> MemberItems { get; set; }
    }
}