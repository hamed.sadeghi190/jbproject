﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Catalog;

namespace Jumbula.Core.Business
{
    public interface IMessageBusiness
    {
        MessageHeader Get(int id);
        List<MessageHeader> GetInboxUserMessages(int userId, int clubId, int start, int take, ref int total);

        IQueryable<MessageHeader> GetRecentMessages(int userId, int clubId);

        JbUser GetClubMessageReciever(Club club);

        MessageHeader GetChat(int senderId, int clubSenderId, int receiverId, int clubRecieverId, string subject, string text, MessageType messageType = MessageType.Ordinary, string attribute = "", long? programId = null);

        MessageHeader GetChat(string chatId, int userId);

        MessageHeader Send(string chatId, int currentUserId, int currentClubId, int? senderId, int? senderClubId, int? receiverId, int? receiverClubId, ref int realReceiverId, string subject = "", string text = "", MessageType messageType = MessageType.Ordinary, string attribute = "", long? programId = null);

        MessageHeader Send(string chatId, int senderId, int senderClubId, string text);
        MessageHeader Send(string chatId, int senderId, int senderClubId, int recieverId, int recieverClubId, string subject, string text);

        Club GetAudience(MessageHeader messageHeader, int currentClubId);

        JbUser GetAudienceUser(MessageHeader messageHeader, int currentUserId);
        bool Reply(int headerId, int clubId, string replyText, int senderId, int receiverId);
        int GetUnreadCount(int userId, int clubId);

        void SetAsRead(string chatId, int userId);

        JbUser GetPartnerMessageReciever(Club partner, Club school, long outSourceSeasonId);

        int AddProgramIdForOldMessages();
    }
}
