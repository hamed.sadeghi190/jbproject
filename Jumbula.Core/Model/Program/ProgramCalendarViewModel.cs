﻿using System;

namespace Jumbula.Core.Model.Program
{
    public class ProgramCalendarViewModel
    {
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string StrStartTime { get; set; }
        public string StrEndTime { get; set; }
        public bool IsAllDay { get; set; }
        public bool IsFull { get; set; }
        public string Date { get; set; }
        public int? ProgramSessionId { get; set; }
        public int Enrollment { get; set; }
        public string Capacity { get; set; }
        public string SeasonDomain { get; set; }
        public long ProgramId { get; set; }
        public string ViewPageUrl { get; set; }
        public bool HideOutSourceProgramInProvider { get; set; }

    }
}
