﻿

namespace Jumbula.Common.Enums
{
    public enum CampaignScheduleType : int
    {
        Sent = 0,
        Scheduled = 1,
        Draft = 2,
        Cancel = 3
    }
}
