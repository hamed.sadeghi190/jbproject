﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramCalendarItemViewModel
    {
        public int TaskId { get; set; }

        public string Title { get; set; }

        public string Start { get; set; }

        public string End { get; set; }

        public string StartTimezone { get; set; }

        public string EndTimezone { get; set; }

        public string Description { get; set; }

        public int RecurrenceId { get; set; }

        public string RecurrenceRule { get; set; }

        public string RecurrenceException { get; set; }

        public int OwnerId { get; set; }

        public bool IsAllDay { get; set; }

        public bool IsAssigned { get; set; }

        public bool IsSelected { get; set; }

        public bool IsFull { get; set; }

        public int ProgramSessionId { get; set; }

        public string ProgramType { get; set; }
        public string ScheduleMinGrade { get; set; }
        public string ScheduleMaxGrade { get; set; }
        public List<KeyValuePair<string, int>> ProgramGradeList { get; set; }

        public string Date { get; set; }
    }
}