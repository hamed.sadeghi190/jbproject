Alter Table [dbo].[PaymentPlans] add [Name] [nvarchar](128) NOT NULL DEFAULT ('')
go

update PaymentPlans set [Name] = CAST(NumOfInstallments AS NVARCHAR(128))  + ' Installment' where Name = '' And NumOfInstallments = 1
go

update PaymentPlans set [Name] = CAST(NumOfInstallments AS NVARCHAR(128))  + ' Installments' where Name = '' And NumOfInstallments > 1
go