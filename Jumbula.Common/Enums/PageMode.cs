﻿namespace Jumbula.Common.Enums
{
    public enum PageMode : byte
    {
        Create = 1,
        Edit = 2,
        Display = 3
    }
}
