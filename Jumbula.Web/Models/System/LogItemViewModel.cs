﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Sistem
{
    public class LogItemViewModel
    {
        public string Message { get; set; }

        public string LogTime { get; set; }

        public string LogDate { get; set; }

        public string LogCategory { get; set; }
    }
}