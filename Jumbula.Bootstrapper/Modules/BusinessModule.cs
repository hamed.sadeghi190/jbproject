﻿using Autofac;
using Jb.Framework.Common.Forms;
using Jumbula.Business;
using Jumbula.Core.Business;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.JbPage;
using Jumbula.Web.WebLogic.Payment;

namespace Jumbula.Bootstrapper.Modules
{
    public class BusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType(typeof(ContactPersonBusiness)).As(typeof(IContactPersonBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(PlayerProfileBusiness)).As(typeof(IPlayerProfileBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(ClubBusiness)).As(typeof(IClubBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(UserProfileBusiness)).As(typeof(IUserProfileBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(StorageBusiness)).As(typeof(IStorageBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(FormBusiness)).As(typeof(IFormBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(JbFormBusiness)).As(typeof(IJbFormBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(OrderBusiness)).As(typeof(IOrderBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(SecurityBusiness)).As(typeof(ISecurityBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(AttributeBusiness)).As(typeof(IAttributeBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(FieldSettingBusiness)).As(typeof(IFieldSettingBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CouponBusiness)).As(typeof(ICouponBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(OldReportBusiness)).As(typeof(IReportBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ReportBusiness)).As(typeof(INewReportBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(PaypalIPNBusiness)).As(typeof(IPaypalIPNBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(JbFileBusiness)).As(typeof(IJbFileBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(SeasonFileBusiness)).As(typeof(ISeasonFileBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(TransactionActivityBusiness)).As(typeof(ITransactionActivityBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(OrderHistoryBusiness)).As(typeof(IOrderHistoryBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CustomElementManager)).As(typeof(ICustomElementManager)).InstancePerRequest();
            builder.RegisterType(typeof(StripeWebhookBusiness)).As(typeof(IStripeWebhookBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(SeasonBusiness)).As(typeof(ISeasonBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(ProgramBusiness)).As(typeof(IProgramBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(ClubBusiness)).As(typeof(IClubBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(PricePlanBusiness)).As(typeof(IPricePlanBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CategoryBusiness)).As(typeof(ICategoryBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CampaignBusiness)).As(typeof(ICampaignBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(MailListBusiness)).As(typeof(IMailListBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(DiscountBusiness)).As(typeof(IDiscountBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(PaymentPlanBusiness)).As(typeof(IPaymentPlanBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CartBusiness)).As(typeof(ICartBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(OrderSessionBusiness)).As(typeof(IOrderSessionBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(ProgramSessionBusiness)).As(typeof(IProgramSessionBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(OldEmailBusiness)).As(typeof(IOldEmailBusiness)).InstancePerRequest().WithParameter("sendEmailToSupport", WebConfigHelper.IsProductionMode).WithParameter("config", WebConfigHelper.SmtpMailSettings);
            builder.RegisterType(typeof(CustomReportBusiness)).As(typeof(ICustomReportBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(OrderInstallmentBusiness)).As(typeof(IOrderInstallmentBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ClientBusiness)).As(typeof(IClientBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ClientCreditCardBusiness)).As(typeof(IClientCreditCardBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(FollowupFormBusiness)).As(typeof(IFollowupFormBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(OrderPreapprovalBusiness)).As(typeof(IOrderPreapprovalBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(PendingPreapprovalTransactionBusiness)).As(typeof(IPendingPreapprovalTransactionBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(OrderItemBusiness)).As(typeof(IOrderItemBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(PageBusiness)).As(typeof(IPageBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(PaypalTrackingBusiness)).As(typeof(IPaypalTrackingBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(PaymentDetailBusiness)).As(typeof(IPaymentDetailBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CatalogBusiness)).As(typeof(ICatalogBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CatalogSettingBusiness)).As(typeof(ICatalogSettingBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CountryBusiness)).As(typeof(ICountryBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ImageGalleryBusiness)).As(typeof(IImageGalleryBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ImageGalleryItemBusiness)).As(typeof(IImageGalleryItemBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(UserCreditCardBusiness)).As(typeof(IUserCreditCardBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(VerificationBusiness)).As(typeof(IVerificationBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(EmailCampaignTemplateBusiness)).As(typeof(IEmailCampaignTemplateBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(UserEventBusiness)).As(typeof(IUserEventBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(StudentAttendanceBusiness)).As(typeof(IStudentAttendanceBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ClientPaymentMethodBusiness)).As(typeof(IClientPaymentMethodBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(WaitListBusiness)).As(typeof(IWaitListBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(InvoiceBusiness)).As(typeof(IInvoiceBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(InvoiceHistoryBusiness)).As(typeof(IInvoiceHistoryBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(OrderItemWaiverBusiness)).As(typeof(IOrderItemWaiverBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(SubscriptionBusiness)).As(typeof(ISubscriptionBusiness)).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType(typeof(MessageBusiness)).As(typeof(IMessageBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(DiscountEngineRuleBusiness)).As(typeof(IDiscountEngineRuleBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(PlayerProgramNoteBusiness)).As(typeof(IPlayerProgramNoteBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(JbESignatureBusiness)).As(typeof(IJbESignatureBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(JbFlyerBusiness)).As(typeof(IJbFlyerBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ClubFlyerBusiness)).As(typeof(IClubFlyerBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CampaignCallbackBusiness)).As(typeof(ICampaignCallbackBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ScheduleTaskBusiness)).As(typeof(IScheduleTaskBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(SubsidyBusiness)).As(typeof(ISubsidyBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(FamilyBusiness)).As(typeof(IFamilyBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(FamilyContactBusiness)).As(typeof(IFamilyContactBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ChargeBusiness)).As(typeof(IChargeBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(AuditBusiness)).As(typeof(IAuditBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(SeasonPaymentPlanBusiness)).As(typeof(ISeasonPaymentPlanBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(LocationBusiness)).As(typeof(ILocationBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(AccountingBusiness)).As(typeof(IAccountingBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(RegisterBusiness)).As(typeof(IRegisterBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(LotteryBusiness)).As(typeof(ILotteryBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(AutoChargeBusiness)).As(typeof(IAutoChargeBusiness)).InstancePerRequest();    
            builder.RegisterType(typeof(JbPaymentService)).As(typeof(IPaymentBusiness));
            builder.RegisterType(typeof(ClubSettingBusiness)).As(typeof(IClubSettingBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(EmailBusiness)).As(typeof(IEmailBusiness)).InstancePerRequest().WithParameter("deploymentEnvironment", WebConfigHelper.DeploymentEnvironment);
            builder.RegisterType(typeof(PeopleBusiness)).As(typeof(IPeopleBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(FileBusiness)).As(typeof(IFileBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(ContactBusiness)).As(typeof(IContactBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(CommunicationHistoryBusiness)).As(typeof(ICommunicationHistoryBusiness)).InstancePerRequest();
            builder.RegisterType(typeof(EmailTemplateBusiness)).As(typeof(IEmailTemplateBusiness)).InstancePerRequest();
        }
    }
}
