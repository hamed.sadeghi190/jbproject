﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class ProgramController : DashboardBaseController
    {
        #region Fields
        private readonly IProgramSessionBusiness _programSessionBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly ILocationBusiness _locationBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;
        #endregion

        #region Constractors
        public ProgramController(IProgramBusiness programBusiness, IProgramSessionBusiness programSessionBusiness, IOrderItemBusiness orderItemBusiness, ILocationBusiness locationBusiness, IOrderSessionBusiness orderSessionBusiness)
        {
            _programBusiness = programBusiness;
            _programSessionBusiness = programSessionBusiness;
            _orderItemBusiness = orderItemBusiness;
            _locationBusiness = locationBusiness;
            _orderSessionBusiness = orderSessionBusiness;
        }
        #endregion
        public virtual ActionResult Summary()
        {
            return View();
        }
        public virtual ActionResult ConfirmRegisterForNewUser()
        {
            return View();
        }
        public virtual ActionResult Register(int? programId, int userId = 0)
        {
            if (programId.HasValue)
            {
                if (userId == 0)
                {
                    var order = Ioc.OrderBusiness.GetLastOfflineOrderForClub(this.ActiveClub.Id);
                    if (order != null)
                    {
                        userId = order.UserId;
                    }
                }
                if (userId > 0)
                {
                    var program = _programBusiness.Get(programId.Value);

                    var url = Url.EventRegister(program.Club.Domain, program.Season.Domain, program.Domain, userId, UrlHelpers.HomeUrl().HasSubDaomin());

                    url += "&offlineMode=true";

                    Response.Redirect(url);
                    Response.End();
                }
            }
            return View();
        }

        [HttpGet]
        public virtual ActionResult ProgramAddEditStep1()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult ProgramAddEditStep2()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult ProgramAddEditStep2Partial(ProgramTypeCategory programType)
        {
            switch (programType)
            {
                case ProgramTypeCategory.Calendar:
                    {
                        return View("_CalendarAddEditStep2");
                    }
                case ProgramTypeCategory.ChessTournament:
                    {
                        return View("_TournamentAddEditStep2");
                    }
                case ProgramTypeCategory.Subscription:
                    {
                        return View("_SubscriptionAddEditStep2");
                    }
                case ProgramTypeCategory.BeforeAfterCare:
                    {
                        return View("_BeforeAfterCareAddEditStep2");
                    }
                default:
                    {
                        return View("_ProgramAddEditStep2");
                    }
            }
        }

        [HttpGet]
        public virtual ActionResult CalendarAddEditStep2Point5()
        {
            return View("_CalendarAddEditStep2Point5");
        }

        [HttpGet]
        public virtual ActionResult SubscriptionAddEditStep2Point5()
        {
            return View("_SubscriptionAddEditStep2Point5");
        }
        public virtual ActionResult BeforeAfterCareAddEditStep2Fees()
        {
            return View("_BeforeAfterCareAddEditStep2Fees");
        }
        public virtual ActionResult BeforeAfterCareAddEditStep2ScheduleList()
        {
            return View("_BeforeAfterCareAddEditStep2ScheduleList");
        }

        [HttpGet]
        public virtual ActionResult ProgramAddEditStep3()
        {
            return View();
        }
        public virtual ActionResult ProgramRespondToInvitation()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult ProgramAddEditStep4()
        {
            return View();
        }
        [HttpGet]
        public virtual ActionResult ProgramParentsNotification()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult Sessions()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult AddToWaitlist()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult Waitlist()
        {
            return View();
        }

        [HttpGet]
        public virtual JsonNetResult GetAddToWaitlist(int programId, bool isIndividual = false)
        {
            var program = _programBusiness.Get(programId);

            var season = program.Season;

            var isWaitlistEnabled = false;
            var waitlistPolicy = string.Empty;
            var message = string.Empty;

            if (season.Setting != null && season.Setting.SeasonAdditionalSetting != null)
            {
                if (!program.IsWaitListAvailable && !season.Setting.SeasonAdditionalSetting.IsWaitListAvailable)
                {
                    message = "You have not enabled the waitlist option.";
                }
            }
            else
            {
                if (!program.IsWaitListAvailable)
                {
                    message = "You have not enabled the waitlist option.";
                }
            }

            if (isIndividual)
            {
                isWaitlistEnabled = program.IsWaitListAvailable;
                waitlistPolicy = !string.IsNullOrEmpty(program.WaitlistPolicy) ? program.WaitlistPolicy : Constants.DefaultWaitlistPolicy;
            }
            else
            {
                if (season.Setting != null && season.Setting.SeasonAdditionalSetting != null)
                {
                    isWaitlistEnabled = season.Setting.SeasonAdditionalSetting.IsWaitListAvailable;
                    waitlistPolicy = !string.IsNullOrEmpty(season.Setting.SeasonAdditionalSetting.WaitlistPolicy) ? season.Setting.SeasonAdditionalSetting.WaitlistPolicy : string.Empty;
                }
            }

            var model = new AddToWaitListAdminViewModel()
            {
                GradeItems = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null),
                ProgramName = program.Name,
                WaitlistPolicy = waitlistPolicy,
                Message = message,
                Schedules = program.ProgramSchedules.Where(c => c.IsDeleted == false).Select(c => c.ToViewModel<ScheduleViewModel>()).ToList(),
                ProgramType = program.TypeCategory
            };
            return JsonNet(model);

        }

        [HttpPost]
        public virtual ActionResult SaveToWaitlist(AddToWaitListAdminViewModel model, long programId, int userId)
        {

            OperationStatus result = new OperationStatus();
            var program = _programBusiness.Get(programId);
            var user = Ioc.UserProfileBusiness.Get(userId);
            var isTestMode = program.Season.Status == SeasonStatus.Test;

            var programScheduleId = program.ProgramSchedules.Where(s => !s.IsDeleted).FirstOrDefault().Id;

            if (Ioc.WaitListBusiness.IsAlreadyExistInWaitListByScheduleId(programScheduleId, userId, model.FirstName, model.LastName))
            {
                ModelState.AddModelError("", "This person already added to waitlist of program.");
            }

            if (program.TypeCategory != ProgramTypeCategory.Camp)
            {
                ModelState.Remove("model.scheduleId");
            }
            else if (model.scheduleId.HasValue)
            {
                var schedule = program.ProgramSchedules.Where(s => s.Id == model.scheduleId).FirstOrDefault();

                if (schedule.Title != null)
                {
                    model.ProgramName = string.Format("{0}, {1} ({2}-{3})", model.ProgramName, schedule.Title, schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat));
                }
                else
                {
                    model.ProgramName = string.Format("{0} ({1}-{2})", model.ProgramName, schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat));
                }
            }


            if (ModelState.IsValid)
            {
                var waitList = new WaitList()
                {
                    Date = DateTime.UtcNow,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    ProgramId = programId,
                    UserId = userId,
                    PhoneNumber = model.PhoneNumber,
                    Grade = model.SelectedGrade.Value,
                    IsLive = !isTestMode,
                    ScheduleId = program.TypeCategory == ProgramTypeCategory.Camp && model.scheduleId.HasValue ? model.scheduleId.Value : programScheduleId,
                };

                result = Ioc.WaitListBusiness.Create(waitList);

                var contactEmail = string.Empty;
                var clubDomain = string.Empty;
                var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

                if (club.PartnerId.HasValue)
                {
                    contactEmail = club.PartnerClub.ContactPersons.First().Email;
                    clubDomain = club.PartnerClub.Domain;
                }
                else
                {
                    clubDomain = club.Domain;
                    contactEmail = club.ContactPersons.First().Email;
                }
                var datetime = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), waitList.Date).ToString("MMM dd, yyyy, h:mm tt");
                // send email to parent
                EmailService emailService = EmailService.Get(this.ControllerContext);
                emailService.SendAddedToWaitlist(user.UserName, model.ProgramName, club.Name, club.Site, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo, contactEmail, clubDomain, program?.Season?.Title);

                var participantName = model.FirstName + " " + model.LastName;
                // send email to admin
                emailService.SendAddedToWaitlistForAdmin(datetime, participantName, model.SelectedGrade.ToDescription(), this.GetCurrentUserName(), model.ProgramName, club.Name, club.Site, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo, contactEmail, clubDomain);

                if (result.Status)
                {
                    return Json(new JResult { Status = true, Message = "token", RecordsAffected = 1 });
                }
                else
                {
                }
            }
            return base.JsonFormResponse();
        }
        [HttpGet]
        public virtual ActionResult SessionAttendance()
        {
            return View();
        }

        public virtual ActionResult AssignInstructors()
        {
            return View();
        }

        public virtual ActionResult AssignRoom()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult ProgramSessionDetails(int programId)
        {
            var program = _programBusiness.Get(programId);

            return JsonNet(new { ProgramName = program.Name });
        }

        private List<ProgramSessionItemViewModel> GetAllSessionItems(long programId)
        {

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var model = _programBusiness.GetSessions(program)
                .OrderBy(o => o.Start)
                    .ToList()
                    .Select(s => new ProgramSessionItemViewModel
                    {
                        Id = s.Id,
                        Date = s.Start.ToString("ddd, MMM dd, yyyy"),
                        StartTime = new TimeSpan(s.Start.Hour, s.Start.Minute, s.Start.Second).ToString(),
                        EndTime = new TimeSpan(s.End.Hour, s.End.Minute, s.End.Second).ToString(),
                        Time = string.Format("{0}-{1}", s.Start.ToShortTimeString(), s.End.ToShortTimeString())
                    })
                    .ToList();

            return model;
        }

        [HttpGet]
        public virtual JsonNetResult GetSessionList(int programId)
        {
            var programBusiness = _programBusiness;

            var model = GetAllSessionItems(programId);

            return JsonNet(model);
        }

        [HttpPost]
        public virtual JsonNetResult GetWaitList(PaginationModel paginationModel, int programId, long? scheduleId)
        {
            var waitListItems = Ioc.WaitListBusiness.GetList(programId).ToList();

            if (scheduleId.HasValue && scheduleId.Value > 0)
            {
                waitListItems = waitListItems.Where(item => item.ScheduleId == scheduleId.Value).ToList();
            }

            var model = waitListItems.Select(w =>
            new WaitlistItemViewModel
            {
                Id = w.Id,
                Date = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, w.Date).ToString("MMM dd, yyyy, h:mm tt"),
                Email = w.User.UserName,
                Grade = w.Grade.ToDescription(),
                ParticipantName = string.Concat(w.FirstName, " ", w.LastName),
                PhoneNumber = w.PhoneNumber,
                UserId = w.UserId,
                Schedule = w.Program.TypeCategory == ProgramTypeCategory.Camp ? w.Schedule.Title != null ? string.Format("{0} ({1}-{2})", w.Schedule.Title, w.Schedule.StartDate.ToString(Constants.DefaultDateFormat), w.Schedule.EndDate.ToString(Constants.DefaultDateFormat)) : string.Format("{0}-{1}", w.Schedule.StartDate.ToString(Constants.DefaultDateFormat), w.Schedule.EndDate.ToString(Constants.DefaultDateFormat)) : null,
                ProgramType = w.Program.TypeCategory,
                ScheduleId = w.ScheduleId
            });

            var dataSource = model.Any() && model.FirstOrDefault().ProgramType == ProgramTypeCategory.Camp ? model.OrderBy(w => w.ScheduleId).Page(paginationModel).ToList() : model.Page(paginationModel).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = model.Count() });

        }

        public virtual JsonNetResult GetCampSchedules(long programId)
        {
            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var model = new
            {
                Schedules = program.ProgramSchedules.Where(c => c.IsDeleted == false).Select(c => c.ToViewModel<ScheduleViewModel>()).ToList(),
                ProgramType = program.TypeCategory
            };

            return JsonNet(model);
        }
        [HttpPost]
        public virtual JsonNetResult DeleteWaitlistItems(List<int> ids)
        {
            var result = Ioc.WaitListBusiness.DeleteWhere(w => ids.Contains(w.Id));

            return JsonNet(new { Status = result.Status, Message = result.Message, RecordsAffected = result.RecordsAffected });
        }

        [HttpPost]
        public virtual ActionResult SendEmailToWaitlistUser(int[] userIds, int programId)
        {
            List<string> memebersUserNames;
            var campaignName = string.Empty;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            memebersUserNames = Ioc.UserProfileBusiness.GetList().Where(u => userIds.ToList().Contains(u.Id)).Select(u => u.UserName).ToList();
            campaignName = string.Concat(program.Name, " waitlist.");

            var campaignId = 0;
            var result = Ioc.CampaignBusiness.MakeCampaignOnReport(memebersUserNames, ActiveClub.Domain, campaignName);
            campaignId = result.RecordsAffected;
            return Json(new { result.Status, Data = campaignId, Message = result.ExceptionMessage });
        }

        [HttpPost]
        [JbAuthorize(JbAction.ProgramSession_Edit)]
        public virtual ActionResult UpdateSessionItem(ProgramSessionItemViewModel model, long programId)
        {
            if (ModelState.IsValid)
            {
                var session = new ScheduleSession();

                var date = DateTime.Parse(model.Date);
                var startTime = DateTime.Parse(model.StartTime);
                var endTime = DateTime.Parse(model.EndTime);

                session.Id = model.Id;
                session.Start = new DateTime(date.Year, date.Month, date.Day, startTime.Hour, startTime.Minute, 0);
                session.End = new DateTime(date.Year, date.Month, date.Day, endTime.Hour, endTime.Minute, 0);

                var result = _programBusiness.UpdateSessionItem(session, programId);

                var data = GetAllSessionItems(programId);

                return JsonNet(new { Status = result.Status, Data = data, Message = result.Message, RecordsAffected = result.RecordsAffected });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.ProgramSession_Delete)]
        public virtual JsonNetResult DeleteSessionItem(long programId, int sessionId)
        {
            var result = _programBusiness.DeleteSessionItem(sessionId, programId);

            var data = GetAllSessionItems(programId);

            return JsonNet(new { Status = result.Status, Data = data, Message = result.Message, RecordsAffected = result.RecordsAffected });
        }

        //[JbAuthorize(JbAction.Program_List_View)]
        public virtual JsonNetResult GetSessionAttendanceList(int programId, int sessionId)
        {
            var programBusiness = _programBusiness;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var model = _programBusiness.GetSessionAttendance(program.Id, sessionId).ToList()
                    .Select(s => new SessionAttendanceItemViewModel
                    {
                        Id = s.Id,
                        ProfileId = s.ProfileId,
                        ScheduleId = s.ScheduleId,
                        SessionId = s.SessionId,
                        FullName = s.PlayerProfile.Contact.FullName,
                        Status = s.Status
                    });

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult UpdateSessionAttendanceList(List<SessionAttendanceItemViewModel> model)
        {
            var attendances = model.Select(s =>
                    new ScheduleAttendance
                    {
                        Status = s.Status,
                        Id = s.Id,
                        ProfileId = s.ProfileId,
                        ScheduleId = s.ScheduleId,
                        SessionId = s.SessionId,
                    })
                    .ToList();

            var result = _programBusiness.UpdateAttendances(attendances.ToList());

            return JsonNet(new { Status = result.Status, Message = result.Message, RecordsAffected = result.RecordsAffected });
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_Add)]
        public virtual ActionResult Create(ProgramTypeCategory programType, string seasonDomain)
        {
            return RedirectToAction("ProgramCreateEditStep1", new { programType = programType, seasonDomain = seasonDomain, pageMode = PageMode.Create });
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual ActionResult Edit(long id, bool fromTemplate = false)
        {
            return RedirectToAction("ProgramCreateEditStep1", new { pageMode = PageMode.Edit, id = id, fromTemplate = fromTemplate });
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual JsonNetResult ProgramCreateEditStep1(ProgramTypeCategory? programType, PageMode pageMode, string seasonDomain, long? id, bool fromTemplate = false)
        {
            var model = new ProgramCreateEditModelStep1();
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            switch (pageMode)
            {

                case PageMode.Create:
                    {

                        model.Name = string.Empty;
                        model.Domain = string.Empty;
                        model.TypeCategory = programType.Value;
                        model.SeasonDomain = seasonDomain;
                        model.ClubDomain = ActiveClub.Domain;
                        model.PageStep = ProgramPageStep.Step1;
                        model.LastCreatedPage = ProgramPageStep.NotCreated;

                        model.AllCategories = Ioc.CategoryBuisness.GetList()
                              .Where(e => e.ParentId != null)
                              .ToList().Select(x =>
                              new SelectKeyValue<string>()
                              {
                                  Text = string.Format("{0} --> {1}", x.ParentCategory.Name, x.Name),
                                  Value = x.Id.ToString()
                              })
                              .ToList();

                        model.AllLocations = club.ClubLocations.Where(l => !l.IsDeleted).Select(l =>
                            new SelectKeyValue<string>
                            {
                                Text = l.PostalAddress.Address + (!string.IsNullOrEmpty(l.Name) ? string.Format(" ({0})", l.Name) : string.Empty),
                                Value = l.Id.ToString()
                            })
                           .ToList();

                        model.SelectedLocation = model.AllLocations.Select(a => a.Value).FirstOrDefault();

                        model.AllInstructors = Ioc.ClubBusiness.GetClubInstructors(ActiveClub.Id).Where(i => !i.IsFrozen && !i.IsDeleted).Select(l =>
                          new SelectKeyValue<int>
                          {
                              Text = l.Contact.FullName,
                              Value = l.Id
                          })
                          .ToList();

                        model.WaitlistPolicy = Constants.DefaultWaitlistPolicy;

                    }
                    break;
                case PageMode.Edit:
                    {
                        Program program = _programBusiness.Get(id.Value);
                        CheckResourceForAccess(program);

                        model = program.ToViewModel<ProgramCreateEditModelStep1>();

                        model.AllCategories = Ioc.CategoryBuisness.GetList()
                                .Where(e => e.ParentId != null)
                                .ToList()
                                .Select(x =>
                                new SelectKeyValue<string>()
                                {
                                    Text = string.Format("{0} --> {1}",
                                    x.ParentCategory.Name, x.Name),
                                    Value = x.Id.ToString()
                                })
                                .ToList();

                        var locations = _locationBusiness.GetClubLocationsByClubId(ActiveClub.Id);

                        model.AllLocations = locations.Select(l =>
                               new SelectKeyValue<string>
                               {
                                   Text = l.PostalAddress.Address + (!string.IsNullOrEmpty(l.Name) ? string.Format(" ({0})", l.Name) : string.Empty),
                                   Value = l.Id.ToString()
                               })
                               .ToList();

                        model.SelectedCategories = program.Categories.Select(c => c.Id.ToString()).ToList();
                        model.HasTestimonials = program.HasTestimonial;

                        model.SelectedLocation = locations.First(c => c.PostalAddress.Id == program.ClubLocation.PostalAddress.Id).Id.ToString();

                        model.AllInstructors = Ioc.ClubBusiness.GetClubInstructors(ActiveClub.Id).Where(i => !i.IsFrozen && !i.IsDeleted).Select(l =>
                         new SelectKeyValue<int>
                         {
                             Text = l.Contact.FullName,
                             Value = l.Id
                         })
                         .ToList();

                        model.SelectedInstructors = program.Instructors.Select(c => c.Id).ToList();

                        if (program.OutSourceSeasonId != null)
                        {

                            model.AllInstructors = Ioc.ClubBusiness.GetClubInstructors(program.OutSourceSeason.ClubId).Where(i => !i.IsFrozen).Select(l =>
                             new SelectKeyValue<int>
                             {
                                 Text = l.Contact.FullName,
                                 Value = l.Id
                             })
                        .ToList();
                        }

                        model.IsWaitListAvailable = program.IsWaitListAvailable;

                        if (string.IsNullOrEmpty(model.WaitlistPolicy))
                        {
                            model.WaitlistPolicy = Constants.DefaultWaitlistPolicy;
                        }

                        if (fromTemplate)
                        {
                            model.Name = string.Format("Copy of {0}", program.Name);
                            model.FromTemplate = fromTemplate;
                        }
                        //Get images
                        if (program.ImageGalleryId.HasValue)
                        {
                            var clubDomain = program.ImageGallery.ClubDomain;
                            var fileNames = program.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();
                            if (fileNames.Any())
                            {
                                model.ProgramImages = GetProgramImagesURL(fileNames, clubDomain);
                            }
                        }
                        if (program.CatalogId.HasValue)
                        {
                            var fileNames = new List<string>();
                            var clubDomain = program.OutSourceSeason.Club.Domain;

                            fileNames.Add(program.Catalog.ImageFileName);

                            if (program.Catalog.ImageGallery_Id.HasValue)
                            {
                                fileNames.AddRange(program.Catalog.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList());

                            }

                            if (fileNames.Any())
                            {
                                model.ProgramImages = GetProgramImagesURL(fileNames, clubDomain);
                            }
                            model.HasCatalogId = true;
                        }
                    }
                    break;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual ActionResult ProgramCreateEditStep1(ProgramCreateEditModelStep1 model)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            OperationStatus res = null;

            Program program = new Program();

            Step1ValidationCheck(ModelState, model);

            var today = DateTime.UtcNow;

            List<string> newImages = new List<string>();
            List<string> hasImages = new List<string>();
            var listImgs = new Dictionary<string, string>();

            if (ModelState.IsValid)
            {
                switch (model.PageMode)
                {
                    case PageMode.Create:
                        {
                            model.ClubDomain = ActiveClub.Domain;

                            Season season = Ioc.SeasonBusiness.Get(model.SeasonDomain, ActiveClub.Id);

                            program = model.ToModel<Program>();

                            program.ClubId = ActiveClub.Id;
                            program.SeasonId = season.Id;

                            #region Temporary(flyer)
                            program.ClubLocationId = int.Parse(model.SelectedLocation);

                            program.Flyer = new Flyer()
                            {
                                FileName = "testFlyer",
                                FlyerType = FlyerType.Generate
                            };
                            #endregion
                            //Add images for prgram
                            var listCropAndSourceImage = new Dictionary<string, string>();

                            if (model.ProgramImages.Count > 0)
                            {
                                listCropAndSourceImage = model.ProgramImages.Zip(model.SourceImages, (k, v) => new { Key = k, Value = v }).ToDictionary(x => x.Key, x => x.Value);
                            }

                            var pathImages = UploadProgramImages(listCropAndSourceImage, ActiveClub.Domain);

                            if (pathImages.Any())
                            {
                                _programBusiness.AddImageGalleryToProgram(program, pathImages, model.Name, model.ClubDomain);
                            }

                            program.Domain = _programBusiness.GenerateSubDomainName(ActiveClub.Domain, season.Domain, model.Name);
                            program.RegistrationMode = RegistrationMode.Open;
                            program.Status = ProgramStatus.Open;
                            program.LastCreatedPage = ProgramPageStep.Step1;

                            if (model.SelectedCategories != null && model.SelectedCategories.Any())
                            {
                                var categories = Ioc.CategoryBuisness.GetList(model.SelectedCategories.Select(c => int.Parse(c))).ToList();

                                program.Categories.AddEntities<Category>(categories);
                            }

                            if (model.SelectedInstructors != null && model.SelectedInstructors.Any())
                            {
                                var instructors = Ioc.ClubBusiness.GetClubInstructors(ActiveClub.Id).Where(i => model.SelectedInstructors.Select(c => c).ToList().Contains(i.Id)).ToList();

                                program.Instructors.AddEntities(instructors);
                            }
                            res = _programBusiness.Create(program);
                        }
                        break;
                    case PageMode.Edit:
                        {
                            Season season = Ioc.SeasonBusiness.Get(model.SeasonDomain, ActiveClub.Id);

                            if (model.FromTemplate)
                            {
                                var oldProgram = _programBusiness.Get(model.Id);
                                var oldImageGallery = oldProgram.ImageGallery;

                                program = _programBusiness.Copy(model.Id);
                                CheckResourceForAccess(program);

                                if (oldImageGallery != null)
                                {
                                    var clubDomain = oldImageGallery.ClubDomain;
                                    program.ImageGallery = null;
                                    program.ImageGalleryId = null;

                                    _programBusiness.Update(program);

                                    if (program.ImageGallery == null)
                                    {
                                        program.ImageGallery = new ImageGallery()
                                        {
                                            Name = program.Name,
                                            ClubDomain = clubDomain,
                                            MetaData = new MetaData() { DateCreated = today, DateUpdated = today, },
                                            Type = ImageGalleryType.Program,
                                            Status = EventStatusCategories.open,
                                        };

                                    }

                                    foreach (var item in model.ProgramImages)
                                    {
                                        if (!item.Contains("http"))
                                        {
                                            newImages.Add(item);
                                        }
                                        else
                                        {
                                            var path = item.Split('/').Last();
                                            hasImages.Add(path);
                                        }
                                    }
                                    var availableImage = oldImageGallery.Images.Where(i => hasImages.Contains(i.Path));

                                    foreach (var oldImage in availableImage)
                                    {
                                        program.ImageGallery.Images.Add(new ImageGalleryItem()
                                        {
                                            Title = oldImage.Title,
                                            Path = oldImage.Path,
                                            Status = EventStatusCategories.open,
                                            Order = 0,
                                            MetaData = new MetaData { DateCreated = today, DateUpdated = today }
                                        });
                                    }

                                    if (newImages != null && newImages.Any())
                                    {
                                        listImgs = newImages.Zip(model.SourceImages, (k, v) => new { Key = k, Value = v })
                                                   .ToDictionary(x => x.Key, x => x.Value);

                                        var pathImages = UploadProgramImages(listImgs, clubDomain);
                                        foreach (var path in pathImages)
                                        {
                                            program.ImageGallery.Images.Add(new ImageGalleryItem()
                                            {
                                                Title = model.Name,
                                                Path = path,
                                                Status = EventStatusCategories.open,
                                                Order = 0,
                                                MetaData = new MetaData { DateCreated = today, DateUpdated = today }
                                            });
                                        }
                                    }
                                }

                                program.Name = model.Name;

                                program.Domain = _programBusiness.GenerateSubDomainName(ActiveClub.Domain, season.Domain, model.Name);

                                program.Description = model.Description;
                                program.Testimonials = model.Testimonials;
                                program.HasTestimonial = model.HasTestimonials;
                                program.Room = model.Room;

                                program.IsWaitListAvailable = model.IsWaitListAvailable;
                                program.WaitlistPolicy = model.WaitlistPolicy;

                                program.HasRegisterPIN = model.HasRegisterPIN;
                                program.RegisterPIN = model.RegisterPIN;
                                program.RegisterPINMessage = model.RegisterPINMessage;
                            }
                            else
                            {

                                var currentProgram = _programBusiness.Get(model.Id);
                                var currentProgramName = currentProgram.Name;

                                program = model.ToModel<Program>(currentProgram);

                                if (!model.Name.Equals(currentProgramName, StringComparison.OrdinalIgnoreCase))
                                    program.Domain = _programBusiness.GenerateSubDomainName(ActiveClub.Domain, season.Domain, model.Name);

                                CheckResourceForAccess(program);

                                #region Program images
                                var clubDomain = program.ImageGallery != null ? program.ImageGallery.ClubDomain : club.Domain;
                                var upadateImageslist = new List<ImageGalleryItem>();
                                if (model.ProgramImages.Count > 0 && !program.CatalogId.HasValue)
                                {
                                    foreach (var item in model.ProgramImages)
                                    {

                                        if (!item.Contains("http"))
                                        {
                                            newImages.Add(item);
                                        }
                                        else
                                        {
                                            hasImages.Add(item);
                                        }

                                    }
                                    var removeList = new List<string>();
                                    if (hasImages.Any())
                                    {


                                        var fileNames = program.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();
                                        var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                                        var ImagesInDb = GetProgramImagesURL(fileNames, clubDomain);

                                        removeList.AddRange(ImagesInDb);
                                        foreach (var item in ImagesInDb)
                                        {
                                            if (hasImages.Contains(item))
                                            {
                                                removeList.Remove(item);
                                            }
                                        }

                                        foreach (var fileName in removeList)
                                        {
                                            var subFilName = fileName.Substring(fileName.LastIndexOf("/") + 1);
                                            //var sourcePath = StorageManager.PathCombine(string.Format(@"{0}/{1}", ActiveClub.Domain, Constants.Path_CatalogItems_Folder), fileName);
                                            //var targetPath = StorageManager.PathCombine(string.Format(@"{0}/{1}", ActiveClub.Domain, Constants.Path_CatalogItems_Folder), fileName);
                                            var url = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", ActiveClub.Domain, Constants.Path_CatalogItems_Folder), subFilName).AbsoluteUri;
                                            if (StorageManager.IsExist(url))
                                            {
                                                //storageManager.ReadFile(sourcePath, targetPath);
                                                storageManager.DeleteBlob(string.Format(@"{0}\{1}", ActiveClub.Domain, Constants.Path_CatalogItems_Folder), subFilName);
                                            }
                                        }

                                    }
                                    if (removeList != null && removeList.Any())
                                    {
                                        OperationStatus result = new OperationStatus() { Status = false };

                                        var path = "";
                                        foreach (var item in removeList)
                                        {
                                            path = item.Substring(item.LastIndexOf("/") + 1);
                                            var removeImage = program.ImageGallery.Images.Where(i => i.Path == path).FirstOrDefault();
                                            upadateImageslist.Add(removeImage);
                                        }
                                        result = Ioc.ImageGalleryItemBusiness.DeleteImages(upadateImageslist);
                                    }

                                    if (program.ImageGallery == null)
                                    {
                                        program.ImageGallery = new ImageGallery()
                                        {
                                            Name = program.Name,
                                            ClubDomain = club.Domain,
                                            MetaData = new MetaData() { DateCreated = today, DateUpdated = today, },
                                            Type = ImageGalleryType.Catalog,
                                            Status = EventStatusCategories.open,
                                        };

                                    }

                                    if (newImages != null && newImages.Any())
                                    {
                                        listImgs = newImages.Zip(model.SourceImages, (k, v) => new { Key = k, Value = v })
                                                   .ToDictionary(x => x.Key, x => x.Value);

                                        var pathImages = UploadProgramImages(listImgs, clubDomain);
                                        foreach (var path in pathImages)
                                        {
                                            program.ImageGallery.Images.Add(new ImageGalleryItem()
                                            {
                                                Title = model.Name,
                                                Path = path,
                                                Status = EventStatusCategories.open,
                                                Order = 0,
                                                MetaData = new MetaData { DateCreated = today, DateUpdated = today }
                                            });
                                        }
                                    }

                                }

                                if (model.ProgramImages.Count == 0 && model.ProgramImages != null && !program.CatalogId.HasValue)
                                {
                                    if (program.ImageGalleryId.HasValue)
                                    {
                                        var fileNames = program.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();

                                        OperationStatus result = new OperationStatus() { Status = false };

                                        foreach (var item in fileNames)
                                        {
                                            var removeImage = program.ImageGallery.Images.Where(i => i.Path == item).FirstOrDefault();
                                            upadateImageslist.Add(removeImage);
                                        }
                                        result = Ioc.ImageGalleryItemBusiness.DeleteImages(upadateImageslist);
                                    }
                                }
                                #endregion end edit images
                            }
                            program.Testimonials = model.Testimonials;
                            program.HasTestimonial = model.HasTestimonials;
                            program.DisableCapacityRestriction = model.DisableCapacityRestriction;

                            var locationId = int.Parse(model.SelectedLocation);
                            var newLocation = _locationBusiness.GetClubLocationsByClubId(ActiveClub.Id).FirstOrDefault(c => c.Id == locationId);
                            if (program.ClubLocation.PostalAddress.Id != newLocation.PostalAddress.Id)
                            {
                                program.ClubLocationId = locationId;
                            }

                            if (model.SelectedCategories != null && model.SelectedCategories.Any())
                            {
                                var categories = Ioc.CategoryBuisness.GetList(model.SelectedCategories.Select(c => int.Parse(c))).ToList();

                                program.Categories.Clear();

                                program.Categories.AddEntities<Category>(categories);
                            }

                            program.Instructors.Clear();

                            if (model.SelectedInstructors != null && model.SelectedInstructors.Any())
                            {
                                var activeClub = ActiveClub.Id;
                                if (program.OutSourceSeasonId != null)
                                {
                                    activeClub = program.OutSourceSeason.ClubId;
                                }
                                var instructors = Ioc.ClubBusiness.GetClubInstructors(activeClub).Where(i => model.SelectedInstructors.Select(c => c).ToList().Contains(i.Id)).ToList();

                                program.Instructors.AddEntities<ClubStaff>(instructors);
                            }

                            res = _programBusiness.Update(program);
                        }
                        break;
                }

                var json = new JavaScriptSerializer().Serialize(new { programId = program.Id.ToString(), typeCategory = program.TypeCategory });

                return Json(new JResult { Data = json, Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual JsonNetResult ProgramCreateEditStep2(int programId)
        {

            var model = new ProgramCreateEditModelStep2();

            var clubDomain = ActiveClub.Domain;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            model = program.ToViewModel<ProgramCreateEditModelStep2>();

            model.PageStep = ProgramPageStep.Step2;

            var allDaysOfWeek = new List<ProgramScheduleDayViewModel>()
                {
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Monday },
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Tuesday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Wednesday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Thursday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Friday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Saturday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Sunday}
                };

            model.Sessions = SeedSession(120);
            model.Capacities = SeedCapacity(Constants.MaximumCapacity);
            model.MinimumEnrollments = DropdownHelpers.SeedKeyValueNumbers(Constants.MaximumCapacity);
            model.Ages = SeedAges(120);

            switch (model.PageMode)
            {
                case PageMode.Create:
                    {
                        model.Schedules = new List<ProgramScheduleViewModel>()
                        {
                           NewSchedule()
                        };
                    }
                    break;
                case PageMode.Edit:
                    {
                        DateTime date = new DateTime(0);

                        var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted).ToList().OrderBy(s => s.StartDate).ThenByDescending(s => s.EndDate);

                        model.Schedules = programSchedules.Select(s =>
                            new ProgramScheduleViewModel
                            {
                                Id = s.Id,

                                Days = allDaysOfWeek.Select(a =>
                                new ProgramScheduleDayViewModel
                                {
                                    DayOfWeek = a.DayOfWeek,
                                    IsChecked = ((ScheduleAttribute)s.Attributes).Days != null ? ((ScheduleAttribute)s.Attributes).Days.Any(d => d.DayOfWeek == a.DayOfWeek) : false,
                                    StartTime = ((ScheduleAttribute)s.Attributes).Days != null ? (((ScheduleAttribute)s.Attributes).Days.Any(d => d.DayOfWeek == a.DayOfWeek) ? date + ((ScheduleAttribute)s.Attributes).Days.Single(d => d.DayOfWeek == a.DayOfWeek).StartTime : null) : null,
                                    EndTime = ((ScheduleAttribute)s.Attributes).Days != null ? (((ScheduleAttribute)s.Attributes).Days.Any(d => d.DayOfWeek == a.DayOfWeek) ? date + ((ScheduleAttribute)s.Attributes).Days.Single(d => d.DayOfWeek == a.DayOfWeek).EndTime : null) : null
                                })
                                .ToList(),

                                Title = s.Title,
                                EnableDropIn = ((ScheduleAttribute)s.Attributes).EnableDropIn,
                                DropInLable = ((ScheduleAttribute)s.Attributes).DropInLable,
                                MinimumEnrollment = ((ScheduleAttribute)s.Attributes).MinimumEnrollment,
                                Capacity = ((ScheduleAttribute)s.Attributes).Capacity.ToString(),
                                ShowCapacityLeft = ((ScheduleAttribute)s.Attributes).ShowCapacityLeft,
                                ShowCapacityLeftNumber = !((ScheduleAttribute)s.Attributes).ShowCapacityLeft ? Constants.CapacityLeftNumber_Default : ((ScheduleAttribute)s.Attributes).ShowCapacityLeftNumber,
                                Tuitions = s.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted).Select(c =>
                                new Tuition
                                {
                                    Id = c.Id,
                                    Capacity = c.Capacity.ToString(),
                                    HasEarlyBird = c.HasEarlyBird,
                                    Label = c.Name,
                                    Price = c.Amount,
                                    EarlyBirds = c.Attributes.EarlyBirds,
                                    Description = c.Description,
                                    IsProrate = c.Attributes.IsProrate,
                                    ProrateStartDate = c.Attributes.ProrateStartDate,
                                    ProrateSessionPrice = c.Attributes.ProrateSessionPrice,
                                    DropInName = c.Attributes.DropInName,
                                    DropInPrice = c.Attributes.DropInPrice,
                                    HasOrder = _orderItemBusiness.HasTutionOrderItems(c)
                                })
                                .ToList(),

                                Charges = s.Charges.Where(c => c.Category != ChargeDiscountCategory.EntryFee && !c.IsDeleted).Select(c =>
                                new Charges
                                {
                                    Id = c.Id,
                                    Capacity = c.Capacity.ToString(),
                                    Label = c.Name,
                                    Price = c.Amount,
                                    Category = c.Category,
                                    Description = c.Description,
                                    StartTime = new DateTime(1, 1, 1) + c.Attributes.StartTime,
                                    EndTime = new DateTime(1, 1, 1) + c.Attributes.EndTime,
                                    IsMandatory = c.Attributes.IsMandatory
                                })
                                .ToList(),

                                HasOrder = _programBusiness.HasActiveOrder(s),

                                IsCampOvernight = s.IsCampOvernight,

                                GenderItems = DropdownHelpers.ToSelectList<Genders>(),
                                SelectedGender = s.AttendeeRestriction.Gender.Value,

                                ContinueTypes = DropdownHelpers.ToSelectList<ContinueType>(),
                                SelectedContinueType = ((ScheduleAttribute)s.Attributes).ContinueType,

                                CapacityLeftNumbers = DropdownHelpers.SeedKeyValueNumbers(20),

                                RestrictionTypes = DropdownHelpers.ToSelectList<RestrictionType>(),
                                SelectedRestrictionType = s.AttendeeRestriction.RestrictionType,

                                Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null),
                                SelectedMinGrade = s.AttendeeRestriction.MinGrade,
                                SelectedMaxGrade = s.AttendeeRestriction.MaxGrade,

                                SelectedSession = ((ScheduleAttribute)s.Attributes).Occurances.ToString(),

                                SelectedMinAge = s.AttendeeRestriction.MinAge.HasValue ? s.AttendeeRestriction.MinAge.Value.ToString() : "0",
                                SelectedMaxAge = s.AttendeeRestriction.MaxAge.HasValue ? s.AttendeeRestriction.MaxAge.Value.ToString() : "0",

                                AppliesRestrictionAtTheProgramStart = s.AttendeeRestriction.ApplyAtProgramStart,

                                RegistrationPeriod = new RegistrationPeriodViewModel()
                                {
                                    RegisterStartDate = s.RegistrationPeriod.RegisterStartDate,
                                    RegisterEndDate = s.RegistrationPeriod.RegisterEndDate,
                                    RegisterStartTime = s.RegistrationPeriod.RegisterStartTime.HasValue ? date + s.RegistrationPeriod.RegisterStartTime.Value : date,
                                    RegisterEndTime = s.RegistrationPeriod.RegisterEndTime.HasValue ? date + s.RegistrationPeriod.RegisterEndTime.Value : date.AddHours(23).AddMinutes(59),
                                },

                                StartDate = s.StartDate,
                                EndDate = s.EndDate,

                                IsProrate = ((ScheduleAttribute)s.Attributes).IsProrate,

                                StartTime = ((ScheduleAttribute)s.Attributes).Days.Any() ? date + ((ScheduleAttribute)s.Attributes).Days.FirstOrDefault().StartTime : null,
                                EndTime = ((ScheduleAttribute)s.Attributes).Days.Any() ? date + ((ScheduleAttribute)s.Attributes).Days.FirstOrDefault().EndTime : null,
                            })
                            .ToList();
                    }
                    break;
            }

            return JsonNet(model);
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual JsonNetResult TournamentCreateEditStep2(int programId)
        {

            var model = new TournamentCreateEditModelStep2();

            var clubDomain = ActiveClub.Domain;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            model = program.ToViewModel<TournamentCreateEditModelStep2>();

            model.PageStep = ProgramPageStep.Step2;

            model.Capacities = SeedCapacity(Constants.MaximumCapacity);
            model.MinimumEnrollments = DropdownHelpers.SeedKeyValueNumbers(Constants.MaximumCapacity);
            model.Sessions = SeedSession(120);
            model.Ages = SeedAges(120);

            switch (model.PageMode)
            {
                case PageMode.Create:
                    {
                        model.Schedules = new List<ProgramScheduleViewModel>()
                        {
                            NewTournamentSchedule()
                        };

                        model.TourneySections = new List<TournamentSection>()
                        {
                            new TournamentSection()
                        };

                        model.TourneySchedules = new List<TournamentSchedule>()
                        {
                            new TournamentSchedule()
                        };

                        model.TournamentTypes = DropdownHelpers.ToSelectList<TournamentType>();
                    }
                    break;
                case PageMode.Edit:
                    {
                        DateTime date = new DateTime(0);

                        model.Schedules = program.ProgramSchedules.Where(s => !s.IsDeleted).Select(s =>
                            new ProgramScheduleViewModel
                            {
                                Id = s.Id,

                                Tuitions = s.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted).Select(c =>
                            new Tuition
                            {
                                Id = c.Id,
                                Capacity = c.Capacity.ToString(),
                                HasEarlyBird = c.HasEarlyBird,
                                Label = c.Name,
                                Price = c.Amount,
                                EarlyBirds = c.Attributes.EarlyBirds,
                                Description = c.Description,
                            })
                                .ToList(),

                                Charges = s.Charges.Where(c => c.Category != ChargeDiscountCategory.EntryFee && !c.IsDeleted).Select(c =>
                                new Charges
                                {
                                    Id = c.Id,
                                    Capacity = c.Capacity.ToString(),
                                    Label = c.Name,
                                    Price = c.Amount,
                                    Category = c.Category,
                                    Description = c.Description,
                                    StartTime = new DateTime(1, 1, 1) + (c.Attributes.StartTime != TimeSpan.MinValue ? c.Attributes.StartTime.Value : TimeSpan.FromTicks(0)),
                                    EndTime = new DateTime(1, 1, 1) + c.Attributes.EndTime,
                                    IsMandatory = c.Attributes.IsMandatory
                                })
                                .ToList(),

                                GenderItems = DropdownHelpers.ToSelectList<Genders>(),
                                SelectedGender = s.AttendeeRestriction.Gender.Value,

                                CapacityLeftNumbers = DropdownHelpers.SeedKeyValueNumbers(20),

                                RestrictionTypes = DropdownHelpers.ToSelectList<RestrictionType>(),
                                SelectedRestrictionType = s.AttendeeRestriction.RestrictionType,

                                Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null),
                                SelectedMinGrade = s.AttendeeRestriction.MinGrade,
                                SelectedMaxGrade = s.AttendeeRestriction.MaxGrade,

                                AppliesRestrictionAtTheProgramStart = s.AttendeeRestriction.ApplyAtProgramStart,

                                SelectedMinAge = s.AttendeeRestriction.MinAge.HasValue ? s.AttendeeRestriction.MinAge.Value.ToString() : "0",
                                SelectedMaxAge = s.AttendeeRestriction.MaxAge.HasValue ? s.AttendeeRestriction.MaxAge.Value.ToString() : "0",

                                RegistrationPeriod = new RegistrationPeriodViewModel()
                                {
                                    RegisterStartDate = s.RegistrationPeriod.RegisterStartDate,
                                    RegisterEndDate = s.RegistrationPeriod.RegisterEndDate
                                },

                                StartDate = s.StartDate,
                                EndDate = s.EndDate,
                            })
                            .ToList();

                        TournamentScheduleAttribute attribute = (TournamentScheduleAttribute)program.ProgramSchedules.FirstOrDefault().Attributes;

                        model.PrizeFund = attribute.PrizeFund;
                        model.FullEntries = attribute.FullEntries;
                        model.PrizeFundGuarantee = attribute.PrizeFundGuarantee;


                        if (attribute.Sections != null && attribute.Sections.Any())
                        {
                            model.TourneySections = attribute.Sections;
                        }
                        else
                        {
                            model.TourneySections = new List<TournamentSection>()
                            {
                                new TournamentSection()
                            };
                        }

                        if (attribute.Schedules != null && attribute.Schedules.Any())
                        {
                            model.TourneySchedules = attribute.Schedules;
                        }
                        else
                        {
                            model.TourneySchedules = new List<TournamentSchedule>()
                            {
                                new TournamentSchedule()
                            };
                        }

                        model.ByeNote = attribute.ByeNote;
                        model.LastRoundBye = attribute.LastRoundBye;
                        model.MaxOfByes = attribute.MaxOfByes;
                        model.NumberOfRounds = attribute.NumberOfRounds;
                        model.Trophies = attribute.Trophies;
                        model.HideViewEntries = attribute.HideViewEntries;

                        model.TournamentType = attribute.TournamentType;
                        model.TournamentTypes = DropdownHelpers.ToSelectList<TournamentType>();

                    }
                    break;
            }

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.Program_Edit)]
        public virtual JsonNetResult SubscriptionCreateEditStep2(int programId)
        {
            var model = new SubscriptionCreateEditModelStep2();

            var clubDomain = ActiveClub.Domain;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            model = program.ToViewModel<SubscriptionCreateEditModelStep2>();

            model.PageStep = ProgramPageStep.Step2;

            model.ContinueTypes = DropdownHelpers.ToSelectList<ContinueType>();

            model.GenderItems = DropdownHelpers.ToSelectList<Genders>();

            model.RestrictionTypes = DropdownHelpers.ToSelectList<RestrictionType>();

            model.Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);

            model.Capacities = SeedCapacity(Constants.MaximumCapacity);

            model.Sessions = SeedSession(120);

            model.Ages = SeedAges(120);

            model.DaysBeforeBillingItems = SeedDaysBeforeBillingItems(30);

            var allDaysOfWeek = new List<ProgramScheduleDayViewModel>()
                {
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Monday },
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Tuesday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Wednesday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Thursday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Friday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Saturday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Sunday}
                };

            switch (model.PageMode)
            {
                case PageMode.Create:
                    {
                        model.SelectedMinAge = "0";
                        model.SelectedMaxAge = "0";
                        model.Capacity = "0";

                        model.Subscriptions = new List<SubscriptionViewModel>()
                        {
                            new SubscriptionViewModel
                            {
                              Id = new Random().Next(100000000, 999999999),
                              DaysBeforeBilling = "10",
                              Occurances = "1",
                            }
                        };

                        model.EarlyBirds = new List<EarlyBird>()
                        {
                            new EarlyBird { }
                        };
                        model.Days = allDaysOfWeek;
                    }
                    break;
                case PageMode.Edit:
                    {
                        DateTime date = new DateTime(0);

                        var defaultProgramSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted);
                        var defaultScheduleAttribute = (ScheduleSubscriptionAttribute)defaultProgramSchedule.Attributes;

                        if (defaultProgramSchedule.Charges.Any(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted))
                        {
                            model.RegistrationFee = defaultProgramSchedule.Charges.Last(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted).Amount;
                        }

                        model.StartDate = defaultProgramSchedule.StartDate;
                        model.EndDate = defaultProgramSchedule.EndDate;

                        model.IsProrate = defaultScheduleAttribute.IsProrate;
                        model.ProrateTime = defaultScheduleAttribute.ProrateTime;

                        var applicationFee = _programBusiness.GetProgramApplicationFee(program);

                        model.RegistrationFee = applicationFee != null ? applicationFee.Amount : (decimal?)(null);// defaultScheduleAttribute.RegistrationFee;

                        model.DueDate = defaultScheduleAttribute.DueDate;

                        model.Days = allDaysOfWeek;

                        var days = defaultScheduleAttribute.Days;

                        foreach (var item in model.Days)
                        {
                            if (days.Select(a => a.DayOfWeek).ToList().Contains(item.DayOfWeek))
                            {
                                item.IsChecked = true;
                                item.StartTime = days.Single(s => s.DayOfWeek == item.DayOfWeek).StartTime.HasValue ? new DateTime(2012, 01, 01) + days.Single(s => s.DayOfWeek == item.DayOfWeek).StartTime.Value : (DateTime?)null;
                                item.EndTime = days.Single(s => s.DayOfWeek == item.DayOfWeek).EndTime.HasValue ? new DateTime(2012, 01, 01) + days.Single(s => s.DayOfWeek == item.DayOfWeek).EndTime.Value : (DateTime?)null;
                            }
                        }

                        defaultScheduleAttribute.Days.Select(s =>
                        new ProgramScheduleDayViewModel
                        {
                            DayOfWeek = s.DayOfWeek,
                            //StartTime = s.StartTime,
                            IsChecked = true,
                        })
                        .ToList();

                        model.HasPayInFullOption = defaultScheduleAttribute.HasPayInFullOption;
                        model.FullPayDiscount = defaultScheduleAttribute.FullPayDiscount;
                        model.FullPayDiscountMessage = defaultScheduleAttribute.FullPayDiscountMessage;

                        model.EnableDropIn = defaultScheduleAttribute.EnableDropIn;
                        model.DropInSessionPrice = defaultScheduleAttribute.DropInSessionPrice;
                        model.DropInSessionLabel = defaultScheduleAttribute.DropInSessionLabel;

                        model.Subscriptions = defaultScheduleAttribute.Subscriptions.Select(t =>
                            new SubscriptionViewModel
                            {
                                Id = t.Id,
                                Amount = t.Amount,
                                DaysBeforeBilling = t.DaysBeforeBilling,
                                WeekDays = t.WeekDays,
                                PriceLabel = t.PriceLabel,
                                RepeatType = t.RepeatType,
                            })
                            .ToList();

                        var earlyBirds = new List<EarlyBird>() { new EarlyBird() };
                        if (applicationFee != null)
                        {
                            var chargeEarlyBirds = applicationFee.Attributes.EarlyBirds;
                            model.HasEarlyBird = applicationFee.HasEarlyBird;
                            earlyBirds = applicationFee.HasEarlyBird ? applicationFee.Attributes.EarlyBirds.Select(e =>
                                     new EarlyBird()
                                     {
                                         ExpireDate = e.ExpireDate,
                                         Price = e.Price,
                                         PriorDay = e.PriorDay
                                     })
                                    .ToList()
                                    : new List<EarlyBird>() { new EarlyBird() };
                        }

                        model.EarlyBirds = earlyBirds;

                        model.Capacity = defaultScheduleAttribute.Capacity.ToString();

                        model.RegistrationPeriod = new RegistrationPeriodViewModel()
                        {
                            RegisterStartDate = defaultProgramSchedule.RegistrationPeriod.RegisterStartDate,
                            RegisterEndDate = defaultProgramSchedule.RegistrationPeriod.RegisterEndDate,
                        };

                        model.SelectedGender = defaultProgramSchedule.AttendeeRestriction.Gender.Value;
                        model.SelectedContinueType = defaultScheduleAttribute.ContinueType;
                        model.SelectedRestrictionType = defaultProgramSchedule.AttendeeRestriction.RestrictionType;
                        model.AppliesRestrictionAtTheProgramStart = defaultProgramSchedule.AttendeeRestriction.ApplyAtProgramStart;

                        model.SelectedMinGrade = defaultProgramSchedule.AttendeeRestriction.MinGrade;
                        model.SelectedMaxGrade = defaultProgramSchedule.AttendeeRestriction.MaxGrade;

                        model.SelectedMinAge = defaultProgramSchedule.AttendeeRestriction.MinAge.HasValue ? defaultProgramSchedule.AttendeeRestriction.MinAge.Value.ToString() : "0";
                        model.SelectedMaxAge = defaultProgramSchedule.AttendeeRestriction.MaxAge.HasValue ? defaultProgramSchedule.AttendeeRestriction.MaxAge.Value.ToString() : "0";
                        model.ProgramScheduleId = defaultProgramSchedule.Id;
                    }
                    break;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual ActionResult SubscriptionCreateEditStep2(SubscriptionCreateEditModelStep2 model, bool isSubscriptionSchemaChanged)
        {
            SubscriptionStep2ValidationCheck(ModelState, model);

            Program program = _programBusiness.Get(model.Id);
            CheckResourceForAccess(program);


            if (isSubscriptionSchemaChanged && _orderSessionBusiness.HasActiveOrder(program))
            {
                return JsonNet(new { Status = false, Message = "Can't change schedule when program has order." });
            }

            if (ModelState.IsValid)
            {
                OperationStatus res = null;

                if (program.LastCreatedPage < ProgramPageStep.Step2)
                {
                    model.LastCreatedPage = ProgramPageStep.Step2;
                }

                program = model.ToModel(program);

                var currentSubscriptionItems = new List<ProgramSubscriptionItem>();

                if (program.ProgramSchedules.Any() && program.ProgramSchedules.Last(p => !p.IsDeleted).Attributes != null)
                {
                    currentSubscriptionItems = ((ScheduleSubscriptionAttribute)(program.ProgramSchedules.Last(p => !p.IsDeleted).Attributes)).SubscriptionItems;
                }

                var schedules = new List<ProgramSchedule>();

                ProgramSchedule programSchedule = new ProgramSchedule();

                var subscriptionScheduleItems = new List<SubscriptionSchedule>();

                programSchedule.Charges = new List<Charge>();

                if (model.RegistrationFee.HasValue)
                {
                    long chargeId = 0;

                    if (program.ProgramSchedules.Any(s => !s.IsDeleted) && program.ProgramSchedules.Last(s => !s.IsDeleted).Charges.Any(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted))
                    {
                        var charge = _programBusiness.GetProgramApplicationFee(program);

                        chargeId = charge.Id;
                    }

                    programSchedule.Charges.Add(
                        new Charge
                        {
                            Name = ChargeDiscountCategory.ApplicationFee.ToDescription(),
                            Amount = model.RegistrationFee.Value,
                            Category = ChargeDiscountCategory.ApplicationFee,
                            MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                            HasEarlyBird = model.HasEarlyBird,
                            Attributes = new ChargeAttribute()
                            {
                                IsMandatory = true,
                                EarlyBirds = model.HasEarlyBird ? model.EarlyBirds.Select(eb =>
                                 new EarlyBird
                                 {
                                     Price = eb.Price,
                                     ExpireDate = eb.ExpireDate,
                                     PriorDay = eb.PriorDay
                                 })
                                .ToList()
                                : null
                            }
                        });
                }

                Random rnd = new Random();

                subscriptionScheduleItems = model.Subscriptions.Select(s =>
                    new SubscriptionSchedule
                    {
                        Id = rnd.Next(10000, 99000),
                        Amount = s.Amount,
                        DaysBeforeBilling = s.DaysBeforeBilling,
                        Occurances = s.Occurances,
                        PriceLabel = s.PriceLabel,
                        RepeatType = s.RepeatType,
                        WeekDays = s.WeekDays.HasValue ? s.WeekDays.Value : 0
                    })
                    .ToList();

                foreach (var item in subscriptionScheduleItems)
                {
                    programSchedule.Charges.Add(
                        new Charge
                        {
                            //Id = item.Id,
                            Name = item.PriceLabel,
                            Amount = item.Amount,
                            Category = ChargeDiscountCategory.EntryFee,
                            MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                            HasEarlyBird = false,
                            Attributes = new SubscriptionChargeAttribute()
                            {
                                SubscriptionScheduleId = item.Id,
                                WeekDays = item.WeekDays
                            }
                        });
                }

                programSchedule.Attributes = new ScheduleSubscriptionAttribute()
                {
                    ContinueType = model.SelectedContinueType,
                    Occurances = model.SelectedContinueType == ContinueType.For ? int.Parse(model.SelectedSession) : 0,
                    IsProrate = model.IsProrate,
                    ProrateTime = model.ProrateTime,
                    HasPayInFullOption = model.HasPayInFullOption,
                    FullPayDiscount = model.FullPayDiscount,
                    FullPayDiscountMessage = model.FullPayDiscountMessage,
                    EnableDropIn = model.EnableDropIn,
                    DropInSessionLabel = model.DropInSessionLabel,
                    DropInSessionPrice = model.DropInSessionPrice,
                    Capacity = int.Parse(model.Capacity),

                    Subscriptions = subscriptionScheduleItems,

                    DueDate = model.DueDate.Value,
                    Days = model.Days.Where(d => d.IsChecked).ToList().Select(d =>
                        new ProgramScheduleDay
                        {
                            DayOfWeek = d.DayOfWeek,
                            EndTime = d.EndTime.HasValue ? d.EndTime.Value.TimeOfDay : (TimeSpan?)null,
                            StartTime = d.StartTime.HasValue ? d.StartTime.Value.TimeOfDay : (TimeSpan?)null,
                        })
                        .ToList(),

                    //SubscriptionItems = GenerateSubscriptionSchedules(model.StartDate.Value, model.EndDate.Value, model.Subscriptions.Max(a => a.Amount), model.DueDate.Value)
                };

                programSchedule.Title = string.Empty;

                programSchedule.AttendeeRestriction = new AttendeeRestriction();
                programSchedule.AttendeeRestriction.Gender = model.SelectedGender;
                programSchedule.AttendeeRestriction.MinGrade = model.SelectedMinGrade;
                programSchedule.AttendeeRestriction.MaxGrade = model.SelectedMaxGrade;
                programSchedule.AttendeeRestriction.ApplyAtProgramStart = model.AppliesRestrictionAtTheProgramStart;

                programSchedule.AttendeeRestriction.RestrictionType = model.SelectedRestrictionType;

                if (string.IsNullOrEmpty(model.SelectedMinAge) || model.SelectedMinAge.Equals("0"))
                {
                    programSchedule.AttendeeRestriction.MinAge = null;
                }
                else
                {
                    programSchedule.AttendeeRestriction.MinAge = int.Parse(model.SelectedMinAge);
                }

                if (string.IsNullOrEmpty(model.SelectedMaxAge) || model.SelectedMaxAge.Equals("0"))
                {
                    programSchedule.AttendeeRestriction.MaxAge = null;
                }
                else
                {
                    programSchedule.AttendeeRestriction.MaxAge = int.Parse(model.SelectedMaxAge);
                }

                programSchedule.StartDate = model.StartDate.Value;

                if (model.SelectedContinueType == ContinueType.Until)
                {
                    programSchedule.EndDate = model.EndDate.Value;
                }
                else
                {
                    programSchedule.EndDate = model.StartDate.Value;

                    for (int i = 0; i < int.Parse(model.SelectedSession); i++)
                    {
                        programSchedule.EndDate = programSchedule.EndDate.AddDays(7);
                    }
                }

                programSchedule.RegistrationPeriod = new RegistrationPeriod()
                {
                    RegisterStartDate = model.RegistrationPeriod.RegisterStartDate,
                    RegisterEndDate = model.RegistrationPeriod.RegisterEndDate,
                };

                programSchedule.ProgramId = program.Id;
                programSchedule.Id = model.ProgramScheduleId;

                schedules.Add(programSchedule);

                res = _programBusiness.Update(program, schedules);

                if (program.TypeCategory == ProgramTypeCategory.Subscription && isSubscriptionSchemaChanged)
                {
                    res = Ioc.ProgramSessionBusiness.Create(program, model.DueDate.Value);

                    //var SubscriptionItems = GenerateSubscriptionSchedules(model.StartDate.Value, model.EndDate.Value, model.Subscriptions.Max(a => a.Amount), model.DueDate.Value);
                }

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        public virtual ActionResult IsSubscriptionSchedulesPreviouslyEdited(long programId)
        {
            var result = Ioc.SubscriptionBusiness.IsSubscriptionSchedulesPreviouslyEdited(programId);

            return Json(new JResult { Status = result, Message = "" });
        }

        public virtual ActionResult SubscriptionCreateEditStep2Point5(long programId)
        {
            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var programSchedule = program.ProgramSchedules.Last();

            var scheduleAttribute = programSchedule.Attributes as ScheduleSubscriptionAttribute;

            var subscriptionParts = _programBusiness.GetScheduleParts(programId);
            var clubCurrency = Ioc.ClubBusiness.GetClubCurrency(this.GetCurrentClubDomain());

            var subscriptionItems = subscriptionParts.Select(s =>
                                    new
                                    {
                                        StartDate = s.StartDate,
                                        EndDate = s.EndDate,
                                        DueDate = s.DueDate,
                                        Amount = string.Join(", ", scheduleAttribute.Subscriptions.Select(p => CurrencyHelper.FormatCurrencyWithoutPenny(p.Amount, clubCurrency)).ToList())
                                    })
                                    .ToList();

            return JsonNet(subscriptionItems);
        }

        [HttpPost]
        public virtual ActionResult SubscriptionCreateEditStep2Point5(List<ProgramSubscriptionItem> model, long programId)
        {
            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var programSchedule = program.ProgramSchedules.Last();

            var scheduleAttribute = programSchedule.Attributes as ScheduleSubscriptionAttribute;

            scheduleAttribute.SubscriptionItems = model;

            programSchedule.AttributesSerialized = JsonConvert.SerializeObject(scheduleAttribute);

            var res = _programBusiness.Update(program);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        public virtual JsonNetResult BeforeAfterCareCreateEditStep2(int programId)
        {
            var model = new BeforeAfterCareCreateEditModelStep2();

            var clubDomain = ActiveClub.Domain;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            model = program.ToViewModel<BeforeAfterCareCreateEditModelStep2>();

            model.PageStep = ProgramPageStep.Step2;

            model.ContinueTypes = DropdownHelpers.ToSelectList<ContinueType>();

            model.GenderItems = DropdownHelpers.ToSelectList<Genders>();

            model.RestrictionTypes = DropdownHelpers.ToSelectList<RestrictionType>();

            model.Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);

            model.PaymentSchaduleItems = DropdownHelpers.ToSelectList<PaymentSchedule>("Select", null);
            model.PaymentSchaduleTypes = DropdownHelpers.ToSelectList<MonthlyPaymentScheduleType>("Select", null);

            model.Sessions = SeedSession(120);

            model.Ages = SeedAges(120);

            model.PunchcardSessions = SeedPunchcardSessions(50);

            model.DaysBeforeBillingItems = SeedDaysBeforeBillingItems(30);
            var allDaysOfWeek = new List<BeforAfterCareScheduleDaysViewModel>()
                {
                    new BeforAfterCareScheduleDaysViewModel{DayOfWeek = DayOfWeek.Monday },
                    new BeforAfterCareScheduleDaysViewModel{DayOfWeek = DayOfWeek.Tuesday},
                    new BeforAfterCareScheduleDaysViewModel{DayOfWeek = DayOfWeek.Wednesday},
                    new BeforAfterCareScheduleDaysViewModel{DayOfWeek = DayOfWeek.Thursday},
                    new BeforAfterCareScheduleDaysViewModel{DayOfWeek = DayOfWeek.Friday},
                    new BeforAfterCareScheduleDaysViewModel{DayOfWeek = DayOfWeek.Saturday},
                    new BeforAfterCareScheduleDaysViewModel{DayOfWeek = DayOfWeek.Sunday}
                };
            switch (model.PageMode)
            {
                case PageMode.Create:
                    {
                        model.SelectedMinAge = "0";
                        model.SelectedMaxAge = "0";
                        model.Days = allDaysOfWeek;

                        //set default for get first installment in registration time
                        model.GetPaymentAtRefister = true;
                    }
                    break;
                case PageMode.Edit:
                    {
                        var programParts = _programBusiness.GetScheduleParts(program.Id);

                        model.ProgramScheduleParts = programParts.Select(p => new ProgramSchedulePartModel
                        {
                            StartDate = p.StartDate,
                            EndDate = p.EndDate,
                            Id = p.Id

                        }).ToList();

                        if (model.ProgramScheduleParts.Any())
                        {
                            var minStartDate = model.ProgramScheduleParts.Min(p => p.StartDate);
                            var maxEndDate = model.ProgramScheduleParts.Max(p => p.EndDate);

                            model.FirstProgramPart = model.ProgramScheduleParts.First(p => p.StartDate == minStartDate);
                            model.LastProgramPart = model.ProgramScheduleParts.First(p => p.EndDate == maxEndDate);
                        }

                        model.HasProgramActiveOrder = _orderSessionBusiness.HasActiveOrder(program);
                        DateTime date = new DateTime(0);

                        var defaultProgramSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both);
                        var defaultScheduleAttribute = (ScheduleAfterBeforeCareAttribute)defaultProgramSchedule.Attributes;

                        model.StartDate = defaultProgramSchedule.StartDate;
                        model.EndDate = defaultProgramSchedule.EndDate;

                        model.HasProrate = defaultScheduleAttribute.IsProrate;
                        model.ProrateTime = defaultScheduleAttribute.ProrateTime;
                        model.Days = allDaysOfWeek;
                        var days = defaultScheduleAttribute.Days;

                        //get program schedules
                        var beforeschedule = program.ProgramSchedules.FirstOrDefault(p => p.ScheduleMode == TimeOfClassFormation.AM);
                        var Afterchedule = program.ProgramSchedules.FirstOrDefault(p => p.ScheduleMode == TimeOfClassFormation.PM);
                        var ComboSchedule = program.ProgramSchedules.FirstOrDefault(p => p.ScheduleMode == TimeOfClassFormation.Both);

                        model.BeforeProgramScheduleId = beforeschedule != null ? beforeschedule.Id : 0;
                        model.AfterProgramScheduleId = Afterchedule != null ? Afterchedule.Id : 0;
                        model.ComboProgramScheduleId = ComboSchedule != null ? ComboSchedule.Id : 0;

                        model.IsFreezedAfterProgramSchedule = Afterchedule != null ? Afterchedule.IsFreezed : false;
                        model.IsFreezedBeforeProgramSchedule = beforeschedule != null ? beforeschedule.IsFreezed : false;
                        model.IsFreezedComboProgramSchedule = ComboSchedule != null ? ComboSchedule.IsFreezed : false;

                        foreach (var item in model.Days)
                        {
                            if (days.Select(a => a.DayOfWeek).ToList().Contains(item.DayOfWeek))
                            {
                                item.IsChecked = true;
                                if (program.ProgramSchedules.Any(p => !p.IsDeleted && p.ScheduleMode == TimeOfClassFormation.AM))
                                {
                                    var beforeScheduleAttribute = (ScheduleAfterBeforeCareAttribute)beforeschedule.Attributes;
                                    var beforeDays = beforeScheduleAttribute.Days;
                                    item.MorningStartTime = beforeDays.Single(s => s.DayOfWeek == item.DayOfWeek).StartTime.HasValue ? new DateTime(2012, 01, 01) + beforeDays.Single(s => s.DayOfWeek == item.DayOfWeek).StartTime.Value : (DateTime?)null;
                                    item.MorningEndTime = beforeDays.Single(s => s.DayOfWeek == item.DayOfWeek).EndTime.HasValue ? new DateTime(2012, 01, 01) + beforeDays.Single(s => s.DayOfWeek == item.DayOfWeek).EndTime.Value : (DateTime?)null;
                                }

                                if (program.ProgramSchedules.Any(p => !p.IsDeleted && p.ScheduleMode == TimeOfClassFormation.PM))
                                {
                                    var AfterScheduleAttribute = (ScheduleAfterBeforeCareAttribute)Afterchedule.Attributes;
                                    var AfterDays = AfterScheduleAttribute.Days;
                                    item.AfternoonStartTime = AfterDays.Single(s => s.DayOfWeek == item.DayOfWeek).StartTime.HasValue ? new DateTime(2012, 01, 01) + AfterDays.Single(s => s.DayOfWeek == item.DayOfWeek).StartTime.Value : (DateTime?)null;
                                    item.AfternoonEndTime = AfterDays.Single(s => s.DayOfWeek == item.DayOfWeek).EndTime.HasValue ? new DateTime(2012, 01, 01) + AfterDays.Single(s => s.DayOfWeek == item.DayOfWeek).EndTime.Value : (DateTime?)null;
                                }

                            }
                        }

                        defaultScheduleAttribute.Days.Select(s =>
                        new ProgramScheduleDayViewModel
                        {
                            DayOfWeek = s.DayOfWeek,
                            //StartTime = s.StartTime,
                            IsChecked = true,
                        })
                        .ToList();

                        GetProgramAttributeValues(program, model);
                    }
                    break;
                default:
                    break;
            }

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult BeforeAfterCareCreateEditStep2(BeforeAfterCareCreateEditModelStep2 model, bool isSchemaChanged, bool isSchedulePartsChanged)
        {
            BeforeAfterCareStep2ValidationCheck(ModelState, model);

            var program = _programBusiness.Get(model.Id);
            CheckResourceForAccess(program);

            var editMode = program.LastCreatedPage < ProgramPageStep.Step2 ? false : true;

            if (program.LastCreatedPage < ProgramPageStep.Step2)
            {
                model.LastCreatedPage = ProgramPageStep.Step2;
            }

            if (!model.IsAfterSchool && !model.IsBeforeSchool && !model.IsCombo)
            {
                return JsonNet(new { Status = false, Message = Jumbula.Common.Constants.Validation.NumberOfSelectedProgram });
            }

            if (ModelState.IsValid)
            {
                if (model.NumberOfClassDays != null && model.NumberOfClassDays.All(c => c.Value != 1))
                {
                    return JsonNet(new { Status = false, Message = Jumbula.Common.Constants.Validation.NumberOfSelectedDays });
                }

                OperationStatus res = null;
                program = model.ToModel(program);

                var schedules = new List<ProgramSchedule>();

                if (model.IsBeforeSchool)
                {
                    var beforSchedule = program.ProgramSchedules.FirstOrDefault(s => s.ScheduleMode == TimeOfClassFormation.AM);
                    var beforScheduleAttribute = (ScheduleAfterBeforeCareAttribute)beforSchedule?.Attributes;

                    decimal fullPayDiscount = 0;
                    var capacitiy = 0;

                    if (beforScheduleAttribute != null)
                    {
                        fullPayDiscount = beforScheduleAttribute.FullPayDiscount;
                        capacitiy = beforScheduleAttribute.Capacity;
                    }

                    ProgramSchedule beforeProgramSchedule = new ProgramSchedule();

                    beforeProgramSchedule.ScheduleMode = TimeOfClassFormation.AM;
                    beforeProgramSchedule.Title = model.BeforeSchoolTitle;
                    beforeProgramSchedule.Description = model.BeforeSchoolDescription;
                    beforeProgramSchedule.IsFreezed = beforSchedule != null ? beforSchedule.IsFreezed : false;

                    beforeProgramSchedule = SetBeforeAfterCareSchedules(beforeProgramSchedule, program, model, fullPayDiscount, capacitiy);
                    beforeProgramSchedule.ProgramId = program.Id;
                    beforeProgramSchedule.Id = model.BeforeProgramScheduleId;

                    schedules.Add(beforeProgramSchedule);
                }

                if (model.IsAfterSchool)
                {
                    var afterSchedule = program.ProgramSchedules.FirstOrDefault(s => s.ScheduleMode == TimeOfClassFormation.PM);
                    var afterScheduleAttribute = (ScheduleAfterBeforeCareAttribute)afterSchedule?.Attributes;

                    ProgramSchedule afterProgramSchedule = new ProgramSchedule();
                    decimal fullPayDiscount = 0;
                    var capacity = 0;
                    if (afterScheduleAttribute != null)
                    {
                        fullPayDiscount = afterScheduleAttribute.FullPayDiscount;
                        capacity = afterScheduleAttribute.Capacity;
                    }

                    afterProgramSchedule.ScheduleMode = TimeOfClassFormation.PM;
                    afterProgramSchedule.Title = model.AfterSchoolTitle;
                    afterProgramSchedule.Description = model.AfterSchoolDescription;
                    afterProgramSchedule.IsFreezed = afterSchedule != null ? afterSchedule.IsFreezed : false;

                    afterProgramSchedule = SetBeforeAfterCareSchedules(afterProgramSchedule, program, model, fullPayDiscount, capacity);
                    afterProgramSchedule.ProgramId = program.Id;
                    afterProgramSchedule.Id = model.AfterProgramScheduleId;
                    schedules.Add(afterProgramSchedule);
                }

                if (model.IsCombo)
                {
                    var comboSchedule = program.ProgramSchedules.FirstOrDefault(s => s.ScheduleMode == TimeOfClassFormation.Both);
                    var comboScheduleAttribute = (ScheduleAfterBeforeCareAttribute)comboSchedule?.Attributes;

                    decimal fullPayDiscount = 0;

                    if (comboScheduleAttribute != null)
                    {
                        fullPayDiscount = comboScheduleAttribute.FullPayDiscount;
                    }

                    ProgramSchedule comboProgramSchedule = new ProgramSchedule();
                    comboProgramSchedule.ScheduleMode = TimeOfClassFormation.Both;
                    comboProgramSchedule.Title = model.ComboTitle;
                    comboProgramSchedule.Description = model.ComboDescription;
                    comboProgramSchedule.IsFreezed = comboSchedule != null ? comboSchedule.IsFreezed : false; 

                    comboProgramSchedule = SetBeforeAfterCareSchedules(comboProgramSchedule, program, model, fullPayDiscount);
                    comboProgramSchedule.ProgramId = program.Id;
                    comboProgramSchedule.Id = model.ComboProgramScheduleId;
                    schedules.Add(comboProgramSchedule);
                }

                res = _programBusiness.Update(program, schedules, model.NumberOfClassDays);

                var deletedSchedules = program.ProgramSchedules.Where(s => s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both);

                if (deletedSchedules.Any())
                {
                    foreach (var schedule in deletedSchedules)
                    {
                        _programSessionBusiness.DeleteLogicaly(schedule);
                    }
                }

                if (res.Status && (isSchemaChanged || isSchedulePartsChanged))
                {
                    var orderItems = program.ProgramSchedules.Where(s => !s.IsDeleted).SelectMany(s => _orderItemBusiness.GetInitiatedScheduleOrderitems(s));

                    if (orderItems.Any())
                    {
                        orderItems.ToList().ForEach(o => o.ItemStatus = OrderItemStatusCategories.deleted);
                    }
                }

                if (isSchemaChanged)
                {
                    res = _programSessionBusiness.CreateScheduleSessions(schedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both).ToList());
                }
                else
                {
                    _programBusiness.CheckAndAddSecheduleProgramSession(program);

                    _programSessionBusiness.UpdateProgramSessionsTime(program);
                }

                if (isSchedulePartsChanged)
                {
                    var defaultProgramschedule = program.ProgramSchedules.FirstOrDefault(p => !p.IsDeleted);
                    if (defaultProgramschedule != null)
                    {
                        var defaultProgramscheduleAttribiute =
                            (ScheduleAfterBeforeCareAttribute)defaultProgramschedule.Attributes;
                    }

                    if (model.StartDate != null)
                    {
                        var programStartTime = model.StartDate.Value;
                        if (model.EndDate != null)
                        {
                            var programEndTime = model.EndDate.Value;
                            var paymentDuedate = model.SelectedDueDate;
                            var paymentMode = model.SelectedPaymentSchaduleMode.HasValue ? model.SelectedPaymentSchaduleMode.Value.ToString() : string.Empty;
                            var monthlyScheduleType = model.SelectedPaymentSchaduleType.HasValue ? model.SelectedPaymentSchaduleType.Value.ToString() : string.Empty;

                            res = _programBusiness.CreateScheduleParts(program, programStartTime, programEndTime, paymentMode, monthlyScheduleType, paymentDuedate, true);
                        }
                    }
                }
                else if (editMode && (model.IsProgramStartDateChanged || model.IsProgramEndDateChanged))
                {
                    var deletedSchedulePartsByEndDate = model.DeletedProgramSchedulePartsByEndDate != null ? model.DeletedProgramSchedulePartsByEndDate.Select(p => new ProgramSchedulePart
                    {
                        StartDate = p.StartDate,
                        EndDate = p.EndDate

                    }).ToList() : new List<ProgramSchedulePart>();

                    var deletedSchedulePartsByStartDate = model.DeletedProgramSchedulePartsByStartDate != null ? model.DeletedProgramSchedulePartsByStartDate.Select(p => new ProgramSchedulePart
                    {
                        StartDate = p.StartDate,
                        EndDate = p.EndDate

                    }).ToList() : new List<ProgramSchedulePart>();

                    res = _programSessionBusiness.UpdateSessionWithStartEndDateProgram(program, deletedSchedulePartsByEndDate, deletedSchedulePartsByStartDate, model.IsProgramStartDateChanged, model.IsProgramEndDateChanged);
                }

                return JsonNet(res);
            }

            return JsonFormResponse();
        }
        [HttpGet]
        public virtual JsonNetResult GetBeforeAfterCareCreateEditStep2Fees(long programId, bool isSchedulePartsChanged)
        {
            var model = new BeforeAfterCareAddEditStep2FeesViewModel();
            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var defaultSchedule = program.ProgramSchedules.LastOrDefault(s => !s.IsDeleted);

            if (defaultSchedule != null)
            {
                var defaultScheduleAttribute = defaultSchedule.Attributes as ScheduleAfterBeforeCareAttribute;

                model.Capacities = SeedCapacity(Constants.MaximumCapacity);
                model.AfterCapacity = "0";
                model.BeforeCapacity = "0";

                model.isSchedulePartsChanged = isSchedulePartsChanged;
                if (defaultScheduleAttribute != null)
                {
                    model.IsDropIn = defaultScheduleAttribute.EnableDropIn;
                    model.IsFullPay = defaultScheduleAttribute.HasPayInFullOption;

                    model.NumberOfClassDays = defaultScheduleAttribute.NumberOfClassDays;
                }
            }

            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted);

            model.HasBeforeSchedule = programSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.AM);
            model.HasAfterSchedule = programSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.PM);
            model.HasComboSchedule = programSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.Both);

            if (model.HasBeforeSchedule)
            {
                var beforSchedule = programSchedules.FirstOrDefault(s => s.ScheduleMode == TimeOfClassFormation.AM);
                var beforScheduleAttribute = (ScheduleAfterBeforeCareAttribute)beforSchedule.Attributes;

                model.BeforeFullPayDiscount = beforScheduleAttribute.FullPayDiscount > 0 ? beforScheduleAttribute.FullPayDiscount : (decimal?)null;
                model.BeforeCapacity = beforScheduleAttribute.Capacity.ToString();
            }

            if (model.HasAfterSchedule)
            {
                var afterSchedule = programSchedules.FirstOrDefault(s => s.ScheduleMode == TimeOfClassFormation.PM);
                var afterScheduleAttribute = (ScheduleAfterBeforeCareAttribute)afterSchedule.Attributes;

                model.AfterFullPayDiscount = afterScheduleAttribute.FullPayDiscount > 0 ? afterScheduleAttribute.FullPayDiscount : (decimal?)null;
                model.AfterCapacity = afterScheduleAttribute.Capacity.ToString();
            }

            if (model.HasComboSchedule)
            {
                var comboSchedule = programSchedules.FirstOrDefault(s => s.ScheduleMode == TimeOfClassFormation.Both);
                var comboScheduleAttribute = (ScheduleAfterBeforeCareAttribute)comboSchedule.Attributes;

                model.ComboFullPayDiscount = comboScheduleAttribute.FullPayDiscount > 0 ? comboScheduleAttribute.FullPayDiscount : (decimal?)null;
            }

            //set prices option for each schedules
            var schedules = program.ProgramSchedules;
            var allBeforeAfterFees = new List<BeforeAfterFeesItemViewModel>();
            foreach (var schedule in programSchedules)
            {
                var optionfeesModel = getScheduleOptionFess(schedule, model.NumberOfClassDays);
                allBeforeAfterFees.AddRange(optionfeesModel);
            }

            //set schedule prices for view
            model.BeforeFees = allBeforeAfterFees.Where(p => p.ScheduleMode == TimeOfClassFormation.AM).ToList();
            model.AfterFees = allBeforeAfterFees.Where(p => p.ScheduleMode == TimeOfClassFormation.PM).ToList();
            model.ComboFees = allBeforeAfterFees.Where(p => p.ScheduleMode == TimeOfClassFormation.Both).ToList();

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult BeforeAfterCareCreateEditStep2Fees(BeforeAfterCareAddEditStep2FeesViewModel model, long programId, bool ScheduleChargesChanged)
        {
            OperationStatus res = null;
            Program program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var schedules = new List<ProgramSchedule>();

            if (ModelState.IsValid)
            {
                if (model.HasBeforeSchedule)
                {
                    var beforSchedule = program.ProgramSchedules.Where(s => s.ScheduleMode == TimeOfClassFormation.AM).FirstOrDefault();
                    var beforScheduleAttribute = (ScheduleAfterBeforeCareAttribute)beforSchedule.Attributes;
                    beforScheduleAttribute.Capacity = int.Parse(model.BeforeCapacity);

                    if (model.IsFullPay)
                    {
                        beforScheduleAttribute.FullPayDiscount = model.BeforeFullPayDiscount.HasValue ? model.BeforeFullPayDiscount.Value : 0;
                    }
                    else
                    {
                        beforScheduleAttribute.FullPayDiscount = 0;
                    }

                    beforSchedule.Attributes = beforScheduleAttribute;
                    schedules.Add(beforSchedule);
                }

                if (model.HasAfterSchedule)
                {
                    var afterSchedule = program.ProgramSchedules.Where(s => s.ScheduleMode == TimeOfClassFormation.PM).FirstOrDefault();
                    var afterScheduleAttribute = (ScheduleAfterBeforeCareAttribute)afterSchedule.Attributes;
                    afterScheduleAttribute.Capacity = int.Parse(model.AfterCapacity);

                    if (model.IsFullPay)
                    {
                        afterScheduleAttribute.FullPayDiscount = model.AfterFullPayDiscount.HasValue ? model.AfterFullPayDiscount.Value : 0;
                    }
                    else
                    {
                        afterScheduleAttribute.FullPayDiscount = 0;
                    }

                    afterSchedule.Attributes = afterScheduleAttribute;
                    schedules.Add(afterSchedule);
                }

                if (model.HasComboSchedule)
                {
                    var comboSchedule = program.ProgramSchedules.Where(s => s.ScheduleMode == TimeOfClassFormation.Both).FirstOrDefault();
                    var comboScheduleAttribute = (ScheduleAfterBeforeCareAttribute)comboSchedule.Attributes;

                    if (model.IsFullPay)
                    {
                        comboScheduleAttribute.FullPayDiscount = model.ComboFullPayDiscount.HasValue ? model.ComboFullPayDiscount.Value : 0;
                    }
                    else
                    {
                        comboScheduleAttribute.FullPayDiscount = 0;
                    }

                    comboSchedule.Attributes = comboScheduleAttribute;
                    schedules.Add(comboSchedule);
                }

                //create schedule part for program
                var programScheduleParts = _programBusiness.GetScheduleParts(program.Id);
                if (model.isSchedulePartsChanged || programScheduleParts.Count == 0)
                {
                    var defaultProgramschedule = program.ProgramSchedules.Where(p => !p.IsDeleted).FirstOrDefault();
                    var defaultProgramscheduleAttribiute = (ScheduleAfterBeforeCareAttribute)defaultProgramschedule.Attributes;

                    var programStartTime = defaultProgramschedule.StartDate.Date;
                    var programEndTime = defaultProgramschedule.EndDate.Date;
                    var paymentDuedate = defaultProgramscheduleAttribiute.PaymentDueDate;
                    var paymentMode = defaultProgramscheduleAttribiute.PaymentScheduleMode;
                    var monthlyScheduleType = defaultProgramscheduleAttribiute.MonthlyScheduleType;

                    res = _programBusiness.CreateScheduleParts(program, programStartTime, programEndTime, paymentMode, monthlyScheduleType, paymentDuedate, false);
                }

                //Add charge for each schedule
                var allBeforeAfterFees = new List<BeforeAfterFeesItemViewModel>();
                if (model.BeforeFees != null)
                {
                    allBeforeAfterFees.AddRange(model.BeforeFees);
                }
                if (model.AfterFees != null)
                {
                    allBeforeAfterFees.AddRange(model.AfterFees);
                }
                if (model.ComboFees != null)
                {
                    allBeforeAfterFees.AddRange(model.ComboFees);
                }

                addChargeToSchedule(allBeforeAfterFees, program, model.isSchedulePartsChanged, ScheduleChargesChanged);

                res = _programBusiness.Update(program, schedules);

                return JsonNet(res);

            }

            return JsonFormResponse();
        }

        [HttpGet]
        public virtual JsonNetResult BeforeAfterCareCreateEditStep2ScheduleList(long programId)
        {
            var model = new BeforeAfterCareStep2ScheduleListViewModel();
            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted);
            var programScheduleParts = _programBusiness.GetScheduleParts(programId);

            model.ScheduleParts = new List<BeforeAfterCareSchedulePartViewModel>();

            model.HasBeforeSchedule = programSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.AM);
            model.HasAfterSchedule = programSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.PM);
            model.HasComboSchedule = programSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.Both);

            var firstPartDate = programScheduleParts.Min(p => p.StartDate);
            var lastPartDate = programScheduleParts.Max(p => p.EndDate);
            foreach (var part in programScheduleParts)
            {
                var schedulePart = new BeforeAfterCareSchedulePartViewModel();
                var partCharges = _programBusiness.GetSchedulePartCharges(part.Id);

                schedulePart.Id = part.Id;
                schedulePart.StartDate = part.StartDate;
                schedulePart.EndDate = part.EndDate;
                schedulePart.DueDate = part.DueDate;
                schedulePart.IsFirstPartOfProgram = part.StartDate == firstPartDate;
                schedulePart.IsLastPartOfProgram = part.EndDate == lastPartDate;

                schedulePart.schedulePartCharges = new List<SchedulePartChargeModel>();

                foreach (var partCharge in partCharges)
                {
                    var schedule = program.ProgramSchedules.Where(s => !s.IsDeleted).SelectMany(s => s.Charges.Where(c => c.Id == partCharge.ChargeId && !c.IsDeleted).Select(c => c.ProgramSchedule)).FirstOrDefault();
                    var schedulePartCharge = new SchedulePartChargeModel();

                    schedulePartCharge.Id = partCharge.Id;
                    schedulePartCharge.ChargeId = partCharge.ChargeId;
                    schedulePartCharge.Amount = partCharge.ChargeAmount;
                    if (schedule != null) schedulePartCharge.ScheduleMode = schedule.ScheduleMode;

                    schedulePart.schedulePartCharges.Add(schedulePartCharge);
                }

                model.ScheduleParts.Add(schedulePart);
            }

            return JsonNet(model);

        }
        [HttpPost]
        public virtual ActionResult BeforeAfterCareCreateEditStep2ScheduleList(BeforeAfterCareStep2ScheduleListViewModel model, long programId)
        {
            OperationStatus res = null;
            var editedScheduleParts = new List<ProgramSchedulePart>();
            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var programSchedules = program.ProgramSchedules.Where(s => !s.IsDeleted);
            var programScheduleParts = _programBusiness.GetScheduleParts(programId);

            //updated schedule part and charges
            foreach (var schedulePart in model.ScheduleParts)
            {
                var orginalSchedulePart = programScheduleParts.Where(p => p.Id == schedulePart.Id).FirstOrDefault();

                orginalSchedulePart.StartDate = schedulePart.StartDate;
                orginalSchedulePart.EndDate = schedulePart.EndDate;
                orginalSchedulePart.DueDate = schedulePart.DueDate;

                foreach (var charge in schedulePart.schedulePartCharges)
                {
                    var orginalPartCharge = orginalSchedulePart.Charges.Where(c => c.Id == charge.Id).FirstOrDefault();

                    orginalPartCharge.ChargeAmount = charge.Amount;
                }

                editedScheduleParts.Add(orginalSchedulePart);
            }

            res = _programBusiness.UpdateScheduleParts(editedScheduleParts);

            return JsonNet(new JResult(res));
        }
        [HttpPost]
        public virtual ActionResult IsProgramSchedulesChargeChanged(long programId, List<BeforeAfterFeesItemViewModel> newScheduleCharges)
        {
            var program = _programBusiness.Get(programId);

            var scheduleCharges = program.ProgramSchedules.SelectMany(s => s.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee)).ToList();

            var result = false;

            var changedCount = 0;

            if (scheduleCharges.Count == 0)
            {
                result = false;
            }
            else
            {
                if (scheduleCharges.Where(c => !c.IsDeleted).ToList().Count != newScheduleCharges.Count)
                {
                    result = true;
                }
                else
                {

                    foreach (var charge in newScheduleCharges)
                    {
                        if (scheduleCharges.Any(c => !c.IsDeleted && c.Id == charge.ChargeId))
                        {
                            var currentCharge = scheduleCharges.Where(c => !c.IsDeleted && c.Id == charge.ChargeId).FirstOrDefault();
                            if (currentCharge.Amount != charge.Amount || (currentCharge.ProgramSchedule.IsDeleted == true && !currentCharge.IsDeleted))
                            {
                                changedCount++;
                            }
                        }
                    }

                    //if charge is deleted but have schedulePartCharge we should generate programSchedulePartCharge
                    if (changedCount == 0)
                    {
                        foreach (var oldCharge in scheduleCharges.Where(c => c.IsDeleted))
                        {
                            var hasSchedulePartCharges = oldCharge.SchdulePartCharges;

                            if (hasSchedulePartCharges.Count > 0)
                            {
                                changedCount++;
                            }
                        }
                    }

                    if (changedCount > 0)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }

            return JsonNet(new { Changed = result, status = true });
        }
        [HttpPost]
        public virtual ActionResult IsProgramSchedulePartChanged(long programId, BeforeAfterCareCreateEditModelStep2 model)
        {
            var program = _programBusiness.Get(programId);

            if (program.ProgramSchedules.Count == 0)
            {
                return JsonNet(new { Changed = false, status = true });
            }
            else
            {
                if (!model.StartDate.HasValue || !model.EndDate.HasValue || !model.SelectedPaymentSchaduleMode.HasValue)
                {
                    return JsonNet(new { Changed = false, status = true });
                }

                if (model.SelectedPaymentSchaduleMode.HasValue && model.SelectedPaymentSchaduleMode == PaymentSchedule.Monthly && !model.SelectedPaymentSchaduleType.HasValue)
                {
                    return JsonNet(new { Changed = false, status = true });
                }
                else
                {
                    var startDate = model.StartDate.Value;
                    var endDate = model.EndDate.Value;
                    var scheduleMode = model.SelectedPaymentSchaduleMode.Value.ToString();
                    var scheduleMonthlyType = model.SelectedPaymentSchaduleType.HasValue ? model.SelectedPaymentSchaduleType.Value.ToString() : null;

                    var result = _programBusiness.IsProgramSchedulePartEdited(program, startDate, endDate, scheduleMode, scheduleMonthlyType, model.SelectedDueDate);

                    return JsonNet(new { Changed = result, status = true });
                }
            }
        }

        [HttpPost]
        public virtual ActionResult CheckOrderItemsRelationWithDeletedSchedulePart(long programId, BeforeAfterCareCreateEditModelStep2 model, DateTime startDate, DateTime endDate, bool isProgramStartDateChanged, bool isProgramEndDateChanged)
        {
            BeforeAfterCareStep2ValidationCheck(ModelState, model);
            var programDeletedSchedulePartsOfEitEndDate = _programBusiness.GetProgramDeletedScheduleParts(programId, endDate, true).ToList();
            var programDeletedSchedulePartsOfEditStartDate = _programBusiness.GetProgramDeletedScheduleParts(programId, startDate, false).ToList();
            var message = string.Empty;
            var deletedSchedules = string.Empty;

            var result = false;

            if (ModelState.IsValid)
            {
                var scheduleDates = new List<string>();
                var hasProgramDeletedSchedulePartOfEditEndDate = programDeletedSchedulePartsOfEitEndDate.Any();
                var hasProgramDeletedSchedulePartOfEditStartDate = programDeletedSchedulePartsOfEditStartDate.Any();

                if (isProgramStartDateChanged)
                {
                    if (hasProgramDeletedSchedulePartOfEditStartDate)
                    {
                        model.DeletedProgramSchedulePartsByStartDate = programDeletedSchedulePartsOfEditStartDate.Select(p => new ProgramSchedulePartModel
                        {
                            Id = p.Id,
                            StartDate = p.StartDate,
                            EndDate = p.EndDate,
                        }).ToList();

                        foreach (var item in programDeletedSchedulePartsOfEditStartDate)
                        {
                            scheduleDates.Add($"{item.StartDate.Date.ToString(Constants.DefaultDateFormat)}-{item.EndDate.Date.ToString(Constants.DefaultDateFormat)}");
                        }

                        message = _orderItemBusiness.GetOrderItemConfirmationIdMessage(programId, programDeletedSchedulePartsOfEditStartDate, startDate, false);
                    }
                    else
                    {
                        message = _orderItemBusiness.GetOrderItemConfirmationIdMessage(programId, programDeletedSchedulePartsOfEditStartDate, startDate, false, true);
                    }
                }


                if (isProgramEndDateChanged)
                {
                    message += !string.IsNullOrEmpty(message) ? "<hr/>" : message;

                    if (hasProgramDeletedSchedulePartOfEditEndDate)
                    {
                        model.DeletedProgramSchedulePartsByEndDate = programDeletedSchedulePartsOfEitEndDate.Select(p => new ProgramSchedulePartModel
                        {
                            Id = p.Id,
                            StartDate = p.StartDate,
                            EndDate = p.EndDate
                        }).ToList();
                        message += _orderItemBusiness.GetOrderItemConfirmationIdMessage(programId, programDeletedSchedulePartsOfEitEndDate, endDate, true);

                        foreach (var item in programDeletedSchedulePartsOfEitEndDate)
                        {
                            scheduleDates.Add($"{item.StartDate.Date.ToString(Constants.DefaultDateFormat)}-{item.EndDate.Date.ToString(Constants.DefaultDateFormat)}");
                        }
                    }
                    else
                    {
                        message += _orderItemBusiness.GetOrderItemConfirmationIdMessage(programId, programDeletedSchedulePartsOfEitEndDate, endDate, true, true);
                    }
                }

                deletedSchedules = scheduleDates.Any() ? string.Join(", ", scheduleDates) : string.Empty;
                var warrningMessage = Jumbula.Common.Constants.Messages.EditProgramDateWarrning + deletedSchedules + ". ";

                var hasDeletedSchedulePart = hasProgramDeletedSchedulePartOfEditEndDate || hasProgramDeletedSchedulePartOfEditStartDate;

                result = string.IsNullOrEmpty(message) ? false : true;
                return JsonNet(new { Status = result, Message = message, DeletedScheduleParts = warrningMessage, HasDeletedSchedulePart = hasDeletedSchedulePart, DeletedPartsByStartDate = model.DeletedProgramSchedulePartsByStartDate, DeletedPartsByEndDate = model.DeletedProgramSchedulePartsByEndDate });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        public virtual ActionResult ProgramHaveOrders(long programId, bool isSchemaChanged)
        {
            var program = _programBusiness.Get(programId);

            if (isSchemaChanged && _orderSessionBusiness.HasActiveOrder(program))
            {
                return JsonNet(new { Status = true });
            }

            return JsonNet(new { Status = false });
        }
        public virtual ActionResult IsScheduleHaveOrder(long programId, long scheduleId)
        {
            var programSchedule = _programBusiness.Get(programId).ProgramSchedules.FirstOrDefault(s => s.Id == scheduleId);

            if (_orderSessionBusiness.HasActiveOrderSession(programSchedule))
            {
                return JsonNet(new { Status = true });
            }
            return JsonNet(new { Status = false });
        }
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual JsonNetResult CalendarCreateEditStep2(int programId)
        {

            var model = new CalendarCreateEditModelStep2();

            var clubDomain = ActiveClub.Domain;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            model = program.ToViewModel<CalendarCreateEditModelStep2>();

            model.PageStep = ProgramPageStep.Step2;

            var allDaysOfWeek = new List<ProgramScheduleDayViewModel>()
                {
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Monday },
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Tuesday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Wednesday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Thursday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Friday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Saturday},
                    new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Sunday}
                };

            model.GenderItems = DropdownHelpers.ToSelectList<Genders>();

            model.RestrictionTypes = DropdownHelpers.ToSelectList<RestrictionType>();

            model.Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);

            model.Sessions = SeedSession(120);

            model.Ages = SeedAges(120);

            model.Capacities = SeedCapacity(Constants.MaximumCapacity);

            switch (model.PageMode)
            {
                case PageMode.Create:
                    {
                        model.Schedules = new List<ProgramScheduleViewModel>()
                        {
                           NewSchedule(),
                        };

                        model.SelectedMinAge = "0";
                        model.SelectedMaxAge = "0";
                    }
                    break;
                case PageMode.Edit:
                    {
                        DateTime date = new DateTime(0);

                        model.Schedules = program.ProgramSchedules.Where(s => !s.IsDeleted).Select(s =>
                            new ProgramScheduleViewModel
                            {
                                Id = s.Id,

                                Days = allDaysOfWeek.Select(a =>
                                new ProgramScheduleDayViewModel
                                {
                                    DayOfWeek = a.DayOfWeek,
                                    IsChecked = ((ScheduleAttribute)s.Attributes).Days != null ? ((ScheduleAttribute)s.Attributes).Days.Any(d => d.DayOfWeek == a.DayOfWeek) : false,
                                    StartTime = ((ScheduleAttribute)s.Attributes).Days != null ? (((ScheduleAttribute)s.Attributes).Days.Any(d => d.DayOfWeek == a.DayOfWeek) ? date + ((ScheduleAttribute)s.Attributes).Days.Single(d => d.DayOfWeek == a.DayOfWeek).StartTime : null) : null,
                                    EndTime = ((ScheduleAttribute)s.Attributes).Days != null ? (((ScheduleAttribute)s.Attributes).Days.Any(d => d.DayOfWeek == a.DayOfWeek) ? date + ((ScheduleAttribute)s.Attributes).Days.Single(d => d.DayOfWeek == a.DayOfWeek).EndTime : null) : null
                                })
                                .ToList(),

                                ContinueTypes = DropdownHelpers.ToSelectList<ContinueType>(),

                                SelectedContinueType = ((ScheduleAttribute)s.Attributes).ContinueType,

                                Title = s.Title,
                                Capacity = s.Attributes.Capacity.ToString(),
                                SelectedSession = ((ScheduleAttribute)s.Attributes).Occurances.ToString(),

                                StartDate = s.StartDate,
                                EndDate = s.EndDate,

                                Tuitions = new List<Tuition>()
                                {
                                    new Tuition
                                    {
                                        Id = s.Charges.FirstOrDefault().Id,
                                        Price = s.Charges.FirstOrDefault().Amount,
                                        Color = s.Charges.FirstOrDefault().Attributes.Color,
                                        HasEarlyBird = s.Charges.FirstOrDefault().HasEarlyBird,

                                        EarlyBirds = new List<EarlyBird>()
                                        {
                                            new EarlyBird()
                                            {
                                                Price = s.Charges.FirstOrDefault().Attributes.EarlyBirds != null ? s.Charges.FirstOrDefault().Attributes.EarlyBirds.FirstOrDefault().Price : 0,
                                                PriorDay = s.Charges.FirstOrDefault().Attributes.EarlyBirds != null ? s.Charges.FirstOrDefault().Attributes.EarlyBirds.FirstOrDefault().PriorDay: 0,
                                            }
                                        }
                                    }
                                }
                            })
                            .ToList();

                        var defaultProgramSchedule = program.ProgramSchedules.FirstOrDefault();
                        var defaultScheduleAttribute = (ScheduleAttribute)defaultProgramSchedule.Attributes;

                        model.RegistrationPeriod = new RegistrationPeriodViewModel()
                        {
                            RegisterStartDate = defaultProgramSchedule.RegistrationPeriod.RegisterStartDate,
                            RegisterEndDate = defaultProgramSchedule.RegistrationPeriod.RegisterEndDate,
                        };

                        model.SelectedGender = defaultProgramSchedule.AttendeeRestriction.Gender.Value;
                        model.SelectedContinueType = defaultScheduleAttribute.ContinueType;
                        model.SelectedRestrictionType = defaultProgramSchedule.AttendeeRestriction.RestrictionType;

                        model.SelectedMinGrade = defaultProgramSchedule.AttendeeRestriction.MinGrade;
                        model.SelectedMaxGrade = defaultProgramSchedule.AttendeeRestriction.MaxGrade;

                        model.SelectedMinAge = defaultProgramSchedule.AttendeeRestriction.MinAge.HasValue ? defaultProgramSchedule.AttendeeRestriction.MinAge.Value.ToString() : "0";
                        model.SelectedMaxAge = defaultProgramSchedule.AttendeeRestriction.MaxAge.HasValue ? defaultProgramSchedule.AttendeeRestriction.MaxAge.Value.ToString() : "0";
                    }
                    break;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual ActionResult CalendarCreateEditStep2(CalendarCreateEditModelStep2 model)
        {
            CalendarStep2ValidationCheck(ModelState, model);

            var allErrors = ModelState.Values.SelectMany(v => v.Errors);

            if (ModelState.IsValid)
            {
                OperationStatus res = null;

                Program program = _programBusiness.Get(model.Id);
                CheckResourceForAccess(program);

                if (program.LastCreatedPage < ProgramPageStep.Step2)
                {
                    model.LastCreatedPage = ProgramPageStep.Step2;
                }

                program = model.ToModel<Program>(program);

                var schedules = new List<ProgramSchedule>();

                foreach (var schedule in model.Schedules)
                {
                    ProgramSchedule programSchedule = new ProgramSchedule();

                    programSchedule.Id = schedule.Id;

                    programSchedule.Charges = new List<Charge>();

                    programSchedule.Charges = schedule.Tuitions.Select(s =>
                        new Charge
                        {
                            Id = s.Id,
                            Amount = s.Price.Value,
                            Category = ChargeDiscountCategory.EntryFee,
                            MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                            HasEarlyBird = s.HasEarlyBird,
                            Attributes = new ChargeAttribute
                            {
                                EarlyBirds = s.EarlyBirds,
                                Color = s.Color,
                            },
                        })
                        .ToList();


                    programSchedule.Attributes = new ScheduleAttribute()
                    {
                        Days = schedule.Days.Where(s => s.IsChecked).Select(d =>
                                new ProgramScheduleDay
                                {
                                    DayOfWeek = d.DayOfWeek,
                                    StartTime = d.StartTime.Value.TimeOfDay,
                                    EndTime = d.EndTime.Value.TimeOfDay
                                })
                                 .ToList(),

                        ContinueType = schedule.SelectedContinueType,
                        Occurances = int.Parse(schedule.SelectedSession),
                        Capacity = int.Parse(schedule.Capacity),

                        //IsProrate = schedule.IsProrate,
                    };

                    programSchedule.Title = schedule.Title;

                    programSchedule.AttendeeRestriction = new AttendeeRestriction();
                    programSchedule.AttendeeRestriction.Gender = model.SelectedGender;
                    programSchedule.AttendeeRestriction.MinGrade = model.SelectedMinGrade;
                    programSchedule.AttendeeRestriction.MaxGrade = model.SelectedMaxGrade;

                    programSchedule.AttendeeRestriction.RestrictionType = model.SelectedRestrictionType;

                    if (model.SelectedMinAge.Equals("0"))
                    {
                        programSchedule.AttendeeRestriction.MinAge = null;
                    }
                    else
                    {
                        programSchedule.AttendeeRestriction.MinAge = int.Parse(model.SelectedMinAge);
                    }

                    if (model.SelectedMaxAge.Equals("0"))
                    {
                        programSchedule.AttendeeRestriction.MaxAge = null;
                    }
                    else
                    {
                        programSchedule.AttendeeRestriction.MaxAge = int.Parse(model.SelectedMaxAge);
                    }

                    programSchedule.StartDate = schedule.StartDate.Value;

                    if (schedule.SelectedContinueType == ContinueType.Until)
                    {
                        programSchedule.EndDate = schedule.EndDate.Value;
                    }
                    else
                    {
                        programSchedule.EndDate = schedule.StartDate.Value;

                        for (int i = 0; i < int.Parse(schedule.SelectedSession); i++)
                        {
                            programSchedule.EndDate = programSchedule.EndDate.AddDays(7);
                        }
                    }

                    programSchedule.RegistrationPeriod = new RegistrationPeriod()
                    {
                        RegisterStartDate = model.RegistrationPeriod.RegisterStartDate,
                    };

                    programSchedule.Sessions = _programBusiness.GenerateSessions(programSchedule.StartDate, programSchedule.EndDate, ((ScheduleAttribute)programSchedule.Attributes).Days, ((ScheduleAttribute)programSchedule.Attributes).ContinueType, ((ScheduleAttribute)programSchedule.Attributes).Occurances);

                    programSchedule.ProgramId = program.Id;

                    schedules.Add(programSchedule);
                }

                res = _programBusiness.Update(program, schedules);


                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual ActionResult ProgramCreateEditStep2(ProgramCreateEditModelStep2 model)
        {
            Step2ValidationCheck(ModelState, model);

            if (ModelState.IsValid)
            {
                OperationStatus res = null;

                Program program = _programBusiness.Get(model.Id);
                CheckResourceForAccess(program);

                var isEditMode = program.ProgramSchedules.Any(s => !s.IsDeleted) ? true : false;


                if (program.LastCreatedPage < ProgramPageStep.Step2)
                {
                    model.LastCreatedPage = ProgramPageStep.Step2;
                }

                program = model.ToModel<Program>(program);

                var schedules = new List<ProgramSchedule>();

                foreach (var schedule in model.Schedules)
                {
                    ProgramSchedule programSchedule = new ProgramSchedule();

                    programSchedule.Id = schedule.Id;

                    programSchedule.Charges = schedule.Tuitions.Select(s =>
                        new Charge
                        {
                            Id = s.Id,
                            Amount = s.Price.Value,
                            Capacity = int.Parse(s.Capacity),
                            Category = ChargeDiscountCategory.EntryFee,
                            Description = s.Description,
                            MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                            Name = s.Label,
                            HasEarlyBird = s.EarlyBirds != null && s.EarlyBirds.Any(),
                            Attributes = new ChargeAttribute
                            {
                                EarlyBirds = s.EarlyBirds,
                                IsProrate = s.IsProrate,
                                ProrateStartDate = s.ProrateStartDate,
                                ProrateSessionPrice = s.ProrateSessionPrice,
                                DropInPrice = s.DropInPrice,
                                DropInName = s.DropInName
                            },
                        })
                        .ToList();

                    if (schedule.Charges != null)
                    {
                        foreach (var item in schedule.Charges)
                        {
                            programSchedule.Charges.Add(
                                new Charge
                                {
                                    Id = item.Id,
                                    Amount = item.Price,
                                    Capacity = int.Parse(item.Capacity),
                                    Category = item.Category,
                                    Description = item.Description,
                                    MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                                    Name = item.Label,
                                    Attributes = new ChargeAttribute { StartTime = item.StartTime.HasValue ? item.StartTime.Value.TimeOfDay : (TimeSpan?)null, EndTime = item.EndTime.HasValue ? item.EndTime.Value.TimeOfDay : (TimeSpan?)null, IsMandatory = item.IsMandatory },
                                });
                        }
                    }

                    programSchedule.AttendeeRestriction = new AttendeeRestriction();
                    programSchedule.AttendeeRestriction.Gender = schedule.SelectedGender;
                    programSchedule.AttendeeRestriction.MinGrade = schedule.SelectedMinGrade;
                    programSchedule.AttendeeRestriction.MaxGrade = schedule.SelectedMaxGrade;
                    programSchedule.AttendeeRestriction.ApplyAtProgramStart = schedule.AppliesRestrictionAtTheProgramStart;

                    programSchedule.IsCampOvernight = schedule.IsCampOvernight;

                    programSchedule.AttendeeRestriction.RestrictionType = schedule.SelectedRestrictionType;

                    if (schedule.SelectedMinAge.Equals("0"))
                    {
                        programSchedule.AttendeeRestriction.MinAge = null;
                    }
                    else
                    {
                        programSchedule.AttendeeRestriction.MinAge = int.Parse(schedule.SelectedMinAge);
                    }

                    if (schedule.SelectedMaxAge.Equals("0"))
                    {
                        programSchedule.AttendeeRestriction.MaxAge = null;
                    }
                    else
                    {
                        programSchedule.AttendeeRestriction.MaxAge = int.Parse(schedule.SelectedMaxAge);
                    }

                    DateTime date = new DateTime(0);

                    switch (program.TypeCategory)
                    {
                        case ProgramTypeCategory.Class:
                            {

                            }
                            break;
                        case ProgramTypeCategory.Camp:
                            {
                                schedule.Days = schedule.Days.Select(d =>
                                   new ProgramScheduleDayViewModel
                                   {
                                       DayOfWeek = d.DayOfWeek,
                                       IsChecked = d.IsChecked,
                                       StartTime = schedule.StartTime,
                                       EndTime = schedule.EndTime
                                   })
                                   .ToList();
                            }
                            break;
                        case ProgramTypeCategory.SeminarTour:
                            {

                            }
                            break;
                        case ProgramTypeCategory.Calendar:
                            break;
                        default:
                            break;
                    }

                    programSchedule.Attributes = new ScheduleAttribute()
                    {
                        Days = schedule.Days.Where(s => s.IsChecked).Select(d =>
                        new ProgramScheduleDay
                        {
                            DayOfWeek = d.DayOfWeek,
                            StartTime = d.StartTime.Value.TimeOfDay,
                            EndTime = d.EndTime.Value.TimeOfDay
                        }
                        )
                        .ToList(),

                        DropInLable = schedule.DropInLable,
                        EnableDropIn = schedule.EnableDropIn,
                        ContinueType = schedule.SelectedContinueType,
                        Occurances = int.Parse(schedule.SelectedSession),
                        IsProrate = schedule.IsProrate,
                        MinimumEnrollment = schedule.MinimumEnrollment,
                        Capacity = int.Parse(schedule.Capacity),
                        ShowCapacityLeft = schedule.ShowCapacityLeft,
                        ShowCapacityLeftNumber = (schedule.ShowCapacityLeft) ? schedule.ShowCapacityLeftNumber : 0
                    };

                    programSchedule.Title = schedule.Title;

                    programSchedule.StartDate = schedule.StartDate.Value;

                    if (schedule.SelectedContinueType == ContinueType.Until)
                    {
                        programSchedule.EndDate = schedule.EndDate.Value;
                    }
                    else
                    {
                        programSchedule.EndDate = schedule.StartDate.Value;

                        var dayOfWeeks = new List<DayOfWeek>();
                        foreach (var item in schedule.Days.Where(d => d.IsChecked))
                        {
                            dayOfWeeks.Add(item.DayOfWeek);
                        }
                        var sessionWeek = int.Parse(schedule.SelectedSession);
                        programSchedule.EndDate = _programBusiness.GetEndDateFromSessionWeeks(dayOfWeeks, schedule.StartDate.Value, sessionWeek);

                    }

                    programSchedule.RegistrationPeriod = new RegistrationPeriod()
                    {
                        RegisterStartDate = schedule.RegistrationPeriod.RegisterStartDate,
                        RegisterEndDate = schedule.RegistrationPeriod.RegisterEndDate,
                        RegisterStartTime = schedule.RegistrationPeriod.RegisterStartTime.HasValue ? schedule.RegistrationPeriod.RegisterStartTime.Value.TimeOfDay : new TimeSpan(0, 0, 0),
                        RegisterEndTime = schedule.RegistrationPeriod.RegisterEndTime.HasValue ? schedule.RegistrationPeriod.RegisterEndTime.Value.TimeOfDay : new TimeSpan(23, 59, 0)
                    };

                    if (program.TypeCategory == ProgramTypeCategory.Class)
                    {
                        if (schedule.IsScheduleChanged)
                        {
                            programSchedule.Sessions = _programBusiness.GenerateSessions(programSchedule.StartDate, programSchedule.EndDate, ((ScheduleAttribute)programSchedule.Attributes).Days, ((ScheduleAttribute)programSchedule.Attributes).ContinueType, ((ScheduleAttribute)programSchedule.Attributes).Occurances);
                        }
                        else
                        {
                            programSchedule.Sessions = program.ProgramSchedules.SingleOrDefault(p => p.Id == programSchedule.Id).Sessions;
                        }
                    }

                    programSchedule.ProgramId = program.Id;

                    schedules.Add(programSchedule);
                }

                res = _programBusiness.Update(program, schedules);

                if (res.Status && program.TypeCategory == ProgramTypeCategory.Camp)
                {
                    var charges = new List<Charge>();

                    foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
                    {
                        charges.AddRange(schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.ProgramSessions == null));
                    }
                    var scheduleNotChanged = program.ProgramSchedules.Where(s => model.Schedules.Where(m => !m.IsScheduleChanged).Select(p => p.Id).Contains(s.Id)).ToList();
                    var updatedSessions = new List<ProgramSession>();

                    foreach (var schedule in scheduleNotChanged)
                    {
                        var attribute = ((ScheduleAttribute)schedule.Attributes);
                        var days = attribute.Days;

                        var day = days.First();

                        var startTime = day.StartTime;
                        var endTime = day.EndTime;

                        var currentSchedule = program.ProgramSchedules.FirstOrDefault(s => s.Id == schedule.Id);

                        currentSchedule.Charges.Where(p => p.ProgramSessions != null).SelectMany(c => c.ProgramSessions).ToList().ForEach(s => s.EndDateTime = s.EndDateTime.Date + endTime.Value);
                        currentSchedule.Charges.Where(p => p.ProgramSessions != null).SelectMany(c => c.ProgramSessions).ToList().ForEach(s => s.StartDateTime = s.StartDateTime.Date + startTime.Value);

                        updatedSessions.AddRange(currentSchedule.Charges.Where(p => p.ProgramSessions != null).SelectMany(c => c.ProgramSessions).ToList());
                    }

                    var programSchedules = program.ProgramSchedules.Where(s => model.Schedules.Where(m => m.IsScheduleChanged).Select(p => p.Id).Contains(s.Id)).ToList();

                    if (programSchedules.Any() || updatedSessions.Any() || charges.Any())
                    {
                        var sessionResult = Ioc.ProgramSessionBusiness.CreateTutionSessions(programSchedules, charges, updatedSessions);
                    }
                }

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual ActionResult TournamentCreateEditStep2(TournamentCreateEditModelStep2 model)
        {
            TournamentStep2ValidationCheck(ModelState, model);

            var allErrors = ModelState.Values.SelectMany(v => v.Errors);

            if (ModelState.IsValid)
            {
                OperationStatus res = null;

                Program program = _programBusiness.Get(model.Id);
                CheckResourceForAccess(program);

                if (program.LastCreatedPage < ProgramPageStep.Step2)
                {
                    model.LastCreatedPage = ProgramPageStep.Step2;
                }

                program = model.ToModel<Program>(program);

                var schedules = new List<ProgramSchedule>();

                #region Schedule
                foreach (var schedule in model.Schedules)
                {
                    ProgramSchedule programSchedule = new ProgramSchedule();

                    programSchedule.Id = schedule.Id;

                    programSchedule.Charges = schedule.Tuitions.Select(s =>
                        new Charge
                        {
                            Id = s.Id,
                            Amount = s.Price.Value,
                            Capacity = int.Parse(s.Capacity),
                            Category = ChargeDiscountCategory.EntryFee,
                            Description = s.Description,
                            MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                            Name = s.Label,
                            HasEarlyBird = s.EarlyBirds != null && s.EarlyBirds.Any(),
                            Attributes = new ChargeAttribute { EarlyBirds = s.EarlyBirds },
                        })
                        .ToList();

                    if (schedule.Charges != null)
                    {
                        foreach (var item in schedule.Charges)
                        {
                            programSchedule.Charges.Add(
                                new Charge
                                {
                                    Id = item.Id,
                                    Amount = item.Price,
                                    Capacity = int.Parse(item.Capacity),
                                    Category = item.Category,
                                    Description = item.Description,
                                    MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                                    Name = item.Label,
                                    Attributes = new ChargeAttribute { StartTime = item.StartTime.HasValue ? item.StartTime.Value.TimeOfDay : TimeSpan.MinValue, EndTime = item.EndTime.HasValue ? item.EndTime.Value.TimeOfDay : (TimeSpan?)null, IsMandatory = item.IsMandatory },
                                });
                        }
                    }

                    programSchedule.AttendeeRestriction = new AttendeeRestriction();
                    programSchedule.AttendeeRestriction.Gender = schedule.SelectedGender;
                    programSchedule.AttendeeRestriction.MinGrade = schedule.SelectedMinGrade;
                    programSchedule.AttendeeRestriction.MaxGrade = schedule.SelectedMaxGrade;
                    programSchedule.AttendeeRestriction.ApplyAtProgramStart = schedule.AppliesRestrictionAtTheProgramStart;

                    programSchedule.AttendeeRestriction.RestrictionType = schedule.SelectedRestrictionType;

                    if (schedule.SelectedMinAge.Equals("0"))
                    {
                        programSchedule.AttendeeRestriction.MinAge = null;
                    }
                    else
                    {
                        programSchedule.AttendeeRestriction.MinAge = int.Parse(schedule.SelectedMinAge);
                    }

                    if (schedule.SelectedMaxAge.Equals("0"))
                    {
                        programSchedule.AttendeeRestriction.MaxAge = null;
                    }
                    else
                    {
                        programSchedule.AttendeeRestriction.MaxAge = int.Parse(schedule.SelectedMaxAge);
                    }

                    DateTime date = new DateTime(0);

                    programSchedule.Attributes = new TournamentScheduleAttribute()
                    {
                        FullEntries = model.FullEntries,
                        PrizeFund = model.PrizeFund,
                        PrizeFundGuarantee = model.PrizeFundGuarantee,

                        Schedules = model.TourneySchedules,
                        Sections = model.TourneySections,

                        ByeNote = model.ByeNote,
                        LastRoundBye = model.LastRoundBye,
                        MaxOfByes = model.MaxOfByes,
                        NumberOfRounds = model.NumberOfRounds,

                        TournamentType = model.TournamentType,
                        Trophies = model.Trophies,
                        HideViewEntries = model.HideViewEntries
                    };

                    programSchedule.RegistrationPeriod = new RegistrationPeriod()
                    {
                        RegisterStartDate = schedule.RegistrationPeriod.RegisterStartDate,
                        RegisterEndDate = schedule.RegistrationPeriod.RegisterEndDate
                    };

                    programSchedule.StartDate = model.TourneySchedules.Min(m => m.StartDate.Value);
                    programSchedule.EndDate = model.TourneySchedules.Max(m => m.EndDate.Value);

                    programSchedule.ProgramId = program.Id;

                    schedules.Add(programSchedule);
                }

                #endregion

                res = _programBusiness.Update(program, schedules);

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual JsonNetResult ProgramCreateEditStep3(int programId)
        {
            var model = new ProgramCreateEditModelStep3();

            bool isTeamChessTournament = false;
            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var season = program.Season;

            model = program.ToViewModel<ProgramCreateEditModelStep3>();

            model.PageStep = ProgramPageStep.Step3;

            var formTemplates = Ioc.ClubBusiness.GetFormTemplates(ActiveClub.Id, null);

            if (_programBusiness.IsTeamChessTourney(program))
            {
                isTeamChessTournament = true;
                if (!formTemplates.Any(f => f.FormType == FormType.Registration && f.Title.ToLower().Equals(Constants.Default_Team_ChessTournament_Form.ToLower())))
                {
                    Ioc.ClubBusiness.AddDefaultChessTeamTemplate(ActiveClub.Id);
                    formTemplates = Ioc.ClubBusiness.GetFormTemplates(ActiveClub.Id, null);
                }
            }

            model.FormTemplates = formTemplates.Where(w => w.FormType == FormType.Registration).Select(c =>
                new SelectKeyValue<string>
                {
                    Text = c.Title,
                    Value = c.Id.ToString()
                })
                .OrderBy(s => s.Value)
                .ToList();

            model.AllFollowUpForms = formTemplates.Where(f => f.FormType == FormType.FollowUp || f.FormType == FormType.UploadForm).Select(s =>
                new SelectKeyValue<string>
                {
                    Text = s.Title,
                    Value = s.Id.ToString()
                })
                .ToList();

            model.SelectedForms = program.ClubFormTemplates.Where(s => s.FormType == FormType.FollowUp || s.FormType == FormType.UploadForm).Select(s => s.Id.ToString()).ToList();

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            model.AllWaivers = club.ClubWaivers.Where(w => !w.IsDeleted).Select(w =>
                new SelectKeyValue<string>
                {
                    Value = w.Id.ToString(),
                    Text = w.Name
                })
                .ToList();

            switch (model.PageMode)
            {
                case PageMode.Create:
                    {
                        if (!isTeamChessTournament)
                        {
                            model.FormTemplate = Ioc.SeasonBusiness.GetSeasonRegistrationForm(season).Id.ToString();
                        }
                        else
                        {
                            model.FormTemplate = formTemplates.Where(w => w.FormType == FormType.Registration).SingleOrDefault(s => s.Title.ToLower().Equals(Constants.Default_Team_ChessTournament_Form.ToLower())).Id.ToString();
                        }

                        model.SelectedWaivers = Ioc.SeasonBusiness.GetSeasonWaivers(season).Select(w => w.Id.ToString()).ToList();
                    }
                    break;
                case PageMode.Edit:
                    {
                        model.FormTemplate = program.ClubFormTemplates.SingleOrDefault(f => f.FormType == FormType.Registration).Id.ToString();

                        model.SelectedWaivers = program.ClubWaivers.Select(c => c.Id.ToString()).ToList();
                    }
                    break;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual ActionResult ProgramCreateEditStep3(ProgramCreateEditModelStep3 model)
        {
            OperationStatus res = null;
            if (!model.FormTemplates.Any(c => c.Value == model.FormTemplate))
            {
                ModelState.AddModelError("model.formTemplate", "The registration form is required");
            }
            if (ModelState.IsValid)
            {
                var program = _programBusiness.Get(model.Id);
                CheckResourceForAccess(program);

                ClubFormTemplate formTemplates = null;
                if (program.ClubFormTemplates.Any(c => c.FormType == FormType.Registration))
                {
                    formTemplates = program.ClubFormTemplates.Single(c => c.FormType == FormType.Registration);
                }

                if (program.LastCreatedPage < ProgramPageStep.Step3)
                {
                    model.LastCreatedPage = ProgramPageStep.Step3;
                }

                program = model.ToModel<Program>(program);

                if (model.SelectedWaivers != null && model.SelectedWaivers.Any())
                {
                    var waivers = Ioc.ClubBusiness.GetWaivers(model.SelectedWaivers.Select(c => int.Parse(c))).ToList();

                    program.ClubWaivers.Clear();

                    program.ClubWaivers.AddEntities<ClubWaiver>(waivers);
                }
                else
                {
                    program.ClubWaivers.Clear();
                }

                var selectedForms = model.SelectedForms;

                if (selectedForms == null)
                {
                    selectedForms = new List<string>();
                }

                selectedForms.Add(model.FormTemplate);

                if (selectedForms != null && selectedForms.Any())
                {
                    selectedForms.Add(model.FormTemplate);

                    var forms = Ioc.ClubBusiness.GetFormTemplates(ActiveClub.Id, selectedForms.Select(c => int.Parse(c)), null);

                    program.ClubFormTemplates.Clear();

                    program.ClubFormTemplates.AddEntities<ClubFormTemplate>(forms);
                }
                else
                {
                    program.ClubFormTemplates.Clear();
                }
                if (formTemplates != null && (program.ClubFormTemplates == null || !program.ClubFormTemplates.Any(c => c.FormType == FormType.Registration)))
                {
                    program.ClubFormTemplates.Add(formTemplates);
                }
                res = _programBusiness.Update(program);

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected, Data = model.Id.ToString() });
            }

            return JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual JsonNetResult ProgramCreateEditStep4(int programId)
        {
            var model = new ProgramCreateEditModelStep4();

            var clubDomain = ActiveClub.Domain;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            model = program.ToViewModel<ProgramCreateEditModelStep4>();

            model.PageStep = ProgramPageStep.Step4;
            model.IsPartnerProvider = (ActiveClub.HasPartner && (!ActiveClub.IsSchool) && program.OutSourceSeasonId != null);
            if (model.IsPartnerProvider)
            {
                model.PartnerCommisionRate = (program.CustomFields != null) ? program.CustomFields.CommisionRate : null;
                model.PartnerLateCheckInPenalty = (program.CustomFields != null) ? program.CustomFields.LateCheckInPenalty : null;
                model.PartnerNoshowpenalty = (program.CustomFields != null) ? program.CustomFields.Noshowpenalty : null;
            }

            var programAttributes = _programBusiness.GetAttributes(programId);

            if (programAttributes != null && programAttributes.Any())
            {
                var hasPTAFee = programAttributes.Any(p => p.Attribute.Name == AttributeName.PTAFee);
                var hasLeadGeneration = programAttributes.Any(p => p.Attribute.Name == AttributeName.LeadGeneration);

                model.HasPTAFee = hasPTAFee;
                model.PTAFee = hasPTAFee ? programAttributes.Single(p => p.Attribute.Name == AttributeName.PTAFee).Value : "";

                model.HasGenerationFee = hasLeadGeneration;
                model.LeadGenerationFee = hasLeadGeneration ? programAttributes.Single(p => p.Attribute.Name == AttributeName.LeadGeneration).Value : "";
            }

            switch (model.PageMode)
            {
                case PageMode.Create:
                    {

                    }
                    break;
                case PageMode.Edit:
                    {

                    }
                    break;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual ActionResult ProgramCreateEditStep4(ProgramCreateEditModelStep4 model)
        {
            OperationStatus res = null;

            if (ModelState.IsValid)
            {
                Program program = _programBusiness.Get(model.Id);
                CheckResourceForAccess(program);

                if (program.LastCreatedPage < ProgramPageStep.Step4)
                {
                    model.LastCreatedPage = ProgramPageStep.Step4;
                }

                program = model.ToModel<Program>(program);

                List<JbAttribute> attributes = new List<JbAttribute>();

                Dictionary<AttributeName, string> attributeValues = new Dictionary<AttributeName, string>();

                var PTAFee = !string.IsNullOrEmpty(model.PTAFee) ? Math.Round(decimal.Parse(model.PTAFee), 2) : 0;
                var LeadGenerationFee = !string.IsNullOrEmpty(model.LeadGenerationFee) ? Math.Round(decimal.Parse(model.LeadGenerationFee), 2) : 0;

                attributeValues.Add(AttributeName.PTAFee, !string.IsNullOrEmpty(model.PTAFee) ? PTAFee.ToString() : null);
                attributeValues.Add(AttributeName.LeadGeneration, !string.IsNullOrEmpty(model.LeadGenerationFee) ? LeadGenerationFee.ToString() : null);

                if (model.IsPartnerProvider)
                {
                    _programBusiness.SetAttributes(program, attributeValues);
                }


                if (ActiveClub.HasPartner && (!ActiveClub.IsSchool) && program.OutSourceSeasonId != null && program.ClubId != ActiveClub.Id)
                {
                    if (program.CustomFields == null)
                    {
                        program.CustomFields = new ProgramCustomFields();
                    }
                    program.CustomFields.CommisionRate = model.PartnerCommisionRate;
                    program.CustomFields.LateCheckInPenalty = model.PartnerLateCheckInPenalty;
                    program.CustomFields.Noshowpenalty = model.PartnerNoshowpenalty;
                    program.IsCustomFieldChanged = true;
                }
                res = _programBusiness.Update(program);

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected, Data = model.Id.ToString() });
            }

            return JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_Edit)]
        public virtual JsonNetResult CalendarCreateEditStep2Point5(int programId)
        {
            var model = new CalendarCreateEditStep2Point5();

            var clubDomain = ActiveClub.Domain;

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            model.LastCreatedPage = program.LastCreatedPage;

            model.Date = _programBusiness.StartDate(program).Value;
            model.EndDate = _programBusiness.EndDate(program).Value;

            model.Schedules = program.ProgramSchedules.Select(s =>
                  new CalendarScheduleViewModel
                  {
                      Text = "",
                      Value = s.Id,
                      Color = s.Charges.FirstOrDefault().Attributes.Color
                  })
                  .ToList();

            return JsonNet(model);
        }

        [HttpPost]
        public virtual JsonNetResult CopySchedule(ProgramScheduleViewModel schedule, int scheduleCount = 1)
        {
            var schedules = new List<ProgramScheduleViewModel>();

            DateTime startDate = schedule.StartDate.Value;
            DateTime endDate = schedule.EndDate.Value;

            double days = (endDate - startDate).TotalDays;

            int weeks = (int)Math.Ceiling((double)days / 7);

            for (int i = 0; i < scheduleCount + 1; i++)
            {
                schedules.Add(JsonHelper.JsonDeserialize<ProgramScheduleViewModel>(JsonHelper.JsonSerializer(schedule)));

                if (i != 0)
                {
                    schedules[i].Id = 0;
                }

                schedules[i].StartDate = startDate;
                schedules[i].EndDate = endDate;

                startDate = startDate.AddDays(weeks * 7);
                endDate = endDate.AddDays(weeks * 7);
            }

            return JsonNet(schedules);
        }

        [HttpGet]
        public virtual JsonNetResult AddSchedule()
        {
            var schedule = NewSchedule();

            return JsonNet(schedule);
        }

        [HttpPost]
        public virtual ActionResult IsSessionsChanged(long scheduleId)
        {

            var result = _programBusiness.IsSessionsEdited(scheduleId);

            return JsonNet(new { Changed = result, Status = true });
        }

        private ProgramScheduleViewModel NewSchedule()
        {
            return new ProgramScheduleViewModel()
            {
                Days = new List<ProgramScheduleDayViewModel>()
                        {
                            new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Monday },
                            new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Tuesday},
                            new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Wednesday},
                            new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Thursday},
                            new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Friday},
                            new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Saturday},
                            new ProgramScheduleDayViewModel{DayOfWeek = DayOfWeek.Sunday}
                        },

                Tuitions = new List<Tuition>()
                        {
                            new Tuition
                            {
                                Capacity="0",
                                Color = "#1e49e3",
                                EarlyBirds = new List<EarlyBird>()
                                {

                                },
                                IsProrate=false

                            },
                        },

                MinimumEnrollment = Constants.MinimumEnrollment_Default,
                Capacity = "0",
                ShowCapacityLeft = false,
                ShowCapacityLeftNumber = Constants.CapacityLeftNumber_Default,
                GenderItems = DropdownHelpers.ToSelectList<Genders>(),
                SelectedGender = Genders.NoRestriction,

                ContinueTypes = DropdownHelpers.ToSelectList<ContinueType>(),
                SelectedContinueType = ContinueType.Until,

                CapacityLeftNumbers = DropdownHelpers.SeedKeyValueNumbers(20),
                SelectedCapacity = 0,

                RestrictionTypes = DropdownHelpers.ToSelectList<RestrictionType>(),
                SelectedRestrictionType = RestrictionType.None,

                Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null),
                SelectedMinGrade = null,
                SelectedMaxGrade = null,

                SelectedSession = "1",

                SelectedMinAge = "0",
                SelectedMaxAge = "0",

                RegistrationPeriod = new RegistrationPeriodViewModel()
                {
                    RegisterStartDate = DateTime.Now
                }
            };
        }

        private ProgramScheduleViewModel NewTournamentSchedule()
        {

            return new ProgramScheduleViewModel()
            {
                Tuitions = new List<Tuition>()
                        {
                            new Tuition
                            {
                                Capacity="0",
                                EarlyBirds = new List<EarlyBird>()
                                {

                                }
                            },
                        },

                Capacity = "0",
                GenderItems = DropdownHelpers.ToSelectList<Genders>(),
                SelectedGender = Genders.NoRestriction,

                SelectedCapacity = 0,

                RestrictionTypes = DropdownHelpers.ToSelectList<RestrictionType>(),
                SelectedRestrictionType = RestrictionType.None,

                Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null),
                SelectedMinGrade = null,
                SelectedMaxGrade = null,

                SelectedMinAge = "0",
                SelectedMaxAge = "0",

                RegistrationPeriod = new RegistrationPeriodViewModel()
                {
                    RegisterStartDate = DateTime.Now
                }
            };
        }

        [HttpPost]
        public virtual ActionResult ProgramScheduleValidate(ProgramCreateEditModelStep2 model)
        {

            Program program = _programBusiness.Get(model.Id);
            var scheduleHaveOrdersCount = 0;
            var message = new List<string>();

            Step2ValidationCheck(ModelState, model);

            if (ModelState.IsValid)
            {
                if (model.Schedules.Any(s => s.IsScheduleChanged) && _orderSessionBusiness.HasActiveOrder(program))
                {
                    foreach (var schedule in model.Schedules)
                    {
                        ProgramSchedule currentSchedule;

                        if (schedule.IsScheduleChanged)
                        {
                            currentSchedule = program.ProgramSchedules.FirstOrDefault(s => s.Id == schedule.Id);
                            var scheduleHaveOrder = _orderSessionBusiness.HasActiveOrder(currentSchedule);

                            if (scheduleHaveOrder)
                            {
                                scheduleHaveOrdersCount++;
                                var schedulename = schedule.Title != null ? $"{schedule.Title} ({schedule.StartDate.Value.Date.ToString(Constants.DefaultDateFormat)}-{schedule.EndDate.Value.Date.ToString(Constants.DefaultDateFormat)})"
                                    : $"({schedule.StartDate.Value.Date.ToString(Constants.DefaultDateFormat)}-{schedule.EndDate.Value.Date.ToString(Constants.DefaultDateFormat)})";
                                message.Add(schedulename);
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }

                    if (model.Schedules.Count == scheduleHaveOrdersCount)
                    {
                        return JsonNet(new { Status = false, Message = "Can't change schedule when program has orders." });
                    }
                    else if (scheduleHaveOrdersCount > 0)
                    {
                        var errorMessage = string.Join(", ", message);
                        return JsonNet(new { Status = false, Message = "You cannot change the schedule: " + errorMessage + " because this schedule has orders." });
                    }
                }
                return Json(new JResult { Status = true });
            }

            return JsonFormResponse();
        }

        private List<SelectKeyValue<string>> SeedCapacity(int max = Constants.MaximumCapacity)
        {
            var result = new List<SelectKeyValue<string>>();

            result.Add(new SelectKeyValue<string> { Text = "Unlimited", Value = "0" });

            for (int i = 1; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<string> { Text = i.ToString(), Value = i.ToString() });
            }

            return result;
        }

        private List<SelectKeyValue<string>> SeedSession(int max)
        {
            var result = new List<SelectKeyValue<string>>();

            result.Add(new SelectKeyValue<string> { Text = "1 week", Value = "1" });

            for (int i = 2; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<string> { Text = string.Format("{0} weeks", i.ToString()), Value = i.ToString() });
            }

            return result;
        }

        private List<SelectKeyValue<string>> SeedAges(int max)
        {
            var result = new List<SelectKeyValue<string>>();

            result.Add(new SelectKeyValue<string> { Text = "Select age", Value = "0" });

            for (int i = 1; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<string> { Text = i.ToString(), Value = i.ToString() });
            }

            return result;
        }

        private List<SelectKeyValue<string>> SeedDaysBeforeBillingItems(int max)
        {
            var result = new List<SelectKeyValue<string>>();

            result.Add(new SelectKeyValue<string> { Text = "On the same day of schedule", Value = "0" });

            result.Add(new SelectKeyValue<string> { Text = "1 day before schedule", Value = "1" });

            for (int i = 2; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<string> { Text = string.Format("{0} days before schedule", i.ToString()), Value = i.ToString() });
            }

            return result;
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_List_View)]
        public virtual JsonNetResult GetDetails(long programId)
        {

            var model = new ProgramSummeryViewModel();
            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id).IsPartner ? program.Club : clubBusiness.Get(ActiveClub.Id);

            if (program != null && (_programBusiness.IsProgramBelongsToClub(club.Id, programId) || (program.OutSourceSeason != null && program.OutSourceSeason.ClubId == club.Id)))
            {

                model = program.ToViewModel<ProgramSummeryViewModel>();
                model.Schedules = program.ProgramSchedules.Where(c => c.IsDeleted == false).Select(c => c.ToViewModel<ScheduleViewModel>()).ToList();

                model.StatusReason = new List<JbTitleValue>();
                model.StatusReason.Add(new JbTitleValue() { Title = "All", Value = -1 });
                model.StatusReason.AddRange(Enum.GetValues(typeof(OrderItemStatusReasons))
                    .Cast<OrderItemStatusReasons>()
                    .Where(v => v.ToDescription() != "All")
                    .Select(v => new JbTitleValue() { Title = v.ToDescription(), Value = (int)v })
                    .ToList());

                var completedItems = Ioc.OrderItemBusiness.GetOrderItems(programId, program.Season.Status == SeasonStatus.Test);

                model.OrderItemsCount = completedItems.Any() ? completedItems.Count() : 0;
                model.TotalAmount = completedItems.Any() ? completedItems.Sum(item => item.TotalAmount) : 0;
                model.DisplayLink = program.ClubId == club.Id ? Url.EventDomain(program.Season.Domain, program.Domain) : UrlHelpers.GetOutSourceUrl(Request.Url.Host, program.Club.Domain, program.Season.Domain, program.Domain);

                model.SearchTypes = DropdownHelpers.ToSelectList<ParticipantSearchType>();
            }

            return JsonNet(model);
        }

        [HttpGet]
        public virtual ActionResult AnyOutSourcePrograms(string seasonDomain)
        {
            dynamic model = new ExpandoObject();
            model.ClubType = (ActiveClub.ClubType.ParentType != null) ? ActiveClub.ClubType.ParentType.EnumType : ActiveClub.ClubType.EnumType;

            model.HasOutSource = _programBusiness.GetList(ActiveClub.Id, seasonDomain).Any(c => c.OutSourceSeasonId.HasValue && c.OutSourceSeasonId.Value > 0);

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_List_View)]
        public virtual JsonNetResult GetList(PaginationModel paginationModel, string seasonDomain, string nameSearchTerm, DateTime? startDateCreatedSearchTerm, DateTime? endDateCreatedSearchTerm, SaveType? statusSearchTerm, ProgramTypeCategory? typeSearchTerm)
        {
            var programBusiness = _programBusiness;
            var isSchool = ActiveClub.IsSchool;
            var isProvider = (ActiveClub.ClubType != null && ((ActiveClub.ClubType.EnumType == ClubTypesEnum.Provider) || (ActiveClub.ClubType.ParentType != null && ActiveClub.ClubType.ParentType.EnumType == ClubTypesEnum.Provider)));
            int? instructorId = null;

            if (this.GetCurrentUserRole() == RoleCategory.Instructor)
            {
                instructorId = Ioc.ClubBusiness.Get(ActiveClub.Id).ClubStaffs.Single(c => c.JbUserRole.UserId == this.GetCurrentUserId()).Id;
            }

            var clubComRate = 0;

            if (!isSchool)
            {
                clubComRate = Ioc.ClubBusiness.Get(ActiveClub.Id).PartnerCommisionRate;
            }

            var season = Ioc.SeasonBusiness.Get(seasonDomain, ActiveClub.Id);

            var seasonIds = season.OutSourcePrograms.Select(p => p.SeasonId).ToList();

            seasonIds.Add(season.Id);

            var coupons = Ioc.CouponBusiness.GetList(seasonIds);
            var model = _programBusiness.GetList(ActiveClub.Id, seasonDomain, nameSearchTerm, startDateCreatedSearchTerm, endDateCreatedSearchTerm, statusSearchTerm, typeSearchTerm, instructorId)
                     .ToList()
                     .Select(p => new ProgramItemViewModel
                     {
                         Id = p.Id,
                         ClubDomain = p.ClubDomain,
                         SeasonDomain = p.SeasonDomain,
                         Domain = p.Domain,
                         Name = p.Name,
                         IsProvider = isProvider,
                         WaitList = p.WaitLists.Count(w => w.IsLive == (w.Program.Season.Status == SeasonStatus.Live)),
                         CommisionRate = (p.CustomFields != null && p.CustomFields.CommisionRate.HasValue) ? p.CustomFields.CommisionRate.Value : clubComRate,
                         RespondStatus = (p.RespondToInvitation != null && p.RespondToInvitation.Any()) ? p.RespondToInvitation.LastOrDefault().RespondType.ToString() : "",
                         Status = p.Status,
                         OutSourceProgramStatus = p.OutSourceProgramStatus,
                         HideOutSourceProgramInProvider = (isProvider && p.OutSourceSeasonId.HasValue && p.OutSourceSeason.ClubId == ActiveClub.Id),
                         OutSourceClubAndSeasonNameForSchool = (isSchool && p.OutSourceSeasonId.HasValue && p.OutSourceSeason != null) ? p.OutSourceSeason.Club.Name : "",
                         OutSourceClubAndSeasonNameForProvider = (isProvider && p.OutSourceSeasonId.HasValue && p.OutSourceSeason != null) ? p.Club.Name : "",
                         Schedule = (p.ProgramSchedules.Where(s => !s.IsDeleted) != null && p.ProgramSchedules.Where(s => !s.IsDeleted).Any()) ? p.ProgramSchedules.Where(s => !s.IsDeleted).First() : null,
                         MinimumCapacity = (p.TypeCategory == ProgramTypeCategory.Class) && p.ProgramSchedules.Any(s => !s.IsDeleted) ? (p.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes.MinimumEnrollment : 0,
                         //MaximumCapacity = (p.TypeCategory != ProgramTypeCategory.ChessTournament) && p.ProgramSchedules.Any(s => !s.IsDeleted) ? ((ScheduleAttribute)(p.ProgramSchedules.Where(s => !s.IsDeleted).First()).Attributes).Capacity : 0,
                         LastCreatedPage = p.LastCreatedPage,
                         SaveType = GetSaveType(p.LastCreatedPage),
                         TypeCategory = p.TypeCategory,
                         TotalAmount = Ioc.OrderItemBusiness.GetOrderItems(p.Id, p.Season.Status == SeasonStatus.Test).Where(o => o.ItemStatus == OrderItemStatusCategories.completed).Sum(item => (decimal?)item.TotalAmount) ?? 0,
                         ParticipantCount = Ioc.OrderItemBusiness.GetOrderItems(p.Id, p.Season.Status == SeasonStatus.Test).Where(o => o.ItemStatus == OrderItemStatusCategories.completed).Count(),
                         ViewPageUrl = p.ClubId == ActiveClub.Id ? Url.EventDomain(p.Season.Domain, p.Domain) : UrlHelpers.GetOutSourceUrl(Request.Url.Host, p.Club.Domain, p.Season.Domain, p.Domain),

                         ProviderStatus = (p.OutSourceSeasonId.HasValue && p.OutSourceSeason != null) ? programBusiness.GetEmProgramStatus(p, p.Season.Status == SeasonStatus.Test) : "",
                         TotalProviderFundedScholarships = p.ProgramSchedules.SelectMany(o => o.OrderItems)
                             .Where(c => c.ItemStatus == OrderItemStatusCategories.completed && c.Order.IsLive == (p.Season.Status == SeasonStatus.Live) && c.OrderChargeDiscounts.Any(ch => !ch.IsDeleted))
                             .SelectMany(c => c.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Coupon && c.CouponId.HasValue
                             && _programBusiness.GetProviderFundedCoupons(p, coupons.ToList()).Select(s => s.Id)
                             .Contains(c.CouponId.Value)).Count(),
                         NumberOfScheduleNotFreezed = p.ProgramSchedules.Count(s => !s.IsFreezed && !s.IsDeleted),
                         SchedulesDetail = p.ProgramSchedules.Where(s => !s.IsDeleted).Select(s => new SchedulesDetail
                         {
                             Id = s.Id,
                             Title = s.Title,
                             ScheduleMode = s.ScheduleMode,
                             IsFreezed = s.IsFreezed
                         }).ToList()
                     });

            var dataSource = model.OrderBy(m => m.ProgramDays.DayOfWeek).ThenBy(m => m.ProgramDays.StartTime).Page(paginationModel).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = model.Count() });
        }


        public virtual JsonNetResult GetCalendar(long programId)
        {

            var program = _programBusiness.Get(programId);
            CheckResourceForAccess(program);

            var model = new List<ProgramCalendarItemViewModel>();

            int ownerId = 1;
            foreach (var schedule in program.ProgramSchedules.Where(s => !s.IsDeleted))
            {
                var sessions = schedule.Sessions;

                foreach (var item in sessions)
                {
                    model.Add(
                       new ProgramCalendarItemViewModel
                       {
                           TaskId = item.Id,
                           Start = item.Start.ToString(),
                           End = item.End.ToString(),
                           Title = schedule.Title,
                           OwnerId = Convert.ToInt32(schedule.Id),
                       });
                }

                ownerId++;
            }

            var result = JsonNet(model);

            return result;
        }

        private SaveType GetSaveType(ProgramPageStep lastCreatedPage)
        {
            return (lastCreatedPage >= ProgramPageStep.Step4) ? SaveType.Publish : SaveType.Draft;
        }

        [HttpGet]
        public virtual JsonNetResult GetListForDropDown(string seasonDomain, string term, ProgramTypeCategory type = ProgramTypeCategory.Class)
        {
            var termValue = Request.Params["term"] ?? string.Empty;
            var allprograms =
                _programBusiness.GetList()
                    .Where(
                        p =>
                            p.ClubId == ActiveClub.Id && p.Season.Domain.ToLower() == seasonDomain.ToLower() &&
                            p.Status == ProgramStatus.Open && p.Name.StartsWith(termValue));

            var model = new List<ProgramsViewModel>();

            if (type != ProgramTypeCategory.Camp)
            {
                model = allprograms.Select(p =>
                    new ProgramsViewModel
                    {
                        Id = p.Id,
                        ClubDomain = p.Club.Domain,
                        SeasonDomain = p.Season.Domain,
                        Name = p.Name,
                        TypeCategory = p.TypeCategory
                    })
                    .ToList();
            }
            else
            {
                model = allprograms.Where(p => p.TypeCategory == ProgramTypeCategory.Camp).Select(p =>
                    new ProgramsViewModel
                    {
                        Id = p.Id,
                        ClubDomain = p.Club.Domain,
                        SeasonDomain = p.Season.Domain,
                        Name = p.Name,
                        TypeCategory = p.TypeCategory
                    })
                    .ToList();
            }

            return JsonNet(model, new JsonSerializerSettings(), Constants.DefaultDateTimeFormat);
        }

        [HttpGet]
        public virtual JsonNetResult GetCategories()
        {
            var termValue = Request.Params["term"] ?? string.Empty;

            var categories = Ioc.CategoryBuisness.GetList()
                .Where(e => e.Name.StartsWith(termValue) && e.ParentId != null).Take(10)
                .ToList().Select(x => new JbTitleValue(string.Format("{0} --> {1}", x.ParentCategory.Name, x.Name), x.Id));

            if (!categories.Any())
            {
                categories = Ioc.CategoryBuisness.GetList()
                    .Where(e => e.Name.Contains(termValue) && e.ParentId != null).Take(10)
                    .ToList().Select(x => new JbTitleValue(string.Format("{0} --> {1}", x.ParentCategory.Name, x.Name), x.Id));
            }

            return JsonNet(categories);
        }

        [HttpGet]
        public virtual JsonNetResult GetWaivers()
        {
            string termValue = string.Empty;
            if (Request.Params["term"] != null)
            {
                termValue = Request.Params["term"].ToLower() ?? string.Empty;
            }

            var waivers = Ioc.ClubBusiness.GetWaivers(ActiveClub.Id)
                .Where(e => e.Name.ToLower().StartsWith(termValue)).Take(10)
                .ToList().Select(x => new JbTitleValue(x.Name, x.Id));

            if (!waivers.Any())
            {
                waivers = Ioc.ClubBusiness.GetWaivers(ActiveClub.Id)
                .Where(e => e.Name.ToLower().Contains(termValue)).Take(10)
                .ToList().Select(x => new JbTitleValue(x.Name, x.Id));
            }

            return JsonNet(waivers);
        }


        [JbAuthorize(JbAction.Program_Delete)]
        public virtual ActionResult Delete(long id)
        {
            OperationStatus res = null;

            var program = _programBusiness.Get(id);

            if (program.TypeCategory != ProgramTypeCategory.BeforeAfterCare && program.TypeCategory != ProgramTypeCategory.Subscription && _programBusiness.HasOrder(program, true))
            {
                res = new OperationStatus() { Status = false, Message = "The program can not be deleted as it has some completed orders." };
            }
            else if ((program.TypeCategory == ProgramTypeCategory.BeforeAfterCare || program.TypeCategory == ProgramTypeCategory.Subscription) && _programBusiness.HasOrder(program))
            {
                res = new OperationStatus() { Status = false, Message = "The program can not be deleted as it has some completed orders." };
            }
            else
            {
                res = _programBusiness.Delete(id);
            }

            return JsonNet(res);
        }

        [JbAuthorize(JbAction.Program_Freeze)]
        public virtual ActionResult Freeze(long id)
        {
            var res = _programBusiness.Freeze(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [JbAuthorize(JbAction.Program_Freeze)]
        public virtual ActionResult UnFreeze(long id)
        {
            var res = _programBusiness.UnFreeze(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [JbAuthorize(JbAction.Program_Freeze)]
        public virtual ActionResult FreezeSchedule(long id)
        {
            var res = _programBusiness.FreezeSchedule(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [JbAuthorize(JbAction.Program_Freeze)]
        public virtual ActionResult UnFreezeSchedule(long id)
        {
            var res = _programBusiness.UnFreezeSchedule(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        public virtual JsonNetResult GetListSelector(string seasonDomain, string term)
        {
            var clubId = base.ActiveClub.Id;
            var termValue = Request.Params["term"] ?? string.Empty;
            var programes = _programBusiness.GetList()
                .Where(p => p.ClubId == clubId && p.Season.Domain.ToLower() == seasonDomain.ToLower() && p.Status == ProgramStatus.Open && p.Name.StartsWith(termValue))
                .ToList().Select(x => new JbTitleValue<long>(x.Name, x.Id));

            //if (!programes.Any())
            //    programes = _programBusiness.GetList()
            //        .Where(p => p.ClubId == clubId && p.Season.Domain.ToLower() == seasonDomain.ToLower() && p.Status == ProgramStatus.Open && p.Name.Contains(termValue)).Take(10)
            //        .ToList().Select(x => new JbTitleValue<long>(x.Name, x.Id));

            return JsonNet(programes);
        }

        [HttpGet]
        [JbAuthorize(JbAction.Program_RespondInvitation)]
        public virtual JsonNetResult RespondToInvitationProgram(long? programId)
        {
            var model = new ProgramRespondToInvitationViewModel()
            {
                ProgramId = programId.HasValue ? programId.Value : 0,
                ProgramName = programId.HasValue ? _programBusiness.Get(programId.Value).Name : ""
            };

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Program_RespondInvitation)]
        public virtual ActionResult RespondToInvitationProgram(ProgramRespondToInvitationViewModel model)
        {
            RespondToInvitationValidation(ModelState, model);
            if (ModelState.IsValid)
            {
                var respondType = (ProgramRespondType)model.RespondType;
                var respond = new ProgramRespondToInvitation()
                {
                    ProgramId = model.ProgramId,
                    RespondType = respondType

                };

                switch (respondType)
                {
                    case ProgramRespondType.Accept:
                        respond.Respond = new AcceptRespond()
                        {
                            AvailableAnyDay = model.AvailableAnyDay,
                            FirstDayPreference = (DayOfWeek)model.FirstDayPreference,
                            SecondDayPreference = (DayOfWeek)model.SecondDayPreference
                        };
                        break;
                    case ProgramRespondType.Modify:
                        respond.Respond = new BaseRespond()
                        {
                            Note = model.Note
                        };
                        break;
                    case ProgramRespondType.Decline:
                        respond.Respond = new DeclineRespond()
                        {
                            DeclineResaon = (PorgramRespondDeclineResaon)model.DeclineResaon,
                            Note = model.Note
                        };
                        break;
                }

                var res = _programBusiness.InsertRespond(respond);
                if (res.Status)
                {
                    EmailService.Get(this.ControllerContext).SendProviderRespondToInvitation(respond);
                }
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [JbAuthorize(JbAction.Program_AssignInstructors)]
        public virtual ActionResult GetAssignInstructors(long programId)
        {
            var allInstructors = Ioc.ClubBusiness.GetClubInstructors(ActiveClub.Id).Where(i => !i.IsFrozen && !i.IsDeleted).Select(l =>
                            new SelectKeyValue<int>
                            {
                                Text = l.Contact.FullName,
                                Value = l.Id
                            })
                          .ToList();

            var selectedInstructors = _programBusiness.Get(programId).Instructors.Select(c => c.Id).ToList();

            var model = new AssignInstructorsViewModel()
            {
                AllInstructors = allInstructors,
                SelectedInstructors = selectedInstructors
            };

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.Program_AssignInstructors)]
        public virtual ActionResult SaveAssignInstructors(AssignInstructorsViewModel model, long programId)
        {
            var program = _programBusiness.Get(programId);

            program.Instructors.Clear();

            if (model.SelectedInstructors != null && model.SelectedInstructors.Any())
            {
                var instructors = Ioc.ClubBusiness.GetClubInstructors(ActiveClub.Id).Where(i => model.SelectedInstructors.Select(c => c).ToList().Contains(i.Id)).ToList();

                program.Instructors.AddEntities(instructors);
            }

            var result = _programBusiness.Update(program);

            return Json(new JResult { Status = result.Status, Message = result.Message, RecordsAffected = result.RecordsAffected });
        }

        [JbAuthorize(JbAction.Program_AssignRoom)]
        public virtual ActionResult GetAssignRoom(long programId)
        {
            var program = _programBusiness.Get(programId);

            var model = new AssignRoomViewModel
            {
                Room = program.Room
            };

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.Program_AssignRoom)]
        public virtual ActionResult SaveAssignRoom(AssignRoomViewModel model, long programId)
        {
            var program = _programBusiness.Get(programId);

            program.Room = model.Room;

            var result = _programBusiness.Update(program);

            return Json(new JResult { Status = result.Status, Message = result.Message, RecordsAffected = result.RecordsAffected });
        }

        private List<ProgramSubscriptionItem> GenerateSubscriptionSchedules(DateTime startDate, DateTime endDate, decimal amount, int dueDate)
        {
            var result = Ioc.SubscriptionBusiness.GenerateSubscriptionSchedules(startDate, endDate, amount, dueDate);

            return result;
        }

        private void RespondToInvitationValidation(ModelStateDictionary modelState, ProgramRespondToInvitationViewModel model)
        {

            if (model.RespondType < 1)
            {
                modelState.AddModelError("model.RespondType", "Respond is required.");
                return;
            }
            var respondType = (ProgramRespondType)model.RespondType;
            if (respondType == ProgramRespondType.Accept && !model.AvailableAnyDay && model.FirstDayPreference < 0)
            {
                modelState.AddModelError("model.FirstDayPreference", "1st day preference is required.");
            }
            if (respondType == ProgramRespondType.Modify && string.IsNullOrEmpty(model.Note))
            {
                modelState.AddModelError("model.Note", "Note is required.");
            }
            if (respondType == ProgramRespondType.Decline && model.DeclineResaon < 1)
            {
                modelState.AddModelError("model.DeclineResaon", "Decline resaon is required.");
            }
            if (respondType == ProgramRespondType.Decline && (PorgramRespondDeclineResaon)model.DeclineResaon == PorgramRespondDeclineResaon.Other && string.IsNullOrEmpty(model.Note))
            {
                modelState.AddModelError("model.Note", "Note is required.");
            }
        }

        private void Step1ValidationCheck(ModelStateDictionary modelState, ProgramCreateEditModelStep1 model)
        {
            if (model.TypeCategory != ProgramTypeCategory.ChessTournament && (model.SelectedCategories == null || !model.SelectedCategories.Any()))
            {
                ModelState.AddModelError("model.SelectedCategories", "You should select at least one category.");
            }
            if (model.HasRegisterPIN && string.IsNullOrEmpty(model.RegisterPIN))
            {
                ModelState.AddModelError("model.RegisterPIN", "Access password is required.");
            }
            if (model.HasRegisterPIN && string.IsNullOrEmpty(model.RegisterPINMessage))
            {
                ModelState.AddModelError("model.RegisterPINMessage", "Restriction message is required.");
            }

            if (!model.HasTestimonials)
            {
                ModelState.Remove("model.Testimonials");
                model.Testimonials = string.Empty;
            }
        }

        private void Step2ValidationCheck(ModelStateDictionary modelState, ProgramCreateEditModelStep2 model)
        {
            for (int i = 0; i < model.Schedules.Count(); i++)
            {
                if (!model.Schedules[i].Days.Any(a => a.IsChecked) && (model.TypeCategory == ProgramTypeCategory.Class || model.TypeCategory == ProgramTypeCategory.Camp))
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "Days"), "You should choose at least one day.");
                }

                if (model.Schedules[i].StartDate.HasValue && model.Schedules[i].EndDate.HasValue && model.Schedules[i].StartDate.Value > model.Schedules[i].EndDate.Value)
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "StartDate"), "Start date should be before of end date.");
                }

                if (model.Schedules[i].RegistrationPeriod.RegisterStartDate.HasValue && model.Schedules[i].RegistrationPeriod.RegisterEndDate.HasValue && model.Schedules[i].RegistrationPeriod.RegisterStartDate.Value > model.Schedules[i].RegistrationPeriod.RegisterEndDate.Value)
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "RegistrationPeriod.RegisterStartDate"), "Register Start date should be before register end date.");
                }

                if (model.Schedules[i].RegistrationPeriod.RegisterEndDate.HasValue && model.Schedules[i].EndDate.HasValue && model.Schedules[i].RegistrationPeriod.RegisterEndDate.Value > model.Schedules[i].EndDate.Value)
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "RegistrationPeriod.RegisterEndDate"), "Register end date should be before program end date.");
                }

                if (model.Schedules[i].SelectedContinueType == ContinueType.For)
                {
                    ModelState.Remove(string.Format("model.Schedules[{0}].EndDate", i));
                }

                if (!model.Schedules[i].EnableDropIn)
                {
                    ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "DropInLable"));
                }

                for (int j = 0; j < model.Schedules[i].Tuitions.Count; j++)
                {
                    ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "Id"));

                    if (model.Schedules[i].Tuitions[j].IsProrate)
                    {
                        if (!model.Schedules[i].Tuitions[j].ProrateSessionPrice.HasValue)
                        {
                            ModelState.AddModelError(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "ProrateSessionPrice"), "The price for each missing session is required.");
                        }
                        if (model.Schedules[i].Tuitions[j].ProrateSessionPrice.HasValue && model.Schedules[i].Tuitions[j].Price.HasValue && model.Schedules[i].Tuitions[j].ProrateSessionPrice.Value > model.Schedules[i].Tuitions[j].Price.Value)
                        {
                            ModelState.AddModelError(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "ProrateSessionPrice"), "The price for each missing session must be less than the tution price.");
                        }
                        if (!model.Schedules[i].Tuitions[j].ProrateStartDate.HasValue)
                        {
                            ModelState.AddModelError(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "ProrateStartDate"), "The prorate start date is required.");
                        }
                        if (model.Schedules[i].Tuitions[j].ProrateStartDate.HasValue && model.Schedules[i].Tuitions[j].ProrateStartDate.Value.CompareTo(model.Schedules[i].StartDate) < 0)
                        {
                            ModelState.AddModelError(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "ProrateStartDate"), "The prorate start date should be after the program start date.");
                        }
                    }

                    if (!model.Schedules[i].EnableDropIn)
                    {
                        ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "DropInName"));
                        ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "DropInPrice"));
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(model.Schedules[i].Tuitions[j].DropInName) && model.Schedules[i].Tuitions[j].DropInName.Length > 20)
                        {
                            ModelState.AddModelError(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "DropInName"), "The input cannot exceed 20 characters.");
                        }
                    }

                }
                switch (model.TypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        {
                            for (int j = 0; j < model.Schedules[i].Days.Count(); j++)
                            {
                                if (!model.Schedules[i].Days[j].IsChecked)
                                {
                                    ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "StartTime"));

                                    ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "EndTime"));
                                }


                                if (model.Schedules[i].Days[j].IsChecked && model.Schedules[i].Days[j].StartTime.HasValue && model.Schedules[i].Days[j].EndTime.HasValue && model.Schedules[i].Days[j].StartTime.Value >= model.Schedules[i].Days[j].EndTime.Value)
                                {
                                    ModelState.AddModelError(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "StartTime"), "Start time should be before of end time.");
                                }
                            }

                            if (model.Schedules[i].Capacity != "0")
                            {
                                if (model.Schedules[i].Tuitions.Sum(c => int.Parse(c.Capacity)) > int.Parse(model.Schedules[i].Capacity))
                                {
                                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "Capacity"), "Total capacity of tuitions must be equal or smaller than program capacity.");
                                }
                            }

                            modelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "ShowCapacityLeftNumber"));

                            if (model.Schedules[i].ShowCapacityLeft && model.Schedules[i].Capacity != "0")
                            {

                                if (model.Schedules[i].ShowCapacityLeftNumber <= 0)
                                {
                                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "ShowCapacityLeftNumber"), "The capacity left number is required.");
                                }
                                else if (model.Schedules[i].ShowCapacityLeftNumber > int.Parse(model.Schedules[i].Capacity))
                                {
                                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "ShowCapacityLeftNumber"), "The capacity left number must be equal or smaller than program capacity.");
                                }
                            }
                            else if (model.Schedules[i].ShowCapacityLeft && model.Schedules[i].Capacity == "0" && model.Schedules[i].Tuitions.Max(c => int.Parse(c.Capacity)) > 0)
                            {
                                if (model.Schedules[i].ShowCapacityLeftNumber <= 0)
                                {
                                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "ShowCapacityLeftNumber"), "The capacity left number is required.");
                                }
                                else if (model.Schedules[i].ShowCapacityLeftNumber > model.Schedules[i].Tuitions.Max(c => int.Parse(c.Capacity)))
                                {
                                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "ShowCapacityLeftNumber"), "The capacity left number must be equal or smaller than maximum capacity of tuitions.");
                                }
                            }
                        }
                        break;
                    case ProgramTypeCategory.Camp:
                        {
                            if (!model.Schedules[i].IsCampOvernight && model.Schedules[i].StartTime.HasValue && model.Schedules[i].EndTime.HasValue && model.Schedules[i].StartTime.Value >= model.Schedules[i].EndTime.Value)
                            {
                                ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "StartTime"), "Start time should be before of end time.");
                            }

                            for (int j = 0; j < model.Schedules[i].Days.Count(); j++)
                            {
                                ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "StartTime"));

                                ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "EndTime"));
                            }
                        }
                        break;
                    case ProgramTypeCategory.SeminarTour:
                        {
                            for (int j = 0; j < model.Schedules[i].Days.Count(); j++)
                            {
                                ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "StartTime"));

                                ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "EndTime"));
                            }
                        }
                        break;
                    case ProgramTypeCategory.Calendar:
                        break;
                    default:
                        break;
                }

                if (model.TypeCategory != ProgramTypeCategory.Camp)
                {
                    ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "StartTime"));
                    ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "EndTime"));
                }

                RestrictionsValidationCheck(model.Schedules[i], i);

                if (model.Schedules[i].Charges != null)
                {
                    for (int j = 0; j < model.Schedules[i].Charges.Count(); j++)
                    {
                        if (model.Schedules[i].Charges[j].Category == ChargeDiscountCategory.DayCare && model.Schedules[i].Charges[j].StartTime.HasValue && model.Schedules[i].Charges[j].EndTime.HasValue && model.Schedules[i].Charges[j].StartTime.Value >= model.Schedules[i].Charges[j].EndTime.Value)
                        {
                            ModelState.AddModelError(string.Format("model.Schedules[{0}].Charges[{1}].{2}", i, j, "StartTime"), "Start time should be before of end time.");
                        }

                        ModelState.Remove(string.Format("model.Schedules[{0}].Charges[{1}].{2}", i, j, "Id"));
                    }
                }
            }
        }

        private void TournamentStep2ValidationCheck(ModelStateDictionary modelState, TournamentCreateEditModelStep2 model)
        {
            for (int i = 0; i < model.Schedules.Count(); i++)
            {
                for (int j = 0; j < model.Schedules[i].Days.Count(); j++)
                {
                    ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "StartTime"));

                    ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "EndTime"));
                }
                if (model.TypeCategory == ProgramTypeCategory.ChessTournament)
                {
                    if (!model.Schedules[i].EnableDropIn)
                    {
                        ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "DropInLable"));
                    }

                    for (int j = 0; j < model.Schedules[i].Tuitions.Count; j++)
                    {
                        ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "Id"));

                        if (!model.Schedules[i].EnableDropIn)
                        {
                            ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "DropInName"));
                            ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[{1}].{2}", i, j, "DropInPrice"));
                        }
                    }
                }

                modelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "StartDate"));
                modelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "EndDate"));

                modelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "StartTime"));
                modelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "EndTime"));

                RestrictionsValidationCheck(model.Schedules[i], i);

                if (model.Schedules[i].RegistrationPeriod.RegisterStartDate.HasValue && model.Schedules[i].RegistrationPeriod.RegisterEndDate.HasValue && model.Schedules[i].RegistrationPeriod.RegisterStartDate.Value > model.Schedules[i].RegistrationPeriod.RegisterEndDate.Value)
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "RegistrationPeriod.RegisterStartDate"), "Register Start date should be before register end date.");
                }

            }

            for (int i = 0; i < model.TourneySchedules.Count; i++)
            {
                if (model.TourneySchedules.ElementAt(i).StartDate.HasValue && model.TourneySchedules.ElementAt(i).EndDate.HasValue && model.TourneySchedules.ElementAt(i).StartDate > model.TourneySchedules.ElementAt(i).EndDate)
                {
                    modelState.AddModelError(string.Format("model.TourneySchedules[{0}].{1}", i, "StartDate"), "Start date should be before end date.");
                }
            }

        }

        private void CalendarStep2ValidationCheck(ModelStateDictionary modelState, CalendarCreateEditModelStep2 model)
        {
            for (int i = 0; i < model.Schedules.Count(); i++)
            {
                if (!model.Schedules[i].Days.Any(a => a.IsChecked))
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "Days"), "You should choose at least one day.");
                }

                for (int j = 0; j < model.Schedules[i].Days.Count(); j++)
                {
                    if (!model.Schedules[i].Days[j].IsChecked)
                    {
                        ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "StartTime"));

                        ModelState.Remove(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "EndTime"));
                    }

                    if (model.Schedules[i].Days[j].IsChecked && model.Schedules[i].Days[j].StartTime.HasValue && model.Schedules[i].Days[j].EndTime.HasValue && model.Schedules[i].Days[j].StartTime.Value >= model.Schedules[i].Days[j].EndTime.Value)
                    {
                        ModelState.AddModelError(string.Format("model.Schedules[{0}].Days[{1}].{2}", i, j, "StartTime"), "Start time should be before of end time.");
                    }
                }

                if (string.IsNullOrEmpty(model.Schedules[i].Title))
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "Title"), "Schedule title is required.");
                }

                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "RegistrationPeriod.RegisterEndDate"));

                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "StartTime"));

                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "EndTime"));

                ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[0].{1}", i, "Label"));

                ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[0].EarlyBirds[0].{1}", i, "ExpireDate"));

                if (model.Schedules[i].StartDate.HasValue && model.Schedules[i].EndDate.HasValue && model.Schedules[i].StartDate.Value > model.Schedules[i].EndDate.Value)
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "StartDate"), "Start date should be before of end date.");
                }

                if (!model.Schedules[i].Tuitions[0].HasEarlyBird)
                {
                    ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[0].EarlyBirds[0].{1}", i, "PriorDay"));
                    ModelState.Remove(string.Format("model.Schedules[{0}].Tuitions[0].EarlyBirds[0].{1}", i, "Price"));
                }

                if (model.Schedules[i].SelectedContinueType == ContinueType.For)
                {
                    ModelState.Remove(string.Format("model.Schedules[{0}].EndDate", i));
                }
            }

            if (model.SelectedRestrictionType == RestrictionType.Age)
            {
                ModelState.Remove(string.Format("model.{0}", "SelectedMinGrade"));
                ModelState.Remove(string.Format("model.{0}", "SelectedMaxGrade"));

                if (model.SelectedMinAge == "0" && model.SelectedMaxAge == "0")
                {
                    ModelState.AddModelError(string.Format("model.{0}", "SelectedMinAge"), "You must select min or max age.");
                }

                if (model.SelectedMinAge != "0" && model.SelectedMaxAge != "0" && int.Parse(model.SelectedMinAge) > int.Parse(model.SelectedMaxAge))
                {
                    ModelState.AddModelError(string.Format("model.{0}", "SelectedMinAge"), "Min age must be smaller than max age.");
                }
            }
            else if (model.SelectedRestrictionType == RestrictionType.Grade)
            {
                ModelState.Remove(string.Format("model.{0}", "SelectedMinAge"));
                ModelState.Remove(string.Format("model.{0}", "SelectedMaxAge"));

                if (model.SelectedMinGrade == null && model.SelectedMaxGrade == null)
                {
                    ModelState.AddModelError(string.Format("model.{0}", "SelectedMinGrade"), "You must select min or max grade.");
                }
                else
                {
                    if (model.SelectedMinGrade.GetOrder() > model.SelectedMaxGrade.GetOrder())
                    {
                        ModelState.AddModelError($"model.{"SelectedMinGrade"}", "Min grade must be smaller than max grade.");
                    }
                }
            }

            ModelState.Remove(string.Format("model.{0}", "RegistrationPeriod.RegisterEndDate"));
        }

        private void SubscriptionStep2ValidationCheck(ModelStateDictionary modelState, SubscriptionCreateEditModelStep2 model)
        {
            if (model.Days == null || !model.Days.Where(d => d.IsChecked).Any())
            {
                ModelState.AddModelError(string.Format("model.{0}", "Days"), "You should choose at least one day.");
            }

            if (model.StartDate.HasValue && model.EndDate.HasValue && model.StartDate.Value > model.EndDate.Value)
            {
                ModelState.AddModelError(string.Format("model.{0}", "StartDate"), "Start date should be before of end date.");
            }

            if (model.RegistrationPeriod == null)
            {
                model.RegistrationPeriod = new RegistrationPeriodViewModel();
            }

            if (model.RegistrationPeriod.RegisterStartDate.HasValue && model.RegistrationPeriod.RegisterEndDate.HasValue && model.RegistrationPeriod.RegisterStartDate.Value > model.RegistrationPeriod.RegisterEndDate.Value)
            {
                ModelState.AddModelError(string.Format("model.{0}", "RegistrationPeriod.RegisterStartDate"), "Register Start date should be before register end date.");
            }

            if (model.RegistrationPeriod.RegisterEndDate.HasValue && model.EndDate.HasValue && model.RegistrationPeriod.RegisterEndDate.Value > model.EndDate.Value)
            {
                ModelState.AddModelError(string.Format("model.{0}", "RegistrationPeriod.RegisterEndDate"), "Register end date should be before program end date.");
            }

            if (model.SelectedContinueType == ContinueType.For)
            {
                ModelState.Remove(string.Format("model.{0}", "EndDate"));
            }

            //if (!model.HasApplicationFee)
            //{
            //    ModelState.Remove(string.Format("model.{0}", "ApplicationFee"));
            //}

            if (model.SelectedRestrictionType == RestrictionType.None)
            {
                ModelState.Remove(string.Format("model.{0}", "SelectedMinAge"));
                ModelState.Remove(string.Format("model.{0}", "SelectedMaxAge"));

                ModelState.Remove(string.Format("model.{0}", "SelectedMinGrade"));
                ModelState.Remove(string.Format("model.{0}", "SelectedMaxGrade"));
            }
            else if (model.SelectedRestrictionType == RestrictionType.Age)
            {
                ModelState.Remove(string.Format("model.{0}", "SelectedMinGrade"));
                ModelState.Remove(string.Format("model.{0}", "SelectedMaxGrade"));

                if (model.SelectedMinAge == "0" && model.SelectedMaxAge == "0")
                {
                    ModelState.AddModelError(string.Format("model.{0}", "SelectedMinAge"), "You must select min or max age.");
                }

                if (model.SelectedMinAge != "0" && model.SelectedMaxAge != "0" && int.Parse(model.SelectedMinAge) > int.Parse(model.SelectedMaxAge))
                {
                    ModelState.AddModelError(string.Format("model.{0}", "SelectedMinAge"), "Min age must be smaller than max age.");
                }
            }
            else if (model.SelectedRestrictionType == RestrictionType.Grade)
            {
                ModelState.Remove(string.Format("model.{0}", "SelectedMinAge"));
                ModelState.Remove(string.Format("model.{0}", "SelectedMaxAge"));

                if (model.SelectedMinGrade == null && model.SelectedMaxGrade == null)
                {
                    ModelState.AddModelError(string.Format("model.{0}", "SelectedMinGrade"), "You must select min or max grade.");
                }
                else
                {
                    if (model.SelectedMinGrade.GetOrder() > model.SelectedMaxGrade.GetOrder())
                    {
                        ModelState.AddModelError(string.Format("model.{0}", "SelectedMinGrade"), "Min grade must be smaller than max grade.");
                    }
                }
            }

            if (!model.IsProrate)
            {
                ModelState.Remove(string.Format("model.{0}", "ProrateTime"));
            }

            foreach (var item in model.Days)
            {

                ModelState.Remove(string.Format("model.{0}", "ProrateTime"));
            }

            for (int i = 0; i < model.Days.Count(); i++)
            {
                ModelState.Remove(string.Format("model.Days[{0}].{1}", i, "StartTime"));
                ModelState.Remove(string.Format("model.Days[{0}].{1}", i, "EndTime"));
            }

            if (!model.HasEarlyBird)
            {
                for (int i = 0; i < model.EarlyBirds.Count(); i++)
                {
                    ModelState.Remove(string.Format("model.EarlyBirds[{0}].{1}", i, "Price"));
                    ModelState.Remove(string.Format("model.EarlyBirds[{0}].{1}", i, "ExpireDate"));
                }
            }

            if (!model.HasPayInFullOption)
            {
                ModelState.Remove("model.FullPayDiscount");
            }

            if (!model.EnableDropIn)
            {
                ModelState.Remove("model.DropInSessionLabel");
                ModelState.Remove("model.DropInSessionPrice");
            }
        }

        private void RestrictionsValidationCheck(ProgramScheduleViewModel schedule, int i)
        {
            if (schedule.SelectedRestrictionType == RestrictionType.None)
            {
                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "SelectedMinAge"));
                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "SelectedMaxAge"));

                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "SelectedMinGrade"));
                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "SelectedMaxGrade"));
            }
            else if (schedule.SelectedRestrictionType == RestrictionType.Age)
            {
                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "SelectedMinGrade"));
                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "SelectedMaxGrade"));

                if (schedule.SelectedMinAge == "0" && schedule.SelectedMaxAge == "0")
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "SelectedMinAge"), "You must select min or max age.");
                }

                if (schedule.SelectedMinAge != "0" && schedule.SelectedMaxAge != "0" && int.Parse(schedule.SelectedMinAge) > int.Parse(schedule.SelectedMaxAge))
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "SelectedMinAge"), "Min age must be smaller than max age.");
                }
            }
            else if (schedule.SelectedRestrictionType == RestrictionType.Grade)
            {
                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "SelectedMinAge"));
                ModelState.Remove(string.Format("model.Schedules[{0}].{1}", i, "SelectedMaxAge"));

                if (schedule.SelectedMinGrade == null && schedule.SelectedMaxGrade == null)
                {
                    ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "SelectedMinGrade"), "You must select min or max grade.");
                }
                else
                {
                    if (schedule.SelectedMinGrade.GetOrder() > schedule.SelectedMaxGrade.GetOrder())
                    {
                        ModelState.AddModelError(string.Format("model.Schedules[{0}].{1}", i, "SelectedMinGrade"), "Min grade must be smaller than max grade.");
                    }
                }
            }
        }

        private List<string> UploadProgramImages(Dictionary<string, string> listImg, string clubDomain)
        {
            var club = base.ActiveClub;

            var result = new List<string>();

            foreach (var image in listImg)
            {
                var newImage = image.Key;
                var srcImage = image.Value;
                if (!string.IsNullOrEmpty(newImage) && !newImage.Contains("http:"))
                {
                    string fileName = string.Format("{0}.jpg", Guid.NewGuid());
                    //Add src
                    string srcFileName = "large-" + fileName;
                    srcImage = srcImage.Replace("data:image/jpeg;base64,", string.Empty);

                    srcImage = srcImage.Replace("data:image/png;base64,", string.Empty);
                    var srcFileData = Convert.FromBase64String(srcImage);
                    Stream srcFileStream = new MemoryStream(srcFileData);


                    newImage = newImage.Replace("data:image/jpeg;base64,", string.Empty);

                    newImage = newImage.Replace("data:image/png;base64,", string.Empty);

                    var fileData = Convert.FromBase64String(newImage);

                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

                    var contentType = "image/jpeg";

                    Stream fileStream = new MemoryStream(fileData);

                    sm.UploadBlob(string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), fileName, fileStream, contentType);
                    sm.UploadBlob(string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), srcFileName, srcFileStream, contentType);

                    fileStream.Close();
                    srcFileStream.Close();
                    result.Add(fileName);
                }
            }

            return result;
        }

        public List<string> GetProgramImagesURL(List<string> fileNames, string clubDomain)
        {
            var ImageUrls = new List<string>();
            foreach (var name in fileNames)
            {
                string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), name).AbsoluteUri;

                if (string.IsNullOrEmpty(name))
                {
                    imageUri = string.Empty;
                }

                ImageUrls.Add(imageUri);
            }
            return ImageUrls;
        }

        public virtual ActionResult HasOrder(long programId)
        {
            var program = _programBusiness.Get(programId);

            if (_orderSessionBusiness.HasOrder(program))
            {
                return JsonNet(new { ChangeAllowed = false });
            }

            return JsonNet(new { ChangeAllowed = true });
        }

        [HttpGet]
        public virtual JsonNetResult SendParentsNotification(int programId, bool IsConfirmation)
        {
            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Domain);
            var program = _programBusiness.Get(programId);

            Club partner = null;
            var partnerId = club.PartnerId;
            if (partnerId.HasValue)
            {
                partner = club.PartnerClub;
                var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(partner.Id);
                var NotificationEmailFromPartner = partnerSetting.PortalParameters.NotificationEmailFromPartner;

                if (NotificationEmailFromPartner)
                {
                    club = partner;
                }
            }

            var model = new ProgramParentsEmailViewModel
            {
                EmailStyle = EmailStyle.Text,
                SelectedTemplateId = "0",
                From = club.ContactPersons.FirstOrDefault().Email,
                ProgramId = programId,
                IsConfirmation = IsConfirmation,
                Body = IsConfirmation ? clubBusiness.GetClubNotification(club, TypeClubNotification.Confirmation) : clubBusiness.GetClubNotification(club, TypeClubNotification.Cancellation),
                Subject = IsConfirmation ? "Class confirmation for " + program.Name : "Class cancellation for " + program.Name,

            };

            var templatesNames =
                club.Templates.Where(t => t.Type == TemplateType.Email)
                    .Select(temp => new SelectKeyValue<string>
                    {
                        Value = temp.Id.ToString(),
                        Text = temp.TemplateName
                    })
                    .ToList();

            templatesNames.Add(new SelectKeyValue<string> { Value = "0", Text = "New Template ..." });

            model.TemplateNames = templatesNames;

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SendParentsNotification(ProgramParentsEmailViewModel model)
        {
            var program = _programBusiness.Get(model.ProgramId);
            var programOrderItems = _programBusiness.GetProgramOrderitems(model.ProgramId);

            var programParents = programOrderItems.Select(o => o.Order.User).Distinct().ToList();

            var headerBackgroundName = "";
            var footerBackgroundName = "";

            CheckModelValidationEmailNotifications(model);

            if (ModelState.IsValid)
            {
                string body;

                if (model.EmailStyle == EmailStyle.Text)
                {
                    body =
                        HttpUtility.HtmlDecode(this.RenderViewToString(Constants.Path_EmailTemplate,
                            new EmailTemplateModel { Body = model.Body }));
                }
                else
                {
                    model.MailTemplate.ClubDomain = program.ClubDomain;
                    if (model.SelectedTemplateId == "0" && model.EmailStyle == EmailStyle.Template)
                    {
                        headerBackgroundName = model.MailTemplate.HeaderBackground;
                        footerBackgroundName = model.MailTemplate.FooterBackground;

                        if (!string.IsNullOrEmpty(model.MailTemplate.HeaderBackground) && !model.MailTemplate.HeaderBackground.Contains("http:") && !model.MailTemplate.HeaderBackground.Contains("https:"))
                        {
                            var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.MailTemplate.HeaderBackground, program.ClubDomain, "emailTemplate");
                            model.MailTemplate.HeaderBackground = headerPath;
                            headerBackgroundName = headerPath;
                        }

                        if (!string.IsNullOrEmpty(model.MailTemplate.FooterBackground) && !model.MailTemplate.FooterBackground.Contains("http:") && !model.MailTemplate.FooterBackground.Contains("https:"))
                        {
                            var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.MailTemplate.FooterBackground, program.ClubDomain, "emailTemplate");
                            model.MailTemplate.FooterBackground = headerPath;
                            footerBackgroundName = headerPath;
                        }


                        if (!string.IsNullOrEmpty(model.MailTemplate.HeaderBackground) && !model.MailTemplate.HeaderBackground.Contains("http:") && !model.MailTemplate.HeaderBackground.Contains("https:"))
                        {
                            model.MailTemplate.HeaderBackground = GetImageURL(model.MailTemplate.HeaderBackground, program.ClubDomain, "emailTemplate");
                        }

                        if (!string.IsNullOrEmpty(model.MailTemplate.FooterBackground) && !model.MailTemplate.FooterBackground.Contains("http:") && !model.MailTemplate.FooterBackground.Contains("https:"))
                        {
                            model.MailTemplate.FooterBackground = GetImageURL(model.MailTemplate.FooterBackground, program.ClubDomain, "emailTemplate");
                        }
                    }

                    //model.MailTemplate.IsLive = true;
                    body =
                        HttpUtility.HtmlDecode(this.RenderViewToString(Constants.Path_EmailTemplate, model.MailTemplate));
                }

                var cc = new List<MailAddress> { new MailAddress(model.From) };

                var from = new MailAddress(model.From);

                List<MailAddress> recivers = programParents.Select(m => new MailAddress(m.UserName, m.UserName)).ToList();

                if (recivers.Count == 0)
                {
                    return
                       JsonNet(new JResult
                       {
                           Message = "You do not have recipients email.",
                           Status = false,
                       });
                }
                var providerName = "";
                var partnerName = "";

                var clubBusiness = Ioc.ClubBusiness;
                var club = clubBusiness.Get(ActiveClub.Domain);

                Club partner = null;
                var partnerId = club.PartnerId;

                if (partnerId.HasValue)
                {
                    partner = club.PartnerClub;
                    partnerName = partner.Name;
                }

                if (program.OutSourceSeasonId.HasValue)
                {
                    providerName = program.OutSourceSeason.Club.Name;
                }

                body = Ioc.OldEmailBusiness.GetParametersFromContact(body, program.Name, program.Season.Name.ToDescription(), "", ActiveClub.Name, providerName, partnerName);

                Ioc.OldEmailBusiness.SendProgramParentsEmail(programParents, from, model.Subject, body, programOrderItems);

                if (model.SelectedTemplateId == "0" && model.EmailStyle == EmailStyle.Template)
                {
                    model.MailTemplate.HeaderBackground = headerBackgroundName;
                    model.MailTemplate.FooterBackground = footerBackgroundName;
                    model.MailTemplate.Name = model.MailTemplate.NewName;

                    var newTemp = new EmailTemplate
                    {
                        Template = JsonConvert.SerializeObject(model.MailTemplate),
                        TemplateName = model.MailTemplate.NewName,
                        Type = TemplateType.Email,
                        Club_Id = ActiveClub.Id,
                    };

                    Ioc.EmailCampaignTemplate.SaveCampaignTemplate(newTemp);

                    var templateModel = (newTemp != null) ? JsonConvert.DeserializeObject<EmailTemplateModel>(newTemp.Template) : new EmailTemplateModel(); // new EmailTemplateModel();
                }


                if (model.ProgramId != 0)
                {
                    return JsonNet(new JResult { Status = true, Message = "Program" });
                }
            }
            return JsonFormResponse();
        }

        public string GetImageURL(string fileName, string clubDomain, string folderName)
        {

            string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, folderName), fileName).AbsoluteUri;

            imageUri = string.Concat(imageUri, "?", "rnd=", Guid.NewGuid());

            if (string.IsNullOrEmpty(fileName))
            {
                imageUri = string.Empty;
            }

            return imageUri;
        }
        [HttpPost]
        public virtual ActionResult SendTestEmailParentsNotification(ProgramParentsEmailViewModel model)
        {
            var program = _programBusiness.Get(model.ProgramId);
            CheckResourceForAccess(program);

            //CheckModelValidationEmailNotifications(model);
            if (ModelState.IsValid)
            {
                string body;

                if (model.EmailStyle == EmailStyle.Text)
                {
                    body =
                        HttpUtility.HtmlDecode(this.RenderViewToString(Constants.Path_EmailTemplate,
                            new EmailTemplateModel { Body = model.Body }));
                }
                else
                {
                    model.MailTemplate.ClubDomain = program.ClubDomain;

                    if (model.SelectedTemplateId == "0" && model.EmailStyle == EmailStyle.Template)
                    {

                        if (!string.IsNullOrEmpty(model.MailTemplate.HeaderBackground) && !model.MailTemplate.HeaderBackground.Contains("http:") && !model.MailTemplate.HeaderBackground.Contains("https:"))
                        {
                            var headerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.MailTemplate.HeaderBackground, program.ClubDomain, "emailTemplate");
                            model.MailTemplate.HeaderBackground = GetImageURL(headerPath, program.ClubDomain, "emailTemplate");

                        }
                        if (!string.IsNullOrEmpty(model.MailTemplate.FooterBackground) && !model.MailTemplate.FooterBackground.Contains("http:") && !model.MailTemplate.FooterBackground.Contains("https:"))
                        {
                            var footerPath = Ioc.EmailCampaignTemplate.UploadImageTemplate(model.MailTemplate.FooterBackground, program.ClubDomain, "emailTemplate");
                            model.MailTemplate.FooterBackground = GetImageURL(footerPath, program.ClubDomain, "emailTemplate");

                        }

                    }
                    //model.MailTemplate.IsLive = true;
                    body =
                        HttpUtility.HtmlDecode(this.RenderViewToString(Constants.Path_EmailTemplate, model.MailTemplate));
                }

                var cc = new List<MailAddress> { new MailAddress(model.From) };

                var from = new MailAddress(model.From);

                var providerName = "";
                var partnerName = "";

                var clubBusiness = Ioc.ClubBusiness;
                var club = clubBusiness.Get(ActiveClub.Domain);

                Club partner = null;
                var partnerId = club.PartnerId;
                if (partnerId.HasValue)
                {
                    partner = club.PartnerClub;
                    partnerName = partner.Name;
                }
                if (program.OutSourceSeasonId.HasValue)
                {
                    providerName = program.OutSourceSeason.Club.Name;
                }
                body = Ioc.OldEmailBusiness.GetParametersFromContact(body, program.Name, program.Season.Name.ToDescription(), "", ActiveClub.Name, providerName, partnerName);

                Ioc.OldEmailBusiness.SendEmail(from, new MailAddress(model.TestEmail), from, model.Subject, body, true);

                if (model.ProgramId != 0)
                {
                    return JsonNet(new JResult { Status = true, Message = "Program" });
                }

            }
            return JsonFormResponse();
        }

        public virtual JsonNetResult GetAllRecipients(long programId)
        {

            var termValue = Request.Params["term"] ?? string.Empty;
            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var programOrderItems = _programBusiness.GetProgramOrderitems(programId);
            var programParents = programOrderItems.Select(o => o.Order.User).Distinct().ToList();
            var allClubMembers = programParents.Select(user => new JbUser
            {
                Email = user.UserName,
                Id = user.Id,
            }).ToList();

            if (!string.IsNullOrEmpty(termValue))
            {
                allClubMembers =
                    allClubMembers.Where(
                        m =>
                            m.Email.ToLower().Contains(termValue.ToLower())).ToList();
            }

            var dataSource =
                allClubMembers.OrderByDescending(u => u.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            var total = allClubMembers.Count;

            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        private void CheckModelValidationEmailNotifications(ProgramParentsEmailViewModel model)
        {
            if ((model.MailTemplate.ChangeTemplate && int.Parse(model.SelectedTemplateId) == 0 &&
                 string.IsNullOrEmpty(model.MailTemplate.NewName)) && model.EmailStyle == EmailStyle.Template)
            {
                ModelState.AddModelError("model.MailTemplate.NewName", "Please enter a name for new templates.");
            }

            ModelState.Remove("model.TestEmail");
        }

        private void BeforeAfterCareStep2ValidationCheck(ModelStateDictionary modelState, BeforeAfterCareCreateEditModelStep2 model)
        {
            if (model.Days == null || !model.Days.Where(d => d.IsChecked).Any())
            {
                for (int j = 0; j < model.Days.Count(); j++)
                {
                    if (!model.Days[j].IsChecked)
                    {
                        ModelState.Remove($"model.Days[{j}].{"MorningStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"MorningEndTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonEndTime"}");
                    }
                }

                ModelState.AddModelError($"model.{"Days"}", "You should choose at least one day.");
            }

            if (model.StartDate.HasValue && model.EndDate.HasValue && model.StartDate.Value > model.EndDate.Value)
            {
                ModelState.AddModelError($"model.{"StartDate"}", "Start date should be before of end date.");
            }

            if (model.RegistrationPeriod == null)
            {
                model.RegistrationPeriod = new RegistrationPeriodViewModel();
            }

            if (model.RegistrationPeriod.RegisterStartDate.HasValue && model.RegistrationPeriod.RegisterEndDate.HasValue && model.RegistrationPeriod.RegisterStartDate.Value > model.RegistrationPeriod.RegisterEndDate.Value)
            {
                ModelState.AddModelError($"model.{"RegistrationPeriod.RegisterStartDate"}", "Register Start date should be before register end date.");
            }

            if (model.RegistrationPeriod.RegisterEndDate.HasValue && model.EndDate.HasValue && model.RegistrationPeriod.RegisterEndDate.Value > model.EndDate.Value)
            {
                ModelState.AddModelError($"model.{"RegistrationPeriod.RegisterEndDate"}", "Registration end date should be before the program end date.");
            }

            if (model.SelectedContinueType == ContinueType.For)
            {
                ModelState.Remove($"model.{"EndDate"}");
            }

            if (!model.IsBeforeSchool)
            {
                ModelState.Remove("model.BeforeSchoolDescription");
                ModelState.Remove("model.BeforeSchoolTitle");
            }
            if (!model.IsAfterSchool)
            {
                ModelState.Remove("model.AfterSchoolDescription");
                ModelState.Remove("model.AfterSchoolTitle");
            }

            if (!model.IsCombo)
            {
                ModelState.Remove("model.ComboDescription");
                ModelState.Remove("model.ComboTitle");
            }

            if (model.IsBeforeSchool && !model.IsAfterSchool)
            {
                for (int j = 0; j < model.Days.Count(); j++)
                {
                    if (!model.Days[j].IsChecked)
                    {
                        ModelState.Remove($"model.Days[{j}].{"MorningStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"MorningEndTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonEndTime"}");
                    }
                    else
                    {
                        ModelState.Remove($"model.Days[{j}].{"AfternoonStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonEndTime"}");
                    }

                    if (model.Days[j].IsChecked && model.Days[j].MorningStartTime.HasValue && model.Days[j].MorningEndTime.HasValue && model.Days[j].MorningStartTime.Value >= model.Days[j].MorningEndTime.Value)
                    {
                        ModelState.AddModelError($"model.Days[{j}].{"MorningStartTime"}", "Start time should be before of end time.");
                    }
                }
            }

            if (!model.IsBeforeSchool && model.IsAfterSchool)
            {
                for (int j = 0; j < model.Days.Count(); j++)
                {

                    if (!model.Days[j].IsChecked)
                    {
                        ModelState.Remove($"model.Days[{j}].{"MorningStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"MorningEndTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonEndTime"}");
                    }
                    else
                    {
                        ModelState.Remove($"model.Days[{j}].{"MorningStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"MorningEndTime"}");
                    }

                    if (model.Days[j].IsChecked && model.Days[j].AfternoonStartTime.HasValue && model.Days[j].AfternoonEndTime.HasValue && model.Days[j].AfternoonStartTime.Value >= model.Days[j].AfternoonEndTime.Value)
                    {
                        ModelState.AddModelError($"model.Days[{j}].{"AfternoonStartTime"}", "Start time should be before of end time.");
                    }
                }
            }

            if (!model.IsBeforeSchool && !model.IsAfterSchool)
            {
                for (int j = 0; j < model.Days.Count(); j++)
                {
                    if (!model.Days[j].IsChecked)
                    {
                        ModelState.Remove($"model.Days[{j}].{"MorningStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"MorningEndTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonEndTime"}");

                    }
                }
            }

            if (model.IsBeforeSchool && model.IsAfterSchool)
            {
                for (int j = 0; j < model.Days.Count(); j++)
                {
                    if (!model.Days[j].IsChecked)
                    {
                        ModelState.Remove($"model.Days[{j}].{"MorningStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"MorningEndTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonStartTime"}");

                        ModelState.Remove($"model.Days[{j}].{"AfternoonEndTime"}");
                    }

                    if (model.Days[j].IsChecked && model.Days[j].MorningStartTime.HasValue && model.Days[j].MorningEndTime.HasValue && model.Days[j].MorningStartTime.Value >= model.Days[j].MorningEndTime.Value)
                    {
                        ModelState.AddModelError($"model.Days[{j}].{"MorningStartTime"}", "Start time should be before of end time.");
                    }
                    if (model.Days[j].IsChecked && model.Days[j].AfternoonStartTime.HasValue && model.Days[j].AfternoonEndTime.HasValue && model.Days[j].AfternoonStartTime.Value >= model.Days[j].AfternoonEndTime.Value)
                    {
                        ModelState.AddModelError($"model.Days[{j}].{"AfternoonStartTime"}", "Start time should be before of end time.");
                    }
                }
            }

            if (model.SelectedRestrictionType == RestrictionType.None)
            {
                ModelState.Remove($"model.{"SelectedMinAge"}");
                ModelState.Remove($"model.{"SelectedMaxAge"}");

                ModelState.Remove($"model.{"SelectedMinGrade"}");
                ModelState.Remove($"model.{"SelectedMaxGrade"}");
            }
            else if (model.SelectedRestrictionType == RestrictionType.Age)
            {
                ModelState.Remove($"model.{"SelectedMinGrade"}");
                ModelState.Remove($"model.{"SelectedMaxGrade"}");

                if (model.SelectedMinAge == "0" && model.SelectedMaxAge == "0")
                {
                    ModelState.AddModelError($"model.{"SelectedMinAge"}", "You must select min or max age.");
                }

                if (model.SelectedMinAge != "0" && model.SelectedMaxAge != "0" && int.Parse(model.SelectedMinAge) > int.Parse(model.SelectedMaxAge))
                {
                    ModelState.AddModelError($"model.{"SelectedMinAge"}", "Min age must be smaller than max age.");
                }
            }
            else if (model.SelectedRestrictionType == RestrictionType.Grade)
            {
                ModelState.Remove($"model.{"SelectedMinAge"}");
                ModelState.Remove($"model.{"SelectedMaxAge"}");

                if (model.SelectedMinGrade == null && model.SelectedMaxGrade == null)
                {
                    ModelState.AddModelError($"model.{"SelectedMinGrade"}", "You must select min or max grade.");
                }
                else
                {
                    if (model.SelectedMinGrade.GetOrder() > model.SelectedMaxGrade.GetOrder())
                    {
                        ModelState.AddModelError($"model.{"SelectedMinGrade"}", "Min grade must be smaller than max grade.");
                    }
                }
            }

            if (model.SelectedPaymentSchaduleMode == null)
            {
                ModelState.AddModelError("model.SelectedPaymentSchaduleMode", "You should select payment period.");
            }

            if (model.SelectedPaymentSchaduleMode != PaymentSchedule.Monthly)
            {
                ModelState.Remove("model.SelectedPaymentSchaduleType");
            }
            else
            {
                if (model.SelectedPaymentSchaduleType != MonthlyPaymentScheduleType.Calendar && model.SelectedPaymentSchaduleType != MonthlyPaymentScheduleType.RollOver)
                {
                    ModelState.AddModelError("model.SelectedPaymentSchaduleType", "Schedule type is required.");
                }
            }

            if (model.EnablePunchCard)
            {
                if (model.SelectedPunchcard == 0)
                {
                    ModelState.AddModelError("model.SelectedPunchcard", "You should select the number of days.");
                }
            }
            else
            {
                ModelState.Remove("model.SelectedPunchcard");
                ModelState.Remove("model.PunchcardSessionPrice");
                ModelState.Remove("model.PunchcardSessionLabel");
            }

            if (!model.EnableDropIn)
            {
                ModelState.Remove("model.DropInSessionPrice");
                ModelState.Remove("model.DropInSessionLabel");
                ModelState.Remove("model.DropInSameSessionPrice");
            }

            if (!model.EnableSameDropInPrice)
            {
                ModelState.Remove("model.DropInSameSessionPrice");
            }
        }

        private ProgramSchedule SetBeforeAfterCareSchedules(ProgramSchedule programSchedule, Program program, BeforeAfterCareCreateEditModelStep2 model, decimal payInFullDiscount, int capacity = 0)
        {
            programSchedule.Charges = new List<Charge>();

            //get day for each schedule
            var days = new List<ProgramScheduleDay>();

            switch (programSchedule.ScheduleMode)
            {
                case TimeOfClassFormation.AM:
                    {
                        programSchedule.Id = model.BeforeProgramScheduleId;

                        days = model.Days.Where(d => d.IsChecked).ToList().Select(d =>
                                          new ProgramScheduleDay
                                          {
                                              DayOfWeek = d.DayOfWeek,
                                              EndTime = d.MorningEndTime.HasValue ? d.MorningEndTime.Value.TimeOfDay : (TimeSpan?)null,
                                              StartTime = d.MorningStartTime.HasValue ? d.MorningStartTime.Value.TimeOfDay : (TimeSpan?)null,
                                          })
                                            .ToList();
                    }
                    break;
                case TimeOfClassFormation.PM:
                    {
                        programSchedule.Id = model.AfterProgramScheduleId;
                        days = model.Days.Where(d => d.IsChecked).ToList().Select(d =>
                                          new ProgramScheduleDay
                                          {
                                              DayOfWeek = d.DayOfWeek,
                                              EndTime = d.AfternoonEndTime.HasValue ? d.AfternoonEndTime.Value.TimeOfDay : (TimeSpan?)null,
                                              StartTime = d.AfternoonStartTime.HasValue ? d.AfternoonStartTime.Value.TimeOfDay : (TimeSpan?)null,
                                          })
                                             .ToList();
                    }
                    break;
                case TimeOfClassFormation.Both:
                    {
                        programSchedule.Id = model.ComboProgramScheduleId;
                        days = model.Days.Where(d => d.IsChecked).ToList().Select(d =>
                                          new ProgramScheduleDay
                                          {
                                              DayOfWeek = d.DayOfWeek,
                                          })
                                             .ToList();
                    }
                    break;
                default:
                    {
                        throw new NotImplementedException();
                    }
            }

            var numberOfClassDays = new List<SelectKeyValue<int>>();
            numberOfClassDays = model.NumberOfClassDays;
            decimal dropInFee = model.DropInSessionPrice.HasValue ? model.DropInSessionPrice.Value : 0;
            decimal dropInSameFee = model.DropInSameSessionPrice.HasValue ? model.DropInSameSessionPrice.Value : 0;

            programSchedule.Attributes = new ScheduleAfterBeforeCareAttribute()
            {
                ContinueType = model.SelectedContinueType,
                Occurances = model.SelectedContinueType == ContinueType.For ? int.Parse(model.SelectedSession) : 0,
                IsProrate = model.HasProrate,
                ProrateTime = model.ProrateTime,
                HasPayInFullOption = model.HasPayInFullOption,
                EnableDropIn = model.EnableDropIn,
                EnableSameDropInPrice = model.EnableSameDropInPrice,
                PaymentDueDate = model.SelectedDueDate,
                GetFirstPaymentAtTheRegisterTime = model.GetPaymentAtRefister,
                PaymentScheduleMode = model.SelectedPaymentSchaduleMode.ToString(),
                MonthlyScheduleType = model.SelectedPaymentSchaduleType.ToString(),
                NumberOfClassDays = numberOfClassDays,
                Days = days,
                DropInSessionLabel = model.DropInSessionLabel,
                DropInSessionPrice = dropInFee,
                DropInSameSessionPrice = dropInSameFee,
                FullPayDiscount = payInFullDiscount,
                Capacity = capacity,
                PunchCard = new PunchCardAttribute()
                {
                    Enabled = model.EnablePunchCard,
                    Amount = model.PunchcardSessionPrice.HasValue ? model.PunchcardSessionPrice.Value : (decimal?)null,
                    Title = model.PunchcardSessionLabel,
                    SessionNumber = model.SelectedPunchcard,
                }
            };

            programSchedule.AttendeeRestriction = new AttendeeRestriction();
            programSchedule.AttendeeRestriction.Gender = model.SelectedGender;
            programSchedule.AttendeeRestriction.MinGrade = model.SelectedMinGrade;
            programSchedule.AttendeeRestriction.MaxGrade = model.SelectedMaxGrade;
            programSchedule.AttendeeRestriction.ApplyAtProgramStart = model.AppliesRestrictionAtTheProgramStart;

            programSchedule.AttendeeRestriction.RestrictionType = model.SelectedRestrictionType;

            if (string.IsNullOrEmpty(model.SelectedMinAge) || model.SelectedMinAge.Equals("0"))
            {
                programSchedule.AttendeeRestriction.MinAge = null;
            }
            else
            {
                programSchedule.AttendeeRestriction.MinAge = int.Parse(model.SelectedMinAge);
            }

            if (string.IsNullOrEmpty(model.SelectedMaxAge) || model.SelectedMaxAge.Equals("0"))
            {
                programSchedule.AttendeeRestriction.MaxAge = null;
            }
            else
            {
                programSchedule.AttendeeRestriction.MaxAge = int.Parse(model.SelectedMaxAge);
            }

            programSchedule.StartDate = model.StartDate.Value;

            if (model.SelectedContinueType == ContinueType.Until)
            {
                programSchedule.EndDate = model.EndDate.Value;
            }
            else
            {
                programSchedule.EndDate = model.StartDate.Value;

                for (int i = 0; i < int.Parse(model.SelectedSession); i++)
                {
                    programSchedule.EndDate = programSchedule.EndDate.AddDays(7);
                }
            }

            programSchedule.RegistrationPeriod = new RegistrationPeriod()
            {
                RegisterStartDate = model.RegistrationPeriod.RegisterStartDate,
                RegisterEndDate = model.RegistrationPeriod.RegisterEndDate,
            };

            programSchedule.ProgramId = program.Id;


            return programSchedule;
        }

        private void GetProgramAttributeValues(Program program, BeforeAfterCareCreateEditModelStep2 model)
        {
            var defaultProgramSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both);
            var defaultScheduleAttribute = (ScheduleAfterBeforeCareAttribute)defaultProgramSchedule.Attributes;

            var beforeschedule = program.ProgramSchedules.Where(p => !p.IsDeleted && p.ScheduleMode == TimeOfClassFormation.AM).FirstOrDefault();
            var Afterchedule = program.ProgramSchedules.Where(p => !p.IsDeleted && p.ScheduleMode == TimeOfClassFormation.PM).FirstOrDefault();
            var comboSchedule = program.ProgramSchedules.Where(p => !p.IsDeleted && !p.IsDeleted && p.ScheduleMode == TimeOfClassFormation.Both).FirstOrDefault();

            model.IsAfterSchool = Afterchedule != null ? true : false;
            model.IsBeforeSchool = beforeschedule != null ? true : false;
            model.IsCombo = comboSchedule != null ? true : false;
            model.AfterSchoolTitle = Afterchedule != null ? Afterchedule.Title : model.AfterSchoolTitle;
            model.BeforeSchoolTitle = beforeschedule != null ? beforeschedule.Title : model.BeforeSchoolTitle;
            model.ComboTitle = comboSchedule != null ? comboSchedule.Title : model.ComboTitle;

            //description
            model.BeforeSchoolDescription = beforeschedule != null ? beforeschedule.Description : model.BeforeSchoolDescription;
            model.AfterSchoolDescription = Afterchedule != null ? Afterchedule.Description : model.AfterSchoolDescription;
            model.ComboDescription = comboSchedule != null ? comboSchedule.Description : model.ComboDescription;


            model.HasPayInFullOption = defaultScheduleAttribute.HasPayInFullOption;
            model.EnableDropIn = defaultScheduleAttribute.EnableDropIn;
            model.SelectedDueDate = defaultScheduleAttribute.PaymentDueDate;
            model.GetPaymentAtRefister = defaultScheduleAttribute.GetFirstPaymentAtTheRegisterTime;
            model.EnablePunchCard = defaultScheduleAttribute.PunchCard.Enabled;
            model.EnableSameDropInPrice = defaultScheduleAttribute.EnableSameDropInPrice;

            if (model.EnableDropIn)
            {
                model.DropInSessionLabel = defaultScheduleAttribute.DropInSessionLabel;
                model.DropInSessionPrice = defaultScheduleAttribute.DropInSessionPrice;

                if (model.EnableSameDropInPrice)
                {
                    model.DropInSameSessionPrice = defaultScheduleAttribute.DropInSameSessionPrice;
                }
            }
            if (model.EnablePunchCard)
            {
                model.PunchcardSessionLabel = defaultScheduleAttribute.PunchCard.Title;
                model.PunchcardSessionPrice = defaultScheduleAttribute.PunchCard.Amount;
                model.SelectedPunchcard = defaultScheduleAttribute.PunchCard.SessionNumber;
            }

            if (defaultScheduleAttribute.PaymentScheduleMode == "Monthly")
            {
                model.SelectedPaymentSchaduleMode = PaymentSchedule.Monthly;
            }
            else if (defaultScheduleAttribute.PaymentScheduleMode == "BiWeekly")
            {
                model.SelectedPaymentSchaduleMode = PaymentSchedule.BiWeekly;
            }
            else
            {
                model.SelectedPaymentSchaduleMode = PaymentSchedule.Weekly;
            }
            //get payment schedule type
            if (defaultScheduleAttribute.MonthlyScheduleType == "Calendar")
            {
                model.SelectedPaymentSchaduleType = MonthlyPaymentScheduleType.Calendar;
            }
            else if (defaultScheduleAttribute.MonthlyScheduleType == "RollOver")
            {
                model.SelectedPaymentSchaduleType = MonthlyPaymentScheduleType.RollOver;
            }

            model.RegistrationPeriod = new RegistrationPeriodViewModel()
            {
                RegisterStartDate = defaultProgramSchedule.RegistrationPeriod.RegisterStartDate,
                RegisterEndDate = defaultProgramSchedule.RegistrationPeriod.RegisterEndDate,
            };

            model.SelectedGender = defaultProgramSchedule.AttendeeRestriction.Gender.Value;
            model.SelectedContinueType = defaultScheduleAttribute.ContinueType;
            model.SelectedRestrictionType = defaultProgramSchedule.AttendeeRestriction.RestrictionType;

            model.SelectedMinGrade = defaultProgramSchedule.AttendeeRestriction.MinGrade;
            model.SelectedMaxGrade = defaultProgramSchedule.AttendeeRestriction.MaxGrade;

            model.SelectedMinAge = defaultProgramSchedule.AttendeeRestriction.MinAge.HasValue ? defaultProgramSchedule.AttendeeRestriction.MinAge.Value.ToString() : "0";
            model.SelectedMaxAge = defaultProgramSchedule.AttendeeRestriction.MaxAge.HasValue ? defaultProgramSchedule.AttendeeRestriction.MaxAge.Value.ToString() : "0";

            model.AppliesRestrictionAtTheProgramStart = defaultProgramSchedule.AttendeeRestriction.ApplyAtProgramStart;

            model.NumberOfClassDays = defaultScheduleAttribute.NumberOfClassDays;
        }

        private List<BeforeAfterFeesItemViewModel> getScheduleOptionFess(ProgramSchedule schedule, List<SelectKeyValue<int>> numberOfClassDays)
        {
            var result = new List<BeforeAfterFeesItemViewModel>();

            var editMode = schedule.Charges.Any(c => c.Category == ChargeDiscountCategory.EntryFee) ? true : false;

            if (editMode)
            {
                var charges = schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted);

                foreach (var charge in charges)
                {
                    var chargeWeekDay = ((BeforeAfterCareChargeAttribute)charge.Attributes).NumberOfClassDay;

                    foreach (var numberDay in numberOfClassDays)
                    {
                        if (numberOfClassDays.Any(n => chargeWeekDay.Contains(n.Text)))
                        {
                            result.Add(new BeforeAfterFeesItemViewModel
                            {
                                NumberOfClassDay = chargeWeekDay,
                                ScheduleMode = schedule.ScheduleMode,
                                ScheduleId = schedule.Id,
                                Amount = charge.Amount,
                                TuitionLabel = charge.Name,
                                ChargeId = charge.Id,
                                IsDeleted = charge.IsDeleted
                            });

                            break;
                        }
                        else
                        {
                            //if number day is checked
                            if (numberDay.Value == 1)
                            {
                                result.Add(new BeforeAfterFeesItemViewModel
                                {
                                    NumberOfClassDay = numberDay.Text,
                                    ScheduleMode = schedule.ScheduleMode,
                                    ScheduleId = schedule.Id,
                                    IsDeleted = false
                                });
                            }
                            break;
                        }
                    }
                }

                var newDays = numberOfClassDays.Where(n => n.Value == 1 && !result.Select(r => r.NumberOfClassDay).ToList().Contains(n.Text));

                foreach (var day in newDays)
                {
                    result.Add(new BeforeAfterFeesItemViewModel
                    {
                        NumberOfClassDay = day.Text,
                        ScheduleMode = schedule.ScheduleMode,
                        ScheduleId = schedule.Id,
                    });
                }
            }
            else
            {
                foreach (var numberDay in numberOfClassDays)
                {
                    //if number day is checked
                    if (numberDay.Value == 1)
                    {
                        result.Add(new BeforeAfterFeesItemViewModel
                        {
                            NumberOfClassDay = numberDay.Text,
                            ScheduleMode = schedule.ScheduleMode,
                            ScheduleId = schedule.Id,
                        });
                    }
                }
            }

            return result;
        }

        private void addChargeToSchedule(List<BeforeAfterFeesItemViewModel> fees, Program program, bool isProgramSchedulePartChanged, bool isChargesChanged)
        {
            var schedules = new List<ProgramSchedule>();
            var groupScheduleFee = fees.GroupBy(f => f.ScheduleId);

            var addPartCharges = fees.Any(f => f.ChargeId > 0) ? false : true;

            foreach (var groupFee in groupScheduleFee)
            {
                var firstFee = groupFee.First();

                var schedule = program.ProgramSchedules.Where(s => s.Id == firstFee.ScheduleId).First();

                foreach (var fee in groupFee)
                {
                    if (fee.ChargeId != 0)
                    {
                        var orginalCharge = schedule.Charges.Where(c => c.Id == fee.ChargeId).FirstOrDefault();

                        orginalCharge.Name = fee.TuitionLabel;
                        orginalCharge.Amount = fee.Amount.Value;
                        orginalCharge.MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow };
                        orginalCharge.HasEarlyBird = false;
                        orginalCharge.Attributes = new BeforeAfterCareChargeAttribute()
                        {
                            NumberOfClassDay = fee.NumberOfClassDay,
                        };
                    }
                    else
                    {
                        schedule.Charges.Add(
                            new Charge
                            {
                                Id = fee.ChargeId,
                                Name = fee.TuitionLabel,
                                Amount = fee.Amount.Value,
                                Category = ChargeDiscountCategory.EntryFee,
                                MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                                HasEarlyBird = false,
                                Attributes = new BeforeAfterCareChargeAttribute()
                                {
                                    NumberOfClassDay = fee.NumberOfClassDay,
                                }
                            });
                    }
                }
            }

            schedules = program.ProgramSchedules.ToList();
            var res = _programBusiness.UpdateCharges(program, schedules);

            //set price for each schedule part
            if (res.Status)
            {
                var programScheduleParts = _programBusiness.GetScheduleParts(program.Id);
                var programCharges = program.ProgramSchedules.Where(p => !p.IsDeleted).SelectMany(s => s.Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee)).ToList();
                if (isProgramSchedulePartChanged || isChargesChanged || addPartCharges)
                {
                    foreach (var schedulePart in programScheduleParts)
                    {
                        _programBusiness.CreateSchedulePartCharges(programCharges, schedulePart);
                    }
                }
            }
        }
        private List<SelectKeyValue<int>> SeedPunchcardSessions(int max)
        {
            var result = new List<SelectKeyValue<int>>();

            result.Add(new SelectKeyValue<int> { Text = "Select", Value = 0 });

            for (int i = 1; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<int> { Text = i.ToString(), Value = i });
            }

            return result;
        }
    }
}