﻿using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Data.Repositories
{
    public class ClubRepository : BaseRepository<Club>, IClubRepository
    {
        public ClubRepository(IEntitiesContext context)
            : base(context)
        {
        }

        public IQueryable<Club> GetMembersOfPartner(long partnerId, bool includePartner = false)
        {
            var result = GetList().Where(c => c.PartnerId == partnerId || includePartner && c.Id == partnerId);

            return result;
        }

        public IQueryable<Club> GetList(List<int> clubsIds)
        {
            var result = base.GetList(c => clubsIds.Contains(c.Id)); 

            return result;
        }
    }
}
