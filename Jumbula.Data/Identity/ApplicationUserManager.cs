﻿using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Jumbula.Data.Identity
{
    public class ApplicationUserManager : UserManager<JbUser, int>, IApplicationUserManager<JbUser, int>
    {
        public ApplicationUserManager(IUserStore<JbUser, int> store)
            : base(store)
        {

            ConfigureValidations(this);
        }

        public bool IsUserExist(string userName)
        {
            var result = FindByNameAsync(userName).Result;

            return result != null;
        }

        public override Task<IList<string>> GetRolesAsync(int userId)
        {
            var allRoles = base.GetRolesAsync(userId);

            IList<string> roles = allRoles.Result;


            return Task.FromResult(roles);
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options,
            IOwinContext context)
        {
            var manager = new ApplicationUserManager(
                new JbUserStore(context.Get<JumbulaDataContext>()));

            ConfigureValidations(manager);

            // Register two factor authentication providers. This application uses Phone 
            // and Emails as a step of receiving a code for verifying the user 
            // You can write your own provider and plug in here. 
            manager.RegisterTwoFactorProvider("PhoneCode",
                new PhoneNumberTokenProvider<JbUser, int>
                {
                    MessageFormat = "Your security code is: {0}"
                });
            manager.RegisterTwoFactorProvider("EmailCode",
                new EmailTokenProvider<JbUser, int>
                {
                    Subject = "Security Code",
                    BodyFormat = "Your security code is: {0}"
                });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<JbUser, int>(
                        dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return manager;
        }

        private static void ConfigureValidations(ApplicationUserManager manager)
        {
            // Configure validation logic for usernames 
            manager.UserValidator = new UserValidator<JbUser, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,
            };

            // Configure validation logic for passwords 
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 8,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };
        }

        public bool IsInRole(int userId, string role)
        {
            return IsInRoleAsync(userId, role).Result;
        }

        public IdentityResult AddToRole(int userId, string role)
        {
            return AddToRoleAsync(userId, role).Result;
        }

        public IdentityResult ConfirmEmail(int userId, string token)
        {
            return ConfirmEmailAsync(userId, token).Result;
        }

        public string GeneratePasswordResetToken(int userId)
        {

            UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(
               new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create(
                   "EmailConfirmation"))
            {
                TokenLifespan = TimeSpan.FromDays(15.0)
            };

            return GeneratePasswordResetTokenAsync(userId).Result;
        }

        public IdentityResult ResetPassword(int userId, string token, string newPassword)
        {
            return ResetPasswordAsync(userId, token, newPassword).Result;
        }

        public bool IsEmailConfirmed(int userId)
        {
            return IsEmailConfirmedAsync(userId).Result;
        }

        public IdentityResult RemovePassword(int userId)
        {
            return RemovePasswordAsync(userId).Result;
        }

        public IdentityResult AddPassword(int userId, string password)
        {
            return AddPasswordAsync(userId, password).Result;
        }

        public JbUser FindById(int userId)
        {
            return FindByIdAsync(userId).Result;
        }

        public JbUser FindByName(string userName)
        {
            return FindByNameAsync(userName).Result;
        }

        public IdentityResult Create(JbUser user)
        {
            return CreateAsync(user).Result;
        }

        public IdentityResult Update(JbUser user)
        {
            return UpdateAsync(user).Result;
        }

        public bool CheckPassword(JbUser user, string password)
        {
            return CheckPasswordAsync(user, password).Result;
        }

        public string GenerateEmailConfirmationToken(int userId)
        {
            return GenerateEmailConfirmationTokenAsync(userId).Result;
        }

        public IdentityResult Create(JbUser user, string password)
        {
            return CreateAsync(user, password).Result;
        }

        public IdentityResult ChangePassword(int userId, string currentPassword, string newPassword)
        {
            return ChangePasswordAsync(userId, currentPassword, newPassword).Result;
        }
    }

    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
