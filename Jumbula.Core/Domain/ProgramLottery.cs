﻿using Jumbula.Common.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class ProgramLottery : BaseEntity
    {
        [ForeignKey("ProgramId")]
        public virtual Program Program { get; set; }
        public int ProgramId { get; set; }

        public DateTime DrawDateTime { get; set; }
    }
}
