﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AdvanceBackgroundCheckViewModel
    {
        public AdvanceBackgroundCheckViewModel()
        {
            BackgroundCertificatesURLs = new List<string>();
            BackgroundCertificatesNames = new List<string>();
            BackgroundCertificatesDates = new List<string>();
        }

        public List<string> BackgroundCertificatesURLs { get; set; }

        public List<string> BackgroundCertificatesDates { get; set; }

        public List<string> BackgroundCertificatesNames { get; set; }

        public bool IsAdmin { get; set; }

    }



}