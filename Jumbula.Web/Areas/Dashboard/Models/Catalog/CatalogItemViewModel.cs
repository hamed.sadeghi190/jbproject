﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CatalogItemViewModel
    {
        public int Id { get; set; }

        public string StringId
        {
            get
            {
                return Id == 0 ? Guid.NewGuid().ToString() : Id.ToString();
            }
        }

        public CatalogStatus ItemStatus { get; set; }

        public string Name { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }
    }
}