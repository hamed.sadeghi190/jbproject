﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;
using Newtonsoft.Json;

namespace Jumbula.Core.Domain
{
    public class Season : BaseEntity<long>
    {
        public Season()
        {
            MetaData = new MetaData()
            {
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };

            HasOutSourcePrograms = false;

            OutSourcePrograms = new HashSet<Program>();
            Discounts = new HashSet<Discount>();
            PaymentPlans = new HashSet<PaymentPlan>();
            Coupons = new HashSet<Coupon>();
        }

        private SeasonSettings _setting;
        [MaxLength(128)]
        [MinLength(3)]
        public string Title { get; set; }
        public bool IsSandBox { get; set; }
        [MaxLength(128)]
        [MinLength(3)]
        public string Domain { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }
        public SeasonNames? Name { get; set; }
        public int? Year { get; set; }

        [MaxLength(4000)]
        public string Note { get; set; }
        public virtual ICollection<SeasonFile> Files { get; set; }
        public SeasonStatus Status { get; set; }
        public MetaData MetaData { get; set; }
        public int ClubId { get; set; }
        public virtual Club Club { get; set; }
        public bool HasOutSourcePrograms { get; set; }
        [Column("Setting")]
        public string SettingSerialized { get; set; }

        [ForeignKey("ParentId")]
        public virtual Season Parent { get; set; }

        public bool IsArchived { get; set; }

        public long? ParentId { get; set; }

        public virtual ICollection<Season> Childs { get; set; }
        public virtual ICollection<SeasonPaymentPlan> SeasonPaymentPlans { get; set; }

        [NotMapped]
        public bool IsSettingChanged
        {
            get; set;
        }
        [NotMapped]
        public SeasonSettings Setting
        {
            get
            {
                if (_setting == null)
                {

                    if (!string.IsNullOrEmpty(SettingSerialized))
                    {
                        if (Club != null && Club.IsSchool)
                        {
                            _setting = JsonConvert.DeserializeObject<SeasonSchoolSetting>(SettingSerialized);
                        }
                        else
                        {
                            _setting = JsonConvert.DeserializeObject<SeasonSettings>(SettingSerialized);
                        }

                    }
                    else
                    {
                        if (Club != null && Club.IsSchool)
                        {
                            _setting = new SeasonSchoolSetting();
                        }
                        else
                        {
                            _setting = new SeasonSettings();
                        }
                    }
                }

                return _setting;
            }

            set
            {
                _setting = value;

            }
        }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        public virtual ICollection<Coupon> Coupons { get; set; }

        public virtual ICollection<Charge> Charges { get; set; }
        public virtual ICollection<Discount> Discounts { get; set; }
        public virtual ICollection<PaymentPlan> PaymentPlans { get; set; }
        public virtual EmailTemplate Template { get; set; }

        public virtual ICollection<Program> Programs { get; set; }
        public virtual ICollection<Program> OutSourcePrograms { get; set; }
        public virtual ICollection<TransactionActivity> TransactionActivities { get; set; }
    }
    public enum SeasonStatus
    {
        Test,
        Live,
        Deleted
    }

    public enum SeasonRegStatus
    {
        [Description("Not opened")]
        NotOpened = 0,
        [Description("Early open")]
        EarlyOpen = 1,
        [Description("General open")]
        GeneralOpen = 2,
        [Description("Late open")]
        LateOpen = 3,
        [Description("Closed")]
        Closed = 4,
        [Description("Early closed")]
        EarlyClosed = 5,
        [Description("General closed")]
        GeneralClosed = 6,
        [Description("Pre register open")]
        PreRegisterOpen = 7,
        [Description("Lottery open")]
        LotteryOpen = 8,
    }

    public enum SeasonNames : byte
    {
        [Description("Select season")]
        SelectSeason = 0,
        Fall = 5,
        [Description("Fall interim")]
        FallInterim = 10,
        [Description("Winter interim")]
        WinterInterim = 15,
        Winter = 20,
        Spring = 25,
        Summer = 30,
        [Description("Full year")]
        FullYear = 50
    }
}
