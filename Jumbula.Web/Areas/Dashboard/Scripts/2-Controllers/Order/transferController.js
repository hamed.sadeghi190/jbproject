﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('transferController', ['$scope', '$http', 'jbToast', '$state', '$stateParams', 'paymentHistoryService', 'jbConfirmation', function ($scope, $http, jbToast, $state, $stateParams, paymentHistoryService, jbConfirmation) {
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

      

        var counter = function () {
            setTimeout(function () {
                var options = {
                    useEasing: true,
                    useGrouping: true,
                    separator: ',',
                    decimal: '.',
                    prefix: symbol,
                    suffix: ''
                };
                var remainingStart = Number($("#remaining").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    newAmountStart = Number($("#newAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    differenceStart = Number($("#difference").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    paidStart = Number($("#paidAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0,
                    orderAmountStart = Number($("#orderAmount").text().replace(new RegExp("[^0-9.-]", 'g'), '')) || 0;


                var remainingEnd = Number($scope.calculateBalance()) || 0,
                    newAmountEnd = Number($scope.calculateTotalAmount()) || 0,
                    differenceEnd = Number($scope.calculateDifference()) || 0,
                    paidEnd = Number($scope.Model.PaidAmount) || 0,
                    orderAmountEnd = Number($scope.Model.OrderAmount) || 0;

                new countUp("remaining", remainingStart, remainingEnd, 2, .5, options).start();
                new countUp("newAmount", newAmountStart, newAmountEnd, 2, .5, options).start();
                new countUp("difference", differenceStart, differenceEnd, 2, .5, options).start();
                new countUp("paidAmount", paidStart, paidEnd, 2, .5, options).start();
                new countUp("orderAmount", orderAmountStart, orderAmountEnd, 2, .5, options).start();
            }, 200);
        };

        $scope.Number = Number;

        var returnedByBack = $state.returnByBack();
        $scope.Model = {};
   
        if (returnedByBack) {
            $scope.Model = paymentHistoryService.get();
            counter();
            paymentHistoryService.set(null);
        }
        if (!returnedByBack || $scope.Model == null) {
            if ($stateParams.orderId) {

                $http.get('Dashboard/Order/GetTransfer', { params: { orderItemId: $stateParams.orderId } })
                    .success(function (data, status, headers, config) {
                        if (data.Status == false) {

                            jbToast.error("We have encountered an internal error and cannot submit your changes. Please contact Jumbula support (support@jumbula.com) and provide a screenshot of this page.")

                        } else {
                            $scope.Model = data;
                            $scope.Model.PageStep = 'FirstPage';
                        }
                    })
            }
        }


        $scope.$watch("Model",
            function (newValue, oldValue) {
                if (newValue != oldValue) {
                    counter();
                }
            }, true
        );

        $scope.showInstallmentDetail = function (installmentId) {
            paymentHistoryService.set($scope.Model);
            $state.forward('PaymentHistory', { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderId, installmentId: installmentId }, 'Transfer', { seasonDomain: $stateParams.seasonDomain, orderId: $stateParams.orderId }, null);

        }

        $scope.transfer = function () {

            if ($scope.Model.ProgramType != 'Subscription' && $scope.Model.ProgramType != 'BeforeAfterCare') {        
                if (!$scope.hasInstallment() || $scope.calculateTotalAmount() == $scope.calculateTotalInstallments()) {

                    $scope.submit();
                }
                else {          
                    var message = 'Warning! The total sum of installments is: ' + $scope.calculateTotalInstallments().formatCurrencyWithPenny(2) + ' while the order total is: ' + $scope.calculateTotalAmount().formatCurrencyWithPenny(2) +
                        ' Your organization is responsible to handle the balance.';

                    jbConfirmation.message(message).yes(function () {

                        $scope.submit();
                    })
                        .confirm();
                }

            } else {
                $scope.submitTransfer();
            }
        }


        $scope.submit = function () {
            $http.post('Dashboard/Order/Transfer', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    if (data.Status == true) {

                        $scope.errors = null;

                        jbToast.success("The program is transferred successfully.");
                        if ("goToFamilyOrder" in window) {
                            if (window.goToFamilyOrder > 0) {
                                var userId = window.goToFamilyOrder;
                                window.goToFamilyOrder = -1;
                                $state.go('FamilyOrders', { userId: userId });
                            }
                        }
                        else {
                            $state.go('OrderItem', { seasonDomain: $stateParams.seasonDomain, orderItemId: data.Data });
                        }
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                })
        };



        $scope.submitTransfer = function () {

            var checkedDays = [];
            for (var i = 0; i < $scope.Days.length; i++) {

                if ($scope.Days[i].IsChecked == true) {

                    checkedDays.push($scope.Days[i].Title);
                }
            }
            $scope.Model.SelectedDays = checkedDays;

            $http.post('Dashboard/Order/TransferSubscription', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    if (data.Status == true) {

                        $scope.errors = null;

                        jbToast.success("The program is transferred successfully.");
                        if ("goToFamilyOrder" in window) {
                            if (window.goToFamilyOrder > 0) {
                                var userId = window.goToFamilyOrder;
                                window.goToFamilyOrder = -1;
                                $state.go('FamilyOrders', { userId: userId });
                            }
                        }
                        else {
                            $state.go('OrderItem', { seasonDomain: $stateParams.seasonDomain, orderItemId: data.Data });
                        }
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                })
        }


        $scope.loadSchedules = function () {

            $scope.Model.SelectedTution = null;
            $scope.Model.ChargeDiscounts = null;

            $http.get('Dashboard/Order/GetSchedules', { params: { programId: $scope.Model.SelectedProgram, orderItemId: $stateParams.orderId } })
                .success(function (data, status, headers, config) {

                    $scope.Schedules = data.Schedules;
                    $scope.Model.IsFrozen = data.IsFrozen;
                    $scope.Model.Schedules = data.Schedules;
                })
            $scope.GetSubscriptionDays($scope.Model.SelectedProgram);
        }

        $scope.GetSubscriptionDays = function (programId) {

            $http.get('Dashboard/Order/GetSubscriptionDays', { params: { programId: programId } })
                .success(function (data, status, headers, config) {

                    $scope.Days = data.AllDays;
                })
        }

        $scope.loadChargeDiscounts = function () {

            if ($scope.Model.ProgramType == 'Subscription' || $scope.Model.ProgramType == 'BeforeAfterCare') {

                $http.get('Dashboard/Order/GetChargeDiscounts', { params: { sourceOrderItemId: $scope.Model.Id, sourceProgramId: $scope.Model.SourceProgramId, targetProgramId: $scope.Model.SourceProgramId, tutionId: $scope.Model.SourceTutionId } })
                    .success(function (data, status, headers, config) {

                        $scope.Model.OrderChargeDiscounts = data.OrderChargeDiscounts;
                    })
            } else {
                $http.get('Dashboard/Order/GetChargeDiscounts', { params: { sourceOrderItemId: $scope.Model.Id, sourceProgramId: $scope.Model.SourceProgramId, targetProgramId: $scope.Model.SelectedProgram, tutionId: $scope.Model.SelectedTution } })
                    .success(function (data, status, headers, config) {

                        $scope.Model.OrderChargeDiscounts = data.OrderChargeDiscounts;
                    })
            }

        }

        $scope.addChargeDiscount = function (chargeDiscountId) {
            if (is.existy($scope.Model.OrderChargeDiscounts) || is.not.empty($scope.Model.OrderChargeDiscounts)) {

                for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                    if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {
                        $scope.Model.OrderChargeDiscounts[i].Checked = true;
                    }
                }
            }
        }

        $scope.editItem = function (chargeDiscount) {
            chargeDiscount._Amount = chargeDiscount.Amount;

            chargeDiscount.EditMode = true;
        }

        $scope.cancelEditItem = function (chargeDiscount) {
            chargeDiscount.Amount = chargeDiscount._Amount;

            chargeDiscount.EditMode = false;
        }

        $scope.removeChargeDiscount = function (chargeDiscountId) {

            var message = 'Are you sure you want to proceed with this action?';

            if ($scope.getChargeDiscount(chargeDiscountId).Category == 'EntryFee') {
                message = 'Are you sure you want to cancel order?';
            }
            else {
                message = 'Are you sure you want to delete this item?';
            }

            jbConfirmation.message(message).yes(function () {

                if ($scope.Model.OrderChargeDiscounts) {

                    for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {
                        if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {

                            if ($scope.Model.OrderChargeDiscounts[i].Category == 'EntryFee') {

                                for (var l = 0; l < $scope.Model.OrderChargeDiscounts.length; l++) {
                                    $scope.Model.OrderChargeDiscounts[l].Checked = false;
                                }
                            }
                            else {
                                $scope.Model.OrderChargeDiscounts[i].Checked = false;
                            }
                        }
                    }
                }

            })
                .confirm();
        }

        $scope.calculateDifference = function () {

            if ($scope.Model.SelectedTution) {

                return $scope.calculateTotalAmount() - $scope.Model.OrderAmount;
            }
            else {
                return 0;
            }
        }

        $scope.getSelectedTution = function () {

            if ($scope.Model && $scope.Model.SelectedTution) {
                var tutionId = $scope.Model.SelectedTution;
              
                if (tutionId) {
                    for (var j = 0; j < $scope.Model.Schedules.length; j++) {
                        for (var i = 0; i < $scope.Model.Schedules[j].Tutions.length; i++) {
                            if ($scope.Model.Schedules[j].Tutions[i].Id == tutionId) {
                                return $scope.Model.Schedules[j].Tutions[i];
                            }
                        }
                    }
                }
            }
            else {
                return { Name: '', Price: 0 };
            }
        }

        $scope.getSelectedShedule = function () {

            if ($scope.Model && $scope.Model.SelectedTution) {
                var tutionId = $scope.Model.SelectedTution;

                if (tutionId) {
                    for (var j = 0; j < $scope.Model.Schedules.length; j++) {
                        for (var i = 0; i < $scope.Model.Schedules[j].Tutions.length; i++) {
                            if ($scope.Model.Schedules[j].Tutions[i].Id == tutionId) {
                                return $scope.Model.Schedules[j];
                            }
                        }
                    }
                }
            }
            else {
                return null;
            }
        }

        $scope.sortChargeDiscounts = function (item) {
            var result = 0;

            switch (item.Category) {
                case 'EntryFee':
                    {
                        result = 0;
                        break;
                    }
                case 'CustomCharge':
                    {
                        result = 1000;
                        break;
                    }
                case 'CustomDiscount':
                    {
                        result = 2000;
                        break;
                    }
                default:
                    {
                        result = 100;
                        break;
                    }
            }

            if (!item.Checked) {
                result += 10000;
            }

            return result;
        }

        $scope.getChargeDiscount = function (chargeDiscountId) {

            for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                if ($scope.Model.OrderChargeDiscounts[i].Id == chargeDiscountId) {
                    return $scope.Model.OrderChargeDiscounts[i];
                }
            }

            return null;
        }

        $scope.getProgram = function (id) {
            for (var i = 0; i < $scope.Model.AllPrograms.length; i++) {
                if ($scope.Model.AllPrograms[i].Value == id) {
                    return $scope.Model.AllPrograms[i];
                }
            }
        }

        $scope.hasInstallment = function () {
            if ($scope.Model.OrderInstallments != null && $scope.Model.OrderInstallments != 'undefined' && $scope.Model.OrderInstallments.length) {
                return true;
            }

            return false;
        }



        $scope.calculateBalance = function () {

            if ($scope.Model) {
                if ($scope.Model.SelectedTution) {
                    if (($scope.calculateTotalAmount() != null && $scope.calculateTotalAmount() != 'undefined')) {
                        return $scope.calculateTotalAmount() - $scope.Model.PaidAmount;
                    }
                    else {
                        return 0;
                    }
                }
                else {
                    return $scope.Model.OrderAmount - $scope.Model.PaidAmount;
                }
            }
            else {
                return 0;
            }
        }

        $scope.validateDate = function (date) {

            if (new Date(date) == 'Invalid Date') {
                return false;
            }

            return true;
        }

        $scope.formatProgramName = function () {

            var programName = $scope.getProgram($scope.Model.SelectedProgram).Text;

            var scheduleDates = $scope.getSelectedShedule().Dates;

            var tuitionName = $scope.getSelectedTution().Name;

            return programName + ' (' + scheduleDates + '), ' + tuitionName;
        }

        $scope.addNewInstallment = function () {

            var today = new Date();
            var dueDate = new Date($scope.Model._dueDate);

            if ($scope.Model._dueDate == 0 || $scope.Model._dueDate == '' || $scope.Model._dueDate == null || $scope.Model._dueDate == 'undefined') {
                jbToast.error('You should enter Due date.');
            }
            else if (!$scope.validateDate(dueDate)) {
                jbToast.error('The format of date is invalid.');
            }
            else if (dueDate < today) {
                jbToast.error('Due date can not be before today.');
            }
            else if ($scope.Model._amount == 0 || $scope.Model._amount == '' || $scope.Model._amount == null || $scope.Model._amount == 'undefined') {
                jbToast.error('You should enter amount.');
            }
            else if (isNaN($scope.Model._amount)) {
                jbToast.error('The format of amount is invalid.');
            }
            else {

                var isManually = true;
                for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {

                    if (!$scope.Model.OrderInstallments[i].EnableReminder && $scope.Model.OrderIsAutocharge) {
                        isManually = false;
                    }
                }

                var installmentItem = { Amount: $scope.Model._amount, DueDate: $scope.Model._dueDate, IsDeposit: false, Status: '-', EnableReminder: isManually, NoticeSent: false, IsDeleted: false, ItemStatus: 'initiated' };

                $scope.Model.OrderInstallments.push(installmentItem);

                $scope.Model._amount = '';
                $scope.Model._dueDate = '';
            }
        }

        $scope.continue = function () {

            if ($scope.Model.SelectedProgram) {
                if ($scope.Model.SelectedTution) {
                    if ($scope.Model.PageStep == 'FirstPage') {

                        if ($scope.Model.ProgramType == 'Subscription' || $scope.Model.ProgramType == 'BeforeAfterCare') {

                            if ($scope.Model.SourceTutionId == $scope.Model.SelectedTution) {
                                jbToast.error("You cannot transfer a program to itself, source and target programs are the same.");
                            }
                            else
                                if ($scope.CalculateSelectedDaysCount() != $scope.CalculateTutionDay()) {

                                    jbToast.error("You should select " + $scope.CalculateTutionDay() + " class days.");

                                }
                                else if ($scope.Model.NewEffectiveDate == "" || $scope.Model.NewEffectiveDate == null) {

                                    jbToast.error("Effective date is required.");
                                }
                                else if ($scope.Model.NewEffectiveDate != null) {
                                    var effectiveDate = new Date($scope.Model.NewEffectiveDate);
                                    var Date2 = new Date($scope.Model.CurrentDesiredStartDate);
                                    var Date3 = new Date($scope.Model.TodayDate);
                                    var Date4 = new Date($scope.Model.LastSessionDate);

                                    if (effectiveDate < Date3) {
                                        jbToast.error("Effective date cannot be before today.");
                                    }
                                    else if (effectiveDate < Date2) {
                                        jbToast.error("Effective date cannot be before desired start date.");
                                    }
                                    else if (Date4 <= effectiveDate) {
                                        jbToast.error("Effective date must be before the program end date.");
                                    } else {
                                        $scope.Model.PageStep = 'Review';
                                    }
                                }

                        } else {

                            $scope.Model.PageStep = 'PaymentPlan';
                        }
                    } else if ($scope.Model.PageStep == 'PaymentPlan') {

                        if ($scope.Model.ProgramType != 'Subscription' && $scope.Model.ProgramType != 'BeforeAfterCare') {

                            if (!$scope.hasInstallment() || $scope.calculateTotalAmount() == $scope.calculateTotalInstallments() ) {

                                $scope.Model.PageStep = 'Review';
                            }
                            else {
                                var message = 'Warning! The total sum of installments is: ' + $scope.calculateTotalInstallments().formatCurrencyWithPenny(2) + ' while the order total is: ' + $scope.calculateTotalAmount().formatCurrencyWithPenny(2) +
                                    ' Your organization is responsible to handle the balance.';

                                jbToast.error(message);
                            }
                        }
                    } else {
                        $scope.edit();
                    }

                }
                else {
                    jbToast.error("Please select the tuition option.");
                }
            }
            else {
                jbToast.error("Please select the target program.");
            }
        }

        $scope.calculateTotalPaids = function () {
            var result = 0;

            if ($scope.hasInstallment()) {

                if ($scope.Model.ProgramType != 'Subscription' && $scope.Model.ProgramType != 'BeforeAfterCare') {
                    for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                       
                        if (!$scope.Model.OrderInstallments[i].IsDeleted && $scope.Model.OrderInstallments[i].PaidAmount !=null) {
                           
                            result += parseFloat($scope.Model.OrderInstallments[i].PaidAmount);
                          
                        }
                    
                    }
                  
                } else {
                    for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                      
                        if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && !$scope.Model.OrderInstallments[i].IsDeletedNormalItem) {
                           
                            result += parseFloat($scope.Model.OrderInstallments[i].PaidAmount);
                        }
                    }
                }
            }
          
            return result;
        }

        $scope.calculateTotalAmount = function () {

            var charges = 0;

            var discounts = 0;

            var transferFee = 0;

            var result = 0;

            var tuitionFee = $scope.getSelectedTution().Price;

            if ($scope.Model.TransferFee) {
                transferFee = parseFloat($scope.Model.TransferFee);
            }

            if ($scope.Model && $scope.Model.OrderChargeDiscounts) {
                for (var i = 0; i < $scope.Model.OrderChargeDiscounts.length; i++) {

                    if ($scope.Model.OrderChargeDiscounts[i].SubCategory == 'Charge' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        charges += parseFloat($scope.Model.OrderChargeDiscounts[i].Amount)
                    }

                    if ($scope.Model.OrderChargeDiscounts[i].SubCategory == 'Discount' && $scope.Model.OrderChargeDiscounts[i].Checked) {
                        discounts += parseFloat($scope.Model.OrderChargeDiscounts[i].Amount)
                    }
                }

                result = tuitionFee + charges + transferFee - discounts;


                return result;
            }

            return result;
        }

        $scope.calculateTotalInstallments = function () {

            var result = 0;

            if ($scope.hasInstallment()) {

                if ($scope.Model.ProgramType != 'Subscription' && $scope.Model.ProgramType != 'BeforeAfterCare') {
                    for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                        if (!$scope.Model.OrderInstallments[i].IsDeleted) {
                            result += parseFloat($scope.Model.OrderInstallments[i].Amount);
                        }
                    }
                } else {
                    for (var i = 0; i < $scope.Model.OrderInstallments.length; i++) {
                        if (!$scope.Model.OrderInstallments[i].WasDeleted && !$scope.Model.OrderInstallments[i].IsDeletedSessions && !$scope.Model.OrderInstallments[i].IsDeletedNormalItem) {
                            result += parseFloat($scope.Model.OrderInstallments[i].Amount);
                        }
                    }
                }
            }

            return result;
        }


        $scope.IsSelected = false;
        $scope.GetItemSelectedTution = function () {
            $scope.IsSelected = false;
            for (var i = 0; i < $scope.Model.Schedules.length; i++) {

                for (var j = 0; j < $scope.Model.Schedules[i].Tutions.length; j++) {

                    if ($scope.Model.SourceTutionId == $scope.Model.Schedules[i].Tutions[j].Id) {
                        $scope.IsSelected = true;
                        break;
                    }
                }
            }
            return $scope.IsSelected;
        }

        var day = 0;
        $scope.CalculateTutionDay = function () {
            for (var i = 0; i < $scope.Model.Schedules.length; i++) {

                for (var j = 0; j < $scope.Model.Schedules[i].Tutions.length; j++) {

                    if ($scope.Model.SelectedTution == $scope.Model.Schedules[i].Tutions[j].Id) {
                        day = $scope.Model.Schedules[i].Tutions[j].WeekDay;
                    }
                }

            }
            return day;
        }

        $scope.CalculateSelectedDaysCount = function () {
            var selectedDay = 0;
            for (var i = 0; i < $scope.Days.length; i++) {

                if ($scope.Days[i].IsChecked == true) {
                    selectedDay = selectedDay + 1;
                }
            }
            return selectedDay;
        }

        $scope.CalculateSubscriptionInstallment = function () {

            $http.get('Dashboard/Order/GetSubscriptionInstallment', { params: { sourceOrderItemId: $scope.Model.Id, sourceProgramId: $scope.Model.SourceProgramId, targetProgramId: $scope.Model.SelectedProgram, tutionId: $scope.Model.SelectedTution, effectiveDate: $scope.Model.NewEffectiveDate } })
                .success(function (data, status, headers, config) {

                    $scope.Model.SubscriptionOrderInstallments = data.SubscriptionOrderInstallments;
                })

        }

        $scope.back = function () {

            if ($scope.Model.PageStep == 'FirstPage') {
                history.back();
            }
            else if ($scope.Model.PageStep == 'Review') {

                $scope.Model.PageStep = 'PaymentPlan';

            } else if ($scope.Model.PageStep == 'PaymentPlan') {

                $scope.Model.PageStep = 'FirstPage';
            }
        }

        $scope.formatDate = function (date) {

            if (date != null && date != 'undefined') {
                return new Date(date).format('mm/dd/yyyy');
            }
        }

        $scope.removeInstallmentItem = function (installmentItem) {

            var message = 'Are you sure you want to delete this installment?';
            jbConfirmation.message(message).yes(function () {

                installmentItem.IsDeleted = true;
            })
                .confirm();
        }

        $scope.removeInstallmentSubscriptionItem = function (installmentItem, isDeletedSessions) {

            if (isDeletedSessions) {
                var message = 'Are you sure you want to delete this installment and all its sessions?';
                jbConfirmation.message(message).yes(function () {

                    installmentItem.IsDeletedNormalItem = false;
                    installmentItem.IsDeletedSessions = true;

                    installmentItem.IsActiveInstallmentAndSessions = false;

                })
                    .confirm();
            } else {
                var message = 'Are you sure you want to delete this installment?';
                jbConfirmation.message(message).yes(function () {

                    installmentItem.IsDeletedNormalItem = true;
                    installmentItem.IsDeletedSessions = false;

                    installmentItem.IsActiveInstallment = false;

                })
                    .confirm();
            }
        }

        $scope.editInstallmentItem = function (installmentItem) {

            installmentItem.EditMode = true;

            installmentItem._amount = installmentItem.Amount;
            installmentItem._dueDate = installmentItem.DueDate;
        }

        $scope.saveInstallment = function (installmentItem) {
            $http.post('Dashboard/Order/ValidateTransferInstallments', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status) {
                        
                        var amount = installmentItem.Amount;
                        var dueDate = installmentItem.DueDate;

                        var today = new Date();
                        var dueDate = new Date(dueDate);

                        if (dueDate == 0 || dueDate == '' || dueDate == null || dueDate == 'undefined') {
                            jbToast.error('You should enter Due date.');
                        }
                        else if (!$scope.validateDate(dueDate)) {
                            jbToast.error('The format of date is invalid.');
                        }
                        else if (amount == '' || amount == null || amount == 'undefined') {
                            jbToast.error('You should enter amount.');
                        }
                        else if (isNaN(amount)) {
                            jbToast.error('The format of amount is invalid.');
                        }
                        else {
                            installmentItem.EditMode = false;
                        }
                    } else {

                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.cancelInstallmentItem = function (installmentItem) {

            installmentItem.EditMode = false;

            installmentItem.DueDate = installmentItem._dueDate;
            installmentItem.Amount = installmentItem._amount;
        }

      

        $scope.edit = function () {
            $scope.submit();

        }

    }])
})();