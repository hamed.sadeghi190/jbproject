﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;
using System;
using Jumbula.Core.Model.Catalog;

namespace Jumbula.Core.Business
{
    public interface ICatalogBusiness
    {
        CatalogItem Get(int id);

        IQueryable<CatalogItem> GetList(int clubId);

        IQueryable<CatalogItem> GetList(string clubDomain);

        IQueryable<CatalogItem> GetList();

        OperationStatus Create(CatalogItem entity);

        OperationStatus Update(CatalogItem entity);

        OperationStatus Delete(int id);

        OperationStatus Hide(int id);

        OperationStatus Archive(int id);

        OperationStatus Active(int id);

        OperationStatus UpdateSettings(CatalogSetting catalogSetting, int clubId);
        CatalogInvitationMessageViewModel BuildInviteMessageBody(Program program, Season outsourceSeason, DateTime? startTime, DateTime? endTime);
    }
}
