﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using Jumbula.Common.Enums;
using Constants = Jumbula.Common.Constants.Constants;
namespace Jumbula.Core.Domain
{
    public class StorageMetaData
    {
        public StorageMetaData()
        {

        }

        public StorageMetaData(bool generateRandomFileName)
        {
            FileName = Guid.NewGuid().ToString();
        }
        public StorageMetaData(StorageFileType storageFileType, string fileName, string clubDomain = "")
        {
            switch (storageFileType)
            {
                case StorageFileType.ESignature:
                {
                    FileName = Path.GetFileNameWithoutExtension(fileName).Replace(' ', '_') + "_" + DateTime.UtcNow.ToString("MMddyyyyHHmm") + Path.GetExtension(fileName.ToLower());

                    FolderName = string.Concat(clubDomain, Constants.S_Slash, "esignature");
                }
                    break;
                case StorageFileType.Flyer:
                {
                    FileName = Path.GetFileNameWithoutExtension(fileName).Replace(' ', '_') + "_" + DateTime.UtcNow.ToString("MMddyyyyHHmm") + Path.GetExtension(fileName.ToLower());

                    FolderName = string.Concat(clubDomain, Constants.S_Slash, "flyer");
                }
                    break;
                default:
                    break;
            }
        }
        public string FileName { get; set; }

        public string FolderName { get; set; }

        [NotMapped]
        public string FullPath => string.Concat(FolderName, Constants.S_Slash, FileName);
    }
}
