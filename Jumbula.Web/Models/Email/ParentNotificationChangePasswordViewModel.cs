﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Email
{
    public class ParentNotificationChangePasswordViewModel
    {
        public string UserEmail { get; set; }

        public string HomeTitle { get; set; }

        public string LogoUrl { get; set; }
    }
}