﻿using Jb.Framework.Common;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ISeasonPaymentPlanBusiness
    {
        OperationStatus Create(Season season, List<long> paymentPlanIds);

        IEnumerable<PaymentPlan> GetPaymentPlansByUserIdForMultiRegister(Season season, int userId);
    }
}
