﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Common.Enums
{
    public enum CurrencyCodes : short
    {
        // United States Dollar
        [Description("en-US")]
        USD = 0,
        // United Kingdom Pound
        [Description("en-GB")]
        GBP = 1,
        // China Yuan Renminbi
        [Description("zh-CN")]
        CNY = 2,
        // Canadian Dollar
        [Description("en-CA")]
        CAD = 3,
        // Malaysian Ringgit
        [Description("ms-MY")]
        MYR = 4,
        // Australian Dollar
        [Description("en-au")]
        AUD = 5

    }
}
