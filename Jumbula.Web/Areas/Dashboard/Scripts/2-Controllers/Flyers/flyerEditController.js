﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('flyerEditController', [
		'$scope', '$http', 'jbToast', '$state', '$stateParams', function ($scope, $http, jbToast, $state, $stateParams) {
		
		    window.app.globalObjects.initializeView();
		    $scope.MC.stateParams = $stateParams;

		    var baseUrl = window.location.origin + '/Dashboard/Flyer';
		    $http.get(baseUrl + '/GetFlyer', { params: { id: $stateParams.flyerId } })
		
					.success(function (data, status, headers, config) {
					    
					    $scope.Model = data;
					});

		    $scope.edit = function () {

		        $http.post(baseUrl + '/EditFlyer', { model: $scope.Model })
					.success(function (data, status, headers, config) {

					    if (data.Status) {

					        $scope.errors = null;

					        jbToast.success("Flyer rename successfully.", "");

					        $state.go('ClubFlyers');
					    }
					    else {
					        $scope.MC.handleErrors($scope, data);
					    }
					});
		    }
		}
    ]);
})();