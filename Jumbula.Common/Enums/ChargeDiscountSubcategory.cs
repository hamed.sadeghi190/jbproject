﻿

namespace Jumbula.Common.Enums
{

    public enum ChargeDiscountSubcategory : int
    {
        Charge = 0,
        Discount = 1
    }
}
