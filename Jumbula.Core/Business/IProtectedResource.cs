﻿namespace Jumbula.Core.Business
{
    public interface IProtectedResource
    {
        bool DoesTheClubOwnIt(int clubId);

        bool DoesTheParentOwnIt(int userId);
    }
}
