﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Areas.Dashboard.Models
{
    public class AccountBillingHistoryViewModel : BaseViewModel<ClientBillingHistory, long>
    {
        public AccountBillingHistoryViewModel()
        { }
       
        public string BillingId { get; set; }
        public string Description { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        
        public DateTime? BillingDate { get; set; }
        public string Date
        {
            get
            {
                if (BillingDate.HasValue)
                {
                    return BillingDate.Value.ToString(Constants.DefaultDateFormat);
                }
                else return string.Empty;
            }
        }
        public string StrDebit
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(Debit, CurrencyCodes.USD);
            }
        }
        public string StrCredit
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(Credit, CurrencyCodes.USD);
            }
        }
       
    }
}

