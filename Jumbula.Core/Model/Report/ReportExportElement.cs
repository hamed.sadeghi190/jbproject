﻿namespace Jumbula.Core.Model.Report
{
    public class ReportExportElement
    {
        public string Name { get; set; }
        public string Template { get; set; }
    }
}
