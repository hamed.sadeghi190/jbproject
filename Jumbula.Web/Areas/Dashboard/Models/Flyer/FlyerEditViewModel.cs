﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FlyerEditViewModel 
    {
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }
        public int Id { get; set; }
    }


}