﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class PaymentPlanConfig : EntityTypeConfiguration<PaymentPlan>
    {
        public PaymentPlanConfig()
        {
            HasKey(c => c.Id);

            HasMany(d => d.Programs).WithMany(p => p.PaymentPlans).Map(pd =>
            {
                pd.MapLeftKey("PaymentPlanId");
                pd.MapRightKey("ProgramId");
                pd.ToTable("ProgramPaymentPlans");
            });


        }
    }
}
