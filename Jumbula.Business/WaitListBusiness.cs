﻿using Jb.Framework.Common;
using Jumbula.Core.Business;
using System;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class WaitListBusiness : IWaitListBusiness
    {
        private readonly IRepository<WaitList> _waitlistRepository;

        public WaitListBusiness(IRepository<WaitList> waitlistRepository)
        {
            _waitlistRepository = waitlistRepository;
        }

        public WaitList GetById(int Id)
        {
            return _waitlistRepository.GetList().FirstOrDefault(w => w.Id == Id);
        }
        public WaitList Get(int Id)
        {
            return _waitlistRepository.Get(Id);
        }

        public WaitList Get(Expression<Func<WaitList, bool>> predicate)
        {
            return _waitlistRepository.Get(predicate);
        }

        public IQueryable<WaitList> GetList()
        {
            return _waitlistRepository.GetList();
        }

        public IQueryable<WaitList> GetList(long programId)
        {
            return _waitlistRepository.GetList().Where(w => w.ProgramId == programId && (w.Program.Season.Status == SeasonStatus.Test) != w.IsLive);
        }

        public IQueryable<WaitList> GetListByUser(long UserId)
        {
            return _waitlistRepository.GetList().Where(w => w.UserId == UserId && w.IsLive);
        }

        public IQueryable<WaitList> GetList<TKey>(Expression<Func<WaitList, bool>> predicate, Expression<Func<WaitList, TKey>> orderBy)
        {
            return _waitlistRepository.GetList<TKey>(predicate, orderBy);
        }

        public OperationStatus Update(WaitList entity)
        {
            return _waitlistRepository.Update(entity);
        }

        public OperationStatus Create(WaitList entity)
        {
            return _waitlistRepository.Create(entity);
        }

        public bool IsAlreadyExistInWaitListByScheduleId(long scheduleId, int userId, string firstName, string lastName)
        {
            return GetList().Any(w => w.ScheduleId == scheduleId && w.UserId == userId && (w.Schedule.Program.Season.Status == SeasonStatus.Test) != w.IsLive && w.FirstName.Equals(firstName) && w.LastName.Equals(lastName));
        }
        public OperationStatus Delete(int entityId)
        {
            return _waitlistRepository.Delete(entityId);
        }

        public OperationStatus Delete(WaitList entity)
        {
            return _waitlistRepository.Delete(entity);
        }

        public OperationStatus DeleteWhere(Expression<Func<WaitList, bool>> filter = null, Func<IQueryable<WaitList>, IOrderedQueryable<WaitList>> orderBy = null, string includeProperties = "")
        {
            return _waitlistRepository.DeleteWhere(filter, orderBy, includeProperties);
        }
    }
}
