﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Jumbula.Core.Domain
{
    public class PaymentPlan : BaseEntity<long>, IProtectedResource
    {
        public PaymentPlan()
        {
            MetaData = new MetaData();
            Programs = new List<Program>();
            Users = new List<UserPaymentPlan>();
        }

        [Required]
        [StringLength(128)]
        [DefaultValue("")]
        public string Name { get; set; }

        public decimal Deposit { get; set; }
        public ChargeDiscountType DepositType { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public Int16 NumOfInstallments { get; set; }
        public string InstallmentDueDates { get; set; }
        [StringLength(1000)]
        public string CustomInstallmentLable { get; set; }

        public bool IsAutoChargeMandatory { get; set; }
        public bool IsAllProgram { get; set; }
        public bool IsAllUser { get; set; }
        public bool IsUseCustomPrompt { get; set; }
        public CalculationType CalculationType { get; set; }// = CalculationType.OnlyTuition;
        public decimal? InstallmentFee { get;set; }
        public long SeasonId { get; set; }
        public MetaData MetaData { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsNameVisible { get; set; }

        public virtual Season Season { get; set; }
        public virtual ICollection<Program> Programs { get; set; }
        public virtual ICollection<UserPaymentPlan> Users { get; set; }

        [NotMapped]
        public IEnumerable<string> ListOfInstallmentDates
        {
            get
            {
                if (string.IsNullOrEmpty(InstallmentDueDates))
                {
                    return null;
                }
                else
                {
                    return InstallmentDueDates.Split(',').ToList();
                }
            }
        }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Season.ClubId == clubId)
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }
}
