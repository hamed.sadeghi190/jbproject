﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('confirmFamilyTakePaymentController', ['$scope', '$http', '$stateParams', '$state', 'TakePaymentInvoiceService', '$filter', 'jbToast', 'familyOrdersService', 'familyOrderItemsService', function ($scope, $http, $stateParams, $state, TakePaymentInvoiceService, $filter, jbToast, familyOrderItemsService, familyOrdersService) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/People";

        $scope.Model = TakePaymentInvoiceService.get();
        $scope.ModelOrder = familyOrderItemsService.get();
        $scope.OrderitemModel =  familyOrdersService.get();

        $scope.cancelAction = function () {
            TakePaymentInvoiceService.set($scope.Model);
            $state.back();
        }
        $scope.formatDate = function () {
            return new Date($scope.Model.PaymentDate).format('mmm d, yyyy');

        }

        $scope.totalAmount = 0;
        for (var i = 0; i < $scope.ModelOrder.length; i++) {

            if ($scope.ModelOrder[i].Checked) {

                $scope.ModelOrder[i].orginalAmount = $scope.ModelOrder[i].InvoiceAmount;
                $scope.totalAmount += $scope.ModelOrder[i].orginalAmount;
            }

        }
        var orderItemModel = [];
        $scope.Model.OrderItems = [];
        for (var i = 0; i < $scope.ModelOrder.length; i++) {

            if ($scope.ModelOrder[i].Checked) {

                for (var j = 0; j < $scope.OrderitemModel[$scope.ModelOrder[i].Id].length; j++) {

                    if ($scope.OrderitemModel[$scope.ModelOrder[i].Id][j].Checked) {

                        orderItemModel.push($scope.OrderitemModel[$scope.ModelOrder[i].Id][j]);
                    }
                }
            }
        }
     
        $scope.getPaymentMethodText = function () {
            var paymentMethodValue = $scope.Model.SelectedPaymentMethod;
            var paymentMethod = $filter('getById')($scope.Model.PaymentMethods, paymentMethodValue);
            return paymentMethod.Text;
        }
       

        $scope.TakePayment = function () {
            $('#submitTakePayment').prop("disabled", true);
            $http.post($scope.baseUrl + '/SubmitTakePayment', { model: $scope.Model , orderitems:orderItemModel})
                .success(function (data, status, headers, config) {
                    
                    if (data.Status == true) {
                        $scope.MC.backQueueItems.pop();
                        if (data.result == "Refresh") {
                            $state.go("DisplayFamilyTakePaymentPaymentMessage", { status: true, message: '' });
                        }
                        if (data.result == "Redirect") {
                            window.onbeforeunload = null;
                            window.location.replace(data.url);
                        }
                    }
                    else if (data.Status == false) {
                       
                        if (data.result == "Refresh") {
                            jbToast.error("Fail take payment");
                        }
                    }
                });

        };
    }])

})();