-- Migrate JbUserRoles to Clubstaffs
DECLARE @userRoleCount INT 

select @userRoleCount = COUNT(Id) from JbUserRoles

WHILE 1 = 1
BEGIN

INSERT INTO [dbo].[ClubStaffs]
           (
		    [UserRoleId]
		   ,[ClubId]
           )
    SELECT TOP(@userRoleCount) 
             s.Id
			,s.ClubId
    FROM [dbo].[JbuserRoles] s
    WHERE NOT EXISTS ( 
        SELECT 1
        FROM [dbo].[ClubStaffs]
        WHERE [UserRoleId] = s.Id
    ) 
	And
    Not	ISNULL(clubId, '') = ''
	And
	RoleId != ''

    IF @@ROWCOUNT < @userRoleCount BREAK
    
END

