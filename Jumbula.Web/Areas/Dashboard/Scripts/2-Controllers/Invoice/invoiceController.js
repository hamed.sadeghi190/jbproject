﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('invoiceController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Invoices", order: 0, menu: "INVOICE" });
        }
        $scope.baseUrl = window.location.origin + '/Dashboard/Invoice';

        $http.get($scope.baseUrl + '/InvoicePage').success(function (data, status, headers, config) {
            $scope.Model = data;

            $scope.filter.InvoiceType = 'All';
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.searchTerm = "";
        $scope.searchEmail = "";
        $scope.filter = {};
        $scope.filter.InvoiceType = "0";
        $scope.filter.typeId = "-1";
        $scope.filter.endDate = "";
        $scope.filter.startDate = "";

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetAllInvoices/',
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                               term: $scope.searchTerm,
                               emailTerm: $scope.searchEmail,
                               typeId: $scope.filter.typeId,
                               endDate: $scope.filter.endDate,
                               startDate: $scope.filter.startDate,
                               InvoiceType: $scope.filter.InvoiceType,
                           }, data);
                        return JSON.stringify(data);
                    }
                },

            },
            pageSize: 50,
            batch: true,
            serverPaging: true,
            serverSorting: false,
            schema: {
                data: "data",
                total: "total"
            },
            sortable: false,
            sort: { field: "Id", dir: "desc" }
        });
        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            serverPaging: true,
            scrollable: true,
            columnMenu: true,
            columns: [

            {
                field: "StrDate",
                title: "Date",
                template: '<div style="color:black;"> #=StrDate # </div>',
                labels: {
                    visible: true,
                },
                width: "150px",
            }, {
                field: "InvoiceId",
                title: "Invoice #",
                width: "150px",
                template: '<div style="color:black;"> #=InvoiceId # </div>',
                labels: {
                    visible: true,
                },
            }, {
                field: "Recipient",
                title: "Recipient",
                width: "250px",
                template: '<div style="color:black;"> #=Recipient # </div>',
                labels: {
                    visible: true,
                },
            },
            {
                field: "Status",
                title: "Status",
                template: '<div style="color:black;"> #=Status # </div><div> #if(data.Status=="Unpaid"){# Due #= data.StrDueDate# #}# </div>  ',
                labels: {
                    visible: true,
                },
                width: "150px",
            }, {

                field: "StrAmount",
                title: "Amount",
                template: '<div style="color:black;text-align: right;"> #=StrAmount # </div>',
                labels: {
                    visible: true,
                },
                width: "100px",
            },
              {
                  title: "Actions",
                  width: "70px",
                  headerTemplate: "<span class='actions-title'>Actions</span>",
                  template: '<ul kendo-menu style="display: inline-block;position:absolute;overflow: visible" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li ui-sref="InvoiceDetail({invoiceId: #=Id#})" >\
                                          <span class="k-link ">View</span>\
                                    </li>\
                                    <li ui-sref="ParentInvoiceDetail({invoiceId: #=Id#})" >\
                                          <span class="k-link ">View as recipient</span>\
                                    </li>\
                                   #if(data.Status=="Unpaid" && data.Status!="Canceled"){#\
                                   <hr/>\<li ng-click="GetInvoiceStatus(#:Id#)" >\
                                          <span class="k-link">Take a payment</span>\
                                    </li>\
                                         <li ng-click="ReminderInvoice(#:Id#)"" >\
                                          <span class="k-link">Send reminder</span>\
                                    </li>\
                                   #}#\
                                  #if(data.Status!="Canceled" && data.Status=="Unpaid"){#\
                                   <li ng-click="CancelationInvoice(#:Id#)" >\
                                          <span class="k-link">Cancel</span>\
                                    </li>\
                                   #}#\
                            </ul>\
                        </li>\
                    </ul>'
              }],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },

        };

        $scope.GetInvoiceStatus = function (invoiceId) {

            $http.get($scope.baseUrl + '/GetInvoiceStatus', { params: { invoiceId: invoiceId } }).success(function (data, status, headers, config) {

                console.log(data);

                if (data.Status) {

                    $state.go("TakePaymentInvoice", { invoiceId: invoiceId }, 'Invoices', function () { });
                }
                else {

                    jbToast.error("You have already canceled or paid this invoice.");
                }
            });

        };

        $scope.CancelationInvoice = function (invoiceId) {

            $http.get($scope.baseUrl + '/GetInvoiceStatus', { params: { invoiceId: invoiceId } }).success(function (data, status, headers, config) {
                if (data.Status) {

                    $state.go("CancelationInvoice", { invoiceId: invoiceId }, 'Invoices', function () { });
                }
                else {

                    jbToast.error("You have already canceled or paid this invoice.");
                }
            });

        };

        $scope.ReminderInvoice = function (invoiceId) {

            $http.get($scope.baseUrl + '/GetInvoiceStatus', { params: { invoiceId: invoiceId } }).success(function (data, status, headers, config) {
                if (data.Status) {

                    $state.go("ReminderInvoice", { invoiceId: invoiceId }, 'Invoices', function () { });
                }
                else {

                    jbToast.error("You have already canceled or paid this invoice.");
                }
            });

        };

        $scope.search = function () {
            $("#userGrid").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {

            $scope.searchTerm = "";
            $scope.searchEmail = "";
            $scope.filter.InvoiceType = "0";
            $scope.filter.typeId = "-1";
            $scope.filter.subTypeId = "-1";
            $scope.filter.endDate = "";
            $scope.filter.startDate = "";
            $("#userGrid").data("kendoGrid").dataSource.read();
        };

        $scope.AddInvoice = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("Families", null);
                }
            }
            else {
                $state.go("Families", null);
            }
        };
    }]);

})();
