﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsGeneralController', ['$scope', '$http', 'jbToast', '$stateParams', function ($scope, $http, jbToast, $stateParams) {
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSetting", order: 2, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;
        $http.get('Dashboard/Season/EditSchoolSettingsGeneralInfo?seasonDomain=' + $stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model = data;            

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

     
        $scope.save = function () {

         
            $http.post('Dashboard/Season/EditSchoolSettingsGeneralInfo', { model: $scope.Model, seasonDomain: $stateParams.seasonDomain })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     $scope.errors = null;
                     jbToast.success("General information updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }
  
    }])

})();