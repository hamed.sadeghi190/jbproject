﻿using Jb.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ICategoryBusiness
    {
        IQueryable<Category> GetList();

        IQueryable<Category> GetList(IEnumerable<int> ids);

        OperationStatus Update(Category category);

        int Update();

        List<SelectKeyValue<int>> GetCategories();
    }
}
