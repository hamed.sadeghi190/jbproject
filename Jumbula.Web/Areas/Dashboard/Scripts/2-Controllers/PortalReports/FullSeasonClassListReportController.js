﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('FullSeasonClassListReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};
        var isBegin = false;

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.SeasonNames = [{ Value: "Fall", Title: "Fall" }, { Value: "Winter", Title: "Winter" }, { Value: "Spring", Title: "Spring" }, { Value: "Summer", Title: "Summer" }];
        $scope.filter.SeasonName = "Fall";
        $scope.filter.printMode = false;
        $scope.filter.Year = 0;

        $http.get(baseUrl + '/GetFullSeasonClassListReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.Model.Years = data.Years;
            $scope.filter.Year = data.Years[0].Text;
            dataInfo = data;
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        
       
        var manipulateFullSeasonClassList = function () {

            isBegin = true;

            $scope.FullSeasonClassListDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetFullSeasonClassListReportInfo",
                        dataType: "json",
                        type: "POST",
                        data: function () {
                            return {
                                seasonName: $scope.filter.SeasonName,
                                year: $scope.filter.Year,
                                printMode: $scope.filter.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.FullSeasonClassListGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Full season class list).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: $scope.filter.SeasonName + " " + $scope.filter.Year
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Full season class list - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.FullSeasonClassListDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#FullSeasonClassListGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "SchoolName",
                        title: "School",
                        width: "150px"
                    }, {
                        field: "ClassName",
                        title: "Class name",
                        width: "170px",
                        template: '<span style="white-space: pre-line" > #=ClassName# </span>'
                    }, {
                        field: "ProviderName",
                        title: "Provider name",
                        width: "130px"
                    }, {
                        field: "Day",
                        title: "Day",
                        width: "160px",
                        template: '<span style="white-space: pre-line" > #=Day# </span>'
                    }, {
                        field: "ClassStatus",
                        title: "Status",
                        width: "80px",
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            if ($("#FullSeasonClassListGrid").data("kendoGrid") != null) {
                cfpLoadingBar.start();
                $scope.filter.printMode = false;
                $("#FullSeasonClassListGrid").getKendoGrid().saveAsExcel();
            }
        }

        $scope.saveToPDF = function () {
            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetFullSeasonClassListReportInfo', {
                seasonName: $scope.filter.SeasonName,
                year: $scope.filter.Year,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (FullSeasonClassList).pdf";
                a.click();
            });
            $scope.filter.printMode = false;

        }

        $scope.print = function () {
            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetFullSeasonClassListReportInfo', {
                seasonName: $scope.filter.SeasonName,
                year: $scope.filter.Year,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (FullSeasonClassList).pdf";
                a.click();
            });
            $scope.filter.printMode = false;
        }

        $scope.search = function () {
            if (!isBegin) {
                manipulateFullSeasonClassList();
            } else {
                $scope.filter.printMode = false;
                $("#FullSeasonClassListGrid").data("kendoGrid").dataSource.read();
            }
        };

        $scope.clearFilters = function () {
            $scope.filter.SeasonName = "Fall";
            $scope.filter.Year = $scope.Model.Years[0].Text;
            $scope.filter.printMode = false;
            $("#FullSeasonClassListGrid").data("kendoGrid").dataSource.read();
        }
    }
    ]);

})();