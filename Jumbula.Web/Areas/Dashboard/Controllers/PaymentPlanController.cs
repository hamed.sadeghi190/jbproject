﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class PaymentPlanController : DashboardBaseController
    {
        public virtual ActionResult PaymentPlans()
        {
            return View();
        }

        public virtual JsonNetResult CreateEditPaymentPlan(long paymentPlanId, string seasonDomain)
        {
            var model = new PaymentPlanViewModel();

            if (paymentPlanId > 0)
            {
                var paymentPlan = Ioc.PaymentPlanBusiness.Get(paymentPlanId);
                CheckResourceForAccess(paymentPlan);

                model = paymentPlan.ToViewModel<PaymentPlanViewModel>();

                model.SelectedPrograms = new List<string>();
                foreach (var item in paymentPlan.Programs.Select(c => c.Id).ToList())
                {
                    model.SelectedPrograms.Add(item.ToString());
                }

                model.SelectedUser = paymentPlan.Users.Select(c => c.UserId).ToList();
                model.IsNameVisibleToUsers = paymentPlan.IsNameVisible;
                model.IsUseCustomPrompt = paymentPlan.IsUseCustomPrompt;
                model.CustomInstallmentLabel = paymentPlan.IsUseCustomPrompt ? paymentPlan.CustomInstallmentLable : null;
                model.ListOfInstallmentDates = paymentPlan.ListOfInstallmentDates;

                if (paymentPlan.ExpiryDate.HasValue)
                {
                    model.ExpiryDate = paymentPlan.ExpiryDate.Value;
                }
            }

            var programs = GetListPrograms(seasonDomain,null);
            model.ProgramsList = new List<SelectKeyValue<string>>();

            foreach (var program in programs)
            {
                model.ProgramsList.Add(new SelectKeyValue<string>
                {
                    Text = program.Name,
                    Value = program.Id.ToString()
                });
            }

            model.InstallmentItems = SeedInstallmentItems();
            model.SeasonDomain = seasonDomain;
            model.SelectedInstallments = new SelectKeyValue<string>() { Text = String.Format("{0} {1}", NumberHelper.ToOrdinal(model.NumOfInstallments), "Installment"), Value = model.NumOfInstallments.ToString() };
            return JsonNet(model);
        }

        private List<SelectKeyValue<string>> SeedInstallmentItems()
        {
            List<SelectKeyValue<string>> InstallmentItems = new List<SelectKeyValue<string>>();
            for (int i = 1; i < 13; i++)
            {
                InstallmentItems.Add(new SelectKeyValue<string>() { Text = String.Format("{0} {1}", NumberHelper.ToOrdinal(i), "Installment"), Value = i.ToString() });
            }

            return InstallmentItems;
        }

        [HttpPost]
        public virtual ActionResult SavePaymentPlan(PaymentPlanViewModel model)
        {
            CheckModelIntegrity(ModelState, model);
            if (ModelState.IsValid)
            {
                PaymentPlan paymentPlan;
                OperationStatus res;
                var club = ActiveClub;

                if (model.Id > 0)
                {
                    paymentPlan = model.ToModel<PaymentPlan>(Ioc.PaymentPlanBusiness.Get(model.Id));
                }
                else
                {
                    paymentPlan = model.ToModel<PaymentPlan>();
                }

                if (model.ExpiryDate.HasValue)
                {
                    paymentPlan.ExpiryDate = model.ExpiryDate;
                }
                else
                {
                    paymentPlan.ExpiryDate = null;
                }

                if (model.IsUseCustomPrompt)
                {
                    paymentPlan.IsUseCustomPrompt = true;
                    paymentPlan.CustomInstallmentLable = model.CustomInstallmentLabel;
                }
                else
                {
                    paymentPlan.CustomInstallmentLable = null;
                }

                paymentPlan.Season = Ioc.SeasonBusiness.Get(model.SeasonDomain, club.Id);
                paymentPlan.SeasonId = paymentPlan.Season.Id;

                if (!model.IsAllProgram && model.SelectedPrograms != null && model.SelectedPrograms.Any())
                {
                    paymentPlan.Programs.Clear();

                    foreach (var id in model.SelectedPrograms)
                    {
                        var programId = long.Parse(id);
                        paymentPlan.Programs.Add(Ioc.ProgramBusiness.Get(programId));
                    }

                    paymentPlan.IsAllProgram = false;
                }
                else
                {
                    paymentPlan.Programs.Clear();
                    paymentPlan.IsAllProgram = true;
                }
                if (!model.IsAllUser && model.SelectedUser != null && model.SelectedUser.Any())
                {
                    paymentPlan.Users.Clear();

                    foreach (var user in model.SelectedUser)
                    {

                        paymentPlan.Users.Add(new UserPaymentPlan() { UserId = user, PaymentPlanId = paymentPlan.Id });
                    }

                    paymentPlan.IsAllUser = false;
                }
                else
                {
                    paymentPlan.Users.Clear();
                    paymentPlan.IsAllUser = true;
                }

                paymentPlan.IsNameVisible = model.IsNameVisibleToUsers;
                if (paymentPlan.Id == 0)
                {
                    paymentPlan.MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow
                    };

                    res = Ioc.PaymentPlanBusiness.Create(paymentPlan);
                }
                else
                {
                    paymentPlan.MetaData.DateUpdated = DateTime.UtcNow;
                    res = Ioc.PaymentPlanBusiness.Update(paymentPlan);
                }

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }


            return JsonFormResponse();
        }

        public virtual JsonResult DeletePaymentPlan(long paymentPlanId)
        {

            var res = Ioc.PaymentPlanBusiness.Delete(paymentPlanId);
            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        public virtual ActionResult PaymentPlanManage()
        {

            return View();
        }

        [HttpGet]
        public virtual JsonNetResult GetAllPaymentPlans(string seasonDomain)
        {
            var club = ActiveClub;
            var seasonId = Ioc.SeasonBusiness.Get(seasonDomain, club.Id).Id;
            var paymentPalns = Ioc.PaymentPlanBusiness.
                GetList(seasonId).Select(c => new PaymentPlanViewModel() { Id = c.Id, Name = c.Name, NumOfInstallments = c.NumOfInstallments, InstallmentDueDates = c.InstallmentDueDates }).ToList();
            return JsonNet(new { DataSource = paymentPalns, TotalCount = paymentPalns.Count });
        }

        private void CheckModelIntegrity(ModelStateDictionary modelState, PaymentPlanViewModel model)
        {
            if (model.IsAutoChargeMandatory)
            {
                var paymentMethods = Ioc.ClientPaymentMethodBusiness.GetByClub(ActiveClub.Id);
                if (string.IsNullOrEmpty(paymentMethods.StripeCustomerId) && (string.IsNullOrEmpty(paymentMethods.AuthorizeLoginId) && string.IsNullOrEmpty(paymentMethods.AuthorizeTransactionKey)))
                {
                    modelState.AddModelError("model.IsAutoChargeMandatory", "To use auto billing, you must either select Stripe or Authorize.Net as payment method.");
                }
            }
            if (!model.IsAllProgram && (model.SelectedPrograms == null || model.SelectedPrograms.Count == 0))
            {
                modelState.AddModelError("model.SelectedPrograms", Constants.E_SelectedProgram);
            }
            else
            {
                modelState.Remove("model.SelectedPrograms");
            }
            if (!model.IsAllUser && (model.SelectedUser == null || model.SelectedUser.Count == 0))
            {
                modelState.AddModelError("model.SelectedUser", Constants.E_SelectedProgram);
            }
            else
            {
                modelState.Remove("model.SelectedUser");
            }
            if (model.DepositType == ChargeDiscountType.Percent && (model.Deposit < 0 || model.Deposit > 100))
            {
                modelState.AddModelError("model.Deposit", Constants.E_PercentageAmount);
            }
            if (model.DepositType == ChargeDiscountType.Fixed && (model.Deposit < 0))
            {
                modelState.AddModelError("model.Deposit", Constants.E_Amount);
            }

            if (string.IsNullOrEmpty(model.InstallmentDueDates))
            {
                modelState.AddModelError("model.installmentDateInputs[0].duedate", "installment date is required");
            }
            else
            {
                var dateList = model.InstallmentDueDates.Split(',');
                string keyformat = "model.installmentDateInputs[{0}].duedate";
                string emptyMsgformat = "{0} installment date is required";
                string compareMsgformat = "{0} installment date must be after {1}";
                for (int i = 0; i < model.NumOfInstallments; i++)
                {
                    if (string.IsNullOrEmpty(dateList[i]))
                    {
                        modelState.AddModelError(string.Format(keyformat, i), string.Format(emptyMsgformat, NumberHelper.ToOrdinal(i + 1)));
                    }
                    else if (i > 0)
                    {
                        var prevDate = Convert.ToDateTime(dateList[i - 1]);
                        var currentDate = Convert.ToDateTime(dateList[i]);

                        if (DateTime.Compare(currentDate, prevDate) <= 0)
                        {
                            modelState.AddModelError(string.Format(keyformat, i), string.Format(compareMsgformat, NumberHelper.ToOrdinal(i + 1), prevDate.Date));
                        }
                    }
                }
            }

            if (!model.IsUseCustomPrompt)
            {
                modelState.Remove("model.CustomInstallmentLabel");
            }
            else
            {
                if (string.IsNullOrEmpty(model.CustomInstallmentLabel))
                {
                    modelState.AddModelError("model.CustomInstallmentLabel", "");
                }
            }

        }

        public virtual List<ProgramsViewModel> GetListPrograms(string seasonDomain, string term)
        {
            var termValue = Request.Params["term"] ?? string.Empty;
            var allprograms =
                Ioc.ProgramBusiness.GetList()
                    .Where(
                        p =>
                            p.ClubId == ActiveClub.Id && p.Season.Domain.ToLower() == seasonDomain.ToLower() &&
                            p.Status == ProgramStatus.Open && p.Name.StartsWith(termValue));

            List<ProgramsViewModel> model;

           
                model = allprograms.Select(p =>
                    new ProgramsViewModel
                    {
                        Id = p.Id,
                        ClubDomain = p.Club.Domain,
                        SeasonDomain = p.Season.Domain,
                        Name = p.Name,
                        TypeCategory = p.TypeCategory
                    })
                    .ToList();

            return model;
        }
    }
}