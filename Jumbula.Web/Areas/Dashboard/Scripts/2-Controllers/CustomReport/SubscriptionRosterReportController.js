﻿(function () {
    //'use strict';

    angular.module('dashboardApp').controller('SubscriptionRosterReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};

        $scope.Filters = {};
        $scope.Model = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/GetSubscriptionRosterReportDetail', { params: { programId: $scope.Model._SelectedPrograms } }).success(function (data, status, headers, config) {

            dataInfo = data;
            $scope.ProgramName = data.ProgramName;
            $scope.ClubName = data.ClubName;
            $scope.RoomAssignment = data.Room;
            $scope.DayOfWeek = data.Days;
            $scope.Date = data.Date;
            $scope.Filters.StartDate = data.StartDate;
            $scope.Filters.EndDate = data.EndDate;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });



        var model = $scope.Model;

        var manipulateRosterGrid = function () {

            $scope.SubscriptionRosterReportsDataSource = null;

            $scope.SubscriptionRosterReportsDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/SubscriptionRosterReportRun',
                            type: "POST",
                            data: function () {
                                return {
                                    programId: model._SelectedPrograms,
                                    startDate: $scope.Filters.StartDate,
                                    endDate: $scope.Filters.EndDate,
                                };
                            }
                        },
                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverPaging: true,
                pageSize: 50,
                batch: true,
            });

            $scope.SubscriptionRosterReportGridOptions = null;

            $scope.SubscriptionRosterReportGridOptions = {
                dataSource: $scope.SubscriptionRosterReportsDataSource,
                scrollable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                           5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    customReportService.initializeDoubleScroll(e);
                    $scope.oldColumns = $("#SubscriptionRosterReportGrid").data("kendoGrid").columns;

                    if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                        for (var i = 0; i < $scope.oldColumns.length; i++) {
                            var column = [];
                            if ($scope.oldColumns[i].field == 'Full_Name') {
                                column = {
                                    field: $scope.oldColumns[i].field,
                                    title: "Full name",
                                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderItemDetail(#=OrderItemId#)'  class='block'> #= Full_Name # </a> #if(ItemStatusReason == 1) {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == 2) {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == 5) {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == 4){# <i class='grid-label label warning'>Canceled</i> #}#"
                                }
                            } else if ($scope.oldColumns[i].field == 'Confirmation') {
                                column = {
                                    field: $scope.oldColumns[i].field,
                                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderDetail(#=OrderId#)' class='block'> #= Confirmation # </a>",
                                    title: "Confirmation"
                                }
                            } else if ($scope.oldColumns[i].field == 'OrderId' || $scope.oldColumns[i].field == 'OrderItemId' || $scope.oldColumns[i].field == 'ItemStatusReason') {
                                column = {
                                    field: $scope.oldColumns[i].field,
                                    hidden: true
                                }
                            } else {



                                column = {
                                    field: $scope.oldColumns[i].field,
                                    title: $scope.oldColumns[i].field
                                        .replace(/^(_(s)_)/, "(")
                                        .replace(/(_(p)_)/, ")")
                                        .replace(/_/g, " ").trimLeft()
                                }
                            }

                            columns.push(column);
                        }
                    }
                }
            }
        }

        manipulateRosterGrid();

        $scope.clearFilters = function () {
            $scope.Filtes.StartDate = '';

            $scope.SubscriptionRosterReportGridOptions.dataSource.read();
        }

        var columns = [];

        $scope.filterData = function () {

            manipulateRosterGrid();

            columns = [];
            $scope.oldColumns = [];

            $("#SubscriptionRosterReportGrid").html('');

            $("#SubscriptionRosterReportGrid").kendoGrid({
                dataSource: $scope.SubscriptionRosterReportsDataSource,
                scrollable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                           5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    customReportService.initializeDoubleScroll();
                    $scope.oldColumns = $("#SubscriptionRosterReportGrid").data("kendoGrid").columns;

                    if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                        for (var i = 0; i < $scope.oldColumns.length; i++) {
                            var column = [];
                            if ($scope.oldColumns[i].field == 'Full_Name') {
                                column = {
                                    field: $scope.oldColumns[i].field,
                                    title: "Full name",
                                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderItemDetail(#=OrderItemId#)'  class='block'> #= Full_Name # </a> #if(ItemStatusReason == 1) {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == 2) {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == 5) {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == 4){# <i class='grid-label label warning'>Canceled</i> #}#"
                                }
                            } else if ($scope.oldColumns[i].field == 'Confirmation') {
                                column = {
                                    field: $scope.oldColumns[i].field,
                                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderDetail(#=OrderId#)' class='block'> #= Confirmation # </a>",
                                    title: "Confirmation"
                                }
                            } else if ($scope.oldColumns[i].field == 'OrderId' || $scope.oldColumns[i].field == 'OrderItemId' || $scope.oldColumns[i].field == 'ItemStatusReason') {
                                column = {
                                    field: $scope.oldColumns[i].field,
                                    hidden: true
                                }
                            } else {



                                column = {
                                    field: $scope.oldColumns[i].field,
                                    title: $scope.oldColumns[i].field
                                        .replace(/^(_(s)_)/, "(")
                                        .replace(/(_(p)_)/, ")")
                                        .replace(/_/g, " ").trimLeft()
                                }
                            }

                            columns.push(column);
                        }
                    }
                }
            });


            $(document).bind("ajaxComplete.changeColumns", function () {

                $("#SubscriptionRosterReportGrid").data("kendoGrid").setOptions({
                    columns: columns,
                });

                $(document).unbind("ajaxComplete.changeColumns");
            });

        }

        $scope.saveToExcel = function () {
            $("#SubscriptionRosterReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.saveToPDF = function () {

            var url = baseUrl + "/GetSubscriptionSessionsPdf?programId=" + $scope.Model._SelectedPrograms + "&startDate=" + $scope.Filters.StartDate + "&endDate=" + $scope.Filters.EndDate;

            url = encodeURI(url);

            downloadURI(url, '')
        }

        function downloadURI(uri, name) {
            var link = document.createElement("a");
            link.download = name;
            link.href = uri;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            delete link;
        }
    }
    ]);

})();