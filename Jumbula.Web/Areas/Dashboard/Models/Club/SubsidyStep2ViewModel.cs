﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{

    public class SubsidyStep2ViewModel
    {
        [Display(Name = "Phone number")]
        [RegularExpression("[0-9]+$",
           ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string Phone { get; set; }
        [EmailAddress(ErrorMessage = "Email is not a valid email address.")]
        [Display(Name = "Email")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string Email { get; set; }

        //[Url(ErrorMessage = "Website is not a valid fully-qualified http or https URL.")]
        //[DataType(DataType.Url)]
        [RegularExpression("((?:https?:|www.?)(?:[-a-z0-9]+.)*[-a-z0-9]+.*)",
          ErrorMessage = "Website is not a valid fully-qualified www.")]
        [Display(Name = "WebSite")]
        public string WebSite { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }
        public List<SelectKeyValue<int>> States { get; set; }
        public int SelectedState { get; set; }
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Instructions { get; set; }

    }
}