﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum FollowUpFormMode : byte
    {
        [Description("After registration")]
        Offline = 0,
        [Description("During registration")]
        Online = 1
    }
}
