﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{
    public class EnrollmentViewModel
    {
        public EnrollmentViewModel()
        {

            AllSeason = SeedSeasonValue();
            YearSeason = SeedYearValue();
        }

        [Required(ErrorMessage = "{0} is required.")]
        public SeasonNames Name { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        public int Year { get; set; }
        List<SelectKeyValue<string>> SeedSeasonValue()
        {

            return DropdownHelpers.ToSelectList<SeasonNames>();
        }
        public List<SelectKeyValue<string>> AllSeason { get; set; }

        public List<SelectKeyValue<int>> YearSeason { get; set; }
        List<SelectKeyValue<int>> SeedYearValue()
        {

            var result = new List<SelectKeyValue<int>>()
            {
                new SelectKeyValue<int> { Value = 0, Text = "Select year"},
                new SelectKeyValue<int> { Value = 2016, Text = "2016"},
                new SelectKeyValue<int> { Value = 2017, Text = "2017"},
                new SelectKeyValue<int> { Value = 2018, Text = "2018"},
                new SelectKeyValue<int> { Value = 2019, Text = "2019"},
                new SelectKeyValue<int> { Value = 2020, Text = "2020"},
                new SelectKeyValue<int> { Value = 2021, Text = "2021"},
                new SelectKeyValue<int> { Value = 2022, Text = "2022"},
                new SelectKeyValue<int> { Value = 2023, Text = "2023"},
                new SelectKeyValue<int> { Value = 2024, Text = "2024"},
                new SelectKeyValue<int> { Value = 2025, Text = "2025"},

        };
            return result;
        }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}