﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('staffController', ['$scope', '$http', function ($scope, $http) {
        window.app.globalObjects.pageClass = 'staff-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/Staff", order: 0, menu: "STAFF" });
    }])

})();