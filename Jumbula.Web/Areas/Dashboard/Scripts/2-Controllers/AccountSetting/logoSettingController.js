﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('logoSettingController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/AdvanceSetting";

        $http.get($scope.baseUrl + '/GetSchoolLogoSetting').success(function (data, status, headers, config) {
            $scope.Model = data;
        });

        $scope.save = function () {
            $http.post($scope.baseUrl + '/SaveSchoolLogoSetting', { model: $scope.Model }).success(function (data, status, headers, config) {

                if (data.Status) {
                    $scope.errors = null;
                    jbToast.success('Settings saved successfully.');
                }
                else {
                    $scope.MC.handleErrors($scope, data);
                }
            });
        }
       

    }]);
})();