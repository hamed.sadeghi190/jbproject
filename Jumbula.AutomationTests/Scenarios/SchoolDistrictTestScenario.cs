﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
    
    public class SchoolDistrictTestScenario: BaseTestScenario
    {
        [Test]
        public void ViewSchoolDistricts()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to Subsidies
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToDistricts();

            // View districts
            SchoolDistrictTest schoolDistrictTest = new SchoolDistrictTest(WebDriver);
            schoolDistrictTest.View();

            WebDriver.Close();
        }

        [Test]
        public void FilterSchoolDistricts()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to Subsidies
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToDistricts();

            // View districts
            SchoolDistrictTest schoolDistrictTest = new SchoolDistrictTest(WebDriver);
            schoolDistrictTest.Filetr();

            WebDriver.Close();
        }

        [Test]
        public void EditSchoolDistrict()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to Subsidies
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToDistricts();

            // View districts
            SchoolDistrictTest schoolDistrictTest = new SchoolDistrictTest(WebDriver);
            schoolDistrictTest.Edit();

            WebDriver.Close();
        }

        [Test]
        public void AddSchoolDistrict()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to Subsidies
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToAddDistrict();

            // View districts
            SchoolDistrictTest schoolDistrictTest = new SchoolDistrictTest(WebDriver);
            schoolDistrictTest.Add();

            WebDriver.Close();
        }

    }
}
