﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('templateAfterSchoolClassesPreview2Controller', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'templateEditorService', '$timeout', function ($scope, $http, jbToast, $stateParams, $state, templateEditorService, $timeout) {
        window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/Flyer/";
        $scope.MC.stateParams = $stateParams;

        // init model.
        $scope.Model = {};
        $scope.Model.MailTemplate = {};
        $scope.Model.CountProgram = 0;
        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        }

        $scope.existingImages = templateEditorService.existingImages;

        $scope.setBackgroundImageToNone = function () {
            $scope.Model.MailTemplate.HeaderBackground = '';
        }

        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        }

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        }

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        }

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        }

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        }

        $scope.enterEditing = function () {
            return templateEditorService.enterEditing(this);
        }

        $scope.saveEditing = function () {
            return templateEditorService.saveEditing(this);
        }

        $scope.cancelEditing = function () {
            return templateEditorService.cancelEditing(this);
        }

        $http.get($scope.baseUrl + '/CreateEditFlyer').success(function (data, status, headers, config) {

            $scope.Model = data.Data;
        });

        $scope.useHtmlBreaks = function (str) {
            if (str)
                return str.replace(new RegExp('\n', 'g'), "<br>").replace(new RegExp('\r', 'g'), "<br>");
        }

        $scope.SaveFlyer = function () {

            $http.post($scope.baseUrl + '/SaveFlyer', { model: $scope.Model, typeFlyer: 3 }).success(function (data, status, headers, config) {
                if (data.Status == true) {
                    jbToast.success("Flyer saved");
                } else {
                    jbToast.error("An error ocured while saving");
                }
            }).error(function () {
                jbToast.error("An error ocured while saving");
            });
        }

        var timer = false;

        $scope.Model = {};


        $scope.Model.SchoolId = 0;
        $scope.Model.SeasonId = 0;


        $http.get($scope.baseUrl + '/GetAllSchool') //GetProvider delete
            .success(function (data, status, headers, config) {

                $scope.AllSchools = data;
            });

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };


        $scope.$watch('AllSeasons', function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.fillSeasonData = function (schoolId) {
            $http.get($scope.baseUrl + '/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllSeasons = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.Model.SchoolId = schoolId;

        }


        var checkValidation = function (schoolId, seasonId, name, email, phone) {

            if (is.undefined(schoolId) || schoolId <= 0) {
                jbToast.error("Please select a school.");
                return false;
            }
            else if (is.undefined(seasonId) || seasonId <= 0) {
                jbToast.error("Please select a season.");
                return false;
            }
            else {
                if (email == null || email == "") {
                    jbToast.error("Please enter email.");
                    return false;
                }
                else if (phone == null || phone == "") {
                    jbToast.error("Please enter phone.");
                    return false;
                }
                else if (name == null) {
                    jbToast.error("Please enter flyer name.");
                    return false;
                }
                else if (!validateEmail(email)) {
                    jbToast.error("Email is not valid.");
                    return false;
                }
                else {
                    return true;
                }


            }
        }

        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }

        $scope.FillData = function () {

            if (checkValidation($scope.Model.SchoolId, $scope.Model.SeasonId, $scope.Model.Name, $scope.Model.Email, $scope.Model.Phone)) {

                var seasonId = $scope.Model.SeasonId;
                var schoolId = $scope.Model.SchoolId;
                var name = $scope.Model.Name;
                var Email = $scope.Model.Email;
                var Phone = $scope.Model.Phone;
                var Comments = $scope.Model.Comments;
                var IsGreenColor = $scope.Model.IsGreenColor;
                var IsBlueColor = $scope.Model.IsBlueColor;

                $http.get($scope.baseUrl + '/GetDataFlyer', { params: { clubId: schoolId, seasonId: seasonId, name: name, email: Email, phone: Phone, Comments: Comments, IsGreenColor: IsGreenColor, IsBlueColor: IsBlueColor  } }).success(function (data, status, headers, config) {

                    $scope.Model = data;
                    $scope.Model.SeasonId = seasonId;
                    $scope.Model.SchoolId = schoolId;
                    $scope.Model.Email = Email;
                    $scope.Model.Phone = Phone;
                    $scope.Model.Comments = Comments;
                    $scope.Model.IsGreenColor = IsGreenColor;
                    $scope.Model.IsBlueColor = IsBlueColor;

                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });

            }
        }
    }]);

})();
