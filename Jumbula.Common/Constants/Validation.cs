﻿
namespace Jumbula.Common.Constants
{
    public class Validation
    {
        public const string UrlWitoutHttpRegex = @"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$";
        public const string RequiredFormat = "{0} is required";
        public const string RegistrationGeneralErrorMessage = "We have encountered an internal issue and need to recapture the registration information.If this issue happens again, please notify your administrator.";
        public const string EmailRegex = @"^([a-zA-Z0-9_\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        public const string NumberOfSelectedProgram = "You should choose at least one program.";
        public const string NumberOfSelectedDays = "You should choose at least one day.";
        public const string ActivateStaff = "Staff actived successfully.";
        public const string PriceRegex = @"^[0-9]+(\.[0-9]{1,2})?$";
        public const string PriceRegexErrorMessage = "{0} is not valid";
        public const string RequiredMessage = "{0} is required.";
        public const string MaxStringLength = "Max {0} length is {1}";
    }
}