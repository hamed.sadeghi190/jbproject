﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Common.Enums
{
    public enum SpaceRequirementType : byte
    {
        Indoor = 0,
        Outdoor = 1
    }
}
