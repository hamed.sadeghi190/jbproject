﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignsController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaigns-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/Campaign';

        $scope.campaignDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetAllCampaigns',
                        type: "POST",
                        data: function () {
                            return {
                                term: $scope.searchTerm,
                            };
                        },
                    },
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverPaging: true,
            pageSize: 10,
        });

        $scope.gridOptions = {
            dataSource: $scope.campaignDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "CampaignName",
                    title: "Campaign",
                    template: "<strong class='grid-row-title'>#= CampaignName #</strong>#if(Status == 'Draft'){# <i class='grid-label label' style='background-color:\\#ffbc00;color:white'>Draft</i> #}else if(Status == 'Cancel'){# <i class='grid-label label warning'>Canceled</i> #} else if(Status == 'Scheduled'){# <i class='grid-label label success'>Campaign Scheduled on #= kendo.toString(kendo.parseDate(SendDate), 'MMM dd, yyyy h:mm tt') #</i>#}else{# <i class='grid-label label success'>Campaign Sent</i> #}#",
                    width: "200px",
                }, {
                    field: "CreateDate",
                    title: "Created date",
                    width: "120px",
                    template: "#= kendo.toString(kendo.parseDate(CreateDate), 'MMM dd, yyyy h:mm tt') #"
                }, {
                    field: "SendDate",
                    title: "Sent date",
                    width: "120px",
                    template: "#= SendDate != null ? kendo.toString(kendo.parseDate(SendDate), 'MMM dd, yyyy h:mm tt') : '<sup class=\"grid-row-empty\">____________________</sup>' #",
                },
                {
                    field: "",
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "#if(Status !='Cancel'){#<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                               #if(Status=='Draft'){#\
                                 #if(LastCreatedPage == 'Step1'){#\
                                      <li ui_sref='EmailCampaignAddStep2({campaignId: #=Id#,campaignType:#= '\"' + CampaignType + '\"' # })' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Continue setup</span>\
                                      </li>\
                                  #}\
                                  else if(LastCreatedPage == 'Step2'){#\
                                            <li ui_sref='EmailCampaignAddStep3({campaignId: #=Id#,campaignType:#= '\"' + CampaignType + '\"' # })' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Continue setup</span>\
                                      </li>\
                                  #}\
                                  else if(LastCreatedPage == 'Step3'){#\
                                            <li ui_sref='EmailCampaignAddStep3({campaignId: #=Id#,campaignType:#= '\"' + CampaignType + '\"' # })' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Continue setup</span>\
                                      </li>\
                                   #}\
                                     else if(LastCreatedPage == 'Step4'){#\
                                            <li ui_sref='EmailCampaignAddStep2({campaignId: #=Id#,campaignType:#= '\"' + CampaignType + '\"' # })' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Continue setup</span>\
                                      </li>\
                                   #}#\
                                 #}\
                                else if(Status !='Cancel'){#\
                                  <li  ngaction='CampaignRecipients_View' ui_sref='EmailCampaign({campaignId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-show'>View report</span>\
                                      </li>\
                                      <li  ui_sref='ReviewCampaign({campaignId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-show'>View campaign</span>\
                                      </li>\
                                 #}#\
                                 #if(Status=='Scheduled' && HaveTimeForCancel==true){#\ <hr/>\<li ng-click='CancelCampaign(#= Id#)' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-remove'>Cancel</span>\
                                      </li>\
                                    #}#\
                                     #if(Status=='Scheduled' && HaveTimeForCancel==false){#\ <hr/>\<li class='no-access k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-remove'>Cancel</span>\
                                      </li>\
                                    #}#\
                                #if(Status=='Draft' ){#\
                                <li disabled='disabled'><hr /></li>\
                                \<li ng-click='deleteEmailCampaign(#= Id #)'>\
                                    <span class='k-link jbi-remove'>Delete</span>\
                                </li> #}#\
                            </ul>\
                        </li>\
                    </ul>#}#",
                }
            ],
        };

        $scope.deleteEmailCampaign = function (Id) {
            jbConfirmation.yes(function () {
                $http.post(baseUrl + '/DeleteCampaign', { campaignId: Id }).success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.gridOptions.dataSource.read();
                    }
                });
            }).confirm();
        }

        $scope.campaignSearch = function () {
            $scope.gridOptions.dataSource.read();
        }


        $scope.CancelCampaign = function (Id) {
            jbConfirmation
                .yes(function () {
                    $http.post(baseUrl + '/CancelScheduledCampaign', { campaignId: Id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $scope.gridOptions.dataSource.read();
                            jbToast.success("Your campaign has been canceled successfuly.");
                        } else {
                            jbToast.error(data.Message)
                        }
                    })
                })
               .title('Warrning')
               .message('Are you sure you want to proceed with this action?')
               .confirm();

        }


        $scope.mailListrecipientsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetAllListRecipients?listId=' + $scope.MC.stateParams.listId,
                        type: "POST",
                        data: function () {
                            return {
                                term: $scope.recipientSearchTerm,
                            };
                        },
                    },
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverPaging: true,
            pageSize: 10,
        });

        $scope.recipientsGridOptions = {
            dataSource: $scope.mailListrecipientsDataSource,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            sortable: true,
            columns: [
                {
                    field: "EmailAddress",
                    title: "Email",
                    template: "",
                    width: "200px",
                }, {
                    field: "FullName",
                    title: "recipient Name",
                    width: "120px"
                }
            ]
        };

        $scope.recipientsSearch = function () {
            $scope.recipientsGridOptions.dataSource.read();
        }

        $scope.editEmailListDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetAllListRecipients?listId=' + $scope.MC.stateParams.listId,
                        type: "POST",
                        data: function () {
                            return {
                                term: $scope.recipientSearchTerm,
                            };
                        },
                    },
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverPaging: true,
            pageSize: 10,
        });

        $scope.editEmailListGridOptions = {
            dataSource: $scope.editEmailListDataSource,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            sortable: true,
            columns: [
                {
                    field: "EmailAddress",
                    title: "Email",
                    template: "",
                    width: "200px",
                }, {
                    field: "FullName",
                    title: "Recipient name",
                    width: "120px"
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template:
                        "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				        <ul>\
				            <li ng-click='deleteListRecipient(#=Id#)'>\
					            <span class='k-link jbi-remove'>Delete</span>\
				            </li>\
				            </ul>\
			            </li>\
		            </ul>",
                    attributes: {
                        class: "grid-actions"
                    }
                }
            ]
        };

        $scope.deleteListRecipient = function (Id) {
            jbConfirmation.yes(function () {
                $http.post(baseUrl + '/DeleteEmailListRecipient', { listId: $scope.MC.stateParams.listId, recipientId: Id }).success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.editEmailListGridOptions.dataSource.read();
                    }
                });
            }).confirm();
        }

        $scope.recipientsSearch = function () {
            $scope.editEmailListGridOptions.dataSource.read();
        }

    }]);
})();