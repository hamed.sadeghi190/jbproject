﻿using System.ComponentModel;


namespace Jumbula.Common.Enums
{
    public enum TimeZone
    {
        [Description("Eastern Standard Time")]
        EST = 0,
        [Description("Central Standard Time")]
        CST = 1,
        [Description("Mountain Standard Time")]
        MST = 2,
        [Description("Pacific Standard Time")]
        PST = 3,
        [Description("UTC")]
        UTC = 4,
        [Description("Central European Standard Time")]
        ECT = 5,
        [Description("E. Europe Standard Time")]
        EET = 6,
        [Description("Egypt Standard Time")]
        ART = 7,
        [Description("E. Africa Standard Time")]
        EAT = 8,
        [Description("Middle East Standard Time")]
        MET = 9,
        [Description("Pakistan Standard Time")]
        PKT = 11,
        [Description("India Standard Time")]
        IST = 12,
        [Description("Bangladesh Standard Time")]
        BST = 13,
        [Description("SE Asia Standard Time")]
        SAST = 14,
        [Description("Taipei Standard Time")]
        TIST = 15,
        [Description("Tokyo Standard Time")]
        TST = 16,
        [Description("Cen. Australia Standard Time")]
        ACT = 17,
        [Description("E. Australia Standard Time")]
        AET = 18,
        [Description("Samoa Standard Time")]
        SST = 19,
        [Description("New Zealand Standard Time")]
        NST = 20,
        [Description("China Standard Time")]
        CT = 21,
        [Description("Hawaiian Standard Time")]
        HST = 22,
        [Description("Alaskan Standard Time")]
        AKST = 23,
        [Description("Atlantic Standard Time")]
        AST = 26,
        [Description("Newfoundland Standard Time")]
        CNT = 27,
        [Description("Argentina Standard Time")]
        AGT = 28,
        [Description("Central Brazilian Standard Time")]
        BET = 29,
        [Description("W. Central Africa Standard Time")]
        CAT = 30,
    }

}
