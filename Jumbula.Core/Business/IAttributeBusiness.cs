﻿using System.Linq;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface IAttributeBusiness
    {
        IQueryable<JbAttribute> GetList(AttributeReferenceType referenceType, int clubId, int? partnerId = null);
        IQueryable<JbAttribute> GetList(AttributeReferenceType referenceType, Club club, int? partnerId = null);
    }
}
