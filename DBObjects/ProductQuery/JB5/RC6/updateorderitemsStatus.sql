alter  TABLE [dbo].[OrderItems] add	[ItemStatus] [smallint]  NULL
alter  TABLE [dbo].[OrderItems] add	[ItemStatusReason] [smallint]  NULL
update [OrderItems] set [ItemStatus]=0
update [OrderItems] set [ItemStatusReason]=0
alter  TABLE [dbo].[OrderItems] alter column 	[ItemStatus] [smallint] NOT NULL
alter  TABLE [dbo].[OrderItems] alter column 	[ItemStatusReason] [smallint] NOT NULL