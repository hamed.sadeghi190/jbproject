﻿using System.Web.Mvc;
using Jumbula.Web.Mvc.Controllers;

namespace Jumbula.Web.Controllers
{
    public class PagesController : JbBaseController
    {
        [AllowAnonymous]
        public virtual ActionResult BSJ()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult BalletSanJose(string section = "")
        {
            return View(viewName: "BalletSanJose" + section);
        }

        [AllowAnonymous]
        public virtual ActionResult SVB(string section = "")
        {
            return View(viewName: "BalletSanJose" + section);
        }

        [AllowAnonymous]
        public virtual ActionResult Ventana()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult Sequoia()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult funmandarin()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult LJESFall2015()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult LearnersChessFall2015()
        {
            return View();
        }

        [AllowAnonymous]
        public virtual ActionResult chessodysseyprograms()
        {
            return View();
        }

        public virtual ActionResult SendGridMail()
        {
            return View();
        }

     
    }

    public class CategoryViewModel
    {
        public int Id { get; set; }

        public string Parent { get; set; }

        public string Name { get; set; }

        public TypeCategory CategoryType
        {
            get
            {
                return string.IsNullOrEmpty(this.Parent) ? TypeCategory.Parent : TypeCategory.Child;
            }
        }
    }

    public enum TypeCategory
    {
        Parent = 0,
        Child = 1
    }
}
