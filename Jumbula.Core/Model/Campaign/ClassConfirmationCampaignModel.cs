﻿using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Core.Model.Campaign
{
    public class ClassConfirmationCampaignModel
    {
        [Display(Name = "Class date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? ClassDateTime { get; set; }

        public bool IsAllSchools { get; set; }
        [Display(Name = "season name")]
        [Required(ErrorMessage = "{0} is required.")]
        public SeasonNames SeasonName { get; set; }

        [Display(Name = "Year")]
        [Required(ErrorMessage = "{0} is required.")]
        public int Year { get; set; }

        public List<SelectKeyValue<long>> AllSchools { get; set; }

        public List<long> SelectedSchools { get; set; }

        public List<SelectKeyValue<string>> AllSeasons { get; set; }

        public List<SelectKeyValue<int>> AllYears { get; set; }
    }
}
