﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class ContactElements : BaseElements
    {
        public ContactElements(IWebDriver driver)
            : base(driver)
        {

        }
        #region Header
        public IWebElement BackLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//i[@class='icon-arrow-left']"));
            }
        }
        public IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//div[@class='title']/h1"));
            }
        }

        public IWebElement AddContactLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@href='#/Contact/Add']"));
            }
        }
        #endregion Header

        #region Filtering
        public IWebElement FilterType
        {
            get
            {
                return Driver.FindElement(By.Id("filterType"));
            }
        }
        public IWebElement FilterNameOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Name']"));
            }
        }
        public IWebElement FilterEmailOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Email']"));
            }
        }
        public IWebElement FilterPhoneOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Phone']"));
            }
        }
        public IWebElement FilterTerm
        {
            get
            {
                return Driver.FindElement(By.Id("filterTerm"));
            }
        }
        public IWebElement FilterTitle
        {
            get
            {
                return Driver.FindElement(By.Id("filterTitle"));
            }
        }
        public IWebElement FilterTitle_GetOptionByTitle(string title)
        {
            return Driver.FindElement(By.XPath(string.Format("//option[starts-with(@label,'{0}')]", title)));
        }
        public IWebElement FilterOrganization
        {
            get
            {
                return Driver.FindElement(By.Id("filterOrganization_chosen"));
            }
        }
        public IWebElement FilterSelectedOrganization
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='filterOrganization_chosen']/a/span"));
            }
        }
        public IWebElement FilterOrganization_GetOptionByTitle(string option)
        {
           return Driver.FindElement(By.XPath(string.Format("//*[@id='filterOrganization_chosen']//li[text()='{0}']",option)));

        }
        public IWebElement FilterOrganization_GetOptionByIndex(int index)
        {
            return Driver.FindElement(By.XPath(string.Format("//*[@id='filterOrganization_chosen']//li[{0}]", index)));

        }
        public IWebElement filterButton
        {
            get
            {
                return Driver.FindElement(By.Id("filterButton"));
            }
        }
        public IWebElement clearFilters
        {
            get
            {
                return Driver.FindElement(By.Id("clearFilters"));
            }
        }
        #endregion Filtering
       
        #region Grid Headers
        
        public IWebElement NameHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='FullName']"));
            }
        }
        public IWebElement OrganizationHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='OrganizationName']"));
            }
        }
        public IWebElement EmailHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='Email']"));
            }
        }
        public IWebElement PhoneHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='Phone']"));
            }
        }
        public IWebElement CellPhoneHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='CellPhone']"));
            }
        }
        public IWebElement WorkPhoneHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='WorkPhone']"));
            }
        }
        public IWebElement NicknameHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='Nickname']"));
            }
        }
        public IWebElement TitleHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='Title']"));
            }
        }
        public IWebElement DoBHeader
        {
            get
            {
                return Driver.FindElement(By.XPath("//th[@data-field='DoB']"));
            }
        }
        #endregion Grid Headers

        #region AddContact
        public IWebElement FirstName
        {
            get
            {
                return Driver.FindElement(By.Id("Model.FirstName"));
            }
        }
        public bool FirstNameExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Model.FirstName")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement LastName
        {
            get
            {
                return Driver.FindElement(By.Id("Model.LastName"));
            }
        }
        public bool LastNameExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Model.LastName")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Email
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Email"));
            }
        }
        public bool EmailExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Model.Email")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement CellPhone
        {
            get
            {
                return Driver.FindElement(By.Id("Model.CellPhone"));
            }
        }
        public bool CellPhoneExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Model.CellPhone")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Phone
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Phone"));
            }
        }
        public bool PhoneExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Model.Phone")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement PhoneExtension
        {
            get
            {
                return Driver.FindElement(By.Id("Model.PhoneExtension"));
            }
        }
        public bool PhoneExtensionExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Model.PhoneExtension")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement OrganizationList
        {
            get
            {
                return Driver.FindElement(By.Id("OrganizationId_chosen"));
            }
        }
        public IWebElement OrganizationListSelect
        {
            get
            {
                return Driver.FindElement(By.Id("OrganizationId"));
            }
        }
        public bool OrganizationExist
        {
            get
            {
                if (Driver.FindElements(By.Id("OrganizationId_chosen")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SelectedOrganization
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='OrganizationId_chosen']/a/span"));
            }
        }
        public IWebElement TitleList
        {
            get
            {
                return Driver.FindElement(By.Id("Title"));
            }
        }
        public IWebElement GetTitle(string title)
        {
            return Driver.FindElement(By.XPath(string.Format("//*[@id='Title']/option[starts-with(@label,'{0}')]", title)));
        }
        public bool TitleExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Title")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Nickname
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Nickname"));
            }
        }
        public bool NicknameExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Model.Nickname")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement WorkPhone
        {
            get
            {
                return Driver.FindElement(By.Id("Model.WorkPhone"));
            }
        }
        public bool WorkPhoneExist
        {
            get
            {
                if (Driver.FindElements(By.Id("Model.WorkPhone")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement DoB
        {
            get
            {
                return Driver.FindElement(By.Id("dob"));
            }
        }
        public bool DoBExist
        {
            get
            {
                if (Driver.FindElements(By.Id("dob")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Organization_GetOptionByTitle(string option)
        {
            return Driver.FindElement(By.XPath(string.Format("//*[@id='OrganizationId_chosen']//li[text()='{0}']", option)));
        }
        public IWebElement Organization_GetOptionByIndex(int index)
        {
            return Driver.FindElement(By.XPath(string.Format("//*[@id='OrganizationId_chosen']//li[{0}]", index)));
        }
        public IWebElement CancleLink
        {
            get
            {
                return Driver.FindElement(By.LinkText("Cancel"));
            }
        }
        public IWebElement SubmitBtn
        {
            get
            {
                IList<IWebElement> btns = Driver.FindElements(By.TagName("button"));
                return btns.First(btn => btn.Text == "Submit");
            }
        }
        public IWebElement MessageboxTitle
        {
            get
            {
                return Driver.FindElement(By.ClassName("toast-title"));
            }
        }
        public IWebElement MessageboxBody
        {
            get
            {
                return Driver.FindElement(By.ClassName("toast-message"));
            }
        }
        public bool MessageboxExist
        {
            get
            {
                if (Driver.FindElements(By.ClassName("toast-message")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool WarningExists
        {
            get
            {
                var elements = Driver.FindElements(By.TagName("h3"));
                var warning = elements.FirstOrDefault(elm => elm.Text == "Warning!");
                if (warning != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement CancelBtn
        {
            get
            {
                var elements = Driver.FindElements(By.TagName("button"));
                return elements.FirstOrDefault(elm => elm.Text == "Cancel");
                
            }
        }
        public IWebElement YesBtn
        {
            get
            {
                var elements = Driver.FindElements(By.TagName("button"));
                return elements.FirstOrDefault(elm => elm.Text == "Yes, I'm sure");

            }
        }
        #endregion AddContact

        #region ContactSetting
        public IWebElement FirstNameEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='FirstName']"));
            }
        }
        public IWebElement FirstNameEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='FirstName']//following-sibling::span"));
            }
        }
        public IWebElement FirstNameShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='FirstName']"));
            }
        }
        public IWebElement FirstNameShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='FirstName']//following-sibling::span"));
            }
        }
        public IWebElement LastNameEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='LastName']"));
            }
        }
        public IWebElement LastNameEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='LastName']//following-sibling::span"));
            }
        }
        public IWebElement LastNameShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='LastName']"));
            }
        }
        public IWebElement LastNameShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='LastName']//following-sibling::span"));
            }
        }
        public IWebElement OrganizationEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='OrganizationName']"));
            }
        }
        public IWebElement OrganizationEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='OrganizationName']//following-sibling::span"));
            }
        }
        public IWebElement OrganizationShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='OrganizationName']"));
            }
        }
        public IWebElement OrganizationShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='OrganizationName']//following-sibling::span"));
            }
        }
        public IWebElement EmailEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Email']"));
            }
        }
        public IWebElement EmailEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Email']//following-sibling::span"));
            }
        }
        public IWebElement EmailShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Email']"));
            }
        }
        public IWebElement EmailShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Email']//following-sibling::span"));
            }
        }
        public IWebElement PhoneEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Phone']"));
            }
        }
        public IWebElement PhoneEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Phone']//following-sibling::span"));
            }
        }
        public IWebElement PhoneShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Phone']"));
            }
        }
        public IWebElement PhoneShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Phone']//following-sibling::span"));
            }
        }
        public IWebElement CellPhoneEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Cell']"));
            }
        }
        public IWebElement CellPhoneEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Cell']//following-sibling::span"));
            }
        }
        public IWebElement CellPhoneShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Cell']"));
            }
        }
        public IWebElement CellPhoneShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Cell']//following-sibling::span"));
            }
        }
        public IWebElement WorkPhoneEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Work']"));
            }
        }
        public IWebElement WorkPhoneEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Work']//following-sibling::span"));
            }
        }
        public IWebElement WorkPhoneShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Work']"));
            }
        }
        public IWebElement WorkPhoneShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Work']//following-sibling::span"));
            }
        }
        public IWebElement TitleEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Title']"));
            }
        }
        public IWebElement TitleEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Title']//following-sibling::span"));
            }
        }
        public IWebElement TitleShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Title']"));
            }
        }
        public IWebElement TitleShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Title']//following-sibling::span"));
            }
        }
        public IWebElement NicknameEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Nickname']"));
            }
        }
        public IWebElement NicknameEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='Nickname']//following-sibling::span"));
            }
        }
        public IWebElement NicknameShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Nickname']"));
            }
        }
        public IWebElement NicknameShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='Nickname']//following-sibling::span"));
            }
        }
        public IWebElement DoBEnabledInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='DoB']"));
            }
        }
        public IWebElement DoBEnabledSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-enable='DoB']//following-sibling::span"));
            }
        }
        public IWebElement DoBShowInGridInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='DoB']"));
            }
        }
        public IWebElement DoBShowInGridSpan
        {
            get
            {
                return Driver.FindElement(By.XPath("//input[@data-showingrid='DoB']//following-sibling::span"));
            }
        }
        #endregion ContactSetting
    }
}
