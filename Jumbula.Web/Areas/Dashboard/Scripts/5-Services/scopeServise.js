﻿(function () {
    'use strict';

    angular.module('jbTools').service("scopeServise", [function () {
        var scope;
        this.set = function ($scope) {
            if (is.existy($scope)) {
                scope = $scope;
            } else {
                throw "Scope is null or not defined";
            }
            return true;
        };
        this.get = function () {
            return scope;
        };
    }]);
})();