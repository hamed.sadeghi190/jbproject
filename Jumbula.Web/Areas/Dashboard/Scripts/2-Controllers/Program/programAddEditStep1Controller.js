﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programAddEditStep1Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', 'uiComponentsService', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation, uiComponentsService) {

        $scope.baseUrl = window.location.origin + "/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;

        if ($state.returnByBack()) {
            $scope.Model = programService.getModel();
        }

        //editor
        //$scope.testimonialEditorOptions = {
        //    tools: [
        //        'bold',
        //        'italic',
        //        'underline',
        //        'fontSize',
        //        'createLink',
        //        'unlink',
        //        'viewHtml',
        //        "insertFile",
        //    ],
        //    fileBrowser: {
        //        messages: {
        //            dropFilesHere: "Drop files here"
        //        },

        //        transport: {
        //            type: "imagebrowser-aspnetmvc",
        //            read: "FileBrowser/Read",
        //            destroy: {
        //                url: "FileBrowser/Destroy",
        //                type: "POST"
        //            },
        //            create: {
        //                url:  "FileBrowser/Create",
        //                type: "POST"
        //            },
        //            uploadUrl:  "FileBrowser/upload",
        //            fileUrl:  "FileBrowser/File?path={0}"
        //        }
        //    }
        //};

        var pageMode = null;
        var fromTemplate = false;

        $scope.arrayCropper = [];
        $scope.arraySourceCropper = [];
        $scope.cropper = {};
        $scope.cropper.sourceImage = null;
        $scope.cropper.croppedImage = null;
        $scope.bounds = {};
        $scope.bounds.left = 0;
        $scope.bounds.right = 0;
        $scope.bounds.top = 0;
        $scope.bounds.bottom = 0;
        $scope.getSize = function (elem) {
            var element = document.getElementById("btn_file");
            var files = element.files;
            var file = files[0];
            var reader = new FileReader();
            reader.onload = loadFinished;
            reader.readAsDataURL(file);
            function loadFinished(event) {
                var data = event.target.result;
                var image = new Image();
                image.src = data;
                image.onload = function () {
                    $scope.bounds = {};
                    $scope.bounds.left = 0;
                    $scope.bounds.right = image.naturalWidth;
                    $scope.bounds.top = image.naturalHeight;
                    $scope.bounds.bottom = 0;
                }
            }
        }

        var programId = $stateParams.programId;

        if (programId == 'undefined' || programId == '') {
            pageMode = 'Create';
        }
        else {
            pageMode = 'Edit';
        }

        if (!($stateParams.fromTemplate == 'undefined' || $stateParams.fromTemplate == '')) {
            fromTemplate = $stateParams.fromTemplate;
        }

        if (!$state.returnByBack()) {

            if (pageMode == 'Create') {

                programService.getStep1Create(function (data, status, headers, config) {
                    $scope.Model = data;
                },
                function () {
                }, { programType: $stateParams.programType, seasonDomain: $stateParams.seasonDomain });
            }
            else {

                programService.getStep1Edit(function (data, status, headers, config) {
                    $scope.Model = data;

                    if ($scope.Model.ProgramImages.length > 0) {
                        for (var i = 0; i < $scope.Model.ProgramImages.length; i++) {
                            $scope.arrayCropper.push($scope.Model.ProgramImages[i])
                        }
                    }
                },
                function () {
                }, { id: programId, fromTemplate: fromTemplate });
            }
        }

        $scope.categoriesOptions = {
            placeholder: "Select category...",
            dataTextField: "Title",
            dataValueField: "Value",
            autoBind: true,
            dataSource: programService.categories
        };

        $scope.addLocation = function () {

            programService.setModel($scope.Model);

            $state.forward('ClubLocations', { action: 'add', locationId: 0 }, 'ProgramAddEditStep1', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId },
                 function () {

                     $scope.setAllLocations();

                 });
        }

        $scope.addInstructor = function () {

            programService.setModel($scope.Model);

            $state.forward('AddStaff', { staffRole: 'Instructor' }, 'ProgramAddEditStep1', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId },
                 function () {

                 });
        }

        $scope.editLocation = function () {

            programService.setModel($scope.Model);

            $state.forward('ClubLocations', { action: 'edit', locationId: $scope.Model.SelectedLocation }, 'ProgramAddEditStep1', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId },
                function () {
                    $scope.setAllLocations();
                });
        }

        $scope.removeLocation = function () {

            $http.post('Dashboard/Club/IsLocationAssignToAnotherProgram', { programId: $scope.MC.stateParams.programId, locationId: $scope.Model.SelectedLocation }).success(function (data, status, headers, config) {
                    if (data) {
                        jbToast.error("You cannot delete this location because it used in other programs.");
                    } else {

                        jbConfirmation
                            .yes(function () {

                                $http.post('Dashboard/Club/DeleteLocation', { id: $scope.Model.SelectedLocation }).success(function (data, status, headers, config) {

                                    $scope.setAllLocations();

                                    $scope.Model.SelectedLocation = $scope.Model.AllLocations[0].Value;

                                    jbToast.success("Club location deleted.");
                                });

                            })
                            .confirm();
                    }  
                });
        }

        $scope.changePIN = function () {
            if (!$scope.Model.HasRegisterPIN)
                $("#register-pin input").val("");
        }

        $scope.setAllLocations = function () {

            $http.get('Dashboard/club/GetLocations').success(function (data, status, headers, config) {
                $scope.Model.AllLocations = data;
            });
        }


        $scope.AddTo = function (cropImg, sourcImg) {

            $scope.arrayCropper.push(cropImg);
            $scope.arrayCropper = $scope.arrayCropper.filter(function (elem, index, self) {
                return index == self.indexOf(elem);
            })

            $scope.Model.ProgramImages = $scope.arrayCropper;

            $scope.arraySourceCropper.push(sourcImg);
            $scope.Model.SourceImages = $scope.arraySourceCropper;

            $scope.cropper.sourceImage = null;

            if ($scope.Model.ProgramImages.length == 5) {

                document.getElementById("btn_Add").disabled = true;

            }

        };

        $scope.removeImage = function (index) {

            $scope.arrayCropper.splice(index, 1);
            $scope.Model.ProgramImages = $scope.arrayCropper;
            document.getElementById("btn_Add").disabled = false;
            $scope.disable = false;

        }
        $scope.disable = false;

        $scope.Canvac = function () {
            $scope.cropper.sourceImage = null;


            if ($scope.Model.ProgramImages.length == 5) {
                $scope.disable = true;
            }



        }

        $scope.save = function () {

            $scope.Model.ProgramImages = $scope.arrayCropper;

            $scope.Model.SourceImages = $scope.arraySourceCropper;

            $scope.Model.DisableCapacityRestriction = $scope.Model.HasRegisterPIN ? $scope.Model.DisableCapacityRestriction : false;

            programService.setModel($scope.Model);

            programService.saveStep1(function (data, status, headers, config) {

                if (data.Status) {

                    var result = JSON.parse(data.Data);

                    $state.go('ProgramAddEditStep2', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: result.programId })
                }
                else {
                    $scope.MC.handleErrors($scope, data);
                }
            });
        };

        if ($state.returnByBack()) {
            $scope.MC.backQueueItems.pop();
        }

        $scope.toggleTooltip = function (containerId) {
            if (arguments[2])
                uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2]);
            else if (arguments[1]) {
                uiComponentsService.toggleTooltip(containerId, arguments[1]);
            }
            else
                uiComponentsService.toggleTooltip(containerId);
        }
    }]);
})();

