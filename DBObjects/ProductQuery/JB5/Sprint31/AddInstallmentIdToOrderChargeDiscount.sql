 
ALTER TABLE OrderChargeDiscounts Add InstallmentId bigint NULL

Go

ALTER TABLE [dbo].[OrderChargeDiscounts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderChargeDiscounts_dbo.OrderInstallments_InstallmentId] FOREIGN KEY([InstallmentId])
REFERENCES [dbo].[OrderInstallments] ([Id])

