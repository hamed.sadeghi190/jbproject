﻿using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models    //.system
{
    public class ReveneuReportViewModel : BaseViewModel<ClientBillingHistory, long>
    {


        public ReveneuReportViewModel()
        { }
        public string Date { get; set; }
        public string Name { get; set; }
        public decimal ChargeWithCredit { get; set; }
        public decimal Percent { get; set; }

        public decimal ChareWithManualy { get; set; }
        public decimal TotalCharge { get; set; }

    }  
}