﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('paymentHistoryController', ['$scope', '$http', '$log', '$stateParams', '$state', 'paymentHistoryService', function ($scope, $http, $log, $stateParams, $state, paymentHistoryService) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        $scope.baseUrl = window.location.origin + "/Dashboard/Order/";
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;

        $scope.installmentId = $stateParams.installmentId;
        $scope.orderItemId = $stateParams.orderItemId;
        $scope.Model = paymentHistoryService.get();




        $scope.cancelAction = function () {
            paymentHistoryService.set($scope.Model);
            $state.back();
        }
        $scope.installmentTransactionsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: window.location.origin + '/Dashboard/Order/GetOrderItemTransactions/?orderItemId=' + $scope.orderItemId+'&installmentId='+$scope.installmentId,
                        type: "POST",
                    },

                },
            schema: {
                total: "total",
                data: "data"
            },
            serverSorting: false,
            batch: true,
            resizable: false,
        });
        $scope.installmentTransactionsGridOptions = {
            dataSource: $scope.installmentTransactionsDataSource,
            sortable: true,
            resizable: false,
            columns: [
                {
                    field: "StrTransactionDate",
                    title: "Date",
                },
                {
                    field: "StrAmount",
                    title: "Amount",
                },
                {
                    field: "StrPaymentMethod",
                    title: "Payment method",
                },
                {
                    field: "StrTransactionStatus",
                    title: "Status",
                },
                {
                    field: "TransactionId",
                    title: "Transaction Id",
                },
                 {
                     field: "Note",
                     title: "Memo",
                 }
            ],
        }

    }])
})();