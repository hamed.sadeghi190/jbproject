﻿using System.Web.Mvc;
using SportsClub.Db;
using SportsClub.Domain;
using SportsClub.Infrastructure;
using SportsClub.Infrastructure.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Jb.Framework.Web.Model;
using Jb.Framework.Common.Forms;


namespace SportsClub.Models
{
    public class EditCourseViewModel : BaseViewModel<Domain.Order>
    {
        private string orderID;

        public Course course { get; set; }
        public EventBaseInfoMetaData ContactMetaData { get; set; }
        public Order Order { get; set; }
        public string SelectedCourse { get; set; }
        public string ClubDomain { get; set; }
        public string Domain { get; set; }
        public int CourseId { get; set; }
        public Course[] Courses { get; set; }
        public Domain.Event[] Events { get; set; }
        public Domain.Event CourseEvent { get; set; }
        public int OrderId { get; set; }
        public string Name { get; set; }
        public bool HasSessionPrice { get; set; }
        public bool HasProRatingPrice { get; set; }
        public bool IsAdultLoggedInUser { get; set; }
        List<CourseOrderItem> courseOrderItems { get; set; }
        public bool? IsAdminMode { get; set; }
        public CourseType CourseType { get; set; }
        public decimal Financial { get; set; }
        public EventRunningStatus EventRunningStatus { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        // i dont know really we need them!!
        public List<ChargeDiscountViewModel> MandetoryCharges { get; set; }
        public List<OptionalChargeDiscountViewModel> OptionalCharges { get; set; }
        public List<ChargeDiscountViewModel> CustomDiscounts { get; set; }
        public IEnumerable<SelectListItem> AllTeachers { get; set; }
        public DonationViewModel Donation { get; set; }
        [DisplayName("Early bird discount")]
        public ChargeDiscountViewModel EarlybirdDiscount { get; set; }
        [DisplayName("Sibling discount")]
        public SiblingViewModel SiblingDiscount { get; set; }
        [DisplayName("Club membership discount")]
        public ChargeDiscountViewModel MembershipDiscount { get; set; }
        [Required]
        public CourseRegistrationType RegistrationType { get; set; }
        [Display(Name = "Teacher Name")]
        public int? TeacherId { get; set; }
        public decimal? FullPrice { get; set; }
        public decimal? SessionPrice { get; set; }
        public int? NumOfSessionLeft { get; set; }
        public int TotalSessions { get; set; }
        public int CategoryId { get; set; }
        public Nullable<DateTime> DropInSessionDate { get; set; }
        public bool HasDropInMode { get; set; }
        //
        public string PlayerImageUrl { get; set; }
        public IEnumerable<SelectListItem> PlayerLists { get; set; }
        public List<SelectListItem> courseItemsForDropDown { get; set; }
        public JbForm JbForm { get; set; }

        public EditCourseViewModel()
        {
            init();
            Donation = new DonationViewModel();
        }

        public EditCourseViewModel(int orderId, string domain, string subDomain)
        {
            ClubDomain = domain;
            Domain = subDomain;
            SelectedCourse = subDomain;
            OrderId = orderId;
            Order = Ioc.IOrderDbService.GetOrder(orderId, false);
            
            switch (Order.SubDomainCategory)
            {
                
                case SubDomainCategories.Course:
                    {
                        #region course
                        SeedChargeDiscounts();
                        Courses = Ioc.ICourseDbService.Read(domain);
                        course = Courses.Single(m => m.Domain == subDomain);
                        Events = null;
                        Name = course.Name;
                        Donation = new DonationViewModel(course.Donation);
                        EventRunningStatus = Utilities.GetEventRunningStatus(course.RecurrentStartDate.Value, course.RecurrentEndDate, course.TimeZone);
                        CourseType = course.CourseType;
                        courseItemsForDropDown = Courses.Select(item => new SelectListItem()
                        {
                            Text = item.Name,
                            Value = item.Domain,
                            Selected = item.Name == SelectedCourse ? true : false
                        }).ToList();
                        TotalSessions = Ioc.ICourseDbService.CalculateTotalSessions((DateTime)course.RecurrentStartDate,
                            course.RecurrentEndDate, course.RecurrenceTime);
                        NumOfSessionLeft = Ioc.ICourseDbService.CalculateLeftSessions((DateTime)course.RecurrentStartDate,
                            course.RecurrentEndDate, course.RecurrenceTime);
                        HasSessionPrice = course.HasSessionPrice;
                        HasProRatingPrice = course.HasProRatingPrice;
                        SessionPrice = course.SessionPrice;
                        SeedFullPrice(course.HasProRatingPrice, (decimal)course.FullPrice);
                        JbForm = Order.JbForm;
                        #region Code for ChargeDiscount Edit (old)
                        ////var orderCustomDiscounts =
                        ////    Order.OrderItems.Single()
                        ////        .OrderCharges.Where(m => m.Category == ChargeDiscountCategories.Custom)
                        ////        .Select(m => m);

                        //var orderCustomDiscounts =
                        //    Order.OrderCharges.Where(
                        //        m =>
                        //            m.Subcategory == ChargeDiscountSubcategory.Discount &&
                        //            m.Category == ChargeDiscountCategories.Custom).Select(m => m);

                        ////var orderSibling =
                        ////    Order.OrderItems.Single()
                        ////        .Discounts.Where(m => m.Category == ChargeDiscountCategories.Sibling)
                        ////        .SingleOrDefault();

                        //var orderSibling =
                        //    Order.OrderCharges.Where(
                        //        m =>
                        //            m.Subcategory == ChargeDiscountSubcategory.Discount &&
                        //            m.Category == ChargeDiscountCategories.Sibling).SingleOrDefault();

                        //var courseSibling =
                        //    course.ChargeDiscounts.Where(m => m.Category == ChargeDiscountCategories.Sibling)
                        //        .SingleOrDefault();

                        //var courseCustomDiscounts =
                        //    course.ChargeDiscounts.Where(m => m.Category == ChargeDiscountCategories.Custom)
                        //        .Select(m => m);

                        //if (orderCustomDiscounts != null)
                        //{
                        //    foreach (var discount in courseCustomDiscounts)
                        //    {
                        //        if (orderCustomDiscounts.Where(m => m.Amount == discount.Amount && m.Description.Contains(discount.Desc)).Any())
                        //        {
                        //            ChargeDiscountViewModel tmp = new ChargeDiscountViewModel(discount);
                        //            CustomDiscounts.Add(tmp);
                        //        }
                        //        else
                        //        {
                        //            ChargeDiscountViewModel tmp = new ChargeDiscountViewModel(discount, false);
                        //            CustomDiscounts.Add(tmp);
                        //        }
                        //    }
                        //}

                        //if (orderSibling != null)
                        //{
                        //    this.SiblingDiscount.Amount = (int)orderSibling.Amount;
                        //    this.SiblingDiscount.Category = orderSibling.Category;
                        //    //this.SiblingDiscount.ChargeDiscountType = orderSibling.ChargeDiscountType;
                        //    this.SiblingDiscount.Desc = orderSibling.Description;
                        //    var siblingName = orderSibling.Description.Split(',');
                        //    siblingName = siblingName[1].Split(':');
                        //    this.SiblingDiscount.SiblingName =
                        //        siblingName[0].Substring(siblingName[0].IndexOf("sibling") + 8);
                        //    this.SiblingDiscount.IsChecked = true;
                        //}
                        //else if (courseSibling != null)
                        //{
                        //    this.SiblingDiscount.Amount = (int)courseSibling.Amount;
                        //    this.SiblingDiscount.Category = courseSibling.Category;
                        //    this.SiblingDiscount.ChargeDiscountType = courseSibling.ChargeDiscountType;
                        //}
                        #endregion
                        #endregion
                    }
                    break;                
                case SubDomainCategories.NewRegular:
                    {
                        #region new regular
                        SeedChargeDiscounts();
                        Events = Ioc.IEventService.GetClubEvents(ClubDomain, EventTypeCategory.NewRegular);
                        CourseEvent = Events.Single(m => m.Domain == subDomain);
                        Courses = null;
                        var atteributes = JsonConvert.DeserializeObject<EventCourseRegSettings>(CourseEvent.EventAttributesSerialized);
                        Name = CourseEvent.Name;
                        Donation = null;
                        EventRunningStatus = Utilities.GetEventRunningStatus(CourseEvent.EventSchedule.StartDate.Value, CourseEvent.EventSchedule.EndDate, CourseEvent.TimeZone);
                        CourseType = CourseType.NewRegular;

                        courseItemsForDropDown = Events.Select(item => new SelectListItem()
                        {
                            Text = item.Name,
                            Value = item.Domain,
                            Selected = item.Name == SelectedCourse ? true : false
                        })
                        .ToList();

                        TotalSessions = Ioc.ICourseDbService.CalculateTotalSessions((DateTime)CourseEvent.EventSchedule.StartDate,
                            CourseEvent.EventSchedule.EndDate, atteributes.RecurrenceTime);
                        NumOfSessionLeft = Ioc.ICourseDbService.CalculateLeftSessions((DateTime)CourseEvent.EventSchedule.StartDate,
                            CourseEvent.EventSchedule.EndDate, atteributes.RecurrenceTime);
                        HasSessionPrice = atteributes.HasSessionPrice;
                        HasProRatingPrice = atteributes.HasProRatingPrice;
                        SessionPrice = atteributes.SessionPrice;
                        SeedFullPrice(atteributes.HasProRatingPrice, (decimal)atteributes.FullPrice);

                        if (Order.JbForm!=null)
                        {
                            JbForm = Order.JbForm;
                        }
                        #region code for ChargeDiscount Edit (old)
                        //var orderCustomDiscounts =
                        //    Order.OrderItems.Single().Discounts.Where(m => m.Category == ChargeDiscountCategories.Custom).Select(m => m);

                        //var EventCustomDiscount = CourseEvent.ChargeDiscounts.Where(m => m.Category == ChargeDiscountCategories.Custom).Select(m => m);

                        //if (orderCustomDiscounts != null)
                        //{
                        //    foreach (var discount in EventCustomDiscount)
                        //    {
                        //        if (orderCustomDiscounts.Where(m => m.Amount == discount.Amount && m.Desc.Contains(discount.Desc)).Any())
                        //        {
                        //            ChargeDiscountViewModel tmp = new ChargeDiscountViewModel(discount);
                        //            CustomDiscounts.Add(tmp);
                        //        }
                        //        else
                        //        {
                        //            ChargeDiscountViewModel tmp = new ChargeDiscountViewModel(discount, false);
                        //            CustomDiscounts.Add(tmp);
                        //        }

                        //    }
                        //}

                        //var orderSibling =
                        //    Order.OrderItems.Single()
                        //        .Discounts.Where(m => m.Category == ChargeDiscountCategories.Sibling)
                        //        .SingleOrDefault();

                        //var eventSibling =
                        //    CourseEvent.ChargeDiscounts.Where(m => m.Category == ChargeDiscountCategories.Sibling)
                        //        .SingleOrDefault();

                        //if (orderSibling != null)
                        //{
                        //    this.SiblingDiscount.Amount = (int)orderSibling.Amount;
                        //    this.SiblingDiscount.Category = orderSibling.Category;
                        //    this.SiblingDiscount.ChargeDiscountType = orderSibling.ChargeDiscountType;
                        //    this.SiblingDiscount.Desc = orderSibling.Desc;
                        //    var siblingName = orderSibling.Desc.Split(',');
                        //    siblingName = siblingName[1].Split(':');
                        //    this.SiblingDiscount.SiblingName =
                        //        siblingName[0].Substring(siblingName[0].IndexOf("sibling") + 8);
                        //    this.SiblingDiscount.IsChecked = true;
                        //}
                        //else if (eventSibling != null)
                        //{
                        //    this.SiblingDiscount.Amount = (int)eventSibling.Amount;
                        //    this.SiblingDiscount.Category = eventSibling.Category;
                        //    this.SiblingDiscount.ChargeDiscountType = eventSibling.ChargeDiscountType;
                        //}
                        #endregion
                        #endregion
                    }
                    break;
                default:
                    break;
            }
        }

        public EditCourseViewModel(string orderID)
        {
            // TODO: Complete member initialization
            this.orderID = orderID;
        }
        
        void init()
        {
            MembershipDiscount = null;
            SiblingDiscount = null;
        }
        
        public void SeedFullPrice(bool HasProRate, decimal price)
        {
            if (HasProRate && NumOfSessionLeft.HasValue)
            {
                FullPrice = Math.Round((price * NumOfSessionLeft.Value) / TotalSessions);
            }
            else
            {
                FullPrice = price;
            }
        }

        private void SeedChargeDiscounts()
        {
            CustomDiscounts = new List<ChargeDiscountViewModel>();
            //EarlybirdDiscount = new ChargeDiscountViewModel(false, ChargeDiscountCategories.EarlyBird, null, Constants.M_Camp_CD_EarlyBird, ChargeDiscountType.Fixed);
            SiblingDiscount = new SiblingViewModel();
            //MembershipDiscount = new ChargeDiscountViewModel(false, ChargeDiscountCategories.ClubMembership, null, Constants.M_Camp_CD_ClubMembership, ChargeDiscountType.Fixed);
        }
    }
}