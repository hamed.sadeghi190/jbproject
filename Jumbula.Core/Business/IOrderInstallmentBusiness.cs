﻿using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.AutoCharge;
using Jumbula.Core.Model.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IOrderInstallmentBusiness
    {
        List<OrderInstallment> MergeOrderItemInstallments(Order order);
        OrderInstallment Get(long installmentId);
        OrderInstallment GetForAutoCharge(long installmentId);
        IQueryable<OrderInstallment> GetListByToken(string token);
        IQueryable<OrderInstallment> GetListByOrderItemId(long orderItemId);
        IQueryable<OrderInstallment> GetAllInstallmentsWithNoReminder(DateTime installmentDate);
        IQueryable<OrderInstallment> GetAllUnpaidInstallmentsByDate(DateTime installmentDate);
        string GetInstallmentPaymentMode(OrderInstallment orderInstallment);
        OperationStatus Update(OrderInstallment orderInstallment);
        OperationStatus Create(OrderInstallment orderInstallment);

        Dictionary<int, IEnumerable<long>> GetAllInstallmentsByDueDate(DateTime dueDate);
        List<ClubInstallmentPair> GetAllClubInstallmentPairByDueDate(DateTime dueDate);

        bool HasOrderItemPastDueDate(OrderItem orderItem, DateTime installmentDate);

        void CalculateInstallments(Order order, bool isStripeEnabled);
        OperationStatus Delete(List<long> ids);

        OperationStatus Update(List<OrderInstallment> orderInstallments);

        OperationStatus Create(List<OrderInstallment> orderInstallments);
        IQueryable<OrderInstallment> GetAllAutoChargeInstallmentsWithNoReminder(DateTime installmentDate);
        IQueryable<OrderInstallment> GetAllAutoChargeInstallments(DateTime installmentDate);
        IQueryable<OrderInstallment> GetAllInstallmentsByDate(DateTime? startDate, DateTime? endDate, int? clubId);

        int CountInstallmentParentNotPaid(IQueryable<OrderItem> orderItems);
        OperationStatus DeleteInstallments(List<OrderInstallment> orderInstallments);
        void SetStatusDeletedInstallment(List<OrderInstallment> orderInstallments, DeleteReason reason);
        ProgramSubscriptionItem GetScheduleInstallment(long installmentId);
        string GetSchedulePartInstallment(long installmentId, long programId);
        IQueryable<OrderInstallment> GetAllInstallments(DateTime? startDate, DateTime? endDate);
        List<OrderInstallment> TotalInstallments(Order order);
        OperationStatus UndoFailedInstallment(long id);

        IQueryable<OrderInstallment> GetOrderItemInstallments(IQueryable<OrderItem> orderOrderItems);

        dynamic UpdateInstallments(List<OrderInstallment> orderInstallments);
        IQueryable<OrderInstallment> GetInstallmentOfTransactionDate(DateTime? start, DateTime? end, IQueryable<OrderInstallment> pastInstallments);
        string GetInstallmentStatus(OrderInstallment installment);
        IQueryable<OrderInstallment> GetInstallmentsByStatus(string status, IQueryable<OrderInstallment> pastInstallments);
        IQueryable<OrderInstallment> GetInstallmentsByMode(string mode, IQueryable<OrderInstallment> installments);
        IQueryable<OrderInstallment> GetInstallmentsByParticipantInfo(PeopleSearchType? searchType, IQueryable<OrderInstallment> installments, string term, string customeStripeErrorMessage, string firstName, string lastName);
        IQueryable<TransactionActivity> GetInstallmentTransactions(IQueryable<OrderInstallment> installments);
        IQueryable<OrderInstallment> GetInstallmentsByCustomeErrorMessage(string stripeErrorMessage, IQueryable<OrderInstallment> installments);
        IQueryable<OrderInstallment> GetInstallmentsByErrorMessage(string customeStripeErrorMessage, string term, IQueryable<OrderInstallment> installments);
        List<SelectListItem> GetStripeErrorMessage();
        IEnumerable<OrderInstallment> GetCalendarOrderItemInstallments(IEnumerable<OrderItem> orderItem);
        bool CheckIsPayable(OrderInstallment installment);
        PaginatedList<ParentOrderItemInstallmentsViewModel> GetOrderItemInstallments(long orderItemId, List<Sort> sorts, int pageIndex, int pageSize, RoleCategoryType roleType, InstallmentType? installmentType);
        OrderInstallment CreateAddOnInstallment(decimal amount);
        OrderInstallment GetInstallmentToBeRefund(OrderItemsViewModel model);
        decimal GetAmountAutomaticallyTransactions(OrderInstallment installment);
        RefundResultModel Refund(OrderItemsViewModel model, ref decimal refundedAmount);
        List<RefundInformation> GetRefundModes();
        IQueryable<OrderInstallment> GetOrderInstallments(List<long> orderInstallmentIds);
        IQueryable<OrderInstallment> GetDelinquentInstallmentItemsAsNoTracking(int clubId, int autoChargeFailAttemptNumber);
    }
}
