﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using NPOI.HSSF.Model;
using RestSharp.Extensions;
using SportsClub.Domain;
using SportsClub.Validations;
using SportsClub.Db;
using Microsoft.Practices.Unity;
using System.ComponentModel;
using System.Web.Mvc;
using System.Collections.ObjectModel;
using System.Web.Helpers;
using SportsClub.Infrastructure;
using System.Text.RegularExpressions;
using SportsClub.Infrastructure.Helper;
using SportsClub.Infrastructure.JBMembership;
using SportsClub.Infrastructure.Validation;
using Jb.Framework.Common.Forms;
using SportsClub.Models.FormModels;
using Jb.Framework.Common.Model;

namespace SportsClub.Models
{
    #region SeperatedSteps

    public class ChessBaseCreateEditModel
    {
        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        public PageMode WizardMode { get; set; }

        public PageMode PageMode { get; set; }

        [DefaultValue(EventPageStep.BasicInformation)]
        public EventPageStep EventPageStep { get; set; }

        [Required]
        [DisplayName("Time zone")]
        public SportsClub.Domain.TimeZone TimeZone { get; set; }

        public SaveType SaveType { get; set; }

        public DateTime DateUpdated { get; set; }

        public EventPageStep LastCreatedPage { get; set; }

        public string ActionName
        {
            get
            {
                return HttpContext.Current.Request.RequestContext.RouteData.GetRequiredString("action");
            }
        }

        public string ControllerName
        {
            get
            {
                return HttpContext.Current.Request.RequestContext.RouteData.GetRequiredString("controller");
            }
        }

        #region Constructors

        public ChessBaseCreateEditModel()
        {
            ClubDomain = JBMembership.GetActiveClubBaseInfo().Domain;
        }

        public ChessBaseCreateEditModel(string domain)
            : this()
        {
            Domain = domain;
        }

        #endregion
    }

    public class ChessBasicInformationEditModel : ChessBaseCreateEditModel
    {
        [Required]
        [DisplayName("Tournament name")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        [MinLength(5, ErrorMessage = "Enter a complete {0}")]
        public string Name { get; set; }

        [Required]
        [Range(1, 3, ErrorMessage = "Tournament type should be selected.")]
        [DisplayName("Tournament type")]
        public TourneyRewardType TourneyType { get; set; }

        [DisplayName("Subtitle 1")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string SubTitle1 { get; set; }

        [DisplayName("Subtitle 2")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string SubTitle2 { get; set; }

        [Required]
        [DisplayName("Chief tournament director")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(5, ErrorMessage = "Enter a complete {0}")]
        public string ChiefTD { get; set; }

        [Required]
        [DisplayName("Chief organizer")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(5, ErrorMessage = "Enter a complete {0}")]
        public string ChiefOrganizer { get; set; }

        [Required]
        [DisplayName("Is this location a hotel?")]
        public bool HotelChecked { get; set; }

        [DisplayName("Hotel rate")]
        [Range(Constants.HotelFee_Min, Constants.HotelFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? HotelRate { get; set; }

        [DisplayName("Discount code")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        public string HotelRateCode { get; set; }

        public ClubLocationViewModel ClubLocationViewModel { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public int ClubLocationId { get; set; }

        public DisplayClubLocationViewModel DisplayClubLocationViewModel { get; set; }

        public string LocationName { get; set; }

        public string FileName { get; set; }

        [DisplayName("Upload an existing flyer")]
        public FlyerType FlyerType { get; set; }

        public SelectList Ages { get; set; }

        [DisplayName("Outsourcer")]
        public string OutSourcerClubDomain { get; set; }

        public IEnumerable<OutSourcerList> OutSourcers { get; set; }

        public SelectList OutSourcerSelectList { get; set; }

        public int ClubId { get; set; }

        public bool IsVendor { get; set; }

        [DisplayName("Is this an outsourcer event for a school or club?")]
        public EventSponserType IsVendorEvent { get; set; }

        public int OutSourcerClubLocationId { get; set; }

        public string ClubLogo { get; set; }

        public EventSearchField EventSearchField { get; set; }

        public DateTime DateCreated { get; set; }

        public Tourney ToTourney(PageMode pageMode)
        {
            Tourney tourney = new Tourney();

            var clubLoc = Ioc.IClubLocationDb.GetClubLocation(DisplayClubLocationViewModel.SelectedClubLocationId.HasValue ? DisplayClubLocationViewModel.SelectedClubLocationId.Value : ClubLocationId);
            tourney.ClubLocation = clubLoc;

            // Address and Geo
            tourney.Geography = Utilities.NewDbGeography(clubLoc.PostalAddress.Lat, clubLoc.PostalAddress.Lng);

            PostalAddress = clubLoc.PostalAddress;

            tourney.TimeZone = TimeZoneHelper.GetTimeZone(clubLoc.PostalAddress.Lat, clubLoc.PostalAddress.Lng);

            tourney.FlyerType = FlyerType;
            tourney.FileName = FileName;
            tourney.Name = Name;
            tourney.SubTitle1 = SubTitle1;
            tourney.SubTitle2 = SubTitle2;
            tourney.ChiefTD = ChiefTD;
            tourney.Domain = Domain;

            tourney.ChiefOrganizer = ChiefOrganizer;

            tourney.TourneyType = TourneyType;

            tourney.ItemId = Utilities.GenerateItemId();
            tourney.EventStatus = EventStatusCategories.open;

            tourney.MetaData = new MetaData { DateCreated = DateTime.Now, DateUpdated = DateTime.Now };
            tourney.CategoryId = CategoryModel.ChessId;

            tourney.OutSourcerClubDomain = IsVendorEvent == EventSponserType.OutSourcer ? OutSourcerClubDomain : null;

            // hotel
            // Note: Hotel can be selected but it is ok that no hotel rate may be present
            if (HotelRate.HasValue) // no hotel rate, set optional param
            {
                tourney.Hotel = new Hotel { Checked = HotelChecked, Rate = HotelRate.Value, RateCode = HotelRateCode };
            }
            else // no rate
            {
                tourney.Hotel = new Hotel { Checked = HotelChecked, Rate = -1, RateCode = HotelRateCode };
            }

            // Note
            // Take the charge/discount/note categories from the ChargeCategories/DiscountCategories, not postback values in the model
            // For lists, the model builder does NOT re-create the categories b/c they were not present in the view


            // Assign hidden fields
            // Note: No need for ClubLogo

            tourney.ClubDomain = ClubDomain;

            // System fields
            // Note that Domain is null within create tourney, but is valid durig edit

            tourney.EventSearchField = new EventSearchField
            {
                MinAge = EventSearchField.MinAge,
                MaxAge = EventSearchField.MaxAge,
                MinGrade = EventSearchField.MinGrade.HasValue && EventSearchField.MinGrade.Value != SchoolGradeType.None ? EventSearchField.MinGrade : null,
                MaxGrade = EventSearchField.MaxGrade.HasValue && EventSearchField.MaxGrade.Value != SchoolGradeType.None ? EventSearchField.MaxGrade : null,
                Level = EventSearchField.Level.HasValue && EventSearchField.Level.Value != Level.None ? EventSearchField.Level : null,
                Gender = EventSearchField.Gender.HasValue ? EventSearchField.Gender : null,
            };

            if (pageMode == Models.PageMode.Create)
            {
                tourney.EF = new EntryFee();

                tourney.Prize = new Prize();

                tourney.Bye = new Bye();

                tourney.EventSearchTags = new EventSearchTags();

                tourney.RegisterDeadLine = new RegisterDeadLine();
                //tourney.RegisterDeadLine.StartRegisterDeadLineDate = DateTime.Now;
                tourney.LastCreatedPage = EventPageStep.BasicInformation;

            }

            return tourney;
        }

        #region Internals

        private void SeedOUtSourcers()
        {
            var outSourcers = Ioc.IClubVendorDb.GetSchoolsForClub(ClubId);
            OutSourcers = outSourcers.Select(m => new OutSourcerList
            {
                OutSourcerClubDomain = m.OutSourcerClubDomain,
                OutSourcerClubName = m.OutSourcerLocation.PostalAddress.Address + "(" + m.OutSourcerClubName + ")",
                OutSourcerLocation = m.OutSourcerLocation,

            }).ToList();
            OutSourcerSelectList = new SelectList(OutSourcers, "OutSourcerClubDomain", "OutSourcerClubName");

        }

        public void SeedAges()
        {
            var ageList = new List<SelectListItem>();
            for (int i = 1; i <= 120; i++)
            {
                ageList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            Ages = new SelectList(ageList, "Value", "Text");
        }

        public void SeedClubLocation()
        {
            DisplayClubLocationViewModel.ClubLocations = Ioc.IClubLocationDb.GetClubLocations(ClubDomain);
        }

        public void Seed()
        {
            if (this.DisplayClubLocationViewModel.ClubLocations == null)
            {
                SeedClubLocation();
            }
        }

        #endregion

        public ChessBasicInformationEditModel()
            : base()
        {
            EventSearchField = new EventSearchField();
        }

        public ChessBasicInformationEditModel(string domain)
            : base(domain)
        {
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(ClubDomain);

            Seed();
            SeedAges();
        }


        public ChessBasicInformationEditModel(Tourney tourney)
            : this(tourney.Domain)
        {
            var club = JBMembership.GetActiveClubBaseInfo();
            // set event for a special outsourcer
            LastCreatedPage = tourney.LastCreatedPage;
            ClubId = club.Id;
            //IsVendor = isVendor;
            IsVendorEvent = string.IsNullOrEmpty(tourney.OutSourcerClubDomain) ? EventSponserType.Vendor : EventSponserType.OutSourcer;
            OutSourcerClubDomain = tourney.OutSourcerClubDomain;
            SeedOUtSourcers();

            Name = tourney.Name;
            SubTitle1 = tourney.SubTitle1;
            SubTitle2 = tourney.SubTitle2;
            ChiefTD = tourney.ChiefTD;
            ChiefOrganizer = tourney.ChiefOrganizer;

            PostalAddress = tourney.ClubLocation.PostalAddress;
            HotelChecked = tourney.Hotel.Checked;
            TimeZone = tourney.TimeZone;
            //LocationName = tourney.AddressName;
            FlyerType = tourney.FlyerType;
            FileName = tourney.FileName;
            TourneyType = tourney.TourneyType;

            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(tourney.ClubDomain)
            {
                SelectedClubLocationId = tourney.ClubLocation.Id
            };

            if (tourney.Hotel.Rate != -1 && tourney.Hotel.Rate.HasValue)
            {
                HotelRate = (int)tourney.Hotel.Rate;
            }
            else // leave it blank
            {
            }

            EventSearchField = new EventSearchField
            {
                Gender = tourney.EventSearchField.Gender,
                Level = tourney.EventSearchField.Level,
                MaxAge = tourney.EventSearchField.MaxAge,
                MinAge = tourney.EventSearchField.MinAge,
                MaxGrade = tourney.EventSearchField.MaxGrade == SchoolGradeType.College || tourney.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool ? SchoolGradeType.None : tourney.EventSearchField.MaxGrade,
                MinGrade = tourney.EventSearchField.MinGrade == SchoolGradeType.College || tourney.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool ? SchoolGradeType.None : tourney.EventSearchField.MinGrade,

            };

            HotelRateCode = tourney.Hotel.RateCode;

        }

        //IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        //{
        //    foreach (var result in base.Validate(validationContext))
        //    {
        //    }

        //    yield return new ValidationResult("");
        //}
    }

    public class ChessRegSettingsEditModel : ChessBaseCreateEditModel
    {
        public TourneyRewardType TourneyType { get; set; }

        [DisplayName("Prize fund")]
        // [Range(Constants.Tourney_PrizeFund_Min, Constants.Tourney_PrizeFund_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? PrizeFund { get; set; }

        // [Required]
        [DisplayName("Number of full entries")]
        // [Range(Constants.Tourney_PrizeFundBased_Min, Constants.Tourney_PrizeFundBased_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? PrizeFundBasedOn { get; set; }

        [DisplayName("Prize fund guarantee")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        public string PrizeFundGuarantee { get; set; }

        [DisplayName("Trophies")]
        [AllowHtml]
        public string TrophyInfo { get; set; }

        //[Required]
        public ICollection<SectionViewModel> Sections { get; set; }

        [Required]
        public ICollection<NoteViewModel> PrizeNotes { get; set; }

        //[Required]
        public ICollection<ScheduleViewModel> Schedules { get; set; }

        [DisplayName("Online fee")]
        // [Range(Constants.Tourney_EntryFee_Min, Constants.Tourney_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? OnLineEF { get; set; }

        [Required]
        [DisplayName("Online reg deadline")]
        [DataType(DataType.Date)]
        public DateTime? OnLineEFDeadline { get; set; }

        [Required]
        [DisplayName("First late charge?")]
        public bool OnLineEF1Checked { get; set; }

        [DisplayName("After")]
        [DataType(DataType.Date)]
        public DateTime? OnLineEF1After { get; set; }

        [DisplayName("Total entry fee")]
        // [Range(Constants.Tourney_EntryFee_Min, Constants.Tourney_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? OnLineEF1 { get; set; }

        [Required]
        [DisplayName("Second late charge?")]
        public bool OnLineEF2Checked { get; set; }

        [DisplayName("After")]
        [DataType(DataType.Date)]
        public DateTime? OnLineEF2After { get; set; }

        [DisplayName("Total entry fee")]
        //[Range(Constants.Tourney_EntryFee_Min, Constants.Tourney_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? OnLineEF2 { get; set; }

        // [Required]
        [DisplayName("Onsite fee")]
        //[Range(Constants.Tourney_EntryFee_Min, Constants.Tourney_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? OnSiteEF { get; set; }

        [DisplayName("Max # of byes")]
        [Range(0, int.MaxValue, ErrorMessage = "{0} must be a positive number or zero")]
        public int? ByeMax { get; set; }

        [DisplayName("Number of rounds in tourney")]
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be a positive number")]
        public int? ByeRounds { get; set; }

        [DisplayName("Last round to request a bye")]
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be a positive number")]
        public int? ByeCommitBy { get; set; }

        [DisplayName("Bye note")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        public string ByeNote { get; set; }

        [Required]
        [DisplayName("GM/IM plays free?")]
        public ChargeDiscountViewModel GmImDis { get; set; }

        [Required]
        [DisplayName("Economy option?")]
        public ChargeDiscountViewModel EconDis { get; set; }

        [Required]
        [DisplayName("Senior discount?")]
        public ChargeDiscountViewModel SeniorDis { get; set; }

        [Required]
        [DisplayName("Play-up option?")]
        public ChargeDiscountViewModel Playup { get; set; }

        [Required]
        [DisplayName("Re-entry option?")]
        public ChargeDiscountViewModel Reentry { get; set; }

        public RegisterDeadLine RegisterDeadLine { get; set; }

        public List<CustomDiscountViewModel> CustomDiscounts { get; set; }

        public DonationViewModel Donation { get; set; }

        [Required]
        [DisplayName("Allow players to request USCF membership during registration?")]
        public bool UscfMembershipChecked { get; set; }

        //[Required]
        [DisplayName("Provide USCF membership?")]
        public ICollection<ChargeDiscountViewModel> UscfMembership { get; set; }

        public RestrictedViewModel Restricted { get; set; }


        #region Vlaidations

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    if (UscfMembershipChecked && UscfMembership == null || (UscfMembership != null && !UscfMembership.Any()))
        //    {
        //        yield return new ValidationResult("You must add Ucf membership.", new[] { "UscfMembership" });
        //    }
        //}

        #endregion

        #region Internals
        private void SeedChargeDiscountOptions()
        {
            GmImDis = new ChargeDiscountViewModel(false, ChargeDiscountCategories.GmIm, null, string.Empty, ChargeDiscountType.Fixed);
            EconDis = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Econ, null, string.Empty, ChargeDiscountType.Fixed);
            SeniorDis = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Senior, null, string.Empty, ChargeDiscountType.Fixed);
            Playup = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Playup, null, string.Empty, ChargeDiscountType.Fixed);
            Reentry = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Reentry, null, string.Empty, ChargeDiscountType.Fixed);
        }

        private void SeedUscfMembership()
        {
            ChargeDiscountViewModel m1 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.UscfMem, Desc = string.Empty };
            ChargeDiscountViewModel m2 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.UscfMem, Desc = string.Empty };
            UscfMembership.Add(m1);
            UscfMembership.Add(m2);
        }

        #endregion

        public Tourney ToTourney()
        {
            Tourney t = new Tourney();

            if (!ByeMax.HasValue || ByeMax == 0) // no bye options, set optional params ByeRounds and ByeNote, they are undefined
            {
                t.Bye = new Bye { IsDefined = false, Max = 0, Rounds = -1, CommitBy = -1, Note = string.Empty };
            }
            else
            {
                t.Bye = new Bye { IsDefined = true, Max = ByeMax.Value, Rounds = ByeRounds.Value, CommitBy = ByeCommitBy.Value, Note = ByeNote };
            }
            if (ByeRounds.HasValue && ByeRounds.Value > 0)
            {
                t.Bye.Rounds = ByeRounds.Value;
            }

            t.CategoryId = CategoryModel.ChessId;

            if (Playup.IsChecked == true)
            {
                //ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.Playup, Amount = Playup.Amount.Value, Desc = Playup.Desc.Contains("$") ? Playup.Desc : Playup.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(Playup.Amount.Value) };
                ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.Playup, Amount = Playup.Amount.Value, Desc = Playup.Desc };
                t.ChargeDiscounts.Add(c);
            }

            if (Reentry.IsChecked == true)
            {
                //ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.Reentry, Amount = Reentry.Amount.Value, Desc = Reentry.Desc.Contains("$") ? Reentry.Desc : Reentry.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(Reentry.Amount.Value) };
                ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.Reentry, Amount = Reentry.Amount.Value, Desc = Reentry.Desc };
                t.ChargeDiscounts.Add(c);
            }

            if (UscfMembershipChecked == true)
            {
                foreach (ChargeDiscountViewModel cvm in UscfMembership)
                {
                    //ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.UscfMem, Amount = cvm.Amount.Value, Desc = cvm.Desc.Contains("$") ? cvm.Desc : cvm.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(cvm.Amount.Value) };
                    ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.UscfMem, Amount = cvm.Amount.Value, Desc = cvm.Desc };
                    t.ChargeDiscounts.Add(c);
                }
            }

            if (GmImDis.IsChecked == true)
            {
                //ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.GmIm, Amount = GmImDis.Amount.Value, Desc = GmImDis.Desc.Contains("$") ? GmImDis.Desc : GmImDis.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(GmImDis.Amount.Value) };
                ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.GmIm, Amount = GmImDis.Amount.Value, Desc = GmImDis.Desc };
                t.ChargeDiscounts.Add(d);
            }

            if (EconDis.IsChecked == true)
            {
                //ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.Econ, Amount = EconDis.Amount.Value, Desc = EconDis.Desc.Contains("$") ? EconDis.Desc : EconDis.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(EconDis.Amount.Value) };
                ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.Econ, Amount = EconDis.Amount.Value, Desc = EconDis.Desc };
                t.ChargeDiscounts.Add(d);
            }

            if (SeniorDis.IsChecked == true)
            {
                //ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.Senior, Amount = SeniorDis.Amount.Value, Desc = SeniorDis.Desc.Contains("$") ? SeniorDis.Desc : SeniorDis.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(SeniorDis.Amount.Value) };
                ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.Senior, Amount = SeniorDis.Amount.Value, Desc = SeniorDis.Desc };
                t.ChargeDiscounts.Add(d);
            }

            t.Prize = new Prize { Fund = PrizeFund.HasValue ? PrizeFund.Value : 0, BasedOn = PrizeFundBasedOn.HasValue ? PrizeFundBasedOn.Value : 0, Guarantee = PrizeFundGuarantee };
            t.TrophyInfo = TrophyInfo;

            if (OnLineEF1Checked == false) // no first late fee, set optional params, they are undefined
            {
                OnLineEF1 = -1;
                OnLineEF1After = DateTime.Today;
            }

            if (OnLineEF2Checked == false) // no second late fee, set optional params, they are undefined
            {
                OnLineEF2 = -1;
                OnLineEF2After = DateTime.Today;
            }

            t.EF = new EntryFee
            {
                OnLine = OnLineEF.Value,
                OnLineDeadline = OnLineEFDeadline.Value,
                OnLine1Checked = OnLineEF1Checked,
                OnLine1 = OnLineEF1.Value,
                OnLine1After = OnLineEF1After.Value,
                OnLine2Checked = OnLineEF2Checked,
                OnLine2 = OnLineEF2.Value,
                OnLine2After = OnLineEF2After.Value,
                OnSite = (OnSiteEF.HasValue) ? OnSiteEF.Value : (decimal?)null
            };

            if (CustomDiscounts != null)
            {
                foreach (var discount in CustomDiscounts)
                {
                    ChargeDiscount customDiscount = null;
                    if (discount.Discount.ChargeDiscountType == ChargeDiscountType.Fixed)
                    {
                        customDiscount = new ChargeDiscount
                        {
                            Category = ChargeDiscountCategories.Custom,
                            Amount = (decimal)discount.Discount.Amount,
                            Desc = discount.Discount.Desc,
                            ChargeDiscountType = discount.Discount.ChargeDiscountType
                        };
                    }
                    else
                    {
                        customDiscount = new ChargeDiscount
                        {
                            Category = ChargeDiscountCategories.Custom,
                            Amount = (decimal)discount.Discount.Amount,
                            Desc = discount.Discount.Desc,
                            ChargeDiscountType = discount.Discount.ChargeDiscountType
                        };
                    }

                    t.ChargeDiscounts.Add(customDiscount);
                }
            }

            if (Restricted.IsRestricted && Restricted.Users != null && Restricted.Users.Count > 0)
            {
                t.RegistrationMode = RegistrationMode.Restricted;
            }
            else
            {
                t.RegistrationMode = RegistrationMode.Open;
            }

            if (Donation.Checked)
            {
                t.Donation.Checked = Donation.Checked;
                t.Donation.Title = Donation.Title;
                t.Donation.Description = Donation.Description;
            }

            if (PrizeNotes != null)
            {
                foreach (NoteViewModel nvm in PrizeNotes)
                {
                    Note n = new Note { Category = NoteCategories.Prize, Desc = nvm.Desc };
                    t.Notes.Add(n);
                }
            }
            if (Sections != null)
            {
                foreach (SectionViewModel vm in Sections)
                {
                    TourneySection s = new TourneySection { Name = vm.Name, Rating = vm.Rating, Prize = vm.Prize };
                    t.Sections.Add(s);
                }
            }

            const string Or = " or ";
            string dates = string.Empty;

            for (int i = 0; i < Schedules.Count; ++i)
            {
                var vm = Schedules.ElementAt(i);
                TourneySchedule s = new TourneySchedule { Start = vm.Start.Value, End = vm.End.Value, Games = vm.Games, RegOnSite = vm.RegOnSite, TimeControl = vm.TimeControl, Name = vm.GetSchName(), ScheduleLabel = vm.ScheduleLabel };
                t.Schedules.Add(s);

                // Setup the diplay date
                //

                dates = vm.GetSchMonDayName();

                if (i < Schedules.Count - 1)
                {
                    dates += Or;
                }
                else
                {
                    dates += Schedules.ElementAt(Schedules.Count - 1).End.Value.ToString(" yyyy");
                }
            }

            if (PageMode == PageMode.Create && Utilities.CompareEnumValues<EventPageStep>(EventPageStep.RegSettings, LastCreatedPage) != -1)
            {
                t.LastCreatedPage = SportsClub.Domain.EventPageStep.RegSettings;
            }
            else
            {
                t.LastCreatedPage = LastCreatedPage;
            }

            t.RegisterDeadLine = this.RegisterDeadLine;

            return t;
        }

        #region Constructors

        public ChessRegSettingsEditModel()
            : base()
        {

        }

        public ChessRegSettingsEditModel(string domain)
            : base(domain)
        {
            Donation = new DonationViewModel();
            OnLineEF1Checked = false;
            OnLineEF2Checked = false;
            UscfMembershipChecked = false;
            UscfMembership = new HashSet<ChargeDiscountViewModel>();
            Sections = new HashSet<SectionViewModel>();
            PrizeNotes = new HashSet<NoteViewModel>();
            Schedules = new HashSet<ScheduleViewModel>();
            RegisterDeadLine = new RegisterDeadLine();
            CustomDiscounts = new List<CustomDiscountViewModel>();
            LastCreatedPage = EventPageStep.RegSettings;
            Restricted = new RestrictedViewModel(ClubDomain);
        }

        public ChessRegSettingsEditModel(Tourney tourney)
            : this(tourney.Domain)
        {
            //TemplateName = tourney.TemplateName;

            //OfflineRegChecked = tourney.OfflineRegChecked;
            //ExternalRegUrl = tourney.ExternalRegUrl;

            //Donation
            LastCreatedPage = tourney.LastCreatedPage;
            Donation.Checked = tourney.Donation.Checked;
            Donation.Title = tourney.Donation.Title;
            Donation.Description = tourney.Donation.Description;
            TrophyInfo = tourney.TrophyInfo;
            //AsTemplate = !fromTemplate && tourney.AsTemplate;
            //FromTemplate = fromTemplate;

            PrizeFund = tourney.Prize.Fund;
            PrizeFundBasedOn = tourney.Prize.BasedOn;
            PrizeFundGuarantee = tourney.Prize.Guarantee;
            OnLineEF = (int)tourney.EF.OnLine;
            OnLineEFDeadline = tourney.EF.OnLineDeadline;
            OnLineEF1Checked = tourney.EF.OnLine1Checked;

            RegisterDeadLine = tourney.RegisterDeadLine;

            if (tourney.EF.OnLine1 != -1)
            {
                OnLineEF1 = (int)tourney.EF.OnLine1;
            }

            OnLineEF1After = tourney.EF.OnLine1After;
            OnLineEF2Checked = tourney.EF.OnLine2Checked;

            if (tourney.EF.OnLine2 != -1)
            {
                OnLineEF2 = (int)tourney.EF.OnLine2;
            }

            OnLineEF2After = tourney.EF.OnLine2After;
            OnSiteEF = (tourney.EF.OnSite.HasValue) ? (int)tourney.EF.OnSite.Value : (int?)null;
            ByeMax = tourney.Bye.Max;

            if (tourney.Bye.Rounds != -1)
            {
                ByeRounds = tourney.Bye.Rounds;
            }

            if (tourney.Bye.CommitBy != -1)
            {
                ByeCommitBy = tourney.Bye.CommitBy;
            }

            ByeNote = tourney.Bye.Note;

            // Rebuild the optional charge and discounts
            // Call the seed method first to set up default values
            // The foreach statement overrides the default values if they exist in db

            SeedChargeDiscountOptions();
            TourneyType = tourney.TourneyType;

            foreach (ChargeDiscount item in tourney.ChargeDiscounts)
            {
                switch (item.Category)
                {
                    case ChargeDiscountCategories.Playup:
                        Playup = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Reentry:
                        Reentry = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.UscfMem:
                        UscfMembership.Add(new ChargeDiscountViewModel(item));
                        break;

                    case ChargeDiscountCategories.GmIm:
                        GmImDis = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Econ:
                        EconDis = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Senior:
                        SeniorDis = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Custom:
                        {
                            CustomDiscounts.Add(new CustomDiscountViewModel
                            {
                                Discount = new ChargeDiscountViewModel
                                {
                                    IsChecked = true,
                                    Amount = (int)item.Amount,
                                    ChargeDiscountType = item.ChargeDiscountType,
                                    Desc = item.Desc
                                }
                            });
                            break;
                        }

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_ChargeDiscountCategoryNotSupported, item.Category));
                }
            }

            if (UscfMembership.Count == 0) // db did not contain membership, use default
            {
                SeedUscfMembership();
            }
            else
            {
                UscfMembershipChecked = true;
            }

            foreach (TourneySection item in tourney.Sections)
            {
                Sections.Add(new SectionViewModel(item));
            }

            foreach (Note item in tourney.Notes)
            {
                PrizeNotes.Add(new NoteViewModel(item));
            }

            foreach (TourneySchedule item in tourney.Schedules)
            {
                Schedules.Add(new ScheduleViewModel(item));
            }

            // check for restricted Mode

            if (tourney.RegistrationMode== RegistrationMode.Restricted)
            {
                var selectedUsers = tourney.RestrictedUsers.ToList();

                Restricted.Users = new List<string>();
                foreach (var restricted in selectedUsers)
                {
                    Restricted.Users.Add(restricted.Id.ToString());
                }

                Restricted.IsRestricted = true;
            }

            // Hidden fields
            // Note: clublogo will set in controller, does not exist in tourney db

            ClubDomain = tourney.ClubDomain;
            //EventSearchTags = tourney.EventSearchTags;
            Domain = tourney.Domain;
        }

        #endregion
    }

    public class ChessAdditionalInfoEditModel : ChessBaseCreateEditModel
    {
        public JbForm JbForm { get; set; }

        public Tourney ToTourney()
        {
            Tourney tourney = new Tourney();

            if (PageMode == PageMode.Create && Utilities.CompareEnumValues<EventPageStep>(EventPageStep.AdditionalInfo, LastCreatedPage) != -1)
            {
                tourney.LastCreatedPage = SportsClub.Domain.EventPageStep.AdditionalInfo;
            }
            else
            {
                tourney.LastCreatedPage = LastCreatedPage;
            }

            return tourney;
        }

        public ChessAdditionalInfoEditModel()
            : base()
        {
            //AssignForm = new AssignFormViewModel();
        }

        public ChessAdditionalInfoEditModel(string domain)
            : base(domain)
        {
            LastCreatedPage = EventPageStep.AdditionalInfo;

            //var forms = DependencyResolverHelper.IFormsDbService.GetForms(this.ClubDomain, EventStatusCategories.open);
            //AssignForm = new AssignFormViewModel() { AllForms = new SelectList(forms, "Id", "Name") };

            //AssignForm.Forms = forms.Select(x => new FormListViewModel
            //{
            //    Id = x.Id,
            //    Name = x.Name
            //}).ToList();
        }

        public ChessAdditionalInfoEditModel(Tourney tourney)
            : this(tourney.Domain)
        {
            LastCreatedPage = tourney.LastCreatedPage;
        }
    }

    public class ChessLastPageEditModel : ChessBaseCreateEditModel
    {
        [Required]
        [ForceTrue]
        [DisplayName("I understand that I should double check the system generated PDF file and the tournament registration page.")]
        public bool IUnderstandChecked { get; set; }

        public bool IsFinished { get; set; }


        public Tourney ToTourney()
        {
            Tourney tourney = new Tourney();


            SaveType = SportsClub.Domain.SaveType.Publish;
            if (PageMode == PageMode.Create && Utilities.CompareEnumValues<EventPageStep>(EventPageStep.Lastpage, LastCreatedPage) != -1)
            {
                tourney.LastCreatedPage = SportsClub.Domain.EventPageStep.Lastpage;
            }
            else
            {
                tourney.LastCreatedPage = LastCreatedPage;
            }

            return tourney;
        }

        public ChessLastPageEditModel()
            : base()
        {

        }

        public ChessLastPageEditModel(string domain)
            : base(domain)
        {
            LastCreatedPage = SportsClub.Domain.EventPageStep.Lastpage;
        }

        public ChessLastPageEditModel(Tourney tourney)
            : this(tourney.Domain)
        {
            LastCreatedPage = tourney.LastCreatedPage;
        }
    }

    #endregion

    public class CreateEditChessTourneyViewModel
    {
        [Required]
        [DisplayName("Tournament name")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        [MinLength(5, ErrorMessage = "Enter a complete {0}")]
        public string Name { get; set; }

        [DisplayName("Subtitle 1")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string SubTitle1 { get; set; }

        [DisplayName("Subtitle 2")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string SubTitle2 { get; set; }


        //[Required]
        [DisplayName("Chief tournament director")]
        //  [StringLength(64, ErrorMessage = "{0} is too long")]
        // [MinLength(5, ErrorMessage = "Enter a complete {0}")]
        public string ChiefTD { get; set; }

        //[Required]
        [DisplayName("Chief organizer")]
        //[StringLength(64, ErrorMessage = "{0} is too long")]
        // [MinLength(5, ErrorMessage = "Enter a complete {0}")]
        public string ChiefOrganizer { get; set; }

        [Required]
        [DisplayName("Is this location a hotel?")]
        public bool HotelChecked { get; set; }

        [DisplayName("Hotel rate")]
        [Range(Constants.HotelFee_Min, Constants.HotelFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? HotelRate { get; set; }

        [DisplayName("Discount code")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        public string HotelRateCode { get; set; }

        [StringLength(128, ErrorMessage = "{0} is too long")]
        [DisplayName("External web site Url")]
        public string ExternalRegUrl { get; set; }


        [DefaultValue(EventPageStep.BasicInformation)]
        public EventPageStep CurrentStep { get; set; }

        [Required]
        [DisplayName("Time zone")]
        public SportsClub.Domain.TimeZone TimeZone { get; set; }

        //[Required]
        [DisplayName("Prize fund")]
        // [Range(Constants.Tourney_PrizeFund_Min, Constants.Tourney_PrizeFund_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? PrizeFund { get; set; }

        // [Required]
        [DisplayName("Number of full entries")]
        // [Range(Constants.Tourney_PrizeFundBased_Min, Constants.Tourney_PrizeFundBased_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? PrizeFundBasedOn { get; set; }

        [DisplayName("Prize fund guarantee")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        public string PrizeFundGuarantee { get; set; }

        // [Required]
        [DisplayName("Online fee")]
        // [Range(Constants.Tourney_EntryFee_Min, Constants.Tourney_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? OnLineEF { get; set; }

        [Required]
        [DisplayName("Online reg deadline")]
        [DataType(DataType.Date)]
        public DateTime? OnLineEFDeadline { get; set; }

        [Required]
        [DisplayName("First late charge?")]
        public bool OnLineEF1Checked { get; set; }

        [DisplayName("After")]
        [DataType(DataType.Date)]
        public DateTime? OnLineEF1After { get; set; }

        [DisplayName("Total entry fee")]
        // [Range(Constants.Tourney_EntryFee_Min, Constants.Tourney_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? OnLineEF1 { get; set; }

        [Required]
        [DisplayName("Second late charge?")]
        public bool OnLineEF2Checked { get; set; }

        [DisplayName("After")]
        [DataType(DataType.Date)]
        public DateTime? OnLineEF2After { get; set; }

        [DisplayName("Total entry fee")]
        //[Range(Constants.Tourney_EntryFee_Min, Constants.Tourney_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? OnLineEF2 { get; set; }

        // [Required]
        [DisplayName("Onsite fee")]
        //[Range(Constants.Tourney_EntryFee_Min, Constants.Tourney_EntryFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public int? OnSiteEF { get; set; }


        [DisplayName("Max # of byes")]
        [Range(0, int.MaxValue, ErrorMessage = "{0} must be a positive number or zero")]
        public int? ByeMax { get; set; }

        [DisplayName("Number of rounds in tourney")]
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be a positive number")]
        public int? ByeRounds { get; set; }

        [DisplayName("Last round to request a bye")]
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be a positive number")]
        public int? ByeCommitBy { get; set; }

        [DisplayName("Bye note")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        public string ByeNote { get; set; }

        [Required]
        [DisplayName("GM/IM plays free?")]
        public ChargeDiscountViewModel GmImDis { get; set; }

        [Required]
        [DisplayName("Economy option?")]
        public ChargeDiscountViewModel EconDis { get; set; }

        [Required]
        [DisplayName("Senior discount?")]
        public ChargeDiscountViewModel SeniorDis { get; set; }

        [Required]
        [DisplayName("Play-up option?")]
        public ChargeDiscountViewModel Playup { get; set; }

        [Required]
        [DisplayName("Re-entry option?")]
        public ChargeDiscountViewModel Reentry { get; set; }

        [Required]
        [DisplayName("Allow players to request USCF membership during registration?")]
        public bool UscfMembershipChecked { get; set; }

        [DisplayName("Upload an existing flyer")]
        public FlyerType FlyerType { get; set; }

        [Required]
        [DisplayName("I understand that I should double check the system generated PDF file and the tournament registration page.")]
        public bool IUnderstandChecked { get; set; }

        //[Required]
        [DisplayName("Disable Jumbula online registration")]
        public bool OfflineRegChecked { get; set; }

        [Required]
        [DisplayName("Provide USCF membership?")]
        public ICollection<ChargeDiscountViewModel> UscfMembership { get; set; }

        [Required]
        public ICollection<SectionViewModel> Sections { get; set; }

        [Required]
        public ICollection<NoteViewModel> PrizeNotes { get; set; }

        [Required]
        public ICollection<ScheduleViewModel> Schedules { get; set; }

        [DisplayName("Trophies")]
        [AllowHtml]
        public string TrophyInfo { get; set; }

        // Hidden inputs, not user provided, already set by the controller
        public string ClubDomain { get; set; }
        public string ClubLogo { get; set; }

        public string Domain { get; set; } // set after post back

        public byte[] JBLogo { get; set; }  // Jumbula logo

        public ICollection<Uri> Images { get; set; } // system generated (only club logo) for this release, R1+ admin can add more images

        public PageMode PageMode { get; set; }

        [DisplayName("Check if you want to save a copy of this event as template.")]
        public bool AsTemplate { get; set; }

        public bool FromTemplate { get; set; }

        public EventSearchField EventSearchField { get; set; }

        public EventSearchTags EventSearchTags { get; set; }

        public DonationViewModel Donation { get; set; }

        public int PostalAdderssId { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public int ClubLocationId { get; set; }

        public string LocationName { get; set; }

        public ClubLocationViewModel ClubLocationViewModel { get; set; }

        public DisplayClubLocationViewModel DisplayClubLocationViewModel { get; set; }

        public string FileName { get; set; }

        [DisplayName("Event type")]
        public TourneyRewardType EventType { get; set; }

        [DisplayName("Tournament type")]
        public TourneyRewardType TourneyType { get; set; }

        [DisplayName("Template name")]
        public string TemplateName { get; set; }

        public SelectList Ages { get; set; }

        [DisplayName("Outsourcer")]
        public string OutSourcerClubDomain { get; set; }

        public IEnumerable<OutSourcerList> OutSourcers { get; set; }

        public SelectList OutSourcerSelectList { get; set; }

        public int ClubId { get; set; }

        public bool IsVendor { get; set; }

        [DisplayName("Is this an outsourcer event for a school or club?")]
        public EventSponserType IsVendorEvent { get; set; }

        public int OutSourcerClubLocationId { get; set; }

        public RegisterDeadLine RegisterDeadLine { get; set; }

        public List<CustomDiscountViewModel> CustomDiscounts { get; set; }

        public CreateEditChessTourneyViewModel()
        {
            Donation = new DonationViewModel();
            HotelChecked = false;
            OnLineEF1Checked = false;
            OnLineEF2Checked = false;
            UscfMembershipChecked = false;

            UscfMembership = new HashSet<ChargeDiscountViewModel>();
            Sections = new HashSet<SectionViewModel>();
            PrizeNotes = new HashSet<NoteViewModel>();
            Schedules = new HashSet<ScheduleViewModel>();
            Images = new HashSet<Uri>();
            TourneyType = TourneyRewardType.None;
            EventSearchField = new EventSearchField();
            ClubLocationViewModel = new ClubLocationViewModel(false);
            EventSearchTags = new EventSearchTags();
            SeedAges();
            OutSourcers = new List<OutSourcerList>();
            RegisterDeadLine = new RegisterDeadLine();
            CustomDiscounts = new List<CustomDiscountViewModel>();
        }

        public CreateEditChessTourneyViewModel(string clubDomain, string logo, Domain.TimeZone timeZone, PageMode pageMode, int clubId, bool isVendor)
            : this()
        {
            ClubDomain = clubDomain;
            ClubLogo = logo;
            TimeZone = timeZone;
            CurrentStep = EventPageStep.BasicInformation;
            PageMode = pageMode;
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(clubDomain);
            Seed();
            SeedSections();
            SeedSchedules();

            ClubId = clubId;
            IsVendor = isVendor;
            SeedOUtSourcers();

        }

        public CreateEditChessTourneyViewModel(Tourney t, bool fromTemplate = false, int clubId = 0, bool isVendor = false)
            : this()
        {
            // set event for a special outsourcer
            ClubId = clubId;
            IsVendor = isVendor;
            IsVendorEvent = string.IsNullOrEmpty(t.OutSourcerClubDomain) ? EventSponserType.Vendor : EventSponserType.OutSourcer;
            OutSourcerClubDomain = t.OutSourcerClubDomain;
            SeedOUtSourcers();
            RegisterDeadLine = t.RegisterDeadLine;

            Name = t.Name;
            SubTitle1 = t.SubTitle1;
            SubTitle2 = t.SubTitle2;
            ChiefTD = t.ChiefTD;
            ChiefOrganizer = t.ChiefOrganizer;
            OfflineRegChecked = t.OfflineRegChecked;
            ExternalRegUrl = t.ExternalRegUrl;
            PostalAddress = t.ClubLocation.PostalAddress;
            HotelChecked = t.Hotel.Checked;
            TimeZone = t.TimeZone;
            LocationName = t.AddressName;
            FlyerType = t.FlyerType;
            FileName = t.FileName;
            TourneyType = t.TourneyType;
            TrophyInfo = t.TrophyInfo;
            AsTemplate = !fromTemplate && t.AsTemplate;
            FromTemplate = fromTemplate;
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(t.ClubDomain)
                {
                    SelectedClubLocationId = t.ClubLocation.Id
                };

            TemplateName = t.TemplateName;

            //Donation
            Donation.Checked = t.Donation.Checked;
            Donation.Title = t.Donation.Title;
            Donation.Description = t.Donation.Description;

            if (t.Hotel.Rate != -1 && t.Hotel.Rate.HasValue)
            {
                HotelRate = (int)t.Hotel.Rate;
            }
            else // leave it blank
            {
            }


            EventSearchField = new EventSearchField
                {
                    Gender = t.EventSearchField.Gender,
                    Level = t.EventSearchField.Level,
                    MaxAge = t.EventSearchField.MaxAge,
                    MinAge = t.EventSearchField.MinAge,
                    MaxGrade = t.EventSearchField.MaxGrade == SchoolGradeType.College || t.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool ? SchoolGradeType.None : t.EventSearchField.MaxGrade,
                    MinGrade = t.EventSearchField.MinGrade == SchoolGradeType.College || t.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool ? SchoolGradeType.None : t.EventSearchField.MinGrade,

                };

            HotelRateCode = t.Hotel.RateCode;
            PrizeFund = t.Prize.Fund;
            PrizeFundBasedOn = t.Prize.BasedOn;
            PrizeFundGuarantee = t.Prize.Guarantee;
            OnLineEF = (int)t.EF.OnLine;
            OnLineEFDeadline = t.EF.OnLineDeadline;
            OnLineEF1Checked = t.EF.OnLine1Checked;

            if (t.EF.OnLine1 != -1)
            {
                OnLineEF1 = (int)t.EF.OnLine1;
            }

            OnLineEF1After = t.EF.OnLine1After;
            OnLineEF2Checked = t.EF.OnLine2Checked;

            if (t.EF.OnLine2 != -1)
            {
                OnLineEF2 = (int)t.EF.OnLine2;
            }

            OnLineEF2After = t.EF.OnLine2After;
            OnSiteEF = (t.EF.OnSite.HasValue) ? (int)t.EF.OnSite.Value : (int?)null;
            ByeMax = t.Bye.Max;

            if (t.Bye.Rounds != -1)
            {
                ByeRounds = t.Bye.Rounds;
            }

            if (t.Bye.CommitBy != -1)
            {
                ByeCommitBy = t.Bye.CommitBy;
            }

            ByeNote = t.Bye.Note;

            // Rebuild the optional charge and discounts
            // Call the seed method first to set up default values
            // The foreach statement overrides the default values if they exist in db

            SeedChargeDiscountOptions();
            TourneyType = t.TourneyType;
            TrophyInfo = t.TrophyInfo;
            foreach (ChargeDiscount item in t.ChargeDiscounts)
            {
                switch (item.Category)
                {
                    case ChargeDiscountCategories.Playup:
                        Playup = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Reentry:
                        Reentry = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.UscfMem:
                        UscfMembership.Add(new ChargeDiscountViewModel(item));
                        break;

                    case ChargeDiscountCategories.GmIm:
                        GmImDis = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Econ:
                        EconDis = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Senior:
                        SeniorDis = new ChargeDiscountViewModel(item);
                        break;

                    case ChargeDiscountCategories.Custom:
                        {
                            CustomDiscounts.Add(new CustomDiscountViewModel
                            {
                                Discount = new ChargeDiscountViewModel
                                {
                                    IsChecked = true,
                                    Amount = (int)item.Amount,
                                    ChargeDiscountType = item.ChargeDiscountType,
                                    Desc = item.Desc
                                }
                            });
                            break;
                        }

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_ChargeDiscountCategoryNotSupported, item.Category));
                }
            }

            if (UscfMembership.Count == 0) // db did not contain membership, use default
            {
                SeedUscfMembership();
            }
            else
            {
                UscfMembershipChecked = true;
            }

            foreach (TourneySection item in t.Sections)
            {
                Sections.Add(new SectionViewModel(item));
            }

            foreach (Note item in t.Notes)
            {
                PrizeNotes.Add(new NoteViewModel(item));
            }

            foreach (TourneySchedule item in t.Schedules)
            {
                Schedules.Add(new ScheduleViewModel(item));
            }

            // Hidden fields
            // Note: clublogo will set in controller, does not exist in tourney db

            ClubDomain = t.ClubDomain;
            EventSearchTags = t.EventSearchTags;
            Domain = t.Domain;

            // System fields
            // Used during postback
        }

        public void SeedTourneyViewModel(TourneyRewardType tournyType)
        {
            SeedSections();
            if (tournyType == TourneyRewardType.Cash)
            {
                SeedPrizeNotes();
            }
            SeedSchedules();
            SeedChargeDiscountOptions();
            SeedUscfMembership();
        }

        public Tourney ToTourney(bool setDomain = false)
        {
            var t = new Tourney();

            var clubLoc = Ioc.IClubLocationDb.GetClubLocation(DisplayClubLocationViewModel.SelectedClubLocationId.HasValue ? DisplayClubLocationViewModel.SelectedClubLocationId.Value : ClubLocationId);
            t.ClubLocation = clubLoc;
            t.FlyerType = FlyerType;
            t.Name = Name;
            t.SubTitle1 = SubTitle1;
            t.SubTitle2 = SubTitle2;
            t.ChiefTD = ChiefTD;

            t.ChiefOrganizer = ChiefOrganizer;
            t.OfflineRegChecked = OfflineRegChecked;
            t.ExternalRegUrl = ExternalRegUrl;
            t.Prize = new Prize { Fund = PrizeFund.HasValue ? PrizeFund.Value : 0, BasedOn = PrizeFundBasedOn.HasValue ? PrizeFundBasedOn.Value : 0, Guarantee = PrizeFundGuarantee };
            t.TourneyType = TourneyType;
            t.TrophyInfo = Utilities.GetDescription(TrophyInfo);
            t.Domain = setDomain ? Utilities.GenerateSubDomainName(ClubDomain, Name) : Domain;
            t.ItemId = Utilities.GenerateItemId();
            t.EventStatus = EventStatusCategories.open;
            t.TrophyInfo = TrophyInfo;
            t.MetaData = new MetaData { DateCreated = DateTime.Now, DateUpdated = DateTime.Now };
            t.CategoryId = CategoryModel.ChessId;
            t.EventSearchTags = EventSearchTags;
            t.TemplateName = TemplateName;
            t.OutSourcerClubDomain = IsVendorEvent == EventSponserType.OutSourcer ? OutSourcerClubDomain : null;
            t.RegisterDeadLine = RegisterDeadLine;

            //Donation
            t.Donation.Checked = Donation.Checked;
            if (!Donation.Checked)
            {// if donation is unchecked set description and title null
                t.Donation.Description = null;
                t.Donation.Title = null;
            }
            else
            {// if donation is checked set description and title to model value
                t.Donation.Description = Donation.Description;
                t.Donation.Title = Donation.Title;
            }

            // hotel
            // Note: Hotel can be selected but it is ok that no hotel rate may be present
            if (HotelRate.HasValue) // no hotel rate, set optional param
            {
                t.Hotel = new Hotel { Checked = HotelChecked, Rate = HotelRate.Value, RateCode = HotelRateCode };
            }
            else // no rate
            {
                t.Hotel = new Hotel { Checked = HotelChecked, Rate = -1, RateCode = HotelRateCode };
            }

            if (!ByeMax.HasValue || ByeMax == 0) // no bye options, set optional params ByeRounds and ByeNote, they are undefined
            {
                t.Bye = new Bye { IsDefined = false, Max = 0, Rounds = -1, CommitBy = -1, Note = string.Empty };
            }
            else
            {
                t.Bye = new Bye { IsDefined = true, Max = ByeMax.Value, Rounds = ByeRounds.Value, CommitBy = ByeCommitBy.Value, Note = ByeNote };
            }

            if (OnLineEF1Checked == false) // no first late fee, set optional params, they are undefined
            {
                OnLineEF1 = -1;
                OnLineEF1After = DateTime.Today;
            }

            if (OnLineEF2Checked == false) // no second late fee, set optional params, they are undefined
            {
                OnLineEF2 = -1;
                OnLineEF2After = DateTime.Today;
            }

            t.EF = new EntryFee
            {
                OnLine = OnLineEF.Value,
                OnLineDeadline = OnLineEFDeadline.Value,
                OnLine1Checked = OnLineEF1Checked,
                OnLine1 = OnLineEF1.Value,
                OnLine1After = OnLineEF1After.Value,
                OnLine2Checked = OnLineEF2Checked,
                OnLine2 = OnLineEF2.Value,
                OnLine2After = OnLineEF2After.Value,
                OnSite = (OnSiteEF.HasValue) ? OnSiteEF.Value : (decimal?)null
            };

            // Address and Geo
            t.Geography = Utilities.NewDbGeography(PostalAddress.Lat, PostalAddress.Lng);

            // Note
            // Take the charge/discount/note categories from the ChargeCategories/DiscountCategories, not postback values in the model
            // For lists, the model builder does NOT re-create the categories b/c they were not present in the view


            if (Playup.IsChecked == true)
            {
                ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.Playup, Amount = Playup.Amount.Value, Desc = Playup.Desc.Contains("$") ? Playup.Desc : Playup.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(Playup.Amount.Value) };
                t.ChargeDiscounts.Add(c);
            }

            if (Reentry.IsChecked == true)
            {
                ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.Reentry, Amount = Reentry.Amount.Value, Desc = Reentry.Desc.Contains("$") ? Reentry.Desc : Reentry.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(Reentry.Amount.Value) };
                t.ChargeDiscounts.Add(c);
            }

            if (UscfMembershipChecked == true)
            {
                foreach (ChargeDiscountViewModel cvm in UscfMembership)
                {
                    ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.UscfMem, Amount = cvm.Amount.Value, Desc = cvm.Desc.Contains("$") ? cvm.Desc : cvm.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(cvm.Amount.Value) };
                    t.ChargeDiscounts.Add(c);
                }
            }

            if (GmImDis.IsChecked == true)
            {
                ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.GmIm, Amount = GmImDis.Amount.Value, Desc = GmImDis.Desc.Contains("$") ? GmImDis.Desc : GmImDis.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(GmImDis.Amount.Value) };
                t.ChargeDiscounts.Add(d);
            }

            if (EconDis.IsChecked == true)
            {
                ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.Econ, Amount = EconDis.Amount.Value, Desc = EconDis.Desc.Contains("$") ? EconDis.Desc : EconDis.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(EconDis.Amount.Value) };
                t.ChargeDiscounts.Add(d);
            }

            if (SeniorDis.IsChecked == true)
            {
                ChargeDiscount d = new ChargeDiscount { Category = ChargeDiscountCategories.Senior, Amount = SeniorDis.Amount.Value, Desc = SeniorDis.Desc.Contains("$") ? SeniorDis.Desc : SeniorDis.Desc + ": " + Utilities.FormatCurrencyWithoutPenny(SeniorDis.Amount.Value) };
                t.ChargeDiscounts.Add(d);
            }

            if (CustomDiscounts.Any())
            {
                foreach (var discount in CustomDiscounts)
                {
                    ChargeDiscount customDiscount = null;
                    if (discount.Discount.ChargeDiscountType == ChargeDiscountType.Fixed)
                    {
                        customDiscount = new ChargeDiscount
                        {
                            Category = ChargeDiscountCategories.Custom,
                            Amount = (decimal)discount.Discount.Amount,
                            Desc =
                                discount.Discount.Desc.Contains("$")
                                    ? discount.Discount.Desc
                                    : discount.Discount.Desc + ": " +
                                      Utilities.FormatCurrencyWithoutPenny(discount.Discount.Amount.Value),
                            ChargeDiscountType = discount.Discount.ChargeDiscountType
                        };
                    }
                    else
                    {
                        customDiscount = new ChargeDiscount
                        {
                            Category = ChargeDiscountCategories.Custom,
                            Amount = (decimal)discount.Discount.Amount,
                            Desc =
                                discount.Discount.Desc.Contains("%")
                                    ? discount.Discount.Desc
                                    : discount.Discount.Desc + ": " +
                                      Utilities.FormatPercentWithoutPenny(discount.Discount.Amount.Value),
                            ChargeDiscountType = discount.Discount.ChargeDiscountType
                        };
                    }

                    t.ChargeDiscounts.Add(customDiscount);
                }
            }

            foreach (NoteViewModel nvm in PrizeNotes)
            {
                Note n = new Note { Category = NoteCategories.Prize, Desc = nvm.Desc };
                t.Notes.Add(n);
            }

            foreach (SectionViewModel vm in Sections)
            {
                TourneySection s = new TourneySection { Name = vm.Name, Rating = vm.Rating, Prize = vm.Prize };
                t.Sections.Add(s);
            }

            const string Or = " or ";
            string dates = string.Empty;

            for (int i = 0; i < Schedules.Count; ++i)
            {
                var vm = Schedules.ElementAt(i);
                TourneySchedule s = new TourneySchedule { Start = vm.Start.Value, End = vm.End.Value, Games = vm.Games, RegOnSite = vm.RegOnSite, TimeControl = vm.TimeControl, Name = vm.GetSchName(), ScheduleLabel = vm.ScheduleLabel };
                t.Schedules.Add(s);

                // Setup the diplay date
                //

                dates = vm.GetSchMonDayName();

                if (i < Schedules.Count - 1)
                {
                    dates += Or;
                }
                else
                {
                    dates += Schedules.ElementAt(Schedules.Count - 1).End.Value.ToString(" yyyy");
                }
            }

            t.DisplayDate = dates;

            // Assign hidden fields
            // Note: No need for ClubLogo

            t.ClubDomain = ClubDomain;

            // System fields
            // Note that Domain is null within create tourney, but is valid durig edit

            t.EventSearchField = new EventSearchField
            {
                MinAge = EventSearchField.MinAge,
                MaxAge = EventSearchField.MaxAge,
                MinGrade = EventSearchField.MinGrade.HasValue && EventSearchField.MinGrade.Value != SchoolGradeType.None ? EventSearchField.MinGrade : null,
                MaxGrade = EventSearchField.MaxGrade.HasValue && EventSearchField.MaxGrade.Value != SchoolGradeType.None ? EventSearchField.MaxGrade : null,
                Level = EventSearchField.Level.HasValue && EventSearchField.Level.Value != Level.None ? EventSearchField.Level : null,
                Gender = EventSearchField.Gender.HasValue ? EventSearchField.Gender : null,

            };

            t.AsTemplate = AsTemplate;

            PostalAddress = clubLoc.PostalAddress;

            t.TimeZone = TimeZoneHelper.GetTimeZone(clubLoc.PostalAddress.Lat, clubLoc.PostalAddress.Lng);

            return t;
        }

        public string GetRegOnlineEnd()
        {
            return OnLineEFDeadline.Value.ToString("ddd d MMM");
        }

        private void SeedChargeDiscountOptions()
        {
            GmImDis = new ChargeDiscountViewModel(false, ChargeDiscountCategories.GmIm, null, string.Empty, ChargeDiscountType.Fixed);
            EconDis = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Econ, null, string.Empty, ChargeDiscountType.Fixed);
            SeniorDis = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Senior, null, string.Empty, ChargeDiscountType.Fixed);
            Playup = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Playup, null, string.Empty, ChargeDiscountType.Fixed);
            Reentry = new ChargeDiscountViewModel(false, ChargeDiscountCategories.Reentry, null, string.Empty, ChargeDiscountType.Fixed);
        }

        private void SeedPrizeNotes()
        {
            var note1 = new NoteViewModel { Category = NoteCategories.Prize, Desc = string.Empty };
            var note2 = new NoteViewModel { Category = NoteCategories.Prize, Desc = string.Empty };
            PrizeNotes.Add(note1);
            PrizeNotes.Add(note2);
        }

        private void SeedSchedules()
        {
            ScheduleViewModel sch1 = new ScheduleViewModel();

            ScheduleViewModel sch2 = new ScheduleViewModel();

            Schedules.Add(sch1);
            Schedules.Add(sch2);
        }

        private void SeedUscfMembership()
        {
            ChargeDiscountViewModel m1 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.UscfMem, Desc = string.Empty };
            ChargeDiscountViewModel m2 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.UscfMem, Desc = string.Empty };
            UscfMembership.Add(m1);
            UscfMembership.Add(m2);
        }

        private void SeedSections()
        {
            SectionViewModel sec = new SectionViewModel();
            Sections.Add(sec);
            sec = new SectionViewModel();
            Sections.Add(sec);

        }

        private void SeedAges()
        {
            var ageList = new List<SelectListItem>();
            for (int i = 1; i <= 120; i++)
            {
                ageList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            Ages = new SelectList(ageList, "Value", "Text");
        }



        public void SeedClubLocation()
        {
            DisplayClubLocationViewModel.ClubLocations = Ioc.IClubLocationDb.GetClubLocations(ClubDomain);
        }

        private void SeedOUtSourcers()
        {
            var outSourcers = Ioc.IClubVendorDb.GetSchoolsForClub(ClubId);
            OutSourcers = outSourcers.Select(m => new OutSourcerList
            {
                OutSourcerClubDomain = m.OutSourcerClubDomain,
                OutSourcerClubName = m.OutSourcerLocation.PostalAddress.Address + "(" + m.OutSourcerClubName + ")",
                OutSourcerLocation = m.OutSourcerLocation,

            }).ToList();
            OutSourcerSelectList = new SelectList(OutSourcers, "OutSourcerClubDomain", "OutSourcerClubName");

        }

        public void Seed()
        {
            if (DisplayClubLocationViewModel.ClubLocations == null)
            {
                SeedClubLocation();
            }
        }
    }

    public class RegisterChessTourneyViewModel
    {
        public int? ProfileId { get; set; }

        public string EditModeIdentifire { get; set; }

        // indicate user that logged in is adult or not
        public bool IsAdultLoggedInUser { get; set; }

        public string PlayerImageUrl { get; set; }

        public string OutSourcerClubDomain { get; set; }

        //[Display(Name = "FIDE ID")]
        //[StringLength(128, ErrorMessage = "{0} is too long")]
        //public string FideId { get; set; }

        //[Display(Name = "FIDE rating")]
        //[StringLength(128, ErrorMessage = "{0} is too long")]
        //public string FideRating { get; set; }

        public string PrimaryPlayerEmail { get; set; }

        // if player was not adult and doesent have parent it set to true
        public bool ParentRequired { get; set; }


        // this Property for dropdownlist
        [Display(Name = "Registering for")]
        public int? SelectedProfileId { get; set; }

        // this Property for dropdownlist get it value from playerprofile view model
        public IEnumerable<SelectListItem> PlayerLists { get; set; }

        // view model for insert parent
        public AddPlayerProfileViewModel Parent { get; set; }


        public DonationViewModel Donation { get; set; }

        public PlayerProfileComplitionFormContactViewModel Contact { get; set; }

        // public ChessProfileInfoViewModel Sport { get; set; }

        [Required]
        [Display(Name = "Available sections")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Section { get; set; }

        [Required]
        [Display(Name = "Available schedules")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Schedule { get; set; }

        public ChargeDiscountPlayerViewModel EF { get; set; }
        public ChargeDiscountPlayerViewModel Playup { get; set; }
        //
        public List<ChargeDiscountViewModel> CustomDiscounts { get; set; }


        public bool ProvidedDiscountsIsAvail { get; set; }

        [Display(Name = "I acknowledge that I would like to apply for the following discounts:")]
        public bool ProvidedDiscountsIsChecked { get; set; }

        public ChargeDiscountCategories? ProvidedDiscountsSelected { get; set; }

        public Bye Bye { get; set; }

        public bool[] ByeRequests { get; set; }

        public JbForm JbForm { get; set; }

        public ICollection<TourneySection> Sections { get; set; }
        public ICollection<ScheduleViewModel> Schedules { get; set; }
        public dynamic UscfMembership { get; set; }
        public dynamic ProvidedDiscounts { get; set; }

        // System assigned fields set after postback

        //public string firstName { get; set; }
        //public string lastName { get; set; }
        public PlayerProfile Player { get; set; }
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public string Domain { get; set; }
        public string ClubDomain { get; set; }
        public string ByesRequested { get; set; }
        public string optionsRequested { get; set; }
        public decimal total { get; set; }


        public bool IsEligibleUser { get; set; }
        public RegistrationMode RegistrationMode { get; set; }

        public TourneyPageViewModel TourneyInfo { get; set; }

        public OrderMode OrderMode { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                //// Get cart
                //var cartDb = Ioc.ICartDbService;

                //// Get domain of orders in the cart(if there is no order in cart return empty)
                //string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                //if (!string.IsNullOrEmpty(currentDomain))
                //{
                //    return !(currentDomain == this.ClubDomain);
                //}

                return false;
            }
        }

        public RegisterChessTourneyViewModel()
        {
            Donation = new DonationViewModel();
        }

        public RegisterChessTourneyViewModel(Tourney t)
        {
            Donation = new DonationViewModel(t.Donation);

            InitRegisterChessTourneyViewModel(t);

            CustomDiscounts =
                t.ChargeDiscounts.Where(p => p.Category == ChargeDiscountCategories.Custom)
                    .Select(p => new ChargeDiscountViewModel(p, false))
                    .ToList();

        }

        public void InitRegisterChessTourneyViewModel(Tourney t)
        {
            
            TourneyInfo = new TourneyPageViewModel(t);

            TourneyInfo.Map = new GoogleMapsViewModel()
            {
                Name = t.AddressName,
            };

            Domain = t.Domain;
            ClubDomain = t.ClubDomain;

            ItemName = t.Name;

            Playup = new ChargeDiscountPlayerViewModel { IsAvailable = false };
            Schedules = new HashSet<ScheduleViewModel>();
            UscfMembership = new HashSet<ChargeDiscount>(); // need the embedded Id, don't convert to view model
            ProvidedDiscounts = new HashSet<ChargeDiscount>(); // need the embedded Id, don't convert to view model

            Sections = t.Sections;

            // Use view model for schedules b/c we need to call its methods to get schedule name, etc.

            foreach (TourneySchedule item in t.Schedules)
            {
                Schedules.Add(new ScheduleViewModel(item));
            }

            EF = new ChargeDiscountPlayerViewModel { IsAvailable = true, IsChecked = true, Category = ChargeDiscountCategories.EF, Desc = string.Format(Constants.W_Tourney_Ef, t.EF.OnLine), Amount = (int)t.EF.OnLine };

            // Calculate late fees if any

            if (t.EF.OnLine1Checked) // we have late fee # 1
            {
                if (DateTime.Today > t.EF.OnLine1After) // player is registering after first late fee charge
                {
                    EF.Amount = (int)t.EF.OnLine1;
                    EF.Desc = string.Format(Constants.W_Tourney_Ef_After, Utilities.FormatDateWithoutTime(t.EF.OnLine1After.Value), Utilities.FormatCurrencyWithoutPenny(EF.Amount));
                }
            }

            if (t.EF.OnLine2Checked) // we have late fee # 2
            {
                if (DateTime.Today > t.EF.OnLine2After) // player is registering after first late fee charge
                {
                    EF.Amount = (int)t.EF.OnLine2;
                    EF.Desc = string.Format(Constants.W_Tourney_Ef_After, Utilities.FormatDateWithoutTime(t.EF.OnLine2After.Value), Utilities.FormatCurrencyWithoutPenny(EF.Amount));
                }
            }

            // Note: All IsCheck fields for ChargeDiscountCategories items must have been set to false
            //       They will be re-written if they exist
            // Note: Reentry charge does not apply during registration

            foreach (ChargeDiscount item in t.ChargeDiscounts)
            {
                switch (item.Category)
                {
                    case ChargeDiscountCategories.Playup:
                        Playup = new ChargeDiscountPlayerViewModel(item);
                        break;

                    case ChargeDiscountCategories.UscfMem:
                        UscfMembership.Add(item);
                        break;

                    case ChargeDiscountCategories.GmIm:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Econ:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Senior:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Custom:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Reentry: // this charge does not apply to registration
                        break;

                    case ChargeDiscountCategories.EF: // EF should not have been included this way and is handled differntly
                        break;

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_ChargeDiscountCategoryNotSupported, item.Category));
                }
            }

            if (ProvidedDiscounts.Count > 0)
            {
                ProvidedDiscountsIsAvail = true;
            }
            else
            {
                ProvidedDiscountsIsAvail = false;
            }

            // Bye options

            Bye = t.Bye;

            // Hidden fields

            ItemId = t.ItemId;

            RegistrationMode = t.RegistrationMode;
        }
    }

    public class SectionViewModel
    {
        [Required]
        [DisplayName("Section name")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Section rating")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Rating { get; set; }


        [DisplayName("Section prize")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Prize { get; set; }

        public SectionViewModel()
        {

        }

        public SectionViewModel(TourneySection s)
        {
            Name = s.Name;
            Rating = s.Rating;
            Prize = s.Prize;
        }

        public override string ToString()
        {
            return Name + Constants.S_OpenPran + Constants.S_Space + Rating + Constants.S_Space + Constants.S_OpenPran + Constants.S_Colon + Constants.S_Space + Prize;
        }
    }

    public class ScheduleViewModel
    {
        [Required]
        [DisplayName("Start date")]
        [DataType(DataType.Date)]
        public DateTime? Start { get; set; }

        [Required]
        [DisplayName("End date")]
        [DataType(DataType.Date)]
        public DateTime? End { get; set; }

        [Required]
        [DisplayName("Onsite reg day & time")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string RegOnSite { get; set; }

        [Required]
        [DisplayName("Time control")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string TimeControl { get; set; }

        [Required]
        [DisplayName("Games")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Games { get; set; }

        [StringLength(128)]
        [DisplayName("Label")]
        public string ScheduleLabel { get; set; }

        public ScheduleViewModel()
        {
            RegOnSite = string.Empty;
            TimeControl = string.Empty;
            Games = string.Empty;
            ScheduleLabel = string.Empty;
        }

        public ScheduleViewModel(TourneySchedule sch)
        {
            Start = sch.Start;
            End = sch.End;
            RegOnSite = sch.RegOnSite;
            TimeControl = sch.TimeControl;
            Games = sch.Games;
            ScheduleLabel = sch.ScheduleLabel;
        }

        public string GetSchName()
        {
            string schName = (End.Value.Subtract(Start.Value).Days + 1) + "-day";
            if (!string.IsNullOrEmpty(ScheduleLabel))
                schName += string.Format(" {0}", ScheduleLabel);
            return schName;
        }

        public string GetSchFullName()
        {
            string name = GetSchName();
            string from = Start.Value.ToShortDateString();
            string to = End.Value.ToShortDateString();
            return name + Constants.S_Space + Constants.S_OpenPran + from + Constants.S_Comma + Constants.S_Space + to + Constants.S_ClosePran;
        }

        public string GetSchMonDayName()
        {
            string s = Start.Value.ToString("MMMM ");
            s = s + Start.Value.Day;

            if (End.Value.Subtract(Start.Value).Days > 0)
            {
                if (Start.Value.Month == End.Value.Month)
                {
                    s = s + Constants.S_Dash + End.Value.Day;
                }
                else
                {
                    s = s + Constants.S_Dash + End.Value.ToString(" MMMM ") + End.Value.Day;
                }
            }

            return s;
        }
    }

    public class ChessCoverageViewModel
    {
        public string ClubDomain { get; set; }
        public string Domain { get; set; }

        public int CoverageTypeId { get; set; }
        public IEnumerable<SelectListItem> CoverageType { get; set; }


        [DisplayName("Section")]
        public int SectionId { get; set; }

        public SelectList SectionsList { get; set; }


        [DisplayName("Round")]
        public int RoundId { get; set; }

        public SelectList RoundsList { get; set; }

        [Required]
        [DisplayName("Upload your file")]
        public string FileContent { get; set; }

        public TourneyCoverage ToTourneyCoverage()
        {
            return new TourneyCoverage()
            {
                CoverageType = (TourneyCoverageType)CoverageTypeId,
                Data = this.FileContent,
                RoundNo = this.RoundId
            };
        }

    }

    public class ChessResultViewModel
    {
        public ChessResultViewModel()
        {
            TourneyCoverages = new List<TourneyCoverage>();
        }
        public string ClubDomain { get; set; }
        public string Domain { get; set; }
        public string TourneyName { get; set; }
        public List<TourneyCoverage> TourneyCoverages { get; set; }

    }

    public class RoundViewModel
    {
        public string Name { get; set; }

        public int Id { get; set; }
    }
}