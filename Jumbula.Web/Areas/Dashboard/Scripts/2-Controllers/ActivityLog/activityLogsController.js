﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('activityLogsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', 'customReportService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, customReportService) {
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value) {
            if (value.Domain === $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $http.get(window.location.origin + "/Dashboard/ActivityLog/GetFilters").success(function (data) {
            $scope.Filters = data;

        }).error(function (data) {
            $scope.MC.handleErrors($scope, data);
        });

        $scope.loadGrid = function () {
            $scope.activityLogDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport: {
                    read:
                        {
                            url: window.location.origin + '/Dashboard/ActivityLog/GetList/',
                            contentType: "application/json; charset=utf-8",
                            type: "POST"
                        },
                    parameterMap: function (data, operation) {
                        if (operation === "read") {
                            data = $.extend(
                                {
                                    sort: null,
                                    filters: $scope.Filters
                                },
                                data);
                        }
                        return JSON.stringify(data);
                    }
                },
                schema:
                    {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                sort:
                    [{
                        field: "Action",
                        dir: "asc"
                    }],
                pageSize: 50,
                batch: true,
                serverFiltering: true,
                serverPaging: true,
                serverSorting: true,
                requestStart: function () {
                    kendo.ui.progress($('[kendo-grid]'), true);
                },
                requestEnd: function () {
                    kendo.ui.progress($('[kendo-grid]'), false);
                }
            });

            $scope.activityLogGridOptions = {
                dataSource: $scope.activityLogDataSource,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                dataBound: function () {
                    customReportService.initializeDoubleScroll();
                },
                columns: [
                    {
                        width: "40px",
                        template: "#if(Status){#<span style='color: green' class='jbi-check-2'> </span>#}else{# <span style='color: red' class='jbi-exclamation'> </span> #}#"
                    },
                    {
                    field: "Action",
                    title: "Activity",
                    width: "200px"
                },
                {
                    field: "Description",
                    title: "Description"
                },
                {
                    field: "DateTime",
                    title: "Time",
                    width: "170px"
                },
                {
                    field: "UserName",
                    title: "User",
                    width: "200px"
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                                 <li > ... \ ' +
                                   '<ul>\
                                     <li ng-click="goDetail(\'#=Id#\')">\
                                          <span class="k-link">Details</span>\
                                      </li>\
                                    </ul>\
                                 </li>\
                               </ul>'
                }
            ]
            };

        }

        $scope.loadGrid();

        $scope.goDetail = function (id) {
            $state.go("ActivityLogDetail", { id: id});
        }

        $scope.search = function () {
            $scope.activityLogGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Filters.UserName = '';
            $scope.Filters.Activity = null;

            $scope.activityLogGridOptions.dataSource.read();
        }

        $scope.init = function () {
            $('[data-role=dropdown]').dropdown();
        }
    }]);
})();

