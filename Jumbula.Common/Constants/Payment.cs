﻿using System;

namespace Jumbula.Common.Constants
{

    public static class Payment
    {
        public const string JumbulaConfirmation = "Jumbula confirmation: ";
        public const int StripeMaximumMetaData = 20;

        //MetaData
        public const string ConfirmationId = "Confirmation id";
        public const string Email = "Email";
        public const string Source = "source";
        public const string JumbulaWithDash = "Jumbula - ";
        public const string SignJumbulaToTransactionId = "jb_";
        public const string UnderlineParticipant = "_Participant";
        public const string UnderlineProgram = "_Program";
        public const string UnderlineSeason = "_Season";
        public const string Donation = "Donation";
        public const string InstallmentToken = "installmentToken";
        public const string Participant = "Participant";
        public const string InstallmentDueDate = "_Installment_DueDate";
        public const string InstallmentAmount = "_Installment_Amount";
        public const string InvoiceAmount = "Invoice_Amount";
        public const string TakePaymentAmount = "TakePayment_Amount";
        public const string Season = "Season";
        public const string UserName = "UserName";
        public const string ClientId = "Client_Id";
        public const string BillingId = "Billing_Id";

        //Parent Payment calender scheduler
        public const string ParentPaymentSchedulerTitle = "parent Payment Scheduler";
        public const string ParentPaymentSchedulerMessage = "{0} {1} {2} (paid: {3})";
        public const string ParentPaymentSchedulerTooltipMessage = "{0} {1} {2} (paid: {3}) {4} ({5})";

        //Installment Status
        public const string UnpaidInstallmentStatus = "Unpaid";
        public const string FailInstallmentStatus = "Suspended";
        public const string PartiallyPaidInstallmentStatus = "Partially paid";
        public const string PaidInstallmentStatus = "Paid";
        public const string CanceledInstallmentStatus = "Canceled";
        public const string OverPaidInstallmentStatus = "Overpaid";
        public const string PastDueInstallmentStatus = "Past due";

        public const string PaymentAmountValidationMoreThanBalance = "The amount can not be more than balance.";
        public const string PaymentAmountValidationLessThanZero= "The amount can not be less than zero.";

        //History description
        public const string ParentOrderPaid = "Order {0} is placed and paid by the family.";
        public const string ParentOrderInstallmentPaid = "The amount ${0} is paid for the installment due date on {1} by the family.";
        public const string OrderParentMakePayment = "The amount ${0} is recieved by the family. Memo: {1}";
        public const string RefundInstallment = "The amount {0} is refunded from the {1} installment.";
        public const string PartiallyRefundInstallment = "The amount {0} is refunded from the {1} installment, and {2} didn't refunded";

        public const string ParentInvoiceDescriptionPaided = "{0} made a {1} payment by the family.";
        public const string ParentInvoiceDescription = "The amount ${0} is recieved by the family.";

        public const string OrderPartiallyRefund = "The amount {0} is refunded but cannot refund {1}. Memo: {2}";

        //Auto charge schedule 
        public static readonly DateTime AutoChargeStartPoint = new DateTime(2018, 9, 1);

        //Payment note
        public const string SystemTakeAPaymentNote = "Automatic take a payment due to cancellation";
        public const string SystemRefundNote = "Automatic refund due to cancellation";

        //Error message
        public const string RefundInstallmentFailedSaveDb = "We successfully refunded the money but could not update our internal database. Please contact Jumbula at support@jumbula.com";
        public const string FailProcessRefund = "The was a general issue and the refund failed. Please contact Jumbula support for more information.";
        public const string NotRefundWithAutomatically = "You can not refund the amount with automatically. Please contact Jumbula support for more information.";

        //Validations
        public const string RefundAmountRequiredMessage = "The amount is required.";
    }
 
}
