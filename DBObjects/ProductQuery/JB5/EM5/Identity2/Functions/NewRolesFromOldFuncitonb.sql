
/****** Object:  UserDefinedFunction [dbo].[GetNewRoleIdFromOld]    Script Date: 23/02/1395 10:58:16 �.� ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetNewRoleIdFromOld] 
 (@oldRoleId int)
 
RETURNS int
    BEGIN 
        DECLARE @oldRoleName nvarchar(50)
		DECLARE @newRoleId int

		select @oldRoleName = [RoleName] from [dbo].[webpages_Roles] where RoleId = @oldRoleId

		if @oldRoleName = 'Player'
		Begin
		  set @oldRoleName = 'Parent'
		End

		
		if @oldRoleName = 'ClubAdmin'
		Begin
		  set @oldRoleName = 'Admin'
		End

		select @newRoleId = [Id] from [dbo].[JbRoles] where [Name] = @oldRoleName

        RETURN @newRoleId
    END
GO


