﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('delinquentsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'EmailService', function ($scope, $http, $log, $stateParams, $state, jbToast, emailService) {

        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "Delinquents" });
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        $http.get(window.location.origin + '/Dashboard/People/GetDelinquents').success(function (data) {

            $scope.Model = data;
            $scope.Model.checkedIds = [];

            $scope.loadGrid();

        }).error(function (data) {
            $scope.MC.handleErrors($scope, data);
        });

        $scope.loadGrid = function () {
            $scope.delinquentDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport: {
                    read:
                        {
                            url: window.location.origin + '/Dashboard/People/GetDelinquentList/',
                            contentType: "application/json; charset=utf-8",
                            type: "POST"
                        },
                    parameterMap: function (data, operation) {
                        if (operation === "read") {
                            data = $.extend(
                                {
                                    filters: $scope.Filters
                                },
                                data);

                            return JSON.stringify(data);
                        }
                        return null;
                    }
                },
                schema:
                    {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                sort:
                    [{
                        field: "Id",
                        dir: "asc"
                    }],
                pageSize: 50,
                batch: true,
                serverFiltering: true,
                serverPaging: true,
                serverSorting: true,
                requestStart: function () {
                    kendo.ui.progress($('[kendo-grid]'), true);

                    $http.get(window.location.origin + '/Dashboard/People/GetDelinquentsTotal').success(function (data) {

                        $scope.Model = data;
                        $scope.Model.checkedIds = [];
                    }).error(function (data) {
                        $scope.MC.handleErrors($scope, data);
                    });
                },
                requestEnd: function () {
                    kendo.ui.progress($('[kendo-grid]'), false);
                }
            });

            $scope.delinquentGridOptions = {
                dataSource: $scope.delinquentDataSource,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                resizable: true,
                columns: [
                    {
                        template: "<input type='checkbox' class='checkbox' />",
                        width: "30px",
                        headerTemplate: '<input type="checkbox" class="checkAll" />'
                    },
                    {
                        field: "Email",
                        title: "Family",
                        sortable: true,
                        width: "170px",
                        template: "<span title='#=Email#' > #=Email# </span>"
                    },
                    {
                        field: "Participant",
                        title: "Participant",
                        sortable: true,
                        width: "130px",
                        template: "<span title='#=Participant#' > #=Participant# </span>"
                    },
                    {
                        field: "Program",
                        title: "Program",
                        sortable: true,
                        width: "125px",
                        template: "<span title='#=Program#' > #=Program# </span>"
                    },
                    {
                        field: "Season",
                        title: "Season",
                        sortable: true,
                        width: "95px",
                        template: "<span title='#=Season#' > #=Season# </span>"
                    },
                    {
                        field: "Confirmation",
                        title: "Confirmation",
                        sortable: true,
                        width: "110px"
                    },
                    {
                        field: "TotalAmount",
                        title: "Amount",
                        sortable: true,
                        width: "80px"
                    },
                    {
                        field: "PaidAmount",
                        title: "Paid",
                        sortable: true,
                        width: "60px"
                    },
                    {
                        field: "Balance",
                        title: "Balance",
                        sortable: true,
                        width: "80px"
                    },
                    {
                        field: "Policy",
                        title: "Policy",
                        sortable: false,
                        width: "70px"
                    },
                    {
                        field: "Attempts",
                        title: "Attempt #",
                        sortable: true,
                        width: "90px"
                    },
                    {
                        field: "",
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                                <li> ... \
                                    <ul>\
                                        <li ng-action='People_Delinquents_SendMail' ng-click='sendDelinquentEmail(dataItem.RelatedEntity,dataItem.RelatedEntityId)'>\
                                            <span class='k-link jbi-email'>Email</span>\
                                        </li>\
                                        <li ng-action='People_Delinquents_ViewHistory' ng-click='viewHistory(dataItem.RelatedEntity, dataItem.RelatedEntityId)'>\
                                                <span class='k-link icon-history'>View history</span>\
                                        </li>\
                                    </ul>\
                                </li>\
                              </ul>"
                    }
                ]
            };

        }

        $(document.body).on('click', '#delinquentGrid table .checkbox', selectRow);
        $(document.body).on('click', '#delinquentGrid table .checkAll', checkAll);

        function selectRow() {
            var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#delinquentGrid").data("kendoGrid"),
                dataItem = grid.dataItem(row);

            if (checked) {
                $scope.Model.checkedIds.push({
                    RelatedEntityName: dataItem.RelatedEntity,
                    RelatedEntityId: dataItem.RelatedEntityId
                });
                //-select the row
                row.addClass("k-state-selected");
            } else {

                var index = -1;
                for (var i = 0; i < $scope.Model.checkedIds.length; i++) {
                    if ($scope.Model.checkedIds[i].RelatedEntityName === dataItem.RelatedEntity && $scope.Model.checkedIds[i].RelatedEntityId === dataItem.RelatedEntityId) {
                        index = i;
                        break;
                    }
                }

                if (index > -1) {
                    $scope.Model.checkedIds.splice(index, 1);
                }

                //-remove selection
                row.removeClass("k-state-selected");
            }
        }

        function checkAll() {
            var grid = $("#delinquentGrid").data("kendoGrid");
            var state = $("#delinquentGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
            var checkbox;
            $scope.Model.checkedIds = [];

            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                checkbox = $this.find("td .checkbox");
                checkbox.prop("checked", state);
                var dataItem = grid.dataItem($this);

                if (state) {
                    $scope.Model.checkedIds.push({
                        RelatedEntityName: dataItem.RelatedEntity,
                        RelatedEntityId: dataItem.RelatedEntityId
                    });

                    //-select the row
                    $this.addClass("k-state-selected");
                } else {
                    //-remove selection
                    $this.removeClass("k-state-selected");
                }
            });
        }

        $scope.viewHistory = function (relatedEntity, relatedEntityId) {
            $state.go("ViewHistory", { RelatedEntity: relatedEntity, RelatedEntityId: relatedEntityId });
        }

        $scope.sendDelinquentEmail = function (relatedEntity, relatedEntityId) {

            $http.post('Dashboard/People/DelinquentEmail', { entityName: relatedEntity, id: relatedEntityId })
                .success(function (data) {
                    emailService.set(data);
                    $state.go("SendEmail");
                });
        }

        $scope.sendDelinquentEmails = function () {

            $http.post('Dashboard/People/DelinquentEmails', { entities: $scope.Model.checkedIds })
                .success(function (data) {
                    if (data.Status === true) {
                        emailService.set(data.Data);
                        $state.go("SendEmail");
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }
    }]);
})();

