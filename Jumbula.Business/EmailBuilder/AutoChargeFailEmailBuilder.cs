﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Jumbula.Business.EmailBuilder
{
    public class AutoChargeFailEmailBuilder : BaseEmailBuilder<AutoChargeFailEmailParameterModel>
    {

        #region Constants
        private const string AdminSubCategory = "Admin";
        private const string ParentSubCategory = "Parent";
        private const string EmailTemplateFileName = "InstallmentAutoChargePaymentFailure";

        private const string Subject = " Auto-charge payment failure";
        private const string AdminSubjectSplicer = "for";

        private const string PreviewProgramName = "Advanced chess class";
        private const string PreviewConfirmationId = "7A53-79-3AF0";
        private const string PreviewPlayerName = "John Doe";
        private const string PreviewSeasonName = "Summer2019";
        private const decimal PreviewAmount = 200;

        private const string NextAttemptDateBodyParameter = "NextAttemptDate";
        #endregion

        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private readonly IClubSettingBusiness _clubSettingBusiness;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;

        private OrderInstallment _orderInstallment;
        private TransactionActivity _transactionActivity;
        private List<string> _clubOwnerEmails;
        private bool _sendToParent;
        private bool _sendToAdmin;
        private JbUser _user;
        private int _attemptNumber;
        private AutoChargePolicy _autoChargePolicy;

        #endregion

        #region Constructors

        public AutoChargeFailEmailBuilder(IClubBusiness clubBusiness, IOrderInstallmentBusiness orderInstallmentBusiness, IClubSettingBusiness clubSettingBusiness, IOrderItemBusiness orderItemBusiness)
        {
            _clubBusiness = clubBusiness;
            _clubSettingBusiness = clubSettingBusiness;
            _orderInstallmentBusiness = orderInstallmentBusiness;
            _orderItemBusiness = orderItemBusiness;
        }
        #endregion

        #region Properties
        public override bool ShouldSendToSupport => true;

        public override EmailCategory Category => EmailCategory.AutoChargeFail;
        public override string TemplateName => EmailTemplateFileName;

        public override IEnumerable<string> SubCategories => new List<string> { ParentSubCategory, AdminSubCategory };

        public override EmailCallerType CallerType => EmailCallerType.AutoCharge;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.Low;
        #endregion

        #region Public Methods
        public override void Initiate(AutoChargeFailEmailParameterModel parameters)
        {
            _orderInstallment = _orderInstallmentBusiness.Get(parameters.InstallmentId);

            var club = _orderInstallment.OrderItem.Order.Club;
            _clubOwnerEmails = _clubBusiness.ClubOwnerEmailList(club);

            _autoChargePolicy = _clubSettingBusiness.ReadAutoChargePolicy(club);
            _user = _orderInstallment.OrderItem.Order.User;
            _transactionActivity = _orderInstallment.TransactionActivities.SingleOrDefault(t => t.Id == parameters.TransactionActivityId);

            _attemptNumber = parameters.AttemptNumber;
            _sendToParent = ShouldSendToParent(parameters.AttemptNumber);
            _sendToAdmin = ShouldSendToAdmin(parameters.AttemptNumber);
            Parameters = parameters;
        }

        public override void InitiatePreview(AutoChargeFailEmailParameterModel parameters)
        {
            var club = _clubBusiness.Get(parameters.ClubId);

            _autoChargePolicy = _clubSettingBusiness.ReadAutoChargePolicy(club);
            _attemptNumber = parameters.AttemptNumber;
            Parameters = parameters;
        }

        public override string GetSubject(string subCategory = null)
        {
            var result = Subject;

            if (subCategory == AdminSubCategory)
                result = $"{result} {AdminSubjectSplicer} {_user.UserName}";

            return result;
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            return new MailAddress(Constants.W_NoReplyEmailAddress, _orderInstallment.OrderItem.Order.Club.Name);
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return _orderInstallment.OrderItem.Order.Club.Name;
        }

        public override string GetReplyTo(string subCategory = null)
        {
            var result = Constants.W_NoReplyEmailAddress;

            if (subCategory == ParentSubCategory && _autoChargePolicy.HasReplyTo)
                result = _autoChargePolicy.ReplyToEmailAddress;

            return result;
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            var result = new List<MailAddress>();

            if (subCategory == ParentSubCategory && _sendToParent)
                result.Add(new MailAddress(_orderInstallment.OrderItem.Order.User.UserName));

            if (subCategory == AdminSubCategory && _sendToAdmin)
                result = _clubOwnerEmails.Select(e => new MailAddress(e)).ToList();

            return result;
        }

        public override IEnumerable<MailAddress> GetCCs(string subCategory = null)
        {
            return new List<MailAddress>();
        }


        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            var order = _orderInstallment.OrderItem.Order;
            var club = order.Club;

            var result = new AutoChargeFailEmailBodyModel
            {
                ClubName = club.Name,
                TransactionId = _transactionActivity != null ? _transactionActivity.PaymentDetail.TransactionId : string.Empty,
                ErrorMessage = _transactionActivity != null ? _transactionActivity.PaymentDetail.TransactionMessage : string.Empty,
                ClubCurrency = club.Currency,
                ClubEmail = club.ContactPersons.Any() ? club.ContactPersons.First().Email : string.Empty,
                ClubPhone = club.ContactPersons.Any() ? PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.First().Phone) : string.Empty,
                ClubDomain = club.Domain,
                SeasonDomain = _orderInstallment.OrderItem.Season.Domain,
                PlayerFullName = _orderInstallment.OrderItem.FullName,
                InstallmentDate = _transactionActivity != null ? _transactionActivity.TransactionDate.ToString(Constants.DefaultDateFormat) : string.Empty,
                BodyMailTitle = RepairBodyHtmlEditorTags(GetFailTryMessage(_attemptNumber, subCategory)),
                ClubImageUrl = _clubBusiness.GetClubLogo(club),
                ReservationItems = new List<AutoChargeEmailBodyItemModel>
                {
                    new AutoChargeEmailBodyItemModel(club.Currency)
                    {
                        EventName = (_orderInstallment.OrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || _orderInstallment.OrderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)? _orderItemBusiness.GetSubscriptionSchedulePartDateRange(_orderInstallment) : _orderInstallment.OrderItem.Name,
                        ConfirmationId = _orderInstallment.OrderItem.Order.ConfirmationId,
                        Price = _orderInstallment.TransactionActivities.Any()
                            ? _orderInstallment.TransactionActivities.Last().Amount
                            : 0
                    }
                },
                IsAdminMode = subCategory != null && subCategory.Equals(AdminSubCategory),
                PreviewMode = previewMode
            };

            if (_transactionActivity?.PaymentDetail.CreditCardId != null)
            {
                result.LastDigits = _transactionActivity.PaymentDetail.CreditCard.LastDigits;
            }

            return result;
        }

        public override object BuildPreviewBodyModel(AutoChargeFailEmailParameterModel parameters, string subCategory)
        {
            var club = _clubBusiness.Get(parameters.ClubId);

            return new AutoChargeFailEmailBodyModel
            {
                ClubName = club.Name,
                TransactionId = "43585",
                ErrorMessage = EnumHelper.GetEnumDescription(StripeErrorMessage.CardExpired),
                ClubCurrency = CurrencyCodes.USD,
                ClubEmail = club.ContactPersons.Any() ? club.ContactPersons.First().Email : string.Empty,
                ClubPhone = club.ContactPersons.Any() ? PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.First().Phone) : string.Empty,
                ClubDomain = club.Domain,
                SeasonDomain = PreviewSeasonName,
                PlayerFullName = PreviewPlayerName,
                InstallmentDate = DateTime.UtcNow.ToString(Constants.DefaultDateFormat),
                BodyMailTitle = RepairBodyHtmlEditorTags(parameters.FailMessage),
                ClubImageUrl = _clubBusiness.GetClubLogo(club),
                ReservationItems = new List<AutoChargeEmailBodyItemModel>
                {
                    new AutoChargeEmailBodyItemModel(CurrencyCodes.USD)
                    {
                        EventName = PreviewProgramName,
                        ConfirmationId = PreviewConfirmationId,
                        Price = PreviewAmount
                    }
                },
                IsAdminMode = subCategory?.Equals(AdminSubCategory) ?? false,
                PreviewMode = EmailBodyGenerationMode.SamplePreview
            };
        }

        public override List<CommunicationHistory> GetHistories()
        {
            return new List<CommunicationHistory>
            {
                new CommunicationHistory{ RelatedEntity = nameof(OrderInstallment), RelatedEntityId = _orderInstallment.Id}
            };
        }

        public override string ManipulateBodyParameter(string parameter)
        {

            if (parameter.Equals(NextAttemptDateBodyParameter) || Regex.Match(parameter, "date <((.+?)) days>").Success)
            {
                var intervalDays = 0;

                if (parameter.Equals(NextAttemptDateBodyParameter))
                {
                    var nextAttemptNumber = _attemptNumber + 1;
                    var nextAttemptPolicy = _autoChargePolicy.Attempts.SingleOrDefault(a => a.AttemptOrder == nextAttemptNumber);

                    if (nextAttemptPolicy != null)
                        intervalDays = Parameters.NextAttemptIntervalDays ?? nextAttemptPolicy.IntervalDays;
                }
                else if (Regex.Match(parameter, @"date <((.+?)) days>").Success)
                {
                    try
                    {
                        intervalDays = Regex.Matches(parameter, @"date <((.+?)) days>")
                            .Cast<Match>()
                            .Select(m => int.Parse(m.Groups[1].Value))
                            .Single();
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                    return null;

                if (intervalDays == 0)
                    return parameter;

                var nextAttemptDate = DateTime.UtcNow.AddDays(intervalDays);

                if (_transactionActivity != null)
                    nextAttemptDate = _transactionActivity.TransactionDate.AddDays(intervalDays);

                return nextAttemptDate.ToString(Constants.DefaultDateFormat);
            }

            return null;
        }

        #endregion

        #region Private Methods
        private string GetFailTryMessage(int attemptNumber, string subCategory)
        {
            if (subCategory.Equals(ParentSubCategory) || subCategory.Equals(Common.Constants.Email.SupportSubCategory))
                return _autoChargePolicy.Attempts.Single(r => r.AttemptOrder == attemptNumber).ParentEmailTemplate;

            if (subCategory.Equals(AdminSubCategory))
                return _autoChargePolicy.Attempts.Single(r => r.AttemptOrder == attemptNumber).AdminEmailTemplate;

            return null;
        }

        private bool ShouldSendToParent(int attemptNumber)
        {
            return _autoChargePolicy.Attempts.Single(r => r.AttemptOrder == attemptNumber).FailedEmailToParent;
        }

        private bool ShouldSendToAdmin(int attemptNumber)
        {
            return _autoChargePolicy.Attempts.Single(r => r.AttemptOrder == attemptNumber).FailedEmailToAdmin;
        }

        private string RepairBodyHtmlEditorTags(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
                return text.Replace("<p>", string.Empty).Replace("</p>", "</br>");

            return text;
        }
        #endregion
    }
}
