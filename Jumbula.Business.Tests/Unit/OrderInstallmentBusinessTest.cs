﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;


namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class OrderInstallmentBusinessTest
    {
        #region Fields
        private Mock<IProgramBusiness> _mockProgramBusiness;
        private Mock<IUserProfileBusiness> _mockUserProfileBusiness;
        private Mock<ISeasonBusiness> _mockSeasonBusiness;
        private Mock<ISubscriptionBusiness> _mockSubscriptionBusiness;
        private Mock<IPaymentPlanBusiness> _mockPaymentPlanBusiness;
        private Mock<IOrderItemBusiness> _mockOrderItemBusiness;
        private Mock<IRepository<OrderInstallment, long>> _mockOrderInstallmentRepository;
        private Mock<IEntitiesContext> _mockDataContext;
        private Mock<IChargeBusiness> _mockChargeBusiness;
        private Mock<IClubBusiness> _mockClubBusiness;
        private Mock<IOrderHistoryBusiness> _mockOrderHistoryBusiness;
        private Mock<IPaymentBusiness> _mockPaymentBusiness;
        private Mock<ITransactionActivityBusiness> _mockTransactionActivityBusiness;

        private const long orderItemId = 1;
        private const long ProgramScheduleId = 1;
        private readonly List<long> _orderInstallmentIds = new List<long> { 1, 10, 12 };
        private const long Id = 10;
        #endregion

        [TestMethod]
        public void GetAllInstallmentsByDueDate_PassAllConditoins_shouldReturnInstallment()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(CreateInstallment().AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentDateGreaterThanDueDate_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            installments.ElementAt(0).InstallmentDate = new DateTime(2018, 12, 2);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentDateLessThanAutoChargeStartPoint_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            installments.ElementAt(0).InstallmentDate = new DateTime(2016, 5, 15);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentStatusNotOrNotinitiated_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            installments.ElementAt(0).Status = OrderStatusCategories.canceled;

            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentAmountEqualZero_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            installments.ElementAt(0).Amount = 0;

            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentAmountGreaterThanZeroAndPaidAmountLessThanZero_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            installments.ElementAt(0).Amount = 100;
            installments.ElementAt(0).PaidAmount = -100;
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentBalaceLessThanZero_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            installments.ElementAt(0).Amount = 10;
            installments.ElementAt(0).PaidAmount = 20;

            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentIsDeleted_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            installments.ElementAt(0).IsDeleted = true;


            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        // EnableReminder means IsManual
        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentIsManual_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            // EnableReminder means IsManual
            installments.ElementAt(0).EnableReminder = true;

            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentOrderItemStatusNotEqualCompeleted_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            // EnableReminder means IsManual
            installments.ElementAt(0).OrderItem.ItemStatus = OrderItemStatusCategories.deleted;

            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentOrderItemPaymentPlanStatusNotEqualStarted_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            // EnableReminder means IsManual
            installments.ElementAt(0).OrderItem.PaymentPlanStatus = PaymentPlanStatus.Stopped;

            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentOrderIsNotAutoCharge_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            // EnableReminder means IsManual
            installments.ElementAt(0).OrderItem.Order.IsAutoCharge = false;

            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetAllInstallmentsByDueDate_InstallmentOrderIsNotLive_ShouldReturnEmptyList()
        {
            //arrange
            var service = CreateService();
            var dueDate = new DateTime(2018, 12, 1);
            var installments = CreateInstallment();
            // EnableReminder means IsManual
            installments.ElementAt(0).OrderItem.Order.IsLive = false;

            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(installments.AsQueryable);

            //act
            var result = service.GetAllInstallmentsByDueDate(dueDate);

            //assert
            result.Count().Should().Be(0);
        }


        [TestMethod]
        public void CheckIsPayable_BalanceIsMoreThanZero_ShouldReturnTrue()
        {
            //Arrange
            var service = CreateService();

            var orderItem = FactoryGirl.Build<OrderItem>(p => p.Order.Id = 1);
            var installment = FactoryGirl.Build<OrderInstallment>(o =>
            {
                o.Id = 1;
                o.Amount = 300;
                o.PaidAmount = 100;
                o.OrderItem = orderItem;
            });

            //Act
            var result = service.CheckIsPayable(installment);

            //Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void CheckIsPayable_BalanceIsLessThanZero_ShouldReturnFalse()
        {
            //Arrange
            var service = CreateService();

            var orderItem = FactoryGirl.Build<OrderItem>(p => p.Order.Id = 1);
            var installment = FactoryGirl.Build<OrderInstallment>(o =>
            {
                o.Id = 1;
                o.Amount = 300;
                o.PaidAmount = 350;
                o.OrderItem = orderItem;
            });

            //Act
            var result = service.CheckIsPayable(installment);

            //Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void GetOrderItemInstallments_OrderItemIdIncludeOrderInstallments_ShouldReturnValidResult()
        {
            // Arrange
            var orderItem = FactoryGirl.Build<OrderItem>(p => p.Order.Id = 1);
            var service = CreateService();
            var orderInstallments = CreateOrderInstallments();
            _mockOrderItemBusiness.Setup(x => x.GetItem(orderItemId)).Returns(orderItem);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable());

            // Act
            var result = service.GetOrderItemInstallments(orderItemId, null, 0, 10, RoleCategoryType.Parent, null);

            // Assert
            result.Count.Should().Be(orderInstallments.Where(o => !o.IsDeleted).Count());

        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsComplatedClassAndInstallmentsIsDeletedIsFalseAndAmountMoreThanZeroAndStatusIsInitiated_ShouldReturnCountTwo()
        {
            // Arrange

            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = false;
            orderItems.ElementAt(2).Installments.First().Amount = 200;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.initiated;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            orderItems.ElementAt(2).Installments.ElementAt(1).IsDeleted = false;
            orderItems.ElementAt(2).Installments.ElementAt(1).Amount = 200;
            orderItems.ElementAt(2).Installments.ElementAt(1).Status = OrderStatusCategories.initiated;
            orderItems.ElementAt(2).Installments.ElementAt(1).OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(2);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsComplatedClassAndInstallmentsIsDeletedIsFalseAndAmountIsZeroAndPaidAmountMoreThanZeroAndStatusIsInitiated_ShouldReturnCountTwo()
        {
            // Arrange

            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = false;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.initiated;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            orderItems.ElementAt(2).Installments.ElementAt(1).IsDeleted = false;
            orderItems.ElementAt(2).Installments.ElementAt(1).Amount = 0;
            orderItems.ElementAt(2).Installments.ElementAt(1).PaidAmount = 50;
            orderItems.ElementAt(2).Installments.ElementAt(1).Status = OrderStatusCategories.initiated;
            orderItems.ElementAt(2).Installments.ElementAt(1).OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(2);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsComplatedClassAndInstallmentsIsDeletedIsTrueAndPaidAmountMoreThanZeroAndStatusIsInitiated_ShouldReturnCountTwo()
        {
            // Arrange
            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = true;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.initiated;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            orderItems.ElementAt(2).Installments.ElementAt(1).IsDeleted = true;
            orderItems.ElementAt(2).Installments.ElementAt(1).Amount = 0;
            orderItems.ElementAt(2).Installments.ElementAt(1).PaidAmount = 50;
            orderItems.ElementAt(2).Installments.ElementAt(1).Status = OrderStatusCategories.initiated;
            orderItems.ElementAt(2).Installments.ElementAt(1).OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(2);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsComplatedClassAndInstallmentsIsDeletedIsTrueAndPaidAmountMoreThanZeroAndStatusIsError_ShouldReturnCountOne()
        {
            // Arrange
            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = true;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.error;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;


            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(1);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsComplatedClassAndInstallmentsIsDeletedIsTrueAndPaidAmountMoreThanZeroAndStatusIsComplated_ShouldReturnCountTwo()
        {
            // Arrange
            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = true;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.completed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            orderItems.ElementAt(2).Installments.ElementAt(1).IsDeleted = true;
            orderItems.ElementAt(2).Installments.ElementAt(1).Amount = 0;
            orderItems.ElementAt(2).Installments.ElementAt(1).PaidAmount = 50;
            orderItems.ElementAt(2).Installments.ElementAt(1).Status = OrderStatusCategories.completed;
            orderItems.ElementAt(2).Installments.ElementAt(1).OrderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.completed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(2);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsCanceledClassAndInstallmentsIsDeletedIsTrueAndPaidAmountMoreThanZeroAndStatusIsChanged_ShouldReturnCountZero()
        {
            // Arrange
            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = true;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.canceled;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatusReason = OrderItemStatusReasons.canceled;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            orderItems.ElementAt(2).Installments.ElementAt(1).IsDeleted = true;
            orderItems.ElementAt(2).Installments.ElementAt(1).Amount = 0;
            orderItems.ElementAt(2).Installments.ElementAt(1).PaidAmount = 50;
            orderItems.ElementAt(2).Installments.ElementAt(1).OrderItem.ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).Installments.ElementAt(1).OrderItem.ItemStatusReason = OrderItemStatusReasons.canceled;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Class;

            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsCanceledBeforeAfterAndNotAnyOrderSessionsAndInstallmentsIsDeletedIsTrueAndPaidAmountMoreThanZeroAndStatusIsChanged_ShouldReturnCountZero()
        {
            // Arrange
            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = true;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.canceled;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatusReason = OrderItemStatusReasons.canceled;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.BeforeAfterCare;

            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsCanceledBeforeAfterAndHasOrderSessionsAndInstallmentsIsDeletedIsTrueAndPaidAmountMoreThanZeroAndStatusIsCompleted_ShouldReturnCountOne()
        {
            // Arrange
            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = true;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.completed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatusReason = OrderItemStatusReasons.canceled;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ProgramTypeCategory = ProgramTypeCategory.BeforeAfterCare;
            orderItems.ElementAt(2).Installments.First().OrderItem.OrderSessions = CreateOrderSession();
            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(1);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsCanceledSubscriptionAndNotAnyOrderSessionsAndInstallmentsIsDeletedIsTrueAndPaidAmountMoreThanZeroAndStatusIsChanged_ShouldReturnZeroCount()
        {
            // Arrange
            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = true;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.canceled;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatusReason = OrderItemStatusReasons.canceled;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).ProgramTypeCategory = ProgramTypeCategory.Subscription;

            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public void GetCalendarOrderItemInstallments_OrderItemIsCanceledSubscriptionAndHasOrderSessionsAndInstallmentsIsDeletedIsTrueAndPaidAmountMoreThanZeroAndStatusIsCompleted_ShouldReturnOneCount()
        {
            // Arrange
            var service = CreateService();
            var orderItems = CreateOrderItems();

            orderItems.ElementAt(2).Installments.First().IsDeleted = true;
            orderItems.ElementAt(2).Installments.First().Amount = 0;
            orderItems.ElementAt(2).Installments.First().PaidAmount = 100;
            orderItems.ElementAt(2).Installments.First().Status = OrderStatusCategories.completed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ItemStatusReason = OrderItemStatusReasons.canceled;
            orderItems.ElementAt(2).ItemStatus = OrderItemStatusCategories.changed;
            orderItems.ElementAt(2).Installments.First().OrderItem.ProgramTypeCategory = ProgramTypeCategory.Subscription;
            orderItems.ElementAt(2).Installments.First().OrderItem.OrderSessions = CreateOrderSession();
            List<OrderItem> orderItemsTest = new List<OrderItem> { orderItems.ElementAt(2), orderItems.ElementAt(3) };

            // Act
            var result = service.GetCalendarOrderItemInstallments(orderItemsTest).ToList();

            // Assert
            result.Count.Should().Be(1);
        }

        [TestMethod]
        public void GetOrderInstallments_GetListWithoutInputIds_ShouldReturnZeroCount()
        {
            // Arrange
            var service = CreateService();
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(FillOrderInstallmentsWithoutValidIds().AsQueryable);

            // Act
            var result = service.GetOrderInstallments(_orderInstallmentIds).ToList();

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public void GetOrderInstallments_GetListWithoutInputIds_ShouldReturnValidCount()
        {
            // Arrange
            var service = CreateService();
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(FillOrderInstallmentsWithValidIds().AsQueryable);

            // Act
            var result = service.GetOrderInstallments(_orderInstallmentIds).ToList();

            // Assert
            result.Count.Should().Be(_orderInstallmentIds.Count);
        }

        [TestMethod]
        public void GetDelinquentOrderInstallmentItems_PartnerIdEqualSpeciallyClubId_ShouldBeHaveTwoResult()
        { // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentWithPartnerId(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_InstallmentDateIsFuture_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentAllDueDateIsFuture(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_DueDateIsToday_ShouldReturnCountBeOne()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentDueDateIsToday(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_InstallmentDateIsPastAndAmountIsLowerToAndAttemptNumberIsGreaterThanAutoChargeFailAttemptNumber_ShouldReturnCountBeOne()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentDueDateIsPast(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_InstallmentAmountEqualToPaidAmount_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentAmountEqualToPaidAmount(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_InstallmentAmountLowerThanPaidAmount_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentAmountLowerThanPaidAmount(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_EnableReminderIsTrue_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentEnableReminderIsTrue(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_ProgramScheduleIdIsNull_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentProgramScheduleIdIsNull(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_InstallmentDeletedIsTrue_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentInstallmentDeletedIsTrue(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_PaymentPlanTypeIsNotInstallment_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentPaymentPlanTypeIsNotInstallment(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_AutoChargeIsFalse_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentAutoChargeIsFalse(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_IsLiveIsFalse_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentLiveIsFalse(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_AttemptIsLowerThanAutoChargeFailAttemptNumber_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentAttemptIsLowerThanAutoChargeFailAttemptNumber(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_AttemptIsEqualToAutoChargeFailAttemptNumber_ShouldReturnCountBeOne()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentAttemptIsEqualToAutoChargeFailAttemptNumber(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_AttemptIsGreaterThanAutoChargeFailAttemptNumber_ShouldReturnCountBeOne()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentAttemptIsGreaterThanAutoChargeFailAttemptNumber(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_PaidAmountIsNull_ShouldReturnCountBeOne()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentPaidAmountIsNull(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_InstallmentAmountGreaterThanPaidAmount_ShouldReturnCountBeOne()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallmentInstallmentAmountLowerThanPaidAmount(ProgramTypeCategory.Camp, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_orderItemStatusIsDeleted_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallments(ProgramTypeCategory.Camp, OrderItemStatusCategories.deleted, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_orderItemStatusIsInitiated_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallments(ProgramTypeCategory.Camp, OrderItemStatusCategories.initiated, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentInstallmentItemsAsNoTracking_orderItemStatusIsNotInitiated_ShouldReturnCountBeZero()
        {
            // Arrange
            var service = CreateService();
            var orderInstallments = FillOrderInstallments(ProgramTypeCategory.Camp, OrderItemStatusCategories.notInitiated, OrderItemStatusReasons.transferIn);
            _mockOrderInstallmentRepository.Setup(x => x.GetList()).Returns(orderInstallments.AsQueryable);
            // Act
            var result = service.GetDelinquentInstallmentItemsAsNoTracking(ClubId, MaxAttempts);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void Refund_InstallmentIsNotNullRefundAmountMoreThanZeroFullRefundByAddToCredit_AddRefundAmountToLiveFamilyCreditSaveTransactionSuccessAndInstallmentStatusRefunded()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 20m;
            var refundedAmount2 = 0m;
            var updateResult = new OperationStatus {Status = true};

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });


            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Credit;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";

            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 20;

            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            installment.OrderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Order.IsLive = true;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;

            });

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 20,
                Mode = RefundMode.AddToCredit,
                IsSelected = true,
                Title = "Transfer the balance to the family credit"
            };

            var modelRefund = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 20,
                RefundNote = null
            };

            var modes = new List<RefundInformation> { mode1 };

            var model = new OrderItemsViewModel
            {
                Installments = CreateInstallments(),
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,

            };
            
            var historyDescription =
                $"The amount $20.00 is refunded from the {DateTimeHelper.FormatDate(installment.InstallmentDate.Date)} installment.";
            var status = true;

            _mockOrderItemBusiness.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);
            _mockOrderItemBusiness.Setup(x => x.SetRefundTransaction(installment.OrderItem, installment.Id, It.IsAny<PaymentDetail>(), modelRefund.TransactionStatus, modelRefund.RefundAmount, It.IsAny<DateTime>(), modelRefund.RefundNote, HandleMode.Online)).Returns(transaction);

            _mockOrderItemBusiness.Setup(x => x.UpdateRefund(status, installment.OrderItem, refundedAmount, model, historyDescription)).Returns(updateResult);

            //Act
            var result = service.Refund(model, ref refundedAmount2);

            //Assert
            result.Status.Should().Be(true);
            installment.Status.Should().Be(OrderStatusCategories.refunded);

            installment.TransactionActivities.Count(t => t.Amount == refundedAmount2
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Credit
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == model.CurrentUserName).Should().Be(1);

        }

        [TestMethod]
        public void Refund_InstallmentIsNotNullRefundAmountMoreThanZeroFullRefundByAddToTestCredit_AddRefundAmountToTestFamilyCreditSaveTransactionSuccessAndInstallmentStatusRefunded()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 20m;
            var refundedAmount2 = 0m;
            var updateResult = new OperationStatus {Status = true};

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Credit;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";

            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 20;

            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            installment.OrderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Order.IsLive = false;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;

            });

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 20,
                Mode = RefundMode.AddToCredit,
                IsSelected = true,
                Title = "Transfer the balance to the family credit"
            };

            var modelRefund = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 20,
                RefundNote = null
            };

            var modes = new List<RefundInformation> { mode1 };

            var model = new OrderItemsViewModel()
            {
                Installments = CreateInstallments(),
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,

            };

            var historyDescription =
                $"The amount $20.00 is refunded from the {DateTimeHelper.FormatDate(installment.InstallmentDate.Date)} installment.";
            var status = true;

            _mockOrderItemBusiness.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);
            _mockOrderItemBusiness.Setup(x => x.SetRefundTransaction(installment.OrderItem, installment.Id, It.IsAny<PaymentDetail>(), modelRefund.TransactionStatus, modelRefund.RefundAmount, It.IsAny<DateTime>(), modelRefund.RefundNote, HandleMode.Online)).Returns(transaction);
            _mockOrderItemBusiness.Setup(x => x.UpdateRefund(status, installment.OrderItem, refundedAmount, model, historyDescription)).Returns(updateResult);

            //Act
            var result = service.Refund(model, ref refundedAmount2);

            //Assert
            result.Status.Should().Be(true);

            installment.Status.Should().Be(OrderStatusCategories.refunded);

            installment.TransactionActivities.Count(t => t.Amount == refundedAmount2
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Credit
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == model.CurrentUserName).Should().Be(1);

        }

        [TestMethod]
        public void Refund_InstallmentIsNotNullRefundAmountMoreThanZeroPartiallyRefundByAddToFamilyCredit_SaveTransactionSuccessAndInstallmentStatusNotRefunded()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 10m;
            var refundedAmount2 = 0m;
            var updateResult = new OperationStatus {Status = true};

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Credit;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";

            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;

            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            installment.OrderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Order.IsLive = true;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;

            });

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.AddToCredit,
                IsSelected = true,
                Title = "Transfer the balance to the family credit"
            };

            var modelRefund = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 10,
                RefundNote = null
            };

            var modes = new List<RefundInformation> { mode1 };

            var model = new OrderItemsViewModel()
            {
                Installments = CreateInstallments(),
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,

            };
            var historyDescription =
                $"The amount $10.00 is refunded from the {DateTimeHelper.FormatDate(installment.InstallmentDate.Date)} installment.";
            var status = true;

            _mockOrderItemBusiness.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);
            _mockOrderItemBusiness.Setup(x => x.SetRefundTransaction(installment.OrderItem, installment.Id, It.IsAny<PaymentDetail>(), modelRefund.TransactionStatus, modelRefund.RefundAmount, It.IsAny<DateTime>(), modelRefund.RefundNote, HandleMode.Online)).Returns(transaction);
            _mockOrderItemBusiness.Setup(x => x.UpdateRefund(status, installment.OrderItem, refundedAmount, model, historyDescription)).Returns(updateResult);

            //Act
            var result = service.Refund(model, ref refundedAmount2);

            //Assert
            result.Status.Should().Be(true);

            installment.Status.Should().NotBe(OrderStatusCategories.refunded);

            installment.TransactionActivities.Count(t => t.Amount == refundedAmount2
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Credit
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == model.CurrentUserName).Should().Be(1);

        }

        [TestMethod]
        public void Refund_InstallmentIsNotNullRefundAmountMoreThanZeroFullRefundByManually_SaveTransactionSuccessAndInstallmentStatusRefundedAndPaymentMethodCash()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 20m;
            var refundedAmount2 = 0m;
            var updateResult = new OperationStatus {Status = true};

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Cash;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";

            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 20;
            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            installment.OrderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Order.IsLive = true;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 20,
                Mode = RefundMode.Manually,
                IsSelected = true,
                Title = "My organization will issue the refund manually"
            };

            var mode2 = new RefundInformation()
            {
                Priority = 1,
                Amount = 20,
                Mode = RefundMode.AddToCredit,
                IsSelected = false,
                Title = "Transfer the balance to the family credit"
            };

            var modelRefund = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 20,
                RefundNote = null
            };

            var modes = new List<RefundInformation> { mode1, mode2 };

            var model = new OrderItemsViewModel()
            {
                Installments = CreateInstallments(),
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,

            };
            var historyDescription =
                $"The amount $20.00 is refunded from the {DateTimeHelper.FormatDate(installment.InstallmentDate.Date)} installment.";
            var status = true;

            _mockOrderItemBusiness.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);
            _mockOrderItemBusiness.Setup(x => x.SetRefundTransaction(installment.OrderItem, installment.Id, It.IsAny<PaymentDetail>(), modelRefund.TransactionStatus, modelRefund.RefundAmount, It.IsAny<DateTime>(), modelRefund.RefundNote, HandleMode.Online)).Returns(transaction);
            _mockOrderItemBusiness.Setup(x => x.UpdateRefund(status, installment.OrderItem, refundedAmount, model, historyDescription)).Returns(updateResult);

            //Act
            var result = service.Refund(model, ref refundedAmount2);

            //Assert
            result.Status.Should().Be(true);

            installment.Status.Should().Be(OrderStatusCategories.refunded);

            installment.TransactionActivities.Count(t => t.Amount == refundedAmount2
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Cash
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == model.CurrentUserName).Should().Be(1);

        }

        [TestMethod]
        public void Refund_InstallmentIsNotNullRefundAmountMoreThanZeroPartiallyRefundByManually_SaveTransactionSuccessAndInstallmentStatusNotRefunded()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 10m;
            var refundedAmount2 = 0m;
            var updateResult = new OperationStatus {Status = true};

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Cash;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";

            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;

            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            installment.OrderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Order.IsLive = true;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.Manually,
                IsSelected = true,
                Title = "My organization will issue the refund manually"
            };

            var mode2 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.AddToCredit,
                IsSelected = false,
                Title = "Transfer the balance to the family credit"
            };

            var modelRefund = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 10,
                RefundNote = null
            };

            var modes = new List<RefundInformation> { mode1, mode2 };

            var model = new OrderItemsViewModel()
            {
                Installments = CreateInstallments(),
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };
            var historyDescription =
                $"The amount $10.00 is refunded from the {DateTimeHelper.FormatDate(installment.InstallmentDate.Date)} installment.";
            var status = true;

            _mockOrderItemBusiness.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);
            _mockOrderItemBusiness.Setup(x => x.SetRefundTransaction(installment.OrderItem, installment.Id, It.IsAny<PaymentDetail>(), modelRefund.TransactionStatus, modelRefund.RefundAmount, It.IsAny<DateTime>(), modelRefund.RefundNote, HandleMode.Online)).Returns(transaction);
            _mockOrderItemBusiness.Setup(x => x.UpdateRefund(status, installment.OrderItem, refundedAmount, model, historyDescription)).Returns(updateResult);

            //Act
            var result = service.Refund(model, ref refundedAmount2);

            //Assert
            result.Status.Should().Be(true);

            installment.Status.Should().NotBe(OrderStatusCategories.refunded);

            installment.TransactionActivities.Count(t => t.Amount == refundedAmount2
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Cash
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == model.CurrentUserName).Should().Be(1);

        }

        [TestMethod]
        public void Refund_InstallmentIsNotNullRefundAmountMoreThanZeroPartiallyRefundByAutomaticallyByStripe_SaveTransactionSuccessAndInstallmentStatusNotRefundedAndPaymentDetailCategoryIsCard()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 10m;
            var refundedAmount2 = 0m;
            var updateResult = new OperationStatus();
            updateResult.Status = true;
            var refunded = new List<TransactionActivity>();
            var isPaypalTransaction = false;
            var isTestMode = true;


            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";

            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;
            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            installment.OrderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Order.IsLive = false;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var orderItem = installment.OrderItem;
            var orderInstallment = installment;

            var paymentdetails = new PaymentDetailModel()
            {
                Amount = 20,
                AmountRefunded = 0,
                Status = true,
            };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.Automatically,
                IsSelected = true,
                Title = "Issue the refund automatically"
            };

            var mode2 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.AddToCredit,
                IsSelected = false,
                Title = "Transfer the balance to the family credit"
            };

            var modelRefund = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 10,
                RefundNote = null,
                Status = true,
            };
            var club = user1.Club;

            var refundAmount = modelRefund.RefundAmount;

            var modes = new List<RefundInformation> { mode1, mode2 };

            var model = new OrderItemsViewModel()
            {
                Installments = CreateInstallments(),
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };
            var historyDescription =
                $"The amount $10.00 is refunded from the {DateTimeHelper.FormatDate(installment.InstallmentDate.Date)} installment.";
            var status = true;
 
            var isPaypal = false;
            var remainingAmount = refundedAmount;
            var oldRefunded = new List<TransactionActivity>();
            _mockOrderItemBusiness.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);
            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(installment)).Returns(CreateInstallmentTransactions);
            _mockTransactionActivityBusiness.Setup(x => x.GetRefundAutomaticallyTransactions(installment)).Returns(new List<TransactionActivity>());
            _mockPaymentBusiness.Setup(x => x.GetPaymentDetails(orderItem.Order.Club.Domain, transaction.PaymentDetail.PayKey, refunded, isPaypalTransaction, isTestMode)).Returns(paymentdetails);
            _mockPaymentBusiness.Setup(x => x.Refund(It.IsAny<TransactionActivity>(), It.IsAny<List<OrderInstallment>>(), null, club.Domain, refundAmount, !orderInstallment.OrderItem.Order.IsLive)).Returns(modelRefund);
            _mockOrderItemBusiness.Setup(x => x.SetRefundTransaction(installment.OrderItem, installment.Id, It.IsAny<PaymentDetail>(), modelRefund.TransactionStatus, modelRefund.RefundAmount, It.IsAny<DateTime>(), modelRefund.RefundNote, HandleMode.Online)).Returns(transaction);
            _mockOrderItemBusiness.Setup(x => x.UpdateRefund(status, installment.OrderItem, refundedAmount, model, historyDescription)).Returns(updateResult);
            _mockOrderItemBusiness.Setup(x => x.GetRefundAmount(It.IsAny<TransactionActivity>(), It.IsAny<OrderItem>(), remainingAmount, !installment.OrderItem.Order.IsLive, oldRefunded, isPaypal)).Returns(10);

            //Act
            var result = service.Refund(model, ref refundedAmount2);

            //Assert
            result.Status.Should().Be(true);
            
            //installment.PaidAmount.Should().Be(installmentPaidAmount - refundedAmount2);
            installment.Status.Should().NotBe(OrderStatusCategories.refunded);

            installment.TransactionActivities.Count(t => t.Amount == refundedAmount2
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Card
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == model.CurrentUserName).Should().Be(1);
        }

        [TestMethod]
        public void Refund_InstallmentIsNotNullRefundAmountMoreThanZeroFullRefundByAutomaticallyByStripe_StatusIsFalseAndInstallmentStatusNotRefundedAndPaymentDetailCategoryIsCardTransactionstatusFailur()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 10m;
            var refundedAmount2 = 0m;
            var updateResult = new OperationStatus { Status = true };
            var refunded = new List<TransactionActivity>();
            var isPaypalTransaction = false;
            var isTestMode = true;

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.ERROR;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";

            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Failure;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;

            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            installment.OrderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Order.IsLive = false;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;

            });

            var orderItem = installment.OrderItem;
            var orderInstallment = installment;

            var paymentdetails = new PaymentDetailModel()
            {
                Amount = 20,
                AmountRefunded = 0,
                Status = true,
            };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.Automatically,
                IsSelected = true,
                Title = "Issue the refund automatically"
            };

            var mode2 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.AddToCredit,
                IsSelected = false,
                Title = "Transfer the balance to the family credit"
            };

            var modelRefund = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 10,
                RefundNote = null,
                Status = false,
            };
            var club = user1.Club;

            var refundAmount = modelRefund.RefundAmount;

            var modes = new List<RefundInformation> { mode1, mode2 };

            var model = new OrderItemsViewModel()
            {
                Installments = CreateInstallments(),
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,

            };

            var orderItemPaidAmount = installment.OrderItem.PaidAmount;
            var installmentPaidAmount = installment.PaidAmount;
            var historyDescription =
                $"The amount $20.00 is refunded from the {DateTimeHelper.FormatDate(installment.InstallmentDate.Date)} installment.";
            var status = true;
            var isPaypal = false;
            var oldRefunded = new List<TransactionActivity>();
            var remainingAmount = refundedAmount;

            _mockOrderItemBusiness.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);
            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(installment)).Returns(CreateInstallmentTransactions);
            _mockTransactionActivityBusiness.Setup(x => x.GetRefundAutomaticallyTransactions(installment)).Returns(new List<TransactionActivity>());
            _mockPaymentBusiness.Setup(x => x.GetPaymentDetails(orderItem.Order.Club.Domain, transaction.PaymentDetail.PayKey, refunded, isPaypalTransaction, isTestMode)).Returns(paymentdetails);
            _mockPaymentBusiness.Setup(x => x.Refund(It.IsAny<TransactionActivity>(), It.IsAny<List<OrderInstallment>>(), null, club.Domain, refundAmount, !orderInstallment.OrderItem.Order.IsLive)).Returns(modelRefund);
            _mockOrderItemBusiness.Setup(x => x.SetRefundTransaction(installment.OrderItem, installment.Id, It.IsAny<PaymentDetail>(), modelRefund.TransactionStatus, modelRefund.RefundAmount, It.IsAny<DateTime>(), modelRefund.RefundNote, HandleMode.Online)).Returns(transaction);
            _mockOrderItemBusiness.Setup(x => x.UpdateRefund(status, installment.OrderItem, refundedAmount, model, historyDescription)).Returns(updateResult);
            _mockOrderItemBusiness.Setup(x => x.GetRefundAmount(It.IsAny<TransactionActivity>(), It.IsAny<OrderItem>(), remainingAmount, !installment.OrderItem.Order.IsLive, oldRefunded, isPaypal)).Returns(10);

            //Act
            var result = service.Refund(model, ref refundedAmount2);

            //Assert
            result.Status.Should().Be(false);

            installment.OrderItem.PaidAmount.Should().Be(orderItemPaidAmount);
            installment.PaidAmount.Should().Be(installmentPaidAmount);
            installment.Status.Should().NotBe(OrderStatusCategories.refunded);

        }

        [TestMethod]
        public void GetInstallmentToBeRefund_OneInstallmentIsSelectedTrue_ReturnOneInstallmentValid()
        {
            //Arrange
            var service = CreateService();

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var result1 = new ParentOrderItemInstallmentsViewModel() { Id = 1, Amount = 20, IsSelected = true, IsDeleted = true };
            var result2 = new ParentOrderItemInstallmentsViewModel() { Id = 2, Amount = 10, IsSelected = false, IsDeleted = true };

            var installmets = new List<ParentOrderItemInstallmentsViewModel> { result1, result2 };

            var mode = new OrderItemsViewModel()
            {
                Installments = installmets,
                AddOnInstallments = null,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);

            //Act
            var result = service.GetInstallmentToBeRefund(mode);


            //Assert
            result.Id.Should().Be(installment.Id);
            result.Amount.Should().Be(installment.Amount);
        }

        [TestMethod]
        public void GetInstallmentToBeRefund_NotAnyInstallmentSelected_ReturnNull()
        {
            //Arrange
            var service = CreateService();

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var result1 = new ParentOrderItemInstallmentsViewModel() { Id = 1, Amount = 20, IsSelected = false, IsDeleted = true };
            var result2 = new ParentOrderItemInstallmentsViewModel() { Id = 2, Amount = 10, IsSelected = false, IsDeleted = true };

            var installmets = new List<ParentOrderItemInstallmentsViewModel> { result1, result2 };

            var mode = new OrderItemsViewModel()
            {
                Installments = installmets,
                AddOnInstallments = installmets,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            _mockOrderInstallmentRepository.Setup(x => x.Get(It.IsAny<Expression<Func<OrderInstallment, bool>>>())).Returns(installment);

            //Act
            var result = service.GetInstallmentToBeRefund(mode);

            //Assert
            result.Should().Be(null);
        }

        [TestMethod]
        public void GetAmountAutomaticallyTransactions_SumTransactionsAmount_ReturnValidAmount()
        {
            //Arrange
            var service = CreateService();
            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var transactions = CreateInstallmentTransactions().Where(t => t.Installment.Id == installment.Id &&
                          t.PaymentDetail != null &&
                         !string.IsNullOrEmpty(t.PaymentDetail.PayKey)
                         && t.TransactionType == TransactionType.Payment
                         && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Pending));

            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(installment)).Returns(transactions);

            //Act
            var result = service.GetAmountAutomaticallyTransactions(installment);

            //Assert
            result.Should().Be(transactions.Where(t => t.Installment.Id == installment.Id &&
                          t.PaymentDetail != null &&
                         !string.IsNullOrEmpty(t.PaymentDetail.PayKey)
                         && t.TransactionType == TransactionType.Payment
                         && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Pending)).Sum(t => t.Amount));
        }

        [TestMethod]
        public void GetRefundModes_CreateListOfRefundModes_ReturnValidList()
        {
            //Arrange
            var service = CreateService();
            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.PaidAmount = 20;
                p.Amount = 20;
                p.InstallmentDate = DateTime.UtcNow;
            });

            var transactions = CreateInstallmentTransactions().Where(t => t.Installment.Id == installment.Id &&
                          t.PaymentDetail != null &&
                         !string.IsNullOrEmpty(t.PaymentDetail.PayKey)
                         && t.TransactionType == TransactionType.Payment
                         && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Pending));

            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(installment)).Returns(transactions);

            //Act
            var result = service.GetRefundModes();

            //Assert
            result.Count().Should().Be(3);
            result.Single(m => m.Priority == 1).Mode.Should().Be(RefundMode.AddToCredit);
            result.Single(m => m.Priority == 2).Mode.Should().Be(RefundMode.Manually);
            result.Single(m => m.Priority == 3).Mode.Should().Be(RefundMode.Automatically);
        }


        #region private Methods

        private List<TransactionActivity> CreateInstallmentTransactions()
        {
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var installment2 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 2);
            var installment3 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 3);

            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 1;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PaymentMethod = PaymentMethod.Card;
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Failure;
                t.Amount = 10;
            });

            var transaction2 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 2;
                t.Installment = installment2;
                t.InstallmentId = 2;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test payKey";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Pending;
                t.Amount = 10;
            });

            var transaction3 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 3;
                t.Installment = installment3;
                t.InstallmentId = 3;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test payKey";
                t.TransactionType = TransactionType.Refund;
                t.TransactionStatus = TransactionStatus.Success;
                t.Amount = 10;
            });

            var transaction4 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 4;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test payKey";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Success;
                t.Amount = 10;
            });

            var transaction5 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 5;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test payKey";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Success;
                t.Amount = 10;
            });


            return new List<TransactionActivity> { transaction1, transaction2, transaction3, transaction4, transaction5 };
        }

        private List<ParentOrderItemInstallmentsViewModel> CreateInstallments()
        {
            var result1 = new ParentOrderItemInstallmentsViewModel() { Id = 1, Amount = 20, IsSelected = true, IsDeleted = true };
            var result2 = new ParentOrderItemInstallmentsViewModel() { Id = 2, Amount = 10, IsSelected = false, IsDeleted = true };

            return new List<ParentOrderItemInstallmentsViewModel> { result1, result2 };

        }

        private List<OrderSession> CreateOrderSession()
        {
            var programSession1 = FactoryGirl.Build<ProgramSession>(p =>
            {
                p.Id = 1;
                p.StartDateTime = DateTime.UtcNow;
                p.EndDateTime = DateTime.UtcNow;
            });

            var orderSession1 = FactoryGirl.Build<OrderSession>(p =>
            {
                p.Id = 1;
                p.OrderItemId = 1;
                p.OrderItem = FactoryGirl.Build<OrderItem>();
                p.OrderItem.Id = 1;
                p.ProgramSession = FactoryGirl.Build<ProgramSession>();
                p.ProgramSession = programSession1;
            });

            var orderSession2 = FactoryGirl.Build<OrderSession>(p =>
            {
                p.Id = 1;
                p.OrderItemId = 11;
                p.OrderItem = FactoryGirl.Build<OrderItem>();
                p.OrderItem.Id = 11;
                p.ProgramSession = FactoryGirl.Build<ProgramSession>();
                p.ProgramSession = programSession1;
            });

            return new List<OrderSession> { orderSession1, orderSession2 };
        }

        private OrderInstallmentBusiness CreateService()
        {
            _mockProgramBusiness = new Mock<IProgramBusiness>();
            _mockUserProfileBusiness = new Mock<IUserProfileBusiness>();
            _mockSeasonBusiness = new Mock<ISeasonBusiness>();
            _mockSubscriptionBusiness = new Mock<ISubscriptionBusiness>();
            _mockPaymentPlanBusiness = new Mock<IPaymentPlanBusiness>();
            _mockOrderItemBusiness = new Mock<IOrderItemBusiness>();
            _mockOrderInstallmentRepository = new Mock<IRepository<OrderInstallment, long>>();
            _mockDataContext = new Mock<IEntitiesContext>();
            _mockChargeBusiness = new Mock<IChargeBusiness>();
            _mockClubBusiness = new Mock<IClubBusiness>();
            _mockOrderHistoryBusiness = new Mock<IOrderHistoryBusiness>();
            _mockPaymentBusiness = new Mock<IPaymentBusiness>();
            _mockTransactionActivityBusiness = new Mock<ITransactionActivityBusiness>();

            return new OrderInstallmentBusiness(_mockProgramBusiness.Object,
               _mockUserProfileBusiness.Object,
                _mockSeasonBusiness.Object,
               _mockSubscriptionBusiness.Object,
               _mockPaymentPlanBusiness.Object,
               _mockOrderItemBusiness.Object,
               _mockOrderInstallmentRepository.Object,
               _mockDataContext.Object,
               _mockChargeBusiness.Object,
               _mockClubBusiness.Object,
               _mockOrderHistoryBusiness.Object, _mockPaymentBusiness.Object, _mockTransactionActivityBusiness.Object);
        }

        private List<OrderInstallment> CreateOrderInstallments()
        {
            var orderItem = FactoryGirl.Build<OrderItem>(p => p.Id = 1);

            var installment1 = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 10;
                p.OrderItem = orderItem;
                p.OrderItemId = 1;
                p.OrderItem.Order = FactoryGirl.Build<Order>();
                p.OrderItem.Order.Club = FactoryGirl.Build<Club>();
                p.OrderItem.Order.Club.Currency = CurrencyCodes.USD;
                p.OrderItem.Player = FactoryGirl.Build<PlayerProfile>();
                p.OrderItem.Player.Contact = FactoryGirl.Build<ContactPerson>();
                p.InstallmentDate = DateTime.UtcNow;
                p.EnableReminder = false;
                p.Amount = 200;
                p.PaidAmount = 0;
                p.OrderItem.Player.Contact.FirstName = "Maryam";
                p.OrderItem.Player.Contact.LastName = "Saeedi";
            });

            var installment2 = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 11;
                p.OrderItem = orderItem;
                p.OrderItemId = 1;
                p.OrderItem.Order = FactoryGirl.Build<Order>();
                p.OrderItem.Order.Club = FactoryGirl.Build<Club>();
                p.OrderItem.Order.Club.Currency = CurrencyCodes.USD;
                p.OrderItem.Player = FactoryGirl.Build<PlayerProfile>();
                p.OrderItem.Player.Contact = FactoryGirl.Build<ContactPerson>();
                p.InstallmentDate = DateTime.UtcNow;
                p.EnableReminder = false;
                p.Amount = 200;
                p.PaidAmount = 0;
                p.OrderItem.Player.Contact.FirstName = "Maryam";
                p.OrderItem.Player.Contact.LastName = "Saeedi";
            });

            var installment3 = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 12;
                p.OrderItem = orderItem;
                p.OrderItemId = 1;
                p.OrderItem.Order = FactoryGirl.Build<Order>();
                p.OrderItem.Order.Club = FactoryGirl.Build<Club>();
                p.OrderItem.Order.Club.Currency = CurrencyCodes.USD;
                p.OrderItem.Player = FactoryGirl.Build<PlayerProfile>();
                p.OrderItem.Player.Contact = FactoryGirl.Build<ContactPerson>();
                p.InstallmentDate = DateTime.UtcNow;
                p.EnableReminder = false;
                p.Amount = 200;
                p.PaidAmount = 0;
                p.IsDeleted = true;
            });

            return new List<OrderInstallment> { installment1, installment2, installment3 };
        }

        private List<OrderItem> CreateOrderItems()
        {

            var orderItem1 = FactoryGirl.Build<OrderItem>(p => p.Id = Id);
            var orderItem2 = FactoryGirl.Build<OrderItem>(p => p.Id = Id + 1);
            var orderItem3 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 2;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.Installments = CreateOrderInstallments();

            });
            var orderItem4 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 3;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.ProgramSchedule = null;
                p.ProgramScheduleId = null;
                p.Installments = CreateOrderInstallments();
            });
            var orderItem5 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 4;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Test;
                p.ItemStatus = OrderItemStatusCategories.completed;

            });
            var orderItem6 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 5;
                p.Order.IsLive = false;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.changed;
            });
            var orderItem7 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 6;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.changed;
            });

            return new List<OrderItem> { orderItem1, orderItem2, orderItem3, orderItem4, orderItem5, orderItem6, orderItem7 };
        }


        private static List<OrderInstallment> CreateInstallment()
        {
            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = 1;
                i.AutoChargeAttemps = 0;
                i.InstallmentDate = new DateTime(2018, 12, 1);
                i.Status = OrderStatusCategories.completed;
                i.Amount = 100;
                i.PaidAmount = 0;
                i.IsDeleted = false;
                i.EnableReminder = false;

                i.OrderItem = FactoryGirl.Build<OrderItem>(oi =>
                {
                    oi.ItemStatus = OrderItemStatusCategories.completed;
                    oi.PaymentPlanStatus = PaymentPlanStatus.Started;
                    oi.ProgramTypeCategory = ProgramTypeCategory.Class;
                    oi.Order = FactoryGirl.Build<Order>(o =>
                    {
                        o.ClubId = 1;
                        o.IsAutoCharge = true;
                        o.IsLive = true;
                    });
                });

            });

            return new List<OrderInstallment>() { installment };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentsWithoutValidIds()
        {
            return new List<OrderInstallment>
            {
                new OrderInstallment { Id = 15},
                new OrderInstallment { Id = 16},
                new OrderInstallment { Id = 17},
                new OrderInstallment { Id = 18}
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentsWithValidIds()
        {
            var orderInstallments = new List<OrderInstallment>();

            foreach (var id in _orderInstallmentIds)
            {
                orderInstallments.Add(new OrderInstallment { Id = id });
            }

            return orderInstallments;
        }

        private OrderInstallment FillOrderInstallments(bool enableReminder, bool isDeleted, decimal amount, decimal? paidAmount, DateTime installmentDate, int autoChargeAttempts, ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons, PaymentPlanType paymentPlanType, bool isLive, bool isAutoCharge)
        {
            return new OrderInstallment
            {
                EnableReminder = enableReminder,
                IsDeleted = isDeleted,
                AutoChargeAttemps = autoChargeAttempts,
                Amount = amount,
                PaidAmount = paidAmount,
                InstallmentDate = installmentDate,
                OrderItem = FillOrderItem(programTypeCategory, orderItemStatus, orderItemStatusReasons, paymentPlanType, isLive, isAutoCharge)
            };
        }

        private static OrderItem FillOrderItem(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons, PaymentPlanType paymentPlanType, bool isLive, bool isAutoCharge)
        {
            return new OrderItem
            {
                ItemStatus = orderItemStatus,
                ItemStatusReason = orderItemStatusReasons,
                ProgramTypeCategory = programTypeCategory,
                Order = new Order { IsLive = isLive, IsAutoCharge = isAutoCharge, ClubId = ClubId, Club = new Club { Id = ClubId, PartnerId = null } },
                PaymentPlanType = paymentPlanType,
                ProgramScheduleId = ProgramScheduleId
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentAllDueDateIsFuture(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, null, _futureDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(false, false, 200, null, _futureDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(false, false, 200, null, _futureDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentAmountEqualToPaidAmount(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 200, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(false, false, 200, 200, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(false, false, 200, 200, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentAmountLowerThanPaidAmount(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 300, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(false, false, 200, 300, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(false, false, 200, 300, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentEnableReminderIsTrue(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(true, false, 200,0, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(true, false, 200,0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(true, false, 200,0, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentProgramScheduleIdIsNull(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            var orderInstallments = new List<OrderInstallment>
            {
                FillOrderInstallments(true, false, 200,0, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(true, false, 200,0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(true, false, 200,0, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true)
            };

            foreach (var installment in orderInstallments)
            {
                installment.OrderItem.ProgramScheduleId = null;
            }

            return orderInstallments;
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentInstallmentDeletedIsTrue(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, true, 200,0, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(false, true, 200,0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true),
                FillOrderInstallments(false, true, 200,0, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true,true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentPaymentPlanTypeIsNotInstallment(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 0, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.FullPaid,true,true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.FullPaid,true,true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.FullPaid,true,true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentAutoChargeIsFalse(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 0, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, false),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, false),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, false)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentLiveIsFalse(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 0, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,false, true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,false, true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,false, true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentAttemptIsLowerThanAutoChargeFailAttemptNumber(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 200, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),//attemptIsZero
                FillOrderInstallments(false, false, 200, 0, _futureDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentAttemptIsEqualToAutoChargeFailAttemptNumber(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 200, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),//attemptIsZero
                FillOrderInstallments(false, false, 200, 0, _futureDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentAttemptIsGreaterThanAutoChargeFailAttemptNumber(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 200, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),//attemptIsZero
                FillOrderInstallments(false, false, 200, 0, _futureDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentPaidAmountIsNull(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 200, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, null, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, null,_futureDate,0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment, true, true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentInstallmentAmountLowerThanPaidAmount(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 200, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 100, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _futureDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentDueDateIsToday(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 200, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _currentDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _futureDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentDueDateIsPast(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 200, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _futureDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallmentWithPartnerId(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            var order = new Order { IsLive = true, IsAutoCharge = true, ClubId = 0, Club = new Club { Id = 0, PartnerId = ClubId } };

            var firstInstallment = FillOrderInstallments(false, false, 200, 200, _pastDate, 2, programTypeCategory,
                orderItemStatus, orderItemStatusReasons, PaymentPlanType.Installment, true, true);
            var secondInstallment = FillOrderInstallments(false, false, 200, 0, _pastDate, 1, programTypeCategory,
                orderItemStatus, orderItemStatusReasons, PaymentPlanType.Installment, true, true);
            var thirdInstallment = FillOrderInstallments(false, false, 200, 0, _futureDate, 0, programTypeCategory, orderItemStatus,
                orderItemStatusReasons, PaymentPlanType.Installment, true, true);

            firstInstallment.OrderItem.Order = secondInstallment.OrderItem.Order = thirdInstallment.OrderItem.Order = order;

            return new List<OrderInstallment>
            {
                firstInstallment, secondInstallment, thirdInstallment
            };
        }

        private IEnumerable<OrderInstallment> FillOrderInstallments(ProgramTypeCategory programTypeCategory, OrderItemStatusCategories orderItemStatus, OrderItemStatusReasons orderItemStatusReasons)
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallments(false, false, 200, 0, _pastDate, 2,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _pastDate, 1,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true),
                FillOrderInstallments(false, false, 200, 0, _futureDate, 0,programTypeCategory,orderItemStatus,orderItemStatusReasons,PaymentPlanType.Installment,true, true)
            };
        }

        private readonly DateTime _futureDate = DateTime.UtcNow.Date.AddDays(10);
        private readonly DateTime _pastDate = DateTime.UtcNow.Date.AddDays(-10);
        private readonly DateTime _currentDate = DateTime.UtcNow.Date.AddDays(-10);
        private const int ClubId = 10;
        private const int MaxAttempts = 1;

        #endregion


    }

}
