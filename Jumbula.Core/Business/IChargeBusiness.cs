﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IChargeBusiness
    {
        Charge Get(long chargeId);
        IQueryable<Charge> GetList(long seasonId);

        OperationStatus Delete(long chargeId);
        List<string> GetAllSeasonPartnerCharges(int partnerId);
        List<string> GetAllSeasonClubCharges(int clubId);

        IEnumerable<Charge> GetProgramSeasonCharges(Program program, ChargeDiscountCategory category);
        IEnumerable<Charge> GetProgramClubCharge(Program program, ChargeDiscountCategory category);
        List<Charge> GetProgramClubCharges(Program program);
        string GetChargeType(Charge charge, bool hasPartner);
        decimal GetChargeAmountForEditAndTransfer(Charge charge);
        List<Charge> GetLatePickupFee();
        List<Charge> GetLateRegistationFee();
        List<Charge> GetInstallmentFee();
        List<Charge> GetCustomCharge();
        List<ChargeDiscountItemViewModel> GetChargesForTransfer(OrderItem sourceOrderItem, Program targetProgram, long? tutionId);
        List<ChargeDiscountItemViewModel> GetChargesForEdit(OrderItem orderItem, Program program, List<ChargeDiscountItemViewModel> currentOrderChargeDiscount);
    }
}
