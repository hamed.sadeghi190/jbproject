﻿using System.Linq;
using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Jumbula.AutomationTests.Tests
{
    class EditOrderTests : BaseTest<EditOrderElements>
    {
        public EditOrderTests(IWebDriver webDriver)
            : base(webDriver)
        {
        }
        ProgramAndOrderElements programAndOrderElements;
        OrdersElements ordersElements;
        OrderItemDetailsElements orderItemDetailsElements;
        public void EditDropInOrder()
        {
            SeacrhProgram();
            GoToOrders();
            GoToViewDetails();
            //Actions actions = new Actions(WebDriver);
            //actions.MoveToElement(Elements.Step1Session(2, "Date", "div")).Perform();
            //actions.MoveToElement(Elements.Step1SessionAction(1, "...")).Perform();
            //Helper.ExtraAPI.Wait(1000);
            //Elements.Step1SessionAction(1, "Change date").Click();
            //Helper.ExtraAPI.Wait(5000);
            //// Check Calendar
            //while (Elements.MonthDate.Text != "October, 2017")
            //{
            //    Elements.PrevMonth.Click();
            //    Helper.ExtraAPI.Wait(1000);
            //}
            //Elements.NextMonth.Click();
            //Helper.ExtraAPI.Wait(1000);
            //Elements.NextMonth.Click();
            //Helper.ExtraAPI.Wait(10000);
            //var elm1 = WebDriver.FindElement(By.XPath("//*[@id='sessionScheduler']/table/tbody/tr[2]/td/div/div[6]/span[1]"));
            //var elm2 = WebDriver.FindElement(By.XPath("//*[@id='sessionScheduler']/table/tbody/tr[2]/td/div/div[9]/span[1]"));
            //actions.MoveToElement(elm2);
            //actions.MoveToElement(elm1);
            //elm1.Click();
            //elm2.Click();
            //Elements.CalendarDay("2017-12-07").First().Click();
            CheckAmounts();
            CheckMainInfo();
            Step1CheckChargeAndDiscount();
            Step1CheckSessions();
            ExtraAPI.Wait(5000);
            AddCharge();
            AddDiscount();
            CancelFirstSession();
            CancelSecondSession();
            UndoCancelFirstSession();
            ChangeDateFirstSession();
            GoToStep2();
            Step2CheckChargeAndDiscount();
            Step2CheckSessions();
            Submit();
            orderItemDetailsElements = new OrderItemDetailsElements(WebDriver);
            OrderItemCheckMainInfo();
            OrderItemCheckChargeDiscounts();
            OrderItemCheckSessions();
        }

        private void OrderItemCheckSessions()
        {
            var sessions = orderItemDetailsElements.Sessions.ToList();
            Assert.AreEqual("Dec 07 7:00am-8:00am", sessions[0].Text);
            Assert.AreEqual("Dec 28 7:00am-8:00am", sessions[1].Text);
            Assert.AreEqual("Jan 01 8:00am-9:00am", sessions[2].Text);
            Assert.AreEqual("Jan 02 10:00am-11:00am", sessions[3].Text);
            Assert.AreEqual("Jan 04 7:00am-8:00am", sessions[4].Text);
            Assert.AreEqual("Feb 12 8:00am-9:00am", sessions[5].Text);
            Assert.AreEqual("Feb 13 10:00am-11:00am", sessions[6].Text);
            Assert.AreEqual("Feb 15 7:00am-8:00am", sessions[7].Text);
            Assert.AreEqual("Apr 02 8:00am-9:00am", sessions[8].Text);
            Assert.AreEqual("Apr 03 10:00am-11:00am", sessions[9].Text);
            Assert.AreEqual("Apr 05 7:00am-8:00am", sessions[10].Text);
        }

        private void OrderItemCheckChargeDiscounts()
        {
            Assert.AreEqual("Subscription (Dec 25, 2017 - Apr 05, 2018), Drop-in", orderItemDetailsElements.Chargediscount(1, "Item").Text);
            Assert.AreEqual("$110.00", orderItemDetailsElements.Chargediscount(1, "Amount").Text);
            Assert.AreEqual("Application fee Service", orderItemDetailsElements.Chargediscount(2, "Item").Text);
            Assert.AreEqual("$10.00", orderItemDetailsElements.Chargediscount(2, "Amount").Text);
            Assert.AreEqual("Multi person - Selected programs Discount", orderItemDetailsElements.Chargediscount(3, "Item").Text);
            Assert.AreEqual("-$20.00", orderItemDetailsElements.Chargediscount(3, "Amount").Text);
            Assert.AreEqual("$100.00", orderItemDetailsElements.Total.Text);
            Assert.AreEqual("$0.00", orderItemDetailsElements.PaidAmount.Text);
        }

        private void OrderItemCheckMainInfo()
        {
            Assert.AreEqual("$100.00", orderItemDetailsElements.Balance.Text);
        }

        private void Submit()
        {
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SendEmail.Click();
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("The program is edited successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();
        }

        private void Step2CheckSessions()
        {
            // 1
            Assert.AreEqual("12/07/2017", Elements.Step2Session(1, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step2Session(1, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(1, "Amount", "").Text);
            // 3
            Assert.AreEqual("12/28/2017", Elements.Step2Session(2, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step2Session(2, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(2, "Amount", "").Text);
            // 3
            Assert.AreEqual("01/01/2018", Elements.Step2Session(3, "Date", "div").Text);
            Assert.AreEqual("8:00am-9:00am", Elements.Step2Session(3, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(3, "Amount", "").Text);
            // 4
            Assert.AreEqual("01/02/2018", Elements.Step2Session(4, "Date", "div").Text);
            Assert.AreEqual("10:00am-11:00am", Elements.Step2Session(4, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(4, "Amount", "").Text);
            // 5
            Assert.AreEqual("01/04/2018", Elements.Step2Session(5, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step2Session(5, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(5, "Amount", "").Text);
            // 6
            Assert.AreEqual("02/12/2018", Elements.Step2Session(6, "Date", "div").Text);
            Assert.AreEqual("8:00am-9:00am", Elements.Step2Session(6, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(6, "Amount", "").Text);
            // 7
            Assert.AreEqual("02/13/2018", Elements.Step2Session(7, "Date", "div").Text);
            Assert.AreEqual("10:00am-11:00am", Elements.Step2Session(7, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(7, "Amount", "").Text);
            // 8
            Assert.AreEqual("02/15/2018", Elements.Step2Session(8, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step2Session(8, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(8, "Amount", "").Text);
            // 9
            Assert.AreEqual("04/02/2018", Elements.Step2Session(9, "Date", "div").Text);
            Assert.AreEqual("8:00am-9:00am", Elements.Step2Session(9, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(9, "Amount", "").Text);
            // 10
            Assert.AreEqual("04/03/2018", Elements.Step2Session(10, "Date", "div").Text);
            Assert.AreEqual("10:00am-11:00am", Elements.Step2Session(10, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(10, "Amount", "").Text);
            // 11
            Assert.AreEqual("04/05/2018", Elements.Step2Session(11, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step2Session(11, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step2Session(11, "Amount", "").Text);
        }

        private void Step2CheckChargeAndDiscount()
        {
            // Check charges and discounts
            Assert.AreEqual("Subscription Nov 01, 2017 - Apr 30, 2018 (Drop-in)", Elements.Step2ChargeDiscounts(1, "Item", "span").Text);
            Assert.AreEqual("$110.00", Elements.Step2ChargeDiscounts(1, "Amount", "").Text);
            Assert.AreEqual("Application fee", Elements.Step2ChargeDiscounts(2, "Item", "span").Text);
            Assert.AreEqual("$10.00", Elements.Step2ChargeDiscounts(2, "Amount", "").Text);
            Assert.AreEqual("Multi person - Selected programs", Elements.Step2ChargeDiscounts(3, "Item", "span").Text);
            Assert.AreEqual("$20.00", Elements.Step2ChargeDiscounts(3, "Amount", "").Text);
        }

        private void GoToStep2()
        {
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.ContinueBtn.Click();
        }

        private void ChangeDateFirstSession()
        {
            Actions actions = new Actions(WebDriver); 
            actions.MoveToElement(Elements.Step1Session(2, "Date", "div")).Perform();
            actions.MoveToElement(Elements.Step1SessionAction(1, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1SessionAction(1, "Change date").Click();
            ExtraAPI.Wait(5000);
            // Check Calendar
            while (Elements.MonthDate.Text != "October, 2017")
            {
                Elements.PrevMonth.Click();
                ExtraAPI.Wait(1000);
            }
            // Oct
            Assert.AreEqual(0, Elements.CalendarDay("2017-10-26").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-10-27").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-10-28").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-10-29").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-10-30").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-10-31").Count);
            // Nov
            Elements.NextMonth.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-01").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-02").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-02").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-02").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-03").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-04").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-05").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-06").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-06").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-06").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-07").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-07").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-07").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-08").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-09").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-09").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-09").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-10").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-11").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-12").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-13").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-13").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-13").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-14").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-14").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-14").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-15").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-16").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-16").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-16").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-17").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-18").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-19").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-20").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-20").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-20").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-21").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-21").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-21").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-22").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-23").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-23").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-23").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-24").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-25").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-26").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-27").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-27").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-27").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-28").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-28").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-28").First().GetAttribute("class").ToString().Contains("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-11-29").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-11-30").Count);
            Assert.IsFalse(Elements.CalendarDay("2017-11-30").First().GetAttribute("class").ToString().Contains("checked"));
            Assert.IsFalse(Elements.CalendarDay("2017-11-30").First().GetAttribute("class").ToString().Contains("disabled"));
            // Dec
            Elements.NextMonth.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-01").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-02").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-03").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-04").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-05").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-06").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-07").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-08").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-09").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-10").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-11").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-12").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-13").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-14").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-15").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-16").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-17").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-18").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-19").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-20").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-21").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-22").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-23").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-24").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-25").Count);
            Assert.AreEqual("true", Elements.CalendarDay("2017-12-25").First().GetAttribute("checked"));
            Assert.AreEqual(null, Elements.CalendarDay("2017-12-25").First().GetAttribute("disabled"));
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-26").Count);
            Assert.AreEqual("true", Elements.CalendarDay("2017-12-26").First().GetAttribute("checked"));
            Assert.AreEqual("true", Elements.CalendarDay("2017-12-26").First().GetAttribute("disabled"));
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-27").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2017-12-28").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-29").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-30").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2017-12-31").Count);
            // Change day
            ExtraAPI.Wait(5000);
            //actions.MoveToElement(Elements.CalendarDay("2017-12-07").First()).Perform();
            Elements.CalendarDay("2017-12-07").First().Click();
            Assert.AreEqual(null, Elements.CalendarDay("2017-12-25").First().GetAttribute("checked"));
            Assert.AreEqual("true", Elements.CalendarDay("2017-12-07").First().GetAttribute("checked"));
            
            // Jan
            Elements.NextMonth.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-01").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-02").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-04").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-05").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-06").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-07").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-08").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-09").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-10").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-11").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-12").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-13").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-14").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-15").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-16").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-17").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-18").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-19").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-20").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-21").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-22").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-23").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-24").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-25").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-26").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-27").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-28").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-29").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-01-30").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-01-31").Count);
            // Feb
            Elements.NextMonth.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-01").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-02").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-03").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-04").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-05").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-06").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-07").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-08").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-09").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-10").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-11").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-12").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-13").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-14").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-15").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-16").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-17").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-18").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-19").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-20").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-21").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-22").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-23").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-24").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-25").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-26").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-02-27").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-02-28").Count);
            // March
            Elements.NextMonth.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-01").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-02").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-03").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-04").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-05").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-06").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-07").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-08").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-09").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-10").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-11").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-12").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-13").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-14").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-15").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-16").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-17").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-18").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-19").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-20").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-21").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-22").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-23").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-24").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-25").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-26").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-27").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-28").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-03-29").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-30").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-03-31").Count);
            // April
            Elements.NextMonth.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-01").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-02").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-03").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-04").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-05").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-06").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-07").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-08").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-09").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-10").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-11").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-12").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-13").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-14").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-15").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-16").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-17").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-18").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-19").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-20").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-21").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-22").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-23").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-24").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-25").Count);
            Assert.AreEqual(1, Elements.CalendarDay("2018-04-26").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-27").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-28").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-29").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-04-30").Count);
            // May
            Elements.NextMonth.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-01").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-02").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-03").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-04").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-05").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-06").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-07").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-08").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-09").Count);
            Assert.AreEqual(0, Elements.CalendarDay("2018-05-10").Count);

            Elements.CalendarSaveBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("12/07/2017", Elements.Step1Session(1, "Date", "div").Text);
        }

        private void UndoCancelFirstSession()
        {
            // Undo cancel first session
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Step1Session(2, "Date", "div")).Perform();
            actions.MoveToElement(Elements.Step1SessionAction(1, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1SessionAction(1, "Undo cancel").Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.WarningExists);
            Elements.WarningCancelBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.Step1Session(1, "Date", "").GetAttribute("class").Contains("line-through"));
            Assert.IsTrue(Elements.Step1Session(1, "Time", "").GetAttribute("class").Contains("line-through"));
            Assert.IsTrue(Elements.Step1Session(1, "Amount", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$90.00", Elements.NewAmount.Text);
            Assert.AreEqual("$-30.00", Elements.Difference.Text);
            Assert.AreEqual("$90.00", Elements.RemainingBalance.Text);
            Assert.AreEqual("$100.00", Elements.Step1ChargeDiscounts(1, "Amount", "span").Text);
            actions.MoveToElement(Elements.Step1SessionAction(1, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1SessionAction(1, "Undo cancel").Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.WarningExists);
            Elements.WarningYesBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.IsFalse(Elements.Step1Session(1, "Date", "").GetAttribute("class").Contains("line-through"));
            Assert.IsFalse(Elements.Step1Session(1, "Time", "").GetAttribute("class").Contains("line-through"));
            Assert.IsFalse(Elements.Step1Session(1, "Amount", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$100.00", Elements.NewAmount.Text);
            Assert.AreEqual("$-20.00", Elements.Difference.Text);
            Assert.AreEqual("$100.00", Elements.RemainingBalance.Text);
            Assert.AreEqual("$110.00", Elements.Step1ChargeDiscounts(1, "Amount", "span").Text);
        }

        private void CancelSecondSession()
        {
            // cancel second session
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Step1Session(3, "Date", "div")).Perform();
            actions.MoveToElement(Elements.Step1SessionAction(2, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1SessionAction(2, "Cancel").Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.WarningExists);
            Elements.WarningYesBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.Step1Session(2, "Date", "").GetAttribute("class").Contains("line-through"));
            Assert.IsTrue(Elements.Step1Session(2, "Time", "").GetAttribute("class").Contains("line-through"));
            Assert.IsTrue(Elements.Step1Session(2, "Amount", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$90.00", Elements.NewAmount.Text);
            Assert.AreEqual("$-30.00", Elements.Difference.Text);
            Assert.AreEqual("$90.00", Elements.RemainingBalance.Text);
            Assert.AreEqual("$100.00", Elements.Step1ChargeDiscounts(1, "Amount", "span").Text);
        }

        private void CancelFirstSession()
        {
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Step1Session(2, "Date", "div")).Perform();
            actions.MoveToElement(Elements.Step1SessionAction(1, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1SessionAction(1, "Cancel").Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.WarningExists);
            Elements.WarningCancelBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.IsFalse(Elements.Step1Session(1, "Date", "").GetAttribute("class").Contains("line-through"));
            Assert.IsFalse(Elements.Step1Session(1, "Time", "").GetAttribute("class").Contains("line-through"));
            Assert.IsFalse(Elements.Step1Session(1, "Amount", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$110.00", Elements.NewAmount.Text);
            Assert.AreEqual("$-10.00", Elements.Difference.Text);
            Assert.AreEqual("$110.00", Elements.RemainingBalance.Text);
            Assert.AreEqual("$120.00", Elements.Step1ChargeDiscounts(1, "Amount", "span").Text);
            actions.MoveToElement(Elements.Step1SessionAction(1, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1SessionAction(1, "Cancel").Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.WarningExists);
            Elements.WarningYesBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.Step1Session(1, "Date", "").GetAttribute("class").Contains("line-through"));
            Assert.IsTrue(Elements.Step1Session(1, "Time", "").GetAttribute("class").Contains("line-through"));
            Assert.IsTrue(Elements.Step1Session(1, "Amount", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$100.00", Elements.NewAmount.Text);
            Assert.AreEqual("$-20.00", Elements.Difference.Text);
            Assert.AreEqual("$100.00", Elements.RemainingBalance.Text);
            Assert.AreEqual("$110.00", Elements.Step1ChargeDiscounts(1, "Amount", "span").Text);
        }

        private void AddDiscount()
        {
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Step1ChargeDiscountsAction(3, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1ChargeDiscountsAction(3, "Add").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$129.00", Elements.NewAmount.Text);
            Assert.AreEqual("$9.00", Elements.Difference.Text);
            Assert.AreEqual("$129.00", Elements.RemainingBalance.Text);
            actions.MoveToElement(Elements.Step1ChargeDiscountsAction(3, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1ChargeDiscountsAction(3, "Manually adjust amount").Click();
            ExtraAPI.Wait(5000);
            Elements.Step1ChargeDiscounts(3, "Amount", "input").Clear();
            Elements.Step1ChargeDiscounts(3, "Amount", "input").SendKeys("10");
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$120.00", Elements.NewAmount.Text);
            Assert.AreEqual("$0.00", Elements.Difference.Text);
            Assert.AreEqual("$120.00", Elements.RemainingBalance.Text);
            Elements.Step1ChargeDiscountscancelBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$129.00", Elements.NewAmount.Text);
            Assert.AreEqual("$9.00", Elements.Difference.Text);
            Assert.AreEqual("$129.00", Elements.RemainingBalance.Text);
            actions.MoveToElement(Elements.Step1ChargeDiscountsAction(3, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1ChargeDiscountsAction(3, "Manually adjust amount").Click();
            ExtraAPI.Wait(5000);
            Elements.Step1ChargeDiscounts(3, "Amount", "input").Clear();
            Elements.Step1ChargeDiscounts(3, "Amount", "input").SendKeys("20");
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$110.00", Elements.NewAmount.Text);
            Assert.AreEqual("$-10.00", Elements.Difference.Text);
            Assert.AreEqual("$110.00", Elements.RemainingBalance.Text);
            Elements.Step1ChargeDiscountsSaveBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$110.00", Elements.NewAmount.Text);
            Assert.AreEqual("$-10.00", Elements.Difference.Text);
            Assert.AreEqual("$110.00", Elements.RemainingBalance.Text);
        }

        private void AddCharge()
        {
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Step1ChargeDiscountsAction(2, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1ChargeDiscountsAction(2, "Add").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$170.00", Elements.NewAmount.Text);
            Assert.AreEqual("$50.00", Elements.Difference.Text);
            Assert.AreEqual("$170.00", Elements.RemainingBalance.Text);
            actions.MoveToElement(Elements.Step1ChargeDiscountsAction(2, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1ChargeDiscountsAction(2, "Manually adjust amount").Click();
            ExtraAPI.Wait(5000);
            Elements.Step1ChargeDiscounts(2, "Amount", "input").Clear();
            Elements.Step1ChargeDiscounts(2, "Amount", "input").SendKeys("10");
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$130.00", Elements.NewAmount.Text);
            Assert.AreEqual("$10.00", Elements.Difference.Text);
            Assert.AreEqual("$130.00", Elements.RemainingBalance.Text);
            Elements.Step1ChargeDiscountscancelBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$170.00", Elements.NewAmount.Text);
            Assert.AreEqual("$50.00", Elements.Difference.Text);
            Assert.AreEqual("$170.00", Elements.RemainingBalance.Text);
            actions.MoveToElement(Elements.Step1ChargeDiscountsAction(2, "...")).Perform();
            ExtraAPI.Wait(1000);
            Elements.Step1ChargeDiscountsAction(2, "Manually adjust amount").Click();
            ExtraAPI.Wait(5000);
            Elements.Step1ChargeDiscounts(2, "Amount", "input").Clear();
            Elements.Step1ChargeDiscounts(2, "Amount", "input").SendKeys("10");
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$130.00", Elements.NewAmount.Text);
            Assert.AreEqual("$10.00", Elements.Difference.Text);
            Assert.AreEqual("$130.00", Elements.RemainingBalance.Text);
            Elements.Step1ChargeDiscountsSaveBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("$130.00", Elements.NewAmount.Text);
            Assert.AreEqual("$10.00", Elements.Difference.Text);
            Assert.AreEqual("$130.00", Elements.RemainingBalance.Text);
        }

        private void Step1CheckSessions()
        {
            // 1
            Assert.AreEqual("12/25/2017", Elements.Step1Session(1, "Date", "div").Text);
            Assert.AreEqual("8:00am-9:00am", Elements.Step1Session(1, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(1, "Amount", "div").Text);
            // 2
            Assert.AreEqual("12/26/2017", Elements.Step1Session(2, "Date", "div").Text);
            Assert.AreEqual("10:00am-11:00am", Elements.Step1Session(2, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(2, "Amount", "div").Text);
            // 3
            Assert.AreEqual("12/28/2017", Elements.Step1Session(3, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step1Session(3, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(3, "Amount", "div").Text);
            // 4
            Assert.AreEqual("01/01/2018", Elements.Step1Session(4, "Date", "div").Text);
            Assert.AreEqual("8:00am-9:00am", Elements.Step1Session(4, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(4, "Amount", "div").Text);
            // 5
            Assert.AreEqual("01/02/2018", Elements.Step1Session(5, "Date", "div").Text);
            Assert.AreEqual("10:00am-11:00am", Elements.Step1Session(5, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(5, "Amount", "div").Text);
            // 6
            Assert.AreEqual("01/04/2018", Elements.Step1Session(6, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step1Session(6, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(6, "Amount", "div").Text);
            // 7
            Assert.AreEqual("02/12/2018", Elements.Step1Session(7, "Date", "div").Text);
            Assert.AreEqual("8:00am-9:00am", Elements.Step1Session(7, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(7, "Amount", "div").Text);
            // 8
            Assert.AreEqual("02/13/2018", Elements.Step1Session(8, "Date", "div").Text);
            Assert.AreEqual("10:00am-11:00am", Elements.Step1Session(8, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(8, "Amount", "div").Text);
            // 9
            Assert.AreEqual("02/15/2018", Elements.Step1Session(9, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step1Session(9, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(9, "Amount", "div").Text);
            // 10
            Assert.AreEqual("04/02/2018", Elements.Step1Session(10, "Date", "div").Text);
            Assert.AreEqual("8:00am-9:00am", Elements.Step1Session(10, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(10, "Amount", "div").Text);
            // 11
            Assert.AreEqual("04/03/2018", Elements.Step1Session(11, "Date", "div").Text);
            Assert.AreEqual("10:00am-11:00am", Elements.Step1Session(11, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(11, "Amount", "div").Text);
            // 12
            Assert.AreEqual("04/05/2018", Elements.Step1Session(12, "Date", "div").Text);
            Assert.AreEqual("7:00am-8:00am", Elements.Step1Session(12, "Time", "").Text);
            Assert.AreEqual("$10.00", Elements.Step1Session(12, "Amount", "div").Text);

            
        }

        private void Step1CheckChargeAndDiscount()
        {
            // 1
            Assert.AreEqual("Subscription Nov 01, 2017 - Apr 30, 2018 (Drop-in)", Elements.Step1ChargeDiscounts(1, "Item", "span").Text);
            Assert.IsFalse(Elements.Step1ChargeDiscounts(1, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$120.00", Elements.Step1ChargeDiscounts(1, "Amount", "span").Text);
            Assert.IsFalse(Elements.Step1ChargeDiscountsActionExist(1, "..."));
            // 2
            Assert.AreEqual("Application fee", Elements.Step1ChargeDiscounts(2, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(2, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$50.00", Elements.Step1ChargeDiscounts(2, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(2, "..."));
            // 3
            Assert.AreEqual("Multi person - Selected programs", Elements.Step1ChargeDiscounts(3, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(3, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$1.00", Elements.Step1ChargeDiscounts(3, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(3, "..."));
            // 4
            Assert.AreEqual("Multi program - Selected programs", Elements.Step1ChargeDiscounts(4, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(4, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$1.00", Elements.Step1ChargeDiscounts(4, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(4, "..."));
            // 5
            Assert.AreEqual("General discount - Selected programs", Elements.Step1ChargeDiscounts(5, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(5, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$1.00", Elements.Step1ChargeDiscounts(5, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(5, "..."));
            // 6
            Assert.AreEqual("Multi person - All programs", Elements.Step1ChargeDiscounts(6, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(6, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$10.00", Elements.Step1ChargeDiscounts(6, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(6, "..."));
            // 7
            Assert.AreEqual("Multi program - All programs", Elements.Step1ChargeDiscounts(7, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(7, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$10.00", Elements.Step1ChargeDiscounts(7, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(7, "..."));
            // 8
            Assert.AreEqual("Multi schedule - All programs", Elements.Step1ChargeDiscounts(8, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(8, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$10.00", Elements.Step1ChargeDiscounts(8, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(8, "..."));
            // 9
            Assert.AreEqual("General discount - All programs", Elements.Step1ChargeDiscounts(9, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(9, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$10.00", Elements.Step1ChargeDiscounts(9, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(9, "..."));
            // 10
            Assert.AreEqual("Custom charge", Elements.Step1ChargeDiscounts(10, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(10, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$0.00", Elements.Step1ChargeDiscounts(10, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(10, "..."));
            // 11
            Assert.AreEqual("Custom discount - Selected programs", Elements.Step1ChargeDiscounts(11, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(11, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$1.00", Elements.Step1ChargeDiscounts(11, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(11, "..."));
            // 12
            Assert.AreEqual("Custom discount - All programs", Elements.Step1ChargeDiscounts(12, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(12, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$10.00", Elements.Step1ChargeDiscounts(12, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(12, "..."));
            // 13
            Assert.AreEqual("Custom discount", Elements.Step1ChargeDiscounts(13, "Item", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscounts(13, "Item", "").GetAttribute("class").Contains("line-through"));
            Assert.AreEqual("$0.00", Elements.Step1ChargeDiscounts(13, "Amount", "span").Text);
            Assert.IsTrue(Elements.Step1ChargeDiscountsActionExist(13, "..."));
        }

        private void CheckMainInfo()
        {
            // Check name and confirmation
            Assert.AreEqual("Eva Smith", Elements.Name.Text);
            Assert.AreEqual("637F-99-BCC9", Elements.Confirmation.Text);
        }

        private void CheckAmounts()
        {
            Assert.AreEqual("$120.00", Elements.OrderAmount.Text);
            Assert.AreEqual("$120.00", Elements.NewAmount.Text);
            Assert.AreEqual("$0.00", Elements.PaidAmount.Text);
            Assert.AreEqual("$0.00", Elements.Difference.Text);
            Assert.AreEqual("$120.00", Elements.RemainingBalance.Text);
        }

        private void GoToViewDetails()
        {
            ordersElements = new OrdersElements(WebDriver);
            ordersElements.SearchTerm.SendKeys("637F-99-BCC9");
            ordersElements.SearchBtn.Click();
            ExtraAPI.Wait(5000);
            ordersElements.Row1Actions.Click();
            ExtraAPI.Wait(1000);
            ordersElements.Row1ViewDetailsAction.Click();
            ExtraAPI.Wait(5000);
            orderItemDetailsElements = new OrderItemDetailsElements(WebDriver);
            orderItemDetailsElements.EditCancelIcon.Click();
            ExtraAPI.Wait(5000);
        }

        private void GoToOrders()
        {
            programAndOrderElements.Row1Actions.Click();
            ExtraAPI.Wait(1000);
            programAndOrderElements.Row1OrderslAction.Click();
            ExtraAPI.Wait(5000);
        }

        private void SeacrhProgram()
        {
            programAndOrderElements = new ProgramAndOrderElements(WebDriver);
            programAndOrderElements.SearchTerm.SendKeys("Subscription");
            programAndOrderElements.SearchBtn.Click();
            ExtraAPI.Wait(5000);
        }
    }
}
