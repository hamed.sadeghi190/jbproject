﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using Jumbula.Common.Enums;
using Newtonsoft.Json;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Constants = Jumbula.Common.Constants.Constants;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ClubViewModel : BaseViewModel<Club>
    {
        public ClubViewModel()
        {
            SelectedTimeZone = "-1";
            Provider1099 = false;
            CustomAttributes = new ClubCustomAttributesViewModel();
            AllStaffTitles = new List<SelectKeyValue<string>>();
        }

        public ClubViewModel(string type, bool isCreateEditMode = false)
            : this()
        {

            OrganizationTimeZone = DropdownHelpers.SeedTimeZones();
            if (type == "School")
            {
                OrganizationSubTypes = Ioc.ClubBusiness.GetClubTypesForSchool();
            }
            else
            {
                OrganizationSubTypes = Ioc.ClubBusiness.GetClubTypesForProvider();
            }


            AllCategories = Ioc.CategoryBuisness.GetCategories();
            OrganizationRoles = SeedRoles();
        }

        public int CommissionRate { get; set; }
        public string CommissionRateString { get; set; }
        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Last name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete last name.")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "{0} is required")]
        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string Email { get; set; }

        [Display(Name = "Title")]
        public StaffTitle? Title { get; set; }

        public List<SelectKeyValue<string>> AllStaffTitles { get; set; }

        [Url]
        [DataType(DataType.Url)]
        [Display(Name = "Club website")]
        public string Site { get; set; }

        [Display(Name = "Phone number")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$",
            ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string Phone { get; set; }

        public string StrPhone
        {
            get
            {
                if (!string.IsNullOrEmpty(Phone))
                {
                    return PhoneNumberHelper.FormatPhoneNumber(Phone);
                }
                return string.Empty;
            }
        }

        public string ExtensionPhone { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 3)]
        [RegularExpression("[A-Za-z0-9-]+",
             ErrorMessage = "{0} cannot contain any spaces or special characters such as *, &, !, _, etc")]
        public string Domain { get; set; }

        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? MemberSince { get; set; }
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? AgreementExpirationDate { get; set; }

        public string StrAgreementExpirationDate
        {
            get
            {
                if (AgreementExpirationDate.HasValue)
                {
                    return AgreementExpirationDate.Value.ToString(Constants.DefaultDateFormat);
                }
                return string.Empty;
            }
        }
        public string TaxIDFEIN { get; set; }
        public string TaxIDSSN { get; set; }
        public string EMDiscountSchools { get; set; }
        public bool Provider1099 { get; set; }

        public bool TaxExemptNonprofit { get; set; }
        public bool IsInActive { get; set; }
        public bool AddNewAdmin { get; set; }
        public string typeMerge { get; set; }
        public string ClubType { get; set; }

        public bool IsSchool { get; set; }

        public string AdminName
        {
            get
            { return string.Format(Constants.F_FullName, FirstName, LastName); }
        }
        [Required(ErrorMessage = "Address is required.")]
        public string Address { get; set; }
        public string County { get; set; }
        public string Description { get; set; }

        public List<SelectKeyValue<int>> PartnerMessageResponders { get; set; }

        public int? SelectedMessageResponder { get; set; }

        public List<int> SelectedUser { get; set; }
        public List<SelectKeyValue<int>> PartnerAdmins { get; set; }
        public List<SelectKeyValue<string>> OrganizationRoles { get; set; }

        public string SelectedTimeZone { get; set; }
        public List<SelectKeyValue<string>> OrganizationTimeZone { get; set; }

        public int SelectedSubType { get; set; }
        public List<SelectKeyValue<int>> OrganizationSubTypes { get; set; }
        public int SelectedCategory { get; set; }

        public List<SelectKeyValue<int>> AllCategories { get; set; }

        public ClubCustomAttributesViewModel CustomAttributes { get; set; }

        List<SelectKeyValue<string>> SeedRoles()
        {
            var result = new List<SelectKeyValue<string>>();
            result.Add(new SelectKeyValue<string>() { Text = "Select", Value = "0" });
            foreach (RoleCategory type in Enum.GetValues(typeof(RoleCategory)))
            {
                if (type == RoleCategory.Accountant || type == RoleCategory.Instructor || type == RoleCategory.Owner || type == RoleCategory.Parent || type == RoleCategory.SysAdmin || type == RoleCategory.Support || type == RoleCategory.Partner)
                {
                    continue;
                }
                result.Add(new SelectKeyValue<string>() { Text = type.ToDescription(), Value = ((int)type).ToString() });
            }

            return result;
        }
    }

}