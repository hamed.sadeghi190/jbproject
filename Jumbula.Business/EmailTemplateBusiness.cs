﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System;
using System.Linq;

namespace Jumbula.Business
{
    public class EmailTemplateBusiness : IEmailTemplateBusiness
    {
        #region Constants
        private const string FolderName = "EmailTemplate";
        private const string ImageFolder = "emailTemplate";

        #endregion

        #region Fields
        private readonly IStorageBusiness _storageBusiness;
        private readonly IFileBusiness _fileBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;
        #endregion

        #region Constructors
        public EmailTemplateBusiness(IStorageBusiness storageBusiness, IClubBusiness clubBusiness, IFileBusiness fileBusiness, IRepository<EmailTemplate> emailTemplateRepository)
        {
            _storageBusiness = storageBusiness;
            _fileBusiness = fileBusiness;
            _clubBusiness = clubBusiness;
            _emailTemplateRepository = emailTemplateRepository;
        }
        #endregion

        #region Public Methods
        public EmailTemplate Get(int id)
        {
            return _emailTemplateRepository.Get(id);
        }

        public IQueryable<EmailTemplate> GetClubTemplates(int clubId)
        {
            return
                _emailTemplateRepository
                    .GetList()
                    .Where(t => t.Club_Id == clubId && t.Type == TemplateType.Email)
                    .OrderBy(o => o.TemplateName);
        }

        public OperationStatus UpdateTemplate(EmailTemplate template)
        {
            return _emailTemplateRepository.Update(template);
        }

        public OperationStatus CreateTemplate(EmailTemplate template)
        {
            return _emailTemplateRepository.Create(template);
        }

        public OperationStatus DeleteTemplate(int templateId)
        {
            try
            {
                return _emailTemplateRepository.Delete(templateId);
            }
            catch (Exception)
            {
                return new OperationStatus { Status = false };
            }

        }

        public EmailTemplate GetDefaultClubTemplate()
        {
            return _emailTemplateRepository.GetList().SingleOrDefault(n => n.TemplateName == "Default");
        }

        public string UploadImageTemplate(string image, string clubDomain, string folderName)
        {
            return _fileBusiness.UploadImage(image, clubDomain, folderName);
        }

        public EmailTemplateModel GetEmailTemplate(int? templateId)
        {
            var templateModel = new EmailTemplateModel();

            if (templateId.HasValue)
            {
                var template = Get(templateId.Value);

                templateModel = JsonHelper.JsonDeserialize<EmailTemplateModel>(template.Template);

                if (UrlPathHelper.IsRelationalUrl(templateModel.ClubLogo))
                    templateModel.ClubLogo = _clubBusiness.GetClubLogoURL(templateModel.ClubDomain, templateModel.ClubLogo);

                if (UrlPathHelper.IsRelationalUrl(templateModel.HeaderBackground))
                    templateModel.HeaderBackground = GetImageUrl(templateModel.HeaderBackground, templateModel.ClubDomain);

                if (UrlPathHelper.IsRelationalUrl(templateModel.FooterBackground))
                    templateModel.FooterBackground = GetImageUrl(templateModel.FooterBackground, templateModel.ClubDomain);
            }

            return templateModel;
        }

        public OperationStatus SaveTemplate(EmailTemplateModel template, int clubId, int? templateId)
        {
            var club = _clubBusiness.Get(clubId);

            template.ClubDomain = template.ClubDomain ?? club.Domain;

            if (UrlPathHelper.IsRelationalUrl(template.HeaderBackground))
                template.HeaderBackground = UploadImageTemplate(template.HeaderBackground, template.ClubDomain, ImageFolder);

            if (UrlPathHelper.IsRelationalUrl(template.FooterBackground))
                template.FooterBackground = UploadImageTemplate(template.FooterBackground, template.ClubDomain, ImageFolder);

            var newTemplate = new EmailTemplate
            {
                Template = JsonHelper.JsonSerializer(template),
                TemplateName = template.Name,
                Type = TemplateType.Email,
                Club_Id = club.Id
            };

            if (!templateId.HasValue)
            {
                template.Name = template.NewName;
                template.NewName = string.Empty;

                newTemplate.TemplateName = template.Name;

                CreateTemplate(newTemplate);
            }
            else
            {
                newTemplate.Id = templateId.Value;

                UpdateTemplate(newTemplate);
            }

            return new OperationStatus { Status = true, Data = newTemplate.Id };
        }

        #endregion

        #region Private Methods
        private string GetImageUrl(string fileName, string clubDomain)
        {
            var imageUri = _storageBusiness.GetUri(Constants.Path_ClubFilesContainer, $@"{clubDomain}\{FolderName}", fileName).AbsoluteUri;

            return string.IsNullOrEmpty(fileName) ? string.Empty : string.Concat(imageUri, "?", "rnd=", Guid.NewGuid());
        }
        #endregion
    }
}
