﻿namespace Jumbula.Core.Domain
{
    public class ReportRecipient
    {
        public string Email { get; set; }

        public string Lastname { get; set; }

        //public string FirstName { get; set; }
    }
}
