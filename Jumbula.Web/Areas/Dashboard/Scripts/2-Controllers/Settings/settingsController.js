﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('settingsController', ['$scope', '$http', function ($scope, $http) {
        window.app.globalObjects.pageClass = 'settings-view';
        window.app.globalObjects.initializeView();
        
        $scope.MC.openBlade({ url: "Dashboard/Blade/Settings", order: 0, menu: "SETTINGS" });
    }])

})();