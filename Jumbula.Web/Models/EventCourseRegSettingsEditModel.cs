﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using Newtonsoft.Json;
using RestSharp.Deserializers;
using SportsClub.Db;
using SportsClub.Domain;
using SportsClub.Infrastructure.Helper;
using SportsClub.Infrastructure.JBMembership;
using SportsClub.Models.FormModels;
using Utilities = SportsClub.Infrastructure.Utilities;
using Jb.Framework.Common.Model;

namespace SportsClub.Models
{
    public class EventCourseRegSettingsEditModel : EventBaseCreateEditModel
    {
        public EventCourseRegSettings ToEventCourseRegSettingsCourse()
        {
            // recurrencetime
            ICollection<CourseRecurrenceTime> courseRecurrenceTimeList = new HashSet<CourseRecurrenceTime>();
            foreach (var item in RecurrenceTime.RecurrenceTimes)
            {
                if (item.IsChecked)
                {
                    courseRecurrenceTimeList.Add(new CourseRecurrenceTime
                    {
                        Day = item.Day,
                        StartTime = item.StartTime,
                        FinishTime = item.FinishTime
                    });
                }
            }

            // if weeksCount set finishdate as well
            if (FinishDateType == FinishDateType.NumberOfSession)
            {
                SeedFinishDate();
            }
            else if (FinishDateType == FinishDateType.UnSpecifiedEndDate)
            {
                FinishDate = null;
                HasProRatingPrice = false;
            }

            // add all selected form to eventforms table
            //var eventForms = AssignForm.SelectedForms.Select(item => new EventForms() { FormId = item }).ToList();
            EventCourseRegSettings CourseDifferent = new EventCourseRegSettings
            {
                Capacity = this.Capacity,
                ExternalRegUrl = this.ExternalRegUrl,
                FullPrice = this.FullPrice,
                RecurrentStartDate = this.StartDate,
                RecurrentEndDate = this.FinishDate,
                WeeksCount = this.WeeksCount,
                RecurrenceTime = courseRecurrenceTimeList,
                HasProRatingPrice = this.HasProRatingPrice,
                HasSessionPrice = this.HasSessionPrice,
                SessionPrice = this.SessionPrice,
                FinishDateType = this.FinishDateType,
                //LastCreatedPage=this.LastCreatedPage,
                EarlybirdDiscounts = this.EarlybirdDiscounts
            };
            CourseDifferent.ReservedCapacity = this.ReservedCapacity;
            return CourseDifferent;
        }

        #region Constructors

        public EventCourseRegSettingsEditModel()
            : base()
        {
            //PaymentPlan = new PaymentPlanViewModel();
            //AssignForm = new AssignFormViewModel();
            ProRatingPrice = new decimal();
            //OutSourcers = new List<OutSourcerList>();
            FinishDateType = FinishDateType.NumberOfSession;
            SeedCourseRecurrenceTime(null);
            SeedCapacity();
        }

        public EventCourseRegSettingsEditModel(string domain)
            : base(domain)
        {
            SeedCourseRecurrenceTime(null);
            SeedCapacity();
            EarlybirdDiscounts = new List<EventEarlybirdDiscount>();
            EventType = EventTypeCategory.NewRegular;
        }

        // constructor for create ...
        public EventCourseRegSettingsEditModel(string domain, Domain.TimeZone timeZone, bool isVendor, int clubId)
            : base(domain)
        {
            ClubId = clubId;
            //SeedOUtSourcers();
            SeedCourseRecurrenceTime(null);
            SeedCapacity();

            EventType = EventTypeCategory.NewRegular;
        }

        public EventCourseRegSettingsEditModel(Domain.Event newEvent)
        {
            var attributes = JsonConvert.DeserializeObject<EventCourseRegSettings>(newEvent.EventAttributesSerialized);

            SeedCourseRecurrenceTime(attributes.RecurrenceTime);
            SeedCapacity();

            this.Capacity = attributes.Capacity;
            this.FinishDate = attributes.RecurrentEndDate;
            this.StartDate = attributes.RecurrentStartDate;
            this.FullPrice = attributes.FullPrice;
            this.HasProRatingPrice = attributes.HasProRatingPrice;
            this.HasSessionPrice = attributes.HasSessionPrice;
            this.EventType = EventTypeCategory.NewRegular;

            // if weeksCount exsists
            if (attributes.WeeksCount.HasValue)
            {
                WeeksCount = attributes.WeeksCount.Value;
                FinishDate = attributes.RecurrentEndDate.Value;
                FinishDateType = Models.FinishDateType.NumberOfSession;
            }
            else if (attributes.RecurrentEndDate.HasValue)
            {
                FinishDate = attributes.RecurrentEndDate.Value;
                FinishDateType = Models.FinishDateType.EndDate;
            }
            else
            {
                FinishDate = null;
                FinishDateType = FinishDateType.UnSpecifiedEndDate;
            }

            this.TotalSessions = Ioc.ICourseDbService.CalculateTotalSessions(StartDate.Value, FinishDate, attributes.RecurrenceTime);
            this.NumOfSessionLeft = Ioc.ICourseDbService.CalculateLeftSessions(StartDate.Value, FinishDate, attributes.RecurrenceTime);

            if (HasSessionPrice)
            {
                this.SessionPrice = attributes.SessionPrice;
            }

            if (HasProRatingPrice && NumOfSessionLeft.HasValue)
            {
                ProRatingPrice = Math.Floor((FullPrice.Value * NumOfSessionLeft.Value) / TotalSessions);
            }

            if (attributes.EarlybirdDiscounts != null && attributes.EarlybirdDiscounts.Count > 0)
            {
                this.EarlybirdDiscounts = attributes.EarlybirdDiscounts;
            }
        }
        #endregion

        private void SeedFinishDate()
        {
            int numWeeks = 0;
            TotalSessions = 0;
            DateTime tmpStart = (DateTime)StartDate;
            DayOfWeek dayofWeekend = tmpStart.AddDays(-1).DayOfWeek;
            while (numWeeks < WeeksCount)
            {
                foreach (CourseRecurrenceTimeViewModel rec in RecurrenceTime.RecurrenceTimes)
                {
                    if (tmpStart.DayOfWeek.ToString().Equals(rec.Day.ToString()) && rec.IsChecked)
                    {
                        TotalSessions++;
                        break;
                    }
                }
                tmpStart = tmpStart.AddDays(1);
                if (tmpStart.DayOfWeek.Equals(dayofWeekend))
                {
                    numWeeks++;
                }
            }
            FinishDate = tmpStart;
        }

        public static DateTime CalculateFinishDate(DateTime startDate, int weeksCount, List<CourseRecurrenceTime> recurrentTimes)
        {
            int numWeeks = 0;
            int totalSessions = 0;
            DateTime result = (DateTime)startDate;

            DayOfWeek dayofWeekend = result.AddDays(-1).DayOfWeek;
            while (numWeeks < weeksCount)
            {
                foreach (CourseRecurrenceTime rec in recurrentTimes)
                {
                    if (result.DayOfWeek.ToString().Equals(rec.Day.ToString()))
                    {
                        totalSessions++;
                        break;
                    }
                }
                result = result.AddDays(1);
                if (result.DayOfWeek.Equals(dayofWeekend))
                {
                    numWeeks++;
                }
            }


            return result;
        }

        private void SeedCapacity()
        {
            var capacityList = new List<SelectListItem>();
            for (int i = 1; i <= 1000; i++)
            {
                capacityList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            CapacityList = new SelectList(capacityList, "Value", "Text");
        }

        public bool HasSession(DateTime start, DateTime end, List<CourseRecurrenceTimeViewModel> RecurrenceTime)
        {
            int totalSessions = 0;
            bool isOnProgress = start.CompareTo(DateTime.Now) < 0;

            DateTime tmpDate = DateTime.Now;
            bool isCounting = false;
            var recurences = RecurrenceTime.Where(c => c.IsChecked).ToList();
            while (!(start.CompareTo(end) > 0))
            {
                if (isOnProgress && start.Date.Equals(tmpDate.Date))
                {
                    isCounting = true;
                }

                foreach (CourseRecurrenceTimeViewModel recTime in recurences)
                {
                    if (start.DayOfWeek.ToString().Equals(recTime.Day.ToString()))
                    {
                        totalSessions++;

                        break;
                    }
                }

                start = start.AddDays(1);
                if (isCounting)
                {
                    tmpDate = tmpDate.AddDays(1);
                }
            }

            return totalSessions > 0;
        }

        private void SeedCourseRecurrenceTime(ICollection<CourseRecurrenceTime> list)
        {
            //RecurrenceTime = new HashSet<CourseRecurrenceTimeViewModel>();
            RecurrenceTime = new MeetUpTimes();
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Sunday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Monday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Tuesday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Wednesday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Thursday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Friday });
            RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Saturday });

            if (list != null)
            {
                foreach (var item in list)
                {
                    var recDb = RecurrenceTime.RecurrenceTimes.Single(p => p.Day == item.Day);
                    if (recDb != null)
                    {
                        RecurrenceTime.RecurrenceTimes.Remove(recDb);
                        RecurrenceTime.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = item.Day, FinishTime = item.FinishTime, StartTime = item.StartTime, IsChecked = true });
                    }
                }
            }

            RecurrenceTime.RecurrenceTimes = RecurrenceTime.RecurrenceTimes.OrderBy(c => c.Day).ToList();
        }

        public int ClubId { get; set; }

        public int ReservedCapacity { get; set; }

        public SelectList CapacityList { get; set; }

        public FinishDateType FinishDateType { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Start date")]
        public DateTime? StartDate { get; set; }

        [DisplayName("Capacity")]
        public int? Capacity { get; set; }

        [Required]
        public MeetUpTimes RecurrenceTime { get; set; }

        [Required]
        [DisplayName("All sessions price")]
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be a positive number")]
        public decimal? FullPrice { get; set; }

        [DisplayName("Do you have per session (drop-in) price?")]
        public decimal? SessionPrice { get; set; }

        [DisplayName("Allow late registration and prorate the price?")]
        public bool HasProRatingPrice { get; set; }

        [DisplayName("Number of weeks")]
        public int? WeeksCount { get; set; }

        [DisplayName("External registration url")]
        public string ExternalRegUrl { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [DisplayName("Finish date")]
        public DateTime? FinishDate { get; set; }

        public MetaData MetaData { get; set; }

        public bool HasSessionPrice { get; set; }

        public int? NumOfSessionLeft { get; set; }

        public decimal ProRatingPrice { get; set; }

        public int TotalSessions { get; set; }

        public List<EventEarlybirdDiscount> EarlybirdDiscounts { get; set; }
    }
}