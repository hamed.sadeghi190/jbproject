﻿namespace Jumbula.Common.Enums
{
    public enum ReportExportType
    {
        Excel = 0,
        Pdf = 1
    }
}
