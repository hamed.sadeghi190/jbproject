﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class CampaignViewModelStep4 : BaseViewModel<Campaign>
    {
        public CampaignViewModelStep4()
        {
          
            MailTemplate = new EmailTemplateModel();
        }
        public CampaignScheduleType ScheduleType { get; set; }

        public EmailCampaignStep LastCreatedPage { get; set; }
        public EmailTemplateModel MailTemplate { get; set; }

        public string Body { get; set; }

        public string From { get; set; }
        public string ReplyTo { get; set; }
        public bool IsReview { get; set; }
        public string CampaignName { get; set; }
        public string Subject { get; set; }
        public string ScheduleMethod { get; set; }
        public DateTime? SendDate { get; set; }
        public string SelectedTemplateId { get; set; }

        public bool IsUserRestrictedManager { get; set; }
        public List<AttachmentModel> Attachments { get; set; }
        public string StrSendDate
        {
            get
            {
                var date = SendDate.HasValue;
                if (date)
                {
                    return SendDate.Value.ToString("MMM dd, yyyy, h:mm tt");
                }
                else
                {
                    return null;
                }

            }
        }

        [Display(Name = "Send Time")]
        public DateTime? SendTime { get; set; }
    }
}