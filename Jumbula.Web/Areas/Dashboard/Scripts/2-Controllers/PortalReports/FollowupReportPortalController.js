﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('FollowupReportPortalController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;
        $scope.IsReportRun = false;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;


        $scope.Model.Filters = {};
        $scope.Model.Year = 0;
        $scope.Model._SelectedClubs = [];
        $scope.ClubTypes = [{ Value: "school", Title: "Schools" }, { Value: "provider", Title: "Providers" }, { Value: "district", Title: "School districts" }];

        $scope.Model.IsAllPrograms = "true";

        $scope.Model.ClubType = "school";
        $scope.Model.IsAllClubs = "true";

        $scope.updateChosenRadio = function () {
            $scope.Model.IsAllClubs = "true";
        }


        $scope.getClubs = function (clubType) {
            if (clubType === "school")
                getAllSchools();
            else if (clubType === "provider")
                getAllProviders();
            else
                getAllDistricts();
        }

        $http.get(baseUrl + '/GetFullSeasonClassListReportHeader').success(function (data) {

            $scope.Years = data.Years;
            $scope.Model.AllSeason = data.Seasons;

            $scope.Model.Year = data.Years[0].Value;
            $scope.Model.SeasonName = data.Seasons[0].Value;
        }).error(function (data) {
            jbToast.error("error " + data);
        });



        $scope.filterData = function () {
            $scope.followupReportGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters = {};
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.Status = "regular";
            $scope.Model.Filters.FollowupFilter = "None";
            $scope.searchTerm = "";
            $scope.followupReportGridOptions.dataSource.read();
            $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model }).success(function (data, status, headers, config) {
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            });
        }

        $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model })
            .success(function (data, status, headers, config) {

                $scope.Model.Filters = {};
                $scope.Model.Filters.Status = "regular";
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
                $scope.Model.Filters.FollowupFilter = "None";

            }).error(function (data, status, headers, config) {
            });

        $scope.prepareGrid = function () {

            if (!$scope.IsReportRun) {

                $scope.IsReportRun = true;                

                $scope.followupReportsDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport:
                        {
                            read: {
                                url: baseUrl + '/FollowupReportRun',
                                type: "POST",
                                data: function () {
                                    return {
                                        model: $scope.Model,
                                        term: $scope.searchTerm,
                                    };
                                },
                            },

                        },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                    serverSorting: false,
                    serverPaging: true,
                    pageSize: 50,
                    batch: true,
                    resizable: true,
                });

                $scope.followupReportGridOptions = {
                    dataSource: $scope.followupReportsDataSource,
                    sortable: { mode: 'single' },
                    resizable: true,
                    pageable: {
                        refresh: true,
                        pageSizes:
                            [
                                50, 100, 200
                            ],
                        buttonCount: 5
                    },
                    dataBound: function () {
                        $('td[data-status="-"]').html("-");
                        var grid = $("#followupReportGrid").data("kendoGrid");
                        grid.thead.closest('table').css({ "max-width": "initial", "min-width": "110px" });
                        grid.tbody.closest('table').css({ "max-width": "initial", "min-width": "110px" });

                        customReportService.initializeDoubleScroll(grid);
                    },
                    columns: [
                        {
                            field: "ClubName",
                            title: "School",
                            width: "140px",
                        },
                        {
                            field: "FullName",
                            title: "Participant",
                            width: "140px",
                        },
                        {
                            field: "Email",
                            title: "Email",
                            width: "150px"
                        }, {
                            field: "ProgramName",
                            title: "Program",
                            width: "auto"
                        },
                        {
                            field: "RegDate",
                            title: "Registration date",
                            width: "140px",
                        },
                        {
                            field: "ConfirmationId",
                            title: "Confirmation",
                            width: "130px"
                        },
                        {
                            title: "Form",
                            template: '' +
                                '<table>' +
                                '<tbody>' +
                                '<tr ng-repeat=\'row in dataItem.FollowupForms\'>' +
                                '<td style="width:auto">{{row.FormName}}</td>' +
                                '</tr>' +
                                '</tbody>' +
                                '</table>',
                            filed: "FollowupForms",
                            width: "auto"
                        },
                        {
                            title: "Status",
                            template: '' +
                                '<table>' +
                                '<tbody>' +
                                '<tr ng-repeat=\'row in dataItem.FollowupForms\'>' +
                                '<td style="width:40px">{{row.Status}}</td>' +
                                '</tr>' +
                                '</tbody>' +
                                '</table>',
                            filed: "FollowupForms",
                            width: "auto"
                        }
                    ]
                }
            } else {

                $scope.followupReportGridOptions.dataSource.page(1);
                $("#followupReportGrid").data("kendoGrid").dataSource.read();
            }

        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $http.post(baseUrl + '/FollowupReportRun', { model: model, filters: model.Filters, isExportToExcel: true, term: $scope.searchTerm })
                .success(function (data, status, headers, config) {
                    window.location.href = data.Data;
                    cfpLoadingBar.complete();

                });
        }

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/FollowupReportRun', { model: model, filters: model.Filters, makeCampaign: true, term: $scope.searchTerm })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        $scope.goOrderDetail = function (orderId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderDetail($scope.MC.stateParams.seasonDomain, orderId);
        }

        $scope.goOrderItemDetail = function (orderItemId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderItemDetail($scope.MC.stateParams.seasonDomain, orderItemId);
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "FollowupForm" });
        }

        function getAllSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data) {
                    $scope.AllClubs = data.AllSchools;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllProviders() {
            $http.get(baseUrl + "/GetAllProvider")
                .success(function (data) {
                    $scope.AllClubs = data;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch('AllClubs', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllDistricts() {
            $http.get(baseUrl + '/GetAllPartnerSchoolDistricts')
                .success(function (data) {
                    $scope.AllClubs = data.AllDisctricts;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

    }]);
})();