﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class MailListContact : BaseEntity
    {
        [Required]
        [StringLength(128)]
        [EmailAddress]
        public string EmailAddress { get; set; }

        [StringLength(128)]
        public string RecipientName { get; set; }

        [StringLength(128)]
        public string RecipientLastName { get; set; }

        [NotMapped]
        public string FullName { get { return $"{RecipientName} {RecipientLastName}"; } }

        public int? MailList_Id { get; set; }

        [ForeignKey("MailList_Id")]
        public virtual MailList List { get; set; }
    }
}
