﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using Jumbula.Common.Attributes;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Core.Model.Notification;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.ClubsService;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.WebLogic.UserService
{
    public static class JbUserService
    {
        #region User Data

        private static CurrentUser GetCurrentUser(HttpContext httpContext)
        {
            return GetAdditionalData(httpContext).CurrentUser as CurrentUser;
        }

        #endregion region

        #region Additional Data

        private static UserAdditionalData GetAdditionalData(HttpContext httpContext)
        {
            if (httpContext.Items["AdditionalData"] != null)
            {
                return httpContext.Items["AdditionalData"] as UserAdditionalData;
            }

            var result = httpContext.User.Identity.GetAdditionalData<UserAdditionalData>();
            httpContext.Items.Add("AdditionalData", result);

            return result;
        }

        #endregion

        #region Is Authenticated

        public static bool IsUserAuthenticated()
        {
            return IsUserAuthenticated(GetHttpContext());
        }

        public static bool IsUserAuthenticated(this Controller controller)
        {
            return IsUserAuthenticated(GetHttpContext(controller));
        }

        public static bool IsUserAuthenticated(this WebPageRenderingBase webPage)
        {
            return IsUserAuthenticated(GetHttpContext(webPage));
        }

        private static bool IsUserAuthenticated(HttpContext httpContext)
        {
            return httpContext.User.Identity.IsAuthenticated;
        }

        #endregion

        #region User Name

        public static string GetCurrentUserName()
        {
            return GetCurrentUserName(GetHttpContext());
        }

        public static string GetCurrentUserName(this Controller controller)
        {
            return GetCurrentUserName(GetHttpContext(controller));
        }

        public static string GetCurrentUserName(this WebPageRenderingBase webPage)
        {
            return GetCurrentUserName(GetHttpContext(webPage));
        }

        private static string GetCurrentUserName(HttpContext httpContext)
        {
            return httpContext.User.Identity.GetUserName();
        }

        #endregion

        #region User Full Name

        public static string GetCurrentUserFullName()
        {
            return GetCurrentUserFullName(GetHttpContext());
        }

        public static string GetCurrentUserFullName(this Controller controller)
        {
            return GetCurrentUserFullName(GetHttpContext(controller));
        }

        public static string GetCurrentUserFullName(this WebPageRenderingBase webPage)
        {
            return GetCurrentUserFullName(GetHttpContext(webPage));
        }

        private static string GetCurrentUserFullName(HttpContext httpContext)
        {
            return httpContext.User.Identity.GetUserName().Split('@')[0];
        }

        #endregion

        #region User Id

        public static int GetCurrentUserId()
        {
            return GetCurrentUserId(GetHttpContext());
        }

        public static int GetCurrentUserId(this Controller controller)
        {
            return GetCurrentUserId(GetHttpContext(controller));
        }

        public static int GetCurrentUserId(this WebPageRenderingBase webPage)
        {
            return GetCurrentUserId(GetHttpContext(webPage));
        }

        private static int GetCurrentUserId(HttpContext httpContext)
        {
            return httpContext.User.Identity.GetUserId<int>();
        }

        #endregion

        #region User Role

        public static RoleCategory GetCurrentUserRole()
        {
            return GetCurrentUserRole(GetHttpContext());
        }

        public static RoleCategory GetCurrentUserRole(this Controller controller)
        {
            return GetCurrentUserRole(GetHttpContext(controller));
        }

        public static RoleCategory GetCurrentUserRole(this WebPageRenderingBase webPage)
        {
            return GetCurrentUserRole(GetHttpContext(webPage));
        }

        private static RoleCategory GetCurrentUserRole(HttpContext httpContext)
        {
            if (IsUserAuthenticated(httpContext))
            {
                return GetCurrentUser(httpContext).Role;
            }

            throw new Exception("User is not authenticated");
        }

        #endregion

        #region User Role Type

        public static RoleCategoryType GetCurrentUserRoleType()
        {
            return GetCurrentUserRoleType(GetHttpContext());
        }

        public static RoleCategoryType GetCurrentUserRoleType(this Controller controller)
        {
            return GetCurrentUserRoleType(GetHttpContext(controller));
        }

        public static RoleCategoryType GetCurrentUserRoleType(this WebPageRenderingBase webPage)
        {
            return GetCurrentUserRoleType(GetHttpContext(webPage));
        }

        private static RoleCategoryType GetCurrentUserRoleType(HttpContext httpContext)
        {
            return RoleTypeAttribute.GetRoleType(GetCurrentUserRole(httpContext));
        }

        #endregion

        #region User Notification

        public static IEnumerable<NotificationModel> GetUserNotifications()
        {
            var userId = GetCurrentUserId();
            var club = GetCurrentClubBaseInfo(GetHttpContext());
            var notificationQueue = new List<NotificationModel>();
            var notificationModel = Ioc.PeopleBusiness.GetDelinquentNotificationItem(userId, club);

            if(notificationModel != null) notificationQueue.Add(notificationModel);

            return notificationQueue.AsEnumerable();
        }

        #endregion

        #region Club

        public static ClubBaseInfoModel GetCurrentClubBaseInfo()
        {
            return GetCurrentClubBaseInfo(GetHttpContext());
        }

        public static ClubBaseInfoModel GetCurrentClubBaseInfo(this Controller controller)
        {
            return GetCurrentClubBaseInfo(GetHttpContext(controller));
        }

        public static ClubBaseInfoModel GetCurrentClubBaseInfo(this WebPageRenderingBase webPage)
        {
            return GetCurrentClubBaseInfo(GetHttpContext(webPage));
        }

        private static ClubBaseInfoModel GetCurrentClubBaseInfo(HttpContext httpContext)
        {
            if (IsUserAuthenticated(httpContext))
            {
                return GetAuthenticatedCurrentClubBaseInfo(httpContext);
            }

            return GetNotAuthenticatedCurrentClubBaseInfo(httpContext);
        }

        private static ClubBaseInfoModel GetAuthenticatedCurrentClubBaseInfo(HttpContext httpContext)
        {
            return GetAdditionalData(httpContext).CurrentOrganization as ClubBaseInfoModel;
        }

        private static ClubBaseInfoModel GetNotAuthenticatedCurrentClubBaseInfo(HttpContext httpContext)
        {
            ClubBaseInfoModel clubBaseInfo = null;

            if (httpContext.Request.Cookies["ClubBaseInfo"] != null && !string.IsNullOrWhiteSpace(httpContext.Request.Cookies["ClubBaseInfo"].Value))
            {
                try
                {
                    clubBaseInfo =
                        JsonConvert.DeserializeObject<ClubBaseInfoModel>(
                            StringCipher.Decrypt(httpContext.Request.Cookies["ClubBaseInfo"].Value,
                                DateTime.UtcNow.ToShortDateString().Replace("/", "")));
                }
                catch
                {

                }
            }

            if (clubBaseInfo == null)
            {
                var clubDomain = GetCurrentClubDomain(httpContext);

                var club = Ioc.ClubBusiness.GetBaseInfo(clubDomain);

                clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);

                var cookie = new HttpCookie("ClubBaseInfo",
                    StringCipher.Encrypt(JsonConvert.SerializeObject(clubBaseInfo),
                        DateTime.UtcNow.ToShortDateString().Replace("/", "")));

                cookie.Expires = DateTime.Now.AddMinutes(10);

                httpContext.Response.Cookies.Add(cookie);
            }

            return clubBaseInfo;
        }

        #endregion

        #region Club Name

        public static string GetCurrentClubName()
        {
            return GetCurrentClubBaseInfo(GetHttpContext()).Name;
        }

        public static string GetCurrentClubName(this Controller controller)
        {
            return GetCurrentClubBaseInfo(GetHttpContext(controller)).Name;
        }

        public static string GetCurrentClubName(this WebPageRenderingBase webPage)
        {
            return GetCurrentClubBaseInfo(GetHttpContext(webPage)).Name;
        }

        #endregion

        #region Club Id

        public static int GetCurrentClubId()
        {
            return GetCurrentClubBaseInfo(GetHttpContext()).Id;
        }

        public static int GetCurrentClubId(this Controller controller)
        {
            return GetCurrentClubBaseInfo(GetHttpContext(controller)).Id;
        }

        public static int GetCurrentClubId(this WebPageRenderingBase webPage)
        {
            return GetCurrentClubBaseInfo(GetHttpContext(webPage)).Id;
        }

        #endregion

        #region Club Domain

        public static string GetCurrentClubDomain()
        {
            return GetCurrentClubDomain(GetHttpContext());
        }

        public static string GetCurrentClubDomain(this Controller controller)
        {
            return GetCurrentClubDomain(GetHttpContext());
        }

        public static string GetCurrentClubDomain(this WebPageRenderingBase webPage)
        {
            return GetCurrentClubDomain(GetHttpContext(webPage));
        }

        private static string GetCurrentClubDomain(HttpContext httpContext)
        {
            if (GetCurrentRequestType(httpContext) == RequestType.Branded)
            {
                return httpContext.Request.Url.GetSubDomain();
            }

            return String.Empty;
        }

        #endregion

        #region Request type

        public static RequestType GetCurrentRequestType()
        {
            return GetCurrentRequestType(GetHttpContext());
        }

        public static RequestType GetCurrentRequestType(this Controller controller)
        {
            return GetCurrentRequestType(GetHttpContext(controller));
        }

        public static RequestType GetCurrentRequestType(this WebPageRenderingBase webPage)
        {
            return GetCurrentRequestType(GetHttpContext(webPage));
        }

        private static RequestType GetCurrentRequestType(HttpContext httpContext)
        {

            if (httpContext.Request.Url.HasSubDaomin())
            {
                return RequestType.Branded;
            }

            return RequestType.NotBranded;
        }
        #endregion

        #region Check if user is admin of club

        public static bool IsUserAdminForClub(string clubDomain, int? userId)
        {
            return IsUserAdminForClub(GetHttpContext(), clubDomain, userId);
        }

        public static bool IsUserAdminForClub(this Controller controller, string clubDomain, int? userId = null)
        {
            return IsUserAdminForClub(GetHttpContext(controller), clubDomain, userId);
        }
        public static bool IsUserAdminForClub(this Controller controller, int clubId, int? userId = null)
        {
            return IsUserAdminForClub(GetHttpContext(controller), clubId, userId);
        }
        public static bool IsUserAdminForClub(this WebPageRenderingBase webPage, string clubDomain, int? userId = null)
        {
            return IsUserAdminForClub(GetHttpContext(webPage), clubDomain, userId);
        }

        private static bool IsUserAdminForClub(HttpContext httpContext, string clubDomain, int? userId = null)
        {
            var club = GetCurrentClubBaseInfo(httpContext);
            userId = userId.HasValue ? userId.Value : GetCurrentUserId(httpContext);

            if (!club.Domain.Equals(clubDomain, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            return IsUserAdminForClub(httpContext, club.Id, userId);
        }

        private static bool IsUserAdminForClub(HttpContext httpContext, int clubId, int? userId = null)
        {

            userId = userId ?? GetCurrentUserId(httpContext);

            var club = Ioc.ClubBusiness.Get(clubId);
            var result = club.ClubStaffs.Any(s => s.JbUserRole.UserId == userId);

            if (!result && club.DistrictId.HasValue)
            {
                var roles = new List<string> { RoleCategory.Admin.ToString(), RoleCategory.Manager.ToString(), RoleCategory.OnsiteSupportManager.ToString() };

                result = club.District.ClubStaffs.Any(s => s.JbUserRole.UserId == userId && roles.Contains(s.JbUserRole.Role.Name));
            }

            if (!result && club.PartnerId.HasValue)
            {
                var roles = new List<string> { RoleCategory.Partner.ToString(), RoleCategory.Admin.ToString(), RoleCategory.Manager.ToString() };

                result = club.PartnerClub.ClubStaffs.Any(s => s.JbUserRole.UserId == userId && roles.Contains(s.JbUserRole.Role.Name));
            }

            if (!result)
            {
                var userName = GetCurrentUserName(httpContext);

                result = Ioc.UserProfileBusiness.IsUserInRole(userName, RoleCategory.Support);
            }

            return result;
        }
        #endregion

        #region SwitchedParentUser

        public static bool IsSwitchedAsParentMode()
        {
            var result = false;
            var currentUserId = 0;
            RoleCategoryType? roleCategoryType = null;

            var httpContext = GetHttpContext();

            if(!IsUserAuthenticated())
            {
                return false;
            }

            currentUserId = GetCurrentUserId();
            roleCategoryType = GetCurrentUserRoleType();

            if (roleCategoryType != RoleCategoryType.Parent && httpContext.Request.Cookies["SwitchedParent"] != null && httpContext.Request.Cookies["SwitchedParent"][currentUserId.ToString()] != null)
            {
                var userIdParameter = httpContext.Request.Cookies["SwitchedParent"][currentUserId.ToString()];

                result = !string.IsNullOrEmpty(userIdParameter);
            }

            return result;
        }

        public static int GetSwitchedParentUserId(int adminUserId)
        {
            int result = 0;

            var httpContext = GetHttpContext();

            if (httpContext.Request.Cookies["SwitchedParent"] != null && httpContext.Request.Cookies["SwitchedParent"][adminUserId.ToString()] != null)
            {
                var userIdParameter = httpContext.Request.Cookies["SwitchedParent"][adminUserId.ToString()];

                result = int.Parse(userIdParameter);

                var currentClubId = JbUserService.GetCurrentClubId();
                var currentClub = JbUserService.GetCurrentClubBaseInfo();
                var isPartner = currentClub.ClubType.EnumType == ClubTypesEnum.Partner;
                var isParentDashboardAccesibleByAdmin = Ioc.UserProfileBusiness.IsParentDashboardAccesibleByAdmin(currentClubId, result, isPartner);

                if (!isParentDashboardAccesibleByAdmin)
                {
                    throw new Exception("Admin hasn't access to parent dashboard.");
                }
            }
            else
            {
                throw new Exception("Admin hasn't access to parent dashboard.");
            }

            return result;

        }
        #endregion

        #region Ensure user is admin for club

        public static void EnsureUserIsAdminForClub(string domain)
        {
            EnsureUserIsAdminForClub(GetHttpContext(), domain);
        }

        public static void EnsureUserIsAdminForClub(this Controller controller, string domain)
        {
            EnsureUserIsAdminForClub(GetHttpContext(controller), domain);
        }

        public static void EnsureUserIsAdminForClub(this WebPageRenderingBase webPage, string domain)
        {
            EnsureUserIsAdminForClub(GetHttpContext(webPage), domain);
        }

        private static void EnsureUserIsAdminForClub(HttpContext httpContext, string domain)
        {
            return;
            if (!IsUserAdminForClub(httpContext, domain))
            {
                throw new Exception(string.Format(Constants.F_UserIsNotAdminForClub, GetCurrentUserName(httpContext),
                    domain));
            }
        }

        #endregion

        #region HttpContext

        private static HttpContext GetHttpContext()
        {
            return HttpContext.Current;
        }

        private static HttpContext GetHttpContext(Controller controller)
        {
            return controller.HttpContext.ApplicationInstance.Context;
        }

        private static HttpContext GetHttpContext(WebPageRenderingBase webPage)
        {
            return webPage.Request.RequestContext.HttpContext.ApplicationInstance.Context;
        }

        #endregion
    }
}