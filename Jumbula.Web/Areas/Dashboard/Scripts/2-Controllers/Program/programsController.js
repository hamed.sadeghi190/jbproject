﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });
        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain) {
                $scope.MC.currentSeason = value;
            }


        });

        $scope.array = ['a', 'b', 'c'];

        if (is.not.undefined(seasonService.getCopySeasonState()) && seasonService.getCopySeasonState()) {
            setTimeout(function () {
                window.location.reload();
            }, 10);
        }

        $scope.programDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport: {
                read:
                    {
                        url: window.location.origin + '/Dashboard/Program/GetList/',
                        contentType: "application/json; charset=utf-8", // tells the web method to serialize JSON
                        type: "POST" //use HTTP POST or get
                    },
                parameterMap: function (data, operation) {
                    if (operation == "read") {
                        data = $.extend(
                            {
                                sort: null,
                                filter: null,
                                seasonDomain: $scope.stateParams.seasonDomain,
                                nameSearchTerm: $scope.nameSearchTerm,
                                startDateCreatedSearchTerm: $scope.startDateCreatedSearchTerm,
                                endDateCreatedSearchTerm: $scope.endDateCreatedSearchTerm,
                                statusSearchTerm: $scope.statusSearchTerm,
                                typeSearchTerm: $scope.typeSearchTerm,
                            }, data);

                        return JSON.stringify(data);
                    }
                }
            },
            schema:
                {
                    total: "TotalCount",
                    data: "DataSource",
                },
            pageSize: 100,
            batch: true,
            serverFiltering: true,
            serverPaging: true,
            serverSorting: false,
            requestStart: function (e) {
                kendo.ui.progress($('[kendo-grid]'), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($('[kendo-grid]'), false);
            }
        });

        $scope.programGridOptions = {
            dataSource: $scope.programDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [{
                field: "Name",
                title: "Name",
                template: "<strong class='grid-row-title'>#= Name #</strong> #if(SaveType == 'Publish') {# #if(Status == 'Open') {# <i class='grid-label label success'>Setup completed</i> #}else {# <i class='grid-label label info'>Frozen</i> #}# #}else {# <i class='grid-label label warning'>Draft</i> #if(RespondStatus == 'Accept') {# <i class='grid-label label success'>Accepted</i> #}else if(RespondStatus == 'Modify') {# <i class='grid-label label info'>Modified</i> #}else if(RespondStatus == 'Decline') {# <i class='grid-label label error'>Rejected</i> #}# #}#"
            }, {
                field: "TypeCategory",
                title: "Type",
                width: "140px"
            },
            {
                field: "Days",
                title: "Day/Time",
                width: "160px"

            },
            {
                field: "GradeAge",
                title: "Grade/Age"
            },
            {
                field: "OutSourceClubAndSeasonNameForProvider",
                title: "School",
                hidden: true
            },
            {
                field: "OutSourceClubAndSeasonNameForSchool",
                title: "Provider",
                hidden: true
            },
            {
                field: "",
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                               #if(SaveType=='Draft'){#\
                                 #if(LastCreatedPage == 'Step1'){#\
                                      <li ngaction='Program_Edit' ui_sref='ProgramAddEditStep2({seasonDomain:MC.stateParams.seasonDomain, programType: \"#=TypeCategory#\", programId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Continue</span>\
                                      </li>\
                                  #}\
                                  else{#\
                                          <li ngaction='Program_Draft_View'>\
                                                 <a class='k-link jbi-show' target='_blank' href='#:ViewPageUrl#'>View</a>\
                                          </li>\
                                 #if(LastCreatedPage == 'Step2'){#\
                                     #if(TypeCategory == 'BeforeAfterCare'){#\
                                            <li ngaction='Program_Edit' ui_sref='ProgramAddEditStep2({seasonDomain:MC.stateParams.seasonDomain, programType: \"#=TypeCategory#\", programId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Continue</span>\
                                      </li>\
                                  #}\
                                    else{#\
                                            <li ngaction='Program_Edit' ui_sref='ProgramAddEditStep3({seasonDomain:MC.stateParams.seasonDomain, programType: \"#=TypeCategory#\", programId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Continue</span>\
                                      </li>\
                                      #}#\
                                  #}\
                                  else if(LastCreatedPage == 'Step3'){#\
                                            <li ngaction='Program_Edit' ui_sref='ProgramAddEditStep4({seasonDomain: MC.stateParams.seasonDomain, programType: \"#=TypeCategory#\", programId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Continue</span>\
                                      </li>\
                                   #}#\
                               #}#\
                              #if(OutSourceProgramStatus=='Invitation' && IsProvider){#\
                                  <li ngaction='Program_RespondInvitation' ui_sref='ProgramRespondToInvitation({seasonDomain:MC.stateParams.seasonDomain,  programId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                            <span class='k-link jbi-angle-right'>Respond</span>\
                                   </li>\
                                   #}#\
                                 #}\
                                else{#\
                                  <li ngaction='Program_Edit' ui_sref='ProgramAddEditStep1({seasonDomain: MC.stateParams.seasonDomain, programType: \"#=TypeCategory#\", programId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                        <span class='k-link jbi-pencil'>Edit basic info</span>\
                                  </li>\
                                  <li ngaction='Program_Edit' ui_sref='ProgramAddEditStep2({seasonDomain:MC.stateParams.seasonDomain, programType: \"#=TypeCategory#\", programId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                        <span class='k-link jbi-pencil'>Edit schedules & fees</span>\
                                  </li>\
                                  <li ngaction='Program_Edit' ui_sref='ProgramAddEditStep3({seasonDomain: MC.stateParams.seasonDomain, programType: \"#=TypeCategory#\", programId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
                                        <span class='k-link jbi-pencil'>Edit forms</span>\
                                  </li>\
                               #}#\
                                    <li disabled='disabled'><hr /></li>\
                                    <li ngaction='Program_Delete' ng-click='deleteProgram(#:Id#)'>\
                                          <span class='k-link jbi-remove'>Delete</span>\
                                    </li>\
                                    #if(SaveType == 'Publish'){#\
                                       #if(Status == 'Open' && TypeCategory != 'BeforeAfterCare'){#\
                                          <li ngaction='Program_Freeze' ng-click='freezeProgram(#:Id#)'>\
                                                <span class='k-link jbi-ice'>Freeze</span>\
                                          </li>\
                                       #}\
                                       else if(TypeCategory != 'BeforeAfterCare'){#\
                                          <li ngaction='Program_Freeze' ng-click='unFreezeProgram(#:Id#)'>\
                                                <span class='k-link jbi-ice'>Unfreeze</span>\
                                          </li>\
                                       #}#\
                                       #if(TypeCategory != 'Subscription'){#\
                                          <li ngaction='Program_Copy'  ng-click='copyProgram(#=Id#, \"#=TypeCategory#\")' data-typeCategory ='#=TypeCategory#)'>\
                                                <span class='k-link jbi-files'>Copy</span>\
                                          </li>\
                                       #}#\
                                          <li ngaction='Program_View'>\
                                                <a class='k-link jbi-show' target='_blank' href='#:ViewPageUrl#'>View</a>\
                                          </li>\
                                          <li ngaction='ProgramSession_List_View' ng-hide='#= TypeCategory ==\'ChessTournament\' || TypeCategory ==\'Calendar\' || TypeCategory ==\'SeminarTour\' || TypeCategory ==\'BeforeAfterCare\' || TypeCategory ==\'Subscription\'|| TypeCategory ==\'Camp\' #' ng-click='sessions(#=Id#)'>\
                                              <span class='k-link jbi-angle-right'>Sessions</span>\
                                          </li>\
                                          #if(TypeCategory == 'BeforeAfterCare'){ #<hr />\
                                              #if(Status == 'Open'){#\
                                                  <li ngaction='Program_Freeze' ng-click='freezeProgram(#:Id#)'>\
                                                            <span class='k-link jbi-ice'>Freeze entire program</span>\
                                                      </li>\
                                                <ul ng-repeat='schedule in dataItem.SchedulesDetail'>\
                                                       <li ng-if='!schedule.IsFreezed' ng-hide='dataItem.NumberOfScheduleNotFreezed == 1' class='k-link jbi-ice' style='padding-left: 14px; padding-top: 3px; padding-bottom: 5px;color: gray' ng-click='freezeProgramSchedule(schedule.Id)'>\
                                                             <span style='padding-left: 5px; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;display: inline-block; width: 180px;'>Freeze {{schedule.Title}}</span>\
                                                       </li>\
                                                       <li ng-if='schedule.IsFreezed' class='k-link jbi-ice' style='padding-left: 14px; padding-top: 3px; padding-bottom: 5px;color: gray' ng-click='UnfreezeProgramSchedule(schedule.Id)'>\
                                                             <span style='padding-left: 5px; white-space: nowrap; overflow: hidden !important; text-overflow: ellipsis;display: inline-block; width: 180px;'>Unfreeze {{schedule.Title}}</span>\
                                                       </li>\
                                               </ul>\
                                                  #}\ else{#\
                                                          <li ngaction='Program_Freeze' ng-click='unFreezeProgram(#:Id#)'>\
                                                                <span class='k-link jbi-ice'>Unfreeze entire program</span>\
                                                          </li>\
                                                       #}#\
                                        #}#\
                                    #}#\
                            </ul>\
                        </li>\
                    </ul>",
            }],
        };

        $http.get(window.location.origin + '/Dashboard/Program/AnyOutSourcePrograms?seasonDomain=' + $scope.stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model = data;
            if ($scope.Model.ClubType == "Provider" && $scope.Model.HasOutSource) {
                $("#programGrid").data("kendoGrid").showColumn(4);

            }
            if ($scope.Model.ClubType == "School" && $scope.Model.HasOutSource) {
                $("#programGrid").data("kendoGrid").showColumn(5);
            }

        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });
        $scope.programSearch = function () {
            $("#programGrid").data("kendoGrid").dataSource.read();
        }

        $scope.programSearchClearFilters = function () {
            $scope.nameSearchTerm =
                $scope.startDateCreatedSearchTerm =
                $scope.endDateCreatedSearchTerm =
                $scope.statusSearchTerm =
                $scope.typeSearchTerm = "";

            $("#programGrid").data("kendoGrid").dataSource.read();
        }

        $scope.switchAdvancedSearch = function () {

            if ($scope.advancedSearch) {
                $scope.startDateCreatedSearchTerm = '';
                $scope.endDateCreatedSearchTerm = '';
                $scope.statusSearchTerm = '';
                $scope.typeSearchTerm = '';
            }

            $scope.advancedSearch = !$scope.advancedSearch;
        }

        $scope.deleteProgram = function (programId) {
            jbConfirmation
                .yes(function () {
                    $http.post('Dashboard/Program/Delete', { id: programId })
                        .success(function (data, status, headers, config) {

                            if (data.Status == true) {

                                jbToast.success('Program deleted successfully.');

                                $scope.programGridOptions.dataSource.read();
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                })
                .confirm();
        }

        $scope.freezeProgramSchedule = function (scheduleId) {

            jbConfirmation
                .yes(function () {
                    $http.post('Dashboard/Program/FreezeSchedule', { id: scheduleId })
                        .success(function (data, status, headers, config) {

                            if (data.Status == true) {

                                jbToast.success('schedule frozen successfully.');

                                $scope.programGridOptions.dataSource.read();
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                })
                .confirm();
        }

        $scope.UnfreezeProgramSchedule = function (scheduleId) {

            jbConfirmation
                .yes(function () {
                    $http.post('Dashboard/Program/UnFreezeSchedule', { id: scheduleId })
                        .success(function (data, status, headers, config) {

                            if (data.Status == true) {

                                jbToast.success('schedule unfreezed successfully.');

                                $scope.programGridOptions.dataSource.read();
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                })
                .confirm();
        }

        $scope.copyProgram = function (programId, typeCategory) {


            $state.go('ProgramAddEditStep1', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: typeCategory, programId: programId, fromTemplate: true })
        }

        $scope.freezeProgram = function (programId) {
            jbConfirmation
                .yes(function () {
                    $http.post('Dashboard/Program/Freeze', { id: programId })
                        .success(function (data, status, headers, config) {

                            if (data.Status == true) {

                                jbToast.success('Program frozen successfully.');

                                $scope.programGridOptions.dataSource.read();
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                })
                .confirm();
        }

        $scope.unFreezeProgram = function (programId) {
            jbConfirmation
                .yes(function () {
                    $http.post('Dashboard/Program/UnFreeze', { id: programId })
                        .success(function (data, status, headers, config) {

                            if (data.Status == true) {

                                jbToast.success('Program unfreezed successfully.');

                                $scope.programGridOptions.dataSource.read();
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                })
                .confirm();
        }

        $scope.sessions = function (programId) {

            $state.go("ProgramSession", { seasonDomain: $stateParams.seasonDomain, programId: programId });
        }

        $scope.init = function () {
            $('[data-role=dropdown]').dropdown();
        }
    }]);
})();

