﻿using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class CountryBusiness :  ICountryBusiness
    {
        private readonly IEntitiesContext _dataContext;
        private readonly IRepository<Country> _countryRepository;
        public CountryBusiness(IRepository<Country> countryRepository, IEntitiesContext dataContext)
        {
            _countryRepository = countryRepository;
            _dataContext = dataContext;
        }

        public IQueryable<State> GetStates()
        {
            return _countryRepository.GetList().SingleOrDefault(c => c.ISO == "US").States.AsQueryable();
        }
        public IQueryable<State> GetAllStates()
        {
            return _countryRepository.GetList().SelectMany(c=>c.States).AsQueryable();
        }
        public State GetState(int stateId)
        {
            return _dataContext.Set<State>().Single(c => c.Id == stateId);
        }

        public IQueryable<County> GetCounties()
        {
            return _countryRepository.GetList().SingleOrDefault(c => c.ISO == "US").States.SelectMany(c => c.Counties).AsQueryable();
        }
        public IQueryable<Country> GetUsAndCsCountries()
        {
            return _countryRepository.GetList().Where(c => c.ISO == "US" || c.ISO == "CA");
        }

        public IQueryable<County> GetCounties(List<int> ids)
        {
            return _countryRepository.GetList().SingleOrDefault(c => c.ISO == "US").States.SelectMany(c => c.Counties).Where(c => ids.Contains(c.Id)).AsQueryable();
        }

        public IQueryable<County> GetCounties(int stateId)
        {
            return _countryRepository.GetList().SingleOrDefault(c => c.ISO == "US").States.SingleOrDefault(s => s.Id == stateId).Counties.AsQueryable();
        }

        public IQueryable<County> GetStatesCounties(List<int> states)
        {
            if (states != null && states.Any())
            {
                return _countryRepository.GetList().SingleOrDefault(c => c.ISO == "US").States.Where(s => states.Contains(s.Id)).SelectMany(a => a.Counties).AsQueryable();
            }

            return new List<County>().AsQueryable();
        }

        public County GetCounty(int countId)
        {
            return _countryRepository.GetList().SingleOrDefault(c => c.ISO == "US").States.SelectMany(c => c.Counties).Single(c => c.Id == countId);
        }
    }
}
