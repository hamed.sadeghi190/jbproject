﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SignDocumentViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "{0} name is required.")]
        public string Company { get; set; }
        public string SignedDate { get; set; }
        public DateTime Deadline { get; set; }
        public string Signature { get; set; }
        public bool IsSign { get; set; }
        [Required(ErrorMessage = "{0} is required.")]
        public string Title { get; set; }
        public string PageTitle { get; set; }
        public string StrDeadLine
        {
            get
            {

                return Deadline.ToString("MMM dd, yyyy");
            }
        }
    }
}