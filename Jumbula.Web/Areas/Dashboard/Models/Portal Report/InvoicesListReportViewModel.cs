﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class InvoicesListReportViewModel
    {
        public DateTime Date { get; set; }
        public string StrDate
        {
            get
            {

                return Date.ToString("MMM dd, yyyy");
            }
        }
        public long Id { get; set; }
        public long InvoiceNumber { get; set; }
        public int ClubId { get; set; }
        public string Status { get; set; }
        public string ClubName { get; set; }
        public decimal Amount { get; set; }
        public string StrAmount { get; set; }
        public string Recipient { get; set; }
        public int? DueDateValue { get; set; }
        public int UserId { get; set; }
        public DateTime DueDate
        {
            get
            {
                var dateTime = Date;

                if (DueDateValue > 0)
                {

                    return dateTime.AddDays(DueDateValue.Value);

                }
                else
                {

                    return dateTime;

                }

            }
        }

        public string StrDueDate
        {
            get
            {
                return DueDate.ToString("MMM dd, yyyy");
            }
        }
    }
}
