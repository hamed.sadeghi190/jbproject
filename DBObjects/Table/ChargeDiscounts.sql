
IF OBJECT_ID('[dbo].[ChargeDiscounts]') IS NULL
BEGIN
CREATE TABLE [dbo].[ChargeDiscounts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Category] [int] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Desc] [nvarchar](256) NULL,
	[Camp_Id] [int] NULL,
	[OrderItem_Id] [int] NULL,
	[OrderItem_Id1] [int] NULL,
	[Tourney_Id] [int] NULL,
	[Course_Id] [int] NULL,
	[Order_Id] [int] NULL,
	[Order_Id1] [int] NULL,
	[TTTourney_Id] [int] NULL,
	[ChargeDiscountType] [int] NOT NULL,
	[Detail] [xml] NULL,
	[Course_Id1] [int] NULL,
	[Course_Id2] [int] NULL,
	[Event_Id] [int] NULL,
	[Migrate] [bit] NULL)
END
GO

--<<ADD PRIMARY KEY>>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.ChargeDiscounts')
BEGIN
	ALTER TABLE [dbo].[ChargeDiscounts] ADD CONSTRAINT [PK_dbo.ChargeDiscounts] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

--<< ADD CLOLUMNS >>--
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.ChargeDiscounts') AND name='Event_Id')
BEGIN
	ALTER TABLE [dbo].[ChargeDiscounts] ADD [Event_Id] int null;
END
GO
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.ChargeDiscounts') AND name='Migrate')
BEGIN
	ALTER TABLE [dbo].[ChargeDiscounts] ADD [Migrate] [bit] NULL
END
GO


--<< ADD FOREIGN KEYS >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.ChargeDiscounts_dbo.Events_Event_Id')
BEGIN
	ALTER TABLE [dbo].[ChargeDiscounts] 
	ADD CONSTRAINT [FK_dbo.ChargeDiscounts_dbo.Events_Event_Id] FOREIGN KEY ([Event_Id]) 
		REFERENCES [dbo].[Events] ([Id]) 
		ON DELETE CASCADE
		ON UPDATE CASCADE
	;
END
GO