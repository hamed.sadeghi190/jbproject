﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('tourJbHomeSiteController', ['$scope', '$http', '$sce', '$stateParams', function ($scope, $http, $sce, $stateParams) {
        window.app.globalObjects.pageClass = 'settings-tour-view';
        window.app.globalObjects.initializeView();

        //window.app.globalObjects.pageClass = 'tour';

        $scope.MC.stateParams = $stateParams;

        $scope.AllTours = {
            Tours: [
                {
                    YouTubeId: "-Bcnga0qMNI",
                    Title: "How to create a class",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "TCSFh4ymbUA",
                    Title: "How to create a custom discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },

                {
                    YouTubeId: "B0UffieZ5X4",
                    Title: "How to create a multi-program discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
                {
                    YouTubeId: "cVi7DX2jLN4",
                    Title: "How to create a sibling discount?",
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                },
            ]
        };

        for (var i = 0; i < $scope.AllTours.Tours.length; i++) {

            var currentTour = $scope.AllTours.Tours[i];

            if ($scope.MC.stateParams.tourId == currentTour.YouTubeId) {
                $scope.Model = {
                    YouTubeId: currentTour.YouTubeId,
                    Title: currentTour.Title,
                    Cover: "/Images/Dashboard/Tours/cover-create-class.jpg"
                };
            }
        }

        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        }

        $scope.generateYouTubeUrl = function (id) {
            return $scope.trustSrc("//youtube.com/embed/" + id + "?autohide=1&color=white&rel=0&theme=dark");
        };

    }]);
})();