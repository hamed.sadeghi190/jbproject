﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CreditCardsListViewModel
    {

        public string User { get; set; }
        public bool HasError { get; set; }
        public bool HasNoCreditCardError { get; set; }
        public int userCardsCount {get;set;}
        public string ClubDomain { get; set; }
        public string CustomerId { get; set; }
        public string LastDigits { get; set; }
        public string Expiry { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public bool IsDefault { get; set; }
        public string TypeCreditcard { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public string DefaultDate { get; set; }
        public string LastOrderMode { get; set; }
        public string CustomerId2 { get; set; }
        public string LastDigits2 { get; set; }
        public string Expiry2 { get; set; }
        public string Name2 { get; set; }
        public string Brand2 { get; set; }
        public bool IsDefault2 { get; set; }
        public string TypeCreditcard2 { get; set; }
        public string CreateDate2 { get; set; }
        public string UpdateDate2 { get; set; }
        public string DefaultDate2 { get; set; }

        public string CustomerId3 { get; set; }
        public string LastDigits3 { get; set; }
        public string Expiry3 { get; set; }
        public string Name3 { get; set; }
        public string Brand3 { get; set; }
        public bool IsDefault3 { get; set; }
        public string TypeCreditcard3 { get; set; }
        public string CreateDate3 { get; set; }
        public string UpdateDate3 { get; set; }
        public string DefaultDate3 { get; set; }
        public DateTime LastOrderDate { get; set; }
    }

 
}