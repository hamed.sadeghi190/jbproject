﻿using System.ComponentModel.DataAnnotations;

namespace Jumbula.Core.Model.Club
{
    public class ClubMembers
    {
        public int Id { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Name { get; set; }
    }
}
