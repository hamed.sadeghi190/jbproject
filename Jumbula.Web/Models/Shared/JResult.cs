﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Data;

namespace SportsClub.Models
{
    public enum JComplateStatus
    {
        Complate,
        Success,
        InnderException,
    }
    public class JResult
    {
        public JResult()
        {

        }

        public JResult(OperationStatus operationStatus)
        {
            if(operationStatus.Status)
            {
                this.Status = true;
                this.RecordsAffected = operationStatus.RecordsAffected;
            }
            else
            {
                if(operationStatus.RecordsAffected == 0 && operationStatus.Exception == null)
                {
                    Status = true;
                }
            }
        }

        public JResult(OperationStatus operationStatus, string data)
            :this(operationStatus)
        {
            Data = data;
        }

        public bool Status { get; set; }

        public string Message { get; set; }

        public string Data { get; set; }

        public int RecordsAffected { get; set; }

        public IEnumerable<FormValidationError> FormErrors { get; set; }
    }

    public class FormValidationError
    {
        public string Key { get; set; }

        public List<string> Messages { get; set; }
    }
}