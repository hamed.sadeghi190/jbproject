
IF OBJECT_ID('[dbo].[OrderInstallments]') IS NULL
BEGIN
CREATE TABLE [dbo].[OrderInstallments](
       [Id] [int] IDENTITY(1,1) NOT NULL,
       [Token] [nvarchar](max) NOT NULL,
       [InstallmentDate] [datetime] NOT NULL,
       [PaidDate] [datetime] NULL,
       [Amount] [decimal](18, 2) NOT NULL,
       [PaidAmount] [decimal](18, 2) NULL,
       [TransactionId] [nvarchar](128) NULL,
       [TransactionMessage] [nvarchar](128) NULL,
       [CardSecurityCode] [int] NULL,
       [Currency] [nvarchar](32) NULL,
       [CardFirstName] [nvarchar](128) NULL,
       [CardLastName] [nvarchar](128) NULL,
       [CardEmail] [nvarchar](128) NULL,
       [CardNumber] [nvarchar](128) NULL,
       [CardExpireDate] [datetime] NULL,
       [CardType] [tinyint] NULL,
       [PaypalIPN] [nvarchar](max) NULL,
       [PaymentMethod] [int] NOT NULL,
       [PaymentStatus] [int] NOT NULL,
       [BillingAddress_Id] [int] NULL,
       [Order_Id] [int] NULL)
END
GO


--<< Primary Key >>--
IF NOT EXISTS(select 1 from sys.objects where name='PK_dbo.OrderInstallments')
BEGIN
ALTER TABLE [dbo].[OrderInstallments] ADD CONSTRAINT [PK_dbo.OrderInstallments] PRIMARY KEY CLUSTERED(id asc)
END

---<< ADD Foreign Keys >>--
IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.OrderInstallments_dbo.Orders_Order_Id')
BEGIN
	ALTER TABLE [dbo].[OrderInstallments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderInstallments_dbo.Orders_Order_Id] FOREIGN KEY([Order_Id])
	REFERENCES [dbo].[Orders] ([Id])
END
GO

IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.OrderInstallments_dbo.PostalAddresses_BillingAddress_Id')
BEGIN
	ALTER TABLE [dbo].[OrderInstallments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderInstallments_dbo.PostalAddresses_BillingAddress_Id] FOREIGN KEY([BillingAddress_Id])
	REFERENCES [dbo].[PostalAddresses] ([Id])
END
GO

-----<< ADD COLUMNS >>----
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[OrderInstallments]') AND NAME='NoticeSent')
BEGIN
	ALTER TABLE [dbo].[OrderInstallments] 
	ADD [NoticeSent] [bit] Null DEFAULT(0);
	
	DECLARE @InstallmentDate nvarchar(20)='2014-10-31';
	DECLARE @cmd nvarchar(100)='update [dbo].[OrderInstallments] set [NoticeSent]=1 where paymentstatus=3 or InstallmentDate<='+@InstallmentDate;
	EXEC(@cmd);

	SET @cmd='update [dbo].[OrderInstallments] set [NoticeSent]=0 where NoticeSent is null';
	EXEC(@cmd);
	
	ALTER TABLE [dbo].[OrderInstallments] 
	ALTER COLUMN [NoticeSent] [bit] NOT Null;
END