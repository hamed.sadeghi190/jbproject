﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('partiallyRefundController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cancelOrderService', 'jbToast', function ($scope, $http, $log, $stateParams, $state, cancelOrderService, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/Order";


        var returndeByBack = $state.returnByBack();
        $scope.Model = {};
        if (returndeByBack) {
            $scope.Model = cancelOrderService.get();
        }
        if (!returndeByBack) {

            $http.get(window.location.origin + '/Dashboard/Order/GetOrderItem/?orderItemId=' + $stateParams.orderItemId + '&installmentId=' + $stateParams.installmentId).success(function (data, status, headers, config) {
                $scope.Model = data;
                calculateRefundAmount();
            }).error(function (data, status, headers, config) {
                $scope.MC.handleErrors($scope, data);
            });
        }

        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId });
                }
            }
            else if ("goToFamilyOrder" in window) {
                if (window.goToFamilyOrder > 0) {
                    var userId = window.goToFamilyOrder;
                    window.goToFamilyOrder = -1;
                    $state.go('FamilyOrders', { userId: userId });
                }
            }
            else {
                $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId });
            }
        }

        $scope.confirmCancellation = function () {

            if ($scope.Model.HasInstallments) {

                $scope.RefundByInstallment($scope.Model);

            } else {

                $scope.RefundByOrderItem($scope.Model);
            }
        };

        $scope.RefundByOrderItem = function (model) {
            if ($scope.CheckValidations(model)) {
                $http.post($scope.baseUrl + '/ValidateRefund', { model: $scope.Model })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            cancelOrderService.set($scope.Model);
                            $state.forward('ConfirmPartiallyRefundOrderItem', { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, orderItemId: $stateParams.orderItemId }, 'PartiallyRefundOrderItem', { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, orderItemId: $stateParams.orderItemId });
                        }
                        else {

                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            }
        }

        $scope.RefundByInstallment = function (model) {

            if ($scope.CheckValidations(model)) {
                $http.post($scope.baseUrl + '/ValidateRefund', { model: model })
                    .success(function (data, status, headers, config) {

                        if (data.Status) {
                            cancelOrderService.set(model);
                            $state.forward('ConfirmPartiallyRefundOrderItem', { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, orderItemId: $stateParams.orderItemId }, 'PartiallyRefundOrderItem', { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, orderItemId: $stateParams.orderItemId });
                        }
                        else {

                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            }
        }

        $scope.orderItemTransactionsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: window.location.origin + '/Dashboard/Order/GetOrderItemTransactions/?orderItemId=' + $stateParams.orderItemId,
                        type: "POST",
                    },

                },
            schema: {
                total: "total",
                data: "data"
            },
            serverSorting: false,
            batch: true,
            resizable: false,
        });
        $scope.orderItemTransactionsGridOptions = {
            dataSource: $scope.orderItemTransactionsDataSource,
            sortable: true,
            resizable: false,
            columns: [
                {
                    field: "StrTransactionDate",
                    title: "Date",
                    width: "90px",
                },
                {
                    field: "StrAmount",
                    title: "Amount",
                    width: "80px",
                },
                {
                    field: "StrPaymentMethod",
                    title: "Method",
                    width: "100px",
                },
                {
                    field: "CardNumber",
                    title: "Card number",
                    width: "120px",
                },
                {
                    field: "TransactionId",
                    title: "Transaction ID",
                    width: "240px",
                    template: "<span title='#=TransactionId#' style='display: inline-block;max-width: 220px;min-width: 220px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;'>#=TransactionId#</span>"
                },
                {
                    field: "StrCategory",
                    title: "Category",
                    width: "120px",
                },
                {
                    field: "Payer",
                    title: "Agent",
                    width: "80px",

                },
                {
                    field: "StrTransactionStatus",
                    title: "Status",
                    width: "70px",
                },
            ],
        }

        if (returndeByBack) {
            $scope.MC.backQueueItems.pop();
        }

        $scope.CheckValidations = function (model) {

            var isMoreThanMaxAmount = $scope.CheckIsMoreThanMaxAmount($scope.Model);
            var isSelectedAnyInstallment = model.Installments.length > 0 ? $scope.CheckIsSelectedAnyInstallment($scope.Model) : true;

            var SelectedRefundModes = $scope.Model.RefundModes.filter(function (x) {
                return x.IsSelected == true;
            });

            if (SelectedRefundModes.length == 0) {
                var message = 'Please select a refund option.';
                jbToast.error(message);
                return false;
            }

            if (isSelectedAnyInstallment) {

                if (!isMoreThanMaxAmount) {

                    var message = 'The total sum of refunds is: ' + $scope.SumRefundAmount.formatCurrencyWithPenny(2) + ' but the max refund is: ' + $scope.MaxRefund.formatCurrencyWithPenny(2);
                    jbToast.error(message);
                    return false;
                }

            } else {

                var message = 'Please select one of the installments';
                jbToast.error(message);
                return false;
            }

            return true;
        }

        $scope.CheckIsMoreThanMaxAmount = function (model) {

            $scope.SumRefundAmount = 0;
            $scope.MaxRefund = 0;

            for (var i = 0; i < model.RefundModes.length; i++) {

                if (model.RefundModes[i].IsSelected) {
                    $scope.SumRefundAmount += parseFloat(model.RefundModes[i].Amount);
                }

                if (model.RefundModes[i].Mode == 'Manually') {
                    $scope.MaxRefund = parseFloat(model.RefundModes[i].MaxAmount);
                }
            }

            if (($scope.SumRefundAmount > $scope.MaxRefund) || $scope.SumRefundAmount == 0) {
                return false;
            }

            return true;
        }

        $scope.CheckIsSelectedAnyInstallment = function (model) {

            $scope.IsSelectedAnyInstallment = false;

            for (var i = 0; i < model.Installments.length; i++) {

                if (model.Installments[i].IsSelected) {
                    $scope.IsSelectedAnyInstallment = true;
                }
            }

            if (model.HasAddOnInstallments) {

                for (var i = 0; i < model.AddOnInstallments.length; i++) {

                    if (model.AddOnInstallments[i].IsSelected) {
                        $scope.IsSelectedAnyInstallment = true;
                    }
                }
            }


            return $scope.IsSelectedAnyInstallment;
        }

        $scope.ShowTransactions = false;
        $scope.AmountAutomatically = 0;

        var calculateRefundAmount = function () {

            var elements = document.getElementsByName("AmountSelected");

            for (var i = 0; i < elements.length; i++) {
                elements[i].disabled = false;
            }

            $scope.balanceOrderItem = -1 * $scope.Model.OrderInfo.Balance;

            var SelectedInstallment = $scope.Model.Installments.filter(function (x) {
                return x.IsSelected == true;
            });

            if (SelectedInstallment.length == 0 && $scope.Model.PaymentPlanType == "Installment") {
                console.log($scope.Model.PaymentPlanType)
                for (var i = 0; i < $scope.Model.RefundModes.length; i++) {

                    $scope.Model.RefundModes[i].MaxAmount = 0;
                }
            }

            if ($scope.Model.AmountAutomaticallyTransactions > $scope.balanceOrderItem && ((SelectedInstallment.length > 0 && $scope.Model.PaymentPlanType == "Installment") || $scope.Model.PaymentPlanType == "FullPaid")) { // if more paid was automatically

                for (var i = 0; i < $scope.Model.RefundModes.length; i++) {

                    $scope.Model.RefundModes[i].MaxAmount = $scope.balanceOrderItem;
                }

                $scope.AmountManually = '';
                $scope.AmountAutomatically = 0;

            } else if ((SelectedInstallment.length > 0 && $scope.Model.PaymentPlanType == "Installment") || $scope.Model.PaymentPlanType == "FullPaid" ) {
                for (var i = 0; i < $scope.Model.RefundModes.length; i++) {

                    if ($scope.Model.RefundModes[i].Mode == "Automatically") {
                        $scope.Model.RefundModes[i].MaxAmount = $scope.Model.AmountAutomaticallyTransactions;
                        $scope.AmountAutomatically = $scope.Model.RefundModes[i].Amount;
                    } else if ($scope.Model.RefundModes[i].Mode == "Manually" || $scope.Model.RefundModes[i].Mode == "AddToCredit") {
                        $scope.Model.RefundModes[i].MaxAmount = $scope.balanceOrderItem - $scope.AmountAutomatically;
                    }
                }
            }

        }

        $scope.calculateBalance = function (installment) {

            var elements = document.getElementsByName("AmountSelected");

            for (var i = 0; i < elements.length; i++) {
                elements[i].disabled = false;
            }

            if (installment.IsSelected) {

                $scope.ShowTransactions = true;
                $scope.balanceInstallment = installment.RefundAmount;

                if (installment.AmountAutomaticallyTransactions > $scope.balanceInstallment) { // if more paid was automatically

                    for (var i = 0; i < $scope.Model.RefundModes.length; i++) {

                        $scope.Model.RefundModes[i].MaxAmount = $scope.balanceInstallment;
                    }

                    $scope.AmountManually = '';
                    $scope.AmountAutomatically = 0;
                } else {
                    for (var i = 0; i < $scope.Model.RefundModes.length; i++) {

                        if ($scope.Model.RefundModes[i].Mode == "Automatically") {
                            $scope.Model.RefundModes[i].MaxAmount = installment.AmountAutomaticallyTransactions;
                            $scope.AmountAutomatically = $scope.Model.RefundModes[i].Amount;
                        } else if ($scope.Model.RefundModes[i].Mode == "Manually" || $scope.Model.RefundModes[i].Mode == "AddToCredit") {
                            $scope.Model.RefundModes[i].MaxAmount = $scope.balanceInstallment - $scope.AmountAutomatically;
                        }
                    }
                }

                GetInstallmentTransaction(installment);

            } else {

                $("#installmentTransactions").data('kendoGrid').dataSource.data([]);
                $("#installmentTransactions").data("kendoGrid").dataSource.read();
            }

            for (var i = 0; i < $scope.Model.Installments.length; i++) {

                if (installment.Id != $scope.Model.Installments[i].Id) {

                    $scope.Model.Installments[i].IsSelected = false;
                }
                else {
                    installment.IsSelected = true;
                }
            }

            for (var i = 0; i < $scope.Model.AddOnInstallments.length; i++) {

                if (installment.Id != $scope.Model.AddOnInstallments[i].Id) {

                    $scope.Model.AddOnInstallments[i].IsSelected = false;
                }
                else {
                    installment.IsSelected = true;
                }
            }

        }

        var isInitGrid = false;
        var GetInstallmentTransaction = function (installment) {

            $scope.installmentId = installment.Id;

            if (!isInitGrid) {

                isInitGrid = true;

                $scope.installmentTransactionsDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport:
                        {
                            read: {
                                url: window.location.origin + '/Dashboard/Order/GetOrderItemTransactions',
                                type: "POST",
                                data: function (data) {
                                    return {
                                        orderItemId: $stateParams.orderItemId,
                                        installmentId: $scope.installmentId,
                                    };
                                },
                            },

                        },
                    schema: {
                        total: "total",
                        data: "data"
                    },
                    serverSorting: false,
                    batch: true,
                    resizable: false,


                });

                $scope.installmentTransactionsGridOptions = {

                    dataSource: $scope.installmentTransactionsDataSource,
                    sortable: true,
                    resizable: false,
                    columns: [
                        {
                            field: "StrTransactionDate",
                            title: "Date",
                        },
                        {
                            field: "StrAmount",
                            title: "Amount",
                        },
                        {
                            field: "StrPaymentMethod",
                            title: "Method",
                        },
                        {
                            field: "CardNumber",
                            title: "Card number",
                            width: "160px",
                        },
                        {
                            field: "TransactionId",
                            title: "Transaction ID",
                            width: "210px",
                            template: "<span title='#=TransactionId#' style='display: inline-block;max-width: 200px;min-width: 200px;text-overflow: ellipsis;overflow: hidden;white-space: nowrap;'>#=TransactionId#</span>"
                        },
                        {
                            field: "StrCategory",
                            title: "Category",

                        },
                        {
                            field: "Payer",
                            title: "Agent",

                        },
                        {
                            field: "StrTransactionStatus",
                            title: "Status",
                        },
                    ],
                }
            }
            else {

                $("#installmentTransactions").data('kendoGrid').dataSource.data([]);
                $("#installmentTransactions").data("kendoGrid").dataSource.read();
            }
        }

    }]);
})();