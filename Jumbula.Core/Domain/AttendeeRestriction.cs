﻿using System.ComponentModel;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class AttendeeRestriction
    {
        public AttendeeRestriction()
        {
            Gender = Genders.NoRestriction;
        }

        [DisplayName("Minimum")]
        //[Range(1, Constants.M_MaxAge, ErrorMessage = "{0} should be a number between {1} and {2}")]
        public int? MinAge { get; set; }

        [DisplayName("Maximum")]
        [DefaultValue(120)]
        //[Range(1, Constants.M_MaxAge, ErrorMessage = "{0} should be a number between {1} and {2}")]
        public int? MaxAge { get; set; }

        [DisplayName("Minimum")]
        public SchoolGradeType? MinGrade { get; set; }

        [DisplayName("Maximum")]
        public SchoolGradeType? MaxGrade { get; set; }

        public Genders? Gender { get; set; }

        public RestrictionType RestrictionType { get; set; }

        public bool ApplyAtProgramStart { get; set; }
    }
}
