﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Common.Enums
{
    public enum ContactSearchType : byte
    {
        Name,
        Email,
        Phone
    }
}
