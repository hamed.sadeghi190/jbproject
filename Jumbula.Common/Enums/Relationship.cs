﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum Relationship
    {
        [Description("Mother")]
        [Display(Name = "Mother")]
        Mother = 10,

        [Description("Father")]
        [Display(Name = "Father")]
        Father = 20,

        [Description("Guardian")]
        [Display(Name = "Guardian")]
        Gaurdian = 30,

        [Description("Grandmother")]
        [Display(Name = "Grandmother")]
        Grandmother = 40,

        [Description("Grandfather")]
        [Display(Name = "Grandfather")]
        Grandfather = 50,

        [Description("Brother")]
        [Display(Name = "Brother")]
        Brother = 60,

        [Description("Sister")]
        [Display(Name = "Sister")]
        Sister = 70,

        [Description("Nanny/au pair")]
        [Display(Name = "Nanny/au pair")]
        Nannyaupair = 80,

        [Description("Friend/neighbor")]
        [Display(Name = "Friend/neighbor")]
        FriendNeighbor = 90,

        [Description("Other")]
        [Display(Name = "Other")]
        Other = 200,
    }
}
