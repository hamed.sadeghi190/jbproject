﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class HolidaysDateViewModel
    {
        public HolidaysDateViewModel()
        {
            AllHolidayDates = new List<SelectKeyValue<string>>();
            HolidayDates = new List<string>();

            AllHolidayAMDates= new List<SelectKeyValue<string>>();
            HolidayAMDates = new List<string>();

            AllHolidayPMDates = new List<SelectKeyValue<string>>();
            HolidayPMDates = new List<string>();
        }
        public List<SelectKeyValue<string>> AllHolidayDates { get; set; }
        public List<string> HolidayDates { get; set; }
        public List<SelectKeyValue<string>> AllHolidayAMDates { get; set; }
        public List<string> HolidayAMDates { get; set; }
        public List<SelectKeyValue<string>> AllHolidayPMDates { get; set; }
        public List<string> HolidayPMDates { get; set; }
    }

}
