﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IClientCreditCardBusiness
    {
        ClientCreditCard Get(long cardId);
        OperationStatus Update(ClientCreditCard client);
      
        OperationStatus Delete(long cardId);
        OperationStatus Create(ClientCreditCard client);
        IQueryable<ClientCreditCard> GetClientCards(long clientId);
    }

    
}
