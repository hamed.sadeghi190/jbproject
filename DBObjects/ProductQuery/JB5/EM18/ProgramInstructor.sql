

ALTER TABLE [dbo].[ClubStaffs]  add [Id] [int] IDENTITY(1,1) NOT NULL
GO

ALTER TABLE [dbo].[ClubStaffs] ADD PRIMARY KEY ([Id])
GO


SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProgramInstructors](
	[ProgramId] [bigint] NOT NULL,
	[ClubStaffId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ProgramInstructors] PRIMARY KEY CLUSTERED 
(
	[ProgramId] ASC,
	[ClubStaffId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProgramInstructors]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramInstructors_dbo.ClubStaffs_ClubStaffId] FOREIGN KEY([ClubStaffId])
REFERENCES [dbo].[ClubStaffs] ([Id])
GO

ALTER TABLE [dbo].[ProgramInstructors] CHECK CONSTRAINT [FK_dbo.ProgramInstructors_dbo.ClubStaffs_ClubStaffId]
GO

ALTER TABLE [dbo].[ProgramInstructors]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramInstructors_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
GO

ALTER TABLE [dbo].[ProgramInstructors] CHECK CONSTRAINT [FK_dbo.ProgramInstructors_dbo.Programs_ProgramId]
GO





