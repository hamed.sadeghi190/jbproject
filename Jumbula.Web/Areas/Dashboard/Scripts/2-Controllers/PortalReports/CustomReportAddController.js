﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CustomReportAddController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = {};

        $http.get(baseUrl + '/CreateEditCustomReport', { params: { id: null, seasonDomain: $scope.MC.stateParams.seasonDomain, customType: $scope.MC.stateParams.customType } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
            });

        $scope.saveReport = function () {
            $http.post(baseUrl + '/CreateEditCustomReport', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        jbToast.success("Report saved successfully.");
                        $state.go('CustomReportPortal');
                    }
                    else {
                        if (is.not.undefined(data.FormErrors[0]) && data.FormErrors[0].Key == "model.NoSelection") {
                            jbToast.error($scope.MC.joinStringsWithBreak(data.FormErrors[0].Messages));
                        }
                        else if (is.not.undefined(data.FormErrors[1]) && data.FormErrors[1].Key == "model.NoSelection") {
                            jbToast.error($scope.MC.joinStringsWithBreak(data.FormErrors[1].Messages));
                        }
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }

        $scope.checkAll = function (sectionCheckAllModel, sectionCheckModels) {
            angular.forEach(sectionCheckModels, function (item) {
                if (item.Name[0] != '%') {
                    item.Value = sectionCheckAllModel;
                }
            });
        };

    }]);
})();