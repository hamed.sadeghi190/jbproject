﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('providerReportRunController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();


        if ($stateParams.reportName == 'RegistrationForm' || $stateParams.reportName == 'FollowupForm') {
            $scope.MC.openBlade({ url: "Dashboard/Blade/RegistrationDataReports", order: 3, menu: "SEASONS" });
        }
        else if ($stateParams.reportName == 'Balance' || $stateParams.reportName == 'Finance' || $stateParams.reportName == 'ChargeDiscount' || $stateParams.reportName == 'Installment' || $stateParams.reportName == 'Coupon' || $stateParams.reportName == 'Provider Financial' || $stateParams.reportName == 'Coordinator Financial') {
            $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReport", order: 3, menu: "SEASONS" });
        }

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var reportName = $scope.MC.stateParams.reportName;

        $http.get(baseUrl + '/GetAllProviders', { params: { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: reportName } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;

            });

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };


        switch ($scope.MC.stateParams.reportType) {
            case "InstructorListReport":
                {
                    $scope.reportName = "Instructor Report";
                    break;
                }
            case "CatalogDetailedActivities":
                {
                    $scope.reportName = "Catalog detailed activities";
                    break;
                }
        }


        $scope.runReport = function () {
            $scope.Model.ReportId = $scope.MC.stateParams.reportId;
            $scope.Model.SeasonDomain = $scope.MC.stateParams.seasonDomain;
            $scope.providers = $scope.Model._SelectedProvides;
            $scope.Model.ReportName = $scope.MC.stateParams.reportName;
            $scope.isAllProviders = $scope.Model.IsAllProviders;

            customReportService.setModelInStorage($scope.Model);

            switch ($scope.MC.stateParams.reportType) {

                case "InstructorListReport":
                    $state.go('InstructorListPortalReports', { Model: $scope.Model });
                    break;

                case "CatalogDetailedActivities":
                    $state.go('CatalogDetailedActivities', { Model: $scope.Model });
                    break;
            }



        }

    }]);
})();

