﻿using System.Web.Mvc;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;

namespace Jumbula.Web.Infrastructure
{
    public static class Ioc
    {
        public static IOrderBusiness OrderBusiness =>
            (IOrderBusiness) DependencyResolver.Current.GetService(typeof(IOrderBusiness));

        public static IStripeWebhookBusiness StripeWebhookBusiness =>
            (IStripeWebhookBusiness) DependencyResolver.Current.GetService(typeof(IStripeWebhookBusiness));


        public static IClientCreditCardBusiness ClientCreditCardBusiness =>
            (IClientCreditCardBusiness) DependencyResolver.Current.GetService(typeof(IClientCreditCardBusiness));

        public static IClientPaymentMethodBusiness ClientPaymentMethodBusiness =>
            (IClientPaymentMethodBusiness) DependencyResolver.Current.GetService(typeof(IClientPaymentMethodBusiness));

        public static ICartBusiness CartBusiness =>
            (ICartBusiness) DependencyResolver.Current.GetService(typeof(ICartBusiness));

        public static IUserCreditCardBusiness UserCreditCardBusiness =>
            (IUserCreditCardBusiness) DependencyResolver.Current.GetService(typeof(IUserCreditCardBusiness));

        public static IJbFileBusiness JbFileBusiness =>
            (IJbFileBusiness) DependencyResolver.Current.GetService(typeof(IJbFileBusiness));

        public static ISeasonFileBusiness SeasonFileBusiness =>
            (ISeasonFileBusiness) DependencyResolver.Current.GetService(typeof(ISeasonFileBusiness));

        public static IPlayerProfileBusiness PlayerProfileBusiness =>
            (IPlayerProfileBusiness) DependencyResolver.Current.GetService(typeof(IPlayerProfileBusiness));

        public static IAttributeBusiness AttributeBusiness =>
            (IAttributeBusiness) DependencyResolver.Current.GetService(typeof(IAttributeBusiness));

        public static IPricePlanBusiness PricePlanBusiness =>
            (IPricePlanBusiness) DependencyResolver.Current.GetService(typeof(IPricePlanBusiness));

        public static IOrderItemBusiness OrderItemBusiness =>
            (IOrderItemBusiness) DependencyResolver.Current.GetService(typeof(IOrderItemBusiness));

        public static IOrderItemWaiverBusiness OrderItemWaiverBusiness =>
            (IOrderItemWaiverBusiness) DependencyResolver.Current.GetService(typeof(IOrderItemWaiverBusiness));

        public static ITransactionActivityBusiness TransactionActivityBusiness =>
            (ITransactionActivityBusiness) DependencyResolver.Current.GetService(typeof(ITransactionActivityBusiness));

        public static IPaymentDetailBusiness PaymentDetailBusiness =>
            (IPaymentDetailBusiness) DependencyResolver.Current.GetService(typeof(IPaymentDetailBusiness));

        public static IInvoiceBusiness InvoiceBusiness =>
            (IInvoiceBusiness) DependencyResolver.Current.GetService(typeof(IInvoiceBusiness));

        public static IInvoiceHistoryBusiness InvoiceHistoryBusiness =>
            (IInvoiceHistoryBusiness) DependencyResolver.Current.GetService(typeof(IInvoiceHistoryBusiness));

        public static IJbESignatureBusiness JbESignatureBusiness =>
            (IJbESignatureBusiness) DependencyResolver.Current.GetService(typeof(IJbESignatureBusiness));

        public static IJbFlyerBusiness JbFlyerBusiness =>
            (IJbFlyerBusiness) DependencyResolver.Current.GetService(typeof(IJbFlyerBusiness));

        public static IClubFlyerBusiness ClubFlyerBusiness =>
            (IClubFlyerBusiness) DependencyResolver.Current.GetService(typeof(IClubFlyerBusiness));

        public static IDiscountEngineRuleBusiness discountEngine =>
            (IDiscountEngineRuleBusiness) DependencyResolver.Current.GetService(typeof(IDiscountEngineRuleBusiness));

        public static ICouponBusiness CouponBusiness =>
            (ICouponBusiness) DependencyResolver.Current.GetService(typeof(ICouponBusiness));

        public static IDiscountBusiness DiscountBusiness =>
            (IDiscountBusiness) DependencyResolver.Current.GetService(typeof(IDiscountBusiness));

        public static IPaypalIPNBusiness PaypalIPNBusiness =>
            (IPaypalIPNBusiness) DependencyResolver.Current.GetService(typeof(IPaypalIPNBusiness));

        public static IPaypalTrackingBusiness PaypalTrackingBusiness =>
            (IPaypalTrackingBusiness) DependencyResolver.Current.GetService(typeof(IPaypalTrackingBusiness));

        public static IPaymentPlanBusiness PaymentPlanBusiness =>
            (IPaymentPlanBusiness) DependencyResolver.Current.GetService(typeof(IPaymentPlanBusiness));

        public static IReportBusiness ReportBusiness =>
            (IReportBusiness) DependencyResolver.Current.GetService(typeof(IReportBusiness));

        public static INewReportBusiness NewReportBusiness =>
            (INewReportBusiness)DependencyResolver.Current.GetService(typeof(INewReportBusiness));

        public static dynamic IChargeDiscountService => null;

        public static IJbFormBusiness JbFormBusiness =>
            (IJbFormBusiness) DependencyResolver.Current.GetService(typeof(IJbFormBusiness));


        public static IOrderHistoryBusiness OrderHistoryBusiness =>
            (IOrderHistoryBusiness) DependencyResolver.Current.GetService(typeof(IOrderHistoryBusiness));

        public static IOrderPreapprovalBusiness OrderPreapprovalBusiness =>
            (IOrderPreapprovalBusiness) DependencyResolver.Current.GetService(typeof(IOrderPreapprovalBusiness));

        public static ISeasonBusiness SeasonBusiness =>
            (ISeasonBusiness) DependencyResolver.Current.GetService(typeof(ISeasonBusiness));

        public static IProgramBusiness ProgramBusiness =>
            (IProgramBusiness) DependencyResolver.Current.GetService(typeof(IProgramBusiness));

        public static IClubBusiness ClubBusiness =>
            (IClubBusiness) DependencyResolver.Current.GetService(typeof(IClubBusiness));

        public static IUserProfileBusiness UserProfileBusiness =>
            (IUserProfileBusiness) DependencyResolver.Current.GetService(typeof(IUserProfileBusiness));

        public static ICampaignBusiness CampaignBusiness =>
            (ICampaignBusiness) DependencyResolver.Current.GetService(typeof(ICampaignBusiness));

        public static IEmailCampaignTemplateBusiness EmailCampaignTemplate =>
            (IEmailCampaignTemplateBusiness) DependencyResolver.Current.GetService(
                typeof(IEmailCampaignTemplateBusiness));

        public static IMailListBusiness MailListBusiness =>
            (IMailListBusiness) DependencyResolver.Current.GetService(typeof(IMailListBusiness));

        public static ICategoryBusiness CategoryBuisness =>
            (ICategoryBusiness) DependencyResolver.Current.GetService(typeof(ICategoryBusiness));

        public static IOrderSessionBusiness OrderSessionBusiness =>
            (IOrderSessionBusiness) DependencyResolver.Current.GetService(typeof(IOrderSessionBusiness));

        public static IProgramSessionBusiness ProgramSessionBusiness =>
            (IProgramSessionBusiness) DependencyResolver.Current.GetService(typeof(IProgramSessionBusiness));

        public static IOldEmailBusiness OldEmailBusiness =>
            (IOldEmailBusiness) DependencyResolver.Current.GetService(typeof(IOldEmailBusiness));

        public static ICustomReportBusiness CustomReportBusiness =>
            (ICustomReportBusiness) DependencyResolver.Current.GetService(typeof(ICustomReportBusiness));

        public static IOrderInstallmentBusiness InstallmentBusiness =>
            (IOrderInstallmentBusiness) DependencyResolver.Current.GetService(typeof(IOrderInstallmentBusiness));

        public static IAutoChargeBusiness AutoChargeBusiness =>
            (IAutoChargeBusiness)DependencyResolver.Current.GetService(typeof(IAutoChargeBusiness));

        public static IClientBusiness ClientBusiness =>
            (IClientBusiness) DependencyResolver.Current.GetService(typeof(IClientBusiness));

        public static IFollowupFormBusiness FollowupBusiness =>
            (IFollowupFormBusiness) DependencyResolver.Current.GetService(typeof(IFollowupFormBusiness));

        public static IFormBusiness FormBusiness =>
            (IFormBusiness) DependencyResolver.Current.GetService(typeof(IFormBusiness));

        public static IPendingPreapprovalTransactionBusiness PendingPreapprovalTransactionBusiness =>
            (IPendingPreapprovalTransactionBusiness) DependencyResolver.Current.GetService(
                typeof(IPendingPreapprovalTransactionBusiness));

        public static IPageBusiness PageBusiness =>
            (IPageBusiness) DependencyResolver.Current.GetService(typeof(IPageBusiness));

        public static ICatalogBusiness CatalogBusiness =>
            (ICatalogBusiness) DependencyResolver.Current.GetService(typeof(ICatalogBusiness));

        public static ICatalogSettingBusiness CatalogSettingBusiness =>
            (ICatalogSettingBusiness) DependencyResolver.Current.GetService(typeof(ICatalogSettingBusiness));

        public static ICountryBusiness CountryBusiness =>
            (ICountryBusiness) DependencyResolver.Current.GetService(typeof(ICountryBusiness));

        public static IImageGalleryBusiness ImageGalleryBusiness =>
            (IImageGalleryBusiness) DependencyResolver.Current.GetService(typeof(IImageGalleryBusiness));

        public static IImageGalleryItemBusiness ImageGalleryItemBusiness =>
            (IImageGalleryItemBusiness) DependencyResolver.Current.GetService(typeof(IImageGalleryItemBusiness));

        public static ISecurityBusiness SecurityBusiness =>
            (ISecurityBusiness) DependencyResolver.Current.GetService(typeof(ISecurityBusiness));

        public static IVerificationBusiness VerificationBusiness =>
            (IVerificationBusiness) DependencyResolver.Current.GetService(typeof(IVerificationBusiness));

        public static IUserEventBusiness UserEventBusiness =>
            (IUserEventBusiness) DependencyResolver.Current.GetService(typeof(IUserEventBusiness));

        public static IStudentAttendanceBusiness StudentAttendanceBusiness =>
            (IStudentAttendanceBusiness) DependencyResolver.Current.GetService(typeof(IStudentAttendanceBusiness));

        public static IWaitListBusiness WaitListBusiness =>
            (IWaitListBusiness) DependencyResolver.Current.GetService(typeof(IWaitListBusiness));

        public static ISubscriptionBusiness SubscriptionBusiness =>
            (ISubscriptionBusiness) DependencyResolver.Current.GetService(typeof(ISubscriptionBusiness));

        public static IMessageBusiness MessageBusiness =>
            (IMessageBusiness) DependencyResolver.Current.GetService(typeof(IMessageBusiness));

        public static IPlayerProgramNoteBusiness PlayerProgramNoteBusiness =>
            (IPlayerProgramNoteBusiness) DependencyResolver.Current.GetService(typeof(IPlayerProgramNoteBusiness));

        public static ICampaignCallbackBusiness CampaignCallbackBusiness =>
            (ICampaignCallbackBusiness) DependencyResolver.Current.GetService(typeof(ICampaignCallbackBusiness));

        public static IScheduleTaskBusiness ScheduleTaskBusiness =>
            (IScheduleTaskBusiness) DependencyResolver.Current.GetService(typeof(IScheduleTaskBusiness));

        public static IContactPersonBusiness ContactPersonBusiness =>
            (IContactPersonBusiness) DependencyResolver.Current.GetService(typeof(IContactPersonBusiness));

        public static IFieldSettingBusiness FieldSettingBusiness =>
            (IFieldSettingBusiness) DependencyResolver.Current.GetService(typeof(IFieldSettingBusiness));

        public static ISubsidyBusiness SubsidyBusiness =>
            (ISubsidyBusiness) DependencyResolver.Current.GetService(typeof(ISubsidyBusiness));

        public static IFamilyBusiness FamilyBusiness =>
            (IFamilyBusiness) DependencyResolver.Current.GetService(typeof(IFamilyBusiness));

        public static IFamilyContactBusiness FamilyContactBusiness =>
            (IFamilyContactBusiness) DependencyResolver.Current.GetService(typeof(IFamilyContactBusiness));

        public static IChargeBusiness ChargeBusiness =>
            (IChargeBusiness) DependencyResolver.Current.GetService(typeof(IChargeBusiness));

        public static ISeasonPaymentPlanBusiness SeasonPaymentPlanBusiness =>
            (ISeasonPaymentPlanBusiness) DependencyResolver.Current.GetService(typeof(ISeasonPaymentPlanBusiness));

        public static IAuditBusiness AuditBusiness =>
            (IAuditBusiness) DependencyResolver.Current.GetService(typeof(IAuditBusiness));

        public static IApplicationUserManager<JbUser, int> ApplicationUserManager =>
            (IApplicationUserManager<JbUser, int>)DependencyResolver.Current.GetService(typeof(IApplicationUserManager<JbUser, int>));

        public static IApplicationSignInManager ApplicationSignInManager =>
            (IApplicationSignInManager)DependencyResolver.Current.GetService(typeof(IApplicationSignInManager));

        public static IAuthenticationAdditionalHelpers AuthenticationAdditionalHelpers =>
            (IAuthenticationAdditionalHelpers)DependencyResolver.Current.GetService(typeof(IAuthenticationAdditionalHelpers));

        public static IAccountingBusiness AccountingBusiness =>
            (IAccountingBusiness)DependencyResolver.Current.GetService(typeof(IAccountingBusiness));

        //todo: after create notification table we should be remove this line
        public static IPeopleBusiness PeopleBusiness =>
            (IPeopleBusiness)DependencyResolver.Current.GetService(typeof(IPeopleBusiness));

        public static IPaymentBusiness PaymentBusiness =>
            (IPaymentBusiness)DependencyResolver.Current.GetService(typeof(IPaymentBusiness));

        public static IEmailBusiness EmailBusiness =>
            (IEmailBusiness)DependencyResolver.Current.GetService(typeof(IEmailBusiness));

        public static IEmailRepository EmailRespository =>
            (IEmailRepository)DependencyResolver.Current.GetService(typeof(IEmailRepository));
    }
}