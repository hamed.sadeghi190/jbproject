
alter TABLE [dbo].[Orders] add [IsLive] [bit]  NULL 

Go

update orders set islive=1

go

alter TABLE [dbo].[Orders] alter column [IsLive] [bit] Not NULL 

go

ALTER TABLE [dbo].[Orders] ADD  DEFAULT ((0)) FOR [IsLive]
