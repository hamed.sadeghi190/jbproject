﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('addToWaitlistController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {
    
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.init = function () {
            app.globalObjects.initializeView();
        }

        $scope.Model = {};
      
        $http.get('/Dashboard/Program/GetAddToWaitlist?programId=' + $stateParams.programId +'&userId=').success(function (data, status, headers, config) {
            
            $scope.Model = data;
            if (data.Message) {
                jbToast.warning(data.Message);
            }
            
        });
    
        $scope.add = function () {

            $http.post('Dashboard/Program/SaveToWaitlist', { model: $scope.Model, programId: $stateParams.programId, userId: $stateParams.userId })
                .success(function (data, status, headers, config) {

                    if (data.Status) {

                        $scope.errors = null;
                        jbToast.success("The participant has been added to the waitlist.", "");

                        $state.go("ProgramWaitlist", { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId});
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }
    }]);
})();