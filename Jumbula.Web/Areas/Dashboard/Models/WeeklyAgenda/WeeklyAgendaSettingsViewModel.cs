﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Utilities;

namespace SportsClub.Areas.Dashboard.Models
{
    public class WeeklyAgendaSettingsViewModel
    {
        public bool IsEnabled { get; set; }
        public bool IsExactDay { get; set; }

        [Display(Name = "Date time")]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? ExactDateTime { get; set; }

        public int HourAfterClassEnds { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name="Subject")]
        public string Subject { get; set; }

        public string Wrapper { get; set; }
    }
}