﻿using System.Xml.Linq;
using SportsClub.Domain;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using SportsClub.Infrastructure;
using System.ComponentModel;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using SportsClub.Infrastructure.Validation;
using System;

namespace SportsClub.Models
{

    public class ShoppingCartViewModel
    {
        public int Count { get; set; }
        public decimal Total { get; set; }
    }

    public class DisplayCartViewModel
    {
        public Cart.Cart Cart { get; set; }

        public string TotalWithoutConvenienceFee
        {
            get
            {
                decimal convfee = 0;
                
                // check if convenience fee hase value show real amount on button
                foreach (var order in Cart.Orders)
                {
                    var convenince=order.Charges.FirstOrDefault(c=>c.Category==ChargeDiscountCategories.Convenience);
                    if(convenince!=null){
                        convfee += order.PaymentPlanMetaData.DueDates.Any() ? (convenince.Amount / order.PaymentPlanMetaData.DueDates.Count()) : convenince.Amount;
                    }
                }

                 return convfee != 0 ? " (" + Utilities.FormatCurrencyWithPenny(Cart.Total - convfee) + ")" : string.Empty;
            }
        }

        public string CouponMessage { get; set; }

        public bool EnablePayByCash
        {
            get
            {
                var clubDomains = Cart.Orders.GroupBy(c => c.Domain).Select(m => m.Key).ToList();
                var clubConfigurations = DependencyResolverHelper.IClubDbService.GetClubsConfigurations(clubDomains);
                var clubConfigurationModel = new ClubConfigurationModel();
                List<bool> enablePayByCashes = clubConfigurations.Select(config => (clubConfigurationModel.DeserializeConfig(XDocument.Parse(config.Value), config.Name)).PayByCash).ToList();
                if (enablePayByCashes.Any(item => item == false))
                {
                    return false;
                }

                return true;
            }
        }

        public string ClubDomain
        {
            get
            {
                if (this.Cart != null && this.Cart.Orders.Any())
                {
                    return this.Cart.Orders.First().Domain;
                }

                return string.Empty;
            }
        }

        public DisplayCartViewModel()
        {
            Cart = new Cart.Cart();
        }
    }

    public class DirectPaymentViewModel
    {
        [DisplayName("Credit Card Type")]
        public CreditCartType CreditCardType { get; set; }

        [Required(ErrorMessage = "The card number is required.")]
        [DisplayName("Card Number")]
        [RegularExpression(@"^4[0-9]{12}(?:[0-9]{3})?$", ErrorMessage = "Card type and card number do not match.")]
        public string CardNumber { get; set; }

        [DisplayName("Expire Date")]
        public ExpireDate ExpireDate { get; set; }

        [Required(ErrorMessage = "Security Code is required.")]
        [DisplayName("Security Code")]
        [RegularExpression(@"[0-9]{3}", ErrorMessage = "The card security code must be 3 digits.")]
        public string CVV2 { get; set; }

        [DisplayName("Cardholder Name")]
        [Required(ErrorMessage = "The cardholder name is required")]
        public string FullName { get; set; }

        public string FirstName
        {
            // Split first part of full name as first name
            get
            {
                if (!string.IsNullOrEmpty(FullName))
                {
                    string[] words = FullName.Split(' ');
                    List<string> wordsResult = new List<string>();
                    string result = string.Empty;

                    if (words.Any())
                    {
                        for (int i = 0; i < words.Count() - 1; i++)
                        {
                            wordsResult.Add(words[i]);
                        }

                        result = string.Join(" ", wordsResult);
                    }

                    return result;
                }

                return string.Empty;
            }
        }

        public string LastName
        {
            // Split last part of full name as last name
            get
            {
                if (!string.IsNullOrEmpty(FullName))
                {
                    string[] words = FullName.Split(' ');

                    if (words.Count() > 1)
                    {
                        return FullName.Split(' ').Last();
                    }
                }

                return string.Empty;
            }
        }

        public string ClubDomain { get; set; }

        [DisplayName("Address")]
        public PostalAddressAutoCompleteViewModel AutoCompleteAddress { get; set; }

        [DisplayName("Address Line 1")]
        public string AddressLine1 { get; set; }

        [Required(ErrorMessage = "The address is required.")]
        [DisplayName("Address Line 2")]
        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "The city is required.")]
        [DisplayName("City")]
        public string City { get; set; }

        [Required(ErrorMessage = "The state is required.")]
        [DisplayName("State")]
        public string State { get; set; }

        [Required(ErrorMessage = "The zip code is required.")]
        [DisplayName("Zip Code")]
        public string Zip { get; set; }

        public string Email { get; set; }

        public decimal AMT { get; set; }
        public string CurrenyCode { get; set; }
        public string PaymentAction { get; set; }
        public string IPAddress { get; set; }

        public string CountryCode { get; set; }
        public List<string> Message { get; set; }

        public string ConfirmatinId { get; set; }

        public bool PayWithClubPaypal { get; set; }
        public string ClubPaypalApiUserName { get; set; }
        public string ClubPaypalApiPassword { get; set; }
        public string ClubPaypalApiSignature { get; set; }

        public DirectPaymentViewModel()
        {
            ExpireDate = new ExpireDate();
            AutoCompleteAddress = new PostalAddressAutoCompleteViewModel();
            Message = new List<string>();
        }
    }

    public class ExpireDate
    {
        [DisplayName("Month")]
        [Required(ErrorMessage = "The month is required.")]
        [Range(1, 12, ErrorMessage = "The month is required.")]
        public int Month { get; set; }

        [DisplayName("Year")]
        [Required(ErrorMessage = "The year is required.")]
        //[StringLength(4, ErrorMessage = "The {0} must be {1} characters.")]
        //[Range(1, 12, ErrorMessage = "The {0} must be between {1} to {2}")]
        [RegularExpression(@"\d{4}", ErrorMessage = "The year is required.")]
        public int Year { get; set; }

        public List<SelectListItem> MonthList { get; set; }
        public List<SelectListItem> YearList { get; set; }

        public ExpireDate()
        {
            MonthList = new List<SelectListItem>()
            {
                new SelectListItem{Text = string.Empty, Value = "0", Selected= true},
                new SelectListItem{Text = "01", Value = "1", },
                new SelectListItem{Text = "02", Value = "2", },
                new SelectListItem{Text = "03", Value = "3", },
                new SelectListItem{Text = "04", Value = "4", },
                new SelectListItem{Text = "05", Value = "5", },
                new SelectListItem{Text = "06", Value = "6", },
                new SelectListItem{Text = "07", Value = "7", },
                new SelectListItem{Text = "08", Value = "8", },
                new SelectListItem{Text = "09", Value = "9", },
                new SelectListItem{Text = "10", Value = "10", },
                new SelectListItem{Text = "11", Value = "11", },
                new SelectListItem{Text = "12", Value = "12", }
            };

            // Get this year
            int thisYear = DateTime.Now.Year;
            // Get ten years later 
            int tenYearsLater = DateTime.Now.AddYears(10).Year;


            YearList = new List<SelectListItem>()
            {
                new SelectListItem{Text = string.Empty, Value = "0", Selected= true},
            };

            // Add days from this year to ten later to dropdownlist
            for (int i = thisYear; i <= tenYearsLater; i++)
            {
                YearList.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            Year = 0;
        }
    }

    public class ConfirmationViewModel
    {
        public string ClubDomain { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public Cart.Cart Cart { get; set; }

        public string ClubName { get; set; }

        public string ConfirmationId { get; set; }

        public string ClubLogoUrl { get; set; }


        public ConfirmationViewModel(Cart cart, PaymentMethod paymentMethod, string clubDomain, string confirmationId)
        {
            ClubDomain = clubDomain;
            Cart = cart;
            PaymentMethod = paymentMethod;
            ConfirmationId = confirmationId;

            // Get club from db
            // Set clubname and club logo url for use in payment confirmation page
            var club = DependencyResolverHelper.IClubDbService.GetBasicInfo(clubDomain, true);
            ClubName = club.Name;
            ClubLogoUrl = Utilities.GetClubLogoURL(club.Domain, club.Logo, club.CategoryId);

            if (PaymentMethod == PaymentMethod.Cash)
            {
                foreach (var order in cart.Orders)
                {
                    var convenience = order.Charges.SingleOrDefault(m => m.Category == ChargeDiscountCategories.Convenience);
                    order.Charges.Remove(convenience);
                }
            }
        }
    }

    public class PaypalPaymentErrorViewModel
    {
        public string ClubDomain { get; set; }

        public string ClubName { get; set; }

        public string ClubLogoUrl { get; set; }

        public List<string> ErrorMessages { get; set; }

        public PaypalPaymentErrorViewModel()
        {
            ErrorMessages = new List<string>();
        }
    }   
}