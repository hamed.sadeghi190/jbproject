using Jb.Framework.Common.Forms.JsonFormatter;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using JsonHelper = Jumbula.Common.Helper.JsonHelper;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbRadioList : JbBaseElement, IInputElement
    {
        public JbRadioList()
        {
            Items = new List<JbElementItem>();
        }
        private string _stringElements;

        public List<JbElementItem> Items { get; set; }
        public string Value { get; set; }
        [JsonIgnore]
        public string JsonItems
        {
            get
            {

                if (Items != null && Items.Any())
                {
                    JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
                    _stringElements = Jumbula.Common.Helper.JsonHelper.JsonSerializer(Items, settings);
                }
                return _stringElements;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _stringElements = value;
                    JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
                    Items = JsonHelper.JsonDeserialize<List<JbElementItem>>(_stringElements, settings);
                }
            }
        }

        public override object GetValue()
        {
           return Items.Any() && Items.Any(i => i.Value == Value) ? Items.Single(i => i.Value == Value).Text : null;
        }


        public bool IsRequired
        {
            get;
            set;
        }

    }
}