﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum FormType : byte
    {
        [Description("Registration form")]
        Registration = 0,
        [Description("Online form")]
        FollowUp = 1,
        [Description("Supplemental form")]
        UploadForm = 2
    }
}
