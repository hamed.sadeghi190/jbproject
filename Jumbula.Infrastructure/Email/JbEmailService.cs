﻿using Autofac.Features.Indexed;
using Jumbula.Common.Delegates;
using Jumbula.Common.Enums;
using Jumbula.Core.Infrastructure.Email;
using System;
using System.Net.Mail;

namespace Jumbula.Infrastructure.Email
{
    public class JbEmailService : IJbEmailService
    {

        #region Fields
        private readonly IIndex<string, IEmailProvider> _emailProviderBuilder;
        private readonly IEmailProvider _emailProvider;
        private readonly DeploymentEnvironment _deploymentEnvironment;
        #endregion

        #region Constructor
        public JbEmailService(IIndex<string, IEmailProvider> emailProviderBuilder, DeploymentEnvironment deploymentEnvironment)
        {
            _emailProviderBuilder = emailProviderBuilder;
            _deploymentEnvironment = deploymentEnvironment;
            _emailProvider = GetEmailProvider();
        }
        #endregion

        public void Send(MailMessage message, int emailId, EmailCompletedEventHandler emailCompletedEventHandler, bool isCampaignMode = false)
        {
            try
            {
                _emailProvider.Send(message, emailId, emailCompletedEventHandler, isCampaignMode);
            }
            catch (Exception ex)
            {
                emailCompletedEventHandler(_emailProvider, new EmailCompletedEventArgs(emailId, ex));
            }
        }

        private IEmailProvider GetEmailProvider()
        {
            if (_deploymentEnvironment == DeploymentEnvironment.Local)
                return _emailProviderBuilder[nameof(CommonEmailProvider)];

            return _emailProviderBuilder[nameof(SendGridEmailProvider)];
        }
    }
}
