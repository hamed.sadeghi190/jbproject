
Drop table [dbo].[ClientCreditCards]
GO
CREATE TABLE [dbo].[ClientCreditCards](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ClientId] [bigint] NOT NULL,
	[CardHolderName] [nvarchar](max) NULL,
	[ExpiryYear] [nvarchar](4) NULL,
	[ExpiryMonth] [nvarchar](2) NULL,
	[Brand] [nvarchar](20) NULL,
	[IsDefault] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Address_Id] [int] NULL,
	[UserId] [int] NOT NULL,
	[StripeCustomerId] [nvarchar](128) NULL,
	[LastDigits] [nvarchar](4) NULL,
	[StripeCustomerResponse] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.ClientCreditCards] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClientCreditCards] ADD  DEFAULT ((0)) FOR [UserId]
GO

ALTER TABLE [dbo].[ClientCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientCreditCards_dbo.Clients_ClientId] FOREIGN KEY([ClientId])
REFERENCES [dbo].[Clients] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClientCreditCards] CHECK CONSTRAINT [FK_dbo.ClientCreditCards_dbo.Clients_ClientId]
GO

ALTER TABLE [dbo].[ClientCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientCreditCards_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClientCreditCards] CHECK CONSTRAINT [FK_dbo.ClientCreditCards_dbo.JbUsers_UserId]
GO

ALTER TABLE [dbo].[ClientCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientCreditCards_dbo.PostalAddresses_Address_Id] FOREIGN KEY([Address_Id])
REFERENCES [dbo].[PostalAddresses] ([Id])
GO

ALTER TABLE [dbo].[ClientCreditCards] CHECK CONSTRAINT [FK_dbo.ClientCreditCards_dbo.PostalAddresses_Address_Id]
GO


