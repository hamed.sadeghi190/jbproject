﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;

namespace SportsClub.Models
{
    public class OrderHistoryViewModel
    {
        public string UserName { get; set; }

        public OrderAction Action { get; set; }

        public string ActionDate { get; set; }

        public string Description { get; set; }

        public OrderHistoryViewModel()
        {

        }

        public OrderHistoryViewModel(OrderHistory history)
        {
            if (history.UserId > 0)
            {
                this.UserName = Ioc.UserProfileBusiness.Get(history.UserId).UserName;
            }
            else if (history.UserId == -1)
            {
                //var jbForm = Ioc.IOrderDbService.GetOrder(history.OrderId, false).JbForm;
                //var infoSection = Ioc.IPlayerProfileDbService.GetParticipantSection(jbForm);
                //var userEmail = ((JbEmail)infoSection.Elements.Single(e => e.Name == ElementName.Email.ToString())).Value;
                //this.UserName = userEmail;
            }
            this.Action = history.Action;
            this.ActionDate = history.ActionDate.ToString("g");
            this.Description = history.Description;
        }
    }
}