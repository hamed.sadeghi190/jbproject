﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CountyProvidersController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = {};
        $scope.Model.checkedIds = {};
        var detailExportPromises = [];

        var dataSourceFulldata = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: baseUrl + '/GetAllCountryProviders?countyId=' + $scope.MC.stateParams.county
            },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            }
        });

        dataSourceFulldata.read();

        $scope.CountyProvidersReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetCountryProvider',
                        type: "POST",
                        data: function () {
                            return {
                                countyId: $scope.MC.stateParams.county
                            };
                        },
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
        });

        $scope.CountyProvidersReportGridOptions = {
            excel: {
                allPages: true,
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {
                e.preventDefault();
                var workbook = e.workbook;
                detailExportPromises = [];
                // kendo new version
                var masterData = e.data;
                for (var rowIndex = 0; rowIndex < masterData.length; rowIndex++) {
                    exportChildData(masterData[rowIndex].ClubId, rowIndex);
                }
                $.when.apply(null, detailExportPromises)
                    .then(function () {
                        // get the export results
                        var detailExports = $.makeArray(arguments);

                        // sort by masterRowIndex
                        detailExports.sort(function (a, b) {
                            return a.masterRowIndex - b.masterRowIndex;
                        });

                        // add an empty column
                        workbook.sheets[0].columns.unshift({
                            width: 30
                        });

                        // prepend an empty cell to each row
                        for (var i = 0; i < workbook.sheets[0].rows.length; i++) {
                            workbook.sheets[0].rows[i].cells.unshift({});
                        }

                        // merge the detail export sheet rows with the master sheet rows
                        // loop backwards so the masterRowIndex doesn't need to be updated
                        for (var i = detailExports.length - 1; i >= 0; i--) {
                            var masterRowIndex = detailExports[i].masterRowIndex + 1; // compensate for the header row

                            var sheet = detailExports[i].sheet;

                            // prepend an empty cell to each row
                            for (var ci = 0; ci < sheet.rows.length; ci++) {
                                if (sheet.rows[ci].cells[0].value) {
                                    sheet.rows[ci].cells.unshift({});
                                }
                            }

                            // insert the detail sheet rows after the master row
                            [].splice.apply(workbook.sheets[0].rows, [masterRowIndex + 1, 0].concat(sheet.rows));
                        }

                        // save the workbook
                        kendo.saveAs({
                            dataURI: new kendo.ooxml.Workbook(workbook).toDataURL(),
                            fileName: "CountyProviders.xlsx",
                            proxyURL: "/Dashboard/PortalReport/Save"
                        });


                    });
                cfpLoadingBar.complete();
            },
            dataSource: $scope.CountyProvidersReportDataSource,
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5
            },
            detailTemplate: kendo.template($("#template").html()),
            detailInit: detailInit,
            dataBound: function (e) {
                //customReportService.initializeDoubleScroll();
                this.expandRow(this.tbody.find("tr.k-master-row").first());
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if ($scope.Model.checkedIds[view[i].ClubId]) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    }
                }
            },
            sortable: { mode: 'single' },
            columns: [
                {
                    template: "<input type='checkbox' class='checkbox' />",
                    width: "30px",
                    headerTemplate: '<input type="checkbox" class="checkAll" />'
                },
                {
                    field: "Name",
                    title: "Provider",
                    width: "auto",
                },
            ],
        }

        $(document.body).on('click', '#CountyProvidersReportGrid table .checkbox', selectRow);
        $(document.body).on('click', '#CountyProvidersReportGrid table .checkAll', checkAll);

        function exportChildData(clubId, rowIndex) {

            var deferred = $.Deferred();
            detailExportPromises.push(deferred);

            dataSourceFulldata.filter({ field: "ClubId", operator: "eq", value: clubId });

            var exporter = new kendo.ExcelExporter({
                columns: [
                    {
                        field: "County",
                        title: "County"
                    }, {
                        field: "Mabye",
                        title: "Location"
                    }
                ],
                dataSource: dataSourceFulldata
            });

            exporter.workbook().then(function (book, data) {
                deferred.resolve({
                    masterRowIndex: rowIndex,
                    sheet: book.sheets[0]
                });
            });
        }

        //on click of the checkbox:
        function selectRow() {
            var checked = this.checked,
            row = $(this).closest("tr"),
            grid = $("#CountyProvidersReportGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);
            $scope.Model.checkedIds[dataItem.ClubId] = checked;
            if (checked) {
                //-select the row
                row.addClass("k-state-selected");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
            }
        }

        $scope.MakeCampaign = function () {
            var checked = [];
            //for (var i = 0; $scope.Model.checkedIds.length; i++) {
            for (var i in $scope.Model.checkedIds) {
                if ($scope.Model.checkedIds[i]) {
                    checked.push(i);
                }
            }
            // Redirect to the special action
            if (checked.length > 0) {
                $http.post(baseUrl + "/SendEmailToMembers", { membersIds: checked, reportName: "CountryProvider" })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                        } else {
                            jbToast.error(data.Message);
                        }
                    }).error(function (data, status, headers, config) {
                    });
            } else {
                jbToast.error("Please select at lest a member to send mail.");
            }
        };

        function checkAll() {
            var grid = $("#CountyProvidersReportGrid").data("kendoGrid");
            var state = $("#CountyProvidersReportGrid table > thead > tr > th:nth-child(2) > input").prop("checked");
            var ckbox;
            grid.tbody.find("tr.k-master-row").each(function () {
                var $this = $(this);
                ckbox = $this.find("td .checkbox");
                ckbox.prop("checked", state);
                var dataItem = grid.dataItem($this);
                $scope.Model.checkedIds[dataItem.ClubId] = state;
                if (state) {
                    //-select the row
                    $this.addClass("k-state-selected");
                } else {
                    //-remove selection
                    $this.removeClass("k-state-selected");
                }
            });
        }

        function detailInit(e) {
            var detailRow = e.detailRow;
            detailRow.find(".orders").kendoGrid({
                dataSource: {
                    type: "json",
                    transport: {
                        read: {
                            url: baseUrl + '/GetClubCounties',
                            type: "POST",
                            data: function () {
                                return {
                                    clubId: e.data.ClubId
                                };
                            },
                        },
                    },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                    serverPaging: true,
                    pageSize: 10,
                },
                excelExport: function (e) {
                    e.preventDefault();
                },
                scrollable: false,
                sortable: true,
                pageable: true,
                columns: [
                    {
                        field: "County",
                        title: "County",
                        width: "70px"
                    },
                    {
                        field: "Mabye",
                        title: "Location",
                        width: "110px",
                    }
                ]
            });
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#CountyProvidersReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();