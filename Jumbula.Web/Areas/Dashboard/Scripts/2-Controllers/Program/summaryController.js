﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('summaryController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation) {
        window.app.globalObjects.pageClass = 'seasons-view program';
        window.app.globalObjects.initializeView();
       
        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.MC.stateParams.seasonDomain;

        $scope.SendReservation = function (orderitemId) {

            $http.post(window.location.origin + '/Dashboard/Order/SendReservation/?orderitemId=' + orderitemId)
              .success(function (data, status, headers, config) {
                  if (data.Status == true) {
                      jbToast.success("The reservation sent successfully.");
                      $scope.mainGridOptions.dataSource.read();
                  }
                  else {
                      $scope.MC.handleErrors($scope, data);
                  }
              });
        }

        $scope.hideConfirmation = !$scope.MC.isAllowedAction('Orders_ConfirmationId_View');
        $scope.hideBalance = !$scope.MC.isAllowedAction('Orders_Balance_View');


        var programId = $stateParams.programId;
        $scope.searchTerm = "";
        $scope.filter = {};
        $scope.filter.startDate = "";
        $scope.filter.endDate = "";
        $scope.filter.chessSchedule = "";
        $scope.filter.chessSection = "";
        $scope.filter.entryFeeId = 0;
        $scope.filter.scheduleId = 0;
        $scope.filter.statusReasonId = 0;
        $scope.filter.searchType = 'Name';
        $scope.filter.firstName = '';
        $scope.filter.lastName = '';

        $scope.orderDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: window.location.origin + '/Dashboard/Order/GetList/?programId=' + programId,
                    contentType: "application/json; charset=utf-8", // tells the web method to serialize JSON
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                               term: $scope.searchTerm,
                               searchType: $scope.filter.searchType,
                               firstName: $scope.filter.firstName,
                               lastName: $scope.filter.lastName,
                               scheduleId: $scope.filter.scheduleId,
                               startDate: $scope.filter.startDate,
                               endDate: $scope.filter.endDate,
                               chessSchedule: $scope.filter.chessSchedule,
                               chessSection: $scope.filter.chessSection,
                               entryFeeId: $scope.filter.entryFeeId,
                               statusReasonId: $scope.filter.statusReasonId,
                           }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            pageSize: 10,
            batch: true,
            serverPaging: true,
            serverSorting: false,
            schema: {
                total: "total",
                data: function (response) {

                    $scope.TotalAmount = response.totalAmount;
                    $scope.OrderCount = response.total;
                    return response.data;
                }
            },
            sortable: true
        });

        var baseColumns = [{
            field: "Attendee",
            title: "Participant",
            width: "150px",
            template: "<a  ngaction='OrderDetail_View' ui_sref='OrderItem({seasonDomain:MC.stateParams.seasonDomain ,orderItemId:  #=Id#})'  class='block'> #= Attendee # #if(ItemStatusReason == '1') {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == '2') {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == '5') {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == '4'){# <i class='grid-label label warning'>Canceled</i> #} else if (ItemStatus == '1'){# <i class='grid-label label warning'>Draft</i> #}#"
        }, {
            field: "StrOrderDate",
            title: "Order date",
            width: "140px",
           
        }, {
            field: "ConfirmationId",
            title: "Confirmation",
            width: "120px",
            hidden: $scope.hideConfirmation,
            template: "<a ui_sref='Order({seasonDomain:MC.stateParams.seasonDomain ,orderId:  #=OrderId#})'  class='block'> #if(ItemStatus == '1'){# <p></p> #} else {# #= ConfirmationId # #}#</a>"
        }, {
            field: "EntryFeeName",
            title: 'Tuition option',
            width: '140px',
            template: '<span> #=EntryFeeName# (#=EntryFee#)</span>'
        }, {
            field: "Balance",
            title: 'Balance',
            hidden: $scope.hideBalance,
            width: '70px',
            template: "<span>#if(ItemStatusReason == '2' || ItemStatus == '1'){# <p>-</p> #} else if(Balance >0) {# <p class='fg-red'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance <0) {# <p class='fg-green'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance ==0) {# <p>#=Balance#</p> #}#"
        }];
        var actionColumn = [{
            title: "Actions",
            width: "70px",
            headerTemplate: "<span class='actions-title'>Actions</span>",
            template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li ngaction="OrderDetail_View" ui_sref="OrderItem({seasonDomain:MC.stateParams.seasonDomain ,orderItemId:  #=Id#})">\
                                          <span class="k-link jbi-angle-right">View details</span>\
                                    </li>\
                                   <li ngaction="OrderDetail_View" ng-show="#=ItemStatus == 1#" ng-click="CompleteOrder(#= Id#)">\
                                          <span class="k-link jbi-angle-right">Complete the order</span>\
                                    </li>\
                                    <li ngaction="Order_Edit" ng-hide="#=ItemStatus!=3 || ProgramType ==\'Subscription\' || ProgramType ==\'BeforeAfterCare\' || ProgramType ==\'Calendar\'#" ng-click="edit(#=Id#)">\
                                          <span class="k-link jbi-pencil">Edit/Cancel</span>\
                                    </li>\
                                    <li ngaction="Order_Edit" ng-hide="#=ItemStatus!=3 || (IsDropIn == true && ProgramType ==\'BeforeAfterCare\') || IsDropIn == false #" ng-click="editDropIn(#=Id#)">\
                                          <span class="k-link jbi-pencil">Edit/Cancel</span>\
                                    </li>\
                                    <li ng-hide="#=ItemStatus!=3 && ItemStatus!=4 || ProgramType !=\'Subscription\' || IsDropIn != false ||  PaymentPlanType != 1 || ProgramType ==\'Calendar\' #" ng-click="edit(#=Id#,\'#=SeasonDomain#\')">\
                                          <span class="k-link jbi-pencil">Edit</span>\
                                    </li>\
                                    <li ngaction="OrderDetail_View" ng-show="#=ItemStatus == 1#" ng-click="DeleteOrder(#= Id#)">\
                                          <span class="k-link jbi-angle-right">Delete</span>\
                                    </li>\
                                    <li ngaction="Order_TakePayment" ng-show="#=(Balance > 0 && PaymentPlanType==0 ) || (EnableInstallmentTakePayment > 0 && PaymentPlanType==1)#" ng-click="makePayment(#= Id#)">\
                                          <span class="k-link jbi-coins">Take a payment</span>\
                                    </li>\
                                    <li ngaction="Order_Refund" ng-show="#=(Balance < 0 && PaymentPlanType==0 ) || (EnableInstallmentRefund >0 && PaymentPlanType==1)#" ng-click="refund(#= Id#)">\
                                          <span class="k-link jbi-fully-refund">Refund</span>\
                                    </li>\
                                     <li ngaction="invoice_View" ng-show="#=Balance>0 && IsSandbox !=true#" ui_sref="FamilyInvoicesOrTakePayment({userId:#=UserId#,type:\'Invoice\',orderId:#=OrderId#})">\
                                          <span class="k-link jbi-invoic">Invoice</span>\
                                    </li>\
                                    <li ngaction="Order_Transfer" ng-hide="#=ItemStatus!=3 || ProgramType !=\'Subscription\'|| PaymentPlanType != 1 || ProgramType ==\'ChessTournament\' || IsDropIn != false || ProgramType ==\'Calendar\'#" ng-click="transfer(#=Id#)">\
                                          <span class="k-link jbi-transfer">Transfer</span>\
                                    </li>\
                                    <li ngaction="Order_Transfer" ng-hide="#=ItemStatus!=3 || ProgramType ==\'Subscription\' || ProgramType ==\'BeforeAfterCare\'|| ProgramType ==\'ChessTournament\' || IsDropIn != false || ProgramType ==\'Calendar\'#" ng-click="transfer(#=Id#)">\
                                          <span class="k-link jbi-transfer">Transfer</span>\
                                    </li>\
                                    <li ngaction="Order_History" ng-click="history(#=Id#,#=OrderId#)">\
                                          <span ng-show="#=ItemStatus# != 1" class="k-link icon-history">History</span>\
                                    </li>\
                                    <li ngaction="Orders_EmailParticipant" ui_sref="ParticipantEmail({seasonDomain:MC.stateParams.seasonDomain,programId:MC.stateParams.programId ,userId:#=UserId#})">\
                                          <span class="k-link jbi-email">Email participant</span>\
                                    </li>\
                                    <li ng-click="SendReservation(#=Id#)">\
                                          <span ng-show="#=ItemStatus# != 1" class="k-link jbi-email">Resend confirmation email</span>\
                                    </li>\
                                     #if(ProgramType ==\'Subscription\' && IsDropIn == false){#\
                                     <hr/>\
                                      <li ng-click="ChangeDays(#=Id#)" >\
                                          <span class="k-link">Change class days</span>\
                                      </li>\
                                        <li ng-hide="#=PaymentPlanType != 1#"  ng-click="ChangeDesiredStart(#=Id#)" >\
                                          <span class="k-link">Change desired start date</span>\
                                      </li>\
                                   #}#\
                            </ul>\
                        </li>\
                    </ul>'
        }];

        var BeforeAfterCareCampsColumns = [{
            field: "Schedule",
            width: "180px",
            title: "Schedule",
            template: "{{ dataItem.Schedule}}"
        }];

        var chessColumns = [{
            field: "ChessSection",
            wodth: "180px",
            title: "Section",
            template: '{{"#=ChessSection#"}}'
        }, {
            field: "ChessSchedule",
            wodth: "180px",
            title: "Schedule",
            template: '{{"#=ChessSchedule#"}}'
        }];

        $http.get(window.location.origin + '/dashboard/program/GetDetails/?programid=' + programId).success(function (data, status, headers, config) {
            $scope.Model = data;
            if ($scope.Model.TypeCategory == "Camp")
                baseColumns = baseColumns.concat(BeforeAfterCareCampsColumns).concat(actionColumn);
            else if ($scope.Model.TypeCategory == "BeforeAfterCare")
                baseColumns = baseColumns.concat(BeforeAfterCareCampsColumns).concat(actionColumn);
            else if ($scope.Model.TypeCategory == "ChessTournament")
                baseColumns = baseColumns.concat(chessColumns).concat(actionColumn);
            else
                baseColumns = baseColumns.concat(actionColumn);
            $("#orderGrid").data("kendoGrid").setOptions({
                columns: baseColumns
            });

        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.orderGridOptions = {
            dataSource: $scope.orderDataSource,
            sortable: true,
            serverPaging: true,
            columns: [{
                field: "Attendee",
                title: "Participant",
                width: "150px",
                template: "<a ngaction='OrderDetail_View' ui_sref='OrderItem({seasonDomain:MC.stateParams.seasonDomain ,orderItemId:  #=Id#})'  class='block'> #= Attendee # #if(ItemStatusReason == '1') {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == '2') {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == '5') {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == '4'){# <i class='grid-label label warning'>Canceled</i> #} else if (ItemStatus == '1'){# <i class='grid-label label warning'>Draft</i> #}#"
            }, {
                field: "StrOrderDate",
                title: "Order date",
                width: "140px",
              
            }, {
                field: "ConfirmationId",
                title: "Confirmation",
                hidden: $scope.hideConfirmation,
                width: "120px",
                template: "<a ui_sref='Order({seasonDomain:MC.stateParams.seasonDomain ,orderId:  #=OrderId#})'  class='block'> #if(ItemStatus == '1'){# <p></p> #} else {# #= ConfirmationId # #}#</a>"
            }, {
                field: "EntryFeeName",
                title: 'Tuition option',
                width: '140px',
                template: '<span> #=EntryFeeName# (#=EntryFee#)</span>'
            }, {
                field: "Balance",
                title: 'Balance',
                hidden: $scope.hideBalance,
                width: '70px',
                template: "<span>#if(ItemStatusReason == '2' || ItemStatus == '1'){# <p>-</p> #} else if(Balance >0) {# <p class='fg-red'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance <0) {# <p class='fg-green'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance ==0) {# <p>#=Balance#</p> #}#"
            }, {
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li ui_sref="OrderItem({seasonDomain:MC.stateParams.seasonDomain ,orderItemId:  #=Id#})">\
                                          <span class="k-link jbi-angle-right">View details</span>\
                                    </li>\
                                    <li ngaction="Order_Edit" ng-hide="#=ItemStatus!=3 || ProgramType ==\'Subscription\' || ProgramType ==\'BeforeAfterCare\' || ProgramType ==\'Calendar\'#" ng-click="edit(#=Id#)">\
                                          <span class="k-link jbi-pencil">Edit/Cancel</span>\
                                    </li>\
                                    <li ngaction="Order_Edit" ng-hide="#=ItemStatus!=3 ||  (IsDropIn == true && ProgramType ==\'BeforeAfterCare\') || IsDropIn == false #" ng-click="editDropIn(#=Id#)">\
                                          <span class="k-link jbi-pencil">Edit/Cancel</span>\
                                    </li>\
                                    <li ng-hide="#=ItemStatus!=3 || ProgramType !=\'Subscription\' || IsDropIn != false ||  PaymentPlanType != 1 || ProgramType ==\'Calendar\' #" ng-click="edit(#=Id#,\'#=SeasonDomain#\')">\
                                          <span class="k-link jbi-pencil">Edit</span>\
                                    </li>\
                                    <li ngaction="Order_TakePayment" ng-show="#=Balance>0#" ng-click="makePayment(#= Id#)">\
                                          <span class="k-link jbi-coins">Take a payment</span>\
                                    </li>\
                                    <li ngaction="Order_Refund" ng-show="#=Balance<0#" ng-click="refund(#= Id#)">\
                                          <span class="k-link jbi-fully-refund">Refund</span>\
                                    </li>\
                                    <li ngaction="invoice_View" ng-show="#=Balance>0 && IsSandbox !=true#" ui_sref="FamilyInvoicesOrTakePayment({userId:#=UserId#,type:\'Invoice\',orderId:#=OrderId#})">\
                                          <span class="k-link jbi-invoic">Invoice</span>\
                                    </li>\
                                    <li ngaction="Order_Transfer" ng-hide="#=ItemStatus!=3 || ProgramType ==\'Subscription\' || ProgramType ==\'ChessTournament\' || IsDropIn != false || ProgramType ==\'Calendar\'#" ng-click="transfer(#=Id#)">\
                                          <span class="k-link jbi-transfer">Transfer</span>\
                                    </li>\
                                    <li ngaction="Order_Transfer" ng-hide="#=ItemStatus!=3 || ProgramType !=\'Subscription\' || PaymentPlanType != 1 || ProgramType ==\'ChessTournament\' || IsDropIn != false || ProgramType ==\'Calendar\'#" ng-click="transfer(#=Id#)">\
                                          <span class="k-link jbi-transfer">Transfer</span>\
                                    </li>\
                                    <li ngaction="Order_History" ng-click="history(#=Id#,#=OrderId#)">\
                                          <span ng-show="#=ItemStatus# != 1" class="k-link icon-history">History</span>\
                                    </li>\
                                    <li ui_sref="ParticipantEmail({seasonDomain:MC.stateParams.seasonDomain,programId:MC.stateParams.programId ,userId:#=UserId#})">\
                                          <span class="k-link jbi-email">Email Participant</span>\
                                    </li>\
                                   <li ng-click="SendReservation(#=Id#)">\
                                          <span ng-show="#=ItemStatus# != 1" class="k-link jbi-email">Resend confirmation email</span>\
                                    </li>\
                                     #if(ProgramType ==\'Subscription\' && IsDropIn == false){#\
                                     <hr/>\
                                      <li ng-click="ChangeDays(#=Id#)" >\
                                          <span class="k-link">Change class days</span>\
                                      </li>\
                                       <li ng-hide="#=PaymentPlanType != 1#" ng-click="ChangeDesiredStart(#=Id#)" >\
                                          <span class="k-link">Change desired start date</span>\
                                      </li>\
                                   #}#\
                            </ul>\
                        </li>\
                    </ul>'
            }],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
        };

        $scope.search = function () {
            $("#orderGrid").data("kendoGrid").dataSource.read();
        };

        $scope.transfer = function (id) {

            $state.go('Transfer', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderId: id });
        };

        $scope.edit = function (id) {

            $state.go('EditOrder', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderId: id });
        };

        $scope.history = function (id, orderId) {

            $state.go('OrderHistory', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderId: orderId, orderItemId: id });
        };
        $scope.makePayment = function (id) {
            $state.go("MakePayment", { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, orderItemId: id });
        }
        $scope.refund = function (id) {

            $state.go("PartiallyRefundOrderItem", { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId, orderItemId: id });
        }
        $scope.ChangeDays = function (id) {
            $state.go("OrderChandgeDays", { seasonDomain: $stateParams.seasonDomain, orderId: id });
        }
        $scope.ChangeDesiredStart = function (id) {
            $state.go("OrderChandgeDesiredStartDate", { seasonDomain: $stateParams.seasonDomain, orderId: id });
        }
        $scope.cancelSubscription = function (id) {
            $state.go("CancelSubscriptionOrder", { seasonDomain: $stateParams.seasonDomain, orderId: id });
        }
        $scope.editDropIn = function (id) {
            $state.go("EditCancelDropInOrder", { seasonDomain: $stateParams.seasonDomain, orderId: id });
        }
        $scope.DeleteOrder = function (id) {
            $http.get(window.location.origin + '/Dashboard/Order/DeleteDraftOrder', { params: { orderItemId: id } }).success(function (data, status, headers, config) {
                if (data.Status) {
                    $("#orderGrid").data("kendoGrid").dataSource.read();
                    jbToast.success("The order deleted successfully.");
                }
               
            });
        }

        $scope.CompleteOrder = function (id) {
            jbConfirmation
                .yes(function () {
                 
                    $http.post(window.location.origin + '/Dashboard/Order/CompleteDraftOrder', { orderItemId: id })
                        .success(function (data, status, headers, config) {
                            if (data.Status) {
                                $("#orderGrid").data("kendoGrid").dataSource.read();
                                jbToast.success("The order completed successfully");
                            } else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        })
                })
                .confirm();
        }


        $scope.clearFilters = function () {
            $scope.searchTerm = "";
            $scope.filter.startDate = "";
            $scope.filter.endDate = "";
            $scope.filter.entryFeeId = 0;
            $scope.filter.scheduleId = 0;
            $scope.filter.statusReasonId = -1;
            $scope.filter.paymentMode = 2;
            $scope.filter.firstName = '';
            $scope.filter.lastName = '';

            $("#orderGrid").data("kendoGrid").dataSource.read();
        }

        $scope.scheduleChange = function () {
            $scope.filter.entryFeeId = 0;
        }

        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;
        
        $scope.MakeCampaign = function () {
            $http.post(window.location.origin + '/Dashboard/Order/GetList/?programId=' + programId, { makeCampaign: true, statusReasonId: $scope.filter.statusReasonId, entryFeeId: $scope.filter.entryFeeId, chessSection: $scope.filter.chessSection, chessSchedule: $scope.filter.chessSchedule, term: $scope.searchTerm, scheduleId: $scope.filter.scheduleId, startDate: $scope.filter.startDate, endDate: $scope.filter.endDate })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }


        $scope.RegisterSomeOne = function (programId) {

            $http.post(window.location.origin + '/Dashboard/Order/DoesOfflineOrderExist/')
              .success(function (data, status, headers, config) {
                  if (data.Status == true) {
                      if (data.RecordsAffected == 0) {
                          $state.go("Register", { seasonDomain: $stateParams.seasonDomain, programId: programId, Type: "register" });
                      }
                      else {
                          $state.go("ConfirmNewRegisteration", { seasonDomain: $stateParams.seasonDomain, programId: programId });
                      }
                  }
                  else {
                      $scope.MC.handleErrors($scope, data);
                  }
              });
        }

    }]).filter('chargeFilter', function () {
        return function (items, filter) {
            if (!items)
                return items;

            var result = [];
            for (var i = 0; i < items.length; i++) {
                if (items[i].scheduleId == 0 || ((items[i].ProgramScheduleId == filter.scheduleId || filter.scheduleId == 0) && items[i].Category == 'EntryFee'))
                    result.push(items[i]);
            }
            return result;
        };
    })
})();
