﻿using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Jumbula.Common.Utilities
{

    public class Sort
    {
        public string Field { get; set; }

        public SortDirection Dir { get; set; }
    }

    public enum SortDirection
    {
        Asc = 0,
        Desc = 1
    }

    public class PaginationAdditionalOption
    {
        public PaginationAdditionalOption()
        {
            SortAlternates = new Dictionary<string, string>();
        }

        public Dictionary<string, string> SortAlternates { get; set; }

        public void AddSort(string key, string value)
        {
            SortAlternates.Add(key, value);
        }
    }
    public static class Pagination
    {
        public static IOrderedEnumerable<T> OrderBy<T>(this IEnumerable<T> source, List<Sort> sortList, PaginationAdditionalOption option = null) where T : class
        {

            IOrderedEnumerable<T> result = Enumerable.Empty<T>().OrderBy(x => 1);

            result = Enumerable.OrderBy<T, object>(source, AccessorCache.GetAccessor("Id"), Comparer<object>.Default);

            int sortStage = 0;

            if (sortList != null && sortList.Any())
            {
                foreach (var sortItem in sortList)
                {

                    if (option != null && option.SortAlternates.Any())
                    {
                        if (option.SortAlternates.ContainsKey(sortItem.Field))
                        {
                            string value = null;

                            if (option.SortAlternates.TryGetValue(sortItem.Field, out value))
                            {
                                sortItem.Field = value;
                            }

                        }
                    }

                    if (sortStage < 1)
                    {
                        if (sortItem.Dir == SortDirection.Asc)
                        {
                            result = Enumerable.OrderBy<T, object>(source, AccessorCache.GetAccessor(sortItem.Field), Comparer<object>.Default);
                        }
                        else
                        {
                            result = Enumerable.OrderByDescending<T, object>(source, AccessorCache.GetAccessor(sortItem.Field), Comparer<object>.Default);
                        }
                    }
                    else
                    {
                        if (sortItem.Dir == SortDirection.Asc)
                        {
                            result = Enumerable.ThenBy<T, object>(result, AccessorCache.GetAccessor(sortItem.Field), Comparer<object>.Default);
                        }
                        else
                        {
                            result = Enumerable.ThenByDescending<T, object>(result, AccessorCache.GetAccessor(sortItem.Field), Comparer<object>.Default);
                        }
                    }

                    sortStage++;
                }
            }

            return result;
        }



        public static PaginatedList<T> ToPaginatedList<T>(this IEnumerable<T> query, int pageIndex, int pageSize, int total)
        {
            var list = query.ToList();
            return new PaginatedList<T>(list, pageIndex, pageSize, total);
        }

        public static PaginatedList<T> ToPaginatedList<T>(this IQueryable<T> query, int pageIndex, int pageSize, int total)
        {
            var list = query.ToList();
            return new PaginatedList<T>(list, pageIndex, pageSize, total);
        }

        public static IQueryable<T> Paginate<T>(this IQueryable<T> query, int pageIndex, int pageSize)
        {
            var entities = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return entities;
        }

        public static IQueryable<T> Paginate<T>(this List<T> query, int pageIndex, int pageSize)
        {
            var entities = query.Skip((pageIndex - 1) * pageSize).Take(pageSize).AsQueryable();
            return entities;
        }

        public static IEnumerable<T> Paginate<T>(this IEnumerable<T> query, int pageIndex, int pageSize)
        {
            var entities = query.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return entities;
        }
    }

    public interface IPaginatedList : IList
    {
        int TotalCount { get; set; }
    }

    public class PaginatedList<T> : List<T>, IPaginatedList
    {
        public int PageIndex { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; set; }
        public int TotalPageCount { get; private set; }

        public bool HasPreviousPage => (PageIndex > 1);

        public bool HasNextPage => PageIndex < TotalPageCount;

        public PaginatedList(IEnumerable<T> source, int pageIndex, int pageSize, int totalCount)
        {

            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            AddRange(source);
            PageIndex = pageIndex;
            PageSize = pageSize;
            TotalCount = totalCount;
            TotalPageCount = (int)Math.Ceiling(totalCount / (double)pageSize);
        }
    }

    public static class AccessorCache
    {
        private static readonly Hashtable accessors = new Hashtable();

        private static readonly Hashtable callSites = new Hashtable();

        private static CallSite<Func<CallSite, object, object>> GetCallSiteLocked(string name)
        {
            var callSite = (CallSite<Func<CallSite, object, object>>)callSites[name];
            if (callSite == null)
            {
                callSites[name] = callSite = CallSite<Func<CallSite, object, object>>.Create(
                            Binder.GetMember(CSharpBinderFlags.None, name, typeof(AccessorCache),
                            new CSharpArgumentInfo[] { CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null) }));
            }
            return callSite;
        }

        internal static Func<dynamic, object> GetAccessor(string name)
        {
            Func<dynamic, object> accessor = (Func<dynamic, object>)accessors[name];
            if (accessor == null)
            {
                lock (accessors)
                {
                    accessor = (Func<dynamic, object>)accessors[name];
                    if (accessor == null)
                    {
                        if (name.IndexOf('.') >= 0)
                        {
                            string[] props = name.Split('.');
                            CallSite<Func<CallSite, object, object>>[] arr = Array.ConvertAll(props, GetCallSiteLocked);
                            accessor = target =>
                            {
                                object val = (object)target;
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    var cs = arr[i];
                                    val = cs.Target(cs, val);
                                }
                                return val;
                            };
                        }
                        else
                        {
                            var callSite = GetCallSiteLocked(name);
                            accessor = target =>
                            {
                                return callSite.Target(callSite, (object)target);
                            };
                        }
                        accessors[name] = accessor;
                    }
                }
            }
            return accessor;
        }

        public static IEnumerable<dynamic> Page(this IEnumerable<dynamic> source, PaginationModel paginationModel)
        {
            if (paginationModel.Sort != null && paginationModel.Sort.Any())
            {
                var result = source.Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize);
                return result;
            }
            else
            {
                return source.Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize);
            }
        }

        public static IEnumerable<T> Pagination<T>(this IEnumerable<T> source, PaginationModel paginationModel, PaginationAdditionalOption option = null) where T : class
        {
            if (paginationModel == null)
            {
                return source;
            }

            if (paginationModel.Sort != null && paginationModel.Sort.Any())
            {
                var result = source.OrderBy(paginationModel.Sort, option).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize);
                return result;
            }
            else
            {
                return source.Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize);
            }
        }


    }

    public class PaginationModel
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public List<Sort> Sort { get; set; }
    }

}
