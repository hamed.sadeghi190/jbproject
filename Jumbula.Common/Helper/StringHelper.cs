﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Common.Helper
{
    public static class StringHelper
    {
        public static string RemoveBadChar(this string val, string badCharacters)
        {
            foreach (var chr in badCharacters.Select(c => c.ToString()).ToList())
                val = val.Replace(chr, "");
            return val;
        }

        public static bool EqualsIgnoreSpaces(this string str1, string str2)
        {
            if (String.IsNullOrEmpty(str1) || String.IsNullOrEmpty(str2))
                return false;
            return str1.Replace(" ", "").ToLower().Equals(str2.Replace(" ", "").ToLower());
        }

        public static bool EqualsTrim(this string str1, string str2)
        {
            if (String.IsNullOrEmpty(str1) || String.IsNullOrEmpty(str2))
                return false;
            return str1.Trim().Equals(str2.Trim(), StringComparison.OrdinalIgnoreCase);
        }

        public static string Join(string separator, params string[] args) => String.Join(separator, args.Where(a => !String.IsNullOrEmpty(a)));

        public static string JoinStringList(string[] strings, string separator)
        {
            List<string> list = new List<string>();

            foreach (var item in strings)
            {
                if (!String.IsNullOrEmpty(item))
                {
                    list.Add(item);
                }
            }

            return String.Join(separator, list);
        }

        public static string TruncateName(string name, int maxLength)
        {
            if (String.IsNullOrEmpty(name) || maxLength < 1)
            {
                return String.Empty;
            }

            if (name.Length >= maxLength)
            {
                return name.Substring(0, maxLength - 3) + Constants.Constants.S_TrancatedNames;
            }
            return name;
        }

        public static string ReturnEmptyIfNull(string term)
        {
            return term ?? String.Empty;
        }

        public static string FirstCharToUpper(this string input)
        {
            return input == null || input.Length <= 0 ? input : input.First().ToString().ToUpper() + input.Substring(1);
        }
    }
}