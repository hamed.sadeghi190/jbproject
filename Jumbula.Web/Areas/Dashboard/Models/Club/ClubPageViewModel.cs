﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System;
using Jumbula.Core.Model.Generic;
using SportsClub.Models;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ClubPageViewModel
    {
        public List<JbTitleValue> OrganizationTypes { get; set; }
        public List<JbTitleValue> OrganizationSubTypes { get; set; }
        public List<SelectKeyValue<string>> FilterTypes { get; set; }

        public List<SelectKeyValue<bool?>> OrganizationActiveTypes { get; set; }
    
    }
    public enum FilterTypes : byte
    {
        Name,
        Domain,
        [Description("Expiration date")]
        ExpirationDate
    }

}