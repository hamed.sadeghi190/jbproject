IF OBJECT_ID('[dbo].[EventForms]') IS NULL
BEGIN
	CREATE TABLE [dbo].[EventForms](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[FormId] [int] NOT NULL,
		[Camp_Id] [int] NULL,
		[Course_Id] [int] NULL,
		[Tourney_Id] [int] NULL,
		[TTTourney_Id] [int] NULL,
		[Event_Id] [int] NULL
		)	
END
GO

--<< Add Primary Key >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.EventForms')
BEGIN
	ALTER TABLE [dbo].[EventForms] ADD CONSTRAINT [PK_dbo.EventForms] PRIMARY KEY CLUSTERED(id desc)
END
GO

--<< ADD CLOLUMNS >>--
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[EventForms]') AND NAME='Event_Id')
BEGIN
	ALTER TABLE [dbo].[EventForms] ADD Event_Id int null;
END
GO

--<< ADD FOREIGN KEYS >>--
IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.EventForms_dbo.Events_Event_Id')
BEGIN
	ALTER TABLE [dbo].[EventForms] 
	ADD CONSTRAINT [FK_dbo.EventForms_dbo.Events_Event_Id] FOREIGN KEY ([Event_Id]) 
		REFERENCES [dbo].[Events] ([Id]) 
		ON DELETE CASCADE
		ON UPDATE CASCADE;
END
GO