﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Areas.Dashboard.Models
{
    public class EditOrderViewModel
    {
        public long Id { get; set; }

        public string ProgramName { get; set; }

        public string PlayerName { get; set; }

        public string ConfirmationId { get; set; }

        public string Schedule { get; set; }

        public decimal OrderAmount { get; set; }

        public OrderItemMode Mode { get; set; }

        public decimal PaidAmount { get; set; }

        public decimal SubscriptionRefundFee { get; set; }
        public string ProgramType { get; set; }

        public long ProgramId { get; set; }

        [RegularExpression(Jumbula.Common.Constants.Validation.PriceRegex, ErrorMessage = Jumbula.Common.Constants.Validation.PriceRegexErrorMessage)]
        public decimal? EditFee { get; set; }

        [RegularExpression(Jumbula.Common.Constants.Validation.PriceRegex, ErrorMessage = Jumbula.Common.Constants.Validation.PriceRegexErrorMessage)]
        public decimal? CancellationFee { get; set; }

        [RegularExpression(Jumbula.Common.Constants.Validation.PriceRegex, ErrorMessage = Jumbula.Common.Constants.Validation.PriceRegexErrorMessage)]
        public decimal? ProrateFee { get; set; }

        public int SelectedChargeDiscount { get; set; }

        public bool RefundOrPay { get; set; }

        public bool SendEmail { get; set; }

        public string CancelEditMemo { get; set; }
        public DateTime? DesiredStartDate { get; set; }
        public DateTime TodayDate { get; set; }

        public bool HasInstallment => OrderInstallments != null && OrderInstallments.Any();

        public string Gender { get; set; }
        public bool IsAutoCharge { get; set; }
        public int Age { get; set; }

        public PaymentPlanType PaymentPlanType { get; set; }
        public List<ChargeDiscountItemViewModel> OrderChargeDiscounts { get; set; }

        public List<OrderInstallmentViewModel> OrderInstallments { get; set; }
        public List<DropInSessionsViewModel> OrderSessions { get; set; }
        public bool HasSessions { get; set; }

        public List<ProgramPartViewModel> ProgramParts { get; set; }

        public List<OrderItemSchedulePartsViewModel> OrderItemScheduleParts { get; set; }
    }

    public class OrderInstallmentViewModel
    {
        public OrderInstallmentViewModel()
        {

        }

        public OrderInstallmentViewModel(OrderInstallment installment, List<ProgramPartViewModel> programParts = null)
        {
            Id = installment.Id;
            HasTransaction = installment.TransactionActivities != null && installment.TransactionActivities.Any(c => c.TransactionStatus != TransactionStatus.Draft && c.TransactionStatus != TransactionStatus.Failure && c.TransactionStatus != TransactionStatus.Deleted);
            Amount = installment.Amount;
            OldAmountBeforeEdit = installment.Amount;
            DueDate = installment.InstallmentDate;
            StrDueDate = installment.InstallmentDate.Date.ToString(Constants.DefaultDateFormat);
            Description = installment.Type != InstallmentType.Noraml ? string.Format("({0})", installment.Type.ToDescription()) : string.Empty;
            PaidDate = installment.PaidDate.HasValue ? installment.PaidDate.Value.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US")) : "";
            EnableReminder = installment.EnableReminder;
            NoticeSent = installment.NoticeSent;
            PaidAmount = installment.PaidAmount;
            Status = (installment.Status == OrderStatusCategories.completed && installment.PaidDate.HasValue) ? string.Format("Paid on: {0}", installment.PaidDate.Value.ToString(Constants.DefaultDateFormat)) : "-";
            Token = installment.Token;
            ItemStatus = installment.Status;
            TypeProgram = installment.OrderItem.ProgramTypeCategory;
            WasDeleted = installment.IsDeleted;
            IsDeleted = installment.IsDeleted;
            Type = installment.Type;
            DeleteReason = installment.DeleteReason;
            if (installment.OrderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription || installment.OrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                if (installment.ProgramSchedulePartId.HasValue)
                {
                    ProgramSchedulePartId = installment.ProgramSchedulePartId.Value;
                }

                Schedule = programParts.ToList().Where(p => p.Id == installment.ProgramSchedulePartId).FirstOrDefault();
            }
        }

        public long Id { get; set; }
        public string Token { get; set; }
        public InstallmentType Type { get; set; }
        public DateTime DueDate { get; set; }

        public string StrDueDate { get; set; }
        public string PaidDate { get; set; }
        public DateTime? PaidDateTime { get; set; }
        public string Status { get; set; }
        public OrderStatusCategories OrderStatus { get; set; }
        public decimal? PaidAmount { get; set; }

        public int ProgramSchedulePartId { get; set; }
        public decimal Amount { get; set; }
        public bool NoticeSent { get; set; }
        public bool EnableReminder { get; set; }
        public bool HasTransaction { get; set; }
        public bool IsDeposit { get; set; }
        public bool IsDeleted { get; set; }
        public OrderStatusCategories ItemStatus { get; set; }
        public string Description { get; set; }
        //public ProgramSubscriptionItem Schedule { get; set; }
        public ProgramPartViewModel Schedule { get; set; }
        public ProgramTypeCategory TypeProgram { get; set; }
        public bool IsDeletedSessions { get; set; }
        public bool WasDeleted { get; set; }
        public bool IsDeletedNormalItem { get; set; }
        public bool IsActiveInstallment { get; set; }
        public bool IsActiveInstallmentAndSessions { get; set; }
        public DeleteReason? DeleteReason { get; set; }

        public decimal OldAmountBeforeEdit { get; set; }

        public List<ProgramPartViewModel> ProgramParts { get; set; }
    }

    public class DropInSessionsViewModel
    {
        public DropInSessionsViewModel()
        {

        }
        public DropInSessionsViewModel(OrderSession session, decimal amount)
        {
            Id = session.Id;
            ProgramSessionId = session.ProgramSessionId;
            Amount = amount;
            Date = session.ProgramSession.StartDateTime.Date;
            NewDate = session.ProgramSession.StartDateTime.Date;
            Time = session.ProgramSession.StartDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + "-" + session.ProgramSession.EndDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "");
            IsOldSession = true;
            NewTime = session.ProgramSession.StartDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + "-" + session.ProgramSession.EndDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "");
            StrDate = session.ProgramSession.StartDateTime.Date.ToString(Constants.DateTime_Comma);
            IsSameDay = session.IsSameDayDropIn;
        }
        public DateTime Date { get; set; }
        public DateTime NewDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string StrDate { get; set; }
        public string Time { get; set; }
        public string NewTime { get; set; }

        public bool IsSameDay { get; set; }
        public decimal Amount { get; set; }
        public bool IsDeleted { get; set; }
        public long Id { get; set; }
        public int? ProgramSessionId { get; set; }
        public bool IsOldSession { get; set; }
        public string Status { get; set; }
    }

    public class ProgramPartViewModel
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DueDate { get; set; }

    }

    public class OrderItemSchedulePartsViewModel
    {
        public int Id { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime DueDate { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsRestored { get; set; }

        public int ProgramSchedulePartId { get; set; }

    }

}