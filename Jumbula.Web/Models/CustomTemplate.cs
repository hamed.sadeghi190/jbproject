﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using FluentValidation;
using SportsClub.Models.FormModels;

namespace SportsClub.Models
{
    //  must be change for geting control from base class
    //public static class CustomModelMetaDataHelper
    //{
    //    /// <summary>
    //    /// 
    //    /// </summary>
    //    /// <param name="model"></param>
    //    /// <returns></returns>
    //    public static CustomModelMetaData GetControls(CustomModelMetaData model)
    //    {
    //        //  
    //        var result = new CustomModelMetaData();
    //        result.Sections = model.Sections;

    //        foreach (var item in model.Fields)
    //        {

    //            if (item.Type == CustomFieldType.Nvarchar)
    //            {
    //                result.Fields.Add(new TextBox()
    //                {
    //                    SectionId = item.SectionId,
    //                    Title = item.Title,
    //                    Type = item.Type,
    //                    Validations = item.Validations,
    //                });
    //            }

    //            else if (item.Type == CustomFieldType.TextArea)
    //            {
    //                result.Fields.Add(new TextArea()
    //                {
    //                    SectionId = item.SectionId,
    //                    Title = item.Title,
    //                    Type = item.Type,
    //                    Validations = item.Validations,
    //                });
    //            }
    //            else if (item.Type == CustomFieldType.Int)
    //            {
    //                // by default this control has OnlyInt validation
    //                result.Fields.Add(new TextBox()
    //                {
    //                    SectionId = item.SectionId,
    //                    Title = item.Title,
    //                    Type = item.Type,
    //                    Validations = item.Validations
    //                });

    //                // if it dosent have OnlyInt Validation add it
    //                if (item.Validations.Any(x => x.Type != QuestionValidationType.OnlyInt))
    //                {
    //                    result.Fields.Last().Validations.Add(new QuestionValidator() { Type = QuestionValidationType.OnlyInt });
    //                }
    //            }
    //            else if (item.Type == CustomFieldType.Boolean)
    //            {
    //                result.Fields.Add(new CheckBox()
    //                {
    //                    SectionId = item.SectionId,
    //                    Title = item.Title,
    //                    Type = item.Type,
    //                    // ignore validation for check box
    //                    //Validations = item.Validations
    //                });
    //            }
    //            else if (item.Type == CustomFieldType.DateTime)
    //            {
    //                result.Fields.Add(new DatePicker()
    //                {
    //                    SectionId = item.SectionId,
    //                    Title = item.Title,
    //                    Type = item.Type,

    //                });
    //                // just required validation consider for datetime 
    //                if (item.Validations.Any(x => x.Type == QuestionValidationType.Required))
    //                {
    //                    result.Fields.Last().Validations.Add(new QuestionValidator() { Type = QuestionValidationType.Required });

    //                    // set default value if required
    //                    ((DatePicker)result.Fields.Last()).Value = DateTime.Now.Date;

    //                }
    //            }

    //        } // foreach
    //        return result;
    //    }
    //}

   
    //#region NewFormBuilder

      


    //        [Serializable]
    //        public class Form
    //        {
    //            public Form()
    //            {
    //                Questions = new List<Question>();
    //            }

    //            public List<Question> Questions { get; set; }
    //        }

    //        [XmlInclude(typeof(QTextArea))]
    //        [XmlInclude(typeof(QTextBox))]
    //        [XmlInclude(typeof(QCheckBox))]
    //        [XmlInclude(typeof(QDatePicker))]
    //        [Serializable]
    //        public class Question
    //        {
    //            public string Title { get; set; }
    //            public List<QuestionValidator> Validations { get; set; }

    //            public Question()
    //            {
    //                Validations = new List<QuestionValidator>();
    //            }
    //        }

    //        [FluentValidation.Attributes.Validator(typeof(FieldModelValidator))]
    //        [Serializable]
    //        public class QTextBox : Question
    //        {
    //            public string Value { get; set; }
    //        }

    //        [Serializable]
    //        public class QTextArea : Question
    //        {
    //            public string Value { get; set; }
    //        }

    //        [Serializable]
    //        public class QDatePicker : Question
    //        {
    //            public DateTime? Value { get; set; }
    //        }

    //        [Serializable]
    //        public class QCheckBox : Question
    //        {
    //            public bool Value { get; set; }
    //        }

    //#endregion

    // Note: in migration you should consider this and uncomment this class to migration 

    //[Serializable]
    //public class CustomModelMetaData
    //{
    //    public List<Question> Fields { get; set; }

    //    public List<ModelSection> Sections { get; set; }

    //    public CustomModelMetaData()
    //    {
    //        Fields = new List<Question>();
    //        Sections = new List<ModelSection>();
    //    }
    //}

    //[Serializable]
    //public class ModelSection
    //{
    //    public int SectionId { get; set; }
    //    public string SectionName { get; set; }
    //}



    [Serializable]
    public class CustomModelMetaData
    {
        public List<CustomField> Fields { get; set; }

        public List<ModelSection> Sections { get; set; }

        public CustomModelMetaData()
        {
            Fields = new List<CustomField>();
            Sections = new List<ModelSection>();
        }
    }

    [XmlInclude(typeof(TextArea))]
    [XmlInclude(typeof(TextBox))]
    [XmlInclude(typeof(CheckBox))]
    [XmlInclude(typeof(DatePicker))]
    [Serializable]
    public class CustomField
    {
        public CustomField()
        {
            Validations = new List<CustomFieldValidation>();
        }

        public string Title { get; set; }
        public string Watermark { get; set; }
        public CustomFieldType Type { get; set; }
        public int SectionId { get; set; }

        public List<CustomFieldValidation> Validations { get; set; }
    }

    public enum CustomFieldType
    {
        [Description("Select")]
        None,

        [Description("Text")]
        Nvarchar,

        Boolean,

        [Description("Date")]
        DateTime,

        [Description("Number")] // must be correct
        Int,

        [Description("Text Area")]
        TextArea

    }

    public enum CustomFieldValidationType
    {
        [Description("Select")]
        None,

        Required,

        [Description("Only number")]
        OnlyInt,

        Range,

        Length,

        Email


    }

    [Serializable]
    public class CustomFieldValidation
    {
        public CustomFieldValidation()
        {
            switch (Type)
            {
                case CustomFieldValidationType.OnlyInt:
                    Message = "You Must Enter An Integer!";
                    break;
                case CustomFieldValidationType.Required:
                    Message = "The Filed Can Not Be Empty!";
                    break;
                case CustomFieldValidationType.Range:
                    Message = "Not valid number in range";
                    break;
            }
        }

        public CustomFieldValidationType Type { get; set; }

        public string Message { get; set; }

        public int MinValue { get; set; }

        public int MaxValue { get; set; }
    }

    [Serializable]
    public class ModelSection
    {
        public int SectionId { get; set; }
        public string SectionName { get; set; }

    }




    [Serializable]
    public class TextBox : CustomField
    {
        public string Value { get; set; }
    }

    [Serializable]
    public class TextArea : CustomField
    {
        public string Value { get; set; }
    }

    [Serializable]
    public class DatePicker : CustomField
    {
        public DateTime? Value { get; set; }
    }

    [Serializable]
    public class CheckBox : CustomField
    {
        public bool Value { get; set; }
    }

   

}