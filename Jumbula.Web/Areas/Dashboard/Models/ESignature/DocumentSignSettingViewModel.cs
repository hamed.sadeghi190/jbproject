﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DocumentSignSettingViewModel
    {
        public List<SelectKeyValue<int>> ActiveStaffs { get; set; }
        public int SelectedStaffSignatory { get; set; }
        public bool HasPartner { get; set; }
    }
}