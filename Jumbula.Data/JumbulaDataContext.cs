﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Configuration;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Base;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Data.DbConfigs;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Jumbula.Data
{
    public class JumbulaDataContext : IdentityDbContext<JbUser, JbRole, int, JbUserLogin, JbUserRole, JbUserClaim>, IEntitiesContext
    {

        #region Constractors
        public JumbulaDataContext()
          : base((WebConfigurationManager.GetSection("jumbula") as NameValueCollection)?["AzureDbConnection"])
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
        }

        public JumbulaDataContext(string connectionString)
            : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
        }

        #endregion

        #region Properties
        public DbSet<SettlementReportOrders> OrderSettlementReports { get; set; }
        public DbSet<SettlementReport> SettlementReports { get; set; }

        public DbSet<EventRoaster> EventRoasters { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<JbFile> JbFiles { get; set; }
        public DbSet<ClubUser> ClubUsers { get; set; }
        public DbSet<ClubType> ClubTypes { get; set; }
        public DbSet<ClubRevenue> ClubRevenues { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderPreapproval> OrderPreapprovals { get; set; }
        public DbSet<OrderInstallment> OrderInstallments { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<StripeWebhook> StripeWebhooks { get; set; }
        public DbSet<BillingNote> BillingNotes { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceHistory> InvoiceHistories { get; set; }
        public DbSet<JbESignature> JbESignatures { get; set; }
        public DbSet<JbFlyer> JbFlyers { get; set; }
        public DbSet<ClubFlyer> ClubFlyers { get; set; }
        public DbSet<ClientBillingHistory> ClientBillingHistories { get; set; }
        public DbSet<BillingTransactionActivity> BillingTransactionActivities { get; set; }
        public DbSet<PlayerProfile> PlayerProfiles { get; set; }
        public DbSet<ReservedDomain> ReservedDomains { get; set; }
        public DbSet<ImageGallery> ImageGallerys { get; set; }
        public DbSet<ImageGalleryItem> ImageGalleryItems { get; set; }
        public DbSet<ContactPerson> ContactPersons { get; set; }
        public DbSet<PaypalIPN> PaypalIpn { get; set; }
        public DbSet<PaypalTracking> PaypalTracking { get; set; }
        public DbSet<JbPage> JbPage { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CetgoryTemplate> CetgoryTemplates { get; set; }
        public DbSet<ClubLocation> ClubLocations { get; set; }
        public DbSet<PostalAddress> PostalAddresses { get; set; }
        public DbSet<Coupon> Coupons { get; set; }

        public DbSet<Charge> Charges { get; set; }
        public DbSet<PaymentPlan> PaymentPlans { get; set; }
        public DbSet<UserPaymentPlan> UserPaymentPlans { get; set; }
        public DbSet<OrderHistory> OrderHistories { get; set; }
        public DbSet<PendingPreapprovalTransaction> PendingPreapprovalTransactions { get; set; }
        public DbSet<JbForm> JbForms { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<MailList> MailLists { get; set; }
        public DbSet<MailListContact> MailListContacts { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<CampaignRecipients> CampaignRecipients { get; set; }
        public DbSet<CampaignCallback> CampaignCallbacks { get; set; }
        public DbSet<CampaignCallbackDetail> CampaignCallbackDetails { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<ProgramRespondToInvitation> ProgramRespondToInvitation { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<PricePlan> PricePlans { get; set; }
        public DbSet<OrderItemFollowupForm> OrderItemFollowupForms { get; set; }
        public DbSet<CatalogItem> Catalogs { get; set; }
        public DbSet<CatalogSetting> CatalogSettings { get; set; }
        public DbSet<UserCreditCard> UserCreditCards { get; set; }
        public DbSet<UserVerification> Verifications { get; set; }
        public DbSet<UserEvent> UserEvents { get; set; }
        public DbSet<StudentAttendance> StudentAttendances { get; set; }
        public DbSet<ScheduleAttendance> ScheduleAttendances { get; set; }
        public DbSet<WaitList> WaitLists { get; set; }
        public DbSet<OrderItemWaiver> OrderItemWaivers { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<UserMessage> UserMessages { get; set; }
        public DbSet<PlayerProgramNote> PlayerProgramNotes { get; set; }
        public DbSet<ScheduleTask> ScheduleTasks { get; set; }
        public DbSet<JbAttribute> JbAttribute { get; set; }
        public DbSet<JbAttributeOption> JbAttributeOption { get; set; }
        public DbSet<ClubAttributeValue> ClubAttributeValues { get; set; }
        public DbSet<ProgramAttributeValue> ProgramAttributeValues { get; set; }
        public DbSet<JbFieldSetting> JbFieldSettings { get; set; }
        public DbSet<Family> Families { get; set; }
        public DbSet<FamilyContact> FamilyContacts { get; set; }
        public DbSet<PlayerInfo> PlayerInfoes { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<Subsidy> Subsidies { get; set; }
        public DbSet<ClubSubsidy> ClubSubsidies { get; set; }
        public DbSet<ProgramSchedulePart> ProgramScheduleParts { get; set; }
        public DbSet<ProgramSession> ProgramSessions { get; set; }
        public DbSet<OrderItemSchedulePart> OrderItemScheduleParts { get; set; }
        public DbSet<JbAudit> JbAudits { get; set; }
        public DbSet<DataMigration> DataMigrations { get; set; }
        public DbSet<CommunicationHistory> CommunicationHistories{ get; set; }

        #endregion


        #region Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
          
            modelBuilder.Entity<Club>().Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<Club>().
                HasMany(p => p.TransactionActivities).WithRequired(c => c.Club).HasForeignKey(c => c.ClubId);

            modelBuilder.Entity<Club>().HasOptional(c => c.PartnerClub).WithMany(c => c.RelatedClubs).HasForeignKey(c => c.PartnerId);

            modelBuilder.Entity<Season>().HasOptional(c => c.Parent).WithMany(c => c.Childs).HasForeignKey(c => c.ParentId);

            modelBuilder.Entity<EventRoaster>()
                      .HasOptional(ev => ev.Season)
                      .WithMany()
                      .HasForeignKey(ev => ev.SeasonId);

            modelBuilder.Entity<ClientCreditCard>().HasRequired(c => c.Client).WithMany(p => p.CreditCards).HasForeignKey(c => c.ClientId);

            modelBuilder.Entity<Season>().ToTable("Seasons");

            modelBuilder.Entity<Club>().ToTable("Clubs");
            modelBuilder.Entity<ClientCreditCard>().ToTable("ClientCreditCards");
            modelBuilder.Entity<CatalogSetting>().ToTable("CatalogSettings");

            modelBuilder.Entity<OrderItem>().ToTable("OrderItems");
            modelBuilder.Entity<OrderInstallment>().ToTable("OrderInstallments");

            modelBuilder.Entity<PlayerProfile>().ToTable("PlayerProfiles");
            modelBuilder.Entity<ImageGallery>().ToTable("ImageGallerys");
            modelBuilder.Entity<ImageGalleryItem>().ToTable("ImageGalleryItems");
            modelBuilder.Entity<ScheduleAttendance>().ToTable("ScheduleAttendances");
            // no class hierarchy, just table name
            modelBuilder.Entity<PendingPreapprovalTransaction>().ToTable("PendingPreapprovalTransactions");
            modelBuilder.Entity<ContactPerson>().ToTable("ContactPersons");
            modelBuilder.Entity<PaypalIPN>().ToTable("PaypalIPN");
            modelBuilder.Entity<OrderPreapproval>().ToTable("OrderPreapprovals");

            modelBuilder.ComplexType<MetaData>();

            modelBuilder.Configurations.Add(new ChargeConfig());
            modelBuilder.Configurations.Add(new CouponConfig());
            modelBuilder.Configurations.Add(new PaymentPlanConfig());

            modelBuilder.Configurations.Add(new ClubUsersConfig());

            modelBuilder.Configurations.Add(new SeasonProfileConfig());
            modelBuilder.Configurations.Add(new OrderConfig());
            modelBuilder.Configurations.Add(new OrderItemConfig());
            modelBuilder.Configurations.Add(new OrderPreapprovalConfig());
            modelBuilder.Configurations.Add(new DiscountConfig());
            modelBuilder.Configurations.Add(new InstallmentConfig());
            modelBuilder.Configurations.Add(new ClientConfig());
            modelBuilder.Configurations.Add(new PaypalIPNConfig());
            modelBuilder.Configurations.Add(new PaypalTrackingConfig());
            modelBuilder.Configurations.Add(new UserCreditCardConfig());
            modelBuilder.Configurations.Add(new ProgramProfileConfig());
            modelBuilder.Configurations.Add(new UserPaymentPlanConfig());
            modelBuilder.Configurations.Add(new ProgramRespondToInvitationConfig());

            modelBuilder.Entity<Program>()
                .HasMany(s => s.Instructors)
                .WithMany(c => c.Programs)
                .Map(cs =>
                {
                    cs.MapLeftKey("ProgramId");
                    cs.MapRightKey("ClubStaffId");
                    cs.ToTable("ProgramInstructors");
                });

            modelBuilder.Entity<JbESignature>()
                .HasRequired(c => c.Sender)
                .WithMany(p => p.PartnerSignatures)
                .HasForeignKey(c => c.SenderId).WillCascadeOnDelete(false);

            modelBuilder.Entity<JbESignature>()
               .HasOptional(c => c.Reciver)
               .WithMany(p => p.MemberSignatures)
               .HasForeignKey(c => c.ReciverId).WillCascadeOnDelete(false);

            modelBuilder.Entity<ClubSubsidy>()
           .HasRequired(c => c.Subsidy)
           .WithMany()
           .HasForeignKey(c => c.SubsidyId).WillCascadeOnDelete(false);

            modelBuilder.Entity<JbForm>().ToTable("JbForms");

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<JbUser>().ToTable("JbUsers");
            modelBuilder.Entity<JbRole>().ToTable("JbRoles");
            modelBuilder.Entity<JbUserRole>().ToTable("JbUserRoles");
            modelBuilder.Entity<JbUserLogin>().ToTable("JbUserLogins");
            modelBuilder.Entity<JbUserClaim>().ToTable("JbUserClaims");

            modelBuilder.Entity<JbUserRole>().HasKey(t => t.Id);
            modelBuilder.Entity<JbUserRole>().Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<JbUserRole>().Property(t => t.UserId).HasColumnName("UserId");
            modelBuilder.Entity<JbUserRole>().Property(t => t.RoleId).HasColumnName("RoleId");
        }

        public static JumbulaDataContext Create()
        {
            return new JumbulaDataContext();
        }

        IDbSet<TEntity> IEntitiesContext.Set<TEntity>()
        {
            return base.Set<TEntity>();
        }

        //TODO
        public void BeginTransaction()
        {
            throw new NotImplementedException();
        }

        public int Commit()
        {
            throw new NotImplementedException();
        }

        public Task<int> CommitAsync()
        {
            throw new NotImplementedException();
        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }

        public void SetAsAdded<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            throw new NotImplementedException();
        }

        public void SetAsDeleted<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            throw new NotImplementedException();
        }

        public void SetAsModified<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            throw new NotImplementedException();
        }

        protected override void Dispose(bool disposing)
        {
        }

        #endregion
    }
}
