﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('jbHomeSiteSettingsController', ['$scope', '$http', function ($scope, $http, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'jb-home-setting-view';
        window.app.globalObjects.initializeView();
        
        $scope.MC.openBlade({ url: "Dashboard/Blade/JbHomeSiteSettings", order: 2, menu: "SETTINGS" });
        
    }]);
})();