﻿using System;
using System.Collections.Generic;

namespace Jumbula.Test.Utility
{
    // Based on FactoryGirl.NET: https://github.com/JamesKovacs/FactoryGirl.NET
    public static class FactoryGirl
    {
        private static readonly IDictionary<Type, Func<object>> Builders = new Dictionary<Type, Func<object>>();

        public static void Define<T>(Func<T> builder)
        {
            if (Builders.ContainsKey(typeof(T)))
                throw new Exception($"{typeof(T).FullName} is already registered. You can only register one factory per type.");

            Builders.Add(typeof(T), () => builder());
        }

        public static T Build<T>()
        {
            return Build<T>(x => { });
        }

        public static T Build<T>(Action<T> overrides)
        {
            if (!Builders.ContainsKey(typeof(T)))
                throw new Exception($"{typeof(T).FullName} is not registered. You must register a type before building instances.");

            var obj = (T)Builders[typeof(T)]();
            overrides(obj);
            return obj;
        }
    }
}