﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SportsClub.Infrastructure.Helper;
using SportsClub.Domain;
using System.ComponentModel.DataAnnotations;
using SportsClub.Infrastructure;
using System.ComponentModel;

namespace SportsClub.Models
{
    public class ChooseProfile
    {
        public string Domain { get; set; }

        public int CategoryId { get; set; }

        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        [Required(ErrorMessage = "Please select a profile.")]
        [Range(1, int.MaxValue)]
        public int ProfileId { get; set; }

        public bool IsAdultLoggedInUser { get; set; }

        public IEnumerable<SelectListItem> PlayerLists;

        // Detect which one (Course,camp,torrny)
        public SubDomainCategories? SubDomainCategory { get; set; }

        public bool ShowDemo { get; set; }

        public ChooseProfile()
        {
            int UserId = TheMembership.GetCurrentUserId();
            //Get Current userID
            var playerProfile = Ioc.IPlayerProfileDbService.GetByUserId(UserId).ToList();
            PlayerLists = playerProfile.Select(p => new SelectListItem()
            {
                Value = p.Id.ToString(),
                Text = p.Contact.FirstName,
                Selected = false
            });
        }

        public ChooseProfile(string domain, string clubDomain, string seasonDomain, SubDomainCategories? subDomainCategory)
            : this()
        {
            int UserId = TheMembership.GetCurrentUserId();

            //Take all PlayerProfiler of MainUser
            var playerProfile = Ioc.IPlayerProfileDbService.GetByUserId(UserId).ToList();

            //set Domain,ClubDomain,categoryId
            Domain = domain;
            SeasonDomain = seasonDomain;
            ClubDomain = clubDomain;
            SubDomainCategory = subDomainCategory;

            //check for IsAdult loggedUser
            //var loggedUserContact = Ioc.IPlayerProfileDbService.GetByUserId(UserId)
            //                                .Single(x => x.Type == PlayerProfileType.Primary)
            //                                .Contact;
            //if (loggedUserContact.DoB == null)
            //    IsAdultLoggedInUser = true;
            //else
            //    IsAdultLoggedInUser = Utilities.CalculateAge(loggedUserContact.DoB.Value) >= Constants.PlayerProfile_Min_Age;
        }

    }
}