﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using TimeZone = Jumbula.Common.Enums.TimeZone;

namespace Jumbula.Common.Helper
{
    public static class DateTimeHelper
    {
        #region Timezone
        public static DateTime GetCurrentLocalDateTime(TimeZone timeZone)
        {
            var systemTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone.ToDescription());
            var localDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, systemTimeZone);

            return localDateTime;
        }

        public static DateTime ConvertUtcDateTimeToLocal(DateTime dateTime, TimeZone? timeZone)
        {
            DateTime localDateTime;

            if (timeZone.HasValue)
            {
                var localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone.ToDescription());
                localDateTime = TimeZoneInfo.ConvertTimeFromUtc(dateTime, localTimeZone);
            }
            else
            {
                localDateTime = dateTime;
            }

            return localDateTime;
        }

        public static DateTime ConvertLocalDateTimeToUtc(DateTime dateTime, TimeZone timeZone)
        {
            var tz = TimeZoneInfo.FindSystemTimeZoneById(timeZone.ToDescription());
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, tz);
        }

        #endregion


        #region Format
        public static string FormatTimeSpan(TimeSpan time)
        {
            return DateTime.Today.Add(time).ToString("h:mm tt").Replace(" AM", " am").Replace(" PM", " pm");
        }

        public static string FormatTimeRange(TimeSpan startTime, TimeSpan endTime)
        {
            string result;

            if ((startTime.Hours < 12 && endTime.Hours < 12) || (startTime.Hours > 12 && endTime.Hours > 12))
            {
                result = $"{DateTime.Today.Add(startTime):h:mm} - {FormatTimeSpan(endTime)}";
            }
            else
            {
                result = $"{FormatTimeSpan(startTime)} - {FormatTimeSpan(endTime)}";
            }

            return result;
        }

        public static string FormatTimeSpanAmPm(TimeSpan time)
        {
            return DateTime.Today.Add(time).ToString("h:mm tt");
        }

        public static string FormatTimeRangeAmPm(TimeSpan startTime, TimeSpan endTime)
        {
            string result;

            if ((startTime.Hours < 12 && endTime.Hours < 12) || (startTime.Hours > 12 && endTime.Hours > 12))
            {
                result = $"{DateTime.Today.Add(startTime):h:mm} - {FormatTimeSpanAmPm(endTime)}";
            }
            else
            {
                result = $"{FormatTimeSpanAmPm(startTime)} - {FormatTimeSpanAmPm(endTime)}";
            }

            return result;
        }

        public static string DateTimeLable(DateTime dateTime, TimeZone? timeZone)
        {
            var today = DateTime.UtcNow;

            var minutes = (int)(today - dateTime).TotalMinutes;

            if (minutes == 1) return $"{minutes} minute ago.";

            if (minutes < 60) return $"{minutes} minutes ago.";

            if (minutes >= 60)
            {
                var hours = (int)(today - dateTime).TotalHours;

                if (hours == 1) return $"{hours} hour ago.";
                if (hours < 24) return $"{hours} hours ago.";

                var day = (int)(today - dateTime).TotalDays;

                if (day == 0) return "Today";
                if (day == 1) return "Yesterday";
                if (day == 2) return "Two days ago";
                if (day > 2) return ConvertUtcDateTimeToLocal(dateTime, timeZone).ToString(Constants.Constants.DateTime_Comma);
            }
            return null;
        }

        public static string GetIsoDateTimeFormat(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd HH':'mm':'ss");
        }

        public static string GetIsoDateFormat(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd");
        }

        public static string FormatDate(DateTime date)
        {
            return date.ToString("M/d/yyyy");
        }
        public static string FormatShortDateWithoutYearBySlash(DateTime date)
        {
            return date.ToString(Constants.Constants.DefaultShortDate);
        }
        public static string FormatShortDateSeperetedByCamma(DateTime date)
        {
            return date.ToString(Constants.Constants.DateTime_Comma);
        }
        public static string FormatShortDateWithoutYear(DateTime date)
        {
            return date.ToString(Constants.Constants.DateWithoutYear);
        }
        public static string FormatShortDateTimeSeperetedByCamma(DateTime date)
        {
            return date.ToString(Constants.Constants.F_DateTimeWithAMPM_Camma);
        }
        #endregion

        #region Convert

        public static DateTime? JbParse(string dateTime)
        {
            if (!dateTime.Contains("/") && dateTime.Length == 8)
            {
                var month = dateTime.Substring(0, 2);
                var day = dateTime.Substring(2, 2);
                var year = dateTime.Substring(4, 4);
                var parse = $"{month}/{day}/{year}";

                return DateTime.Parse(parse);
            }

            return null;
        }

        public static long DateTimetoTimeStamp(DateTime datetime)
        {
            return long.Parse(datetime.ToString("yyyyMMddHHmmssfff"));
        }

        public static int ConvertToUnixTimeStamp(DateTime dateTime)
        {
            return (int)(dateTime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public static DateTime ConvertTimespanToDateTime(long timespan)
        {
            var date = new DateTime(1970, 1, 1, 0, 0, 0, 0);

            var convertDate = date.AddSeconds(timespan);

            return convertDate;
        }

        public static DateTime ToZeroTime(this DateTime datetime)
        {
            return datetime.Date;
        }
        #endregion

        public static bool IsValidDateFormat(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                if (date.Replace("/", "").Length < 6 || !DateTime.TryParse(date, out var dob))
                {
                    return false;
                }
                else
                {
                    if (dob.Year < 1900)
                    {
                        return false;
                    }
                    return true;
                }

            }
            else
            {
                return true;
            }

        }

        public static bool IsTimeAm(TimeSpan time)
        {
            var timePm = new TimeSpan(12, 0, 0);
            var timeAm = new TimeSpan(0, 0, 0);
            var classStartTime = time >= timeAm && time < timePm;


            return classStartTime;
        }

        public static string GetMonthAbbreviation(DateTime date)
        {
            var culture = CultureInfo.InvariantCulture;

            string[] names = culture.DateTimeFormat.AbbreviatedMonthNames;

            return names[date.Month - 1];
        }

        public static string GetDayOfWeekAbbreviation(DayOfWeek dayOfWeek, AbbreviatedDayNameMode abbreviatedDayName = AbbreviatedDayNameMode.ThreeLetter)
        {
            var culture = CultureInfo.InvariantCulture;
            string[] names = { };

            switch (abbreviatedDayName)
            {
                case AbbreviatedDayNameMode.FullName:
                    names = culture.DateTimeFormat.DayNames;
                    break;

                case AbbreviatedDayNameMode.OneLetter:
                    names = new[] { "U", "M", "T", "W", "R", "F", "S" };
                    break;

                case AbbreviatedDayNameMode.TwoLetter:
                    names = culture.DateTimeFormat.ShortestDayNames;
                    break;

                case AbbreviatedDayNameMode.ThreeLetter:
                    names = culture.DateTimeFormat.AbbreviatedDayNames;
                    break;
            }

            return names[(int)dayOfWeek];
        }

        public static List<string> GetDayOfWeekAbbreviation(List<DayOfWeek> days, AbbreviatedDayNameMode abbreviatedDayName = AbbreviatedDayNameMode.ThreeLetter)
        {
            var daysFormated = new List<string>();
            foreach (var day in days)
            {
                daysFormated.Add(GetDayOfWeekAbbreviation(day, abbreviatedDayName));
            }

            return daysFormated;
        }

        public static int CalculateAge(DateTime birthDate, DateTime? currentDate = null)
        {
            if (currentDate == null)
            {
                currentDate = DateTime.UtcNow;
            }

            var result = currentDate.Value.Year - birthDate.Year;
            if (currentDate.Value.DayOfYear < birthDate.DayOfYear)
            {
                result--;
            }

            return result;
        }

        public static SelectList GetYearForDropdownlist()
        {

            return new SelectList(new List<SelectListItem>
            { new SelectListItem { Text = "16", Value = "16" },new SelectListItem { Text = "17", Value = "17" },
                new SelectListItem { Text = "18", Value = "18" },new SelectListItem { Text = "19", Value = "19" },
                new SelectListItem { Text = "20", Value = "20" },new SelectListItem { Text = "21", Value = "21" },
                new SelectListItem { Text = "22", Value = "22" },new SelectListItem { Text = "23", Value = "23" },
                new SelectListItem { Text = "24", Value = "24" },new SelectListItem { Text = "25", Value = "25" },
                new SelectListItem { Text = "26", Value = "26" },new SelectListItem { Text = "27", Value = "27" },
                new SelectListItem { Text = "28", Value = "28" },new SelectListItem { Text = "29", Value = "29" }}, "Value", "Text", 16);

        }

        public static SelectList GetMonthForDropdownlist()
        {
            return new SelectList(new List<SelectListItem>
            { new SelectListItem { Text = "01", Value = "01" },new SelectListItem { Text = "02", Value = "02" },
                new SelectListItem { Text = "03", Value = "03" },new SelectListItem { Text = "04", Value = "04" },
                new SelectListItem { Text = "05", Value = "05" },new SelectListItem { Text = "06", Value = "06" },
                new SelectListItem { Text = "07", Value = "07" },new SelectListItem { Text = "08", Value = "08" },new SelectListItem { Text = "09", Value = "09" },
                new SelectListItem { Text = "10", Value = "10" },new SelectListItem { Text = "11", Value = "11" },new SelectListItem { Text = "12", Value = "12" }}, "Value", "Text", 01);
        }

        public static string GetDaysSeperatedByComma(List<DateTime> dates)
        {
            var result = String.Empty;
            var checkMonth = new List<int>();

            foreach (var date in dates)
            {
                var isCheck = false;
                foreach (var check in checkMonth)
                {
                    if (check == date.Month)
                    {
                        result += ", " + date.ToString("dd");
                        isCheck = true;
                        break;
                    }
                }
                checkMonth.Add(date.Month);
                if (!isCheck)
                {
                    result += ", " + date.ToString("MMM dd");
                }
            }


            return result.Substring(2);
        }
    }
}
