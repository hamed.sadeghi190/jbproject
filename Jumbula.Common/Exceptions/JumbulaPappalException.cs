﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaPappalException : JumbulaPaymentException
    {
        public JumbulaPappalException(Exception e)
            : base(e)
        {
        }
    }
}
