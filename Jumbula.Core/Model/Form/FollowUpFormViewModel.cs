﻿using System;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Model.Form
{
    public class FollowUpFormViewModel : BaseViewModel<OrderItemFollowupForm>
    {
        public string FormName { get; set; }
        public FollowupStatus Status { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public DateTime DateCreated { get; set; }
        public FormType FormType { get; set; }
        public FollowUpFormMode FollowUpFormMode { get; set; }

        public string StrFollowUpFormMode
        {
            get
            {
                return FollowUpFormMode.ToDescription();
            }
        }
    }
}