//Requirements
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    browserify = require('gulp-browserify'),
    minifyCSS = require('gulp-minify-css'),
    csslint = require('gulp-csslint'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify');

//Variable for resources and destination
var homeCssSrc,
    homeJsSrc,
    homeCssDest,
    homeJsDest,
    commonCssSources,
    libraryCssSources,
    commonJsSources,
    libraryJsSources,
    homeCss, homeJs,
    homeMarketsCss, homeMarketsCssSrc, homeMarketsCssDest, homeMarketsJsDest, homeMarketCss,
    homeMarketCssSrc, homeMarketEnterpriseCss, homeMarketEnterprisejs, homeMarketEnterpriseCssSrc, homeMarketEnterpriseJsSrc,
    homePricingCss, homePricingCssSrc, homePricingCssDest,homePricingJs,homePricingJsSrc,homePricingJsDest,
    homeCommonJsSrc, homeCommonJsDest,
    homeContactCss,homeContactCssSrc,homeContactCssDest, homeContactJs, homeContactJsSrc,homeContactJsDest;

//resources

commonCssSources = [
    'dev/common/css/bootstrap.css',
    'dev/common/css/bootstrap-theme.css',
    'dev/common/css/bootstrap-custom.css'
];
libraryCssSources = [
    'dev/lib/css/animate.css',
    'dev/lib/css/jumbula-icons.css',
    'dev/lib/css/owl.carousel.css',
    'dev/lib/css/owl.theme.css',
    'dev/lib/css/owl.transitions.css'
];

commonJsSources = [
    'dev/common/js/jquery-2.1.3.min.js',
    'dev/common/js/is.js',
    'dev/common/js/bootstrap.min.js'
];
libraryJsSources = [
    'dev/lib/js/owl.carousel.min.js',
    'dev/lib/js/svg.js',
    'dev/lib/js/wow.js',
    'dev/lib/js/jquery.lazylinepainter-1.5.1.min.js',
    'dev/lib/js/scroll-to-section.js'
];

//Home project start

homeCommonJsSrc = [].concat(commonJsSources, libraryJsSources);
homeCommonJsDest = '../Scripts/Home/Common/';
gulp.task('commonJsMin', function () {
    gulp.src(homeCommonJsSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('common.js'))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeCommonJsDest))
});


//****Root start
homeCss = [
    'dev/projects/Home/css/special/sharedStyle.css',
    'dev/projects/Home/css/special/studentStyle.css',
    'dev/projects/Home/css/special/Style.css',
    'dev/projects/Home/css/special/Solution.css',
    'dev/projects/Home/css/*.css',
    'dev/projects/Home/css/special/homeStyle.css'

];
homeJs = [
    'dev/projects/Home/js/*.js',
    'dev/projects/Home/js/special/Home.js'
];
homeCssSrc = [].concat(commonCssSources, libraryCssSources, homeCss);
homeJsSrc = [].concat(commonJsSources, libraryJsSources, homeJs);

//destination
homeCssDest = '../Content/Home/';
homeJsDest = '../Scripts/Home/';

gulp.task('homeJsMin', function () {
    gulp.src(homeJsSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('home.js'))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeJsDest))
});

gulp.task('homeCssMin', function () {
    gulp.src(homeCssSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('home.css'))
        .pipe(minifyCSS())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeCssDest))

});

//****Root end
//****Pricing Start
homePricingCss = [
    'dev/projects/Home/css/special/sharedStyle.css',
    'dev/projects/Home/css/special/studentStyle.css',
    'dev/projects/Home/css/special/Style.css',
    'dev/projects/Home/css/special/Solution.css',
    'dev/projects/Home/css/*.css',
    'dev/projects/Home/Pricing/css/Pricing.css'
];
homePricingJs = [
    'dev/projects/Home/Pricing/js/selectFx.js',
    'dev/projects/Home/Pricing/js/_FreeTrialPopUp.js'
];
homePricingCssSrc = [].concat(commonCssSources, libraryCssSources, homePricingCss);
homePricingJsSrc = [].concat(commonJsSources, libraryJsSources, homePricingJs);

//destination
homePricingCssDest = '../Content/Home/Pricing/';
homePricingJsDest = '../Scripts/Home/Pricing/';

gulp.task('homePricingJsMin', function () {
    gulp.src(homePricingJsSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('pricing.js'))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homePricingJsDest))
});


gulp.task('homePricingCssMin', function () {
    gulp.src(homePricingCssSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('pricing.css'))
        .pipe(minifyCSS())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homePricingCssDest))

});

//****Pricing End
//****Contact Start
homeContactCss = [
    'dev/projects/Home/css/special/sharedStyle.css',
    'dev/projects/Home/css/special/studentStyle.css',
    'dev/projects/Home/css/special/Style.css',
    'dev/projects/Home/css/special/homeStyle.css',
    'dev/projects/Home/css/*.css',
    'dev/projects/Home/Contact/css/Contact.css'
];
homeContactJs = [
    'dev/projects/Home/Contact/js/jquery.form.js',
    'dev/projects/Home/Contact/js/jquery.unobtrusive-ajax.min.js',
    'dev/projects/Home/Contact/js/jquery.validate.min.js',
    'dev/projects/Home/Contact/js/jquery.validate.unobtrusive.min.js'
];
homeContactCssSrc = [].concat(commonCssSources, libraryCssSources, homeContactCss);
homeContactJsSrc = [].concat(commonJsSources, libraryJsSources, homeContactJs);
//destination
homeContactCssDest = '../Content/Home/Contact/';
homeContactJsDest = '../Scripts/Home/Contact/';

gulp.task('homeContactJsMin', function () {
    gulp.src(homeContactJsSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('contact.js'))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeContactJsDest))
});


gulp.task('homeContactCssMin', function () {
    gulp.src(homeContactCssSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('contact.css'))
        .pipe(minifyCSS())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeContactCssDest))

});

//****Contact End

//****Markets start
homeMarketsCss = [
    'dev/projects/Home/css/special/sharedStyle.css',
    'dev/projects/Home/css/special/studentStyle.css',
    'dev/projects/Home/css/special/Style.css',


    'dev/projects/Home/Markets/css/special/Markets.css',
    'dev/projects/Home/Markets/css/*.css'
];

homeMarketsCssSrc = [].concat(commonCssSources, libraryCssSources, homeMarketsCss);


//destination
homeMarketsCssDest = '../Content/Home/Markets/';


gulp.task('homeMarketsCssMin', function () {
    gulp.src(homeMarketsCssSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('markets.css'))
        .pipe(minifyCSS())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeMarketsCssDest))

});


//market page start
homeMarketCss = [
    'dev/projects/Home/css/special/sharedStyle.css',
    'dev/projects/Home/css/special/studentStyle.css',
    'dev/projects/Home/css/special/Style.css',


    'dev/projects/Home/Markets/css/Markets-page/Market.css'
];

homeMarketCssSrc = [].concat(commonCssSources, libraryCssSources, homeMarketCss);

gulp.task('homeMarketCssMin', function () {
    gulp.src(homeMarketCssSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('market.css'))
        .pipe(minifyCSS())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeMarketsCssDest))

});
//market page end


//market page start
homeMarketEnterpriseCss = [
    'dev/projects/Home/css/special/sharedStyle.css',
    'dev/projects/Home/css/special/studentStyle.css',
    'dev/projects/Home/css/special/Style.css',
    'dev/projects/Home/Markets/css/special/Markets.css',

    'dev/projects/Home/Markets/css/enterprisePage/component-bg.css',
    'dev/projects/Home/Markets/css/enterprisePage/enterpriselevel.css'
];
homeMarketEnterprisejs = [
    'dev/projects/Home/Markets/js/enterprisePage/raf.js',
    'dev/projects/Home/Markets/js/enterprisePage/bg-bollets.js'

];

homeMarketEnterpriseCssSrc = [].concat(commonCssSources, libraryCssSources, homeMarketEnterpriseCss);
homeMarketEnterpriseJsSrc = [].concat(commonJsSources, libraryJsSources, homeMarketEnterprisejs);
homeMarketsJsDest = '../Scripts/Home/Markets/';
gulp.task('homeMarketEnterpriseJsMin', function () {
    gulp.src(homeMarketEnterpriseJsSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('enterprise.js'))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeMarketsJsDest))
});

gulp.task('homeMarketEnterpriseCssMin', function () {
    gulp.src(homeMarketEnterpriseCssSrc)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('enterprise.css'))
        .pipe(minifyCSS())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(homeMarketsCssDest))

});
//market page end

//****Markets end

//Home project end

//Gulp tasks
//Compress workflow Start


//Compress workflow end


gulp.task('watch', function () {
    gulp.watch(homeCommonJsSrc, ['commonJsMin']);
    gulp.watch(homeCssSrc, ['homeCssMin']);
    gulp.watch(homeJsSrc, ['homeJsMin']);
    gulp.watch(homeMarketsCssSrc, ['homeMarketsCssMin']);
    gulp.watch(homeMarketCss, ['homeMarketCssMin']);
    gulp.watch(homeMarketEnterpriseCss, ['homeMarketEnterpriseCssMin']);
    gulp.watch(homeMarketEnterprisejs, ['homeMarketEnterpriseJsMin']);
    gulp.watch(homePricingCss, ['homePricingCssMin']);
    gulp.watch(homeContactCssSrc, ['homeContactCssMin']);
    gulp.watch(homeContactJsSrc, ['homeContactJsMin']);
    gulp.watch(homePricingJsSrc, ['homePricingJsMin']);


});


gulp.task('default', ['homeJsMin', 'homeCssMin',  'homeMarketsCssMin', 'homeMarketCssMin','homeContactCssMin','homeContactJsMin' ,'homeMarketEnterpriseJsMin', 'homeMarketEnterpriseCssMin','homePricingCssMin','homePricingJsMin','commonJsMin', 'watch']);







