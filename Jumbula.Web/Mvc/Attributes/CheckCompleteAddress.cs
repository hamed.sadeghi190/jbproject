﻿using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Mvc.Attributes
{
    public class CheckCompleteAddress : ValidationAttribute
    {
        private string _validationFor;
        public CheckCompleteAddress(string validationFor)
        {
            _validationFor = validationFor;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string errormessage;
            PostalAddressAutoCompleteViewModel autoCompleteViewModel = null;

            if (value is PostalAddress)
            {
                var postalAddress = value as PostalAddress;
                autoCompleteViewModel = new PostalAddressAutoCompleteViewModel(postalAddress, false);

            }

            else if (value is string)
            {
                PostalAddress postalAddress = new PostalAddress();
                GoogleMap.GetGeo(value as string, ref postalAddress);
                autoCompleteViewModel = new PostalAddressAutoCompleteViewModel(postalAddress, false);
            }

            else if (value is PostalAddressAutoCompleteViewModel)
            {
                autoCompleteViewModel = value as PostalAddressAutoCompleteViewModel;

                // for when user copy an address in this time autocompleteaddress has value but locality is null
                if (!string.IsNullOrEmpty(autoCompleteViewModel.AutoCompletedAddress) && string.IsNullOrEmpty(autoCompleteViewModel.Locality))
                {
                    PostalAddress postalAddress = new PostalAddress();
                    GoogleMap.GetGeo(autoCompleteViewModel.AutoCompletedAddress, ref postalAddress);
                    autoCompleteViewModel = new PostalAddressAutoCompleteViewModel(postalAddress, false);
                }
            }


            if (autoCompleteViewModel == null)
            {
                errormessage = "The address field is required.";
                return new ValidationResult(errormessage);
            }

            if (!autoCompleteViewModel.IsAddressComplete())
            {
                errormessage = Constants.M_AddressNotComplete;
                return new ValidationResult(errormessage, new[] { _validationFor });
            }

            return ValidationResult.Success;
        }
    }
}
