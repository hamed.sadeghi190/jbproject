﻿(function () {
    'use strict';

    angular.module('jbTools').factory("jbToast", ["scopeServise", function (scopeServise) {
        var checkValitity = function (Message, Title, Options) {
            var $scope = scopeServise.get();
            if (is.undefined($scope) || is.null($scope) || typeof $scope == "string" || is.null(Message) || is.undefined(Message)) {
                console.error("Please send parameters with this structure: (Message, Title, Options)");
                return false;
            }
            return true;
        },
        error = function (Message, Title, Options) {
            if (checkValitity(Message, Title, Options) == false) {
                return
            }
            if (is.undefined(Title) || is.null(Title) || is.empty(Title)) {
                Title = "Error";
            }
            var $scope = scopeServise.get();
            if ($scope) {
                $scope.notification.error(Message, Title, Options);
            } else {
                $scope.notification.error(Message, Title, Options);
            }
        },
        info = function (Message, Title, Options) {
            if (checkValitity(Message, Title, Options) == false) {
                return
            }
            if (is.undefined(Title) || is.null(Title) || is.empty(Title)) {
                Title = "Info";
            }
            var $scope = scopeServise.get();
            if ($scope)
                $scope.notification.info(Message, Title, Options);
            else
                $scope.notification.info(Message, Title, Options);
        },
        success = function (Message, Title, Options) {
            if (checkValitity(Message, Title, Options) == false) {
                return
            }
            if (is.undefined(Title) || is.null(Title) || is.empty(Title)) {
                Title = "Success";
            }
            var $scope = scopeServise.get();
            if ($scope)
                $scope.notification.success(Message, Title, Options);
            else
                $scope.notification.success(Message, Title, Options);
        },
        warning = function (Message, Title, Options) {
            if (checkValitity(Message, Title, Options) == false) {
                return
            }
            if (is.undefined(Title) || is.null(Title) || is.empty(Title)) {
                Title = "Warning";
            }
            var $scope = scopeServise.get();
            if ($scope)
                $scope.notification.warning(Message, Title, Options);
            else
                $scope.notification.warning(Message, Title, Options);
        };
        return {
            error: error,
            info: info,
            success: success,
            warning: warning
        }
    }]);
})();