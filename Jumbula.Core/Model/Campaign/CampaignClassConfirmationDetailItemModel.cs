﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Core.Model.Campaign
{
    public class CampaignClassConfirmationDetailItemModel
    {
        public string ParticipantName { get; set; }

        public string Email { get; set; }
    }
}
