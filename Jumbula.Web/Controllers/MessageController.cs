﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using SportsClub.Models;

namespace Jumbula.Web.Controllers
{
    [System.Web.Mvc.Authorize(Roles = "Parent, Admin, Manager, Contributor, Partner")]
    public class MessageController : JbBaseController
    {
        public virtual ActionResult Read(int id)
        {
            var model = new MessageReadViewModel();

            var currentUserId = this.GetCurrentUserId();

            var header = Ioc.MessageBusiness.Get(id);

            model.Id = header.Id;
            model.Messages = header.Messages.OrderBy(o => o.Id).Select(h => new MessageReadItemViewModel { Sender = h.Sender.UserName, Title = h.Text }).ToList();
            model.Title = header.Subject;// message.MessageHeader.Subject;
            model.Sender = header.Messages.First().Sender.UserName;

            return View(model);
        }

        public virtual ActionResult Compose()
        {
            var model = new MessageComposeViewModel();

            //model.CurrentClubName = this.GetCurrentClubName();

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Compose(MessageComposeViewModel model)
        {
            var messageBusiness = Ioc.MessageBusiness;

            if (ModelState.IsValid)
            {
                var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

                var recieverUser = messageBusiness.GetClubMessageReciever(club);

                var recieverUsers = new List<JbUser> { recieverUser };


                var context = GlobalHost.ConnectionManager.GetHubContext<SignalRHub>();


            }

            return JsonNet(new { status = true });
        }

        [HttpPost]
        public virtual ActionResult Send(MessageComposeViewModel model)
        {
            var messageBusiness = Ioc.MessageBusiness;

            if (ModelState.IsValid)
            {
                var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

                var receiverId = 0;

                var result = messageBusiness.Send(model.ChatId, this.GetCurrentUserId(), this.GetCurrentClubId(), null, null, null, null, ref receiverId, string.Empty, model.Text);

                var context = GlobalHost.ConnectionManager.GetHubContext<SignalRHub>();

                var receiverUser = Ioc.UserProfileBusiness.Get(receiverId);

                var lastUserMessage = result.Messages.SelectMany(s => s.UserMessages).Last();

                var audience = messageBusiness.GetAudience(result, club.Id);

                var chats = new List<object>();

                long programId = 0;

                switch (result.Type)
                {
                    case MessageType.Ordinary:
                        break;
                    case MessageType.ScheduleDraftSession:
                        {
                            var chatAttriute = ((MessageScheduleDraftSessionAttribute)(result.Attribute));
                            programId = chatAttriute.ProgramId;

                            chats = new List<object>()
                            {
                                new
                                {
                                    Id = result.Id,
                                    ChatId = result.ChatId,
                                    Subject = result.Subject,
                                    MessageType = result.Type,
                                    ShowActions = result.Type == MessageType.ScheduleDraftSession && chatAttriute.Status == ScheduleDraftSessionStatus.Draft && this.GetCurrentUserRole() != RoleCategory.Partner,
                                    ProgramId = chatAttriute.ProgramId,
                                    MessageStatus =Jumbula.Common.Helper.EnumHelper.ToDescription(chatAttriute.Status),
                                    Summary = GetSenderClubName(result),
                                    Logo = UrlHelpers.GetClubLogoUrl(audience.Domain, audience.Logo),
                                    IsUnread = true,
                                    DateTimeTicks = result.Messages.Max(m => m.CreatedDate).Ticks,
                                    DateTime = result.Messages.Max(m => m.CreatedDate),
                                    Unread = result.Messages.SelectMany(m => m.UserMessages).Count(m => !m.IsRead && m.ReceiverId == receiverId && m.ClubId == audience.Id),

                                    //Text = model.Text,
                                    Messages = new List<object>()
                                    {
                                        new
                                        {
                                            Text = model.Text,
                                            DateTime = result.Messages.Last().CreatedDate,
                                            DateTimeTicks = result.Messages.Last().CreatedDate.Ticks
                                            //RecieverClubId = audience.Id
                                        }
                                    }
                                }
                            };
                        }
                        break;
                    case MessageType.ESignature:
                        {
                            var chatAttriute = ((MessageEsignatureAttribute)(result.Attribute));

                            chats = new List<object>()
                            {
                                new
                                {
                                    Id = result.Id,
                                    ChatId = result.ChatId,
                                    Subject = result.Subject,
                                    MessageType = result.Type,
                                    ShowActions = false,
                                    MessageStatus = Jumbula.Common.Helper.EnumHelper.ToDescription(chatAttriute.Status),
                                    Summary = GetSenderClubName(result),
                                    Logo = UrlHelpers.GetClubLogoUrl(audience.Domain, audience.Logo),
                                    IsUnread = true,
                                    DateTimeTicks = result.Messages.Max(m => m.CreatedDate).Ticks,
                                    DateTime = result.Messages.Max(m => m.CreatedDate),
                                    Unread = result.Messages.SelectMany(m => m.UserMessages).Count(m => !m.IsRead && m.ReceiverId == receiverId && m.ClubId == audience.Id),

                                    //Text = model.Text,
                                    Messages = new List<object>()
                                    {
                                        new
                                        {
                                            Text = model.Text,
                                            DateTime = result.Messages.Last().CreatedDate,
                                            DateTimeTicks = result.Messages.Last().CreatedDate.Ticks
                                            //RecieverClubId = audience.Id
                                        }
                                    }
                                }
                            };
                        }
                        break;
                    default:
                        break;
                }

                var messageResponse = new
                {
                    Unread = Ioc.MessageBusiness.GetUnreadCount(receiverId, lastUserMessage.ClubId),
                    RefreshCurrentMessage = false,
                    Chats = chats,
                };

                context.Clients.User(receiverUser.UserName).refreshSignal(messageResponse);

                var program = Ioc.ProgramBusiness.Get(programId);

                EmailService.Get(this.ControllerContext).SendMessagingEmail(receiverUser.UserName, this.GetCurrentUserName(), model.Text, club.Domain,  result.ChatId, club.Logo, program.Name, club.Name, audience.Domain, club.IsPartner);
            }

            return JsonNet(new { status = true });
        }

        [HttpPost]
        public virtual ActionResult Reply(MessageReplyViewModel model)
        {

            var receiver = Ioc.UserProfileBusiness.Get(model.Receiver);

            Ioc.MessageBusiness.Reply(model.Id, this.GetCurrentClubId(), model.Text, this.GetCurrentUserId(), receiver.Id);

            return RedirectToAction("Read", new { id = model.Id });
        }

        [HttpGet]
        public virtual ActionResult GetInboxContacts(int start, int take)
        {
            var messageBusiness = Ioc.MessageBusiness;

            int total = 0;

            var currentClubId = this.GetCurrentClubId();
            var currentUserId = this.GetCurrentUserId();

            var messages = Ioc.MessageBusiness.GetInboxUserMessages(this.GetCurrentUserId(), currentClubId, start, take, ref total).Select(s =>
               new MessageItemViewModel
               {
                   ChatId = s.ChatId,
                   Id = s.Id,
                   Subject = s.Subject,
                   Audience = messageBusiness.GetAudience(s, currentClubId).Name,
                   Summary = s.Messages.First().Club.Name,
                   Sender = s.Messages.First().Sender.UserName,
                   Logo = UrlHelpers.GetClubLogoUrl(messageBusiness.GetAudience(s, currentClubId).Domain, messageBusiness.GetAudience(s, currentClubId).Logo),
                   IsUnread = s.Messages.Any(m => m.UserMessages.Any(u => !u.IsRead && u.ReceiverId == currentUserId && u.ClubId == currentClubId)),
                   DateTime = s.Messages.Max(m => m.CreatedDate),
                   DateTimeTicks = s.Messages.Max(m => m.CreatedDate).Ticks,
                   Unread = s.Messages.SelectMany(m => m.UserMessages).Count(m => !m.IsRead && m.ReceiverId == currentUserId && m.ClubId == currentClubId)

               })
               .ToList();

            var model = new
            {
                Messages = messages,
                Total = total
            };

            return JsonNet(model);
        }

        [HttpGet]
        public virtual ActionResult GetChat(string chatId)
        {

            var messageBusiness = Ioc.MessageBusiness;

            var currentUserId = this.GetCurrentUserId();
            var currentClubId = this.GetCurrentClubId();

            var chat = Ioc.MessageBusiness.GetChat(chatId, currentUserId);
            var currentClub = Ioc.ClubBusiness.Get(currentClubId);

            var model = new ChatViewModel(chat, currentUserId, currentClub);

            return JsonNet(model);
        }

        [HttpGet]
        //[JbAuthorize(JbAction.Program_RespondInvitation)]
        public virtual JsonNetResult GetMessageAction(long? programId)
        {
            var program = Ioc.ProgramBusiness.Get(programId.HasValue ? programId.Value : 0);
            var programSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted);
            var attribute = programSchedule.Attributes as ScheduleAttribute;

            var model = new MessageActionViewModel()
            {
                ProgramId = programId.HasValue ? programId.Value : 0,
                ProgramName = program != null ? program.Name : string.Empty,
                WeekDaysMode = attribute.WeekDaysMode,
                SelectedWeekDays = attribute.Days.Select(d => d.DayOfWeek.ToString()).ToList(),
                SelectedMinGrade = programSchedule.AttendeeRestriction.MinGrade,
                SelectedMaxGrade = programSchedule.AttendeeRestriction.MaxGrade,
            };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveMessageAction(MessageActionViewModel model, string chatId)
        {
            var res = new OperationStatus();
            var respondType = model.RespondType;
            ScheduleDraftSessionStatus? scheduleDraftSessionStatus = null;

            Program program = null;

            var respond = new ProgramRespondToInvitation()
            {
                ProgramId = model.ProgramId,
            };

            var currentClub = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

            switch (respondType)
            {
                case MessageActionType.Accept:
                    respond.Respond = new AcceptRespond()
                    {
                        //AvailableAnyDay = model.AvailableAnyDay,
                        FirstDayPreference = model.FirstDayPreference.HasValue ? model.FirstDayPreference.Value : (DayOfWeek)(-1),
                        SecondDayPreference = model.SecondDayPreference.HasValue ? model.SecondDayPreference.Value : (DayOfWeek)(-1),
                    };

                    respond.RespondType = ProgramRespondType.Accept;
                    scheduleDraftSessionStatus = ScheduleDraftSessionStatus.Accepted;

                    break;
                case MessageActionType.ModifyRequest:
                    respond.Respond = new BaseRespond()
                    {
                        Note = model.Note
                    };

                    respond.RespondType = ProgramRespondType.Modify;
                    scheduleDraftSessionStatus = ScheduleDraftSessionStatus.Draft;
                    break;
                case MessageActionType.Decline:
                    respond.Respond = new DeclineRespond()
                    {
                        DeclineResaon = model.DeclineResaon,
                        Note = model.Note
                    };

                    respond.RespondType = ProgramRespondType.Decline;
                    scheduleDraftSessionStatus = ScheduleDraftSessionStatus.Declined;
                    break;
                case MessageActionType.Modify:
                    {
                        program = Ioc.ProgramBusiness.Get(model.ProgramId);
                        var programSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted);

                        var scheduleAttribute = programSchedule.Attributes as ScheduleAttribute;

                        switch (model.WeekDaysMode)
                        {
                            case WeekDaysMode.Any:
                                break;
                            case WeekDaysMode.SpecialDay:
                                {
                                    var startTime = scheduleAttribute.Days.First().StartTime;
                                    var endTime = scheduleAttribute.Days.First().EndTime;

                                    scheduleAttribute.Days = new List<ProgramScheduleDay>();

                                    for (int i = 0; i < model.SelectedWeekDays.Count; i++)
                                    {
                                        scheduleAttribute.Days.Add(new ProgramScheduleDay
                                        {
                                            DayOfWeek = Jumbula.Common.Helper.EnumHelper.ParseEnum<DayOfWeek>(model.SelectedWeekDays[i]),
                                            StartTime = startTime,
                                            EndTime = endTime
                                        });
                                    }
                                }
                                break;
                            default:
                                break;
                        }

                        scheduleDraftSessionStatus = ScheduleDraftSessionStatus.Modified;

                        programSchedule.AttendeeRestriction.MinGrade = model.SelectedMinGrade;
                        programSchedule.AttendeeRestriction.MaxGrade = model.SelectedMaxGrade;

                        scheduleAttribute.WeekDaysMode = model.WeekDaysMode;

                        programSchedule.AttributesSerialized = JsonConvert.SerializeObject(scheduleAttribute);

                        res = Ioc.ProgramBusiness.Update(program);
                    }
                    break;
            }


            if (model.RespondType != MessageActionType.Modify)
            {
                res = Ioc.ProgramBusiness.InsertRespond(respond);
            }

            var messageBusiness = Ioc.MessageBusiness;

            if (res.Status)
            {
                var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

                int receiverId = 0;

                var body = GetMessageActionBodyText(model, program);

                var attribute = new MessageScheduleDraftSessionAttribute()
                {
                    Status = scheduleDraftSessionStatus.Value,//  model.RespondType == ProgramRespondType.Accept ? ScheduleDraftSessionStatus.Accepted : (model.RespondType == ProgramRespondType.Decline ? ScheduleDraftSessionStatus.Declined : ScheduleDraftSessionStatus.Draft),
                    ProgramId = model.ProgramId,
                };

                var attributeSerialized = JsonConvert.SerializeObject(attribute);

                var result = Ioc.MessageBusiness.Send(chatId, this.GetCurrentUserId(), this.GetCurrentClubId(), null, null, null, null, ref receiverId, string.Empty, body, MessageType.ScheduleDraftSession, attributeSerialized);

                var context = GlobalHost.ConnectionManager.GetHubContext<SignalRHub>();

                var receiverUser = Ioc.UserProfileBusiness.Get(receiverId);

                var chatAttriute = ((MessageScheduleDraftSessionAttribute)(result.Attribute));

                var lastUserMessage = result.Messages.SelectMany(s => s.UserMessages).Last();
                var messageResponse = new
                {
                    Unread = Ioc.MessageBusiness.GetUnreadCount(receiverId, lastUserMessage.ClubId),
                    RefreshCurrentMessage = true,
                    Chats = new List<object>()
                    {
                        new
                        {
                            Id = result.Id,
                            ChatId = result.ChatId,
                            Subject = result.Subject,
                            MessageType = result.Type,
                            ShowActions = result.Type == MessageType.ScheduleDraftSession && chatAttriute.Status == ScheduleDraftSessionStatus.Draft && this.GetCurrentUserRole() != RoleCategory.Partner,
                            ProgramId = chatAttriute.ProgramId,
                            MessageStatus = Jumbula.Common.Helper.EnumHelper.ToDescription(chatAttriute.Status),
                            Summary = GetSenderClubName(result),
                            IsUnread = true,
                            DateTimeTicks = result.Messages.Max(m => m.CreatedDate).Ticks,
                            DateTime = result.Messages.Max(m => m.CreatedDate),
                            Unread = result.Messages.SelectMany(m => m.UserMessages).Count(m => !m.IsRead && m.ReceiverId == receiverId && m.ClubId == messageBusiness.GetAudience(result, club.Id).Id),
                            Logo = UrlHelpers.GetClubLogoUrl(messageBusiness.GetAudience(result, club.Id).Domain, messageBusiness.GetAudience(result, club.Id).Logo),
                            Messages = new List<object>()
                            {
                                new
                                {
                                    Text = body,
                                    DateTime = result.Messages.Last().CreatedDate,
                                    DateTimeTicks = result.Messages.Last().CreatedDate.Ticks
                                }
                            }
                        }
                    },

                };

                context.Clients.User(receiverUser.UserName).refreshSignal(messageResponse);

                if(program == null)
                   program = Ioc.ProgramBusiness.Get(model.ProgramId);

                var audience = messageBusiness.GetAudience(result, club.Id);

                EmailService.Get(this.ControllerContext).SendMessagingEmail(receiverUser.UserName, this.GetCurrentUserName(), body, club.Domain, result.ChatId, club.Logo, program.Name, club.Name, audience.Domain, club.IsPartner);
            }

            var message = string.Empty;

            if (model.RespondType == MessageActionType.Modify)
            {
                if (this.GetCurrentUserRole() == RoleCategory.Partner)
                {
                    message = "Draft session has been modified.";
                }
                else
                {
                    message = "Program changed successfully.";
                }
            }
            else
            {
                message = "Response is successfully submitted.";
            }

            return JsonNet(new { Status = true, Message = message });
        }

        private string GetSenderClubName(MessageHeader messageHeader)
        {
            var result = string.Empty;

            result = messageHeader.Messages.Last().Club.Name;

            return result;
        }

        private string GetMessageActionBodyText(MessageActionViewModel model, Program program)
        {
            var result = string.Empty;
            var messageBusiness = Ioc.MessageBusiness;

            switch (model.RespondType)
            {
                case MessageActionType.Accept:
                    {
                        result = "Provider has accepted this class.";

                        result += "<br/>";

                        if (model.AvailableAnyDay)
                        {
                            result += " Availability: Any day ";
                        }
                        else
                        {
                            result += string.Format(" Availability: {0} {1}", model.FirstDayPreference != null ? Jumbula.Common.Helper.EnumHelper.ToDescription(model.FirstDayPreference) : string.Empty, model.SecondDayPreference != null ? Jumbula.Common.Helper.EnumHelper.ToDescription(model.SecondDayPreference) : string.Empty);
                        }
                    }
                    break;
                case MessageActionType.ModifyRequest:
                    {
                        result += "Provider wants to modify this class.";

                        result += "<br/>";

                        result += string.Format(" Note: {0}", model.Note);
                    }
                    break;
                case MessageActionType.Modify:
                    {
                        result += "Draft session has been modified.";

                        result += "<br/>";

                        result += string.Format("Grades: {0} - {1}", Jumbula.Common.Helper.EnumHelper.ToDescription(model.SelectedMinGrade), Jumbula.Common.Helper.EnumHelper.ToDescription(model.SelectedMaxGrade));

                        result += " <br/> ";

                        if (model.WeekDaysMode == WeekDaysMode.Any)
                        {
                            result += "Weekdays: any day";
                        }
                        else
                        {
                            result += "Weekdays: " + string.Join(", ", model.WeekDays.Where(w => model.SelectedWeekDays.Contains(w.Value)).Select(d => d.Text));
                        }
                    }
                    break;
                case MessageActionType.Decline:
                    {
                        result = "Provider has declined this class.";

                        result += "<br/>";

                        if (model.DeclineResaon == PorgramRespondDeclineResaon.Other)
                        {
                            result += " Reason: " + model.Note;
                        }
                        else
                        {
                            result += " Reason: " + (EnumHelper.ToDescription(model.DeclineResaon) != "0"? EnumHelper.ToDescription(model.DeclineResaon): string.Empty);
                        }
                    }
                    break;
                default:
                    break;
            }

            return result;
        }

        public virtual ActionResult GetMessagesSummary()
        {
            dynamic model = new ExpandoObject();

            model = new
            {
                Unread = Ioc.MessageBusiness.GetUnreadCount(this.GetCurrentUserId(), this.GetCurrentClubId()),
            };

            return JsonNet(model);
        }

        public virtual ActionResult GetRecentMessages()
        {
            var currentClubId = this.GetCurrentClubId();
            var currentUserId = this.GetCurrentUserId();

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var messageBusiness = Ioc.MessageBusiness;
            var programBusiness = Ioc.ProgramBusiness;

            var query = Ioc.MessageBusiness.GetRecentMessages(currentUserId, currentClubId);

            var messages = query.ToList().Where(q => (q.Attribute as MessageScheduleDraftSessionAttribute).Status == ScheduleDraftSessionStatus.Draft).OrderByDescending(item => item.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList();

            var model = new List<RecentMessageViewModel>();

            foreach (var item in messages)
            {
                var messageItem = new RecentMessageViewModel();

                var messageAttribute = item.Attribute as MessageScheduleDraftSessionAttribute;
                var programSchedule = item.Program.ProgramSchedules.Last(s => !s.IsDeleted);
                var scheduleAttribute = programSchedule.Attributes as ScheduleAttribute;

                messageItem.ClassName = item.Program.Name;
                messageItem.SchoolSeasonName = item.Program.Season.Title;
                messageItem.Day = scheduleAttribute.WeekDaysMode == WeekDaysMode.Any ? "Any day" : string.Join(", ", programBusiness.GetClassDays(item.Program).ToList().Select(d => Jumbula.Common.Helper.EnumHelper.ToDescription(d.DayOfWeek)).ToList());
                messageItem.Grades = programBusiness.GenerateGradeInfoLabel(item.Program).Trim(')').Trim('(');
                messageItem.ChatId = item.ChatId;
                messageItem.IsUnread = item.Messages.Any(m => m.UserMessages.Any(u => !u.IsRead && u.ReceiverId == currentUserId && u.ClubId == currentClubId));

                model.Add(messageItem);
            }

            return JsonNet(new { DataSource = model, TotalCount = query.Count() });
        }

        public virtual ActionResult AddProgramIdForOldMessages()
        {
            var result = string.Empty;

            var res = Ioc.MessageBusiness.AddProgramIdForOldMessages();

            result = res.ToString();

            return Content(result);
        }
    }
}