﻿jQuery(function ($) {
    $('.mask_date').mask('99/99/9999', { placeholder: "MM/DD/YYYY" });
    $(".mask_phone-us").mask("999-999-9999", { placeholder: "e.g. 555-666-7777" });
    $(".mask_phone-international").mask("000000000000", { placeholder: "e.g. 123456789" });
});