﻿

namespace Jumbula.Common.Enums
{
    public enum OptionalRegDataCategories
    {
        School,
        SkillLevel,
        ParentGuardian,
        EmergencyContact,
        Insurance,
        Doctor
    }
}
