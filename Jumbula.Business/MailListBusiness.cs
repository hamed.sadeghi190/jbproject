﻿using Jb.Framework.Common;
using System;
using System.Data.Entity;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using MailList = Jumbula.Core.Domain.MailList;

namespace Jumbula.Business
{
    public class MailListBusiness :  IMailListBusiness
    {
        private readonly IRepository<MailList> _mailListRepository;
        private readonly IEntitiesContext _dataContext;

        public MailListBusiness(IRepository<MailList> mailListRepository, IEntitiesContext dataContext)
        {
            _mailListRepository = mailListRepository;
            _dataContext = dataContext;
        }

        public MailList Get(int id)
        {
            return _dataContext.Set<MailList>().Single(m => m.Id == id);
        }
        public OperationStatus CreateList(MailList newMailList)
        {
            try
            {
                return _mailListRepository.Create(newMailList);
            }
            catch (Exception e)
            {
                return new OperationStatus { Status = false, Message = e.Message };
            }
        }

        public IQueryable<MailList> GetAllLists(int clubId)
        {
            try
            {
                return _mailListRepository.GetList().Where(m => m.Club_Id == clubId).OrderByDescending(m => m.CreateDate);
            }
            catch
            {
                throw;
            }
        }

        public OperationStatus DeleteMailList(int listId)
        {
            try
            {
                var list = Get(listId);

                _dataContext.Entry<MailList>(list).State = EntityState.Deleted;
                _mailListRepository.Save();

                return new OperationStatus { Status = true };
            }
            catch (Exception)
            {
                return new OperationStatus { Status = false };
            }
        }

        public IQueryable<MailListContact> GetListRecipients(int listId)
        {
            return Get(listId).ListContact.AsQueryable();
        }

        public OperationStatus DeleteMailListRecipient(int listId, int recipientId)
        {

            try
            {
                var listDb = Get(listId);
                var recipient = listDb.ListContact.Single(r => r.Id == recipientId);

                _dataContext.Entry<MailListContact>(recipient).State = EntityState.Deleted;

                _mailListRepository.Save();

                return new OperationStatus { Status = true };
            }
            catch (Exception e)
            {
                return new OperationStatus { Status = false, Message = e.Message };
            }

        }

        public OperationStatus UpdateMailList(MailList newList)
        {
            var list = Get(newList.Id);

            list.ListContact.ToList<MailListContact>().ForEach(x => _dataContext.Entry(x).State = EntityState.Deleted);

            list.ListName = newList.ListName;
            list.ListType = newList.ListType;
            list.QueryString = newList.QueryString;
            list.CreateDate = newList.CreateDate;
            list.Club_Id = newList.Club_Id;
            list.ListContact = newList.ListContact;

            return _mailListRepository.Save(); 

        }
    }
}
