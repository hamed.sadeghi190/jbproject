﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsPaymentPlanController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', function ($scope, $http, jbToast, $stateParams, $state) {
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSettingsRegistration", order: 4, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;


        $http.get('Dashboard/Season/GetSeasonPaymentPlan?seasonDomain=' + $stateParams.seasonDomain).success(function (data, status, headers, config) {

            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });



        $scope.save = function () {

            $http.post('Dashboard/Season/SaveSeasonPaymentPlan', { model: $scope.Model, seasonDomain: $stateParams.seasonDomain })
                .success(function (data, status, headers, config) {

                    if (data.Status == true) {
                        $scope.errors = null;
                        jbToast.success("payment plans updated successfully.", "");
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                })
        }
    }])

})();