﻿using System.ComponentModel.DataAnnotations;

namespace Jumbula.Core.Domain
{
    public class PostalAddress
    {
        public int Id { get; set; }

        public double Lat { get; set; }

        public double Lng { get; set; }

        [StringLength(64)]
        public string StreetNumber { get; set; }

        [StringLength(128)]
        public string Route { get; set; }

        [StringLength(128)]
        public string Street { get; set; }

        [StringLength(128)]
        public string Street2 { get; set; }

        [StringLength(64)]
        public string City { get; set; }
        [StringLength(64)]
        public string County { get; set; }

        [StringLength(64)]
        public string State { get; set; }

        [StringLength(64)]
        public string Country { get; set; }

        [StringLength(64)]
        public string Zip { get; set; }

        [StringLength(256)]
        public string AutoCompletedAddress { get; set; }

        [StringLength(256)]
        public string Address { get; set; }

        public Jumbula.Common.Enums.TimeZone? TimeZone { get; set; }

        [StringLength(256)]
        public string AdditionalInfo { get; set; }

        public PostalAddress()
        {
        }

        public PostalAddress(string streetNumber, string street, string city, string state, string country, string zip)
        {
            StreetNumber = streetNumber;
            Street = street;
            City = city;
            State = state;
            Country = country;
            Zip = zip;

            Address = string.Format("{0}, {1}, {2}, {3} {4}", street, city, state, country, zip);
        }
    }
}
