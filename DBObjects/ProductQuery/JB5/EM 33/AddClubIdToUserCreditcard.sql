
ALTER TABLE [dbo].[UserCreditCards] add [ClubId] [int] NULL


ALTER TABLE [dbo].[UserCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserCreditCards_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
GO
