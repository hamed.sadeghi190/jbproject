﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using System.Collections.Generic;
using System.Net.Mail;

namespace Jumbula.Business.EmailBuilder
{
    public class RequestDemoEmailBuilder : BaseEmailBuilder<RequestDemoEmailParameterModel>
    {
        #region Constants
        private const string SubjectPattern = "Request demo from {0}";
        private const string RequestDemoEmailRecivier = "nacho@jumbula.com";
        #endregion

        #region Fields
        private RequestDemoEmailParameterModel _parameters;
        #endregion

        #region Properties
        public override EmailCategory Category => EmailCategory.RequestDemo;

        public override string TemplateName => "RequestDemo";

        public override EmailCallerType CallerType => EmailCallerType.User;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.Normal;
        #endregion

        #region Public Methods

        public override void Initiate(RequestDemoEmailParameterModel parameters)
        {
            _parameters = parameters;
        }

        public override void InitiatePreview(RequestDemoEmailParameterModel parameters)
        {
        }

        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            return new RequestDemoEmailBodyModel
            {
                FirstName = _parameters.FirstName,
                LastName = _parameters.LastName,
                Message = _parameters.Message,
                Email = _parameters.Email,
                Organization = _parameters.OrganizationName,
                PhoneNumber = _parameters.PhoneNumber
            };
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            return new MailAddress(_parameters.Email);
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return _parameters.Email;
        }

        public override string GetReplyTo(string subCategory = null)
        {
            return _parameters.Email;
        }

        public override string GetSubject(string subCategory = null)
        {
            return string.Format(SubjectPattern, _parameters.OrganizationName);
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            return new List<MailAddress> { GetSupportRecipient() };
        }

        public override IEnumerable<MailAddress> GetCCs(string subCategory = null)
        {
            return new List<MailAddress> { new MailAddress(RequestDemoEmailRecivier) };
        }

        public override List<CommunicationHistory> GetHistories()
        {
            return new List<CommunicationHistory>();
        }

        #endregion
    }
}
