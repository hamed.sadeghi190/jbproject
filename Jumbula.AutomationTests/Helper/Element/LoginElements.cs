﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    public class LoginElements : BaseElements
    {
        public LoginElements(IWebDriver driver)
            :base(driver)
        {

        }

        public IWebElement UsernameTextbox
        {
            get
            {
                return Driver.FindElement(By.Name("UserNameLogin")); 
            }
        }

        public IWebElement PasswordTextbox
        {
            get
            {
                return Driver.FindElement(By.Name("PasswordLogin")); 
            }
        }

        public IWebElement LoginButton
        {
            get
            {
                IList<IWebElement> Buttons = Driver.FindElements(By.TagName("button"));
                return Buttons.First(btn => btn.Text == "LOG IN");
            }
        }
        public IWebElement UsernameError
        {
            get
            {
                return Driver.FindElement(By.XPath("//span[@data-valmsg-for='UserNameLogin']"));
            }
        }
    }
}




