﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class EmailTemplateModel
    {
        public EmailTemplateModel()
        {
            Locations = new List<ClubAddressViewModel>();
        }

        public string Name { get; set; }

        [Display(Name = "New template name")]
        public string NewName { get; set; }

        public bool ChangeTemplate { get; set; }

        public bool HasHeader { get; set; }

        public bool HasLogo { get; set; }

        public bool HasFooter { get; set; }

        public bool HasMap { get; set; }
        public bool IsTest { get; set; }
        public bool HasHeaderText { get; set; }

        public bool HasFooterText { get; set; }

        public string HeaderText { get; set; }

        public string HeaderBackground { get; set; }

        public string BodyBackColor { get; set; } = "#fff";

        public string Body { get; set; }

        public string FooterText { get; set; }

        public string FooterBackground { get; set; }

        public string ClubLogo { get; set; }

        public int MapZoom { get; set; }

        public string MapLocation { get; set; }

        public List<ClubAddressViewModel> Locations { get; set; }

        // use For Registration Email Templates

        public string CongratulationMessage { get; set; }

        public string CustomMessage { get; set; }

        public bool IsRegisterationEmail { get; set; }

        public bool HasMoreQuestions { get; set; }

        public string MoreQuestionsPhone { get; set; }

        public string MoreQuestionsEmail { get; set; }
        public string ClubDomain { get; set; }

        public bool UseProgramLocation { get; set; }

        [NotMapped]
        public EmailBodyGenerationMode PreviewMode { get; set; }
    }

    public class ClubAddressViewModel
    {
        public string Address { get; set; }

        public string AddressPoint { get; set; }
    }
}
