﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IJbFileBusiness
    {
        OperationStatus Create(JbFile file);
        IQueryable<JbFile> GetList();
        JbFile Get(int FileId);
    }
}
