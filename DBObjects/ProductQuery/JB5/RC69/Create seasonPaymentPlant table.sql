

CREATE TABLE [dbo].[SeasonPaymentPlans](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SeasonId] [bigint] NOT NULL,
	[PaymentPlanId] [bigint] NOT NULL,
	[IsDeleted] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.SeasonPaymentPlans] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SeasonPaymentPlans]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SeasonPaymentPlans_dbo.PaymentPlans_PaymentPlanId] FOREIGN KEY([PaymentPlanId])
REFERENCES [dbo].[PaymentPlans] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[SeasonPaymentPlans] CHECK CONSTRAINT [FK_dbo.SeasonPaymentPlans_dbo.PaymentPlans_PaymentPlanId]
GO

ALTER TABLE [dbo].[SeasonPaymentPlans]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SeasonPaymentPlans_dbo.Seasons_SeasonId] FOREIGN KEY([SeasonId])
REFERENCES [dbo].[Seasons] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[SeasonPaymentPlans] CHECK CONSTRAINT [FK_dbo.SeasonPaymentPlans_dbo.Seasons_SeasonId]
GO


