﻿
namespace SportsClub.Areas.Dashboard.Models
{
    public class InstallmentsViewModel
    {
    
        public string User { get; set; }
        public long Id { get; set; }
        public string ClubDomain { get; set; }
        public string Season { get; set; }
        public string Program { get; set; }
        public string ScheduleAndDate { get; set; }
        
        public string ParticipantName { get; set; }
        public string OrderItemUrl { get; set; }
        public string TransactionId { get; set; }
        public string OrderMode { get; set; }
        public bool SendReminder { get; set; }
        public string Parent { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string DueDate { get; set; }
        public string Expiry { get; set; }
        public string PaidDate { get; set; }
        public string Status { get; set; }
        public string LastDigit { get; set; }
        public string ConfirmationId { get; set; }
        public string Amount { get; set; }
        public string AmountPaid { get; set; }
        public string Balance { get; set; }
        public string Stopped { get; set; }
        public string TransactionDate { get; set; }
        public string Description { get; set; }
        public string SeasonDomain { get; set; }
        public long OrderItemId { get; set; }
        public int ClubId { get; set; }

    }

 
}