﻿using Jumbula.AutomationTests.Helper.Element;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
  
    public class BaseTest<T> : BaseMain where T : BaseElements 
    {
        public BaseTest(IWebDriver webDriver)
            :base(webDriver)
        {

        }


        public T Elements
        {
            get
            {
                return new ElementProvider<T>().Get(WebDriver);
            }

        }

        //public IWebDriver WebDriver
        //{
        //    get
        //    {
        //        if (_webDriver == null)
        //        {
        //            _webDriver = GetWebDriver(Browser);
        //        }

        //        return _webDriver;
        //    }
        //    set
        //    {
        //        _webDriver = value;
        //    }
           
        //}

        //public string BaseUrl
        //{
        //    get
        //    {
        //        return _baseUrl;
        //    }
        //    set
        //    {
        //        _baseUrl = value;
        //    }
        //}

        //public Browser Browser
        //{
        //    get
        //    {
        //        return _browser;
        //    }
        //    set
        //    {
        //        _browser = value;
        //    }
        //}

        //protected void ChangeBrowser(Browser browser)
        //{
        //    _browser = browser;
        //    _webDriver = GetWebDriver(browser);
        //}

        //public IWebDriver GetWebDriver(Browser browser)
        //{
        //    switch (browser)
        //    {
        //        case Browser.Chrome:
        //            {
        //                return new ChromeDriver();
        //            }
        //        case Browser.FireFox:
        //            {
        //                return new FirefoxDriver();
        //            }
        //        case Browser.IE:
        //            {
        //                return new InternetExplorerDriver();
        //            }
        //        default:
        //            break;
        //    }

        //    return null;
        //}
    }

   
}
