﻿namespace Jumbula.Common.Constants
{
    public static partial class Constants
    {
        public static class SchoolTypeConstants
        {
            public const string Partner = "Partner";
            public const string School = "School";
            public const string Provider = "Provider";
            public const string SchoolDistrict = "School district";
        }
    }
}
