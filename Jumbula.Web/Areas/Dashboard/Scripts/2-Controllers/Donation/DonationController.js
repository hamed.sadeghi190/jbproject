﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('DonationController', ['$scope', '$http', '$state', 'jbToast', function ($scope, $http, $state, jbToast) {
        window.app.globalObjects.initializeView();
        window.app.globalObjects.pageClass = 'seasons-view step1';
          
        $scope.searchTerm = '';
        $scope.donationDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: window.location.origin + '/Dashboard/Donation/GetList/',
                    cache: false
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            term: $scope.searchTerm,
                            skip: data.skip,
                            take: data.take,
                            page: data.page,
                            pageSize: data.pageSize,
                            sort: data.sort
                        }
                    }
                }
            },
            pageSize: 10,
            batch: true,
            serverPaging: true,
            serverSorting: true,
            schema: {
                data: "data",
                total: "total"
            },
            requestStart: function (e) {
                kendo.ui.progress($('[kendo-grid]'), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($('[kendo-grid]'), false);
            }
        });

        $scope.donationGridOptions = {
            dataSource: $scope.donationDataSource,
            sortable: true,
            serverPaging: true,
            columns: [{
                field: "Attendee",
                title: "Participant",
                width: "150px",
                template: "<a ui_sref='DonationDiplay({orderItemId:  #=Id#})'  class='block'> #= Attendee # </a>"
            }, {
                field: "OrderDate",
                title: "Date",
                width: "145px",              
            }, {
                field: "PaymentStatus",
                title: "Status",
                width: "120px",
                template: "{{'#=PaymentStatus#'}}"
            }, {
                field: "ConfirmationId",
                title: "Confirmation",
                width: "120px"               
            }, {
                field: "AmountStr",
                title: 'Amount',
                width: '70px'
            }],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
        };
    }])


    angular.module('dashboardApp').controller('DonationDisplayController', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', function ($scope, $http, jbToast, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;        
        $scope.MC.stateParams = $stateParams;        

        $scope.Model = {};
        $scope.Model.Id = $stateParams.orderItemId;

        $scope.loadModel = function (editable) {
            $http.get(window.location.origin + '/Dashboard/Donation/GetDonation/?orderItemId=' + $scope.Model.Id).success(function (data, status, headers, config) {
                $scope.Model = data;
                $scope.Model.Editable = editable;
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }  

        $scope.loadModel(false);

    }]);
})();