﻿


(function () {
    'use strict';

    angular.module('dashboardApp').controller('schoolProgramsListCheckInOutController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            var timer = false;

            $scope.Model = {};


            $scope.Model.schoolId = [];
            $scope.Model.seasonId = [];

            $http.get(baseUrl + '/GetAllSchool') //GetProvider delete
            .success(function (data, status, headers, config) {

                $scope.AllSchools = data;


            });    
            
            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $scope.$watch('AllSeasons', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });
            
            $scope.fillSeasonData = function (schoolId) {
                $http.get(baseUrl + '/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                    $scope.AllSeasons = data;
                    $scope.Model.schoolId = schoolId;
                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }

            $scope.fillProgramsData = function (seasonId) {
                $http.get(baseUrl + '/GetAllPrograms', { params: { seasonId: seasonId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                    $scope.AllPrograms = data;
             
                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }

         
                $http.get(baseUrl + '/GetDate').success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
               
                    $scope.Model.DateProgram = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
         
            var checkValidation = function (schoolId, seasonId) {
                if (is.undefined(schoolId) || schoolId <= 0) {
                    jbToast.error("Please select a school.");
                    return false;
                }
                else if (is.undefined(seasonId) || seasonId <= 0) {
                    jbToast.error("Please select a season.");
                    return false;
                }
                if ($scope.Model.DateProgram=="") {
                    jbToast.error("Please select a session start date.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {
                if (checkValidation($scope.Model.schoolId, $scope.Model.SeasonId)) { 
                    $http.get(baseUrl + '/ProgramsListSignOutSheetReport', { params: { seasonId: $scope.Model.SeasonId, schoolId: $scope.Model.schoolId, programId: $scope.Model.ProgramId, date: $scope.Model.DateProgram } }).success(function (data, status, headers, config) {

                        $scope.ModelPrograms = data;

                    }).error(function (data, status, headers, config) {
                        jbToast.error("error " + data);
                    });
                }
            }

   
            $scope.saveToPdf = function () {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = '/Dashboard/PortalReport/GenerateSignOutSheetPdf?seasonId=' + $scope.Model.SeasonId + '&schoolId=' + $scope.Model.schoolId + '&programId=' + $scope.Model.ProgramId + '&date='+ $scope.Model.DateProgram ;
                a.download = "ReportPrograms.Pdf";
                a.click();
            };

            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = '/Dashboard/PortalReport/GenerateSignOutSheetExcel?seasonId=' + $scope.Model.SeasonId + '&schoolId=' + $scope.Model.schoolId + '&programId=' + $scope.Model.ProgramId + '&date=' + $scope.Model.DateProgram;
                a.download = "ReportPrograms.xlsx";
                a.click();
                cfpLoadingBar.complete();
            };
        }
    ]);


})();