﻿using System.Collections.Generic;
using System.IO;
using Jumbula.Core.Data;
using Jumbula.Core.Model.Email;

namespace Jumbula.Core.Business
{
    public interface IFileBusiness
    {
        List<string> GetLimitExtensions();

        bool IsAllowedFile(FileStream fileStream);

        OperationStatus ValidateAttachment(List<AttachmentModel> attachmentModels);

        string UploadImage(string imagePath, string clubDomain, string folderName);

    }
}
