﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('FullClassListReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;
        $scope.IsReportRun = false;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;


        $scope.Model.Filters = {};
        $scope.Model.Year = 0;
        $scope.Model._SelectedClubs = [];
        $scope.ClubTypes = [{ Value: "school", Title: "Schools" }, { Value: "provider", Title: "Providers" }, { Value: "district", Title: "School districts" }];

        $scope.Model.IsAllPrograms = "true";

        $scope.Model.ClubType = "school";
        $scope.Model.IsAllClubs = "true";

        $scope.updateChosenRadio = function () {
            $scope.Model.IsAllClubs = "true";
        }


        $scope.getClubs = function (clubType) {
            if (clubType === "school")
                getAllSchools();
            else if (clubType === "provider")
                getAllProviders();
            else
                getAllDistricts();
        }

        $http.get(baseUrl + '/GetFullSeasonClassListReportHeader').success(function (data) {

            $scope.Years = data.Years;
            $scope.Model.AllSeason = data.Seasons;

            $scope.Model.Year = data.Years[0].Value;
            $scope.Model.SeasonName = data.Seasons[0].Value;
        }).error(function (data) {
            jbToast.error("error " + data);
        });


        $scope.clearFilters = function () {
            $scope.Model.Filters = {};
            $scope.Model.Filters.Status = "regular";
            $scope.searchTerm = "";
            $scope.fullClassListReportGridOptions.dataSource.read();
        }


        $scope.prepareGrid = function () {

            if (!$scope.IsReportRun) {

                $scope.IsReportRun = true;

                $scope.fullClassListReportsDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport:
                        {
                            read: {
                                url: baseUrl + '/GetFullClassListReport',
                                type: "POST",
                                data: function () {
                                    return {
                                        model: $scope.Model
                                    };
                                },
                            },

                        },
                    schema: {
                        total: "TotalCount",
                        data: function (response) {

                            $scope.Titles = response.Titles;
                            return response.DataSource;
                        },
                    },
                    serverSorting: false,
                    serverPaging: true,
                    pageSize: 50,
                    batch: true,
                    resizable: true,
                    requestStart: function () {
                        kendo.ui.progress($("[kendo-grid]"), true);
                    },
                    requestEnd: function () {
                        kendo.ui.progress($("[kendo-grid]"), false);
                    }
                });

                $scope.fullClassListReportGridOptions = {
                    excel: {
                        allPages: true,
                        fileName: 'FullClassListReport.xlsx',
                        proxyURL: baseUrl + '/KendoExport',
                    },
                    excelExport: function (e) {
                        cfpLoadingBar.complete();
                    },
                    dataSource: $scope.fullClassListReportsDataSource,
                    sortable: { mode: 'single' },
                    resizable: true,
                    scrollable: true,
                    pageable: {
                        refresh: true,
                        pageSizes:
                            [
                                50, 100, 200
                            ],
                        buttonCount: 5
                    },
                    dataBound: function (e) {
                        var grid = $("#fullClassListReportGrid").data("kendoGrid");
                        customReportService.initializeDoubleScroll(grid);

                        $scope.oldColumns = grid.columns;

                        //Rename columns when select another program with different columns
                        for (var j = 0; j < e.sender.columns.length; j++) {

                            var column = e.sender.columns[j];

                            var newTitle = $scope.Titles[j].replace(/_/g, " ").trimLeft().replace(/sDash/g, "<br>");

                            $("#fullClassListReportGrid thead [data-field=" + column.field + "] .k-link").html(newTitle);
                        }

                    }
                }
            } else {

                var grid = $("#fullClassListReportGrid").data("kendoGrid");

                grid.setOptions({
                    columns: []
                });

                $scope.fullClassListReportGridOptions.dataSource.page(1);
                $("#fullClassListReportGrid").data("kendoGrid").dataSource.read();
            }

            var columns = [];
            $scope.oldColumns = [];

            $(document).bind("ajaxComplete.changeColumns",
                function () {
                    if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                        for (var i = 0; i < $scope.oldColumns.length; i++) {
                            var column = [];
                            column = {
                                field: $scope.oldColumns[i].field,
                                title: $scope.Titles[i].replace(/_/g, " ").trimLeft().replace(/sDash/g, "<br>")
                                    .trimLeft()
                            }
                            columns.push(column);
                        }
                        $("#fullClassListReportGrid").data("kendoGrid").setOptions({
                            columns: columns
                        });
                    }
                    $(document).unbind("ajaxComplete.changeColumns");
                });

        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#fullClassListReportGrid").getKendoGrid().saveAsExcel();
        }

        function getAllSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data) {
                    $scope.AllClubs = data.AllSchools;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllProviders() {
            $http.get(baseUrl + "/GetAllProvider")
                .success(function (data) {
                    $scope.AllClubs = data;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch('AllClubs', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllDistricts() {
            $http.get(baseUrl + '/GetAllPartnerSchoolDistricts')
                .success(function (data) {
                    $scope.AllClubs = data.AllDisctricts;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

    }]);
})();