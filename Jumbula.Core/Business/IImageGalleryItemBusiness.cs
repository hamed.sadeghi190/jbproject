﻿using Jb.Framework.Common;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IImageGalleryItemBusiness
    {
        ImageGalleryItem Get(int id);
        OperationStatus DeleteImages(List<ImageGalleryItem> images);
    }
}
