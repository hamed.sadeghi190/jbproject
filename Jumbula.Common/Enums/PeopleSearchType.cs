﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum PeopleSearchType : byte
    {
        Name,
        Email,
        [Description("Confirmation id")]
        ConfirmationId,
        Credit,
        Phone,
        Season,
        Schools,
        ErrorMessage
    }
}
