﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('clubLocationController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'programService', function ($scope, $http, $log, $stateParams, $state, jbToast, programService) {

        $scope.MC.stateParams = $stateParams;

        $scope.pageMode = $stateParams.action;

        $http.get('Dashboard/Club/GetLocation', { params: { id: $stateParams.locationId } }).success(function (data, status, headers, config) {

            $scope.Model = data;

        }).error(function () {

        });

        $scope.Model = {};
        $scope.Model.Name = '';
        $scope.Model.Address = '';

        $scope.save = function () {

            if ($scope.pageMode == 'add') {
                $http.post('Dashboard/club/CreateLocation', { model: $scope.Model }).success(function (data, status, headers, config) {

                    if (data.Status) {

                        var _programModel = programService.getModel();

                        _programModel.SelectedLocation = data.Data;

                        programService.setModel(_programModel);

                        $state.back();
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }

                });
            }

            if ($scope.pageMode == 'edit') {
                $http.post('Dashboard/club/EditLocation', { model: $scope.Model }).success(function (data, status, headers, config) {

                    if (data.Status) {

                        $state.back();
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
            }
        }

    }]);
})();
