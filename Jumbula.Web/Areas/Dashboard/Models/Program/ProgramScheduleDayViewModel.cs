﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jumbula.Common.Utilities;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramScheduleDayViewModel 
    {
        public DayOfWeek DayOfWeek { get; set; }

        [Required(ErrorMessage="{0} is required.")]
        //[DataType(DataType.Time, ErrorMessage = "{0} is not in valid format.")]
        [Display(Name="Start time")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? StartTime { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomTimeConverter))]
        //[DataType(DataType.Time, ErrorMessage="{0} is not in valid format.")]
        [Display(Name = "End time")]
        public DateTime? EndTime { get; set; }

        public bool IsChecked { get; set; }
    }
 }