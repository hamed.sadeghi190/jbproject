﻿(function () {
    'use strict';

    angular.module('jbTools').service("classConfirmationService", [function () {

        var _model;

        function setModel(data) {
            _model = data;
        }

        function getModel() {
            return _model;
        }

        return {
            set: setModel,
            get: getModel
        }
    }]);
})();