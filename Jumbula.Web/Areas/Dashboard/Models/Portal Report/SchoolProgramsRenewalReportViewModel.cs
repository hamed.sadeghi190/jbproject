﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolProgramsRenewalReportViewModel
    {
        public SchoolProgramsRenewalReportViewModel()
        {

        }

        public SeasonNames? ProgramSeason { get; set; }
        public string StrProgramSeason { get; set; }
        public string ProgramName { get; set; }
        public string Grade { get; set; }
        public string ProviderName { get; set; }
        public int? ProgramYear { get; set; }
        public int MinimumCapacity { get; set; }
        public int MaximumCapacity { get; set; }
        public int Enrollment { get; set; }
        public string Price { get; set; }
        public decimal TotalPrice { get; set; }
        public string IsRun { get; set; }
        public List<DayOfWeek> Days { get; set; }
        public string StrDays
        {
            get
            {
                var programDays = new List<string>();
                if (Days != null && Days.Any())
                {
                    foreach (var item in Days)
                    {

                        var day = item == DayOfWeek.Thursday ? item.ToString().Substring(0, 2) : item.ToString().Substring(0, 1);
                        programDays.Add(day);
                    }
                    return string.Join(" ", programDays);

                }
                return string.Empty;
            }
        }

    }
}