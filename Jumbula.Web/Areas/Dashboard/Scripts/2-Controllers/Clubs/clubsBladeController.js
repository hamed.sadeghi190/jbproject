﻿(function () {
    'use strict';
    angular.module('dashboardApp').controller('clubsBladeController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {

        window.app.globalObjects.pageClass = 'Members-view';
        $scope.MC.openBlade({ url: "Dashboard/Blade/Clubs", order: 1, menu: "Clubs" });

        $scope.baseUrl = window.location.origin + "/Dashboard/Club";

        $http.get($scope.baseUrl + '/ClubPage').success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.searchTerm = "";
        $scope.filter = {};
        $scope.filter.typeId = "-1";
        $scope.filter.activeType = "";
        $scope.filter.endDate = "";
        $scope.filter.startDate = "";

        $scope.search = function () {
            $("#mainGrid").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {
            $scope.searchTerm = "";
            $scope.filter.typeId = "-1";
            $scope.filter.subTypeId = "-1";
            $scope.filter.activeType = "-1";
            $scope.filter.endDate = "";
            $scope.filter.startDate = "";

            $("#mainGrid").data("kendoGrid").dataSource.read();
        }
        $scope.openClub = function (clubDomain)
        {
            var originUrl = window.location.origin;
            
            var endIndex = originUrl.indexOf(".");
            var mainDomain = originUrl.substring(8, endIndex);
           
            var url = originUrl.replace(mainDomain, clubDomain)+'/login';
          
            window.open(url);
        }

        $scope.deleteClub = function (id) {
            jbConfirmation
                .yes(function () {
                    $http.post($scope.baseUrl + '/DeleteClub', { clubId: id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $scope.mainGridOptions.dataSource.read();

                        }
                    });
                })
                .confirm();
        };

        $scope.resetClubPassword = function (id) {
            jbConfirmation
                .yes(function () {
                    $http.post($scope.baseUrl + '/DeleteClub', { clubId: id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $scope.mainGridOptions.dataSource.read();

                        }
                    });
                })
                .confirm();
        };
    }]);
})();