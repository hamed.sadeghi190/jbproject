﻿(function (angular) {
    'use strict';

    angular
        .module('jbTools')
        .directive('jbTooltip', function () {
            return {
                restrict: 'A',
                link: function ($scope, element) {
                    element.unbind("mouseover,mouseleave,mouseout,mouseenter,mousein").mouseover(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).tooltip("show");
                    }).mouseout(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).tooltip("hide");
                    });
                }
            };
        });
})(angular);
