﻿using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class HomeSiteElements : BaseElements
    {
        public HomeSiteElements(IWebDriver driver)
            : base(driver)
        {

        }
        #region Toolbox
        public IWebElement SeasonGridTool
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='sideBar']/div/div/ul/li[8]"));
                //*[@id="sideBar"]/div/div/ul/li[8]/p
            }
        }
        public IWebElement Tab1Body
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='contents_JbPTab83350']"));
            }
        }
        #endregion Toolbox
        #region Seasongrid
        public IWebElement SeasonGridTitle
        {
            get
            {
                var elements = Driver.FindElements(By.TagName("h4"));
                return elements.FirstOrDefault(elm => elm.Text == "Season grid");
            }
        }
        public IWebElement SeasonGridEditIcon
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='contents_JbPTab83350']/li[1]/div[1]/div[2]/span[2]"));
            }
        }
        #endregion Seasongrid
    }
}
