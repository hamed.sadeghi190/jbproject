﻿using SportsClub.Domain;
using SportsClub.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ChessTourneyRegViewModel
    {

        public ChessTourneyRegViewModel()
        {

        }

        public ChessTourneyRegViewModel(Tourney t)
        {
            TourneyInfo = new TourneyPageViewModel(t);

            TourneyInfo.Map = new GoogleMapsViewModel()
            {
                Name = t.AddressName,
            };

            Domain = t.Domain;
            ClubDomain = t.ClubDomain;

            ItemName = t.Name;

            Playup = new ChargeDiscountPlayerViewModel { IsAvailable = false };
            Schedules = new HashSet<ScheduleViewModel>();
            UscfMembership = new HashSet<ChargeDiscount>(); // need the embedded Id, don't convert to view model
            ProvidedDiscounts = new HashSet<ChargeDiscount>(); // need the embedded Id, don't convert to view model

            Sections = t.Sections;
            foreach (TourneySchedule item in t.Schedules)
            {
                Schedules.Add(new ScheduleViewModel(item));
            }

            EF = new ChargeDiscountPlayerViewModel { IsAvailable = true, IsChecked = true, Category = ChargeDiscountCategories.EF, Desc = string.Format(Constants.W_Tourney_Ef, t.EF.OnLine), Amount = (int)t.EF.OnLine };

            // Calculate late fees if any

            if (t.EF.OnLine1Checked) // we have late fee # 1
            {
                if (DateTime.Today > t.EF.OnLine1After) // player is registering after first late fee charge
                {
                    EF.Amount = (int)t.EF.OnLine1;
                    EF.Desc = string.Format(Constants.W_Tourney_Ef_After, Utilities.FormatDateWithoutTime(t.EF.OnLine1After.Value), Utilities.FormatCurrencyWithoutPenny(EF.Amount));
                }
            }

            if (t.EF.OnLine2Checked) // we have late fee # 2
            {
                if (DateTime.Today > t.EF.OnLine2After) // player is registering after first late fee charge
                {
                    EF.Amount = (int)t.EF.OnLine2;
                    EF.Desc = string.Format(Constants.W_Tourney_Ef_After, Utilities.FormatDateWithoutTime(t.EF.OnLine2After.Value), Utilities.FormatCurrencyWithoutPenny(EF.Amount));
                }
            }
            foreach (ChargeDiscount item in t.ChargeDiscounts)
            {
                switch (item.Category)
                {
                    case ChargeDiscountCategories.Playup:
                        Playup = new ChargeDiscountPlayerViewModel(item);
                        break;

                    case ChargeDiscountCategories.UscfMem:
                        UscfMembership.Add(item);
                        break;

                    case ChargeDiscountCategories.GmIm:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Econ:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Senior:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Custom:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Reentry: // this charge does not apply to registration
                        break;

                    case ChargeDiscountCategories.EF: // EF should not have been included this way and is handled differntly
                        break;

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_ChargeDiscountCategoryNotSupported, item.Category));
                }
            }

            if (ProvidedDiscounts.Count > 0)
            {
                ProvidedDiscountsIsAvail = true;
            }
            else
            {
                ProvidedDiscountsIsAvail = false;
            }

            // Bye options

            Bye = t.Bye;

            // Hidden fields

            ItemId = t.ItemId;
        }

        public void InitRegisterChessTourneyViewModel(Tourney t)
        {

            // indecate logged in user is adult or not
            //var loggedUserContact = DependencyResolverHelper.IPlayerProfileDbService.GetByUserId(TheMembership.GetCurrentUserId())
            //                                 .Single(x => x.Type == PlayerProfileType.Primary)
            //                                 .Contact;
            //IsAdultLoggedInUser = Utilities.CalculateAge(loggedUserContact.DoB.Value) >= Constants.PlayerProfile_Min_Age;

            TourneyInfo = new TourneyPageViewModel(t);

            TourneyInfo.Map = new GoogleMapsViewModel()
            {
                Name = t.AddressName,
            };

            Domain = t.Domain;
            ClubDomain = t.ClubDomain;

            ItemName = t.Name;

            Playup = new ChargeDiscountPlayerViewModel { IsAvailable = false };
            Schedules = new HashSet<ScheduleViewModel>();
            UscfMembership = new HashSet<ChargeDiscount>(); // need the embedded Id, don't convert to view model
            ProvidedDiscounts = new HashSet<ChargeDiscount>(); // need the embedded Id, don't convert to view model

            Sections = t.Sections;

            // Use view model for schedules b/c we need to call its methods to get schedule name, etc.

            foreach (TourneySchedule item in t.Schedules)
            {
                Schedules.Add(new ScheduleViewModel(item));
            }

            EF = new ChargeDiscountPlayerViewModel { IsAvailable = true, IsChecked = true, Category = ChargeDiscountCategories.EF, Desc = string.Format(Constants.W_Tourney_Ef, t.EF.OnLine), Amount = (int)t.EF.OnLine };

            // Calculate late fees if any

            if (t.EF.OnLine1Checked) // we have late fee # 1
            {
                if (DateTime.Today > t.EF.OnLine1After) // player is registering after first late fee charge
                {
                    EF.Amount = (int)t.EF.OnLine1;
                    EF.Desc = string.Format(Constants.W_Tourney_Ef_After, Utilities.FormatDateWithoutTime(t.EF.OnLine1After.Value), Utilities.FormatCurrencyWithoutPenny(EF.Amount));
                }
            }

            if (t.EF.OnLine2Checked) // we have late fee # 2
            {
                if (DateTime.Today > t.EF.OnLine2After) // player is registering after first late fee charge
                {
                    EF.Amount = (int)t.EF.OnLine2;
                    EF.Desc = string.Format(Constants.W_Tourney_Ef_After, Utilities.FormatDateWithoutTime(t.EF.OnLine2After.Value), Utilities.FormatCurrencyWithoutPenny(EF.Amount));
                }
            }

            // Note: All IsCheck fields for ChargeDiscountCategories items must have been set to false
            //       They will be re-written if they exist
            // Note: Reentry charge does not apply during registration

            foreach (ChargeDiscount item in t.ChargeDiscounts)
            {
                switch (item.Category)
                {
                    case ChargeDiscountCategories.Playup:
                        Playup = new ChargeDiscountPlayerViewModel(item);
                        break;

                    case ChargeDiscountCategories.UscfMem:
                        UscfMembership.Add(item);
                        break;

                    case ChargeDiscountCategories.GmIm:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Econ:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Senior:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Custom:
                        ProvidedDiscounts.Add(item);
                        break;

                    case ChargeDiscountCategories.Reentry: // this charge does not apply to registration
                        break;

                    case ChargeDiscountCategories.EF: // EF should not have been included this way and is handled differntly
                        break;

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_ChargeDiscountCategoryNotSupported, item.Category));
                }
            }

            if (ProvidedDiscounts.Count > 0)
            {
                ProvidedDiscountsIsAvail = true;
            }
            else
            {
                ProvidedDiscountsIsAvail = false;
            }

            // Bye options

            Bye = t.Bye;

            // Hidden fields

            ItemId = t.ItemId;
        }

        public int ProfileId { get; set; }
        public string EditModeIdentifire { get; set; }

        public string PrimaryPlayerEmail { get; set; }

        public int OrderId { get; set; }

        [Required]
        [Display(Name = "Name")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Name { get; set; }

        public int[] Byes { get; set; }

        [Required]
        [Display(Name = "USCF ID")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string UscfId { get; set; }

        [Display(Name = "I don't have USCF ID")]
        public bool NoUscfIdChecked { get; set; }

        [Required]
        [Display(Name = "USCF regular rating (approximate ok)")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string UscfRatingReg { get; set; }

        [Display(Name = "USCF quick rating")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string UscfRatingQuick { get; set; }

        [Required]
        [Display(Name = "USCF expiration date")]
        public DateTime? UscfExpiration { get; set; }

        public ChessProfileInfoViewModel Sport { get; set; }

        [Required]
        [Display(Name = "Available sections")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Section { get; set; }

        [Required]
        [Display(Name = "Available schedules")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Schedule { get; set; }

        public ChargeDiscountPlayerViewModel EF { get; set; }
        public ChargeDiscountPlayerViewModel Playup { get; set; }

        [Display(Name = "I acknowldege that I would like to apply for the following discounts:")]
        public bool ProvidedDiscountsIsChecked { get; set; }

        public ChargeDiscountCategories? ProvidedDiscountsSelected { get; set; }

        [Display(Name = "I require USCF membership")]
        public bool UscfMembershipIsChecked { get; set; }

        public int? UscfMembershipSelected { get; set; }

        public bool ProvidedDiscountsIsAvail { get; set; }
        public Bye Bye { get; set; }
        public bool[] ByeRequests { get; set; }
        public ICollection<TourneySection> Sections { get; set; }
        public ICollection<ScheduleViewModel> Schedules { get; set; }
        public dynamic UscfMembership { get; set; }
        public dynamic ProvidedDiscounts { get; set; }
        public string ItemId { get; set; }
        public string ItemName { get; set; }
        public string Domain { get; set; }
        public string ClubDomain { get; set; }
        public string ByesRequested { get; set; }
        public string optionsRequested { get; set; }
        public decimal total { get; set; }
        public TourneyPageViewModel TourneyInfo { get; set; }
    }
}