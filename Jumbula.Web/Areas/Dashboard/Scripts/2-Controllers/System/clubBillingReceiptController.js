﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('clubBillingReceiptController', ['$scope', '$http', '$stateParams', '$state', function ($scope, $http, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/System";


        $scope.Model = {};
        $scope.Model.clubId = $stateParams.clubId;

        $http.get($scope.baseUrl + '/GetClubBillingHistoryDetail?billingId=' + $stateParams.billingId).success(function (data, status, headers, config) {
            $scope.Model = data;

        });
        
        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("SystemClubBillingHistory", { clubId: $stateParams.clubId });
                }
            }
            else {
                $state.go("SystemClubBillingHistory", { clubId: $stateParams.clubId });
            }
        }

    }])

})();