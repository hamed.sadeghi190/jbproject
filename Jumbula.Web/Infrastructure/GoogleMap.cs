﻿using System;
using System.Linq;
using Jumbula.Core.Domain;
using Jumbula.Web.Mvc.WebConfig;

namespace Jumbula.Web.Infrastructure
{
    public static class GoogleMap
    {
        #region Google Map Api

        public static double CalculateDistanceBetweenTwoPoint(double latPoint1, double lngPoint1, double latPoint2, double lngPoint2)
        {
            int R = 6378137; // Earth’s mean radius in meter
            double distanceLatitude = (latPoint2 - latPoint1) * Math.PI / 180;
            double distanceLongitude = (lngPoint2 - lngPoint1) * Math.PI / 180;
            double a = Math.Sin(distanceLatitude / 2) * Math.Sin(distanceLatitude / 2) +
                       Math.Cos(latPoint1 * Math.PI / 180) * Math.Cos(latPoint2 * Math.PI / 180) *
                       Math.Sin(distanceLongitude / 2) * Math.Sin(distanceLongitude / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = R * c;
            return d; 
        }

        public static void GetGeo(string address, ref double lat, ref double lng)
        {
            var googleMapApiKey = WebConfigHelper.GoogleMapAPIKey;
            Velyo.Google.Services.MapsApiContext context = new Velyo.Google.Services.MapsApiContext(googleMapApiKey);
            Velyo.Google.Services.GeocodingRequest request = new Velyo.Google.Services.GeocodingRequest(address, context);

            Velyo.Google.Services.GeocodingResponse geocode = request.GetResponse();

            if (geocode != null && geocode.Results.Any())
            {
                lat = geocode.Results.ToArray().ElementAt(0).Geometry.Location.Latitude;
                lng = geocode.Results.ToArray().ElementAt(0).Geometry.Location.Longitude;
            }
        }

        public static void GetGeo(string address, ref PostalAddress postalAddress, string additionalInfo = null)
        {
            if (postalAddress == null) throw new ArgumentNullException(nameof(postalAddress));
            var googleMapApiKey = WebConfigHelper.GoogleMapAPIKey;
            Velyo.Google.Services.MapsApiContext context = new Velyo.Google.Services.MapsApiContext(googleMapApiKey);
            Velyo.Google.Services.GeocodingRequest request = new Velyo.Google.Services.GeocodingRequest(address, context);
            Velyo.Google.Services.GeocodingResponse geocode = request.GetResponse();

            postalAddress = GetPostalAddressFromGeocodingRequest(geocode);
            postalAddress.AdditionalInfo = additionalInfo;
        }

        public static void GetGeo(double lat, double lng, ref PostalAddress postalAddress, string additionalInfo = null)
        {
            if (postalAddress == null) throw new ArgumentNullException(nameof(postalAddress));
            var googleMapApiKey = WebConfigHelper.GoogleMapAPIKey;
            Velyo.Google.Services.MapsApiContext context = new Velyo.Google.Services.MapsApiContext(googleMapApiKey);
            Velyo.Google.Services.GeocodingRequest request = new Velyo.Google.Services.GeocodingRequest(lat, lng, context);
            Velyo.Google.Services.GeocodingResponse geocode = request.GetResponse();

            postalAddress = GetPostalAddressFromGeocodingRequest(geocode);
            postalAddress.AdditionalInfo = additionalInfo;
        }

        private static PostalAddress GetPostalAddressFromGeocodingRequest(Velyo.Google.Services.GeocodingResponse geocode)
        {
            PostalAddress result = null;
            if (geocode != null && geocode.Results.Any())
            {
                string streetNumber = string.Empty;

                if (
                    geocode.Results.ToArray()
                        .ElementAt(0)
                        .AddressComponents.FirstOrDefault(a => a.Types.Contains("street_number")) != null)
                {
                    streetNumber =
                        geocode.Results.ToArray()
                            .ElementAt(0)
                            .AddressComponents.FirstOrDefault(a => a.Types.Contains("street_number"))
                            ?.LongName;
                }

                string route = string.Empty;

                if (
                    geocode.Results.ToArray()
                        .ElementAt(0)
                        .AddressComponents.FirstOrDefault(a => a.Types.Contains("route")) != null)
                {
                    route =
                        geocode.Results.ToArray()
                            .ElementAt(0)
                            .AddressComponents.FirstOrDefault(a => a.Types.Contains("route"))
                            ?.LongName;
                }

                string city = string.Empty;

                if (
                    geocode.Results.ToArray()
                        .ElementAt(0)
                        .AddressComponents.FirstOrDefault(a => a.Types.Contains("locality")) != null)
                {
                    city =
                        geocode.Results.ToArray()
                            .ElementAt(0)
                            .AddressComponents.FirstOrDefault(a => a.Types.Contains("locality"))
                            ?.LongName;
                }
                string county = string.Empty;

                if (
                    geocode.Results.ToArray()
                        .ElementAt(0)
                        .AddressComponents.FirstOrDefault(a => a.Types.Contains("administrative_area_level_2")) != null)
                {
                    county =
                        geocode.Results.ToArray()
                            .ElementAt(0)
                            .AddressComponents.FirstOrDefault(a => a.Types.Contains("administrative_area_level_2"))
                            ?.LongName;
                }
                string state = string.Empty;

                if (
                    geocode.Results.ToArray()
                        .ElementAt(0)
                        .AddressComponents.FirstOrDefault(a => a.Types.Contains("administrative_area_level_1")) != null)
                {
                    state =
                        geocode.Results.ToArray()
                            .ElementAt(0)
                            .AddressComponents.FirstOrDefault(a => a.Types.Contains("administrative_area_level_1"))
                            ?.LongName;
                }

                string country = string.Empty;

                if (
                    geocode.Results.ToArray()
                        .ElementAt(0)
                        .AddressComponents.FirstOrDefault(a => a.Types.Contains("country")) != null)
                {
                    country =
                        geocode.Results.ToArray()
                            .ElementAt(0)
                            .AddressComponents.FirstOrDefault(a => a.Types.Contains("country"))
                            ?.ShortName;
                }

                string postalcode = string.Empty;

                if (
                    geocode.Results.ToArray()
                        .ElementAt(0)
                        .AddressComponents.FirstOrDefault(a => a.Types.Contains("postal_code")) != null)
                {
                    postalcode =
                        geocode.Results.ToArray()
                            .ElementAt(0)
                            .AddressComponents.FirstOrDefault(a => a.Types.Contains("postal_code"))
                            ?.LongName;
                }

                result = new PostalAddress()
                {
                    Lat = geocode.Results.ToArray().ElementAt(0).Geometry.Location.Latitude,
                    Lng = geocode.Results.ToArray().ElementAt(0).Geometry.Location.Longitude,
                    StreetNumber = streetNumber,
                    Route = route,
                    Street = route,
                    City = city,
                    County = county,
                    State = state,
                    Country = country,
                    Address = geocode.Results.ToArray().ElementAt(0).FormattedAddress,
                    AutoCompletedAddress = geocode.Results.ToArray().ElementAt(0).FormattedAddress,
                    Zip = postalcode,
                };

            }

            return result;
        }

        #endregion
    }
}