﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FinanceViewModel
    {
        public string UserEmail { get; set; }

        public string PayerEmail { get; set; }

        public string FullName { get; set; }

        public string Date { get; set; }

        public string Confirmation { get; set; }
        public string ClubName { get; set; }

        public TransactionType TransactionType { get; set; }

        public TransactionCategory TransactionCategory { get; set; }

        public decimal Charge { get; set; }

        public decimal Payment { get; set; }

        public decimal Balance { get; set; }

        public PaymentMethod? PaymentMethod { get; set; }

        public string Note { get; set; }

        public long OrderId { get; set; }

        public string CheckId { get; set; }

        public string PaypalHandler { get; set; }

        public string ProgramName { get; set; }
        public string SeasonName { get; set; }

        public decimal ListPrice { get; set; }
        public decimal Discount { get; set; }
        public string DiscountName { get; set; }
        public decimal Fees { get; set; }
        public string FeeName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}