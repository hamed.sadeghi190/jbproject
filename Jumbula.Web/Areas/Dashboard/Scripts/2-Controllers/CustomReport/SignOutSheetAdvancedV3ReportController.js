﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('SignOutSheetAdvancedV3ReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();
            $scope.MC.collapseAllBlades();

            var baseUrl = window.location.origin + '/Dashboard/CustomReport';

            var timer = false;

            $scope.Model = {};

            $scope.stateParams = $stateParams;

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.Model.Sorts = [{ Value: "Participant", Title: "Participant" }, { Value: "Grade", Title: "Grade" }];
            $scope.Model.SortBy = "Participant";

            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get(baseUrl + '/GetAllProgramsSignOutSheetReport', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllPrograms = data;
                $scope.Model.ScheduleIds = null;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            var checkValidation = function (schoolId, seasonId) {
                if ($scope.Model.DateProgram == null) {
                    jbToast.error("Please select a session start date.");
                    return false;
                }
                else if ($scope.Model.ScheduleIds == null || $scope.Model.ScheduleIds.length == 0) {
                    jbToast.error("Please select a program.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $http.get(baseUrl + '/GetReportDate').success(function (data, status, headers, config) {

                $scope.Model.Date = data.Date;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.runReport = function () {
                
                if (checkValidation()) {
                    $http.get(baseUrl + '/ProgramsListSignOutSheetReportV3', { params: { seasonDomain: $scope.stateParams.seasonDomain, scheduleIds: $scope.Model.ScheduleIds, date: $scope.Model.DateProgram, sortBy: $scope.Model.SortBy } }).success(function (data, status, headers, config) {

                        $scope.ModelPrograms = data;

                        showEmptyDataMessage(data);

                    }).error(function (data, status, headers, config) {
                        jbToast.error("error " + data);
                    });
                }
            }
            $scope.saveToPdf = function () {
                cfpLoadingBar.start();
                $http.post(baseUrl + '/GenerateSignOutSheetV3Pdf', {
                    seasonDomain: $scope.stateParams.seasonDomain ,
                    scheduleIds: $scope.Model.ScheduleIds,
                    date: $scope.Model.DateProgram,
                    sortBy: $scope.Model.SortBy
                }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/pdf" });

                    var fileUrl = URL.createObjectURL(blob);
                    a.href = fileUrl;
                    a.download = "SingOutSheetV3.pdf";
                    a.click();
                    cfpLoadingBar.complete();
                    });

            };
            $scope.saveToExcel = function () {
                cfpLoadingBar.start();

                $http.post(baseUrl + '/GenerateSignOutSheetV3Excel', {
                    seasonDomain: $scope.stateParams.seasonDomain,
                    scheduleIds: $scope.Model.ScheduleIds,
                    date: $scope.Model.DateProgram,
                    sortBy: $scope.Model.SortBy
                }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/excel" });

                    var fileUrl = URL.createObjectURL(blob);
                    a.href = fileUrl;
                    a.download = "SingOutSheetV3.xls";
                    a.click();
                });

                cfpLoadingBar.complete();
            };

            function showEmptyDataMessage(data) {
                var isEmptyData = true;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].ProgramParticipants != null) {
                        isEmptyData = false;
                    }
                }

                if (isEmptyData) {
                    jbToast.info("There is no sign-out information for " + $scope.Model.DateProgram);
                }
            }
        }
    ]);


})();