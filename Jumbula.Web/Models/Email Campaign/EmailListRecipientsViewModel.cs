﻿using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models.Email_Campaign
{
    public class EmailListRecipientsViewModel : BaseViewModel<MailListContact>
    {
        public string EmailAddress { get; set; }

        public string FullName { get; set; }
    }
}