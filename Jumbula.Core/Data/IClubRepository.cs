﻿using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Data
{
    public interface IClubRepository : IRepository<Club>
    {
        IQueryable<Club> GetMembersOfPartner(long partnerId, bool includePartner = false);
        IQueryable<Club> GetList(List<int> clubsIds);
    }
}
