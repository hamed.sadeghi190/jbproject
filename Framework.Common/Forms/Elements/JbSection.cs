using Jb.Framework.Common.Forms.JsonFormatter;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbSection : JbBaseElement
    {
        public JbSection()
        {
            Elements = new List<IJbBaseElement>();
        }

        public List<IJbBaseElement> Elements { get; set; }
    }
}