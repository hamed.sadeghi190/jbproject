﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Model.People
{
    public class DelinquentModel
    {
        public OrderItem OrderItem;
        public OrderInstallment OrderInstallment;
        public string Email;
        public int? Attempts;
        public decimal? TotalAmount;
        public decimal? PaidAmount;
        public string Program;
        public string Season;
        public string FirstName;
        public string LastName;
        public string Confirmation;
        public DelinquentPolicies DelinquentPolicy;
    }
}
