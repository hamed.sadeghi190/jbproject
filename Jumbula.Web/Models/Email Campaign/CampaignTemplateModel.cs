﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class CampaignTemplateModel
    {
        public string HeaderBackground { get; set; }

        public string ClubLogo { get; set; }

        public bool HasLogo { get; set; }

        public bool HasHeaderText { get; set; }

        public string HeaderText { get; set; }

        public string Body { get; set; }

        public bool HasFooterText { get; set; }

        public string FooterBackground { get; set; }

        public string FooterText { get; set; }

        public string MapLocation { get; set; }

        public int MapZoom { get; set; }

        public string BodyBackColor { get; set; }

        public bool HasMap { get; set; }

    }
}