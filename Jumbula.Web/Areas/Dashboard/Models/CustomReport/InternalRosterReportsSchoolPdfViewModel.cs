﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class InternalRosterReportsSchoolPdfViewModel
    {
        public string SchoolLogo { get; set; }
        public string ClubLogo { get; set; }
        public List<InternalRosterSchoolReportViewModel> Programs { get; set; }

    }
}