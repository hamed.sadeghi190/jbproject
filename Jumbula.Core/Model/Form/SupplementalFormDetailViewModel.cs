﻿using Jumbula.Common.Enums;
using System.Collections.Generic;

namespace Jumbula.Core.Model.Form
{
    public class SupplementalFormDetailViewModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string DateCreated { get; set; }
        public FormType FormType { get; set; }

        public List<string> FormUrls { get; set; }
    }
}
