﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('assignInstructorsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {

        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.init = function () {
            app.globalObjects.initializeView();
        }

        var programId = $stateParams.programId;

        $scope.Model = {};
        $scope.Model.AllInstructors = [];

        $http.get('/Dashboard/Program/GetAssignInstructors?programId=' + programId).success(function (data, status, headers, config) {

            $scope.Model = data;
        });

        $scope.save = function () {
            if ($scope.Model.SelectedInstructors.length > 0) {
                $http.post('/Dashboard/Program/SaveAssignInstructors', { model: $scope.Model, programId: programId })
                    .success(function (data, status, headers, config) {
                        if (data.Status) {

                            jbToast.success("The instructors assigned to the program successfully.");

                            $state.go('ProgramsReview', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                        }
                    })
            }
        }

    }]);
})();