﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;

namespace Jumbula.Core.Model.Program
{
    public class ProgramSessionsItemViewModel
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public DateTime? SecondStart { get; set; }
        public DateTime? SecondEnd { get; set; }
        public DayOfWeek Day { get; set; }
        public String Title { get; set; }
        public int Id { get; set; }
        public SchoolGradeType ScheduleMinGrade { get; set; }
        public SchoolGradeType ScheduleMaxGrade { get; set; }
        public List<KeyValuePair<string, int>> ProgramGradeList { get; set; }
        public string ProgramStatus { get; set; }
        public int? CapacityLeft { get; set; }
        public string Domain { get; set; }
        public string SeasonDomain { get; set; }
        public string Room { get; set; }
    }
}
