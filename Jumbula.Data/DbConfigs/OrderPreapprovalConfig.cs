﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class OrderPreapprovalConfig : EntityTypeConfiguration<OrderPreapproval>
    {
        public OrderPreapprovalConfig()
        {
            Property(d => d.PaypalPreapprovalID).HasMaxLength(128).IsRequired();
        }
    }
}
