﻿using System;

namespace Jumbula.Common.Exceptions
{
    public abstract class JumbulaPaymentException : JumbulaException
    {
        protected JumbulaPaymentException(Exception e)
            : base(Constants.Constants.M_E_Payment, e)
        {
            throw new Exception(e.ToString());
        }
    }
}
