﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Helper;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProviderAddressListReportViewModel
    {
        public string ProviderName { get; set; }

        public string ContactName { get; set; }

        public string StreetAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }
        public string Email { get; set; }
        
        public DateTime? AgreementExpirationDate { get; set; }

        public int? CommissionRate { get; set; }
        public string CommissionRatestr
        {
            get
            {

                return CommissionRate.HasValue && CommissionRate != 0 ? CurrencyHelper.FormatPercentWithoutPenny(CommissionRate) : "-";


            }
        }
    }
}