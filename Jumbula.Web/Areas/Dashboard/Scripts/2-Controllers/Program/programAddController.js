﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programAddController', ['$scope', '$http', '$log','$stateParams','$state', function ($scope, $http, $log,$stateParams,$state) {

        $scope.baseUrl = window.location.origin + "/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.Schedules = [
            {
                earlybirds: [],
                charges: []
            }
        ];

        $scope.init = function () {
            app.globalObjects.initializeView();
        }

        $scope.Waivers = [
            {
                Title: "Waiver 1",
                Value: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
            },
            {
                Title: "Waiver 2",
                Value: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
            },
            {
                Title: "Waiver 3",
                Value: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa."
            }
        ];

    }]);
})();