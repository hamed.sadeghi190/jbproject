﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Model.Payment
{
    public class AutoChargeResult
    {
        public string ErrorMessage;
        public AutoChargeResultType ResultType;
        public long InstallmentId;
        public int ClubId;
        public long TransActionActivityId;
        public JbPaymentResult PaymentResult;

        public AutoChargeResult()
        {

        }
        public AutoChargeResult(long installmentId, int clubId, JbPaymentResult paymentResult, AutoChargeResultType resultType, string errorMessage, long transActionActivityId = 0)
        {
            InstallmentId = installmentId;
            PaymentResult = paymentResult;
            ResultType = resultType;
            ErrorMessage = errorMessage;
            ClubId = clubId;
            TransActionActivityId = transActionActivityId;
        }
    }
}
