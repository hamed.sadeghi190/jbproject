﻿using Jumbula.Core.Business;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Core.Domain
{

    public class ClubUser : IProtectedResource
    {
        [Key, Column(Order = 2, TypeName = "int")]
        [ForeignKey("User")]
        public int UserId { get; set; }


        [Key, Column(Order = 1, TypeName = "int")]
        [ForeignKey("Club")]
        public int ClubId { get; set; }

        public virtual Club Club { get; set; }

        public virtual JbUser User { get; set; }

        public decimal Credit { get; set; }

        public decimal TestCredit { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
           if(ClubId == clubId)
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

 
}
