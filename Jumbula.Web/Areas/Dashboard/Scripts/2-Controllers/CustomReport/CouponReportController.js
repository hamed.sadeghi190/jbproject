﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CouponReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        if (is.undefined($scope.Model.Filters) || $scope.Model.ReportName != $scope.MC.stateParams.reportName) {
            $scope.Model = {};
            $scope.Model.Filters = {};
            $scope.Model.Filters.CouponFilter = "0";
        }

        $http.get(baseUrl + '/FillCouponFilter', { params: { seasonDomain: $scope.MC.stateParams.seasonDomain } })
                .success(function (data, status, headers, config) {
                    if (is.undefined($scope.Model)) {
                        $scope.Model = {};
                    }
                    $scope.Model.couponFilters = data;
                });

        $scope.couponFilters = function () {
            var newArr = [];
            if ($scope.Model && $scope.Model.couponFilters) {
                if ($scope.Model.couponFilters[0].Value != "0") {
                    newArr.push({ Text: "All Coupons", Value: "0" });
                    for (var i = 0; i < $scope.Model.couponFilters.length; i++) {
                        newArr.push({ Text: $scope.Model.couponFilters[i].Text, Value: $scope.Model.couponFilters[i].Value });
                    }
                    return $scope.Model.couponFilters = newArr;
                } else {
                    return $scope.Model.couponFilters;
                }
            }
            else {
                newArr.push({ Text: "All Coupons", Value: "0" });
                return $scope.Model.couponFilters = newArr;
            }
        }

        $scope.couponReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/CouponsReportRun',
                        type: "POST",
                        data: function () {
                            return {
                                seasonDomain: $scope.MC.stateParams.seasonDomain,
                                filters: $scope.Model.Filters,
                                term: $scope.searchTerm,
                            };
                        },
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
            batch: true,
            resizable: true,
        });

        $scope.CouponReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "coupon (" + $scope.MC.stateParams.seasonDomain + ").xlsx",
                proxyURL: "/Dashboard/CustomReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.couponReportsDataSource,
            sortable: { mode: 'single' },
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100
                ],
                buttonCount: 5
            },
            dataBound: function (e) {
                customReportService.initializeDoubleScroll();
            },
            columnMenu: true,
            columns: [
                {
                    field: "FullName",
                    title: "Full name",
                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderItemDetail(#=OrderItemId#)'  class='block'> #= FullName # </a>",
                    width: "180px",
                    locked: true,
                    lockable: true,
                },
                {
                    field: "Email",
                    title: "Email",
                    lockable: true,
                },
                {
                    field: "ProgramName",
                    title: "Program",
                    width: "200px",
                    locked: true,
                    lockable: true,
                },
                {
                    field: "RegDate",
                    title: "Regsitration date",
                    width: "165px",
                    lockable: true,
                },
                {
                    field: "ConfirmationId",
                    title: "Confirmation",
                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderDetail(#=OrderId#)' class='block'> #= ConfirmationId # </a>",
                    width: "155px",
                    lockable: true,
                },
                {
                    field: "CouponName",
                    title: "Coupon name",
                    locked: true,
                    lockable: true,
                    width: "145px",
                },
                {
                    field: "CouponCode",
                    title: "Coupon code",
                    lockable: true,
                    width: "145px",
                },
                {
                    field: "Amount",
                    title: "Coupon amount",
                    lockable: true,
                    width: "145px",
                }
            ],
        }

        $scope.filterData = function () {
            $scope.CouponReportGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.CouponFilter = "0";
            $scope.searchTerm = "";
            $scope.CouponReportGridOptions.dataSource.read();
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#CouponReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/CouponsReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain, makeCampaign: true, term: $scope.searchTerm, filters: $scope.Model.Filters })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        $scope.goOrderDetail = function (orderId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderDetail($scope.MC.stateParams.seasonDomain, orderId);
        }

        $scope.goOrderItemDetail = function (orderItemId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderItemDetail($scope.MC.stateParams.seasonDomain, orderItemId);
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            if (is.undefined($scope.searchTerm)) {
                $scope.Model.Filters.Term = "";
            }
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "Coupon" });
        }

    }]);
})();