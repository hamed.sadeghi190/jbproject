
ALTER TABLE [dbo].[Programs]  add [ParentId] [bigint] NULL
Go

ALTER TABLE [dbo].[Programs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Programs_dbo.Programs_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Programs] ([Id])
GO

ALTER TABLE [dbo].[Programs] CHECK CONSTRAINT [FK_dbo.Programs_dbo.Programs_ParentId]
GO


ALTER TABLE [dbo].[Seasons]  add [ParentId] [bigint] NULL
Go

ALTER TABLE [dbo].[Seasons]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Seasons_dbo.Seasons_ParentId] FOREIGN KEY([ParentId])
REFERENCES [dbo].[Seasons] ([Id])
GO

ALTER TABLE [dbo].[Seasons] CHECK CONSTRAINT [FK_dbo.Seasons_dbo.Seasons_ParentId]
GO

