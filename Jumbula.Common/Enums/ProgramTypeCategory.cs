﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ProgramTypeCategory
    {
        [Description("Class")]
        Class = 0,
        [Description("Camp Program")]
        Camp = 1,
        [Description("Seminar Tour")]
        SeminarTour = 2,
        [Description("Calendar")]
        Calendar = 3,
        [Description("Chess Tournament")]
        ChessTournament = 4,
        [Description("Subscription")]
        Subscription = 5,
        [Description("Before & After care")]
        BeforeAfterCare = 6
    }
}
