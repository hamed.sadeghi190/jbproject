﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class WeeklyAgendaEmailModel
    {
        public string MessageWrapper { get; set; }

        public string Message { get; set; }
    }
}