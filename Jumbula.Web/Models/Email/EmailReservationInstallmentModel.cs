﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.WebLogic.UserService;

namespace SportsClub.Models.Email
{
    public class EmailReservationInstallmentModel
    {
        public EmailReservationInstallmentModel()
        {
            Items = new List<EmailReservationInstallmentItemModel>();
            ItemsAmountNow = new List<RegularOrderItemsAmountDueNow>();
        }

        public EmailReservationInstallmentModel(Order order , ReservationEmailMode orderMode = ReservationEmailMode.Normal)
        {
            var itemName = string.Empty;
            var program = string.Empty;
            var clubOfOrder = order.Club;
            var date = string.Empty;

            ItemIsReservation = orderMode == ReservationEmailMode.Normal ? true : false;
            var installmentList = order.RegularOrderItems.Where(i => i.Installments != null && i.Installments.Any()).SelectMany(i => i.Installments).Where(i => i.Type != InstallmentType.AddOn && !i.IsDeleted);

            if (installmentList != null)
            {
                ItemsAmountNow = new List<RegularOrderItemsAmountDueNow>();
                decimal totalPaidAmount = 0;

                HaveItemsCustomPrompt = order.RegularOrderItems.Any(item => item.PaymentPlanType == PaymentPlanType.Installment && item.PaymentPlan != null && item.PaymentPlan.IsUseCustomPrompt) ? true : false;

                var installmentGroup = installmentList.GroupBy(i => i.InstallmentDate.Date);

                var installmentItems = installmentGroup.Select(i => new EmailReservationInstallmentItemModel(i)).ToList();

                Items = installmentItems.OrderBy(o => o.DateTime).ToList();

                foreach (var orderItem in order.RegularOrderItems)
                {
                    if (orderItem.ProgramScheduleId.HasValue)
                    {
                        itemName = orderItem.FullName;
                    }
                    else
                    {
                        itemName = orderItem.FirstName;
                    }

                    if (clubOfOrder.Setting != null)
                    {
                        program = orderItem.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName) : "Donation";

                        if (!clubOfOrder.Setting.AppearanceSetting.HideProgramDatesInCart)
                        {
                            program = orderItem.ProgramScheduleId.HasValue ? orderItem.ProgramSchedule.Program.Name : "Donation";
                            date = orderItem.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramDateWithScheduleTitle(orderItem.ProgramSchedule) : null;
                        }
                        else
                        {
                            program = orderItem.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName) : "Donation";

                        }
                    }
                    else
                    {
                        program = orderItem.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName) : "Donation";

                    }

                    if (orderItem.PaidAmount != 0)
                    {
                        ItemsAmountNow.Add(new RegularOrderItemsAmountDueNow
                        {
                            ParticipantName = itemName,
                            ProgramName = program,
                            Amount = CurrencyHelper.FormatCurrencyWithPenny(orderItem.PaidAmount, clubOfOrder.Currency),
                            ProgramDate = date
                        });
                    }

                    totalPaidAmount += orderItem.PaidAmount;
                }

                TotalPaidAmount = CurrencyHelper.FormatCurrencyWithPenny(totalPaidAmount, clubOfOrder.Currency);
            }
        }

        public List<EmailReservationInstallmentItemModel> Items { get; set; }

        public List<RegularOrderItemsAmountDueNow> ItemsAmountNow { get; set; }

        public string TotalPaidAmount { get; set; }

        public bool ItemIsReservation { get; set; }

        public bool HaveItemsCustomPrompt { get; set; }
    }

    public class EmailReservationInstallmentItemModel
    {
        public EmailReservationInstallmentItemModel(IGrouping<DateTime, OrderInstallment> itemGrouped)
        {
            _paidAmount = itemGrouped.Where(p => p.PaidAmount.HasValue).Any() ? itemGrouped.Where(p => p.PaidAmount.HasValue).Sum(i => i.PaidAmount.Value) : 0;
            DateTime = itemGrouped.Key;
            Amount = itemGrouped.Sum(i => i.Amount);
        }

        private decimal _paidAmount;

        public DateTime DateTime { get; set; }

        public decimal Amount { get; set; }

        public string Description
        {
            get
            {
                if (_paidAmount == 0)
                {
                    return string.Empty;
                }

                if (_paidAmount == Amount)
                {
                    return "paid";
                }
                else
                {
                    var clubCurrency = Ioc.ClubBusiness.GetClubCurrency(JbUserService.GetCurrentClubDomain());
                    return $"paid {CurrencyHelper.FormatCurrencyWithPenny(_paidAmount, clubCurrency)}";
                }
            }
        }
    }

    public class RegularOrderItemsAmountDueNow
    {
        public string ParticipantName { get; set; }
        public string ProgramName { get; set; }

        public string ProgramDate { get; set; }
        public string Amount { get; set; }
    }
}