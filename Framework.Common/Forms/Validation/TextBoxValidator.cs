﻿using FluentValidation;
using System.Linq;

namespace Jb.Framework.Common.Forms.Validation
{
    public class TextBoxValidator : AbstractValidator<JbTextBox>
    {
        public TextBoxValidator()
        {

            // Required
            RuleFor(x => x.Value)
                .NotEmpty()
                .When(w => w.CurrentMode != AccessMode.Design && w.VisibleMode == VisibilityMode.Both && w.Validations.Any(x => x.Type == ElementValidationType.Required))
                .WithMessage("{0}", x => x.Validations.Single(w => w.Type == ElementValidationType.Required).Message);

        }

    }
}