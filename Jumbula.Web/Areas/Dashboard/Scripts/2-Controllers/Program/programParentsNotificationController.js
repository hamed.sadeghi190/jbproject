﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programParentsNotificationController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'templateEditorService', 'emailNotificationService', 'imageUploadService', function ($scope, $http, jbToast, $stateParams, $state, templateEditorService, emailNotificationService, imageUploadService) {
        window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + "/Dashboard/Program";


        //Image validate and crop start
        $scope.validateImage = function (elem, pos) {
            if (pos == 'header') {
                $scope.positionBanner = 'header';
            }
            else if (pos = 'footer') {
                $scope.positionBanner = 'footer';
            }
            imageUploadService.validateImage(this, elem)
        }
        $scope.typeBannerHeader = function (typeb) {
            $scope.typeBannerHeaderValue = typeb;
        }
        $scope.typeBannerFooter = function (typeb) {
            $scope.typeBannerFooterValue = typeb;
        }
        $scope.showCropper1 = true;
        $scope.showCropper2 = false;
        $scope.hideCropper1 = function () {
            $scope.showCropper1 = false;
            $scope.showCropper2 = true;
            $scope.cropper = imageUploadService.clearCropper(this);
        }

        $scope.hideCropper2 = function () {
            $scope.showCropper1 = true;
            $scope.showCropper2 = false;
            $scope.cropper = imageUploadService.clearCropper(this);
        }
        $scope.clearCropper = function () {
            $scope.cropper = imageUploadService.clearCropper(this);

        }


        // init model.
        if (is.undefined($scope.Model)) {
            $scope.Model = {};
            $scope.Model.MailTemplate = {};
        }

        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        }

        $scope.existingImages = templateEditorService.existingImages;

        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        }

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        }

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        }

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        }

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        }

        $scope.enterEditing = function () {
            return templateEditorService.enterEditing(this);
        }

        $scope.saveEditing = function (posbanner, typeBannerHV, typeBannerFV) {
            $scope.positionBanner = null;
            $scope.Model.MailTemplate.ChangeTemplate = true;
            return templateEditorService.saveEditing(this, posbanner, typeBannerHV, typeBannerFV);
        }

        $scope.cancelEditing = function () {
            $scope.positionBanner = null;
            return templateEditorService.cancelEditing(this);
        }
        
        var returnedByBack = $state.returnByBack();

        if (returnedByBack) {

            $scope.Model = emailNotificationService.get();

        }
        if (!returnedByBack) {

            $http.get(baseUrl + '/SendParentsNotification', { params: { programId: $scope.MC.stateParams.programId, IsConfirmation: $scope.MC.stateParams.typeNotification == 'Confirmation' ? true : false } })
                .success(function (data, status, headers, config) {
                    $scope.Model = data;

                    $scope.ChangeTemplate(0);
                });
        }
        var decodeHtml = function (text) {
            var txt = document.createElement("textarea");
            txt.innerHTML = text;
            return txt.value;
        }

        $scope.editRecipients = function () {
            if (is.not.undefined($scope.Model.Body)) {
                $scope.Model.Body = decodeHtml($scope.Model.Body);
            }
            emailNotificationService.set($scope.Model);
            $state.forward('RecipientList', { programId: $scope.MC.stateParams.programId }, 'ParentsNotification', { programId: $scope.MC.stateParams.programId, seasonDomain: $scope.MC.stateParams.seasonDomain, typeNotification: $scope.MC.stateParams.typeNotification == 'Confirmation' ? true : false }, function () { });
        }

        $scope.ChangeTemplate = function (idx) {
            $http.get(window.location.origin + "/Dashboard/Campaign/GetTemplate", { params: { id: idx } }).success(function (data, stats, headers, config) {
                $scope.Model.MailTemplate = data;
                if ($scope.Model.MailTemplate.Name == "") {
                    $scope.Model.MailTemplate.ChangeTemplate = true;
                } else {
                    $scope.Model.MailTemplate.ChangeTemplate = false;
                }
            });
        };

        $scope.SendEmail = function () {
            emailNotificationService.set($scope.Model);
            $http.post(baseUrl + '/SendParentsNotification', { model: $scope.Model })
              .success(function (data, status, headers, config) {
                  if (data.Status == true, data.Message == 'Program') {
                      jbToast.success("Email is sent successfully.");
                      $state.go('ProgramsReview', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                  }
                  else {
                      $scope.MC.handleErrors($scope, data);
                  }
              });
        }

        $scope.ShowTestEmail = false;
        $scope.RemoveBtn_testEmail = false;

        $scope.ShowTestEmailBox = function (mode) {
            if (mode) {
                $scope.RemoveBtn_testEmail = true;
                $scope.ShowTestEmail = true;
            } else {
                $scope.ShowTestEmail = false;
            }

        }
        $scope.cancelTestEmail = function () {

            $scope.ShowTestEmail = false;
            $scope.RemoveBtn_testEmail = false;
        }

        $scope.SendTestEmail = function () {

            $http.post(baseUrl + '/SendTestEmailParentsNotification', { model: $scope.Model })
             .success(function (data, status, headers, config) {
                 if (data.Status == true, data.Message == 'Program') {
                     jbToast.success("Email is sent successfully.");
                     $scope.ShowTestEmail = false;
                     $scope.RemoveBtn_testEmail = false;
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             });
        }

        if (returnedByBack) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();