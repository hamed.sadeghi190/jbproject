﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Newtonsoft.Json;
using System;
using System.Linq;
using Jumbula.Core.Domain;
using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramItemViewModel
    {
        public long Id { get; set; }
        public string ClubDomain { get; set; }
        public bool IsProvider { get; set; }
        public string SeasonDomain { get; set; }
        public int WaitList { get; set; }
        public string RespondStatus { get; set; }
        public int MinimumCapacity { get; set; }
        public int MaximumCapacity { get; set; }
        public string Domain { get; set; }
        private string _day;
        public int CommisionRate { get; set; }
        public string Days
        {
            get
            {
                _day = string.Empty;
                if (Schedule != null && TypeCategory != ProgramTypeCategory.ChessTournament && TypeCategory != ProgramTypeCategory.Subscription && TypeCategory != ProgramTypeCategory.BeforeAfterCare) 
                {
                    foreach (var dayItem in ((ScheduleAttribute)Schedule.Attributes).Days)
                    {
                        if (!string.IsNullOrEmpty(_day))
                        {
                            _day += ", ";
                        }
                        _day += dayItem.DayOfWeek.ToString().Substring(0, 3);
                        _day += string.Format(" {0}-{1}", DateTime.Today.Add(dayItem.StartTime.Value).ToString("h:mmtt").ToLower(), DateTime.Today.Add(dayItem.EndTime.Value).ToString("h:mmtt").ToLower());
                    }
                }
                else if(Schedule != null && TypeCategory == ProgramTypeCategory.Subscription)
                {
                    foreach (var dayItem in ((ScheduleSubscriptionAttribute)Schedule.Attributes).Days)
                    {
                        if (!string.IsNullOrEmpty(_day))
                        {
                            _day += ", ";
                        }
                        _day += dayItem.DayOfWeek.ToString().Substring(0, 3);

                        var startDate = dayItem.StartTime.HasValue ? DateTime.Today.Add(dayItem.StartTime.Value).ToString("h:mmtt").ToLower() : null ;
                        var endDate = dayItem.EndTime.HasValue ? DateTime.Today.Add(dayItem.EndTime.Value).ToString("h:mmtt").ToLower() : null;
                        if (startDate != null && endDate != null)
                        {
                            _day += string.Format(" {0}-{1}", startDate, endDate);
                        } 
                    }
                }
                return _day;
            }
        }

        public ProgramScheduleDay day;
        public ProgramScheduleDay ProgramDays
        {
            get
            {
                ProgramScheduleDay day = new ProgramScheduleDay();
                if (Schedule != null && TypeCategory == ProgramTypeCategory.Subscription)
                {
                    var attribuite = Schedule.Attributes as ScheduleSubscriptionAttribute;

                    day = attribuite.Days.FirstOrDefault();
                }
                else if (Schedule != null && TypeCategory != ProgramTypeCategory.ChessTournament && TypeCategory != ProgramTypeCategory.SeminarTour && TypeCategory != ProgramTypeCategory.BeforeAfterCare)
                {
                    var attribuite = ((ScheduleAttribute)Schedule.Attributes);
                    day = attribuite.Days.FirstOrDefault();
                }

                return day;
            }
        }

        public string GradeAge
        {
            get
            {

                var restrictionType = Schedule != null ? Schedule.AttendeeRestriction.RestrictionType : RestrictionType.None;

                switch (restrictionType)
                {
                    case RestrictionType.None:
                        {
                            break;
                        }
                    case RestrictionType.Age:
                        {
                            if (Schedule.AttendeeRestriction.MinAge.HasValue && Schedule.AttendeeRestriction.MaxAge.HasValue)
                            {
                                if (Schedule.AttendeeRestriction.MinAge.Value == Schedule.AttendeeRestriction.MaxAge.Value)
                                {
                                    return " Age: " + Schedule.AttendeeRestriction.MinAge.Value;
                                }
                                else
                                {
                                    return " Ages: " + Schedule.AttendeeRestriction.MinAge.Value + " - " + Schedule.AttendeeRestriction.MaxAge.Value;
                                }

                            }
                            if (Schedule.AttendeeRestriction.MinAge.HasValue)
                            {
                                return "Min age: " + Schedule.AttendeeRestriction.MinAge.Value;
                            }
                            if (Schedule.AttendeeRestriction.MaxAge.HasValue)
                            {
                                return "Max age: " + Schedule.AttendeeRestriction.MaxAge.Value;
                            }
                        }
                        break;
                    case RestrictionType.Grade:
                        {
                            if (Schedule.AttendeeRestriction.MinGrade.HasValue && Schedule.AttendeeRestriction.MaxGrade.HasValue)
                            {
                                if (Schedule.AttendeeRestriction.MinGrade.Value == Schedule.AttendeeRestriction.MaxGrade.Value)
                                {
                                    return " Grade: " + Schedule.AttendeeRestriction.MaxGrade.Value.ToDescription();
                                }
                                else
                                {
                                    return "Grades: " + Schedule.AttendeeRestriction.MinGrade.Value.ToDescription() + " - " + Schedule.AttendeeRestriction.MaxGrade.Value.ToDescription();
                                }
                            }
                            if (Schedule.AttendeeRestriction.MinGrade.HasValue)
                            {
                                return "Min grade: " + Schedule.AttendeeRestriction.MinGrade.Value.ToDescription();
                            }
                            if (Schedule.AttendeeRestriction.MaxGrade.HasValue)
                            {
                                return "Max grade: " + Schedule.AttendeeRestriction.MaxGrade.Value.ToDescription();
                            }
                        }
                        break;
                    default:
                        break;
                }


                return string.Empty;
            }
        }

        public string Capacity
        {
            get
            {
                if (MinimumCapacity> 0 && MaximumCapacity> 0)
                {
                    return  MinimumCapacity + " - " + MaximumCapacity;
                }
                if (MinimumCapacity > 0 && MaximumCapacity <= 0)
                {
                    return "Min: " + MinimumCapacity;
                }
                if (MinimumCapacity <= 0 && MaximumCapacity > 0)
                {
                    return "Max: " + MaximumCapacity;
                }

                return string.Empty;
            }
        }
        public string Name { get; set; }
        public string OutSourceClubAndSeasonNameForSchool { get; set; }
        public string OutSourceClubAndSeasonNameForProvider { get; set; }
        
        public bool HideOutSourceProgramInProvider { get; set; }
        public string ProviderStatus { get; set; }
        [JsonIgnore]
        public ProgramSchedule Schedule { get; set; }
        public ProgramStatus Status { get; set; }
        public OutSourceProgramStatus OutSourceProgramStatus { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public ProgramTypeCategory TypeCategory { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        public ProgramPageStep LastCreatedPage { get; set; }

        public SaveType SaveType { get; set; }
        public decimal? TotalAmount { get; set; }
        public int ParticipantCount { get; set; }

        public string ViewPageUrl { get; set; }

        public int TotalProviderFundedScholarships { get; set; }

        public int NumberOfScheduleNotFreezed { get; set; }
        public List<SchedulesDetail> SchedulesDetail { get; set; }
    }

    public class SchedulesDetail
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public bool IsFreezed { get; set; }
        public TimeOfClassFormation ScheduleMode { get; set; }
    }
}