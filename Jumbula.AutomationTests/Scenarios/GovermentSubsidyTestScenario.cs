﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
   
    public class GovermentSubsidyTestScenario: BaseTestScenario
    {
        [Test]
        public void ViewGovermentSubsidies()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to Subsidies
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToSubsidies();

            // View subsidies test
            var govermentSubsidyTest = new GovermentSubsidyTest(WebDriver);
            govermentSubsidyTest.View();

            WebDriver.Close();
        }

        [Test]
        public void FilterGovermentSubsidies()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to Subsidies
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToSubsidies();

            // View subsidies test
            var govermentSubsidyTest = new GovermentSubsidyTest(WebDriver);
            govermentSubsidyTest.Filter();

            WebDriver.Close();
        }

        [Test]
        public void EditGovermentSubsidies()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to Subsidies
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToSubsidies();

            // Edit subsidy 
            var govermentSubsidyTest = new GovermentSubsidyTest(WebDriver);
            govermentSubsidyTest.Edit();

            WebDriver.Close();
        }

        [Test]
        public void AddGovermentSubsidies()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();

            // Go to Subsidies
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToAddSubsidy();

            // Add subsidy 
            var govermentSubsidyTest = new GovermentSubsidyTest(WebDriver);
            govermentSubsidyTest.Add();

            WebDriver.Close();
        }
    }
}
