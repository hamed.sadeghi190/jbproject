﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ProviderAddressListReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};

        $scope.Model = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.ProviderNames = data.ProviderNames;
            dataInfo = data;
            manipulateProviderAddressListGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        var model = $scope.Model;


        var manipulateProviderAddressListGrid = function () {
            $scope.ProviderAddressListDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetProviderAddressListReportInfo",
                        dataType: "json",
                        data: function () {
                            return {
                                startDate: $scope.startDate,
                                endDate: $scope.endDate
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.ProviderAddressListGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (ProviderAddressList).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Provider address list - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.ProviderAddressListDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#ProviderAddressListGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "ProviderName",
                        title: "Provider name",
                        width: "190px"
                    },
                    {
                        field: "ContactName",
                        title: "Contact name",
                        width: "120px"
                    }, {
                        field: "StreetAddress",
                        title: "Street address",
                        width: "300px"
                    }, {
                        field: "City",
                        title: "City",
                        width: "120px"
                    }, {
                        field: "State",
                        title: "State",
                        width: "120px"
                    }, {
                        field: "Zip",
                        title: "Zip",
                        width: "90px"
                    },
                    {
                       field: "CommissionRatestr",
                       title: "Rate",
                       width: "120px"
                   },
                   {
                      field: "Email",
                      title: "Email (Contact)",
                      width: "160px"
                 },
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ProviderAddressListGrid").getKendoGrid().saveAsExcel();
        }
        $scope.saveToPDF = function () {
            cfpLoadingBar.start();
            $http.get(baseUrl + "/GetProviderAddressListReportInfo?printMode=" + true)
            .success(function (data, status, headers, config) {
                cfpLoadingBar.complete();
            });

        }
        $scope.print = function () {
            cfpLoadingBar.start();
            printGrid();
            cfpLoadingBar.complete();
        }

        $scope.search = function () {
            $("#ProviderAddressListGrid").data("kendoGrid").dataSource.read();
        };

        $scope.clearFilters = function () {
            $scope.endDate = "";
            $scope.startDate = "";
            $("#ProviderAddressListGrid").data("kendoGrid").dataSource.read();
        }


        function printGrid() {

            cfpLoadingBar.start();
            $http.get(baseUrl + "/GetProviderAddressListReportInfo?printMode=" + true)
            .success(function (data, status, headers, config) {
                cfpLoadingBar.complete();
            });
        }
    }
    ]);

})();