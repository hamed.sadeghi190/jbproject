﻿using Jumbula.Core.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System.Security.Claims;
using System.Threading.Tasks;
using Jumbula.Core.Domain;

namespace Jumbula.Data.Identity
{
    public class ApplicationSignInManager : SignInManager<JbUser, int>, IApplicationSignInManager
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public IAdditionalData AdditionalData { get; set; }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(JbUser user)
        {
            var serializedData = JsonConvert.SerializeObject(AdditionalData);

            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager, serializedData);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options,
            IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        public async Task<SignInStatus> PasswordSignInAsync(string userName, string password,
            ICurrentOrganization currentOrganization, ICurrentUser currentUser, bool isPersistent, bool shouldLockout)
        {
            AdditionalData = new UserAdditionalData()
            {
                CurrentOrganization = currentOrganization,
                CurrentUser = currentUser
            };

            return await base.PasswordSignInAsync(userName, password, isPersistent, shouldLockout);
        }

        public virtual async Task SignInAsync(JbUser user,
           ICurrentOrganization currentOrganization, ICurrentUser currentUser, bool isPersistent, bool rememberBrowser)
        {
            AdditionalData = new UserAdditionalData()
            {
                CurrentOrganization = currentOrganization,
                CurrentUser = currentUser
            };

            await base.SignInAsync(user, isPersistent, rememberBrowser);
        }
    }
}
