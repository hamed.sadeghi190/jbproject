﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class PaymentDetailConfig : EntityTypeConfiguration<PaymentDetail>
    {
        public PaymentDetailConfig()
        {
            HasMany(p => p.TransactionActivities).WithRequired(c => c.PaymentDetail).HasForeignKey(c => c.PaymentDetailId);

        }
    }
}
