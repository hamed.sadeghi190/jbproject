﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('subsidiesController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {
        window.app.globalObjects.pageClass = 'Subsidies_View';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + '/Dashboard/Club';

        $scope.searchTerm = "";
        $scope.filter = {};

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetAllSubsidies/',
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                               term: $scope.searchTerm,
                           }, data);
                        return JSON.stringify(data);
                    }
                },

            },
            pageSize: 50,
            batch: true,
            serverPaging: true,
            serverSorting: false,

            schema: {
                data: "data",
                total: "total"
            },
            sortable: false,
        });
        $scope.SubsidyGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            serverPaging: true,
            scrollable: true,
            columns: [
            {
                field: "Name",
                title: "Name",
                labels: {
                    visible: true,
                },
            }, {
                field: "StateName",
                title: "State",
                labels: {
                    visible: true,
                },
            },
            {
                field: "Email",
                title: "Email",
                labels: {
                    visible: true,
                },
                width: "250px",
            },
            {

                field: "Phone",
                title: "Phone",
                labels: {
                    visible: true,
                },
               
            },
            {
                field: "WebSite",
                title: "Website",
                labels: {
                    visible: true,
                },
                width: "150px",
            },
            {
                title: "Action",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: '<ul kendo-menu style="display: inline-block;overflow: visible" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li ui-sref="AddSubsidyStep2({subsidyId: #=Id#})">\
                                           <span class="k-link jbi-pencil">View/Edit</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>'
            }
            ],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },

        };
        $scope.search = function () {
            $("#Subsidy").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {

            $scope.searchTerm = "";
            $("#Subsidy").data("kendoGrid").dataSource.read();
        };

        $scope.AddSubSidy = function () {
            $state.go('AddSubsidy');
        }

    }])

})();