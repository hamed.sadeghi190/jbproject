﻿using Jb.Framework.Common.Forms.JsonFormatter;
using Jb.Framework.Common.Forms.Validation;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Jb.Framework.Common.Forms
{
    [FluentValidation.Attributes.Validator(typeof(EmailValidator))]
    [JsonConverter(typeof(JbElementConverter))]
    public class JbEmail : JbTextBox
    {
        public JbEmail()
        {
            _invalidEmailMessage = "Invalid email address.";
        }

        private string _invalidEmailMessage;
        public string InvalidEmailMessage
        {
            get { return _invalidEmailMessage; }
            set { _invalidEmailMessage = value; }
        }

        [EmailAddress(ErrorMessage = "Invalid email address.")]
        [DataType(DataType.EmailAddress)]
        public override string Value
        {
            get
            {
                return base.Value;
            }
            set
            {
                base.Value = value;
            }
        }        
    }
}
