﻿(function () {
    'use strict';

    angular.module('jbTools').service("waiverService", ['$http', function ($http) {

        var _model = {};

        function setModel(data) {
            _model = data
        }

        function getModel() {
            return _model
        }


        var waivers = function () {

            return new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: window.location.origin + '/Club/GetWaivers',
                            type: "POST",
                        },
                        parameterMap: function (data, type) {
                            if (type === "read") {
                                return {
                                    //term: $scope.recipientSearchTerm,
                                    skip: data.skip,
                                    take: data.take,
                                    page: data.page,
                                    pageSize: data.pageSize,
                                    sort: data.sort
                                }
                            }
                        }
                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverFiltering: true,
                serverSorting: true,
                serverPaging: true,
                pageSize: 10,
            });
        }

        return {
            setModel: setModel,
            getModel: getModel,
        }
    }]);
})();