﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Areas.Dashboard.Models;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class HomeController : DashboardBaseController
    {
        [JbAuthorize(JbAction.AdminDashboard_View)]
        public virtual ActionResult Index()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>()
            {
                new DashboardMenuItemViewModel { Id = 1, Name = "Overview", Title = "Home", Link = "Overview", Icon = "icon-dashboard", Action = JbAction.Home_View},
                new DashboardMenuItemViewModel{ Id= 2, Name = "Seasons", Title = "Seasons", Link = "Seasons", Icon="icon-calendar", Action = JbAction.Season_View},
                new DashboardMenuItemViewModel{ Id = 3, Name = "Clubs", Title = "Organizations", Link = "ClubsBlade", Icon = "jbi-sitemap", Action = JbAction.Member_View},
                new DashboardMenuItemViewModel{ Id = 10, Name = "Clubs", Title = "Clubs", Link = "SystemClubs", Icon = "icon-user", Action = JbAction.Clubs_View},
                new DashboardMenuItemViewModel{ Id = 4, Name = "Campaigns", Title = "Campaigns", Link = "CampaignsOverview", Icon = "jbi-comments", Action = JbAction.Campaign_View},
                new DashboardMenuItemViewModel{ Id = 5, Name = "People", Title = "People", Link = "People", Icon = "jbi-people", Action = JbAction.People_View},
                new DashboardMenuItemViewModel{ Id = 6, Name = "Contacts", Title = "Contacts", Link = "Contacts", Icon = "jbi-people", Action = JbAction.Contact_View},
                new DashboardMenuItemViewModel{ Id = 7, Name = "Staff", Title = "Staff", Link = "Staff", Icon = "jbi-staff", Action = JbAction.Staff_View},
                new DashboardMenuItemViewModel{ Id = 11, Name = "Invoicing", Title = "Invoicing", Link = "Invoices", Icon = "jbi-invoice", Action = JbAction.invoice_View},
                new DashboardMenuItemViewModel{ Id = 12, Name = "MessagingInbox", Title = "Messaging", Link = "MessagingInbox", Icon = "jbi-comments-1", Action = JbAction.Messaging_View},
                new DashboardMenuItemViewModel{ Id = 13, Name = "E-Signatures", Title = "E-Signatures", Link = "ESignature", Icon = "jbi-signing-a-contract", Action = JbAction.ESignature_View},
                new DashboardMenuItemViewModel{ Id = 14, Name = "Flyers", Title = "Flyers", Link = "Flyers", Icon = "jbi-megaphone", Action = JbAction.Flyer_View},
                new DashboardMenuItemViewModel{ Id = 6, Name = "PortalReports", Title = "Reports", Link = "PortalReports", Icon = "jbi-reports", Action = JbAction.PortalReport_View},
                new DashboardMenuItemViewModel{ Id = 7, Name = "Catalog", Title = "Catalog", Link = "Catalog", Icon = "jbi-catalog", Action = JbAction.Catalogs_View},
                new DashboardMenuItemViewModel{ Id = 8, Name = "WeeklyAgenda", Title = "Weekly agenda", Link = "WeeklyAgenda", Icon = "jbi-weekly-email", Action = JbAction.Catalogs_View},
                new DashboardMenuItemViewModel{ Id = 9, Name = "Settings", Title = "Settings", Link = "Settings", Icon = "jbi-gears", Action = JbAction.Settings_View},
                new DashboardMenuItemViewModel{ Id = 15, Name = "Toolbox", Title = "Toolbox", Link = "Toolbox", Icon = "jbi-more", Action = JbAction.Toolbox_view},
                new DashboardMenuItemViewModel{ Id = 11, Name = "Help", Title = "Support", Link = "Tours", Icon = "jbi-question", Action = JbAction.Help_View},
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            if (!ActiveClub.HasPartner && ActiveClub.ClubType.EnumType != ClubTypesEnum.Partner)
            {
                model.RemoveAll(item => item.Name == "E-Signatures");
            }

            if ((!(ActiveClub.Domain.Equals("flexacademies", StringComparison.OrdinalIgnoreCase)) && !(ActiveClub.Domain.Equals("rightatschool", StringComparison.OrdinalIgnoreCase)) && !(ActiveClub.Domain.Equals("boomacademies", StringComparison.OrdinalIgnoreCase))) || ActiveClub.Domain.Equals("jbenterprise", StringComparison.OrdinalIgnoreCase))
            {
                model.RemoveAll(item => item.Name == "Flyers");
            }

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            if(!((club.PartnerId.HasValue && club.PartnerClub.Domain.Equals("flexacademies", StringComparison.OrdinalIgnoreCase)) && club.IsSchool))
            {
                model.RemoveAll(item => item.Name == "WeeklyAgenda");
            }

            return View(model);
        }

        public virtual JsonNetResult GetAllowedActions()
        {

            var actitonRoles = Ioc.SecurityBusiness.GetActionRoles();

            var currentRole = this.GetCurrentUserRole();

            var model = actitonRoles.Where(a => a.Roles.Contains(currentRole)).Select(a => a.Action).ToList();

            return JsonNet(model);
        }

        public virtual ActionResult Overview()
        {
            return View();
        }

        public virtual ActionResult SeasonOverview()
        {
            return View();
        }

        public virtual ActionResult Programs()
        {
            return View();
        }

        public virtual ActionResult ProgramsReview()
        {
            return View();
        }
      
        public virtual ActionResult ProgramAddStep5()
        {
            return View();
        }

        public virtual ActionResult EmailCampaigns()
        {
            return View();
        }

        public virtual ActionResult EmailCampaign()
        {
            return View();
        }

        public virtual ActionResult CampaignsOverview()
        {
            return View();
        }

        public virtual ActionResult _EmailCampaignAddStepNavigation()
        {
            return View();
        }

        public virtual ActionResult EmailCampaignTemplates()
        {
            return View();
        }

        public virtual ActionResult EmailCampaignTemplateAddEdit()
        {
            return View();
        }

        public virtual ActionResult _EmailCampaignTemplateDesigner()
        {
            return View();
        }

        public virtual ActionResult RecipientList()
        {
            return View();
        }
        public virtual ActionResult ClassRecipientList()
        {
            return View();
        }
        public virtual ActionResult Reports()
        {
            return View();
        }

        public virtual ActionResult Form()
        {
            return View();
        }

        public virtual ActionResult FollowUpForms()
        {
            return View();
        }

        public virtual ActionResult EditFormConfirmation()
        {
            return View();
        }
        public virtual ActionResult _FormElementPreviewMode()
        {
            return View();
        }
        public virtual ActionResult _FormElementReviewMode()
        {
            return View();
        }

        public virtual ActionResult _FormElementEditMode()
        {
            return View();
        }

        public virtual ActionResult _FormElementsEditModeSharedView()
        {
            return View();
        }

        public virtual ActionResult _FormElementsPreviewModeSharedView()
        {
            return View();
        }

        public virtual ActionResult _ProgramAddEditStepNavigation()
        {
            return View();
        }

        public virtual ActionResult EmailListRecipients()
        {
            return View();
        }

        public virtual ActionResult EditEmailListRecipients()
        {
            return View();
        }

        public virtual ActionResult Tours()
        {
            return View();
        }

        public virtual ActionResult Tour()
        {
            return View();
        }

        public virtual ActionResult AdminFollowupForm()
        {
            return View("../FollowupForm/AdminFollowupForm");
        }
    }
}