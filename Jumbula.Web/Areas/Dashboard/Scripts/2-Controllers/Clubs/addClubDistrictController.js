﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('addClubDistrictController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', '$timeout', function ($scope, $http, $stateParams, $state, jbToast,$timeout) {
        window.app.globalObjects.pageClass = 'Subsidies_View';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + '/Dashboard/Club';
        $scope.districtId = 0;
        $scope.districtId = $scope.MC.stateParams.districtId;

        $scope.Model = {};

    
        $http.get($scope.baseUrl + '/GetCreateDistrict').success(function (data, status, headers, config) {

                $scope.Model = data;

        });

        jQuery(function ($) {
            $(".icon").click(function () {
                $("~ .icon-msg-box", this).slideToggle(500);
            });
        });
        
        $scope.save = function () {
            $http.post('Dashboard/Club/SaveDistrictStep1', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     console.log(data.Status)
                    $state.go('AddClubDistrictStep2', { clubId: data.Data });
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

    }])

})();