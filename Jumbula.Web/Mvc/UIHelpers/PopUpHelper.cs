﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Jumbula.Web.Mvc.UIHelpers
{
    public static partial class MvcHelpers
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="url"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static MvcHtmlString PopUpLink(this HtmlHelper helper, string url, string text, string lightBoxClass)
        {
            var a = new TagBuilder("a");
            a.SetInnerText(text);
            a.Attributes["href"] = url;
            a.Attributes["data-href"] = "#mainPopUp";
            a.Attributes["data-url"] = url;
            a.Attributes["data-toggle"] = "modal";
            a.Attributes["data-targetClass"] = lightBoxClass;
            a.Attributes["rel"] = "nofollow";
            return MvcHtmlString.Create(a.ToString());
        }

        public static MvcHtmlString PopUpLink(this HtmlHelper helper, string url, string text, string lightBoxClass, string onShowFuncCallBack)
        {
            var a = new TagBuilder("a");
            a.SetInnerText(text);
            a.Attributes["href"] = url;
            a.Attributes["data-href"] = "#mainPopUp";
            a.Attributes["data-url"] = url;
            a.Attributes["data-toggle"] = "modal";
            a.Attributes["data-targetClass"] = lightBoxClass;
            a.Attributes["data-FuncName"] = onShowFuncCallBack;
            return MvcHtmlString.Create(a.ToString());
        }

        public static MvcHtmlString PopUpLink(this HtmlHelper helper, string url, string text, string lightBoxClass, IDictionary<string, object> htmlAttributes)
        {
            var a = new TagBuilder("a");
            a.SetInnerText(text);
            a.Attributes["href"] = url;
            a.Attributes["data-href"] = "#mainPopUp";
            a.Attributes["data-url"] = url;
            a.Attributes["data-toggle"] = "modal";
            a.Attributes["data-targetClass"] = lightBoxClass;

            if (htmlAttributes != null)
            {
                foreach (KeyValuePair<string, object> attr in htmlAttributes)
                {
                    a.Attributes.Add(attr.Key, attr.Value.ToString());
                }
            }

            return MvcHtmlString.Create(a.ToString());
        }

        /// <summary>
        /// Create popup link with i tag
        /// </summary>
        /// <param name="helper">Html helper instance ( used in extension methods )</param>\
        /// <param name="title">link title</param>
        /// <param name="url">link url</param>
        /// <param name="lightBoxClass">lightbox class</param>
        /// <param name="htmlAttributes">custom html attributes</param>
        /// <returns></returns>
        public static MvcHtmlString CloseTabPopUpLink(this HtmlHelper helper, string url, string title, string lightBoxClass, object htmlAttributes = null)
        {
            TagBuilder tag = new TagBuilder("i");

            tag.Attributes.Add("data-target", "#maipPopUp");
            tag.Attributes.Add("data-url", url);
            tag.Attributes.Add("data-targetClass", lightBoxClass);
            tag.Attributes.Add("data-toggle", "modal");
            tag.Attributes.Add("title", title);
            tag.AddCssClass("close-tab");

            if (htmlAttributes != null)
            {
                try
                {
                    IDictionary<string, object> attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

                    tag.MergeAttributes(attributes);
                }
                catch 
                {

                }
            }

            return new MvcHtmlString(tag.ToString(TagRenderMode.Normal));
        }
    }
}