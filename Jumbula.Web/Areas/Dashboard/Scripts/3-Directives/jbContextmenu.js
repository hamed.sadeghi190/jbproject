﻿(function (angular) {
    'use strict';

    angular
        .module('jbTools', [])
        .directive('jbContextmenu', function ($compile) {
            return {
                restrict: 'A',
                scope: {
                    jbContextmenu: '='
                },
                link: function (scope, element, attrs, controller) {
                    function extendOptions(options) {
                        return angular.extend({
                            event: 'click',
                            position: 'bottom-right',
                            fadeInSpeed: 200,
                            fadeOutSpeed: 100
                        }, options);
                    }
                    scope.jbContextmenu = extendOptions(scope.jbContextmenu)
                    var $el = angular.element(element);
                    $el.on(scope.jbContextmenu.event, function (e) {
                        if (!angular.element(".jb-contextmenu").size() > 0) {
                            var setContextmenuLotaion = function () {
                                scope.jbContextmenu = extendOptions(scope.jbContextmenu)
                                switch (scope.jbContextmenu.position.split("-")[0]) {
                                    case "top":
                                        contextmenucontainer.css("top", $el.offset().top - angular.element(contextmenucontainer).outerHeight());
                                        break;
                                    case "bottom":
                                        contextmenucontainer.css("top", $el.offset().top + $el.outerHeight());
                                        break;
                                }
                                switch (scope.jbContextmenu.position.split("-")[1]) {
                                    case "right":
                                        contextmenucontainer.css("left", $el.offset().left);
                                        break;
                                    case "left":
                                        contextmenucontainer.css("left", $el.offset().left - angular.element(contextmenucontainer).outerWidth() + $el.outerWidth());
                                        break;
                                }
                            }
                            scope.jbContextmenu = extendOptions(scope.jbContextmenu)
                            angular.element(this).addClass("active");

                            var contextmenu;
                            var contextmenucontainer = angular.element("<div class='jb-contextmenu'/>").addClass(scope.jbContextmenu.class).addClass(scope.jbContextmenu.position.split("-").join(" "));
                            var contextmenu = angular.element("<ul class='dropdown-menu' data-role='dropdown'/>");
                            contextmenucontainer.append(contextmenu);

                            for (var item in scope.jbContextmenu.items) {
                                if (typeof scope.jbContextmenu.items[item] === "object") {
                                    var menuitem;
                                    if (scope.jbContextmenu.items[item].text == "divider") {
                                        menuitem = angular.element("<li class='divider'></li>");
                                    }
                                    else {
                                        menuitem = angular.element("<li><a>" + scope.jbContextmenu.items[item].text + "</a></li>");
                                    }
                                    contextmenu.append(menuitem);
                                    menuitem = menuitem.find("a");
                                    menuitem.unbind("click.jbContextmenu").bind("click.jbContextmenu", scope.jbContextmenu.items[item].click);
                                    if (scope.jbContextmenu.items[item].attrs) {
                                        var itemAttrs = Object.keys(scope.jbContextmenu.items[item].attrs);
                                        for (var itemAttr in itemAttrs) {
                                            if (typeof itemAttrs[itemAttr] !== "function") {
                                                menuitem.attr(itemAttrs[itemAttr], scope.jbContextmenu.items[item].attrs[itemAttrs[itemAttr]]);
                                            }
                                        }
                                    }
                                }
                            }

                            angular.element("body").append($compile(contextmenucontainer)(scope)).bind("mouseup.jbContextmenu", function (e) {
                                scope.jbContextmenu = extendOptions(scope.jbContextmenu)
                                angular.element(contextmenucontainer).fadeOut(scope.jbContextmenu.fadeOutSpeed);
                                setTimeout(function () { angular.element(contextmenucontainer).remove(); }, scope.jbContextmenu.fadeOutSpeed)
                                angular.element(this).unbind("mouseup.jbContextmenu");
                                angular.element("*").unbind("scroll.jbContextmenu");
                                angular.element(window).unbind("resize.jbContextmenu");
                                $el.removeClass("active");
                            });

                            contextmenucontainer.hide(0);
                            setContextmenuLotaion();
                            angular.element("*").bind("scroll.jbContextmenu", function () {
                                setContextmenuLotaion();
                            })
                            angular.element(window).bind("resize.jbContextmenu", function () {
                                setContextmenuLotaion();
                            });
                            contextmenucontainer.fadeIn(scope.jbContextmenu.fadeInSpeed);
                        }
                    });
                }
            };
        });
})(angular);
