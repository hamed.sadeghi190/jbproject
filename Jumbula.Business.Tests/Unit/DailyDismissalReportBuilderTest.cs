﻿using Jumbula.Business.ReportBuilders;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Jumbula.Common.Utilities;
using Jumbula.Core.Model.Report;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class DailyDismissalReportBuilderBusinessTest
    {
        private Mock<IOrderItemBusiness> _mockOrderItemBusiness;
        private Mock<IFormBusiness> _mockFormBusiness;
        private Mock<IProgramBusiness> _mockProgramBusiness;
        private Mock<INewReportBusiness> _mockNewReportBusiness;
        private Mock<ISeasonBusiness> _mockSeasonBusiness;

        private const string ProgramIdParam = "programId";
        private const string SessionIdParam = "sessionId";
        private const string SeasonDomainParam = "seasonDomain";
        private const long Id = 10;
        private const long ProgramId = 10;
        private const int SessionId = 20;
        private const string SeasonDomain = "Fall2018";
        private const int Page = 1;
        private const int PageSize = 20;

        [TestMethod]
        public void BindData_GetSessionAttendanceIsEmpty_ShouldReturnEmpty()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain} };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(new List<ScheduleAttendance>());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel(){Page = Page, PageSize = PageSize});

            // Assert
            result.Data.DataSource.Count.Should().Be(0);
            result.Data.DataSource.Should().BeEmpty();
        }

        [TestMethod]
        public void BindData_GetSessionAttendanceHasData_ShouldReturnValidResult()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count);
        }

        [TestMethod]
        public void BindData_IsPickedUp_ShouldReturnDismissed()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.IsPickedUp).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.IsPickedUp));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().IsPickedUp.Should().Be("Dismissed");
        }

        [TestMethod]
        public void BindData_IsNotPickedUp_ShouldReturnEmpty()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => !c.IsPickedUp).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => !c.IsPickedUp));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().IsPickedUp.Should().BeEmpty();
        }

        [TestMethod]
        public void BindData_StudentNameWhenPlayerProfileIsNull_ShouldReturnEmpty()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.PlayerProfile == null).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.PlayerProfile == null));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().StudentName.Should().Be("");
        }

        [TestMethod]
        public void BindData_StudentNameWhenPlayerProfileContactIsNull_ShouldReturnEmpty()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.PlayerProfile != null && c.PlayerProfile.Contact == null).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.PlayerProfile != null && c.PlayerProfile.Contact == null));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().StudentName.Should().BeEmpty();
        }

        [TestMethod]
        public void BindData_StudentNameWhenPlayerProfileContactIsNotNull_ShouldReturnValidResult()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.PlayerProfile?.Contact != null).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.PlayerProfile?.Contact != null));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().StudentName.Should().NotBeEmpty();
        }

        [TestMethod]
        public void BindData_DismissalFromEnrichmentWhenPlayerProfileIsNull_ShouldReturnEmpty()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.PlayerProfile == null).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.PlayerProfile == null));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().DismissalFromEnrichment.Should().BeEmpty();
        }

        [TestMethod]
        public void BindData_PickupTimeWhenPickupIsNull_ShouldReturnEmpty()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.Pickup == null).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.PlayerProfile == null));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().PickupTime.Should().BeEmpty();
        }

        [TestMethod]
        public void BindData_PickupTimeWhenPickupIsNotNull_ShouldReturnLocalDateTime()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.Pickup != null).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.Pickup != null));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().PickupTime.Should().NotBeEmpty();
        }

        [TestMethod]
        public void BindData_PickupNameWhenPickupIsNull_ShouldReturnEmpty()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.Pickup == null).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.Pickup == null));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().PickupName.Should().BeEmpty();
        }

        [TestMethod]
        public void BindData_PickupNameWhenPickupIsNotNull_ShouldReturnFullName()
        {
            // Arrange
            var parameters = new Dictionary<string, object> { { ProgramIdParam, ProgramId }, { SessionIdParam, SessionId }, { SeasonDomainParam, SeasonDomain } };
            var service = CreateService();
            _mockProgramBusiness.Setup(x => x.GetSessionAttendance(ProgramId, SessionId)).Returns(CreateScheduleAttendances().Where(c => c.Pickup != null).ToList());

            // Act
            var result = service.BindData(parameters, new ClubBaseInfoModel(), new PaginationModel() { Page = Page, PageSize = PageSize });

            // Assert
            result.Data.DataSource.Count.Should().Be(CreateScheduleAttendances().Count(c => c.Pickup != null));
            result.Data.DataSource.ElementAt(0).As<DailyDismissalReportBuilderViewModel>().PickupName.Should().NotBeEmpty();
        }


        private DailyDismissalReportBuilderBusiness CreateService()
        {
            _mockOrderItemBusiness = new Mock<IOrderItemBusiness>();
            _mockFormBusiness = new Mock<IFormBusiness>();
            _mockProgramBusiness = new Mock<IProgramBusiness>();
            _mockNewReportBusiness = new Mock<INewReportBusiness>();
            _mockSeasonBusiness = new Mock<ISeasonBusiness>();

            _mockOrderItemBusiness.Setup(x => x.GetProgramOrderItems(ProgramId, OrderItemStatusCategories.completed)).Returns(CreateOrderItems().AsQueryable());

            return new DailyDismissalReportBuilderBusiness(_mockOrderItemBusiness.Object, _mockFormBusiness.Object, _mockProgramBusiness.Object, _mockNewReportBusiness.Object, _mockSeasonBusiness.Object);
        }

        private List<OrderItem> CreateOrderItems()
        {
            var orderItem1 = FactoryGirl.Build<OrderItem>(p => p.Id = Id);
            var orderItem2 = FactoryGirl.Build<OrderItem>(p => p.Id = Id + 1);
            var orderItem3 = FactoryGirl.Build<OrderItem>(p => p.Id = Id + 2);

            return new List<OrderItem> { orderItem1, orderItem2, orderItem3 };
        }

        private List<ScheduleAttendance> CreateScheduleAttendances()
        {
            var scheduleAttendance1 = FactoryGirl.Build<ScheduleAttendance>(p => p.Id = SessionId);
            var scheduleAttendance2 = FactoryGirl.Build<ScheduleAttendance>();
            var scheduleAttendance3 = FactoryGirl.Build<ScheduleAttendance>(p =>
            {
                p.Id = SessionId + 1;
                p.IsPickedUp = true;
                p.PlayerProfile = null;
                p.PickupSerialized = null;
            });
            var scheduleAttendance5 = FactoryGirl.Build<ScheduleAttendance>(p =>
            {
                p.Id = SessionId + 1;
                p.IsPickedUp = true;
                p.PlayerProfile.Contact = null;
            });

            return new List<ScheduleAttendance> { scheduleAttendance1, scheduleAttendance2, scheduleAttendance3, scheduleAttendance5 };
        }
    }
}
