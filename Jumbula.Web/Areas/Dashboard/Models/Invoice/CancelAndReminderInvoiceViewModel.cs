﻿using System.ComponentModel.DataAnnotations;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CancelAndReminderInvoiceViewModel
    {
        public long InvoiceNumber { get; set; }
        public int InvoiceId { get; set; }
        public decimal AmountDue { get; set; }
        public string UserName { get; set; }
        public string NoteToRecipent { get; set; }
        public bool SendEmail { get; set; }

        [Display(Name = "Email Cc")]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Email format is invalid.")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string EmailCc { get; set; }
    }
}