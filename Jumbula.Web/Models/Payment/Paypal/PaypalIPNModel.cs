﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Payment
{
    public class PaypalIPNModel
    {
        /// use this to verify its not spoofed, this is our info
        #region "Transaction and Notification-Related Variables"
        public string custom { get; set; }

        /// In the case of a refund, reversal, or canceled reversal, 
        /// this variable contains the txn_id of the original transaction, 
        /// while txn_id contains a new ID for the new transaction.
        public string parent_txn_id { get; set; }
        public string receiver_email { get; set; } 
        public string receiver_id { get; set; }
        public bool resend { get; set; }
        /// Keep this ID to avoid processing the transaction twice
        /// The merchant’s original transaction identification number for the payment from the buyer, against which the case was registered.
        public string txn_id { get; set; }
        /// The kind of transaction for which the IPN message was sent.
        public string txn_type { get; set; }
        /// Encrypted string used to validate the authenticity of the transaction
        public string verify_sign { get; set; }


        #endregion

        #region "Buyer Information Variables"

        public string address_country { get; set; } //64
        public string address_city { get; set; } //40
        public string address_country_code { get; set; }  //2
        public string address_name { get; set; } //128 - prob don't need
        public string address_state { get; set; } //40
        public string address_status { get; set; }  //confirmed/unconfirmed
        public string address_street { get; set; } //200
        public string address_zip { get; set; } //20
        public string contact_phone { get; set; } //20
        public string first_name { get; set; } //64
        public string last_name { get; set; } //64
        public string payer_email { get; set; } //127
        public string payer_id { get; set; }  //13

        public int? Zip
        {
            get
            {
                int temp;
                if (int.TryParse(address_zip, out temp))
                {
                    return temp;
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion

        //Payment information identifies the amount and status of a payment transaction, including fees
        #region "Payment Information Variables"

        public string auth_amount { get; set; }
        public string auth_exp { get; set; } 
        public string auth_id { get; set; } 
        public string auth_status { get; set; }
        
        ///The time an eCheck was processed
        ///hh:mm:ss MM DD, YYYY ZONE, e.g. 04:55:30 May 26, 2011 PDT.
        public string echeck_time_processed { get; set; }

        /// Passthrough variable you can use to identify your Invoice Number for this purchase. If omitted, no variable is passed back.
        public string invoice { get; set; } //127
        /////// Item name as passed by you, the merchant. Or, if not passed by you, as
        /////// entered by your customer. If this is a shopping cart transaction, PayPal
        /////// will append the number of the item (e.g., item_name1, item_name2,
        /////// and so forth).
        ////public string item_name1 { get; set; } //127
        ////public string item_number1 { get; set; } //127

        //currency of the payment.
        public string mc_currency { get; set; } 

        //Transaction fee associated with the payment
        public string mc_fee { get; set; } 
        
        //Full amount of the customer's payment 
        public string mc_gross { get; set; }   
        
        ///Memo as entered by your customer in PayPal Website Payments note field.
        public string memo { get; set; }

        /// Whether the customer has a verified PayPal account.
        /// verified – Customer has a verified PayPal account
        /// unverified – Customer has an unverified PayPal account.
        public string payer_status { get; set; }
        /// HH:MM:SS Mmm DD, YYYY PDT (28chars)
        public string payment_date { get; set; }

        ///USD transaction fee associated with the payment. 
        ///payment_gross minus payment_fee equals the amount deposited into the receiver email account. 
        ///If this amount is negative, it signifies a refund or reversal
        public string payment_fee { get; set; }
      

        /// The status of the payment:
        /// Canceled_Reversal: A reversal has been canceled. For example, you
        /// won a dispute with the customer, and the funds for the transaction that was
        /// reversed have been returned to you.
        /// Completed: The payment has been completed, and the funds have been added successfully to your account balance
        ///Created: A German ELV payment is made using Express Checkout. Denied: You denied the payment. This happens only if the payment was
        /// previously pending because of possible reasons described for the pending_reason variable or the Fraud_Management_Filters_x variable.
        /// Expired: This authorization has expired and cannot be captured.
        ///Failed: The payment has failed. This happens only if the payment was made from your customer’s bank account.
        ///Pending: The payment is pending. See pending_reason for more information.
        ///Refunded: You refunded the payment.
        ///Reversed: A payment was reversed due to a chargeback or other type of reversal. The funds have been removed from your account balance and
        ///returned to the buyer. The reason for the reversal is specified in the ReasonCode element.
        ///Processed: A payment has been accepted.
        ///Voided: This authorization has been voided.
        public string payment_status { get; set; }
        /// echeck: This payment was funded with an eCheck.
        /// instant: This payment was funded with PayPal balance, credit card, or Instant Transfer.
        public string payment_type { get; set; }
        /// This variable is set only if payment_status = Pending. - too many reasons (look it up in pdf)
        public string pending_reason { get; set; }
        public string protection_eligibility { get; set; }
        public string quantity { get; set; }
        public string reason_code { get; set; }

#endregion

        #region "Pay Message Variables"
        public string transaction_type{get;set;}

        public string status{get;set;}

        public string action_type{get;set;}

        public string payment_request_date {get;set;}
        public string pay_key{get;set;}

        #endregion

        #region "Preapproval Message Variables"
        public string preapproval_key{get;set;}
        public bool approved{get;set;}

        ///First date for which the preapproval is valid.
        public string starting_date{get;set;}

        ///Last date for which the preapproval is valid. Time is currently not supported.
        public string ending_date{get;set;}

        public string sender_email{get;set;}
        #endregion
      

    }
}