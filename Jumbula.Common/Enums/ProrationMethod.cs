﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ProrationMethod 
    {
        [Description("Month days")]
        CalculateByMonthDays,

        [Description("Month sessions")]
        CalculateBySessions
    }
}
