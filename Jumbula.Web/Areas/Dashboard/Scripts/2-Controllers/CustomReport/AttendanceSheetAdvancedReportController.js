﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('AttendanceSheetAdvancedReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            var baseUrl = window.location.origin + '/Dashboard/CustomReport';

            var timer = false;

            $scope.Model = {};

            $scope.stateParams = $stateParams;


            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get(baseUrl + '/GetAllPrograms', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllPrograms = data;
                $scope.AllPrograms.unshift({ Text: "All", Value: null });
                $scope.Model.ProgramId = null;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            var checkValidation = function (schoolId, seasonId) {
                if ($scope.Model.DateProgram == "") {
                    jbToast.error("Please select a session start date.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $http.get(baseUrl + '/GetReportDate').success(function (data) {

                $scope.Model.Date = data.Date;

            }).error(function (data) {
                jbToast.error(data.Message);
            });

            $http.get(baseUrl + '/GetDate').success(function (data, status, headers, config) {

                $scope.Model.DateProgram = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.runReport = function () {
                if (checkValidation()) {
                    $http.get(baseUrl + '/ProgramsListAttendanceSheetAdvancedReport', { params: { seasonDomain: $scope.stateParams.seasonDomain, programId: $scope.Model.ProgramId, date: $scope.Model.DateProgram } }).success(function (data, status, headers, config) {

                        $scope.ModelPrograms = data;

                    }).error(function (data, status, headers, config) {
                        jbToast.error("error " + data);
                    });
                }
            }
            $scope.saveToPdf = function () {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = baseUrl + '/GenerateAttendanceSheetAdvancedPdf?seasonDomain=' + $scope.stateParams.seasonDomain + '&programId=' + $scope.Model.ProgramId + '&date=' + $scope.Model.DateProgram;
                a.download = "AttendanceSheetReport.Pdf";
                a.click();
            };
            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = '/Dashboard/CustomReport/GenerateAttendanceSheetAdvancedExcel?seasonDomain=' + $scope.stateParams.seasonDomain + '&programId=' + $scope.Model.ProgramId + '&date=' + $scope.Model.DateProgram;
                a.download = "AttendanceSheetReport.xlsx";
                a.click();
                cfpLoadingBar.complete();
            };
        }
    ]);


})();