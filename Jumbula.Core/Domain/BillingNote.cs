﻿using System.Collections.Generic;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class BillingNote : BaseEntity
    {

        public string Description { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
    }

    
}
