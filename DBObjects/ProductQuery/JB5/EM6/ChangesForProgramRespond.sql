
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProgramRespondToInvitations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RespondType] [int] NOT NULL,
	[ProgramId] [bigint] NOT NULL,
	[Respond] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.ProgramRespondToInvitations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProgramRespondToInvitations]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramRespondToInvitations_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ProgramRespondToInvitations] CHECK CONSTRAINT [FK_dbo.ProgramRespondToInvitations_dbo.Programs_ProgramId]
GO


alter TABLE [dbo].[Programs] add [OutSourceProgramStatus] [int]  NULL
 
 go 

 update  programs set [OutSourceProgramStatus]=1
 go
 update programs set OutSourceProgramStatus=2 where OutSourceSeasonId is not null and OutSourceSeasonId>0
 go

 alter TABLE [dbo].[Programs] alter column [OutSourceProgramStatus] [int] not NULL

