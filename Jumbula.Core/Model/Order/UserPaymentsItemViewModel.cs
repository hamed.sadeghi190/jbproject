﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Kendo.Mvc.UI;
using System;
using System.Web;

namespace Jumbula.Core.Model.Order
{
    public class UserPaymentsItemViewModel  : ISchedulerEvent
    {
        public long Id { get; set; }
        public bool IsPastDue => Start < DateTime.Now;
        public long UserId { get; set; }
        public bool? Status { get; set; }
        public bool IsAutoCharge { get; set; }
        public decimal PaidAmount { get; set; }
        public bool IsManually { get; set; }
        public bool IsCancel { get; set; }
        public bool IsPayable { get; set; }
        public int TaskID { get; set; }
        public string PaymentType { get; set; }
        public string Title { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
        public string EndTimezone { get; set; }
        public string StartTimezone { get; set; }
        public string PaymentMessage { get; set; }
        public string TooltipMessage { get; set; }
        public string Description{ get; set; }
        public bool IsAllDay { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }
        public CurrencyCodes Currency { get; set; }
        
        public string ReturnUrl => HttpUtility.UrlEncode("/Registrant/Payments?id=" + Id + "&paymentType="+ PaymentType);

        public bool IsPresentable => IsPayable || PaidAmount > 0;
    }
}
