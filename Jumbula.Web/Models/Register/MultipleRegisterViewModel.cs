﻿using Jb.Framework.Common.Forms;
using System.Collections.Generic;
using System.Web.Mvc;
using System;
using System.Linq;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;


namespace SportsClub.Models
{
    public class MultipleRegisterViewModel
    {
        public MultipleRegisterViewModel()
        {
            Programs = new List<MultipleRegisterProgramItemModel>();
            Waivers = new List<RegisterWaiverItemModel>();
            SelectedItems = new List<long>();
            SelectedTuitions = new List<SelectedTuitonItemModel>();
            FollowUpForms = new List<JbForm>();
            FollowUpFormItems = new List<FollowUpFormItemModel>();

        }

        public string SeasonDomain { get; set; }

        public string SeasonTitle { get; set; }

        public string ClubDomain { get; set; }
        public List<SelectListItem> AllGrades { get; set; }

        public bool IsWaitlistEnabled { get; set; }

        public RegistrationStep LastStep { get; set; }

        public int? SelectedPlayer { get; set; }

        public List<SelectListItem> PlayerList { get; set; }

        public JbForm Form { get; set; }

        public List<FollowUpFormItemModel> FollowUpFormItems { get; set; }

        public List<JbForm> FollowUpForms { get; set; }

        public int FormId { get; set; }

        public bool HasWaiver { get; set; }

        public bool HasPaymentPlan { get; set; }
        public bool IsPreRegisterationMode { get; set; }

        public bool IsLotteryMode { get; set; }

        public DateTime? GeneralOpenRegDate { get; set; }

        public DateTime? LotteryEndDate { get; set; }

        public CurrencyCodes Currency { get; set; }

        public List<MultipleRegisterProgramItemModel> Programs { get; set; }

        public List<SelectedTuitonItemModel> SelectedTuitions { get; set; }

        public List<RegisterWaiverItemModel> Waivers { get; set; }

        public List<long> SelectedItems { get; set; }

        public MultipleRegisterFilterViewModel Filter { get; set; }

        public List<PaymentPlan> PaymentPlanLists { get; set; }

        public int? SelectedPaymentPlan { get; set; }

        public bool HasFollowUpForms { get; set; }

    }

    public class MultipleRegisterFilterViewModel
    {
        Club _club;

        public MultipleRegisterFilterViewModel()
        {

        }

        public MultipleRegisterFilterViewModel(int clubId, string callerId)
        {

            _club = Ioc.ClubBusiness.Get(clubId);

            if (!string.IsNullOrWhiteSpace(callerId))
            {

                var page = Ioc.PageBusiness.GetList().SingleOrDefault(p => p.Club.Id == clubId && p.SaveType == SaveType.Publish);

                var jbPage = JsonHelper.JsonComplexDeserialize<JbPage>(page.JsonBody);
                var selectedTab = jbPage.Elements.Where(e => e.ElementId == callerId).FirstOrDefault() as JbPTab;
                var seasonGrid = selectedTab.Elements.Where(e => e.TypeTitle == "Grid").FirstOrDefault() as JbPSeasonGrid;
                if (seasonGrid != null)
                {
                    Tab = callerId;
                    EnabledPopup = seasonGrid.EnabledPopup;
                    ShowAddress = seasonGrid.Filter.HasAddress;
                    ShowCity = seasonGrid.Filter.HasCity;
                    ShowGrade = seasonGrid.Filter.HasGrade;
                    ShowCategory = seasonGrid.Filter.HasCategory;
                    ShowWeekday = seasonGrid.Filter.HasWeekDay;
                    ShowStatus = seasonGrid.Filter.HasStatus;
                    ShowLocationName = seasonGrid.Filter.HasLocationName;
                    Setting = seasonGrid.Filter.Setting;
                    SeasonId = seasonGrid.SeasonId;
                    IncludeCalendar = seasonGrid.IncludeCalendar;
                    IncludeCamp = seasonGrid.IncludeCamp;
                    IncludeChessTournament = seasonGrid.IncludeChessTournament;
                    IncludeClasses = seasonGrid.IncludeClasses;
                    IncludeSeminarTour = seasonGrid.IncludeSeminarTour;
                    Grades = ShowGrade ? GetGrades(seasonGrid.Filter.Grades) : new List<SelectKeyValue<string>>();
                    ProgramsAddress = ShowAddress ? GetProgramsAddress(seasonGrid.SeasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour) : new List<SelectKeyValue<string>>();
                    ProgramsCity = ShowCity ? GetProgramsCities(seasonGrid.SeasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour) : new List<SelectKeyValue<string>>();
                    ProgramsLocationName = ShowLocationName ? GetProgramsLocationsName(seasonGrid.SeasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour) : new List<SelectKeyValue<string>>();
                    ProgramsCategory = ShowCategory ? GetProgramsCategories(seasonGrid.SeasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour) : new List<SelectKeyValue<string>>();
                    //ProgramsWeekday = ShowWeekday ? GetProgramsWeekDay(seasonGrid.SeasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour) : new List<DayOfWeek>();
                }
            }
        }
        public string Tab { get; set; }
        public bool ShowAddress { get; set; }
        public bool ShowCity { get; set; }
        public bool EnabledPopup { get; set; }
        public bool ShowGrade { get; set; }
        public bool ShowCategory { get; set; }
        public bool ShowLocationName { get; set; }
        public bool ShowStatus { get; set; }
        public bool ShowWeekday { get; set; }
        public bool IncludeCalendar { get; set; }
        public bool IncludeCamp { get; set; }
        public bool IncludeChessTournament { get; set; }
        public bool IncludeClasses { get; set; }
        public bool IncludeSeminarTour { get; set; }
        public List<SelectKeyValue<string>> Grades { get; set; }
        public JbPGridSetting Setting { get; set; }
        public List<SelectKeyValue<string>> ProgramsAddress { get; set; }
        public List<SelectKeyValue<string>> ProgramsLocationName { get; set; }
        public List<SelectKeyValue<string>> ProgramsCity { get; set; }
        public List<SelectKeyValue<string>> ProgramsCategory { get; set; }
        //public List<DayOfWeek> ProgramsWeekday { get; set; }
        public long SeasonId { get; set; }
        public string SelectedAddress { get; set; }
        public string SelectedCity { get; set; }
        public string SelectedLocationName { get; set; }
        public string SelectedCategory { get; set; }
        public string SelectedGrade { get; set; }
        public string selectedDays { get; set; }
        public string datePart { get; set; }
        public string SelectedStatus { get; set; }


        private List<SelectKeyValue<string>> GetProgramsAddress(long seasonId, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var clubLocations = GetAllLocationsInSeason(seasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);
            var address = clubLocations != null && clubLocations.Any() ? clubLocations.Where(c => c.Club.Id == _club.Id).Select(a => a.PostalAddress).DistinctBy(a => a.Address.ToLower()).ToList() : null;
            var model = new List<SelectKeyValue<string>>();

            model.AddRange(address.Select(a => new SelectKeyValue<string>
            {
                Text = a.Address,
                Value = a.Address,
            }).OrderBy(c => c.Text).ToList());

            return model.DistinctBy(a => a.Text).ToList();
        }

        private List<ClubLocation> GetAllLocationsInSeason(long seasonId, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var programs = GetAllPrograms(seasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);

            var clubLocations = programs.Select(p => p.ClubLocation).ToList();

            return clubLocations;
        }

        private List<Program> GetAllPrograms(long seasonId, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {

            var seasons = Ioc.SeasonBusiness.GetList(_club.Id).Where(s => s.Id == seasonId);

            var programs = Ioc.ProgramBusiness.GetList(_club.Id).Where(p => p.LastCreatedPage == ProgramPageStep.Step4 && p.TypeCategory == ProgramTypeCategory.Class && seasons.Select(s => s.Id).ToList().Contains(p.SeasonId));
            var Allporograms = new List<Program>();

            foreach (var program in programs)
            {
                if (program.TypeCategory == ProgramTypeCategory.Class || program.TypeCategory == ProgramTypeCategory.Camp)
                {
                    foreach (var schedule in program.ProgramSchedules)
                    {
                        DateTime currentDate = schedule.StartDate;
                        while (currentDate <= schedule.EndDate)
                        {
                            DayOfWeek dayOfWeek = currentDate.DayOfWeek;
                            foreach (var item in ((ScheduleAttribute)schedule.Attributes).Days)
                            {
                                if (dayOfWeek == item.DayOfWeek)
                                {
                                    Allporograms.Add(program);
                                }
                            }
                            currentDate = currentDate.AddDays(1);
                        }
                    }
                }
                else
                {
                    Allporograms.Add(program);
                }

            }

            return Allporograms;
        }

        private List<SelectKeyValue<string>> GetProgramsCities(long seasonId, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {

            var clubLocations = GetAllLocationsInSeason(seasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);
            var address = clubLocations != null && clubLocations.Any() ? clubLocations.Where(c => c.Club.Id == _club.Id).Select(a => a.PostalAddress).DistinctBy(c => c.City.ToLower()).ToList() : null;

            var model = new List<SelectKeyValue<string>>();
            model.AddRange(address.Where(a => !string.IsNullOrEmpty(a.City)).Select(a => new SelectKeyValue<string>
            {
                Text = a.City,
                Value = a.City,
            }).OrderBy(c => c.Text).ToList());

            return model.DistinctBy(c => c.Text.ToLower()).ToList();
        }

        private List<SelectKeyValue<string>> GetProgramsLocationsName(long seasonId, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {

            var clubLocations = GetAllLocationsInSeason(seasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour).ToList();

            var model = new List<SelectKeyValue<string>>();
            model.AddRange(clubLocations.Where(l => l.Name != null).Select(a => new SelectKeyValue<string>
            {
                Text = a.Name,
                Value = a.Name,
            }).OrderBy(c => c.Text).ToList());


            return model.DistinctBy(l => l.Text.ToLower()).ToList();
        }

        private List<SelectKeyValue<string>> GetProgramsCategories(long seasonId, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var programs = GetAllPrograms(seasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);

            var programsCategories = programs.SelectMany(p => p.Categories);

            var model = new List<SelectKeyValue<string>>();
            model.AddRange(programsCategories.ToList().Distinct().Select(a => new SelectKeyValue<string>
            {
                Text = a.Name,
                Value = a.Name,
            }).OrderBy(c => c.Text).ToList());

            return model;
        }

        private List<DayOfWeek> GetProgramsWeekDay(long seasonId, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var programs = GetAllPrograms(seasonId, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);
            var programBusiness = Ioc.ProgramBusiness;
            List<DayOfWeek> programsDay = new List<DayOfWeek>();

            foreach (var item in programs)
            {
                programsDay.AddRange(programBusiness.GetClassDays(item).Select(pr => pr.DayOfWeek).ToList());
            }

            return programsDay;
        }
        private List<SelectKeyValue<string>> GetGrades(List<JbPFilterGradeItem> Grades)
        {
            var model = new List<SelectKeyValue<string>>();
            model.AddRange(Grades.Where(g => g.IsEnabled).ToList().Select(g => new SelectKeyValue<string>
            {
                Text = g.Title,
                Value = g.Title,
            }).ToList());

            return model;
        }
    }

    public class RegisterWaiverItemModel
    {
        public int Id { get; set; }

        public string Signiture { get; set; }

        public string Text { get; set; }

        public string Name { get; set; }

        public bool Agreed { get; set; }

        public int WaiverId { get; set; }

        public WaiverConfirmationType WaiverConfirmationType { get; set; }

        public bool IsRequired { get; set; }

        public int Order { get; set; }
    }

    public class SelectedTuitonItemModel
    {
        public SelectedTuitonItemModel()
        {
            Charges = new List<SelectedChargeItemModel>();
        }
        public long Id { get; set; }

        public List<SelectedChargeItemModel> Charges { get; set; }
    }

    public class SelectedChargeItemModel
    {
        public long Id { get; set; }

        public bool IsChecked { get; set; }
    }


    public class MultipleRegisterProgramItemModel
    {
        public long Id { get; set; }
        public string ProgramType { get; set; }
        public string DisplayUrl { get; set; }
        public string Domain { get; set; }

        public string Name { get; set; }

        public Genders Gender { get; set; }

        public bool IsGenderRestricted { get; set; }

        public SchoolGradeType? MinGrade { get; set; }
        public SchoolGradeType? MaxGrade { get; set; }

        public bool IsGradeRestricted { get; set; }

        public string GradeRestrictionMessage { get; set; }

        public bool IsAgeRestricted { get; set; }

        public bool IsConflictProgram { get; set; }
        public string ConflictProgramMessage { get; set; }

        public string AgeRestrictionMessage { get; set; }

        public List<ProgramScheduleDayModel> Days { get; set; }

        public string Dates { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramStatus Status { get; set; }

        public string ProgramEMStatus { get; set; }

        public bool Checked { get; set; }

        public string Room { get; set; }

        public CurrencyCodes Currency { get; set; }


        public List<MultipleRegisterScheduleItemModel> Schedules { get; set; }

        public List<MultipleRegisterTuitionItemModel> Tuitions { get; set; }

        public List<MultipleRegisterChargeItemModel> Charges { get; set; }
    }

    public class MultipleRegisterScheduleItemModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        //public List<SelectListItem> Tuitions { get; set; }
        public List<MultipleRegisterTuitionItemModel> Tuitions { get; set; }

        public List<MultipleRegisterChargeItemModel> Charges { get; set; }
    }
    public class MultipleRegisterSelectedItemsModel
    {
        public MultipleRegisterSelectedItemsModel()
        {
            Tuitions = new List<MultipleRegisterTuitionItemModel>();
            Charges = new List<MultipleRegisterChargeItemModel>();
        }

        public List<MultipleRegisterTuitionItemModel> Tuitions { get; set; }

        public List<MultipleRegisterChargeItemModel> Charges { get; set; }
    }

    public class TestSelectedViewModel
    {
        public TestSelectedViewModel()
        {
            Tuitions = new List<SelectListItem>();
            Charges = new List<SelectListItem>();
        }

        public List<SelectListItem> Tuitions { get; set; }

        public List<string> SelectedTuitions { get; set; }

        public List<SelectListItem> Charges { get; set; }
    }


    public class MultipleRegisterTuitionItemModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public decimal Price { get; set; }

        public int ScheduleId { get; set; }

        public decimal? EarlyBirdPrice { get; set; }

        public string Group { get; set; }

        public bool Checked { get; set; }
    }

    public class ProgramScheduleDayModel
    {
        public DayOfWeek DayOfWeek { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }

        public int ScheduleId { get; set; }
    }

    public class MultipleRegisterChargeItemModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public decimal Price { get; set; }

        public bool Checked { get; set; }

        public bool IsMandatory { get; set; }
    }

    public class MultipleRegisterStep2ViewModel
    {
        public JbForm Form { get; set; }

        public int SelectedPlayer { get; set; }

        public List<long> SelectedTuitionOptions { get; set; }

        public List<SelectListItem> PlayerLists { get; set; }
    }

    public class MultipleRegisterFormModel
    {
        public int ProfileId { get; set; }

        public List<int> Forms { get; set; }
    }

    public class MultipleRegisterFormViewModel
    {
        public string PlayerName { get; set; }

        public JbForm Form { get; set; }
    }

    public enum MultipeRegisterStep : byte
    {
        SelectProgram = 0,
        SelectProfile = 1,
        ConfirmWaivers = 2
    }

    public class FollowUpFormItemModel
    {
        public string Name { get; set; }

        public string AssignedProgramsName { get; set; }

        public int FormId { get; set; }

    }
}