﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CreditViewModel
    {
        public CreditViewModel()
        {

            PaymentMethods = SeedMakePaymentMethods();
            SelectedPaymentMethod = "0";
            
            
        }
        public int ClubId { get; set; }
        public string ClubName { get; set; }
        public int ClientId { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string BillingId { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal Balance { get; set; }
        public List<SelectKeyValue<string>> PaymentMethods { get; set; }
        public string SelectedPaymentMethod { get; set; }
        List<SelectKeyValue<string>> SeedMakePaymentMethods()
        {
            var result = new List<SelectKeyValue<string>>();
            result.Add(new SelectKeyValue<string>() { Text = "Select a payment method", Value = "0" });
            foreach (PaymentMethod payMethod in Enum.GetValues(typeof(PaymentMethod)))
            {
                if (payMethod != PaymentMethod.None && payMethod != PaymentMethod.Credit)
                {
                    result.Add(new SelectKeyValue<string>() { Text = payMethod.ToDescription(), Value = ((int)payMethod).ToString() });
                }
            }

            return result;
        }



    }
}