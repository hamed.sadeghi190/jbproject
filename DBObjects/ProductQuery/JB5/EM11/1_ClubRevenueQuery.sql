
CREATE TABLE [dbo].[ClubRevenues](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Minimum] [int] NOT NULL,
	[Maximum] [int] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL
 CONSTRAINT [PK_dbo.ClubRevenues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

Go 


INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(0,100000,40)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(100001,200000,50)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(200001,300000,60)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(300001,400000,70)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(400001,500000,80)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(500001,600000,90)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(600001,700000,100)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(700001,800000,110)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(800001,900000,120)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(900001,1000000,130)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(1000001,1500000,180)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(1500001,2000000,230)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(2000001,2500000,280)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(2500001,3000000,330)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(3000001,3500000,380)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(3500001,4000000,430)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(4000001,4500000,480)
Go
INSERT INTO [dbo].[ClubRevenues]([Minimum] ,[Maximum], [Price]) VALUES(4500001,5000000,530)
Go
alter TABLE [dbo].[PricePlans] add [ClubRevenueId] [int]  NULL
Go
update PricePlans set ClubRevenueId=1

Go
ALTER TABLE [dbo].[PricePlans]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PricePlans_dbo.ClubRevenues_ClubRevenueId] FOREIGN KEY([ClubRevenueId])
REFERENCES [dbo].[ClubRevenues] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PricePlans] CHECK CONSTRAINT [FK_dbo.PricePlans_dbo.ClubRevenues_ClubRevenueId]
GO
ALTER TABLE [dbo].[Clubs] add [IsNonProfit] [bit]  NULL
Go 
update clubs set [IsNonProfit]=0
Go
ALTER TABLE [dbo].[Clubs] alter column [IsNonProfit] [bit]  Not NULL
Go
ALTER TABLE [dbo].[Clubs] add [ClubRevenueId] [int]  NULL
Go
update clubs set ClubRevenueId=1

GO
ALTER TABLE [dbo].[Clubs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Clubs_dbo.ClubRevenues_ClubRevenueId] FOREIGN KEY([ClubRevenueId])
REFERENCES [dbo].[ClubRevenues] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Clubs] CHECK CONSTRAINT [FK_dbo.Clubs_dbo.ClubRevenues_ClubRevenueId]
GO


