﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SeasonChargeViewModel : BaseViewModel<Charge, long>
    {
        public string Name { get; set; }
        public ChargeDiscountCategory Category { get; set; } 
        public decimal Amount { get; set; }

        public string StrAmount { get; set; }

        public string CategoryDescription
        {
            get
            {
                return Category.ToDescription();
            }
        }

        public string Description { get; set; }
    }
}