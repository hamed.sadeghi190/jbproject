﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class CouponBusiness :  ICouponBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderBusiness _orderBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly ISubscriptionBusiness _subsscriptionBusiness;
        private readonly ICartBusiness _cartBusiness;
        private readonly IRepository<Coupon> _couponRepository;
        private readonly IEntitiesContext _dataContext;

        public CouponBusiness(IClubBusiness clubBusiness, IProgramBusiness programBusiness, IOrderBusiness orderBusiness, IOrderItemBusiness orderItemBusiness, ISubscriptionBusiness subsscriptionBusiness, ICartBusiness cartBusiness, IRepository<Coupon> couponRepository, IEntitiesContext dataContext)
        {
            _clubBusiness = clubBusiness;
            _programBusiness = programBusiness;
            _orderBusiness = orderBusiness;
            _orderItemBusiness = orderItemBusiness;
            _subsscriptionBusiness = subsscriptionBusiness;
            _cartBusiness = cartBusiness;
            _couponRepository = couponRepository;
            _dataContext = dataContext;
        }

        public  Coupon Get(int id)
        {
            return _couponRepository.Get(c => c.Id == id);
        }

        public List<int> GetListId(int clubId, string couponCode, bool isTestMode)
        {
            var result = new List<int>();

            var todayMin = _clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToZeroTime().AddSeconds(-1);
            var todayMax = todayMin.AddDays(1).AddSeconds(2);

            var coupons = GetList(c => c.Deleted == false &&
                 c.Code.ToLower() == couponCode.ToLower() &&
                 c.Season.ClubId == clubId &&
                 c.StartDate < todayMax &&
                 c.EndDate > todayMin).ToList();

            foreach (var coupon in coupons)
            {
                if (coupon.IsOneTimeUse)
                {
                    if (GetCouponUseCount(coupon.Id, isTestMode) < 1)
                    {
                        result.Add(coupon.Id);
                    }
                }
                else
                {
                    result.Add(coupon.Id);
                }
            }

            return result;
        }

        public  IQueryable<Coupon> GetList(Expression<Func<Coupon, bool>> predicate)
        {
            return _couponRepository.GetList(predicate);
        }

        public IQueryable<Coupon> GetList()
        {
            return _couponRepository.GetList().Where(c => c.Deleted == false);
        }
        public IQueryable<Coupon> GetList(long seasonId)
        {
            return _couponRepository.GetList().Where(c => c.SeasonId == seasonId && c.Deleted == false);
        }
        public IQueryable<Coupon> GetList(long seasonId, bool isDeleted)
        {
            return isDeleted ? _couponRepository.GetList().Where(c => c.SeasonId == seasonId && c.Deleted == false) : _couponRepository.GetList().Where(c => c.SeasonId == seasonId);
        }

        public IQueryable<Coupon> GetList(List<long> seasonIds)
        {
            return _couponRepository.GetList().Where(c => !c.Deleted && c.SeasonId.HasValue && seasonIds.Contains(c.SeasonId.Value));
        }

        public IQueryable<Coupon> GetList(List<long> seasonIds, bool isDeleted)
        {
            return isDeleted ? _couponRepository.GetList().Where(c => !c.Deleted && c.SeasonId.HasValue && seasonIds.Contains(c.SeasonId.Value)) : _couponRepository.GetList().Where(c => c.SeasonId.HasValue && seasonIds.Contains(c.SeasonId.Value));
        }

        public List<Coupon> GetProgramCoupons(long programId)
        {
            var result = new List<Coupon>();

            var program = _programBusiness.Get(programId);

            var seasonCoupons = GetList(program.SeasonId);

            result.AddRange(program.Coupons);

            foreach (var coupon in seasonCoupons.Where(c => !c.Deleted))
            {
                if (coupon.IsAllProgram)
                {
                    result.Add(coupon);
                }
            }

            return result;
        }

        public bool IsExistCouponCode(long seasonId, string couponCode)
        {
            return _couponRepository.GetList(c => c.Deleted == false &&
                c.SeasonId == seasonId &&
                c.Code.ToLower() == couponCode.ToLower()
                //&&
                //c.StartDate < DateTime.UtcNow &&
                //c.EndDate > DateTime.UtcNow
                ).Any();
        }
        public bool IsExistCouponCode(long seasonId, string couponCode, int couponId)
        {
            return _couponRepository.GetList(c => c.Deleted == false &&
                c.SeasonId == seasonId &&
                c.Code.ToLower() == couponCode.ToLower() &&
                c.Id != couponId &&
                c.StartDate < DateTime.UtcNow &&
                c.EndDate > DateTime.UtcNow).Any();
        }

        public OperationStatus Create(Coupon coupon)
        {
            return _couponRepository.Create(coupon);
        }

        public OperationStatus Update(Coupon coupon)
        {
            return _couponRepository.Update(coupon);
        }

        public OperationStatus Delete(Coupon coupon)
        {
            coupon.Deleted = true;
            return _couponRepository.Update(coupon);
        }
        public OperationStatus Delete(int couponId)
        {
            return Delete(Get(couponId));
        }

        public IEnumerable<Coupon> GetAllBy(string clubDomain, int pageSize, int currentPage, out int total)
        {
            int skip = (currentPage - 1) * pageSize;
            total = _couponRepository.GetList().Count(c => c.ClubDomain == clubDomain);
            return _couponRepository.GetList().Where(c => c.ClubDomain == clubDomain && !c.Deleted).OrderBy(c => c.Id).Skip(skip).Take(pageSize).ToList();
        }

        public Coupon GetBy(int couponId)
        {
            return _couponRepository.Get(couponId);
        }



        public void CalulateCoupon(Cart cart)
        {
            CalulateCoupon(cart, null);
        }

        public void CalulateCoupon(Cart cart, List<int> newCouponsId)
        {
            var order = cart.Order;

            var cartCoupons = _cartBusiness.GetAllCoupons(cart).Select(c => c.Id).ToList();
            if (newCouponsId != null)
                foreach (var nci in newCouponsId)
                    if (!cartCoupons.Contains(nci))
                        cartCoupons.Add(nci);

            foreach (var item in order.RegularOrderItems)
                if (item.GetOrderChargeDiscounts().ToList().Any())
                    foreach (var ch in item.OrderChargeDiscounts.Where(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.Coupon).ToList())
                        item.OrderChargeDiscounts.Remove(ch);

            var allPrograms = order.RegularOrderItems.Where(i => i.ProgramScheduleId.HasValue).GroupBy(item => new { item.SeasonId, item.ProgramSchedule.ProgramId })
                .Select(po => po.Key.ProgramId).Distinct().ToList();
            var allSeasons = order.RegularOrderItems.Where(i => i.ProgramScheduleId.HasValue).GroupBy(item => new { item.SeasonId, item.ProgramSchedule.ProgramId })
             .Select(po => po.Key.SeasonId).Distinct().ToList();

            var coupons = GetList().Where(cup => cartCoupons.Contains(cup.Id))
                .Where(cup => (cup.IsAllProgram && allSeasons.Contains(cup.SeasonId.Value)) || cup.Programs.Any(p => allPrograms.Contains(p.Id))).ToList();
            var isDiscountAppliedOnOrigin = true;
            if (cart.Order.Club != null && cart.Order.Club.Setting != null)
            {
                isDiscountAppliedOnOrigin = cart.Order.Club.Setting.IsDiscountApplyOnOriginPrice;
            }
            foreach (var coupon in coupons)
            {
                if (coupon.IsAllProgram)
                {
                    if (coupon.CouponType == CouponType.AllSchedule)
                        foreach (var item in order.RegularOrderItems)
                            AddCouponToOrderItem(item, coupon, isDiscountAppliedOnOrigin);
                    if (coupon.CouponType == CouponType.MostExpensiveSchedule)
                    {
                        var uniqItems = order.RegularOrderItems.Where(o => o.ProgramScheduleId.HasValue).GroupBy(item => item.ProgramSchedule.ProgramId)
                            .Select(item => item.OrderByDescending(i => i.EntryFee).FirstOrDefault());
                        foreach (var item in uniqItems)
                            AddCouponToOrderItem(item, coupon, isDiscountAppliedOnOrigin);
                    }
                    else if (coupon.CouponType == CouponType.MostExpensiveOrder)
                    {
                        var uniqItems = order.RegularOrderItems.Where(o => o.ProgramScheduleId.HasValue).GroupBy(item => item.ProgramSchedule.ProgramId)
                            .Select(item => item.OrderByDescending(i => i.EntryFee).FirstOrDefault());

                        var firstItem = uniqItems.FirstOrDefault();
                        var expensiveItem = new OrderItem();

                        foreach (var item in uniqItems)
                        {
                            if (item.EntryFee > firstItem.EntryFee)
                            {
                                expensiveItem = item;
                            }
                        }
                        if (expensiveItem.Id == 0)
                        {
                            expensiveItem = firstItem;
                        }

                        AddCouponToOrderItem(expensiveItem, coupon, isDiscountAppliedOnOrigin);

                    }
                }
                else
                {
                    if (coupon.CouponType == CouponType.AllSchedule)
                        foreach (var item in order.RegularOrderItems.Where(it => it.ProgramScheduleId.HasValue && coupon.Programs.Any(cp => cp.Id == it.ProgramSchedule.ProgramId)))
                            AddCouponToOrderItem(item, coupon, isDiscountAppliedOnOrigin);

                    if (coupon.CouponType == CouponType.MostExpensiveSchedule)
                    {
                        var uniqItems = order.RegularOrderItems.Where(it => it.ProgramScheduleId.HasValue && coupon.Programs.Any(cp => cp.Id == it.ProgramSchedule.ProgramId))
                            .GroupBy(item => item.ProgramSchedule.ProgramId).Select(item => item.OrderByDescending(i => i.EntryFee).FirstOrDefault()).ToList();
                        foreach (var item in uniqItems)
                            AddCouponToOrderItem(item, coupon, isDiscountAppliedOnOrigin);
                    }

                    else if (coupon.CouponType == CouponType.MostExpensiveOrder)
                    {
                        var uniqItems = order.RegularOrderItems.Where(it => it.ProgramScheduleId.HasValue && coupon.Programs.Any(cp => cp.Id == it.ProgramSchedule.ProgramId))
                            .GroupBy(item => item.ProgramSchedule.ProgramId).Select(item => item.OrderByDescending(i => i.EntryFee).FirstOrDefault()).ToList();

                        var firstItem = uniqItems.FirstOrDefault();
                        var expensiveItem = new OrderItem();

                        foreach (var item in uniqItems)
                        {
                            if (item.EntryFee > firstItem.EntryFee)
                            {
                                expensiveItem = item;
                            }
                        }
                        if (expensiveItem.Id == 0)
                        {
                            expensiveItem = firstItem;
                        }

                        AddCouponToOrderItem(expensiveItem, coupon, isDiscountAppliedOnOrigin);
                    }
                }
            }

            if (coupons.Any())
            {
                foreach (var item in order.OrderItems)
                {
                    item.CalculateTotalAmount();
                }

                _orderBusiness.Update(order);
            }
        }

        private void AddCouponToOrderItem(OrderItem item, Coupon coupon, bool isDiscountAppliedOnOrigin)
        {
            decimal discountAmount = item.CalculateDiscounts();

            if (!item.OrderChargeDiscounts.Any(i => !i.IsDeleted && i.CouponId == coupon.Id) && item.SeasonId == coupon.SeasonId)
            {
                decimal amount = 0;

                if (isDiscountAppliedOnOrigin)
                {
                    switch (coupon.CouponCalculateType)
                    {
                        case CalculationType.OnlyTuition:
                            {
                                var couponamount = coupon.AmounType == ChargeDiscountType.Fixed ? coupon.Amount : ((item.EntryFee * coupon.Amount) / 100);
                                amount = (item.EntryFee - discountAmount) < couponamount ? item.EntryFee - discountAmount : couponamount;
                            }
                            break;
                        case CalculationType.TotalAmount:
                            {
                                var totalAmount = item.CalculateTotalAmount();
                                if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience || c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge))
                                {
                                    totalAmount -= item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience || c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge).Sum(c => c.Amount);
                                }

                                if (coupon.AmounType == ChargeDiscountType.Fixed)
                                    amount = totalAmount < coupon.Amount ? item.TotalAmount : coupon.Amount;
                                else
                                {
                                    amount = totalAmount * coupon.Amount / 100;
                                }
                            }
                            break;
                        case CalculationType.OnlyExtraServices:
                            {
                                var allServises = item.GetOrderChargeDiscounts().Where(o => o.Category == ChargeDiscountCategory.DayCare || o.Category == ChargeDiscountCategory.Lunch || o.Category == ChargeDiscountCategory.CustomCharge);
                                if (allServises.Any())
                                {
                                    var totalAmountServises = allServises.Sum(s => s.Amount);

                                    if (coupon.AmounType == ChargeDiscountType.Fixed)
                                        amount = totalAmountServises < coupon.Amount ? totalAmountServises : coupon.Amount;
                                    else if (totalAmountServises > 0)
                                    {
                                        amount = totalAmountServises * coupon.Amount / 100;
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    decimal couponAmount = 0;
                    if (item.OrderChargeDiscounts.Any(orcd => !orcd.IsDeleted && orcd.Category == ChargeDiscountCategory.Coupon))
                    {
                        couponAmount = item.GetOrderChargeDiscounts().Where(orcd => orcd.Category == ChargeDiscountCategory.Coupon).Sum(r => Math.Abs(r.Amount));
                    }

                    switch (coupon.CouponCalculateType)
                    {
                        case CalculationType.OnlyTuition:
                            {
                                var couponamount = coupon.AmounType == ChargeDiscountType.Fixed ? coupon.Amount : ((item.EntryFee * coupon.Amount) / 100);
                                amount = (item.EntryFee - discountAmount - couponAmount) < couponamount ? (item.EntryFee - discountAmount - couponAmount) : couponamount;
                            }
                            break;
                        case CalculationType.TotalAmount:
                            {
                                var totalAmount = item.CalculateTotalAmount();
                                if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience || c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge))
                                {
                                    totalAmount -= item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience || c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge).Sum(c => c.Amount);
                                }
                                if (coupon.AmounType == ChargeDiscountType.Fixed)
                                    amount = totalAmount < coupon.Amount ? item.TotalAmount : coupon.Amount;
                                else
                                {
                                    amount = totalAmount * coupon.Amount / 100;
                                }
                            }
                            break;
                        case CalculationType.OnlyExtraServices:
                            {
                                var allServises = item.GetOrderChargeDiscounts().Where(o => o.Category == ChargeDiscountCategory.DayCare || o.Category == ChargeDiscountCategory.Lunch || o.Category == ChargeDiscountCategory.CustomCharge);

                                if (allServises.Any())
                                {
                                    var totalAmountServises = allServises.Sum(s => s.Amount);

                                    if (coupon.AmounType == ChargeDiscountType.Fixed)
                                        amount = totalAmountServises < coupon.Amount ? totalAmountServises : coupon.Amount;
                                    else if (totalAmountServises > 0)
                                    {
                                        amount = totalAmountServises * coupon.Amount / 100;
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }

                if ((item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && item.Mode == OrderItemMode.Noraml)
                {
                    if (coupon.CouponCalculateType != CalculationType.OnlyExtraServices)
                    {
                        amount = _subsscriptionBusiness.CalculateSubscriptionCoupon(item, coupon);
                    } 
                }

                amount = CurrencyHelper.RoundCurrencyWithPenny(amount);

                if (amount > 0)
                {
                    item.OrderChargeDiscounts.Add(new OrderChargeDiscount()
                    {
                        Amount = -1 * Math.Abs(amount),
                        Category = ChargeDiscountCategory.Coupon,
                        CouponId = coupon.Id,
                        Coupon = coupon,
                        Name = coupon.Name,
                        Subcategory = ChargeDiscountSubcategory.Discount
                    });
                }
            }
        }
        public OperationStatus CopyInSeason(long couponId, long seasonId, int clubId, string newSeasonDomain, bool forwardDates, int? monthNum)
        {
            var dbCoupon =
                _couponRepository.GetList()
                    .AsNoTracking()
                    .SingleOrDefault(c => !c.Deleted && c.Id == couponId);
            if (dbCoupon != null)
            {
                dbCoupon.SeasonId = seasonId;

                dbCoupon.Season = null;

                if (forwardDates && monthNum.HasValue)
                {
                    dbCoupon.StartDate = dbCoupon.StartDate.AddMonths(monthNum.Value);
                    dbCoupon.EndDate = dbCoupon.EndDate.AddMonths(monthNum.Value);
                }

                if (!dbCoupon.IsAllProgram)
                {
                    var programs = dbCoupon.Programs.Select(p => p.Domain).ToList();
                    dbCoupon.Programs.Clear();

                    foreach (
                        var newProgram in
                            programs.Select(
                                program =>
                                    _programBusiness.Get(clubId, newSeasonDomain, program))
                                .Where(newProgram => newProgram != null))
                    {
                        dbCoupon.Programs.Add(newProgram);
                    }
                }

                return _couponRepository.Create(dbCoupon);
            }
            return new OperationStatus { Status = false };
        }
        private int GetCouponUseCount(int couponId, bool isTestMode)
        {
            int result = 0;

            result = _orderItemBusiness.GetList().Where(o => o.Order.IsLive != isTestMode)
                    .SelectMany(item => item.OrderChargeDiscounts)
                    .Count(i => !i.IsDeleted && i.CouponId == couponId);

            return result;
        }


        public OperationStatus CreateList(List<Coupon> coupons)
        {
            try
            {
                _dataContext.Set<Coupon>().AddRange(coupons);

                return _couponRepository.Save();
            }
            catch (Exception e)
            {
                return new OperationStatus { Status = false, Message = e.Message };
            }
        }
    }
}
