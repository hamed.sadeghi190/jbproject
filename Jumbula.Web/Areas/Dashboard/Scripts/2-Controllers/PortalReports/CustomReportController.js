﻿(function () {
    'use strict';

    angular.module('dashboardApp')
        .controller('CustomReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();

            $scope.MC.stateParams = $stateParams;

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            $scope.reportsDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                {
                    read: {
                        url: baseUrl + '/GetAllReports',
                        type: "POST",
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverFiltering: true,
                serverPaging: true,
                pageSize: 10,
            });

            $scope.reportListGridOptions = {
                dataSource: $scope.reportsDataSource,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                    buttonCount: 5
                },
                sortable: true,
                columns: [
                    {
                        field: "Title",
                        title: "Report name",
                        template: "<a ui-sref='CustomReportPortalEdit({customType:\"#= Type #\", reportId: #=Value#})'  class='block'> #= Title # </a>",
                    }, {
                        field: "Type",
                        title: "Report type",
                        template: "#if(Type == 'Admin') {# <span> Registration </span> #} else if(Type == 'Parent') {# <span> Profile </span> #}#"
                    }, {
                        field: "",
                        title: "",
                        template: "#if(Type == 'Admin') {# <a ui-sref='' class='block'><i class='icon-play on-left'></i>Run</a> #} else if(Type == 'Parent'){# <a ui-sref='CustomReportPortalRun({reportId:  #= Value #, reportType:\"Parent\", reportName:\"#= Title #\"})' class='block'><i class='icon-play on-left'></i>Run</a> #}#",
                    }, {
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				                    <ul><li ui-sref='CustomReportPortalEdit({customType:\"#= Type #\", reportId: #=Value#})' class='k-item k-state-default k-first' role='menuitem'>\
						                <span class='k-link jbi-pencil'>Edit</span>\
				                        </li>" +
                        "<li ng-click='deleteReport(#=Value#)' class='k-item k-state-default' role='menuitem'>\
						                <span class='k-link jbi-remove'>Delete</span>\
				                        </li>" +
                        "</ul>\
			                        </li>\
		                      </ul>",
                        attributes: {
                            class: "grid-actions"
                        }
                    }
                ],
            };

            $scope.deleteReport = function (reportId) {
                jbConfirmation.yes(function () {
                    $http.post(baseUrl + '/DeleteReport', { reportId: reportId }).success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $scope.reportListGridOptions.dataSource.read();
                        }
                    });
                }).confirm();


            }
        }])
        

})();
