﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ProviderFinancialReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;

        $scope.ProviderFinancialReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/ProviderFinancialRun',
                        type: "POST",
                        data: function () {
                            return {
                                model: model,
                                term: $scope.searchTerm,
                            };
                        },
                    },

                },
            change: function (e) {
            },
           
            schema: {
                total: "TotalCount",
                data: "DataSource",
                model: {
                    fields: {

                        EMRate: { type: "number" }
                    }
                }
            },
            aggregate: [
                    { field: "TotalRegistrations", aggregate: "sum" },
                     { field: "TotalProviderFundedScholarships", aggregate: "sum" },
                     { field: "TotalPaidEnrollments", aggregate: "sum" },
                     { field: "ClassFee", aggregate: "sum" },
                     { field: "TotalPaymentsCollected", aggregate: "sum" },
                     { field: "EMFeeDeducted", aggregate: "sum" },
                     { field: "ProviderPenaltiesDeducted", aggregate: "sum" },
                     { field: "ProviderPayment", aggregate: "sum" },
                     { field: "EMRate", aggregate: "average" },
            ],
            //serverFiltering: true,
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
            batch: true,
            resizable: true,
        });
          
        $scope.ProviderFinancialReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "Provider Financial Report.xlsx",
                proxyURL: "/Dashboard/CustomReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.ProviderFinancialReportsDataSource,
                sortable: { mode: 'single' },
                resizable: true,
                scrollable: true,
            columns: [
                {
                    field: "ClassName",
                    title: "Class name",
                    width: "200px",
                    footerTemplate: 'Total'
                },
                 {
                     field: "SchoolName",
                     title: "School name",
                     width: "200px",
                 },
                {
                    field: "TotalRegistrations",
                    title: "Total # of registrations",
                    width: "200px",
                    footerTemplate: ' #= kendo.toString(sum) #'
                },
                {
                    field: "TotalProviderFundedScholarships",
                    title: "Total # of provider-funded scholarships",                  
                    width: "300px",
                    footerTemplate: ' #= kendo.toString(sum) #'
                },
                {
                    field: "TotalPaidEnrollments",
                    title: "Total # of paid enrollments",
                    width: "200px",
                    footerTemplate: ' #= kendo.toString(sum) #'
                },
                {
                    field: "ClassFee",
                    title: "Class fee",
                    format: "{0:c2}",
                    width: "160px",
                    footerTemplate: ' #= kendo.toString(sum, "c2") #'
                },
                {
                    field: "TotalPaymentsCollected",
                    title: "Total payments collected",
                    format: "{0:c2}",
                    width: "200px",
                    footerTemplate: ' #= kendo.toString(sum, "c2") #'
                },              
                {
                    field: "EMRate",
                    title: "EM rate",
                    width: "200px",
                    footerTemplate: 'Average: #=kendo.toString(average,"n2")#'
                },
                {
                    field: "EMFeeDeducted",
                    title: "EM fee deducted",
                    format: "{0:c2}",
                    width: "150px",
                    footerTemplate: ' #= kendo.toString(sum, "c2") #'
                },
                {
                    field: "ProviderPenaltiesDeducted",
                    title: "Provider deductions",
                    width: "250px",
                    format: "{0:c2}",
                    footerTemplate: ' #= kendo.toString(sum, "c2") #'
                },
                {
                    field: "ProviderPayment",
                    title: "Provider payment",
                    format: "{0:c2}",
                    width: "150px",
                    footerTemplate: ' #= kendo.toString(sum, "c2") #'
                }]
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ProviderFinancialReportGrid").getKendoGrid().saveAsExcel();
        }

    }]);
})();