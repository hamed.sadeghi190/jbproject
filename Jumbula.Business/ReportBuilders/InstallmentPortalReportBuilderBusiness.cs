﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Model.Report;
using Jumbula.Core.Model.Report.Filter;
using System;
using System.Collections.Generic;

namespace Jumbula.Business.ReportBuilders
{
    public class InstallmentPortalReportBuilderBusiness : BaseReportBuilder
    {
        private readonly INewReportBusiness _reportBusiness;

        private int _partnerId;

        public InstallmentPortalReportBuilderBusiness(INewReportBusiness reportBusiness)
        {
            _reportBusiness = reportBusiness;
        }

        public override ReportName ReportName => ReportName.InstallmentPortal;
        public override PdfTemplateName? PdfTemplate => PdfTemplateName.Default;
        public override ExcelTemplateName? ExcelTemplate => ExcelTemplateName.Default;

        public override void Initiate()
        {
            _partnerId = ClubBaseInfo.Id;
        }

        public override List<IJbReportFilterElement> GetFilters()
        {
            var organizationFilter = new JbDropDownReportFilter
            {
                Id = 0,
                Name = ReportFilterElementName.Organization,
                Title = ReportFilterElementName.Organization.ToDescription(),
                SectionName = ReportConstants.Organization,
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.PrepareData,
                TargetFilter = ReportFilterElementName.Season,
                ParameterName = ReportConstants.ClubIdParam,
                Data = _reportBusiness.GetSchoolsAndProviders(_partnerId),
            };

            var seasonFilter = new JbDropDownReportFilter
            {
                Id = 1,
                Name = ReportFilterElementName.Season,
                Title = ReportFilterElementName.Season.ToDescription(),
                SectionName = ReportConstants.Organization,
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.NoAction,
                TargetFilter = ReportFilterElementName.Program,
                ParameterName = ReportConstants.SeasonIdParam,
            };

            var programFilter = new JbDropDownReportFilter()
            {
                Id = 2,
                Name = ReportFilterElementName.Program,
                Title = ReportFilterElementName.Program.ToDescription(),
                SectionName = ReportConstants.Organization,
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.NoAction,
                ParameterName = ReportConstants.ProgramIdParam,
                IsBr = true
            };

            var identifierFilter = new JbDropDownReportFilter()
            {
                Id = 3,
                Name = ReportFilterElementName.Identifier,
                Title = ReportFilterElementName.Identifier.ToDescription(),
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.NoAction,
                ParameterName = ReportConstants.ProgramIdParam,
                Data = _reportBusiness.GetIdentifierItems(),
            };

            var firstNameFilter = new JbTextBoxReportFilter()
            {
                Id = 4,
                Name = ReportFilterElementName.FirstName,
                Title = ReportFilterElementName.FirstName.ToDescription(),
                Type = ReportFilterType.TextBox,
                ActionFilter = ReportFilterAction.NoAction,
                ParameterName = ReportConstants.FirstName,
                ShowByFilterId = identifierFilter.Id,
                ShowByValue = ReportConstants.NameField,
                Width = ReportConstants.OneFourth
            };

            var lastNameFilter = new JbTextBoxReportFilter()
            {
                Id = 5,
                Name = ReportFilterElementName.LastName,
                Title = ReportFilterElementName.LastName.ToDescription(),
                Type = ReportFilterType.TextBox,
                ActionFilter = ReportFilterAction.NoAction,
                ParameterName = ReportConstants.LastName,
                ShowByFilterId = identifierFilter.Id,
                ShowByValue = ReportConstants.NameField,
                Width = ReportConstants.OneFourth,
                IsHr = true
            };

            var installmentPaymentModeFilter = new JbDropDownReportFilter()
            {
                Id = 6,
                Name = ReportFilterElementName.InstallmentPaymentMode,
                Title = ReportFilterElementName.InstallmentPaymentMode.ToDescription(),
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.NoAction,
                ParameterName = ReportConstants.Mode,
                Data = _reportBusiness.GetInstallmentPaymentModeItems()
            };


            return new List<IJbReportFilterElement>() {organizationFilter, seasonFilter, programFilter, identifierFilter, firstNameFilter, lastNameFilter, installmentPaymentModeFilter};
        }

        public override List<ReportHeader> GetHeaders()
        {
            return null;
        }

        public override List<ReportGridColumn> GetColumns()
        {
            throw new NotImplementedException();
        }

        public override PaginatedList<object> GetData()
        {
            throw new NotImplementedException();
        }
    }
}
