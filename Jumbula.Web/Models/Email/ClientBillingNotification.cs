﻿using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models.Email
{
    public class ClientBillingNotification
    {
        public ClientBillingNotification()
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
          
        }
        public string BillingId { get; set; }
        public string Description { get; set; }
        public string Received { get; set; }
        public string Text { get; set; }
        public string ClubName { get; set; }
        public bool isFail { get; set; }
        public string PeriodDate { get; set; }
        public string RootSiteUrl { get; set; }
        public string Brand { get; set; }
        public string LastFour { get; set; }
        public string TransactionType { get; set; }
        public string Amount { get; set; }
        public string TransactionDate { get; set; }
       
       
        public string TransactionId { get; set; }
        public string Status { get; set; }
      
    }

   
}