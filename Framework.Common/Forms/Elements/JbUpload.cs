﻿using Jb.Framework.Common.Forms.JsonFormatter;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbUpload : JbBaseElement, IInputElement
    {
        public virtual string Value
        {
            get;
            set;
        }
        public bool IsOriginalUploadedForm { get; set; }

        public string GetValue()
        {
            return Value;
        }
    }
}
