﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Models
{
    public class CreateClubStep1ViewModel
    {
        public CreateClubStep1ViewModel() { }
        public CreateClubStep1ViewModel(string token)
        {
            Token = token;
        }

        [Required(ErrorMessage = "{0} is required")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        [EmailAddress(ErrorMessage = "{0} is not valid")]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "{0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Organization name is required")]
        [DisplayName("Organization name")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(3, ErrorMessage = "Enter a complete organization name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [DisplayName("Home page URL")]
        [StringLength(64, ErrorMessage = "{0} must be at least {2} characters long", MinimumLength = 3)]

        [RegularExpression("[A-Za-z0-9-]+",
               ErrorMessage = "{0} cannot contain any spaces or special characters such as *, &, !, _")]
        public string Domain { get; set; }
        [RegularExpression(Jumbula.Common.Constants.Validation.UrlWitoutHttpRegex, ErrorMessage = "{0} is not a valid URL")]
        [Display(Name = "Website")]
        public string Site { get; set; }
        public string Token { get; set; }
        [Display(Name = "Phone")]
        [RegularExpression(@"[0-9+\-()\s]+$", ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876)")]
        [StringLength(18, ErrorMessage = "{0} is too long")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code")]
        public string Phone { get; set; }
    }

    public class CreateClubStep2ViewModel
    {
        public CreateClubStep2ViewModel() { }
        public CreateClubStep2ViewModel(string token)
        {
            Token = token;
        }
        public string Token { get; set; }

        public string Domain { get; set; }
        [Display(Name = "First name")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete first name.")]
        public string FirstName { get; set; }


        [Display(Name = "Last name")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete last name.")]
        public string LastName { get; set; }


        [DisplayName("Address")]
        [Required(ErrorMessage = "{0} is required")]
        [MinLength(10, ErrorMessage = "Address is not valid.")]
        public string Address { get; set; }

        //[Required]
        [Display(Name = "Time zone")]
        public Jumbula.Common.Enums.TimeZone TimeZone { get; set; }

        public SelectList HowDidYouHear
        {
            get
            {
                return new SelectList(DropdownHelpers.ToSelectList<HowToHearAboutUs>(), dataValueField: "Value", dataTextField: "Text", selectedValue: "");
            }
        }

        [Required(ErrorMessage = "How did you hear about us?")]
        public string SelectedHowDidYouHear { get; set; }

        public SelectList Revenue => Ioc.PricePlanBusiness.GetClubRevenueList();

        [Required(ErrorMessage = "Annual revenue is required.")]
        public string SelectedRevenue { get; set; }
    }
    public class CreateClubViewModel : IValidatableObject
    {

        [Required]
        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string Password { get; set; }

        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Url]
        [DataType(DataType.Url)]
        [Display(Name = "Club website")]
        public string Site { get; set; }

        [Required]
        [DisplayName("Club name")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(3, ErrorMessage = "Enter a complete {0}")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Club page name")]
        [StringLength(64, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [RegularExpression("[A-Za-z0-9-]+",
             ErrorMessage = "{0} cannot contain any spaces or special characters such as *, &, !, _, etc")]
        public string Domain { get; set; }

        [DisplayName("Club description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        public ContactPersonViewModel ContactPerson { get; set; }


        [Required]
        [DisplayName("Address")]
        public string Address { get; set; }

        [DisplayName("Convinence Fee")]
        public decimal ConvinenceFee { get; set; }

        public int Category { get; set; }

        [Required]
        [Display(Name = "Time zone")]
        public Jumbula.Common.Enums.TimeZone TimeZone { get; set; }
        public string Phone { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Paypal email address")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string PaypalEmail { get; set; }

        [Display(Name = "Enable Cache Payment")]
        public bool EnableCashPayment { get; set; }

        [Display(Name = "Enable Donation")]
        public bool EnableDonation { get; set; }

        [Display(Name = "Can player edit order")]
        public bool CanPlayerEditOrder { get; set; }

        [Display(Name = "Has Chess Tourney")]
        public bool HasChessTourney { get; set; }

        [Display(Name = "Is SandBox Mode")]
        public bool IsSandBoxMode { get; set; }



        public string OrganizationName { get; set; }

        public CreateClubViewModel()
        {
            ContactPerson = new ContactPersonViewModel();

        }

        public Club ToClub()
        {
            Club club = new Club();

            club.Name = Name.Replace(Constants.S_DoubleQuote, string.Empty);
            club.Name = club.Name.Replace(Constants.S_BackSlash, string.Empty);

            club.Domain = Domain;
            club.Description = Description;

            if (string.IsNullOrEmpty(Description))
            {
                club.Description = string.Empty;
            }

            this.ContactPerson = new ContactPersonViewModel()
            {
                Email = this.Email,
                FirstName = this.FirstName,
                LastName = this.LastName,
            };

            var contactPerson = ContactPerson.ToModel<ContactPerson>();
            contactPerson.IsPrimary = true;

            club.ContactPersons.Add(contactPerson);
            club.Site = Site;
            club.TimeZone = this.TimeZone;

            club.Client = new Client()
            {

                EnableDonation = this.EnableDonation,
                HasChessTourney = this.HasChessTourney,
                CanPlayerEditOrder = this.CanPlayerEditOrder,
                ConvenienceFee = this.ConvinenceFee,

            };
            club.Client.PaymentMethods = new ClientPaymentMethod()
            {
                EnableCashPayment = this.EnableCashPayment,
                EnablePaypalPayment = true,
                IsPaypalTestMode = this.IsSandBoxMode,
                PaypalEmail = this.PaypalEmail
            };
            return club;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var clubDb = Ioc.ClubBusiness;

            if (clubDb.IsClubDomainExist(Domain))
            {
                yield return new ValidationResult(string.Format(Constants.F_ClubPageNameNotAvailable, Domain), new string[] { "Domain" });
            }
        }
    }


}

