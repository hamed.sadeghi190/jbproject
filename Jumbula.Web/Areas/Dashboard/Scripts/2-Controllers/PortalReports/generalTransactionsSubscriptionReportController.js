﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('generalTransactionsSubscriptionReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', 'reportService', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, reportService) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();
        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;


        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/FillFinanceFilters')
            .success(function (data, status, headers, config) {
                if (is.undefined($scope.Model.Filters) || $scope.Model.ReportName != $scope.MC.stateParams.reportName) {
                    $scope.Model.Filters = {};
                    $scope.Model.Filters.TransactionFilters = {};
                }
                $scope.AllMembers = data.AllMembers;
                $scope.memberId = "";
                $scope.Model.Filters.TransactionFilters.TransactionCategories = data.TransactionsCategories;
                $scope.Model.Filters.TransactionFilters.PaymentMethods = data.PaymentMethods;
                $scope.Model.Filters.TransactionFilters.GroupBy = data.GroupBy;
                $scope.Model.Filters.TransactionFilters.PaymentMethod = "";
                $scope.Model.Filters.TransactionFilters.TransactionCategory = "";
                $scope.Model.Filters.TransactionFilters.IsGroupBy = false;

                prepareGrid();
            }).error(function (data, status, headers, config) {
                //alert(data);
            });

        var prepareGrid = function () {
            $scope.transactionReportsDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/GeneralReportSubscriptionRun',
                            type: "POST",
                            data: function () {
                                return {
                                    seasonDomain: $scope.MC.stateParams.seasonDomain,
                                    filters: $scope.Model.Filters,
                                    memberId: $scope.memberId,
                                    isGroupBy: $scope.Model.Filters.TransactionFilters.IsGroupBy,
                                    isExcelExport: false
                                };
                            },
                        },

                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                batch: true,
                resizable: true,
            });

            $scope.TransactionReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "General transactions.xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.transactionReportsDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    customReportService.initializeDoubleScroll();
                },
                columnMenu: true,
                sortable: true,
                columns: [
                    {
                        field: "ClubName",
                        title: "Organiziation",
                        width: "150px",
                        lockable: true
                    },
                    {
                        field: "FullName",
                        title: "Name",
                        width: "150px",
                        lockable: true,
                    },
                    {
                        field: "SeasonName",
                        title: "Season",
                        width: "150px",
                        lockable: true,
                    },
                    {
                        field: "ProgramName",
                        title: "Program",
                        width: "160px"
                    },
                    {
                        field: "ProgramType",
                        title: "Program type",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "ProgramScheduleTitle",
                        title: "Schedule name",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "UserEmail",
                        title: "Account email",
                        width: "150px",
                        lockable: true,
                    },
                    {
                        field: "Date",
                        title: "Transaction date",
                        width: "150px",
                        lockable: true,
                    },
                    {
                        field: "PayerEmail",
                        title: "Payer email",
                        width: "150px",
                        lockable: true,
                    },
                    {
                        field: "Confirmation",
                        title: "Confirmation",
                        width: "140px",
                        lockable: true,
                    },
                    {
                        field: "StrTransactionType",
                        title: "Transaction type",
                        width: "140px",
                        lockable: true,
                    },
                    {
                        field: "TransactionCategory",
                        title: "Transaction category",
                        width: "155px",
                        lockable: true,
                    },
                    // I put it here.
                    {
                        field: "CheckId",
                        title: "Transaction ID",
                        width: "180px",
                        lockable: true,
                    },
                    {
                        field: "PaymentMethod",
                        title: "Payment method",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "PaypalHandler",
                        title: "Payment handler",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Note",
                        title: "Memo",
                        lockable: true,
                    },
                    {
                        field: "Payment",
                        title: "Payment",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Charge",
                        title: "Deposit",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "ListPrice",
                        title: "List price",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Discount",
                        title: "Discount",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "DiscountName",
                        title: "Discount names",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "Fees",
                        title: "Fee",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "FeeName",
                        title: "Fee names",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "StartDate",
                        title: "Start date",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "EndDate",
                        title: "End date",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "ProgramStartDate",
                        title: "Program start date",
                        width: "160px",
                        lockable: true,
                    },
                    {
                        field: "ProgramEndDate",
                        title: "Program end date",
                        width: "160px",
                        lockable: true,
                    },
                ],
            }
        }

        $scope.saveToExcel = function () {

            cfpLoadingBar.start();

            $http.post(baseUrl + '/GeneralReportSubscriptionRun', {
                filters: $scope.Model.Filters,
                memberId: $scope.memberId,
                isGroupBy: $scope.Model.Filters.TransactionFilters.IsGroupBy,
                isExcelExport: true
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {

                    reportService.exportExcelData(data, "Transaction");
                    cfpLoadingBar.complete();

                }).error(function (data, status, headers, config) {
                    cfpLoadingBar.complete();

                });

        }

        $scope.showGrid = false;
        $scope.runReport = function () {

            $scope.showGrid = true;
            $scope.TransactionReportGridOptions.dataSource.read();
        }

        $scope.filterData = function () {
            customReportService.setModelInStorage($scope.Model);
            $scope.TransactionReportGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.TransactionFilters.TransactionCategory = {},
                $scope.Model.Filters.TransactionFilters.PaymentMethod = {};
            $scope.searchTerm = "";
            $scope.memberId = "";
            $scope.TransactionReportGridOptions.dataSource.read();
        }

        $scope.emailReport = function () {

            var reportModel = {
                MemberId: $scope.memberId,
                Filters: $scope.Model.Filters,
            };

            customReportService.setModelInStorage(reportModel);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: 'TransactionSubscriptionFinance' });
        }
    }]);
})();