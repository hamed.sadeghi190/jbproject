ALTER TABLE [dbo].[ClientPaymentMethods] add [DefaultPaymentGateway] [int] not NULL DEFAULT(0)
ALTER TABLE [dbo].[ClientPaymentMethods] add [AuthorizeTransactionKey] nvarchar(128)  null  
ALTER TABLE [dbo].[ClientPaymentMethods] add [AuthorizeLoginId] nvarchar(128)  null
ALTER TABLE [dbo].[ClientPaymentMethods] add [EnableAuthorizePayment] [bit] not NULL DEFAULT(0)

ALTER TABLE [dbo].[UserCreditCards] add [TypePaymentGateway] [int] NULL DEFAULT(0) 

EXEC sp_RENAME 'UserCreditCards.StripeCustomerId', 'CustomerId', 'COLUMN'
EXEC sp_RENAME 'UserCreditCards.StripeCustomerResponse', 'CustomerResponse', 'COLUMN'

ALTER TABLE [dbo].[UserCreditCards] add [CreatedDate] [datetime] NULL
ALTER TABLE [dbo].[UserCreditCards] add [UpdatedDate] [datetime] NULL
ALTER TABLE [dbo].[UserCreditCards] add [DeletedDate] [datetime] NULL
ALTER TABLE [dbo].[UserCreditCards] add [DefaultDate] [datetime] NULL

EXEC sp_RENAME 'ClientPaymentMethods.StripeCheckoutLabel', 'CreditCardCheckoutLabel', 'COLUMN'
EXEC sp_RENAME 'ClientPaymentMethods.IsStripeTestMode', 'IsCreditCardTestMode', 'COLUMN'
EXEC sp_RENAME 'ClientPaymentMethods.StripePreferredLabel', 'CreditCardPreferredLabel', 'COLUMN'