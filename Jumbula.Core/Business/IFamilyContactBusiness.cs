﻿using Jb.Framework.Common;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IFamilyContactBusiness
    {
        OperationStatus Create(FamilyContact entity);
        FamilyContact GetFamilyContact(int Id);
        OperationStatus Delete(FamilyContact familyContact);
        OperationStatus DeleteFamilyContact(int id);
        List<FamilyContact> GetAuthorizedPickups(int userId);
        List<FamilyContact> GetEmergencyContacts(int userId);
    }
}
