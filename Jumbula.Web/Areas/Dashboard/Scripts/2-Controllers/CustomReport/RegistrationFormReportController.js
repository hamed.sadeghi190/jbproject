﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('RegistrationFormReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/SeasonBlade", order: 1, menu: "SEASONS" });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.MC.stateParams = $stateParams;

        $scope.Model = customReportService.get();

        var model = $scope.Model;

        cfpLoadingBar.start();
        $http.post(baseUrl + '/RegistrationFormReportRun', { model: model })
            .success(function (data, status, headers, config) {
                window.location.href = data.Data;
            });

    }]);
})();