declare  @catagoryName nvarchar(128);
declare  @parentCategory nvarchar(128);
declare @parentId int;

-- Set category name and parent name before run query
set @catagoryName ='Boy Scout Membership';
set @parentCategory = 'Outdoors';

select @parentId=Id from Categories where Name = @parentCategory;

INSERT INTO [dbo].[Categories]
           ([ParentId]
           ,[Name]
           ,[LogoUri]
           ,[IsPrimary]
           ,[SupportTourney]
           ,[Configuration]
           ,[Order])
     VALUES
           (@parentId
           ,@catagoryName
           ,null
           ,0
           ,0
           ,null
           ,null)
GO

