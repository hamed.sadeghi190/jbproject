﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Email
{
    public enum SendType
    {
        Create = 1,
        Edit = 2,
        Cancel = 3
    }
}