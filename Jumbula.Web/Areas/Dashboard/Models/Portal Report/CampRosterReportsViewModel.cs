﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CampRosterReportsViewModel
    {
        public CampRosterReportsViewModel()
        {

        }

        public CampRosterReportsViewModel(OrderItem orderItem, PlayerProfile participant, PlayerProfile parent, FamilyContact authorize1, FamilyContact authorize2, FamilyContact emergency)
        {
            ParticipantName = participant != null && participant.Contact != null ? string.Format("{0}, {1}", participant.Contact.LastName, participant.Contact.FirstName) : string.Empty;
            Gender = participant != null && participant.Contact != null ? (participant.Gender == GenderCategories.Male || participant.Gender == GenderCategories.Female || participant.Gender == GenderCategories.GenderNeutral) ? participant.Gender.ToDescription() : string.Empty : string.Empty;
            Grade = participant != null && participant.Info != null ? participant.Info.Grade != 0 ? participant.Info.Grade.ToDescription() : string.Empty : string.Empty;
            ParentName = parent != null && parent.Contact != null ? string.Format("{0}, {1}", parent.Contact.LastName, parent.Contact.FirstName) : string.Empty;
            ParentPhone = parent != null && parent.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(parent.Contact.Phone) : string.Empty;
            ParentEmail = parent != null && parent.Contact != null ? parent.Contact.Email : string.Empty;
            EmergencyContactName = emergency != null && emergency.Contact != null ? string.Format("{0}, {1}", emergency.Contact.LastName, emergency.Contact.FirstName) : string.Empty;
            EmergencyContactPhone = emergency != null && emergency.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(emergency.Contact.Phone) : string.Empty;
            AuthorizeAdult1Name = authorize1 != null && authorize1.Contact != null ? string.Format("{0}, {1}", authorize1.Contact.LastName, authorize1.Contact.FirstName) : string.Empty;
            AuthorizeAdult1Phone = authorize1 != null && authorize1.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorize1.Contact.Phone) : string.Empty;
            AuthorizeAdult2Name = authorize2 != null && authorize2.Contact != null ? string.Format("{0}, {1}", authorize2.Contact.LastName, authorize2.Contact.FirstName) : string.Empty;
            AuthorizeAdult2Phone = authorize2 != null && authorize2.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(authorize2.Contact.Phone) : string.Empty;
            MedicalCondition = participant != null && participant.Info != null ? participant.Info.SpecialNeeds : string.Empty;
            Allergies = participant != null && participant.Info != null ? participant.Info.Allergies : string.Empty;
            ScheduleName = orderItem.ProgramSchedule.StartDate.ToString(Constants.DefaultDateFormat) + "-" +
                         orderItem.ProgramSchedule.EndDate.ToString(Constants.DefaultDateFormat);
            TuitionName = orderItem.EntryFeeName;
        }



        public string ParticipantName { get; set; }
        public string Gender { get; set; }
        public string Grade { get; set; }
        public string ParentName { get; set; }
        public string ParentEmail { get; set; }
        public string ParentPhone { get; set; }
        public string AuthorizeAdult1Name { get; set; }
        public string AuthorizeAdult1Phone { get; set; }
        public string AuthorizeAdult2Name { get; set; }
        public string AuthorizeAdult2Phone { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string MedicalCondition { get; set; }
        public string Allergies { get; set; }
        public string ScheduleName { get; set; }
        public string TuitionName { get; set; }

        
    }
}