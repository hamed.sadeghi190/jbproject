﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FillDataViewModel
    {
        public string SchoolId { get; set; }

        public string SeasonId { get; set; }
    }
}