﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ReconciliationDetails
    {
        [Description("Cash payments")]
        CashPayments,
        [Description("Donations")]
        Donations,
        [Description("PTA fees")]
        PTAFees,
        [Description("PTA scholarships")]
        PTAScholarships
    }
}
