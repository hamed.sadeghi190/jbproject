﻿using LINQtoCSV;
using System.IO;

namespace Jumbula.Common.Helper
{
    public static class CsvHelper
    {
        public static bool IsInCorrectFormat<T>(this CsvContext csv, StreamReader stream) where T : class, new()
        {
            try
            {
                csv.Read<T>(stream);
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
