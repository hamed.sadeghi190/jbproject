﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Report;
using Jumbula.Core.Model.Report.Filter;
using System.Collections.Generic;

namespace Jumbula.Business.ReportBuilders
{
    public abstract class BaseReportBuilder: IReportBuilderBusiness
    {
        private INewReportBusiness _reportBusiness;

        public INewReportBusiness ReportBusiness
        {
            get => _reportBusiness;
            set => _reportBusiness = value;
        }

        protected Dictionary<string, object> Headers { get; set; }
        protected string FilterView { get; set; }
        protected JbReportFilter Filters { get; set; }
        protected List<ReportExportElement> Exports { get; set; }
        protected virtual string Title => ReportName.ToDescription();
        protected ReportGridModel ReportGridModel { get; set; }
        protected ReportModel Model { get; set; }


        public abstract ReportName ReportName { get; }
        public abstract PdfTemplateName? PdfTemplate { get; }
        public abstract ExcelTemplateName? ExcelTemplate { get; }

        public abstract void Initiate();

        public virtual ReportModel GetSchema(Dictionary<string, object> parameters, ClubBaseInfoModel clubBaseInfo)
        {
            Parameters = parameters;
            ClubBaseInfo = clubBaseInfo;
            Initiate();

            InitializeHeader(GetHeaders());
            InitializeFilter(GetFilters());
            InitializeExport();
            InitializeReportGridModel(GetReportGrid());

            InitializeReportModel();
            return Model;
        }

        public virtual ReportDataModel BindData(Dictionary<string, object> parameters, ClubBaseInfoModel clubBaseInfo,
            PaginationModel paginationModel, List<ReportGridSort> sort = null)
        {
            Parameters = parameters;
            ClubBaseInfo = clubBaseInfo;
            PaginationModel = paginationModel;
            Sort = sort;

            return new ReportDataModel(GetData(), GetColumns());
        }

        public object GetParameter(Dictionary<string, object> parameters, string parameterName)
        {
            object value = null;

            if (parameters.ContainsKey(parameterName))
                parameters.TryGetValue(parameterName, out value);

            return value;
        }

        public void InitializeReportModel()
        {
            Model = new ReportModel(Title, Headers, Exports, ReportGridModel, FilterView, Filters);
        }

        public void InitializeFilter(IEnumerable<IJbReportFilterElement> filters)
        {
            var jbReportFilter = new JbReportFilter();

            jbReportFilter.Elements.AddRange(filters);

            FilterView = ViewHelper.RenderViewToString($"{ReportConstants.EditorTemplates}/{ReportConstants.JbReportFilter}", jbReportFilter);

            Filters = jbReportFilter;
        }

        public void InitializeExport()
        {
            var exports = new List<ReportExportElement>();
            if (PdfTemplate.HasValue)
            {
                exports.Add(new ReportExportElement() { Name = ReportConstants.Pdf, Template = PdfTemplate.ToDescription() });
            }

            if (ExcelTemplate.HasValue)
            {
                exports.Add(new ReportExportElement() { Name = ReportConstants.Excel, Template = ExcelTemplate.ToString() });
            }

            Exports = exports;
        }

        private void InitializeReportGridModel(ReportGridModel reportGrid)
        {
            ReportGridModel = reportGrid ?? new ReportGridModel();
        }

        public void InitializeHeader(List<ReportHeader> reportHeaders)
        {
            if (reportHeaders != null)
            {
                Parameters.Add(ReportConstants.ClubIdParam, ClubBaseInfo.Id);
                Headers = _reportBusiness.GetReportHeaders(Parameters, reportHeaders);
            }
        }


        public virtual Dictionary<string, object> Parameters { get; set; }
        public virtual ClubBaseInfoModel ClubBaseInfo { get; set; }
        public virtual List<ReportGridSort> Sort { get; set; }
        public virtual PaginationModel PaginationModel { get; set; }
        public abstract List<IJbReportFilterElement> GetFilters();
        public abstract List<ReportHeader> GetHeaders();

        public virtual ReportGridModel GetReportGrid()
        {
            return null;
        }
        public abstract List<ReportGridColumn> GetColumns();
        public abstract PaginatedList<object> GetData();
    }
}
