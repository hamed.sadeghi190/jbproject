﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Newtonsoft.Json;

namespace Jumbula.Core.Domain
{
    public class StudentAttendance : BaseEntity
    {
        [ForeignKey("OrderSessionId")]
        public virtual OrderSession OrderSession { get; set; }
        public long OrderSessionId { get; set; }

        [ForeignKey("AttendanceStaffId")]
        public virtual ClubStaff AttendanceStaff { get; set; }

        public int? AttendanceStaffId { get; set; }

        public AttendanceStatus? AttendanceStatus { get; set; }

        public DateTime? AttendanceDate { get; set; }

        [ForeignKey("DismissalStaffId")]
        public virtual ClubStaff DismissalStaff { get; set; }

        public int? DismissalStaffId { get; set; }

        public bool IsDismissed { get; set; }

        [Column("DismissalInfo")]
        [StringLength(4000)]
        public string DismissalInfoSerialized { get; set; }

        StudentAttendanceDismissalInfo _dismissalInfo;

        [NotMapped]
        public StudentAttendanceDismissalInfo DismissalInfo
        {
            get
            {
                if (_dismissalInfo == null && !string.IsNullOrEmpty(DismissalInfoSerialized))
                {
                    _dismissalInfo = JsonConvert.DeserializeObject<StudentAttendanceDismissalInfo>(DismissalInfoSerialized);
                }

                return _dismissalInfo;
            }

        }
        public DateTime? DismissalDate { get; set; }
    }

    public class StudentAttendanceDismissalInfo
    {
        public string FullName { get; set; }
        public DateTime LocalDateTime { get; set; }
        public string DismissalFromEnrichment { get; set; }
        public string Phone { get; set; }
    }

}
