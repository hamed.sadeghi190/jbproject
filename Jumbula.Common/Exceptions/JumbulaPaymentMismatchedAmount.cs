﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaPaymentMismatchedAmount : JumbulaPaymentException
    {
        public JumbulaPaymentMismatchedAmount(Exception e)
            : base(e)
        {
            throw new Exception(e.ToString());
        }
    }
}
