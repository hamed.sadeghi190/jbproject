﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaTokenAlreadyConfirmedException : JumbulaAccountException
    {
        public JumbulaTokenAlreadyConfirmedException(Exception e)
            : base(e)
        {
        }
    }
}
