﻿
namespace Jumbula.Core.Model.Email
{
    public class RefundEmailParameterModel 
    {
        public long? OrderItemId { get; set; }
        public long? OrderInstallmentId { get; set; }
        public decimal RefundedAmount { get; set; }
        public string RefundNote { get; set; }
    }
}
