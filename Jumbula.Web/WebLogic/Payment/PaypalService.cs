﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.WebConfig;
using Newtonsoft.Json;
using Serilog;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;
using static Jumbula.Common.Constants.Payment;

namespace Jumbula.Web.WebLogic.Payment
{
    public class PaypalService : IPaymentGatewayService
    {
        public PaypalService(string clubDomain, bool isLive)
        {
            ClubDomain = clubDomain;
            isSandBox = true;

            Club club = null;
            if (!string.IsNullOrEmpty(clubDomain))
            {
                club = Ioc.ClubBusiness.Get(clubDomain);
            }

            if (club != null && club.Client != null)
            {
                var client = club.Client;

                if (club != null && club.PartnerId.HasValue && club.PartnerClub != null && club.IsSchool)
                {
                    if (club.PartnerClub.Client != null)
                    {
                        client = club.PartnerClub.Client;
                    }
                    else
                    {
                        client = Ioc.ClientBusiness.Get(club.PartnerId.Value);
                    }
                }
                isSandBox = (client.PaymentMethods.IsPaypalTestMode) || (isLive == false);
                PaypalEmail = isSandBox ? Constants.W_Sandbox_Paypal_Email : client.PaymentMethods.PaypalEmail;
            }
        }

        private string ClubDomain;
        private string PaypalEmail = Constants.W_Sandbox_Paypal_Email;
        private const string REQUESTMETHOD = "POST";
        private const int DEFAULT_TIMEOUT = 3600000;
        private const string CONTENT_TYPE = "text/json";
        private const string XPAYPALSERVICEVERSION = "X-PAYPAL-SERVICE-VERSION";
        private const string XPAYPALSECURITYUSERID = "X-PAYPAL-SECURITY-USERID";
        private const string XPAYPALSECURITYPASSWORD = "X-PAYPAL-SECURITY-PASSWORD";
        private const string XPAYPALSECURITYSIGNATURE = "X-PAYPAL-SECURITY-SIGNATURE";
        private const string XPAYPALDEVICEIPADDRESS = "X-PAYPAL-DEVICE-IPADDRESS";
        private const string XPAYPALREQUESTDATAFORMAT = "X-PAYPAL-REQUEST-DATA-FORMAT";
        private const string XPAYPALRESPONSEDATAFORMAT = "X-PAYPAL-RESPONSE-DATA-FORMAT";
        private const string XPAYPALAPPLICATIONID = "X-PAYPAL-APPLICATION-ID";
        private const string PAYPALREQUESTDATAFORMAT = "JSON";
        private const string PAYPALRESPONSEDATAFORMAT = "JSON";



        private HttpWebRequest objRequest = null;


        public string PreapprovalDetails(string preapprovalKey)
        {
            PreapprovalDetailsRequest pdRequest = new PreapprovalDetailsRequest(preapprovalKey);
            payLoad = null;
            try
            {
                payLoad = JsonConvert.SerializeObject(pdRequest);
                apiProfile = new PaypalAPIProfile(isSandBox)
                {
                    RequestDataformat = PAYPALREQUESTDATAFORMAT,
                    ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                    ApiOperation = PaypalAPIOperations.PreapprovalDetails,
                    DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
                };
                return CallAPI();
            }
            catch
            {

                throw;
            }

        }

        public string SetPaymentOptions(List<OrderItem> orderItems, string payKey, int clubId)
        {
            var club = Ioc.ClubBusiness.Get(clubId);
            Client client = null;
            if (club.PartnerId.HasValue && club.PartnerClub != null && club.IsSchool)

            {
                if (club.PartnerClub.Client != null)
                {
                    client = club.PartnerClub.Client;
                }
                else
                {
                    client = Ioc.ClientBusiness.Get(club.PartnerId.Value);
                }
            }
            else
            {
                client = club.Client;
            }
            SetPaymentOptionsRequest request = new SetPaymentOptionsRequest();
            request.payKey = payKey;
            request.senderOptions = new SenderOptions()
            {
                referrerCode = "Jumbula_SP"
            };
            List<InvoiceItem> invoiceItems = new List<InvoiceItem>();

            foreach (var item in orderItems)
            {
                invoiceItems.Add(new InvoiceItem()
                {
                    identifier = item.Id.ToString(),
                    name = item.Name,
                    price = item.TotalAmount
                });
            }
            InvoiceData invoiceData = new InvoiceData(invoiceItems);
            ReceiverOptions receiverOption = new ReceiverOptions(invoiceData, PaypalEmail);
            request.receiverOptions = new List<ReceiverOptions>();
            request.receiverOptions.Add(receiverOption);
            payLoad = null;
            try
            {
                payLoad = JsonConvert.SerializeObject(request);
                apiProfile = new PaypalAPIProfile(isSandBox)
                {
                    RequestDataformat = PAYPALREQUESTDATAFORMAT,
                    ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                    ApiOperation = PaypalAPIOperations.SetPaymentOptions,
                    DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
                };
                return CallAPI();


            }
            catch
            {

                throw;
            }
        }
        public string paymentDetails(string payKey)
        {
            PaymentDetailRequest pdRequest = new PaymentDetailRequest(payKey);
            payLoad = null;
            try
            {
                payLoad = JsonConvert.SerializeObject(pdRequest);
                apiProfile = new PaypalAPIProfile(isSandBox)
                {
                    RequestDataformat = PAYPALREQUESTDATAFORMAT,
                    ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                    ApiOperation = PaypalAPIOperations.PaymentDetails,
                    DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
                };
                return CallAPI();


            }
            catch
            {

                throw;
            }

        }
        public string paymentDetailsWithTrackingId(string trackingId)
        {
            PaymentDetailRequest pdRequest = new PaymentDetailRequest();
            pdRequest.trackingId = trackingId;
            payLoad = null;
            try
            {
                payLoad = JsonConvert.SerializeObject(pdRequest);
                apiProfile = new PaypalAPIProfile(isSandBox)
                {
                    RequestDataformat = PAYPALREQUESTDATAFORMAT,
                    ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                    ApiOperation = PaypalAPIOperations.PaymentDetails,
                    DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
                };
                return CallAPI();


            }
            catch
            {

                throw;
            }

        }
        public string Preapproval(string returnUrl, string cancelUrl, string ipnNotificationUrl, string currency)
        {

            PreapprovalRequest request = new PreapprovalRequest();
            request.requestEnvelope = new RequestEnvelope();
            request.cancelUrl = cancelUrl;
            request.returnUrl = returnUrl;
            request.ipnNotificationUrl = ipnNotificationUrl;

            request.currencyCode = currency;
            request.startingDate = DateTime.UtcNow.AddDays(1);
            request.endingDate = DateTime.UtcNow.AddYears(1);
            request.maxTotalAmountOfAllPayments = Constants.Default_Paypal_MaxPreapprovalAmount;
            payLoad = JsonConvert.SerializeObject(request);
            apiProfile = new PaypalAPIProfile(isSandBox)
            {
                RequestDataformat = PAYPALREQUESTDATAFORMAT,
                ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                ApiOperation = PaypalAPIOperations.Preapproval,
                DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
            };
            return CallAPI();

        }
        public GetPaymentOptionsResponse GetPaymentDetailOption(string payKey)
        {
            GetPaymentOptionsRequest pdRequest = new GetPaymentOptionsRequest();
            pdRequest.payKey = payKey;
            pdRequest.requestEnvelope = new RequestEnvelope();

            payLoad = null;
            try
            {
                payLoad = JsonConvert.SerializeObject(pdRequest);
                apiProfile = new PaypalAPIProfile(isSandBox)
                {
                    RequestDataformat = PAYPALREQUESTDATAFORMAT,
                    ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                    ApiOperation = PaypalAPIOperations.GetPaymentOptions,
                    DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
                };
                var res = CallAPI();

                return JsonConvert.DeserializeObject<GetPaymentOptionsResponse>(res);

            }
            catch
            {

                throw;
            }

        }
        public string ExecutePayment(string payKey)
        {
            ExecutePaymentRequest pdRequest = new ExecutePaymentRequest();
            pdRequest.payKey = payKey;
            pdRequest.requestEnvelope = new RequestEnvelope();

            payLoad = null;
            try
            {
                payLoad = JsonConvert.SerializeObject(pdRequest);
                apiProfile = new PaypalAPIProfile(isSandBox)
                {
                    RequestDataformat = PAYPALREQUESTDATAFORMAT,
                    ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                    ApiOperation = PaypalAPIOperations.PaymentDetails,
                    DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
                };
                return CallAPI();


            }
            catch
            {

                throw;
            }

        }
        public string Pay(int clubId, decimal totalAmount, string cid, string returnUrl, string cancelUrl, string ipnNotificationUrl, JumbulaSubSystem caller, long callerId, string note, string preapprovalKey = "")
        {
            var isOrderTestMode = false;
            var order = Ioc.OrderBusiness.GetOrdersByConfirmationID(cid);
            var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(clubId);

            if (order == null)
            {
                if (caller == JumbulaSubSystem.Installment)
                {
                    order = Ioc.InstallmentBusiness.GetListByToken(cid).ToList().First().OrderItem.Order;
                }
                else { order = Ioc.TransactionActivityBusiness.GetInvoicedByToken(cid).ToList().First().OrderItem.Order; }
            }

            isOrderTestMode = !order.IsLive;

            try
            {
                var trackingId = Ioc.PaypalTrackingBusiness.GenerateTrackingId(caller, callerId, cid);
                var club = Ioc.ClubBusiness.Get(clubId);

                Client client = null;
                if (club.PartnerId.HasValue && club.PartnerClub != null && club.IsSchool)

                {
                    if (club.PartnerClub.Client != null)
                    {
                        client = club.PartnerClub.Client;
                    }
                    else
                    {
                        client = Ioc.ClientBusiness.Get(club.PartnerId.Value);
                    }
                }
                else
                {
                    client = club.Client;
                }

                List<Receiver> reciverLists = new List<Receiver>();
                decimal commission = 0;
                if (client.PricePlan.TransactionFee > 0)
                {
                    commission = Math.Round((totalAmount * client.PricePlan.TransactionFee) / 100, 2);
                }

                var firstReceiver = new Receiver()
                {
                    email = PaypalEmail,
                    amount = totalAmount //- commission,

                };
                if (commission > 0)
                {
                    firstReceiver.primary = true;
                    reciverLists.Add(new Receiver()
                    {

                        email = (clinetPaymentMethod.IsPaypalTestMode || isOrderTestMode) ? WebConfigHelper.Paypal_Test_JumbulaEmail : WebConfigHelper.Paypal_JumbulaEmail,
                        amount = commission,
                        primary = false

                    });
                }

                reciverLists.Add(firstReceiver);
                apiProfile = new PaypalAPIProfile(isSandBox)
                {
                    RequestDataformat = PAYPALREQUESTDATAFORMAT,
                    ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                    ApiOperation = PaypalAPIOperations.Pay,
                    DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
                };

                if (!string.IsNullOrEmpty(note) && note.Length > Constants.Default_Paypal_MaxMemoChar)
                {
                    note = note.Substring(0, (Constants.Default_Paypal_MaxMemoChar - 1)) + "...";
                }


                PayRequest payRequest = new PayRequest()
                {
                    actionType = PaypalAPIOperations.Pay.ToString().ToUpper(),
                    currencyCode = club.Currency.ToString(),
                    requestEnvelope = new RequestEnvelope(),
                    feesPayer = PaypalFeeReceiver.EACHRECEIVER.ToString().ToUpper(),
                    cancelUrl = cancelUrl,
                    returnUrl = returnUrl,
                    ipnNotificationUrl = ipnNotificationUrl,
                    memo = string.IsNullOrEmpty(note) ? string.Format(Constants.F_Paypal_Pay_Memo, cid) : note

                };
                if (commission > 0)
                {
                    payRequest.feesPayer = PaypalFeeReceiver.PRIMARYRECEIVER.ToString().ToUpper();
                }
                if (!string.IsNullOrEmpty(trackingId))
                {
                    payRequest.trackingId = trackingId;
                }
                if (!string.IsNullOrEmpty(preapprovalKey))
                {
                    payRequest.preapprovalKey = preapprovalKey;
                }
                payRequest.receiverList = new ReceiverList();
                payRequest.receiverList.receiver = reciverLists;
                payLoad = JsonConvert.SerializeObject(payRequest);
                return CallAPI();
            }

            catch
            {

                throw;
            }

        }

        public string Refund(string payKey, RefundType refundType, int clubId, decimal totalAmount, string currencyCode, bool isLive)
        {
            payLoad = null;
            try
            {
                RefundRequest request = new RefundRequest(payKey);
                var club = Ioc.ClubBusiness.Get(clubId);
                Client client = null;

                if (club.PartnerId.HasValue && club.PartnerClub != null && club.IsSchool)

                {
                    if (club.PartnerClub.Client != null)
                    {
                        client = club.PartnerClub.Client;
                    }
                    else
                    {
                        client = Ioc.ClientBusiness.Get(club.PartnerId.Value);
                    }
                }
                else
                {
                    client = club.Client;
                }

                request.currencyCode = currencyCode;
                List<Receiver> reciverLists = new List<Receiver>();
                decimal commission = 0;

                if (client.PricePlan.TransactionFee > 0)
                {
                    commission = Math.Round((totalAmount * client.PricePlan.TransactionFee) / 100, 2);
                }

                reciverLists.Add(new Receiver()
                {
                    email = PaypalEmail,
                    amount = totalAmount,
                });

                if (commission > 0)
                {
                    reciverLists.Add(new Receiver()
                    {
                        email = isLive ? WebConfigHelper.Paypal_JumbulaEmail : WebConfigHelper.Paypal_Test_JumbulaEmail,
                        amount = commission,
                    });
                }

                request.receiverList = new ReceiverList();
                request.receiverList.receiver = reciverLists;

                payLoad = JsonConvert.SerializeObject(request);
                apiProfile = new PaypalAPIProfile(isSandBox)
                {
                    RequestDataformat = PAYPALREQUESTDATAFORMAT,
                    ResponseDataformat = PAYPALRESPONSEDATAFORMAT,
                    ApiOperation = PaypalAPIOperations.Refund,
                    DeviceIPAddress = HttpContext.Current.Request.UserHostAddress,
                };

                return CallAPI();
            }
            catch
            {
                throw;
            }

        }
        private string CallAPI()
        {

            string responseString = string.Empty;
            try
            {
                //  
                if (this.apiProfile == null)
                {
                    throw new NullReferenceException();
                }

                objRequest = (HttpWebRequest)WebRequest.Create(apiProfile.APIEndPointUrl);
                objRequest.Method = REQUESTMETHOD;
                objRequest.Timeout = DEFAULT_TIMEOUT;
                objRequest.ContentType = CONTENT_TYPE;

                SetupHeaders();

                using (StreamWriter myWriter = new StreamWriter(objRequest.GetRequestStream()))
                {
                    myWriter.Write(this.payLoad);
                }

                using (WebResponse response = objRequest.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        responseString = sr.ReadToEnd();
                        return responseString;
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objRequest = null;
            }


        }

        private void SetupHeaders()
        {
            objRequest.Headers.Add(XPAYPALSECURITYUSERID, this.apiProfile.APIUsername);
            objRequest.Headers.Add(XPAYPALSECURITYPASSWORD, this.apiProfile.APIPassword);
            objRequest.Headers.Add(XPAYPALSECURITYSIGNATURE, this.apiProfile.APISignature);
            // objRequest.Headers.Add(XPAYPALDEVICEIPADDRESS, this.apiProfile.DeviceIPAddress);
            objRequest.Headers.Add(XPAYPALREQUESTDATAFORMAT, this.apiProfile.RequestDataformat);
            objRequest.Headers.Add(XPAYPALRESPONSEDATAFORMAT, this.apiProfile.ResponseDataformat);
            objRequest.Headers.Add(XPAYPALAPPLICATIONID, this.apiProfile.ApplicationID);
        }


        private PaypalAPIProfile apiProfile { get; set; }
        private string payLoad { get; set; }





        public string GetRefundError(RefundResponse response)
        {
            var refundInfo = response.refundInfoList.refundInfo.Where(i => i.refundStatus != RefundStatus.REFUNDED.ToString() && i.refundStatus != RefundStatus.REFUNDED_PENDING.ToString());
            if (refundInfo != null && refundInfo.Count() > 0)
            {
                return ((RefundStatus)Enum.Parse(typeof(RefundStatus), refundInfo.FirstOrDefault().refundStatus)).ToDescription();
            }
            return Constants.Paypal_Unsuccessful_Refund_Message;

        }

        public string AuthorizePreapproval(string preapprovalKey)
        {

            if (isSandBox)
            {
                return String.Format("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd={0}&preapprovalkey={1}", PaypalCmd._ap_preapproval.ToDescription(), preapprovalKey);
            }
            return String.Format("{0}?cmd={1}&preapprovalkey={2}", WebConfigHelper.Paypal_CmdApiEndPoint_Sig_Live, PaypalCmd._ap_preapproval.ToDescription(), preapprovalKey);
        }
        public string AuthorizePayment(string payKey)
        {

            if (isSandBox)
            {

                return String.Format("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd={0}&paykey={1}", PaypalCmd._ap_payment.ToDescription(), payKey);
            }
            return String.Format("{0}?cmd={1}&paykey={2}", WebConfigHelper.Paypal_CmdApiEndPoint_Sig_Live, PaypalCmd._ap_payment.ToDescription(), payKey);
        }


        public bool ValidateIPNMessage(string ipnRequest, Encoding ipnEncoding)
        {

            string ipnEndpoint = WebConfigHelper.Paypal_CmdApiEndPoint_Sig_Live;
            if (isSandBox)
            {
                ipnEndpoint = WebConfigHelper.Paypal_CmdApiEndPoint_Sig_Test;
            }
            try
            {
                ipnRequest += "&cmd=" + PaypalCmd._notify_validate.ToDescription();

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ipnEndpoint);
                request.Method = REQUESTMETHOD;
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = ipnRequest.Length;

                using (StreamWriter streamOut = new StreamWriter(request.GetRequestStream(), ipnEncoding))
                {
                    streamOut.Write(ipnRequest);
                }

                string strResponse = string.Empty;
                using (StreamReader streamIn = new StreamReader(request.GetResponse().GetResponseStream()))
                {
                    strResponse = streamIn.ReadToEnd();
                }

                if (strResponse.Equals("VERIFIED"))
                {
                    return true;
                }
                else
                {
                    LogHelper.LogException(new Exception("IPN validation failed. Got response: " + strResponse));
                    return false;
                }
            }
            catch (System.Exception ex)
            {
                LogHelper.LogException(ex);

            }
            return false;
        }

        public RefundResultModel Refund(TransactionActivity transactionActivity, string clubDomain, decimal refundAmount, bool isTestMode, Dictionary<string, string> metadata)
        {
            var paypal = new PaypalService(clubDomain, !isTestMode);
            var result = new RefundResultModel();
            var currencyCode = transactionActivity.OrderItem.Order.Club.Currency;

            var refundType = transactionActivity.Amount > refundAmount ? RefundType.Partial : RefundType.Full;

            var paypalResult = paypal.Refund(transactionActivity.PaymentDetail.PayKey, refundType, transactionActivity.OrderItem.Order.ClubId, refundAmount, currencyCode.ToString(), !isTestMode);
            var response = JsonConvert.DeserializeObject<RefundResponse>(paypalResult);
            result.Response = paypalResult;

            ValidatePayPalResponse(response, clubDomain, isTestMode, paypalResult, result);

            return result;
        }

        private void ValidatePayPalResponse(RefundResponse response, string clubDomain, bool isTestMode, string paypalResult, RefundResultModel model)
        {
            var paypal = new PaypalService(clubDomain, !isTestMode);

            if (response != null)
            {
                if (response.responseEnvelope.ack.HasValue && (response.responseEnvelope.ack.Value == AckCode.SUCCESS || response.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING))
                {
                    if (response.refundInfoList.refundInfo.TrueForAll(i => i.refundStatus == RefundStatus.REFUNDED.ToString() || i.refundStatus == RefundStatus.REFUNDED_PENDING.ToString()))
                    {
                        model.Status = true;
                        model.TransactionId = response.refundInfoList.refundInfo.FirstOrDefault()?.encryptedRefundTransactionId;
                    }
                    else
                    {
                        model.Status = false;
                        model.ErrorMessage = paypal.GetRefundError(response);
                    }

                }
                else
                {
                    model.Status = false;
                    var faultMessage = JsonConvert.DeserializeObject<PPFaultMessage>(paypalResult);
                    model.ErrorMessage = faultMessage.error[0].message;

                }
            }
            else
            {
                model.Status = false;
                model.TransactionId = response.refundInfoList.refundInfo.FirstOrDefault().encryptedRefundTransactionId;
                model.ErrorMessage = FailProcessRefund;
                Log.Error("Response is null {TransactionId}", model.TransactionId);
            }

        }

        public PaymentDetailModel GetPaymentDetails(string clubDomain, string payKey, IEnumerable<TransactionActivity> oldRefunded, bool isPaypalTransaction, bool isTestMode)
        {
            var paypal = new PaypalService(clubDomain, !isTestMode);

            var payDetail = paypal.paymentDetails(payKey);
            var pdResponse = JsonConvert.DeserializeObject<PaymentDetailsResponse>(payDetail);

            return new PaymentDetailModel
            {
                Amount = pdResponse.paymentInfoList.paymentInfo.Select(c => c.receiver).Count() > 1 ?
                pdResponse.paymentInfoList.paymentInfo.Where(c => c.receiver.primary).Sum(c => c.receiver.amount ?? 0) : pdResponse.paymentInfoList.paymentInfo.Sum(c => c.receiver.amount ?? 0),
                AmountRefunded = pdResponse.paymentInfoList.paymentInfo.Select(c => c.receiver).Count() > 1 ? 
                pdResponse.paymentInfoList.paymentInfo.Where(c => c.receiver.primary).Sum(c => c.refundedAmount ?? 0) : pdResponse.paymentInfoList.paymentInfo.Sum(c => c.refundedAmount ?? 0),
                Status = pdResponse.responseEnvelope.ack.HasValue && (pdResponse.responseEnvelope.ack.Value == AckCode.SUCCESS || pdResponse.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING)
            };
        }

        private bool isSandBox { get; set; }

    }
}