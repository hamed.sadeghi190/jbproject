﻿using System;
using System.Web.Mvc;
using Jb.Framework.Common.Forms.Validation;

namespace Jumbula.Web.Mvc.ModelBinder
{
    public class JbElementValidatorModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {

            var typeValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName + ".QVModelType");
            Type type;
            if (typeValue == null)
            {
                type = typeof(BaseElementValidator);
                var model = Activator.CreateInstance(type);
                bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => model, type);
                return model;
            }
            else
            {
                type = Type.GetType(
                    (string)typeValue.ConvertTo(typeof(string)) + ",Jb.Framework.Common", true);
                var model = Activator.CreateInstance(type);
                bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => model, type);
                return model;
            }

        }
    }
}
