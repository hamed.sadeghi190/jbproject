﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AddStaffFromMemberViewModel
    {
        public AddStaffFromMemberViewModel()
        {

        }

        public List<SelectKeyValue<int>> AllStaffs { get; set; }

        [Display(Name = "Staff name")]
        [Required(ErrorMessage = "{0} is required.")]
        public int? SelectedStaff { get; set; }

        public bool SendNotificationMessage { get; set; }
    }
}