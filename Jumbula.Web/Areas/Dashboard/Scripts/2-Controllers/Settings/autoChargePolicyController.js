﻿(function () {
    "use strict";

    angular.module("dashboardApp").controller("autoChargePolicyController",
        [
            "$scope", "$http", "jbToast", "uiComponentsService", function ($scope, $http, jbToast, jbConfirmation) {
                window.app.globalObjects.pageClass = "school-siteinfo-settings-view";
                window.app.globalObjects.initializeView();
                $scope.previewText = "";

                $http.get("Dashboard/Setting/GetAutoChargePolicy").success(
                    function (data) {
                        $scope.Model = data;
                        $scope.attemptLength = $scope.Model.EnableAttemptsCount.toString();

                        $scope.Model.FailedEmailToAdmin = $scope.Model.Attempts[0].FailedEmailToAdmin,
                            $scope.Model.FailedEmailToParent = $scope.Model.Attempts[0].FailedEmailToParent,
                            $scope.Model.FailedAdminEmailTemplate = $scope.Model.Attempts[0].FailedAdminEmailTemplate,
                            $scope.Model.FailedParentEmailTemplate = $scope.Model.Attempts[0].FailedParentEmailTemplate;
                    }).error(function (data) {
                        jbToast.error("error " + data);
                    });

                $scope.customPolicyCountsChanged = function (value) {
                    var emailTemplates = $scope.Model.Attempts;
                    $scope.Model.Attempts = [];

                    for (var i = 0; i < emailTemplates.length; i++) {

                        if (emailTemplates[i].Enabled && i < value) {
                            $scope.Model.Attempts.push(emailTemplates[i]);
                        }
                        else if (i < value) {
                            $scope.Model.Attempts.push({
                                AttemptOrder: emailTemplates[i].AttemptOrder,
                                IntervalDays: emailTemplates[i].IntervalDays,
                                FailedEmailToAdmin: emailTemplates[i].FailedEmailToAdmin,
                                FailedEmailToParent: emailTemplates[i].FailedEmailToParent,
                                FailedAdminEmailTemplate: emailTemplates[i].FailedAdminEmailTemplate,
                                FailedParentEmailTemplate: emailTemplates[i].FailedParentEmailTemplate,
                                TemplateTitle: emailTemplates[i].TemplateTitle,
                                Enabled: true
                            });
                        }
                        else {
                            $scope.Model.Attempts.push({
                                AttemptOrder: emailTemplates[i].AttemptOrder,
                                IntervalDays: emailTemplates[i].IntervalDays,
                                FailedEmailToAdmin: emailTemplates[i].FailedEmailToAdmin,
                                FailedEmailToParent: emailTemplates[i].FailedEmailToParent,
                                FailedAdminEmailTemplate: emailTemplates[i].FailedAdminEmailTemplate,
                                FailedParentEmailTemplate: emailTemplates[i].FailedParentEmailTemplate,
                                TemplateTitle: emailTemplates[i].TemplateTitle,
                                Enabled: false
                            });
                        }
                    }
                };

                $scope.save = function () {
                    $scope.Model.Attempts[0] = {
                        AttemptOrder: 1,
                        IntervalDays: 0,
                        FailedEmailToAdmin: $scope.Model.FailedEmailToAdmin,
                        FailedEmailToParent: $scope.Model.FailedEmailToParent,
                        FailedAdminEmailTemplate: $scope.Model.FailedAdminEmailTemplate,
                        FailedParentEmailTemplate: $scope.Model.FailedParentEmailTemplate,
                        TemplateTitle: "",
                        Enabled: true
                    };

                    $http.post("Dashboard/Setting/SaveAutoChargePolicy", { model: $scope.Model })
                        .success(function (data) {

                            if (data.Status) {
                                $scope.errors = null;
                                jbToast.success("Auto charge policy updated successfully.", "");
                            } else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                };



                $scope.Preview = function (emailType) {

                    var message = "";
                    var templateUrl = "";
                    var attemptNumber = 0;
                    var isAdmin = false;
                    // Get the modal
                    var modal = document.getElementById('previewModal');
                    var modalContent = document.getElementById('myContent');

                    switch (emailType) {

                        case "successParent":
                            message = $scope.Model.SuccessParentEmailTemplate;
                            templateUrl = "Dashboard/Setting/SuccessEmailPreview";

                            break;
                        case "successAdmin":
                            message = $scope.Model.SuccessAdminEmailTemplate;
                            templateUrl = "Dashboard/Setting/SuccessEmailPreview";
                            isAdmin = true;

                            break;
                        default:
                            if (emailType.startsWith("fail")) {

                                var splitedType = emailType.split("-");
                                var index = parseInt(splitedType[1]);

                                attemptNumber = $scope.Model.Attempts[index].AttemptOrder;

                                var nextAttemptIntervalDays = 0;

                                if ($scope.attemptLength > (index + 1))
                                    nextAttemptIntervalDays = $scope.Model.Attempts[index + 1].IntervalDays;

                                templateUrl = "Dashboard/Setting/FailEmailPreview";

                                switch (splitedType[0]) {
                                    case "failParent":

                                        if (index === 0)
                                            message = $scope.Model.FailedParentEmailTemplate;
                                        else
                                            message = $scope.Model.Attempts[index].FailedParentEmailTemplate;
                                        break;

                                    case "failAdmin":
                                        if (index === 0)
                                            message = $scope.Model.FailedAdminEmailTemplate;
                                        else
                                            message = $scope.Model.Attempts[index].FailedAdminEmailTemplate;
                                        isAdmin = true;
                                        break;
                                }
                            }
                    }

                    if (templateUrl !== "") {
                        $http.post(templateUrl, { message: message, attemptNumber: attemptNumber, nextAttemptIntervalDays: nextAttemptIntervalDays, isAdmin: isAdmin })
                            .success(function (response) {
                                $scope.previewText = response;
                                modal.style.display = "block";
                            });
                    }

                }

                $scope.dismissPopup = function () {
                    $scope.isShowPopup = false;
                };
                $scope.closeModal = function () {
                    // Get the <span> element that closes the modal
                    var span = document.getElementsByClassName("close")[0];
                    var modal = document.getElementById('previewModal');
                    modal.style.display = "none";
                };
            }
        ]);
})();