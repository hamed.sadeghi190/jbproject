﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using System.Web.Mvc;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CreateEditChargeViewModel
    {
        public CreateEditChargeViewModel()
        {
            //EarlyBird = new EarlyBird();
        }
        public long Id { get; set; }

        [MaxLength(256, ErrorMessage = "{0} cannot be longer than 256 characters.")]
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public ChargeDiscountCategory Category { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Amount")]
        public decimal? Amount { get; set; }
        
        public long SeasonId { get; set; }

        public bool EnableApplicationFee { get; set; }

        public bool HasEarlyBird { get; set; }

        //public EarlyBird EarlyBird { get; set; }

        [Required(ErrorMessage = "Expire date is required.")]
        public DateTime? ExpireDate { get; set; }

        [Required(ErrorMessage = "Amount is required.")]
        public decimal? EarlyBirdAmount { get; set; }
        public ChargeApplyType ApplicationFeeApplyType { get; set; }

        public bool IsAllProgram { get; set; }

        [Required(ErrorMessage = "Select at least one program.")]
        public List<string> SelectedPrograms { get; set; }

        public List<SelectListItem> ProgramsList { get; set; }

    }
}