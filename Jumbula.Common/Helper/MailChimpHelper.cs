﻿using MailChimp;
using MailChimp.Helper;

namespace Jumbula.Common.Helper
{
    public class MailChimpHelper
    {
        public static void AddSubescriber(string emailAddress, object model, string listId, string mailChimpApiKey)
        {
            var mc = new MailChimpManager(mailChimpApiKey);

            //  Create the email parameter
            var email = new EmailParameter()
            {
                Email = emailAddress
            };

            mc.Subscribe(listId, email, doubleOptIn: false, sendWelcome: false, mergeVars: model);
        }
    }
}