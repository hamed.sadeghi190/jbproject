﻿using Jb.Framework.Common.Forms.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jb.Framework.Common.Forms
{
    public class JbBaseElementViewModel
    {
        public JbBaseElementViewModel()
        {
            Validations = new List<JbElementItem>();
            ElementValidators = new List<BaseElementValidator>();
        }

        [Required]
        public string Title { get; set; }

        public string HelpText { get; set; }

        public IEnumerable<JbElementItem> Validations { get; set; }

        public int SelectedValidationId { get; set; }

        public IList<BaseElementValidator> ElementValidators { get; set; }        

        public int Order { get; set; }

        public virtual object ToObject()
        {
            throw new NotImplementedException("ToObject not implemented");
        }
    }
}
