
ALTER TABLE [dbo].[Clients]
add EnableDonation bit not null default(0)

ALTER TABLE [dbo].[Clients]
add DonationFormId int null 

ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Clients_dbo.JbForms_DonationFormId] FOREIGN KEY([DonationFormId])
REFERENCES [dbo].[JbForms] ([Id])
GO