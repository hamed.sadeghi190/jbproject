﻿namespace Jumbula.Common.Constants
{
    public static partial class Constants
    {
        public static class AuditingConstants
        {
            public const string AuditingAddFormat = "{0} was added";
            public const string AuditingChangeFormat = "{0} was changed";
            public const string AuditingDeleteFormat = "{0} was deleted";

            public const string ContactSaveSettings = "Contact settings was channged";
        }
    }
}
