
----------------------------------------


-- Migrate [dbo].[webpages_UsersInRoles] to JbUserRoles
DECLARE @userRoleCount INT 

select @userRoleCount = COUNT(Id) from [dbo].[webpages_UsersInRoles]

WHILE 1 = 1
BEGIN

INSERT INTO [dbo].[JbUserRoles]
           (
		    [UserId]
           ,[RoleId]
		   ,[ClubId])
    SELECT TOP(@userRoleCount) 
            s.UserId
		   ,dbo.GetNewRoleIdFromOld(s.RoleId)
		   ,s.ClubId
    FROM [dbo].[webpages_UsersInRoles] s
    WHERE NOT EXISTS ( 
        SELECT 1
        FROM [dbo].[JbUserRoles]
        WHERE UserId = s.UserId And RoleId = dbo.GetNewRoleIdFromOld(s.RoleId) And ClubId = s.ClubId
    )
	
    IF @@ROWCOUNT < @userRoleCount BREAK
    
END


