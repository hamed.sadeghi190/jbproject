Alter Table [MessageHeaders] Add ProgramId bigint null
GO

ALTER TABLE [dbo].[MessageHeaders]  WITH CHECK ADD  CONSTRAINT [FK_dbo.MessageHeaders_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
ON DELETE CASCADE

