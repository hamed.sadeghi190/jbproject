﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class OrderItemWaiver : BaseEntity
    {
       
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        public string Text { get; set; }

        public long OrderItemId { get; set; }
        [ForeignKey("OrderItemId")]
        public virtual OrderItem OrderItem { get; set; }

        public string Signature { get; set;}
        public bool Agreed { set; get; }
        public bool IsRequired { get; set; }
        [Required]
        public WaiverConfirmationType WaiverConfirmationType { get; set; }
    }
}
