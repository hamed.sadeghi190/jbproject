﻿using Autofac;
using Jumbula.Core.Data;
using Jumbula.Core.Data.DbHelpers;
using Jumbula.Core.Domain;
using Jumbula.Data;
using Jumbula.Data.DbHelpers;
using Jumbula.Data.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Jumbula.Bootstrapper.Modules
{
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(UnitOfWork)).As(typeof(IUnitOfWork)).InstancePerRequest();
            builder.RegisterType(typeof(JumbulaDataContext)).As(typeof(IEntitiesContext)).InstancePerRequest();
            builder.RegisterType(typeof(JumbulaDataContext)).As(typeof(IdentityDbContext<JbUser, JbRole, int, JbUserLogin, JbUserRole, JbUserClaim>)).InstancePerRequest();
            builder.RegisterGeneric(typeof(BaseRepository<>)).As(typeof(IRepository<>)).InstancePerRequest();
            builder.RegisterGeneric(typeof(BaseRepository<,>)).As(typeof(IRepository<,>)).InstancePerRequest();

            builder.RegisterType(typeof(ClubRepository)).As(typeof(IClubRepository)).InstancePerRequest();
            builder.RegisterType(typeof(ContactRepository)).As(typeof(IContactRepository)).InstancePerRequest();
            builder.RegisterType(typeof(AuditRepository)).As(typeof(IAuditRepository)).InstancePerRequest();
            builder.RegisterType(typeof(EmailRepository)).As(typeof(IEmailRepository)).InstancePerRequest();
            builder.RegisterType(typeof(JbDbFunctions)).As(typeof(IJbDbFunctions)).InstancePerRequest();
        }
    }
}
