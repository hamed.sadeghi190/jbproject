if not exists(select 1 from sys.columns where object_id=OBJECT_ID('Coupons') and name='CouponCalculateType')
begin
	Alter Table Coupons
	Add CouponCalculateType tinyint not null default(0);
end