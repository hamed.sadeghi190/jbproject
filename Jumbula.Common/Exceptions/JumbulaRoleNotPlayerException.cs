﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaRoleNotPlayerException : JumbulaAccountException
    {
        public JumbulaRoleNotPlayerException(Exception e)
            : base(e)
        {
        }
    }
}
