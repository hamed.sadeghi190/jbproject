﻿(function () {
    'use strict';

    angular.module("dashboardApp").directive('directiveWhenScrolled', function ($compile, $parse) {
        return function (scope, elm, attr) {
            var raw = elm[0];

            elm.bind('scroll', function () {

                if (raw.scrollTop + raw.offsetHeight >= raw.scrollHeight) {

                    scope.$apply(attr.directiveWhenScrolled);
                }
            });
        };
    });
})();
