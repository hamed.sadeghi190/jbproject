﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Infrastructure;
using System.Collections.Generic;

namespace Jumbula.Infrastructure
{
    public class JbLocker : IJbLocker
    {
        #region Constructors
        public JbLocker()
        {
            _workers = new Dictionary<JbWorker, bool>();

            var allWorkers = EnumHelper.GetEnumValues<JbWorker>();

            foreach (var worker in allWorkers)
            {
                _workers.Add(worker, false);
            }
        }

        #endregion

        #region Fields
        private Dictionary<JbWorker, bool> _workers;
        #endregion

        public bool TryLock(JbWorker worker)
        {
            if (IsLocked(worker))
                return false;

            _workers[worker] = true;
            return true;
        }

        public void UnLock(JbWorker worker)
        {
            _workers[worker] = false;
        }

        private bool IsLocked(JbWorker worker)
        {
            return _workers[worker];
        }
    }
}
