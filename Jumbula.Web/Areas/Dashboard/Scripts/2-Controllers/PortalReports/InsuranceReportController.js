﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('InsuranceReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        //$scope.MC.openBlade({ url: "Dashboard/Blade/PortalReports", order: 0, menu: "Reports" });

        $scope.stateParams = $stateParams;
        //$scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = {};
        $scope.Model.checkedIds = {};


        $scope.InsuranceReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetInsuranceReportInfo',
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
        });

        $scope.InsurancereportReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "Insurance Report",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.InsuranceReportDataSource,
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5
            },
            sortable: { mode: 'single' },
            columns: [
                {
                    field: "ProviderName",
                    title: "Provider name",
                    width: "180px"
                }, {
                    field: "Email",
                    title: "Email",
                    width: "180px"
                },
                {
                    field: "Insurance",
                    title: "Insurance list",
                    width: "200px"
                },
                {
                    field: "Date",
                    title: "Expire date",
                    width: "200px"
                }
            ]
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#InsurancereportReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();