﻿using Jumbula.Common.Delegates;
using System.Net.Mail;

namespace Jumbula.Core.Infrastructure.Email
{
    public interface IEmailProvider
    {
        void Send(MailMessage message, int emailId, EmailCompletedEventHandler emailCompletedEventHandler, bool isCampaignMode = false);
    }
}
