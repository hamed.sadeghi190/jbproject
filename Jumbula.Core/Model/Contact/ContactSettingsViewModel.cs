﻿using System.Collections.Generic;


namespace Jumbula.Core.Model.Contact
{
    public class ContactSettingsViewModel
    {
        public List<PropertyItemContacttSettingsViewModel> Fields { get; set; }
    }

    public class PropertyItemContacttSettingsViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Title { get; set; }

        public bool IsEnabled { get; set; }

        public bool ShowInGrid { get; set; }

        public bool IsReadOnly { get; set; }
    }
}
