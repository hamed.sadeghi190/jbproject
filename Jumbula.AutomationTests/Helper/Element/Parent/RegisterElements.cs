﻿using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element.Parent
{
    class RegisterElements: BaseElements
    {
        public RegisterElements(IWebDriver driver)
            : base(driver)
        {
        }
            #region Display Page

        public IWebElement RegisterBtn
        {
            get
            {
                return Driver.FindElement(By.Id("continue"));
            }
        }


        #endregion Display Page
        #region Tuition Option Page
        public IWebElement FirstTuitionCheckbox
        {
            get
            {
                return Driver.FindElement(By.XPath("(//span[contains(@class,'tuition-select')])[1]"));
            }
        }
        public IWebElement FirstTuitionLabel
        {
            get
            {
                return Driver.FindElement(By.XPath("(//span[contains(@class,'tuition-label')])[1]"));
            }
        }
        public IWebElement FirstTuitionPrice
        {
            get
            {
                return Driver.FindElement(By.XPath("(//span[contains(@class,'tuition-select')])[1]//following-sibling::span[contains(@class,'price')]"));
            }
        }
        public IWebElement SecondTuitionCheckbox
        {
            get
            {
                return Driver.FindElement(By.XPath("(//span[contains(@class,'tuition-select')])[2]"));
            }
        }
        public IWebElement SecondTuitionLabel
        {
            get
            {
                return Driver.FindElement(By.XPath("(//span[contains(@class,'tuition-label')])[2]"));
            }
        }
        public IWebElement SecondTuitionPrice
        {
            get
            {
                return Driver.FindElement(By.XPath("(//span[contains(@class,'tuition-select')])[2]//following-sibling::span[contains(@class,'price')]"));
            }
        }
        public IWebElement FirstCharge
        {
            get
            {
                return Driver.FindElement(By.XPath("(//ul[contains(@class,'charge-bar')])[1]//li[1]/span[1]"));
            }
        }
        public IWebElement SecondCharge
        {
            get
            {
                return Driver.FindElement(By.XPath("(//ul[contains(@class,'charge-bar')])[1]//li[2]/span[1]"));
            }
        }
        #endregion Tuition Option Page

        #region Checkout box
        public IWebElement CheckoutTuitionLabel
        {
            get
            {
                return Driver.FindElement(By.XPath("(//ul[contains(@class,'tuitions')])[1]/li[1]/span[contains(@class,'title')]"));
            }
        }
        public IWebElement CheckoutTuitionPrice
        {
            get
            {
                return Driver.FindElement(By.XPath("(//ul[contains(@class,'tuitions')])[1]/li[1]/span[contains(@class,'price')]"));
            }
        }
        public IWebElement CheckoutFirstChargeLabel
        {
            get
            {
                return Driver.FindElement(By.XPath("//ul[contains(@class,'charges')]//li[1]/span[contains(@class,'title')]"));
            }
        }
        public IWebElement CheckoutFirstChargePrice
        {
            get
            {
                return Driver.FindElement(By.XPath("//ul[contains(@class,'charges')]//li[1]/span[contains(@class,'price')]"));
            }
        }
        public IWebElement CheckoutTotalPrice
        {
            get
            {
                return Driver.FindElement(By.Id("totalPrice"));
            }
        }
        #endregion Checkout box

        public IWebElement ContinueButton
        {
            get
            {
                var elements = Driver.FindElements(By.TagName("button"));
                return elements.First(elm => elm.Text == "CONTINUE");
            }
        }

    }
}

