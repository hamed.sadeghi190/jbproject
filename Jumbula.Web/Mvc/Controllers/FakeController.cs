﻿using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;

namespace Jumbula.Web.Mvc.Controllers
{
    public class FakeControllerContext : ControllerContext
    {
        public override HttpContextBase HttpContext { get; set; } = new FakeHttpContext();
    }

    public class FakeHttpContext : HttpContextBase
    {
        public override HttpRequestBase Request { get; } = new FakeHttpReqeuest();
    }

    public class FakeHttpReqeuest : HttpRequestBase
    {
        public override string this[string key] => null;

        public override NameValueCollection Headers => new NameValueCollection();
    }
}
