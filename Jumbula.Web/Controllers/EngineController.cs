﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.Register;


namespace Jumbula.Web.Controllers
{
    public class EngineController : JbBaseController
    {

        //[AjaxRequest]
        public virtual JsonResult UscfMemSearch(string term)
        {
            try
            {
                Uscf uscf = new Uscf();
                string[] values = uscf.UscfMemSearch(term);

                return Json(values, JsonRequestBehavior.AllowGet);
            }

            catch (Exception e)
            {
                throw e;
            }
        }

        //[AjaxRequest]
        public virtual JsonResult UscfIdSearch(string term)
        {
            try
            {
                Uscf uscf = new Uscf();
                List<UscfIdSearch> recs = uscf.UscfIdSearch(term);

                return Json(recs, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //[AjaxRequest]
        public virtual JsonResult UsttMemSearch(string term)
        {
            try
            {
                Ustt ustt = new Ustt();
                var recs = ustt.UsaTTMemSearch(term);

                return Json(recs, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        //[AjaxRequest]
        public virtual JsonResult UsttIdSearch(string term)
        {
            try
            {
                Ustt ustt = new Ustt();
                var recs = ustt.UscTTIdSearch(term);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = recs != null,
                        model = recs
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
