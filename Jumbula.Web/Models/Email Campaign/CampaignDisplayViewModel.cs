﻿using Jumbula.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class CampaignDisplayViewModel : BaseViewModel<Campaign>
    {
        public string CampaignName { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SendDate { get; set; }

        public CampaignScheduleType Status { get; set; }

        public CampaignReciepientsMode CampaignType { get; set; }

        public EmailCampaignStep LastCreatedPage { get; set; }

        public decimal OpenedRate { get; set; }

        public decimal ClickedRate { get; set; }

        public decimal DeliveredRate { get; set; }

        public int OpenedNum { get; set; }

        public int ClickedNum { get; set; }

        public int DeliveredNum { get; set; }

        public string LastOpenedDate { get; set; }

        public string LastClickedDate { get; set; }
        public bool HaveTimeForCancel { get; set; }
    }
}