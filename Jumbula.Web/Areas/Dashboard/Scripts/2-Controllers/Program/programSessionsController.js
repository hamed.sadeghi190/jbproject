﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programSessionsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var programId = $scope.stateParams.programId;

        $http.get(window.location.origin + '/Dashboard/Program/GetSessionList?programId=' + programId).success(function (data, status, headers, config) {
            $scope.Model = data;
        });

        $scope.edit = function (session) {

            session.editMode = true;
        }

        $scope.save = function (session) {

            $http.post('/Dashboard/Program/UpdateSessionItem', { model: session, programId: programId })
             .success(function (data, status, headers, config) {

                 if (data.Status) {
                     session.editMode = false;
                     $scope.Model = data.Data;

                     jbToast.success("Session edited successfully.");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.delete = function (sessionId) {

            jbConfirmation
               .yes(function () {
                   $http.post('/Dashboard/Program/DeleteSessionItem', { programId: programId, sessionId: sessionId })
                                .success(function (data, status, headers, config) {
                                    $scope.Model = data.Data;

                                    jbToast.success("Session deleted successfully.");
                                })

               })
              .title('Session delete')
              .message('Are you sure you want to delete this session?')
              .confirm();
        };

        $scope.attendances = function (sessionId) {

            $state.go('SessionAttendance', { seasonDomain: $scope.MC.stateParams.seasonDomain, programId: programId, sessionId: sessionId })
        }
    }]);
})();

