﻿var geocoder_google;
var map_google;
var infowindow_google;
function google_init(canvas, zoom, options) {
    if (!options) {
        options = {};
    }
    var latlng = new google.maps.LatLng(37.708333, -122.280278);
    var mapOptions = {
        zoom: zoom,
        center: latlng,
        mapTypeId: options.mapTypeId != 'undefined' ? options.mapTypeId : google.maps.MapTypeId.ROADMAP,
        zoomControl: options.zoomControl != 'undefined' ? options.zoomControl : true,
        zoomControlOptions: options.zoomControlOptions != 'undefined' ? options.zoomControlOptions : {
            style: google.maps.ZoomControlStyle.SMALL
        },
        mapTypeControl: options.mapTypeControl != 'undefined' ? options.mapTypeControl : false,
        overviewMapControl: options.overviewMapControl != 'undefined' ? options.overviewMapControl : false,
        panControl: options.panControl != 'undefined' ? options.panControl : true,
        scaleControl: options.scaleControl != 'undefined' ? options.scaleControl : false,
        rotateControl: options.rotateControl != 'undefined' ? options.rotateControl : false,
        scrollwheel: options.scrollwheel != 'undefined' ? options.scrollwheel : true,
        streetViewControl: false,
        styles: options.styles != 'undefined' ? options.styles : [
            {
                "featureType": "water",
                "stylers": [
                    { "color": "#239bb4" }
                ]
            }, {
                "featureType": "road.highway.controlled_access",
                "stylers": [
                    { "visibility": "on" },
                    { "color": "#acb7c0" }
                ]
            }, {
                "featureType": "road.local",
                "stylers": [
                    { "visibility": "on" },
                    { "color": "#f5ad57" },
                    { "weight": 0.5 }
                ]
            }, {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    { "color": "#ffffff" }
                ]
            }
        ]
    }


    // geocoder_google = new google.maps.Geocoder();
    infowindow_google = new google.maps.InfoWindow();
    map_google = new google.maps.Map(canvas, mapOptions);
}

function google_initAutoComp(input) {
    var options = {
        //types: ['(regions)'],
        componentRestrictions: { country: 'us' }
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            autoComplete_updateData_failure();
        }
        else {
            autoComplete_updateData_success(place);
        }
    });
}

function google_initAutoComp(input, autoFunc) {
    var options = {
        //types: ['(regions)'],
        componentRestrictions: { country: 'us' }
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            autoComplete_updateData_failure();
        }
        else {
            autoFunc(place);
        }
    });
}

function google_initAutoCompCityStateOnly(input, autoFunc) {
    var options = {
        types: ['(cities)'],
        componentRestrictions: { country: 'us' }
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            autoComplete_updateData_failure();
        }
        else {
            autoFunc(place);
        }
    });
}

function google_addMarkers(locations) {
    var marker;

    for (var i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker(
            {
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map_google
            });


        // In a later release we add this functionality

        //google.maps.event.addListener(
        //    marker,
        //    'click',
        //    (function (marker, i) {
        //        return function () {
        //            infowindow_google.setContent(locations[i][0]);
        //            infowindow_google.open(map_google, marker);
        //        }
        //    })
        //    (marker, i));
    }

    // If we have only one location, center it
    // If we have more than one location, extend the map to include all

    if (locations.length == 1) {
        map_google.setCenter(new google.maps.LatLng(locations[0][1], locations[0][2]))
    }
    else if (locations.length > 1) {
        var fullBounds = new google.maps.LatLngBounds();

        for (var i = 0; i < locations.length; i++) {
            var point = new google.maps.LatLng(locations[i][1], locations[i][2]);
            fullBounds.extend(point);
        }

        map_google.fitBounds(fullBounds);
    }


}

function google_addMarkersSearchResult(locations) {
    for (var i = 0; i < locations.length; i++) {
        marker = new MarkerWithLabel(
             {
                 position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                 map: map_google,
                 labelContent: locations[i][3],
                 labelAnchor: new google.maps.Point(8, 27),
                 labelClass: "pointMap", // the CSS class for the label
                 labelInBackground: false,
                 icon: '/Images/ico/pin-icon.png'
             });

        if (locations[i][3].length == 2) {
            marker.labelClass = marker.labelClass + " " + "two-digit";
        } else if (locations[i][3].length == 3) {
            marker.labelClass = marker.labelClass + " " + "three-digit";
        } else if (locations[i][3].length == 4) {
            marker.labelClass = marker.labelClass + " " + "four-digit";
        } else if (locations[i][3].length == 5) {
            marker.labelClass = marker.labelClass + " " + "five-digit";
        }

        boxText = document.createElement("div");
        boxText.setAttribute("class", "arrow");

        mainBox = document.createElement("div");
        mainBox.setAttribute("class", "tooltipMap");
        mainBox.innerHTML = "<span class='title'>" + locations[i][0] + "</span>";
        mainBox.appendChild(boxText);

        myOptions = {
            content: mainBox
            //, disableAutoPan: false
            //, maxWidth: 0
           , pixelOffset: new google.maps.Size(-99, -96)
            //, zIndex: null
            , boxStyle: {
                //background: "url('tipbox.gif') no-repeat"
                //opacity: 0.0
                width: "200px",
            }
            //, closeBoxMargin: "10px 2px 2px 2px"
            //, closeBoxURL: "https://www.google.com/intl/en_us/mapfiles/close.gif"
           , infoBoxClearance: new google.maps.Size(1, 1)
            //, isHidden: false
            //, pane: "floatPane"
            //, enableEventPropagation: false
        };

        //google.maps.event.addListener(marker, "mouseover", function (event) {
        //    var ib = new InfoBox(myOptions);
        //    ib.open(map_google, this);
        //});

        var ib = null;
        ib = new InfoBox(myOptions);
        google.maps.event.addListener(marker, "mouseover", ShowTitle1(map_google, ib, locations[i][3]));
        google.maps.event.addListener(marker, "mouseout", ShowTitle2(map_google, ib, locations[i][3]));

        //google.maps.event.addListener(marker, "click", ShowTitle(map_google, marker, locations[i][0]));


        ////map pin size for more than 1 digit 
        //alert($('.pointMap').length);
        ////$('.pointMap').each(function () {
        ////    if ($(this).html().length == 2) {
        ////        $(this).addClass('two-digit');
        ////    } else if ($(this).html().length == 3) {
        ////        $(this).addClass('three-digit');
        ////    }
        ////});
    }


    function ShowTitle1(map, ib, counter) {
        return function () {
            ib.open(map, this);
            $('div.counter > span').each(function () {
                if (counter == $(this).html()) {
                    //alert($(this).parents('.result-item').find('div.counter > span').html());
                    //alert($(this).parents('.result-item').attr('class'));
                    //$(this).parents('.result-item').trigger('click');

                    //$(this).parents('.result-items').trigger('mouseover');
                    //$('.item-icon', $(this).parents('.result-item') ).fadeIn(200);
                    //$('.image', $(this).parents('.result-item')).css({ 'opacity': 1 });
                    //$(this).parents('.result-item.normal').css({ 'border-left': '3px solid #56606D' });
                    //$(this).parents('.result-item').css({ 'background': 'none repeat scroll 0 0 #e6e9ed' });
                    //$(this).parents('.result-item').css({ '-webkit-transition': 'all 0.3s ease-in-out 0s' });
                    //$(this).parents('.result-item').css({ '-moz-transition': 'all 0.3s ease-in-out 0s' });
                    //$(this).parents('.result-item').css({ '-o-transition': 'all 0.3s ease-in-out 0s' });
                    //$(this).parents('.result-item').css({ 'transition': 'all 0.3s ease-in-out 0s' });
                }
            });
        }
    }

    function ShowTitle2(map, ib, counter) {
        return function () {
            ib.close(map, this);
            $('div.counter > span').each(function () {
                if (counter == $(this).html()) {
                    //$(this).parents('.result-item').trigger('hover');
                    //$('.item-icon', $(this).parents('.result-item')).fadeOut(200);
                    //$('.image', $(this).parents('.result-item')).css({ 'opacity': 0.5 });
                    //$(this).parents('.result-item.normal').css({ 'border-left': '0px solid #56606D' });
                    //$(this).parents('.result-item').css({ 'background': 'none' });
                    //$(this).parents('.result-item').css({ '-webkit-transition': 'all 0.3s ease-in-out 0s' });
                    //$(this).parents('.result-item').css({ '-moz-transition': 'all 0.3s ease-in-out 0s' });
                    //$(this).parents('.result-item').css({ '-o-transition': 'all 0.3s ease-in-out 0s' });
                    //$(this).parents('.result-item').css({ 'transition': 'all 0.3s ease-in-out 0s' });
                }
            });
        }

    }
    function ShowTitle(map, marker, title) {
        return function () {
            var infowindow = new google.maps.InfoWindow({ content: title });

            infowindow.close();
            infowindow.open(map, marker);
        }
    }

    //locations[i][0]

    // In a later release we add this functionality

    //google.maps.event.addListener(
    //    marker,
    //    'click',
    //    (function (marker, i) {
    //        return function () {
    //            infowindow_google.setContent(locations[i][0]);
    //            infowindow_google.open(map_google, marker);
    //        }
    //    })
    //    (marker, i));
    //}

    // If we have only one location, center it
    // If we have more than one location, extend the map to include all

    if (locations.length == 1) {
        map_google.setCenter(new google.maps.LatLng(locations[0][1], locations[0][2]))
    }
    else if (locations.length > 1) {
        var fullBounds = new google.maps.LatLngBounds();

        for (var i = 0; i < locations.length; i++) {
            var point = new google.maps.LatLng(locations[i][1], locations[i][2]);
            fullBounds.extend(point);
        }

        map_google.fitBounds(fullBounds);
    }
}

// This is not a general purpose function yet
//
function google_getGeoCode(address) {
    geocoder_google.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map_google.setCenter(results[0].geometry.location);

            var marker = new google.maps.Marker({
                map: map_google,
                position: results[0].geometry.location
            });

            $('#addressLng').val(results[0].geometry.location.lng());
            $('#addressLat').val(results[0].geometry.location.lat());
        }
        else {
            $('#addressGeoCodeStatus').val(1);
        }
    });
}

