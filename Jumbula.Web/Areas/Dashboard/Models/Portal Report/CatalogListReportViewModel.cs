﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CatalogListReportViewModel
    {
        public string ProviderName { get; set; }
        public string CatalogName { get; set; }
        public string UpdateDate { get; set; }
        public string MinGrade { get; set; }
        public string MaxGrade { get; set; }
        public int MinEnrollment { get; set; }
        public int MaxEnrollment { get; set; }
        public int Duration { get; set; }
        public int Week { get; set; }
        public decimal Price { get; set; }
        public string CountiesDistricts { get; set; }
        public List<CategoryViewModel> ActivityCategories { get; set; }
        public string StrActivityCategories {
            get
            {
                if (ActivityCategories != null && ActivityCategories.Any())
                {
                    return string.Join(", ", ActivityCategories.Select(c => c.Name));
                }
                return string.Empty;
            }

        }
        public string ActivityDescription { get; set; }
    }



    }

public class CategoryViewModel
{
    public long Id { get; set; }
    public string Name { get; set; }
}