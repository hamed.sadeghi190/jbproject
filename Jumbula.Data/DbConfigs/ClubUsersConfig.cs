﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class ClubUsersConfig : EntityTypeConfiguration<ClubUser>
    {
        public ClubUsersConfig()
        {
            HasRequired(i => i.Club).WithMany(c => c.Users).HasForeignKey(i => i.ClubId);

        }
    }
}
