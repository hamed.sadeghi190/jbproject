﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Contact;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Jumbula.Business
{
    public class ContactPersonBusiness : IContactPersonBusiness
    {
        #region Fields
        private readonly IContactRepository _contactRepository;
        private readonly IClubRepository _clubRepository;
        private readonly IFieldSettingBusiness _fieldSettingBusiness;
        private readonly IClubBusiness _clubBusiness;

        private readonly string _cell = PropertyHelper.GetPropertyName<ContactPerson, string>(a => a.Cell);
        private readonly string _email = PropertyHelper.GetPropertyName<ContactPerson, string>(a => a.Email);
        private readonly string _nickname = PropertyHelper.GetPropertyName<ContactPerson, string>(a => a.Nickname);
        private readonly string _work = PropertyHelper.GetPropertyName<ContactPerson, string>(a => a.Work);
        private readonly string _doB = PropertyHelper.GetPropertyName<ContactPerson, DateTime?>(a => a.DoB);
        private readonly string _title = PropertyHelper.GetPropertyName<ContactPerson, StaffTitle?>(a => a.Title);
        private readonly string _phone = PropertyHelper.GetPropertyName<ContactPerson, string>(a => a.Phone);
        private readonly string _occupation = PropertyHelper.GetPropertyName<ContactPerson, string>(a => a.Occupation);
        private readonly string _employer = PropertyHelper.GetPropertyName<ContactPerson, string>(a => a.Employer);
        #endregion

        #region Constractors
        public ContactPersonBusiness(IContactRepository contactRepository, IClubRepository clubRepository, IFieldSettingBusiness fieldSettingBusiness, IClubBusiness clubBusiness)
        {
            _contactRepository = contactRepository;
            _clubRepository = clubRepository;
            _fieldSettingBusiness = fieldSettingBusiness;
            _clubBusiness = clubBusiness;
        }
        #endregion

        #region Public methods
        public ContactPerson Get(int id)
        {
            return _contactRepository.Get(id);
        }

        public IQueryable<ContactPerson> GetList()
        {
            return _contactRepository.GetList();
        }

        public IQueryable<ContactPerson> GetList(int clubId)
        {
            return GetList().Where(c => c.Club_Id == clubId);
        }

        public OperationStatus Delete(int id)
        {
            return _clubRepository.Delete(id);
        }

        public ContactCreateViewModel GetCreate(int clubId)
        {
            var fieldSettings = GetContactFieldSettings(clubId);

            var result = new ContactCreateViewModel
            {
                ShowPhone = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _phone),
                ShowEmail = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _email)
            };

            return result;
        }

        public ContactFilterViewModel GetFilters(int partnerId)
        {
            var partner = _clubBusiness.Get(partnerId);

            var result = new ContactFilterViewModel
            {
                Titles = GetStaffTitles(),
                SearchTypes = GetSearchTypes(),
                SearchType = ContactSearchType.Name,
                Members = GetMembers(partner.Id, partner.Name, DefaultDropdownItem.All)
            };

            return result;
        }

        public OperationStatus Create(ContactCreateViewModel contact, int clubId)
        {
            var contactDb = new ContactPerson
            {
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                Phone = contact.Phone,
                PhoneExtension = contact.PhoneExtension,
                Email = contact.Email,
                Club_Id = clubId
            };

            var result = Create(contactDb);

            result.Data = new { contactDb.Id };

            return result;
        }

        public ContactEditViewModel GetEdit(int id, ClubBaseInfoModel currentClub)
        {
            var contact = Get(id);
            var partnerId = currentClub.Id;

            var fieldSettings = GetContactFieldSettings(currentClub.Id);

            var relatedClubs = _clubRepository.GetMembersOfPartner(partnerId, true);

            var isStaff = relatedClubs.SelectMany(s => s.ClubStaffs).Any(s => s.ContactId == contact.Id);
            var isLimitedForChange = isStaff || contact.IsPrimary;

            var email = isStaff ? relatedClubs.SelectMany(s => s.ClubStaffs).FirstOrDefault(s => s.ContactId == contact.Id)?.JbUserRole.User.UserName : contact.Email;

            var result = new ContactEditViewModel()
            {
                Email = email,
                FirstName = contact.FirstName,
                Id = contact.Id,
                LastName = contact.LastName,
                Phone = contact.Phone,
                Nickname = contact.Nickname,
                WorkPhone = contact.Work,
                CellPhone = contact.Cell,
                PhoneExtension = contact.PhoneExtension,
                DoB = contact.DoB,
                Title = contact.Title.ToDescription(),
                StaffTitles = GetStaffTitles(),
                Organizations = GetMembers(partnerId, currentClub.Name),
                IsStaff = isStaff,
                IsAssignable = !isLimitedForChange,
                IsPrimary = contact.IsPrimary,
                OrganizationId = isStaff ? relatedClubs.SelectMany(s => s.ClubStaffs).FirstOrDefault(s => s.ContactId == contact.Id)?.ClubId : contact.Club_Id,
                ShowCellPhone = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _cell),
                ShowEmail = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _email),
                ShowPhone = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _phone),
                ShowNickname = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _nickname),
                ShowWorkPhone = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _work),
                ShowDoB = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _doB),
                ShowTitle = _fieldSettingBusiness.GetFieldActiveStatus(fieldSettings, _title)
            };

            return result;
        }

        public OperationStatus Update(ContactEditViewModel contact, ClubBaseInfoModel currentClub)
        {
            var contactDb = MapContactPerson(contact);

            var partnerId = currentClub.Id;

            var relatedClubs = _clubRepository.GetMembersOfPartner(partnerId, true);

            var isStaff = relatedClubs.SelectMany(s => s.ClubStaffs).Any(s => s.ContactId == contact.Id);

            if (!isStaff)
            {
                contactDb.Club_Id = contact.OrganizationId;
            }

            var result = Update(contactDb);

            return result;
        }

        public ContactSettingsViewModel GetSettings(int clubId)
        {
            var result = new ContactSettingsViewModel();

            var fieldSettings = _fieldSettingBusiness.GetFieldSettings(JbClass.ContactPerson, clubId);

            result.Fields = fieldSettings.Select(f =>
                new PropertyItemContacttSettingsViewModel
                {
                    Id = f.Id,
                    IsEnabled = f.IsEnabled,
                    Name = f.FieldName,
                    Title = f.DisplayName,
                    ShowInGrid = f.ShowInGrid,
                    IsReadOnly = f.IsReadOnly
                })
                .ToList();

            return result;
        }

        public ContactListSettingViewModel GetListSettings(int currentClubId)
        {

            var fieldSettings = _fieldSettingBusiness.GetFieldSettings(JbClass.ContactPerson, currentClubId);

            var result = new ContactListSettingViewModel
            {
                ShowEmail = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _email),
                ShowWorkPhone = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _work),
                ShowCellPhone = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _cell),
                ShowNickname = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _nickname),
                ShowPhone = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _phone),
                ShowTitle = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _title),
                ShowDoB = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _doB),
                ShowOccupation = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _occupation),
                ShowEmployer = _fieldSettingBusiness.GetFieldShowInGridStatus(fieldSettings, _employer)
            };

            return result;
        }

        public OperationStatus UpdateSettings(ContactSettingsViewModel contactSettings, int clubId)
        {
            var contactSettingsDbs = contactSettings.Fields.Select(
                    c =>
                        new JbFieldSetting
                        {
                            Class = JbClass.ContactPerson,
                            ClubId = clubId,
                            FieldName = c.Name,
                            IsEnabled = c.IsEnabled,
                            ShowInGrid = c.ShowInGrid,
                            Id = c.Id
                        })
                .ToList();

            var result = _fieldSettingBusiness.SaveSettings(contactSettingsDbs);

            return result;
        }

        public PaginatedList<ContactItemViewModel> GetPartnerContacts(int partnerId, ContactFilterViewModel filters, List<Sort> sorts, int pageIndex, int pageSize)
        {
            var membersOfPartner = _clubRepository.GetMembersOfPartner(partnerId, true);

            var membersOfPartnerIds = membersOfPartner.Select(r => r.Id).ToList();

            if (filters.MemberId.HasValue)
                membersOfPartnerIds = membersOfPartnerIds.Where(i => i == filters.MemberId.Value).ToList();

            var emailAddress = string.Empty;
            if (filters.SearchType == ContactSearchType.Email)
                emailAddress = filters.Term;

            var clubContacts = _contactRepository.GetClubContacts(membersOfPartnerIds, GetContactFilter(filters), emailAddress, sorts, pageIndex, pageSize);

            var result = clubContacts.Select(MapContactItemViewModel).ToPaginatedList(pageIndex, pageSize, clubContacts.TotalCount);

            return result;
        }

        public OperationStatus Create(ContactPerson contactPerson)
        {
            return _contactRepository.Create(contactPerson);
        }

        public OperationStatus Update(ContactPerson entity)
        {
            var currentContact = Get(entity.Id);

            currentContact.Cell = entity.Cell;
            currentContact.DoB = entity.DoB;
            currentContact.Email = entity.Email;
            currentContact.Employer = entity.Employer;
            currentContact.FirstName = entity.FirstName;
            currentContact.LastName = entity.LastName;
            currentContact.Nickname = entity.Nickname;
            currentContact.Occupation = entity.Occupation;
            currentContact.Phone = entity.Phone;
            currentContact.PhoneExtension = entity.PhoneExtension;
            currentContact.Title = entity.Title;
            currentContact.Work = entity.Work;
            currentContact.Club_Id = entity.Club_Id;

            return _contactRepository.Update(currentContact);
        }

        #endregion

        #region Private methods

        private static Expression<Func<ContactPerson, bool>> GetContactFilter(ContactFilterViewModel filters)
        {
            Expression<Func<ContactPerson, bool>> result = r => true ;

            if (filters == null) return result;

            if (!string.IsNullOrEmpty(filters.Title))
            {
                var enumTitle = EnumHelper.ParseEnum<StaffTitle>(filters.Title);
                result = person => person.Title == enumTitle;
            }

            var term = !string.IsNullOrWhiteSpace(filters.Term) ? filters.Term.ToLower() : string.Empty;
            if (string.IsNullOrWhiteSpace(filters.Term)) return result;
            switch (filters.SearchType)
            {
                case ContactSearchType.Name:
                    {
                        result = person => person.FirstName.Contains(term) || person.LastName.Contains(term);
                    }
                    break;

                case ContactSearchType.Phone:
                    {
                        term = term.Replace("-", "");
                        result = person => person.Phone.Contains(term);
                    }
                    break;
            }

            return result;
        }

        private static string GetContactTitle(ContactPerson contact)
        {
            var result = contact.Title.HasValue ? contact.Title.ToDescription() : string.Empty;

            if (string.IsNullOrWhiteSpace(contact.StaffRole)) return result;

            result = StringHelper.Join(Constants.S_Slash, result, contact.StaffRole);

            return result;
        }

        private static ContactItemViewModel MapContactItemViewModel(ContactPerson contactPerson)
        {
            return new ContactItemViewModel
            {
                OrganizationName = contactPerson.Club_Id.HasValue ? contactPerson.Club.Name : string.Empty,
                CellPhone = PhoneNumberHelper.FormatPhoneNumber(contactPerson.Cell),
                Email = contactPerson.Email,
                FirstName = contactPerson.FirstName,
                LastName = contactPerson.LastName,
                FullName = $"{contactPerson.FirstName} {contactPerson.LastName}",
                Id = contactPerson.Id,
                Nickname = contactPerson.Nickname,
                Phone = PhoneNumberHelper.FormatPhoneNumber(contactPerson.Phone),
                PhoneExtension = contactPerson.PhoneExtension,
                Title = GetContactTitle(contactPerson),
                WorkPhone = PhoneNumberHelper.FormatPhoneNumber(contactPerson.Work),
                DoB = contactPerson.DoB,
                IsStaff = contactPerson.IsStaff,
                StaffStatus = contactPerson.StaffStatus.ToDescription(),
                IsPrimary = contactPerson.IsPrimary,
                IsLimitedForChange = contactPerson.IsStaff || contactPerson.IsPrimary
            };
        }

        private static ContactPerson MapContactPerson(ContactEditViewModel contactPerson)
        {
            return new ContactPerson
            {
                Cell = contactPerson.CellPhone,
                Email = contactPerson.Email,
                FirstName = contactPerson.FirstName,
                LastName = contactPerson.LastName,
                Id = contactPerson.Id,
                Nickname = contactPerson.Nickname,
                Phone = contactPerson.Phone,
                PhoneExtension = contactPerson.PhoneExtension,
                Title = GetStaffTitle(contactPerson.Title),
                Work = contactPerson.WorkPhone,
                DoB = contactPerson.DoB,
                IsStaff = contactPerson.IsStaff,
                IsPrimary = contactPerson.IsPrimary,
            };
        }

        private static List<SelectListItem> GetStaffTitles()
        {
            var result = DropdownHelper.GetEnumList<StaffTitle>().OrderBy(o => o.Text).ToList();

            result.AddItemToFirst(DefaultDropdownItem.All);

            return result;
        }

        private static StaffTitle? GetStaffTitle(string staffTitle)
        {
            if (string.IsNullOrWhiteSpace(staffTitle) || staffTitle == DefaultDropdownItem.All.ToDescription())
            {
                return null;
            }

            var result = EnumHelper.ParseEnum<StaffTitle>(staffTitle);

            return result;
        }

        private static List<SelectListItem> GetSearchTypes()
        {
            var result = DropdownHelper.GetEnumList<ContactSearchType>().OrderBy(o => o.Text).ToList();

            return result;
        }

        private List<SelectListItem> GetMembers(int partnerId, string partnerName, DefaultDropdownItem? defaultItem = null)
        {
            var result = new List<SelectListItem>();

            if (defaultItem.HasValue)
            {
                result.AddItemToFirst(defaultItem.Value);
            }

            result.AddItemAtEnd(partnerId.ToString(), partnerName, Constants.SchoolTypeConstants.Partner);

            result.AddRange(_clubBusiness.GetRelatedClubs(partnerId).ToList().OrderBy(o => o.Name).Select(m =>
                    new SelectListItem
                    {
                        Group = new SelectListGroup { Name = m.IsSchool ? Constants.SchoolTypeConstants.School : (m.IsProvider ? Constants.SchoolTypeConstants.Provider : Constants.SchoolTypeConstants.SchoolDistrict) },
                        Text = m.Name,
                        Value = m.Id.ToString()
                    })
                     .ToList());

            result.AddItemAtEnd(DefaultDropdownItem.Other);

            return result;
        }

        private List<JbFieldSetting> GetContactFieldSettings(int clubId)
        {
            return _fieldSettingBusiness.GetFieldSettings(JbClass.ContactPerson, clubId);
        }
        #endregion
    }
}
