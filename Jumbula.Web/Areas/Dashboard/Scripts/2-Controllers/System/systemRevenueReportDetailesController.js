﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemRevenueReportDetailesController', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'seasons-view order'
        window.app.globalObjects.initializeView();
        $scope.baseUrl = window.location.origin + "/Dashboard/System";
       
        $scope.MC.stateParams = $stateParams;
        $scope.reportType = $stateParams.reportType;
        $scope.date = $stateParams.date;
    

        $http.get($scope.baseUrl + '/GetRevenueDate?date=' + $stateParams.date).success(function (data, status, headers, config) {
            $scope.Model = data;
        });


        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetClientMonthlyAmount?date=' + $stateParams.date,
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                              
                           }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            pageSize: 50,
            batch: true,
            serverPaging: true,
            schema: {
                data: "DataSource",
                total: "TotalCount"
            },
            aggregate: [
                        { field: "ChargeWithCredit", aggregate: "sum"},
                         { field: "ChareWithManualy", aggregate: "sum" },
                         { field: "Percent", aggregate: "sum" },
                         { field: "TotalCharge", aggregate: "sum" },

            ],
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            resizable: true,
            serverPaging: true,
            columnMenu:true,
            columns: [
                {
                    field: "Name",
                    title: "Name",
                },
                {
                    field: "ChargeWithCredit",
                    title: "Charge",
                    format: "{0:c2}",
                    footerTemplate: '#= kendo.toString(sum,"c2")#'
                    
                },
                 {
                     field: "ChareWithManualy",
                     title: "Manual Charge",
                     format: "{0:c2}",
                     footerTemplate: ' #= kendo.toString(sum, "c2") #'
                     
                 },
                 {
                     field: "Percent",
                     title: "Percent",
                     format: "{0:c2}",
                     footerTemplate: ' #= kendo.toString(sum, "c2") #'
                     
                 },
                 {
                     field: "TotalCharge",
                     title: "TotalCharge",
                     format: "{0:c2}",
                     footerTemplate: ' #= kendo.toString(sum, "c2") #'
                 },
            ],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
        }
    }]);
})();