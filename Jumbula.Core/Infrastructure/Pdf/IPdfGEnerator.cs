﻿namespace Jumbula.Core.Infrastructure.Pdf
{
    interface IPdfGenerator
    {
        object Generate(object model);
    }
}
