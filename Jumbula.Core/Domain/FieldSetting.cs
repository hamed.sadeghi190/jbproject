﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using Jumbula.Common.Attributes;
using Jumbula.Common.Base;
using Jumbula.Common.Helper;

namespace Jumbula.Core.Domain
{
    public class JbFieldSetting : BaseEntity
    {
        public string FieldName { get; set; }

        [NotMapped]
        public string DisplayName
        {
            get
            {
                var result = string.Empty;

                var type = Type.GetType(this.Class.ToDescription());

                DisplayAttribute displayAttribute = null;

                if (type != null)
                {
                    displayAttribute = type.GetProperty(this.FieldName).GetCustomAttribute<DisplayAttribute>();
                }

                if (displayAttribute != null)
                {
                    result = displayAttribute.Name;
                }
                else
                {
                    result = this.FieldName;
                }

                return result;
            }
        }

        [NotMapped]
        public bool IsReadOnly
        {
            get
            {
                var result = false;

                var type = Type.GetType(this.Class.ToDescription());

                JbCustomFieldAttribute jbCustomizeFieldAttribute = null;

                if (type != null)
                {
                    jbCustomizeFieldAttribute = type.GetProperty(this.FieldName).GetCustomAttribute<JbCustomFieldAttribute>();
                }

                if (jbCustomizeFieldAttribute != null)
                {
                    result = jbCustomizeFieldAttribute.IsReadOnly;
                }
                else
                {
                    result = false;
                }

                return result;
            }
        }

        public JbClass Class { get; set; }

        public bool IsEnabled { get; set; }

        public bool ShowInGrid { get; set; }

        public int ClubId { get; set; }

        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }

        public static List<JbFieldSetting> GetFieldSettings(PropertyInfo[] propertyInfos)
        {
            return propertyInfos.ToList().Select(p =>
              new JbFieldSetting
              {
                  FieldName = p.Name,
                  IsEnabled = ReflectionHelper.GetPropertyAttribute<JbCustomFieldAttribute>(p).IsEnabled,
                  ShowInGrid = ReflectionHelper.GetPropertyAttribute<JbCustomFieldAttribute>(p).ShowInGrid,
              })
              .ToList();
        }
    }

    public enum JbClass
    {
        [Description("Jumbula.Core.Domain.ContactPerson, Jumbula.Core")]
        ContactPerson
    }
}
