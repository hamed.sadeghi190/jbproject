﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class TransactionActivity : BaseEntity<long>
    {
        public TransactionActivity()
        {
            HandleMode=HandleMode.Online;
            this.MetaData = new MetaData()
            {
                DateCreated = DateTime.UtcNow,
                DateUpdated = DateTime.UtcNow
            };
           
        }

        public bool IsAutoCharge { get; set; }
        public int AutoChargePolicyAttempt { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public TransactionType TransactionType { get; set; }
        public TransactionCategory TransactionCategory { get; set; }
        [MaxLength(1000)]
        public string Note { get; set; }
        [MaxLength(128)]
        public string CheckId { get; set; }
        public int ClubId { get; set; }
        public long? SeasonId { get; set; }
        public long? OrderId { get; set; }
        public long? OrderItemId { get; set; }
        public long? InstallmentId { get; set; }
        public long PaymentDetailId { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
        public string Token { get; set; }
        public virtual Club Club { get; set; }
        public virtual Season Season { get; set; }
        public virtual Order Order { get; set; }
        public virtual OrderItem OrderItem { get; set; }
        public virtual OrderInstallment Installment { get; set; }
        public virtual PaymentDetail PaymentDetail { get; set; }

        public MetaData MetaData { get; set; }

        public HandleMode HandleMode { get; set; }

        [NotMapped]
        public string strTransactionDate
        {
            get
            {
                return TransactionDate.ToString("ddMMyyyyHHmmss");
            }
        }

    }

    public enum TransactionCategory : byte
    {
        Sale = 0,
        Cancellation = 1,
        Refund = 2,
        Transfer = 3,
        Installment = 4,
        Invoice = 5,
        Membership = 6,
        [Description("Preapproval sale")]
        PreapprovalSale = 7,
        [Description("Preapproval installment")]
        PreapprovalInstallment = 8,
        [Description("Take a payment")]
        TakePayment = 9,
        [Description("Family credit transfer")]
        FamilyCreditTransfer = 10
    }

    public enum TransactionType : byte
    {
        Payment = 0,
        Refund = 1

    }
    public enum TransactionStatus
    {
        [Description("Received")]
        Success = 0,
        [Description("Failure")]
        Failure = 1,
        [Description("Invoiced")]
        Invoice = 2,
        [Description("Draft")]
        Draft = 3,
        [Description("Deleted")]
        Deleted = 4,
        [Description("Pending")]
        Pending = 5,
    }

    public enum HandleMode : byte
    {
        Online = 0,
        Offline = 1
    }
}
