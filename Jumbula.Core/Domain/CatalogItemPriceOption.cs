﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Jumbula.Core.Domain
{
    public class CatalogItemPriceOption
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public decimal Amount { get; set; }

        public string DurationTitle { get; set; }

        [ForeignKey("CatalogItemId")]
        public virtual CatalogItem CatalogItem { get; set; }

        public int? CatalogItemId { get; set; }

        [ForeignKey("CatalogSettingId")]
        public virtual CatalogSetting CatalogSetting { get; set; }

        public int? CatalogSettingId { get; set; }

        [Column("Attribute")]
        public string AttributeSerialized { get; set; }

        CatalogPriceOptionAttribute _attribute;

        [NotMapped]
        public CatalogPriceOptionAttribute Attribute
        {
            get
            {
                if (_attribute == null && !string.IsNullOrEmpty(AttributeSerialized))
                {
                    _attribute = JsonConvert.DeserializeObject<CatalogPriceOptionAttribute>(AttributeSerialized);
                }

                return _attribute;
            }

        }
    }

    public class CatalogPriceOptionAttribute
    {
        public CatalogPriceOptionAttribute()
        {
            AgendaList = new List<CatalogPriceOptionAgendaItem>();
        }
        public List<CatalogPriceOptionAgendaItem> AgendaList { get; set; }
    }

    public class CatalogPriceOptionAgendaItem
    {
        public string Title { get; set; }
        public string AgendaWeek { get; set; }
    }
}
