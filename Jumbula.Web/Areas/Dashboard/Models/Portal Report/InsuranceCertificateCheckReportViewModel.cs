﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class InsuranceCertificateCheckReportViewModel
    {
        public string ProviderName { get; set; }

        public string Email { get; set; }

        public string Insurance { get; set; }

        public string Date { get; set; }

        public DateTime? AgreementExpirationDate { get; set; }
    }
}