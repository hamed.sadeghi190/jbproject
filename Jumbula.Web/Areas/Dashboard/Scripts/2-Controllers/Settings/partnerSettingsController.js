﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('partnerSettingsController', ['$scope', '$http', 'jbToast', 'uiComponentsService', function ($scope, $http, jbToast, uiComponentsService) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/Setting";

        $http.get($scope.baseUrl + '/GetPartnerSetting').success(function (data, status, headers, config) {
            $scope.Model = data;
        });

        $scope.save = function () {
            $http.post($scope.baseUrl + '/SavePartnerSetting', { model: $scope.Model }).success(function (data, status, headers, config) {

                if (data.Status) {
                    $scope.errors = null;
                    jbToast.success('Partner settings saved successfully.');
                }
                else {
                    $scope.MC.handleErrors($scope, data);
                }
            });
        }

        $scope.toggleTooltip = function (containerId) {
            if (arguments[3])
                uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2], arguments[3]);
            else if (arguments[2])
                uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2]);
            else if (arguments[1])
                uiComponentsService.toggleTooltip(containerId, arguments[1]);
            else
                uiComponentsService.toggleTooltip(containerId);
        }

    }]);
})();