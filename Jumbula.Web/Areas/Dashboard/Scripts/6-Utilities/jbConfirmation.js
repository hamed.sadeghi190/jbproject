﻿(function () {
    'use strict';

    angular.module('jbTools').service("jbConfirmation", ['scopeServise', function (scopeServise) {
        var title, template, isTemplateMode, message, yes, no, yesText, noText,
            clear = function () {
                title = message = yes = no = yesText = noText = null;
                scopeServise.get().jbConfirmation = {};
            };
        this.title = function (t) {
            title = t;
            return this;
        };
        this.message = function (m) {
            message = m;
            return this;
        };
        this.template = function (t) {
            template = t;
            return this;
        };
        this.yes = function (y) {
            yes = y;
            return this;
        };
        this.no = function (n) {
            no = n;
            return this;
        };
        this.yesText = function (y) {
            yesText = y;
            return this;
        };
        this.noText = function (n) {
            noText = n;
            return this;
        };

        this.confirm = function () {
            var scope = scopeServise.get();
            if (is.not.existy(scope) || is.not.object(scope)) {
                throw "Please check your inputs!\n" +
                    "e.g:\n" +
                    "jbConfirmation.yes(function(){alert(\"Deleted\")})\n" +
                    "\t.no(function(){alert(\"Canceled\")}) // optional\n" +
                    "\t.title('Warning') // optional\n" +
                    "\t.message('Are you sure you want to delete this item?') // optional\n" +
                    "\t.yesText('Yes, I\'m sure') // optional\n" +
                    "\t.noText('Cancel') // optional\n" +
                    "\t.confirm();";
            }

            scope.jbConfirmation = {
                show: true,
                keyup: function (e) {
                    if (e.keyCode == 27) {
                        scope.jbConfirmation.no();
                    }
                },
                focusOnYes: function (e) {
                    $(e.currentTarget).find(".yes").focus();
                },
                title: title || "Warning!",
                message: message || "Are you sure you want to proceed with this action?",
                template: template,
                isTemplateMode: template != null && template != 'undefined',
                yes: function () {
                    yes();
                    clear();
                },
                no: function () {
                    if (no) {
                        no();
                    }
                    clear();
                },
                yesText: yesText || "Yes, I'm sure",
                noText: noText || "Cancel"
            };
        };
    }]);
})();