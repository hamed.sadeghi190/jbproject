﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class BillingTransactionActivity : BaseEntity<long>
    {
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public TransactionType TransactionType { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
     
        public PaymentMethod PaymentMethod { get; set; }
       
        public string TransactionResponse { get; set; }
       
        [StringLength(128)]
        public string TransactionId { get; set; }
        public long BillingId { get; set; }
        [ForeignKey("BillingId")]
        public virtual ClientBillingHistory Billing { get; set; }
        public long? CreditCardId { get; set; }
        [ForeignKey("CreditCardId")]
        public virtual ClientCreditCard CreditCard { get; set; }
    }
}
