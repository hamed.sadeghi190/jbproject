﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum DelinquentPolicies : byte
    {
        [Description("Policy #1")]
        AutoChargeFailAttemptNumber = 1,
        [Description("Policy #2")]
        ManualInstallmentDelayDays = 2,
        [Description("Policy #3")]
        FamilyBalanceNoLimitDate = 3
    }
}
