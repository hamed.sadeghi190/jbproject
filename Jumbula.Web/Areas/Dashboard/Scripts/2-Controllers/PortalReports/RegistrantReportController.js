﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('RegistrantReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReports", order: 1, menu: "PortalReports" });


            var baseUrl = window.location.origin + '/Dashboard/PortalReport';


            $scope.stateParams = $stateParams;
            $scope.MC.stateParams = $stateParams;
            $scope.Model = {};


            $scope.reportName = "";
            var nameModel = customReportService.getModelFromStrorage();
            var seasonId = nameModel.seasonId;
            var schoolId = nameModel.schoolId;


            if (is.not.undefined(nameModel) && nameModel != null) {


                $http.get('Dashboard/PortalReport/GetReoprtNameWithSeason', { params: { schoolId: schoolId, seasonId: seasonId } }).success(function (data, status, headers, config) {
                    $scope.reportName = data;


                }).error(function (data, status, headers, config) {

                    jbToast.error(data.Message);
                });
            }

            $scope.ProgramsReportGridOptions = {

                excel: {
                    allPages: true,
                    fileName: "School Season Reconciliation.xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();
                },
                dataSource: {

                    type: "json",
                    transport: {
                        read: {
                            url: '/Dashboard/PortalReport/SelectOrderList?seasonId=' + seasonId + '&schoolId=' + schoolId,
                        }

                    },
                    schema: {

                        total: "TotalCount",

                        data: "DataSource"
                    },
                    aggregate: [
                   { field: "TotalClassFees", aggregate: "sum" },
                    { field: "CashPayment", aggregate: "sum" },
                    { field: "AmtPTAPTOfundedScholarship", aggregate: "sum" },
                    { field: "ProvFundedScholarship", aggregate: "sum" },
                    { field: "SubtotalClassFeesPaid", aggregate: "sum" },
                    { field: "feepaid", aggregate: "sum" },
                    { field: "PTAPTODonationpaid", aggregate: "sum" },
                    { field: "ActivityFeepaid", aggregate: "sum" },
                    { field: "TotalPayment", aggregate: "sum" },
                    { field: "LateRegistrationfees", aggregate: "sum" },
                    { field: "CancellationFeePaid", aggregate: "sum" },
                    { field: "RefundAmount", aggregate: "sum" },
                    { field: "JumbulaFeePaid", aggregate: "sum" },
                    { field: "TotalStripeFees", aggregate: "sum" },
                    { field: "CustomDiscount", aggregate: "sum" },
                    { field: "CustomCharge", aggregate: "sum" },

                    ],

                    batch: true,
                    serverSorting: false,
                    resizable: true,
                },
                scrollable: true,
                sortable: true,
                editable: false,
                dataBound: function () {
                    var grid = $("#ProgramsReportGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                columns: [

                     {
                         field: "RegistrantName",
                         title: "Registrant name",
                         width: "260px",
                         footerTemplate: "Total"
                     },
                      {
                          field: "Confirmation",
                          title: "Confirmation #",
                          width: "170px"
                      },
                       {
                           field: "TotalClassFees",
                           title: "Total class fees",
                           width: "160px",
                           format: "{0:c2}",
                           footerTemplate: '#= kendo.toString(sum, "c2")# ',

                       },

                        {
                            field: "CashPayment",
                            title: "Cash payment to PTA/PTO",
                            width: "200px",
                            format: "{0:c2}",
                            footerTemplate: ' #= kendo.toString(sum, "c2") #'
                        },
                         {
                             field: "AmtPTAPTOfundedScholarship",
                             title: "$ Amt of PTA/PTO-funded scholarship",
                             width: "300px",
                             format: "{0:c2}",
                             footerTemplate: '#= kendo.toString(sum, "c2")# ',
                         },
                          {
                              field: "ProvFundedScholarship",
                              title: "$ Amt of prov-funded scholarship",
                              width: "330px",
                              format: "{0:c2}",
                              footerTemplate: '#= kendo.toString(sum, "c2")# ',
                          },
                          {
                             field: "CustomDiscount",
                             title: "Custom discount",
                             width: "250px",
                             format: "{0:c2}",
                            footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                           },
                           {
                               field: "SubtotalClassFeesPaid",
                               title: "Subtotal class fees paid",
                               width: "250px",
                               format: "{0:c2}",
                               footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                           },
                          {
                              field: "feepaid",
                              title: "EM Fee paid",
                              width: "200px",
                              format: "{0:c2}",
                              footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                          },
                          {
                              field: "PTAPTODonationpaid",
                              title: "PTA/PTO donation paid",
                              width: "200px",
                              format: "{0:c2}",
                              footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                          },
                          {
                              field: "ActivityFeepaid",
                              title: "PTA/PTO activity fee paid",
                              width: "250px",
                              format: "{0:c2}",
                              footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                          },

                          {
                              field: "CustomCharge",
                              title: "Custom charge",
                              width: "250px",
                              format: "{0:c2}",
                              footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                          },
                            {
                                field: "TotalPayment",
                                title: "Total payment",
                                width: "200px",
                                format: "{0:c2}",
                                footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                            },
                            {
                                field: "LateRegistrationfees",
                                title: "Late registration fees",
                                width: "230px",
                                format: "{0:c2}",
                                footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                            },
                            {
                                field: "CancellationFeePaid",
                                title: "Cancellation fee paid",
                                width: "230px",
                                format: "{0:c2}",
                                footerTemplate: ' #= kendo.toString(sum, "c2")# ',

                            },
                              {
                                  field: "RefundAmount",
                                  title: "Refund amount",
                                  width: "200px",
                                  format: "{0:c2}",
                                  footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                              },
                               {
                                   field: "TotalStripeFees",
                                   title: "Total stripe fees",
                                   width: "200px",
                                   format: "{0:c2}",
                                   footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                               },
                                {
                                    field: "JumbulaFeePaid",
                                    title: "Jumbula fee paid",
                                    width: "200px",
                                    format: "{0:c2}",
                                    footerTemplate: ' #= kendo.toString(sum, "c2")# ',

                                },
                ],

                Sort: {
                    field: "RegistrantName",
                    dir: "Asc"
                },
            };

            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                $("#ProgramsReportGrid").getKendoGrid().saveAsExcel();
            }
        }
    ]);


})();