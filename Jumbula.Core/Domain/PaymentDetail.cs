﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class PaymentDetail : BaseEntity<long>
    {

        public PaymentDetail()
        {
        }

        public PaymentDetail(string payerEmail, PaymentDetailStatus paymentDetailStatus, string payKey, PaymentMethod paymentMethod, CurrencyCodes currency, string res,string description, string transactionId, RoleCategoryType payerType, int? creditcardId = null, int? invoiceId = null, ActionReason? actionReason = null, string relatedTransactionId = null)
        {
            Currency = currency;
            PayerEmail = payerEmail;
            PaymentMethod = paymentMethod;
            PaypalIPN = res;
            TransactionMessage = description;
            Status = paymentDetailStatus;
            PayKey = payKey;
            TransactionId = transactionId;
            CreditCardId = creditcardId;
            PayerType = payerType;
            InvoiceId = invoiceId;
            ActionReason = actionReason;
            RelatedTransactionId = relatedTransactionId;
        }
        public PaymentMethod? PaymentMethod { get; set; }
        [StringLength(128)]
        public string PayKey { get; set; }

        [StringLength(1024)]
        public string TransactionMessage { get; set; }
        public string PaypalIPN { get; set; }
        public int? InvoiceId { get; set; }
        [ForeignKey("InvoiceId")]
        public virtual Invoice invoice { get; set; }
        public CurrencyCodes Currency { get; set; }

        [StringLength(128)]

        public string PayerEmail { get; set; }
        public PaymentDetailStatus Status { get; set; }
        public virtual ICollection<TransactionActivity> TransactionActivities { get; set; }
        public virtual PostalAddress BillingAddress { get; set; }
        [StringLength(128)]
        public string TransactionId { get; set; }

        [StringLength(128)]
        public string RelatedTransactionId { get; set; }
        public int? CreditCardId { get; set; }
        [ForeignKey("CreditCardId")]
        public virtual UserCreditCard CreditCard { get; set; }
        public RoleCategoryType PayerType { get; set; }

        public ActionReason? ActionReason { get; set; }

    }

    public enum PaymentDetailStatus
    {
        [Description(" The payment request was received; funds will be transferred once the payment is approved")]
        CREATED = 0,
        [Description("The payment was successful")]
        COMPLETED = 1,
        [Description("Some transfers succeeded and some failed for a parallel payment or, for a delayed chained payment, secondary receivers have not been paid")]
        INCOMPLETE = 2,
        [Description("The payment failed and all attempted transfers failed or all completed transfers were successfully reversed")]
        ERROR = 3,
        [Description("One or more transfers failed when attempting to reverse a payment")]
        REVERSALERROR = 4,
        [Description("The payment is in progress")]
        PROCESSING = 5,
        [Description("The payment is awaiting processing")]
        PENDING = 6,
        [Description("Payment mismatched amount")]
        MISMATCHEDAMOUNT = 7,
        [Description("The Preapproval agreement was cancelled")]
        CANCELED = 8,
        EXPIRED = 9
    }


}
