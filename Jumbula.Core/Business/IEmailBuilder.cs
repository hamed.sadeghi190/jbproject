﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using System.Collections.Generic;
using System.Net.Mail;
using Jumbula.Core.Model.Email;

namespace Jumbula.Core.Business
{
    public interface IEmailBuilder
    {
        EmailCategory Category { get; }

        EmailCallerType CallerType { get; }
        string TemplateName { get; }
        string GenerateBody(Email email, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal);
        string GetSubject(string subCategory = null);
        MailAddress GetFrom(string subCategory = null);
        string GetFromDisplayName(string subCategory = null);
        string GetReplyTo(string subCategory = null);
        IEnumerable<MailAddress> GetCCs(string subCategory = null);
        IEnumerable<MailAddress> GetTos(string subCategory = null);
        IEnumerable<MailAddress> GetBcCs(string subCategory = null);
        object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal);
        Email GetEmail(EmailViewModel emailViewModel);
        string GeneratePreviewBody(EmailViewModel emailViewModel);
        List<CommunicationHistory> GetHistories();
        IEnumerable<string> SubCategories { get; }

        EmailSendPriority EmailSendPriority { get; }

        bool ShouldSendToSupport { get; }
    }

    public interface IEmailBuilder<TEmailParameter> : IEmailBuilder where TEmailParameter : class
    {
        void Initiate(TEmailParameter parameters);

        void InitiatePreview(TEmailParameter parameters);
        IEnumerable<Email> GetEmails(TEmailParameter parameter, bool isProductionMode);

        Email GetEmail(TEmailParameter parameter, string subCategory = Common.Constants.Email.PrimarySubCategory);

        string GeneratePreviewBody(TEmailParameter parameters, string subCategory);
        string SerializeParameter(TEmailParameter parameters);

        TEmailParameter GetParameter(string parameter);

        EmailViewModel GetEmailViewModel(TEmailParameter parameter);
    }
}
