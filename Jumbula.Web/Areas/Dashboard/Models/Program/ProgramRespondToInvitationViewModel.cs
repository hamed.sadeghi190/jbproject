﻿using System;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{

    public class ProgramRespondToInvitationViewModel
    {
        public ProgramRespondToInvitationViewModel()
            {
            ListOfRespondTypes= DropdownHelpers.ToSelectListWithIntValue<ProgramRespondType>();
            ListOfDeclineResaons = DropdownHelpers.ToSelectListWithIntValue<PorgramRespondDeclineResaon>();
            ListOfFirstDaysOfWeek = DropdownHelpers.ToSelectListWithIntValue<DayOfWeek>("Select 1st day preference",-1);
            FirstDayPreference = -1;
            SecondDayPreference = -1;
            ListOfSecondDaysOfWeek = DropdownHelpers.ToSelectListWithIntValue<DayOfWeek>("Select 2nd day preference",-1);
        }
        public List<SelectKeyValue<int>> ListOfRespondTypes { get; set; }
        public int RespondType { get; set; }
        public long ProgramId { get; set; }
        public string ProgramName { get; set; }
        public bool AvailableAnyDay { get; set; }
        public List<SelectKeyValue<int>> ListOfFirstDaysOfWeek { get; set; }
        public int FirstDayPreference { get; set; }
        public List<SelectKeyValue<int>> ListOfSecondDaysOfWeek { get; set; }
        public int SecondDayPreference { get; set; }
        public List<SelectKeyValue<int>> ListOfDeclineResaons { get; set; }
        public int DeclineResaon { get; set; }
        public string Note { get; set; }
    }
}