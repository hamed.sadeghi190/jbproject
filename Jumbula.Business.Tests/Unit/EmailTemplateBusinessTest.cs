﻿using FluentAssertions;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Helper;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class EmailTemplateBusinessTest
    {
        #region Fields
        private Mock<IRepository<EmailTemplate>> _mockEmailTemplateRepository;
        private Mock<IStorageBusiness> _mockStorageBusiness;
        private Mock<IClubBusiness> _mockClubBusiness;
        private Mock<IFileBusiness> _mockFileBusiness;
        //private Mock<MemoryStream> _mockMemoryStream;
        #endregion

        #region Constants
        private const int TemplateId = 10;
        private const int ClubId = 100;
        private const string TemplateName = "Hamid";
        private const string ClubDomain = "qaclubsand";
        private const string FolderName = "EmailTemplate";
        private const string ClubLogo = "http://127.0.0.1:10000/devstoreaccount1/clubs/qaclubsand/https://qaclubsand.jumbula.local/Images/provider-catalog-default-banner.jpg?v=1904650218";
        private const string HeaderBackground = "http://127.0.0.1:10000/devstoreaccount1/clubs/qaclubsand/emailTemplate/a1c91625-2f09-42f4-afb1-f81880d760d5.jpg?rnd=87ea8890-547d-406e-9fdb-de568f18695d";
        //private const string FolderName = "Images";
        //private const string FileName = "xy";
        //private const string ContentType = "image/jpeg";
        #endregion

        #region Test methods
        [TestMethod]
        public void Get_GetWithId_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.Get(TemplateId)).Returns(FillEmailTemplateList().Single(x => x.Id == TemplateId));

            // Act
            var result = service.Get(TemplateId);

            // Assert
            result.Id.Should().Be(TemplateId);
        }

        [TestMethod]
        public void Get_GetWithMistakeId_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var templateId = 15;

            _mockEmailTemplateRepository.Setup(x => x.Get(templateId)).Returns(null as EmailTemplate);

            // Act
            var result = service.Get(templateId);

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetClubTemplates_ClubIdIsDifferent_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.GetList()).Returns(FillEmailTemplateListWithMistakeClubId().AsQueryable);

            // Act
            var result = service.GetClubTemplates(ClubId);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetClubTemplates_TemplateTypeIsDifferent_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.GetList()).Returns(FillEmailTemplateListWithMistakeType().AsQueryable);

            // Act
            var result = service.GetClubTemplates(ClubId);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetClubTemplates_GetWithClubIdAndTemplateType_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.GetList()).Returns(FillEmailTemplateList().AsQueryable);

            // Act
            var result = service.GetClubTemplates(ClubId);

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void UpdateTemplate_UpdateTemplateAndTemplateName_ShouldReturnTrueStatus()
        {
            // Arrange
            var service = CreateService();
            var emailTemplate = FillEmailTemplate();

            _mockEmailTemplateRepository.Setup(x => x.Update(emailTemplate)).Returns(new OperationStatus { Status = true });

            // Act
            var result = service.UpdateTemplate(emailTemplate);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void CreateTemplate_CreateNewTemplate_ShouldReturnTrueStatus()
        {
            // Arrange
            var service = CreateService();
            var fillEmailTemplate = FillEmailTemplate();
            _mockEmailTemplateRepository.Setup(x => x.Create(fillEmailTemplate)).Returns(new OperationStatus { Status = true });

            // Act
            var result = service.CreateTemplate(fillEmailTemplate);

            // Assert
            result.Status.Should().BeTrue();
        }
        [TestMethod]
        public void DeleteTemplate_DeleteWithTemplateId_ShouldReturnTrueStatus()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.Delete(TemplateId)).Returns(new OperationStatus { Status = true });

            // Act
            var result = service.DeleteTemplate(TemplateId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void GetDefaultClubTemplate_GetDefaultWithTemplateName_ShouldReturnValidId()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.GetList()).Returns(FillEmailTemplateList().AsQueryable);

            // Act
            var result = service.GetDefaultClubTemplate();

            // Assert
            result.Id.Should().Be(TemplateId + 3);
        }

        [TestMethod]
        public void GetDefaultClubTemplate_DoNotHaveDefaultTemplate_ShouldReturnNull()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.GetList()).Returns(FillEmailTemplateListWithoutDefault().AsQueryable);

            // Act
            var result = service.GetDefaultClubTemplate();

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void UploadImageTemplate_ImageIsNotNull_ShouldReturnNotBeNullOrEmpty()
        {
            // Arrange
            var service = CreateService();

            _mockFileBusiness.Setup(x => x.UploadImage(It.IsAny<string>(), ClubDomain, FolderName)).Returns($"{Guid.NewGuid()}.jpg");

            // Act
            var result = service.UploadImageTemplate(It.IsAny<string>(), ClubDomain, FolderName);

            // Assert
            result.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public void GetEmailTemplate_TemplateIdIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.GetEmailTemplate(null);

            // Assert
            result.ClubDomain.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void GetEmailTemplate_TemplateIdIsNotNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.Get(TemplateId)).Returns(FillEmailTemplate);
            _mockClubBusiness.Setup(x => x.GetClubLogoURL(ClubDomain, ClubLogo)).Returns(Convert.ToString(new byte[1024]));

            // Act
            var result = service.GetEmailTemplate(TemplateId);

            // Assert
            result.ClubDomain.Should().Be(ClubDomain);
            result.HeaderBackground.Should().Be(HeaderBackground);
            result.FooterBackground.Should().Be(HeaderBackground);
        }

        [TestMethod]
        public void GetEmailTemplate_FooterBackgroundIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.Get(TemplateId)).Returns(FillEmailTemplateWithoutFooterBackground);
            _mockClubBusiness.Setup(x => x.GetClubLogoURL(ClubDomain, ClubLogo)).Returns(Convert.ToString(new byte[1024]));

            // Act
            var result = service.GetEmailTemplate(TemplateId);

            // Assert
            result.ClubDomain.Should().Be(ClubDomain);
            result.HeaderBackground.Should().Be(HeaderBackground);
            result.FooterBackground.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void GetEmailTemplate_HeaderBackgroundIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.Get(TemplateId)).Returns(FillEmailTemplateWithoutHeaderBackground);
            _mockClubBusiness.Setup(x => x.GetClubLogoURL(ClubDomain, ClubLogo)).Returns(Convert.ToString(new byte[1024]));

            // Act
            var result = service.GetEmailTemplate(TemplateId);

            // Assert
            result.ClubDomain.Should().Be(ClubDomain);
            result.HeaderBackground.Should().BeNullOrEmpty();
            result.FooterBackground.Should().Be(HeaderBackground);
        }

        [TestMethod]
        public void GetEmailTemplate_ClubLogoIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockEmailTemplateRepository.Setup(x => x.Get(TemplateId)).Returns(FillEmailTemplateWithoutClubLogo);

            // Act
            var result = service.GetEmailTemplate(TemplateId);

            // Assert
            result.ClubDomain.Should().Be(ClubDomain);
            result.ClubLogo.Should().BeNullOrEmpty();
            result.HeaderBackground.Should().Be(HeaderBackground);
            result.FooterBackground.Should().Be(HeaderBackground);
        }

        [TestMethod]
        public void SaveTemplate_TemplateClubDomainIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var emailTemplate = FillEmailTemplateWithoutClubDomain();

            _mockEmailTemplateRepository.Setup(x => x.Update(emailTemplate)).Returns(new OperationStatus { Status = true });
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClub);

            // Act
            var result = service.SaveTemplate(JsonHelper.JsonDeserialize<EmailTemplateModel>(emailTemplate.Template), ClubId, TemplateId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void SaveTemplate_HeaderBackgroundIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var emailTemplate = FillEmailTemplateWithoutHeaderBackground();

            _mockEmailTemplateRepository.Setup(x => x.Update(emailTemplate)).Returns(new OperationStatus { Status = true });
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClub);

            // Act
            var result = service.SaveTemplate(JsonHelper.JsonDeserialize<EmailTemplateModel>(emailTemplate.Template), ClubId, TemplateId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void SaveTemplate_FooterBackgroundIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var emailTemplate = FillEmailTemplateWithoutFooterBackground();

            _mockEmailTemplateRepository.Setup(x => x.Update(emailTemplate)).Returns(new OperationStatus { Status = true });
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClub);

            // Act
            var result = service.SaveTemplate(JsonHelper.JsonDeserialize<EmailTemplateModel>(emailTemplate.Template), ClubId, TemplateId);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void SaveTemplate_TemplateIdIsNull_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            var emailTemplate = FillEmailTemplateWithoutFooterBackground();

            _mockEmailTemplateRepository.Setup(x => x.Create(emailTemplate)).Returns(new OperationStatus { Status = true });
            _mockClubBusiness.Setup(x => x.Get(ClubId)).Returns(CreateClub);

            // Act
            var result = service.SaveTemplate(JsonHelper.JsonDeserialize<EmailTemplateModel>(emailTemplate.Template), ClubId, null);

            // Assert
            result.Status.Should().BeTrue();
        }
        #endregion

        #region Private methods
        private EmailTemplateBusiness CreateService()
        {
            _mockStorageBusiness = new Mock<IStorageBusiness>();
            _mockClubBusiness = new Mock<IClubBusiness>();
            _mockFileBusiness = new Mock<IFileBusiness>();
            _mockEmailTemplateRepository = new Mock<IRepository<EmailTemplate>>();
            //_mockMemoryStream = new Mock<MemoryStream>();

            return new EmailTemplateBusiness(_mockStorageBusiness.Object, _mockClubBusiness.Object, _mockFileBusiness.Object, _mockEmailTemplateRepository.Object);
        }

        private static IEnumerable<EmailTemplate> FillEmailTemplateList()
        {
            return new List<EmailTemplate>
            {
                new EmailTemplate { Id = TemplateId, Club_Id = ClubId, Template = string.Empty, TemplateName = TemplateName ,Type = TemplateType.Email} ,
                new EmailTemplate { Id = TemplateId+1, Club_Id = ClubId, Template = string.Empty, TemplateName = string.Empty ,Type = TemplateType.Email} ,
                new EmailTemplate { Id = TemplateId+2, Club_Id = ClubId, Template = string.Empty, TemplateName = string.Empty ,Type = TemplateType.Registration} ,
                new EmailTemplate { Id = TemplateId+3, Club_Id = 15, Template = string.Empty, TemplateName = "Default" ,Type = TemplateType.Email}
            };
        }

        private static IEnumerable<EmailTemplate> FillEmailTemplateListWithoutDefault()
        {
            return new List<EmailTemplate>
            {
                new EmailTemplate { Id = TemplateId, Club_Id = ClubId, Template = string.Empty, TemplateName = TemplateName ,Type = TemplateType.Email} ,
                new EmailTemplate { Id = TemplateId+1, Club_Id = ClubId, Template = string.Empty, TemplateName = string.Empty ,Type = TemplateType.Email} ,
                new EmailTemplate { Id = TemplateId+2, Club_Id = ClubId, Template = string.Empty, TemplateName = string.Empty ,Type = TemplateType.Registration}
            };
        }

        private static IEnumerable<EmailTemplate> FillEmailTemplateListWithMistakeClubId()
        {
            return new List<EmailTemplate>
            {
                new EmailTemplate { Id = TemplateId, Club_Id = 15, Template = string.Empty, TemplateName = "Default" ,Type = TemplateType.Email}
            };
        }

        private static IEnumerable<EmailTemplate> FillEmailTemplateListWithMistakeType()
        {
            return new List<EmailTemplate>
            {
                new EmailTemplate { Id = TemplateId+2, Club_Id = ClubId, Template = string.Empty, TemplateName = string.Empty ,Type = TemplateType.Registration}
            };
        }

        private static EmailTemplate FillEmailTemplate()
        {
            return new EmailTemplate
            {
                Id = TemplateId,
                Club_Id = ClubId,
                Template = "{\"Name\":null,\"NewName\":null,\"ChangeTemplate\":true,\"HasHeader\":true,\"HasLogo\":false,\"HasFooter\":false,\"HasMap\":false,\"IsTest\":false,\"HasHeaderText\":false,\"HasFooterText\":false,\"HeaderText\":null,\"HeaderBackground\":\"" + HeaderBackground + "\",\"BodyBackColor\":null,\"Body\":null,\"FooterText\":null,\"FooterBackground\":\"" + HeaderBackground + "\",\"ClubLogo\":\"" + ClubLogo + "\",\"MapZoom\":0,\"MapLocation\":null,\"Locations\":[],\"CongratulationMessage\":null,\"CustomMessage\":\"testdfhg\",\"IsRegisterationEmail\":false,\"HasMoreQuestions\":false,\"MoreQuestionsPhone\":null,\"MoreQuestionsEmail\":null,\"ClubDomain\":\"" + ClubDomain + "\",\"UseProgramLocation\":true}",
                TemplateName = string.Empty,
                Type = TemplateType.Email
            };
        }

        private static EmailTemplate FillEmailTemplateWithoutHeaderBackground()
        {
            return new EmailTemplate
            {
                Id = TemplateId,
                Club_Id = ClubId,
                Template = "{\"Name\":null,\"NewName\":null,\"ChangeTemplate\":true,\"HasHeader\":true,\"HasLogo\":false,\"HasFooter\":false,\"HasMap\":false,\"IsTest\":false,\"HasHeaderText\":false,\"HasFooterText\":false,\"HeaderText\":null,\"HeaderBackground\":null,\"BodyBackColor\":null,\"Body\":null,\"FooterText\":null,\"FooterBackground\":\"" + HeaderBackground + "\",\"ClubLogo\":\"" + ClubLogo + "\",\"MapZoom\":0,\"MapLocation\":null,\"Locations\":[],\"CongratulationMessage\":null,\"CustomMessage\":\"testdfhg\",\"IsRegisterationEmail\":false,\"HasMoreQuestions\":false,\"MoreQuestionsPhone\":null,\"MoreQuestionsEmail\":null,\"ClubDomain\":\"" + ClubDomain + "\",\"UseProgramLocation\":true}",
                TemplateName = string.Empty,
                Type = TemplateType.Email
            };
        }

        private static EmailTemplate FillEmailTemplateWithoutFooterBackground()
        {
            return new EmailTemplate
            {
                Id = TemplateId,
                Club_Id = ClubId,
                Template = "{\"Name\":null,\"NewName\":null,\"ChangeTemplate\":true,\"HasHeader\":true,\"HasLogo\":false,\"HasFooter\":false,\"HasMap\":false,\"IsTest\":false,\"HasHeaderText\":false,\"HasFooterText\":false,\"HeaderText\":null,\"HeaderBackground\":\"" + HeaderBackground + "\",\"BodyBackColor\":null,\"Body\":null,\"FooterText\":null,\"FooterBackground\":null,\"ClubLogo\":\"" + ClubLogo + "\",\"MapZoom\":0,\"MapLocation\":null,\"Locations\":[],\"CongratulationMessage\":null,\"CustomMessage\":\"testdfhg\",\"IsRegisterationEmail\":false,\"HasMoreQuestions\":false,\"MoreQuestionsPhone\":null,\"MoreQuestionsEmail\":null,\"ClubDomain\":\"" + ClubDomain + "\",\"UseProgramLocation\":true}",
                TemplateName = string.Empty,
                Type = TemplateType.Email
            };
        }

        private static EmailTemplate FillEmailTemplateWithoutClubLogo()
        {
            return new EmailTemplate
            {
                Id = TemplateId,
                Club_Id = ClubId,
                Template = "{\"Name\":null,\"NewName\":null,\"ChangeTemplate\":true,\"HasHeader\":true,\"HasLogo\":false,\"HasFooter\":false,\"HasMap\":false,\"IsTest\":false,\"HasHeaderText\":false,\"HasFooterText\":false,\"HeaderText\":null,\"HeaderBackground\":\"" + HeaderBackground + "\",\"BodyBackColor\":null,\"Body\":null,\"FooterText\":null,\"FooterBackground\":\"" + HeaderBackground + "\",\"ClubLogo\":null,\"MapZoom\":0,\"MapLocation\":null,\"Locations\":[],\"CongratulationMessage\":null,\"CustomMessage\":\"testdfhg\",\"IsRegisterationEmail\":false,\"HasMoreQuestions\":false,\"MoreQuestionsPhone\":null,\"MoreQuestionsEmail\":null,\"ClubDomain\":\"" + ClubDomain + "\",\"UseProgramLocation\":true}",
                TemplateName = string.Empty,
                Type = TemplateType.Email
            };
        }

        private static EmailTemplate FillEmailTemplateWithoutClubDomain()
        {
            return new EmailTemplate
            {
                Id = TemplateId,
                Club_Id = ClubId,
                Template = "{\"Name\":null,\"NewName\":null,\"ChangeTemplate\":true,\"HasHeader\":true,\"HasLogo\":false,\"HasFooter\":false,\"HasMap\":false,\"IsTest\":false,\"HasHeaderText\":false,\"HasFooterText\":false,\"HeaderText\":null,\"HeaderBackground\":\"" + HeaderBackground + "\",\"BodyBackColor\":null,\"Body\":null,\"FooterText\":null,\"FooterBackground\":\"" + HeaderBackground + "\",\"ClubLogo\":null,\"MapZoom\":0,\"MapLocation\":null,\"Locations\":[],\"CongratulationMessage\":null,\"CustomMessage\":\"testdfhg\",\"IsRegisterationEmail\":false,\"HasMoreQuestions\":false,\"MoreQuestionsPhone\":null,\"MoreQuestionsEmail\":null,\"ClubDomain\":null,\"UseProgramLocation\":true}",
                TemplateName = string.Empty,
                Type = TemplateType.Email
            };
        }

        private static Club CreateClub()
        {
            return new Club
            {
                Domain = "testDomain"
            };
        }

        #endregion 
    }
}
