﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models.Invoice
{
    public class InvoiceOrderItemViewModel : BaseViewModel<OrderItem, long>
    {
        public int UserId { get; set; }
        public string Attendee { get; set; }
        public string UserName { get; set; }
        public decimal EntryFee { get; set; }
        public string EntryFeeName { get; set; }
        public decimal Balance { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public PaymentPlanType PaymentplanType { get; set; }
        public long PaymentDetailId { get; set; }
        public long OrderId { get; set; }

        public string ConfirmationId { get; set; }
        public long ProgramScheduleId { get; set; }
        public string ProgramName { get; internal set; }
        public bool IsCancel { get; set; }
    }
}