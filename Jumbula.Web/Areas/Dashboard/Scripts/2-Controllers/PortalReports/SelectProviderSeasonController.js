﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SelectProviderSeasonController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'select-county-view';
            window.app.globalObjects.initializeView();

            $scope.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            $scope.MC.stateParams = $stateParams;

            $scope.reportName = "";

            if ($scope.stateParams.reportType) {
                switch ($scope.stateParams.reportType) {
                    case "InstructorAssignmentsPortal":
                        {
                            $scope.reportName = "Instructor Assignments";
                            break;
                        }
                    case "InstructorCheckinPortal":
                        {
                            $scope.reportName = "Instructor Check-in";
                            break;
                        }
                    
                }
            }

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.AllProviders = [];
            $scope.AllSeasons = [];
            $scope.Model = {};
            var timer = false;

            $scope.$watch('AllProviders', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $scope.$watch('AllSeasons', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get('Dashboard/PortalReport/GetAllProvider').success(function (data, status, headers, config) {
                $scope.AllProviders = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.fillSeasonData = function (providerId) {
                $http.get('Dashboard/PortalReport/GetClubSeasons', { params: { clubId: providerId } }).success(function (data, status, headers, config) {
                    $scope.AllSeasons = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }

            var checkValidation = function (providerId, seasonId) {
                if (is.undefined(providerId)) {
                    jbToast.error("Please select a provider.");
                    return false;
                }
                else if (is.undefined(seasonId)) {
                    jbToast.error("Please select a season.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {
                if (checkValidation($scope.Model.ProviderId, $scope.Model.SeasonId)) {                   
                    customReportService.saveSchoolSeasonId($scope.Model.ProviderId, $scope.Model.SeasonId);
                    switch ($scope.stateParams.reportType) {
                        case "InstructorAssignmentsPortal":
                            {
                                $state.go('InstructorAssignmentsPortalReports', { seasonId: $scope.Model.SeasonId });
                                break;
                            }
                        case "InstructorCheckinPortal":
                            {
                                $state.go('InstructorCheckinPortalReports', { seasonId: $scope.Model.SeasonId });
                                break;
                            }
                        
                    }
                }
            }
        }
    ]);

})();