﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AddInfoDistrictViewModel
    {
        public AddInfoDistrictViewModel()
        {
            OrganizationTimeZone = DropdownHelpers.SeedTimeZones();
            SelectedTimeZone = "-1";
        }
        [Display(Name = "Domain")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 3)]
        [RegularExpression("[A-Za-z0-9-]+",
             ErrorMessage = "{0} cannot contain any spaces or special characters such as *, &, !, _, etc")]
        public string Domain { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }
        [Display(Name = "Address")]
        [Required(ErrorMessage = "{0} is required.")]
        public string Address { get; set; }
        [EmailAddress(ErrorMessage = "Email is not a valid email address.")]
        [Display(Name = "Email")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string SuperintendentEmail { get; set; }
        public string SuperintendentLastName { get; set; }
        public string SuperintendentName { get; set; }
        [Display(Name = "Phone number")]
        [RegularExpression("[0-9]+$",ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string SuperintendentPhone { get; set; }
        public List<int> SelectedAreaManager { get; set; }
        public int? SelectedSalesContact { get; set; }
        public List<int> SelectedRegionalDirector { get; set; }
        public string RevenueShare { get; set; }
        public int? NcesId { get; set; }
        public int? SelectedStaffingRatio { get; set; }
        public List<SelectKeyValue<int>> AllValueforStaffingRatio { get; set; }
        public List<SelectKeyValue<int>> AreaManagerStaffs { get; set; }
        public List<SelectKeyValue<int>> RegionalDirectoStaffs { get; set; }
        public List<SelectKeyValue<int>> SalesContactStaffs { get; set; }
        public List<SelectKeyValue<int>> AllSubsidies { get; set; }
        public List<int> SelectedSchools { get; set; }
        public List<SelectKeyValue<int>> AllSchools { get; set; }
        public int DistrictId { get; set; }
        public List<int> SelectedSubsidies { get; set; }
        public List<int> ListSelectedSubsidiesForSave { get; set; }
        public string SelectedTimeZone { get; set; }
        public List<SelectKeyValue<string>> OrganizationTimeZone { get; set; }
        public List<int> DeletedSubsidiesId { get; set; }
        public List<SchoolDistrictGridViewModel> ListSelectedSchool { get; set; }

    }
   
}