﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum DismissalOptions
    {
        [Description("After care")]
        AfterCare,
        [Description("Bus")]
        Bus,
        [Description("Pick-up")]
        PickUp,
        [Description("Walk")]
        Walk
    }
}
