﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class GoogleMapsViewModel
    {
        // your google maps container id ( div or any tag id )
        public string ContainerId { get; set; }

        public string ContainerClass { get; set; } // your css class for google maps container

        public string Name { get; set; }
        
        public PostalAddress PostalAddress { get; set; }

        public string Room { get; set; }

        public bool HideGoogleMapInClassPage { get; set; }
    }
}