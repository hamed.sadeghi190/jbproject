using System.Collections.Generic;
using Jb.Framework.Common.Forms.Validation;

namespace Jb.Framework.Common.Forms
{
    public interface IJbBaseElement
    {
        string ElementId { get; set; }
        string Name { get; set; }
        string Title { get; set; }
        string TypeTitle { get; set; }
        string HelpText { get; set; }
        ElementSize Size { get; set; }
        ElementWidth Width { get; set; }
        AccessMode CurrentMode { get; set; }
        VisibilityMode VisibleMode { get; set; }
        Dictionary<string, AccessMode> AccessRole { get; set; }
        List<BaseElementValidator> Validations { get; set; }
        bool IsConditional { get; set; }
        string RelatedElementId { get; set; }
        string RelatedElementValue { get; set; }
        object GetValue();
        void SetValue(object value);       
        bool Equals(IJbBaseElement jbElement);
    }
}