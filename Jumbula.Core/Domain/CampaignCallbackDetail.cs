﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class CampaignCallbackDetail : BaseEntity
    {
        public DateTime CreatedDate { get; set; }

        public string Email { get; set; }

        public EmailStatus EmailStatus { get; set; }

        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign { get; set; }

        public int CampaignId { get; set; }
    }
}
