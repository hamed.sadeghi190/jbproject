﻿using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Models
{

    public class ShoppingCartViewModel
    {
        public int Count { get; set; }
        public decimal Total { get; set; }

        public CurrencyCodes Currency { get; set; }
    }

    public class DisplayCartViewModel
    {

        public Cart Cart { get; set; }
        public bool BackToDetail { get; set; }
        public List<SelectListItem> CreditCards { get; set; }
        public InvoiceStatus InvoiceStatus { get; set; }
        public IQueryable<UserCreditCard> AllCreditCards { get; set; }
        public StripePaymentModels PaymentModel { get; set; }
        public int? InvoiceId { get; set; }
        public int CreditId { get; set; }
        public int ActiveCreditCardId { get; set; }
        public decimal? AvailableCredit { get; set; }

        public OrderMode OrderMode
        {
            get
            {
                if (Cart.Count > 0)
                {
                    return Cart.Order.OrderMode;
                }

                return OrderMode.Online;
            }
        }
        public bool OfflinePaymentNow { get; set; }
        public string PlaidEnvironment
        {
            get
            {
                return IsTestMode || (ClientPaymentMethods != null && ClientPaymentMethods.IsCreditCardTestMode) ? "tartan" : "production";
            }
        }
        public bool IsTestMode
        {
            get
            {
                return !Cart.Order.IsLive;
            }
        }
        public string ClientName { get; set; }
        public string PlaidKey
        {
            get
            {
                return WebConfigHelper.Plaid_PublicKey;
            }
        }
        public string TotalWithoutConvenienceFee
        {
            get
            {
                decimal convfee = 0;

                // check if convenience fee hase value show real amount on button
                foreach (var item in Cart.Order.RegularOrderItems)
                {

                    if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience))
                    {
                        convfee += item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).Sum(c => c.Amount);
                    }
                }

                return convfee != 0 ? CurrencyHelper.FormatCurrencyWithPenny(Cart.PayableAmount - convfee, Cart.Order.Club.Currency) : string.Empty;
            }
        }
        public string TotalOrderAmountWithoutConvenienceFee
        {
            get
            {
                decimal convfee = 0;

                // check if convenience fee hase value show real amount on button
                foreach (var item in Cart.Order.RegularOrderItems)
                {

                    if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience))
                    {
                        convfee += item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).Sum(c => c.Amount);
                    }
                }

                return convfee != 0 ? CurrencyHelper.FormatCurrencyWithPenny(Cart.Total - convfee, Cart.Order.Club.Currency) : string.Empty;
            }
        }

        public decimal TotalConvFee
        {
            get
            {
                decimal totalConvFee = 0;
                foreach (var item in Cart.Order.RegularOrderItems)
                {
                    foreach (var charge in item.GetOrderChargeDiscounts().Where(cd => (int)cd.Category > 0 && cd.Category != ChargeDiscountCategory.EntryFee))
                    {
                        var convienenceFee = charge.Category == ChargeDiscountCategory.Convenience ? charge.Amount : 0;
                        totalConvFee += convienenceFee;
                    }
                }
                return totalConvFee != 0 ? totalConvFee : 0;
            }

        }
        public string CouponMessage { get; set; }
        public ClientPaymentMethod ClientPaymentMethods { get; set; }

        public string DonationDescription
        {
            get;
            set;
        }
        public string DonationMessage
        {
            get;
            set;
        }
        public bool EnableDonation
        {
            get; set;
        }

        public bool HideCoupon
        {
            get; set;
        }

        public List<long> HiddenPricePrograms
        {
            get; set;
        }

        public bool HideAllProgramsPrice
        {
            get; set;
        }

        public bool EnablePayByCash
        {
            get;
            set;
        }
        public bool EnableACHPayment
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.EnableAchPayment : false;
            }
        }
        public bool EnableBankTransfer
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.EnableBankTransferPayment : false;
            }
        }
        public string StripePaymentCheckouLabel
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.CreditCardCheckoutLabel : string.Empty;
            }
        }
        public string StripePreferredPaymentMethod
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.CreditCardPreferredLabel : string.Empty;
            }
        }
        public string PaypalPaymentCheckouLabel
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.PaypalCheckoutLabel : string.Empty;
            }
        }
        public string AcHPaymentCheckouLabel
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.AchPaymentCheckoutLabel : string.Empty;
            }
        }
        public string AcHPaymentMessage
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.AchPaymentMessage : string.Empty;
            }
        }
        public string CashPaymentCheckouLabel
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.CashCheckoutLabel : string.Empty;
            }
        }
        public string BankPaymentCheckouLabel
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.BankTransferCheckoutLabel : string.Empty;
            }
        }
        public bool EnablePaypalPayment
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.EnablePaypalPayment : false;
            }
        }
        public bool EnableCreditCardPayment
        {
            get
            {

                return ClientPaymentMethods != null ? (ClientPaymentMethods.EnableStripePayment || ClientPaymentMethods.EnableAuthorizePayment) ? true : false : false;
            }
        }
        public bool EnableManualPayment
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.EnableManualPayment : false;
            }
        }

        public string ManualPaymentName
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.ManualPaymentName : string.Empty;
            }
        }

        public string ManualPaymentMessage
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.ManualPaymentMessage : string.Empty;
            }
        }
        public PaymentMethod ManualPaymentMethod
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.ManualPaymentMethod : PaymentMethod.Cash;
            }
        }
        public string PaybyCashMessage
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.CashPaymentMessage : string.Empty;
            }
        }
        public string BankTransferMessage
        {
            get
            {
                return ClientPaymentMethods != null ? ClientPaymentMethods.BankTransferPaymentMessage : string.Empty;
            }
        }
        public string ClubDomain
        {
            get
            {
                if (this.Cart != null && this.Cart.Order.Club != null)
                {
                    return this.Cart.Order.Club.Domain;
                }

                return string.Empty;
            }
        }

        public CartPreregistrationMode PreregistrationMode { get; set; }

        public CartLotteryMode LotteryMode { get; set; }

        public DisplayCartViewModel()
        {
            Cart = new Cart();
            CreditCards = new List<SelectListItem>();
            PaymentModel = new Models.StripePaymentModels();

        }

        public int? UserId { get; set; }

        public bool ShowPaymentError { get; set; }

        public string WebSiteUrl { get; set; }
        public bool HideProgramTime { get; set; }
        public bool ShowCombinedPriceOnlyForClub { get; set; }
        public string WebSiteText { get; set; }
        public bool IsAutoCharge { get; set; }
        public string RegisterSameProgramText { get; set; }
        public DateTime PreRegisterTime { get; set; }

        public DateTime? GeneralOpenDate { get; set; }

        public DateTime? LotteryCloseDate { get; set; }

        public bool IsNotValid { get; set; }
        public bool EnableExpressPaymentInOfflineMode { get; set; }
    }

    public class ConfirmationViewModel
    {
        public StripePaymentModels DirectPayment { get; set; }
        public CurrencyCodes Currency { get; set; }

        public decimal OrderAmount { get; set; }

        public string ClubDomain { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public Cart Cart { get; set; }

        public OrderInstallment Installment { get; set; }
        public List<OrderInstallment> OrderInstallments { get; set; }
        public string ClubName { get; set; }

        public int CreditId { get; set; }
        public string ConfirmationId { get; set; }

        public string ClubLogoUrl { get; set; }

        public bool? IsPrepaid { get; set; }

        public decimal PayableAmount { get; set; }

        public bool HasInstallment { get; set; }
        public int? UserId { get; set; }
        public string PaymentMessage { get; set; }
        public string PaymentInstruction { get; set; }

        public ConfirmationViewModel(Cart cart)
        {
            Cart = cart;

        }
        public ConfirmationViewModel(Cart cart, string clubDomain, string confirmationId, bool isPrepaid, decimal payableAmount, PaymentMethod paymentMethod, int? userId)
        {
            ClubDomain = clubDomain;
            Cart = cart;
            PaymentMethod = paymentMethod;
            ConfirmationId = confirmationId;
            IsPrepaid = isPrepaid;
            UserId = userId;
            var club = Ioc.ClubBusiness.Get(ClubDomain);
            ClubName = club.Name;
            ClubLogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
            PayableAmount = payableAmount;
            HasInstallment = (cart.Order.PaymentPlanType == PaymentPlanType.Installment);
        }
        public ConfirmationViewModel(int clubId, string confirmationId, decimal payableAmount)
        {

            ConfirmationId = confirmationId;
            var club = Ioc.ClubBusiness.Get(ClubDomain);
            ClubName = club.Name;
            ClubLogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
            ClubDomain = club.Domain;
            PayableAmount = payableAmount;
            Cart = null;
            HasInstallment = true;
        }

    }


    public class PayViewModel
    {
        public Cart Cart { get; set; }
        public OrderMode OrderMode
        {
            get
            {
                if (Cart.Count > 0)
                {
                    return Cart.Order.OrderMode;
                }

                return OrderMode.Online;
            }
        }
        public PaymentMethod PaymentMethod { get; set; }
        public bool OfflinePaymentNow { get; set; }
        public string PaymentMessage { get; set; }
        public bool BackToDetail { get; set; }
        public string ReturnUrl { get; set; }
        public string PaymentInstruction { get; set; }
        public string ConfirmationId { get; set; }

        public List<string> ConfirmatiocIds { get; set; }
        public string StrConfirmatiocIds
        {
            get
            {
                if (ConfirmatiocIds != null && ConfirmatiocIds.Any())
                {
                    return string.Join(", ", ConfirmatiocIds);
                }
                return string.Empty;
            }
        }
        public int? UserId { get; set; }

        public bool IsPrepaid { get; set; }

        public int CountInstallment { get; set; }
        public int? InvoiceId { get; set; }
        public string PublishableKey
        {
            get
            {
                return IsTestMode ? WebConfigHelper.Stripe_PublishableKey_Test : WebConfigHelper.Stripe_PublishableKey_Live;

            }
        }

        public bool IsTestMode { get; set; }

        public int clubId { get; set; }
        public long OrderId { get; set; }
        public string Token { get; set; }

        public List<SelectListItem> Types { get; set; }
        public string CreateTokenResponse { get; set; }

        [DisplayName("Cardholder name")]
        [Required(ErrorMessage = "The cardholder name is required")]
        public string CardHolderName { get; set; }

        [Display(Name = "Card number")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter a valid card number.")]
        [StringLength(17, ErrorMessage = "{0} is too long.")]
        public string CardNumber { get; set; }

        public virtual PostalAddress Address { get; set; }

        [Display(Name = "CVC")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter a valid CVC number.")]
        [StringLength(5, ErrorMessage = "{0} is too long.")]
        public string CVV { get; set; }

        [Display(Name = "year")]
        public string Year { get; set; }

        [Display(Name = "year")]
        public int Year2Char { get; set; }

        [Display(Name = "month")]
        [Required(ErrorMessage = "Expiration date is required")]
        public string Month { get; set; }

        public bool IsDefault { get; set; }

        [StringLength(128, ErrorMessage = "{0} is too long.")]
        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Street")]
        public string Street { get; set; }

        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "City")]
        public string City { get; set; }

        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Zip code")]
        public string ZipCode { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "State")]
        public string State { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Country")]
        public Countries Country { get; set; }

        public CurrencyCodes Currency { get; set; }
        public decimal PayableAmount { get; set; }
        public SelectList AccountType
        {
            get
            {
                return new SelectList(DropdownHelpers.ToSelectList<AchPaymentAccountType>(), dataValueField: "Value", dataTextField: "Text", selectedValue: "");
            }
        }
        public string SelectedAccountType { get; set; }

        public JumbulaSubSystem ActionCaller { get; set; }
        public bool IsFamilyTakePayment { get; set; }
        public int CreditId { get; set; }
    }

    public enum CartPreregistrationMode : byte
    {
        None,
        Initiated,
        Confirmed
    }

    public enum CartLotteryMode : byte
    {
        None,
        Initiated,
        Confirmed
    }
}