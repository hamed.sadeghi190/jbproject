﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
   public class ClubSubsidy : BaseEntity
    {
        public int ClubId { get; set; }
        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }
        public int SubsidyId { get; set; }
        [ForeignKey("SubsidyId")]
        public virtual Subsidy Subsidy { get; set; }

    }
}
