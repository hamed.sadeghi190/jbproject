﻿using Jumbula.Common.Enums;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class CampaignViewModelStep1 : BaseViewModel<Campaign>
    {

        public CampaignViewModelStep1()
        {
            SomePeople=new DynamicListModel();
        }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Campaign Name")]
        public string CampaignName { get; set; }

        public PageMode PageMode { get; set; }

        public CampaignReciepientsMode Mode { get; set; }

        public int? ListId { get; set; }

        public DynamicListModel SomePeople { get; set; }

        public string ListName { get; set; }

    }
}