﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IPaypalIPNBusiness
    {
        OperationStatus Create(PaypalIPN paypalIPN);
        IQueryable<PaypalIPN> GetValidUnProceedIPNs();

        OperationStatus Update(PaypalIPN paypalIpn);

        bool CheckTransactionExist(string transactionId);
    }
}
