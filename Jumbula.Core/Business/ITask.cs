﻿namespace Jumbula.Core.Business
{
    public partial interface ITask
    {
        /// <summary>
        /// Executes a task
        /// </summary>
        void Execute();
    }
}
