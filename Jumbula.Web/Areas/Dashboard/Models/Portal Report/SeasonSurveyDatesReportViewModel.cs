﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SeasonSurveyDatesReportViewModel
    {
        public string Season { get; set; }

        public string SurveyOpen { get; set; }

        public string SurveyClose { get; set; }

        public string SurveyResultsDue { get; set; }
    }
}