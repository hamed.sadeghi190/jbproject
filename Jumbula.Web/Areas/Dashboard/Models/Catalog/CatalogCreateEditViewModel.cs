﻿using Jumbula.Common.Enums;
using Jumbula.Core.Model.Catalog;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CatalogCreateEditViewModel : BaseViewModel<CatalogItem>
    {

        public CatalogCreateEditViewModel()
        {
            AttendeeRestriction = new AttendeeRestriction();
            Images = new List<string>();
        }

        [Display(Name = "Item name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }

        [Display(Name = "Categories")]
        public List<int> Categories { get; set; }
        public List<string> CategoriesNames { get; set; }
        public string StrCategoriesNames { get; set; }

        [Display(Name = "Minimum enrollment")]
        [Range(1, int.MaxValue, ErrorMessage = "{0} is required.")]
        public int? MinimumEnrollment { get; set; }

        [Display(Name = "Maximum enrollment")]
        [Range(1, int.MaxValue, ErrorMessage = "{0} is required.")]
        public int? MaximumEnrollment { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        public string Description { get; set; }
        public string HtmlDescription { get; set; }

        [Display(Name = " Additional Comments")]
        [StringLength(1024, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Detail { get; set; }

        public string MaterialsNeeded { get; set; }

        public string Image { get; set; }
        public string SourceImage { get; set; }

        public List<string> Images { get; set; }
        public List<string> SourceImages { get; set; }
        public string DurationTitle { get; set; }

        public AttendeeRestriction AttendeeRestriction { get; set; }

        public string MinGrade { get; set; }
        public string MaxGrade { get; set; }
        public string Gender { get; set; }
        public bool HasPartner { get; set; }


        public List<CatalogPriceGroupViewModel> PriceGroups { get; set; }

        public List<CatalogItemPriceViewModel> PriceOption { get; set; }

        public List<dynamic> PriceOptionForView { get; set; }

        public List<RecommendedGradeGroupingViewModel> RecomendedGradeGroups { get; set; }

        public string StrRecomendedGradeGroups { get; set; }

        public byte Rating { get; set; }

        public List<SpaceRequirementsViewModel> SpaceRequirements { get; set; }

        public string StrSpaceRequirementsIndoor { get; set; }
      
        public string StrSpaceRequirementsOutdoor { get; set; }
        public string SpaceRequirementDetails { get; set; }

        public int? StudentRatio { get; set; }

        public List<SelectKeyValue<int>> AllRatios { get; set; }

        public List<SelectKeyValue<string>> Genders { get; set; }

        public List<SelectKeyValue<string>> Grades { get; set; }

        public List<SelectKeyValue<int>> Ages { get; set; }

        public List<SelectKeyValue<string>> RestrictionTypes { get; set; }

        public List<SelectKeyValue<int>> AllCategories { get; set; }

        public List<SelectKeyValue<int>> AllCapacities { get; set; }

        public PageMode PageMode { get; set; }
        public bool ShowGenderBox { get; set; }
        public string PriceOptionLable { get; set; }
        public string priceOptionNote { get; set; }
    }
}