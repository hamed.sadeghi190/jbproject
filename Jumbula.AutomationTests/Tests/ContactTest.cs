﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Jumbula.AutomationTests.Tests
{
    class ContactTest : BaseTest<ContactElements>
    {
        public ContactTest(IWebDriver webDriver)
            :base(webDriver)
        {

        }

        public void ViewContacts()
        {
            // Row1
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 1, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Active staff", Elements.Grid_GetCellByIndex("contactGrid", 1, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(0, 194, 124);", Elements.Grid_GetCellByIndex("contactGrid", 1, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Admin staff, School", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.AreEqual("School-partner2", Elements.Grid_GetCellByTitle("contactGrid", 1, "Organization").Text);
            Assert.AreEqual("adminstaff@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 1, "Email").Text);
            Assert.AreEqual("452-657-3467", Elements.Grid_GetCellByTitle("contactGrid", 1, "Phone").Text);
            Assert.AreEqual("Administrative assistant / Admin", Elements.Grid_GetCellByTitle("contactGrid", 1, "Title / Role").Text);
            // Row2
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 2, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Active staff", Elements.Grid_GetCellByIndex("contactGrid", 2, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(0, 194, 124);", Elements.Grid_GetCellByIndex("contactGrid", 2, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Admin staff, Provider", Elements.Grid_GetCellByTitle("contactGrid", 2, "Name").Text);
            Assert.AreEqual("Provider-partner2", Elements.Grid_GetCellByTitle("contactGrid", 2, "Organization").Text);
            Assert.AreEqual("adminstaff@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 2, "Email").Text);
            Assert.AreEqual("Owner / Admin", Elements.Grid_GetCellByTitle("contactGrid", 2, "Title / Role").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 2, "Phone").Text);
            // Row3
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 3, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Contact", Elements.Grid_GetCellByIndex("contactGrid", 3, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(171, 171, 171);", Elements.Grid_GetCellByIndex("contactGrid", 3, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Partner, Contact", Elements.Grid_GetCellByTitle("contactGrid", 3, "Name").Text);
            Assert.AreEqual("Partner2 for automation test", Elements.Grid_GetCellByTitle("contactGrid", 3, "Organization").Text);
            Assert.AreEqual("partnerContact@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 3, "Email").Text);
            Assert.AreEqual("234-657-2345 Ext: 567", Elements.Grid_GetCellByTitle("contactGrid", 3, "Phone").Text);
            Assert.AreEqual("Manager", Elements.Grid_GetCellByTitle("contactGrid", 3, "Title / Role").Text);
            // Row4
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 4, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Primary contact", Elements.Grid_GetCellByIndex("contactGrid", 4, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(0, 0, 0);", Elements.Grid_GetCellByIndex("contactGrid", 4, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Partner2, Primary Contact", Elements.Grid_GetCellByTitle("contactGrid", 4, "Name").Text);
            Assert.AreEqual("Partner2 for automation test", Elements.Grid_GetCellByTitle("contactGrid", 4, "Organization").Text);
            Assert.AreEqual("primarycontact@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 4, "Email").Text);
            Assert.AreEqual("234-657-8764", Elements.Grid_GetCellByTitle("contactGrid", 4, "Phone").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 4, "Title / Role").Text);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            // Row5
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 5, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Contact", Elements.Grid_GetCellByIndex("contactGrid", 5, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(171, 171, 171);", Elements.Grid_GetCellByIndex("contactGrid", 5, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Partner2, Contact2", Elements.Grid_GetCellByTitle("contactGrid", 5, "Name").Text);
            Assert.AreEqual("Partner2 for automation test", Elements.Grid_GetCellByTitle("contactGrid", 5, "Organization").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 5, "Email").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 5, "Phone").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 5, "Title / Role").Text);
            // Row6
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 6, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Primary contact", Elements.Grid_GetCellByIndex("contactGrid", 6, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(0, 0, 0);", Elements.Grid_GetCellByIndex("contactGrid", 6, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Partner2, School", Elements.Grid_GetCellByTitle("contactGrid", 6, "Name").Text);
            Assert.AreEqual("School-partner2", Elements.Grid_GetCellByTitle("contactGrid", 6, "Organization").Text);
            Assert.AreEqual("schoolpartner2@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 6, "Email").Text);
            Assert.AreEqual("234-786-3468 Ext: 123", Elements.Grid_GetCellByTitle("contactGrid", 6, "Phone").Text);
            Assert.AreEqual("Principal", Elements.Grid_GetCellByTitle("contactGrid", 6, "Title / Role").Text);
            // Row7
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 7, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Primary contact", Elements.Grid_GetCellByIndex("contactGrid", 7, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(0, 0, 0);", Elements.Grid_GetCellByIndex("contactGrid", 7, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Partner2, Provider", Elements.Grid_GetCellByTitle("contactGrid", 7, "Name").Text);
            Assert.AreEqual("Provider-partner2", Elements.Grid_GetCellByTitle("contactGrid", 7, "Organization").Text);
            Assert.AreEqual("providerpartner2@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 7, "Email").Text);
            Assert.AreEqual("986-356-8752 Ext: 45", Elements.Grid_GetCellByTitle("contactGrid", 7, "Phone").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 7, "Title / Role").Text);
            // Row8
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 8, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Primary contact", Elements.Grid_GetCellByIndex("contactGrid", 8, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(0, 0, 0);", Elements.Grid_GetCellByIndex("contactGrid", 8, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Partner2, District", Elements.Grid_GetCellByTitle("contactGrid", 8, "Name").Text);
            Assert.AreEqual("School district-partner2", Elements.Grid_GetCellByTitle("contactGrid", 8, "Organization").Text);
            Assert.AreEqual("districtSuperintendent@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 8, "Email").Text);
            Assert.AreEqual("234-657-6895", Elements.Grid_GetCellByTitle("contactGrid", 8, "Phone").Text);
            Assert.AreEqual("Superintendent", Elements.Grid_GetCellByTitle("contactGrid", 8, "Title / Role").Text);
            // Row9
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 9, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Pending staff", Elements.Grid_GetCellByIndex("contactGrid", 9, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(251, 204, 24);", Elements.Grid_GetCellByIndex("contactGrid", 9, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Staff, Pending", Elements.Grid_GetCellByTitle("contactGrid", 9, "Name").Text);
            Assert.AreEqual("Partner2 for automation test", Elements.Grid_GetCellByTitle("contactGrid", 9, "Organization").Text);
            Assert.AreEqual("PendingStaff@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 9, "Email").Text);
            Assert.AreEqual("Admin", Elements.Grid_GetCellByTitle("contactGrid", 9, "Title / Role").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 9, "Phone").Text);
            // Row10
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 10, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Active staff", Elements.Grid_GetCellByIndex("contactGrid", 10, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(0, 194, 124);", Elements.Grid_GetCellByIndex("contactGrid", 10, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Staff, Active", Elements.Grid_GetCellByTitle("contactGrid", 10, "Name").Text);
            Assert.AreEqual("Partner2 for automation test", Elements.Grid_GetCellByTitle("contactGrid", 10, "Organization").Text);
            Assert.AreEqual("ActiveStaff@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 10, "Email").Text);
            Assert.AreEqual("Principal / Admin", Elements.Grid_GetCellByTitle("contactGrid", 10, "Title / Role").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 10, "Phone").Text);
            // Row11
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 11, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Frozen staff", Elements.Grid_GetCellByIndex("contactGrid", 11, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(208, 46, 32);", Elements.Grid_GetCellByIndex("contactGrid", 11, 1, "i").GetAttribute("style"));
            Assert.AreEqual("Staff, Frozen", Elements.Grid_GetCellByTitle("contactGrid", 11, "Name").Text);
            Assert.AreEqual("Partner2 for automation test", Elements.Grid_GetCellByTitle("contactGrid", 11, "Organization").Text);
            Assert.AreEqual("FrozenStaff@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 11, "Email").Text);
            Assert.AreEqual("Admin", Elements.Grid_GetCellByTitle("contactGrid", 11, "Title / Role").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 11, "Phone").Text);

        }
        public void FilterContacts()
        {
            // Name
            Elements.FilterTerm.SendKeys("Min");
            Elements.filterButton.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Admin staff, School", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.AreEqual("Admin staff, Provider", Elements.Grid_GetCellByTitle("contactGrid", 2, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid",3));
            Elements.FilterTerm.Clear();
            Elements.FilterTerm.SendKeys("Choo");
            Elements.filterButton.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Admin staff, School", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.AreEqual("Partner2, School", Elements.Grid_GetCellByTitle("contactGrid", 2, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 3));
            // Email
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterType.Click();
            Elements.FilterEmailOption.Click();
            Elements.FilterTerm.SendKeys("Staff");
            Elements.filterButton.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Admin staff, School", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.AreEqual("Admin staff, Provider", Elements.Grid_GetCellByTitle("contactGrid", 2, "Name").Text);
            Assert.AreEqual("Staff, Pending", Elements.Grid_GetCellByTitle("contactGrid", 3, "Name").Text);
            Assert.AreEqual("Staff, Active", Elements.Grid_GetCellByTitle("contactGrid", 4, "Name").Text);
            Assert.AreEqual("Staff, Frozen", Elements.Grid_GetCellByTitle("contactGrid", 5, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 6));
            // Phone
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterType.Click();
            Elements.FilterPhoneOption.Click();
            Elements.FilterTerm.SendKeys("657");
            Elements.filterButton.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Admin staff, School", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.AreEqual("Partner, Contact", Elements.Grid_GetCellByTitle("contactGrid", 2, "Name").Text);
            Assert.AreEqual("Partner2, Primary Contact", Elements.Grid_GetCellByTitle("contactGrid", 3, "Name").Text);
            Assert.AreEqual("Partner2, District", Elements.Grid_GetCellByTitle("contactGrid", 4, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 5));
            // Title
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterTitle.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterTitle_GetOptionByTitle("Manager").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Partner, Contact", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 2));
            // Organization
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterOrganization.Click();
            ExtraAPI.Wait(2000);
            // Check organization list
            Assert.AreEqual("All", Elements.FilterOrganization_GetOptionByIndex(1).Text);
            Assert.AreEqual("Partner", Elements.FilterOrganization_GetOptionByIndex(2).Text);
            Assert.AreEqual("Partner2 for automation test", Elements.FilterOrganization_GetOptionByIndex(3).Text);
            Assert.AreEqual("Provider", Elements.FilterOrganization_GetOptionByIndex(4).Text);
            Assert.AreEqual("Provider-partner2", Elements.FilterOrganization_GetOptionByIndex(5).Text);
            Assert.AreEqual("School district", Elements.FilterOrganization_GetOptionByIndex(6).Text);
            Assert.AreEqual("School district-partner2", Elements.FilterOrganization_GetOptionByIndex(7).Text);
            Assert.AreEqual("School", Elements.FilterOrganization_GetOptionByIndex(8).Text);
            Assert.AreEqual("School-partner2", Elements.FilterOrganization_GetOptionByIndex(9).Text);
            Elements.FilterOrganization_GetOptionByTitle("Provider-partner2").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Admin staff, Provider", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.AreEqual("Partner2, Provider", Elements.Grid_GetCellByTitle("contactGrid", 2, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 3));
            // Name and Title
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterTerm.SendKeys("Min");
            Elements.FilterTitle.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterTitle_GetOptionByTitle("Owner").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Admin staff, Provider", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 2));
            // Name and organization
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterTerm.SendKeys("Min");
            Elements.FilterOrganization.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterOrganization_GetOptionByTitle("Provider-partner2").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Admin staff, Provider", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 2));
            // Email and Title
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterType.Click();
            Elements.FilterEmailOption.Click();
            Elements.FilterTerm.SendKeys("Staff");
            Elements.FilterTitle.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterTitle_GetOptionByTitle("Administrative assistant").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Admin staff, School", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 2));
            // Email and Organization
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterType.Click();
            Elements.FilterEmailOption.Click();
            Elements.FilterTerm.SendKeys("Staff");
            Elements.FilterOrganization.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterOrganization_GetOptionByTitle("Partner2 for automation test").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Staff, Pending", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.AreEqual("Staff, Active", Elements.Grid_GetCellByTitle("contactGrid", 2, "Name").Text);
            Assert.AreEqual("Staff, Frozen", Elements.Grid_GetCellByTitle("contactGrid", 3, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 4));
            // Phone and Title
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterType.Click();
            Elements.FilterPhoneOption.Click();
            Elements.FilterTerm.SendKeys("657");
            Elements.FilterTitle.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterTitle_GetOptionByTitle("Superintendent").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Partner2, District", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 2));
            // Phone and Organization
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterType.Click();
            Elements.FilterPhoneOption.Click();
            Elements.FilterTerm.SendKeys("657");
            Elements.FilterOrganization.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterOrganization_GetOptionByTitle("School district-partner2").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Partner2, District", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 2));
            // Name and Title and Organization
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterTerm.SendKeys("staff");
            Elements.FilterOrganization.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterOrganization_GetOptionByTitle("Partner2 for automation test").Click();
            ExtraAPI.Wait(5000);
            Elements.FilterTitle.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterTitle_GetOptionByTitle("Principal").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Staff, Active", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 2));
            // Email and Title and Organization
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterType.Click();
            Elements.FilterEmailOption.Click();
            Elements.FilterTerm.SendKeys("Partner");
            Elements.FilterOrganization.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterOrganization_GetOptionByTitle("Provider-partner2").Click();
            ExtraAPI.Wait(5000);
            Elements.FilterTitle.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterTitle_GetOptionByTitle("Director").Click();
            ExtraAPI.Wait(5000);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 1));
            // Phone and Title and Organization
            Elements.clearFilters.Click();
            ExtraAPI.Wait(5000);
            Elements.FilterType.Click();
            Elements.FilterPhoneOption.Click();
            Elements.FilterTerm.SendKeys("657");
            Elements.FilterOrganization.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterOrganization_GetOptionByTitle("Partner2 for automation test").Click();
            ExtraAPI.Wait(5000);
            Elements.FilterTitle.Click();
            ExtraAPI.Wait(2000);
            Elements.FilterTitle_GetOptionByTitle("Manager").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Partner, Contact", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.IsFalse(Elements.Grid_RowExist("contactGrid", 2));
            // Clear filter
            Elements.clearFilters.Click();
            ExtraAPI.Wait(10000);
            Assert.AreEqual("true", Elements.FilterNameOption.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.FilterTitle_GetOptionByTitle("All").GetAttribute("selected"));
            Assert.AreEqual("All", Elements.FilterSelectedOrganization.Text);
            Assert.AreEqual("Admin staff, School", Elements.Grid_GetCellByTitle("contactGrid", 1, "Name").Text);
            Assert.AreEqual("Admin staff, Provider", Elements.Grid_GetCellByTitle("contactGrid", 2, "Name").Text);
            Assert.AreEqual("Partner, Contact", Elements.Grid_GetCellByTitle("contactGrid", 3, "Name").Text);
            Assert.AreEqual("Partner2 for automation test", Elements.Grid_GetCellByTitle("contactGrid", 3, "Organization").Text);
            Assert.AreEqual("Partner2, Primary Contact", Elements.Grid_GetCellByTitle("contactGrid", 4, "Name").Text);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Assert.AreEqual("Partner2, Contact2", Elements.Grid_GetCellByTitle("contactGrid", 5, "Name").Text);
            Assert.AreEqual("Partner2, School", Elements.Grid_GetCellByTitle("contactGrid", 6, "Name").Text);
            Assert.AreEqual("Partner2, Provider", Elements.Grid_GetCellByTitle("contactGrid", 7, "Name").Text);
            Assert.AreEqual("Partner2, District", Elements.Grid_GetCellByTitle("contactGrid", 8, "Name").Text);
            Assert.AreEqual("Staff, Pending", Elements.Grid_GetCellByTitle("contactGrid", 9, "Name").Text);
            Assert.AreEqual("Staff, Active", Elements.Grid_GetCellByTitle("contactGrid", 10, "Name").Text);
            Assert.AreEqual("Staff, Frozen", Elements.Grid_GetCellByTitle("contactGrid", 11, "Name").Text);
        }
        public void AddContact()
        {
            Elements.AddContactLink.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Create Contact", Elements.PageTitle.Text);
            // Step1
            // Validation
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("First name is required.", Elements.FirstName.GetAttribute("data-original-title"));
            Assert.AreEqual("Last name is required.", Elements.LastName.GetAttribute("data-original-title"));
            // Enter valid data
            Elements.FirstName.SendKeys("New");
            Elements.LastName.SendKeys("y Contact");
            Elements.Email.SendKeys("newcontact@jumbula.com");
            Elements.Phone.SendKeys("1234567890");
            Elements.PhoneExtension.SendKeys("123");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            // Step2
            //Check data from step1
            Assert.AreEqual("New", Elements.FirstName.GetAttribute("value"));
            Assert.AreEqual("y Contact", Elements.LastName.GetAttribute("value"));
            Assert.AreEqual("newcontact@jumbula.com", Elements.Email.GetAttribute("value"));
            Assert.AreEqual("1234567890", Elements.Phone.GetAttribute("value"));
            Assert.AreEqual("123", Elements.PhoneExtension.GetAttribute("value"));
            // Validation
            Elements.FirstName.Clear();
            Elements.LastName.Clear();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("First name is required.", Elements.FirstName.GetAttribute("data-original-title"));
            Assert.AreEqual("Last name is required.", Elements.LastName.GetAttribute("data-original-title"));
            
            // Eneter valid data
            Elements.FirstName.SendKeys("New");
            Elements.LastName.SendKeys("y Contact");
            Assert.AreEqual("Partner2 for automation test", Elements.SelectedOrganization.Text);
            Elements.OrganizationList.Click();
            ExtraAPI.Wait(2000);
            // Check organization list
            Assert.AreEqual("Partner", Elements.Organization_GetOptionByIndex(1).Text);
            Assert.AreEqual("Partner2 for automation test", Elements.Organization_GetOptionByIndex(2).Text);
            Assert.AreEqual("Provider", Elements.Organization_GetOptionByIndex(3).Text);
            Assert.AreEqual("Provider-partner2", Elements.Organization_GetOptionByIndex(4).Text);
            Assert.AreEqual("School district", Elements.Organization_GetOptionByIndex(5).Text);
            Assert.AreEqual("School district-partner2", Elements.Organization_GetOptionByIndex(6).Text);
            Assert.AreEqual("School", Elements.Organization_GetOptionByIndex(7).Text);
            Assert.AreEqual("School-partner2", Elements.Organization_GetOptionByIndex(8).Text);
            Elements.Organization_GetOptionByTitle("Provider-partner2").Click();
            Elements.TitleList.Click();
            ExtraAPI.Wait(2000);
            Elements.GetTitle("Manager").Click();
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Contact edited successfully.", Elements.MessageboxBody.Text);
            // Check the new contact in the grid
            Assert.AreEqual("jbi-user-male staff-contact", Elements.Grid_GetCellByIndex("contactGrid", 5, 1, "i").GetAttribute("class"));
            Assert.AreEqual("Contact", Elements.Grid_GetCellByIndex("contactGrid", 5, 1, "i").GetAttribute("title"));
            Assert.AreEqual("color: rgb(171, 171, 171);", Elements.Grid_GetCellByIndex("contactGrid", 5, 1, "i").GetAttribute("style"));
            Assert.AreEqual("y Contact, New", Elements.Grid_GetCellByTitle("contactGrid", 12, "Name").Text);
            Assert.AreEqual("Provider-partner2", Elements.Grid_GetCellByTitle("contactGrid", 12, "Organization").Text);
            Assert.AreEqual("newcontact@jumbula.com", Elements.Grid_GetCellByTitle("contactGrid", 12, "Email").Text);
            Assert.AreEqual("123-456-7890 Ext: 123", Elements.Grid_GetCellByTitle("contactGrid", 12, "Phone").Text);
            Assert.AreEqual("Manager", Elements.Grid_GetCellByTitle("contactGrid", 12, "Title / Role").Text);
        }
        public void EditContact()
        {
            // Edit Staff
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 1, "Edit").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("true", Elements.OrganizationListSelect.GetAttribute("disabled"));
            Assert.AreEqual("true", Elements.Email.GetAttribute("disabled"));
            Elements.Phone.Clear();
            Elements.PhoneExtension.Clear();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("",Elements.Grid_GetCellByTitle("contactGrid", 1, "Phone").Text);

            // Edit primary contact
            actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 4));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 4, "Edit").Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("true", Elements.OrganizationListSelect.GetAttribute("disabled"));
            Assert.AreEqual(null, Elements.Email.GetAttribute("disabled"));
            Elements.Email.Clear();
            Elements.Phone.Clear();
            Elements.PhoneExtension.Clear();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Email address is required for primary contacts.", Elements.Email.GetAttribute("data-original-title"));
            Assert.AreEqual("Phone number is required for primary contacts.", Elements.Phone.GetAttribute("data-original-title"));

            // Edit contact
            BladeTest bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContacts();
            actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 3));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 3, "Edit").Click();
            ExtraAPI.Wait(5000);
            // Check the old data
            Assert.AreEqual("Contact", Elements.FirstName.GetAttribute("value"));
            Assert.AreEqual("Partner", Elements.LastName.GetAttribute("value"));
            Assert.AreEqual("Partner2 for automation test", Elements.SelectedOrganization.Text);
            Assert.AreEqual("partnerContact@jumbula.com", Elements.Email.GetAttribute("value"));
            Assert.AreEqual("2346572345", Elements.Phone.GetAttribute("value"));
            Assert.AreEqual("567", Elements.PhoneExtension.GetAttribute("value"));
            Assert.AreEqual("true", Elements.GetTitle("Manager").GetAttribute("selected"));
            Assert.AreEqual(null, Elements.OrganizationListSelect.GetAttribute("disabled"));
            Assert.AreEqual(null, Elements.Email.GetAttribute("disabled"));
            // Change data
            Elements.FirstName.Clear();
            Elements.FirstName.SendKeys("Contact changed");
            Elements.LastName.Clear();
            Elements.LastName.SendKeys("Partner changed");
            Elements.OrganizationList.Click();
            ExtraAPI.Wait(2000);
            Elements.Organization_GetOptionByTitle("Provider").Click();
            Elements.TitleList.Click();
            ExtraAPI.Wait(2000);
            Elements.GetTitle("Director").Click();
            Elements.Phone.Clear();
            Elements.PhoneExtension.Clear();
            Elements.Email.Clear();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Contact edited successfully.", Elements.MessageboxBody.Text);
            // Check the new data in the grid
            Assert.AreEqual("Partner changed, Contact changed", Elements.Grid_GetCellByTitle("contactGrid", 3, "Name").Text);
            Assert.AreEqual("Provider-partner2", Elements.Grid_GetCellByTitle("contactGrid", 3, "Organization").Text);
            Assert.AreEqual("Provider - Director", Elements.Grid_GetCellByTitle("contactGrid", 3, "Title / Role").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 3, "Email").Text);
            Assert.AreEqual("", Elements.Grid_GetCellByTitle("contactGrid", 3, "Phone").Text);
        }
        public void DeleteContact()
        {
            // Delete Staff
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("true",Elements.Grid_Action_Parent("contactGrid", 1 ,"Delete").GetAttribute("ng-hide"));
            // Delete primary contact
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 4));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("true", Elements.Grid_Action_Parent("contactGrid", 4, "Delete").GetAttribute("ng-hide"));
            // Delete contact
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 5));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 5, "Delete").Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.WarningExists);
            Elements.CancelBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Partner2, Contact2", Elements.Grid_GetCellByTitle("contactGrid", 5, "Name").Text);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 5));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 5, "Delete").Click();
            ExtraAPI.Wait(5000);
            Assert.IsTrue(Elements.WarningExists);
            Elements.YesBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Contact deleted successfully.", Elements.MessageboxBody.Text);
            Assert.AreEqual("Partner2, School", Elements.Grid_GetCellByTitle("contactGrid", 5, "Name").Text);
            
        }
        public void CheckSetting()
        {
            CheckDefaultSetting();
            CheckDisableFields();
            DeslecetAllFields();
            BladeTest bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();
            SelecetAllFields();
            bladeTest.GoToContactSetting();
            DeslecetAllFields();
            CheckEmail();
            CheckPhone();
            CheckCellPhone();
            CheckWorkPhone();
            CheckTitle();
            CheckNickname();
            CheckDoB();
        }
        private void CheckDefaultSetting()
        {
            // First name
            Assert.AreEqual("true", Elements.FirstNameEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.FirstNameShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.LastNameEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.LastNameShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.OrganizationEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.OrganizationShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.EmailEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.EmailShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.PhoneEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.PhoneShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.TitleEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.TitleShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual(null, Elements.CellPhoneEnabledInput.GetAttribute("selected"));
            Assert.AreEqual(null, Elements.CellPhoneShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual(null, Elements.WorkPhoneEnabledInput.GetAttribute("selected"));
            Assert.AreEqual(null, Elements.WorkPhoneShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual(null, Elements.NicknameEnabledInput.GetAttribute("selected"));
            Assert.AreEqual(null, Elements.NicknameShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual(null, Elements.DoBEnabledInput.GetAttribute("selected"));
            Assert.AreEqual(null, Elements.DoBShowInGridInput.GetAttribute("selected"));
        }
        private void CheckDisableFields()
        {
            // First name - Show in grid
            Assert.AreEqual("true", Elements.FirstNameShowInGridInput.GetAttribute("disabled"));
            Elements.FirstNameShowInGridSpan.Click();
            Assert.AreEqual("true", Elements.FirstNameShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.FirstNameShowInGridInput.GetAttribute("disabled"));

            // First name - Enabled
            Assert.AreEqual("true", Elements.FirstNameEnabledInput.GetAttribute("disabled"));
            Elements.FirstNameEnabledSpan.Click();
            Assert.AreEqual("true", Elements.FirstNameEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.FirstNameEnabledInput.GetAttribute("disabled"));

            // Last name - Show in grid
            Assert.AreEqual("true", Elements.LastNameShowInGridInput.GetAttribute("disabled"));
            Elements.LastNameShowInGridSpan.Click();
            Assert.AreEqual("true", Elements.LastNameShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.LastNameShowInGridInput.GetAttribute("disabled"));

            // Last name - Enabled
            Assert.AreEqual("true", Elements.LastNameEnabledInput.GetAttribute("disabled"));
            Elements.LastNameEnabledSpan.Click();
            Assert.AreEqual("true", Elements.LastNameEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.LastNameEnabledInput.GetAttribute("disabled"));

            // Organization - Show in grid
            Assert.AreEqual("true", Elements.OrganizationShowInGridInput.GetAttribute("disabled"));
            Elements.OrganizationShowInGridSpan.Click();
            Assert.AreEqual("true", Elements.OrganizationShowInGridInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.OrganizationShowInGridInput.GetAttribute("disabled"));

            // Organization name - Enabled
            Assert.AreEqual("true", Elements.OrganizationEnabledInput.GetAttribute("disabled"));
            Elements.OrganizationEnabledSpan.Click();
            Assert.AreEqual("true", Elements.OrganizationEnabledInput.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.OrganizationEnabledInput.GetAttribute("disabled"));
                        
        }
        private void DeslecetAllFields()
        {
            // Email
            if(Elements.EmailShowInGridInput.GetAttribute("selected")=="true")
            {
                Elements.EmailShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.EmailEnabledInput.GetAttribute("selected") == "true")
            {
                Elements.EmailEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual("true", Elements.EmailShowInGridInput.GetAttribute("disabled"));
            // Phone
            if (Elements.PhoneShowInGridInput.GetAttribute("selected") == "true")
            {
                Elements.PhoneShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.PhoneEnabledInput.GetAttribute("selected") == "true")
            {
                Elements.PhoneEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual("true", Elements.PhoneShowInGridInput.GetAttribute("disabled"));
            // Cell phone
            if (Elements.CellPhoneShowInGridInput.GetAttribute("selected") == "true")
            {
                Elements.CellPhoneShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.CellPhoneEnabledInput.GetAttribute("selected") == "true")
            {
                Elements.CellPhoneEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual("true", Elements.CellPhoneShowInGridInput.GetAttribute("disabled"));
            // Work phone
            if (Elements.WorkPhoneShowInGridInput.GetAttribute("selected") == "true")
            {
                Elements.WorkPhoneShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.WorkPhoneEnabledInput.GetAttribute("selected") == "true")
            {
                Elements.WorkPhoneEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual("true", Elements.WorkPhoneShowInGridInput.GetAttribute("disabled"));
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            // Title
            if (Elements.TitleShowInGridInput.GetAttribute("selected") == "true")
            {
                Elements.TitleShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.TitleEnabledInput.GetAttribute("selected") == "true")
            {
                Elements.TitleEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual("true", Elements.TitleShowInGridInput.GetAttribute("disabled"));
            // Nickname
            if (Elements.NicknameShowInGridInput.GetAttribute("selected") == "true")
            {
                Elements.NicknameShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.NicknameEnabledInput.GetAttribute("selected") == "true")
            {
                Elements.NicknameEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual("true", Elements.NicknameShowInGridInput.GetAttribute("disabled"));
            // DoB
            if (Elements.DoBShowInGridInput.GetAttribute("selected") == "true")
            {
                Elements.DoBShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.DoBEnabledInput.GetAttribute("selected") == "true")
            {
                Elements.DoBEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual("true", Elements.DoBShowInGridInput.GetAttribute("disabled"));
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Contact settings saved successfully.", Elements.MessageboxBody.Text);
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContacts();
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
            
        }
        private void SelecetAllFields()
        {
            // Email
            var a = Elements.EmailEnabledInput.GetAttribute("selected");
            if (Elements.EmailEnabledInput.GetAttribute("selected") == null)
            {
                Elements.EmailEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.EmailShowInGridInput.GetAttribute("selected") == null)
            {
                Elements.EmailShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual(null, Elements.EmailShowInGridInput.GetAttribute("disabled"));
            // Phone
            if (Elements.PhoneEnabledInput.GetAttribute("selected") == null)
            {
                Elements.PhoneEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.PhoneShowInGridInput.GetAttribute("selected") == null)
            {
                Elements.PhoneShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual(null, Elements.PhoneShowInGridInput.GetAttribute("disabled"));
            // Cell phone
            if (Elements.CellPhoneEnabledInput.GetAttribute("selected") == null)
            {
                Elements.CellPhoneEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.CellPhoneShowInGridInput.GetAttribute("selected") == null)
            {
                Elements.CellPhoneShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual(null, Elements.CellPhoneShowInGridInput.GetAttribute("disabled"));
            // Work phone
            if (Elements.WorkPhoneEnabledInput.GetAttribute("selected") == null)
            {
                Elements.WorkPhoneEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.WorkPhoneShowInGridInput.GetAttribute("selected") == null)
            {
                Elements.WorkPhoneShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual(null, Elements.WorkPhoneShowInGridInput.GetAttribute("disabled"));
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            // Title
            if (Elements.TitleEnabledInput.GetAttribute("selected") == null)
            {
                Elements.TitleEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.TitleShowInGridInput.GetAttribute("selected") == null)
            {
                Elements.TitleShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual(null, Elements.TitleShowInGridInput.GetAttribute("disabled"));
            // Nickname
            if (Elements.NicknameEnabledInput.GetAttribute("selected") == null)
            {
                Elements.NicknameEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.NicknameShowInGridInput.GetAttribute("selected") == null)
            {
                Elements.NicknameShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual(null, Elements.NicknameShowInGridInput.GetAttribute("disabled"));
            // DoB
            if (Elements.DoBEnabledInput.GetAttribute("selected") == null)
            {
                Elements.DoBEnabledSpan.Click();
                ExtraAPI.Wait(1000);
            }
            if (Elements.DoBShowInGridInput.GetAttribute("selected") == null)
            {
                Elements.DoBShowInGridSpan.Click();
                ExtraAPI.Wait(1000);
            }
            Assert.AreEqual(null, Elements.DoBShowInGridInput.GetAttribute("disabled"));
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Contact settings saved successfully.", Elements.MessageboxBody.Text);
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContacts();
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.DoBHeader.GetAttribute("style"));

        }
        private void CheckEmail()
        {
            // Enable email
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();
            DeslecetAllFields();
            bladeTest.GoToContactSetting();
            Elements.EmailEnabledSpan.Click();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
            // Check add
            Elements.AddContactLink.Click();
            ExtraAPI.Wait(10000);
            Elements.FirstName.SendKeys("yyy");
            Elements.LastName.SendKeys("yyy");
            Assert.IsTrue(Elements.EmailExist);
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.IsTrue(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Check Edit
            bladeTest.GoToContacts();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 1, "Edit").Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.FirstNameExist);
            Assert.IsTrue(Elements.LastNameExist);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.IsTrue(Elements.EmailExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Select show in grid for email
            bladeTest.GoToContactSetting();
            Elements.EmailShowInGridSpan.Click();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
        }
        public void CheckPhone()
        {
            // Enable phone
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();
            DeslecetAllFields();
            bladeTest.GoToContactSetting();
            Elements.PhoneEnabledSpan.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
            // Check add
            Elements.AddContactLink.Click();
            ExtraAPI.Wait(10000);
            Elements.FirstName.SendKeys("yyy");
            Elements.LastName.SendKeys("yyy");
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsTrue(Elements.PhoneExist);
            Assert.IsTrue(Elements.PhoneExtensionExist);
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsTrue(Elements.PhoneExist);
            Assert.IsTrue(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Check Edit
            bladeTest.GoToContacts();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 1, "Edit").Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.FirstNameExist);
            Assert.IsTrue(Elements.LastNameExist);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.False(Elements.EmailExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsTrue(Elements.PhoneExist);
            Assert.IsTrue(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Select show in grid for email
            bladeTest.GoToContactSetting();
            Elements.PhoneShowInGridSpan.Click();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
        }
        public void CheckCellPhone()
        {
            // Enable phone
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();
            DeslecetAllFields();
            bladeTest.GoToContactSetting();
            Elements.CellPhoneEnabledSpan.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
            // Check add
            Elements.AddContactLink.Click();
            ExtraAPI.Wait(10000);
            Elements.FirstName.SendKeys("yyy");
            Elements.LastName.SendKeys("yyy");
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsTrue(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Check Edit
            bladeTest.GoToContacts();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 1, "Edit").Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.FirstNameExist);
            Assert.IsTrue(Elements.LastNameExist);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.False(Elements.EmailExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsTrue(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Select show in grid for email
            bladeTest.GoToContactSetting();
            Elements.CellPhoneShowInGridSpan.Click();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if(Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
        }
        public void CheckWorkPhone()
        {
            // Enable phone
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();
            DeslecetAllFields();
            bladeTest.GoToContactSetting();
            Elements.WorkPhoneEnabledSpan.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
            // Check add
            Elements.AddContactLink.Click();
            ExtraAPI.Wait(10000);
            Elements.FirstName.SendKeys("yyy");
            Elements.LastName.SendKeys("yyy");
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsTrue(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Check Edit
            bladeTest.GoToContacts();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 1, "Edit").Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.FirstNameExist);
            Assert.IsTrue(Elements.LastNameExist);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.False(Elements.EmailExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsTrue(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Select show in grid for email
            bladeTest.GoToContactSetting();
            Elements.WorkPhoneShowInGridSpan.Click();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
        }
        public void CheckTitle()
        {
            // Enable phone
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();
            DeslecetAllFields();
            bladeTest.GoToContactSetting();
            Elements.TitleEnabledSpan.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
            // Check add
            Elements.AddContactLink.Click();
            ExtraAPI.Wait(10000);
            Elements.FirstName.SendKeys("yyy");
            Elements.LastName.SendKeys("yyy");
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsTrue(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Check Edit
            bladeTest.GoToContacts();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 1, "Edit").Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.FirstNameExist);
            Assert.IsTrue(Elements.LastNameExist);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.False(Elements.EmailExist);
            Assert.IsTrue(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Select show in grid for email
            bladeTest.GoToContactSetting();
            Elements.TitleShowInGridSpan.Click();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
        }
        public void CheckNickname()
        {
            // Enable phone
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();
            DeslecetAllFields();
            bladeTest.GoToContactSetting();
            Elements.NicknameEnabledSpan.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
            // Check add
            Elements.AddContactLink.Click();
            ExtraAPI.Wait(10000);
            Elements.FirstName.SendKeys("yyy");
            Elements.LastName.SendKeys("yyy");
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsTrue(Elements.NicknameExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Check Edit
            bladeTest.GoToContacts();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 1, "Edit").Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.FirstNameExist);
            Assert.IsTrue(Elements.LastNameExist);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.False(Elements.EmailExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsTrue(Elements.NicknameExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsFalse(Elements.DoBExist);
            // Select show in grid for email
            bladeTest.GoToContactSetting();
            Elements.NicknameShowInGridSpan.Click();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
        }
        public void CheckDoB()
        {
            // Enable phone
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToContactSetting();
            DeslecetAllFields();
            bladeTest.GoToContactSetting();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.DoBEnabledSpan.Click();
            ExtraAPI.Wait(1000);
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.DoBHeader.GetAttribute("style"));
            // Check add
            Elements.AddContactLink.Click();
            ExtraAPI.Wait(10000);
            Elements.FirstName.SendKeys("yyy");
            Elements.LastName.SendKeys("yyy");
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.IsFalse(Elements.EmailExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsTrue(Elements.DoBExist);
            // Check Edit
            bladeTest.GoToContacts();
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.Grid_Actions("contactGrid", 1));
            actions.Perform();
            ExtraAPI.Wait(1000);
            Elements.Grid_Action_Child("contactGrid", 1, "Edit").Click();
            ExtraAPI.Wait(10000);
            Assert.IsTrue(Elements.FirstNameExist);
            Assert.IsTrue(Elements.LastNameExist);
            Assert.IsTrue(Elements.OrganizationExist);
            Assert.False(Elements.EmailExist);
            Assert.IsFalse(Elements.TitleExist);
            Assert.IsFalse(Elements.NicknameExist);
            Assert.IsFalse(Elements.PhoneExist);
            Assert.IsFalse(Elements.PhoneExtensionExist);
            Assert.IsFalse(Elements.CellPhoneExist);
            Assert.IsFalse(Elements.WorkPhoneExist);
            Assert.IsTrue(Elements.DoBExist);
            // Select show in grid for email
            bladeTest.GoToContactSetting();
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.DoBShowInGridSpan.Click();
            Elements.SubmitBtn.Click();
            ExtraAPI.Wait(10000);
            if (Elements.MessageboxExist)
            {
                Elements.MessageboxBody.Click();
            }
            
            bladeTest.GoToContacts();
            // Check grid
            Assert.AreEqual("", Elements.NameHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.OrganizationHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.PhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.CellPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.WorkPhoneHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.TitleHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.NicknameHeader.GetAttribute("style"));
            Assert.AreEqual("display: none;", Elements.EmailHeader.GetAttribute("style"));
            Assert.AreEqual("", Elements.DoBHeader.GetAttribute("style"));
        }
    }
}
