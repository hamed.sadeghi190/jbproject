﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DailyReportReportPdfExportViewModel
    {
        public string ClubName { get; set; }
        public string Date { get; set; }
        public List<DailyDismissalReportViewModel> Data { get; set; }
    }
}