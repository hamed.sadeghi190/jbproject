﻿using Jb.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IPaymentPlanBusiness
    {
        IQueryable<PaymentPlan> GetList(long seasonId);
        List<PaymentPlan> GetPaymentPlansByProgramId(long programId, long seasonId);
        PaymentPlan Get(long PaymentPlanId);
        OperationStatus Create(PaymentPlan paymentPlan);
        OperationStatus Update(PaymentPlan paymentPlan);
        OperationStatus Delete(long paymentPlanId);

        IEnumerable<PaymentPlan> GetPaymentPlansByProgramIdForUser(long programId, long seasonId, int userId);

        OperationStatus CopyInSeason(long paymentPlanId, long seasonId, int clubId, string newSeasonDomain, bool forwardDates, int? monthNum);
    }
}
