﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Model.Report;
using Jumbula.Web.Mvc.Controllers;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class ReportController : DashboardBaseController
    {
        private readonly INewReportBusiness _reportBusiness;

        public ReportController(INewReportBusiness reportBusiness)
        {
            _reportBusiness = reportBusiness;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult BindData(ReportName reportName, List<SelectListItem> parameters, PaginationModel paginationModel, List<ReportGridSort> sort = null)
        {
            if (parameters == null)
            {
                return JsonNet(new ReportDataModel(null, null));
            }

            var dictionary = new Dictionary<string, object>();
            foreach (var item in parameters)
            {
                dictionary.Add(item.Text, item.Value);
            }

            var data = _reportBusiness.BindData(reportName, ActiveClub, dictionary, paginationModel, sort);

            return JsonNet(data);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetSchema(ReportName reportName, Dictionary<string, object> parameters)
        {
            var schema = _reportBusiness.GetSchema(reportName, ActiveClub, parameters);

            return JsonNet(schema);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetFilterData(Dictionary<string, object> parameters)
        {
            var filter = _reportBusiness.GetFilter(ActiveClub, parameters);

            return JsonNet(filter);
        }

        [HttpPost]
        [AllowAnonymous]
        public FileResult GetPdf(ReportName reportName, Dictionary<string, object> parameters, PaginationModel paginationModel)
        {
            var pdfModel = _reportBusiness.GetPdfModel(reportName, ActiveClub, parameters, paginationModel);

            var pdfGenerator = new PdfGenerator();

            var file = pdfGenerator.GenerateFromView(ControllerContext, pdfModel.PdfViewName, pdfModel);

            return File(file, FileTypes.PdfMimeType, $"{reportName}.{FileTypes.PdfExtension}");
        }

        [HttpPost]
        [AllowAnonymous]
        public FileResult GetExcel(ReportName reportName, Dictionary<string, object> parameters, PaginationModel paginationModel)
        {
            var file = _reportBusiness.GetExcel(reportName, ActiveClub, parameters, paginationModel, ControllerContext);

            return File(file, FileTypes.ExcelMimeType, reportName.ToString());
        }

    }
}