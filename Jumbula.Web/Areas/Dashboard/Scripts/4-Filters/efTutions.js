﻿(function () {
    'use strict';

    angular.module('dashboardApp').filter('efTutions', [ function () {
        return function (val) {
            var filtered = [];
            angular.forEach(val, function (value, key) {
                if (value.Category == 5) {
                    filtered.push(value);
                }
            })
            return filtered;
        };
    }]);


    angular.module('dashboardApp').filter('extraCharges', [function () {
        return function (val) {
            var filtered = [];
            angular.forEach(val, function(value, key) {
                if (value.Category != 5) {
                    filtered.push(value);
                }
            });
            return filtered;
        };
    }]);

})();