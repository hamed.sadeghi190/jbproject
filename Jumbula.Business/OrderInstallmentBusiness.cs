﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Core.Model.AutoCharge;
using Jumbula.Core.Model.Order;
using static Jumbula.Common.Constants.Payment;
using Jumbula.Common.Utilities;
using System.Globalization;
using Jumbula.Core.Model.Generic;
using Serilog;

namespace Jumbula.Business
{
    public class OrderInstallmentBusiness : IOrderInstallmentBusiness
    {
        private readonly IProgramBusiness _programBusiness;
        private readonly IUserProfileBusiness _userProfileBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly ISubscriptionBusiness _subscriptionBusiness;
        private readonly IPaymentPlanBusiness _paymentPlanBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IRepository<OrderInstallment, long> _orderInstallmentRepository;
        private readonly IEntitiesContext _dataContext;
        private readonly IChargeBusiness _chargeBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderHistoryBusiness _orderHistoryBusiness;
        private readonly IPaymentBusiness _paymentBusiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;

        public OrderInstallmentBusiness(IProgramBusiness programBusiness,
        IUserProfileBusiness userProfileBusiness,
        ISeasonBusiness seasonBusiness,
        ISubscriptionBusiness subscriptionBusiness,
        IPaymentPlanBusiness paymentPlanBusiness,
        IOrderItemBusiness orderItemBusiness,
        IRepository<OrderInstallment, long> orderInstallmentRepository,
        IEntitiesContext dataContext, IChargeBusiness chargeBusiness,
        IClubBusiness clubBusiness, IOrderHistoryBusiness orderHistoryBusiness, IPaymentBusiness paymentBusiness, ITransactionActivityBusiness transactionActivityBusiness)
        {
            _programBusiness = programBusiness;
            _userProfileBusiness = userProfileBusiness;
            _seasonBusiness = seasonBusiness;
            _subscriptionBusiness = subscriptionBusiness;
            _paymentPlanBusiness = paymentPlanBusiness;
            _orderItemBusiness = orderItemBusiness;
            _orderInstallmentRepository = orderInstallmentRepository;
            _dataContext = dataContext;
            _chargeBusiness = chargeBusiness;
            _clubBusiness = clubBusiness;
            _orderHistoryBusiness = orderHistoryBusiness;
            _paymentBusiness = paymentBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
        }

        public OrderInstallment Get(long installmentId)
        {
            return _orderInstallmentRepository.Get(i => i.Id == installmentId && !i.IsDeleted);
        }

        public OrderInstallment GetForAutoCharge(long installmentId)
        {
            return _orderInstallmentRepository.GetList()
                .Include(o => o.OrderItem)
                .Include(o => o.OrderItem.Order)
                .Include(o => o.OrderItem.Order.Club)
                .Include(o => o.OrderItem.Order.Club.PartnerClub)
                .Include(o => o.OrderItem.Order.User)
                .Include(o => o.OrderItem.Player)
                .Include(o => o.OrderItem.Player.Contact)
                .Include(o => o.OrderItem.ProgramSchedule)
                .Include(o => o.OrderItem.ProgramSchedule.Program)
                .Single(i => i.Id == installmentId && !i.IsDeleted);
        }
        public IQueryable<OrderInstallment> GetListByToken(string token)
        {
            return _orderInstallmentRepository.GetList().Where(i => i.Token == token && i.OrderItem.ItemStatus == OrderItemStatusCategories.completed);
        }
        public IQueryable<OrderInstallment> GetListByOrderItemId(long orderItemId)
        {
            return _orderInstallmentRepository.GetList().Where(i => i.OrderItemId == orderItemId);
        }

        public IQueryable<OrderInstallment> GetAllAutoChargeInstallments(DateTime installmentDate)
        {
            return _orderInstallmentRepository.GetList().Where(c => c.InstallmentDate <= installmentDate
            && (c.Status == OrderStatusCategories.initiated || ((c.Amount - (c.PaidAmount.HasValue ? c.PaidAmount.Value : 0) > 0) && c.Status == OrderStatusCategories.completed))
            && c.EnableReminder == false
            && (c.OrderItem.ItemStatus == OrderItemStatusCategories.completed)
            && c.OrderItem.Order.IsAutoCharge
            && c.OrderItem.Order.IsLive
            && c.OrderItem.PaymentPlanStatus == PaymentPlanStatus.Started
            && !c.IsDeleted);
        }

        // this function created for new auto charge system . we have to keep GetAllAutoChargeInstallments function for old system
        public Dictionary<int, IEnumerable<long>> GetAllInstallmentsByDueDate(DateTime dueDate)
        {
            var installments = _orderInstallmentRepository.GetList()
                .Where(c => (c.InstallmentDate <= dueDate && c.InstallmentDate > AutoChargeStartPoint)
                            && (c.Status == OrderStatusCategories.initiated || c.Status == OrderStatusCategories.completed)
                            && (c.Amount > 0 && (!c.PaidAmount.HasValue || (c.PaidAmount.HasValue && c.PaidAmount >= 0)))
                            && (c.Amount - (c.PaidAmount ?? 0) > 0)
                            && !c.IsDeleted
                            && !c.EnableReminder // EnableReminder show this installment is manual or not . it's can called IsManual
                            && c.OrderItem.ItemStatus == OrderItemStatusCategories.completed
                            && c.OrderItem.PaymentPlanStatus == PaymentPlanStatus.Started
                            && c.OrderItem.Order.IsAutoCharge
                            && c.OrderItem.Order.IsLive)
                .Select(i => new ClubInstallmentPair
                {
                    InstallmentId = i.Id,
                    ClubId = i.OrderItem.Order.ClubId

                }).ToList();

            return installments.GroupBy(i => i.ClubId).ToDictionary(o => o.Key, b => b.Select(i => i.InstallmentId));
        }


        // this function created for new auto charge system . we have to keep GetAllAutoChargeInstallments function for old system
        public List<ClubInstallmentPair> GetAllClubInstallmentPairByDueDate(DateTime dueDate)
        {
            var installments = _orderInstallmentRepository.GetList()
                .Where(c => (c.InstallmentDate <= dueDate && c.InstallmentDate > AutoChargeStartPoint)
                            && (c.Status == OrderStatusCategories.initiated || c.Status == OrderStatusCategories.completed)
                            && (c.Amount > 0 && (!c.PaidAmount.HasValue || (c.PaidAmount.HasValue && c.PaidAmount >= 0)))
                            && (c.Amount - (c.PaidAmount ?? 0) > 0)
                            && !c.IsDeleted
                            && !c.EnableReminder // EnableReminder show this installment is manual or not . it's can called IsManual
                            && c.OrderItem.ItemStatus == OrderItemStatusCategories.completed
                            && c.OrderItem.PaymentPlanStatus == PaymentPlanStatus.Started
                            && c.OrderItem.Order.IsAutoCharge
                            && c.OrderItem.Order.IsLive)
                .Select(i => new ClubInstallmentPair
                {
                    InstallmentId = i.Id,
                    ClubId = i.OrderItem.Order.ClubId

                }).ToList();

            return installments;
        }



        public IQueryable<OrderInstallment> GetAllInstallments(DateTime? startDate, DateTime? endDate)
        {
            if (startDate.HasValue && endDate.HasValue)
            {
                return GetAllInstallments().Where(i => DbFunctions.TruncateTime(i.InstallmentDate) <= endDate && DbFunctions.TruncateTime(i.InstallmentDate) >= startDate);
            }

            if (startDate.HasValue)
            {
                return GetAllInstallments().Where(i => DbFunctions.TruncateTime(i.InstallmentDate) >= startDate);
            }

            return endDate.HasValue ? GetAllInstallments().Where(i => DbFunctions.TruncateTime(i.InstallmentDate) <= endDate) : GetAllInstallments();
        }

        public IQueryable<OrderInstallment> GetAllInstallments()
        {
            return _orderInstallmentRepository.GetList().Where(i => i.OrderItem.Order.IsLive && i.OrderItem.ItemStatus == OrderItemStatusCategories.completed && !i.IsDeleted && i.Amount > 0);
        }
        public IQueryable<OrderInstallment> GetInstallmentOfTransactionDate(DateTime? start, DateTime? end, IQueryable<OrderInstallment> installments)
        {
            if (start.HasValue && end.HasValue)
            {
                installments = installments.Where(i => i.TransactionActivities.Max(t => DbFunctions.TruncateTime(t.TransactionDate)) >= start && i.TransactionActivities.Max(t => DbFunctions.TruncateTime(t.TransactionDate)) <= end && i.TransactionActivities.Any(t =>
                                                                             t.TransactionType == TransactionType.Payment
               && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Failure)));
            }
            else if (start.HasValue)
            {
                installments = installments.Where(i => i.TransactionActivities.Max(t => DbFunctions.TruncateTime(t.TransactionDate)) >= start && i.TransactionActivities.Any(t =>
                                                                             t.TransactionType == TransactionType.Payment
               && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Failure)));
            }
            else if (end.HasValue)
            {
                installments = installments.Where(i => i.TransactionActivities.Max(t => DbFunctions.TruncateTime(t.TransactionDate)) <= end && i.TransactionActivities.Any(t =>
                                                            t.TransactionType == TransactionType.Payment
             && (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Failure)));

            }

            return installments;
        }


        public IQueryable<OrderInstallment> GetOrderItemInstallments(IQueryable<OrderItem> orderOrderItems)
        {
            return orderOrderItems.SelectMany(o => o.Installments.Where(i => (!i.EnableReminder && !i.IsDeleted) && i.Amount > 0 &&
             (i.Status == OrderStatusCategories.initiated || i.Status == OrderStatusCategories.error ||
             ((i.Amount - (i.PaidAmount.HasValue ? i.PaidAmount.Value : 0) > 0)
             && i.Status == OrderStatusCategories.completed))));
        }

        public IEnumerable<OrderInstallment> GetCalendarOrderItemInstallments(IEnumerable<OrderItem> orderItems)
        {
            var installments = orderItems.SelectMany(o => o.Installments);

            var withValidPaidOrTotalAmount = installments.Where(i => !i.IsDeleted || (i.IsDeleted && i.PaidAmount > 0));

            return withValidPaidOrTotalAmount.Where(i => (i.Status == OrderStatusCategories.initiated || i.Status == OrderStatusCategories.error
             || i.Status == OrderStatusCategories.completed));

        }

        public bool CheckIsPayable(OrderInstallment installment)
        {
            if (installment == null) return false;

            return installment.Balance > 0 && !installment.IsDeleted;
        }

        public IQueryable<OrderInstallment> GetAllAutoChargeInstallmentsWithNoReminder(DateTime installmentDate)
        {
            return _orderInstallmentRepository.GetList().Where(c => c.InstallmentDate <= installmentDate &&
            (c.Status == OrderStatusCategories.initiated || ((c.Amount - (c.PaidAmount.HasValue ? c.PaidAmount.Value : 0) > 0)
            && c.Status == OrderStatusCategories.completed)) && c.NoticeSent == false && c.EnableReminder == false &&
            (c.OrderItem.ItemStatus == OrderItemStatusCategories.completed) &&
            c.OrderItem.Order.IsAutoCharge && c.OrderItem.Order.IsLive && c.OrderItem.PaymentPlanStatus == PaymentPlanStatus.Started && !c.IsDeleted);
        }
        public IQueryable<OrderInstallment> GetAllInstallmentsWithNoReminder(DateTime installmentDate)
        {
            return _orderInstallmentRepository.GetList().Where(c => c.InstallmentDate <= installmentDate && (c.Status == OrderStatusCategories.initiated || ((c.Amount - (c.PaidAmount.HasValue ? c.PaidAmount.Value : 0) > 0)
            && c.Status == OrderStatusCategories.completed)) && c.NoticeSent == false && c.EnableReminder == true && (c.OrderItem.ItemStatus == OrderItemStatusCategories.completed) && !c.IsDeleted);
        }
        public IQueryable<OrderInstallment> GetAllUnpaidInstallmentsByDate(DateTime installmentDate)
        {
            return _orderInstallmentRepository.GetList().Where(c => c.InstallmentDate <= installmentDate && c.Status == OrderStatusCategories.initiated && (c.OrderItem.ItemStatus == OrderItemStatusCategories.completed));
        }

        public bool HasOrderItemPastDueDate(OrderItem orderItem, DateTime installmentDate)
        {
            return orderItem.Installments.Any() && orderItem.Installments.Any(i => i.InstallmentDate.Date <= installmentDate.Date && !i.PaidDate.HasValue && !i.PaidAmount.HasValue);
        }

        public List<OrderInstallment> MergeOrderItemInstallments(Order order)
        {
            List<OrderInstallment> result = null;
            var installmentList = order.RegularOrderItems.Where(i => i.Installments != null && i.Installments.Any()).SelectMany(i => i.Installments).Where(i => i.Type != InstallmentType.AddOn);
            if (installmentList != null)
            {
                var installmentGroup = installmentList.GroupBy(i => i.InstallmentDate.Date.ToString("ddMMyyyy"));

                if (installmentGroup != null)
                {
                    result = new List<OrderInstallment>();

                    var installmentId = 0;
                    if (installmentList.Any() && installmentList.First().OrderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                    {
                        installmentId = 1;
                    }

                    foreach (var installment in installmentGroup)
                    {
                        var instList = installment.ToList();
                        result.Add(new OrderInstallment()
                        {
                            Id = installmentId++,
                            Amount = instList.Sum(i => i.Amount),
                            InstallmentDate = instList[0].InstallmentDate,
                            Status = installment.FirstOrDefault().Status,
                            Type = installment.FirstOrDefault().Type,
                        });
                    }

                }
            }
            return result;
        }

        public void CalculateInstallments(Order order, bool isStripeEnabled)
        {
            order.IsAutoCharge = false;
            DateTime now = DateTimeHelper.GetCurrentLocalDateTime(order.Club.TimeZone);

            if (isStripeEnabled && order.RegularOrderItems.Any(c => c.PaymentPlanType == PaymentPlanType.Installment && c.ProgramTypeCategory != ProgramTypeCategory.Subscription && c.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && c.PaymentPlan.IsAutoChargeMandatory))
            {
                order.IsAutoCharge = true;
            }

            var isTestMode = !order.IsLive;
            List<OrderInstallment> installmentList = new List<OrderInstallment>();

            foreach (var item in order.RegularOrderItems)
            {
                _orderInstallmentRepository.DeleteWhere(c => !c.IsDeleted && c.OrderItemId == item.Id && c.Status != OrderStatusCategories.completed && c.Status != OrderStatusCategories.pending && !c.TransactionActivities.Any());

                if (item.Installments.Any())
                {
                    item.Installments.Where(c => c.OrderItemId == item.Id && c.Status != OrderStatusCategories.completed && c.Status != OrderStatusCategories.pending && c.TransactionActivities.Any()).ToList().ForEach(i => i.IsDeleted = true);
                }

                var installmentcharge = item.OrderChargeDiscounts.SingleOrDefault(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.InstallmentFee);

                if (installmentcharge != null)
                {
                    item.TotalAmount -= installmentcharge.Amount;
                    item.OrderChargeDiscounts.Remove(installmentcharge);
                }

                if (item.PaymentPlanType == PaymentPlanType.Installment)
                {
                    var installments = new List<OrderInstallment>();

                    if (item.ProgramTypeCategory != ProgramTypeCategory.Subscription && item.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                    {

                        if (!(item.PaymentPlanId.HasValue && item.PaymentPlanId.Value > 0))
                        {
                            item.PaymentPlanType = PaymentPlanType.FullPaid;
                            break;
                        }
                        if (item.PaymentPlan == null)
                        {
                            item.PaymentPlan = _paymentPlanBusiness.Get(item.PaymentPlanId.Value);
                        }
                        if (item.PaymentPlan.IsAllUser == false && !item.PaymentPlan.Users.Any(c => c.UserId == order.UserId) && order.OrderMode == OrderMode.Online)
                        {
                            item.PaymentPlanType = PaymentPlanType.FullPaid;
                            break;
                        }
                        if (item.PaymentPlan.InstallmentFee.HasValue && item.PaymentPlan.InstallmentFee.Value > 0)
                        {
                            item.OrderChargeDiscounts.Add(new OrderChargeDiscount()
                            {
                                Amount = item.PaymentPlan.InstallmentFee.Value,
                                Category = ChargeDiscountCategory.InstallmentFee,
                                Name = "Payment plan fee",
                                OrderItemId = item.Id,
                                Subcategory = ChargeDiscountSubcategory.Charge
                            });
                            item.TotalAmount += item.PaymentPlan.InstallmentFee.Value;
                        }

                        var numOfInstallment = item.PaymentPlan.ListOfInstallmentDates.Where(d => Convert.ToDateTime(d).CompareTo(DateTime.UtcNow.Date) > 0).Count();

                        if (numOfInstallment > 0)
                        {
                            decimal charges = 0;
                            decimal onlyTuitionconvenienceFee = 0;
                            decimal totalPlanConvenienceFee = 0;
                            if (item.PaymentPlan.CalculationType == CalculationType.OnlyTuition)
                            {
                                charges = item.OrderChargeDiscounts.Where(c => !c.IsDeleted && (int)c.Category > 0 && c.Category != ChargeDiscountCategory.EntryFee).Sum(c => c.Amount);
                                onlyTuitionconvenienceFee = item.GetOrderChargeDiscounts().Where(c => (int)c.Category > 0 && c.Category == ChargeDiscountCategory.Convenience).Sum(c => c.Amount);
                            }
                            else if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge) || item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee) || item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience))
                            {
                                if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                                {
                                    charges = item.OrderChargeDiscounts.Where(c => !c.IsDeleted && !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge).Sum(c => c.Amount);
                                }
                                if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee))
                                {
                                    charges += item.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee).Sum(c => c.Amount);
                                }
                                if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience))
                                {
                                    totalPlanConvenienceFee = item.OrderChargeDiscounts.Where(c => !c.IsDeleted && (int)c.Category > 0 && c.Category == ChargeDiscountCategory.Convenience).Sum(c => c.Amount);
                                }
                            }
                            var totalAmount = item.TotalAmount - charges - totalPlanConvenienceFee;
                            var deposit = (item.PaymentPlan.DepositType == ChargeDiscountType.Fixed ? (item.PaymentPlan.Deposit > totalAmount ? totalAmount : item.PaymentPlan.Deposit) : ((totalAmount * item.PaymentPlan.Deposit) / 100));

                            if (item.Installments == null)
                            {
                                item.Installments = new List<OrderInstallment>();
                            }

                            var installmentAmount = (totalAmount - deposit) / item.PaymentPlan.NumOfInstallments;
                            var firstInsatllmentAmount = deposit + (charges - onlyTuitionconvenienceFee);

                            if (numOfInstallment < item.PaymentPlan.NumOfInstallments)
                            {
                                firstInsatllmentAmount += (item.PaymentPlan.NumOfInstallments - numOfInstallment) * installmentAmount;
                            }

                            installments.Add(new OrderInstallment()
                            {
                                OrderItemId = item.Id,
                                Amount = firstInsatllmentAmount,
                                InstallmentDate = DateTime.UtcNow,
                                NoticeSent = false,
                                Status = OrderStatusCategories.initiated,
                                EnableReminder = !(order.IsAutoCharge)
                            });

                            decimal allInstallmentAmounts = 0;
                            if (installmentAmount > 0)
                            {
                                for (int i = 0; i < item.PaymentPlan.NumOfInstallments; i++)
                                {
                                    var duedate = item.PaymentPlan.ListOfInstallmentDates.ToList()[i];
                                    if (Convert.ToDateTime(duedate).CompareTo(DateTime.UtcNow.Date) > 0)
                                    {
                                        installments.Add(new OrderInstallment()
                                        {
                                            OrderItemId = item.Id,
                                            Amount = installmentAmount,
                                            InstallmentDate = Convert.ToDateTime(duedate),
                                            NoticeSent = false,
                                            Status = OrderStatusCategories.initiated,
                                            EnableReminder = !(order.IsAutoCharge)
                                        });

                                        allInstallmentAmounts += Math.Round(installmentAmount, 2);
                                    }
                                }
                            }

                            if (item.Installments.Any())
                            {
                                item.Installments.Where(i => !i.TransactionActivities.Any() && !i.IsDeleted).ToList().Clear();
                            }

                            if (firstInsatllmentAmount > 0)
                            {
                                var currentTotalAmount = item.TotalAmount - totalPlanConvenienceFee - onlyTuitionconvenienceFee;
                                var depositInstallmentAmount = installments.First().Amount;
                                installments.First().Amount = currentTotalAmount - allInstallmentAmounts;

                                if (depositInstallmentAmount != installments.First().Amount)
                                {
                                    Log.Warning("{OrderItemId}: Installment sum has difference.", item.Id);
                                }
                            }

                            item.Installments.AddRange(installments);
                            installmentList.AddRange(installments.Skip(1));
                        }
                    }
                    else if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                    {
                        _subscriptionBusiness.CalculateInstallment(item, installmentList);
                    }
                    else if (item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        _subscriptionBusiness.CalculateOrderInstallment(item, installmentList);
                    }
                    else
                    {
                        item.PaymentPlanType = PaymentPlanType.FullPaid;
                        item.PaymentPlanId = null;
                    }
                }
                else if (item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare && item.Mode != OrderItemMode.DropIn && item.Mode != OrderItemMode.PunchCard)
                {
                    var applicationFee = _chargeBusiness.GetProgramSeasonCharges(item.ProgramSchedule.Program, ChargeDiscountCategory.ApplicationFee).FirstOrDefault();

                    if (item.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee))
                    {
                        var listDeletedCharge = new List<OrderChargeDiscount>();

                        foreach (var charge in item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee).ToList())
                        {
                            listDeletedCharge.Add(charge);
                            item.TotalAmount -= charge.Amount;
                        }

                        if (listDeletedCharge.Any())
                        {
                            _orderItemBusiness.RemoveOrderItemCharges(listDeletedCharge);
                        }
                    }
                    decimal ApplicationAmount = 0;
                    if (applicationFee != null)
                    {
                        ApplicationAmount = applicationFee.Amount;

                        if (applicationFee.Attributes != null && applicationFee.Attributes.EarlyBirds != null && applicationFee.Attributes.EarlyBirds.Any())
                        {
                            foreach (var earlyBrid in applicationFee.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                            {
                                if (now.Date <= earlyBrid.ExpireDate.Value.Date)
                                {
                                    ApplicationAmount = earlyBrid.Price;
                                    break;
                                }
                            }
                        }

                        var applicationFeeType = applicationFee.ApplyType;

                        switch (applicationFeeType)
                        {
                            case ChargeApplyType.OrderItem:
                                {
                                    item.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                    {
                                        Amount = ApplicationAmount,
                                        Category = applicationFee.Category,
                                        ChargeId = applicationFee.Id,
                                        Description = applicationFee.Description,
                                        Name = applicationFee.Name,
                                        Subcategory = ChargeDiscountSubcategory.Charge,

                                    });

                                    item.TotalAmount += ApplicationAmount;
                                }
                                break;
                            case ChargeApplyType.PerFamilyPerSeason:
                                {
                                    var hasPaidApplicationFeeInItems = _userProfileBusiness.HasPaidApplicationFeeInItems(order.ClubId, order.UserId, item.SeasonId.Value, isTestMode);

                                    if (!hasPaidApplicationFeeInItems)
                                    {
                                        item.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                        {
                                            Amount = ApplicationAmount,
                                            Category = applicationFee.Category,
                                            ChargeId = applicationFee.Id,
                                            Description = applicationFee.Description,
                                            Name = applicationFee.Name,
                                            Subcategory = ChargeDiscountSubcategory.Charge
                                        });

                                        item.TotalAmount += ApplicationAmount;
                                    }
                                }
                                break;
                            case ChargeApplyType.PerParticipantPerSeason:
                                {
                                    var hasPaidApplicationFeeParticipant = _userProfileBusiness.HasPaidApplicationFeeParticipant(item.PlayerId.Value, item.SeasonId.Value, isTestMode);

                                    if (!hasPaidApplicationFeeParticipant)
                                    {
                                        item.OrderChargeDiscounts.Add(new OrderChargeDiscount
                                        {
                                            Amount = ApplicationAmount,
                                            Category = applicationFee.Category,
                                            ChargeId = applicationFee.Id,
                                            Description = applicationFee.Description,
                                            Name = applicationFee.Name,
                                            Subcategory = ChargeDiscountSubcategory.Charge
                                        });

                                        item.TotalAmount += ApplicationAmount;
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            if (order.RegularOrderItems.Any(o => (o.ProgramTypeCategory == ProgramTypeCategory.Subscription || o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && o.Mode == OrderItemMode.Noraml))
            {
                order.IsAutoCharge = true;
            }

            if (installmentList == null || !installmentList.Any())
            {
                return;
            }

            var installmentGroup = installmentList.Where(i => i.Type != InstallmentType.AddOn).GroupBy(i => i.InstallmentDate.Date.ToString("ddMMyyyy"));

            if (installmentGroup != null)
            {
                if (order.OrderInstallments == null)
                {
                    order.OrderInstallments = new List<OrderInstallment>();
                }
                else
                {
                    order.OrderInstallments.Clear();
                }

                foreach (var installment in installmentGroup)
                {
                    var instList = installment.ToList();

                    order.OrderInstallments.Add(new OrderInstallment()
                    {
                        Amount = instList.Sum(i => i.Amount),
                        InstallmentDate = instList[0].InstallmentDate
                    });
                }
            }
        }

        public List<OrderInstallment> TotalInstallments(Order order)
        {
            var installmentList = new List<OrderInstallment>();
            var orderInstallments = new List<OrderInstallment>();
            var today = DateTime.UtcNow;

            foreach (var item in order.RegularOrderItems.Where(c => c.Installments.Any() && c.PaymentPlanType == PaymentPlanType.Installment))
            {
                if (item.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && item.ProgramTypeCategory != ProgramTypeCategory.Subscription)
                {
                    var firstInstallment = item.Installments.First();
                    installmentList.AddRange(item.Installments.Where(i => i.Id != firstInstallment.Id));
                }
                else if (item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                {
                    var getFirstInstallment = _programBusiness.GetFirstInstallmentInRegistrationTime(item.ProgramSchedule.Program);
                    var firstInstallment = item.Installments.First(i => i.Type != InstallmentType.AddOn);

                    foreach (var inst in item.Installments.Where(i => i.Type != InstallmentType.AddOn))
                    {
                        if (inst.InstallmentDate.Date > today.Date)
                        {
                            if (inst.Id != firstInstallment.Id)
                            {
                                installmentList.Add(inst);
                            }
                            else if (!getFirstInstallment)
                            {
                                installmentList.Add(inst);
                            }
                        }
                    }
                }
                else
                {
                    var firstInstallment = item.Installments.First(i => i.Type != InstallmentType.AddOn);
                    installmentList.AddRange(item.Installments.Where(i => i.Type != InstallmentType.AddOn && i.Id != firstInstallment.Id));
                }
            }

            var installmentGroup = installmentList.Where(i => i.Type != InstallmentType.AddOn).GroupBy(i => i.InstallmentDate.Date.ToString("ddMMyyyy"));


            if (installmentGroup != null)
            {
                foreach (var installment in installmentGroup)
                {
                    var instList = installment.ToList();

                    orderInstallments.Add(new OrderInstallment()
                    {
                        Amount = instList.Sum(i => i.Amount),
                        InstallmentDate = instList[0].InstallmentDate,
                        // SchedulePartTitle = !string.IsNullOrEmpty(schedulePart) ? schedulePart : null,
                    });
                }
            }

            return orderInstallments.OrderBy(i => i.InstallmentDate).ToList();
        }
        public OperationStatus Delete(List<long> ids)
        {
            var installments = _orderInstallmentRepository.GetList().Where(p => ids.Contains(p.Id));

            installments.ToList().ForEach(x => _dataContext.Entry(x).State = EntityState.Deleted);

            var result = _orderInstallmentRepository.Save();

            return result;
        }

        public OperationStatus Update(List<OrderInstallment> orderInstallments)
        {
            var installments = _orderInstallmentRepository.GetList().Where(o => o.Id == orderInstallments.SingleOrDefault().Id);

            foreach (var item in installments)
            {
                var installmentItem = orderInstallments.Where(o => o.Id == item.Id);

                if (installmentItem != null)
                {
                    _dataContext.Entry(item).CurrentValues.SetValues(installmentItem);
                }
            }

            var result = _dataContext.SaveChanges();

            return new OperationStatus() { Status = result > 0 };

        }

        public OperationStatus Create(List<OrderInstallment> orderInstallments)
        {
            foreach (var item in orderInstallments)
            {
                _orderInstallmentRepository.Create(item);
            }

            return _orderInstallmentRepository.Save();
        }

        public int CountInstallmentParentNotPaid(IQueryable<OrderItem> orderItems)
        {
            var countInstallment = 0;
            foreach (var item in orderItems)
            {
                var balance = item.TotalAmount - item.PaidAmount;
                var listInstallmets = item.Installments.Where(i => !i.IsDeleted).ToList();
                decimal balanceInstallment = 0;
                foreach (var installmet in listInstallmets)
                {
                    balanceInstallment += installmet.Balance;
                }

                if ((balance > 0 || balanceInstallment > 0))
                {
                    countInstallment++;
                }

            }
            return countInstallment;
        }

        public OperationStatus DeleteInstallments(List<OrderInstallment> orderInstallments)
        {
            orderInstallments.ForEach(i => i.IsDeleted = true);

            return _orderInstallmentRepository.Save();
        }
        public void SetStatusDeletedInstallment(List<OrderInstallment> orderInstallments, DeleteReason reason)
        {
            orderInstallments.ForEach(i => i.IsDeleted = true);
            orderInstallments.ForEach(i => i.DeleteReason = reason);
        }
        public IQueryable<OrderInstallment> GetAllInstallmentsByDate(DateTime? startDate, DateTime? endDate, int? clubId)
        {
            var installments = _orderInstallmentRepository.GetList().Include(o => o.OrderItem.Order.Club).Include(o => o.TransactionActivities).Where(o => ((clubId.HasValue && clubId.Value > 0) ? o.OrderItem.Order.ClubId == clubId.Value : true)
                                                        && ((startDate.HasValue) ? o.InstallmentDate >= startDate.Value : true)
                                                        && ((endDate.HasValue) ? o.InstallmentDate <= endDate.Value : true)
            ).Take(5);
            return installments;

        }
        public ProgramSubscriptionItem GetScheduleInstallment(long installmentId)
        {
            var installment = _orderInstallmentRepository.Get(i => i.Id == installmentId);

            var programSubscriptionItems = _subscriptionBusiness.GetProgramSubscriptionItems(installment.OrderItem);
            var schedule = programSubscriptionItems.Where(p => p.DueDate == installment.InstallmentDate).FirstOrDefault();

            return schedule;
        }

        public string GetSchedulePartInstallment(long installmentId, long programId)
        {
            var installment = _orderInstallmentRepository.Get(i => i.Id == installmentId);
            string scheduleDetail;

            if (installment.Type == InstallmentType.Noraml)
            {
                var programParts = _programBusiness.GetScheduleParts(programId);
                var schedule = programParts.FirstOrDefault(p => p.Id == installment.ProgramSchedulePartId && !p.IsDeleted);

                scheduleDetail = schedule != null ? schedule.StartDate.ToString(Constants.DefaultDateFormat) + " - " + schedule.EndDate.ToString(Constants.DefaultDateFormat) : "-";
            }
            else
            {
                scheduleDetail = installment.Type.ToDescription();
            }

            return scheduleDetail;
        }

        public OperationStatus UndoFailedInstallment(long id)
        {
            var installment = Get(id);
            installment.AutoChargeAttemps = 0;
            installment.UnSuspendDate = DateTime.UtcNow;

            installment.Status = installment.PaidAmount > 0 ? OrderStatusCategories.completed : OrderStatusCategories.initiated;

            var result = _orderInstallmentRepository.Save();
            return result;
        }

        public dynamic UpdateInstallments(List<OrderInstallment> orderInstallments)
        {
            dynamic result = new ExpandoObject();
            var message = string.Empty;

            foreach (var installment in orderInstallments)
            {
                var scheduleDate = installment.OrderItem.ProgramSchedule.StartDate;

                installment.InstallmentDate = scheduleDate.AddDays(-2);
                message += installment.Id.ToString() + ", ";
            }

            var res = _orderInstallmentRepository.Save();

            if (!res.Status)
            {
                throw new Exception(res.ExceptionInnerMessage);
            }

            result.Message = message;
            result.Status = res.Status;

            return result;
        }

        public OperationStatus Update(OrderInstallment orderInstallment)
        {
            return _orderInstallmentRepository.Update(orderInstallment);
        }

        public OperationStatus Create(OrderInstallment orderInstallment)
        {
            return _orderInstallmentRepository.Create(orderInstallment);

        }

        public string GetInstallmentPaymentMode(OrderInstallment installment)
        {
            string result;

            switch (installment.OrderItem.PaymentPlanStatus)
            {
                case PaymentPlanStatus.Stopped when installment.OrderItem.Order.IsAutoCharge:
                    result = installment.OrderItem.PaymentPlanStatusUpdateDate != null ?
                        string.Format(ReportConstants.AutoChargeStoppedWithDateFormat, installment.OrderItem.PaymentPlanStatusUpdateDate.Value.ToString(Constants.DateFormatWithDash))
                        : ReportConstants.AutoChargeStoppedFormat;
                    break;
                case PaymentPlanStatus.Started when installment.OrderItem.Order.IsAutoCharge:
                    result = ReportConstants.AutoCharge;
                    break;
                case PaymentPlanStatus.Started when !installment.OrderItem.Order.IsAutoCharge:
                    result = ReportConstants.Manual;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return result;
        }

        public string GetInstallmentStatus(OrderInstallment installment)
        {
            var balance = installment.Amount - (installment.PaidAmount ?? 0);
            var statusDescription = installment.Status.ToDescription();
            switch (statusDescription)
            {
                case "Error":
                    return "Suspended";
                case "Completed" when balance > 0:
                    return "Partially paid";
                case "Completed" when balance == 0:
                    return "Fully paid";
                case "Completed" when balance < 0:
                    return "Overpaid";
                default:
                    {
                        if (statusDescription != "Suspended" && installment.InstallmentDate.Date < DateTime.UtcNow.Date && balance > 0)
                        {
                            return "Past due";
                        }

                        if ((statusDescription != "Initiated" || statusDescription == "Completed") && installment.Amount == 0 && installment.PaidAmount == 0)
                        {
                            return "";
                        }
                        else
                        {
                            return "Unpaid";
                        }
                    }
            }
        }

        public IQueryable<OrderInstallment> GetInstallmentsByStatus(string status, IQueryable<OrderInstallment> installments)
        {
            if (!string.IsNullOrEmpty(status))
            {
                switch (status)
                {
                    case ReportConstants.Unpaid:
                        installments = installments.Where(c => c.Status == OrderStatusCategories.error || (c.Status == OrderStatusCategories.error && (c.Amount - (c.PaidAmount.HasValue ? c.PaidAmount.Value : 0) == 0)) || c.Status == OrderStatusCategories.initiated ||
                        (c.Status == OrderStatusCategories.completed && (c.Amount - (c.PaidAmount.HasValue ? c.PaidAmount.Value : 0) > 0)));
                        break;
                    case ReportConstants.FullyPaid:
                        installments = installments.Where(c => c.Status == OrderStatusCategories.completed && (c.Amount - (c.PaidAmount.HasValue ? c.PaidAmount.Value : 0) == 0));
                        break;
                    case ReportConstants.Suspended:
                        installments = installments.Where(c => c.Status == OrderStatusCategories.error
                        || (c.Status == OrderStatusCategories.error && (c.Amount - (c.PaidAmount.HasValue ? c.PaidAmount.Value : 0) == 0))
                        && !(c.Status == OrderStatusCategories.completed && (c.Amount - (c.PaidAmount.HasValue ? c.PaidAmount.Value : 0) == 0)));
                        break;
                }
            }
            return installments;
        }

        public IQueryable<OrderInstallment> GetInstallmentsByMode(string mode, IQueryable<OrderInstallment> installments)
        {
            if (!string.IsNullOrEmpty(mode))
            {
                switch (mode)
                {
                    case ReportConstants.AutoChargeMode:
                        installments = installments.Where(c => c.OrderItem.Order.IsAutoCharge);
                        break;
                    case ReportConstants.AutoChargeStartMode:
                        installments = installments.Where(c => c.OrderItem.Order.IsAutoCharge && c.OrderItem.PaymentPlanStatus == PaymentPlanStatus.Started);
                        break;
                    case ReportConstants.AutoChargeStoppedMode:
                        installments = installments.Where(c => c.OrderItem.Order.IsAutoCharge && c.OrderItem.PaymentPlanStatus == PaymentPlanStatus.Stopped);
                        break;
                    case ReportConstants.ManualMode:
                        installments = installments.Where(c => !c.OrderItem.Order.IsAutoCharge);
                        break;
                }
            }
            return installments;
        }

        public IQueryable<OrderInstallment> GetInstallmentsByParticipantInfo(PeopleSearchType? searchType, IQueryable<OrderInstallment> installments, string term, string customeStripeErrorMessage, string firstName, string lastName)
        {
            if (searchType.HasValue)
            {
                switch (searchType.Value)
                {
                    case PeopleSearchType.Email:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                installments = installments.Where(c => c.OrderItem.Order.User.UserName.ToLower().Contains(term));
                            }
                        }
                        break;
                    case PeopleSearchType.ConfirmationId:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                installments = installments.Where(c => c.OrderItem.Order.ConfirmationId.ToLower().Contains(term));
                            }
                        }
                        break;
                    case PeopleSearchType.ErrorMessage:
                        {
                            installments = GetInstallmentsByErrorMessage(customeStripeErrorMessage, term, installments);
                        }
                        break;
                    case PeopleSearchType.Name:
                        {
                            if (!string.IsNullOrWhiteSpace(firstName) || !string.IsNullOrWhiteSpace(lastName))
                            {
                                installments = installments.Where(c => (string.IsNullOrEmpty(firstName) || c.OrderItem.FirstName.ToLower().Contains(firstName.ToLower())) && (string.IsNullOrEmpty(lastName) || c.OrderItem.LastName.ToLower().Contains(lastName.ToLower())));

                            }
                        }
                        break;
                }
            }
            return installments;
        }

        public IQueryable<OrderInstallment> GetInstallmentsByErrorMessage(string customeStripeErrorMessage, string term, IQueryable<OrderInstallment> installments)
        {
            installments = term.Equals(DefaultDropdownItem.NotListed.ToString().ToLower()) ? GetInstallmentsByCustomeErrorMessage(customeStripeErrorMessage, installments) : GetInstallmentsByEnumErrorMessage(term, installments);

            return installments;
        }

        public IQueryable<OrderInstallment> GetInstallmentsByEnumErrorMessage(string stripeErrorMessage, IQueryable<OrderInstallment> installments)
        {
            if (!string.IsNullOrWhiteSpace(stripeErrorMessage))
            {
                var transactions = GetInstallmentTransactions(installments);

                var errorMassageTransactions = transactions
                    .GroupBy(i => i.InstallmentId)
                    .Select(t => t.OrderByDescending(o => o.TransactionDate).FirstOrDefault()).Where(t => t.PaymentDetail.TransactionMessage.Equals(stripeErrorMessage));

                return errorMassageTransactions.Select(t => t.Installment);
            }

            return installments;

        }

        public IQueryable<OrderInstallment> GetInstallmentsByCustomeErrorMessage(string stripeErrorMessage, IQueryable<OrderInstallment> installments)
        {
            if (!string.IsNullOrWhiteSpace(stripeErrorMessage))
            {
                var transactions = GetInstallmentTransactions(installments);

                var errorMassageTransactions = transactions
                    .GroupBy(i => i.InstallmentId)
                    .Select(t => t.OrderByDescending(o => o.TransactionDate).FirstOrDefault()).Where(t => t.PaymentDetail.TransactionMessage.Contains(stripeErrorMessage));

                return errorMassageTransactions.Select(t => t.Installment);
            }

            return installments;

        }

        public IQueryable<TransactionActivity> GetInstallmentTransactions(IQueryable<OrderInstallment> installments)
        {
            return installments.SelectMany(i => i.TransactionActivities).Where(t => t.TransactionType == TransactionType.Payment &&
                (t.TransactionStatus == TransactionStatus.Success || t.TransactionStatus == TransactionStatus.Failure));
        }

        public List<SelectListItem> GetStripeErrorMessage()
        {
            var result = new List<SelectListItem>();

            result.AddRange(Enum.GetValues(typeof(StripeErrorMessage))
                 .Cast<StripeErrorMessage>().Select(p => new SelectListItem() { Text = p.DisplayName(), Value = p.ToDescription() }).ToList());

            return result;
        }

        public PaginatedList<ParentOrderItemInstallmentsViewModel> GetOrderItemInstallments(long orderItemId, List<Sort> sorts, int pageIndex, int pageSize, RoleCategoryType roleType, InstallmentType? installmentType)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);

            var model = new List<ParentOrderItemInstallmentsViewModel>();

            var orderItemInstallments = roleType == RoleCategoryType.Parent ?
                GetListByOrderItemId(orderItemId).Where(i => !i.IsDeleted).ToList()
                : GetListByOrderItemId(orderItemId).Where(i => i.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success ||
                (t.TransactionStatus == TransactionStatus.Failure && t.TransactionDate > orderItem.Order.CompleteDate))
                || (!i.IsDeleted || (i.IsDeleted && i.PaidAmount > 0))).ToList();


            if (installmentType.HasValue)
            {
                orderItemInstallments = orderItemInstallments.Where(i => i.Type == installmentType).ToList();
            }

            if (orderItemInstallments.Any())
            {
                model = orderItemInstallments.Select(MapParentOrderItemInstallmentsViewModel).ToPaginatedList(pageIndex, pageSize, orderItemInstallments.Count());

                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                {
                    var programId = orderItem.ProgramSchedule.ProgramId;
                    model.ForEach(c => c.SchedulePart = GetSchedulePartInstallment(c.Id, programId));
                }

                model.ForEach(c => c.PayerType = roleType);

                if (installmentType == InstallmentType.Noraml)
                {
                    model.First().IsDeposit = true;
                }
            }

            return model.ToPaginatedList(pageIndex, pageSize, orderItemInstallments.Count);
        }

        private ParentOrderItemInstallmentsViewModel MapParentOrderItemInstallmentsViewModel(OrderInstallment installment)
        {

            return new ParentOrderItemInstallmentsViewModel()
            {
                Id = installment.Id,
                PaidAmount = installment.PaidAmount,
                DueDate = installment.InstallmentDate,
                Amount = installment.Amount,
                Status = installment.Status,
                ProgramType = installment.OrderItem.ProgramTypeCategory,
                Currency = installment.OrderItem.Order.Club.Currency,
                OrderItemId = installment.OrderItem.Id,
                IsPastDue = installment.InstallmentDate < DateTime.Now && installment.Balance > 0,
                PaidDate = installment.PaidDate,
                ActionColler = JumbulaSubSystem.Installment,
                IsDeleted = installment.IsDeleted,
                Type = installment.Type,
                Category = installment.ChargeCategory != null ? installment.ChargeCategory.ToDescription() : string.Empty,
                AmountAutomaticallyTransactions = GetAmountAutomaticallyTransactions(installment),
                RefundAmount = GetRefundBalance(installment),
                TakePaymentAmount = installment.Balance > 0 && !installment.IsDeleted ? installment.Balance : 0,
            };

        }

        public OrderInstallment CreateAddOnInstallment(decimal amount)
        {
            var installment =
                   new OrderInstallment
                   {
                       Amount = amount,
                       InstallmentDate = DateTime.UtcNow,
                       Token = null,
                       PaidAmount = 0,
                       PaidDate = null,
                       NoticeSent = false,
                       EnableReminder = false,
                       Type = InstallmentType.AddOn,
                       Status = OrderStatusCategories.initiated
                   };

            return installment;
        }

        public IQueryable<OrderInstallment> GetOrderInstallments(List<long> orderInstallmentIds)
        {
            return _orderInstallmentRepository.GetList().Where(x => orderInstallmentIds.Contains(x.Id));
        }

        public IQueryable<OrderInstallment> GetDelinquentInstallmentItemsAsNoTracking(int clubId, int autoChargeFailAttemptNumber)
        {
            //todo: add to DateTimeHelper (JbDateTime)
            var currentDate = DateTime.UtcNow.Date.AddDays(1);

            var result = _orderInstallmentRepository.GetList().Include(x => x.OrderItem).Include(x => x.OrderItem.Season).Include(x => x.OrderItem.Order).Include(x => x.OrderItem.Order.Club).Include(x => x.OrderItem.Order.Club.ClubType).Where(i =>
                      !i.IsDeleted && !i.EnableReminder && i.OrderItem.ProgramScheduleId != null && i.InstallmentDate < currentDate && i.Amount > (i.PaidAmount ?? 0) &&
                      i.AutoChargeAttemps >= autoChargeFailAttemptNumber);

            result = result.Where(x => x.OrderItem.Order.ClubId == clubId || x.OrderItem.Order.Club.PartnerId == clubId);

            result = result.Where(x =>
                x.OrderItem.ItemStatus != OrderItemStatusCategories.deleted && x.OrderItem.ItemStatus != OrderItemStatusCategories.initiated && x.OrderItem.ItemStatus != OrderItemStatusCategories.notInitiated &&
                x.OrderItem.PaymentPlanType == PaymentPlanType.Installment &&
                x.OrderItem.Order.IsAutoCharge &&
                x.OrderItem.Order.IsLive).AsNoTracking();

            return result;
        }

        public RefundResultModel Refund(OrderItemsViewModel model, ref decimal refundedAmount)
        {
            var result = new RefundResultModel();
            var historyDescription = string.Empty;

            //Get selected installment for refund
            var installment = GetInstallmentToBeRefund(model);

            if (installment != null)
            {
                //options that can picked(Automatically, manually, AddToCredit)
                //first AddToCredit, second manually and in last automatically to do
                var refundOptionsPicked = model.RefundModes.Where(i => i.IsSelected && i.Amount > 0)
                    .OrderBy(p => p.Priority);

                result = RefundByModes(refundOptionsPicked, model, installment, ref refundedAmount);
                var fullRefundAmount = refundOptionsPicked.Sum(m => m.Amount);

                // Update refund installment
                if (refundedAmount > 0 && fullRefundAmount == refundedAmount)
                {
                    historyDescription = string.Format(RefundInstallment, CurrencyHelper.FormatCurrencyWithPenny(refundedAmount, installment.OrderItem.Order.Club.Currency), DateTimeHelper.FormatDate(installment.InstallmentDate.Date));
                }
                else if (refundedAmount > 0 && fullRefundAmount != refundedAmount)
                {
                    historyDescription = string.Format(PartiallyRefundInstallment, CurrencyHelper.FormatCurrencyWithPenny(refundedAmount, installment.OrderItem.Order.Club.Currency), DateTimeHelper.FormatDate(installment.InstallmentDate.Date), CurrencyHelper.FormatCurrencyWithPenny(fullRefundAmount, installment.OrderItem.Order.Club.Currency));
                }

                var updateRefund = _orderItemBusiness.UpdateRefund(result.Status, installment.OrderItem, refundedAmount, model, historyDescription);

                if (!result.Status || updateRefund.Status) return result;
                Log.Error("Error in save refund {ErrorMessage} {InstallmentId}", updateRefund.Message,
                    installment.Id);
                result.Status = updateRefund.Status;
                result.ErrorMessage = RefundInstallmentFailedSaveDb;

                return result;
            }

            result.Status = false;
            result.ErrorMessage = FailProcessRefund;
            return result;

        }

        public decimal GetAmountAutomaticallyTransactions(OrderInstallment installment)
        {
            var paymentAutomaticallyAmount = _transactionActivityBusiness.GetPaymentTransactions(installment).Sum(t => t.Amount);
            var refundAutomaticallyAmount = _transactionActivityBusiness.GetRefundAutomaticallyTransactions(installment).Sum(t => t.Amount);
            return paymentAutomaticallyAmount - refundAutomaticallyAmount;
        }

        public List<RefundInformation> GetRefundModes()
        {
            var refundModes = new List<RefundInformation>
            {
                new RefundInformation
                {
                    Priority = 3,
                    Amount = null,
                    Mode = RefundMode.Automatically,
                    Title = "Issue the refund automatically"
                },
                new RefundInformation
                {
                    Priority = 2,
                    Amount = null,
                    Mode = RefundMode.Manually,
                    Title = "My organization will issue the refund manually"
                },
                new RefundInformation
                {
                    Priority = 1,
                    Amount = null,
                    Mode = RefundMode.AddToCredit,
                    Title = "Transfer the balance to the family credit"
                }
            };
            return refundModes;
        }

        public OrderInstallment GetInstallmentToBeRefund(OrderItemsViewModel model)
        {
            var selectedInstallment = model.Installments.Any(i => i.IsSelected) ? model.Installments.Single(i => i.IsSelected) : model.AddOnInstallments.SingleOrDefault(i => i.IsSelected);
            return selectedInstallment != null ? _orderInstallmentRepository.Get(i => i.Id == selectedInstallment.Id) : null;
        }

        #region private
        private RefundResultModel RefundManually(OrderInstallment installment, string currentUserName, RoleCategoryType currentRoleType, string refundNote, decimal refundAmount)
        {
            var model = new RefundResultModel();
            var refundedAmount = 0m;
            var remainingAmount = 0m;

            try
            {
                model.Status = true;
                model.RefundAmount = refundAmount;

                SetInstallmentRefundedToBeSuccess(installment, model, ref remainingAmount, ref refundedAmount);
                SetPaymentTransaction(PaymentMethod.Cash, model, installment, null, currentUserName, currentRoleType, refundNote);
                model.RefundedAmount = refundedAmount;
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.PaymentStatus = PaymentDetailStatus.ERROR;
                model.TransactionStatus = TransactionStatus.Failure;
                model.ErrorMessage = FailProcessRefund;
                Log.Error(ex, "Refund is failed in {LogCategory} {InstallmentId}", "Manually", installment.Id);
            }
            return model;
        }

        private decimal GetRefundBalance(OrderInstallment installment)
        {
            var installmentPaidAmount = installment.PaidAmount ?? 0;
            var installmentBalance = Math.Abs(installment.Balance < 0 ? installment.Balance : 0);

            return installment.IsDeleted ? installmentPaidAmount : installmentBalance;
        }

        private RefundResultModel RefundToFamilyCredit(OrderInstallment installment, string currentUserName, RoleCategoryType currentRoleType, string refundNote, decimal refundAmount)
        {
            var model = new RefundResultModel();
            var refundedAmount = 0m;
            var remainingAmount = 0m;

            try
            {
                var orderItem = installment.OrderItem;

                model.Status = true;
                model.RefundAmount = refundAmount;
                model.TransactionId = _transactionActivityBusiness.GeneratGuidTransactionId();

                _orderItemBusiness.SetToUserCredit(orderItem, model.RefundAmount);

                SetInstallmentRefundedToBeSuccess(installment, model, ref remainingAmount, ref refundedAmount);
                SetPaymentTransaction(PaymentMethod.Credit, model, installment, null, currentUserName, currentRoleType, refundNote);
                model.RefundedAmount = refundedAmount;
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.ErrorMessage = FailProcessRefund;
                Log.Error(ex, "Refund is failed in {LogCategory} {InstallmentId}", "Add to Credit", installment.Id);

                SetPaymentTransaction(PaymentMethod.Credit, model, installment, null, currentUserName, currentRoleType, refundNote);
            }

            return model;
        }

        private RefundResultModel RefundByModes(IEnumerable<RefundInformation> refundOptionsPicked, OrderItemsViewModel model, OrderInstallment installment, ref decimal refundedAmount)
        {
            var result = new RefundResultModel();
            var refundAmount = 0m;

            foreach (var item in refundOptionsPicked)
            {
                if (item.Amount != null) { refundAmount = item.Amount.Value; }
                var remainAmount = refundAmount;

                switch (item.Mode)
                {
                    case RefundMode.Automatically:

                        //Refund by Paypal, stripe, authorize
                        result = RefundAutomatically(installment, model.CurrentUserName, model.CurrentRoleType, model.CustomRefund.Name, remainAmount);

                        break;

                    case RefundMode.Manually:

                        result = RefundManually(installment, model.CurrentUserName, model.CurrentRoleType, model.CustomRefund.Name, refundAmount);

                        break;

                    case RefundMode.AddToCredit:

                        result = RefundToFamilyCredit(installment, model.CurrentUserName, model.CurrentRoleType, model.CustomRefund.Name, refundAmount);

                        break;
                }

                refundedAmount += result.Status ? result.RefundedAmount : 0;

                if (!result.Status)
                {
                    break;
                }

            }

            return result;
        }

        private RefundResultModel RefundAutomatically(OrderInstallment orderInstallment, string currentUserName, RoleCategoryType currentRoleType, string refundNote, decimal remainingAmount)
        {
            var club = orderInstallment.OrderItem.Order.Club;
            var oldTransactions = _transactionActivityBusiness.GetPaymentTransactions(orderInstallment).ToList();
            var oldRefunded = _transactionActivityBusiness.GetRefundAutomaticallyTransactions(orderInstallment).ToList();
            var refundResult = new RefundResultModel();
            var refundedAmount = 0m;
            var tempRefundedAmount = 0m;

            foreach (var transactionActivity in oldTransactions)
            {
                if (transactionActivity.PaymentDetail.PaymentMethod == null || remainingAmount == 0) continue;

                var paymentMethod = transactionActivity.PaymentDetail.PaymentMethod.Value;
                var isPaypal = paymentMethod == PaymentMethod.Paypal;
                var refundAmount = _orderItemBusiness.GetRefundAmount(transactionActivity, orderInstallment.OrderItem, remainingAmount, !orderInstallment.OrderItem.Order.IsLive, oldRefunded, isPaypal);

                if (refundAmount == 0) continue;

                refundResult = _paymentBusiness.Refund(transactionActivity, new List<OrderInstallment> { orderInstallment }, null, club.Domain, refundAmount, !orderInstallment.OrderItem.Order.IsLive);

                if (refundResult.Status)
                {
                    SetInstallmentRefundedToBeSuccess(orderInstallment, refundResult, ref remainingAmount, ref refundedAmount);
                }

                tempRefundedAmount = refundedAmount;
                //Add Payment detail and transaction
                SetPaymentTransaction(paymentMethod, refundResult, orderInstallment, transactionActivity, currentUserName, currentRoleType, refundNote);
            }

            refundResult.RefundedAmount = tempRefundedAmount;

            return refundResult;
        }

        private void SetInstallmentRefundedToBeSuccess(OrderInstallment installment, RefundResultModel model, ref decimal remainingAmount, ref decimal refundedAmount)
        {
            installment.PaidAmount -= model.RefundAmount;
            remainingAmount -= model.RefundAmount;
            refundedAmount += model.RefundAmount;

            if (installment.PaidAmount == 0)
                installment.Status = OrderStatusCategories.refunded;
        }

        private void SetPaymentTransaction(PaymentMethod method, RefundResultModel model, OrderInstallment installment, TransactionActivity transaction, string currentUserName, RoleCategoryType currentRoleType, string refundNote)
        {
            var payKey = transaction != null ? transaction.PaymentDetail.PayKey : string.Empty;
            var creditCardId = transaction?.PaymentDetail.CreditCardId;
            var paymentStatus = model.Status ? PaymentDetailStatus.COMPLETED : PaymentDetailStatus.ERROR;
            var transactionStatus = model.Status ? TransactionStatus.Success : TransactionStatus.Failure;

            var paymentDetail = new PaymentDetail(currentUserName, paymentStatus, payKey, method, installment.OrderItem.Order.Club.Currency, model.Response, model.ErrorMessage, model.TransactionId, currentRoleType, creditCardId);
            installment.TransactionActivities.Add(_orderItemBusiness.SetRefundTransaction(installment.OrderItem, installment.Id, paymentDetail, transactionStatus, model.RefundAmount, DateTime.UtcNow, refundNote));
        }
        #endregion
    }
}
