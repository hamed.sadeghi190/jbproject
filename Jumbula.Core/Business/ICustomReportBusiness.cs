﻿using Jb.Framework.Common.Forms;
using System.Collections.Generic;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;


namespace Jumbula.Core.Business
{
    public interface ICustomReportBusiness
    {
        ExportDocumnetSchema CustomReportToExcel(List<Dictionary<string, object>> dataModel, List<string> titles = null);

        ExportDocumnetSchema ExportCapacityReport(List<object> dataModel);

        ExportDocumnetSchema ExportInstallmentReport(List<object> dataModel);

        ExportDocumnetSchema ExportFollowupReport(List<object> dataModel);

        ExportDocumnetSchema ExportRegistrationFormReport(List<object> dataModel);

        ExportDocumnetSchema ExportBalanceReport(List<object> dataModel);

        ExportDocumnetSchema ExportCouponReport(List<object> dataModel);

        ExportDocumnetSchema ExportChargeDiscountReport(List<Dictionary<string, string>> dataModel);

        ExportDocumnetSchema ExportFinanceReport(List<object> dataModel);

        ExportDocumnetSchema ExportProviderContactInfo(List<object> dataModel);

        ExportDocumnetSchema ExportRoomAssigments(List<object> dataModel);

        JbSection CreateMoreInfoSection();

        JbSection CreateChargeSection(int clubId);

        JbSection CreateDiscountCouponSection(long? seasonId, int clubId);

        JbSection CreateChessTourneySection();

        JbSection CreateChessTournamentSection();
        string GetProgramDayTime(ProgramSchedule schedule);

        List<Dictionary<string, object>> GetCustomReportParentInfo(EventRoaster eventRoaster,
            List<OrderItem> allOrderItems, ref List<string> titles, bool isPartnerDashboard = false,
            bool isShare = false);
        JbForm RemoveUncheckedElementsJbForm(JbForm jbForm);

        JbSection SetAccessControllJbSection(JbSection jbSection, int clubId);
    }
}
