﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Jumbula.Common.Helper
{
    public static class ImageHelper
    {

        public static byte[] ImageSourceToBytes(BitmapEncoder encoder, ImageSource imageSource)
        {
            byte[] bytes = null;

            if (imageSource is BitmapSource bitmapSource)
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

                using (var stream = new MemoryStream())
                {
                    encoder.Save(stream);
                    bytes = stream.ToArray();
                }
            }

            return bytes;
        }

        public static byte[] Compress(byte[] imageData, System.Windows.Media.PixelFormat format, int width = 640, int height = 480)
        {
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            return ImageSourceToBytes(encoder, Compress(imageData, format, encoder, width, height));
        }

        public static ImageSource Compress(byte[] imageData, System.Windows.Media.PixelFormat format, PngBitmapEncoder encoder, int width = 640, int height = 480)
        {

            using (MemoryStream memoryStream = new MemoryStream())
            {

                encoder.Interlace = PngInterlaceOption.On;
                encoder.Frames.Add(BitmapFrame.Create(BitmapSource.Create(width, height, 96, 96, format, null, imageData, width * format.BitsPerPixel / 8)));
                encoder.Save(memoryStream);
                BitmapImage imageSource = new BitmapImage();
                imageSource.BeginInit();
                imageSource.StreamSource = memoryStream;
                imageSource.EndInit();
                return imageSource;
            }
        }

        public static byte[] ResizeImage(byte[] byteImage)
        {
            byte[] currentByteImageArray = byteImage;

            while (currentByteImageArray.Length > 50000)
            {
                MemoryStream resultStream = new MemoryStream();

                currentByteImageArray = resultStream.ToArray();
                resultStream.Dispose();
                resultStream.Close();
            }

            return currentByteImageArray;
        }
        public static Image ByteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        public static byte[] ImageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, ImageFormat.Jpeg);
            return ms.ToArray();
        }
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static Bitmap ResizeImage(Image image, int width)
        {
            var orginalWidth = image.Width;
            int height;

            if (orginalWidth > image.Width)
            {
                var ratio = orginalWidth / width;

                height = image.Height / ratio;
            }
            else
            {
                height = (image.Height * width) / image.Width;
            }

            return ResizeImage(image, width, height);
        }
        public static Stream GetStream(this BitmapImage image)
        {
            MemoryStream stream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(image));
            encoder.Save(stream);
            return stream;
        }
    }
}