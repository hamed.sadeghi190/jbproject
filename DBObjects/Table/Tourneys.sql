--@mn---2014/10/25---


IF OBJECT_ID('[dbo].[Tourneys]') IS NULL
BEGIN
CREATE TABLE [dbo].[Tourneys](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Domain] [nvarchar](128) NOT NULL,
	[ClubDomain] [nvarchar](128) NOT NULL,
	[ItemId] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[DisplayDate] [nvarchar](128) NULL,
	[ResultFileName] [nvarchar](128) NULL,
	[EventStatus] [int] NOT NULL,
	[Geography] [geography] NOT NULL,
	[MetaData_DateCreated] [datetime] NOT NULL,
	[MetaData_DateUpdated] [datetime] NOT NULL,
	[Club_Id] [int] NULL,
	[SubTitle1] [nvarchar](128) NULL,
	[SubTitle2] [nvarchar](128) NULL,
	[ChiefTD] [nvarchar](128) NULL,
	[ChiefOrganizer] [nvarchar](128) NULL,
	[EF_OnLine] [decimal](18, 2) NOT NULL DEFAULT((0)),
	[EF_OnLineDeadline] [datetime] NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[EF_OnLine1Checked] [bit] NOT NULL DEFAULT ((0)),
	[EF_OnLine1After] [datetime] NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[EF_OnLine1] [decimal](18, 2) NOT NULL DEFAULT ((0)),
	[EF_OnLine2Checked] [bit] NOT NULL DEFAULT ((0)),
	[EF_OnLine2After] [datetime] NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[EF_OnLine2] [decimal](18, 2) NOT NULL DEFAULT ((0)),
	[EF_OnSite] [decimal](18, 2) NULL DEFAULT ((0)),
	[Hotel_Checked] [bit] NOT NULL DEFAULT ((0)),
	[Hotel_Rate] [decimal](18, 2) NULL DEFAULT ((0)),
	[Hotel_RateCode] [nvarchar](64) NULL,
	[Prize_Fund] [int] NOT NULL DEFAULT ((0)),
	[Prize_BasedOn] [int] NOT NULL DEFAULT ((0)),
	[Prize_Guarantee] [nvarchar](128) NULL,
	[Bye_IsDefined] [bit] NOT NULL DEFAULT ((0)),
	[Bye_Max] [int] NOT NULL DEFAULT ((0)),
	[Bye_Rounds] [int] NOT NULL DEFAULT ((0)),
	[Bye_CommitBy] [int] NOT NULL DEFAULT ((0)),
	[Bye_Note] [nvarchar](128) NULL,
	[OfflineRegChecked] [bit] NOT NULL DEFAULT ((0)),
	[ExternalRegUrl] [nvarchar](128) NULL,
	[TrophyInfo] [nvarchar](max) NULL,
	[TourneyType] [int] NOT NULL DEFAULT ((0)),
	[TimeZone] [int] NOT NULL DEFAULT ((0)),
	[Hotel_Site] [nvarchar](256) NULL,
	[Hotel_Description] [nvarchar](max) NULL,
	[Hotel_Phone] [nvarchar](64) NULL,
	[Hotel_Name] [nvarchar](256) NULL,
	[CategoryId] [int] NOT NULL DEFAULT ((0)),
	[EventSearchField_MinAge] [int] NULL DEFAULT ((0)),
	[EventSearchField_MaxAge] [int] NULL,
	[EventSearchField_MinGrade] [int] NULL,
	[EventSearchField_MaxGrade] [int] NULL,
	[EventSearchField_Level] [int] NULL DEFAULT ((0)),
	[EventSearchField_Gender] [int] NULL DEFAULT ((0)),
	[AsTemplate] [bit] NOT NULL DEFAULT ((0)),
	[ClubLocation_Id] [int] NULL,
	[Donation_Checked] [bit] NOT NULL DEFAULT ((0)),
	[Donation_Title] [nvarchar](256) NULL,
	[Donation_Description] [nvarchar](max) NULL,
	[EventSearchTags_SpecialNeeds] [bit] NOT NULL DEFAULT ((0)),
	[EventSearchTags_Discounts] [bit] NOT NULL DEFAULT ((0)),
	[EventSearchTags_OverNight] [bit] NOT NULL DEFAULT ((0)),
	[EventSearchTags_AfterSchool] [bit] NOT NULL DEFAULT ((0)),
	[TemplateName] [nvarchar](max) NULL,
	[FileName] [nvarchar](128) NULL,
	[FlyerType] [int] NOT NULL DEFAULT ((0)),
	[OutSourcerClubDomain] [nvarchar](128) NULL,
	[RegisterDeadLine_StartRegisterDeadLineDate] [datetime] NULL,
	[RegisterDeadLine_ShowEvent] [bit] NOT NULL DEFAULT ((0)),
	[JbForm_Id] [int] NULL,
	[LastCreatedPage] [int] NOT NULL DEFAULT ((4)))
END
GO
--<< ADD PRIMARY KEY >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='PK_dbo.Tourneys')
BEGIN 
	ALTER TABLE [dbo].[Tourneys] 
	ADD CONSTRAINT [PK_dbo.Tourneys] PRIMARY KEY CLUSTERED(id desc)
END

--<< ADD COLUMNS >>--
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[tourneys]') AND name='LastCreatedPage')
BEGIN
	ALTER TABLE [dbo].[tourneys]  
	ADD [LastCreatedPage] [int] NOT NULL DEFAULT((4))
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[tourneys]') AND name='JbForm_Id')
BEGIN
	ALTER TABLE [dbo].[tourneys]  
	ADD [JbForm_Id] [int]  NULL 
END

--<< EDIT COLUMNS >>--
IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[tourneys]') AND name='EF_OnSite')
BEGIN
	ALTER TABLE [dbo].[tourneys] 
	ALTER COLUMN [EF_OnSite] decimal(18,2) NULL
END

IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[tourneys]') AND name='DisplayDate')
BEGIN
	ALTER TABLE [dbo].[tourneys] 
	ALTER COLUMN [DisplayDate] nvarchar(128) null
END

IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[tourneys]') AND name='EF_OnlineDeadline')
BEGIN
	ALTER TABLE [dbo].[tourneys]  
	ALTER COLUMN [EF_OnlineDeadline] datetime null
END

IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[tourneys]') AND name='EF_Online1After')
BEGIN
	ALTER TABLE [dbo].[tourneys]  
	ALTER COLUMN [EF_Online1After] datetime null
END

IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[tourneys]') AND name='EF_Online2After')
BEGIN
	ALTER TABLE [dbo].[tourneys]  
	ALTER COLUMN [EF_Online2After] datetime null
END


--<< ADD FOREGIN KEYS >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.Tourneys_dbo.Categories_CategoryId')
BEGIN
	ALTER TABLE [dbo].[Tourneys]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tourneys_dbo.Categories_CategoryId] FOREIGN KEY([CategoryId])
	REFERENCES [dbo].[Categories] ([Id])
	ON DELETE CASCADE
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.Tourneys_dbo.ClubLocations_ClubLocation_Id')
BEGIN
	ALTER TABLE [dbo].[Tourneys]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tourneys_dbo.ClubLocations_ClubLocation_Id] FOREIGN KEY([ClubLocation_Id])
	REFERENCES [dbo].[ClubLocations] ([Id])
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.Tourneys_dbo.Clubs_Club_Id')
BEGIN
	ALTER TABLE [dbo].[Tourneys]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tourneys_dbo.Clubs_Club_Id] FOREIGN KEY([Club_Id])
	REFERENCES [dbo].[Clubs] ([Id])
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.Tourneys_dbo.JbForms_JbForm_Id')
BEGIN
	ALTER TABLE [dbo].[Tourneys]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Tourneys_dbo.JbForms_JbForm_Id] FOREIGN KEY([JbForm_Id])
	REFERENCES [dbo].[JbForms] ([Id])
END
GO
