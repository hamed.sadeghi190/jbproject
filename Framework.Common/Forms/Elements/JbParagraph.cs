﻿using Jb.Framework.Common.Forms.JsonFormatter;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbParagraph : JbBaseElement, IInputElement
    {
        public JbParagraph()
        {
            Width = ElementWidth.Full;
        }

        public virtual string Value
        {
            get;
            set;
        }

        public string GetValue()
        {
            return Value;
        }
    }
}