﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('schoolDistrictController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {
        window.app.globalObjects.pageClass = 'Subsidies_View';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + '/Dashboard/Club';

        $scope.searchTerm = "";
        $scope.filter = {};

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetAllSchoolDistricts/',
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                               term: $scope.searchTerm,
                           }, data);
                        return JSON.stringify(data);
                    }
                },

            },
            pageSize: 50,
            batch: true,
            serverPaging: true,
            serverSorting: false,

            schema: {
                data: "data",
                total: "total"
            },
            sortable: false,
        });
        $scope.DistrictGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            serverPaging: true,
            scrollable: true,
            columns: [
            {
                field: "Name",
                title: "Name",
                labels: {
                    visible: true,
                },
            },
            {
                field: "Domain",
                title: "Domain",
                labels: {
                    visible: true,
                },
            },
            {
                field: "Address",
                title: "Address",
                labels: {
                    visible: true,
                },
            },
            {
                title: "Action",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: '<ul kendo-menu style="display: inline-block;overflow: visible" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li ui-sref="AddClubDistrictStep2({clubId: #=Id#})">\
                                          <span class="k-link jbi-pencil">View/Edit</span>\
                                    </li>\
                                    <li ng-click="deleteDistrict(#=Id#)">\
                                          <span class="k-link jbi-cancel">Delete</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>'
            }
            ],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },

        };
        $scope.search = function () {
            $("#District").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {

            $scope.searchTerm = "";
            $("#District").data("kendoGrid").dataSource.read();
        };

        $scope.AddDistrict = function () {
            $state.go('AddClubDistrict');
        }

        $scope.deleteDistrict = function (id) {
            jbConfirmation
               .yes(function () {
                   $http.post($scope.baseUrl + '/DeleteClub', { clubId: id }).success(function (data, status, headers, config) {
                       if (data.Status == true) {

                           $scope.DistrictGridOptions.dataSource.read();

                       }
                   });
               })
               .confirm();
        }
    }])

})();