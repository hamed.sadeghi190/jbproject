﻿using Jb.Framework.Common;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class SeasonPaymentPlanBusiness : ISeasonPaymentPlanBusiness
    {
        private readonly IRepository<SeasonPaymentPlan, long> _seasonPaymentPlanRepository;
        private readonly IEntitiesContext _dataContext;
        public SeasonPaymentPlanBusiness(IRepository<SeasonPaymentPlan, long> seasonPaymentPlanRepository, IEntitiesContext dataContext)
        {
            _seasonPaymentPlanRepository = seasonPaymentPlanRepository;
            _dataContext = dataContext;
        }

        public OperationStatus Create(Season season, List<long> paymentPlanIds)
        {

            var result = new OperationStatus();
            var seasonPaymentPlans = new List<SeasonPaymentPlan>();

            if (season.SeasonPaymentPlans.Any())
            {
                season.SeasonPaymentPlans.ToList().ForEach(p => p.IsDeleted = true);
            }


            foreach (var paymentPlanId in paymentPlanIds)
            {
                var seasonPaymentPlan = new SeasonPaymentPlan()
                {
                    SeasonId = season.Id,
                    PaymentPlanId = paymentPlanId
                };

                seasonPaymentPlans.Add(seasonPaymentPlan);
            }

            _dataContext.Set<SeasonPaymentPlan>().AddRange(seasonPaymentPlans);

            result = _seasonPaymentPlanRepository.Save();

            return result;
        }

        public List<SeasonPaymentPlan> GetSeasonPaymentPlans(long seasonId)
        {
            return _seasonPaymentPlanRepository.GetList(c => c.SeasonId == seasonId).ToList();
        }

        public IEnumerable<PaymentPlan> GetPaymentPlansByUserIdForMultiRegister(Season season, int userId)
        {
            List<PaymentPlan> paymentplans = new List<PaymentPlan>();

            var allProgramPaymentplan = season.PaymentPlans.Where(p => p.IsAllProgram && !p.IsDeleted && season.SeasonPaymentPlans.Where(pp => !pp.IsDeleted).Select(s => s.PaymentPlanId).Contains(p.Id));

            paymentplans.AddRange(allProgramPaymentplan.ToList());

            return paymentplans.Where(p => (p.IsAllUser || p.Users.Any(c => c.UserId == userId)) && (p.ExpiryDate == null || p.ExpiryDate.Value.Date.CompareTo(DateTime.UtcNow.Date) >= 0));

        }

    }
}
