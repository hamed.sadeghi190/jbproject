﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Attributes;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class ContactPerson : BaseEntity, ICustomizableEntity
    {
        [StringLength(64)]
        [Display(Name = "First name")]
        [JbCustomField(true, false, true)]
        public string FirstName { get; set; }

        [StringLength(64)]
        [Display(Name = "Last name")]
        [JbCustomField(true, false, true)]
        public string LastName { get; set; }

        [Display(Name = "Organization")]
        [JbCustomField(true, false, true)]
        [NotMapped]
        public string OrganizationName => Club != null ? Club.Name : string.Empty;

        [StringLength(64, ErrorMessage = "{0} is too long")]
        [Display(Name = "Occupation")]
        public string Occupation { get; set; }

        [StringLength(64, ErrorMessage = "{0} is too long")]
        [Display(Name = "Employer")]
        public string Employer { get; set; }


        [StringLength(64)]
        [Display(Name = "Nickname")]
        [JbCustomField(false, false, false)]
        public string Nickname { get; set; }

        [MaxLength(128)]
        [Display(Name = "Email")]
        [JbCustomField(true, false, true)]
        public string Email { get; set; }

        [StringLength(64)]
        [Display(Name = "Phone")]
        [JbCustomField(true, false, true)]
        public string Phone { get; set; }

        [StringLength(64)]
        [Display(Name = "Work phone extension")]
        public string PhoneExtension { get; set; }

        [StringLength(64)]
        [Display(Name = "Cell phone")]
        [JbCustomField(false, false, false)]
        public string Cell { get; set; }

        [StringLength(64)]
        [Display(Name = "Work phone")]
        [JbCustomField(false, false, false)]
        public string Work { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Date of birth")]
        [JbCustomField(false, false, false)]
        public DateTime? DoB { get; set; }

        [ForeignKey("Club_Id")]
        public virtual Club Club { get; set; }

        public int? Club_Id { get; set; }

        [NotMapped]
        public int? Age
        {
            get
            {
                if (!DoB.HasValue)
                    return null;


                var currentDate = DateTime.Now;

                var result = currentDate.Year - DoB.Value.Year;
                if (currentDate.DayOfYear < DoB.Value.DayOfYear)
                {
                    result--;
                }
                return result;
            }
        }

        [NotMapped]
        [Display(Name = "Full name")]
        public string FullName => $@"{FirstName} {LastName}";

        [Display(Name = "Title")]
        [JbCustomField(true, false, true)]
        public StaffTitle? Title { get; set; }

        public bool IsPrimary { get; set; }

        public int? AddressId { get; set; }

        [ForeignKey("AddressId")]
        public virtual PostalAddress Address { get; set; }

        public ParentRelationship? Relationship { get; set; }

        [NotMapped]
        public bool IsStaff { get; set; }

        [NotMapped]
        public StaffStatus StaffStatus { get; set; }

        [NotMapped]
        public string StaffRole { get; set; }

        public List<JbFieldSetting> GetDefaultSetting()
        {
            return JbFieldSetting.GetFieldSettings(GetType().GetProperties());
        }
    }
}