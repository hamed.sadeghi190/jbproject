﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class OrderItemSchedulePart: BaseEntity
    {
        public long OrderItemId { get; set; }
        [ForeignKey("OrderItemId")]
        public virtual OrderItem OrderItem { get; set; }

        public int? ProgramPartId { get; set; }

        [ForeignKey("ProgramPartId")]
        public virtual ProgramSchedulePart ProgramPart { get; set; }

        public decimal PartAmount { get; set; }

        public bool IsDeleted { get; set; }

        public DeleteReason? DeleteReason { get; set; }
    }
}
