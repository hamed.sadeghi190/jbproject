﻿using System;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Web.WebLogic.UserService;

namespace Jumbula.Web.Mvc.Controllers
{
    [Authorize(Roles = "Parent, Support, Partner, Admin")]
    public class ParentBaseController : JbBaseController
    {
        private int _currentUserId;
        bool _isSwitchedAsAdminMode;

        public int CurrentUserId => _currentUserId;

        public bool IsSwitchedAsAdminMode => _isSwitchedAsAdminMode;

        public override void CheckResourceForAccess(IProtectedResource protectedResource, bool notAccessibleIfNull = false)
        {
            if (protectedResource == null)
                if (notAccessibleIfNull)
                    PageNotFound();
                else
                    return;

            if (!protectedResource.DoesTheParentOwnIt(CurrentUserId))
                PageNotFound();
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);

            if (!this.IsUserAuthenticated())
            {
                return;
            }

            var currentRoleType = JbUserService.GetCurrentUserRoleType();
            if (currentRoleType == RoleCategoryType.Parent)
            {
                _currentUserId = JbUserService.GetCurrentUserId();
            }
            else
            {
                var currentAdminId = JbUserService.GetCurrentUserId();

                if (JbUserService.IsSwitchedAsParentMode())
                {
                    _currentUserId = JbUserService.GetSwitchedParentUserId(currentAdminId);

                    _isSwitchedAsAdminMode = true;

                    ViewBag.IsSwitchedAsAdminMode = true;
                }
                else
                {
                    throw new Exception("Admin hasn't access to parent dashboard.");
                }
            }
        }
    }
}