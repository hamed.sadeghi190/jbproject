﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AddDebitViewModel
    {
        public int ClubId { get; set; }
        public string ClubName { get; set; }
        public DateTime Date { get; set; }

        public string Description { get; set; }

        public decimal DebitAmount { get; set; }
    }
}