﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('BalanceReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;

        $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model })
            .success(function (data, status, headers, config) {
                if (is.undefined($scope.Model.Filters)) {
                    $scope.Model.Filters = {};
                    $scope.Model.Filters.Status = "regular";
                    $scope.Model.Filters.Tuition = "";
                    $scope.Model.Filters.Tuitions = data.Tuitions;
                    $scope.Model.Filters.OrderStatus = data.OrderStatus;
                    $scope.Model.Filters.BalanceFilter = "All";
                }
                
            }).error(function (data, status, headers, config) {
                //alert(data);
            });

        $scope.balanceReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/BalanceReportRun',
                        type: "POST",
                        data: function () {
                            return {
                                model: model,
                                //filters: model.Filters,
                                term: $scope.searchTerm,
                            };
                        },
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
            batch: true,
            resizable: true,
        });

        $scope.BalacneReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "balance (" + $scope.Model.SeasonDomain + ").xlsx",
                proxyURL: "/Dashboard/CustomReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.balanceReportsDataSource,
            //groupable: true,
            sortable: { mode: 'single' },
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100
                ],
                buttonCount: 5
            },
            dataBound: function (e) {
                customReportService.initializeDoubleScroll();
            },
            columnMenu: true,
            columns: [
                {
                    field: "FullName",
                    title: "Full name",
                    width: "180px",
                    locked: true,
                    lockable: true,
                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderItemDetail(#=OrderItemId#)'  class='block'> #= FullName # </a> #if(ItemStatusReason == 1) {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == 2) {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == 5) {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == 4){# <i class='grid-label label warning'>Canceled</i> #}#"
                },
                {
                    field: "Email",
                    title: "Email",
                    lockable: true,
                },
                {
                    field: "ProgramName",
                    title: "Program",
                    width: "200px",
                    locked: true,
                    lockable: true,
                },
                {
                    field: "RegDate",
                    title: "Regsitration date",
                    width: "165px",
                    lockable: true,
                },
                {
                    field: "ConfirmationId",
                    title: "Confirmation",
                    width: "155px",
                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderDetail(#=OrderId#)' class='block'> #= ConfirmationId # </a>",
                    lockable: true,
                },
                {
                    field: "PaymentDetail",
                    title: "Payment method details",
                    width: "210px",
                    lockable: true,
                },
                {
                    field: "Balance",
                    title: "Balance details",
                    lockable: true,
                    width: "145px",
                }
            ],
        }


        $scope.filterData = function () {
            $scope.BalacneReportGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters = {};
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.Status = "regular";
            $scope.Model.Filters.Tuition = "";
            model.Filters.BalanceFilter = "All";
            $scope.searchTerm = "";
            $scope.BalacneReportGridOptions.dataSource.read();
            $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model }).success(function (data, status, headers, config) {
                $scope.Model.Filters.Tuitions = data.Tuitions;
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            });
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#BalanceReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/BalanceReportRun', { model: model, filters: model.Filters, makeCampaign: true, term: $scope.searchTerm })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        $scope.goOrderDetail = function (orderId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderDetail($scope.MC.stateParams.seasonDomain, orderId);
        }

        $scope.goOrderItemDetail = function (orderItemId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderItemDetail($scope.MC.stateParams.seasonDomain, orderItemId);
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "Balance" });
        }
    }]);
})();