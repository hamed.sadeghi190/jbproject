﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface ISecurityBusiness
    {
        List<ActionRole> GetActionRoles();

        List<ActionRole> GetActionRoles(List<RoleCategory> roles);

        List<ActionRole> GetActionRoles(RoleCategory role);

        List<RoleCategory> GetRolesOfAction(JbAction jbAction);

        List<RoleCategory> GetRolesOfActions(List<JbAction> jbActions);

        List<RoleCategory> GetManageableDasboardRolesWithRole(RoleCategory managerRole, ClubType clubtype);
    }
}
