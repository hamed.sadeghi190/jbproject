﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.UserService;

namespace SportsClub.Models.Register
{
    public class RegisterProgramModel
    {
        #region Properties
        public bool IsPostBack { get; set; }
        public bool IsSelectProfile { get; set; }
        public int UserId { get; set; }
        public string ClubDomain { get; set; }
        public string ClubLogoUrl { get; set; }
        public string SeasonDomain { get; set; }
        public long SeasonId { get; set; }
        public long ProgramId { get; set; }
        public string ProgramDomain { get; set; }
        public bool HideProgramPrice { get; set; }
        public string ProgramName { get; set; }
        public string CLubName { get; set; }
        public DateTime ProgramStartDate { get; set; }
        public DateTime ProgramEndDate { get; set; }

        public DateTime? DesiredStartDate { get; set; }
        public string ClubAddress { get; set; }

        public CurrencyCodes Currency { get; set; }
        public string LocationName { get; set; }
        public ProgramTypeCategory ProgramTypeCategory { get; set; }
        public ProgramStatus ProgramStatus { get; set; }
        public int? ProfileId { get; set; }
        public long? PaymentPlanId { get; set; }
        public RoleCategory? CurrentUserRole { get; set; }
        public PlayerProfile Player { get; set; }

        public bool IsRestrictedAge { get; set; }
        public List<RegisterProgramScheduleModel> ProgramSchedules { get; set; }
        public List<RegisterChargeDiscountModel> ChargeDiscounts { get; set; }
        public string ChargeDiscountsText { get; set; }
        public JbForm JbForm { get; set; }
        public List<PaymentPlan> PaymentPlanLists { get; set; }
        public PaymentPlan PaymentPlan { get; set; }
        public bool IsRestrictedUser { get; set; }
        public int CategoryId { get; set; }
        public int? SelectedProfileId { get; set; }
        public string PlayerImageUrl { get; set; }
        public OrderMode OrderMode { get; set; }
        public bool IsEligibleUser { get; set; }
        public bool IsAdultLoggedInUser { get; set; }
        public RegistrationMode RegistrationMode { get; set; }
        public RegistrationType RegistrationType { get; set; }
        public DonationViewModel Donation { get; set; }
        public List<SelectListItem> PlayerLists { get; set; }
        public List<ClubWaiver> ClubWaivers { get; set; }
        public long? ISTS { get; set; }//First Item Setup TimeStamp

        public string PassedPin { get; set; }
        public bool HasEarlyBird { get; set; }
        public bool IsTeamRegistration { get; set; }

        public RegisterPageStep PageStep { get; set; }
        public GoogleMapsViewModel Map { get; set; }

        public int? MinAge { get; set; }

        public int? MaxAge { get; set; }
        public Jumbula.Common.Enums.TimeZone? ClubTimeZone { get; set; }
        public string ClubPhoneNumber { get; set; }
        public string PhoneExtension { get; set; }
        public string ClubEmail { get; set; }
        public List<OrderItemWaiverViewModel> Waivers { get; set; }

        //public List<TournamentSection> ChessSections { get; set; }
        //public List<TournamentSchedule> ChessSchedules { get; set; }
        public RegisterChessModel ChessTournament { get; set; }

        public string CalendarSelectedItems { get; set; }//list of schedule and selected item e.g. 1007:11

        public bool HidePhoneNumber { get; set; }

        public string ScheduleDate { get; set; }

        public bool DisableCapacityRestriction { get; set; }

        public bool SubscriptionHasFullPayOption { get; set; }

        public SubscriptionMode SubscriptionMode { get; set; }

        public List<DayOfWeek> Days { get; set; }

        public string SubscriptionMessage { get; set; }

        public string DynamicStyle { get; set; }

        public RegisterPageStep? TargetPageStep { get; set; }

        public bool EnabledWaitList { get; set; }
        #endregion
        public RegisterProgramModel()
        {
            IsPostBack = false;
            ProgramSchedules = new List<RegisterProgramScheduleModel>();
            ChargeDiscounts = new List<RegisterChargeDiscountModel>();
            PaymentPlanLists = new List<PaymentPlan>();
            Donation = new DonationViewModel();
            ClubWaivers = new List<ClubWaiver>();
            PlayerLists = new List<SelectListItem>();
            OrderMode = OrderMode.Online;
            ChessTournament = new RegisterChessModel();
            Days = new List<DayOfWeek>();
        }

        public RegisterProgramModel(Program program, int? userId)
            : this()
        {
            if (program.ClubLocation.PostalAddress != null && !string.IsNullOrEmpty(program.ClubLocation.PostalAddress.Address))
            {
                ClubAddress = program.ClubLocation.PostalAddress.Address;
                LocationName = program.ClubLocation.Name;
            }

            SeasonDomain = program.SeasonDomain;
            SeasonId = program.SeasonId;

            ClubDomain = program.ClubDomain;
            CLubName = program.Club.Name;
            ClubPhoneNumber = program.Club.ContactPersons.FirstOrDefault().Phone;
            ClubEmail = program.Club.ContactPersons.FirstOrDefault().Email;

            ProgramDomain = program.Domain;
            HideProgramPrice = program.Club.Setting != null ? program.Club.Setting.ClubProgram.EnablePriceHidden ? !program.Club.Setting.ClubProgram.HideAllProgramsPrice ? program.Club.Setting.ClubProgram.HiddenPricePrograms != null ? program.Club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false;
            ProgramId = program.Id;
            ProgramName = program.Name;
            ProgramStatus = program.Status;
            ProgramTypeCategory = program.TypeCategory;
            foreach (var schedule in program.ProgramSchedules.Where(sc => !sc.IsDeleted).ToList().OrderBy(s => s.StartDate).ThenByDescending(s => s.EndDate))
                ProgramSchedules.Add(new RegisterProgramScheduleModel(schedule, program.Club.TimeZone));

            RegistrationMode = program.RegistrationMode;

            DisableCapacityRestriction = program.DisableCapacityRestriction;

            UserId = userId.HasValue ? userId.Value : JbUserService.GetCurrentUserId();
            OrderMode = (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Dashboard) ? OrderMode.Offline : OrderMode.Online;
            ProgramStartDate = Ioc.ProgramBusiness.StartDate(program).Value;
            ProgramEndDate = Ioc.ProgramBusiness.EndDate(program).Value;
            if (OrderMode == OrderMode.Online)
                ClubWaivers = program.ClubWaivers.ToList();

            Map = new GoogleMapsViewModel()
            {
                Name = program.ClubLocation.Name,
                PostalAddress = program.ClubLocation.PostalAddress,
                ContainerClass = "map-modal-container",
                ContainerId = "mapModalCotainer"
            };
            Currency = program.Club.Currency;
            ClubLogoUrl = UrlHelpers.GetClubLogoUrl(ClubDomain, program.Club.Logo);
            ClubTimeZone = program.Club.TimeZone;
            if (program.TypeCategory == ProgramTypeCategory.ChessTournament)
            {
                ChessTournament.ScheduleAttribute = program.ProgramSchedules.FirstOrDefault().Attributes as TournamentScheduleAttribute;
            }
            else if (program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                var subscriptionAttribute = Ioc.SubscriptionBusiness.GetSubscriptionAtribute(program.ProgramSchedules.Last(s => !s.IsDeleted));

                SubscriptionHasFullPayOption = subscriptionAttribute.HasPayInFullOption;
            }
            else if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                var beforeAfterCareAttribute = Ioc.SubscriptionBusiness.GetBeforAfterCareAtribute(program.ProgramSchedules.Last(s => !s.IsDeleted));

                SubscriptionHasFullPayOption = beforeAfterCareAttribute.HasPayInFullOption;
            }
        }
        public RegisterProgramModel(Program program, RegisterProgramModel oldModel, int? userId)
            : this(program, userId)
        {
            Currency = program.Club.Currency;
            ProfileId = oldModel.ProfileId;
            if (oldModel.ProfileId.HasValue)
                SelectedProfileId = oldModel.ProfileId.Value;
            IsSelectProfile = SelectedProfileId != null ? true : false;
            PlayerLists = oldModel.PlayerLists;
            Player = oldModel.Player;
            Days = oldModel.Days;
            DesiredStartDate = oldModel.DesiredStartDate;
            SubscriptionMode = oldModel.SubscriptionMode;
            IsPostBack = oldModel.IsPostBack;
            ChargeDiscountsText = oldModel.ChargeDiscountsText;
            JbForm = oldModel.JbForm;
            ScheduleDate = oldModel.ScheduleDate;
            PaymentPlanId = oldModel.PaymentPlanId;
            if (program.TypeCategory == ProgramTypeCategory.ChessTournament)
            {
                ChessTournament = oldModel.ChessTournament;
                ChessTournament.ScheduleAttribute = program.ProgramSchedules.FirstOrDefault().Attributes as TournamentScheduleAttribute;
            }
            else if (program.TypeCategory == ProgramTypeCategory.Calendar)
            {
                CalendarSelectedItems = oldModel.CalendarSelectedItems;
            }
        }
    }

    public enum SubscriptionMode : byte
    {
        Normal,
        DropIn,
        PunchCard
    }

    public class RegisterChessModel
    {
        public RegisterChessModel()
        {
            ScheduleAttribute = new TournamentScheduleAttribute();
        }
        public TournamentScheduleAttribute ScheduleAttribute { get; set; }
        public bool[] ByeRequests { get; set; }

        [Required]
        [Display(Name = "Available sections")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Section { get; set; }

        [Required]
        [Display(Name = "Available schedules")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Schedule { get; set; }

        public DateTime? ScheduleStartDate { get; set; }
        public DateTime? ScheduleEndDate { get; set; }
    }
    public enum RegisterPageStep
    {
        registrationOptions = 0,
        selectProfile = 1,
        registrationForm = 2,
        registrationWaivers = 3,
    }
    public class CalendarSelectedItem
    {
        public int Id { get; set; }
        public int ScheduleId { get; set; }
    }

    public class RegisterSubscriptionViewModel
    {
        public decimal Price { get; set; }
        public decimal TuitionPrice { get; set; }
        public string PriceLabel { get; set; }
        public long ScheduleId { get; set; }
        public string Title { get; set; }
        public string ScheduleTitle { get; set; }
        public CurrencyCodes Currency { get; set; }
        public List<RegisterChargeDiscountModel> Charges { get; set; }
    }

    public class OrderItemWaiverViewModel
    {
        public int Id { get; set; }

        public string Signiture { get; set; }

        public string Text { get; set; }

        public string Name { get; set; }

        public bool Agreed { get; set; }

        public int WaiverId { get; set; }

        public WaiverConfirmationType WaiverConfirmationType { get; set; }

        public bool IsRequired { get; set; }
        public int Order { get; set; }
    }

    public class SubscriptionRegisterModel
    {
        public long ProgramId { get; set; }
        public bool EnablePriceHidden { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public SubscriptionRegisterPriceViewModel Price { get; set; }

        public List<SubscriptionRegisterDaysViewModel> Days { get; set; }

        public bool EnableDropIn { get; set; }

        public decimal DropInSessionPrice { get; set; }

        public string DropInLabel { get; set; }

        public long ScheduleId { get; set; }
    }

    public class SubscriptionRegisterPriceViewModel
    {
        public long ScheduleId { get; set; }
        public TimeOfClassFormation ScheduleMode { get; set; }

        public string ScheduleTitle { get; set; }

        public DateTime StartDate { get; set; }

        public ProgramTypeCategory ProgramType { get; set; }

        public DateTime EndDate { get; set; }

        public bool EnableDropIn { get; set; }

        public decimal DropInSessionPrice { get; set; }

        public string DropInLabel { get; set; }

        public bool EnablePunchCard { get; set; }

        public decimal PunchCardPrice { get; set; }

        public string PunchCardLabel { get; set; }
        public string MinGrade { get; set; }
        public string MaxGrade { get; set; }

        public bool EqualMaxAndMinGrade { get; set; }

        public List<ProgramTimesViewModel> Times { get; set; }

        public List<string> ComboTime { get; set; }

        public string Dates
        {
            get
            {
                return string.Format("{0} - {1}", this.StartDate.ToShortDateString(), this.EndDate.ToShortDateString());
            }
        }

        public List<SubscriptionRegisterPriceItemViewModel> Prices { get; set; }
        public bool EnablePriceHidden { get; set; }
        public List<SubscriptionChargeItemRegisterViewModel> Charges { get; set; }
    }

    public class SubscriptionRegisterPriceItemViewModel
    {
        public long Id { get; set; }

        public decimal Amount { get; set; }

        public decimal RegistrationFee { get; set; }

        public string Label { get; set; }

        public string Days { get; set; }

        public bool IsChecked { get; set; }

        public int NumberOfClassDays { get; set; }

        public int ChargeNumber { get; set; }
    }

    public class SubscriptionChargeItemRegisterViewModel
    {
        public long Id { get; set; }
        public string Label { get; set; }

        public decimal Amount { get; set; }

        public ChargeDiscountCategory Category { get; set; }
    }

    public class SubscriptionRegisterDaysViewModel
    {
        public bool IsChecked { get; set; }
        public string Title { get; set; }

        public DayOfWeek Day { get; set; }
    }

    public class SubscriptionRegisterPaymentPlanViewModel
    {
        public bool HasFullPayOption { get; set; }

        public string FullPayDiscountMessage { get; set; }

        public decimal FullTotalAmount { get; set; }

        public decimal WithDiscountFullPayAmount { get; set; }

        public DateTime DesirderStartDate { get; set; }
    }


    public class BeforeAfterCareRegisterModel
    {
        public BeforeAfterCareRegisterModel()
        {
            Prices = new List<SubscriptionRegisterPriceViewModel>();
        }

        public long ProgramId { get; set; }
        public bool EnablePriceHidden { get; set; }
        public long ScheduleId { get; set; }
        public ProgramTypeCategory ProgramType { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool EnableDropIn { get; set; }

        public decimal DropInSessionPrice { get; set; }

        public string DropInLabel { get; set; }

        public bool EnablePunchCard { get; set; }

        public decimal PunchCardPrice { get; set; }

        public string PunchCardLabel { get; set; }

        public List<SubscriptionRegisterPriceViewModel> Prices { get; set; }

        public List<SubscriptionRegisterDaysViewModel> Days { get; set; }
    }

    public class ProgramTimesViewModel
    {
        public string Day { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string MainTime { get; set; }
    }
}