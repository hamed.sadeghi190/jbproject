﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaDbException : JumbulaException
    {
        public JumbulaDbException(Exception e)
            : base(Constants.Constants.M_E_Db, e)
        {

        }
    }
}
