﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Web;
using OfficeOpenXml.FormulaParsing.ExcelUtilities;
using Org.BouncyCastle.Crypto.Tls;

namespace SportsClub.Areas.Dashboard.Models
{
    public class TroopExcelModel
    {
        public string ScoutsName { get; set; }

        public string HomePhone { get; set; }

        public string Scoutsemail { get; set; }

        public string HomeAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public int ZipCode { get; set; }

        public string FathersName { get; set; }

        public string FathersCellPhoneNumber { get; set; }

        public string FathersEmailAddress { get; set; }

        public string Mothersname { get; set; }

        public string MothersCellPhoneNumber { get; set; }

        public string MothersEmailAddress { get; set; }

        public int CurrentGrade { get; set; }

        public string CurrentSchool { get; set; }

        public DateTime DoB { get; set; }

        public long BsaId { get; set; }

        public string ScoutsCellPhoneNumber { get; set; }

        public string Vehicles { get; set; }

        public string NumberOfSeatBelts { get; set; }


        public string CollisionInsuranceCoverage { get; set; }

        public string LicensePlateNumber { get; set; }

        public string EmergencyContacts { get; set; }

        public string InsuranceCompany { get; set; }

        public string InsuranceNo { get; set; }

        public string MedicalInformation { get; set; }
    }
}