﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolsProgramsEnrollmentReportViewModel
    {
        public SchoolsProgramsEnrollmentReportViewModel()
        {

        }
        public string ProgramName { get; set; }
        public int MinimumCapacity { get; set; }
        public int Enrollment { get; set; }
    }
}