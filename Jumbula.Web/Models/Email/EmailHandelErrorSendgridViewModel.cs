﻿using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models.Email
{
    public class EmailHandelErrorSendgridViewModel
    {
        public EmailHandelErrorSendgridViewModel()
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
            ClubUrl = RootSiteUrl + "/" + ClubDomain;
        }


        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string ClubAddress { get; set; }
        public string ClubImageUrl { get; set; }
        public string ClubName { get; set; }
        public string ClubUrl { get; set; }
        public string ClubEmail { get; set; }
        public string ClubLogo { get; set; }
        public string ParentName { get; set; }
        public string ParentEmail { get; set; }
        public string TextError { get; set; }
        public string ClubPhone { get; set; }
        public bool HasPartner { get; set; }
        public string RootSiteUrl { get; set; }
        public string BodyMailTitle { get; set; }

      

    }


    
}