﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('jbContentsSettingsController', ["$scope", '$http', function ($scope, $http) {
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/JBContentsSettings", order: 2, menu: "SETTINGS" });
    }]);
})();