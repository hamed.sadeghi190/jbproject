﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum GenderCategories
    {
        [Description("Select gender")]
        None,

        [Description("Male")]
        Male,

        [Description("Female")]
        Female,

        [Description("All")]
        All,

        [Description("Gender neutral")]
        GenderNeutral,

    }
}
