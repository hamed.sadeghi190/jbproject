﻿using FluentAssertions;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.CommunicationHistory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Infrastructure;
using ClubSetting = Jumbula.Common.Constants.ClubSetting;
using Email = Jumbula.Core.Domain.Email;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class CommunicationHistoryBusinessTest
    {
        #region Fields
        private Mock<IRepository<CommunicationHistory>> _mockRepositoryCommunicationHistory;
        private Mock<IContactBusiness> _mockContactBusiness;
        private Mock<IJbDateTime> _mockJbDateTime;
        private Mock<IClubBusiness> _clubBusiness;
        private Mock<IOrderItemBusiness> _orderItemBusiness;
        private Mock<IOrderInstallmentBusiness> _orderInstallmentBusiness;
        #endregion

        #region Constants
        private const int CommunicationHistoryId = 100;
        private const int ContactId = 100;
        private const int ClubId = 10;
        private const int EmailId = 100;
        private const int SenderId = 150;
        private const string UserName = "Hamid@jumbula.com";
        private const string RelatedEntity = nameof(OrderItem);
        private const int RelatedEntityId = 101;
        private const string RemoveEmailMessage = "You cannot delete email history!";

        private static readonly DateTime FirstDeliverDate = DateTime.UtcNow.AddDays(-5);
        private static readonly DateTime SecondDeliverDate = DateTime.UtcNow.AddDays(-3);
        private static readonly DateTime FirstClickDate = DateTime.UtcNow.AddDays(-4);
        private static readonly DateTime SecondClickDate = DateTime.UtcNow.AddDays(-2);
        private static readonly DateTime FirstOpenDate = DateTime.UtcNow.AddDays(-1);
        private static readonly DateTime SecondOpenDate = DateTime.UtcNow;

        private static readonly Email Email = new Email
        {
            Id = 100,
            DateCreated = new DateTime(2018, 12, 29),
            Recipients = new List<EmailRecipient> { new EmailRecipient { DeliverDate = new DateTime(2019, 01, 05), OpenDate = new DateTime(2019, 01, 06) }, new EmailRecipient { DeliverDate = new DateTime(2019, 01, 01), OpenDate = new DateTime(2019, 01, 05) } },
            CallerType = EmailCallerType.AutoCharge
        };

        private static readonly Contact Contact = new Contact { Id = 100, CreatedDate = new DateTime(2018, 12, 01), Category = ContactCategory.Note, SenderId = SenderId, Sender = new JbUser { Id = SenderId, UserName = UserName }, Note = string.Empty };

        private static readonly PaginationModel PaginationModel = new PaginationModel { Page = 1, PageSize = 5, Skip = 0, Sort = new List<Sort>(), Take = 10 };
        private static readonly CommunicationHistoryFilterViewModel CommunicationHistoryFilterViewModel = new CommunicationHistoryFilterViewModel();
        #endregion

        #region Test methods

        [TestMethod]
        public void Get_MappingEmailData_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockRepositoryCommunicationHistory.Setup(x => x.Get(CommunicationHistoryId)).Returns(GetCommunicationHistoryEmail);

            // Act
            var result = service.Get(CommunicationHistoryId, ClubId);

            // Assert
            result.Date.Should().Be(Email.DateCreated.ToString(Constants.DefaultDateTimeFormatWithAMPMWithoutComma));
            result.Category.Should().Be(ContactCategory.Email);
            result.Sender.Should().Be(Email.CallerType.ToString());
            result.DeliverDateTime.Should().Be(new DateTime(2019, 01, 01).ToString(Constants.DefaultDateTimeFormatWithAMPMWithoutComma));
            result.OpenDateTime.Should().Be(new DateTime(2019, 01, 05).ToString(Constants.DefaultDateTimeFormatWithAMPMWithoutComma));
            result.EmailId.Should().Be(EmailId);
            result.ContactId.Should().BeNull();
            result.RelatedEntity.Should().Be(RelatedEntity);
            result.RelatedEntityId.Should().Be(RelatedEntityId);
        }

        [TestMethod]
        public void Get_MappingContactData_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();

            _mockRepositoryCommunicationHistory.Setup(x => x.Get(CommunicationHistoryId)).Returns(GetCommunicationHistoryContact);

            // Act
            var result = service.Get(CommunicationHistoryId, ClubId);

            // Assert
            result.Date.Should().Be(Contact.CreatedDate.ToString(Constants.DefaultDateTimeFormatWithAMPMWithoutComma));
            result.Category.Should().Be(ContactCategory.Note);
            result.Sender.Should().Be(Contact.Sender.UserName);
            result.DeliverDateTime.Should().BeNullOrEmpty();
            result.OpenDateTime.Should().BeNullOrEmpty();
            result.EmailId.Should().BeNull();
            result.ContactId.Should().Be(Contact.Id);
            result.RelatedEntity.Should().Be(RelatedEntity);
            result.RelatedEntityId.Should().Be(RelatedEntityId);
        }

        [TestMethod]
        public void GetListOfContact_DoNotExistContactId_ShouldReturnValidCount()
        {
            // Arrange
            var service = CreateService();

            _mockRepositoryCommunicationHistory.Setup(x => x.GetList()).Returns(FillCommunicationHistoriesWithDifferentContactId().AsQueryable);

            // Act
            var result = service.GetListOfContact(ContactId);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetListOfContact_GetListOfHistory_ShouldReturnValidCount()
        {
            // Arrange
            var service = CreateService();

            _mockRepositoryCommunicationHistory.Setup(x => x.GetList()).Returns(FillCommunicationHistories().AsQueryable);

            // Act
            var result = service.GetListOfContact(ContactId);

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void GetListOfEmail_DoNotExistEmailId_ShouldReturnZeroCount()
        {
            // Arrange
            var service = CreateService();

            _mockRepositoryCommunicationHistory.Setup(x => x.GetList()).Returns(FillCommunicationHistoriesWithDifferentEmailId().AsQueryable);

            // Act
            var result = service.GetListOfEmail(EmailId);

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetListOfEmail_GetListOfHistory_ShouldReturnValidCount()
        {
            // Arrange
            var service = CreateService();

            _mockRepositoryCommunicationHistory.Setup(x => x.GetList()).Returns(FillCommunicationHistories().AsQueryable);

            // Act
            var result = service.GetListOfEmail(EmailId);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), RemoveEmailMessage)]
        public void Delete_EmailIdHasValue_ShouldReturnExceptionError()
        {
            // Arrange
            var service = CreateService();

            var getCommunicationHistoryEmail = GetCommunicationHistoryEmail();
            _mockRepositoryCommunicationHistory.Setup(x => x.Get(getCommunicationHistoryEmail.Id)).Returns(getCommunicationHistoryEmail);

            // Act
            service.Delete(getCommunicationHistoryEmail.Id);

            // Assert
        }

        [TestMethod]
        public void Delete_ContactHistoriesCountIsZero_ShouldReturnExceptionError()
        {
            // Arrange
            var service = CreateService();

            var getCommunicationHistoryContact = GetCommunicationHistoryContact();
            _mockRepositoryCommunicationHistory.Setup(x => x.Get(getCommunicationHistoryContact.Id)).Returns(getCommunicationHistoryContact);
            _mockRepositoryCommunicationHistory.Setup(x => x.Delete(getCommunicationHistoryContact)).Returns(new OperationStatus { Status = true });

            // Act
            var result = service.Delete(getCommunicationHistoryContact.Id);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void Delete_ContactHistoriesCountIsNotZero_ShouldReturnExceptionError()
        {
            // Arrange
            var service = CreateService();

            var getCommunicationHistoryContact = GetContactWithMultipleCommunicationHistory().Histories.First();
            _mockRepositoryCommunicationHistory.Setup(x => x.Get(getCommunicationHistoryContact.Id)).Returns(getCommunicationHistoryContact);
            _mockRepositoryCommunicationHistory.Setup(x => x.Delete(getCommunicationHistoryContact)).Returns(new OperationStatus { Status = true });

            // Act
            var result = service.Delete(getCommunicationHistoryContact.Id);

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void Save_communicationHistoryViewModelIdIsZero_ShouldReturnTrue()
        {
            // Arrange
            var service = CreateService();

            var getCommunicationHistoryContact = GetCommunicationHistoryContact();
            _mockRepositoryCommunicationHistory.Setup(x => x.Create(It.Is<CommunicationHistory>(y => y.ContactId == getCommunicationHistoryContact.ContactId && y.EmailId == getCommunicationHistoryContact.EmailId && y.RelatedEntity == getCommunicationHistoryContact.RelatedEntity && y.RelatedEntityId == getCommunicationHistoryContact.RelatedEntityId))).Returns(new OperationStatus { Status = true });
            _mockJbDateTime.Setup(x => x.UtcNow).Returns(new DateTime(2018, 12, 01));

            // Act
            var result = service.Save(GetCreationCommunicationHistoryViewModel());

            // Assert

            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void Save_communicationHistoryViewModelIdIsNotZero_ShouldReturnTrue()
        {
            // Arrange
            var service = CreateService();

            var getCommunicationHistoryContact = GetCommunicationHistoryUpdateContact();

            _mockRepositoryCommunicationHistory.Setup(x => x.Get(CommunicationHistoryId)).Returns(getCommunicationHistoryContact);

            _mockRepositoryCommunicationHistory.Setup(x => x.Update(It.Is<CommunicationHistory>(y => y.ContactId == getCommunicationHistoryContact.ContactId && y.EmailId == getCommunicationHistoryContact.EmailId && y.RelatedEntity == getCommunicationHistoryContact.RelatedEntity && y.RelatedEntityId == getCommunicationHistoryContact.RelatedEntityId))).Returns(new OperationStatus { Status = true });

            // Act
            var result = service.Save(GetUpdateCommunicationHistoryViewModel());

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void GetViewHistoryList_RelatedEntityIdIsDifference_ShouldCorrectCount()
        {
            // Arrange
            var service = CreateService();

            CommunicationHistoryFilterViewModel.RelatedEntityId = 56;
            CommunicationHistoryFilterViewModel.RelatedEntity = RelatedEntity;

            _mockRepositoryCommunicationHistory.Setup(x => x.GetList()).Returns(FillCommunicationHistories().AsQueryable);

            // Act
            var result = service.GetViewHistoryList(PaginationModel, CommunicationHistoryFilterViewModel, ClubId);

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public void GetViewHistoryList_RelatedEntityIsDifference_ShouldCorrectCount()
        {
            // Arrange
            var service = CreateService();

            CommunicationHistoryFilterViewModel.RelatedEntityId = RelatedEntityId;
            CommunicationHistoryFilterViewModel.RelatedEntity = nameof(ClubSetting);

            _mockRepositoryCommunicationHistory.Setup(x => x.GetList()).Returns(FillCommunicationHistories().AsQueryable);

            // Act
            var result = service.GetViewHistoryList(PaginationModel, CommunicationHistoryFilterViewModel, ClubId);

            // Assert
            result.Count.Should().Be(0);
        }

        [TestMethod]
        public void GetViewHistoryList_GetValidData_ShouldCorrectCount()
        {
            // Arrange
            var service = CreateService();

            CommunicationHistoryFilterViewModel.RelatedEntityId = RelatedEntityId;
            CommunicationHistoryFilterViewModel.RelatedEntity = RelatedEntity;

            _mockRepositoryCommunicationHistory.Setup(x => x.GetList()).Returns(FillCommunicationHistories().AsQueryable);

            // Act
            var result = service.GetViewHistoryList(PaginationModel, CommunicationHistoryFilterViewModel, ClubId);

            // Assert
            result.Count.Should().Be(4);
            result[0].DeliverDateTime.Should().Be(FirstDeliverDate.ToString(Constants.DefaultDateTimeFormatWithAMPMWithoutComma));
            result[0].OpenDateTime.Should().Be(FirstOpenDate.ToString(Constants.DefaultDateTimeFormatWithAMPMWithoutComma));
        }

        #endregion

        #region Private methods
        private CommunicationHistoryBusiness CreateService()
        {
            _mockRepositoryCommunicationHistory = new Mock<IRepository<CommunicationHistory>>();
            _mockContactBusiness = new Mock<IContactBusiness>();
            _mockJbDateTime = new Mock<IJbDateTime>();
            _clubBusiness = new Mock<IClubBusiness>();
            _orderItemBusiness = new Mock<IOrderItemBusiness>();
            _orderInstallmentBusiness = new Mock<IOrderInstallmentBusiness>();

            return new CommunicationHistoryBusiness(_mockRepositoryCommunicationHistory.Object, _mockContactBusiness.Object, _mockJbDateTime.Object, _clubBusiness.Object, _orderItemBusiness.Object, _orderInstallmentBusiness.Object);
        }

        private static CommunicationHistory GetCommunicationHistoryEmail() => new CommunicationHistory
        {
            ContactId = null,
            EmailId = EmailId,
            Email = Email,
            RelatedEntity = RelatedEntity,
            RelatedEntityId = RelatedEntityId
        };

        private static CommunicationHistory GetCommunicationHistoryContact() => new CommunicationHistory
        {
            ContactId = ContactId,
            Contact = Contact,
            EmailId = null,
            RelatedEntity = RelatedEntity,
            RelatedEntityId = RelatedEntityId
        };

        private static Contact GetContactWithMultipleCommunicationHistory()
        {
            var contact = Contact;

            contact.Histories = new List<CommunicationHistory>
            {
                new CommunicationHistory
                {
                    ContactId = ContactId, Contact = Contact, EmailId = null, RelatedEntity = RelatedEntity,
                    RelatedEntityId = RelatedEntityId
                },
                new CommunicationHistory
                {
                    ContactId = ContactId+2, Contact = Contact, EmailId = null, RelatedEntity = RelatedEntity,
                    RelatedEntityId = RelatedEntityId+2
                }
            };

            return contact;

        }


        private static CommunicationHistory GetCommunicationHistoryUpdateContact() => new CommunicationHistory
        {
            Id = CommunicationHistoryId,
            ContactId = ContactId,
            Contact = Contact,
            EmailId = null,
            RelatedEntity = RelatedEntity,
            RelatedEntityId = RelatedEntityId
        };

        private static CommunicationHistoryViewModel GetCreationCommunicationHistoryViewModel() => new CommunicationHistoryViewModel
        {
            Id = 0,
            Category = ContactCategory.Note,
            Note = string.Empty,
            ContactId = ContactId,
            RelatedEntity = RelatedEntity,
            RelatedEntityId = RelatedEntityId,
            SenderId = SenderId,
            Date = new DateTime(2018, 12, 01).ToString(Constants.DefaultDateTimeFormatWithAMPMWithoutComma)
            //Sender = new JbUser { Id = SenderId }
        };

        private static CommunicationHistoryViewModel GetUpdateCommunicationHistoryViewModel() => new CommunicationHistoryViewModel
        {
            Id = CommunicationHistoryId,
            Category = ContactCategory.Note,
            Note = string.Empty,
            ContactId = ContactId,
            RelatedEntity = RelatedEntity,
            RelatedEntityId = RelatedEntityId,
            SenderId = SenderId
        };

        private static IEnumerable<CommunicationHistory> FillCommunicationHistories()
        {
            return new List<CommunicationHistory>{
                new CommunicationHistory{Email=new Email{Id = EmailId, DateCreated= new DateTime(2019,01,01), Recipients = new List<EmailRecipient> {FillEmailRecipient(SecondClickDate,SecondDeliverDate,SecondOpenDate),FillEmailRecipient(FirstClickDate, FirstDeliverDate, FirstOpenDate) }},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId,EmailId = EmailId},
                new CommunicationHistory{Contact=new Contact{Id = ContactId,Category=ContactCategory.Note,CreatedDate=new DateTime(2018,12,29), SenderId = 10, Sender = new JbUser{ Id = 10, UserName = "Hamid@Jumbula.com"}},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId,ContactId = ContactId},
                new CommunicationHistory{Contact=new Contact{Id=ContactId,Category=ContactCategory.Phone,CreatedDate=new DateTime(2018,12,29), SenderId = 12, Sender = new JbUser{ Id = 12, UserName = "Hamed@Jumbula.com"}},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId,ContactId = ContactId},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Meeting,CreatedDate=new DateTime(2018,12,29), SenderId = 10, Sender = new JbUser{ Id = 10, UserName = "Hamid@Jumbula.com"}},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId},
                new CommunicationHistory{Email=new Email{ DateCreated= new DateTime(2019,01,01), Recipients = new List<EmailRecipient> {FillEmailRecipient(SecondClickDate,SecondDeliverDate,SecondOpenDate),FillEmailRecipient(null, null, null) }},RelatedEntity="Order" , RelatedEntityId=RelatedEntityId},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Note,CreatedDate=new DateTime(2018,12,29), SenderId = 12, Sender = new JbUser{ Id = 12, UserName = "Hamed@Jumbula.com"}},RelatedEntity=RelatedEntity , RelatedEntityId=200},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Note,CreatedDate=new DateTime(2018,12,29)},RelatedEntity="Order" , RelatedEntityId=RelatedEntityId},
            };
        }

        private static IEnumerable<CommunicationHistory> FillCommunicationHistoriesWithDifferentContactId()
        {
            return new List<CommunicationHistory>{
                new CommunicationHistory{Email=new Email{Id = EmailId, DateCreated= new DateTime(2019,01,01)},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId,EmailId = EmailId},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Meeting,CreatedDate=new DateTime(2018,12,29)},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId},
                new CommunicationHistory{Email=new Email{ DateCreated= new DateTime(2019,01,01)},RelatedEntity="Order" , RelatedEntityId=RelatedEntityId},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Note,CreatedDate=new DateTime(2018,12,29)},RelatedEntity=RelatedEntity , RelatedEntityId=200},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Note,CreatedDate=new DateTime(2018,12,29)},RelatedEntity="Order" , RelatedEntityId=RelatedEntityId},
            };
        }

        private static IEnumerable<CommunicationHistory> FillCommunicationHistoriesWithDifferentEmailId()
        {
            return new List<CommunicationHistory>{
                new CommunicationHistory{Contact=new Contact{Id = ContactId,Category=ContactCategory.Note,CreatedDate=new DateTime(2018,12,29)},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId,ContactId = ContactId},
                new CommunicationHistory{Contact=new Contact{Id=ContactId,Category=ContactCategory.Phone,CreatedDate=new DateTime(2018,12,29)},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId,ContactId = ContactId},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Meeting,CreatedDate=new DateTime(2018,12,29)},RelatedEntity=RelatedEntity , RelatedEntityId=RelatedEntityId},
                new CommunicationHistory{Email=new Email{ DateCreated= new DateTime(2019,01,01)},RelatedEntity="Order" , RelatedEntityId=RelatedEntityId},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Note,CreatedDate=new DateTime(2018,12,29)},RelatedEntity=RelatedEntity , RelatedEntityId=200},
                new CommunicationHistory{Contact=new Contact{Category=ContactCategory.Note,CreatedDate=new DateTime(2018,12,29)},RelatedEntity="Order" , RelatedEntityId=RelatedEntityId},
            };
        }

        private static EmailRecipient FillEmailRecipient(DateTime? clickDate, DateTime? deliverDate, DateTime? openDate)
        {
            return new EmailRecipient
            {
                ClickDate = clickDate,
                DeliverDate = deliverDate,
                OpenDate = openDate
            };
        }

        #endregion 
    }
}
