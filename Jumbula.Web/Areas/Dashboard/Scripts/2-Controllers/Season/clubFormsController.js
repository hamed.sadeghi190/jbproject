﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('clubFormsController', ['$scope', function ($scope) {
        window.app.globalObjects.pageClass = 'forms_View';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/ClubForms", order: 2, menu: "TOOLBOX" });
    }]);
})();