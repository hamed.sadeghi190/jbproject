﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Helper;
using Jumbula.Core.Model.Generic;

namespace Jumbula.Web.Mvc.UIHelpers
{
    public static class DropdownHelpers
    {
        public static List<SelectKeyValue<string>> ToOrderedSelectList<T>(string defaultItemTitle, string defaultItemValue) where T : struct, IComparable, IFormattable, IConvertible
        {
            var values = typeof(T).GetWithOrder().Select(g =>
                new SelectKeyValue<string>
                {
                    Text = g.Text,
                    Value = g.Value,

                }).ToList();


            values.Insert(0, new SelectKeyValue<string> { Text = defaultItemTitle, Value = defaultItemValue });

            return values;
        }
        public static List<SelectKeyValue<string>> ToSelectList<T>(string defaultItem = null) where T : struct, IComparable, IFormattable, IConvertible
        {
            var values = Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(d => new SelectKeyValue<string> { Text = EnumHelper.GetEnumDescription(d), Value = d.ToString() })
                .ToList();

            if (!string.IsNullOrEmpty(defaultItem))
            {
                values.Insert(0, new SelectKeyValue<string> { Text = defaultItem, Value = defaultItem.Trim().Replace(" ", string.Empty) });
            }

            return values;
        }

        public static List<SelectKeyValue<string>> ToSelectList<T>(string defaultItemTitle, string defaultItemValue) where T : struct, IComparable, IFormattable, IConvertible
        {

            var values = Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(d => new SelectKeyValue<string> { Text = Jumbula.Common.Helper.EnumHelper.GetEnumDescription(d), Value = d.ToString() })
                .ToList();

            values.Insert(0, new SelectKeyValue<string> { Text = defaultItemTitle, Value = defaultItemValue });

            return values;
        }

        public static List<SelectKeyValue<int>> ToSelectListWithIntValue<T>(string defaultItem = null, int defaultId = 0) where T : struct, IComparable, IFormattable, IConvertible
        {

            var values = Enum.GetValues(typeof(T))
                .Cast<T>()
                .Select(d => new SelectKeyValue<int> { Text = Jumbula.Common.Helper.EnumHelper.GetEnumDescription(d), Value = Convert.ToInt32(d) })
                .ToList();

            if (!string.IsNullOrEmpty(defaultItem))
            {
                values.Insert(0, new SelectKeyValue<int> { Text = defaultItem, Value = defaultId });
            }

            return values;
        }

        public static List<SelectKeyValue<int>> SeedKeyValueNumbers(int max)
        {
            var result = new List<SelectKeyValue<int>>();

            for (int i = 0; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<int> { Text = i.ToString(), Value = i });
            }

            return result;
        }

        public static List<SelectKeyValue<string>> SeedTimeZones()
        {
            var result = new List<SelectKeyValue<string>>
            {
                new SelectKeyValue<string>() {Text = "Select the organization time zone", Value = "-1"}
            };

            foreach (Jumbula.Common.Enums.TimeZone type in Enum.GetValues(typeof(Jumbula.Common.Enums.TimeZone)))
            {
                result.Add(new SelectKeyValue<string>() { Text = type.ToDescription(), Value = ((int)type).ToString() });
            }

            return result;
        }
    }
}