﻿using System;
using System.Collections.Generic;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Model;

namespace SportsClub.Models.Api
{
   


    public class ProgramInfoServiceModel
    {
        public long id { get; set; }
        public string title { get; set; }
        public string domain { get; set; }
        public string clubDomain { get; set; }
        public string seasonDomain { get; set; }
        public DateTime? start { get; set; }
        public DateTime? end { get; set; }
        public string periodDate
        {
            get
            {
                return ((start.HasValue ? start.Value.ToString("MMM dd") : string.Empty) + " - " + (end.HasValue ? end.Value.ToString("MMM dd") : string.Empty));
            }
        }
        public string url
        {
            get
            {
                return string.Format("{0}/{1}", seasonDomain, domain);
            }
        }

        public string location { get; set; }

    }

    public class ChessTourneyInfoServiceModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string domain { get; set; }
        public string clubDomain { get; set; }
        public DateTime? start { get; set; }
        public DateTime? end { get; set; }
        public string url
        {
            get
            {
                return string.Format("{0}/{1}", clubDomain, domain);
            }
        }

    }

    public class TourneyCoverageServiceModel
    {
        public int Id { get; set; }

        public int RoundNo { get; set; }

        public TourneyCoverageType CoverageType { get; set; }

        public string DataUrl { get; set; }

        public string SectionName { get; set; }
    }
}