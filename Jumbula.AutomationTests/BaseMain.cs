﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace Jumbula.AutomationTests
{
    public class BaseMain
    {
        public BaseMain()
        {

        }

        public BaseMain(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        private IWebDriver _webDriver;
        private string _baseUrl;
        private Browser _browser;


        public IWebDriver WebDriver
        {
            get
            {
                if (_webDriver == null)
                {
                    _webDriver = GetWebDriver(Browser);
                }

                return _webDriver;
            }
            set
            {
                _webDriver = value;
            }

        }

        public string BaseUrl
        {
            get
            {
                return _baseUrl;
            }
            set
            {
                _baseUrl = value;
            }
        }

        public Browser Browser
        {
            get
            {
                return _browser;
            }
            set
            {
                _browser = value;
            }
        }

        protected void ChangeBrowser(Browser browser)
        {
            _browser = browser;
            _webDriver = GetWebDriver(browser);
        }

        public IWebDriver GetWebDriver(Browser browser)
        {
            switch (browser)
            {
                case Browser.Chrome:
                    {
                        return new ChromeDriver();
                    }
                case Browser.FireFox:
                    {
                        return new FirefoxDriver();
                    }
                case Browser.IE:
                    {
                        return new InternetExplorerDriver();
                    }
                default:
                    break;
            }

            return null;
        }
    }

    public enum Browser
    {
        Chrome,
        FireFox,
        IE
    }
}
