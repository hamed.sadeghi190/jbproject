﻿using System;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public interface IProgramBodyViewModel
    {
        string Description { get; set; }

        DateTime? StartDate { get; set; }

        DateTime? EndDate { get; set; }

        PostalAddress Address { get; set; }

        string LocationName { get; set; }

        string ClubLogo { get; set; }

        Jumbula.Common.Enums.TimeZone ClubTimeZone { get; set; }

        bool DisableCapacityRestriction { get; set; }
    }
}
