﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Jumbula.AutomationTests.Tests
{
    class SchoolDistrictTest: BaseTest<SchoolDistrictElements>
    {
        public SchoolDistrictTest(IWebDriver webDriver)
            :base(webDriver)
        {
        }

        public void View()
        {
            Assert.AreEqual("School Districts", Elements.PageTitle.Text);

            #region AssertRow1
            Assert.AreEqual("School district2", Elements.Row1Name.Text);
            Assert.AreEqual("schooldistrict2", Elements.Row1Domain.Text);
            Assert.AreEqual("4551 Hwy 6, De Leon, TX 76444, USA", Elements.Row1Address.Text);
            #endregion AsserRow1
            #region AssertRow2
            Assert.AreEqual("School district1", Elements.Row2Name.Text);
            Assert.AreEqual("schooldistrict1", Elements.Row2Domain.Text);
            Assert.AreEqual("Sta Fe 3496, S2002KUF Rosario, Santa Fe, Argentina", Elements.Row2Address.Text);
            #endregion AsserRow2
        }
        public void Filetr()
        {
            Elements.SearchTerm.SendKeys("School district1");
            Elements.SearchButton.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("School district1", Elements.Row1Name.Text);
            Assert.IsFalse(Elements.Row2Exist);
        }
        public void Edit()
        {
            Elements.Row2Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.Row2ViewEditActions.Click();
            ExtraAPI.Wait(10000);

            #region Check the old data

            // Basic info
            Assert.AreEqual("schooldistrict1", Elements.Domain.GetAttribute("value"));
            Assert.AreEqual("School district1", Elements.Name.GetAttribute("value"));
            Assert.AreEqual("Sta Fe 3496, S2002KUF Rosario, Santa Fe, Argentina", Elements.Address.GetAttribute("value"));
            Assert.AreEqual("true", Elements.UTCOption.GetAttribute("selected"));
            // Contact info
            Assert.AreEqual("School district1 Superintendent name", Elements.SuperintendentName.GetAttribute("value"));
            Assert.AreEqual("schooldistrict1@jumbula.com", Elements.SuperintendentEmail.GetAttribute("value"));
            Assert.AreEqual("2455782485", Elements.SuperintendentPhone.GetAttribute("value"));
            Assert.AreEqual("true", Elements.AreaManagerAreaManagerStaffOption.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.RegionalDirectorRegionalDirectorStaffOption.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.SalesContactSalesContactStaffOption.GetAttribute("selected"));
            // Additional info
            Assert.AreEqual("true", Elements.StaffingRatioOption11.GetAttribute("selected"));
            Assert.AreEqual("123", Elements.NCESID.GetAttribute("value"));
            Assert.AreEqual("school district1 revenue share", Elements.RevenueShare.GetAttribute("value"));
            // Subsidy row1
            Assert.AreEqual("Subsidy1", Elements.SubsidiesRow1Name.Text);
            Assert.AreEqual("Delaware", Elements.SubsidiesRow1State.Text);
            Assert.AreEqual("www.subsidy1.com", Elements.SubsidiesRow1Website.Text);
            // Subsidy row2
            Assert.AreEqual("Subsidy2", Elements.SubsidiesRow2Name.Text);
            Assert.AreEqual("Florida", Elements.SubsidiesRow2State.Text);
            Assert.AreEqual("www.subsidy2.com", Elements.SubsidiesRow2Website.Text);
            // Subsidy row3
            Assert.AreEqual("Subsidy5", Elements.SubsidiesRow3Name.Text);
            Assert.AreEqual("District of Columbia", Elements.SubsidiesRow3State.Text);
            Assert.AreEqual("www.subsidy5.com", Elements.SubsidiesRow3Website.Text);
            Assert.IsFalse(Elements.SubsidiesRow4Exist);
            // Schoo1 row1
            Assert.AreEqual("Hayfield Elementary PTA", Elements.SchoolsRow1Name.Text);
            Assert.AreEqual("jumbula-hayfield", Elements.SchoolsRow1Domain.Text);
            Assert.AreEqual("1230", Elements.SchoolsRow1NCESID.Text);
            Assert.AreEqual("4560", Elements.SchoolsRow1ProviderID1.Text);
            Assert.AreEqual("7890", Elements.SchoolsRow1ProviderID2.Text);
            // Schoo1 row2
            Assert.AreEqual("Barrett Elementary PTA", Elements.SchoolsRow2Name.Text);
            Assert.AreEqual("jumbula-barrett", Elements.SchoolsRow2Domain.Text);
            Assert.AreEqual("987", Elements.SchoolsRow2NCESID.Text);
            Assert.AreEqual("0654", Elements.SchoolsRow2ProviderID1.Text);
            Assert.AreEqual("0321", Elements.SchoolsRow2ProviderID2.Text);
            // Schoo1 row3
            Assert.AreEqual("Ashland Elementary PTO", Elements.SchoolsRow3Name.Text);
            Assert.AreEqual("jumbula-ashland", Elements.SchoolsRow3Domain.Text);
            Assert.AreEqual("5678", Elements.SchoolsRow3NCESID.Text);
            Assert.AreEqual("14534", Elements.SchoolsRow3ProviderID1.Text);
            Assert.AreEqual("2223", Elements.SchoolsRow3ProviderID2.Text);
            Assert.IsFalse(Elements.SchoolsRow4Exist);
            #endregion Check the old data

            #region  Enter the new data

            // Basic info
            Elements.Domain.Clear();
            Elements.Domain.SendKeys("schooldistrict1Changed");
            Elements.Name.Clear();
            Elements.Name.SendKeys("School district1 changed");
            Elements.TimeZone.Click();
            ExtraAPI.Wait(1000);
            Elements.PSTOption.Click();
            ExtraAPI.Wait(1000);
            Elements.Address.Clear();
            Elements.Address.SendKeys("45 Bay Street, Toronto, ON, Canada");
            ExtraAPI.Wait(1000);
            Elements.Domain.Click();
            // Contact info
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.AreaManagerList);
            actions.Perform();
            Elements.SuperintendentName.Clear();
            Elements.SuperintendentName.SendKeys("School district1 Superintendent name changed");
            Elements.SuperintendentEmail.Clear();
            Elements.SuperintendentEmail.SendKeys("schooldistrict1changed@jumbula.com");
            Elements.SuperintendentPhone.Clear();
            Elements.SuperintendentPhone.SendKeys("1234567890");
            actions.MoveToElement(Elements.NCESID);
            actions.Perform();

            Elements.AreaManagerList.Click();
            ExtraAPI.Wait(1000);
            Elements.AreaManagerActiveStaffOption.Click();
            ExtraAPI.Wait(1000);

            Elements.RegionalDirectorList.Click();
            ExtraAPI.Wait(1000);
            Elements.RegionalDirectoradminStaffOption.Click();
            ExtraAPI.Wait(1000);

            Elements.SalesContactList.Click();
            ExtraAPI.Wait(1000);
            Elements.SalesContactPartnerStaffOption.Click();
            ExtraAPI.Wait(1000);

            // Additional info
            actions.MoveToElement(Elements.AddSubsidyBtn);
            actions.Perform();
            Elements.NCESID.Clear();
            Elements.NCESID.SendKeys("742");
            Elements.RevenueShare.Clear();
            Elements.RevenueShare.SendKeys("school district1 revenue share changed");
            Elements.StaffingRatio.Click();
            Elements.StaffingRatioOption12.Click();

            // Edit subsidies
            actions.MoveToElement(Elements.AddSchoolBtn);
            actions.Perform();

            //Delete subsidy2 from drop down list
            Elements.AddSubsidyBtn.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenResultsSubsidy2Delete.Click();
            ExtraAPI.Wait(1000);
            Elements.SaveSubsidyBtn.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Subsidy1", Elements.SubsidiesRow1Name.Text);
            Assert.AreEqual("Subsidy5", Elements.SubsidiesRow2Name.Text);
            Assert.IsFalse(Elements.SubsidiesRow3Exist);

            // Delete subsidy1 from grid
            Elements.SubsidiesRow1Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.SubsidiesRow1DeleteActions.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Subsidy5", Elements.SubsidiesRow1Name.Text);
            Assert.IsFalse(Elements.SubsidiesRow2Exist);

            // Add subsidy4 from drop down list
            Elements.AddSubsidyBtn.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenResultsSubsidy2.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenChoicesSubsidy4.Click();
            ExtraAPI.Wait(1000);
            Elements.SaveSubsidyBtn.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Subsidy5", Elements.SubsidiesRow1Name.Text);
            Assert.AreEqual("Subsidy4", Elements.SubsidiesRow2Name.Text);
            Assert.IsFalse(Elements.SubsidiesRow3Exist);

            // Edit schools
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");

            // Delete 2nd school from drop down list
            Elements.AddSchoolBtn.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenResultsSchool2Delete.Click();
            ExtraAPI.Wait(1000);
            Elements.SaveSchoolBtn.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.SchoolsRow1Name.Text);
            Assert.AreEqual("Ashland Elementary PTO", Elements.SchoolsRow2Name.Text);
            Assert.IsFalse(Elements.SchoolsRow3Exist);

            //Delete school1 from grid
            Elements.SchoolsRow1Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.SchoolsRow1Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.SchoolsRow1DeleteAction.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Ashland Elementary PTO", Elements.SchoolsRow1Name.Text);
            Assert.IsFalse(Elements.SchoolsRow2Exist);

            // Add school4 from drop down list
            Elements.AddSchoolBtn.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenResultsSchool2.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenChoicesSchool4.Click();
            ExtraAPI.Wait(1000);
            Elements.SaveSchoolBtn.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Claremont Immersion School PTA", Elements.SchoolsRow1Name.Text);
            Assert.AreEqual("Ashland Elementary PTO", Elements.SchoolsRow2Name.Text);
            Assert.IsFalse(Elements.SchoolsRow3Exist);

            // Edit provider id for school1
            Elements.SchoolsRow1Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.SchoolsRow1Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.SchoolsRow1EditProviderID1Action.Click();
            ExtraAPI.Wait(1000);
            Elements.SchoolsRow1NCESIDInput.SendKeys("556");
            Elements.SchoolsRow1ProviderID1Input.SendKeys("45646");
            Elements.SchoolsRow1ProviderID2Input.SendKeys("32424");
            Elements.SchoolsRow1Save.Click();
            Assert.AreEqual("Claremont Immersion School PTA", Elements.SchoolsRow1Name.Text);
            Assert.AreEqual("556", Elements.SchoolsRow1NCESID.Text);
            Assert.AreEqual("45646", Elements.SchoolsRow1ProviderID1.Text);
            Assert.AreEqual("32424", Elements.SchoolsRow1ProviderID2.Text);
            Assert.AreEqual("Ashland Elementary PTO", Elements.SchoolsRow2Name.Text);
            Assert.IsFalse(Elements.SchoolsRow3Exist);

            Elements.Submit.Click();
            ExtraAPI.Wait(10000);
            #endregion Enter the new data

            #region  Check the new data in the grid

            Assert.AreEqual("School district1 changed", Elements.Row2Name.Text);
            Assert.AreEqual("schooldistrict1changed", Elements.Row2Domain.Text);
            Assert.AreEqual("45 Bay St, Toronto, ON M5E 1Z8, Canada", Elements.Row2Address.Text);

            #endregion Check the new data in the grid

            #region Check the new data in edit mode

            Elements.Row2Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.Row2ViewEditActions.Click();
            ExtraAPI.Wait(10000);
            // Basic info
            Assert.AreEqual("schooldistrict1changed", Elements.Domain.GetAttribute("value"));
            Assert.AreEqual("School district1 changed", Elements.Name.GetAttribute("value"));
            Assert.AreEqual("45 Bay St, Toronto, ON M5E 1Z8, Canada", Elements.Address.GetAttribute("value"));
            Assert.AreEqual("true", Elements.PSTOption.GetAttribute("selected"));
            // Contact info
            Assert.AreEqual("School district1 Superintendent name changed", Elements.SuperintendentName.GetAttribute("value"));
            Assert.AreEqual("schooldistrict1changed@jumbula.com", Elements.SuperintendentEmail.GetAttribute("value"));
            Assert.AreEqual("true", Elements.AreaManagerActiveStaffOption.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.RegionalDirectoradminStaffOption.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.SalesContactPartnerStaffOption.GetAttribute("selected"));
            // Additional info
            Assert.AreEqual("true", Elements.StaffingRatioOption12.GetAttribute("selected"));
            Assert.AreEqual("742", Elements.NCESID.GetAttribute("value"));
            Assert.AreEqual("school district1 revenue share changed", Elements.RevenueShare.GetAttribute("value"));
            // Subsidy row1
            Assert.AreEqual("Subsidy5", Elements.SubsidiesRow1Name.Text);
            Assert.AreEqual("District of Columbia", Elements.SubsidiesRow1State.Text);
            Assert.AreEqual("www.subsidy5.com", Elements.SubsidiesRow1Website.Text);
            // Subsidy row2
            Assert.AreEqual("Subsidy4", Elements.SubsidiesRow2Name.Text);
            Assert.AreEqual("New Jersey", Elements.SubsidiesRow2State.Text);
            Assert.AreEqual("www.subsidy4.com", Elements.SubsidiesRow2Website.Text);
            // Schoo1 row1
            Assert.AreEqual("Claremont Immersion School PTA", Elements.SchoolsRow1Name.Text);
            Assert.AreEqual("556", Elements.SchoolsRow1NCESID.Text);
            Assert.AreEqual("45646", Elements.SchoolsRow1ProviderID1.Text);
            Assert.AreEqual("32424", Elements.SchoolsRow1ProviderID2.Text);
            // Schoo1 row2
            Assert.AreEqual("Ashland Elementary PTO", Elements.SchoolsRow2Name.Text);
            Assert.AreEqual("5678", Elements.SchoolsRow2NCESID.Text);
            Assert.AreEqual("14534", Elements.SchoolsRow2ProviderID1.Text);
            Assert.AreEqual("2223", Elements.SchoolsRow2ProviderID2.Text);
            // Schoo1 row3
            Assert.IsFalse(Elements.SchoolsRow3Exist);
            #endregion Check the new data in edit mode
        }
        public void Add()
        {
            #region Step1
            // Validations
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Name is required.", Elements.Name.GetAttribute("data-original-title"));
            Assert.AreEqual("Address is required.<br>", Elements.Address.GetAttribute("data-original-title"));
            Assert.AreEqual("Domain is required.", Elements.Domain.GetAttribute("data-original-title"));
            Assert.AreEqual("Time zone is required.", Elements.TimeZoneDiv.GetAttribute("data-original-title"));
            Elements.Domain.Clear();
            Elements.Domain.SendKeys("schooldistrict2");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("The domain already exists.", Elements.Domain.GetAttribute("data-original-title"));
            Elements.Domain.Clear();
            Elements.Domain.SendKeys("newdistrcit.");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Domain cannot contain any spaces or special characters such as *, &, !, _, etc", Elements.Domain.GetAttribute("data-original-title"));

            //  Enter valid data
            Elements.Name.SendKeys("New school district");
            Elements.Address.SendKeys("Avenida Corrientes 767, Buenos Aires, Argentina");
            ExtraAPI.Wait(1000);
            Elements.Name.Click();
            Elements.Domain.Clear();
            Elements.Domain.SendKeys("newSchoolDistrict");
            Elements.TimeZone.Click();
            ExtraAPI.Wait(1000);
            Elements.UTCOption.Click();
            ExtraAPI.Wait(1000);
            Elements.Submit.Click();
            ExtraAPI.Wait(10000);

            #endregion Step1
            #region Step2

            // Check data from Step1
            Assert.AreEqual("newschooldistrict", Elements.Domain.GetAttribute("value"));
            Assert.AreEqual("New school district", Elements.Name.GetAttribute("value"));
            Assert.AreEqual("Av. Corrientes 767, C1043AAH CABA, Argentina", Elements.Address.GetAttribute("value"));
            Assert.AreEqual("true", Elements.UTCOption.GetAttribute("selected"));

            // Validation
            Elements.Domain.Clear();
            Elements.Address.Clear();
            Elements.Name.Clear();
            Elements.TimeZone.Click();
            ExtraAPI.Wait(1000);
            Elements.TimeXZoneEmptyOption.Click();
            ExtraAPI.Wait(1000);
            Actions actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.AreaManagerList);
            actions.Perform();
            Elements.SuperintendentEmail.SendKeys("SuperintendentEmail");
            Elements.SuperintendentPhone.SendKeys("12345");
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Name is required.", Elements.Name.GetAttribute("data-original-title"));
            Assert.AreEqual("Address is required.<br>", Elements.Address.GetAttribute("data-original-title"));
            Assert.AreEqual("Domain is required.", Elements.Domain.GetAttribute("data-original-title"));
            Assert.AreEqual("Time zone is required.", Elements.TimeZoneDiv.GetAttribute("data-original-title"));
            Assert.AreEqual("Email is not a valid email address.", Elements.SuperintendentEmail.GetAttribute("data-original-title"));
            Assert.AreEqual("Enter a complete phone number including area code.", Elements.SuperintendentPhone.GetAttribute("data-original-title"));
            Elements.Domain.Clear();
            Elements.Domain.SendKeys("schooldistrict2");
            actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.AreaManagerList);
            actions.Perform();
            Elements.SuperintendentPhone.Clear();
            Elements.SuperintendentPhone.SendKeys("phone");
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("The domain already exists.", Elements.Domain.GetAttribute("data-original-title"));
            Assert.AreEqual("Enter a complete phone number including area code.<br>Please enter the phone number including area code (e.g. 5551239876).", Elements.SuperintendentPhone.GetAttribute("data-original-title"));
            Elements.Domain.Clear();
            Elements.Domain.SendKeys("newdistrcit.");
            actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.AreaManagerList);
            actions.Perform();
            Elements.SuperintendentPhone.Clear();
            Elements.SuperintendentPhone.SendKeys("1234567890123450");
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Domain cannot contain any spaces or special characters such as *, &, !, _, etc", Elements.Domain.GetAttribute("data-original-title"));
            Assert.AreEqual("Phone number is too long.", Elements.SuperintendentPhone.GetAttribute("data-original-title"));

            // Enter Valid data

            // Basic info
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,0)");
            Elements.Name.SendKeys("New school district");
            Elements.Address.Clear();
            Elements.Address.SendKeys("Avenida Corrientes 767, Buenos Aires, Argentina");
            ExtraAPI.Wait(1000);
            Elements.Name.Click();
            Elements.Domain.Clear();
            Elements.Domain.SendKeys("newSchoolDistrict");
            Elements.TimeZone.Click();
            ExtraAPI.Wait(1000);
            Elements.UTCOption.Click();
            ExtraAPI.Wait(1000);
            
            // Contact info
            actions = new Actions(WebDriver);
            actions.MoveToElement(Elements.AreaManagerList);
            actions.Perform();
            Elements.SuperintendentName.SendKeys("New School district Superintendent name");
            Elements.SuperintendentEmail.Clear();
            Elements.SuperintendentEmail.SendKeys("newschooldistrict@jumbula.com");
            Elements.SuperintendentPhone.Clear();
            Elements.SuperintendentPhone.SendKeys("1234567890");
            actions.MoveToElement(Elements.NCESID);
            actions.Perform();

            // Check the staffs in drop down lists for area manager
            Elements.AreaManagerList.Click();
            ExtraAPI.Wait(1000);
            Assert.IsFalse(Elements.AreaManagerPendingStaffOptionExist);
            Assert.IsFalse(Elements.AreaManagerDeletedStaffOptionExist);
            Assert.IsFalse(Elements.AreaManagerFrozenStaffOptionExist);
            Assert.IsTrue(Elements.AreaManagerActiveStaffOptionExist);
            // Select the area manager
            Elements.AreaManagerList.Click();
            ExtraAPI.Wait(1000);
            Elements.AreaManagerAreaManagerStaffOption.Click();
            ExtraAPI.Wait(1000);

            // Check the staffs in drop down lists for Regional Director
            Elements.RegionalDirectorList.Click();
            ExtraAPI.Wait(1000);
            Assert.IsFalse(Elements.RegionalDirectorPendingStaffOptionExist);
            Assert.IsFalse(Elements.RegionalDirectorDeletedStaffOptionExist);
            Assert.IsFalse(Elements.RegionalDirectorFrozenStaffOptionExist);
            Assert.IsTrue(Elements.RegionalDirectorActiveStaffOptionExist);
            // Select the Regional Director
            Elements.RegionalDirectorList.Click();
            ExtraAPI.Wait(1000);
            Elements.RegionalDirectorRegionalDirectorStaffOption.Click();
            ExtraAPI.Wait(1000);

            // Check the staffs in drop down lists for Sales Contact
            Elements.SalesContactList.Click();
            ExtraAPI.Wait(1000);
            Assert.IsFalse(Elements.SalesContactPendingStaffOptionExist);
            Assert.IsFalse(Elements.SalesContactDeletedStaffOptionExist);
            Assert.IsFalse(Elements.SalesContactFrozenStaffOptionExist);
            Assert.IsTrue(Elements.SalesContactActiveStaffOptionExist);
            // Select the Sales Contact
            Elements.SalesContactList.Click();
            ExtraAPI.Wait(1000);
            Elements.SalesContactSalesContactStaffOption.Click();
            ExtraAPI.Wait(1000);

            // Additional info
            actions.MoveToElement(Elements.AddSubsidyBtn);
            actions.Perform();
            Elements.NCESID.SendKeys("64234");
            Elements.RevenueShare.SendKeys("new school district revenue share");
            Elements.StaffingRatio.Click();
            Elements.StaffingRatioOption12.Click();

            // Edit subsidies
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,3000)");
            // Check subsidy list
            Elements.AddSubsidyBtn.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenResultsSubsidy1.Click();
            ExtraAPI.Wait(1000);
            var subsidy1 = Elements.ChosenChoicesSubsidy1.Text;
            var subsidy2 = Elements.ChosenChoicesSubsidy2.Text;
            var subsidy3 = Elements.ChosenChoicesSubsidy3.Text;
            var subsidy4 = Elements.ChosenChoicesSubsidy4.Text;
            var subsidy5 = Elements.ChosenChoicesSubsidy5.Text;

            Assert.AreEqual("Subsidy1", subsidy1);
            Assert.AreEqual("Subsidy2", subsidy2);
            Assert.AreEqual("Subsidy3", subsidy3);
            Assert.AreEqual("Subsidy4", subsidy4);
            Assert.AreEqual("Subsidy5", subsidy5);

            // Add subsidy1, subsidy5 from drop down list
            Elements.ChosenChoicesSubsidy1.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenResultsSubsidy2.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenChoicesSubsidy5.Click();
            ExtraAPI.Wait(1000);
            Elements.SaveSubsidyBtn.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Subsidy1", Elements.SubsidiesRow1Name.Text);
            Assert.AreEqual("Subsidy5", Elements.SubsidiesRow2Name.Text);
            Assert.IsFalse(Elements.SubsidiesRow3Exist);

            // Edit Schools
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,3000)");
            // Check school list
            Elements.AddSchoolBtn.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenResultsSchool1.Click();
            ExtraAPI.Wait(1000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,3000)");
            var School1 = Elements.ChosenChoicesSchool1.Text;
            var School2 = Elements.ChosenChoicesSchool2.Text;
            var School3 = Elements.ChosenChoicesSchool3.Text;
            var School4 = Elements.ChosenChoicesSchool4.Text;
            var School5 = Elements.ChosenChoicesSchool5.Text;
            var School6 = Elements.ChosenChoicesSchool6.Text;
            var School7 = Elements.ChosenChoicesSchool7.Text;
            
            Assert.AreEqual("Ashlawn Elementary PTA", School1);
            Assert.AreEqual("Claremont Immersion School PTA", School2);
            Assert.AreEqual("Discovery Elementary PTA", School3);
            Assert.AreEqual("Jamestown Elementary PTA", School4);
            Assert.AreEqual("John F. Pattie, Sr. Elementary PTCO", School5);
            Assert.AreEqual("McKinley Elementary PTA", School6);
            Assert.AreEqual("Oakton Elementary PTA", School7);
            

            // Add school1, school4 from drop down list
            Elements.ChosenChoicesSchool1.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenResultsSchool2.Click();
            ExtraAPI.Wait(1000);
            Elements.ChosenChoicesSchool4.Click();
            ExtraAPI.Wait(1000);
            Elements.SaveSchoolBtn.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Ashlawn Elementary PTA", Elements.SchoolsRow1Name.Text);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.SchoolsRow2Name.Text);
            Assert.IsFalse(Elements.SubsidiesRow3Exist);

            // Enter provider id for school1
            Elements.SchoolsRow1Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.SchoolsRow1Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.SchoolsRow1EditProviderID1Action.Click();
            ExtraAPI.Wait(1000);
            Elements.SchoolsRow1NCESIDInput.SendKeys("556");
            Elements.SchoolsRow1ProviderID1Input.SendKeys("45646");
            Elements.SchoolsRow1ProviderID2Input.SendKeys("32424");
            Elements.SchoolsRow1Save.Click();
            Assert.AreEqual("Ashlawn Elementary PTA", Elements.SchoolsRow1Name.Text);
            Assert.AreEqual("556", Elements.SchoolsRow1NCESID.Text);
            Assert.AreEqual("45646", Elements.SchoolsRow1ProviderID1.Text);
            Assert.AreEqual("32424", Elements.SchoolsRow1ProviderID2.Text);
            Assert.AreEqual("Hayfield Elementary PTA", Elements.SchoolsRow2Name.Text);
            Assert.IsFalse(Elements.SchoolsRow3Exist);

            Elements.Submit.Click();
            ExtraAPI.Wait(1000);


            #endregion Step2
            #region check the new district in the grid
            Assert.AreEqual("New school district42", Elements.Row1Name.Text);
            Assert.AreEqual("newschooldistrict42", Elements.Row1Domain.Text);
            Assert.AreEqual("Av. Corrientes 767, C1043AAH CABA, Argentina", Elements.Row1Address.Text);
            #endregion check the new district in the grid

            #region check the new district in edit mode

            Elements.Row1Actions.Click();
            ExtraAPI.Wait(1000);
            Elements.Row1ViewEditActions.Click();
            ExtraAPI.Wait(10000);

            // Basic info
            Assert.AreEqual("newschooldistrict", Elements.Domain.GetAttribute("value"));
            Assert.AreEqual("New school district", Elements.Name.GetAttribute("value"));
            Assert.AreEqual("Av. Corrientes 767, C1043AAH CABA, Argentina", Elements.Address.GetAttribute("value"));
            Assert.AreEqual("true", Elements.UTCOption.GetAttribute("selected"));
            // Contact info
            Assert.AreEqual("New School district Superintendent name", Elements.SuperintendentName.GetAttribute("value"));
            Assert.AreEqual("newschooldistrict@jumbula.com", Elements.SuperintendentEmail.GetAttribute("value"));
            Assert.AreEqual("1234567890", Elements.SuperintendentPhone.GetAttribute("value"));
            Assert.AreEqual("true", Elements.AreaManagerAreaManagerStaffOption.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.RegionalDirectorRegionalDirectorStaffOption.GetAttribute("selected"));
            Assert.AreEqual("true", Elements.SalesContactSalesContactStaffOption.GetAttribute("selected"));
            // Additional info
            Assert.AreEqual("true", Elements.StaffingRatioOption12.GetAttribute("selected"));
            Assert.AreEqual("64234", Elements.NCESID.GetAttribute("value"));
            Assert.AreEqual("new school district revenue share", Elements.RevenueShare.GetAttribute("value"));
            // Subsidy row1
            Assert.AreEqual("Subsidy1", Elements.SubsidiesRow1Name.Text);
            Assert.AreEqual("Delaware", Elements.SubsidiesRow1State.Text);
            Assert.AreEqual("www.subsidy1.com", Elements.SubsidiesRow1Website.Text);
            // Subsidy row2
            Assert.AreEqual("Subsidy5", Elements.SubsidiesRow2Name.Text);
            Assert.AreEqual("District of Columbia", Elements.SubsidiesRow2State.Text);
            Assert.AreEqual("www.subsidy5.com", Elements.SubsidiesRow2Website.Text);
            Assert.IsFalse(Elements.SubsidiesRow3Exist);
            // Schoo1 row1
            Assert.AreEqual("Ashlawn Elementary PTA", Elements.SchoolsRow1Name.Text);
            Assert.AreEqual("jumbula-ashlawn", Elements.SchoolsRow1Domain.Text);
            Assert.AreEqual("556", Elements.SchoolsRow1NCESID.Text);
            Assert.AreEqual("45646", Elements.SchoolsRow1ProviderID1.Text);
            Assert.AreEqual("32424", Elements.SchoolsRow1ProviderID2.Text);
            // Schoo1 row2
            Assert.AreEqual("Hayfield Elementary PTA", Elements.SchoolsRow2Name.Text);
            Assert.AreEqual("jumbula-hayfield", Elements.SchoolsRow2Domain.Text);
            Assert.IsFalse(Elements.SchoolsRow3Exist);
            #endregion check the new district in edit mode

        }
    }
}
