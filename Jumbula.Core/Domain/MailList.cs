﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class MailList : BaseEntity
    {
        [Required]
        [StringLength(128)]
        public string ListName { get; set; }

        public string QueryString { get; set; }

        public MailListType ListType { get; set; }

        public DateTime CreateDate { get; set; }

        public virtual ICollection<MailListContact> ListContact { get; set; }

        [ForeignKey("Club_Id")]
        public virtual Club Club { get; set; }

        public int Club_Id { get; set; }
    }
}
