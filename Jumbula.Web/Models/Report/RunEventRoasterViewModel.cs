﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jb.Framework.Common.Forms;
using Jb.Framework.Web.Model;
using SportsClub.Domain;


namespace SportsClub.Models
{
    [JavasciptViewModel]
    public class RunEventRoasterViewModel : BaseViewModel<EventRoaster>
    {
        public string EventName { get; set; }
        public string EventTitle { get; set; }
        public List<JbForm> DataTable { get; set; }
        public JbForm JbForm { get; set; }

        public RunEventRoasterViewModel()
        {
            DataTable = new List<JbForm>();
        }
        public RunEventRoasterViewModel(string eventName, string eventTitle)
            : this()
        {
            EventName = eventName;
            EventTitle = eventTitle;
        }
        public RunEventRoasterViewModel(string eventName, string eventTitle, List<JbForm> dataTable)
            : this(eventName, eventTitle)
        {
            DataTable = dataTable;
        }
        public RunEventRoasterViewModel(string eventName, string eventTitle, EventRoaster eventRoaster, IEnumerable<Order> orders, bool forOldEvents = false)
            : this(eventName, eventTitle)
        {
            JbForm = eventRoaster.JbForm;
            foreach (var section in JbForm.Elements)
                if (section is JbSection)
                    (section as JbSection).Elements.RemoveAll(e => e is JbCheckBox && !(e as JbCheckBox).Value);

            JbForm.Elements.RemoveAll(e => e is JbSection && !(e as JbSection).Elements.Any());

            foreach (var order in orders)
            {
                //var player = DependencyResolverHelper.IPlayerProfileDbService.GetFromForm(order.JbForm);
                //var basicInfo = OrderMetaDataHelper.GetEventBaseInfo(order.Id);
                if (order.JbForm != null)
                {
                    #region Just4Cantabile
                    if (forOldEvents)
                    {
                        try
                        {
                            var flag = false;
                            foreach (var element in order.JbForm.Elements.Where(r => r is JbSection))
                            {
                                if (element.Title == "Other parent information" || element.Title == "Parent 2" ||
                                    element.Title == "Other parent info" || element.Title == "Other parent")
                                {
                                    element.Name = "Parent2GuardianSection2";
                                    flag = true;
                                }
                            }
                            if (flag)
                            {
                                order.JbForm.Elements.RemoveAll(s => s.Name == "Parent2GuardianSection");
                                order.JbForm.Elements.FirstOrDefault(e => e.Name == "Parent2GuardianSection2").Name =
                                    "Parent2GuardianSection";
                            }
                            if (order.JbForm.Elements.Any(e => e.Name == "Parent job information"))
                            {
                                var parentJob =
                                    order.JbForm.Elements.FirstOrDefault(e => e.Name == "Parent job information") as
                                    JbSection;
                                if (parentJob.Elements.Any(e => e.Name == "Occupation"))
                                    (order.JbForm.Elements.SingleOrDefault(e => e.Name == "ParentGuardianSection") as
                                     JbSection)
                                        .Elements.SingleOrDefault(e => e.Name == "Occupation")
                                        .SetValue(
                                            parentJob.Elements.FirstOrDefault(e => e.Name == "Occupation").GetValue());

                                if (parentJob.Elements.Any(e => e.Name == "Employer"))
                                    (order.JbForm.Elements.SingleOrDefault(e => e.Name == "ParentGuardianSection") as
                                     JbSection)
                                        .Elements.SingleOrDefault(e => e.Name == "Employer")
                                        .SetValue(
                                            parentJob.Elements.FirstOrDefault(e => e.Name == "Employer").GetValue());
                            }
                        }
                        catch
                        {
                        }
                    }
                    #endregion

                    var newForm = order.JbForm.RightCopyNewByAny(JbForm, true);
                    newForm.CurrentMode = AccessMode.ReadOnly;
                    DataTable.Add(newForm);
                }
            }
        }

    }
}