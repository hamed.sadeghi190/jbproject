﻿

namespace Jumbula.Common.Enums
{
    public enum RoleCategoryType
    {
        System = 1,
        Dashboard = 2,
        Parent = 3,
    }
}
