﻿

namespace Jumbula.Common.Enums
{
    public enum CreditCartType : byte
    {
        Visa,
        MasterCard,
        Discover,
        Amex,
        Maestro
    }

}
