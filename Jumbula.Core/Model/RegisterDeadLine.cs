﻿using System;
using System.ComponentModel;

namespace Jumbula.Core.Model
{
    public class RegisterDeadLine
    {
        [DisplayName("Register starts on")]
        public DateTime? StartRegisterDeadLineDate { get; set; }

        [DisplayName("Show event")]
        public bool ShowEvent { get; set; }
    }
}
