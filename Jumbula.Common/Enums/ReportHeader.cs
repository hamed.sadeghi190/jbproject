﻿namespace Jumbula.Common.Enums
{
    public enum ReportHeader
    {
        Date = 0,
        SeasonName = 1,
        Room = 2,
        ClassDays = 3
    }
}
