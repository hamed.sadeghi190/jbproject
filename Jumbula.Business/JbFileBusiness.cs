﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class JbFileBusiness : IJbFileBusiness
    {
        private readonly IRepository<JbFile> _jbFileRepository;

        public JbFileBusiness(IRepository<JbFile> jbFileRepository)
        {
            _jbFileRepository = jbFileRepository;
        }

        public OperationStatus Create(JbFile file)
        {
            return _jbFileRepository.Create(file);
        }

        public IQueryable<JbFile> GetList()
        {
            return _jbFileRepository.GetList();
        }
        public JbFile Get(int FileId)
        {
            return _jbFileRepository.GetList().SingleOrDefault(f => f.Id == FileId);
        }
    }
}
