﻿using System.Web.Mvc;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Web.WebLogic.UserService;

namespace Jumbula.Web.Mvc.Controllers
{
    [Authorize(Roles = "SysAdmin, Support, Partner, Owner, Admin, Manager, Contributor, SchoolContributor, Instructor, OnsiteCoordinator, OnsiteSupportManager, RestrictedManager")]
    public class DashboardBaseController : JbBaseController
    {
        protected ClubBaseInfoModel ActiveClub { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ActiveClub = this.GetCurrentClubBaseInfo();
        }

        public ViewResult GetView(string viewName)
        {
            return View(viewName);
        }

        public override void CheckResourceForAccess(IProtectedResource protectedResource, bool notAccessibleIfNull = false)
        {
            if (protectedResource == null)
                if (notAccessibleIfNull)
                    PageNotFound();
                else
                    return;

            if (!protectedResource.DoesTheClubOwnIt(ActiveClub.Id))
                PageNotFound();
        }
    }
}