﻿using System;
using System.Web.Mvc;
using Jumbula.Common.Constants;

namespace Jumbula.Web.Mvc.ModelBinder
{
    public class JbPageBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            string typename;
            Type type;
            var typeValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName + ".ModelType");
            
            if (typeValue == null)
                typename = Constants.SolutionConstants.DomainClassesNamespace+ "." + bindingContext.ValueProvider.GetValue(bindingContext.ModelName + ".Type").ConvertTo(typeof(string));
            else
                typename = (string)typeValue.ConvertTo(typeof(string));

            if (typename.Contains(Constants.SolutionConstants.CoreProjectAssemblyName))
                type = Type.GetType(typename + ","+ Constants.SolutionConstants.CoreProjectAssemblyName, true);
            else
                type = Type.GetType(typename + "," + typename.Split('.')[0], true);

            var model = Activator.CreateInstance(type);
            bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => model, type);
            return model;
        }
    }
}