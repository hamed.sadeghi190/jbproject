﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('paymentSettingsController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', function ($scope, $http, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'peyment-settings-view';
        window.app.globalObjects.initializeView();
        $scope.stripeUrl = "";
        if ($stateParams.callbackMessage) {
            $scope.paymentPlanId = $stateParams.paymentPlanId;
        }

        $http.get('Dashboard/Setting/PaymentSetting').success(function (data, status, headers, config) {
            $scope.Model = data;
            if ($stateParams.callbackMessage) {
                if ($stateParams.callbackMessage == "1" && $scope.Model.StripeCustomerId != "") {
                    jbToast.success("You have connected to stripe successfully.");
                }
                else {
                    jbToast.error($stateParams.callbackMessage);
                }
            }
            else {
                $scope.stripeUrl = "https://connect.stripe.com/oauth/authorize?response_type=code&client_id=" + $scope.Model.StripeClientId + "&scope=read_write" + "&state=" + $scope.Model.Token + "&redirect_uri=" + $scope.Model.ReturnUrl;

            }
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.save = function (settingsSection) {

            $http.post('Dashboard/Setting/PaymentSetting', { model: $scope.Model, settingsSection: settingsSection })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     $scope.errors = null;
                     jbToast.success("Payment method updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.stripeConnect = function () {
            window.location = $scope.stripeUrl;
        }


    }])
})();

