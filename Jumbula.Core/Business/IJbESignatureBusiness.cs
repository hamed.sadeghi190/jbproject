﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;


namespace Jumbula.Core.Business
{
    public interface IJbESignatureBusiness
    {
        IQueryable<JbESignature> GetList();
        JbESignature Get(int id);
        OperationStatus Create(JbESignature eSignature);
        OperationStatus Update(JbESignature eSignature);
    }
}
