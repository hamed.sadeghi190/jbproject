﻿using Effort.Provider;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jumbula.Business.Tests.TestHelpers
{
    [TestClass]
    public static class TestInitializer
    {
        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext testContext)
        {
            EffortProviderConfiguration.RegisterProvider();

            FactoryGirlInitializer.RegisterFactories();
        }
    }
}
