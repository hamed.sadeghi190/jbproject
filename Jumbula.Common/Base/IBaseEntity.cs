﻿namespace Jumbula.Common.Base
{
    public interface IBaseEntity<TPrimaryKey>
        where TPrimaryKey : struct
    {
        TPrimaryKey Id { get; set; }
        TViewModel ToViewModel<TViewModel>() where TViewModel : class, new();
    }
    public interface IBaseEntity : IBaseEntity<int>
    {
    }
}
