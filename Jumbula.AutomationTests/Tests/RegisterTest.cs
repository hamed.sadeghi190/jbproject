﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element.Parent;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
    class RegisterTest : BaseTest <RegisterElements>
    {
        public RegisterTest(IWebDriver webDriver)
           : base(webDriver)
        {
            
        }
        public void RegisterNewClass()
        {
            Assert.AreEqual("Basic", Elements.FirstTuitionLabel.Text);
            Assert.AreEqual("$200.00", Elements.FirstTuitionPrice.Text);
            Assert.AreEqual("Advanced", Elements.SecondTuitionLabel.Text);
            Assert.AreEqual("$300.00", Elements.SecondTuitionPrice.Text);
            Elements.FirstTuitionCheckbox.Click();
            Assert.IsTrue(Elements.FirstCharge.GetAttribute("class").ToString().Contains("mandatory-charge"));
            Assert.AreEqual("Lunch ($30.00)", Elements.FirstCharge.Text);
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Basic", Elements.CheckoutTuitionLabel.Text);
            Assert.AreEqual("$200.00", Elements.CheckoutTuitionPrice.Text);
            Assert.AreEqual("Lunch", Elements.CheckoutFirstChargeLabel.Text);
            Assert.AreEqual("$30.00", Elements.CheckoutFirstChargePrice.Text);
            Assert.AreEqual("$230.00", Elements.CheckoutTotalPrice.Text);
            Elements.ContinueButton.Click();
            ExtraAPI.Wait(10000);
        }
        
        public void RegisterProgramStep1()
        {
            Elements.RegisterBtn.Click();
            ExtraAPI.Wait(5000);
        }
    }
   
}
