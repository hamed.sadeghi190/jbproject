﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using SportsClub.Models;
using Newtonsoft.Json;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CouponItemViewModel : BaseViewModel<Coupon>
    {
        public CouponItemViewModel()
        {
            AmounType = ChargeDiscountType.Fixed;
        }

        [MaxLength(256, ErrorMessage = "{0} cannot be longer than 256 characters.")]
        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }
        public CouponType CouponType { get; set; }
        [Required(ErrorMessage = "Code is required.")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Start date is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? StartDate { get; set; }

        [Required(ErrorMessage = "Expiry date is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? EndDate { get; set; }
        [Required(ErrorMessage = "Amount is required.")]
        public decimal Amount { get; set; }
        public EventStatusCategories Status { get; set; }
        public ChargeDiscountType AmounType { get; set; }
        public CalculationType CouponCalculateType { get; set; }
        public bool IsAllProgram { get; set; }
        public long? SeasonId { get; set; }
        public string SeasonDomain { get; set; }
        public List<long> SelectedEvents { get; set; }

        public bool IsOneTimeUse { get; set; }
        public List<string> CouponsList { get; set; }

    }
}