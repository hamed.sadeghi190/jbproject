

CREATE TABLE [dbo].[UserCreditCards](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[StripeCustomerId] [nvarchar](128) NULL,
	[LastDigits] [nvarchar](4) NULL,
	[ExpiryYear] [nvarchar](4) NULL,
	[ExpiryMonth] [nvarchar](2) NULL,
	[Name] [nvarchar](128) NULL,
	[Brand] [nvarchar](20) NULL,
	[StripeCustomerResponse] [nvarchar](max) NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.UserCreditCards] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[UserCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserCreditCards_dbo.UserProfile_userId] FOREIGN KEY([userId])
REFERENCES [dbo].[UserProfile] ([UserId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserCreditCards] CHECK CONSTRAINT [FK_dbo.UserCreditCards_dbo.UserProfile_userId]
GO


