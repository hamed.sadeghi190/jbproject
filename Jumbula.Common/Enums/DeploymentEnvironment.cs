﻿namespace Jumbula.Common.Enums
{
    public enum DeploymentEnvironment : byte
    {
        Local,
        Dev,
        QA,
        Production
    }
}
