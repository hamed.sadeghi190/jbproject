﻿using iTextSharp.text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;


namespace Jumbula.Common.Utilities
{
    interface IExportFile
    {
        MemoryStream ExportPdf(ExportDocumnetSchema schema);
        MemoryStream ExportExcel(ExportDocumnetSchema schema);
    }

    public class ExportDocumentColumn
    {
        public string DisplayName { get; set; }
        public string DataName { get; set; }
        public int Width { get; set; }
    }

    public class ExportDocumnetSchema
    {
        public List<ExportDocumentColumn> Columns { get; set; }

        public List<object> Data { get; set; }

        public string FileName { get; set; }

        public string PageTitle { get; set; }
    }


    public class ExportFileHelper : IExportFile
    {
        public MemoryStream ExportPdf(ExportDocumnetSchema schema)
        {
            // step 1: creation of a document-object
            var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10, 10, 10, 10);

            // step 2: we create a memory stream that listens to the document
            var output = new MemoryStream();
            iTextSharp.text.pdf.PdfWriter.GetInstance(document, output);

            // step 3: we open the document
            document.Open();

            //step 4: we add content to the document
            var numOfColumns = schema.Columns.Count();
            var dataTable = new iTextSharp.text.pdf.PdfPTable(numOfColumns);

            dataTable.DefaultCell.Padding = 3;
            dataTable.DefaultCell.BorderWidth = 1;
            dataTable.DefaultCell.HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER;


            dataTable.DefaultCell.BackgroundColor = iTextSharp.text.BaseColor.GRAY;

            // Adding headers
            foreach (var item in schema.Columns)
                dataTable.AddCell(item.DisplayName);


            dataTable.DefaultCell.BackgroundColor = iTextSharp.text.BaseColor.WHITE;

            dataTable.HeaderRows = 1;
            dataTable.DefaultCell.BorderWidth = 1;

            foreach (var record in schema.Data)
            {
                foreach (var field in schema.Columns)
                {
                    if (record.GetType().GetProperty(field.DataName)?.GetValue(record) != null)
                        dataTable.AddCell(record.GetType().GetProperty(field.DataName)?.GetValue(record).ToString());
                    else
                        dataTable.AddCell(string.Empty);
                }
            }

            //Set image header settings
            iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("~/Images/JBpower.png"));

            //Set Paragraph header Settings
            Paragraph paragrph = new Paragraph();
            paragrph.IndentationLeft = 50;
            paragrph.IndentationRight = 50;
            paragrph.Add(schema.PageTitle);

            //Set table header  Settings
            var tableHeader = new iTextSharp.text.pdf.PdfPTable(2);
            tableHeader.SetWidths(new int[] { 70, 30 });
            tableHeader.DefaultCell.BorderWidth = 0;
            tableHeader.SpacingBefore = 30;
            tableHeader.AddCell(paragrph);
            tableHeader.AddCell(img);

            //Set datatabe settings
            dataTable.SpacingBefore = 30;

            // Add items to the document
            document.Add(tableHeader);
            document.Add(dataTable);

            //This is important don't forget to close the document
            document.Close();

            // send the memory stream as File
            return output;
        }

        public MemoryStream ExportExcelDicData(ExportDocumnetSchema schema)
        {
            //Create new Excel workbook
            var workbook = new XSSFWorkbook();

            //Create new Excel sheet
            var sheet = workbook.CreateSheet();


            //Create a header row
            var headerRow = sheet.CreateRow(0);

            //Set the column names in the header row
            int colNumber = 0;
            foreach (var item in schema.Columns)
            {
                headerRow.CreateCell(colNumber).SetCellValue(item.DisplayName);
                // sheet.SetColumnWidth(colNumber, 50 * 256);
                colNumber++;
            }

            //(Optional) freeze the header row so it is not scrolled
            sheet.CreateFreezePane(0, 1, 0, 1);

            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var record in schema.Data)
            {
                var row = sheet.CreateRow(rowNumber++);
                var newRecord = (((Dictionary<string, string>)(record)));
                colNumber = 0;
                foreach (var field in schema.Columns)
                {
                    if (newRecord.Any(r => r.Key == field.DataName))
                    {
                        if (newRecord[field.DataName] != null)
                            row.CreateCell(colNumber).SetCellValue(newRecord[field.DataName].ToString());
                    }
                    else if (newRecord.Any(r => r.Key == field.DataName.Replace("_information", "")))
                    {
                        //--------Temporary ****** DELETE in next sprint ******** ------
                        if (newRecord[field.DataName.Replace("_information", "")] != null)
                            row.CreateCell(colNumber).SetCellValue(newRecord[field.DataName.Replace("_information", "")].ToString());
                    }
                    else
                        row.CreateCell(colNumber).SetCellValue(string.Empty);

                    colNumber++;
                }
            }

            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return output;
        }

        //public MemoryStream ExportExcelDicExpandoData(ExportDocumnetSchema schema)
        //{
        //    //Create new Excel workbook
        //    var workbook = new HSSFWorkbook();

        //    //Create new Excel sheet
        //    var sheet = workbook.CreateSheet();


        //    //Create a header row
        //    var headerRow = sheet.CreateRow(0);

        //    //Set the column names in the header row
        //    int colNumber = 0;
        //    foreach (var item in schema.Columns)
        //    {
        //        headerRow.CreateCell(colNumber).SetCellValue(item.DisplayName);
        //        // sheet.SetColumnWidth(colNumber, 50 * 256);
        //        colNumber++;
        //    }

        //    //(Optional) freeze the header row so it is not scrolled
        //    sheet.CreateFreezePane(0, 1, 0, 1);

        //    int rowNumber = 1;

        //    //Populate the sheet with values from the grid data
        //    foreach (var record in schema.Data)
        //    {
        //        var row = sheet.CreateRow(rowNumber++);
        //        var newRecord = (((List<IDictionary<string, string>>)(record)));
        //        colNumber = 0;
        //        foreach (var field in schema.Columns)
        //        {
        //            if (newRecord.Any(r => r.Keys.Any(k => k == field.DataName)))
        //            {
        //                if ((newRecord.Single(c => c.ContainsKey(field.DataName)))[field.DataName] != null)
        //                    row.CreateCell(colNumber).SetCellValue(newRecord.Single(c => c.ContainsKey(field.DataName))[field.DataName].ToString());
        //            }
        //            //else if (newRecord.Any(r => r.Key == field.DataName.Replace("_information", "")))
        //            //{
        //            //    //--------Temporary ****** DELETE in next sprint ******** ------
        //            //    if (newRecord[field.DataName.Replace("_information", "")] != null)
        //            //        row.CreateCell(colNumber).SetCellValue(newRecord[field.DataName.Replace("_information", "")].ToString());
        //            //}
        //            else
        //                row.CreateCell(colNumber).SetCellValue(string.Empty);

        //            colNumber++;
        //        }
        //    }

        //    //Write the workbook to a memory stream
        //    MemoryStream output = new MemoryStream();
        //    workbook.Write(output);

        //    //Return the result to the end user
        //    return output;
        //}

        public MemoryStream ExportExcel(ExportDocumnetSchema schema)
        {
            //Create new Excel workbook
            var workbook = new HSSFWorkbook();

            //Create new Excel sheet
            var sheet = workbook.CreateSheet();


            //Create a header row
            var headerRow = sheet.CreateRow(0);

            //Set the column names in the header row
            int colNumber = 0;
            foreach (var item in schema.Columns)
            {
                headerRow.CreateCell(colNumber).SetCellValue(item.DisplayName);
                // sheet.SetColumnWidth(colNumber, 50 * 256);
                colNumber++;
            }

            //(Optional) freeze the header row so it is not scrolled
            sheet.CreateFreezePane(0, 1, 0, 1);

            int rowNumber = 1;

            //Populate the sheet with values from the grid data
            foreach (var record in schema.Data)
            {
                var row = sheet.CreateRow(rowNumber++);
                colNumber = 0;
                foreach (var field in schema.Columns)
                {
                    if (record.GetType().GetProperty(field.DataName)?.GetValue(record) != null)
                        row.CreateCell(colNumber).SetCellValue(record.GetType().GetProperty(field.DataName)?.GetValue(record).ToString());
                    else
                        row.CreateCell(colNumber).SetCellValue(string.Empty);

                    colNumber++;
                }
            }

            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return output;
        }
    }
}
