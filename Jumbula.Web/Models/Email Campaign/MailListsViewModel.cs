﻿using Jumbula.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class MailListsViewModel : BaseViewModel<MailList>
    {
        public string ListName { get; set; }

        public MailListType ListType { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }
    }
}