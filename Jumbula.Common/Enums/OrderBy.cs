﻿namespace Jumbula.Common.Enums
{
    public enum OrderBy
    {
        Ascending,
        Descending
    }
}
