﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class FSAReportViewModel
    {
        public FSAReportViewModel()
        {

        }

        public int? Year { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public FSAReportFilterType FilterType { get; set; }
    }

    public enum FSAReportFilterType : byte
    {
        Year,
        DateRange
    }
}