﻿
using Jb.Framework.Common;
using System;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
using Jumbula.Core.Model.Toolbox;
using Jumbula.Common.Helper;

namespace Jumbula.Business
{
    public class ClientBusiness : IClientBusiness
    {
        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private readonly IClientPaymentMethodBusiness _clientPaymentMethodBusiness;
        private readonly IRepository<Client, long> _clientRepository;
        private readonly IEntitiesContext _dataContext;
        #endregion

        #region Constructors
        public ClientBusiness(IClubBusiness clubBusiness, IClientPaymentMethodBusiness clientPaymentMethodBusiness, IRepository<Client, long> clientRepository, IEntitiesContext dataContext)
        {
            _clubBusiness = clubBusiness;
            _clientPaymentMethodBusiness = clientPaymentMethodBusiness;
            _clientRepository = clientRepository;
            _dataContext = dataContext;
        }
        #endregion

        #region Public Methods

        public ClientBillingHistory GetClientBillingHistory(long billingId)
        {
            return _dataContext.Set<ClientBillingHistory>().SingleOrDefault(c => c.Id == billingId);
        }
        public Client Get(int clubId)
        {
            var client = _dataContext.Set<Club>().SingleOrDefault(c => c.Id == clubId).Client;

            return client;
        }
        public Client GetById(long clientId)
        {
            return _clientRepository.GetList().SingleOrDefault(c => c.Id == clientId);
        }
        public OperationStatus ActivateClientCreditCard(long clientId, long cardId)
        {
            OperationStatus res = new OperationStatus() { Status = false, Message = Constants.PlayerProfile_Ajax_Request_Failed };
            var client = GetById(clientId);
            if (client.CreditCards != null && client.CreditCards.Any())
            {
                if (client.CreditCards.Any(c => c.IsDefault))
                {
                    foreach (var item in client.CreditCards.Where(c => c.IsDefault && c.Id != cardId))
                    {
                        item.IsDefault = false;
                    }
                }
                if (client.CreditCards.Any(c => c.Id == cardId))
                {
                    client.CreditCards.Single(c => c.Id == cardId).IsDefault = true;
                    res = _clientRepository.Update(client);
                }
            }

            return res;
        }

        public OperationStatus Update(Client entity)
        {
            return _clientRepository.Update(entity);
        }

        public PaymentGateway GetPaymentGatewayClub(int clubId)
        {
            var _club = _clubBusiness.Get(clubId);
            ClientPaymentMethod clientPaymentMethod = _clientPaymentMethodBusiness.Get(_club.ClientId.Value);
            return clientPaymentMethod.DefaultPaymentGateway;
        }

        public IQueryable<Client> GetList()
        {
            return _clientRepository.GetList();
        }

        public string GenerateBillingId()
        {
            byte[] buffer = new byte[3];
            var random = new Random();
            
            random.NextBytes(buffer);

            var s = BitConverter.ToString(buffer, 0, 1) + BitConverter.ToString(buffer, 1, 1) + BitConverter.ToString(buffer, 2, 1);
            var ss = DateTime.UtcNow.ToString("MMddyy");

            return $"B{s}-{ss}";
        }

        public ToolboxGoogleAnalyticsViewModel GetToolboxGoogleAnalytics(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            if(club.PartnerId.HasValue)
            {
                var partnerSetting = JsonHelper.JsonDeserialize<PartnerSetting>(club.PartnerClub.SettingSerialized);

                if (partnerSetting.PortalParameters.ReadGASettingFromPartner)
                    club = club.PartnerClub;
            }

            return MapToolboxGoogleAnalyticsViewModel(club.Client);
        }

        public OperationStatus SaveToolboxGoogleAnalytics(ToolboxGoogleAnalyticsViewModel toolboxGoogleAnalytics, int clubId)
        {
            var client = Get(clubId);

            client.GATrackingId = toolboxGoogleAnalytics.TrackingId;

            return Update(client);
        }

        #endregion

        #region Private Methods
        private ToolboxGoogleAnalyticsViewModel MapToolboxGoogleAnalyticsViewModel(Client client)
        {
            return new ToolboxGoogleAnalyticsViewModel
            {
                TrackingId = client.GATrackingId
            };
        }

        #endregion
    }
}
