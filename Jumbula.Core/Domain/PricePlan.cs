﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class PricePlan : BaseEntity
    {
        public decimal Deposit { get; set; }
        public decimal MonthlyCharges { get; set; }
        public decimal TransactionFee { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Client> Clients { get; set; }
        public bool IsDeleted { get; set; }
        public int? ClubRevenueId { get; set; }
        [ForeignKey("ClubRevenueId")]
        public virtual ClubRevenue ClubRevenue { get; set; }
        public PricePlanType Type { get; set; }
    }
}
