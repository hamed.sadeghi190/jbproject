﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemConfirmCreditController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cancelOrderService', 'makePaymentClubService', 'jbToast', function ($scope, $http, $log, $stateParams, $state, cancelOrderService, makePaymentClubService, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.Model = cancelOrderService.get();

        $scope.Model = makePaymentClubService.get();//get info from step back

        $scope.getPaymentMethodText = function () {
            var paymentMethodValue = $scope.Model.SelectedPaymentMethod;

            for (var i = 0; i < $scope.Model.PaymentMethods.length; i++) {

                if ($scope.Model.PaymentMethods[i].Value == paymentMethodValue) {
                    return $scope.Model.PaymentMethods[i].Text;
                }
            }

            return '';
        }

        $scope.Model.SelectedPaymentMethodText = $scope.getPaymentMethodText();

        $scope.CreditBilling = function () {
            $http.post($scope.baseUrl + '/SubmitCreditClub', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        jbToast.success("Your operation was successful.", "")
                        $state.go('SystemClubBillingHistory', { clubId: $stateParams.clubId });

                    }

                    else {
                        $scope.MC.handleErrors($scope, data);
                        jbToast.error("The operation failed.");
                    }
                  
                });

        };
        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("SystemPaymentWithCredit", { clubId: $stateParams.clubId });
                }
            }
            else {
                $state.go("SystemPaymentWithCredit", { clubId: $stateParams.clubId });
            }
        }
    }]);
})();