﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsClub.Models
{
    public class FamiliesProfileViewModel
    {
        public FamiliesProfileViewModel()
        {
            Parents= new List<ParentsInformationViewModel>();
            Participants = new List<ParticipantViewModel>();
            EmergencyContact = new List<EmergencyContactViewModel>();
            FamilieAddress = new List<ParentsAddressViewModel>();
            AuthorizedPickup = new List<AuthorizepickupViewModel>();
            InsuranceInformation = new List<InsuranceInformationViewModel>();
        }
        public List<ParentsInformationViewModel> Parents { get; set; }
        public List<ParticipantViewModel> Participants { get; set; }

        public List<EmergencyContactViewModel> EmergencyContact { get; set; }
        public List<InsuranceInformationViewModel> InsuranceInformation { get; set; }

        public List<ParentsAddressViewModel> FamilieAddress { get; set; }
        public List<AuthorizepickupViewModel> AuthorizedPickup { get; set; }
        
    }
    
}