﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Model.Notification
{
    public class NotificationModel
    {
        public NotificationCategory Category { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public string NotificationTime { get; set; }
    }
}
