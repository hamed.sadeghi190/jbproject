﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ParentEmailsListReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};


        $scope.Model = customReportService.getModelFromStrorage();
        var model = $scope.Model;
        $scope.filter.printMode = false;
        
        $http.post(baseUrl + '/GetParentEmailsListReportDetail', { model: model } ).success(function (data, status, headers, config) {

            $scope.ProgramNames = data.programsSelected;
            $scope.Date = data.Date;
            $scope.SeasonName = data.SeasonName;
            $scope.ClubName = data.ClubName;
            dataInfo = data;
            prepareGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        var prepareGrid = function () {
            $scope.ParentEmailsListDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetParentEmailsListReportInfo",
                        dataType: "json",
                        type: "POST",
                        data: function () {
                            return {
                                model: $scope.Model,
                                programNames: dataInfo.programsSelected,
                                printMode: $scope.filter.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.ParentEmailsListGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Parent emails list).xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.SeasonName + " - " + dataInfo.programsSelected
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Parent emails list - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.ParentEmailsListDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#ParentEmailsListGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "ProgramName",
                        title: "Program name",
                        width: "140px",
                        sortable: true
                    }, {
                        field: "ParticipantFirstName",
                        title: "First name",
                        width: "70px",
                        sortable: true
                    }, {
                        field: "ParticipantLastName",
                        title: "Last name",
                        width: "100px",
                        sortable: true
                    }, {
                        field: "ParentEmail",
                        title: "Parent email",
                        width: "150px",
                        sortable: true,
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $scope.filter.printMode = false;
            $("#ParentEmailsListGrid").getKendoGrid().saveAsExcel();
        }

        $scope.saveToPDF = function () {

            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetParentEmailsListReportInfo', {
                model: $scope.Model,
                programNames: dataInfo.programsSelected,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (Parent emails list).pdf";
                a.click();
            });
            $scope.filter.printMode = false;

        }

        $scope.print = function () {
            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetParentEmailsListReportInfo', {
                model: $scope.Model,
                programNames: dataInfo.programsSelected,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (Parent emails list).pdf";
                a.click();
            });
            $scope.filter.printMode = false;
        }
    }
    ]);

})();