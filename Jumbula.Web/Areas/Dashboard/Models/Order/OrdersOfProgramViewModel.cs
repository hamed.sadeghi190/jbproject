﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Areas.Dashboard.Models
{
    public class OrdersOfProgramViewModel : BaseViewModel<OrderItem, long>
    {
        public int UserId { get; set; }
        public string Attendee { get; set; }
        public DateTime? OrderDate { get; set; }
        public string StrOrderDate
        {
            get
            {
                return OrderDate?.ToString(Constants.DefaultDateTimeFormat);
            }

        }
        public bool IsSandbox { get; set; }
        public long OrderId { get; set; }
        public long ProgramId { get; set; }
        public string ConfirmationId { get; set; }
        public string EntryFee { get; set; }
        public string EntryFeeName { get; set; }
        public decimal Balance { get; set; }
        public List<ParentOrderItemInstallmentsViewModel> Installments { get; set; }
        public List<ParentOrderItemInstallmentsViewModel> AddOnInstallments { get; set; }
        public bool EnableInstallmentRefund {
            get
            {
                return Installments.Any(i => i.RefundAmount > 0) || AddOnInstallments.Any(i => i.RefundAmount > 0);
            }
        }
        public bool EnableInstallmentTakePayment
        {
            get
            {
                return Installments.Any(i => i.TakePaymentAmount > 0) || AddOnInstallments.Any(i => i.TakePaymentAmount > 0);
            }
        }
        public int ItemStatusReason { get; set; }
        public int ItemStatus { get; set; }
        public string ProgramType { get; set; }
        public string ChessSection { get; set; }
        public string ChessSchedule { get; set; }
        public string Schedule { get; set; }
        public string ProgramName { get; set; }
        public PaymentPlanType PaymentPlanType { get; set; }
        public string Season { get; set; }
        public long? SeasonId { get; set; }
        public string SeasonDomain { get; set; }
        public bool IsDropIn { get; set; }

        public bool IsPartner { get; set; }

        public LotteryStatus LotteryStatus { get; set; }
    }



}