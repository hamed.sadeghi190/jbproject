
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ClientPaymentMethods](
	[Id] [bigint] NOT NULL,
	[EnableStripePayment] [bit] NOT NULL,
	[IsStripeTestMode] [bit] NOT NULL,
	[StripeCustomerId] [nvarchar](max) NULL,
	[StripeAccessToken] [nvarchar](max) NULL,
	[StripeResponse] [nvarchar](max) NULL,
	[StripeCheckoutLabel] [nvarchar](max) NULL,
	[EnablePaypalPayment] [bit] NOT NULL,
	[IsPaypalTestMode] [bit] NOT NULL,
	[PaypalEmail] [nvarchar](max) NULL,
	[PaypalCheckoutLabel] [nvarchar](max) NULL,
	[EnableCashPayment] [bit] NOT NULL,
	[CashCheckoutLabel] [nvarchar](max) NULL,
	[CashPaymentMessage] [nvarchar](max) NULL,
	[CashPaymentInstruction] [nvarchar](max) NULL,
	[EnableBankTransferPayment] [bit] NOT NULL,
	[BankTransferCheckoutLabel] [nvarchar](max) NULL,
	[BankTransferPaymentMessage] [nvarchar](max) NULL,
	[BankTransferPaymentInstruction] [nvarchar](max) NULL,
	[EnableManualPayment] [bit] NOT NULL,
	[ManualPaymentName] [nvarchar](max) NULL,
	[ManualPaymentMessage] [nvarchar](max) NULL,
	[ManualPaymentInstruction] [nvarchar](max) NULL,
	[ManualPaymentMethod] [int] NOT NULL,
	
	
	
 CONSTRAINT [PK_dbo.ClientPaymentMethods] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnablePaypalPayment]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [IsPaypalTestMode]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnableCashPayment]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnableManualPayment]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [ManualPaymentMethod]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnableBankTransferPayment]
GO

ALTER TABLE [dbo].[ClientPaymentMethods]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClientPaymentMethods_dbo.Clients_Id] FOREIGN KEY([Id])
REFERENCES [dbo].[Clients] ([Id])
GO

ALTER TABLE [dbo].[ClientPaymentMethods] CHECK CONSTRAINT [FK_dbo.ClientPaymentMethods_dbo.Clients_Id]
GO


