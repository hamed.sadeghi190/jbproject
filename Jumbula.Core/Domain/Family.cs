﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class Family: BaseEntity, IProtectedResource
    {
        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }

        public int UserId { get; set; }

        public virtual ICollection<FamilyContact> Contacts { get; set; }

        [MaxLength(128)]
        public string InsuranceCompanyName { get; set; }

        [MaxLength(128)]
        public string InsurancePolicyNumber { get; set; }

        [MaxLength(64)]
        public string InsurancePhone { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            if (UserId == userId)
                return true;

            return false;
        }
    }
}
