﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models.Email
{
    public class EmailReminderFamilyInvoiceModel
    {
        public EmailReminderFamilyInvoiceModel()
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
            ClubUrl = RootSiteUrl + "/" + ClubDomain;
            ReservationItems = new List<OrderItemFamilyInvoiceReminderItem>();
        }

        public string PreapprovalKey { get; set; }
        public string AppliedText { get; set; }
        public string PayPalInfo { get; set; }
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string ClubAddress { get; set; }
        public string ClubImageUrl { get; set; }
        public string ClubName { get; set; }
        public string ClubUrl { get; set; }
        public string ClubEmail { get; set; }
        public string ClubLogo { get; set; }
        public string ParentName { get; set; }
        public string ParentEmail { get; set; }
        public string MemberName { get; set; }
        public string ClubPhone { get; set; }
        public bool HasPartner { get; set; }
        public CurrencyCodes ClubCurrency { get; set; }
        public string InstallmentDate { get; set; }
        public string RootSiteUrl { get; set; }
        public string BodyMailTitle { get; set; }

        public string PlayerFullName { get; set; }
        public string TransactionID { get; set; }

        public long InvoiceNumber { get; set; }
        public string ViewLinkForParent { get; set; }
        public string ViewLinkForAdmin { get; set; }
        public string ViewLinkForAdminViewParent { get; set; }

        public decimal TotalAmount { get; set; }
        public decimal AmountDue { get; set; }

        public string NotToRecipeint { get; set; }
        public long  InvoiceId { get; set; }

        public string StrTotalAmount
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(TotalAmount, ClubCurrency);
            }
        }

        public List<string> ConfirmationIds { get; set; }
        public string StrConfirmationIds {
            get
            {
                if (ConfirmationIds != null && ConfirmationIds.Any())
                {
                    return string.Join(", ", ConfirmationIds);
                }
                return string.Empty;
            }
                }
        public string Balance { get; set; }
        public string ParticipantName { get; set; }
        public string PaidAmount { get; set; }
        public List<OrderItemFamilyInvoiceReminderItem> ReservationItems { get; set; }
        public bool isFail { get; set; }
        public DateTime DueDate { get; set; }

        public string StrDueDate { 
             get
            {
                return DueDate.ToString("MMM dd, yyyy");
            }
        }

    }

    public class OrderItemFamilyInvoiceReminderItem
    {
        public OrderItemFamilyInvoiceReminderItem()
        {

        }
        public OrderItemFamilyInvoiceReminderItem(CurrencyCodes currency)
        {
            Currency = currency;
        }
        public string ConfirmationId { get; set; }
        public string PlayerFullName { get; set; }
        public string ParticipantName { get; set; }
        public CurrencyCodes Currency { get; set; }
        public decimal Price { get; set; }
        public string Amount
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(Price, Currency);
            }
        }
        public string EventName { get; set; }


    }
    
}