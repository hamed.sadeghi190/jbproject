﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignTemplatesController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaign-templates';
        window.app.globalObjects.initializeView();

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $scope.templateGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetTemplatesNameList',
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.templateSearchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            //serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.FillTemplateGrid = {
            dataSource: $scope.templateGridDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [{
                field: "TemplateName",
                title: "Template name",
                template: "<a ui-sref='EmailCampaignTemplateAddEdit({templateId: #=Id#})'  class='block'> #= TemplateName # </a>",
            },
            {
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				    <ul><li ui_sref='EmailCampaignTemplateAddEdit({templateId: #=Id#})' class='k-item k-state-default k-first' role='menuitem'>\
						    <span class='k-link jbi-pencil'>Edit</span>\
				      </li><li disabled='disabled'><hr /></li>\
				                <li ng-click='deleteTemplate(#=Id#)'>\
					                <span class='k-link jbi-remove'>Delete</span>\
				                </li>\
				            </ul>\
			            </li>\
		            </ul>",
                attributes: {
                    class: "grid-actions"
                }
            }
            ]
        };

        $scope.templateSearch = function () {
            $scope.FillTemplateGrid.dataSource.read();
        }

        $scope.deleteTemplate = function (Id) {
            jbConfirmation
                .yes(function () {
                    type: "POST",
                $http.post(baseUrl + '/DeleteCampaignTemplate', { templateId: Id }).success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.FillTemplateGrid.dataSource.read();
                    }
                });
                }).confirm();
        }

    }]);
})();