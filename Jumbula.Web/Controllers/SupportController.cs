﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Models;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Core.Business;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    [Authorize(Roles = "Support")]
    public class SupportController : JbBaseController
    {
        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;

        public SupportController(IApplicationUserManager<JbUser, int> applicationUserManager, IOrderInstallmentBusiness orderInstallmentBusiness)
        {
            _applicationUserManager = applicationUserManager;
            _orderInstallmentBusiness = orderInstallmentBusiness;
        }


        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult CreateClub()
        {
            var model = new CreateClubViewModel();

            ViewBag.Categories = Ioc.CategoryBuisness.GetList().ToList()
                              .Where(e => e.ParentId != null)
                              .ToList().Select(x => new SelectListItem() { Text = string.Format(Constants.F_CategoryFormat, x.ParentCategory.Name, x.Name), Value = x.Id.ToString() }).ToList();

            model.Category = 1;

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult CreateClub(CreateClubViewModel model)
        {

            var clubBussiness = Ioc.ClubBusiness;

            if (model.IsSandBoxMode)
            {
                ModelState.Remove("PaypalEmail");
            }


            if (clubBussiness.IsClubDomainExist(model.Domain))
            {
                ModelState.AddModelError("Domain", "A club with this domain is exist.");
            }

            if (ModelState.IsValid)
            {
                var address = new PostalAddress();

                try
                {
                    GoogleMap.GetGeo(model.Address, ref address);
                }
                catch
                {
                    ModelState.AddModelError("Address", "Address is not correct.");

                    ViewBag.Categories = Ioc.CategoryBuisness.GetList().ToList()
                              .Where(e => e.ParentId != null)
                              .ToList().Select(x => new SelectListItem() { Text = string.Format(Constants.F_CategoryFormat, x.ParentCategory.Name, x.Name), Value = x.Id.ToString() }).ToList();

                    return View(model);
                }
                JbUser clubAdmin = Ioc.UserProfileBusiness.Get(model.Email);

                if (clubAdmin == null || clubAdmin.Id == 0)
                {
                    clubAdmin = new JbUser() { UserName = model.Email, Email = model.Email };

                    var res = _applicationUserManager.Create(clubAdmin, model.Password);

                    // confirm email
                    _applicationUserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));
                    string token = _applicationUserManager.GenerateEmailConfirmationTokenAsync(clubAdmin.Id).Result;
                    _applicationUserManager.ConfirmEmail(clubAdmin.Id, token);
                }

                model.TimeZone = address.TimeZone.HasValue ? address.TimeZone.Value : Jumbula.Common.Enums.TimeZone.PST;

                var role = Ioc.UserProfileBusiness.GetRole(RoleCategory.Admin);



                #region CreateClub

                if (model.IsSandBoxMode)
                {
                    model.PaypalEmail = Constants.W_Sandbox_Paypal_Email;
                }

                var club = model.ToClub();
                var priceplan = Ioc.PricePlanBusiness.GetFreeTrialPlan();
                club.Client.PricePlan = priceplan;
                club.Client.PricePlanId = priceplan.Id;
                club.MetaData = new MetaData()
                {
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow
                };

                club.ClubStaffs = new List<ClubStaff>();

                club.ClubStaffs.Add(new ClubStaff()
                {
                    Status = StaffStatus.Active,
                    JbUserRole = new JbUserRole()
                    {
                        UserId = clubAdmin.Id,
                        RoleId = role.Id
                    }
                });

                club.CategoryId = model.Category;
                club.Client.PricePlanId = 1;
                club.Setting = new ClubSetting()
                {
                    CertificateOfInsurances = new CertificateOfInsurances()
                };

                club.ClubLocations = new List<ClubLocation>()
                    {
                        new ClubLocation()
                        {
                            Name = string.Empty,
                            PostalAddress = address,
                        }
                    };

                club.CatalogSetting = new CatalogSetting { CatalogSearchTags = new CatalogSearchTags() };

                club.Address = address;

                club.TypeId = 1;
                club.PartnerId = null;

                var result = Ioc.ClubBusiness.Create(club);

                if (result.Status)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", result.Message);
                }

                #endregion
            }

            ViewBag.Categories = Ioc.CategoryBuisness.GetList().ToList()
                              .Where(e => e.ParentId != null)
                              .ToList().Select(x => new SelectListItem() { Text = string.Format(Constants.F_CategoryFormat, x.ParentCategory.Name, x.Name), Value = x.Id.ToString() }).ToList();

            return View(model);
        }


        public virtual ActionResult SetActiveClub(string name, string domain, int categoryId, int clubId)
        {

            string logoURL = Constants.Club_NoSport_Default_Image_Small;

            //JBMembership.SetSupportMemberClub(name, domain, logoURL, categoryId, false, false, clubId);

            return RedirectToAction("Edit", "Club", new { domain = domain });
        }

        public virtual ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult ChangePassword(LocalPasswordModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            string error = string.Empty;
            bool hasLocalAccount = User.Identity.IsCurrentUserExist();

            if (hasLocalAccount)
            {
                bool result = _applicationUserManager.ChangePasswordAsync(this.GetCurrentUserId(), model.OldPassword, model.NewPassword).Result == IdentityResult.Success;
                if (!result)
                {
                    error = Constants.Profile_ChangePassword_Failed_Invalid_Password;
                }
            }
            else
            {
                error = Constants.Profile_ChangePassword_Failed_Invalid_UserName;
            }

            //chck errors
            if (!string.IsNullOrEmpty(error))
            {
                this.ModelState.AddModelError("", error);
                return View(model);
            }

            //if successfully chenged password go to index
            return RedirectToAction("Index");
        }

        public virtual ActionResult RenameCategories()
        {
            var categoryBussiness = Ioc.CategoryBuisness;

            var categories = categoryBussiness.GetList();

            string[] lines = System.IO.File.ReadAllLines(@"D:\Jumbula\SportsClub-NewUI\SportsClub\App_Data\Categories.txt");

            // Display the file contents by using a foreach loop.
            foreach (string line in lines)
            {
                var sections = line.Split(',');

                string en = sections[0];
                string fa = sections[1];

                foreach (var cat in categories.Where(c => c.Name.Equals(en) && !string.IsNullOrEmpty(fa)))
                {
                    cat.Name = fa.Trim();
                }

            }

            var result = categoryBussiness.Update();

            return null;
        }

        [AllowAnonymous]
        public ActionResult GetDueDateInstallments(DateTime? dueDate = null)
        {
            dueDate = (dueDate ?? DateTime.UtcNow).Date.AddDays(1).AddSeconds(-1);
            List<DueDateInstallmentsViewModel> installments = new List<DueDateInstallmentsViewModel>();
            var allDueclubsInstallmentsPer = _orderInstallmentBusiness.GetAllInstallmentsByDueDate(dueDate.Value);
            foreach (var clubInstallmentPer in allDueclubsInstallmentsPer)
            {
                foreach (var installmentPer in clubInstallmentPer.Value)
                {
                    var installment = _orderInstallmentBusiness.Get(installmentPer);
                    installments.Add(new DueDateInstallmentsViewModel()
                    {
                        Id = installment.Id,
                        Amount = installment.Balance,
                        InstallmentDate = installment.InstallmentDate,
                        FirstName = installment.OrderItem.FirstName,
                        LastName = installment.OrderItem.LastName
                    });
                }
            }

            return View(installments);
        }
    }
}
