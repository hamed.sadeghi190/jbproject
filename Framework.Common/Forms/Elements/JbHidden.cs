﻿using Jb.Framework.Common.Forms.JsonFormatter;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbHidden : JbBaseElement
    {
        public string Value { get; set; }

        public string GetValue()
        {
            return Value;
        }      
    }
}
