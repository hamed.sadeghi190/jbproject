﻿using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Lottery;
using System.Linq;

namespace Jumbula.Core.Business
{
    public interface ILotteryBusiness
    {
        LotteryDetailsViewModel GetLotteryDetails(int programId);

        object Draw(long programId);
        OperationStatus RemoveLotteryItem(long orderItemId);
        OperationStatus AddLotteryItem(long orderItemId);
        bool HasSpaceForLottery(long orderItemId);
        OperationStatus FinalizeLottery(int programId);

        Order CompleteLotteryItem(long orderItemId);

        PaginatedList<LotteryItemViewModel> GetLotteryItems(long programId, int pageIndex, int pageSize);
    }
}
