﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CatalogReportsBladeController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
            window.app.globalObjects.pageClass = 'catalog-reports-view';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/CatalogReports", order: 2, menu: "PortalReports" });

            $scope.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function(value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

        }
    ]);

})();