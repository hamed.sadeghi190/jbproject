﻿using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models.Email
{
    public class ProviderRespondToInvationEmailModel
    {
        public ProviderRespondToInvationEmailModel()
            {
              var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
        }
        public string RootSiteUrl { get; set; }

        public string PartnerName { get; set; }

        public string ProviderName { get; set; }

        public string ProgramName { get; set; }

        public string RespondStr { get; set; }

        public string PreferedDays { get; set; }

        public string Note { get; set; }

        public string Reason { get; set; }

        public int RespondInt { get; set; }

        public int ReasonInt { get; set; }
    }
}