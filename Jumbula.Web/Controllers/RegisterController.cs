﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.ModelState;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.ClubsService;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Register;
using Jumbula.Web.WebLogic.UserService;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using SportsClub.Models;
using SportsClub.Models.FormModels;
using SportsClub.Models.Register;
using Constants = Jumbula.Common.Constants.Constants;
using TimeZones = Jumbula.Common.Enums.TimeZone;
using JsonHelper = Jumbula.Common.Helper.JsonHelper;
using Jumbula.Core.Business;
using Jumbula.Common.Constants;

namespace Jumbula.Web.Controllers
{


    [Authorize(Roles = "Parent, Support, Admin, Manager, Contributor")]
    public class RegisterController : JbBaseController
    {
        private readonly IFollowupFormBusiness _followupFormBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IRegisterBusiness _registerBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IProgramSessionBusiness _programSessionBusiness;
        private readonly IClubSettingBusiness _clubSettingBusiness;
        private readonly IPeopleBusiness _peopleBusiness;

        public RegisterController(IFollowupFormBusiness followupFormBusiness, IProgramBusiness programBusiness, IOrderItemBusiness orderItemBusiness, IRegisterBusiness registerBusiness, ISeasonBusiness seasonBusiness, IProgramSessionBusiness programSessionBusiness, IPeopleBusiness peopleBusiness, IClubSettingBusiness clubSettingBusiness)
        {
            _followupFormBusiness = followupFormBusiness;
            _programBusiness = programBusiness;
            _orderItemBusiness = orderItemBusiness;
            _registerBusiness = registerBusiness;
            _seasonBusiness = seasonBusiness;
            _programSessionBusiness = programSessionBusiness;
            _peopleBusiness = peopleBusiness;
            _clubSettingBusiness = clubSettingBusiness;
        }



        [HttpGet]
        public virtual ActionResult Register(string domain, string seasonDomain, string clubDomain, long? ists, int? userId, bool offlineMode = false, bool isOverrideCapacity = false)
        {
            ViewBag.Message = "";
            if (ists.HasValue && ists.Value <= 0)
            {
                ists = null;
            }
            var club = Ioc.ClubBusiness.Get(clubDomain);

            var clubId = club.Id;
            var program = _programBusiness.Get(clubId, seasonDomain, domain);
            var model = new RegisterProgramModel(program, userId) { ISTS = ists };

            if (this.GetCurrentUserRole() != RoleCategory.Parent && !offlineMode)
            {
                return RedirectToAction(actionName: "DisplayNotAccessibleMessage", controllerName: "SubDomain", routeValues:
                                 new
                                 {
                                     role = RoleCategory.Admin,
                                     message = "You are logged in as a staff member, you can only do off-line registration.",
                                     returnurl = "/" + seasonDomain + "/" + model.ProgramDomain
                                 });
            }


            var clubSetting = club.Setting;

            if (userId.HasValue && userId.Value != this.GetCurrentUserId() && this.GetCurrentUserRoleType() != RoleCategoryType.Dashboard)
            {
                return RedirectToAction("Login", "Account");
            }


            if (program == null || (program != null && program.SaveType == SaveType.Draft))
            {
                PageNotFound();
            }

            if (this.IsUserAuthenticated() && !User.IsInRole(RoleCategory.Parent.ToDescription()) && !offlineMode && !ists.HasValue)
            {
                var order = Ioc.OrderBusiness.GetLastOfflineOrderForClub(clubId);
                if (order == null)
                {
                    string redirectUrl = string.Empty;

                    redirectUrl = string.Format("/Dashboard#/Season/{0}/ProgramsReview", seasonDomain);

                    return Redirect(redirectUrl);
                }
                userId = order.UserId;
            }

            ViewBag.SelectedStep = "";
            var regServ = RegisterService.Get();

            if (_programBusiness.IsTeamChessTourney(program))
            {
                model.IsTeamRegistration = true;
            }

            switch (program.Status)
            {
                case ProgramStatus.Frozen:
                    {
                        if (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
                        {
                            ViewBag.Message = "Program registration has been disabled";
                            break;
                        }
                        return RedirectToAction(actionName: "DisplayFrozenMessage", controllerName: "SubDomain", routeValues:
                                       new
                                       {
                                           domain = model.ProgramDomain,
                                           clubDomain = model.ClubDomain,
                                       });
                    }
                case ProgramStatus.Deleted:
                    {
                        return RedirectToAction(actionName: "DisplayDeletedMessage", controllerName: "SubDomain", routeValues:
                                        new
                                        {
                                            domain = model.ProgramDomain,
                                            clubDomain = model.ClubDomain
                                        });
                    }
                default:
                    break;
            }

            if (offlineMode && program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                if (program.ProgramSchedules.Any(s => s.IsFreezed) && this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
                {
                    var programFreezed = program.ProgramSchedules.Where(s => s.IsFreezed).Select(s => s.Title);
                    var programNames = string.Join(", ", programFreezed);
                    ViewBag.Message = programNames + " have been freezed";
                }
            }

            if (_peopleBusiness.IsDelinquent(this.GetCurrentUserId(), clubId) || (userId != null && _peopleBusiness.IsDelinquent(userId.Value, clubId)))
            {
                if (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
                {
                    ViewBag.Message = Common.Constants.Register.DelinquentPartnerMessage;
                }
                else
                {
                    return RedirectToAction("DisplayDelinquentMessage", "SubDomain");
                }
            }

            var programRegisterStatus = _programBusiness.GetRegStatus(program);

            if (program.Season.Status != SeasonStatus.Test)
            {
                switch (programRegisterStatus)
                {
                    case ProgramRegStatus.NotStarted:
                        {
                            if (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
                            {
                                ViewBag.Message = "Program registration not started";
                                break;
                            }

                            var date = program.ProgramSchedules.First(s => !s.IsDeleted).RegistrationPeriod.RegisterStartDate;
                            var time = program.ProgramSchedules.First(s => !s.IsDeleted).RegistrationPeriod.RegisterStartTime;

                            var registerDateTime = date.Value.Add(time ?? new TimeSpan(0));

                            return RedirectToAction(actionName: "DisplayNotStartedMessage", controllerName: "SubDomain", routeValues:
                                new
                                {
                                    domain = model.ProgramDomain,
                                    clubDomain = model.ClubDomain,
                                    registerStartDate = registerDateTime,
                                });
                        }
                    case ProgramRegStatus.Closed:
                        {
                            if (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
                            {
                                ViewBag.Message = "Program registration has been closed";
                                break;
                            }
                            return RedirectToAction(actionName: "DisplayExpiredMessage", controllerName: "SubDomain", routeValues:
                                new
                                {
                                    domain = model.ProgramDomain,
                                    clubDomain = model.ClubDomain,
                                    expiration =
                                            program.ProgramSchedules.First().RegistrationPeriod.RegisterEndDate.Value.ToString(Constants.F_DateTime_WithComma)
                                });
                        }
                }
            }

            var orderInCart = Ioc.OrderBusiness.GetOrderInCart(model.UserId, this.GetCurrentClubId());
            var programIsLive = program.Season.Status == SeasonStatus.Live;

            bool isTestMode = false;
            if (orderInCart != null)
            {
                isTestMode = !orderInCart.IsLive;
            }
            else
            {
                isTestMode = program.Season.Status == SeasonStatus.Test;
            }

            var cookie = Request.Cookies.Get(_programBusiness.GetProgramPinUniqueKey(program, this.GetCurrentUserName()));


            if (_programBusiness.IsFull(program, isTestMode) && !isOverrideCapacity )
            {
                if (cookie == null && ists == null)
                {
                    if (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
                    {
                        ViewBag.Message = "Program capacity is full";
                    }
                    else
                    {
                        return RedirectToAction(actionName: "DisplayCompleteCapacityMessage", controllerName: "SubDomain", routeValues: new { programId = program.Id, programDomain = program.Domain, seasonDomain = program.Season.Domain, isWaitListAvailable = program.IsWaitListAvailable, disableCapacityRestriction = program.DisableCapacityRestriction });
                    }
                }
            }

            if (model.ProgramTypeCategory != ProgramTypeCategory.Subscription && model.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && model.ProgramSchedules.SelectMany(s => s.Charges).All(c => c.HasNoSession))
            {
                if (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
                {
                    ViewBag.Message = "Program that has no session left";
                }
                else
                    return RedirectToAction(actionName: "DisplayNoSessionMessage", controllerName: "SubDomain");
            }

            if (this.GetCurrentUserRoleType() != RoleCategoryType.Dashboard && program.HasRegisterPIN && !string.IsNullOrEmpty(program.RegisterPIN))
            {
                if ((cookie == null && ists == null) || (cookie != null && !cookie.Value.Equals(program.RegisterPIN, StringComparison.CurrentCultureIgnoreCase)))
                    return RedirectToAction("PinRestriction", "SubDomain", new { domain, seasonDomain, clubDomain });
            }

            if (ists.HasValue)
            {
                model.IsSelectProfile = true;
                model.ChargeDiscountsText = regServ.ManipulateOrderItem(model, program, ists.Value);

                regServ.ManipulatePlayer(model);
            }
            else
            {
                regServ.ManipulatePlayer(model);
                model.IsSelectProfile = false;
                regServ.ManipulateJbForm(model, program.JbForm, program.Id, true);
            }

            if (model.PlayerLists == null || !model.PlayerLists.Any())
            {
                model.ClubWaivers = program.ClubWaivers.Where(w => !w.IsDeleted).ToList();
            }
            var waivers = new List<OrderItemWaiverViewModel>();

            foreach (var item in model.ClubWaivers.Where(w => !w.IsDeleted))
            {
                waivers.Add(new OrderItemWaiverViewModel()
                {
                    Text = item.Text,
                    Name = item.Name,
                    WaiverId = item.Id,
                    WaiverConfirmationType = item.WaiverConfirmationType,
                    IsRequired = item.IsRequired,
                    Order = item.Order
                });
            }
            model.Waivers = waivers;

            regServ.ManipulatePaymentPlans(model);
            if (!model.PaymentPlanId.HasValue) { model.PaymentPlanId = 0; }

            var clubInfo = Ioc.ClubBusiness.GetHelpInformationSetting(club);

            model.CLubName = clubInfo.Name;
            model.ClubPhoneNumber = clubInfo.Phone;
            model.ClubEmail = clubInfo.Email;
            model.PhoneExtension = clubInfo.PhoneExtension;
            model.EnabledWaitList = program.IsWaitListAvailable;
            model.HidePhoneNumber = clubSetting != null ? (clubSetting.AppearanceSetting != null ? clubSetting.AppearanceSetting.HidePhoneNumber : false) : false;

            //Check capacity for each camp schedule

            if (program.TypeCategory == ProgramTypeCategory.Camp)
            {
                foreach (var schedule in model.ProgramSchedules)
                {
                    var orginalSchedule = program.ProgramSchedules.Where(s => !s.IsDeleted && s.Id == schedule.Id).FirstOrDefault();

                    schedule.IsFull = _programBusiness.IsCapacityFull(orginalSchedule.Charges.ToList()) && !program.DisableCapacityRestriction;
                }
            }


            if (program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                var subscriptionAttribute = Ioc.SubscriptionBusiness.GetSubscriptionAtribute(program.ProgramSchedules.Last(s => !s.IsDeleted));

                model.SubscriptionMessage = subscriptionAttribute.FullPayDiscountMessage;

                model.SubscriptionHasFullPayOption = subscriptionAttribute.HasPayInFullOption;

                model.PaymentPlanId = 1;
            }
            //if program is BeforeAfterCare
            if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                var firstProgramSchedule = program.ProgramSchedules.Where(s => !s.IsDeleted).FirstOrDefault();

                var beforeAfterCareAttribiute = Ioc.SubscriptionBusiness.GetBeforAfterCareAtribute(firstProgramSchedule);

                model.SubscriptionHasFullPayOption = beforeAfterCareAttribiute.HasPayInFullOption;

                model.PaymentPlanId = 1;
            }

            model.DynamicStyle = ClubService.Get().GetDynamicStyle(clubSetting != null ? clubSetting.AppearanceSetting : null);

            return View("Program", model);
        }

        [HttpPost, ValidateInput(false)]
        public virtual ActionResult Register(RegisterProgramModel model, string clubDomain, string seasonDomain, string domain, int? userId)
        {
            var regServ = RegisterService.Get();
            var currentClubId = this.GetCurrentClubId();
            var club = Ioc.ClubBusiness.Get(currentClubId);
            model.IsPostBack = true;
            var program = _programBusiness.Get(club.Id, seasonDomain, domain);
            var waivers = new List<OrderItemWaiverViewModel>();

            var isOrderOnline = this.GetCurrentUserRoleType() != RoleCategoryType.Dashboard;

            if (program.HasRegisterPIN)
            {
                model.PassedPin = Request.Cookies.Get(_programBusiness.GetProgramPinUniqueKey(program, this.GetCurrentUserName()))?.Value;
            }

            foreach (var state in ModelState.Keys.Where(k => k.StartsWith("day", StringComparison.OrdinalIgnoreCase)).ToList())
            {
                ModelState.Remove(state);
            }

            //ModelState.Remove("ChessTournament.Schedule");
            //ModelState.Remove("ChessTournament.Section");

            if (!model.IsSelectProfile && model.ProfileId.HasValue)
            {
                regServ.ManipulatePlayer(model);
                if (model.PlayerLists != null && model.PlayerLists.Any())
                {
                    ModelState.Clear();
                    var changedCharges = model.ProgramSchedules.SelectMany(c => c.Charges).Where(c => c.IsPriceChanged);
                    model = new RegisterProgramModel(program, model, userId);

                    foreach (var item in model.ClubWaivers.Where(w => !w.IsDeleted))
                    {
                        waivers.Add(new OrderItemWaiverViewModel()
                        {
                            Text = item.Text,
                            Name = item.Name,
                            WaiverId = item.Id,
                            IsRequired = item.IsRequired,
                            WaiverConfirmationType = item.WaiverConfirmationType,
                            Order = item.Order
                        });
                    }
                    model.Waivers = waivers;

                    regServ.ManipulatePaymentPlans(model);
                    if (!model.PaymentPlanId.HasValue) { model.PaymentPlanId = 0; }

                    regServ.ManipulateJbForm(model, program.JbForm, program.Id, model.ProfileId.HasValue && model.ProfileId == 0);

                    if (program.TypeCategory == ProgramTypeCategory.Subscription && model.DesiredStartDate.HasValue)
                    {
                        model.ChargeDiscountsText = regServ.ManipulateSubscriptionSchedule(model.ChargeDiscountsText, program, model.DesiredStartDate.Value);
                    }

                    model.DynamicStyle = ClubService.Get().GetDynamicStyle(club.Setting != null ? club.Setting.AppearanceSetting : null);

                    var clubInfo = Ioc.ClubBusiness.GetHelpInformationSetting(club);

                    model.CLubName = clubInfo.Name;
                    model.ClubPhoneNumber = clubInfo.Phone;
                    model.ClubEmail = clubInfo.Email;
                    model.PhoneExtension = clubInfo.PhoneExtension;

                    return View("Program", model);
                }
            }

            var season = _seasonBusiness.Get(seasonDomain, club.Id);

            if (_programBusiness.IsTeamChessTourney(program))
            {
                model.IsTeamRegistration = true;
            }

            CheckIndividualRegisterValidation(model);

            if (ModelState.IsValid)
            {

                var uTCDate = DateTime.UtcNow;
                var ists = model.ISTS.HasValue && model.ISTS.Value > 0 ? model.ISTS.Value : DateTimeHelper.DateTimetoTimeStamp(uTCDate);
                if (this.GetCurrentUserRole() == RoleCategory.Parent && model.ProfileId.HasValue && model.ProfileId.Value > 0)
                {
                    Ioc.UserProfileBusiness.EnsureUserIsProfileOwner(this.GetCurrentUserId(),
                        this.GetCurrentUserName(), model.ProfileId.Value);
                }
                bool isInstallmentOrder = false;
                var isModelStateInvalid = false;

                //isModelStateInvalid = regServ.ManipulatePlayerProfile(model, SectionsName.ParticipantSection, ModelState);

                var profileId = model.ProfileId == 0 ? null : model.ProfileId; // profile ke bayad edit shavad

                var playerProfile = model.Player;

                var errorParent = regServ.SetFormToParentDashboard(model.JbForm, profileId, model.UserId, ref playerProfile);

                if (playerProfile != null)
                {
                    model.Player = playerProfile;
                    model.ProfileId = playerProfile.Id;
                }

                if (!errorParent.Status)
                {
                    ManipulateParentDashboardValidations(model.JbForm, errorParent.Message);
                }

                if (!isModelStateInvalid || !errorParent.Status)
                {
                    var allErrors = ModelState.Values.SelectMany(v => v.Errors).ToArray().ToList();
                    if (allErrors != null && allErrors.Any())
                    {
                        model = new RegisterProgramModel(program, model, userId);
                        regServ.ManipulatePlayer(model);
                        foreach (var error in allErrors)
                            ModelState.AddModelError(error.ErrorMessage, error.Exception ?? new Exception());


                        model.DynamicStyle = ClubService.Get().GetDynamicStyle(club.Setting != null ? club.Setting.AppearanceSetting : null);
                        return View("Program", model);
                    }
                }

                //if (!Ioc.PlayerProfileBusiness.GetList(model.UserId).Where(p => p.Relationship == RelationshipType.Parent).Any())
                //{
                //    regServ.ManipulatePlayerProfile(model, SectionsName.ParentGuardianSection, ModelState);
                //    regServ.ManipulatePlayerProfile(model, SectionsName.Parent2GuardianSection, ModelState);
                //}

                if (model.PaymentPlanId.HasValue && model.PaymentPlanId.Value > 0)
                {
                    isInstallmentOrder = true;
                }

                regServ.CheckRestrictions(model, ModelState,isOrderOnline, program);
                if (!ModelState.IsValid && !model.IsTeamRegistration)
                {
                    ModelState.Clear();
                    model = new RegisterProgramModel(program, model, userId);
                    regServ.CheckRestrictions(model, ModelState, isOrderOnline, program);
                    model.DynamicStyle = ClubService.Get().GetDynamicStyle(club.Setting != null ? club.Setting.AppearanceSetting : null);

                    return View("Program", model);
                }

                bool isRestrictedEvent = (program.RegistrationMode == RegistrationMode.Restricted) ? true : false;
                model.IsEligibleUser = true;

                if (!isRestrictedEvent || (isRestrictedEvent && model.IsEligibleUser))
                {
                    var order = Ioc.OrderBusiness.GetOrderInCart(model.UserId, currentClubId) ?? new Order(program.Season.Status == SeasonStatus.Live);
                    order.ClubId = club.Id;
                    order.UserId = model.UserId;
                    order.OrderMode = model.OrderMode;
                    order.CompleteDate = uTCDate;

                    model.JbForm.Id = 0;

                    var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(model.ChargeDiscountsText);

                    var allCalendarItems = new List<CalendarSelectedItem>();
                    var calendarItems = new List<CalendarSelectedItem>();
                    if (model.ProgramTypeCategory == ProgramTypeCategory.Calendar)
                    {
                        allCalendarItems = JsonHelper.JsonDeserialize<List<CalendarSelectedItem>>(model.CalendarSelectedItems);
                    }

                    #region orderItem in each schedule
                    foreach (var schdule in model.ProgramSchedules)
                    {
                        if (program.TypeCategory == ProgramTypeCategory.Subscription)
                        {
                            regServ.ManipulateSubscriptionOrder(order, program, model, ists, uTCDate, isInstallmentOrder);
                        }
                        else if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
                        {
                            var modelScheduleId = charges.Select(c => c.ScheduleId).FirstOrDefault();

                            if (schdule.Id == modelScheduleId)
                            {
                                regServ.ManipulateBeforAfterCareOrder(order, program, model, ists, uTCDate, isInstallmentOrder, schdule.Id);
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else
                        {

                            if (model.SubscriptionMode == SubscriptionMode.DropIn)
                            {
                                if (charges.Any(c => c.ScheduleId == schdule.Id))
                                {
                                    regServ.ManipulateProgramDropIn(order, program, model, ists, uTCDate, isInstallmentOrder, schdule.Id);
                                }
                            }
                            else
                            {
                                foreach (var priceLabel in (model.ProgramTypeCategory == ProgramTypeCategory.Calendar ?
                                    charges.Where(c => c.ScheduleId == schdule.Id && c.Category == ChargeDiscountCategory.EntryFee).Take(1)
                                    : charges.Where(c => c.ScheduleId == schdule.Id && c.Category == ChargeDiscountCategory.EntryFee)))
                                {
                                    OrderItem orderItem;
                                    string itemName;
                                    if (model.ProgramTypeCategory == ProgramTypeCategory.Calendar)
                                    {
                                        calendarItems = allCalendarItems.Where(ci => ci.ScheduleId == schdule.Id).ToList();
                                    }

                                    var entryFeeItem = schdule.Charges.First(c => c.Id == priceLabel.ChargeId);
                                    var orderCharges = charges.Where(c => c.ScheduleId == schdule.Id && c.ParentChargeId == priceLabel.ChargeId)
                                           .Select(c => new OrderChargeDiscount()
                                           {
                                               Amount = (c.Category == ChargeDiscountCategory.EntryFee && model.OrderMode == OrderMode.Offline && entryFeeItem.Id == c.ChargeId && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : c.Amount,
                                               Category = c.Category,
                                               ChargeId = c.ChargeId,
                                               Name = c.Name,
                                               Description = c.Description,
                                               Subcategory = c.Subcategory,
                                           })
                                           .ToArray()
                                           .ToList();

                                    var orderItemWaivers = new List<OrderItemWaiver>();
                                    if ((!model.ISTS.HasValue || model.ISTS == 0) && model.Waivers != null)
                                    {
                                        var programWaivers = program.ClubWaivers.ToList();
                                        foreach (var item in model.Waivers)
                                        {
                                            var programWaiver = programWaivers.Single(p => p.Id == item.WaiverId);

                                            var orderItemWaiver = new OrderItemWaiver();

                                            orderItemWaiver.Signature = item.Signiture;
                                            orderItemWaiver.Agreed = item.Agreed;
                                            orderItemWaiver.Name = programWaiver.Name;
                                            orderItemWaiver.Text = programWaiver.Text;
                                            orderItemWaiver.IsRequired = programWaiver.IsRequired;
                                            orderItemWaiver.WaiverConfirmationType = programWaiver.WaiverConfirmationType;

                                            orderItemWaivers.Add(orderItemWaiver);
                                        }
                                    }

                                    itemName = _programBusiness.GetProgramName(program.Name, schdule.StartDate, schdule.EndDate, "");

                                    //Edit item
                                    if (model.ISTS.HasValue && order.RegularOrderItems.Any(i => i.ISTS == model.ISTS.Value && i.Name.StartsWith(itemName)))
                                    {
                                        var programWaivers = new RegisterProgramModel(program, model, userId);

                                        foreach (var item in programWaivers.ClubWaivers)
                                        {
                                            waivers.Add(new OrderItemWaiverViewModel()
                                            {
                                                Text = item.Text,
                                                Name = item.Name,
                                                WaiverId = item.Id,
                                                WaiverConfirmationType = item.WaiverConfirmationType,
                                                IsRequired = item.IsRequired,
                                                Order = item.Order
                                            });
                                        }

                                        model.Waivers = waivers;

                                        orderItem = order.RegularOrderItems.SingleOrDefault(item => item.ISTS == ists && item.Name.StartsWith(itemName));
                                        //Save chargeDiscount for order if it is coupon
                                        var cuopons = new List<OrderChargeDiscount>();
                                        if (orderItem.GetOrderChargeDiscounts().ToList().Count > 0)
                                        {
                                            var coupon = orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Coupon).ToList();
                                            if (coupon.Count > 0)
                                            {
                                                foreach (var item in coupon)
                                                {
                                                    cuopons.Add(item);
                                                }
                                            }
                                        }

                                        orderItem.OrderChargeDiscounts.Clear();
                                        orderItem.OrderChargeDiscounts = orderCharges;

                                        if (cuopons.Count > 0)
                                        {
                                            foreach (var cuopon in cuopons)
                                            {
                                                orderItem.OrderChargeDiscounts.Add(new OrderChargeDiscount()
                                                {
                                                    Amount = cuopon.Amount,
                                                    Category = cuopon.Category,
                                                    ChargeId = cuopon.ChargeId,
                                                    Name = cuopon.Name,
                                                    Description = cuopon.Description,
                                                    Subcategory = cuopon.Subcategory,
                                                    CouponId = cuopon.CouponId,
                                                    Coupon = cuopon.Coupon,

                                                });
                                            }
                                        }

                                        orderItem.TotalAmount = orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category > 0).Sum(c => c.Amount) -
                                                                orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category < 0).Sum(c => c.Amount);

                                        orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : priceLabel.Amount;

                                        if (model.ProgramTypeCategory != ProgramTypeCategory.Calendar)
                                        {
                                            itemName += " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "") : priceLabel.Amount.ToString().Replace(".00", "").Replace(".0", ""));
                                        }

                                        orderItem.Name = itemName;
                                        orderItem.EntryFeeName = priceLabel.Name;
                                        orderItem.JbForm.JsonElements = model.JbForm.JsonElements;

                                        //orderItem.FirstName = orderItem.Player.Contact.FirstName;
                                        //orderItem.LastName = orderItem.Player.Contact.LastName;

                                        if (program.TypeCategory == ProgramTypeCategory.Camp)
                                        {
                                            Ioc.OrderSessionBusiness.CreateSession(orderItem, priceLabel.ChargeId);
                                        }
                                    }
                                    else
                                    {

                                        if (model.ProgramTypeCategory != ProgramTypeCategory.Calendar)
                                        {
                                            itemName += " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "") : priceLabel.Amount.ToString().Replace(".00", "").Replace(".0", ""));
                                        }
                                        orderItem = new OrderItem();

                                        orderItem.SeasonId = season.Id;
                                        orderItem.Name = itemName;
                                        orderItem.DateCreated = uTCDate;
                                        orderItem.ISTS = ists;
                                        orderItem.PlayerId = model.Player.Id;
                                        orderItem.FirstName = model.Player.Contact.FirstName;
                                        orderItem.LastName = model.Player.Contact.LastName;


                                        orderItem.PassedPin = model.PassedPin;

                                        orderItem.JbFormId = 0;

                                        var parentFormId = program.JbForm.Id;

                                        model.JbForm.ParentId = parentFormId;

                                        orderItem.JbForm = model.JbForm;



                                        orderItem.ProgramTypeCategory = program.TypeCategory;
                                        orderItem.ProgramScheduleId = schdule.Id;
                                        orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schdule.Id);
                                        orderItem.Start = schdule.StartDate;
                                        orderItem.End = schdule.EndDate;

                                        orderItem.ItemStatus = OrderItemStatusCategories.initiated;
                                        orderItem.EntryFeeName = priceLabel.Name;
                                        orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : priceLabel.Amount;

                                        orderItem.OrderChargeDiscounts = orderCharges;

                                        orderItem.OrderItemWaivers = orderItemWaivers;

                                        orderItem.TotalAmount = orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category > 0).Sum(c => c.Amount)
                                                               - orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category < 0).Sum(c => c.Amount);

                                        //add session for orderitem 
                                        if (program.TypeCategory == ProgramTypeCategory.Camp)
                                        {
                                            Ioc.OrderSessionBusiness.CreateSession(orderItem, priceLabel.ChargeId);
                                        }

                                        order.OrderItems.Add(orderItem);
                                    }

                                    if (model.ProgramTypeCategory == ProgramTypeCategory.Calendar)
                                    {
                                        throw new NotImplementedException();
                                    }

                                    //if (orderItem.OrderSessions.Any(ors => !calendarItems.Any(ci => ci.Id == ors.ScheduleSectionId)))
                                    //{
                                    //    var removs = orderItem.ProgramSchedule.Sessions.Where(s => !calendarItems.Any(c => c.Id == s.Id)
                                    //        && orderItem.OrderSessions.Any(c => c.ScheduleSectionId == s.Id)).Select(sec => sec.Id).ToList();

                                    //    var sessions = orderItem.OrderSessions.Where(r => removs.Contains(r.ScheduleSectionId.Value));
                                    //    if (sessions.Any())
                                    //    {
                                    //        Ioc.OrderBusiness.DeleteSessions(sessions.ToList());
                                    //    }
                                    //}

                                    if (program.TypeCategory == ProgramTypeCategory.ChessTournament)
                                    {

                                        orderItem.Start = model.ChessTournament.ScheduleStartDate.HasValue ? model.ChessTournament.ScheduleStartDate.Value : schdule.StartDate;
                                        orderItem.End = model.ChessTournament.ScheduleEndDate.HasValue ? model.ChessTournament.ScheduleEndDate.Value : schdule.EndDate;


                                        if (!model.ISTS.HasValue || orderItem.OrderItemChess == null)
                                        {
                                            orderItem.OrderItemChess = new OrderItemChess();
                                        }
                                        orderItem.OrderItemChess.Schedule = model.ChessTournament.Schedule;
                                        orderItem.OrderItemChess.Section = model.ChessTournament.Section;
                                        string byesRequested = string.Empty;
                                        if (model.ChessTournament.ByeRequests != null)
                                            for (int i = 0; i < model.ChessTournament.ByeRequests.Length; ++i)
                                            {
                                                if (model.ChessTournament.ByeRequests[i])
                                                {
                                                    if (string.IsNullOrEmpty(byesRequested))
                                                    {
                                                        byesRequested = (i + 1).ToString();
                                                    }
                                                    else
                                                    {
                                                        byesRequested = byesRequested + Constants.S_Comma + Constants.S_Space + (i + 1).ToString();
                                                    }
                                                }
                                            }
                                        orderItem.OrderItemChess.Byes = byesRequested;
                                        var finded = false;
                                        foreach (var section in model.JbForm.Elements)
                                        {
                                            foreach (var element in (section as JbSection).Elements)
                                            {
                                                if (element is ICustomElement)
                                                    if (element is USCFInfo)
                                                    {
                                                        orderItem.OrderItemChess.UscfId = (element as USCFInfo).UscfId;
                                                        orderItem.OrderItemChess.UscfExpiration = (element as USCFInfo).UscfExpiration;
                                                        orderItem.OrderItemChess.UscfRatingQuick = (element as USCFInfo).UscfRatingQuick;
                                                        orderItem.OrderItemChess.UscfRatingReg = (element as USCFInfo).UscfRatingReg;

                                                        finded = true;
                                                        break;
                                                    }
                                            }
                                            if (finded)
                                                break;
                                        }

                                    }

                                    orderItem.PaymentPlanType = isInstallmentOrder ? PaymentPlanType.Installment : PaymentPlanType.FullPaid;
                                    orderItem.PaymentPlanId = isInstallmentOrder ? model.PaymentPlanId : null;
                                }
                            }
                        }
                    }
                    #endregion

                    if (model.ISTS != null)
                    {
                        List<OrderItem> deleteItems;
                        if (model.ProgramTypeCategory != ProgramTypeCategory.Calendar)
                        {
                            deleteItems = order.OrderItems.Where(item => item.ISTS == model.ISTS &&
                                 !item.OrderChargeDiscounts.Any(oc => !oc.IsDeleted && charges.Any(c => c.Category == ChargeDiscountCategory.EntryFee && c.ChargeId == oc.ChargeId))).ToList();

                        }
                        else
                        {
                            deleteItems = order.OrderItems.Where(item => item.ISTS == model.ISTS && !item.OrderSessions.Any()).ToList();
                        }
                        if (deleteItems.Any())
                        {
                            _orderItemBusiness.DeleteItems(deleteItems);
                        }
                    }

                    order.PaymentPlanType = order.RegularOrderItems.Any(o => o.PaymentPlanType == PaymentPlanType.Installment) ? PaymentPlanType.Installment : PaymentPlanType.FullPaid;
                    OperationStatus dbRes;

                    if (order.Id == 0)
                    {
                        dbRes = Ioc.OrderBusiness.Create(order);
                    }
                    else
                    {
                        dbRes = Ioc.OrderBusiness.Update(order);
                    }

                    foreach (var item in order.RegularOrderItems.Where(o => (o.ProgramTypeCategory == ProgramTypeCategory.Subscription || o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && !o.OrderSessions.Any()))
                    {
                        var scheduleIsCombo = item.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both ? true : false;

                        switch (item.Mode)
                        {
                            case OrderItemMode.Noraml:
                                {
                                    Ioc.OrderSessionBusiness.Create(item, model.Days, null, null, scheduleIsCombo);
                                }
                                break;
                            case OrderItemMode.DropIn:
                                {
                                    Ioc.OrderSessionBusiness.Create(item, charges.Select(c => c.Id).ToList(), scheduleIsCombo);
                                }
                                break;
                            case OrderItemMode.PunchCard:
                                break;
                            default:
                                {

                                }
                                break;
                        }
                    }

                    if (dbRes.Status)
                    {
                        return RedirectToAction("Index", "Cart", new { userId = userId });
                    }
                    else
                    {
                        ModelState.AddModelError("JbForm.Elements[0].Elements[0].Value", "sorry,register failed.please try again later");
                    }
                }
                else
                {
                    if (isRestrictedEvent)
                    {
                        var notifyMdodel = new NotificationItemViewModel
                        {
                            Title = Constants.Notification_Restricted,
                            Content = string.Format(Constants.Event_Online_Registration_Restricted, model.ProgramDomain),
                        };
                    }
                }
            }
            else
            {
                var allErrors = ModelState.Values.SelectMany(v => v.Errors).ToArray().ToList();
                model = new RegisterProgramModel(program, model, userId);

                regServ.ManipulatePlayer(model);
                foreach (var error in allErrors)
                {
                    ModelState.AddModelError(error.ErrorMessage, error.Exception ?? new Exception());
                    if (error.ErrorMessage == "We have encountered an internal issue and need to recapture the registration information. If this issue happens again, please notify your administrator.")
                    {
                        model.TargetPageStep = RegisterPageStep.registrationOptions;
                    }
                }
            }

            model.DynamicStyle = ClubService.Get().GetDynamicStyle(club.Setting != null ? club.Setting.AppearanceSetting : null);
            var programWaivers2 = new RegisterProgramModel(program, model, userId);

            foreach (var item in programWaivers2.ClubWaivers)
            {
                waivers.Add(new OrderItemWaiverViewModel()
                {
                    Text = item.Text,
                    Name = item.Name,
                    WaiverId = item.Id,
                    WaiverConfirmationType = item.WaiverConfirmationType,
                    IsRequired = item.IsRequired,
                    Order = item.Order
                });
            }
            model.Waivers = waivers;


            return View("Program", model);
        }

        private void CheckIndividualRegisterValidation(RegisterProgramModel model)
        {
            var program = _programBusiness.Get(model.ProgramId);

            if ((program.TypeCategory == ProgramTypeCategory.Subscription || program.TypeCategory == ProgramTypeCategory.BeforeAfterCare) && model.SubscriptionMode == SubscriptionMode.Normal)
            {
                if (model.Days != null && model.Days.Any())
                {
                    var repetitiveDay = model.Days.GroupBy(p => p).Where(g => g.Count() > 1).Select(g => g.Key).ToList();

                    if (repetitiveDay.Count > 0)
                    {
                        ModelState.AddModelError("", Validation.RegistrationGeneralErrorMessage);
                    }
                }
                else
                {
                    ModelState.AddModelError("", Validation.RegistrationGeneralErrorMessage);
                }

                if (!model.DesiredStartDate.HasValue)
                {
                    ModelState.AddModelError("", Validation.RegistrationGeneralErrorMessage);
                }
            }
        }
        [HttpGet]
        [Authorize(Roles = "Parent")]
        public virtual ActionResult MultipleRegister(string seasonDomain, long? ists, SchoolGradeType? grade, string playerGrade, int? playerAge, string gender, MultipleRegisterFilterViewModel filter, string tab = "", RegistrationStep step = RegistrationStep.Step1)
        {
            var programBussiness = _programBusiness;

            var club = this.GetCurrentClubBaseInfo();

            var isPreRegistratioinMode = false;
            var isLotteryMode = false;

            var season = _seasonBusiness.Get(seasonDomain, club.Id);
            var registerService = RegisterService.Get();
            var currentUserId = this.GetCurrentUserId();

            if (season == null)
            {
                PageNotFound();
            }

            if (this.GetCurrentUserRole() != RoleCategory.Parent)
            {
                return RedirectToAction(actionName: "DisplayNotAccessibleMessage", controllerName: "SubDomain", routeValues:
                                 new
                                 {
                                     role = RoleCategory.Admin,
                                     message = "You do not have access to this page. Please login as a parent.",
                                 });
            }

            var seasonRegStatus = _seasonBusiness.GetRegStatus(season);
            var seasonSettings = season.Setting;

            if (seasonSettings != null)
            {
                switch (seasonRegStatus)
                {
                    case SeasonRegStatus.NotOpened:
                        {
                            return RedirectToAction(actionName: "DisplayNotOpenedSeasonMessage", controllerName: "SubDomain", routeValues:
                                new
                                {
                                    seasonName = season.Title,
                                    registerStartDate = season.Setting.SeasonProgramInfoSetting.GeneralRegistrationOpens
                                });
                        }
                    case SeasonRegStatus.EarlyClosed:
                        {
                            return RedirectToAction(actionName: "DisplayEarlyClosedSeasonMessage", controllerName: "SubDomain", routeValues:
                                 new
                                 {
                                     seasonName = season.Title,
                                     generalRegisterStartDate = season.Setting.SeasonProgramInfoSetting.GeneralRegistrationOpens,
                                 });
                        }
                    case SeasonRegStatus.GeneralClosed:
                        {
                            return RedirectToAction(actionName: "DisplayGeneralClosedSeasonMessage", controllerName: "SubDomain", routeValues:
                              new
                              {
                                  seasonName = season.Title,
                                  lateRegisterStartDate = season.Setting.SeasonProgramInfoSetting.LateRegistrationOpens,
                                  lateRegisterEndDate = season.Setting.SeasonProgramInfoSetting.LateRegistrationCloses,
                                  lateRegistrationFee = season.Setting.SeasonProgramInfoSetting.LateRegistrationFee
                              });
                        }
                    case SeasonRegStatus.Closed:
                        {
                            return RedirectToAction(actionName: "DisplayClosedSeasonMessage", controllerName: "SubDomain", routeValues:
                              new
                              {
                                  clubId = club.Id,
                                  seasonName = season.Title,
                              });
                        }
                    case SeasonRegStatus.EarlyOpen:
                        {
                            if (seasonSettings.SeasonProgramInfoSetting != null && seasonSettings.SeasonProgramInfoSetting.HasEarlyRegisterPIN)
                            {
                                var cookie = Request.Cookies.Get((club.Domain + seasonDomain + this.GetCurrentUserName() + "Pin").ToLower());

                                if (!(cookie != null && cookie.Value.Equals(seasonSettings.SeasonProgramInfoSetting.EarlyRegisterPIN, StringComparison.OrdinalIgnoreCase)))
                                {
                                    return RedirectToAction(actionName: "SeasonPinRestriction", controllerName: "SubDomain", routeValues:
                                      new
                                      {
                                          seasonDomain = season.Domain,
                                          clubDomain = club.Domain
                                      });
                                }
                            }
                        }
                        break;
                    case SeasonRegStatus.GeneralOpen:
                        {
                            if (seasonSettings.SeasonProgramInfoSetting != null && seasonSettings.SeasonProgramInfoSetting.HasGeneralRegisterPIN)
                            {
                                var cookie = Request.Cookies.Get((club.Domain + seasonDomain + this.GetCurrentUserName() + "Pin").ToLower());

                                if (!(cookie != null && cookie.Value.Equals(seasonSettings.SeasonProgramInfoSetting.GeneralRegisterPIN, StringComparison.OrdinalIgnoreCase)))
                                {
                                    return RedirectToAction(actionName: "SeasonPinRestriction", controllerName: "SubDomain", routeValues:
                                      new
                                      {
                                          seasonDomain = season.Domain,
                                          clubDomain = club.Domain
                                      });
                                }
                            }
                        }
                        break;
                    case SeasonRegStatus.LateOpen:
                        {
                            if (seasonSettings.SeasonProgramInfoSetting != null && seasonSettings.SeasonProgramInfoSetting.HasLateRegisterPIN)
                            {
                                var cookie = Request.Cookies.Get((club.Domain + seasonDomain + this.GetCurrentUserName() + "Pin").ToLower());

                                if (!(cookie != null && cookie.Value.Equals(seasonSettings.SeasonProgramInfoSetting.LateRegisterPIN, StringComparison.OrdinalIgnoreCase)))
                                {
                                    return RedirectToAction(actionName: "SeasonPinRestriction", controllerName: "SubDomain", routeValues:
                                      new
                                      {
                                          seasonDomain = season.Domain,
                                          clubDomain = club.Domain
                                      });
                                }
                            }
                        }
                        break;
                    case SeasonRegStatus.PreRegisterOpen:
                        {
                            isPreRegistratioinMode = true;
                        }
                        break;
                    case SeasonRegStatus.LotteryOpen:
                        {
                            isLotteryMode = true;
                        }
                        break;
                    default:
                        break;
                }



                if (_peopleBusiness.IsDelinquent(this.GetCurrentUserId(), club.Id))
                {
                    //We don`t have offline registration for admins then execute else part of condition in anytime
                    if (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
                    {
                        ViewBag.Message = Common.Constants.Register.DelinquentPartnerMessage;
                    }
                    else
                    {
                        return RedirectToAction("DisplayDelinquentMessage", "SubDomain");
                    }
                }

            }

            var lastStep = step;

            var hasWaiver = _seasonBusiness.GetSeasonWaivers(season).Any();
            var hasPaymentPlant = _seasonBusiness.HasSeasonMultRegisterPaymentPlan(season, JbUserService.GetCurrentUserId());

            var model = new MultipleRegisterViewModel()
            {
                SeasonDomain = seasonDomain,
                SeasonTitle = season.Title,
                HasWaiver = hasWaiver,
                HasPaymentPlan = hasPaymentPlant,
                ClubDomain = club.Domain
            };


            if (ists.HasValue)
            {
                var orderItems = new List<OrderItem>();
                orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated).ToList();

                var followUpFormsPrograms = _followupFormBusiness.GetProgramClubFormTemplateFromOrderItem(orderItems);

                if (followUpFormsPrograms.Any(f => f.FollowUpFormMode == FollowUpFormMode.Online))
                {
                    model.HasFollowUpForms = true;
                }
            }

            model.IsPreRegisterationMode = isPreRegistratioinMode;
            model.IsLotteryMode = isLotteryMode;

            if (isPreRegistratioinMode)
            {
                model.GeneralOpenRegDate = _seasonBusiness.GetGeneralRegOpenDate(season);
            }

            if (isLotteryMode)
            {
                model.LotteryEndDate = _seasonBusiness.GetLotteryCloseDate(season);
            }

            switch (step)
            {
                case RegistrationStep.Step1:
                    {
                        if (seasonSettings != null && seasonSettings.SeasonAdditionalSetting != null)
                        {
                            model.IsWaitlistEnabled = seasonSettings.SeasonAdditionalSetting.IsWaitListAvailable;
                        }

                        model.AllGrades = ManipulateGrades();

                        var orderItems = new List<OrderItem>();

                        if (ists.HasValue)
                        {
                            orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated).ToList();

                            lastStep = orderItems.Min(o => o.LastStep);
                        }


                        model.LastStep = lastStep;
                        var tabId = !string.IsNullOrEmpty(tab) ? tab : filter.Tab;

                        model.Filter = new MultipleRegisterFilterViewModel(club.Id, tabId);
                        model.Programs = GetMutipleRegisterPrograms(season.Id, orderItems, grade, playerGrade, playerAge, gender, ists, filter);

                        model.Programs.ForEach(p => p.Checked = p.Tuitions.Any(t => t.Checked));


                        return View("_MultipleRegisterStep1", model);
                    }
                case RegistrationStep.Step2:
                    {
                        if (ists != null)
                        {
                            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated).ToList();

                            var orderItem = orderItems.FirstOrDefault();

                            if (orderItem != null)
                            {
                                lastStep = orderItems.Min(o => o.LastStep);
                            }
                        }

                        model.LastStep = lastStep;

                        model.PlayerList = ManipulatePlayers(currentUserId);

                        model.FormId = _seasonBusiness.GetSeasonRegistrationForm(season).JbForm_Id;

                        return View("_MultipleRegisterStep2", model);
                    }
                case RegistrationStep.Step3:
                    {

                        var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated).ToList();

                        var orderItem = orderItems.FirstOrDefault();

                        if (orderItem != null)
                        {
                            lastStep = orderItems.Min(o => o.LastStep);
                        }

                        var jbForm = _seasonBusiness.GetSeasonRegistrationForm(season).JbForm;
                        int jbFormId = 0;

                        if (lastStep >= RegistrationStep.Step3)
                        {
                            if (orderItem != null)
                            {
                                jbForm = orderItem.JbForm;
                                if (orderItem.JbFormId != null) jbFormId = orderItem.JbFormId.Value;
                            }
                        }

                        registerService.ManipulateJbForm(jbForm, currentUserId, orderItem.PlayerId, orderItem.ProgramSchedule.ProgramId, lastStep >= RegistrationStep.Step3);

                        int profileId = orderItem.PlayerId.HasValue ? orderItem.PlayerId.Value : 0;

                        model.Form = jbForm;
                        model.Form.Id = jbFormId;
                        model.FormId = jbFormId;
                        model.Form.VisibleMode = VisibilityMode.Online;

                        model.SelectedPlayer = profileId;

                        model.LastStep = lastStep;

                        var programSchedules = orderItems.Select(o => o.ProgramSchedule).ToList();

                        if (programSchedules.Any(p => programBussiness.HasGenderRestriction(p)))
                        {
                            Ioc.FormBusiness.AddRequiredToGender(model.Form);
                        }

                        if (programSchedules.Any(p => programBussiness.HasGradeRestriction(p)))
                        {
                            Ioc.FormBusiness.AddRequiredToGrade(model.Form);
                        }

                        if (programSchedules.Any(p => programBussiness.HasAgeRestriction(p)))
                        {
                            Ioc.FormBusiness.AddRequiredToDob(model.Form);
                        }

                        return View("_MultipleRegisterStep3", model);
                    }
                case RegistrationStep.Step4:
                    {
                        var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated).ToList();

                        var orderItem = orderItems.FirstOrDefault();
                        var programIds = new List<long>();

                        if (orderItem != null)
                        {
                            lastStep = orderItems.Min(o => o.LastStep);
                            programIds = orderItems.Select(i => i.ProgramSchedule.ProgramId).ToList();
                        }

                        model.LastStep = lastStep;
                        var followUpFormsPrograms = _followupFormBusiness.GetProgramClubFormTemplateFromOrderItem(orderItems);
                        followUpFormsPrograms = followUpFormsPrograms.Distinct();

                        if (followUpFormsPrograms.Any(f => f.FollowUpFormMode == FollowUpFormMode.Online))
                        {
                            model.FollowUpFormItems = followUpFormsPrograms.Where(f => f.FollowUpFormMode == FollowUpFormMode.Online).Select(f => new FollowUpFormItemModel
                            {
                                FormId = f.JbForm.Id,
                                Name = f.Title,
                                AssignedProgramsName = _followupFormBusiness.GetFormProgramsName(f.Id, programIds)
                            }).ToList();


                            if (orderItem != null && lastStep >= RegistrationStep.Step4)
                            {
                                var jbForms = orderItems.SelectMany(i => i.FollowupForms.Select(f => f.JbForm)).DistinctBy(o => o.ParentId);

                                model.FollowUpForms.AddRange(jbForms);
                            }
                            else
                            {
                                model.FollowUpForms.AddRange(followUpFormsPrograms.Where(f => f.FollowUpFormMode == FollowUpFormMode.Online).Select(f => f.JbForm));
                            }
                        }

                        return View("_MultipleRegisterStep4", model);
                    }
                case RegistrationStep.Step5:
                    {
                        model.Waivers = _seasonBusiness.GetSeasonWaivers(season).Select(w =>
                                new RegisterWaiverItemModel
                                {
                                    Name = w.Name,
                                    Text = w.Text,
                                    WaiverId = w.Id,
                                    WaiverConfirmationType = w.WaiverConfirmationType,
                                    IsRequired = w.IsRequired,
                                    Order = w.Order
                                })
                            .ToList();

                        var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated).ToList();

                        var orderItem = orderItems.FirstOrDefault();

                        if (orderItem != null)
                        {
                            lastStep = lastStep = orderItems.Min(o => o.LastStep);
                        }

                        model.LastStep = lastStep;

                        return View("_MultipleRegisterStep5", model);
                    }
                case RegistrationStep.Step6:
                    {
                        if (ists != null)
                        {
                            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated).ToList();
                            var orderItem = orderItems.FirstOrDefault();
                            model.PaymentPlanLists = new List<PaymentPlan>();

                            var paymentPlans = _seasonBusiness.GetMultRegisterPaymentPlan(season, currentUserId);
                            model.PaymentPlanLists.AddRange(paymentPlans);

                            if (orderItem != null)
                            {
                                lastStep = lastStep = orderItems.Min(o => o.LastStep);
                            }
                        }

                        model.LastStep = lastStep;

                        return View("_MultipleRegisterStep6", model);
                    }
                default:
                    break;
            }

            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Parent")]
        public virtual ActionResult MultipleRegister(MultipleRegisterViewModel model, long? ists, SchoolGradeType? grade, string gender, MultipleRegisterFilterViewModel filter, RegistrationStep step = RegistrationStep.Step1)
        {
            var registerService = RegisterService.Get();
            var formBusiness = Ioc.FormBusiness;

            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
            var currentUserId = this.GetCurrentUserId();

            var isOrderOnline = this.GetCurrentUserRoleType() != RoleCategoryType.Dashboard;

            var season = _seasonBusiness.Get(model.SeasonDomain, club.Id);
            var tab = "";
            var tabId = !string.IsNullOrEmpty(tab) ? tab : filter.Tab;
            model.Filter = new MultipleRegisterFilterViewModel(club.Id, tabId);

            switch (step)
            {
                case RegistrationStep.Step1:
                    {
                        var selectedTuitions = model.SelectedTuitions.Select(t => t.Id).ToList();

                        var chargesForDelete = model.SelectedTuitions.SelectMany(t => t.Charges).Where(c => !c.IsChecked);

                        model.SelectedTuitions.ForEach(t => t.Charges.RemoveAll(c => !c.IsChecked));

                        model.SelectedTuitions.RemoveAll(t => t.Id < 1);
                        model.SelectedTuitions = model.SelectedTuitions.DistinctBy(d => d.Id).ToList();

                        var duplicateEnrollmentPolicy = _clubSettingBusiness.GetDuplicateEnrollmentPolicy(club.Id);
                        var conflictPrograms = new Dictionary<long, string>();

                        var selectedPrograms = _programBusiness.GetList().Where(s => s.ProgramSchedules.SelectMany(c => c.Charges).Any(c => selectedTuitions.Contains(c.Id))).ToList();

                        if (model.SelectedItems != null && model.SelectedItems.Any())
                        {
                            foreach (var item in model.SelectedItems)
                            {
                                if (!selectedPrograms.Select(p => p.Id).Contains(item))
                                {
                                    var program = _programBusiness.Get(item);

                                    if (program != null)
                                    {
                                        ModelState.AddModelError("", string.Format("Please select a tution label for {0}", program.Name));
                                    }
                                }
                            }

                            if (ModelState.IsValid && duplicateEnrollmentPolicy.PreventOtherClasses && selectedPrograms.Count > 1)
                            {
                                foreach (var item in model.SelectedItems)
                                {
                                    var programIds = model.SelectedItems.Where(s => s != item).ToList();
                                    var conflictProgramMessage = _registerBusiness.IsConflictPrograms(item, programIds);

                                    if (!string.IsNullOrEmpty(conflictProgramMessage))
                                    {
                                        conflictPrograms.Add(item, conflictProgramMessage);  
                                        ModelState.AddModelError("Tuitions", CartConstants.ConflictingClassSchedule);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (model.SelectedTuitions == null || !model.SelectedTuitions.Any(t => t.Id > 0))
                            {
                                ModelState.AddModelError("Tuitions", "Please select at least one program.");
                            }
                        }
                        


                        if (ModelState.IsValid)
                        {
                            if (ists.HasValue)
                            {
                                EditStep1(model, ists.Value);
                            }
                            else
                            {
                                ists = AddStep1(model);
                            }

                            return RedirectToAction("MultipleRegister", new { seasonDomain = model.SeasonDomain, ists = ists, grade = grade, step = RegistrationStep.Step2 });
                        }

                        model.AllGrades = ManipulateGrades();

                        var orderItems = new List<OrderItem>();

                        if (ists.HasValue)
                        {
                            orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated).ToList();
                        }

                        model.Programs = GetMutipleRegisterPrograms(season.Id, orderItems, grade, null, null, gender, ists, filter);

                        model.Programs.ForEach(p => p.Checked = model.SelectedItems.Contains(p.Id));
                        model.Programs.SelectMany(p => p.Tuitions).ToList().ForEach(t => t.Checked = model.SelectedTuitions.Select(tu => tu.Id).Contains(t.Id));
                        model.Programs.SelectMany(p => p.Charges).ToList().ForEach(t => t.Checked = model.SelectedTuitions.SelectMany(tu => tu.Charges).Select(ch => ch.Id).Contains(t.Id));

                        if (conflictPrograms.Any())
                        {
                            foreach (var item in conflictPrograms)
                            {
                                model.Programs.First(p => p.Id == item.Key).ConflictProgramMessage = item.Value;
                                model.Programs.First(p => p.Id == item.Key).IsConflictProgram = true;
                            }
                        }

                        return View("_MultipleRegisterStep1", model);
                    }
                case RegistrationStep.Step2:
                    {
                        if (!model.SelectedPlayer.HasValue)
                        {
                            ModelState.AddModelError("PlayerProfile", "Please select a profile or register someone else.");
                        }

                        if (ModelState.IsValid)
                        {
                            //Save step 2 (Update order items)
                            if (ists != null)
                            {
                                AddEditStep2(model, false, ists.Value);

                                return RedirectToAction("MultipleRegister",
                                    new
                                    {
                                        seasonDomain = model.SeasonDomain,
                                        ists = ists,
                                        grade = grade,
                                        step = RegistrationStep.Step3
                                    });
                            }
                        }

                        return View("_MultipleRegisterStep2", model);
                    }
                case RegistrationStep.Step3:
                    {

                        var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated);
                        PlayerProfile playerProfile = null;

                        var programs = orderItems.Select(o => o.ProgramSchedule.Program);

                        var playerGenderForRequest = string.Empty;
                        var playerGender = formBusiness.GetPlayerGender(model.Form);

                        var playerGradeObjectForRequest = string.Empty;
                        var playerGradeObject = formBusiness.GetPlayerGradeObject(model.Form);//!!!!

                        int? playerAgeForRequsest = null;
                        var playerDob = formBusiness.GetParticipantDoB(model.Form);
                        if (playerDob.HasValue)
                        {
                            playerAgeForRequsest = DateTimeHelper.CalculateAge(playerDob.Value);
                        }

                        if (playerGradeObject != null)
                        {
                            playerGradeObjectForRequest = playerGradeObject.ToString();
                        }
                        model.Form.VisibleMode =  VisibilityMode.Online;

                        registerService.CheckDobValidation(model.Form, ModelState);
                        programs.ToList().ForEach(f => registerService.CheckGradeRestriction(model.Form, f.ProgramSchedules.First(s => !s.IsDeleted), ModelState, isOrderOnline));
                        programs.ToList().ForEach(f => registerService.CheckGenderRestriction(model.Form, f.ProgramSchedules.First(s => !s.IsDeleted), ModelState, isOrderOnline));
                        programs.ToList().ForEach(f => registerService.CheckAgeRestriction(model.Form, f.ProgramSchedules.First(s => !s.IsDeleted), ModelState, isOrderOnline));

                        var modelErrors = ModelState.Values.SelectMany(v => v.Errors)
                              .Select(v => v.ErrorMessage + " " + v.Exception).ToList().ConvertAll(e => e.ToLower());

                        var isGradeResticted = modelErrors.Any(e => e.Contains("grade"));
                        var isGenderRestricted = modelErrors.Any(e => e.Contains("gender") || e.Contains("male") || e.Contains("female"));
                        var isAgeRestricted = modelErrors.Any(e => e.Contains("years"));

                        playerGenderForRequest = playerGender?.ToString();

                        if (isGenderRestricted || isGradeResticted || isAgeRestricted)
                        {
                            return RedirectToAction("MultipleRegister", new { seasonDomain = model.SeasonDomain, ists = ists, grade = grade, step = RegistrationStep.Step1, gender = playerGenderForRequest, playerGrade = playerGradeObjectForRequest, playerAge = playerAgeForRequsest });
                        }

                        if (ModelState.IsValid)
                        {
                            model.SelectedPlayer = model.SelectedPlayer == 0 ? null : model.SelectedPlayer;
                            var parentDashboardStatus = registerService.SetFormToParentDashboard(model.Form, model.SelectedPlayer, currentUserId, ref playerProfile);

                            if (playerProfile != null)
                                model.SelectedPlayer = playerProfile.Id;

                            if (!parentDashboardStatus.Status)
                            {
                                ManipulateParentDashboardValidations(model.Form, parentDashboardStatus.Message);
                            }

                            if (ModelState.IsValid)
                            {
                                var hasWaiver = _seasonBusiness.GetSeasonWaivers(season).Any();
                                var hasPaymentPlant = _seasonBusiness.HasSeasonMultRegisterPaymentPlan(season, JbUserService.GetCurrentUserId());

                                var followUpFormsPrograms = _followupFormBusiness.GetProgramClubFormTemplateFromOrderItem(orderItems.ToList());
                                var hasFollowUpForms = followUpFormsPrograms.Any(f => f.FollowUpFormMode == FollowUpFormMode.Online);

                                AddEditStep3(model, ists.Value, playerProfile, hasWaiver, hasPaymentPlant, hasFollowUpForms);

                                if (hasFollowUpForms)
                                {
                                    return RedirectToAction("MultipleRegister", new { seasonDomain = model.SeasonDomain, ists = ists, grade = grade, step = RegistrationStep.Step4 });
                                }
                                else if (hasWaiver)
                                {
                                    return RedirectToAction("MultipleRegister", new { seasonDomain = model.SeasonDomain, ists = ists, grade = grade, step = RegistrationStep.Step5 });
                                }
                                else
                                {
                                    if (hasPaymentPlant)
                                    {
                                        return RedirectToAction("MultipleRegister", new { seasonDomain = model.SeasonDomain, ists = ists, grade = grade, step = RegistrationStep.Step6 });
                                    }
                                    else
                                    {
                                        return RedirectToAction("Index", "Cart");
                                    }
                                }
                            }

                            return View("_MultipleRegisterStep3", model);
                        }

                        return View("_MultipleRegisterStep3", model);
                    }
                case RegistrationStep.Step4:
                    {
                        if (ModelState.IsValid)
                        {
                            var hasWaiver = _seasonBusiness.HasSeasonWaiver(season);
                            var hasPaymentPlant = _seasonBusiness.HasSeasonMultRegisterPaymentPlan(season, JbUserService.GetCurrentUserId());

                            if (ists != null)
                            {

                                _registerBusiness.MultiRegisterAddEditStep4(model.FollowUpForms, ists.Value, hasWaiver, hasPaymentPlant, currentUserId);

                                if (hasWaiver)
                                {
                                    return RedirectToAction("MultipleRegister", new { seasonDomain = model.SeasonDomain, ists = ists, grade = grade, step = RegistrationStep.Step5 });
                                }
                                else if (hasPaymentPlant)
                                {
                                    return RedirectToAction("MultipleRegister",
                                        new
                                        {
                                            seasonDomain = model.SeasonDomain,
                                            ists = ists,
                                            grade = grade,
                                            step = RegistrationStep.Step6
                                        });
                                }
                                else
                                {
                                    return RedirectToAction("Index", "Cart");
                                }
                            }
                        }

                        var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId, OrderItemStatusCategories.notInitiated);
                        var programIds = new List<long>();
                        var orderItem = orderItems.FirstOrDefault();
                        if (orderItem != null)
                        {
                            programIds = orderItems.Select(i => i.ProgramSchedule.ProgramId).ToList();
                        }

                        var followUpFormsPrograms = _followupFormBusiness.GetProgramClubFormTemplateFromOrderItem(orderItems.ToList());
                        followUpFormsPrograms = followUpFormsPrograms.Distinct();

                        if (followUpFormsPrograms.Any(f => f.FollowUpFormMode == FollowUpFormMode.Online))
                        {
                            model.FollowUpFormItems = followUpFormsPrograms.Where(f => f.FollowUpFormMode == FollowUpFormMode.Online).Select(f => new FollowUpFormItemModel
                            {
                                FormId = f.JbForm.Id,
                                Name = f.Title,
                                AssignedProgramsName = _followupFormBusiness.GetFormProgramsName(f.Id, programIds)
                            }).ToList();
                        }

                        return View("_MultipleRegisterStep4", model);
                    }
                case RegistrationStep.Step5:
                    {
                        var seasonWaivers = _seasonBusiness.GetSeasonWaivers(season);

                        foreach (var waiver in model.Waivers)
                        {
                            var waiverDb = seasonWaivers.Single(w => w.Id == waiver.WaiverId);

                            if (waiver.WaiverConfirmationType == WaiverConfirmationType.Agree)
                            {

                                if (model.Waivers == null || (!waiver.Agreed && waiverDb.IsRequired))
                                {
                                    ModelState.AddModelError("Waivers", "You need to agree or sign the waivers in order to continue.");
                                    break;
                                }
                            }
                            else
                            {
                                if (waiverDb.IsRequired && (model.Waivers == null || string.IsNullOrEmpty(waiver.Signiture) || waiver.Signiture == "image/jsignature;base30,"))
                                {
                                    ModelState.AddModelError("Waivers", "You need to agree or sign the waivers in order to continue.");
                                    break;
                                }
                            }
                        }

                        if (ModelState.IsValid)
                        {
                            var hasPaymentPlant = _seasonBusiness.HasSeasonMultRegisterPaymentPlan(season, JbUserService.GetCurrentUserId());

                            if (ists != null)
                            {
                                AddEditStep5(model, ists.Value, hasPaymentPlant);

                                if (hasPaymentPlant)
                                {
                                    return RedirectToAction("MultipleRegister",
                                        new
                                        {
                                            seasonDomain = model.SeasonDomain,
                                            ists = ists,
                                            grade = grade,
                                            step = RegistrationStep.Step6
                                        });
                                }
                                else
                                {
                                    return RedirectToAction("Index", "Cart");
                                }
                            }
                        }

                        model.Waivers = seasonWaivers.Select(w =>
                                 new RegisterWaiverItemModel
                                 {
                                     Name = w.Name,
                                     Text = w.Text,
                                     WaiverId = w.Id,
                                     WaiverConfirmationType = w.WaiverConfirmationType,
                                     IsRequired = w.IsRequired,
                                     Order = w.Order
                                 })
                                 .ToList();

                        return View("_MultipleRegisterStep5", model);
                    }
                case RegistrationStep.Step6:
                    {
                        if (!model.SelectedPaymentPlan.HasValue && model.HasPaymentPlan)
                        {
                            ModelState.AddModelError("PaymentPlanLists", "You need to select your payment option.");
                            model.PaymentPlanLists = new List<PaymentPlan>();

                            var paymentPlans = _seasonBusiness.GetMultRegisterPaymentPlan(season, currentUserId);
                            model.PaymentPlanLists.AddRange(paymentPlans);

                            return View("_MultipleRegisterStep6", model);

                        }

                        if (ists != null)
                        {
                            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists.Value, currentUserId,
                                OrderItemStatusCategories.notInitiated);
                        }

                        if (ModelState.IsValid)
                        {
                            if (ists != null) AddEditStep6(model, ists.Value);
                            return RedirectToAction("Index", "Cart");
                        }

                        return View("_MultipleRegisterStep6", model);
                    }
                default:
                    break;
            }

            model.PlayerList = ManipulatePlayers(currentUserId);

            return View(model);
        }

        private List<SelectListItem> ManipulatePlayers(int userId)
        {
            var playerProfiles = Ioc.PlayerProfileBusiness.GetList(userId)
                                        .Select(p => new
                                        {
                                            p.Id,
                                            p.Contact.FirstName,
                                            p.Contact.LastName,
                                            p.Contact.Nickname,
                                            p.Relationship
                                        })
                                        .ToList();

            return playerProfiles.Where(p => p.Relationship == RelationshipType.Registrant).Select(p => new SelectListItem
            {
                Value = p.Id.ToString(),
                Text = p.FirstName + " " + p.LastName + (string.IsNullOrEmpty(p.Nickname) ? "" : "(" + p.Nickname + ")")
            })
           .ToList();
        }

        private List<SelectListItem> ManipulateGrades()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Text = "All grades", Value = "" });

            result.AddRange(DropdownHelpers.ToSelectList<SchoolGradeType>().Select(s =>
                 new SelectListItem
                 {
                     Text = s.Text,
                     Value = s.Value,
                 })
                 .ToList());

            return result;
        }
        public long AddStep1(MultipleRegisterViewModel model)
        {
            long ists = 0;
            decimal earlyBirdAmount = 0;
            var registerService = RegisterService.Get();

            var selectedTuitionIds = model.SelectedTuitions.Select(t => t.Id).ToList();
            var selectedCharegIds = model.SelectedTuitions.SelectMany(c => c.Charges).Select(t => t.Id).ToList();

            var selectedPrograms = _programBusiness.GetList().Where(s => s.ProgramSchedules.SelectMany(c => c.Charges).Any(c => selectedTuitionIds.Contains(c.Id))).ToList();

            var userId = this.GetCurrentUserId();
            var clubId = this.GetCurrentClubId();
            var uTCDate = DateTime.UtcNow;

            var orderMode = OrderMode.Online;
            var isInstallmentOrder = false;

            var order = Ioc.OrderBusiness.GetOrderInCart(userId, clubId) ?? new Order();
            order.ClubId = clubId;
            order.UserId = userId;
            order.OrderMode = orderMode;
            order.CompleteDate = uTCDate;

            ists = DateTimeHelper.DateTimetoTimeStamp(uTCDate);

            foreach (var program in selectedPrograms)
            {
                var charges = program.ProgramSchedules.SelectMany(p => p.Charges).Where(c => (selectedTuitionIds.Contains(c.Id) || (selectedCharegIds.Contains(c.Id) || c.Attributes.IsMandatory)) && !c.IsDeleted).ToList();

                foreach (var item in charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee))
                {
                    var earlyBirdPrice = GetEarlybirdPrice(item.Amount, program.TypeCategory, item.Attributes.EarlyBirds, program.Club.TimeZone);

                    if (earlyBirdPrice.HasValue)
                    {
                        earlyBirdAmount = earlyBirdPrice.Value;
                    }
                }

                foreach (var schedule in program.ProgramSchedules)
                {
                    foreach (var priceLabel in (charges.Where(c => c.ProgramScheduleId == schedule.Id && c.Category == ChargeDiscountCategory.EntryFee)))
                    {
                        OrderItem orderItem;
                        string itemName;

                        var entryFeeItem = schedule.Charges.First(c => selectedTuitionIds.Contains(c.Id));

                        var orderCharges = charges.Where(c => c.ProgramScheduleId.Value == schedule.Id)
                               .Select(c => new OrderChargeDiscount()
                               {
                                   Amount = earlyBirdAmount == 0 ? c.Amount : earlyBirdAmount,
                                   Category = c.Category,
                                   ChargeId = c.Id,
                                   Name = c.Name,
                                   Description = c.Description,
                                   Subcategory = ChargeDiscountSubcategory.Charge,
                               })
                               .ToList();

                        itemName = _programBusiness.GetProgramName(program.Name, schedule.StartDate, schedule.EndDate, "");

                        if (schedule.Program.TypeCategory != ProgramTypeCategory.Calendar)
                        {
                            itemName += priceLabel.Amount.ToString().Replace(".00", "").Replace(".0", ""); // " " + ((orderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "") : priceLabel.Amount.ToString().Replace(".00", "").Replace(".0", ""));
                        }

                        orderItem = new OrderItem();
                        orderItem.LastStep = RegistrationStep.Step1;
                        orderItem.SeasonId = schedule.Program.SeasonId;
                        orderItem.Name = itemName;
                        orderItem.DateCreated = uTCDate;
                        orderItem.ISTS = ists;
                        orderItem.ProgramTypeCategory = program.TypeCategory;
                        orderItem.ProgramScheduleId = schedule.Id;
                        orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schedule.Id);
                        orderItem.Start = schedule.StartDate;
                        orderItem.End = schedule.EndDate;

                        orderItem.ItemStatus = OrderItemStatusCategories.notInitiated;
                        orderItem.EntryFeeName = priceLabel.Name;
                        orderItem.EntryFee = orderCharges.FirstOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee).Amount;
                        orderItem.IsMultiRegister = true;

                        orderItem.OrderChargeDiscounts = orderCharges;

                        orderItem.TotalAmount = orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category > 0).Sum(c => c.Amount)
                                              - orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category < 0).Sum(c => c.Amount);

                        order.OrderItems.Add(orderItem);

                        orderItem.PaymentPlanType = isInstallmentOrder ? PaymentPlanType.Installment : PaymentPlanType.FullPaid;
                        //orderItem.PaymentPlanId = isInstallmentOrder ? model.PaymentPlanId : null;
                    }
                }
            }

            if (order.Id == 0)
            {
                Ioc.OrderBusiness.Create(order);
            }
            else
            {
                Ioc.OrderBusiness.Update(order);
            }

            return ists;
        }

        public void EditStep1(MultipleRegisterViewModel model, long ists)
        {
            decimal earlyBirdAmount = 0;
            var registerService = RegisterService.Get();

            var userId = this.GetCurrentUserId();
            var clubId = this.GetCurrentClubId();

            var order = Ioc.OrderBusiness.GetOrderInCart(userId, clubId) ?? new Order();

            var selectedTuitionIds = model.SelectedTuitions.Select(t => t.Id).ToList();
            var selectedCharegIds = model.SelectedTuitions.SelectMany(c => c.Charges).Select(t => t.Id).ToList();

            var selectedPrograms = _programBusiness.GetList().Where(s => s.ProgramSchedules.SelectMany(c => c.Charges).Any(c => selectedTuitionIds.Contains(c.Id))).ToList();

            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists).ToList();

            var newOrderItems = new List<OrderItem>();

            foreach (var program in selectedPrograms)
            {
                var charges = program.ProgramSchedules.SelectMany(p => p.Charges).Where(c => (selectedTuitionIds.Contains(c.Id) || (selectedCharegIds.Contains(c.Id) || c.Attributes.IsMandatory)) && !c.IsDeleted).ToList();

                foreach (var item in charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee))
                {
                    var earlyBirdPrice = GetEarlybirdPrice(item.Amount, program.TypeCategory, item.Attributes.EarlyBirds, program.Club.TimeZone);

                    if (earlyBirdPrice.HasValue)
                    {
                        earlyBirdAmount = earlyBirdPrice.Value;
                    }
                }

                foreach (var schedule in program.ProgramSchedules)
                {
                    foreach (var priceLabel in (charges.Where(c => c.ProgramScheduleId == schedule.Id && c.Category == ChargeDiscountCategory.EntryFee)))
                    {
                        OrderItem orderItem;
                        string itemName;

                        var entryFeeItem = schedule.Charges.First(c => selectedTuitionIds.Contains(c.Id));

                        var orderCharges = charges.Where(c => c.ProgramScheduleId.Value == schedule.Id)
                               .Select(c => new OrderChargeDiscount()
                               {
                                   Amount = earlyBirdAmount == 0 ? c.Amount : earlyBirdAmount,
                                   Category = c.Category,
                                   ChargeId = c.Id,
                                   Name = c.Name,
                                   Description = c.Description,
                                   Subcategory = ChargeDiscountSubcategory.Charge,
                               })
                               .ToList();

                        itemName = _programBusiness.GetProgramName(program.Name, schedule.StartDate, schedule.EndDate, "");

                        if (schedule.Program.TypeCategory != ProgramTypeCategory.Calendar)
                        {
                            itemName += priceLabel.Amount.ToString().Replace(".00", "").Replace(".0", ""); // " " + ((orderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "") : priceLabel.Amount.ToString().Replace(".00", "").Replace(".0", ""));
                        }

                        int? playerId = null;
                        string playerFirstName = null;
                        string playerLastName = null;
                        int? jbFormId = null;
                        JbForm jbForm = null;
                        List<OrderItemFollowupForm> followUpForms = null;
                        RegistrationStep lastStep = RegistrationStep.Step1;
                        OrderItemStatusCategories itemStatus = OrderItemStatusCategories.notInitiated; ;

                        if (orderItems.Any(o => o.ProgramScheduleId == schedule.Id))
                        {
                            var oldOrderItem = orderItems.Single(o => o.ProgramScheduleId == schedule.Id);

                            playerId = oldOrderItem.PlayerId;
                            playerFirstName = oldOrderItem.FirstName;
                            playerLastName = oldOrderItem.LastName;
                            jbFormId = oldOrderItem.JbFormId;
                            jbForm = oldOrderItem.JbForm;
                            lastStep = oldOrderItem.LastStep;
                            itemStatus = oldOrderItem.ItemStatus;
                            followUpForms = oldOrderItem.FollowupForms.ToList();


                        }

                        orderItem = new OrderItem();

                        orderItem.SeasonId = schedule.Program.SeasonId;
                        orderItem.Name = itemName;
                        orderItem.DateCreated = DateTime.UtcNow;
                        orderItem.ISTS = ists;

                        orderItem.LastStep = lastStep;
                        orderItem.ItemStatus = itemStatus;

                        orderItem.PlayerId = playerId;
                        orderItem.FirstName = playerFirstName;
                        orderItem.LastName = playerLastName;

                        orderItem.JbFormId = jbFormId;
                        orderItem.JbForm = jbForm;

                        orderItem.FollowupForms = followUpForms;

                        orderItem.ProgramTypeCategory = program.TypeCategory;
                        orderItem.ProgramScheduleId = schedule.Id;
                        orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schedule.Id);
                        orderItem.Start = schedule.StartDate;
                        orderItem.End = schedule.EndDate;

                        orderItem.EntryFeeName = priceLabel.Name;
                        orderItem.EntryFee = orderCharges.FirstOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee).Amount;
                        orderItem.IsMultiRegister = true;

                        orderItem.OrderChargeDiscounts = orderCharges;

                        orderItem.TotalAmount = orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category > 0).Sum(c => c.Amount)
                                              - orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category < 0).Sum(c => c.Amount);

                        newOrderItems.Add(orderItem);
                    }
                }
            }

            Ioc.OrderBusiness.Update(order, newOrderItems);
        }

        public void AddEditStep2(MultipleRegisterViewModel model, bool hasWaiver, long ists)
        {
            var registerService = RegisterService.Get();

            PlayerProfile playerProfile = null;

            if (model.SelectedPlayer.HasValue && model.SelectedPlayer != 0)
            {
                playerProfile = Ioc.PlayerProfileBusiness.Get(model.SelectedPlayer.Value);
            }

            RegistrationStep lastStep = RegistrationStep.Step2;

            var userId = this.GetCurrentUserId();
            var clubId = this.GetCurrentClubId();
            var uTCDate = DateTime.UtcNow;

            //var orderMode = OrderMode.Online;

            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists, userId, OrderItemStatusCategories.notInitiated);

            var order = orderItems.First().Order;

            foreach (var orderItem in orderItems)
            {
                if (playerProfile != null)
                {
                    orderItem.PlayerId = playerProfile.Id;
                }
                else
                {
                    orderItem.PlayerId = null;
                }

                orderItem.LastStep = orderItem.LastStep < lastStep ? lastStep : orderItem.LastStep;
            }

            Ioc.OrderBusiness.Update(order);
        }

        public void AddEditStep3(MultipleRegisterViewModel model, long ists, PlayerProfile playerProfile, bool hasWaiver, bool hasPaymentPlant, bool hasFollowUpForms)
        {
            var registerService = RegisterService.Get();
            var formBusiness = Ioc.FormBusiness;

            var currentUserId = this.GetCurrentUserId();
            RegistrationStep lastStep = RegistrationStep.Step3;

            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists, currentUserId, OrderItemStatusCategories.notInitiated);

            var order = orderItems.First().Order;

            var playerFirstName = formBusiness.GetPlayerFirstName(model.Form);
            var playerLastName = formBusiness.GetPlayerLastName(model.Form);

            playerProfile.Contact.FirstName = playerFirstName;
            playerProfile.Contact.LastName = playerLastName;

            foreach (var orderItem in orderItems)
            {
                orderItem.FirstName = playerFirstName;
                orderItem.LastName = playerLastName;

                orderItem.PlayerId = orderItem.PlayerId.HasValue ? orderItem.PlayerId : model.SelectedPlayer;

                if (orderItem.LastStep < lastStep)
                {
                    var jbForm = _seasonBusiness.GetSeasonRegistrationForm(orderItem.Season).JbForm;
                    model.Form.ParentId = jbForm.Id;

                    orderItem.JbForm = model.Form;
                    orderItem.LastStep = lastStep;
                }
                else
                {
                    var jbForm = orderItem.JbForm;

                    jbForm.JsonElements = model.Form.JsonElements;
                    jbForm.LastModifiedDate = DateTime.UtcNow;
                }

                _orderItemBusiness.InitiateOrderItem(orderItem, hasWaiver, hasPaymentPlant, hasFollowUpForms);
            }

            Ioc.OrderBusiness.Update(order);
        }

        public void AddEditStep5(MultipleRegisterViewModel model, long ists, bool hasPaymentPlant)
        {
            RegistrationStep lastStep = RegistrationStep.Step5;
            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
            var season = _seasonBusiness.Get(model.SeasonDomain, club.Id);
            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists, this.GetCurrentUserId(), OrderItemStatusCategories.notInitiated);

            var order = orderItems.First().Order;

            var orderItemWaivers = new List<OrderItemWaiver>();
            var seasonWaivers = _seasonBusiness.GetSeasonWaivers(season).Select(w =>
                           new RegisterWaiverItemModel
                           {
                               Name = w.Name,
                               Text = w.Text,
                               Id = w.Id,
                               WaiverConfirmationType = w.WaiverConfirmationType,
                               IsRequired = w.IsRequired
                           })
                              .ToList();

            foreach (var orderItem in orderItems)
            {
                orderItem.OrderItemWaivers.Clear();
                foreach (var item in model.Waivers)
                {
                    var orderItemWaiver = new OrderItemWaiver();
                    var programWaiver = seasonWaivers.Single(p => p.Id == item.WaiverId);

                    orderItemWaiver.Signature = item.Signiture;
                    orderItemWaiver.Agreed = item.Agreed;
                    orderItemWaiver.Name = programWaiver.Name;
                    orderItemWaiver.Text = programWaiver.Text;
                    orderItemWaiver.IsRequired = programWaiver.IsRequired;
                    orderItemWaiver.WaiverConfirmationType = programWaiver.WaiverConfirmationType;

                    orderItem.OrderItemWaivers.Add(orderItemWaiver);
                }

                orderItem.LastStep = orderItem.LastStep < lastStep ? lastStep : orderItem.LastStep;

                _orderItemBusiness.InitiateOrderItem(orderItem, model.HasWaiver, hasPaymentPlant);
            }

            Ioc.OrderBusiness.Update(order);
        }

        public void AddEditStep6(MultipleRegisterViewModel model, long ists)
        {
            var lastStep = RegistrationStep.Step6;
            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
            var season = _seasonBusiness.Get(model.SeasonDomain, club.Id);
            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists, this.GetCurrentUserId(), OrderItemStatusCategories.notInitiated);

            var order = orderItems.First().Order;
            model.PaymentPlanLists = new List<PaymentPlan>();

            var paymentPlans = _seasonBusiness.GetMultRegisterPaymentPlan(season, this.GetCurrentUserId());
            model.PaymentPlanLists.AddRange(paymentPlans);

            order.PaymentPlanType = PaymentPlanType.Installment;

            foreach (var orderItem in orderItems)
            {
                orderItem.PaymentPlanType = model.SelectedPaymentPlan.HasValue && model.SelectedPaymentPlan.Value > 0 ? PaymentPlanType.Installment : PaymentPlanType.FullPaid;
                orderItem.PaymentPlanId = model.SelectedPaymentPlan.HasValue && model.SelectedPaymentPlan.Value > 0 ? model.SelectedPaymentPlan : null;

                orderItem.LastStep = orderItem.LastStep < lastStep ? lastStep : orderItem.LastStep;

                _orderItemBusiness.InitiateOrderItem(orderItem);
            }

            Ioc.OrderBusiness.Update(order);

        }

        private List<MultipleRegisterProgramItemModel> GetMutipleRegisterPrograms(long seasonId, List<OrderItem> orderItems, SchoolGradeType? grade, string playerGrade, int? playerAge, string gender, long? ists, MultipleRegisterFilterViewModel filter)
        {
            var infoClub = this.GetCurrentClubBaseInfo();

            var programBussiness = _programBusiness;

            var result = new List<MultipleRegisterProgramItemModel>();

            var season = _seasonBusiness.Get(seasonId);

            var programs = _programBusiness.GetListBySeasonId(seasonId).Where(p => p.LastCreatedPage >= ProgramPageStep.Step4 && p.TypeCategory == ProgramTypeCategory.Class);

            programs = FilterPrograms(programs, filter);

            programs = programs.OrderByDescending(o => o.Id);

            var club = programs.FirstOrDefault()?.Club;

            var registerService = RegisterService.Get();

            result = programs.ToList().Select(p =>
                 new MultipleRegisterProgramItemModel()
                 {
                     Id = p.Id,
                     Domain = p.Domain,
                     Gender = programBussiness.GetGender(p),
                     IsGenderRestricted = registerService.IsGenderRestricted(gender, p),
                     GradeRestrictionMessage = playerGrade != null ? programBussiness.CheckGradeRestriction(p.ProgramSchedules.First(), playerGrade) : string.Empty,
                     IsGradeRestricted = registerService.IsGradeRestricted(playerGrade, p),
                     IsAgeRestricted = registerService.IsAgeRestricted(playerAge, p),
                     AgeRestrictionMessage = registerService.GetAgeRestrictionMessage(p),
                     Name = string.Format("{0} {1}", p.Name, programBussiness.GenerateGradeInfoLabel(programBussiness.GetMinGrade(p), programBussiness.GetMaxGrade(p))),
                     Room = p.Room,
                     ProgramType = p.TypeCategory.ToString(),
                     DisplayUrl = $"{p.SeasonDomain}/{p.Domain}/view",
                     Days = GetDaysWithFilter(p, filter),
                     Dates = string.Format("{0} - {1}", programBussiness.StartDate(p).Value.ToString("dd MMM yyyy"), programBussiness.EndDate(p).Value.ToString("dd MMM yyyy")),
                     RegStatus = programBussiness.GetRegStatus(p),
                     Status = p.Status,
                     Currency = infoClub.Currency,
                     ProgramEMStatus = programBussiness.GetEmProgramStatus(p, season.Status == SeasonStatus.Test),
                     Tuitions = p.ProgramSchedules.Where(ps => !ps.IsDeleted).SelectMany(h => h.Charges).Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted).Select(t =>
                        new MultipleRegisterTuitionItemModel
                        {
                            Id = t.Id,
                            Title = t.Name,
                            Price = _programBusiness.CalculateProgramChargeAmount(club, t).Value,

                            EarlyBirdPrice = _programBusiness.CalculateProgramChargeAmount(club, t, t.Attributes.EarlyBirds, true),
                            Checked = orderItems.SelectMany(o => o.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Any(c => c.ChargeId == t.Id)
                        })
                        .ToList(),
                     Charges = p.ProgramSchedules.Where(ps => !ps.IsDeleted).SelectMany(h => h.Charges).Where(c => c.Category != ChargeDiscountCategory.EntryFee && !c.IsDeleted).Select(t =>
                          new MultipleRegisterChargeItemModel
                          {
                              Id = t.Id,
                              Title = t.Name,
                              Price = t.Amount,
                              IsMandatory = t.Attributes.IsMandatory,
                              Checked = orderItems.SelectMany(o => o.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category != ChargeDiscountCategory.EntryFee).Any(c => c.ChargeId == t.Id)
                          })
                           .ToList(),
                 })
                  .ToList();

            var selectedDays = new List<DayOfWeek>();
            if (!string.IsNullOrEmpty(filter.selectedDays))
            {
                selectedDays = JsonHelper.JsonDeserialize<List<DayOfWeek>>(filter.selectedDays);
                ViewBag.SelectedDays = selectedDays;
            }

            return result;
        }

        private List<ProgramScheduleDayModel> GetDaysWithFilter(Program program, MultipleRegisterFilterViewModel filter)
        {
            var days = new List<ProgramScheduleDayModel>();
            TimeSpan timePM = new TimeSpan(12, 0, 0); //pm
            TimeSpan timeAM = new TimeSpan(0, 0, 0); //AM

            var datePart = new List<string>();
            if (filter.datePart != null)
            {
                datePart = JsonHelper.JsonDeserialize<List<string>>(filter.datePart);
            }

            if (datePart.Count() > 1 || datePart.Count() == 0)
            {
                days = _programBusiness.GetClassDays(program).GroupBy(g => g.DayOfWeek).Select(jh => jh).SelectMany(h => h).Select(sl =>
                         new ProgramScheduleDayModel
                         {
                             DayOfWeek = sl.DayOfWeek,
                             StartTime = sl.StartTime,
                             EndTime = sl.EndTime,
                         })
                     .ToList();
            }
            else if (datePart.FirstOrDefault() == "PM")
            {
                days = _programBusiness.GetClassDays(program).Where(d => d.StartTime.Value >= timePM).GroupBy(g => g.DayOfWeek).Select(jh => jh).SelectMany(h => h).Select(sl =>
                           new ProgramScheduleDayModel
                           {
                               DayOfWeek = sl.DayOfWeek,
                               StartTime = sl.StartTime,
                               EndTime = sl.EndTime,
                           })
                     .ToList();
            }
            else
            {
                days = _programBusiness.GetClassDays(program).Where(d => d.StartTime.Value >= timeAM && d.StartTime.Value < timePM).GroupBy(g => g.DayOfWeek).Select(jh => jh).SelectMany(h => h).Select(sl =>
                            new ProgramScheduleDayModel
                            {
                                DayOfWeek = sl.DayOfWeek,
                                StartTime = sl.StartTime,
                                EndTime = sl.EndTime,
                            })
                     .ToList();
            }
            return days;
        }

        public IQueryable<Program> FilterPrograms(IQueryable<Program> programs, MultipleRegisterFilterViewModel filter)
        {
            var selectedDays = new List<DayOfWeek>();
            var programBusiness = _programBusiness;
            var datePart = new List<string>();
            TimeSpan timePM = new TimeSpan(12, 0, 0); //pm
            TimeSpan timeAM = new TimeSpan(0, 0, 0); //AM
            var Allprograms = new List<Program>();
            foreach (var program in programs)
            {
                if (program.TypeCategory == ProgramTypeCategory.Class || program.TypeCategory == ProgramTypeCategory.Camp)
                {
                    foreach (var schedule in program.ProgramSchedules)
                    {
                        DateTime currentDate = schedule.StartDate;
                        while (currentDate <= schedule.EndDate)
                        {
                            DayOfWeek dayOfWeek = currentDate.DayOfWeek;
                            foreach (var item in ((ScheduleAttribute)schedule.Attributes).Days)
                            {
                                if (dayOfWeek == item.DayOfWeek)
                                {
                                    Allprograms.Add(program);
                                }
                            }
                            currentDate = currentDate.AddDays(1);
                        }
                    }
                }
                else
                {
                    Allprograms.Add(program);
                }

            }
            programs = Allprograms.Distinct().AsQueryable();
            if (filter.selectedDays != null)
            {
                selectedDays = JsonHelper.JsonDeserialize<List<DayOfWeek>>(filter.selectedDays);
                ViewBag.SelectedDays = selectedDays;
            }
            if (filter.datePart != null)
            {
                datePart = JsonHelper.JsonDeserialize<List<string>>(filter.datePart);
                ViewBag.dateParts = datePart;
            }
            //var Days = JObject.Parse(filter.selectedDays);
            // var data = JObject.Parse(filter.datePart);

            //if (filter.SelectedGrade.HasValue)
            //{
            //    programs = programs.Where(p => p.ProgramSchedules.Any(s => s.AttendeeRestriction.RestrictionType != RestrictionType.Grade || (s.AttendeeRestriction.RestrictionType == RestrictionType.Grade && (!s.AttendeeRestriction.MinGrade.HasValue || s.AttendeeRestriction.MinGrade <= filter.Grade.Value) && (!s.AttendeeRestriction.MaxGrade.HasValue || s.AttendeeRestriction.MaxGrade >= filter.Grade.Value))));
            //}
            if (!string.IsNullOrEmpty(filter.SelectedAddress))
            {
                programs = programs.Where(p => !string.IsNullOrEmpty(p.ClubLocation.PostalAddress.Address) ? p.ClubLocation.PostalAddress.Address.ToLower().Trim() == filter.SelectedAddress.ToLower().Trim() : false);
            }
            if (!string.IsNullOrEmpty(filter.SelectedCity))
            {
                programs = programs.Where(p => p.ClubLocation.PostalAddress.City == filter.SelectedCity);
            }
            if (!string.IsNullOrEmpty(filter.SelectedLocationName))
            {
                programs = programs.Where(p => !string.IsNullOrEmpty(p.ClubLocation.Name) ? p.ClubLocation.Name.ToLower().Trim() == filter.SelectedLocationName.ToLower().Trim() : false);

            }
            if (!string.IsNullOrEmpty(filter.SelectedCategory))
            {
                programs = programs.Where(p => p.Categories.Any(c => c.Name.ToLower().Trim() == filter.SelectedCategory.ToLower().Trim()));

            }

            if (!string.IsNullOrEmpty(filter.SelectedGrade) && filter.SelectedGrade != "All")
            {

                var programsFilterByGrade = new List<Program>();
                foreach (var item in programs)
                {
                    var min = programBusiness.GetMinGrade(item);
                    var max = programBusiness.GetMaxGrade(item);
                    var selectedGrade = Ioc.PlayerProfileBusiness.GetGradeValueFromDescription(filter.SelectedGrade);

                    if (min != null && max != null)
                    {
                        if (min.GetOrder() <= selectedGrade.GetOrder() && max.GetOrder() >= selectedGrade.GetOrder())
                        {

                            programsFilterByGrade.Add(item);
                        }
                    }
                    else if ((min != null ? (min.GetOrder() <= selectedGrade.GetOrder()) : false) || (max != null ? (max.GetOrder() >= selectedGrade.GetOrder()) : false))
                    {
                        programsFilterByGrade.Add(item);
                    }
                    else if (min == null && max == null)
                    {
                        programsFilterByGrade.Add(item);
                    }
                    programs = programsFilterByGrade.AsQueryable();
                }

            }


            if (selectedDays != null && selectedDays.Any())
            {

                var programsFilterByDay = new List<Program>();
                foreach (var item in programs)
                {
                    var day = programBusiness.GetClassDays(item).ToList();

                    var programList = selectedDays.Where(s => day.Select(p => p.DayOfWeek).ToList().Contains(s)).ToList();

                    if (programList.Count > 0)
                    {
                        programsFilterByDay.Add(item);
                    }
                }
                programs = programsFilterByDay.AsQueryable();

            }
            if (datePart != null && datePart.Any())
            {

                var programsFilterByAMPM = new List<Program>();

                if (datePart.Count > 1)
                {
                    foreach (var item in programs)
                    {
                        var ProgramsPMAndAM = programBusiness.GetClassDays(item).Where(pr => (pr.StartTime.HasValue && pr.StartTime >= timePM) || (pr.StartTime >= timeAM && pr.StartTime < timePM && pr.StartTime.HasValue)).ToList();
                        if (ProgramsPMAndAM.Count > 0)
                        {
                            programsFilterByAMPM.Add(item);
                        }
                    }
                    programs = programsFilterByAMPM.AsQueryable();
                }
                else
                {
                    if (datePart.FirstOrDefault() == "PM")
                    {
                        foreach (var item in programs)
                        {
                            var ProgramsPM = programBusiness.GetClassDays(item).Where(pr => pr.StartTime.HasValue && pr.StartTime >= timePM).ToList();
                            if (ProgramsPM.Count > 0)
                            {
                                programsFilterByAMPM.Add(item);
                            }
                        }
                        programs = programsFilterByAMPM.AsQueryable();
                    }
                    else
                    {

                        foreach (var item in programs)
                        {
                            var ProgramsAM = programBusiness.GetClassDays(item).Where(pr => pr.StartTime >= timeAM && pr.StartTime < timePM && pr.StartTime.HasValue).ToList();
                            if (ProgramsAM.Count > 0)
                            {
                                programsFilterByAMPM.Add(item);
                            }
                        }
                        programs = programsFilterByAMPM.AsQueryable();
                    }
                }

            }

            if (!string.IsNullOrEmpty(filter.SelectedStatus) && filter.SelectedStatus != "All")
            {

                var programsFilterStatus = new List<Program>();
                foreach (var program in programs)
                {
                    var programStatus = programBusiness.GetEmProgramStatus(program, program.Season.Status == SeasonStatus.Test);

                    if (filter.SelectedStatus == programStatus)
                    {
                        programsFilterStatus.Add(program);
                    }
                }
                programs = programsFilterStatus.AsQueryable();

            }
            return programs;
        }

        private decimal? GetEarlybirdPrice(decimal tuitionPrice, ProgramTypeCategory programType, List<EarlyBird> earlyBirds, TimeZones clubTimeZone)
        {
            decimal? result = null;

            if (programType != ProgramTypeCategory.Calendar && earlyBirds != null)
            {
                foreach (var earlyBrid in earlyBirds.OrderBy(e => e.ExpireDate))
                {
                    if (DateTimeHelper.GetCurrentLocalDateTime(clubTimeZone).Date <= earlyBrid.ExpireDate.Value.Date)
                    {
                        result = earlyBrid.Price;
                        break;
                    }
                }
            }

            return result;
        }

        [HttpGet]
        public virtual ActionResult AddToWaitList(long? programId, string returnUrl, long? scheduleId, bool isIndividual = false)
        {
            var program = new Program();

            if (programId.HasValue)
            {
                program = _programBusiness.Get(programId.Value);
            }
            else if (scheduleId.HasValue)
            {
                program = _programBusiness.GetByScheduleId(scheduleId.Value);
            }


            var season = program.Season;

            var isWaitlistEnabled = false;
            var waitlistPolicy = string.Empty;

            if (this.GetCurrentUserRole() != RoleCategory.Parent)
            {
                return RedirectToAction(actionName: "DisplayNotAccessibleMessage", controllerName: "SubDomain", routeValues:
                                 new
                                 {
                                     role = RoleCategory.Admin,
                                     message = "You are logged in as a staff member, you can only do off-line registration.",
                                     returnUrl = "/" + season.Domain + "/" + program.Domain
                                 });
            }

            if (isIndividual)
            {
                isWaitlistEnabled = program.IsWaitListAvailable;
                waitlistPolicy = !string.IsNullOrEmpty(program.WaitlistPolicy) ? program.WaitlistPolicy : Constants.DefaultWaitlistPolicy;
            }
            else
            {
                if (season.Setting != null && season.Setting.SeasonAdditionalSetting != null)
                {
                    isWaitlistEnabled = season.Setting.SeasonAdditionalSetting.IsWaitListAvailable;
                    waitlistPolicy = !string.IsNullOrEmpty(season.Setting.SeasonAdditionalSetting.WaitlistPolicy) ? season.Setting.SeasonAdditionalSetting.WaitlistPolicy : string.Empty;
                }
            }

            if (!isWaitlistEnabled)
            {
                throw new Exception("Waitlist is not available.");
            }

            var model = new AddToWaitListViewModel()
            {
                ProgramName = program.Name,
                WaitlistPolicy = waitlistPolicy
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult AddToWaitList(AddToWaitListViewModel model, long? programId, long? scheduleId, string returnUrl)
        {
            var userId = this.GetCurrentUserId();

            var program = new Program();

            var programScheduleId = scheduleId.HasValue ? scheduleId.Value : (long?)null;

            if (programId.HasValue)
            {
                program = _programBusiness.Get(programId.Value);

                programScheduleId = program.ProgramSchedules.FirstOrDefault().Id;
            }
            else if (scheduleId.HasValue)
            {
                program = _programBusiness.GetByScheduleId(scheduleId.Value);
                programScheduleId = scheduleId.Value;

                var schedule = program.ProgramSchedules.Where(s => s.Id == scheduleId.Value).FirstOrDefault();

                if (schedule.Title != null)
                {
                    model.ProgramName = string.Format("{0}, {1} ({2}-{3})", model.ProgramName, schedule.Title, schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat));
                }
                else
                {
                    model.ProgramName = string.Format("{0} ({1}-{2})", model.ProgramName, schedule.StartDate.ToString(Constants.DefaultDateFormat), schedule.EndDate.ToString(Constants.DefaultDateFormat));
                }
            }


            var isTestMode = program.Season.Status == SeasonStatus.Test;

            if (Ioc.WaitListBusiness.IsAlreadyExistInWaitListByScheduleId(programScheduleId.Value, userId, model.FirstName, model.LastName))
            {
                ModelState.AddModelError("", "This person already added to waitlist of program.");
            }

            if (ModelState.IsValid)
            {
                var waitList = new WaitList()
                {
                    Date = DateTime.UtcNow,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    ProgramId = program.Id,
                    UserId = userId,
                    PhoneNumber = model.PhoneNumber,
                    Grade = model.Grade.Value,
                    IsLive = !isTestMode,
                    ScheduleId = programScheduleId.Value
                };

                var result = Ioc.WaitListBusiness.Create(waitList);

                var contactEmail = string.Empty;
                var clubDomain = string.Empty;
                var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

                if (club.PartnerId.HasValue)
                {
                    contactEmail = club.PartnerClub.ContactPersons.First().Email;
                    clubDomain = club.PartnerClub.Domain;
                }
                else
                {
                    clubDomain = club.Domain;
                    contactEmail = club.ContactPersons.First().Email;
                }
                var datetime = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), waitList.Date).ToString("MMM dd, yyyy, h:mm tt");

                // send email to parent
                EmailService emailService = EmailService.Get(this.ControllerContext);
                emailService.SendAddedToWaitlist(this.GetCurrentUserName(), model.ProgramName, club.Name, club.Site, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo, contactEmail, clubDomain, program?.Season?.Title);

                // send email to admin
                emailService.SendWaitlistForAdmin(datetime, model.FirstName + " " + model.LastName, model.Grade.ToDescription(), this.GetCurrentUserName(), model.ProgramName, club, club.Site, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo, contactEmail, clubDomain);

                if (result.Status)
                {
                    return RedirectToAction("DisplayWaitlistConfirmation", new { returnUrl = returnUrl });
                }
                else
                {
                    ModelState.AddModelError("", "Something went wrong.");
                }
            }

            return View(model);
        }

        public virtual ActionResult DisplayWaitlistConfirmation(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [ChildActionOnly]
        public virtual ActionResult Subscription(long programId, DateTime? desiredStartDate)
        {
            var model = new SubscriptionRegisterModel();

            var registerService = RegisterService.Get();

            var program = _programBusiness.Get(programId);

            var programSchedule = program.ProgramSchedules.Last(p => !p.IsDeleted);

            var subsctriptionAttribute = programSchedule.Attributes as ScheduleSubscriptionAttribute;

            model.ScheduleId = programSchedule.Id;

            model.EnableDropIn = subsctriptionAttribute.EnableDropIn;
            model.DropInLabel = subsctriptionAttribute.DropInSessionLabel;
            model.DropInSessionPrice = subsctriptionAttribute.DropInSessionPrice;

            model.Days = subsctriptionAttribute.Days.Select(s =>
                    new SubscriptionRegisterDaysViewModel
                    {
                        Title = s.DayOfWeek.ToDescription(),
                        Day = s.DayOfWeek
                    })
                    .ToList();

            model.StartDate = programSchedule.StartDate;
            model.EndDate = programSchedule.EndDate;
            model.ProgramId = program.Id;
            model.EnablePriceHidden = program.Club.Setting != null ? program.Club.Setting.ClubProgram.EnablePriceHidden ? !program.Club.Setting.ClubProgram.HideAllProgramsPrice ? program.Club.Setting.ClubProgram.HiddenPricePrograms != null ? program.Club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false;

            var subscriptionBusiness = Ioc.SubscriptionBusiness;

            //var appliedSubscriptionItem = subscriptionBusiness.GetAppliedSubscriptionItem(programSchedule, null);

            var appliedSubscriptionItem = subscriptionBusiness.GetAppliedProgramPart(programSchedule, null);
            var startDate = appliedSubscriptionItem.StartDate;
            var endDate = appliedSubscriptionItem.EndDate;

            model.Price = new SubscriptionRegisterPriceViewModel()
            {
                StartDate = startDate,
                EndDate = endDate,
                ScheduleId = programSchedule.Id,
                EnablePriceHidden = program.Club.Setting != null ? program.Club.Setting.ClubProgram.EnablePriceHidden ? !program.Club.Setting.ClubProgram.HideAllProgramsPrice ? program.Club.Setting.ClubProgram.HiddenPricePrograms != null ? program.Club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false,

                Prices = programSchedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted).Select(s =>//subsctriptionAttribute.Subscriptions.Select(s =>
                            new SubscriptionRegisterPriceItemViewModel
                            {
                                Amount = s.Amount,
                                Label = s.Name,
                                Id = s.Id,

                            })
                            .ToList(),
            };

            var applicationFee = subscriptionBusiness.GetApplicationFee(program);
            var charges = new List<SubscriptionChargeItemRegisterViewModel>();

            if (applicationFee != null)
            {
                charges.Add(new SubscriptionChargeItemRegisterViewModel
                {
                    Id = applicationFee.Id,
                    Amount = applicationFee.Amount,
                    Label = applicationFee.Name,
                    Category = applicationFee.Category
                });
            }

            model.Price.Charges = charges;

            return View("_Subscription", model);
        }

        [ChildActionOnly]
        public virtual ActionResult SubscriptionPaymentPlan(long programId, DateTime? desiredStartDate, string chargeDiscountsText, List<DayOfWeek> selectedDays, bool isDropIn = false)
        {
            var model = new SubscriptionRegisterPaymentPlanViewModel();

            if (isDropIn)
            {
                return View("_SubscriptionPaymentPlan", model);
            }

            var program = _programBusiness.Get(programId);

            var programSchedule = program.ProgramSchedules.Last(p => !p.IsDeleted);

            var subsctriptionAttribute = programSchedule.Attributes as ScheduleSubscriptionAttribute;

            var subscriptionBusiness = Ioc.SubscriptionBusiness;

            decimal totalAmount = 0;
            decimal discountAmount = 0;

            if (desiredStartDate.HasValue)
            {
                var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(chargeDiscountsText);
                var chargeId = charges.First(s => s.Category == ChargeDiscountCategory.EntryFee).ChargeId;

                //totalAmount = subscriptionBusiness.GetTotalBeforeAfterCareAmount(programSchedule, desiredStartDate, chargeId);

                totalAmount = subscriptionBusiness.GetTotalBeforeAfterCareAmount(programSchedule, desiredStartDate, selectedDays, chargeId);

                discountAmount = subscriptionBusiness.GetFullPayDiscount(programSchedule, totalAmount);

                model.DesirderStartDate = desiredStartDate.Value;
            }

            model.HasFullPayOption = subsctriptionAttribute.HasPayInFullOption;
            model.FullPayDiscountMessage = subsctriptionAttribute.FullPayDiscountMessage;
            model.FullTotalAmount = totalAmount;
            model.WithDiscountFullPayAmount = totalAmount - discountAmount;

            return View("_SubscriptionPaymentPlan", model);
        }

        [ChildActionOnly]
        public virtual ActionResult BeforeAfterPaymentPlan(long programId, DateTime? desiredStartDate, string chargeDiscountsText, List<DayOfWeek> selectedDays, bool isDropIn = false)
        {
            var model = new SubscriptionRegisterPaymentPlanViewModel();

            if (isDropIn)
            {
                return View("_SubscriptionPaymentPlan", model);
            }

            var programSchedule = new ProgramSchedule();
            var program = _programBusiness.Get(programId);


            if (chargeDiscountsText != null)
            {
                var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(chargeDiscountsText);
                var scheduleId = charges.First().ScheduleId;
                programSchedule = program.ProgramSchedules.Where(s => s.Id == scheduleId).FirstOrDefault();
            }
            else
            {
                programSchedule = program.ProgramSchedules.Last(p => !p.IsDeleted);
            }

            var beforeAfterAttribute = programSchedule.Attributes as ScheduleAfterBeforeCareAttribute;

            var subscriptionBusiness = Ioc.SubscriptionBusiness;

            decimal totalAmount = 0;
            decimal discountAmount = 0;

            if (desiredStartDate.HasValue)
            {
                var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(chargeDiscountsText);
                var chargeId = charges.First(s => s.Category == ChargeDiscountCategory.EntryFee).ChargeId;

                //totalAmount = subscriptionBusiness.GetTotalBeforeAfterCareAmount(programSchedule, desiredStartDate, chargeId);

                totalAmount = subscriptionBusiness.GetTotalBeforeAfterCareAmount(programSchedule, desiredStartDate, selectedDays, chargeId);

                discountAmount = subscriptionBusiness.GetFullPayDiscount(programSchedule, totalAmount);
                model.DesirderStartDate = desiredStartDate.Value;
            }

            model.HasFullPayOption = beforeAfterAttribute.HasPayInFullOption;
            model.FullPayDiscountMessage = beforeAfterAttribute.FullPayDiscountMessage;
            model.FullTotalAmount = totalAmount;
            model.WithDiscountFullPayAmount = totalAmount - discountAmount;

            return View("_SubscriptionPaymentPlan", model);
        }

        [HttpPost]
        public virtual ActionResult CheckSubscriptionValidations(DateTime? desiredStartDate, long programId, List<DayOfWeek> days, int selectedChargeId)
        {
            object result = null;
            var errors = new List<string>();

            bool isOfflineOrder = false;

            if (this.GetCurrentUserRoleType() != RoleCategoryType.Parent)
            {
                isOfflineOrder = true;
            }

            var subscriptionBusiness = Ioc.SubscriptionBusiness;
            var program = _programBusiness.Get(programId);
            var programSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted);

            var nowDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club, DateTime.UtcNow);

            var attributes = subscriptionBusiness.GetSubscriptionAtribute(programSchedule);

            if (!desiredStartDate.HasValue)
            {
                errors.Add("Desired start date is required.");
            }
            else
            {
                if (desiredStartDate.Value.Date <= nowDateTime.Date && isOfflineOrder == false)
                {
                    errors.Add("Desired start date must be after today.");
                }
                else if (programSchedule.StartDate.Date > desiredStartDate.Value.Date)
                {
                    errors.Add("Desired start date cannot be before program start date.");
                }
                else if (programSchedule.EndDate.Date < desiredStartDate.Value.Date)
                {
                    errors.Add("Desired start date cannot be after program end date.");
                }

                var selectedCharge = subscriptionBusiness.GetSelectedCharge(programSchedule, selectedChargeId);

                var selectedChargeAttribute = JsonConvert.DeserializeObject<SubscriptionChargeAttribute>(selectedCharge.AttributesSerialized);

                var weekDays = selectedChargeAttribute.WeekDays;

                if (days != null && days.Any())
                {
                    if (!days.Contains(desiredStartDate.Value.DayOfWeek))
                    {
                        errors.Add("Desired start date should be one of the selected class days.");
                    }

                    if (weekDays != 0 && weekDays != days.Count())
                    {
                        errors.Add(string.Format("You should select {0} class days.", weekDays));
                    }

                }
                else
                {
                    errors.Add(string.Format("You should select {0} class days.", weekDays));
                }

                if (!errors.Any())
                {
                    // check sessions capacity
                    var programSessionBusiness = Ioc.ProgramSessionBusiness;
                    var programSessions = programSessionBusiness.GetList(programSchedule, desiredStartDate.Value, days);
                    var capacity = attributes.Capacity;

                    if (capacity != 0)
                    {
                        foreach (var item in programSessions)
                        {
                            if (programSessionBusiness.IsFull(item, capacity) && !program.DisableCapacityRestriction)
                            {
                                errors.Add(string.Format("Day {0} is full.", item.StartDateTime.ToShortDateString()));
                                break;
                            }
                        }
                    }
                }
            }

            result = new { HasError = errors.Any(), Errors = errors };

            return JsonNet(result);
        }

        [HttpGet]
        public virtual ActionResult GetScheduleFromDesiredDate(DateTime? desiredStartDate, long programId, int selectedChargeId, string days)
        {
            var selectedDays = !string.IsNullOrEmpty(days) ? JsonConvert.DeserializeObject<List<DayOfWeek>>(days): new List<DayOfWeek>();
            object result = null;

            var scheduleDate = string.Empty;
            var errorMessage = string.Empty;
            var hasError = false;
            decimal currentScheduleAmount = 0;

            var subscriptionBusiness = Ioc.SubscriptionBusiness;
            var program = _programBusiness.Get(programId);

            if (program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                var schedule = program.ProgramSchedules.Where(s => !s.IsDeleted).FirstOrDefault();

                var item = subscriptionBusiness.GetAppliedProgramPart(schedule, desiredStartDate.Value);

                if (item != null)
                {
                    scheduleDate = string.Format("{0} - {1}", item.StartDate.ToShortDateString(), item.EndDate.ToShortDateString());
                }
                else
                {
                    hasError = true;
                    errorMessage = "Desired start date is not valid.";
                }
            }
            else
            {
                var schedule = program.ProgramSchedules.Where(s => !s.IsDeleted).FirstOrDefault();

                var item = subscriptionBusiness.GetAppliedProgramPart(schedule, desiredStartDate.Value);

                if (item != null)
                {
                    scheduleDate = string.Format("{0} - {1}", item.StartDate.ToShortDateString(), item.EndDate.ToShortDateString());
                }
                else
                {
                    hasError = true;
                    errorMessage = "Desired start date is not valid.";
                }

                if (!hasError && selectedChargeId > 0)
                {
                    //currentScheduleAmount = Ioc.SubscriptionBusiness.CalculateCurrentScheduleAmount(program, desiredStartDate.Value, selectedChargeId);

                    currentScheduleAmount = Ioc.SubscriptionBusiness.CalculateCurrentScheduleAmount(program, desiredStartDate.Value, selectedChargeId, selectedDays);
                }
            }

            result = new { ScheduleDate = scheduleDate, HasError = hasError, ErrorMessage = errorMessage, ScheduleAmount = currentScheduleAmount };

            return JsonNet(result);
        }

        [ChildActionOnly]
        public virtual ActionResult BeforeAfterCare(long programId, DateTime? desiredStartDate)
        {

            bool isOfflineOrder = false;

            if (this.GetCurrentUserRoleType() != RoleCategoryType.Parent)
            {
                isOfflineOrder = true;
            }

            var model = new BeforeAfterCareRegisterModel();

            var registerService = RegisterService.Get();

            var program = _programBusiness.Get(programId);

            var programSchedules = isOfflineOrder ? program.ProgramSchedules.Where(p => !p.IsDeleted).OrderByDescending(s => s.ScheduleMode): program.ProgramSchedules.Where(p => !p.IsFreezed && !p.IsDeleted).OrderByDescending(s => s.ScheduleMode);

            model.ProgramType = program.TypeCategory;

            var defaultScheduleAttribute = programSchedules.LastOrDefault().Attributes as ScheduleAfterBeforeCareAttribute;

            model.Days = defaultScheduleAttribute.Days.Select(s =>
                   new SubscriptionRegisterDaysViewModel
                   {
                       Title = s.DayOfWeek.ToDescription(),
                       Day = s.DayOfWeek
                   })
                   .ToList();

            model.StartDate = programSchedules.FirstOrDefault().StartDate;
            model.EndDate = programSchedules.FirstOrDefault().EndDate;
            model.ProgramId = program.Id;
            model.EnablePriceHidden = program.Club.Setting != null ? program.Club.Setting.ClubProgram.EnablePriceHidden ? !program.Club.Setting.ClubProgram.HideAllProgramsPrice ? program.Club.Setting.ClubProgram.HiddenPricePrograms != null ? program.Club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false;

            var subscriptionBusiness = Ioc.SubscriptionBusiness;

            var appliedProgramParts = subscriptionBusiness.GetAppliedProgramPart(programSchedules.LastOrDefault(), null);
            var startDate = appliedProgramParts.StartDate;
            var endDate = appliedProgramParts.EndDate;

            model.EnableDropIn = defaultScheduleAttribute.EnableDropIn;
            model.DropInSessionPrice = defaultScheduleAttribute.DropInSessionPrice;
            model.DropInLabel = defaultScheduleAttribute.DropInSessionLabel;

            model.EnablePunchCard = defaultScheduleAttribute.PunchCard.Enabled;
            model.PunchCardPrice = defaultScheduleAttribute.PunchCard.Amount.HasValue ? defaultScheduleAttribute.PunchCard.Amount.Value : 0;
            model.PunchCardLabel = defaultScheduleAttribute.PunchCard.Title;

            model.ScheduleId = programSchedules.LastOrDefault().Id;

            var comboTime = programSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.Both) ? _programBusiness.GetComboTime(programSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both).ToList()) : null;

            foreach (var schedule in programSchedules)
            {
                var scheduleAttribute = schedule.Attributes as ScheduleAfterBeforeCareAttribute;
                var price = new SubscriptionRegisterPriceViewModel();
                var times = new List<ProgramTimesViewModel>();

                price = new SubscriptionRegisterPriceViewModel()
                {
                    MinGrade = schedule.AttendeeRestriction != null && schedule.AttendeeRestriction.MinGrade != null ? schedule.AttendeeRestriction.MinGrade.ToDescription() : string.Empty,
                    MaxGrade = schedule.AttendeeRestriction != null && schedule.AttendeeRestriction.MaxGrade != null ? schedule.AttendeeRestriction.MaxGrade.ToDescription() : string.Empty,
                    Times = schedule.ScheduleMode != TimeOfClassFormation.Both ? GetTimesProgram(scheduleAttribute.Days) : null,
                    ComboTime = schedule.ScheduleMode == TimeOfClassFormation.Both ? comboTime : null,
                    StartDate = startDate,
                    EndDate = endDate,
                    ScheduleId = schedule.Id,
                    ScheduleTitle = schedule.Title,
                    ProgramType = program.TypeCategory,
                    ScheduleMode = schedule.ScheduleMode,
                    EqualMaxAndMinGrade = schedule.AttendeeRestriction != null && schedule.AttendeeRestriction.MinGrade != null && schedule.AttendeeRestriction.MaxGrade != null && (schedule.AttendeeRestriction.MinGrade == schedule.AttendeeRestriction.MaxGrade) ? true : false,
                };

                var schedulePrices = new List<SubscriptionRegisterPriceItemViewModel>();
                foreach (var charge in schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted))
                {
                    var chargeWeekDay = ((BeforeAfterCareChargeAttribute)charge.Attributes).NumberOfClassDay;

                    var charge1 = new SubscriptionRegisterPriceItemViewModel
                    {
                        Amount = charge.Amount,
                        Label = charge.Name,
                        Id = charge.Id,
                        NumberOfClassDays = model.Days.Count,
                        ChargeNumber = int.Parse(chargeWeekDay),
                    };

                    schedulePrices.Add(charge1);
                }


                price.Prices = schedulePrices.OrderByDescending(p => p.ChargeNumber).ToList();
                price.EnablePriceHidden = program.Club.Setting != null ? program.Club.Setting.ClubProgram.EnablePriceHidden ? !program.Club.Setting.ClubProgram.HideAllProgramsPrice ? program.Club.Setting.ClubProgram.HiddenPricePrograms != null ? program.Club.Setting.ClubProgram.HiddenPricePrograms.Any(p => p == program.Id) : false : true : false : false;

                var charges = new List<SubscriptionChargeItemRegisterViewModel>();

                price.Charges = charges;
                model.Prices.Add(price);
            }

            return View("_BeforeAfterCare", model);
        }

        [HttpPost]
        public virtual ActionResult CheckBeforeAfterCareValidation(DateTime? desiredStartDate, long programId, List<DayOfWeek> days, int selectedChargeId)
        {
            object result = null;
            var errors = new List<string>();

            bool isOfflineOrder = false;

            if (this.GetCurrentUserRoleType() != RoleCategoryType.Parent)
            {
                isOfflineOrder = true;
            }

            var subscriptionBusiness = Ioc.SubscriptionBusiness;
            var program = _programBusiness.Get(programId);

            var charge = subscriptionBusiness.GetCharge(selectedChargeId);

            var programSchedule = charge.ProgramSchedule;

            var nowDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club, DateTime.UtcNow);

            var attributes = subscriptionBusiness.GetBeforAfterCareAtribute(programSchedule);

            if (!desiredStartDate.HasValue)
            {
                errors.Add("Desired start date is required.");
            }
            else
            {
                if (desiredStartDate.Value.Date < nowDateTime.Date && isOfflineOrder == false)
                {
                    errors.Add("Desired start date must be after today.");
                }
                else if (programSchedule.StartDate.Date > desiredStartDate.Value.Date)
                {
                    errors.Add("Desired start date cannot be before program start date.");
                }
                else if (programSchedule.EndDate.Date < desiredStartDate.Value.Date)
                {
                    errors.Add("Desired start date cannot be after program end date.");
                }

                var selectedCharge = subscriptionBusiness.GetSelectedCharge(programSchedule, selectedChargeId);

                var selectedChargeAttribute = JsonConvert.DeserializeObject<BeforeAfterCareChargeAttribute>(selectedCharge.AttributesSerialized);

                var weekDays = selectedChargeAttribute.NumberOfClassDay;

                if (days != null && days.Any())
                {
                    if (!days.Contains(desiredStartDate.Value.DayOfWeek))
                    {
                        errors.Add("Desired start date should be one of the selected class days.");
                    }

                    if (int.Parse(weekDays) != 0 && int.Parse(weekDays) != days.Count())
                    {
                        errors.Add(string.Format("You should select {0} class days.", weekDays));
                    }

                }
                else
                {
                    errors.Add(string.Format("You should select {0} class days.", weekDays));
                }

                if (!errors.Any())
                {
                    var programSessions = new List<ProgramSession>();
                    var programSessionBusiness = Ioc.ProgramSessionBusiness;

                    // check sessions capacity
                    if (programSchedule.ScheduleMode != TimeOfClassFormation.Both)
                    {
                        programSessions = programSessionBusiness.GetList(programSchedule, desiredStartDate.Value, days);
                        var capacity = attributes.Capacity;

                        if (capacity != 0)
                        {
                            foreach (var item in programSessions)
                            {
                                if (programSessionBusiness.IsFull(item, capacity) && !program.DisableCapacityRestriction)
                                {
                                    errors.Add(string.Format("the day {0} is full.", item.StartDateTime.ToShortDateString()));
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        //if schedule is combo we should check the capacity of before school and after school programs 
                        var programSchedules = program.ProgramSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both && !s.IsDeleted);
                        foreach (var schedule in programSchedules)
                        {
                            var scheduleAttribiute = subscriptionBusiness.GetBeforAfterCareAtribute(schedule);
                            programSessions = programSessionBusiness.GetList(schedule, desiredStartDate.Value, days);
                            var capacity = scheduleAttribiute.Capacity;

                            if (capacity != 0)
                            {
                                foreach (var item in programSessions)
                                {
                                    if (programSessionBusiness.IsFull(item, capacity) && !program.DisableCapacityRestriction)
                                    {
                                        errors.Add(string.Format("the day {0} is full.", item.StartDateTime.ToShortDateString()));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            result = new { HasError = errors.Any(), Errors = errors };

            return JsonNet(result);
        }

        [HttpPost]
        public virtual ActionResult Calendar(long programId, long? scheduleId)
        {
            var model = new CalendarModel();

            model.ClubDomain = this.GetCurrentClubDomain();
            model.ProgramId = programId;
            var program = _programBusiness.Get(programId);
            model.ScheduleId = scheduleId.HasValue ? scheduleId.Value : (long?)null;
            model.Start = scheduleId.HasValue ? program.ProgramSchedules.First(s => s.Id == scheduleId).StartDate : DateTime.Now;

            if (program.TypeCategory != ProgramTypeCategory.BeforeAfterCare && program.TypeCategory != ProgramTypeCategory.Subscription)
            {
                return View("_ProgramCalendar", model);
            }
            else
            {
                return View("_SubscriptionCalendar", model);
            }
        }

        [Authorize(Roles = "Admin, Parent, Manager")]
        public virtual ActionResult GetCalendarItems([DataSourceRequest]DataSourceRequest request, long programId, long? scheduleId)
        {
            var sections = new List<CalendarModel>();

            bool isOfflineOrder = false;

            if (this.GetCurrentUserRoleType() != RoleCategoryType.Parent)
            {
                isOfflineOrder = true;
            }

            var program = _programBusiness.Get(programId);
            var club = program.Club;
            var schedules = program.ProgramSchedules.Where(s => !s.IsFreezed && !s.IsDeleted);

            ProgramSchedule schedule = null;

            if (scheduleId.HasValue)
            {
                schedule = _programBusiness.GetSchedule(scheduleId.Value);
            }
            else
            {
                schedule = schedules.Last();
            }

            int capacity;
            string title;
            decimal amount = 0;
            var enabledSameDay = false;

            var todayDate = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow);
            decimal sessionTodaysAmount = 0;

            if (program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                var subscriptionAttribute = Ioc.SubscriptionBusiness.GetSubscriptionAtribute(schedule);
                title = subscriptionAttribute.DropInSessionLabel;
                amount = subscriptionAttribute.DropInSessionPrice;
                capacity = subscriptionAttribute.Capacity;
            }
            else
            {
                var beforeAfterAttribuite = Ioc.SubscriptionBusiness.GetBeforAfterCareAtribute(schedule);
                title = beforeAfterAttribuite.DropInSessionLabel;
                amount = beforeAfterAttribuite.DropInSessionPrice;
                sessionTodaysAmount = beforeAfterAttribuite.DropInSameSessionPrice;
                capacity = beforeAfterAttribuite.Capacity;
                enabledSameDay = beforeAfterAttribuite.EnableSameDropInPrice;
            }

            var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow);

            var programSessionBusiness = Ioc.ProgramSessionBusiness;

            IEnumerable<ProgramSession> programSessions = null;

            if (!isOfflineOrder)
            {
                programSessions = programSessionBusiness.GetList(program).Where(s => s.StartDateTime.Date >= clubDateTime.Date);
            }
            else
            {
                programSessions = programSessionBusiness.GetList(program);
            }

            if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare && !isOfflineOrder)
            {
                var comboSchedule = program.ProgramSchedules.FirstOrDefault(s => !s.IsDeleted && s.ScheduleMode == TimeOfClassFormation.Both);
                var IscomboFreezed = comboSchedule != null ? comboSchedule.IsFreezed : false;

                if (comboSchedule != null && IscomboFreezed)
                {
                    programSessions = programSessions.Where(p => !p.ProgramSchedule.IsFreezed);
                }
            }

            var allProgramSessions = programSessions.ToList();

            _programSessionBusiness.ApplyHolidays(program, allProgramSessions);

            sections = allProgramSessions.Select(s =>
                  new CalendarModel
                  {
                      Id = s.Id,
                      TaskID = s.Id,
                      Selected = false,
                      DayDate = s.StartDateTime.Date.ToShortDateString(),
                      Start = DateTime.SpecifyKind(s.StartDateTime, DateTimeKind.Utc),
                      End = DateTime.SpecifyKind(s.EndDateTime, DateTimeKind.Utc),
                      DoDate = DateTime.SpecifyKind(s.StartDateTime, DateTimeKind.Utc),
                      IsAllDay = false,
                      Title = s.StartDateTime.ToShortTimeString() + "-" + s.EndDateTime.ToShortTimeString(),
                      ScheduleId = schedule.Id,
                      ScheduleTitle = schedule.Title,
                      Amount = enabledSameDay && program.TypeCategory == ProgramTypeCategory.BeforeAfterCare && s.StartDateTime.Date == todayDate.Date ? sessionTodaysAmount : amount,
                      ChargeName = s.StartDateTime.ToString("MMM dd") + " " + s.StartDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + "-" + s.EndDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", ""),
                      ChargeCategory = ChargeDiscountCategory.EntryFee.ToString(),
                      Description2 = schedule.Title,//schedule.Title,
                      StartTimezone = "Etc/UTC",
                      EndTimezone = "Etc/UTC",
                      IsFull = programSessionBusiness.IsFull(s) && !program.DisableCapacityRestriction,
                      FirstTime = s.StartDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + "-" + s.EndDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + Environment.NewLine,
                      SecondTime = string.Empty
                  })
               .ToList();

            return Json(sections.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateInput(false)]
        public virtual ActionResult CheckFormRestrictions(RegisterProgramModel model)
        {
            var errors = new Dictionary<string, string>();// new List<KeyValuePair<string, string>>();
            var hasError = false;
            var keys = new List<string>();

            var regServ = RegisterService.Get();
            var isOrderOnline = this.GetCurrentUserRoleType() != RoleCategoryType.Dashboard;

            regServ.CheckRestrictions(model, ModelState, isOrderOnline);

            if (!ModelState.IsValid)
            {
                errors = ModelState.Where(e => e.Value != null && e.Value.Errors.Any()).Select(t => new { t.Key, t.Value.Errors.First().ErrorMessage })
                   .ToDictionary(t => t.Key, t => t.ErrorMessage);

                hasError = true;

                keys = errors.Select(e => e.Key).ToList();
            }

            return JsonNet(new { HasError = hasError, Errors = errors, ErrorKeys = keys });
        }

        private List<ProgramTimesViewModel> GetTimesProgram(List<ProgramScheduleDay> days)
        {
            var programTimes = new List<ProgramTimesViewModel>();

            foreach (var day in days)
            {
                if (programTimes.Count == 0)
                {
                    programTimes.Add(new ProgramTimesViewModel
                    {
                        Day = day.DayOfWeek.ToString(),
                        StartTime = day.StartTime.Value,
                        EndTime = day.EndTime.Value,
                        MainTime = string.Format("{0} - {1}", DateTime.Today.Add(day.StartTime.Value).ToString("h:mm tt"), DateTime.Today.Add(day.EndTime.Value).ToString("h:mm tt")),

                    });
                }
                else
                {
                    var timeCount = 0;
                    foreach (var availabelTime in programTimes)
                    {
                        if (day.StartTime == availabelTime.StartTime && day.EndTime == availabelTime.EndTime)
                        {
                            timeCount++;

                            availabelTime.Day = availabelTime.Day + ", " + day.DayOfWeek.ToString();
                        }
                    }
                    if (timeCount == 0)
                    {
                        programTimes.Add(new ProgramTimesViewModel
                        {
                            Day = day.DayOfWeek.ToString(),
                            StartTime = day.StartTime.Value,
                            EndTime = day.EndTime.Value,
                            MainTime = string.Format("{0} - {1}", DateTime.Today.Add(day.StartTime.Value).ToString("h:mm tt"), DateTime.Today.Add(day.EndTime.Value).ToString("h:mm tt")),
                        });
                    }
                }
            }

            return programTimes;
        }
        [HttpPost]
        public virtual ActionResult HaveLargestNumberOfDay(long schedualeId, long selectedChargeId)
        {
            object result = null;

            var isLargest = false;

            var schedule = _programBusiness.GetSchedule(schedualeId);

            var selectedChargeNumberOfDay = 0;
            if (schedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare && selectedChargeId > 0)
            {
                var allScheduleCharges = schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted);
                var selectedCharge = allScheduleCharges.Where(c => c.Id == selectedChargeId).FirstOrDefault();
                if (selectedCharge != null)
                {
                    selectedChargeNumberOfDay = int.Parse(((BeforeAfterCareChargeAttribute)selectedCharge.Attributes).NumberOfClassDay);
                    var listNumberDays = new List<int>();

                    foreach (var charge in allScheduleCharges.Where(c => c.Id != selectedChargeId))
                    {
                        var numberOfDay = int.Parse(((BeforeAfterCareChargeAttribute)charge.Attributes).NumberOfClassDay);
                        listNumberDays.Add(numberOfDay);
                    }

                    isLargest = listNumberDays.Count > 0 && listNumberDays.OrderByDescending(c => c).First() > selectedChargeNumberOfDay ? false : true;
                }
            }

            result = new { IsLargest = isLargest, SelectedChargeNumberDays = selectedChargeNumberOfDay };

            return JsonNet(result);
        }

        private void ManipulateParentDashboardValidations(JbForm jbForm, string message)
        {
            if (message == "Participant")
            {
                ModelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.FirstName, "The participant already exists.");
            }
            if (message == "Parent1")
            {
                ModelState.AddModelError(jbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName, "The parent you have entered already exists in the form.");
            }
            if (message == "Parent2")
            {
                ModelState.AddModelError(jbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName, "The parent you have entered already exists in the form.");
            }

            if (message == "authorizedPickup1")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup1, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }
            if (message == "authorizedPickup2")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup2, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }
            if (message == "authorizedPickup3")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup3, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }
            if (message == "authorizedPickup4")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup4, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }
        }

        [Authorize(Roles = "Admin, Parent, Manager")]
        public virtual ActionResult GetProgramCalendarItems([DataSourceRequest]DataSourceRequest request, long programId, long scheduleId)
        {
            var sections = new List<ProgramCalendarModel>();

            //bool isOfflineOrder = false;

            //if (this.GetCurrentUserRoleType() != RoleCategoryType.Parent)
            //{
            //    isOfflineOrder = true;
            //}

            var program = _programBusiness.Get(programId);
            var club = program.Club;
            var schedules = program.ProgramSchedules.Where(s => !s.IsDeleted);

            ProgramSchedule schedule = null;
            schedule = _programBusiness.GetSchedule(scheduleId);

            var dropInCharges = schedule.Charges.Where(c => !c.IsDeleted).OrderBy(c => c.Id).ToList();

            var programSessions = Ioc.ProgramSessionBusiness.GetProgramSessions(dropInCharges);

            sections = programSessions.GroupBy(c => c.StartDateTime.Date).Select(s =>
              new ProgramCalendarModel
              {
                  Selected = false,
                  DayDate = s.Key.ToString(),
                  Start = DateTime.SpecifyKind(s.Key, DateTimeKind.Utc),
                  End = DateTime.SpecifyKind(s.Key.AddHours(1), DateTimeKind.Utc),
                  IsAllDay = false,
                  ScheduleId = schedule.Id,
                  ScheduleTitle = schedule.Title,
                  ChargeCategory = ChargeDiscountCategory.EntryFee.ToString(),
                  Description2 = schedule.Title,
                  StartTimezone = "Etc/UTC",
                  EndTimezone = "Etc/UTC",
                  Sessions = s.Select(a =>
                    new ProgramCalnedarSessinItemModel
                    {
                        Selected = false,
                        DayDate = a.StartDateTime.Date.ToShortDateString(),
                        Start = DateTime.SpecifyKind(a.StartDateTime, DateTimeKind.Utc),
                        End = DateTime.SpecifyKind(a.EndDateTime, DateTimeKind.Utc),
                        DoDate = DateTime.SpecifyKind(a.StartDateTime, DateTimeKind.Utc),
                        IsAllDay = false,
                        Id = a.Id,
                        Description2 = schedule.Title,
                        TaskID = a.Id,
                        Title = dropInCharges.FirstOrDefault(c => c.Id == a.ChargeId).Attributes.DropInName + " " + CurrencyHelper.FormatCurrencyWithPenny(dropInCharges.FirstOrDefault(c => c.Id == a.ChargeId).Attributes.DropInPrice.Value, club.Currency),
                        Amount = dropInCharges.FirstOrDefault(c => c.Id == a.ChargeId).Attributes.DropInPrice.Value,
                        ChargeName = a.StartDateTime.ToString("MMM dd") + " " + a.StartDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + "-" + a.EndDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", ""),
                        IsFull = _programBusiness.IsFull(dropInCharges.FirstOrDefault(c => c.Id == a.ChargeId), a.Charge.ProgramSchedule.Program.Season.Status == SeasonStatus.Test) && !program.DisableCapacityRestriction,
                        FirstTime = dropInCharges.FirstOrDefault(c => c.Id == a.ChargeId).Attributes.DropInName + " " + CurrencyHelper.FormatCurrencyWithPenny(dropInCharges.FirstOrDefault(c => c.Id == a.ChargeId).Attributes.DropInPrice.Value, club.Currency),
                        SecondTime = string.Empty,
                        ScheduleId = schedule.Id,
                        ChargeCategory = ChargeDiscountCategory.EntryFee.ToString(),
                    })
                    .ToList()
              })
           .ToList();

            return Json(sections.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
    }
}