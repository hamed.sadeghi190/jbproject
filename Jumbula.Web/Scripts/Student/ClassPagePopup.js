﻿function showPopupProgram(event) {
    event.preventDefault();
    var autoSlide;
    var programID = event.target.dataset.programId
    var target = event.target || event.srcElement;
    var popupData = {};
    if (event.target.dataset.enabledPopup === "False") {
        location.href = target.getAttribute('href')
    }
    else {
        var sliderInterval, slideIndex = 0;

        function showSlide(showIndex) {
            for (var slide in popupData.ImageUrls) {
                popupData.ImageUrls[slide].slideActive = false;
            }
            popupData.ImageUrls[showIndex].slideActive = true;
        }

        $.post("/JbPage/GetProgramInformation", { 'programId': programID }, function (result) {
            popupData = result.DataSource;
            $.each(popupData.Schedule.Prices,
                function (index, charge) {
                    //Spots
                    charge.spotNote = "";
                    if (charge.CapacityLeft > 0 &&
                        popupData.Schedule.RecurrenceTime.ShowCapacityLeft &&
                        charge.CapacityLeft <= popupData.Schedule.RecurrenceTime.ShowCapacityLeftNumber) {
                        charge.spotNote = charge.CapacityLeft + " spots left";
                    } else if (
                        popupData.Schedule.TotalCapacityLeft > 0 &&
                        popupData.Schedule.RecurrenceTime
                            .ShowCapacityLeft &&
                        popupData.Schedule.TotalCapacityLeft <=
                        popupData.Schedule.RecurrenceTime
                            .ShowCapacityLeftNumber) {
                        charge.spotNote =
                            popupData.Schedule.TotalCapacityLeft +
                            " spots left";
                    }
                    if (charge.CapacityLeft == 0) {
                        charge.spotNote = "Full";
                    }


                });
            //Tuitions end
            //dayOfweek
            popupData.Schedule.RecurrenceTime.FullInfoDays = {
                Sunday: false,
                Monday: false,
                Tuesday: false,
                Wednesday: false,
                Thursday: false,
                Friday: false,
                Saturday: false
            }
            $.each(popupData.ProgramDays,
                function (index, day) {
                    popupData.Schedule.RecurrenceTime.FullInfoDays[day
                        .DayOfWeek] = day
                })   
            //Restrictions
            if (!(
                popupData.Schedule.Restrict.RestrictionType == 'None' &&
                (!popupData.Schedule.Restrict.Gender ||
                    popupData.Schedule.Restrict.Gender &&
                    (popupData.Schedule.Restrict.Gender ==
                        'NoRestriction')))) {

                popupData.Schedule.Restrict.Active = true;
                popupData.RestrictionMessages.Active = "";
                popupData.RestrictionMessages.ActiveGender = "";

                switch (popupData.Schedule.Restrict.RestrictionType) {
                    case 'Age':
                        popupData.RestrictionMessages.Active =
                            popupData.RestrictionMessages.AgeMessage;
                        break;
                    case 'Grade':
                        popupData.RestrictionMessages.Active =
                            popupData.RestrictionMessages.GradeMessage;
                        break;
                }
                if (popupData.Schedule.Restrict.Gender) {
                    if (popupData.Schedule.Restrict.Gender !=
                        'NoRestriction') {
                        popupData.RestrictionMessages.ActiveGender =
                            popupData.RestrictionMessages.GenderMessage;
                    }

                } else {
                    popupData.Schedule.Restrict.Active = false;
                }
            }

            //Categories
            var categoryNames = [];
            $.each(popupData.Categories,
                function (index, category) {
                    categoryNames.push(category.Name);
                    popupData.Categories.Names =
                        categoryNames.join(', ');
                });
            //set text values in popup
            $('[data-popup-name]').text(popupData.Name)
            $('[data-popup-categories-names]').text("(" + popupData.Categories.Names + ")")

            if (popupData.AppearanceSetting.ShowProviderNameInClassPage == true && popupData.HasProvider == true) {
                $('[data-popup-provider-name]').text(popupData.ProviderName)
            }
            else {
                $('[data-popup-provider-name]').hide()
            }

            if (popupData.HasTestimonials) {

                $('[data-popup-testimonials-title]').text(popupData.AppearanceSetting.TestimonialsTitle);
                $('[data-popup-data-testimonials]').html(popupData.Testimonials);
            }
            else {
                $('[data-popup-show-testimonials]').hide();
            }

            var sunday = $('[data-popup-has-sunday]');
            if (!popupData.Schedule.RecurrenceTime.FullInfoDays.Sunday) {          
                sunday.addClass('closed');
                sunday.find('.times').html('<span>&nbsp;</span>');
            }
            else {
                sunday.removeClass('closed');
                sunday.find('.icon').addClass('jbi-check');
                sunday.find('.times').html('<span>' + popupData.Schedule.RecurrenceTime.FullInfoDays.Sunday.StrTimes + '</span>');
            }

            var monday = $('[data-popup-has-monday]');
            if (!popupData.Schedule.RecurrenceTime.FullInfoDays.Monday) {
                monday.addClass('closed');
                monday.find('.times').html('<span>&nbsp;</span>');
            }
            else {
                monday.removeClass('closed');
                monday.find('.icon').addClass('jbi-check');
                monday.find('.times').html('<span>' + popupData.Schedule.RecurrenceTime.FullInfoDays.Monday.StrTimes + '</span>');
            }


            var tuesday = $('[data-popup-has-tuesday]');
            if (!popupData.Schedule.RecurrenceTime.FullInfoDays.Tuesday) {
                tuesday.addClass('closed');
                tuesday.find('.times').html('<span>&nbsp;</span>');
            }
            else {
                tuesday.removeClass('closed');
                tuesday.find('.icon').addClass('jbi-check');
                tuesday.find('.times').html('<span>' + popupData.Schedule.RecurrenceTime.FullInfoDays.Tuesday.StrTimes + '</span>');
            }

            var wednesday = $('[data-popup-has-wednesday]');
            if (!popupData.Schedule.RecurrenceTime.FullInfoDays.Wednesday) {
                wednesday.addClass('closed');
                wednesday.find('.times').html('<span>&nbsp;</span>');
            }
            else {
                wednesday.removeClass('closed');
                wednesday.find('.icon').addClass('jbi-check');
                wednesday.find('.times').html('<span>' + popupData.Schedule.RecurrenceTime.FullInfoDays.Wednesday.StrTimes + '</span>');
            }
            var friday = $('[data-popup-has-friday]');
            if (!popupData.Schedule.RecurrenceTime.FullInfoDays.Friday) {
                friday.addClass('closed');
                friday.find('.times').html('<span>&nbsp;</span>');
            }
            else {
                friday.removeClass('closed');
                friday.find('.icon').addClass('jbi-check');
                friday.find('.times').html('<span>' + popupData.Schedule.RecurrenceTime.FullInfoDays.Friday.StrTimes + '</span>');
            }
            var saturday = $('[data-popup-has-saturday]');
            if (!popupData.Schedule.RecurrenceTime.FullInfoDays.Saturday) {
                saturday.addClass('closed');
                saturday.find('.times').html('<span>&nbsp;</span>');
            }
            else {
                saturday.removeClass('closed');
                saturday.find('.icon').addClass('jbi-check');
                saturday.find('.times').html('<span>' + popupData.Schedule.RecurrenceTime.FullInfoDays.Saturday.StrTimes + '</span>');
            }

            var thursday = $('[data-popup-has-thursday]');
            if (!popupData.Schedule.RecurrenceTime.FullInfoDays.Thursday) {
                thursday.addClass('closed');
                thursday.find('.times').html('<span>&nbsp;</span>');
            }
            else {
                thursday.removeClass('closed');
                thursday.find('.icon').addClass('jbi-check');
                thursday.find('.times').html('<span>' + popupData.Schedule.RecurrenceTime.FullInfoDays.Thursday.StrTimes + '</span>');
            }

            //satrt and end date
            $('[data-popup-start-date]').text(popupData.StrStartDate);
            $('[data-popup-end-date]').text(popupData.StrEndDate);

            //show class dates
            if (popupData.strClassDates && !popupData.AppearanceSetting.HideProgramDates) {
                $('[data-popup-show-class-dates]').show();
                $('[data-popup-class-dates]').text(popupData.strClassDates);
            }
            else {
                $('[data-popup-show-class-dates]').hide();
            }
            $('[data-registration-start-date]').text(popupData.RegistrationPriodStartDate)
            $('[data-registration-end-date]').text(popupData.RegistrationPriodEndDate)
            var schedulePrices = "";
            $.each(popupData.Schedule.Prices, function (index, value) {
                schedulePrices += '<div class="tuition">';
                schedulePrices += '<span class="tuition-label" title="' + value.Name + '">' + value.Name + '</span>';
                if (value.spotNote)
                    schedulePrices += '<span class="label label-danger" style="font-family:\'Segoe UI\', Helvetica, Arial, sans-serif; font-size: 11px; font-weight: 400; letter-spacing: .4px; position: relative; bottom: 2px;">' + value.spotNote + '</span>';
                if (value.Description)
                    schedulePrices += '<span>' + value.Description + '</span>';
                schedulePrices += '<div class="tuition-prices"> <span>' + value.StrAmount + '</span></div>';
                schedulePrices += '<div class="clearfix"></div>';
                schedulePrices += '</div >';
            })
            $('[data-popup-schedule-prices]').html(schedulePrices);
            if (popupData.Schedule.ProgramServices.length !== 0) {
                $('[data-popup-show-services]').show();
                var programServices = "";
                $.each(popupData.Schedule.ProgramServices, function (index, value) {
                    var baseClass = value.Category == 'CustomCharge' && 'jbi-coins' || value.Category == 'Lunch' && 'jbi-lunch' || value.Category == 'DayCare' && 'jbi-mom';
                    programServices += "<div  class='" + baseClass + " program-service'>";
                    programServices += '<p class="program-service-title ellipsis" title="' + value.Name + '"><span>' + value.Name + '</span>';
                    if (value.Description)
                        programServices += '<span class="help-white" title="' + value.Description + '"></span>'
                    programServices += "</p>";
                    programServices += '<span class="program-service-value">' + value.StrAmount + '</span>'
                    programServices += "</div>"
                });
                $('[data-popup-program-services]').html(programServices);
            }
            else {
                $('[data-popup-show-services]').hide()
            }
            if (popupData.Description) {
                $('[data-popup-show-description]').show();
                $('[data-popup-show-description]').html(popupData.Description);
            }
            else {
                $('[data-popup-show-description]').hide();
            }
            if (popupData.Address.Address) {
                $("[data-popup-show-address]").html(popupData.Address.Address);
            }
            if (popupData.MaxEnrollment || popupData.MinEnrollment) {
                $("[data-popup-show-min-max-enrollment]").show();
                if (popupData.MinEnrollment) {
                    $("[data-popup-show-min-enrollment]").html("Minimum: " + popupData.MinEnrollment)
                }
                if (popupData.MaxEnrollment) {
                    $("[data-popup-show-max-enrollment]").html("Maximum: " + popupData.MaxEnrollment)
                }
            }
            else {
                $("[data-popup-show-min-max-enrollment]").hide()
            }
            if (popupData.Schedule.Restrict.Active) {
                $('[data-popup-show-restrict-active]').show();
            }
            else {
                $('[data-popup-show-restrict-active]').hide();
            }
            if (popupData.RestrictionMessages.Active) {
                $("[data-popup-restriction-messages-active]").show().html(popupData.RestrictionMessages.Active);
            }
            else {
                $("[data-popup-restriction-messages-active]").hide();
            }
            if (popupData.RestrictionMessages.ActiveGender) {
                $("[data-popup-restriction-messages-active-gender]").show().html(popupData.RestrictionMessages.ActiveGender);
            }
            else {
                $("[data-popup-restriction-messages-active-gender]").hide();
            }
            var dataContact = "";
            if (!popupData.HasProvider) {
                if (popupData.ClubDomain == 'flexacademies') {

                    if (popupData.ClubDomain == 'flexacademies') {
                        dataContact += "<div>";
                        dataContact += " <p>If you have questions about the class content or activities program, please contact Flex Academies.</p>";

                        if ((popupData.ClubPhone != null && popupData.ClubPhone != undefined) && !popupData.HidePhoneNumber)
                            dataContact += "<div>"
                        dataContact += '<div class="icon-item" style="margin-top:8px;"> <span class="jbi-phone icon"></span><strong>Phone:</strong> <span  title="' + popupData.ClubPhone + '">' + popupData.ClubPhone + '</span></div>';
                        dataContact += "</div>"

                        if (popupData.PhoneExtension) {
                            dataContact += '<div class="icon-item" style="margin-top:8px;">'
                            dataContact += '<span class="jbi-phone icon"></span><strong>Phone:</strong>'
                            dataContact += '<span title="' + popupData.PhoneExtension + '">' + popupData.ClubPhoneExtension + '</span> </div>'
                        }
                        if (popupData.ClubEmail != null) {
                            dataContact += '<div class="icon-item" style="margin-top:4px">';
                            dataContact += ' <span class="jbi-email icon"></span><strong>Email:</strong>'
                            dataContact += ' <a href="mailto:' + popupData.ClubEmail + '" title="' + popupData.ClubEmail + '">' + popupData.ClubEmail + '</a></div>'
                        }
                        dataContact += "</div>";
                    }

                }

                if (popupData.ClubDomain != 'flexacademies') {
                    dataContact += "<div>";
                    dataContact += "<p>Please contact " + popupData.ClubName + " office if you have any questions.</p>";
                    if (popupData.ClubPhone != null && !popupData.AppearanceSetting.HidePhoneNumber) {
                        dataContact += '<div class="icon-item" style="margin-top:8px;">';
                        dataContact += '<span class="jbi-phone icon"></span><strong>Phone: </strong>';
                        if (!popupData.ClubPhoneExtension) {
                            dataContact += '<span title="' + popupData.ClubPhone + '">' + popupData.ClubPhone + '</span>';
                        }
                        if (popupData.ClubPhoneExtension) {
                            dataContact += '<span title="' + popupData.ClubPhone + ' Ext. ' + popupData.ClubPhoneExtensio + '">' + popupData.ClubPhone + " Ext. " + popupData.ClubPhoneExtension + '</span>';
                        }
                        dataContact += '</div>'
                    }
                    if (popupData.ClubEmail != null) {
                        dataContact += '<div class="icon-item" style="margin-top:4px">';
                        dataContact += '<span class="jbi-email icon"></span><strong>Email: </strong>';
                        dataContact += '<a href="mailto:' + popupData.ClubEmail + '" title="' + popupData.ClubEmail + '">' + popupData.ClubEmail + '</a>';
                        dataContact += '</div>';
                    }
                    dataContact += "</div>";
                }
            }
            if (popupData.HasProvider) {

                if (popupData.ClubDomain == 'flexacademies') {
                    dataContact += "<div>";
                    dataContact += '<p>If you have questions about the class content or activities program, please contact Flex Academies.</p>'
                    if ((popupData.ClubPhone != null && popupData.ClubPhone != undefined && popupData.ClubPhone) && !popupData.HidePhoneNumber) {
                        dataContact += "<div>";
                        dataContact += '<div class="icon-item" style="margin-top:8px;">';
                        dataContact += '<span class="jbi-phone icon"></span><strong>Phone:</strong>';
                        dataContact += '<span title="' + popupData.ClubPhone + '">' + popupData.ClubPhone + '</span>';
                        dataContact += '</div>';
                        if (popupData.PhoneExtension) {
                            dataContact += '<div class="icon-item" style="margin-top:8px;">';
                            dataContact += '<span class="jbi-phone icon"></span><strong>Phone:</strong>'
                            dataContact += '<span title="' + popupData.PhoneExtension + '">' + popupData.ClubPhoneExtension + '</span>';
                            dataContact += '</div>';
                        }
                        dataContact += "</div>";
                    }

                    if (popupData.ClubEmail) {
                        dataContact += '<div class="icon-item" style="margin-top:4px">';
                        dataContact += '<span class="jbi-email icon"></span><strong>Email: </strong>';
                        dataContact += '<a href="mailto:' + popupData.ClubEmail + '"  title="' + popupData.ClubEmail + '">' + popupData.ClubEmail + '</a>';
                        dataContact += '</div>'
                    }

                    dataContact += "</div>";
                }

                if (popupData.ClubDomain != 'flexacademies') {
                    dataContact += "<div>";
                    dataContact += '<p>If you have questions about the class content, please contact ' + popupData.ProviderName + '.</p>';
                    if (popupData.ProviderPhone != null) {
                        dataContact += '<div  class="icon-item" style="margin-top:8px;">';
                        dataContact += ' <span class="jbi-phone icon"></span><strong>Phone: </strong>';
                        dataContact += '<span title="' + popupData.ProviderPhone + '">' + popupData.ProviderPhone + '</span>';
                        dataContact += '</div>'
                    }
                    if (popupData.ProviderPhone != null) {
                        dataContact += '<div class="icon-item" style="margin-top:4px">';
                        dataContact += '<span class="jbi-email icon"></span><strong>Email: </strong>';
                        dataContact += '<a href="mailto:' + popupData.ProviderEmail + '"  title="' + popupData.ProviderEmail + '">' + popupData.ProviderEmail +'</a>';
                        dataContact += '</div>';
                    }
                    dataContact += ' <p style="margin-top:10px;">If you have questions about the ' + popupData.ClubName + ' program, please contact the Enrichment Coordinator.</p>';
                    if (popupData.ClubEmail != null) {
                        dataContact += '<div class="icon-item" style="margin-top:4px">';
                        dataContact += '<span class="jbi-email icon"></span><strong>Email: </strong>';
                        dataContact += '<a href="mailto:' + popupData.ClubEmail + '" title="' + popupData.ClubEmail + '">' + popupData.ClubEmail +'</a>';
                        dataContact += '</div>';
                    }

                    dataContact += "</div>";
                }

            }

            $('[data-popup-any-questions]').html(dataContact);
            //slider

            var popupSlider = $('[data-popup-show-image-slider]');
            if (popupData.ImageUrls.length != 0) {
                var sliderImages = "";
                $.each(popupData.ImageUrls, function (index, imgSlide) {
                    sliderImages += '<div>';
                    sliderImages += '<img class="small-image-slide" src="' + imgSlide.SmallUrl + '"/>';
                    var largeImageURL = imgSlide.LargeUrl ? imgSlide.LargeUrl : imgSlide.SmallUrl;
                    sliderImages += '<img class="large-image-slide" src="' + largeImageURL + '"/>';
                    sliderImages += '</div>';
                });
                sliderImages += '<span class="next-slide-area" data-popup-next-slide></span>';
                sliderImages += '<i class="jbi-angle-right" data-popup-next-slide></i>';
                sliderImages += '<span class="prev-slide-area" data-popup-prev-slide></span>';
                sliderImages += '<i class="jbi-angle-left" data-popup-prev-slide></i>';
                sliderImages += '<i class="jbi-arrow-left close-slider"></i>'

             
                popupSlider.html(sliderImages);

                var currentIndex = 0;
                var slideItems = $("[data-popup-show-image-slider]>div");
                var itemCount = slideItems.length;

                function cycleItems() {
                    var item = slideItems.eq(currentIndex);
                    slideItems.hide();
                    item.fadeIn(200);
                }

                autoSlide = setInterval(function () {
                    currentIndex += 1;
                    if (currentIndex > itemCount - 1) {
                        currentIndex = 0;
                    }
                    cycleItems();
                }, 5000);
                cycleItems();
                $("[data-popup-next-slide]").on('click', function () {
                    currentIndex += 1;
                    if (currentIndex > itemCount - 1) {
                        currentIndex = 0;
                    }
                    cycleItems();
                });
                $("[data-popup-prev-slide]").on('click', function () {
                    currentIndex = currentIndex - 1;
                    if (currentIndex < 0) {
                        currentIndex = itemCount - 1;
                    }
                    cycleItems();
                });
                //Slider open
                function sliderOpen() {
                    popupSlider.addClass('popup-slider-open');
                    popupSlider.find('.small-image-slide').hide();
                    popupSlider.find('.large-image-slide').show();
                    popupSlider.find('.close-slider').show();
                }
                //Slider close
                function sliderClose() {
                    popupSlider.find('.large-image-slide').hide();
                    popupSlider.removeClass('popup-slider-open');
                    popupSlider.find('.small-image-slide').show();
                    popupSlider.find('.large-image-slide').hide();
                    popupSlider.find('.close-slider').hide();

                }
                $('.large-image-slide').on('click', function () {
                    sliderClose();
                });
                $('.small-image-slide').on('click', function () {
                    sliderOpen();
                });
                $('.close-slider').on('click', function () {
                    sliderClose();
                });
                $("[data-popup-next-slide],[data-popup-prev-slide]").on('mouseover', function () {
                    clearInterval(autoSlide)
                });
                $("[data-popup-next-slide],[data-popup-prev-slide]").on('mouseleave', function () {

                    autoSlide = setInterval(function () {
                        currentIndex += 1;
                        if (currentIndex > itemCount - 1) {
                            currentIndex = 0;
                        }
                        cycleItems();
                    }, 5000);

                })
            }
            else {
                popupSlider.html("");
            }
         
        });

        //Show popup start

        //get base elements
        var element = $('[data-program-info]'),
            tabContent = element.find('.tab-modal__contents'),
            tabItems = element.find('.tab-modal__items'),
            activeElement = element.find('.tab-modal__item-active'),
            closeBtn = element.find('.modal-fluid__btn-close');

        //    //tab click
        tabItems.find('[data-modal-tab]').on('click',
            function () {

                activeElement = element.find('.tab-modal__item-active');
                tabContent.find('[data-modal-tab]').css('display', 'none');
                activeElement.removeClass('tab-modal__item-active');
                $(this).addClass('tab-modal__item-active');
                activeElement = element.find('.tab-modal__item-active');
                tabContent.find('[data-modal-tab=' + activeElement.attr('data-modal-tab') + ']')
                    .css('display', 'block');
            });
        //close
        closeBtn.on('click',
            function () {

                //element.find('.tab-modal__footer').hide();
                activeElement = element.find('.tab-modal__item-active');
                tabContent.find('[data-modal-tab]').css('display', 'none');
                activeElement.removeClass('tab-modal__item-active');
                tabItems.find("[data-modal-tab=first]").addClass('tab-modal__item-active');
                element.stop().slideUp(500);
                popupData = {};
                clearInterval(autoSlide);
            })

        //show popup
        element.stop().slideDown(500);
        tabContent.find('[data-modal-tab]').css('display', 'none');
        tabContent.find('[data-modal-tab=' + activeElement.attr('data-modal-tab') + ']')
            .css('display', 'block');
    }


}
