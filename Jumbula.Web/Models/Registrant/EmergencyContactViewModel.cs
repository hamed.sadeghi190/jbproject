﻿
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Enums;

namespace SportsClub.Models
{
    public class EmergencyContactViewModel
    {
        public int Age { get; set; }
        public int Id { get; set; }
        public PageMode PageMode { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete first name.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete last name.")]
        public string LastName { get; set; }
        public string DOB { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "The phone number is invalid.")]
        public string PrimaryPhone { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "The phone number is invalid.")]
        public string CellPhone { get; set; }
        public string FullName { get; set; }

        [EmailAddress(ErrorMessage = "Email is not a valid email address.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public int UserId { get; set; }
        public int ContactId { get; set; }

        public Relationship? Relationship { get; set; }
        
    }
    
}