﻿using System;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Constants;
using Jumbula.Core.Model.ClubSetting;

namespace Jumbula.Business
{
    public class RegisterBusiness : IRegisterBusiness
    {
        private readonly IOrderBusiness _orderBusiness;
        private readonly IFollowupFormBusiness _followupFormBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IClubSettingBusiness _clubSettingBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;
        private readonly IClubBusiness _clubBusiness;


        public RegisterBusiness(IOrderBusiness orderBusiness, IFollowupFormBusiness followupFormBusiness, IOrderItemBusiness orderItemBusiness, IProgramBusiness programBusiness, IClubSettingBusiness clubSettingBusiness, IOrderSessionBusiness orderSessionBusiness, IClubBusiness clubBusiness)
        {
            _orderBusiness = orderBusiness;
            _followupFormBusiness = followupFormBusiness;
            _orderItemBusiness = orderItemBusiness;
            _programBusiness = programBusiness;
            _clubSettingBusiness = clubSettingBusiness;
            _orderSessionBusiness = orderSessionBusiness;
            _clubBusiness = clubBusiness;
        }

        public void MultiRegisterAddEditStep4(List<JbForm> followUpForms, long ists, bool hasWaiver, bool hasPaymentPlant, int userId)
        {
            RegistrationStep lastStep = RegistrationStep.Step4;

            var orderItems = _orderItemBusiness.GetOrderItemByISTS(ists, userId, OrderItemStatusCategories.notInitiated);

            var order = orderItems.First().Order;

            foreach (var orderItem in orderItems)
            {
                orderItem.FollowupForms.Clear();

                foreach (var followUp in followUpForms)
                {
                    var clubformTemplate = orderItem.ProgramSchedule.Program.ClubFormTemplates.FirstOrDefault(f => f.JbForm_Id == followUp.Id || f.JbForm_Id == followUp.ParentId);

                    var formParentId = followUp.ParentId ?? followUp.Id;

                    if (clubformTemplate == null) continue;
                    var orderItemForm = _followupFormBusiness.CreateOrderItemMultiRegisterFollowUpForm(followUp, clubformTemplate, orderItem, formParentId);
                    orderItem.FollowupForms.Add(orderItemForm);
                }

                if (orderItem.LastStep < lastStep)
                {
                    orderItem.LastStep = lastStep;
                }
                _orderItemBusiness.InitiateOrderItem(orderItem, hasWaiver, hasPaymentPlant);
            }

            _orderBusiness.Update(order);
        }

        public void CheckDuplicateEnrollment(List<OrderItem> regularOrderItems)
        {
            regularOrderItems = regularOrderItems.Where(o => o.ProgramScheduleId.HasValue).ToList(); //Remove donation

            foreach (var orderItem in regularOrderItems)
            {
                var duplicateEnrollmentPolicy = _clubSettingBusiness.GetDuplicateEnrollmentPolicy(orderItem.Order.ClubId);

                string errorMessage = null;

                var profileOtherOrderItems = regularOrderItems.Where(i => i.Id != orderItem.Id && i.PlayerId == orderItem.PlayerId && i.SeasonId == orderItem.SeasonId).ToList();

                var allProfileRegisteredOrderItems = _orderItemBusiness.GetRegisteredProfileBySeason(orderItem.PlayerId, orderItem.SeasonId).ToList();

                switch (orderItem.ProgramTypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        {
                            errorMessage = CheckDuplicateEnrollmentForClass(duplicateEnrollmentPolicy, allProfileRegisteredOrderItems, profileOtherOrderItems, orderItem);
                        }
                        break;
                    case ProgramTypeCategory.Camp:
                        {
                            errorMessage = CheckDuplicateEnrollmentForCamp(duplicateEnrollmentPolicy, allProfileRegisteredOrderItems, profileOtherOrderItems, orderItem);
                        }
                        break;
                    case ProgramTypeCategory.BeforeAfterCare:
                    case ProgramTypeCategory.Subscription:
                        {
                            errorMessage = CheckDuplicateEnrollmentForBeforeAfter(duplicateEnrollmentPolicy, allProfileRegisteredOrderItems, profileOtherOrderItems, orderItem);
                        }
                        break;
                }

                SetErrorMessageDuplicateEnrollment(orderItem, errorMessage);
            }

        }

        public string IsConflictPrograms(long currentProgramId, List<long> programIds)
        {
            var allProgramIds = new List<long>();
            allProgramIds.Add(currentProgramId);
            allProgramIds.AddRange(programIds);

            var programSchedules = _programBusiness.GetList(allProgramIds).SelectMany(p => p.ProgramSchedules.Where(s => !s.IsDeleted)).ToList();

            var sessions = _programBusiness.GetSessions(programSchedules);

            var currentProgramScheduleId = programSchedules.First(p => p.ProgramId == currentProgramId).Id;

            var currentProgramSessions = sessions.Where(s => s.ScheduleId == currentProgramScheduleId);

            sessions = sessions.Where(s => currentProgramSessions != s).ToList();

            var conflictMessage = "";

            foreach (var session in currentProgramSessions)
            {
                var sameSessions = sessions.Where(s => s.Id != session.Id && s.Start.Date == session.Start.Date).ToList();

                if (sameSessions.Any())
                {
                    foreach (var day in sameSessions)
                    {
                        if (session.End <= day.Start || session.Start >= day.End)
                        {
                            continue;
                        }

                        conflictMessage = $"This program conflicts with {programSchedules.First(p => p.Id == day.ScheduleId).Program.Name}";

                        break;
                    }
                }

                if (!string.IsNullOrEmpty(conflictMessage)) break;
            }

            return conflictMessage;

        }


        private string CheckDuplicateEnrollmentForClass(DuplicateEnrollmentPolicySettingViewModel duplicateEnrollmentPolicy, List<OrderItem> allProfileRegisteredClassOrderItems, List<OrderItem> profileOtherClassOrderItems, OrderItem orderItem)
        {
            allProfileRegisteredClassOrderItems = allProfileRegisteredClassOrderItems.Where(r => r.ProgramTypeCategory == ProgramTypeCategory.Class).ToList();

            profileOtherClassOrderItems = profileOtherClassOrderItems.Where(r => r.ProgramTypeCategory == ProgramTypeCategory.Class).ToList();

            if (duplicateEnrollmentPolicy.PreventSameClass)
            {
                if (IsRegisterSameClassTwice(profileOtherClassOrderItems, orderItem)) return CartConstants.DuplicateRegistrationInCart;

                if (IsRegisterSameClassTwice(allProfileRegisteredClassOrderItems, orderItem)) return CartConstants.AlreadyRegisteredInSeason;
            }

            if (duplicateEnrollmentPolicy.PreventOtherClasses)
            {
                profileOtherClassOrderItems = profileOtherClassOrderItems.Where(p => p.ProgramScheduleId != orderItem.ProgramScheduleId).ToList();
                allProfileRegisteredClassOrderItems = allProfileRegisteredClassOrderItems.Where(p => p.ProgramScheduleId != orderItem.ProgramScheduleId).ToList();

                if (IsRegisterTwoClassesWithConflictingHours(profileOtherClassOrderItems, orderItem)) return CartConstants.ConflictClassSchedule;

                if (IsRegisterTwoClassesWithConflictingHours(allProfileRegisteredClassOrderItems, orderItem)) return  CartConstants.ConflictClassScheduleWithPreviousRegistration;
            }

            return string.Empty;
        }

        private string CheckDuplicateEnrollmentForCamp(DuplicateEnrollmentPolicySettingViewModel duplicateEnrollmentPolicy, List<OrderItem> allProfileRegisteredCampOrderItems, List<OrderItem> profileOtherCampOrderItems, OrderItem orderItem)
        {
            allProfileRegisteredCampOrderItems = allProfileRegisteredCampOrderItems.Where(r => r.ProgramTypeCategory == ProgramTypeCategory.Camp).ToList();

            profileOtherCampOrderItems = profileOtherCampOrderItems.Where(r => r.ProgramTypeCategory == ProgramTypeCategory.Camp).ToList();

            if (duplicateEnrollmentPolicy.PreventSameCampSchedule)
            {
                var registeredOrderItemsNormal = allProfileRegisteredCampOrderItems.Where(o => o.Mode == OrderItemMode.Noraml).ToList();

                var regularOrderItemsNormal = profileOtherCampOrderItems.Where(o => o.Mode == OrderItemMode.Noraml).ToList();

                if (orderItem.Mode == OrderItemMode.Noraml && IsRegisterSameClassTwice(regularOrderItemsNormal, orderItem)) return CartConstants.DuplicateRegistrationInCart;

                if (orderItem.Mode == OrderItemMode.Noraml && IsRegisterSameClassTwice(registeredOrderItemsNormal, orderItem)) return CartConstants.AlreadyRegisteredInSeason;
            }

            if (duplicateEnrollmentPolicy.PreventMultipleCampDropIn)
            {
                var registeredOrderItemsDropIn = allProfileRegisteredCampOrderItems.Where(o => o.Mode == OrderItemMode.DropIn).ToList();

                var regularOrderItemsDropIn = profileOtherCampOrderItems.Where(o => o.Mode == OrderItemMode.DropIn).ToList();

                if (orderItem.Mode == OrderItemMode.DropIn && (IsRegisterOnSameDay(regularOrderItemsDropIn, orderItem) || IsRegisterTwoTuitionOnSameDay(orderItem))) return CartConstants.ConflictDropInInCart;
                
                if (orderItem.Mode == OrderItemMode.DropIn && (IsRegisterOnSameDay(registeredOrderItemsDropIn, orderItem) || IsRegisterTwoTuitionOnSameDay(orderItem))) return CartConstants.ConflictDropInWithPreviousDropIn;
                
            }

            return string.Empty;
        }

        private string CheckDuplicateEnrollmentForBeforeAfter(DuplicateEnrollmentPolicySettingViewModel duplicateEnrollmentPolicy, List<OrderItem> allProfileRegisteredBeforeAfterOrderItems, List<OrderItem> profileOtherBeforeAfterOrderItems, OrderItem orderItem)
        {
            var registeredOrderItemsNormal = allProfileRegisteredBeforeAfterOrderItems.Where(r => r.Mode == OrderItemMode.Noraml && r.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || r.ProgramTypeCategory == ProgramTypeCategory.Subscription).ToList();

            var regularOrderItemsNormal = profileOtherBeforeAfterOrderItems.Where(r => r.Mode == OrderItemMode.Noraml && r.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || r.ProgramTypeCategory == ProgramTypeCategory.Subscription).ToList();

            if (duplicateEnrollmentPolicy.PreventSameProgramBeforeAfterWeekDay)
            {
                if (orderItem.Mode == OrderItemMode.Noraml && IsRegisterOnSameDay(regularOrderItemsNormal, orderItem)) return CartConstants.ConflictProgramsInCart;

                if (orderItem.Mode == OrderItemMode.Noraml && IsRegisterOnSameDay(registeredOrderItemsNormal, orderItem)) return CartConstants.ProgramConflictWithPreviousRegistration;
            }

            return string.Empty;
        }

        private static bool IsRegisterSameClassTwice(List<OrderItem> orderItems, OrderItem orderItem) => orderItems.Any(o => o.ProgramScheduleId == orderItem.ProgramScheduleId);

        private bool IsRegisterTwoClassesWithConflictingHours(List<OrderItem> orderItems, OrderItem orderItem)
        {
            if (!orderItems.Any()) return false;

            var currentDate = _clubBusiness.GetClubDateTime(orderItem.Order.ClubId, DateTime.UtcNow).Date;

            var allProgramSchedules = orderItems.Select(o => o.ProgramSchedule).ToList();
            allProgramSchedules.Add(orderItem.ProgramSchedule);

            var allScheduleSessions = _programBusiness.GetSessions(allProgramSchedules).Where(s => s.Start >= currentDate).ToList();

            var otherSessions = allScheduleSessions.Where(a => orderItems.Select(o => o.ProgramScheduleId).ToList().Contains(a.ScheduleId)).ToList();

            var currentSessions = allScheduleSessions.Where(a => a.ScheduleId == orderItem.ProgramScheduleId).ToList();

            return IsConflictScheduleSessions(otherSessions, currentSessions);
        }
        
        private bool IsRegisterOnSameDay(List<OrderItem> orderItems, OrderItem orderItem)
        {
            if (!orderItems.Any()) return false;

            var currentDate = _clubBusiness.GetClubDateTime(orderItem.Order.ClubId, DateTime.UtcNow).Date;

            var orderItemIds = orderItems.Select(o => o.Id).ToList();

            var allOrderSessions = _orderSessionBusiness.GetList(o => orderItemIds.Contains(o.OrderItemId) || o.OrderItemId == orderItem.Id).Where(s => s.ProgramSession.StartDateTime >= currentDate);
            
            var otherSessions = allOrderSessions.Where(a => orderItemIds.Contains(a.OrderItemId)).Select(p => new ScheduleSession()
            {
                Id = p.ProgramSession.Id,
                Start = p.ProgramSession.StartDateTime,
                End = p.ProgramSession.EndDateTime
            }).ToList();

            var currentSessions = allOrderSessions.Where(o => o.OrderItemId == orderItem.Id).Select(p => new ScheduleSession()
            {
                Id = p.ProgramSession.Id,
                Start = p.ProgramSession.StartDateTime,
                End = p.ProgramSession.EndDateTime
            }).ToList();

            return IsConflictScheduleSessions(otherSessions, currentSessions);
        }

        private bool IsRegisterTwoTuitionOnSameDay(OrderItem orderItem)
        {
            var currentDate = _clubBusiness.GetClubDateTime(orderItem.Order.ClubId, DateTime.UtcNow).Date;

            var allOrderSessions = _orderSessionBusiness.GetList(o => o.OrderItemId == orderItem.Id).Where(s => s.ProgramSession.StartDateTime >= currentDate);

            var currentSessions = allOrderSessions.Where(o => o.OrderItemId == orderItem.Id).Select(p => new ScheduleSession()
            {
                Id = p.ProgramSession.Id,
                Start = p.ProgramSession.StartDateTime,
                End = p.ProgramSession.EndDateTime
            }).ToList();

            foreach (var session in currentSessions)
            {
                if (currentSessions.Any(s => s.Id != session.Id && s.Start == session.Start)) return true;
            }

            return false;
        }

        private static void SetErrorMessageDuplicateEnrollment(OrderItem orderItem, string validationMessage)
        {
            if (string.IsNullOrEmpty(validationMessage)) return;

            orderItem.IsDuplicateEnrollment = true;
            orderItem.ValidationMessages.Add(validationMessage);
        }

        private static bool IsConflictScheduleSessions(List<ScheduleSession> oldScheduleSessions, List<ScheduleSession> currentScheduleSessions)
        {
            var status = false;

            foreach (var session in currentScheduleSessions)
            {
                if (oldScheduleSessions.Any(s => s.Start.Date == session.Start.Date))
                {
                    var sameSessions = oldScheduleSessions.Where(s => s.Start.Date == session.Start.Date);

                    foreach (var day in sameSessions)
                    {
                        if (session.End <= day.Start || session.Start >= day.End)
                        {
                            continue;
                        }
                        
                        status = true;
                        break;
                    }
                }

                if (status) break;
            }

            return status;
        }

    }
}
