﻿namespace Jumbula.Core.Model.Club
{
    public class AllClubMembers
    {
        public string ClubDomain { get; set; }

        public int UserProfileId { get; set; }
    }
}
