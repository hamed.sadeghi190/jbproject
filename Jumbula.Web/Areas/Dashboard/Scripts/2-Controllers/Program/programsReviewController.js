﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programsReviewController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        $scope.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        $scope.hiddenRevenue = !$scope.MC.isAllowedAction('Program_TotalRevenue_View');

        var dataInfo = {};

        $http.get('Dashboard/CustomReport/GetProgramsAndOrdersReportHeader', { params: { seasonDomain: $scope.seasonDomain } }).success(function (data, status, headers, config) {

            dataInfo = data;
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.programGridOptions = {
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: window.location.origin + '/Dashboard/Program/GetList/?seasonDomain=' + $scope.seasonDomain,
                        contentType: "application/json; charset=utf-8", // tells the web method to serialize JSON
                        type: "POST" //use HTTP POST or get
                    },
                    parameterMap: function (data, operation) {
                        if (operation == "read") {
                            data = $.extend(
                            {
                                sort: null,
                                filter: null,
                                typeSearchTerm: $scope.typeSearchTerm,
                                nameSearchTerm: $scope.nameSearchTerm,
                                seasonDomain: $scope.stateParams.seasonDomain,
                                statusSearchTerm: 'Publish',
                            }, data);

                            return JSON.stringify(data);
                        }
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                pageSize: 100,
                batch: true,
                serverPaging: true,
                serverSorting: false,
            },
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            editable: false,
            columns: [{
                field: "Name",
                title: "Name",
                template: "<a ngaction='Orders_View' class='display-link' ui-sref='Program({seasonDomain: MC.stateParams.seasonDomain, programId: #=Id#})'>#=Name#</a>"
            },
             {
                 field: "TypeCategory",
                 title: "Type",
                 width: "100px"
             },
              {
                  field: "Days",
                  title: "Day/Time",
                  width: "170px"
              },
              {
                  field: "GradeAge",
                  title: "Grade/Age"
              },
              {
                  field: "CommisionRate",
                  title: "Rate",
                  hidden: true

              },
               {
                   field: "OutSourceClubAndSeasonNameForProvider",
                   title: "School",
                   hidden: true
               },
            {
                field: "OutSourceClubAndSeasonNameForSchool",
                title: "Provider",
                hidden: true
            },
            {
                field: "ProviderStatus",
                title: "Status",
                hidden: true
            },
            {
                field: "Capacity",
                title: "Capacity",
                width: "100px"
            },
            {
                field: "WaitList",
                title: "Waitlist",
            },
            {
                field: "TotalAmount",
                title: "Revenue",
                width: "120px",
                hidden: $scope.hiddenRevenue,
            }, {
                field: "ParticipantCount",
                title: "Participant",
                width: "90px"
            },
                {
                    field: "TotalProviderFundedScholarships",
                    title: "Scholarships (provider-funded)",
                    hidden: true
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                    <li>\
                                         <a class='k-link jbi-show' target='_blank' href='#:ViewPageUrl#'>View</a>\
                                    </li>\
                                    <li ngaction='Orders_View' ui-sref='Program({seasonDomain: MC.stateParams.seasonDomain, programId: #=Id#})' >\
                                          <span class='k-link jbi-orders'>Orders</span>\
                                    </li>\
                                    <li ng-hide='dataItem.HideOutSourceProgramInProvider' ngaction='Offline_Register' ng-click='offlineRegistr(#=Id#)'>\
                                          <span class='k-link jbi-register'>Offline register</span>\
                                    </li>\
                                    <li style='margin-left: 25px;' ngaction='ProgramSession_List_View' ng-hide='#= TypeCategory ==\'ChessTournament\' || TypeCategory ==\'Calendar\' || TypeCategory ==\'SeminarTour\'|| TypeCategory ==\'Camp\' #' ng-click='sessions(#=Id#)'>\
                                          <span class='k-link'>Sessions</span>\
                                    </li>\
                                    <li style='margin-left: 25px;' ngaction='Waitlist_View' ng-click='waitlist(#=Id#)'>\
                                          <span class='k-link '>Waitlist</span>\
                                    </li>\
                                    <li ui-sref='Lottery({seasonDomain: MC.stateParams.seasonDomain, programId: #=Id#})' >\
                                          <span class='k-link jbi-dice'>Lottery</span>\
                                    </li>\
                                    <li style='margin-left: 25px;' ngaction='Program_AssignInstructors' ng-click='AssignInstructors(#=Id#)'>\
                                          <span class='k-link '>Assign instructors</span>\
                                    </li>\
                                    <li style='margin-left: 25px;' ngaction='Program_AssignRoom' ng-click='AssignRoom(#=Id#)'>\
                                          <span class='k-link '>Assign room</span>\
                                    </li>\
                                    <li ngaction='Program_AssignRoom' >\
                                           <span class='jbi-angle-left'>   Class scheduling email</span>\
                                              <ul>\
                                             <li ngaction='Program_AssignRoom' ui_sref='ParentsNotification({seasonDomain:MC.stateParams.seasonDomain,programId:#=Id#,typeNotification:\"Confirmation\"})'>\
                                                <span class='k-link'>Confirmation email</span>\
                                             </li>\
                                             <li ngaction='Program_AssignRoom'  ui_sref='ParentsNotification({seasonDomain:MC.stateParams.seasonDomain,programId:#=Id#,typeNotification:\"Cancellation\"})'>\
                                                <span class='k-link'>Cancellation email</span>\
                                              </li>\
                                        </ul>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>"
                }
            ],
            excel: {
                allPages: true,
                fileName: $scope.MC.account.name + " (Programs and orders).xlsx",
                proxyURL: "/Dashboard/CustomReport/Save"
            },
            excelExport: function (e) {

                var rows = e.workbook.sheets[0].rows;

                rows.unshift({
                    cells: [{
                        value: ""
                    }]
                });
                rows.unshift({
                    cells: [{
                        value: dataInfo.SeasonName
                    }]
                });
                rows.unshift({
                    cells: [{
                        value: dataInfo.Date
                    }]
                });
                rows.unshift({
                    cells: [{
                        value: "Programs and orders - " + $scope.MC.account.name
                    }]
                });

                cfpLoadingBar.complete();
            },

        };

        $http.get(window.location.origin + '/Dashboard/Program/AnyOutSourcePrograms?seasonDomain=' + $scope.stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model = data;
            if ($scope.Model.ClubType == "Provider" && $scope.Model.HasOutSource) {
                $("#programGrid").data("kendoGrid").showColumn(4);
                $("#programGrid").data("kendoGrid").showColumn(5);
                $("#programGrid").data("kendoGrid").showColumn(7);
                $("#programGrid").data("kendoGrid").showColumn(12);
            }
            if ($scope.Model.ClubType == "School" && $scope.Model.HasOutSource) {
                $("#programGrid").data("kendoGrid").showColumn(6);
                $("#programGrid").data("kendoGrid").showColumn(7);
            }

        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.AssignRoom = function (programId) {
            $state.go("AsignRoom", { seasonDomain: $stateParams.seasonDomain, programId: programId });
        }

        $scope.AssignInstructors = function (programId) {
            $state.go("AsignInstructors", { seasonDomain: $stateParams.seasonDomain, programId: programId });
        }

        $scope.waitlist = function (programId) {

            $state.go("ProgramWaitlist", { seasonDomain: $stateParams.seasonDomain, programId: programId });
        }

        $scope.sessions = function (programId) {

            $state.go("ProgramSession", { seasonDomain: $stateParams.seasonDomain, programId: programId });
        }

        $scope.programSearch = function () {
            $("#programGrid").data("kendoGrid").dataSource.read();
        }

        $scope.offlineRegistr = function (programId) {

            $http.post(window.location.origin + '/Dashboard/Order/DoesOfflineOrderExist/')
              .success(function (data, status, headers, config) {
                  if (data.Status == true) {
                      if (data.RecordsAffected == 0) {
                          $state.go("Register", { seasonDomain: $stateParams.seasonDomain, programId: programId, Type: "register" });
                      }
                      else {
                          $state.go("ConfirmNewRegisteration", { seasonDomain: $stateParams.seasonDomain, programId: programId });
                      }
                  }
                  else {
                      $scope.MC.handleErrors($scope, data);
                  }
              });
        }

        $scope.programSearchClearFilters = function () {
            $scope.nameSearchTerm =
                $scope.startDateCreatedSearchTerm =
                $scope.endDateCreatedSearchTerm =
                $scope.statusSearchTerm =
                $scope.typeSearchTerm = "";

            $("#programGrid").data("kendoGrid").dataSource.read();
        }

        $scope.switchAdvancedSearch = function () {

            if ($scope.advancedSearch) {
                $scope.startDateCreatedSearchTerm = '';
                $scope.endDateCreatedSearchTerm = '';
                $scope.statusSearchTerm = '';
                $scope.typeSearchTerm = '';
            }

            $scope.advancedSearch = !$scope.advancedSearch;
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#programGrid").getKendoGrid().saveAsExcel();

        }

    }]);
})();