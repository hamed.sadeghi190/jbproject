﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SportsClub.Models;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProviderFinancialReportViewModel
    {
        public string ClassName { get; set; }
        public string SchoolName { get; set; }
        
        public int TotalRegistrations { get; set; }

        public decimal TotalProviderFundedScholarships { get; set; }

        public int TotalPaidEnrollments { get; set; }

        public decimal ClassFee { get; set; }

        public decimal? TotalPaymentsCollected
        {
            get { return TotalPaidEnrollments * ClassFee; }
        }
        public int? EMRate { get; set; }

        public decimal? EMFeeDeducted
        {
            get { return TotalPaymentsCollected * EMRate/100; }
        }
        
        public decimal ProviderPenaltiesDeducted { get; set; }

        public decimal? ProviderPayment
        {
            get { return (TotalPaymentsCollected-EMFeeDeducted - ProviderPenaltiesDeducted); }
        }


    }


}