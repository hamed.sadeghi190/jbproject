
ALTER TABLE waitLists ADD  ScheduleId bigint NULL

Go

ALTER TABLE [dbo].[WaitLists]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WaitLists_dbo.ProgramSchedules_ScheduleId] FOREIGN KEY([ScheduleId])
REFERENCES [dbo].[ProgramSchedules] ([Id])


--Migration
Go

UPDATE WaitLists SET WaitLists.ScheduleId = ProgramSchedules.Id
FROM WaitLists AS WaitLists INNER JOIN ProgramSchedules AS ProgramSchedules ON WaitLists.ProgramId =ProgramSchedules.ProgramId 
WHERE ProgramSchedules.IsDeleted = 0

