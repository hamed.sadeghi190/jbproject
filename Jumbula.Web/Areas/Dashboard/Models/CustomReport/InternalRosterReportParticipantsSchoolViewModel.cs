﻿using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class InternalRosterReportParticipantsSchoolViewModel
    {
        public string ParticipantName { get; set; }
        public string Parent1Name { get; set; }
        public string AuthorizedPickup1Name { get; set; }
        public string EmergencyContactName { get; set; }
        public string Parent1Phone { get; set; }
        public string AuthorizedPickup1Phone { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string Parent1Email { get; set; }
        public string AuthorizedPickup1Email { get; set; }
        public string EmergencyContactEmail { get; set; }
        public string TransportHome { get; set; }
        public int Number { get; set; }
        public string Allergies { get; set; }
        public string MedicalConditions { get; set; }
        public OrderItemMode Mode { get; set; }

        public string GradeOrAge { get; set; }
        public string AuthorizedPickup2Name { get; set; }
        public string AuthorizedPickup3Name { get; set; }
        public string Parent2Email { get; set; }
        public string Parent2Name { get; set; }
        public string Parent2Phone { get; set; }
        public string PermissionPhotography { get; set; }
        public string TeacherName { get; set; }
        public string Treatments { get; set; }

    }
}