﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('FollowupReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;

        $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model })
            .success(function (data, status, headers, config) {
                if (is.undefined($scope.Model.Filters)) {
                    $scope.Model.Filters = {};
                    $scope.Model.Filters.Status = "regular";
                    $scope.Model.Filters.Tuition = "";
                    $scope.Model.Filters.Tuitions = data.Tuitions;
                    $scope.Model.Filters.OrderStatus = data.OrderStatus;
                    $scope.Model.Filters.FollowupFilter = "None";
                }
            }).error(function (data, status, headers, config) {
            });

        $scope.filterData = function () {
            $scope.followupReportGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters = {};
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.Status = "regular";
            $scope.Model.Filters.Tuition = "";
            $scope.Model.Filters.FollowupFilter = "None";
            $scope.searchTerm = "";
            $scope.followupReportGridOptions.dataSource.read();
            $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model }).success(function (data, status, headers, config) {
                $scope.Model.Filters.Tuitions = data.Tuitions;
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            });
        }

        $scope.followupReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/FollowupReportRun',
                        type: "POST",
                        data: function () {
                            return {
                                model: $scope.Model,
                                term: $scope.searchTerm,
                            };
                        },
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
            batch: true,
            resizable: true,
        });

        $scope.followupReportGridOptions = {
            dataSource: $scope.followupReportsDataSource,
            sortable: { mode: 'single' },
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100
                ],
                buttonCount: 5
            },
            dataBound: function () {
                $('td[data-status="-"]').html("-");
                var grid = $("#followupReportGrid").data("kendoGrid");
                grid.thead.closest('table').css({ "max-width": "initial", "min-width": "110px" });
                grid.tbody.closest('table').css({ "max-width": "initial", "min-width": "110px" });

                customReportService.initializeDoubleScroll(grid);
            },
            columns: [
                {
                    field: "FullName",
                    title: "Full name",
                    width: "110px",
                    //template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderItemDetail(#=OrderItemId#)'  class='block'> #= FullName # </a>",
                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderItemDetail(#=OrderItemId#)'  class='block'> #= FullName # </a> #if(ItemStatusReason == 1) {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == 2) {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == 5) {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == 4){# <i class='grid-label label warning'>Canceled</i> #}#"
                },
                {
                    field: "Email",
                    title: "Email",
                    width: "130px"
                }, {
                    field: "ProgramName",
                    title: "Program",
                    width: "auto"
                },
                {
                    field: "RegDate",
                    title: "Registration date",
                    width: "auto",
                },
                {
                    field: "ConfirmationId",
                    title: "Confirmation",
                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderDetail(#=OrderId#)' class='block'> #= ConfirmationId # </a>",
                    width: "130px"
                },
                {
                    title: "Forms details",
                    template: '' +
                        '<table>' +
                        '<tbody>' +
                        '<tr ng-repeat=\'row in dataItem.FollowupForms\'>' +
                        '<td style="width:auto">{{row.FormName}}</td>' +
                        '<td style="width:40px">{{row.Status}}</td>' +
                        '<td style="width:40px" data-status="{{row.Status}}"><a ui-sref="OrderItemFollowForm({fId:{{row.FormId}}})">View</a></td>' +
                        '</tr>' +
                        '</tbody>' +
                        '</table>',
                    filed: "FollowupForms",
                    width: "auto"
                }
            ]
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $http.post(baseUrl + '/FollowupReportRun', { model: model, filters: model.Filters, isExportToExcel: true, term: $scope.searchTerm })
                .success(function (data, status, headers, config) {
                    window.location.href = data.Data;
                    cfpLoadingBar.complete();

                });
        }

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/FollowupReportRun', { model: model, filters: model.Filters, makeCampaign: true, term: $scope.searchTerm })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        $scope.goOrderDetail = function (orderId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderDetail($scope.MC.stateParams.seasonDomain, orderId);
        }

        $scope.goOrderItemDetail = function (orderItemId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderItemDetail($scope.MC.stateParams.seasonDomain, orderItemId);
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "FollowupForm" });
        }

    }]);
})();