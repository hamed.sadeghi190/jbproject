﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SportsClub.Models;

namespace SportsClub.Areas.Dashboard.Models.CustomReport
{
    public class InstallmentReportViewModel
    {
        public string FullName { get; set; }

        public int InstallmentsNum { get; set; }

        public int PaidInstallments { get; set; }

        public List<InstallmentItemReportViewModel> Installments { get; set; }

        public string StrBalance { get; set; }
        public decimal Balance { get; set; }

        public double DoubleBalance { get; set; }
        public string Preapproval { get; set; }

        public string ProgramName { get; set; }

        public string PastDue { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }
        public decimal Total { get; set; }
        public double DoubleTotal { get; set; }
        public string StrTotal { get; set; }

        public string StrPaidAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public double DoublePaidAmount { get; set; }
        public DateTime OrderDate { get; set; }

        public string RegDate { get; set; }

        public string ConfirmationId { get; set; }

        public long OrderId { get; set; }

        public long OrderItemId { get; set; }

        public string ToInstallmentDetail()
        {
            var installmentDetails = string.Empty;

            foreach (var installment in Installments)
            {
                var amount = installment.StrAmount;
                var dueDate = installment.DueDate;
                var paymentStatus = installment.PaymentStatus;
                installmentDetails += string.Format("{0} {1} {2}, ", dueDate, amount, paymentStatus);
            }

            installmentDetails = (installmentDetails.Length > 0)
                ? installmentDetails.Remove(installmentDetails.Length - 2)
                : installmentDetails;

            return installmentDetails;
        }

        public int ItemStatusReason { get; set; }
    }

    public class InstallmentItemReportViewModel
    {
        public string DueDate { get; set; }

        public string StrAmount { get; set; }
        public string Amount { get; set; }

        public string PaymentStatus { get; set; }
    }
}