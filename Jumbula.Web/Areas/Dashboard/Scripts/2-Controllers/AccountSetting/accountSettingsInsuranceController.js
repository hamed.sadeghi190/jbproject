﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('accountSettingsInsuranceController', ['$scope', '$http', 'jbToast', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AdvanceSettings", order: 2, menu: "SETTINGS" });

        $scope.addInput = function (filename, fileurl, index) {
            if (filename) {
                
                $scope.Model.Certificates[index].CertificateFileNames.push(filename);
                $scope.Model.Certificates[index].CertificateFileURLs.push(fileurl);
            }
        }
        
        $scope.removeInput = function (index, certiIndex) {
            $scope.Model.Certificates[certiIndex].CertificateFileNames.splice(index, 1);
            $scope.Model.Certificates[certiIndex].CertificateFileURLs.splice(index, 1);
        }
        $scope.hasCertiicate = false;

        $http.get('Dashboard/AccountSetting/EditAccountSetting').success(function (data, status, headers, config) {
            $scope.Model = data;

            var certNo = $scope.Model.NumberOfCertificates;

            if (certNo != 0) {
                for (var i = 0; i < certNo; i++) {
                    $scope['isDisabled_' + i] = true;
                }
                
            }

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.removeFile = function (fileIndex, certiIndex) {
            jbConfirmation
              .yes(function () {
                  var files = $scope.Model.Certificates[certiIndex].CertificateFileNames[fileIndex];
                  $http.post('KendoEditorFiles/RemoveUplodedFile', { fileNames: files, location: '/InsuranceForm/' })
                  .success(function (data, status, headers, config) {

                      if (data.Status == true) {
                          $scope.removeInput(fileIndex, certiIndex);

                          jbToast.warning("The file has been removed successfully. Make sure you click on the Save button.", "");
                      }
                      else {
                          jbToast.error(data.Message);
                      }
                  })
              })
              .title('Delete File')
              .message('Are you sure you want to delete this file?')
              .confirm();
        }
        $scope.save = function () {
            if ($scope.Model.Certificates.length > 0 || ( $scope.Model.Certificates.length<=0 && $scope.Model.NumberOfCertificates>0)) {


                $http.post('Dashboard/AccountSetting/EditAccountSetting', { model: $scope.Model })
                 .success(function (data, status, headers, config) {

                     if (data.Status == true) {
                         $scope.errors = null;
                         jbToast.success("Insurance updated successfully.", "");
                         $state.go('AdvanceSetting');
                     }
                     else {
                         $scope.MC.handleErrors($scope, data);
                     }
                 })
            }
        }
   
        $scope.KendoUploadOptions = {
            
            localization: {
                select: "Upload a document",
            },
            multiple: false,
            batch: true,
            success: function (e) {
                if (e.response.Status) {
                    $scope.addInput(e.response.FileName, e.response.URL, e.response.Index);
                    var ii = e.response.Index;

                  
                    $scope['isDisabled_' + e.response.Index] = true;
                   
                    $scope.$apply();
                    jbToast.warning("You have not saved your uploads. If you leave this page, your uploads will not save to your portal. Make sure you click on the Save button", "");
                }
                else {
                    jbToast.error(e.response.message);
                }
            },
            error: function (e) {
                jbToast.error(e.message);
            }
        }

        $scope.onSelect = function (e) {

            if (e.files[0].extension != ".png" && e.files[0].extension != ".jpg" && e.files[0].extension != ".pdf") {
                e.preventDefault();
                jbToast.error("Please upload an image or a PDF file");

            }

        };


        $scope.addTo = function () {
           
            $scope.Model.PolicyExpirationDate = null;
            if (!$scope.Model.Certificates) {
                $scope.Model.Certificates = [];
            }
            
            $scope.Model.Certificates.push({ CertificateTypes: $scope.Model.CertificateTypes, CertificateFileNames: [],CertificateFileURLs:[], SelectedCertificate: '0' });
            $scope.hasCertiicate = true;
        };

        $scope.removeTo = function (certificateIndex) {

            jbConfirmation
               .yes(function () {
                   $scope['isDisabled_' + certificateIndex] = false;
                   var tmpCertificates = $scope.Model.Certificates;

                   $scope.Model.Certificates = null;
                   $scope.Model.Certificates = [];

                   if (tmpCertificates.length == 1) {
                       $scope.Model.Certificates = null;
                       $scope.Model.Certificates = [];

                   } else {
                       for (var i = 0; i < tmpCertificates.length; i++) {
                           if (i != certificateIndex) {
                               console.log(certificateIndex)
                               $scope.Model.Certificates.push(tmpCertificates[i]);
                           }
                       }
                   }

                 
                   if ($scope.Model.Certificates.length == 0) {
                       $scope.hasCertiicate = false;
                   }

               })
            
              .title('Delete Certificate')
              .message('Are you sure you want to delete this insurance certificate?')
              .confirm();
        };

    }])

})();