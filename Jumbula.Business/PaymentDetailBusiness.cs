﻿using Jb.Framework.Common;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class PaymentDetailBusiness : IPaymentDetailBusiness
    {
        private readonly IRepository<PaymentDetail, long> _paymentDetailRepository;

        public PaymentDetailBusiness(IRepository<PaymentDetail, long> paymentDetailRepository)
        {
            _paymentDetailRepository = paymentDetailRepository;
        }

        public IQueryable<PaymentDetail> GetList()
        {
            return _paymentDetailRepository.GetList();
        }
        public PaymentDetail GetByInvoiceId(int InvoiceId)
        {
            return _paymentDetailRepository.GetList().SingleOrDefault(p => p.Status != PaymentDetailStatus.ERROR && p.InvoiceId == InvoiceId);
        }

        public IQueryable<PaymentDetail> GetByInvoiceIds(List<int> InvoiceIds)
        {
            return _paymentDetailRepository.GetList().Where(p => p.Status != PaymentDetailStatus.ERROR && p.InvoiceId.HasValue && InvoiceIds.Contains(p.InvoiceId.Value));
        }

        public PaymentDetail GetByTransactionId(string transactionId)
        {
            return _paymentDetailRepository.GetList().FirstOrDefault(p => p.TransactionId == transactionId);
        }

        public string GetTransactionIdFromResponse(string response)
        {
            var transactionId = string.Empty;

            if (response == "[object Object]")
            {
                return transactionId;
            }
            dynamic data = response != null ? !string.IsNullOrEmpty(response.Trim()) ? JObject.Parse(response) : null : null;
          
            transactionId = data != null ? data.Response!=null ? data.Response.transactionResponse != null ? 
                data.Response.transactionResponse.transId : string.Empty : string.Empty : string.Empty;

            return transactionId;
        }

        public OperationStatus Update(PaymentDetail paymentDetail)
        {
            return _paymentDetailRepository.Update(paymentDetail);
        }
    }

}
