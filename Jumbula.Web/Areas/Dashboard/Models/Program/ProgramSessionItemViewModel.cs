﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramSessionItemViewModel
    {
        public int Id { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }
    }
}