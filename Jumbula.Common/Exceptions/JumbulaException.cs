﻿using System;

namespace Jumbula.Common.Exceptions
{
    public abstract class JumbulaException : Exception
    {
        protected JumbulaException(string s)
            : base(s)
        {

        }

        protected JumbulaException(string s, Exception e)
            : base(s, e)
        {

        }
    }
}
