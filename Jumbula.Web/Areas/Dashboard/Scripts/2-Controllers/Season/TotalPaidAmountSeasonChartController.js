﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('TotalPaidAmountSeasonChartController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaigns-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/Season';
        $scope.Model = {};
        var colors = ["#c2e4ff", "#8fcdff", "#5cb7ff", "#29a0ff", "#0088f5", "#006cc2", "#00508f", "#00335c"];


        $scope.dataSourceBarChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetTotalPaidMonthlyBarChartReport",
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            seasonDomain: $scope.MC.stateParams.seasonDomain,
                        }
                    }
                }
            },
            schema: {
                data: function (response) {

                    $scope.TotalSale = response.Total
                    return response.DataSource;
                }
            },
        });

        $scope.barChartOption = {
            dataSource: $scope.dataSourceBarChart,
            legend: {
                visible: false
            },
            chartArea: {
                background: "",
                width: 460,
                height: 210
            },
            seriesDefaults: {
                //type: "bar",
                stack: true,
               // type: "column", 
                field: "Amount",
                labels: {
                    field: "Amount",
                    visible: false,
                    background: "transparent"
                }
            },
            seriesColors: colors,
            series: [{
                field: "strDueDate",
                name: "Due date",
                overlay: {
                    gradient: "none"
                },
                color: function (point) {
                    return colors[0];
                }
            }, {
                field: "Amount",
                name: "Amount",
                overlay: {
                    gradient: "none"
                },
                //opacity: 0.3
            }],
            categoryAxis:  {
               // categories: dataItem.DueDate, //["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
                field: "strDueDate",
                majorGridLines: {
                    visible: false
                },
                majorTicks: {
                    visible: false
                },
                crosshair: {
                    visible: false
                },
                justified: true
            },
            valueAxis: {
                majorUnit: 10,
                line: {
                    visible: false
                },
                visible: false,
            },
            tooltip: {
                visible: true,
                template: function (point) {

                    return point.dataItem.strDueDate + " (" + point.value.toLocaleString('USD', { style: 'currency', currency: "USD", minimumFractionDigits: 2, maximumFractionDigits: 2 }) + ")";

                },
            }
        };


    }]);
})();