﻿using System;
using Jb.Framework.Common.Forms;

namespace Jumbula.Core.Domain
{
    public class ChessTourneyEntryItem
    {
        public long OrderItemId { get; set; }
        public string UscfId { get; set; }
        public string UscfExpiration { get; set; }
        public string UscfRatingReg { get; set; }
        public string UscfRatingQuick { get; set; }

        public DateTime Date { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Section { get; set; }

        public string Schedule { get; set; }

        public string Byes { get; set; }

        public string Options { get; set; }

        public int? ProfileId { get; set; }

        public JbForm JbForm { get; set; }
    }
}
