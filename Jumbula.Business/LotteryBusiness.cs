﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Lottery;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Business
{
    public class LotteryBusiness : ILotteryBusiness
    {

        #region Fields
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IOrderBusiness _orderBusiness;
        private readonly IRepository<OrderItem, long> _orderItemRepository;
        #endregion

        #region Constractors
        public LotteryBusiness(IOrderItemBusiness orderItemBusiness, IProgramBusiness programBusiness,
            IRepository<OrderItem, long> orderItemRepository, IClubBusiness clubBusiness, ISeasonBusiness seasonBusiness, IOrderBusiness orderBusiness)
        {
            _orderItemBusiness = orderItemBusiness;
            _programBusiness = programBusiness;
            _orderItemRepository = orderItemRepository;
            _clubBusiness = clubBusiness;
            _seasonBusiness = seasonBusiness;
            _orderBusiness = orderBusiness;
        }

        #endregion

        #region Public methods

        public object Draw(long programId)
        {
            var program = _programBusiness.Get(programId);

            var capacityLeft = _programBusiness.CapacityLeft(program, program.Season.Status == SeasonStatus.Test);

            if (!capacityLeft.HasValue)
                return new
                {
                    Status = false,
                    Message = Lottery.MaximumCapacityRequiredMessage
                };

            var lotteryItems = GetLotteryItems(program);
            var initiateds = lotteryItems.Where(i => i.LotteryStatus == LotteryStatus.Initiated || i.LotteryStatus == LotteryStatus.Lose).ToList();
            var addeds = lotteryItems.Where(i => (i.LotteryStatus == LotteryStatus.Condidate || i.LotteryStatus == LotteryStatus.Won) && i.ItemStatus != OrderItemStatusCategories.completed).ToList();

            var initiatedIds = initiateds.Select(i => i.Id).ToList();

            var maxIndex = capacityLeft.Value - addeds.Count();

            var condidateIds = new List<long>();

            if (initiateds.Count < maxIndex)
            {
                foreach (var item in initiateds)
                {
                    item.LotteryStatus = LotteryStatus.Condidate;
                    condidateIds.Add(item.Id);
                }
            }
            else
            {
                capacityLeft = maxIndex;
                for (int i = 1; i <= capacityLeft; i++)
                {
                    var random = RandomHelper.RandomNumber(0, initiatedIds.Count);

                    var id = initiatedIds[random];
                    initiatedIds.RemoveAt(random);

                    var selected = initiateds.Single(item => item.Id == id);
                    selected.LotteryStatus = LotteryStatus.Condidate;
                    condidateIds.Add(selected.Id);

                    maxIndex--;
                }
            }

            var dbResult = _orderItemRepository.Save();

            var result = new
            {
                Status = dbResult.Status,
                SelectedIds = condidateIds.ToList(),
                Message = string.Empty
            };

            return result;
        }


        public OperationStatus RemoveLotteryItem(long orderItemId)
        {
            var lotteryOrderItem = _orderItemRepository.GetList().Single(o => o.Id == orderItemId);

            lotteryOrderItem.LotteryStatus = LotteryStatus.Initiated;

            return _orderItemRepository.Save();
        }

        public OperationStatus AddLotteryItem(long orderItemId)
        {

            if (HasSpaceForLottery(orderItemId))
            {
                var lotteryOrderItem = _orderItemRepository.GetList().Single(o => o.Id == orderItemId);

                lotteryOrderItem.LotteryStatus = LotteryStatus.Condidate;

                var result = _orderItemRepository.Save();

                if (result.Status)
                    result.Message = Lottery.SuccessfullyCandidateMessage;

                return result;
            }
            else
            {
                return new OperationStatus { Message = Lottery.NoSpaceForLotteryMessage };
            }

        }

        public bool HasSpaceForLottery(long orderItemId)
        {
            var lotteryOrderItem = _orderItemRepository.GetList().Single(o => o.Id == orderItemId);
            var program = lotteryOrderItem.ProgramSchedule.Program;
            var isTestMode = program.Season.Status == SeasonStatus.Test;

            var capacityLeft = _programBusiness.CapacityLeft(program, isTestMode);
            var winnersAndCondidates = GetCandidateAndWinnerItems(GetLotteryItems(program).ToList()).Count();

            return winnersAndCondidates < capacityLeft;
        }

        public OperationStatus FinalizeLottery(int programId)
        {
            var program = _programBusiness.Get(programId);

            var lotteryItems = GetLotteryItems(program).ToList();
            var candidates = GetCondidateItems(lotteryItems).ToList();
            var others = lotteryItems.Where(i => i.LotteryStatus != LotteryStatus.Condidate && i.LotteryStatus != LotteryStatus.Won && i.ItemStatus != OrderItemStatusCategories.completed).ToList();

            candidates.ForEach(a => a.LotteryStatus = LotteryStatus.Won);
            others.ForEach(a => a.LotteryStatus = LotteryStatus.Lose);

            return _orderItemRepository.Save();
        }

        public LotteryDetailsViewModel GetLotteryDetails(int programId)
        {

            var program = _programBusiness.Get(programId);

            var season = program.Season;
           
            if (season.Setting == null || season.Setting.SeasonProgramInfoSetting == null)
            {
                return new LotteryDetailsViewModel
                {
                    ProgramName = program.Name,
                    NumberOfParticipants = 0,
                    TotalCapacity = 0,
                    IsLotteryOpenForAdmin = true,
                    EnableDraw = false,
                    EnableFinalize = false,
                    DrawDescription = Lottery.LotteryIsNotEnabled,
                    FinalizeDescription = Lottery.LotteryIsNotEnabled
                };
            }

            var clubDateTime = _clubBusiness.GetClubDateTime(program.Club, DateTime.UtcNow);
            var isTestMode = program.Season.Status == SeasonStatus.Test;
            var lotteryItems = GetLotteryItems(program).ToList();
            var isLotteryOpenForAdmin = IsLotteryOpenForAdmin(season, clubDateTime);
            var totalCapacity = program.ProgramSchedules.Where(s => !s.IsDeleted).Sum(s => _programBusiness.GetScheduleCapacity(s));
            var capacityLeft = _programBusiness.CapacityLeft(program, isTestMode);

            return new LotteryDetailsViewModel
            {
                ProgramName = program.Name,
                NumberOfParticipants = lotteryItems.Count(),
                TotalCapacity = totalCapacity,
                IsLotteryOpenForAdmin = isLotteryOpenForAdmin,
                EnableDraw = IsDrawEnabled(capacityLeft, isLotteryOpenForAdmin, lotteryItems),
                EnableFinalize = IsFinalizeEnabled(capacityLeft, isLotteryOpenForAdmin, lotteryItems),
                DrawDescription = GetDrawDescription(capacityLeft, totalCapacity, season, isLotteryOpenForAdmin, lotteryItems),
                FinalizeDescription = GetFinalizeDescription(capacityLeft, isLotteryOpenForAdmin, lotteryItems)
            };
        }

        public Order CompleteLotteryItem(long orderItemId)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);

            orderItem.ItemStatus = OrderItemStatusCategories.completed;

            var order = orderItem.Order;
            order.ConfirmationId = _orderBusiness.GenerateConfirmationId();
            order.IsDraft = false;
            order.OrderMode = OrderMode.Offline;

            order.OrderStatus = OrderStatusCategories.completed;

            var result = _orderItemBusiness.Update(orderItem);
            _orderBusiness.Update(order);
            _orderBusiness.SeparateOrderItemFromCartOrder(order, orderItem.Id);

            var res = _clubBusiness.AddUserInClubUsers(order);

            return order;
        }

        public PaginatedList<LotteryItemViewModel> GetLotteryItems(long programId, int pageIndex, int pageSize)
        {
            var program = _programBusiness.Get(programId);

            var club = program.Club;
            var clubDateTime = _clubBusiness.GetClubDateTime(club, DateTime.UtcNow);
            var season = program.Season;

            var lotteryItems = GetLotteryItems(program).ToList();
            var condidatesAndWinners = GetCandidateAndWinnerItems(lotteryItems);
            var numberOfCondidatesAndWinners = condidatesAndWinners.Count();
            var totalCapacity = _programBusiness.CapacityLeft(program, program.Season.Status == SeasonStatus.Test);
            var isLotteryOpenForAdmin = IsLotteryOpenForAdmin(season, clubDateTime);

            var model = lotteryItems.ToList()
                          .Select(item => MapOrderItem(item, club, numberOfCondidatesAndWinners, totalCapacity, isLotteryOpenForAdmin))
                          .OrderBy(l => l.Attendee);

            return model.ToPaginatedList(pageIndex, pageSize, lotteryItems.Count());
        }

        #endregion

        #region Private methods

        private string GetDrawDescription(int? capacityLeft, int totalCapacity, Season season, bool isLotteryOpenForAdmin, List<OrderItem> lotteryItems)
        {
            if (IsDrawEnabled(capacityLeft, isLotteryOpenForAdmin, lotteryItems))
                return Lottery.DrawLottery;

            if (!isLotteryOpenForAdmin)
            {
                var lotteryCloseDateTime = _seasonBusiness.GetLotteryCloseDate(season);
                return $"You cannot draw the lottery until it closes on {lotteryCloseDateTime.Value.ToString(Constants.DefaultDateFormat)} at {lotteryCloseDateTime.Value.ToString(Constants.DefaultTimeFormat)}";
            }

            if (totalCapacity == 0)
                return Lottery.MaximumCapacityRequiredMessage;

            if (!capacityLeft.HasValue)
                return Lottery.MaximumCapacityRequiredMessage;

            if (capacityLeft.HasValue && capacityLeft < 1)
                return Lottery.NoSpaceForLotteryMessage;

            return Lottery.DrawLottery;
        }

        private string GetFinalizeDescription(int? capacityLeft, bool isLotteryOpenForAdmin, List<OrderItem> lotteryItems)
        {
            if (IsFinalizeEnabled(capacityLeft, isLotteryOpenForAdmin, lotteryItems))
                return Lottery.FinalizeLottery;

            return Lottery.DrawBeforeFinalizeMessage;
        }

        private bool IsLotteryOpenForAdmin(Season season, DateTime clubDateTime)
        {
            return season.Setting?.SeasonProgramInfoSetting?.LotteryCloses < clubDateTime && season.Setting?.SeasonProgramInfoSetting?.GeneralRegistrationOpens > clubDateTime;
        }

        private bool IsDrawEnabled(int? capacityLeft, bool isLotteryOpenForAdmin, List<OrderItem> lotteryItems)
        {
            if (!capacityLeft.HasValue)
                return false;

            return GetCandidateAndWinnerItems(lotteryItems).Count() < capacityLeft && isLotteryOpenForAdmin && GetDrawableLotteryItems(lotteryItems).Any();
        }

        private bool IsFinalizeEnabled(int? capacityLeft, bool isLotteryOpenForAdmin, List<OrderItem> lotteryItems)
        {
            if (!capacityLeft.HasValue)
                return false;

            return GetWinnerItems(lotteryItems).Count() < capacityLeft && GetCondidateItems(lotteryItems).Count() > 0 && isLotteryOpenForAdmin;
        }

        private List<OrderItem> GetCondidateItems(List<OrderItem> lotteryItems)
        {
            return GetCandidateAndWinnerItems(lotteryItems).Where(i => i.LotteryStatus == LotteryStatus.Condidate).ToList();
        }

        private List<OrderItem> GetWinnerItems(List<OrderItem> lotteryItems)
        {
            return GetCandidateAndWinnerItems(lotteryItems).Where(i => i.LotteryStatus == LotteryStatus.Won).ToList();
        }

        private List<OrderItem> GetCandidateAndWinnerItems(List<OrderItem> lotteryItems)
        {
            return lotteryItems.Where(i => i.ItemStatus != OrderItemStatusCategories.completed && (i.LotteryStatus == LotteryStatus.Condidate || i.LotteryStatus == LotteryStatus.Won)).ToList();
        }

        private List<OrderItem> GetDrawableLotteryItems(List<OrderItem> lotteryItems)
        {
            return lotteryItems.Where(l => l.LotteryStatus == LotteryStatus.Initiated || l.LotteryStatus == LotteryStatus.Lose).ToList();
        }

        private IQueryable<OrderItem> GetLotteryItems(long programId, bool isTestMode)
        {
            var query = _orderItemBusiness.GetList().Where(item => item.ProgramScheduleId.HasValue && item.ProgramSchedule.ProgramId == programId
                                        && item.LotteryStatus != LotteryStatus.NotInitiated && item.Order.IsLive != isTestMode);

            return query;
        }

        private IQueryable<OrderItem> GetLotteryItems(Program program)
        {
            return GetLotteryItems(program.Id, program.Season.Status == SeasonStatus.Test);
        }

        private LotteryItemViewModel MapOrderItem(OrderItem item, Club club, int numberOfCondidatesAndWinners, int? totalCapacity, bool isLotteryOpenForAdmin)
        {
            return new LotteryItemViewModel
            {
                Id = item.Id,
                OrderId = item.Order_Id,
                Attendee = $"{item.LastName}, {item.FirstName}",
                OrderDate = !item.Order.IsDraft? _clubBusiness.GetClubDateTime(item.Order.Club, item.Order.CompleteDate).ToString(Constants.DefaultDateTimeFormat) : string.Empty,
                ConfirmationId = !string.IsNullOrEmpty(item.Order.ConfirmationId) ? item.Order.ConfirmationId : string.Empty,
                EntryFee = CurrencyHelper.FormatCurrencyWithPenny(item.EntryFee, club.Currency),
                EntryFeeName = item.EntryFeeName,
                LotteryStatus = item.LotteryStatus,
                ItemStatus = item.ItemStatus,
                EnableMarkAsCondidate = numberOfCondidatesAndWinners < totalCapacity && isLotteryOpenForAdmin && (item.LotteryStatus == LotteryStatus.Initiated || item.LotteryStatus == LotteryStatus.Lose),
            };
        }

        #endregion
    }
}
