﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ReportFilterElementName
    {
        [Description("School")]
        School = 0,

        [Description("Season")]
        Season = 1,

        [Description("Program")]
        Program = 2,

        [Description("Program")]
        CampProgram = 3,

        [Description("Session")]
        Session = 4,

        [Description("Schedule")]
        Schedule = 5,

        [Description("Tuition")]
        Tuition = 6,

        [Description("Organization")]
        Organization = 7,

        [Description("Active status")]
        ActiveStatus = 8,

        [Description("Start date")]
        StartDate = 9,

        [Description("End date")]
        EndDate = 10,

        [Description("Organization")]
        ProviderAndSchool = 11,

        [Description("Identifier")]
        Identifier = 12,

        [Description("First name")]
        FirstName = 13,

        [Description("Last name")]
        LastName = 14,

        [Description("Installment payment mode")]
        InstallmentPaymentMode = 15
    }
}
