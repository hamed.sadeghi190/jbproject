﻿(function () {
    'use strict';

    angular.module('dashboardApp').service("templateEditorService", [function () {

        this.mailDesignerInit = function ($scope) {
            $scope.Model.MailTemplate.HasHeader =
                $scope.Model.MailTemplate.HasHeaderText =
                $scope.Model.MailTemplate.HasMap =
                $scope.Model.MailTemplate.HasFooter =
                $scope.Model.MailTemplate.HasMoreQuestions =
                $scope.Model.MailTemplate.HasLogo =
                $scope.Model.MailTemplate.HasFooterText = true;
            $scope.editingEmailSection =
                $scope.editingHeader =
                $scope.editingCongrats =
                $scope.editingCustomMessage =
                $scope.editingMoreQuestions =
                $scope.editingMap =
                $scope.editingFooter = false;
        };

        this.existingImages =
            [
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/all-sport-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/chess-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/class-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/fencing-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/kids-camp-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/kids-ok-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/kids-run-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/mixedmaterialarts-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/school-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/swimming-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/tabletennis-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/wrestling-cover.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/campaign-mail-footer.jpg",
            window.location.origin + "/Images/Dashboard/Campaigns/mail-images/campaign-mail-header.jpg"
            ];

        this.setHeaderBackground = function ($scope, image) {
            if ($scope.Model.MailTemplate) {
                $scope.Model.MailTemplate.HeaderBackground = image;
            }
        }

        this.getHeaderBackground = function ($scope) {
            if ($scope.Model.MailTemplate)
                return $scope.Model.MailTemplate.HeaderBackground;
        }

        this.setFooterBackground = function ($scope, image) {
            if ($scope.Model.MailTemplate)
                $scope.Model.MailTemplate.FooterBackground = image;
        }

        this.getFooterBackground = function ($scope) {
            if ($scope.Model.MailTemplate)
                return $scope.Model.MailTemplate.FooterBackground;
        }

        this.getClubLogo = function ($scope) {
            if ($scope.Model.MailTemplate)
                return $scope.Model.MailTemplate.ClubLogo;
        }

        this.enterEditing = function ($scope) {
            $('#bodyPanel').animate({
                scrollTop: $(".mail-designer").offset().top - $('#bodyPanel > .view').offset().top
            }, 'slow');

            this._BodyBackColor = $scope.Model.MailTemplate.BodyBackColor;
            this._HasHeader = $scope.Model.MailTemplate.HasHeader;
            this._HasLogo = $scope.Model.MailTemplate.HasLogo;
            this._HasHeaderText = $scope.Model.MailTemplate.HasHeaderText;
            this._HeaderText = $scope.Model.MailTemplate.HeaderText;
            this._headerBackMode = $scope.headerBackMode;
            this._HeaderBackground = $scope.Model.MailTemplate.HeaderBackground;

            this._CongratulationMessage = $scope.Model.MailTemplate.CongratulationMessage;
            this._CustomMessage = $scope.Model.MailTemplate.CustomMessage;

            this._HasMap = $scope.Model.MailTemplate.HasMap;
            this._MapLocation = $scope.Model.MailTemplate.MapLocation;
            this._MapZoom = $scope.Model.MailTemplate.MapZoom;

            this._HasMoreQuestions = $scope.Model.MailTemplate.HasMoreQuestions;
            this._MoreQuestionsPhone = $scope.Model.MailTemplate.MoreQuestionsPhone;
            this._MoreQuestionsEmail = $scope.Model.MailTemplate.MoreQuestionsEmail;

            this._HasFooter = $scope.Model.MailTemplate.HasFooter;
            this._HasFooterText = $scope.Model.MailTemplate.HasFooterText;
            this._FooterText = $scope.Model.MailTemplate.FooterText;
            this._footerBackMode = $scope.footerBackMode;
            this._FooterBackground = $scope.Model.MailTemplate.FooterBackground;
        }

        this.saveEditing = function ($scope, posbanner, typeBannerHV, typeBannerFV) {
         
            $scope.editingEmailSection = $scope.editingHeader = $scope.editingCongrats = $scope.editingMoreQuestions = $scope.editingCustomMessage = $scope.editingMap = $scope.editingFooter = $scope.editingBody = false;
            $scope.Model.MailTemplate.ChangeTemplate = true;

            if(posbanner!=null)
            {
                if (posbanner == 'header' && typeBannerHV == 'upload')
                {
                    $scope.setHeaderBackground($scope.cropper.croppedImage);
                }
                else if (posbanner == 'footer' && typeBannerFV == 'upload') {
                    $scope.setFooterBackground($scope.cropper.croppedImage);
                }
                 
               
            }

        }

        this.cancelEditing = function ($scope) {
            $scope.editingEmailSection = $scope.editingHeader = $scope.editingCongrats = $scope.editingMoreQuestions = $scope.editingCustomMessage = $scope.editingMap = $scope.editingFooter = $scope.editingBody = false;

            $scope.Model.MailTemplate.ChangeTemplate = false;

            $scope.Model.MailTemplate.BodyBackColor = this._BodyBackColor;
            $scope.Model.MailTemplate.HasHeader = this._HasHeader;
            $scope.Model.MailTemplate.HasLogo = this._HasLogo;
            $scope.Model.MailTemplate.HasHeaderText = this._HasHeaderText;
            $scope.Model.MailTemplate.HeaderText = this._HeaderText;
            $scope.Model.MailTemplate.headerBackMode = this._headerBackMode;
            $scope.Model.MailTemplate.HeaderBackground = this._HeaderBackground;

            $scope.Model.MailTemplate.CongratulationMessage = this._CongratulationMessage;
            $scope.Model.MailTemplate.CustomMessage = this._CustomMessage;

            $scope.Model.MailTemplate.HasMap = this._HasMap;
            $scope.Model.MailTemplate.MapLocation = this._MapLocation;
            $scope.Model.MailTemplate.MapZoom = this._MapZoom;

            $scope.Model.MailTemplate.HasMoreQuestions = this._HasMoreQuestions;
            $scope.Model.MailTemplate.MoreQuestionsPhone = this._MoreQuestionsPhone;
            $scope.Model.MailTemplate.MoreQuestionsEmail = this._MoreQuestionsEmail;

            $scope.Model.MailTemplate.HasFooter = this._HasFooter;
            $scope.Model.MailTemplate.HasFooterText = this._HasFooterText;
            $scope.Model.MailTemplate.FooterText = this._FooterText;
            $scope.Model.MailTemplate.footerBackMode = this._footerBackMode;
            $scope.Model.MailTemplate.FooterBackground = this._FooterBackground;
        }

    }]);
})();