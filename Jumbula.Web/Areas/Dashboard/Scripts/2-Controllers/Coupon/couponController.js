﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('couponController', ['$scope', '$http', '$log','$stateParams','$state', function ($scope, $http, $log,$stateParams,$state) {
        window.app.globalObjects.pageClass = 'seasons-view step1';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });
    }]);
})();