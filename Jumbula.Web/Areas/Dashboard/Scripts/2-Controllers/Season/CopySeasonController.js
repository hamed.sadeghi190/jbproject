﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CopySeasonController', ['$scope', '$http', '$log', '$stateParams', '$state', 'seasonService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, seasonService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/Season';

        $scope.Model = seasonService.getModelFromStrorage("seasonModel");

        $scope.CopySeasonStep3 = function () {
            $http.post(baseUrl + '/CopySeasonStep2', { model: $scope.Model }).success(function (data, status, headers, config) {
                if (data.Status) {
                    $scope.Model.CopyInfo = data.Data;
                    seasonService.setModelInStorage($scope.Model, "seasonModel");
                    $state.go('CopySeasonStep3');
                } else {
                    $scope.MC.handleErrors($scope, data);
                }
            }).error(function (data, status, headers, config) {

            });
        }

        $scope.Submit = function () {
            seasonService.setModelInStorage($scope.Model, "seasonModel");
            $http.post(baseUrl + '/CopySeason', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status) {
                        seasonService.setCopySeasonState(true);
                        $state.go('Programs', { seasonDomain: data.SeasonDomain });
                    } else {
                    }
                });
        }

        $scope.cancelSeason = function () {
            $scope.Model = {};
            $scope.Model.IsCopy = "false";
            seasonService.set($scope.Model);
            $scope.MC.getSeasons();
            $state.go('Seasons');
        }

    }]);
})();