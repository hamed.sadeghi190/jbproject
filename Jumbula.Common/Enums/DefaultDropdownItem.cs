﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum DefaultDropdownItem : byte
    {
        [Description("")]
        Empty,
        [Description("All")]
        All,
        [Description("Other")]
        Other,
        [Description("Not listed")]
        NotListed
    }
}
