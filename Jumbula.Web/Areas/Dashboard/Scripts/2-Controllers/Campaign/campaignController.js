﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'campaign-details';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $http.get(baseUrl + '/GetCampaign', { params: { id: $scope.MC.stateParams.campaignId } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
                if ($scope.Model.Status == "draft")
                    $state.go('EmailCampaignAdd', { campaignType: "SomePeople", campaignId: $scope.Model.Id });
            });

        $scope.lineChartDataSource = new kendo.data.DataSource({
            transport:
            {
                read:
                {
                    url: baseUrl + "/GetCampaignReport",
                    data: {
                        campaignId: $scope.MC.stateParams.campaignId,
                        type: 'Line'
                    },
                    dataType: "json",
                    cache: false
                }
            },
            sort: {
                field: "DateTime",
                dir: "asc"
            }
        });

        $scope.linearChartOption = {
            dataSource: $scope.lineChartDataSource,
            legend: {
                visible: true,
                position: "top"
            },
            chartArea: {
                background: "",
            },
            animation: {
                duration: 500
            },
            //seriesColors: colors,
            series: [{
                type: "line",
                style: "smooth",
                field: "Opened",
                name: "Opened",
                overlay: {
                    gradient: "none"
                },
                color: "#9BCFC2",
                gap: 1.5,
                spacing: 0.40
            },
            {
                type: "line",
                style: "smooth",
                field: "Clicked",
                name: "Clicked",
                overlay: {
                    gradient: "none"
                },
                color: "#2B99B6",
                gap: 1.5,
                spacing: 0.40
            }],
            categoryAxis: {
                field: "Date",
                labels: {
                    //rotation: -75,
                    step: 3,
                    visible: true
                },
                crosshair: {
                    visible: true
                },
                justified: true
            },
            valueAxis: {
                visible: false,
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= value # (#= category #)"
            },
        };

        $scope.gridOptionsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetRecipientReport',
                        type: "POST",
                        data: function () {
                            return {
                                term: $scope.searchAttendee,
                                campaignId: $scope.MC.stateParams.campaignId,
                            };
                        }
                    },
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverPaging: true,
            pageSize: 10,
        });

        $scope.gridOptions = {
            dataSource: $scope.gridOptionsDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
            {
                field: "EmailAddress",
                title: "Email",
                width: "110px"
            }, {
                field: "IsDeliveryed",
                title: "Delivered",
                width: "90px"
            }, {
                field: "IsOpened",
                title: "Opened",
                width: "70px"
            }, {
                field: "IsClicked",
                title: "Clicked",
                width: "65px"
            }]
        };

        $scope.attendeeSearch = function () {
            $scope.gridOptions.dataSource.read();
        }

        $scope.changeLabel = function (e, type) {
            var pr = e.sender;
            pr.progressStatus.text(e.value + " %");
            switch (type) {
                case "Opened":
                    pr.progressWrapper.css({
                        "background-color": "#9BCFC2",
                        "border-color": "#9BCFC2"
                    });
                    break;
                case "Clicked":
                    pr.progressWrapper.css({
                        "background-color": "#2B99B6",
                        "border-color": "#2B99B6"
                    });
            }
        }
    }]);
})();