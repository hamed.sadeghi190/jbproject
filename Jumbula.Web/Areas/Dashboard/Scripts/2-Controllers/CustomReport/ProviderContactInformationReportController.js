﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ProviderContactInformationReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;        
        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = {};
        var dataInfo = {};
        $scope.filter = {};

        $scope.filter.printMode = false;
        $scope.reportName = "";

        $http.get(baseUrl + '/GetProviderContactInformationReportDetail', { params: { seasonDomain: $scope.MC.stateParams.seasonDomain } }).success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            dataInfo = data;
            prepareGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        var nameModel = customReportService.getModelFromStrorage();


        var prepareGrid = function () {

            $scope.ProvidersContactInfoDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + "/GetProviderContactInformationReport",
                            dataType: "json",
                            type: "GET",
                            data: function () {
                                return {
                                    seasonId: dataInfo.SeasonId,
                                    printMode: $scope.filter.printMode
                                };
                            },
                        },

                    },
                sort: { field: 'ClassName', dir: 'asc' },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                //serverFiltering: true,
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                //batch: true,
                resizable: true,
            });

            $scope.ProvidersContactInfoReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (ProviderContactInformation).xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Provider contact information - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.ProvidersContactInfoDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5
                },
                dataBound: function () {
                    var grid = $("#ProvidersContactInfoReportGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                sortable: { mode: 'single' },
                columns: [

                    {
                        field: "ClassName",
                        title: "Class name",
                        width: "180px",
                    },
                    {
                        field: "ProviderName",
                        title: "Provider name",
                        width: "200px",
                    },
                    {
                        field: "FullName",
                        title: "Full name",
                        width: "200px",
                    },
                    {
                        field: "ContactEmail",
                        title: "Email (Contact)",
                        width: "200px",
                    },
                    {
                        field: "Phone",
                        title: "Phone",
                        width: "200px",
                    },
                    {
                        field: "Instructor1St",
                        title: "1st Instuctor",
                        width: "200px",
                    },
                    {
                        field: "Phone1StInstructor",
                        title: "Phone",
                        width: "200px",
                    },
                    {
                        field: "Instructor2nd",
                        title: "2nd Instuctor",
                        width: "200px",
                    },
                    {
                        field: "Phone2ndInstructor",
                        title: "Phone",
                        width: "200px",
                    }
                ],

            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ProvidersContactInfoReportGrid").getKendoGrid().saveAsExcel();
        }
        $scope.saveToPDF = function () {
            $scope.filter.printMode = true;
            $("#ProvidersContactInfoReportGrid").data("kendoGrid").dataSource.read();
            $scope.filter.printMode = false;
        }
        $scope.print = function () {
            $scope.filter.printMode = true;
            $("#ProvidersContactInfoReportGrid").data("kendoGrid").dataSource.read();
            $scope.filter.printMode = false;
        }
        $scope.shareReport = function () {
            $state.go('SharePortalReportAsAttchment', { seasonId: dataInfo.SeasonId, reportName: "ProviderContactInformation" });
        }
       
    }
    ]);

})();