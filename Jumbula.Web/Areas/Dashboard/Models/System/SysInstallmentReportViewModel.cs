﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Areas.Dashboard.Models    //.system
{
    public class SysInstallmentReportViewModel : BaseViewModel<OrderInstallment,long>
    {

       
        public SysInstallmentReportViewModel()
        { }

        public string ClubName { get; set; }
        public DateTime InstallmentDate { get; set; }
        public decimal Amount { get; set; }
        public OrderStatusCategories Status { get; set; }
        public string StrStatus
        {
            get
            {
                return Status.ToString();
            }
        }
        public string StrInstallmentDate
        {
            get
            {
                return InstallmentDate.ToString(Constants.DefaultDateFormat);
            }
        }
        public DateTime? TransactionDate { get; set; }
        public string StrTransactionDate
        {
            get
            {
                return TransactionDate.HasValue ? TransactionDate.Value.ToString(Constants.DefaultDateFormat): string.Empty;
            }
        }
        public string StrAmount
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(Amount, CurrencyCodes.USD);
            }
        }
        public string ResponseMessage { get; set; }

    }
}