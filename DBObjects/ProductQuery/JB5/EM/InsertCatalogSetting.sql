INSERT INTO [dbo].[CatalogSettings]
           ([Id]
           ,[VirtusCertified]
           ,[CatalogSearchTags_BeforeSchool]
           ,[CatalogSearchTags_AfterSchool]
           ,[CatalogSearchTags_PreSchools]
           ,[CatalogSearchTags_ElementarySchools]
           ,[CatalogSearchTags_MiddleSchools]
           ,[CatalogSearchTags_HighSchools]
           ,[CatalogSearchTags_BannerImage]
           ,[WeeklyEmail]
           ,[FairFaxCounty]
           ,[CatalogSearchTags_NoGlobalSearch])
     VALUES
           (11966
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0
           ,0)
GO
