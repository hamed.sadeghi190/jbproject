﻿namespace Jumbula.Core.Model.Toolbox
{
    public class ToolboxGoogleAnalyticsViewModel
    {
        public string TrackingId { get; set; }

        public bool Enabled => !string.IsNullOrEmpty(TrackingId);
        
    }
}
