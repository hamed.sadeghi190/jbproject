﻿using System.IO;
using System.Web.Mvc;
using iTextSharp.text;
using Jumbula.Common.Helper;

namespace Jumbula.Web.Infrastructure.Flyer
{
    public class FlyerGenerator
    {
        public FlyerGenerator()
        {
            Size = PageSize.A4;
            MarginTop = 0;
            MarginBottom = 0;
            MarginLeft = 10;
            MarginRight = 10;

        }

        public Rectangle Size { get; set; }

        public float MarginTop { get; set; }
        public float MarginBottom { get; set; }

        public float MarginLeft { get; set; }
        public float MarginRight { get; set; }

        public byte[] GenerateFromView(ControllerContext controllerContext, string viewName, object model)
        {
            var html = ViewHelper.RenderViewToString(controllerContext, viewName, model);

            if (viewName == "TemplateClassCatalog1")
            {
                return GenerateFromHtml(html, "TemplateClassCatalog1");
            }
            else if (viewName == "TemplateClassCatalog2")
            {
                return CreatePDF(html, "TemplateClassCatalog2");
            }
            else if (viewName == "TemplateAfterSchoolClasses2")
            {
                return CreatePDF(html, "TemplateAfterSchoolClasses2");
            }
            else if (viewName == "_InternalReportPdf")
            {
                return GenerateFromHtml(html, "_InternalReportPdf");
            }
            else if (viewName == "SchoolEnrollmentsPdfReports")
            {
                return GenerateFromHtml(html, "SchoolEnrollmentsPdfReports");
            }
            else if (viewName == "_AttendanceReportPdf")
            {
                return GenerateFromHtml(html, "_AttendanceReportPdf");
            }
            else if (viewName == "SignOutSheetPdfReports")
            {
                return GenerateFromHtml(html, "SignOutSheetPdfReports");
            }
            else if (viewName == "SignOutSheetPdfSchoolReports")
            {
                return GenerateFromHtml(html, "SignOutSheetPdfSchoolReports");
            }
            else if (viewName == "InternalRosterReportsPdf")
            {
                return GenerateFromHtml(html, "InternalRosterReportsPdf");
            }
            else if (viewName == "InternalRosterSchoolReportsPdf")
            {
                return GenerateFromHtml(html, "InternalRosterSchoolReportsPdf");
            }
            else if (viewName == "_LogoLastName")
            {
                return GenerateFromHtml(html, "_LogoLastName");
            }
            else
            {
                return GenerateFromHtml(html);
            }
            
        }

        public byte[] GenerateFromHtml(string html, string flyerType = "")
        {
            if (flyerType == "TemplateClassCatalog1")
            {
                return CreatePDF(html, "TemplateClassCatalog1");
            }
            else if (flyerType == "TemplateClassCatalog2")
            {
                return CreatePDF(html, "TemplateClassCatalog2");
            }
            else if (flyerType == "TemplateAfterSchoolClasses2")
            {
                return CreatePDF(html, "TemplateClassCatalog1");
            }
            else if (flyerType == "_InternalReportPdf")
            {
                return CreatePDF(html, "_InternalReportPdf");
            }
            else if (flyerType == "SchoolEnrollmentsPdfReports")
            {
                return CreatePDF(html, "SchoolEnrollmentsPdfReports");
            }
            else if (flyerType == "_AttendanceReportPdf")
            {
                return CreatePDF(html, "_AttendanceReportPdf");
            }
            else if (flyerType == "SignOutSheetPdfReports")
            {
                return CreatePDF(html, "SignOutSheetPdfReports");
            }
            else if (flyerType == "SignOutSheetPdfSchoolReports")
            {
                return CreatePDF(html, "SignOutSheetPdfSchoolReports");
            }
            else if (flyerType == "InternalRosterReportsPdf")
            {
                return CreatePDF(html, "InternalRosterReportsPdf");
            }
            else if (flyerType == "InternalRosterSchoolReportsPdf")
            {
                return CreatePDF(html, "InternalRosterSchoolReportsPdf");
            }
            else if (flyerType == "_LogoLastName")
            {
                return CreatePDF(html, "_LogoLastName");
            }
            else
            {
                return CreatePDF(html);
            }

        }

        private byte[] CreatePDF(string html, string flyerType = "")
        {
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();

            if (flyerType == "TemplateClassCatalog1" || flyerType == "TemplateClassCatalog2" || flyerType == "TemplateAfterSchoolClasses2")
            {
              
                htmlToPdf.Margins.Bottom = 5;
                htmlToPdf.Margins.Top = 5;
                htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
                htmlToPdf.Margins.Left = 0;
                htmlToPdf.Margins.Right = 0;
                htmlToPdf.Zoom = (float)(1.273);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }
            else if (flyerType == "_InternalReportPdf")
            {
                htmlToPdf.Margins.Bottom =4;
                htmlToPdf.Margins.Top = 4;
                htmlToPdf.Margins.Left = 4;
                htmlToPdf.Margins.Right = 4;
                htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
                htmlToPdf.PageHeaderHtml= "<div style='display:inline-block; text-align:left; color:#C82647; font-size:13px !important;font-weight:700 !important; font-family:gothambold !important; '>FOR INTERNAL USE ONLY</div>";
                htmlToPdf.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                htmlToPdf.Zoom = (float)(1.275);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }
           else if (flyerType == "SchoolEnrollmentsPdfReports")
            {
                htmlToPdf.Margins.Bottom = 10;
                htmlToPdf.Margins.Top = 10;
                htmlToPdf.Margins.Left = 10;
                htmlToPdf.Margins.Right = 10;
                htmlToPdf.Zoom = (float)(1.273);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }
            else if (flyerType == "_AttendanceReportPdf")
            {
                htmlToPdf.Margins.Bottom = 10;
                htmlToPdf.Margins.Top = 10;
                htmlToPdf.Margins.Left = 10;
                htmlToPdf.Margins.Right = 10;
                htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
                htmlToPdf.Zoom = (float)(1.273);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }
            else if (flyerType == "SignOutSheetPdfSchoolReports")
            {
                htmlToPdf.Margins.Bottom = 10;
                htmlToPdf.Margins.Top = 10;
                htmlToPdf.Margins.Left = 10;
                htmlToPdf.Margins.Right = 10;
                htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
                htmlToPdf.Zoom = (float)(1.273);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }
            else if (flyerType == "InternalRosterReportsPdf")
            {
                htmlToPdf.Margins.Bottom = 4;
                htmlToPdf.Margins.Top = 4;
                htmlToPdf.Margins.Left = 4;
                htmlToPdf.Margins.Right = 4;
                htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
                htmlToPdf.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                htmlToPdf.Zoom = (float)(1.275);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }
            else if (flyerType == "InternalRosterSchoolReportsPdf")
            {
                htmlToPdf.Margins.Bottom = 4;
                htmlToPdf.Margins.Top = 4;
                htmlToPdf.Margins.Left = 4;
                htmlToPdf.Margins.Right = 4;
                htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
                htmlToPdf.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                htmlToPdf.Zoom = (float)(1.275);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }
            else if (flyerType == "_LogoLastName")
            {
                htmlToPdf.Margins.Bottom = 4;
                htmlToPdf.Margins.Top = 4;
                htmlToPdf.Margins.Left = 4;
                htmlToPdf.Margins.Right = 4;
                htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
                htmlToPdf.Orientation = NReco.PdfGenerator.PageOrientation.Landscape;
                htmlToPdf.Zoom = (float)(1.275);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }
            else
            {
                htmlToPdf.Margins.Bottom = 5;
                htmlToPdf.Margins.Top = 5;
                htmlToPdf.Margins.Left = 5;
                htmlToPdf.Margins.Right = 5;
                htmlToPdf.PageFooterHtml = "<div style='display:block;margin-bottom:20px; text-align:center; color:#808080; font-size:13px !important; font-family: gothambold;padding:0 35px;'>Page: <span style='display:inline-block;' class='page'></span></div>";
                htmlToPdf.Zoom = (float)(1.273);
                htmlToPdf.Size = NReco.PdfGenerator.PageSize.A4;
            }

        
            var pdfBytes = htmlToPdf.GeneratePdf(html);

            return pdfBytes;
        }

        private Document CreateDocument()
        {
            return new Document(Size, MarginLeft, MarginRight, MarginTop, MarginBottom);
        }

        private byte[] ConverMemoryStreamToByte(MemoryStream memoryStream)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = memoryStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}