﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ClubPageMenuViewModel
    {
        public string ClubDomain { get; set; }
        public int ClubCategoryId { get; set; }
    }
}