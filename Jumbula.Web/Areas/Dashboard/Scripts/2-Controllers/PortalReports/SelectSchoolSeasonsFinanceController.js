﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SelectSchoolSeasonsFinanceController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReports", order: 1, menu: "PortalReports" });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var timer = false;

        $scope.Model = {};

 
        $scope.Model.schoolId = 0;
        $scope.Model.SeasonId = 0;
        

            $http.get(baseUrl + '/GetAllSchool') //GetProvider delete
            .success(function (data, status, headers, config) {
                
                $scope.AllSchools = data;
            });

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };

        
        $scope.$watch('AllSeasons', function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.fillSeasonData = function (schoolId) {
            $http.get(baseUrl + '/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllSeasons = data;

                //$scope.$apply();
               
            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.Model.schoolId = schoolId;

            $scope.Model.seasonId = seasonId;

        
        }
    

        var checkValidation = function (schoolId,seasonId) {
           
            if (is.undefined(schoolId) || schoolId <= 0) {
                jbToast.error("Please select a school.");
                return false;
            }
            else if (is.undefined(seasonId) || seasonId <= 0) {
                jbToast.error("Please select a season.");
                return false;
            }
            else {
                return true;
            }
        }
      

        $scope.runReport = function () {
            if (checkValidation($scope.Model.schoolId, $scope.Model.SeasonId)) {
                customReportService.saveSchoolSeasonId($scope.Model.schoolId, $scope.Model.SeasonId);
            
                var seasonId = $scope.Model.seasonId;
                var schoolId = $scope.Model.schoolId;
              
                $state.go('AllSeasonReconciliationReport', { seasonId: $scope.Model.SeasonId });
            }
        }
    }
    ]);
})();