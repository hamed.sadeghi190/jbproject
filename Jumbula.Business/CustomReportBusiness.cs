﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Business
{
    public class CustomReportBusiness : ICustomReportBusiness
    {
        private readonly IPlayerProfileBusiness _playerProfileBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IAccountingBusiness _accountingBusiness;
        private readonly IDiscountBusiness _discountBusiness;
        private readonly IChargeBusiness _chargeBusiness;
        private readonly INewReportBusiness _reportBusiness;

        public CustomReportBusiness(IPlayerProfileBusiness playerProfileBusiness, IProgramBusiness programBusiness, IClubBusiness clubBusiness, IAccountingBusiness accountingBusiness, IDiscountBusiness discountBusiness, IChargeBusiness chargeBusiness, INewReportBusiness reportBusiness)
        {
            _playerProfileBusiness = playerProfileBusiness;
            _programBusiness = programBusiness;
            _clubBusiness = clubBusiness;
            _accountingBusiness = accountingBusiness;
            _discountBusiness = discountBusiness;
            _chargeBusiness = chargeBusiness;
            _reportBusiness = reportBusiness;
        }

        public ExportDocumnetSchema CustomReportToExcel(List<Dictionary<string, object>> dataModel, List<string> titles = null)
        {
            var columns = new List<ExportDocumentColumn>();
            if (titles == null)
            {
                columns =
                    dataModel.First()
                        .ToList()
                        .Where(c => c.Key != "OrderId" && c.Key != "OrderItemId")
                        .Select(c => new ExportDocumentColumn
                        {
                            DataName = c.Key,
                            DisplayName = c.Key.Replace("_", " ").TrimStart()
                        }).ToList();
            }
            else
            {
                for (int i = 0; i < dataModel.First().Count; i++)
                {
                    var column = dataModel.First().ElementAt(i);
                    var title = titles.ElementAt(i);

                    columns.Add(new ExportDocumentColumn()
                    {
                        DataName = column.Key,
                        DisplayName = title.Replace("_", " ").Replace("sDash", " ")
                    });

                }
            }

            var allObjects =
                dataModel.Select(item => item.ToList().ToDictionary(prop => prop.Key, prop => (prop.Value != null) ? prop.Value.ToString() : string.Empty))
                    .Cast<object>()
                    .ToList();

            var expsch = new ExportDocumnetSchema
            {
                Columns = columns,
                Data = allObjects.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportCapacityReport(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>()
                        {
                            new ExportDocumentColumn {DataName = "ProgramName", DisplayName = "Program Name"},
                            new ExportDocumentColumn {DataName = "Status", DisplayName = "Status"},
                            new ExportDocumentColumn {DataName = "Open", DisplayName = "Open"},
                            new ExportDocumentColumn {DataName = "Filled", DisplayName = "Registered"},
                            new ExportDocumentColumn {DataName = "Schedule", DisplayName = "Schedule Date"}
                        },
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportInstallmentReport(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>()
                        {
                            new ExportDocumentColumn {DataName = "ProgramName", DisplayName = "Program Name"},
                            new ExportDocumentColumn {DataName = "ParticipantFullName", DisplayName = "Full name"},
                            new ExportDocumentColumn {DataName = "Email", DisplayName = "Email"},
                            new ExportDocumentColumn {DataName = "RegDate", DisplayName = "Registration date"},
                            new ExportDocumentColumn {DataName = "ConfirmationId", DisplayName = "Confirmation"},
                            new ExportDocumentColumn {DataName = "InstallmentsNum", DisplayName = "Installments"},
                            new ExportDocumentColumn {DataName = "PaidInstallments", DisplayName = "Paid installments"},
                            new ExportDocumentColumn {DataName = "Installments", DisplayName = "Installment details"},
                            new ExportDocumentColumn {DataName = "DoubleTotal", DisplayName = "Total"},
                            new ExportDocumentColumn {DataName = "DoublePaidAmount", DisplayName = "Paid amount"},
                            new ExportDocumentColumn {DataName = "DoubleBalance", DisplayName = "Balance"},
                            new ExportDocumentColumn {DataName = "PastDue", DisplayName = "PastDue"},
                            new ExportDocumentColumn {DataName = "Preapproval", DisplayName = "Preapproval"},
                        },
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportFollowupReport(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>()
                        {
                            new ExportDocumentColumn {DataName = "FullName", DisplayName = "Full name"},
                            new ExportDocumentColumn {DataName = "Email", DisplayName = "Email"},
                            new ExportDocumentColumn {DataName = "ProgramName", DisplayName = "Program name"},
                            new ExportDocumentColumn {DataName = "RegistrationDate", DisplayName = "Registration date"},
                            new ExportDocumentColumn {DataName = "ConfirmationId", DisplayName = "Confirmation"},
                            new ExportDocumentColumn {DataName = "FollowupForms", DisplayName = "Follow-up from details"},
                        },
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportBalanceReport(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>()
                        {
                            new ExportDocumentColumn {DataName = "FullName", DisplayName = "Full name"},
                            new ExportDocumentColumn {DataName = "ProgramName", DisplayName = "Program name"},
                            new ExportDocumentColumn {DataName = "Email", DisplayName = "Email"},
                            new ExportDocumentColumn {DataName = "RegDate", DisplayName = "Registration date"},
                            new ExportDocumentColumn {DataName = "ConfirmationId", DisplayName = "Confirmation Id"},
                            new ExportDocumentColumn {DataName = "PaymentDetail", DisplayName = "Payment method details"},
                            new ExportDocumentColumn {DataName = "Balance", DisplayName = "Balance details"},
                        },
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportFinanceReport(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>()
                        {
                            new ExportDocumentColumn {DataName = "FullName", DisplayName = "Full name"},
                            new ExportDocumentColumn {DataName = "UserEmail", DisplayName = "Account email"},
                            new ExportDocumentColumn {DataName = "PayerEmail", DisplayName = "Payer email"},
                            new ExportDocumentColumn {DataName = "TransactionCategory", DisplayName = "Transaction category"},
                            new ExportDocumentColumn {DataName = "CheckId", DisplayName = "Transaction ID"},
                            new ExportDocumentColumn {DataName = "Date", DisplayName = "Transaction date"},
                            new ExportDocumentColumn {DataName = "Confirmation", DisplayName = "Confirmation Id"},
                            new ExportDocumentColumn {DataName = "TransactionType", DisplayName = "Transaction type"},
                            new ExportDocumentColumn {DataName = "PaymentMethod", DisplayName = "Payment method"},
                            new ExportDocumentColumn {DataName = "PaypalHandler", DisplayName = "Paypal handler"},
                            new ExportDocumentColumn {DataName = "Note", DisplayName = "Note"},
                            new ExportDocumentColumn {DataName = "Payment", DisplayName = "Payment"},
                            new ExportDocumentColumn {DataName = "Charge", DisplayName = "Deposit"},
                            new ExportDocumentColumn {DataName = "Balance", DisplayName = "Balance"}
                        },
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportCouponReport(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>()
                        {
                            new ExportDocumentColumn {DataName = "FullName", DisplayName = "Full name"},
                            new ExportDocumentColumn {DataName = "ProgramName", DisplayName = "Program"},
                            new ExportDocumentColumn {DataName = "Email", DisplayName = "Email"},
                            new ExportDocumentColumn {DataName = "RegDate", DisplayName = "Registration date"},
                            new ExportDocumentColumn {DataName = "ConfirmationId", DisplayName = "Confirmation"},
                            new ExportDocumentColumn {DataName = "CouponName", DisplayName = "Coupon name"},
                            new ExportDocumentColumn {DataName = "CouponCode", DisplayName = "Coupon code"},
                            new ExportDocumentColumn {DataName = "Amount", DisplayName = "Coupon amount"},
                        },
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportChargeDiscountReport(List<Dictionary<string, string>> dataModel)
        {
            // filter two Columns (OrderItemId,OrderId), these are hidden field in the grid.
            var columns =
                dataModel.First()
                    .ToList()
                    .Where(c => c.Key != "OrderId" && c.Key != "OrderItemId" && c.Key != "ItemStatusReason")
                    .Select(c => new ExportDocumentColumn
                    {
                        DataName = c.Key,
                        DisplayName = c.Key.Replace("_s_", "(").Replace("_p_", ")").Replace("_", " ").TrimStart()
                    }).ToList();

            var allObjects =
                dataModel.Select(item => item.ToList().ToDictionary(prop => prop.Key, prop => prop.Value.ToString()))
                    .Cast<object>()
                    .ToList();

            var expsch = new ExportDocumnetSchema
            {
                Columns = columns,
                Data = allObjects.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportRegistrationFormReport(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>(),
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportProviderContactInfo(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>()
                        {
                            new ExportDocumentColumn {DataName = "ClassName", DisplayName = "Class name"},
                            new ExportDocumentColumn {DataName = "ProviderName", DisplayName = "Provider name"},
                            new ExportDocumentColumn {DataName = "FullName", DisplayName = "Full name"},
                            new ExportDocumentColumn {DataName = "ContactEmail", DisplayName = "Email (Contact)"},
                            new ExportDocumentColumn {DataName = "Phone", DisplayName = "Phone"},
                            new ExportDocumentColumn {DataName = "Instructor1St", DisplayName = "1st Instuctor"},
                            new ExportDocumentColumn {DataName = "Phone1StInstructor", DisplayName = "Phone"},
                           new ExportDocumentColumn {DataName = "Instructor2nd", DisplayName = "2nd Instuctor"},
                            new ExportDocumentColumn {DataName = "Phone2ndInstructor", DisplayName = "Phone"},
                        },
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public ExportDocumnetSchema ExportRoomAssigments(List<object> dataModel)
        {
            var expsch = new ExportDocumnetSchema()
            {
                Columns = new List<ExportDocumentColumn>()
                        {
                            new ExportDocumentColumn {DataName = "ClassName", DisplayName = "Class name"},
                            new ExportDocumentColumn {DataName = "SpaceReq", DisplayName = "Space Requirements"},
                            new ExportDocumentColumn {DataName = "AdditionalComments", DisplayName = "Additional comments"},
                            new ExportDocumentColumn {DataName = "Duration", DisplayName = "Class Duration"},
                            new ExportDocumentColumn {DataName = "Sessions", DisplayName = "Meeting Dates"},
                            new ExportDocumentColumn {DataName = "Min", DisplayName = "Minimum"},
                            new ExportDocumentColumn {DataName = "Max", DisplayName = "Maximum"},
                            new ExportDocumentColumn {DataName = "Room", DisplayName = "Room Assignment"}
                        },
                Data = dataModel.ToList()
            };

            return expsch;
        }

        public JbSection CreateMoreInfoSection()
        {
            var moreSection = new JbSection
            {
                Name = "MoreInfoSection",
                Title = "More Info",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name="UserEmail",Title="User email" },

                    new JbCheckBox { Name="Tuition",Title="Tuition price" },
                    new JbCheckBox { Name="TuitionName",Title="Tuition name" },

                    new JbCheckBox { Name="ConfirmationId",Title="Confirmation" },

                    new JbCheckBox { Name="OrderDate", Title="Registration date" },
                    new JbCheckBox { Name="OrderType",Title="Order type" },
                    new JbCheckBox { Name="OrderAmount",Title="Order amount" },

                    new JbCheckBox { Name="OrderPaidAmount",Title="Paid amount" },
                    new JbCheckBox { Name="Balance",Title="Balance" },
                    new JbCheckBox { Name="BalanceStatus",Title="Balance status" },

                    new JbCheckBox { Name="DiscountTotal",Title="Total discount" },

                    new JbCheckBox { Name="PaymentStatus",Title="Payment status" },
                    new JbCheckBox { Name="PaymentMethod",Title="Payment method (transaction level)" },
                    new JbCheckBox { Name="PaymentMethodOrder",Title="Payment method (order level)" },
                    new JbCheckBox { Name="PaymentDate",Title="Payment date" },
                    new JbCheckBox { Name="TransactionId",Title="Transaction ID" },

                    new JbCheckBox { Name="ScheduleStartEnd",Title="Schedule start/end date" },
                    new JbCheckBox { Name="ScheduleStartDate",Title="Schedule start date" },
                    new JbCheckBox { Name="ScheduleEndDate",Title="Schedule end date" },

                    new JbCheckBox { Name="ProgramType",Title="Program type" },
                    new JbCheckBox { Name="ProgramLocation",Title="Program location" },
                    new JbCheckBox { Name="ProgramLocationName",Title="Location name" },
                    new JbCheckBox { Name="ProgramInstructor",Title="Program instructor" },
                    new JbCheckBox { Name="ProgramRoomNumber",Title="Program room assignment" },
                    new JbCheckBox { Name="ProgramDayOfWeek",Title="Program day of week" },
                    new JbCheckBox { Name="ProgramStartDate",Title="Program start date" },
                    new JbCheckBox { Name="ProgramEndDate",Title="Program end date" },
                    new JbCheckBox { Name="SchoolId",Title="School ID" },
                    new JbCheckBox { Name="SchoolName",Title="School" },
                    new JbCheckBox { Name="ExtraServices",Title="Extra services" },
                    new JbCheckBox { Name="SchoolDistrict",Title="District" },
                    new JbCheckBox { Name="SeasonName",Title="Season" },
                    new JbCheckBox { Name="ScheduleId",Title="Schedule ID" },
                    new JbCheckBox { Name="ScheduleTitle",Title="Schedule title" },
                    new JbCheckBox { Name="ScheduleStartTime",Title="Schedule start time" },
                    new JbCheckBox { Name="ScheduleEndTime",Title="Schedule end time" },
                    new JbCheckBox { Name="DesiredStartDate",Title="Desired start date" },
                    new JbCheckBox { Name="ClassDays",Title="Class days" },
                    new JbCheckBox { Name="OrganizationActiveType",Title="Member is active" },
                    new JbCheckBox { Name="RegistrationId",Title="Registration ID" },
                    new JbCheckBox { Name="Discounts_Coupons",Title="Discounts/Coupons" },
                    new JbCheckBox { Name="CancellationEffectiveDate",Title="Cancellation effective date" },
                    new JbCheckBox { Name="TransferEffectiveDate",Title="Transfer effective date" },
                    new JbCheckBox { Name="OrderStatus",Title="Order status" },
                    new JbCheckBox { Name="DropinDates",Title="Drop-in dates" },
                }
            };

            return moreSection;
        }

        public JbSection CreateChargeSection(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var allCharges = club.IsPartner ? _chargeBusiness.GetAllSeasonPartnerCharges(clubId) : _chargeBusiness.GetAllSeasonClubCharges(clubId);

            var charges = allCharges
                .Select(c => new JbCheckBox
                {
                    Name = "Charges",
                    Title = c.Trim()
                })
                .ToList<IJbBaseElement>();

            var chargesSection = new JbSection
            {
                Name = "ChargeSection",
                Title = "Charges",
                Elements = charges
            };

            return chargesSection;
        }

        public JbSection CreateDiscountCouponSection(long? seasonId, int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var allDiscountCoupons = seasonId.HasValue
                ? _discountBusiness.GetAllSeasonDiscountCoupon(seasonId.Value)
                : club.IsPartner ? _discountBusiness.GetAllSeasonPartnerDiscountCoupon(clubId) : _discountBusiness.GetAllSeasonClubDiscountCoupon(clubId);

            var discountsCoupons = allDiscountCoupons
                .Select(c => new JbCheckBox
                {
                    Name = "DiscountCoupon",
                    Title = c.Trim()
                })
                .ToList<IJbBaseElement>();

            var discountCouponSection = new JbSection
            {
                Name = "DiscountCouponSection",
                Title = "Discounts and coupons",
                Elements = discountsCoupons
            };

            return discountCouponSection;
        }

        public JbSection CreateChessTourneySection()
        {
            var chessSection = new JbSection
            {
                Name = "chessSection",
                Title = "Chess Tourney Info",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name="SectionName",Title="Section name" },
                    new JbCheckBox { Name="Bye",Title="Bye" },
                }
            };

            return chessSection;
        }

        public JbSection CreateChessTournamentSection()
        {
            var chessTournamentSection = new JbSection
            {
                Name = "ChessTournamentSection",
                Title = "Chess Tournament Info",
                Elements = new List<IJbBaseElement>
                {
                    new JbCheckBox { Name="SectionName",Title="Section name" },
                    new JbCheckBox { Name="ScheduleName",Title="Schedule" },
                }
            };

            return chessTournamentSection;
        }

        public string GetProgramDayTime(ProgramSchedule schedule)
        {
            var day = string.Empty;
            if (schedule != null && schedule.Program.TypeCategory != ProgramTypeCategory.ChessTournament && schedule.Program.TypeCategory != ProgramTypeCategory.Subscription && schedule.Program.TypeCategory != ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var dayItem in ((ScheduleAttribute)schedule.Attributes).Days)
                {
                    if (!string.IsNullOrEmpty(day))
                    {
                        day += ", ";
                    }
                    day += dayItem.DayOfWeek.ToString().Substring(0, 3);
                    if (dayItem.StartTime != null)
                        if (dayItem.EndTime != null)
                            day += $" {DateTime.Today.Add(dayItem.StartTime.Value).ToString("h:mmtt").ToLower()}-{DateTime.Today.Add(dayItem.EndTime.Value).ToString("h:mmtt").ToLower()}";
                }
            }
            else if (schedule != null && schedule.Program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                foreach (var dayItem in ((ScheduleSubscriptionAttribute)schedule.Attributes).Days)
                {
                    if (!string.IsNullOrEmpty(day))
                    {
                        day += ", ";
                    }
                    day += dayItem.DayOfWeek.ToString().Substring(0, 3);

                    var startDate = dayItem.StartTime.HasValue ? DateTime.Today.Add(dayItem.StartTime.Value).ToString("h:mmtt").ToLower() : null;
                    var endDate = dayItem.EndTime.HasValue ? DateTime.Today.Add(dayItem.EndTime.Value).ToString("h:mmtt").ToLower() : null;
                    if (startDate != null && endDate != null)
                    {
                        day += $" {startDate}-{endDate}";
                    }
                }
            }
            return day;
        }

        public List<Dictionary<string, object>> GetCustomReportParentInfo(EventRoaster eventRoaster, List<OrderItem> allOrderItems, ref List<string> titles, bool isPartnerDashboard = false, bool isShare = false)
        {
            var result = new List<Dictionary<string, object>>();
            var titlesName = new List<List<string>>();

            var playerProfileBusiness = _playerProfileBusiness;

            var jbForm = RemoveUncheckedElementsJbForm(eventRoaster.JbForm);

            var sections = GetSectionsJbForm(jbForm);

            var userIds = allOrderItems.Select(a => a.Order.UserId).ToList();

            var allParents = playerProfileBusiness.GetAllParentsProfile(userIds, true);
            var allEmergencyContacts = playerProfileBusiness.GetAllEmergencyFamily(userIds, true);
            var allAuthorizePickups = playerProfileBusiness.GetAllAuthorizePickupFamily(userIds, true);
            var allInsurances = playerProfileBusiness.GetAllInsuranceFamily(userIds);


            foreach (var orderItem in allOrderItems)
            {
                var row = new Dictionary<string, object>();
                var rowTitle = new List<string>();

                if (isPartnerDashboard)
                {
                    row.Add("School", orderItem != null ? orderItem.Order.Club.Name : string.Empty);
                    rowTitle.Add("School");
                }

                foreach (var section in sections)
                {
                    switch (section.Key)
                    {
                        case SectionsName.General:
                            SetProgramToJbForm(section.Value, orderItem, row, rowTitle, isShare);
                            break;

                        case SectionsName.ParticipantSection:
                            SetParticipantInfoJbForm(section.Value, orderItem.Player, row, rowTitle, isShare);
                            break;

                        case SectionsName.ParentGuardianSection:
                            var userId = orderItem.Player.UserId;
                            var parents = allParents.Where(p => p.UserId == userId).ToList();
                            SetParentInfoToJbForm(section.Value, parents, row, rowTitle, isShare);
                            break;

                        case SectionsName.EmergencyContactSection:
                            var emergencyContacts = allEmergencyContacts.Where(e => orderItem != null && e.Family.UserId == orderItem.Player.UserId).ToList();
                            SetEmergencyContactInfoToJbForm(section.Value, emergencyContacts, row, rowTitle, isShare);
                            break;

                        case SectionsName.AuthorizedPickup:
                            var authorizedPickups = allAuthorizePickups.Where(e => orderItem != null && e.Family.UserId == orderItem.Player.UserId).ToList();
                            SetAuthorizeAdultInfoToJbForm(section.Value, authorizedPickups, row, rowTitle, isShare);
                            break;

                        case SectionsName.InsuranceSection:
                            var insurances = allInsurances.Where(e => orderItem != null && e.UserId == orderItem.Player.UserId).ToList();
                            SetInsuranceInfoToJbForm(section.Value, insurances, row, rowTitle, isShare);
                            break;
                    }
                }

                if (jbForm.Elements.Any(e => e.Name == SectionsName.MoreInfoSection.ToString()))
                {
                    var elements = ((JbSection)jbForm.Elements.Single(e => e.Name == SectionsName.MoreInfoSection.ToString())).Elements.ToList();

                    SetMoreInfoSectionToJbForm(elements, orderItem, row, rowTitle, isShare);
                }

                if (jbForm.Elements.Any(e => e.Name == SectionsName.ChargeSection.ToString()))
                {
                    var elements = ((JbSection)jbForm.Elements.Single(e => e.Name == SectionsName.ChargeSection.ToString())).Elements.ToList();

                    SetChargesToJbForm(elements, orderItem, row, rowTitle, isShare);
                }

                if (jbForm.Elements.Any(e => e.Name == SectionsName.DiscountCouponSection.ToString()))
                {
                    var elements = ((JbSection)jbForm.Elements.Single(e => e.Name == SectionsName.DiscountCouponSection.ToString())).Elements.ToList();

                    SetDiscountCouponSectionToJbForm(elements, orderItem, row, rowTitle, isShare);
                }

                if (jbForm.Elements.Any(e => e.Name == SectionsName.ChessTournamentSection.ToString()))
                {
                    var elements = ((JbSection)jbForm.Elements.Single(e => e.Name == SectionsName.ChessTournamentSection.ToString())).Elements.ToList();

                    SetChessTournamentSectionToJbForm(elements, orderItem, row, rowTitle, isShare);
                }

                result.Add(row);
                titlesName.Add(rowTitle);
            }

            result = MergeColumnsCustomReport(result, titlesName, ref titles);

            result = RemoveDuplicates(result);

            if (isPartnerDashboard)
            {
                result = SortDictionatyData(result, "School");
            }

            return result;
        }

        public List<Dictionary<string, object>> SortDictionatyData(List<Dictionary<string, object>> data, string orderBySorting)
        {
            return data.OrderBy(d => d[orderBySorting]).ToList();
        }

        #region Custom report parent


        private static List<Dictionary<T1, T2>> RemoveDuplicates<T1, T2>(List<Dictionary<T1, T2>> source)
        {
            List<Dictionary<T1, T2>> result = new List<Dictionary<T1, T2>>();
            foreach (Dictionary<T1, T2> item in source)
            {
                if (result.All(t => !CompareForEquals(item, t)))
                {
                    result.Add(item);//If not present add to list 
                }
            }
            return result;
        }

        private static bool CompareForEquals<T1, T2>(Dictionary<T1, T2> one, Dictionary<T1, T2> two)
        {
            if (one == null || two == null)
                return false;
            if (one.Count != two.Count)
                return false;
            foreach (KeyValuePair<T1, T2> pair in one)
            {
                if (!two.ContainsKey(pair.Key))
                    return false;
                if (!Equals(two[pair.Key], pair.Value))
                    return false;
            }
            return true;
        }

        private static List<Dictionary<string, object>> MergeColumnsCustomReport(List<Dictionary<string, object>> data, List<List<string>> titleNames, ref List<string> titles)
        {
            if (!data.Any())
            {
                return new List<Dictionary<string, object>>();
            }

            var result = new List<Dictionary<string, object>>();

            var schools = new List<List<string>>();
            var schoolTitleNames = new List<List<string>>();
            var programs = new List<List<string>>();
            var programTitleNames = new List<List<string>>();
            var participants = new List<List<string>>();
            var participantTitleNames = new List<List<string>>();
            var parents = new List<List<string>>();
            var parentTitleNames = new List<List<string>>();
            var authorizedPickups = new List<List<string>>();
            var authorizedPickupTitleNames = new List<List<string>>();
            var emergencyContacts = new List<List<string>>();
            var emergencyContactTitleNames = new List<List<string>>();
            var insurances = new List<List<string>>();
            var insuranceTitleNames = new List<List<string>>();
            var charges = new List<List<string>>();
            var chargeTitleNames = new List<List<string>>();
            var discountCoupons = new List<List<string>>();
            var discountCouponTitleNames = new List<List<string>>();
            var moreInfos = new List<List<string>>();
            var moreInfoTitleNames = new List<List<string>>();
            var chessTournaments = new List<List<string>>();
            var chessTournamentTitleNames = new List<List<string>>();

            for (var i = 0; i < data.Count; i++)
            {
                var row = data[i];
                var title = titleNames[i];

                schools.Add(row.Where(r => r.Key.StartsWith("School")).Select(r => r.Key).ToList());
                schoolTitleNames.Add(title.Where(r => r.StartsWith("School")).ToList());

                programs.Add(row.Where(r => r.Key.StartsWith("_Program")).Select(r => r.Key).ToList());
                programTitleNames.Add(title.Where(r => r.StartsWith("_Program")).ToList());

                participants.Add(row.Where(r => r.Key.StartsWith("_Participant")).Select(r => r.Key).ToList());
                participantTitleNames.Add(title.Where(r => r.StartsWith("_Participant")).ToList());

                parents.Add(row.Where(r => r.Key.StartsWith("_Parent")).Select(r => r.Key).ToList());
                parentTitleNames.Add(title.Where(r => r.StartsWith("_Parent")).ToList());

                authorizedPickups.Add(row.Where(r => r.Key.StartsWith("_Authorized_pickup")).Select(r => r.Key).ToList());
                authorizedPickupTitleNames.Add(title.Where(r => r.StartsWith("_Authorized_pickup")).ToList());

                emergencyContacts.Add(row.Where(r => r.Key.StartsWith("_Emergency")).Select(r => r.Key).ToList());
                emergencyContactTitleNames.Add(title.Where(r => r.StartsWith("_Emergency")).ToList());

                insurances.Add(row.Where(r => r.Key.StartsWith("_Insurances")).Select(r => r.Key).ToList());
                insuranceTitleNames.Add(title.Where(r => r.StartsWith("_Insurances")).ToList());

                charges.Add(row.Where(r => r.Key.StartsWith("_Charges")).Select(r => r.Key).ToList());
                chargeTitleNames.Add(title.Where(r => r.StartsWith("_Charges")).ToList());

                discountCoupons.Add(row.Where(r => r.Key.StartsWith("_Discounts_and_coupons")).Select(r => r.Key).ToList());
                discountCouponTitleNames.Add(title.Where(r => r.StartsWith("_Discounts_and_coupons")).ToList());

                moreInfos.Add(row.Where(r => r.Key.StartsWith("_More_Info")).Select(r => r.Key).ToList());
                moreInfoTitleNames.Add(title.Where(r => r.StartsWith("_More_Info")).ToList());

                chessTournaments.Add(row.Where(r => r.Key.StartsWith("_Chess_tournament_info")).Select(r => r.Key).ToList());
                chessTournamentTitleNames.Add(title.Where(r => r.StartsWith("_Chess_tournament_info")).ToList());
            }

            var schoolColumns = schools.OrderByDescending(s => s.Count).ElementAt(0);
            var schoolTitles = schoolTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var programColumns = programs.OrderByDescending(s => s.Count).ElementAt(0);
            var programTitles = programTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var participantColumns = participants.OrderByDescending(s => s.Count).ElementAt(0);
            var participantTitles = participantTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var parentColumns = parents.OrderByDescending(s => s.Count).ElementAt(0);
            var parentTitles = parentTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var authorizedColumns = authorizedPickups.OrderByDescending(s => s.Count).ElementAt(0);
            var authorizedPickupTitles = authorizedPickupTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var emergencyColumns = emergencyContacts.OrderByDescending(s => s.Count).ElementAt(0);
            var emergencyContactTitles = emergencyContactTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var insuranceColumns = insurances.OrderByDescending(s => s.Count).ElementAt(0);
            var insuranceTitles = insuranceTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var chargeColumns = charges.OrderByDescending(s => s.Count).ElementAt(0);
            var chargeTitles = chargeTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var discountCouponColumns = discountCoupons.OrderByDescending(s => s.Count).ElementAt(0);
            var discountCouponTitles = discountCouponTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var moreInfoColumns = moreInfos.OrderByDescending(s => s.Count).ElementAt(0);
            var moreInfoTitles = moreInfoTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            var chessTournametColumns = chessTournaments.OrderByDescending(s => s.Count).ElementAt(0);
            var chessTournamentTitles = chessTournamentTitleNames.OrderByDescending(s => s.Count).ElementAt(0);

            foreach (var row in data)
            {
                var rowData = new Dictionary<string, object>();

                for (int j = 0; j < schoolColumns.Count; j++)
                {
                    var column = schoolColumns.ElementAt(j);
                    var title = schoolTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < programColumns.Count; j++)
                {
                    var column = programColumns.ElementAt(j);
                    var title = programTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < participantColumns.Count; j++)
                {
                    var column = participantColumns.ElementAt(j);
                    var title = participantTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < parentColumns.Count; j++)
                {
                    var column = parentColumns.ElementAt(j);
                    var title = parentTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < authorizedColumns.Count; j++)
                {
                    var column = authorizedColumns.ElementAt(j);
                    var title = authorizedPickupTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < emergencyColumns.Count; j++)
                {
                    var column = emergencyColumns.ElementAt(j);
                    var title = emergencyContactTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < insuranceColumns.Count; j++)
                {
                    var column = insuranceColumns.ElementAt(j);
                    var title = insuranceTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < chargeColumns.Count; j++)
                {
                    var column = chargeColumns.ElementAt(j);
                    var title = chargeTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < discountCouponColumns.Count; j++)
                {
                    var column = discountCouponColumns.ElementAt(j);
                    var title = discountCouponTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < moreInfoColumns.Count; j++)
                {
                    var column = moreInfoColumns.ElementAt(j);
                    var title = moreInfoTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                for (int j = 0; j < chessTournametColumns.Count; j++)
                {
                    var column = chessTournametColumns.ElementAt(j);
                    var title = chessTournamentTitles.ElementAt(j);

                    var value = row.Keys.Contains(column) ? row[column] : string.Empty;

                    rowData.Add(column, value);
                    titles.Add(title);
                }

                result.Add(rowData);
            }

            return result;
        }

        private static void SetProgramToJbForm(List<ElementsName> fields, OrderItem orderItem, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            foreach (var field in fields)
            {
                switch (field)
                {
                    case ElementsName.ProgramId:
                        {
                            rowTitle.Add("_Program ID");

                            var columnName = isShare == false ? "_ProgramIdsDash" : "_Program_ID ";

                            result.Add(columnName, orderItem.ProgramSchedule.Program.Id);
                        }
                        break;
                    case ElementsName.Program:
                        {
                            rowTitle.Add("_Program");

                            var programName =
                                orderItem.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare
                                    ? $"{orderItem.ProgramSchedule.Program.Name} - {orderItem.ProgramSchedule.Title}"
                                    : orderItem.ProgramSchedule.Program.Name;

                            result.Add("_Program", programName);
                        }
                        break;
                }
            }

        }

        private static void SetParticipantInfoJbForm(List<ElementsName> fields, PlayerProfile player, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var columnName = isShare == false ? "_ParticipantsDash" : "_Participant ";

            foreach (var field in fields)
            {
                switch (field)
                {
                    case ElementsName.ParticipantId:
                        var participantId = player != null ? player.Id.ToString() : string.Empty;
                        result.Add(columnName + ElementsName.ParticipantId.ToDescription().Replace(" ", "_"), participantId);
                        rowTitle.Add(columnName + ElementsName.ParticipantId.ToDescription());
                        break;

                    case ElementsName.FirstName:
                        var firstName = player != null ? player.Contact.FirstName : string.Empty;
                        result.Add(columnName + ElementsName.FirstName.ToDescription().Replace(" ", "_"), firstName);
                        rowTitle.Add(columnName + ElementsName.FirstName.ToDescription());
                        break;

                    case ElementsName.LastName:
                        var lastName = player != null ? player.Contact.LastName : string.Empty;
                        result.Add(columnName + ElementsName.LastName.ToDescription().Replace(" ", "_"), lastName);
                        rowTitle.Add(columnName + ElementsName.LastName.ToDescription());
                        break;

                    case ElementsName.Grade:
                        var grade = player?.Info != null ? player.Info.Grade != 0 ? player.Info.Grade.ToDescription() : string.Empty : string.Empty;
                        result.Add(columnName + ElementsName.Grade.ToDescription().Replace(" ", "_"), grade);
                        rowTitle.Add(columnName + ElementsName.Grade.ToDescription());
                        break;

                    case ElementsName.DoB:
                        var dob = player?.Contact?.DoB != null ? player.Contact.DoB.Value.Date.ToShortDateString() : string.Empty;
                        result.Add(columnName + ElementsName.DoB.ToDescription().Replace(" ", "_"), dob);
                        rowTitle.Add(columnName + ElementsName.DoB.ToDescription());
                        break;

                    case ElementsName.Email:
                        var email = player?.Contact?.Email ?? string.Empty;
                        result.Add(columnName + ElementsName.Email.ToDescription().Replace(" ", "_"), email);
                        rowTitle.Add(columnName + ElementsName.Email.ToDescription());
                        break;

                    case ElementsName.Gender:
                        var gender = player?.Gender != null ? player.Gender.ToDescription() : string.Empty;
                        result.Add(columnName + ElementsName.Gender.ToDescription().Replace(" ", "_"), gender);
                        rowTitle.Add(columnName + ElementsName.Gender.ToDescription());
                        break;

                    case ElementsName.PermissionPhotography:
                        var permissionPhotography = player?.Info != null ? player.Info.HasPermissionPhotography.HasValue ? (player.Info.HasPermissionPhotography.Value ? "Yes" : "No") : string.Empty : string.Empty;
                        result.Add(columnName + ElementsName.PermissionPhotography.ToDescription().Replace(" ", "_"), permissionPhotography);
                        rowTitle.Add(columnName + ElementsName.PermissionPhotography.ToDescription());
                        break;

                    case ElementsName.SchoolName:
                        var schoolName = player?.Info != null ? player.Info.SchoolName : string.Empty;
                        result.Add(columnName + ElementsName.SchoolName.ToDescription().Replace(" ", "_"), schoolName);
                        rowTitle.Add(columnName + ElementsName.SchoolName.ToDescription());
                        break;

                    case ElementsName.Homeroom:
                        var homeRoom = player?.Info != null ? player.Info.HomeRoom : string.Empty;
                        result.Add(columnName + ElementsName.Homeroom.ToDescription().Replace(" ", "_"), homeRoom);
                        rowTitle.Add(columnName + ElementsName.HomePhone.ToDescription());
                        break;

                    case ElementsName.HasAllergiesInfo:
                        var hasAllergiesInfo = player?.Info?.HasAllergies != null ? player.Info.HasAllergies.Value ? "Yes" : "No" : string.Empty;
                        result.Add(columnName + ElementsName.HasAllergiesInfo.ToDescription().Replace(" ", "_").Replace("?", ""), hasAllergiesInfo);
                        rowTitle.Add(columnName + ElementsName.HasAllergiesInfo.ToDescription());
                        break;

                    case ElementsName.AllergiesInfo:
                        var allergies = player?.Info != null ? player.Info.Allergies : string.Empty;
                        result.Add(columnName + ElementsName.AllergiesInfo.ToDescription().Replace(" ", "_"), allergies);
                        rowTitle.Add(columnName + ElementsName.AllergiesInfo.ToDescription());
                        break;

                    case ElementsName.HasMedicalConditions:
                        var hasMedicalConditions = player?.Info?.HasSpecialNeeds != null ? player.Info.HasSpecialNeeds.Value ? "Yes" : "No" : string.Empty;
                        result.Add(columnName + ElementsName.HasMedicalConditions.ToDescription().Replace(" ", "_").Replace("?", ""), hasMedicalConditions);
                        rowTitle.Add(columnName + ElementsName.HasMedicalConditions.ToDescription());
                        break;

                    case ElementsName.MedicalConditions:
                        var specialNeeds = player?.Info != null ? player.Info.SpecialNeeds : string.Empty;
                        result.Add(columnName + ElementsName.MedicalConditions.ToDescription().Replace(" ", "_"), specialNeeds);
                        rowTitle.Add(columnName + ElementsName.MedicalConditions.ToDescription());
                        break;

                    case ElementsName.SelfAdministerMedication:
                        var selfAdminister = player?.Info != null ? player.Info.SelfAdministerMedication.HasValue ? (player.Info.SelfAdministerMedication.Value ? "Yes" : "No") : string.Empty : string.Empty;
                        result.Add(columnName + ElementsName.SelfAdministerMedication.ToDescription().Replace(" ", "_"), selfAdminister);
                        rowTitle.Add(columnName + ElementsName.SelfAdministerMedication.ToDescription());
                        break;

                    case ElementsName.SelfAdministerMedicationDescription:
                        var selfAdministerDescrition = player?.Info != null ? player.Info.SelfAdministerMedicationDescription : string.Empty;
                        result.Add(columnName + ElementsName.SelfAdministerMedicationDescription.ToDescription().Replace(" ", "_"), selfAdministerDescrition);
                        rowTitle.Add(columnName + ElementsName.SelfAdministerMedicationDescription.ToDescription());
                        break;

                    case ElementsName.NutAllergy:
                        var nutAllergies = player?.Info != null ? player.Info.NutAllergy.HasValue ? (player.Info.NutAllergy.Value ? "Yes" : "No") : string.Empty : string.Empty;
                        result.Add(columnName + ElementsName.NutAllergy.ToDescription().Replace(" ", "_"), nutAllergies);
                        rowTitle.Add(columnName + ElementsName.NutAllergy.ToDescription());
                        break;

                    case ElementsName.NutAllergyDescription:
                        var nutAllergiesDescription = player?.Info != null ? player.Info.NutAllergyDescription : string.Empty;
                        result.Add(columnName + ElementsName.NutAllergyDescription.ToDescription().Replace(" ", "_"), nutAllergiesDescription);
                        rowTitle.Add(columnName + ElementsName.NutAllergyDescription.ToDescription());
                        break;

                    case ElementsName.DoctorName:
                        var doctorName = player?.Info != null ? player.Info.DoctorName : string.Empty;
                        result.Add(columnName + ElementsName.DoctorName.ToDescription().Replace(" ", "_"), doctorName);
                        rowTitle.Add(columnName + ElementsName.DoctorName.ToDescription());
                        break;

                    case ElementsName.DoctorPhone:
                        var doctorPhone = player?.Info != null ? PhoneNumberHelper.FormatPhoneNumber(player.Info.DoctorPhone) : string.Empty;
                        result.Add(columnName + ElementsName.DoctorPhone.ToDescription().Replace(" ", "_"), doctorPhone);
                        rowTitle.Add(columnName + ElementsName.DoctorPhone.ToDescription());
                        break;

                    case ElementsName.DoctorLocation:
                        var doctorLocation = player?.Info != null ? player.Info.DoctorLocation : string.Empty;
                        result.Add(columnName + ElementsName.DoctorLocation.ToDescription().Replace(" ", "_"), doctorLocation);
                        rowTitle.Add(columnName + ElementsName.DoctorLocation.ToDescription());
                        break;
                }
            }

        }


        private static void SetParentInfoToJbForm(List<ElementsName> fields, List<PlayerProfile> players, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var columnName = isShare == false ? "_Parent_{0}sDash{1}" : "_Parent_{0} {1}";

            for (var i = 0; i < players.Count; i++)
            {
                var player = players[i];

                foreach (var field in fields)
                {
                    switch (field)
                    {
                        case ElementsName.ParentId:
                            var parentId = player != null ? player.Id.ToString() : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.ParentId.ToDescription().Replace(" ", "_")), parentId);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.ParentId.ToDescription()));
                            break;

                        case ElementsName.FirstName:
                            var firstName = player?.Contact != null ? player.Contact.FirstName : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.FirstName.ToDescription().Replace(" ", "_")), firstName);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.FirstName.ToDescription()));
                            break;

                        case ElementsName.LastName:
                            var lastName = player?.Contact != null ? player.Contact.LastName : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.LastName.ToDescription().Replace(" ", "_")), lastName);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.LastName.ToDescription()));
                            break;

                        case ElementsName.Email:
                            var email = player?.Contact != null ? player.Contact.Email : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Email.ToDescription().Replace(" ", "_")), email);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Email.ToDescription()));
                            break;

                        case ElementsName.Phone:
                            var primaryPhone = player?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(player.Contact.Phone) : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Phone.ToDescription().Replace(" ", "_")), primaryPhone);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Phone.ToDescription()));
                            break;

                        case ElementsName.Cell:
                            var alternatePhone = player?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(player.Contact.Cell) : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Cell.ToDescription().Replace(" ", "_")), alternatePhone);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Cell.ToDescription()));
                            break;

                        case ElementsName.Relationship:
                            var relationship = player?.Contact != null ? player.Contact.Relationship.ToDescription() : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Relationship.ToDescription().Replace(" ", "_")), relationship);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Relationship.ToDescription()));
                            break;

                        case ElementsName.AddressLine1:
                            var address1 = player?.Contact?.Address != null ? player.Contact.Address.Street : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.AddressLine1.ToDescription().Replace(" ", "_")), address1);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.AddressLine1.ToDescription()));
                            break;

                        case ElementsName.AddressLine2:
                            var address2 = player?.Contact?.Address != null ? player.Contact.Address.Street2 : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.AddressLine2.ToDescription().Replace(" ", "_")), address2);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.AddressLine2.ToDescription()));
                            break;

                        case ElementsName.Country:
                            var country = player?.Contact?.Address != null ? player.Contact.Address.Country : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Country.ToDescription().Replace(" ", "_")), country);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Country.ToDescription()));
                            break;

                        case ElementsName.City:
                            var city = player?.Contact?.Address != null ? player.Contact.Address.City : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.City.ToDescription().Replace(" ", "_")), city);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.City.ToDescription()));
                            break;

                        case ElementsName.State:
                            var state = player?.Contact?.Address != null ? player.Contact.Address.State : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.State.ToDescription().Replace(" ", "_")), state);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.State.ToDescription()));
                            break;

                        case ElementsName.Zip:
                            var zip = player?.Contact?.Address != null ? player.Contact.Address.Zip : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Zip.ToDescription().Replace(" ", "_")), zip);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Zip.ToDescription()));
                            break;

                        case ElementsName.Employer:
                            var employer = player?.Contact != null ? player.Contact.Employer : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Employer.ToDescription().Replace(" ", "_")), employer);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Employer.ToDescription()));
                            break;

                        case ElementsName.Occupation:
                            var occupation = player?.Contact != null ? player.Contact.Occupation : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Occupation.ToDescription().Replace(" ", "_")), occupation);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Occupation.ToDescription()));
                            break;
                    }
                }
            }

        }


        private static void SetEmergencyContactInfoToJbForm(List<ElementsName> fields, List<FamilyContact> families, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var columnName = isShare == false ? "_Emergency_{0}sDash{1}" : "_Emergency_{0} {1}";

            for (var i = 0; i < families.Count; i++)
            {
                var family = families[i];

                foreach (var field in fields)
                {
                    switch (field)
                    {
                        case ElementsName.FirstName:
                            var firstName = family?.Contact != null ? family.Contact.FirstName : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.FirstName.ToDescription().Replace(" ", "_")), firstName);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.FirstName.ToDescription()));
                            break;

                        case ElementsName.LastName:
                            var lastName = family?.Contact != null ? family.Contact.LastName : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.LastName.ToDescription().Replace(" ", "_")), lastName);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.LastName.ToDescription()));
                            break;

                        case ElementsName.Phone:
                            var primaryPhone = family?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(family.Contact.Phone) : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Phone.ToDescription().Replace(" ", "_")), primaryPhone);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Phone.ToDescription()));
                            break;

                        case ElementsName.Cell:
                            var alternatePhone = family?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(family.Contact.Cell) : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Cell.ToDescription().Replace(" ", "_")), alternatePhone);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Cell.ToDescription()));
                            break;

                        case ElementsName.Email:
                            var email = family?.Contact != null ? family.Contact.Email : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Email.ToDescription().Replace(" ", "_")), email);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Email.ToDescription()));
                            break;

                        case ElementsName.Relationship:
                            var relationShip = family?.Relationship != null ? family.Relationship.ToDescription() : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Relationship.ToDescription().Replace(" ", "_")), relationShip);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Relationship.ToDescription()));
                            break;
                    }
                }
            }
        }

        private static void SetAuthorizeAdultInfoToJbForm(List<ElementsName> fields, List<FamilyContact> families, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var columnName = isShare == false ? "_Authorized_pickup_{0}sDash{1}" : "_Authorized_pickup_{0} {1}";

            for (var i = 0; i < families.Count; i++)
            {
                var family = families[i];

                foreach (var field in fields)
                {
                    switch (field)
                    {
                        case ElementsName.FirstName:
                            var firstName = family?.Contact != null ? family.Contact.FirstName : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.FirstName.ToDescription().Replace(" ", "_")), firstName);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.FirstName.ToDescription()));
                            break;

                        case ElementsName.LastName:
                            var lastName = family?.Contact != null ? family.Contact.LastName : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.LastName.ToDescription().Replace(" ", "_")), lastName);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.LastName.ToDescription()));
                            break;

                        case ElementsName.Phone:
                            var primaryPhone = family?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(family.Contact.Phone) : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Phone.ToDescription().Replace(" ", "_")), primaryPhone);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Phone.ToDescription()));
                            break;

                        case ElementsName.Cell:
                            var alternatePhone = family?.Contact != null ? PhoneNumberHelper.FormatPhoneNumber(family.Contact.Cell) : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Cell.ToDescription().Replace(" ", "_")), alternatePhone);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Cell.ToDescription()));
                            break;

                        case ElementsName.Relationship:
                            var relationShip = family?.Relationship != null ? family.Relationship.ToDescription() : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.Relationship.ToDescription().Replace(" ", "_")), relationShip);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.Relationship.ToDescription()));
                            break;

                        case ElementsName.IsEmergencyContact:
                            var isEmergencyContact = family != null ? (family.IsEmergencyContact ? "Yes" : "No") : string.Empty;
                            result.Add(string.Format(columnName, i + 1, ElementsName.IsEmergencyContact.ToDescription().Replace(" ", "_")), isEmergencyContact);
                            rowTitle.Add(string.Format(columnName, i + 1, ElementsName.IsEmergencyContact.ToDescription()));
                            break;
                    }
                }

            }

        }

        private static void SetInsuranceInfoToJbForm(List<ElementsName> fields, List<Family> families, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var columnName = isShare == false ? "_InsurancesDash{0}" : "_Insurance {0}";

            foreach (var family in families)
            {
                foreach (var field in fields)
                {
                    switch (field)
                    {
                        case ElementsName.CompanyName:
                            var companyName = family != null ? family.InsuranceCompanyName : string.Empty;
                            result.Add(string.Format(columnName, ElementsName.CompanyName.ToDescription().Replace(" ", "_")), companyName);
                            rowTitle.Add(string.Format(columnName, ElementsName.CompanyName.ToDescription()));
                            break;

                        case ElementsName.CompanyPhone:
                            var compantPhone = family != null ? PhoneNumberHelper.FormatPhoneNumber(family.InsurancePhone) : string.Empty;
                            result.Add(string.Format(columnName, ElementsName.CompanyPhone.ToDescription().Replace(" ", "_")), compantPhone);
                            rowTitle.Add(string.Format(columnName, ElementsName.CompanyPhone.ToDescription()));
                            break;

                        case ElementsName.PolicyNumber:
                            var policyNumber = family != null ? family.InsurancePolicyNumber : string.Empty;
                            result.Add(string.Format(columnName, ElementsName.PolicyNumber.ToDescription().Replace(" ", "_")), policyNumber);
                            rowTitle.Add(string.Format(columnName, ElementsName.PolicyNumber.ToDescription()));
                            break;
                    }
                }
            }
        }

        private void SetMoreInfoSectionToJbForm(List<IJbBaseElement> fields, OrderItem orderItem, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var program = orderItem.ProgramSchedule.Program;

            string columnName = isShare == false ? "_More_InfosDash" : "_More_Info ";

            foreach (var element in fields)
            {
                switch (element.Name)
                {
                    case "ProgramInstructor":
                        {
                            var instructor = string.Empty;
                            if (program.Instructors != null && program.Instructors.Any(c => c.Contact != null))
                            {
                                instructor = string.Join(", ", program.Instructors.Select(c => string.Format(Constants.F_FullName, c.Contact.FirstName, c.Contact.LastName)));
                            }
                            result.Add(columnName + element.Name.Replace(" ", "_"), instructor);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "SchoolId":
                        {
                            var schoolId = orderItem.Order.Club.Id;

                            var value = schoolId;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "SchoolName":
                        {
                            var schoolName = orderItem.Order.Club.Name;

                            var value = schoolName;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramRoomNumber":
                        {
                            var roomNumber = program.Room;

                            var value = roomNumber;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramDayOfWeek":
                        {
                            var dayOfWeek = string.Join(", ", _programBusiness.GetClassDays(program).Select(d => d.DayOfWeek).ToList());

                            var value = dayOfWeek;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "UserEmail":
                        {
                            var value = orderItem.Order.User.UserName;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "Tuition":
                        {
                            var value = CurrencyHelper.FormatCurrencyWithPenny(orderItem.EntryFee, orderItem.Order.Club.Currency);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "TuitionName":
                        {
                            var entryFee = orderItem.OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee);
                            var value = entryFee != null ? entryFee.Name : string.Empty; 

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ScheduleStartEnd":
                        {
                            var value = orderItem.ProgramTypeCategory != ProgramTypeCategory.ChessTournament
                                ? $"{orderItem.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma)} - {orderItem.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma)}"
                                : $"{orderItem.Start.Value.ToString(Constants.DateTime_Comma)} - {orderItem.End.Value.ToString(Constants.DateTime_Comma)}";

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ScheduleStartDate":
                        {
                            var value = orderItem.ProgramTypeCategory != ProgramTypeCategory.ChessTournament
                                ? orderItem.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma)
                                : orderItem.Start.Value.ToString(Constants.DateTime_Comma);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ScheduleEndDate":
                        {
                            var value = orderItem.ProgramTypeCategory != ProgramTypeCategory.ChessTournament
                                ? orderItem.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma)
                                : orderItem.End.Value.ToString(Constants.DateTime_Comma);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "DiscountTotal":
                        {
                            var value = CurrencyHelper.FormatCurrencyWithPenny(
                                orderItem.OrderChargeDiscounts.Where(m => !m.IsDeleted && m.OrderItemId == orderItem.Id
                                                                          && m.Subcategory == ChargeDiscountSubcategory
                                                                              .Discount).Sum(m => m.Amount), orderItem.Order.Club.Currency);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "OrderDate":
                        {
                            var value = DateTimeHelper
                                .ConvertUtcDateTimeToLocal(orderItem.Order.CompleteDate, orderItem.Order.Club.TimeZone)
                                .ToString(Constants.DateTime_Comma);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "OrderAmount":
                        {
                            var value = CurrencyHelper.FormatCurrencyWithPenny(orderItem.TotalAmount, orderItem.Order.Club.Currency);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "OrderPaidAmount":
                        {
                            var value = CurrencyHelper.FormatCurrencyWithPenny(orderItem.PaidAmount, orderItem.Order.Club.Currency);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "Balance":
                        {
                            var itemBalance = _accountingBusiness.OrderItemBalance(orderItem);
                            var value = CurrencyHelper.FormatCurrencyWithPenny(itemBalance, orderItem.Order.Club.Currency);

                            if (orderItem.ItemStatusReason == OrderItemStatusReasons.transferOut)
                            {
                                value = "-";
                            }
                            else if (orderItem.ItemStatusReason == OrderItemStatusReasons.canceled && itemBalance >= 0)
                            {
                                value = "-";
                            }

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "BalanceStatus":
                        {
                            var value = string.Empty;

                            if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid)
                            {
                                if (orderItem.PaidAmount >= orderItem.TotalAmount)
                                {
                                    value = BalanceStatus.Paid.ToDescription();
                                }
                                else
                                {
                                    value = BalanceStatus.PastDue.ToDescription();
                                }
                            }
                            else if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
                            {
                                var date = _clubBusiness.GetClubDateTime(orderItem.Order.ClubId, DateTime.UtcNow);

                                if (orderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && orderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription && (orderItem.ItemStatusReason == OrderItemStatusReasons.canceled || orderItem.ItemStatusReason == OrderItemStatusReasons.transferOut))
                                {
                                    value = string.Empty;
                                }
                                else if (orderItem.PaidAmount >= orderItem.TotalAmount)
                                {
                                    value = BalanceStatus.Paid.ToDescription();
                                }
                                else if (orderItem.Installments.Any(i => !i.IsDeleted && i.Status != OrderStatusCategories.canceled && i.Type != InstallmentType.AddOn && i.InstallmentDate < date && ((i.PaidAmount.HasValue && i.Amount > i.PaidAmount) || i.PaidAmount == null)))
                                {
                                    value = BalanceStatus.PastDue.ToDescription();
                                }
                                else
                                {
                                    value = BalanceStatus.Current.ToDescription();
                                }
                            }

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ConfirmationId":
                        {
                            var value = orderItem.Order.ConfirmationId;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramStart":
                        {
                            var value = orderItem.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramEnd":
                        {
                            var value = orderItem.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramLocation":
                        {
                            var value = (!string.IsNullOrEmpty(orderItem.ProgramSchedule.Program.ClubLocation.Name))
                                ? $"{orderItem.ProgramSchedule.Program.ClubLocation.Name},{orderItem.ProgramSchedule.Program.ClubLocation.PostalAddress.Address}"
                                : orderItem.ProgramSchedule.Program.ClubLocation.PostalAddress.Address;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramLocationName":
                        {
                            var value = (!string.IsNullOrEmpty(orderItem.ProgramSchedule.Program.ClubLocation.Name))
                                ? orderItem.ProgramSchedule.Program.ClubLocation.Name
                                : "N/A";

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramType":
                        {
                            var value = orderItem.ProgramTypeCategory.ToDescription();

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "OrderType":
                        {
                            var value = orderItem.Order.OrderMode.ToString();

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "Charges":
                        {
                            string value;

                            var charge = orderItem.GetOrderChargeDiscounts().Where(
                                c =>
                                    c.Category != ChargeDiscountCategory.EntryFee &&
                                    c.Name.ToLower().Trim() == element.Title.ToLower().Trim());
                            if (charge.Any())
                            {
                                var chargesAmount = orderItem.GetOrderChargeDiscounts().Where(
                                        c =>
                                            c.Category != ChargeDiscountCategory.EntryFee &&
                                            c.Name.ToLower().Trim() == element.Title.ToLower().Trim())
                                    .Sum(c => c.Amount);

                                value = CurrencyHelper.FormatCurrencyWithPenny(chargesAmount, orderItem.Order.Club.Currency);
                            }
                            else
                            {
                                value = "-";
                            }

                            result.Add(columnName + element.Name.Replace(" ", "_") + element.ElementId, value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "PaymentMethod":
                        {
                            var value = (orderItem.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success)
                                ? string.Join(", ", orderItem.TransactionActivities.Where(t => t.TransactionStatus == TransactionStatus.Success).Select(p => p.PaymentDetail.PaymentMethod.ToDescription()))
                                : Constants.S_Dash);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "PaymentDate":
                        {
                            var value = (orderItem.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success)
                                ? string.Join(", ", orderItem.TransactionActivities.Where(t => t.TransactionStatus == TransactionStatus.Success).Select(p => p.TransactionDate.ToString("MM/dd/yyyy")))
                                : Constants.S_Dash);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ExtraServices":
                        {
                            var strServices = new List<string>();
                            var exteraServices = (orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Lunch || c.Category == ChargeDiscountCategory.DayCare || c.Category == ChargeDiscountCategory.CustomCharge));

                            if (exteraServices != null)
                            {
                                foreach (var charge in exteraServices)
                                {
                                    var chargeTitle =
                                        $"{charge.Name} ({CurrencyHelper.FormatCurrencyWithPenny(charge.Amount, orderItem.Order.Club.Currency)})";
                                    strServices.Add(chargeTitle);
                                }
                            }

                            var value = strServices != null ? string.Join(", ", strServices) : Constants.S_Dash;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "PaymentStatus":
                        {
                            string value = string.Empty;

                            if (orderItem.PaidAmount == 0)
                            {
                                value = "Unpaid";
                            }

                            else switch (orderItem.ItemStatus)
                                {
                                    case OrderItemStatusCategories.completed when orderItem.PaidAmount >= orderItem.TotalAmount:
                                        value = "Paid";
                                        break;
                                    case OrderItemStatusCategories.completed:
                                        if (orderItem.PaidAmount < orderItem.TotalAmount)
                                        {
                                            value = "Partially Paid";
                                        }
                                        break;
                                }

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "PaymentMethodOrder":
                        {
                            var value = orderItem.Order.PaymentMethod.HasValue ? orderItem.Order.PaymentMethod.ToDescription() : Constants.S_Dash;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "TransactionId":
                        {
                            var value = (orderItem.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success)
                                ? string.Join(", ", orderItem.TransactionActivities.Where(t => t.TransactionStatus == TransactionStatus.Success).Select(p => !string.IsNullOrEmpty(p.PaymentDetail.TransactionId) ? p.PaymentDetail.TransactionId : !string.IsNullOrEmpty(p.CheckId) ? p.CheckId : "-").ToList())
                                : Constants.S_Dash);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "SchoolDistrict":
                        {
                            var value = orderItem.Order.Club.District != null ? orderItem.Order.Club.District.Name : string.Empty;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "SeasonName":
                        {
                            var value = orderItem.Season.Title;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ScheduleId":
                        {
                            var scheduleId = orderItem.ProgramSchedule.Id;

                            var value = scheduleId;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ScheduleTitle":
                        {
                            var title = orderItem.ProgramSchedule.Title;

                            var value = title;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "DesiredStartDate":
                        {
                            var value = orderItem.DesiredStartDate?.ToString(Constants.DateTime_Comma);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ClassDays":
                        {
                            var value = orderItem.Attributes != null ? string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(orderItem.Attributes.WeekDays, AbbreviatedDayNameMode.TwoLetter)) : string.Empty;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "OrganizationActiveType":
                        {
                            var value = orderItem.Order.Club.IsInActive ? "No" : "Yes";

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "RegistrationId":
                        {
                            var value = orderItem.Id.ToString();

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "Discounts_Coupons":
                        {
                            var value = string.Join(", ", orderItem.GetOrderChargeDiscounts()
                                .Where(o =>
                                    o.Subcategory == ChargeDiscountSubcategory.Discount)
                                .Select(o => o.Name));

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "CancellationEffectiveDate":
                        {
                            var value = orderItem.Attributes.CancelEffectiveDate.HasValue ? orderItem.Attributes.CancelEffectiveDate.Value.ToString(Constants.DateTime_Comma) : string.Empty;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "TransferEffectiveDate":
                        {
                            var value = orderItem.Attributes.TransferEffectiveDate.HasValue ? orderItem.Attributes.TransferEffectiveDate.Value.ToString(Constants.DateTime_Comma) : string.Empty;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "OrderStatus":
                        {
                            var value = orderItem.ItemStatusReason.ToDescription();

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "DropinDates":
                        {
                            var value = orderItem.Mode == OrderItemMode.DropIn ? string.Join(", ", orderItem.OrderSessions.Where(o => o.ProgramSession != null).Select(o => o.ProgramSession.StartDateTime.ToString("MM/dd/yy"))) : string.Empty;

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramStartDate":
                        {
                            var value = orderItem.ProgramSchedule.Program.ProgramSchedules.First().StartDate.ToString(Constants.DateTime_Comma);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ProgramEndDate":
                        {
                            var value = orderItem.ProgramSchedule.Program.ProgramSchedules.Last().EndDate.ToString(Constants.DateTime_Comma);

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ScheduleStartTime":
                        {
                            string value;

                            if (orderItem.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both)
                            {
                                var beforeScheduleTitle = orderItem.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.AM).Title;
                                var afterScheduleTitle = orderItem.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.PM).Title;

                                value = $"{beforeScheduleTitle}: ({_reportBusiness.GetDayOfWeekWithStartTime(((ScheduleAfterBeforeCareAttribute)orderItem.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.AM).Attributes).Days)}) - " +
                                    $"{afterScheduleTitle}: ({_reportBusiness.GetDayOfWeekWithStartTime(((ScheduleAfterBeforeCareAttribute)orderItem.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.PM).Attributes).Days)})";
                            }
                            else
                            {
                                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                {
                                    value = _reportBusiness.GetDayOfWeekWithStartTime(((ScheduleAfterBeforeCareAttribute)orderItem.ProgramSchedule.Attributes).Days);
                                }
                                else if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    value = _reportBusiness.GetDayOfWeekWithStartTime(((ScheduleSubscriptionAttribute)orderItem.ProgramSchedule.Attributes).Days);
                                }
                                else
                                {
                                    value = _reportBusiness.GetDayOfWeekWithStartTime(((ScheduleAttribute)orderItem.ProgramSchedule.Attributes).Days);
                                }
                            }

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                    case "ScheduleEndTime":
                        {
                            string value;

                            if (orderItem.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both)
                            {
                                var beforeScheduleTitle = orderItem.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.AM).Title;
                                var afterScheduleTitle = orderItem.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.PM).Title;

                                value = $"{beforeScheduleTitle} ({_reportBusiness.GetDayOfWeekWithEndTime(((ScheduleAfterBeforeCareAttribute)orderItem.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.AM).Attributes).Days)}) - " +
                                    $"{afterScheduleTitle} ({_reportBusiness.GetDayOfWeekWithEndTime(((ScheduleAfterBeforeCareAttribute)orderItem.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.PM).Attributes).Days)})";
                            }
                            else
                            {
                                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                {
                                    value = _reportBusiness.GetDayOfWeekWithEndTime(((ScheduleAfterBeforeCareAttribute)orderItem.ProgramSchedule.Attributes).Days);
                                }
                                else if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    value = _reportBusiness.GetDayOfWeekWithEndTime(((ScheduleSubscriptionAttribute)orderItem.ProgramSchedule.Attributes).Days);
                                }
                                else
                                {
                                    value = _reportBusiness.GetDayOfWeekWithEndTime(((ScheduleAttribute)orderItem.ProgramSchedule.Attributes).Days);
                                }
                            }

                            result.Add(columnName + element.Name.Replace(" ", "_"), value);
                            rowTitle.Add(columnName + element.Title);
                            break;
                        }
                }
            }
        }

        private void SetDiscountCouponSectionToJbForm(List<IJbBaseElement> fields, OrderItem orderItem, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var columnName = isShare == false ? "_Discounts_and_couponssDash" : "_Discounts_and_coupons ";

            foreach (var element in fields)
            {
                var couponsDiscounts = orderItem.GetOrderChargeDiscounts()
                    .Where(c =>
                        c.Subcategory == ChargeDiscountSubcategory.Discount &&
                        c.Name.ToLower().Trim() == element.Title.Replace("(deleted)", "").ToLower().Trim());

                var orderChargeDiscounts = couponsDiscounts as OrderChargeDiscount[] ?? couponsDiscounts.ToArray();

                var value = orderChargeDiscounts.Any() ? string.Join(", ", orderChargeDiscounts.Select(c => c.Amount)) : "";

                result.Add(columnName + element.Name.Replace(" ", "_") + element.ElementId, value);
                rowTitle.Add(columnName + element.Title);

            }
        }

        private void SetChargesToJbForm(List<IJbBaseElement> fields, OrderItem orderItem, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var columnName = isShare == false ? "_ChargessDash" : "_Charges ";

            foreach (var element in fields)
            {
                var charges = orderItem.GetOrderChargeDiscounts()
                    .Where(c =>
                        c.Category != ChargeDiscountCategory.EntryFee &&
                        ( element.Title == "Surcharge" ? c.Category == ChargeDiscountCategory.Surcharge :
                            element.Title == "Convenience fee" ? c.Category == ChargeDiscountCategory.Convenience :
                            element.Title == "Partner surcharge" ? c.Category == ChargeDiscountCategory.PartnerSurcharge :
                            element.Title == "Partner convenience fee" ? c.Category == ChargeDiscountCategory.Convenience
                        : c.Name.ToLower().Trim() == element.Title.Replace("(deleted)", "").ToLower().Trim())).ToList();

                var value = charges.Any() ? CurrencyHelper.FormatCurrencyWithPenny(charges.Sum(c => c.Amount), orderItem.Order.Club.Currency) : "";

                result.Add(columnName + element.Name.Replace(" ", "_") + element.ElementId, value);
                rowTitle.Add(columnName + element.Title);
            }
        }

        private void SetChessTournamentSectionToJbForm(List<IJbBaseElement> fields, OrderItem orderItem, Dictionary<string, object> result, List<string> rowTitle, bool isShare)
        {
            var columnName = isShare == false ? "_Chess_tournament_infosDash" : "_Chess_tournament_info ";

            foreach (var element in fields)
            {
                switch (element.Name)
                {
                    case "SectionName":
                        {
                            var value = orderItem.OrderItemChessId.HasValue ? orderItem.OrderItemChess.Section : string.Empty;

                            result.Add(columnName + element.Name, value);
                            rowTitle.Add(columnName + element.Title);
                        }
                        break;
                    case "ScheduleName":
                        {
                            var value = orderItem.OrderItemChessId.HasValue ? orderItem.OrderItemChess.Schedule : string.Empty;

                            result.Add(columnName + element.Name, value);
                            rowTitle.Add(columnName + element.Title);
                        }
                        break;
                }
            }
        }

        private Dictionary<SectionsName, List<ElementsName>> GetSectionsJbForm(JbForm jbForm)
        {
            var sections = new Dictionary<SectionsName, List<ElementsName>>();

            foreach (var section in jbForm.Elements)
            {
                if (((JbSection)section).Elements.Any())
                {
                    if (section.Name == SectionsName.MoreInfoSection.ToString() || section.Name == SectionsName.DiscountCouponSection.ToString() || section.Name == SectionsName.ChargeSection.ToString() || section.Name == SectionsName.ChessTournamentSection.ToString())
                    {
                        continue;
                    }
                    var sectionName = (SectionsName)Enum.Parse(typeof(SectionsName), section.Name);
                    var sectionELements = ((JbSection)section).Elements.Select(e => (ElementsName)Enum.Parse(typeof(ElementsName), e.Name.ToString())).ToList();

                    sections.Add(sectionName, sectionELements);
                }
            }

            return sections;
        }

        public JbForm RemoveUncheckedElementsJbForm(JbForm jbForm)
        {
            foreach (var section in jbForm.Elements)
            {
                ((JbSection)section).Elements.RemoveAll(e => e is JbCheckBox box && !box.Value);
            }

            return jbForm;
        }

        #endregion

        public JbSection SetAccessControllJbSection(JbSection jbSection, int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            if (club.IsPartner)
            {
            }
            else if (club.IsSchool)
            {
                jbSection.Elements.RemoveAll(e => e.Name == "SeasonName");
                jbSection.Elements.RemoveAll(e => e.Name == "OrganizationActiveType");
            }
            else if (club.IsProvider)
            {
                jbSection.Elements.RemoveAll(e => e.Name == "SchoolDistrict");
                jbSection.Elements.RemoveAll(e => e.Name == "SeasonName");
                jbSection.Elements.RemoveAll(e => e.Name == "OrganizationActiveType");
            }
            else if (!club.PartnerId.HasValue)
            {
                jbSection.Elements.RemoveAll(e => e.Name == "SchoolDistrict");
                jbSection.Elements.RemoveAll(e => e.Name == "SeasonName");
                jbSection.Elements.RemoveAll(e => e.Name == "OrganizationActiveType");

            }


            return jbSection;
        }
    }
}
