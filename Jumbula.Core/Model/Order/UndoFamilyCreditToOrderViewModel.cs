﻿using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Core.Model.Order
{
    public class UndoFamilyCreditToOrderViewModel
    {
        public Common.Enums.CurrencyCodes Currency { get; set; }

        public decimal FamilyCredit { get; set; }

        public decimal TransactionAmount { get; set; }

        public decimal? ReturnedAmount { get; set; }

        public long TransactionId { get; set; }

        public long orderItemId { get; set; }

        public OrderInformationViewModel OrderInfo { get; set; }

        public TransactionActivitiesViewModel TransactionActivity { get; set; }

        public decimal MaximumAmount { get; set; }

    }
}
