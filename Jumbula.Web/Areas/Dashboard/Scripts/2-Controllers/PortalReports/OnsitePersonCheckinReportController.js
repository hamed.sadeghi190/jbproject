﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('OnsitePersonCheckinReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};
        var isBegin = false;
        var userIds = [];

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.roleType = [{ Value: -1, Title: "All" }, { Value: 1, Title: "Onsite coordinator" }, { Value: 2, Title: "Onsite support manager" }];
        $scope.filter.roleType = -1;

        $scope.Model.eventType = [{ Value: -1, Title: "All" }, { Value: 1, Title: "Check in" }, { Value: 2, Title: "Check out" }];
        $scope.filter.eventType = -1;

        $scope.filter.printMode = false;

        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {
            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            dataInfo = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $http.get('/Dashboard/PortalReport/GetOnsitePersonClub', { params: { roleType: $scope.filter.roleType } }).success(function (data, status, headers, config) {
            $scope.Model.persons = data.Result;

            if ($scope.Model.persons.length != 0) {
                $scope.Model.persons.unshift({ Value: -1, Title: "All" });
                $scope.filter.personId = $scope.Model.persons[0].Value;

            }

        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });


        var model = $scope.Model;

        var prepareGrid = function () {

            isBegin = true;

            $scope.OnsitePersonCheckinDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetOnsitePersonCheckinReportInfo",
                        dataType: "json",
                        type: "POST",
                        data: function () {
                            return {
                                startDate: $scope.filter.startDate,
                                endDate: $scope.filter.endDate,
                                eventType: $scope.filter.eventType,
                                userIds: userIds,
                                printMode: $scope.filter.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: false,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.OnsitePersonCheckinGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Onsite person checkin).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Onsite person checkin - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.OnsitePersonCheckinDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#OnsitePersonCheckinGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                
                sortable: true,
                columns: [

                    {
                        field: "FullName",
                        title: "Name",
                        width: "140px"
                    }, {
                        field: "Date",
                        title: "Date",
                        width: "140px"
                    }, {
                        field: "Address",
                        title: "Address",
                        width: "250px",
                        template: '<span style="white-space: pre-line" > #=Address# </span>'
                    }, {
                        field: "Type",
                        title: "Type",
                        width: "120px"
                    }
                ]
            }
        }


        $scope.search = function () {
            if ($scope.filter.personId == -1) {
                userIds = []
                for (var i = 1; i < $scope.Model.persons.length; i++) {
                    userIds.push($scope.Model.persons[i].Value);
                }
            }
            else {
                userIds = []
                userIds.push($scope.filter.personId);
            }

            if (!isBegin) {
                prepareGrid();
            } else {
                $scope.filter.printMode = false;
                $("#OnsitePersonCheckinGrid").data("kendoGrid").dataSource.read();
            }

        };

        $scope.clearFilters = function () {
        }

        $scope.getOnsitePersons = function () {
            $http.get('/Dashboard/PortalReport/GetOnsitePersonClub', { params: { roleType: $scope.filter.roleType } }).success(function (data, status, headers, config) {
                $scope.Model.persons = data.Result;

                if ($scope.Model.persons.length != 0) {
                    $scope.filter.personId = $scope.Model.persons[0].Value;
                }

            }).error(function (data, status, headers, config) {
                jbToast.error(data);
            });
        }
    }
    ]);

})();