﻿//<![CDATA[  


var msg = $("#JoinMessage");
function AjaxSuccess(data, status) {
    switch (data.Status) {
        case "Success": {
            window.location.reload();
            break;
        }
        case "UserExist": {
            var $target = $("#mainPopUp");
            $target.removeClass($target.attr("data-class"));
            $target.attr("data-class", "dupilicateEmail");
            $target.addClass('dupilicateEmail');

            $target.html("<div style='width:100%; padding:20px 0px; text-align: center;'><img src='/Images/loading-small.gif' /><span style='margin-left:20px; font-size:18px;'>Loading...</span></div>");
            $target.load("@Url.Action("DuplicateEmail", "Account")" + "?username=" +data.Username+"&returnUrl="+window.location.pathname, function (data, status, xhr) {
                //$("form", this).validate({ onkeyup: false });
            });

            break;
        }
        case "Error": {
            msg.html(data.Message);
            $('.loading').button('reset');
            $.get(@Url.Action("LoadCaptcha", "Account"),function(data){
            });

            break;
        }
        case "Captcha": {
            $('#@captcha.BuildInfo.ImageElementId').attr('src', data.Captcha.@captcha.BuildInfo.ImageElementId);
            $('#@captcha.BuildInfo.TokenElementId').attr('value', data.Captcha.@captcha.BuildInfo.TokenElementId);
            $('#@captcha.BuildInfo.InputElementId').attr('value', '');

            msg.html(data.Message);
            $('.loading').button('reset');
               
            break;
        }
    }
}

function AjaxFailure() {
    debugger;
    $('#captcha').html(data.captcha);
    $('a[href="#CaptchaImage"]').trigger("click");
    $('.loading').button('reset');
    msg.html("Error in establish connection with server.");        
}   
//]]>