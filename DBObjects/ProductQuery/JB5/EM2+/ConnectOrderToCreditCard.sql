
alter TABLE [dbo].[UserCreditCards] add [IsDefault] [bit]  NULL
go
update dbo.UserCreditCards set IsDefault=0
go
ALTER TABLE [dbo].[UserCreditCards] ADD  DEFAULT ((0)) FOR [IsDefault]
GO
alter table dbo.[UserCreditCards] alter column isdefault [bit] not null
go
alter TABLE [dbo].[UserCreditCards] add	[Address_Id] [int] NULL
go
ALTER TABLE [dbo].[UserCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserCreditCards_dbo.PostalAddresses_Address_Id] FOREIGN KEY([Address_Id])
REFERENCES [dbo].[PostalAddresses] ([Id])
GO

ALTER TABLE [dbo].[UserCreditCards] CHECK CONSTRAINT [FK_dbo.UserCreditCards_dbo.PostalAddresses_Address_Id]
GO

alter TABLE [dbo].[PaymentDetails] add [CreditCardId] [int] NULL
go
ALTER TABLE [dbo].[PaymentDetails]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PaymentDetails_dbo.UserCreditCards_CreditCardId] FOREIGN KEY([CreditCardId])
REFERENCES [dbo].[UserCreditCards] ([Id])
GO

ALTER TABLE [dbo].[PaymentDetails] CHECK CONSTRAINT [FK_dbo.PaymentDetails_dbo.UserCreditCards_CreditCardId]
GO

alter TABLE [dbo].[OrderItems] add [CreditCardId] [int] NULL
 
 GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderItems_dbo.UserCreditCards_CreditCardId] FOREIGN KEY([CreditCardId])
REFERENCES [dbo].[UserCreditCards] ([Id])
GO

ALTER TABLE [dbo].[OrderItems] CHECK CONSTRAINT [FK_dbo.OrderItems_dbo.UserCreditCards_CreditCardId]
GO










