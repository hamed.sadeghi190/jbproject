﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('reportRunController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', 'jbToast', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, jbToast, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.programs = [];
        $scope.Model = [];
        $scope.Model.HasHeader = false;
        $scope.Model.SingleProgramMode = false;
        var isBegin = false;

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.ReportId = $stateParams.reportId;
        $scope.Model.ReportName = $stateParams.reportName;
        $scope.Model.reportType = $stateParams.reportType;
        
        $scope.Model.Filters = {};
        $scope.Model.Filters.Status = "regular";

        if ($stateParams.reportType == 'Parent') {

            if ($scope.Model.IsAllPrograms == 'false' && $scope.Model._SelectedPrograms != null) {
                
                $scope.Model.Filters._SelectedPrograms = [];
                for (var i = 0; i < $scope.Model._SelectedPrograms.length; i++) {

                    var scheduleIds = $scope.Model._SelectedPrograms[i];
                    if (scheduleIds.length > 0) {
                        for (var j = 0; j < scheduleIds.length; j++) {
                            $scope.Model.Filters._SelectedPrograms.push(scheduleIds[j]);
                        }
                    } else {
                        $scope.Model.Filters._SelectedPrograms.push(scheduleIds);
                    }

                }
                $scope.Model._SelectedPrograms = $scope.Model.Filters._SelectedPrograms;
            }
        }

        $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model, reportType: $stateParams.reportType })
            .success(function (data, status, headers, config) {
                $scope.Model.Filters.Tuition = "";
                $scope.Model.Filters.Tuitions = data.Tuitions;
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            }).error(function (data, status, headers, config) {
            });


        $scope.getReportHeader = function (programId) {
            if ($stateParams.reportType == 'Registration') {
                $http.get(baseUrl + '/GetReportHeader', { params: { reportId: $scope.MC.stateParams.reportId, programId: programId } })
                    .success(function (data, status, headers, config) {
                        $scope.Model.Header = data;

                        var hasReportHeader = false;

                        if (data != null && data != 'undefined' && data != '') {
                            hasReportHeader = true;
                        }

                        $scope.Model.HasHeader = hasReportHeader
                    });
            } else {
                $http.get(baseUrl + '/GetCustomReportHeader', { params: { reportId: $scope.MC.stateParams.reportId, scheduleId: programId } })
                    .success(function (data, status, headers, config) {
                        $scope.Model.Header = data;

                        var hasReportHeader = false;

                        if (data != null && data != 'undefined' && data != '') {
                            hasReportHeader = true;
                        }

                        $scope.Model.HasHeader = hasReportHeader
                    });
            }
        }


        $scope.filterData = function () {

            setHeader();
            $scope.customReportGridOptions.dataSource.page(1);
            $("#customReportGrid").data("kendoGrid").dataSource.read();
            resizeGrid();
        }

        $scope.clearFilters = function () {
            //$scope.Model.Filters = {};
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.Status = "regular";
            $scope.Model.Filters.Tuition = "";
            $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model }).success(function (data, status, headers, config) {
                $scope.Model.Filters.Tuitions = data.Tuitions;
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            });

            setHeader();
            $scope.customReportGridOptions.dataSource.page(1);
            $("#customReportGrid").data("kendoGrid").dataSource.read();
            resizeGrid();

        }

        $scope.customReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
            {
                read: {
                    url: baseUrl + '/RunReport',
                    type: "POST",
                    data: function () {
                        $('#customReportGrid').data('kendoGrid').refresh();
                        return {
                            model: $scope.Model,
                        };
                    }
                },
            },
            schema: {
                total: "TotalCount",
                data: function (response) {

                    $scope.Titles = response.Titles;
                    return response.DataSource;
                },
            },
            serverPaging: true,
            pageSize: 100,
            batch: true,
        });

        $scope.saveToExcel = function () {

            var dataSource = $("#customReportGrid").data("kendoGrid");
            dataSource.columns.forEach(function (column) {
                column.title = column.title.replace("<br>", " \n");
            });

            cfpLoadingBar.start();
            dataSource.saveAsExcel();
        }


        var columns = [];
        $scope.oldColumns = [];

        $scope.customReportGridOptions = {
            excel: {
                allPages: true,
                fileName: $stateParams.reportName + '.xlsx',
                proxyURL: baseUrl + '/KendoExport',
            },
            excelExport: function (e) {

                if ($scope.Model.HasHeader) {

                    var rows = e.workbook.sheets[0].rows;

                    if ($scope.Model.IsAllPrograms == 'true' || ($scope.Model._SelectedPrograms != null && $scope.Model._SelectedPrograms != 'undefined' && $scope.Model._SelectedPrograms.length > 1)) {

                        rows.unshift({
                            cells: [{ value: "" }]
                        });
                        rows.unshift({
                            cells: [{ value: $scope.Model.Header.Date }]
                        });
                        rows.unshift({
                            cells: [{ value: $scope.Model.Header.SeasonName + ", " + $stateParams.reportName }]
                        });
                    }

                    if ($scope.Model._SelectedPrograms != null && $scope.Model._SelectedPrograms != 'undefined' && $scope.Model._SelectedPrograms.length == 1) {
                        rows.unshift({
                            cells: [{ value: "" }]
                        });
                        rows.unshift({
                            cells: [{ value: $scope.Model.Header.Date }]
                        });
                        rows.unshift({
                            cells: [{ value: "Room: " + $scope.Model.Header.RoomAssignment }]
                        });
                        rows.unshift({
                            cells: [{ value: $scope.Model.Header.Days }]
                        });
                        rows.unshift({
                            cells: [{ value: $scope.Model.Header.SeasonName + ": " + $scope.Model.Header.ProgramName }]
                        });
                    }
                }

                cfpLoadingBar.complete();
            },
            dataSource: $scope.customReportsDataSource,
            scrollable: true,
            resizable: true,
            sortable: true,
            columnMenu: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100
                ],
                buttonCount: 5
            },
            dataBound: function (e) {
                customReportService.initializeDoubleScroll(e);
                $scope.oldColumns = $("#customReportGrid").data("kendoGrid").columns;
                
                setHeader();

                resizeGrid();
            }
        }

        $(document).bind("ajaxComplete.changeColumns", function () {
            if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                for (var i = 0; i < $scope.oldColumns.length; i++) {
                    var column = [];
                    column = {
                        field: $scope.oldColumns[i].field,
                        title: $scope.Titles[i].replace(/_/g, " ").trimLeft().replace(/sDash/g, "<br>").trimLeft()
                    }
                    columns.push(column);
                }
                $("#customReportGrid").data("kendoGrid").setOptions({
                    columns: columns
                });
            }
            $(document).unbind("ajaxComplete.changeColumns");
        });

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/RunReport', { model: $scope.Model, makeCampaign: true, term: $scope.searchTerm })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            customReportService.setModelInStorage($scope.Model);
            var reportName = $stateParams.reportType == 'Registration' ? "Custom" : "CustomParent";
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: reportName });
        }

        function setHeader() {
            if ($scope.Model.IsAllPrograms != 'true' && ($scope.Model._SelectedPrograms != null && $scope.Model._SelectedPrograms != 'undefined' && $scope.Model._SelectedPrograms.length == 1)) {
                $scope.Model.SingleProgramMode = true;
                $scope.getReportHeader($scope.Model._SelectedPrograms[0]);
            }

            if ($scope.Model.IsAllPrograms == 'true' || ($scope.Model._SelectedPrograms != null && $scope.Model._SelectedPrograms != 'undefined' && $scope.Model._SelectedPrograms.length > 1)) {
                $scope.Model.SingleProgramMode = false;
                $scope.getReportHeader();
            }
        }
        
        function resizeGrid() {
            $("#customReportGrid").data("kendoGrid").resize();
        }
    }]);
})();
