﻿using Jumbula.Common.Base;
using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SeasonDisplayViewModel : BaseViewModel<Jumbula.Core.Domain.Season, long>
    {
        public string Title { get; set; }
        public string Domain { get; set; }
        public string ClubDomain { get; set; }
        public string Description { get; set; }

        public List<Program> Programs { get; set; }

    }
  
}