﻿namespace Jumbula.Common.Constants
{
    public static class Email
    {
        public const string TemplatesPath = "~/Views/Shared/Email/";
        public const string GeneralEmailTemplatePath = "~/Views/Shared/Email/EmailTemplate.cshtml";
        public const string JumbulaSupportEmailAddress = "support@jumbula.com";
        public const string JumbulaSupportEmailName = "Jumbula Support";

        public const string JumbulaNoReplyEmailAddress = "noreply@jumbula.com";

        public const int MaxSendAttempt = 3;
        public const string SupportSubCategory = "Support";
        public const string PrimarySubCategory = "Primary";

    }
}
