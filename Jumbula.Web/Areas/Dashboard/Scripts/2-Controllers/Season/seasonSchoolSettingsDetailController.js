﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsDetailController', ['$scope', '$http', 'jbToast', '$stateParams', function ($scope, $http, jbToast, $stateParams) {
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSetting", order: 2, menu: "SEASONS" });
       
        var backpackflierdates = [];
        $scope.MC.stateParams = $stateParams;
        $http.get('Dashboard/Season/EditSchoolSettingsDetailInfo?seasonDomain=' + $stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model = data;            

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

     
        $scope.save = function () {

         
            $http.post('Dashboard/Season/EditSchoolSettingsDetailInfo', { model: $scope.Model, seasonDomain: $stateParams.seasonDomain })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     $scope.errors = null;
                     jbToast.success("Season detail updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }
  
        $scope.emailBlastDates = '';
        $scope.backpackFlierDates = '';
        $scope.datetimePickerChanged = function (e, emailType) {
            if (emailType == 'EmailBlastDates') {
                $scope.Model.AllEmailBlastDates.push({ Text: $scope.emailBlastDates, Value: $scope.emailBlastDates });
                $scope.Model.EmailBlastDates.push($scope.emailBlastDates);
            }
            if (emailType == 'BackpackFlierDates') {
                $scope.Model.AllBackpackFlierDates.push({ Text: $scope.backpackFlierDates, Value: $scope.backpackFlierDates });
                $scope.Model.BackpackFlierDates.push($scope.backpackFlierDates);
            }
           
           
        }

    }])

})();