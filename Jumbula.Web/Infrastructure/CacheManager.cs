﻿using System;
using System.IO;
using Constants = Jumbula.Utilities.Constants.Constants;

namespace SportsClub.Infrastructure
{
    /// <summary>
    /// All cached files, images shoud be done here.
    /// Email cached data for templates, etc should be moved here in R1+
    /// </summary>
    public static class CacheManager
    {
        // these constants are probably useless and must be delete later
        public static byte[] CampFlyer_TT_Banner { get; set; }
        public static byte[] CampFlyer_TT_LeftPanel { get; set; }
        public static byte[] CampFlyer_Chess_Banner { get; set; }
        public static byte[] CampFlyer_Chess_LeftPanel { get; set; }
        public static byte[] CampFlyer_Wrestling_Banner { get; set; }
        public static byte[] CampFlyer_Wrestling_LeftPanel { get; set; }
        public static byte[] Trophy_Chess_Tournament_Prize { get; set; }
        public static byte[] ClassFlyer_Chess_LeftPanel { get; set; }
        public static byte[] ClassFlyer_TT_LeftPanel { get; set; }

        // flyer bytes definition
        public static byte[] JbPowerLogo { get; set; }
        public static byte[] CampFlyer_LineSeparator { get; set; }

        public static byte[] CampFlyer_Banner { get; set; }
        public static byte[] ClassFlyer_Banner { get; set; }
        
        static CacheManager()
        {
            // these definitions are probably useless and must be delete later
            CampFlyer_Wrestling_Banner = LoadImage(Constants.Path_Image_CampFlyer_Wrestling_Banner);
            CampFlyer_Wrestling_LeftPanel = LoadImage(Constants.Path_Image_CampFlyer_Wrestling_LeftPanel);
            CampFlyer_TT_Banner = LoadImage(Constants.Path_Image_CampFlyer_TT_Banner);
            CampFlyer_TT_LeftPanel = LoadImage(Constants.Path_Image_CampFlyer_TT_LeftPanel);
            CampFlyer_Chess_Banner = LoadImage(Constants.Path_Image_CampFlyer_Chess_Banner);
            CampFlyer_Chess_LeftPanel = LoadImage(Constants.Path_Image_CampFlyer_Chess_LeftPanel);
            ClassFlyer_Chess_LeftPanel = LoadImage(Constants.Path_Image_ClassFlyer_Chess_LeftPanel);
            ClassFlyer_TT_LeftPanel = LoadImage(Constants.Path_Image_ClassFlyer_TT_LeftPanel);
            Trophy_Chess_Tournament_Prize = LoadImage(Constants.Path_Image_Trophy_Chess_Tournament_Prize);

            // load flyer images
            JbPowerLogo = LoadImage(Constants.Path_Image_JBPowerLogo);
            CampFlyer_LineSeparator = LoadImage(Constants.Path_Image_CampFlyer_LineSeparator);

            CampFlyer_Banner = LoadImage(Constants.Path_Image_CampFlyer_Banner);
            ClassFlyer_Banner = LoadImage(Constants.Path_Image_ClassFlyer_Banner);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="path">Part of path which is in root of directory, the location of project provid in method</param>
        /// <returns></returns>
        static byte[] LoadImage(string path)
        {
            try
            {
                byte[] image = null;
                string filePath = Path.Combine(TheIo.PhysicalApplicationPath(), path);
                if (File.Exists(filePath))
                {
                    image = File.ReadAllBytes(filePath);
                }
                else
                {
                    image = new byte[0];  // check this? report errors? exceptions???
                }

                return image;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}