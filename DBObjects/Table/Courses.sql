--Create time 2014/09/03 --- By MN----
If Object_ID('[dbo].[Courses]') Is Null
BEGIN
CREATE TABLE [dbo].[Courses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [nvarchar](max) NULL,
	[Domain] [nvarchar](max) NULL,
	[ClubDomain] [nvarchar](max) NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[NameLong] [nvarchar](max) NULL,
	[Capacity] [int] NULL,
	[HeadCoach] [nvarchar](max) NULL,
	[AssistantCoaches] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[LocationName] [nvarchar](max) NULL,
	[RecurrentStartDate] [datetime] NOT NULL,
	[RecurrentEndDate] [datetime] NULL,
	[WeeksCount] [int] NULL,
	[EarlybirdEndDate] [datetime] NULL,
	[AdditionalNotes] [nvarchar](1024) NULL,
	[FullPrice] [decimal](18, 2) NULL,
	[HasSessionPrice] [bit] NOT NULL,
	[SessionPrice] [decimal](18, 2) NULL,
	[HasProRatingPrice] [bit] NOT NULL,
	[DisableOnlineRegistration] [bit] NOT NULL,
	[ExternalRegUrl] [nvarchar](max) NULL,
	[MetaData_DateCreated] [datetime] NOT NULL,
	[MetaData_DateUpdated] [datetime] NOT NULL,
	[TimeZone] [int] NOT NULL,
	[EventStatus] [int] NOT NULL,
	[FileName] [nvarchar](128) NULL,
	[Club_Id] [int] NULL,
	[CustomTemplateMetaData] [nvarchar](max) NULL,
	[EventSearchField_MinAge] [int] NULL,
	[EventSearchField_MaxAge] [int] NULL,
	[FlyerType] [int] NOT NULL,
	[ClubLocation_Id] [int] NULL,
	[EventSearchField_MinGrade] [int] NULL,
	[EventSearchField_MaxGrade] [int] NULL,
	[EventSearchField_Level] [int] NULL,
	[EventSearchField_Gender] [int] NULL,
	[AsTemplate] [bit] NOT NULL,
	[Room] [nvarchar](max) NULL,
	[CourseType] [int] NULL,
	[EventSearchTags_AfterSchool] [bit] NOT NULL,
	[EventSearchTags_Discounts] [bit] NOT NULL,
	[EventSearchTags_OverNight] [bit] NOT NULL,
	[EventSearchTags_SpecialNeeds] [bit] NOT NULL,
	[TemplateName] [nvarchar](max) NULL,
	[Donation_Checked] [bit] NOT NULL,
	[Donation_Title] [nvarchar](256) NULL,
	[Donation_Description] [nvarchar](max) NULL,
	[CoachId] [int] NULL,
	[AssistantCoachId] [int] NULL,
	[OutSourcerClubDomain] [nvarchar](128) NULL,
	[CaptureRestriction] [bit] NOT NULL,
	[ReservedCapacity] [int] NOT NULL,
	[RegisterDeadLine_StartRegisterDeadLineDate] [datetime] NULL,
	[RegisterDeadLine_ShowEvent] [bit] NOT NULL,
	[RegistrationPeriod_RegisterStartDate] [datetime] NULL,
	[RegistrationPeriod_RegisterEndDate] [datetime] NULL)

END


--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='PK_dbo.Courses')
BEGIN
	ALTER TABLE [dbo].[Course] ADD Constrant [PK_dbo.Courses] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)
END


--<< ADD CLOLUMNS >>--
IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.Courses') AND name='RegistrationPeriod_RegisterStartDate')
BEGIN
	ALTER TABLE [dbo].[Courses]
	ADD RegistrationPeriod_RegisterStartDate datetime null
END

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.Courses') AND name='RegistrationPeriod_RegisterEndDate')
BEGIN
	ALTER TABLE [dbo].[Courses]
	ADD RegistrationPeriod_RegisterEndDate datetime null
END


--<< UPDATE DATA >>-----
IF NOT EXISTS(SELECT 1 FROM [dbo].[Courses] WHERE RegistrationPeriod_RegisterStartDate is not null AND RegistrationPeriod_RegisterEndDate is not null)
BEGIN
	update [Courses] set RegistrationPeriod_RegisterStartDate=MetaData_DateCreated
	update [Courses] set RegistrationPeriod_RegisterEndDate=RecurrentEndDate where HasProRatingPrice=1
	update [Courses] set RegistrationPeriod_RegisterEndDate=RecurrentStartDate where HasProRatingPrice=0
	update [Courses] set RegistrationPeriod_RegisterEndDate='2015-09-30' where coursetype=1
	print 'update RegistrationPeriod_RegisterStartDate and RegistrationPeriod_RegisterEndDate data'
END


--<< EDIT COLUMN >>------
IF EXISTS(SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.Courses') AND name='RegistrationPeriod_RegisterStartDate')
BEGIN
	ALTER TABLE [dbo].[Courses]
	ALTER COLUMN RegistrationPeriod_RegisterStartDate datetime not null
END

IF EXISTS(SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.Courses') AND name='RegistrationPeriod_RegisterEndDate')
BEGIN
	ALTER TABLE [dbo].[Courses]
	ALTER COLUMN RegistrationPeriod_RegisterEndDate datetime not null
END