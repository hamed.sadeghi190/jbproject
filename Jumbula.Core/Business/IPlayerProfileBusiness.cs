﻿using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;
using System.Web.Mvc;

namespace Jumbula.Core.Business
{
    public interface IPlayerProfileBusiness
    {
        string GetPreapprovalkey(int playerId);
        PlayerProfile Get(int playerId);
        IQueryable<PlayerProfile> GetList();
        IQueryable<PlayerProfile> GetList(string email);
        IQueryable<PlayerProfile> GetList(int userId);
        IQueryable<PlayerProfile> GetList(string firsName, string LastName);
        OperationStatus Create(PlayerProfile playerProfile);
        OperationStatus Update(PlayerProfile playerProfile);
        OperationStatus Delete(PlayerProfile profile);
        PlayerProfile GetFromForm(JbForm jbForm, SectionsName sectionName);
        JbSection GetParticipantSection(JbForm jbForm = null);
        void ProfileSectionsGenerator(JbForm jbForm);
        void ProfileSectionsGenerator2(JbForm jbForm);
        object GetParticipantField(JbForm form, ElementsName name);
        void FillParticipantSection(int playerId, JbForm jbForm);
        void GenerateParticipantSection(JbForm jbForm);

        bool HasPreapprovalkey(int playerId);

        IQueryable<PlayerProfile> GetAllWithPreapproval();
        OperationStatus SetEducationalStatus(int id);
        OperationStatus UpdateGraduatedProfiles(List<PlayerProfile> profiles);
        SchoolGradeType? GetGradeValueFromDescription(object gradeObj);
        OperationStatus UpdateInfoParent1(JbForm formData, List<PlayerProfile> parents);
        OperationStatus CreatePlayerParent1(JbForm formData, OrderItem orderItem);
        OperationStatus UpdateInfoParent2(JbForm formData, List<PlayerProfile> parents);
        OperationStatus CreatePlayerParent2(JbForm formData, OrderItem orderItem);
        bool IsExistPlayer(List<ContactPerson> listContactFamily, string playerFirstName, string playerLastName);

        dynamic GetFamilyProfiles(int userId);
        List<PlayerProfile> GetAllParticipantsProfile(List<int> userIds, bool includeContact = false, bool includeInfo = false);
        List<PlayerProfile> GetAllParentsProfile(List<int> userIds, bool includeContact = false);
        List<FamilyContact> GetAllEmergencyFamily(List<int> userIds, bool includeContact = false);
        List<FamilyContact> GetAllAuthorizePickupFamily(List<int> userIds, bool includeContact = false);
        List<Family> GetAllInsuranceFamily(List<int> userIds);

        OperationStatus DeletePlayerProfile(int id);
        JbForm ParentReportSectionGenerator(JbForm jbForm, long? seasonId, int clubId);

        dynamic GetAllFamiliesProfiles(List<int> userIds);

        void AutoFillProfileForm(JbForm jbForm, int userId, long programId, int? profileId);
        //OperationStatus SetFormToParentDashboard(JbForm jbForm, int? player,int userId);
        IQueryable<PlayerProfile> GetParticipants(int userId);
        IQueryable<PlayerProfile> GetParents(int userId);
        PlayerProfile GetPlayerParticipantjbForm(JbForm jbForm, PlayerProfile player, JbUser user);
        PlayerProfile GetPlayerParentjbForm(JbForm jbForm, PlayerProfile player, JbUser user);
        PlayerProfile GetPlayerParent2jbForm(JbForm jbForm, PlayerProfile player, JbUser user);
        FamilyContact GetAuthorizePickup1jbForm(JbForm jbForm, FamilyContact authorizedPickup, Family family);
        FamilyContact GetAuthorizePickup2jbForm(JbForm jbForm, FamilyContact authorizedPickup2, Family family);
        FamilyContact GetAuthorizePickup3jbForm(JbForm jbForm, FamilyContact authorizedPickup3, Family family);
        FamilyContact GetAuthorizePickup4jbForm(JbForm jbForm, FamilyContact authorizedPickup4, Family family);
        FamilyContact GetEmergencyContactjbForm(JbForm jbForm, FamilyContact emergencyContact, Family family);
        Family GetInsurancejbForm(JbForm jbForm, Family family);
        OperationStatus SaveProfiles(List<PlayerProfile> playerList, List<FamilyContact> familyContactList, Family family);
        List<PlayerProfile> CompareParent1(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm);
        List<PlayerProfile> CompareParent2(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm);

        List<FamilyContact> CompareAuthorizePickup1(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm);
        List<FamilyContact> CompareAuthorizePickup2(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm);
        List<FamilyContact> CompareAuthorizePickup3(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm);
        List<FamilyContact> CompareAuthorizePickup4(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm);
        List<FamilyContact> CompareEmergencyContact(PlayerProfile player, JbForm newRegForm, JbForm oldRegForm);

        List<SelectListItem> GetSchoolGrades();

        List<SelectListItem> GetGenderCategories();
    }
}
