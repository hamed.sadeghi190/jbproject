﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class TourneyOrderPlayerDetailViewModel
    {
        public TourneyOrderPlayerDetailViewModel()
        {
            SportProfile = new SportProfileViewModel();
            Player = new PalyerViewModel();
        }

        //Get it from PlayerProfile.Contact
        public string PlayerProfileFullName { get; set; }

        //Get it from PlayerProfile.Contact
        public string PlayerProfileEmail { get; set; }

       // public SportProfileViewModel SportProfile { get; set; }

        public PalyerViewModel Player { get; set; }

        public class PalyerViewModel
        {
            public string Avatar { get; set; }

            //Get it from EmergrncyContact
            public string FullName { get; set; }

            public string CellPhone { get; set; }

            public string WorkPhone { get; set; }

            public string HomePhone { get; set; }

            //Get it from EmergrncyContact
            public string Email { get; set; }

            public bool IsAdult { get; set; }

            public string Address { get; set; }
        }
    }
}