﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class ProgramElements : BaseElements
    {
        public ProgramElements(IWebDriver driver)
            : base(driver)
        {

        }

        public IWebElement ProgramNameTextbox
        {
            get
            {
                return Driver.FindElement(By.Name("Name")); 
            }
        }
        public IWebElement CategoryList
        {
            get
            {
                return Driver.FindElement(By.ClassName("chosen-choices"));
            }
        }
        public IWebElement AcademicWritingCategory
        {
            get
            {
                IList<IWebElement> spans = Driver.FindElements(By.TagName("li"));
                return spans.First(btn => btn.Text == "Academics --> Academic Writing");
            }
        }
        public IWebElement DescriptionTextbox
        {
            get
            {
                return Driver.FindElement(By.Id("discription"));
            }
        }
        public IWebElement SaveButton
        {
            get
            {
                return Driver.FindElement(By.Id("btn_save"));
            }
        }

    }
}






