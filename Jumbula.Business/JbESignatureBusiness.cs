﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class JbESignatureBusiness : IJbESignatureBusiness
    {
        private readonly IRepository<JbESignature> _jbSigniturerepository;

        public JbESignatureBusiness(IRepository<JbESignature> jbSigniturerepository)
        {
            _jbSigniturerepository = jbSigniturerepository; ;
        }

        public IQueryable<JbESignature> GetList()
        {
            return _jbSigniturerepository.GetList();
        }
        public JbESignature Get(int id)
        {
            return _jbSigniturerepository.GetList().SingleOrDefault(p => p.Id == id);
        }
        public OperationStatus Create(JbESignature eSignature)
        {
            return _jbSigniturerepository.Create(eSignature);

        }
        public OperationStatus Update(JbESignature eSignature)
        {
            return _jbSigniturerepository.Update(eSignature);
        }
    }

}
