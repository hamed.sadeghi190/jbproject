
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JbUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.JbUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[JbUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbUserLogins_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[JbUserLogins] CHECK CONSTRAINT [FK_dbo.JbUserLogins_dbo.JbUsers_UserId]
GO


