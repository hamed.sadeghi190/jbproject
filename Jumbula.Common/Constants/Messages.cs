﻿namespace Jumbula.Common.Constants
{
    public static class Messages
    {
        public const string EditEndDateProgramForDorpInPunchcard = "Before we can proceed to change the program start date or end date, you must edit all the existing drop-in and punch card orders that will be affected by this change. For any affected drop-in orders, you must cancel or reassign the date. For any punch card that has an affected assigned date, you must unassign the date. The following orders are affected by this change: ";
        public const string EditEndDateProgramForSubscriptionOrders = "Before we can proceed to delete the schedules from the program, you must edit all the existing orders and delete the affected schedules. For any affected drop-in orders, you must cancel or reassign the date. For any punch card that has an affected assigned date, you must unassign the date.";
        public const string EditStartDateProgramForSubscriptionOrders = "Before we can proceed to delete the schedules from the program, you must edit all the existing orders and change the desired start dates. For any affected drop-in orders, you must cancel or reassign the date. For any punch card that has an affected assigned date, you must unassign the date. The desired start dates of the following orders must be updated: ";
        public const string EditProgramDateWarrning = "Changing the program end date or start date will cause the following schedules to be deleted from the program setup: ";
        public const string ReassignRegistrationForm = "Reassigned registration form data from ";
        public const string NotSelectedGridRowErrorMessage = "Make a selection first";
        public const string UndoCancelOrder = "Order {0} - {1} ({2}) is uncanceled.";
        public const string UndoFamilyRefunToOrder = "${0} has been returned to order.";
    }
}
