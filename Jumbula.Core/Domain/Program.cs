﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Jumbula.Core.Domain
{

    public class Program : BaseEntity<long>, IProtectedResource
    {
        private ProgramCustomFields _customFields;
        public Program()
        {
            Categories = new HashSet<Category>();
            Instructors = new HashSet<ClubStaff>();
            Coupons = new HashSet<Coupon>();
            WaitLists = new HashSet<WaitList>();
            Charges = new HashSet<Charge>();
        }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(128)]
        public string Domain { get; set; }

        [Required]
        public string Description { get; set; }

        [StringLength(4000)]
        public string Testimonials { get; set; }

        public bool HasTestimonial { get; set; }

        [StringLength(4000)]
        public string Note { get; set; }

        [StringLength(128)]
        public string Room { get; set; }

        [Required]
        public ProgramPageStep LastCreatedPage { get; set; }

        [Required]
        public ProgramTypeCategory TypeCategory { get; set; }

        public ProgramStatus Status { get; set; }

        public RegistrationMode RegistrationMode { get; set; }
        public int? InstructorId { get; set; }

        [ForeignKey("InstructorId")]
        public virtual JbUser Instructor { get; set; }
        [NotMapped]
        public SaveType SaveType
        {
            get
            {
                return (LastCreatedPage >= ProgramPageStep.Step4) ? SaveType.Publish : SaveType.Draft;
            }
        }

        [StringLength(2048)]
        [Column("Instructor")]
        public string InstructorSerialized { get; set; }

        public MetaData MetaData { get; set; }
        public Flyer Flyer { get; set; }

        [Column("CustomFields")]
        public string CustomFieldsSerialized { get; set; }

        [NotMapped]
        public bool IsCustomFieldChanged { get; set; }

        [NotMapped]
        public ProgramCustomFields CustomFields
        {
            get
            {
                if (_customFields == null)
                {


                    _customFields = !string.IsNullOrEmpty(CustomFieldsSerialized) ? JsonConvert.DeserializeObject<ProgramCustomFields>(CustomFieldsSerialized) : new ProgramCustomFields();

                }

                return _customFields;
            }

            set
            {
                _customFields = value;

            }
        }
        [NotMapped]
        public string ClubDomain
        {
            get
            {
                return Club.Domain;
            }
        }
        [NotMapped]
        public string SeasonDomain
        {
            get
            {
                return Season.Domain;
            }
        }
        public long SeasonId { get; set; }
        [ForeignKey("SeasonId")]
        public virtual Season Season { get; set; }

        public int? CatalogId { get; set; }
        [ForeignKey("CatalogId")]
        public virtual CatalogItem Catalog { get; set; }

        public long? OutSourceSeasonId { get; set; }
        [ForeignKey("OutSourceSeasonId")]
        public virtual Season OutSourceSeason { get; set; }
        public OutSourceProgramStatus OutSourceProgramStatus { get; set; }
        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }

        public int ClubId { get; set; }

        [ForeignKey("ClubLocationId")]
        public virtual ClubLocation ClubLocation { get; set; }

        public int ClubLocationId { get; set; }

        public bool HasRegisterPIN { get; set; }
        [MaxLength(64)]
        public string RegisterPIN { get; set; }

        [MaxLength(1024)]
        public string RegisterPINMessage { get; set; }

        public bool DisableCapacityRestriction { get; set; }

        public int? ImageGalleryId { get; set; }
        [ForeignKey("ImageGalleryId")]
        public virtual ImageGallery ImageGallery { get; set; }

        [NotMapped]
        public JbForm JbForm
        {
            get
            {
                ClubFormTemplate clubFormTemplate = this.ClubFormTemplates.SingleOrDefault(s => s.JbForm != null && s.FormType == FormType.Registration);

                if (clubFormTemplate != null)
                {
                    return clubFormTemplate.JbForm;
                }

                return null;
            }

        }

        public bool IsWaitListAvailable { get; set; }

        [MaxLength(1024)]
        public string WaitlistPolicy { get; set; }

        //These two property should remove after flex data manipulation
        [MaxLength(400)]
        public string UniqueKey { get; set; }
        public bool MessageSent { get; set; }

        [ForeignKey("ParentId")]
        public virtual Program Parent { get; set; }

        public long? ParentId { get; set; }

        public virtual ICollection<Category> Categories { get; set; }

        public virtual ICollection<ProgramSchedule> ProgramSchedules { get; set; }

        public virtual ICollection<PaymentPlan> PaymentPlans { get; set; }
        public virtual ICollection<ProgramRespondToInvitation> RespondToInvitation { get; set; }
        public virtual ICollection<Coupon> Coupons { get; set; }

        public virtual ICollection<Charge> Charges { get; set; }

        public virtual ICollection<Discount> Discounts { get; set; }

        public virtual ICollection<ClubWaiver> ClubWaivers { get; set; }

        public virtual ICollection<ClubFormTemplate> ClubFormTemplates { get; set; }

        public virtual ICollection<ClubStaff> Instructors { get; set; }

        public virtual ICollection<WaitList> WaitLists { get; set; }

        public virtual ICollection<ProgramAttributeValue> ProgramAttributeValues { get; set; }

        public virtual ICollection<ProgramSchedulePart> ScheduleParts { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Club.Id == clubId || (OutSourceSeasonId.HasValue && OutSourceSeason.ClubId == clubId))
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

    public enum RepeatType : byte
    {
        Weekly,
        Monthly
    }

    #region Enums


    public enum OutSourceProgramStatus
    {
        Regular = 1,
        Invitation = 2,
        Tentative = 3,
        Scheduled = 4
    }

    public enum ProgramStatus
    {
        Open = 0,
        Frozen = 1,
        Deleted = 2,
    }

    public enum ProgramPageStep
    {
        NotCreated = 0,
        Step1 = 1,
        Step2 = 2,
        Step3 = 3,
        Step4 = 4
    }

    public enum ProgramRunningStatus
    {
        NotStarted = 0,
        [Description("in progress")]
        InProgress = 1,
        Expired = 2,
    }

    public enum ProgramRegStatus
    {
        [Description("Not started")]
        NotStarted = 0,
        [Description("Open")]
        Open = 1,
        [Description("Closed")]
        Closed = 2,
    }

    public enum ProgramStatusType
    {
        Available = 0,
        NotStarted = 1,
        Expired = 2,
        Full = 3,
        Frozen = 4,
    }

    #endregion
}
