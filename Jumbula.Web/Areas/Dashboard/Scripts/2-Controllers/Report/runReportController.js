﻿(function () {
    "use strict";

    angular.module("dashboardApp").controller("runReportController", ["$scope", "$http", "$log", "$stateParams", "$state", "reportService", "jbToast", "cfpLoadingBar", "$timeout", function ($scope, $http, $log, $stateParams, $state, reportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value) {
            if (value.Domain === $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + "/Dashboard/Report";

        $scope.parameters = {
            reportName: $scope.MC.stateParams.reportName,
            seasonDomain: $scope.MC.stateParams.seasonDomain
        };

        $scope.Filters = [];
        $scope.Exports = [];
        $scope.IsReportRun = false;
        $scope.IsInitColumns = false;
        var parameterRequest = {};
        var timer = false;
        $scope.Model = [];


        $http.post(baseUrl + "/GetSchema", { reportName: $scope.MC.stateParams.reportName, parameters: $scope.parameters }).success(function (data) {
            console.log(data);

            $scope.gridOptions = bindKendoGridModel(data.ReportGridModel);
            $scope.ReportName = data.Title;
            $scope.Headers = data.Headers;
            $scope.Exports = data.Exports;
            $scope.FilterView = data.FilterView;
            $scope.Filters = data.Filters;

            if ($scope.Filters != null) {

                for (var i = 0; i < $scope.Filters.Elements.length; i++) {

                    $scope.parameters[$scope.Filters.Elements[Object.keys($scope.Filters.Elements)[i]].ParameterName] = $scope.Filters.Elements[i].ParameterValue;
                    $scope.Model[i] = $scope.Filters.Elements[i].ParameterValue;
                }

                initFilters();
            }


            console.log("parameters");
            console.log($scope.parameters);

            $scope.bindData();

            console.log($scope.parameters);

        }).error(function (data) {
            jbToast.error(data);
        });


        function bindKendoGridModel(data) {
            var gridModel = {
                resizable: data.IsResizable,
                pageable: data.Pageable,
                columnMenu: data.IsColumnMenu,
                sortable: data.IsSortable,
                scrollable: data.IsScrollable,
                pageSize: data.PageSize
            };

            return gridModel;
        }

        function initFilters() {

            //for (var i = 0; i < $scope.Filters.Elements.length; i++) {
            //    if ($scope.Filters.Elements[i].ActionFilter === "PrepareData") {
            //        $scope.parameters["filterName"] = $scope.Filters.Elements[i].Name;

            //        getFilter($scope.MC.stateParams.reportName, $scope.parameters, $scope.Filters.Elements[i].Name);
            //    }
            //}

        }


        $scope.bindData = function () {

            $scope.dataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/BindData",
                        dataType: "json",
                        type: "POST",
                        data: function (data) {

                            if (data.sort != null && data.sort.length != 0) {
                                //data.sort[0].sortPath = $scope.Columns.find(column => column.Field === data.sort[0].field).SortPath;
                            }

                            return {
                                reportName: $scope.MC.stateParams.reportName,
                                parameters: parameterRequest,
                                sort: data.sort
                            };
                        },
                        parameterMap: function (data, operation) {
                            if (operation === "read") {
                                data = $.extend(
                                    {
                                        sort: null,
                                        filter: null
                                    }, data);

                                return JSON.stringify(data);
                            }
                            return JSON.stringify(null);
                        }
                    }
                },
                schema: {
                    total: function (response) {

                        if (response.Data.length === 0) {
                            return 0;
                        }
                        return response.Data.TotalCount;
                    },
                    data: function (response) {
                        $scope.Columns = response.Columns;
                        if (response.Data.length === 0) {
                            return response.Data;
                        }

                        return response.Data.DataSource;
                    }
                },
                serverFiltering: true,
                serverPaging: true,
                serverSorting: true,
                resizable: true,
                batch: true,
                requestStart: function () {
                    kendo.ui.progress($("[kendo-grid]"), true);
                },
                requestEnd: function () {
                    kendo.ui.progress($("[kendo-grid]"), false);
                }
            });

            console.log($scope.dataSource);

            $scope.gridOptions.dataSource = $scope.dataSource;
        };



        $scope.setOnChangeAction = function (filterName, id) {
            console.log("Model: " + $scope.Model[id]);
            var key = $scope.Filters.Elements[Object.keys($scope.Filters.Elements)[id]].ParameterName;
            $scope.parameters[key] = $scope.Model[id];

            if (filterName === null) {
                return;
            }

            $scope.parameters["filterName"] = filterName;

            if ($scope.Model[id] != null)
                getFilter($scope.MC.stateParams.reportName, $scope.parameters, filterName);
        }

        function getFilter(reportName, params, filterName) {

            $http.post(baseUrl + "/GetFilterData", { parameters: params })
                .success(function (data) {
                    for (var i = 0; i < $scope.Filters.Elements.length; i++) {
                        if ($scope.Filters.Elements[i].Name === filterName) {

                            reNewRelatedFilter(i);

                            $scope.Filters.Elements[i].Data = data[filterName].Data;
                            prepareDropDown();
                        }
                    }
                }).error(function (data) {
                    jbToast.error(data);
                });
        }

        function reNewRelatedFilter(index) {

            for (var j = 0; j < $scope.Filters.Elements.length; j++) {

                if ($scope.Filters.Elements[j].SectionName === $scope.Filters.Elements[index].SectionName &&
                    $scope.Filters.Elements[j].Id > $scope.Filters.Elements[index].Id) {

                    $scope.Model[j] = null;
                }
            }
        }

        function prepareDropDown() {

            $scope.$watch($scope.Filters, function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $("select[chosen]").trigger("chosen:updated");
                }, 1500);
            });
        }


        $scope.runReport = function () {

            if ($scope.checkValidations()) {
                initParameterRequest();
                run();
            }
        }

        function run() {
            $scope.IsReportRun = true;
            console.log("RUN PARAMETERS");
            console.log($scope.parameters);
            console.log("-------");
            if ($scope.IsReportRun) {

                var grid = $("#grid").data("kendoGrid");

                if ($scope.IsInitColumns) {
                    $scope.gridOptions.dataSource.page(1);
                }
                grid.dataSource.pageSize($scope.gridOptions.pageSize);
                grid.dataSource.read();

                $scope.bindColumns();

                grid.bind("dataBound", function (e) {
                    reportService.initializeDoubleScroll(e);
                });
            } else {

                bindData();
            }
        }


        $scope.bindColumns = function () {
            var grid = $("#grid").data("kendoGrid");
            $(document).bind("ajaxComplete.changeColumns", function () {
                
                if ($scope.Columns && $scope.Columns.length > 0) {
                    $scope.IsInitColumns = true;
                    var columns = [];
                    for (var i = 0; i < $scope.Columns.length; i++) {

                        var column = {
                            field: $scope.Columns[i].Field,
                            title: $scope.Columns[i].Title,
                            width: $scope.Columns[i].Width,
                            locked: $scope.Columns[i].Locked,
                            template: $scope.Columns[i].Template
                        }

                        columns.push(column);
                    }
                    grid.setOptions({
                        columns: columns
                    });
                }
                $(document).unbind("ajaxComplete.changeColumns");
            });
        }

        $scope.isExistObject = function (object, objectName) {

            if (object == null) {
                return false;
            }

            for (var i = 0; i < object.length; i++) {
                if (object[i].Name === objectName) {
                    return true;
                }
            }

            return false;
        }

        $scope.clearFilters = function () {

            for (var i = 0; i < Object.keys($scope.parameters).length; i++) {
                var parameterName = Object.keys($scope.parameters)[i];

                if (parameterName !== "reportName" || parameterName !== "seasonDomain") {
                    $scope.parameters[parameterName] = null;
                }
            }

            $scope.gridOptions.dataSource.page(1);
            $("#grid").data("kendoGrid").dataSource.read();
        }

        $scope.saveToExcel = function () {
            if (!$scope.IsReportRun) {
                jbToast.error("Please run report");
                return;
            }

            cfpLoadingBar.start();
            $http.post(baseUrl + '/GetExcel', {
                reportName: $scope.MC.stateParams.reportName,
                parameters: $scope.parameters
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/excel" });

                    var fileUrl = URL.createObjectURL(blob);
                    a.href = fileUrl;
                    a.download = $scope.MC.stateParams.reportName + ".xls";
                    a.click();
                });
            cfpLoadingBar.complete();
        }

        $scope.saveToPdf = function () {

            if (!$scope.IsReportRun) {
                jbToast.error("Please run report");
                return;
            }

            cfpLoadingBar.start();

            $http.post(baseUrl + '/GetPdf', {
                reportName: $scope.MC.stateParams.reportName,
                parameters: $scope.parameters
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/pdf" });

                    var fileUrl = URL.createObjectURL(blob);
                    a.href = fileUrl;
                    a.download = $scope.MC.stateParams.reportName + ".pdf";
                    a.click();
                });
        }

        $scope.checkValidations = function () {
            console.log($scope.parameters);
            for (var i = 0; i < $scope.Filters.Elements.length; i++) {

                var element = $scope.Filters.Elements[i];

                var parameterName = $scope.parameters[element.ParameterName];

                var isRequire = element.IsRequired;

                if (isRequire && parameterName === null) {
                    jbToast.error("Select option");
                    return false;
                }
            }
            return true;
        };

        function initParameterRequest() {
            parameterRequest = [];
            for (var i = 0; i < Object.keys($scope.parameters).length; i++) {

                var parameterName = Object.keys($scope.parameters)[i];
                parameterRequest.push({ Text: parameterName, Value: $scope.parameters[parameterName] });
            }
        }
    }
    ]);

})();