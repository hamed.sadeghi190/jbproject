﻿using Jb.Framework.Common.Model;
using SportsClub.Db;
using SportsClub.Domain;
using SportsClub.Infrastructure;
using SportsClub.Infrastructure.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Spatial;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsClub.Models
{
    public class TTTourneyCreateEditViewModels
    {
        public Nullable<TTTourneyEventType> SaveEventMode { get; set; }

        //public virtual ICollection<ChargeDiscountViewModel> ChargeDiscounts { get; set; }

        public virtual ICollection<NoteViewModel> PrizeNotes { get; set; }

        public List<TTTourneyEventViewModels> Events { get; set; }

        public TableTennisPageStep CurrentStep { get; set; }

        public List<TTTourneyPageImageGalleryItemViewModel> Gallery { get; set; }

        public bool IsLoggedIn { get; set; }

        public RoleCategories CurrentUserRole { get; set; }

        public GoogleMapsViewModel Map { get; set; }

        public bool IsAdmin { get; set; }

        public bool Expired { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public string LogoUri { get; set; }

        public string ClubName { get; set; }

        //public string LogoUri { get; set; }

        // affiliator 
        public bool HasAffiliator { get; set; }

        public string AffiliatorLogoUri { get; set; }

        public string AffiliatorDomain { get; set; }

        public string AffiliatorName { get; set; }

        public PageMetaDataViewModel MetaTags { get; set; }

        public string LocationName { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                //// Get cart
                //var cartDb = Ioc.ICartDbService;

                //// Get domain of orders in the cart(if there is no order in cart return empty)
                //string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                //if (!string.IsNullOrEmpty(currentDomain))
                //{
                //    return !(currentDomain == this.ClubDomain);
                //}

                return false;
            }
        }

        public Nullable<DateTime> StartDate
        {
            get
            {
                if (Events.Count > 0)
                {
                    return Events.Select(m => DateTime.Parse(m.StartDate)).Min();
                }
                else
                {
                    return null;
                }
            }
        }

        public Nullable<DateTime> EndDate
        {
            get
            {
                if (Events.Count > 0)
                {
                    return Events.Select(m => DateTime.Parse(m.EndDate)).Max();
                }
                else
                {
                    return null;
                }
            }
        }

        // -1 is when page is not in updateMode and olther value refer to events rownumber
        [DefaultValue(-1)]
        public int EventsUpdateMode { get; set; }

        public string Domain { get; set; }

        public string ClubDomain { get; set; }

        [StringLength(128)]
        public string ItemId { get; set; }

        [Required]
        [DisplayName("Tournament name")]
        [StringLength(128, ErrorMessage = "{0} ")]
        public string Name { get; set; }

        [DisplayName("Additional name1")]
        [StringLength(128, ErrorMessage = "{0} ")]
        public string SubTitle1 { get; set; }

        [DisplayName("Additional name2")]
        [StringLength(128, ErrorMessage = "{0} ")]
        public string SubTitle2 { get; set; }

        [DisplayName("Location name")]
        public string AddressName { get; set; }

        [DisplayName("Hotel Name")]
        public string HotelName { get; set; }

        [DisplayName("Parking Information")]
        public string ParkingInfo { get; set; }

        [Required]
        [DisplayName("Is this location a hotel?")]
        public bool HotelChecked { get; set; }

        [DisplayName("Hotel rate")]
        [Range(Constants.HotelFee_Min, Constants.HotelFee_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public decimal? HotelRate { get; set; }

        [DisplayName("Discount code")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        public string HotelRateCode { get; set; }

        [Phone]
        [Display(Name = "Hotel Phone")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876)")]
        public string HotelPhone { get; set; }

        [Display(Name = "Hotel website")]
        [StringLength(300, ErrorMessage = "{0} is too long")]
        [RegularExpression(@"([a-zA-Z0-9\-]+(?:\.[a-zA-Z0-9\-]+)*\.[a-zA-Z]{2,6}(?:\/?|(?:\/[\w\-]+)*)(?:\/?|\/\w+\.[a-zA-Z]{2,4}(?:\?[\w]+\=[\w\-]+)?)?(?:\&[\w]+\=[\w\-]+)*)$",
          ErrorMessage = "{0} is not a valid website address.")]
        public string HotelSite { get; set; }

        [DisplayName("Hotel description")]
        [DataType(DataType.MultilineText)]
        [StringLength(1024, ErrorMessage = "{0} is too long")]
        public string HotelDescription { get; set; }

        [AllowHtml]
        [DisplayName("Tournament discription")]
        public string Description { get; set; }

        [Required]
        [DisplayName("Tournament Referee")]
        public string Referee { get; set; }

        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Referee_Email { get; set; }

        [Phone]
        [Display(Name = "Phone")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876)")]
        public string Referee_Phone { get; set; }

        [DisplayName("Tournament Umpire")]
        public string Umpire { get; set; }

        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string Umpire_Email { get; set; }

        [Phone]
        [Display(Name = "Phone")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876)")]
        public string Umpire_Phone { get; set; }


        [Required]
        [DisplayName("Tournament Director")]
        public string EventStaff { get; set; }

        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long")]
        public string EventStaff_Email { get; set; }

        [Phone]
        [Display(Name = "Phone")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876)")]
        public string EventStaff_Phone { get; set; }


        [Required]
        [DisplayName("Provide USATT membership?")]
        public bool USATTMembershipChecked { get; set; }

        [Required]
        [DisplayName("Provide USATT membership?")]
        public ICollection<ChargeDiscountViewModel> USATTMembership { get; set; }

        [Required]
        [DisplayName("Online reg deadline")]
        public Nullable<DateTime> OnlineRegistrationDeadLine { get; set; }

        [DisplayName("Players check-in start time")]
        public Nullable<DateTime> CheckInDeadLine { get; set; }

        [Required]
        [DisplayName("Disable Jumbula online registration")]
        public bool OfflineRegChecked { get; set; }

        public string ExternalRegUrl { get; set; }

        [DisplayName("Tournament Equipment")]
        public string Equipment { get; set; }

        [DisplayName("Tournament Rules")]
        public string Rules { get; set; }
        [Required]
        [DisplayName("I understand that I should double check the system generated flier and the tournament registration")]
        public bool IUnderstandChecked { get; set; }

        public MetaData MetaData { get; set; }

        [StringLength(128)]
        public string FileName { get; set; }

        [DisplayName("Upload an existing flyer")]
        public FlyerType FlyerType { get; set; }

        [DisplayName("Template name")]
        public string TemplateName { get; set; }

        [StringLength(128)]
        public string ResultFileName { get; set; }

        [Required]
        public EventStatusCategories EventStatus { get; set; }

        public DonationViewModel Donation { get; set; }

        public TTTourneyEventViewModels RatingEvent { get; set; }

        public TTTourneyEventViewModels Senior { get; set; }

        public TTTourneyEventViewModels CadetsJounior { get; set; }

        public TTTourneyEventViewModels Double { get; set; }

        public SportCategories Sport { get; set; }

        public PolicyViewModel Proof { get; set; }

        public PolicyViewModel General { get; set; }

        public PolicyViewModel Refund { get; set; }

        public PolicyViewModel Waiver { get; set; }

        public PolicyViewModel MedicalRelease { get; set; }

        public string EventJson { get; set; }

        public string UploaderString { get; set; }

        public string Venue { get; set; }

        //[Required]
        //public DbGeography Geography { get; set; }

        //[Required]
        //[DefaultValue(TimeZone.UTC)]

        public string TimeZoneName { get; set; }

        public Domain.TimeZone TimeZone { get; set; }

        public PageMode PageMode { get; set; }

        [DisplayName("Check if you want to save a copy of this event as template.")]
        public bool AsTemplate { get; set; }

        public bool FromTemplate { get; set; }

        public EventSearchField EventSearchField { get; set; }

        public int PostalAdderssId { get; set; }

        public int ClubLocationId { get; set; }

        public DisplayClubLocationViewModel DisplayClubLocationViewModel { get; set; }

        public EventSearchTags EventSearchTags { get; set; }

        public SelectList Ages { get; set; }

        [DisplayName("Outsourcer")]
        public string OutSourcerClubDomain { get; set; }

        public IEnumerable<OutSourcerList> OutSourcers { get; set; }

        public SelectList OutSourcerSelectList { get; set; }

        public int ClubId { get; set; }

        public bool IsVendor { get; set; }

        [DisplayName("Is this an outsourcer event for a school or club?")]
        public EventSponserType IsVendorEvent { get; set; }

        public int OutSourcerClubLocationId { get; set; }
        public bool IsOutsourcerEvent { get; set; }

        public string OutsourcerClubName { get; set; }

        public RegisterDeadLine RegisterDeadLine { get; set; }

        public TTTourneyCreateEditViewModels()
        {
            Donation = new DonationViewModel();
            OfflineRegChecked = false;
            USATTMembershipChecked = false;
            USATTMembership = new HashSet<ChargeDiscountViewModel>();
            Events = new List<TTTourneyEventViewModels>();
            EventSearchField = new EventSearchField();
            EventSearchTags = new EventSearchTags();
            OutSourcers = new List<OutSourcerList>();
            SeedAges();
            RegisterDeadLine = new RegisterDeadLine();

        }

        public TTTourneyCreateEditViewModels(bool temp, string clubDomain, PageMode pageMode, TableTennisPageStep pageStep, int clubId, bool isVendor)
            : this()
        {
            //ChargeDiscounts = new HashSet<ChargeDiscountViewModel>();
            PrizeNotes = new HashSet<NoteViewModel>();
            RatingEvent = new TTTourneyEventViewModels { EventType = TTTourneyEventType.Ratings, Gender = GenderCategories.None, RoundType = TTTourneyRoundType.None, RewardType = TourneyRewardType.None };
            Senior = new TTTourneyEventViewModels { EventType = TTTourneyEventType.Seniors, Gender = GenderCategories.None, RoundType = TTTourneyRoundType.None, RewardType = TourneyRewardType.None };
            CadetsJounior = new TTTourneyEventViewModels { EventType = TTTourneyEventType.Cadets_Juniors, Gender = GenderCategories.None, RoundType = TTTourneyRoundType.None, RewardType = TourneyRewardType.None };
            Double = new TTTourneyEventViewModels { EventType = TTTourneyEventType.Doubles, Gender = GenderCategories.None, RoundType = TTTourneyRoundType.None, RewardType = TourneyRewardType.None };
            SeedPolicies();
            // set Default value for membership
            SeedUSATTMembership();
            PageMode = pageMode;
            ClubDomain = clubDomain;
            CurrentStep = pageStep;
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(clubDomain);
            Seed();
            ClubId = clubId;
            IsVendor = isVendor;
            SeedOUtSourcers();

        }
        public TTTourneyCreateEditViewModels(TTTourney tTTourney, string clubDomain, bool fromTemplate = false, int clubId = 0, bool isVendor = false)
            : this()
        {
            // set event for a special outsourcer
            ClubId = clubId;
            IsVendor = isVendor;
            IsVendorEvent = string.IsNullOrEmpty(tTTourney.OutSourcerClubDomain) ? EventSponserType.Vendor : EventSponserType.OutSourcer;
            // Realize event is outsourcer or not
            IsOutsourcerEvent = !string.IsNullOrEmpty(tTTourney.OutSourcerClubDomain);
            OutSourcerClubDomain = tTTourney.OutSourcerClubDomain;
            RegisterDeadLine = tTTourney.RegisterDeadLine;

            if (IsOutsourcerEvent)
            {
                //Get outsourcer club domain from db
                OutsourcerClubName = Ioc.IClubDbService.GetClubName(OutSourcerClubDomain);
            }

            SeedOUtSourcers();

            // laod some basic info of clubs
            ClubBasicInfo basicclubInfo = Ioc.IClubDbService.GetBasicInfo(tTTourney.ClubDomain);
            ClubDomain = clubDomain;
            EventSearchField = new EventSearchField
            {
                Gender = tTTourney.EventSearchField.Gender,
                Level = tTTourney.EventSearchField.Level,
                MaxAge = tTTourney.EventSearchField.MaxAge,
                MinAge = tTTourney.EventSearchField.MinAge,
                MaxGrade = tTTourney.EventSearchField.MaxGrade == SchoolGradeType.College || tTTourney.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool ? SchoolGradeType.None : tTTourney.EventSearchField.MaxGrade,
                MinGrade = tTTourney.EventSearchField.MinGrade == SchoolGradeType.College || tTTourney.EventSearchField.MaxGrade == SchoolGradeType.NotInSchool ? SchoolGradeType.None : tTTourney.EventSearchField.MinGrade,

            };

            EventSearchTags = tTTourney.EventSearchTags;
            DisplayClubLocationViewModel = new DisplayClubLocationViewModel(tTTourney.ClubDomain)
                {
                    SelectedClubLocationId = tTTourney.ClubLocation.Id
                };

            AsTemplate = !fromTemplate && tTTourney.AsTemplate;
            FromTemplate = fromTemplate;
            Name = tTTourney.Name;
            if (tTTourney.OfflineRegChecked.HasValue)
            {
                OfflineRegChecked = tTTourney.OfflineRegChecked.Value;
            }
            FlyerType = tTTourney.FlyerType;
            ExternalRegUrl = tTTourney.ExternalRegUrl;
            Domain = tTTourney.Domain;
            ClubDomain = tTTourney.ClubDomain;
            ClubName = basicclubInfo.Name;
            ItemId = tTTourney.ItemId;
            Name = tTTourney.Name;
            AddressName = tTTourney.AddressName;
            PostalAddress = tTTourney.ClubLocation.PostalAddress;
            EventStatus = tTTourney.EventStatus;
            MetaData = tTTourney.MetaData;
            SubTitle1 = tTTourney.SubTitle1;
            SubTitle2 = tTTourney.SubTitle2;
            Referee = tTTourney.Referee;
            Referee_Email = tTTourney.Referee_Email;
            Referee_Phone = tTTourney.Referee_Phone;
            Umpire = tTTourney.Umpire;
            Umpire_Email = tTTourney.Umpire_Email;
            Umpire_Phone = tTTourney.Umpire_Phone;
            EventStaff = tTTourney.EventStaff;
            EventStaff_Email = tTTourney.EventStaff_Email;
            EventStaff_Phone = tTTourney.EventStaff_Phone;
            Description = tTTourney.Discription;
            FileName = tTTourney.FileName;
            Equipment = tTTourney.Equipment;
            Rules = tTTourney.Rules;
            Venue = tTTourney.Venue;
            TimeZone = tTTourney.TimeZone;
            ParkingInfo = tTTourney.ParkingInfo;
            TemplateName = tTTourney.TemplateName;

            Donation.Checked = tTTourney.Donation.Checked;
            Donation.Title = tTTourney.Donation.Title;
            Donation.Description = tTTourney.Donation.Description;
            LocationName = tTTourney.ClubLocation.Name;

            // set Hotel value
            if (tTTourney.Hotel.Checked != false) // no hotel rate, set optional param
            {
                HotelChecked = true;
                HotelName = tTTourney.Hotel.Name;
                HotelRate = tTTourney.Hotel.Rate;
                HotelRateCode = tTTourney.Hotel.RateCode;
                HotelPhone = tTTourney.Hotel.Phone;
                HotelSite = tTTourney.Hotel.Site;
                HotelDescription = tTTourney.Hotel.Description;
            }
            else // no rate
            {
                HotelChecked = false;
                HotelName = string.Empty;
                HotelRate = null;
                HotelRateCode = string.Empty;
                HotelPhone = string.Empty;
                HotelSite = string.Empty;
                HotelDescription = string.Empty;
            }

            // get & Set Info for Image Gallery
            List<Domain.ImageGalleryItem> images = Ioc.IImageGalleryDbService.GetImageGalleryEvent(tTTourney.Domain, tTTourney.ClubDomain);
            Gallery = new List<TTTourneyPageImageGalleryItemViewModel>();
            foreach (var item in images)
            {
                Gallery.Add(new TTTourneyPageImageGalleryItemViewModel()
                {
                    OriginUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, tTTourney.ClubDomain, item.Path).AbsoluteUri,
                    ThumbnailUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, tTTourney.ClubDomain, tTTourney.Domain + "/ImageGallery/" + Path.GetFileNameWithoutExtension(item.Path) + "_Thumbnail" + Path.GetExtension(item.Path)).AbsoluteUri
                });
            }


            string logoUri = string.Empty;
            if (string.IsNullOrEmpty(basicclubInfo.Domain) == false &&
                            string.IsNullOrEmpty(basicclubInfo.Logo) == false)
            {
                LogoUri = Utilities.GetClubLogoURL(basicclubInfo.Domain, basicclubInfo.Logo, Sport);
            }

            // check if club has affiliator
            bool hasAffilator = false;
            string affiliatorLogoUri = string.Empty;
            string affiliatorDomain = string.Empty;
            string affiliatorName = string.Empty;
            if (basicclubInfo.AffiliatorID.HasValue)
            {
                hasAffilator = true;
                IClubDb clubDb = Infrastructure.Helper.Ioc.IClubDbService;
                Club affiliatorClub = clubDb.GetAffiliatorClubByAffiliatorID(basicclubInfo.AffiliatorID.Value);

                if (affiliatorClub != null)
                {
                    affiliatorLogoUri = Utilities.GetClubLogoURL(affiliatorClub.Domain, affiliatorClub.Logo, affiliatorClub.CategoryId);
                    affiliatorDomain = affiliatorClub.Domain;
                    affiliatorName = affiliatorClub.Name;
                }
            }
            HasAffiliator = hasAffilator;
            AffiliatorLogoUri = affiliatorLogoUri;
            AffiliatorDomain = affiliatorDomain;
            AffiliatorName = affiliatorName;

            SeedPolicies();

            // set Map Info
            Map = new GoogleMapsViewModel()
            {
                Name = AddressName,
                PostalAddress = PostalAddress
            };


            // set Policy Info
            foreach (Policy item in tTTourney.Policys)
            {
                switch (item.Category)
                {
                    case PolicyCategories.Refund:
                        Refund.Set(item);
                        break;

                    case PolicyCategories.Waiver:
                        Waiver.Set(item);
                        break;

                    case PolicyCategories.MedicalRelease:
                        MedicalRelease.Set(item);
                        break;

                    case PolicyCategories.Proof:
                        Proof.Set(item);
                        break;

                    case PolicyCategories.General:
                        General.Set(item);
                        break;

                    default:
                        throw new NotSupportedException(string.Format(Constants.F_PolicyCategoryNotSupported, item.Category));
                }
            }

            //foreach (var item in tTTourney.ChargeDiscounts.Where(p => p.Category == ChargeDiscountCategories.USATTMembership))
            //{
            //    USATTMembership.Add(new ChargeDiscountViewModel(item));
            //}

            //foreach (ChargeDiscount item in tTTourney.ChargeDiscounts)
            //{
            //    switch (item.Category)
            //    {
            //        case ChargeDiscountCategories.USATTMembership:
            //            USATTMembership.Add(new ChargeDiscountViewModel(item));
            //            break;
            //        default:
            //            throw new NotSupportedException(string.Format(Constants.F_ChargeDiscountCategoryNotSupported, item.Category));
            //    }
            //}

            if (USATTMembership.Count == 0) // db did not contain membership, use default
            {
                SeedUSATTMembership();
            }
            else
            {
                USATTMembershipChecked = true;
            }

            if (tTTourney.OnlineRegistrationDeadLine != null)
            {
                OnlineRegistrationDeadLine = tTTourney.OnlineRegistrationDeadLine;
            }

            if (tTTourney.CheckInDeadLine != null)
            {
                CheckInDeadLine = tTTourney.CheckInDeadLine;
            }

            foreach (var item in tTTourney.TTTourney_Id)
            {
                //item.Event = GetSchMonDayName(item.StartDate,item.EndDate);
                Events.Add(new TTTourneyEventViewModels(item));
            }

            EventJson = Events.ToJson();

            // check user for Is Admin
            IsAdmin = WebMatrix.WebData.WebSecurity.CurrentUserId == basicclubInfo.UserId;

            // check Event for Expire
            DateTime expireDate = Utilities.TourneyExpireDate(OnlineRegistrationDeadLine.Value);
            Expired = Utilities.IsEventExpired(expireDate, TimeZone);

            // Meta tag 
            MetaTags = new PageMetaDataViewModel()
            {
                PageTitle =
                            this.Name +
                            Constants.C_Space +
                            "by" +
                            Constants.C_Space +
                            this.ClubName +
                            Constants.S_SpaceDashSpace +
                            Constants.Category_TableTennis +
                            Constants.C_Space + SubDomainCategories.Tourney,

                MetaKeywords = string.Join(Constants.S_CommaSpace, new string[] 
                            { 
                                this.Name,
                                this.ClubName,
                                Constants.Category_TableTennis + Constants.C_Space + SubDomainCategories.Tourney, 
                                Utilities.StringNullHandle(this.PostalAddress .City,string.Empty), 
                                Utilities.GetAbbreviationState(this.PostalAddress .State) 
                            }),

                MetaDesc = this.Description,
            };
        }

        public TTTourneyEventType GetEventType(int age)
        {
            if (age >= 5 && age < 19)
            {
                return TTTourneyEventType.Cadets_Juniors;
            }
            else if (age >= 19 && age < 40)
            {
                return TTTourneyEventType.YoungMen_Women;
            }
            throw new NotSupportedException();

        }

        private void SeedPolicies()
        {
            IStandardPolicyDb policyDb = Ioc.IStandardPolicyDbService;
            StandardPolicy[] policies = policyDb.Get();

            // Note: PolicyCategories start @ index 1, must reduce by one

            Refund = new PolicyViewModel() { Category = PolicyCategories.Refund, Name = policies[(int)PolicyCategories.Refund - 1].Name, Desc = policies[(int)PolicyCategories.Refund - 1].Description, IsChecked = false,Field1 = 14,Field2 = 0 };
            Waiver = new PolicyViewModel() { Category = PolicyCategories.Waiver, Name = policies[(int)PolicyCategories.Waiver - 1].Name, Desc = policies[(int)PolicyCategories.Waiver - 1].Description, IsChecked = false };
            MedicalRelease = new PolicyViewModel() { Category = PolicyCategories.MedicalRelease, Name = policies[(int)PolicyCategories.MedicalRelease - 1].Name, Desc = policies[(int)PolicyCategories.MedicalRelease - 1].Description, IsChecked = false };
            General = new PolicyViewModel() { Category = PolicyCategories.General, Name = policies[(int)PolicyCategories.General - 1].Name, Desc = policies[(int)PolicyCategories.General - 1].Description, IsChecked = false };
            Proof = new PolicyViewModel() { Category = PolicyCategories.Proof, Name = policies[(int)PolicyCategories.Proof - 1].Name, Desc = policies[(int)PolicyCategories.Proof - 1].Description, IsChecked = false };
        }

        private void SeedUSATTMembership()
        {
            ChargeDiscountViewModel m1 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = " Membership", Amount = 49 };
            ChargeDiscountViewModel m2 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = "Junior Membership", Amount = 25 };
            ChargeDiscountViewModel m3 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = "3-Year Membership", Amount = 130 };
            ChargeDiscountViewModel m4 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = "3-Year Jr Membership", Amount = 60 };
            ChargeDiscountViewModel m5 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = "Household Membership", Amount = 90 };
            ChargeDiscountViewModel m6 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = "3-Year Household Membership", Amount = 250 };
            ChargeDiscountViewModel m7 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = "5-Year Membership", Amount = 200 };
            ChargeDiscountViewModel m8 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = "FASTT PRO Membership", Amount = 30 };
            ChargeDiscountViewModel m9 = new ChargeDiscountViewModel { Category = ChargeDiscountCategories.USATTMembership, Desc = "3-Year FASTT PRO Membership", Amount = 70 };
            USATTMembership.Add(m1);
            USATTMembership.Add(m2);
            USATTMembership.Add(m3);
            USATTMembership.Add(m4);
            USATTMembership.Add(m5);
            USATTMembership.Add(m6);
            USATTMembership.Add(m7);
            USATTMembership.Add(m8);
            USATTMembership.Add(m9);
        }

        private void SeedOUtSourcers()
        {
            var outSourcers = Ioc.IClubVendorDb.GetSchoolsForClub(ClubId);
            OutSourcers = outSourcers.Select(m => new OutSourcerList
            {
                OutSourcerClubDomain = m.OutSourcerClubDomain,
                OutSourcerClubName = m.OutSourcerLocation.PostalAddress.Address + "(" + m.OutSourcerClubName + ")",
                OutSourcerLocation = m.OutSourcerLocation,

            }).ToList();
            OutSourcerSelectList = new SelectList(OutSourcers, "OutSourcerClubDomain", "OutSourcerClubName");

        }

        public void SeedClubLocation()
        {
            var clubLocations = Ioc.IClubLocationDb.GetClubLocations(ClubDomain);
            DisplayClubLocationViewModel.ClubLocations = clubLocations;
        }

        private void SeedAges()
        {
            var ageList = new List<SelectListItem>();
            for (int i = 1; i <= 120; i++)
            {
                ageList.Add(new SelectListItem
                {
                    Text = i.ToString(),
                    Value = i.ToString()
                });
            }

            Ages = new SelectList(ageList, "Value", "Text");
        }

        public void Seed()
        {
            if (DisplayClubLocationViewModel.ClubLocations == null) SeedClubLocation();
        }
        private void AddPolicy(PolicyCategories catg, PolicyViewModel policyVm, TTTourney tTourney)
        {
            if (policyVm.IsChecked == true)
            {
                int f1 = -1;
                int f2 = -1;

                if (policyVm.Field1.HasValue)
                {
                    f1 = policyVm.Field1.Value;
                }

                if (policyVm.Field2.HasValue)
                {
                    f2 = policyVm.Field2.Value;
                }

                Policy p = new Policy { Category = catg, Field1 = f1, Field2 = f2 ,Description = policyVm.Desc};
                tTourney.Policys.Add(p);
            }
        }

        // set program for lucene
        public static string SetProgram(List<TTTourneyEventViewModels> Events)
        {
            // add list Program
            List<string> program = new List<string>();
            foreach (var item in Events)
            {
                program.Add(item.Label);
            }
            return program == null || program.Count == 0 ? string.Empty : string.Join(", ", program);

        }

        // set Prize for lucene
        public static string SetPrize(List<TTTourneyEventViewModels> Events)
        {
            // count of Prize 
            var countPrize = Events.Where(x => !string.IsNullOrEmpty(x.Prize)).Count();

            // check Event for only Scholastic
            if (countPrize.Equals(0))
            {
                return TourneyRewardType.Scholastic.ToString();
            }
            else
            {
                List<int> minPrize = new List<int>();
                List<int> maxPrize = new List<int>();
                foreach (var item in Events)
                {
                    if (!string.IsNullOrEmpty(item.Prize))
                    {
                        minPrize.Add(int.Parse(item.Prize.Split(',').Min()));
                        maxPrize.Add(int.Parse(item.Prize.Split(',').Max()));
                    }
                }
                if (Events.Count != countPrize)
                {
                    return minPrize.Min() + "-" + maxPrize.Max();
                }
                else if (minPrize.Min().Equals(minPrize.Max()))
                {
                    return minPrize.Min().ToString();
                }
                else
                {
                    return minPrize.Min() + "-" + maxPrize.Max();
                }

            }
        }

        public TTTourney ToTTTourney(string domain, bool setDomain = false)
        {
            var tTourney = new TTTourney();
            var clubLoc = Ioc.IClubLocationDb.GetClubLocation(DisplayClubLocationViewModel.SelectedClubLocationId.HasValue ? DisplayClubLocationViewModel.SelectedClubLocationId.Value : ClubLocationId);
            tTourney.ClubLocation = clubLoc;
            PostalAddress = clubLoc.PostalAddress;
            tTourney.ClubDomain = ClubDomain;
            tTourney.Name = Name;
            tTourney.Domain = setDomain ? Utilities.GenerateSubDomainName(ClubDomain, Name) : domain;
            tTourney.FlyerType = FlyerType;
            tTourney.RegisterDeadLine = RegisterDeadLine;

            tTourney.ItemId = ItemId;
            tTourney.EventStatus = EventStatus;
            tTourney.MetaData = MetaData;
            tTourney.SubTitle1 = SubTitle1;
            tTourney.SubTitle2 = SubTitle2;
            tTourney.Referee = Referee;
            tTourney.Referee_Email = Referee_Email;
            tTourney.Referee_Phone = Referee_Phone;
            tTourney.Umpire = Umpire;
            tTourney.Umpire_Email = Umpire_Email;
            tTourney.Umpire_Phone = Umpire_Phone;
            tTourney.EventStaff = EventStaff;
            tTourney.EventStaff_Email = EventStaff_Email;
            tTourney.EventStaff_Phone = EventStaff_Phone;
            tTourney.OfflineRegChecked = OfflineRegChecked;
            tTourney.ExternalRegUrl = ExternalRegUrl;
            tTourney.Discription = Utilities.GetDescription(Description);
            tTourney.TemplateName = TemplateName;

            tTourney.FileName = FileName;
            tTourney.Equipment = Equipment;
            tTourney.Rules = Rules;
            tTourney.Venue = Venue;
            tTourney.ParkingInfo = ParkingInfo;
            tTourney.AsTemplate = AsTemplate;
            tTourney.OutSourcerClubDomain = IsVendorEvent == EventSponserType.OutSourcer ? OutSourcerClubDomain : null;

            tTourney.EventSearchField = new EventSearchField
            {
                MinAge = EventSearchField.MinAge,
                MaxAge = EventSearchField.MaxAge,
                MinGrade = EventSearchField.MinGrade.HasValue && EventSearchField.MinGrade.Value != SchoolGradeType.None ? EventSearchField.MinGrade : null,
                MaxGrade = EventSearchField.MaxGrade.HasValue && EventSearchField.MaxGrade.Value != SchoolGradeType.None ? EventSearchField.MaxGrade : null,
                Level = EventSearchField.Level.HasValue && EventSearchField.Level.Value != Level.None ? EventSearchField.Level : null,
                Gender = EventSearchField.Gender.HasValue ? EventSearchField.Gender : null,

            };

            tTourney.EventSearchTags = EventSearchTags;

            tTourney.ItemId = Utilities.GenerateItemId();
            tTourney.EventStatus = EventStatusCategories.open; 
            DateTime now = DateTime.Now;
            tTourney.MetaData = new MetaData { DateCreated = now, DateUpdated = now };
            tTourney.OfflineRegChecked = OfflineRegChecked;
            tTourney.ExternalRegUrl = ExternalRegUrl;
            Domain = tTourney.Domain;


            //Donation
            tTourney.Donation.Checked = Donation.Checked;
            if (!Donation.Checked)
            {// if donation is unchecked set description and title null
                tTourney.Donation.Description = null;
                tTourney.Donation.Title = null;
            }
            else
            {// if donation is checked set description and title to model value
                tTourney.Donation.Description = Donation.Description;
                tTourney.Donation.Title = Donation.Title;
            }

            //tTourney.ExternalRegUrl = ExternalRegUrl;
            // hotel
            if (HotelChecked) // no hotel rate, set optional param
            {
                tTourney.Hotel = new Hotel { Name = HotelName, Checked = HotelChecked, Rate = HotelRate, RateCode = HotelRateCode, Phone = HotelPhone, Description = HotelDescription, Site = HotelSite };
            }
            else // no rate
            {
                tTourney.Hotel = new Hotel { Name = string.Empty, Checked = HotelChecked, Rate = -1, RateCode = HotelRateCode, Description = string.Empty, Phone = string.Empty, Site = string.Empty };
            }


            //foreach (NoteViewModel nvm in PrizeNotes)
            //{
            //    Note n = new Note { Category = NoteCategories.Prize, Desc = nvm.Desc };
            //    tTourney.Notes.Add(n);
            //}

            // set OnlineRegistrationDeadLine

            if (OnlineRegistrationDeadLine.HasValue)
            {
                tTourney.OnlineRegistrationDeadLine = OnlineRegistrationDeadLine.Value;
            }

            // set CheckInDeadLine

            if (CheckInDeadLine.HasValue)
            {
                tTourney.CheckInDeadLine = CheckInDeadLine.Value;
            }

            // add Events
            foreach (var item in Events)
            {
                tTourney.TTTourney_Id.Add(item.ToTTTourneyEvent(Domain, ClubDomain, item.Id));
            }

            // add USATTMembership
            if (USATTMembershipChecked == true)
            {
                foreach (ChargeDiscountViewModel cvm in USATTMembership)
                {
                    ChargeDiscount c = new ChargeDiscount { Category = ChargeDiscountCategories.USATTMembership, Amount = cvm.Amount.Value, Desc = cvm.Desc };
                    tTourney.ChargeDiscounts.Add(c);
                }
            }

            var startDate = Events.Select(m => DateTime.Parse(m.StartDate)).Min();
            var endDate = Events.Select(m => DateTime.Parse(m.EndDate)).Max();
            //tTourney.DisplayDate = GetSchMonDayName(startDate, endDate);

            // add Policies

            AddPolicy(PolicyCategories.Refund, Refund, tTourney);
            AddPolicy(PolicyCategories.Waiver, Waiver, tTourney);
            AddPolicy(PolicyCategories.MedicalRelease, MedicalRelease, tTourney);
            AddPolicy(PolicyCategories.General, General, tTourney);
            AddPolicy(PolicyCategories.Proof, Proof, tTourney);

            // set timezone
            tTourney.TimeZone = TimeZoneHelper.GetTimeZone(clubLoc.PostalAddress.Lat, clubLoc.PostalAddress.Lng);

            return tTourney;
        }

    }

    public class TTTourneyPageImageGalleryItemViewModel
    {
        public string OriginUrl { get; set; }
        public string ThumbnailUrl { get; set; }
    }

    public class TTTourneyEventViewModels
    {
        [StringLength(128)]
        public string ItemId { get; set; }

        public int Id { get; set; }

        public string Domain { get; set; }

        public string ClubDomain { get; set; }

        public int RowNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayName("Start Date")]
        public string StartDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("End Date")]
        public string EndDate { get; set; }

        public string DisplayEventDate
        {
            get
            {
                DateTime startDate = Convert.ToDateTime(StartDate);
                DateTime endDate = Convert.ToDateTime(EndDate);
                return GetSchMonDayName(startDate, endDate);
            }
        }

        public string EventDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayName("Time")]
        public /*TimeSpan?*/string Time { get; set; }

        [DisplayName("Gender")]
        public Nullable<GenderCategories> Gender { get; set; }

        [Required]
        [DisplayName("Entry price")]
        [Range(1, int.MaxValue, ErrorMessage = "Entry price must be greater than 0")]
        public decimal? EF { get; set; }

        [Required]
        [DisplayName("Format")]
        public TTTourneyRoundType RoundType { get; set; }

        [Required]
        [DisplayName("Prize")]
        public TourneyRewardType RewardType { get; set; }

        [DisplayName("Note")]
        public string RewardNote { get; set; }

        public string Prize
        {

            get
            {
                try
                {
                    if (RewardType.Equals(TourneyRewardType.Cash))
                    {
                        List<int> prizes = new List<int>();

                        if (Prize1.HasValue) { prizes.Add(Prize1.Value); }
                        if (Prize2.HasValue) { prizes.Add(Prize2.Value); }
                        if (Prize3.HasValue) { prizes.Add(Prize3.Value); }
                        if (Prize4.HasValue) { prizes.Add(Prize4.Value); }
                        if (Prize5.HasValue) { prizes.Add(Prize5.Value); }
                        if (Prize6.HasValue) { prizes.Add(Prize6.Value); }
                        if (Prize7.HasValue) { prizes.Add(Prize7.Value); }
                        if (Prize8.HasValue) { prizes.Add(Prize8.Value); }

                        return PsizeToString(prizes);
                    }
                    Prize1 = Prize2 = Prize3 = Prize4 = Prize5 = Prize6 = Prize7 = Prize8 = null;
                    return string.Empty;
                }
                // for the first initials 
                catch (Exception)
                {

                    return string.Empty;
                }
            }
        }

        public string Label
        {
            get
            {
                try
                {
                    return Ioc.ITTTourneyDbService.GetEventName(EventType, Attendtype, Age, RoundType, Rating, Gender);
                }
                // for the first initials 
                catch
                {
                    return string.Empty;
                }
            }
        }

        public string LabelToView
        {
            get
            {
                try
                {
                    string lable = string.Empty;
                    string[] str = Ioc.ITTTourneyDbService.GetEventName(EventType, Attendtype, Age, RoundType, Rating, Gender).Split(' ');
                    if (str.Count() > 0)
                    {
                        for (int i = 0; i < str.Length - 1; i++)
                        {
                            lable += " " + str[i];
                        }
                    }
                    return lable;
                }
                // for the first initials 
                catch
                {
                    return string.Empty;
                }
            }
        }

        public Nullable<int> Prize1 { get; set; }

        public Nullable<int> Prize2 { get; set; }

        public Nullable<int> Prize3 { get; set; }

        public Nullable<int> Prize4 { get; set; }

        public Nullable<int> Prize5 { get; set; }

        public Nullable<int> Prize6 { get; set; }

        public Nullable<int> Prize7 { get; set; }

        public Nullable<int> Prize8 { get; set; }

        [Required]
        [DisplayName("Age")]
        //[Range(Constants.TableTennis_Age_Min, Constants.TableTennis_Age_Max, ErrorMessage = "{0} must be a number between {1} and {2}")]
        public Nullable<int> Age { get; set; }

        [DisplayName("Rating")]
        public string Rating { get; set; }

        [Required]
        [DisplayName("Type")]
        public TTTourneyAttendtype Attendtype { get; set; }

        private Nullable<int> capacity;

        [DisplayName("Capacity")]
        public Nullable<int> Capacity
        {
            get
            {
                return capacity;
            }
            set
            {
                capacity = value == 0 ? null : value;
            }
        }
        public TTTourneyEventType EventType { get; set; }

        public string GetSchMonDayName(DateTime start, DateTime end)
        {
            string s = start.ToString("MMMM ");
            s = s + start.Day;

            if (end.Subtract(start).Days > 0)
            {
                if (start.Month == end.Month)
                {
                    s = s + Constants.S_Dash + end.Day;
                }
                else
                {
                    s = s + Constants.S_Dash + end.ToString(" MMMM ") + end.Day;
                }
            }

            return s;
        }

        public TTTourneyEventViewModels()
        {

        }

        public TTTourneyEventViewModels(TTTourneyEvent TourneyEvent)
        {
            Id = TourneyEvent.Id;
            ItemId = TourneyEvent.ItemId;
            Domain = TourneyEvent.Domain;
            ClubDomain = TourneyEvent.ClubDomain;
            EventType = TourneyEvent.EventType;
            StartDate = TourneyEvent.StartDate.ToString();
            EndDate = TourneyEvent.EndDate.ToString();
            Time = Utilities.FormatTimeSpan(TourneyEvent.Time);
            Gender = TourneyEvent.Gender;
            EF = TourneyEvent.EF;
            RoundType = TourneyEvent.RoundType;
            RewardType = TourneyEvent.RewardType;
            RewardNote = TourneyEvent.RewardNote;
            Prize1 = TourneyEvent.Prize1;
            Prize2 = TourneyEvent.Prize2;
            Prize3 = TourneyEvent.Prize3;
            Prize4 = TourneyEvent.Prize4;
            Prize5 = TourneyEvent.Prize5;
            Prize6 = TourneyEvent.Prize6;
            Prize7 = TourneyEvent.Prize7;
            Prize8 = TourneyEvent.Prize8;
            Age = TourneyEvent.Age;
            Rating = TourneyEvent.Rating;
            Attendtype = TourneyEvent.Attendtype;
            Capacity = TourneyEvent.Capacity;

        }


        string PsizeToString(List<int> prizes)
        {
            return prizes == null || prizes.Count == 0 ? string.Empty : string.Join(", ", prizes);
        }

        public TTTourneyEvent ToTTTourneyEvent(string domain, string clubDomain, int Id)
        {
            TTTourneyEvent ttEvent = new TTTourneyEvent();
            // update properties
            ttEvent.Id = Id;
            ttEvent.Domain = domain;
            ttEvent.ClubDomain = clubDomain;
            ttEvent.EventType = EventType;
            if (!string.IsNullOrEmpty(StartDate))
            {
                ttEvent.StartDate = DateTime.Parse(StartDate);
            }
            if (!string.IsNullOrEmpty(EndDate))
            {
                ttEvent.EndDate = DateTime.Parse(EndDate);
            }
            if (!string.IsNullOrEmpty(Time))
            {
                ttEvent.Time = TimeSpan.Parse(Time.Split(' ')[0]);
            }
            ttEvent.Gender = Gender;

            if (EF.HasValue)
            {
                ttEvent.EF = EF.Value;
            }
            ttEvent.RewardType = RewardType;
            ttEvent.RoundType = RoundType;
            ttEvent.RewardNote = RewardNote;
            if (RewardType.Equals(TourneyRewardType.Scholastic))
            {
                Prize1 = Prize2 = Prize3 = Prize4 = Prize5 = Prize6 = Prize7 = Prize8 = null;
            }
            else
            {
                if (Prize1.HasValue)
                {
                    ttEvent.Prize1 = Prize1.Value;
                }

                if (Prize2.HasValue)
                {
                    ttEvent.Prize2 = Prize2.Value;
                }

                if (Prize3.HasValue)
                {
                    ttEvent.Prize3 = Prize3.Value;
                }

                if (Prize4.HasValue)
                {
                    ttEvent.Prize4 = Prize4.Value;
                }

                if (Prize5.HasValue)
                {
                    ttEvent.Prize5 = Prize5.Value;
                }

                if (Prize6.HasValue)
                {
                    ttEvent.Prize6 = Prize6.Value;
                }

                if (Prize7.HasValue)
                {
                    ttEvent.Prize7 = Prize7.Value;
                }

                if (Prize8.HasValue)
                {
                    ttEvent.Prize8 = Prize8.Value;
                }

            }
            if (Age.HasValue)
            {
                ttEvent.Age = Age.Value;
            }
            if (!String.IsNullOrEmpty(Rating))
            {
                ttEvent.Rating = Rating;
            }
            ttEvent.Attendtype = Attendtype;

            ttEvent.Capacity = Capacity;

            return ttEvent;
        }
    }
}