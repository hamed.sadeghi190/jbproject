﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
    [TestFixture]
    public class OrderTestScenario : BaseTestScenario
    {
        [Test]
        public void EditDropInOrder()
        {
            BaseUrl = DataDriver.SportClubURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            ExtraAPI.Wait(5000);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);
            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginAdmin();
            // Go to programs
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToProgramAndOrders();

            // Edit order
            var orderTest = new EditOrderTests(WebDriver);
            orderTest.EditDropInOrder();

            WebDriver.Close();
        }
    }
}