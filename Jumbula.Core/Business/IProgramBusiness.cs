﻿using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Charge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using Jumbula.Core.Domain.Generic;
using Jumbula.Core.Model.Generic;
using OperationStatus = Jumbula.Core.Data.OperationStatus;
using System.Web.Mvc;
using Jumbula.Core.Model.Program;

namespace Jumbula.Core.Business
{
    public interface IProgramBusiness
    {
        #region ProgramName

        string FormatTuitionOption(string tutionName, decimal amount, DateTime startDate, DateTime endDate);
        string GetEmailProgramDateWithScheduleTitle(ProgramSchedule schedule);
        string GetProgramScheduleTitleAndDate(OrderItem orderItem, string chessSchedule = "");
        decimal? CalculateProgramChargeAmount(Club club, Charge charge, List<EarlyBird> earlyBirds = null, bool isEarlyBird = false);
        decimal? CalculateWithSurcharge(Club club, Charge charge, decimal? programChargeAmount);
        string GetScheduleStartEndDate(ProgramSchedule schedule);

        string GetEmailProgramNameOnlyTuition(ProgramSchedule schedule, string tutionName);
        string GetProgramNameCustomReport(ProgramSchedule schedule, string tutionName);
        string GetEmailProgramDate(ProgramSchedule schedule);
        string GetEmailProgramName(ProgramSchedule schedule, string tutionName);
        string GetProgramNameOnlyTuition(ProgramSchedule schedule, string tutionName);
        string GetProgramName(OrderItem orderItem);
        string GetProgramName(ProgramSchedule schedule);

        string GetProgramName(string programName, DateTime startDate, DateTime endDate, string tutionName);
        string GetProgramName(string programName, string tutionName);

        string GetProgramName(ProgramSchedule schedule, string tutionName);
        #endregion

        List<SelectKeyValue<long>> GetClubProgramItems(int clubId);
        IQueryable<Program> GetClubPrograms(int clubId);
        string GetProgramNameWithScheduleTitleAndDate(OrderItem orderItem, string tutionName, string chessSchedule = "");
        string GradeRestrictionMessage(SchoolGradeType? minGrade, SchoolGradeType? maxGrade, bool applyAtProgramStart);
        string AgeRestrictionMessage(int? minAge, int? maxAge, bool applyAtProgramStart);
        List<ClassSessionsDateTime> ApplyHolidaysToClassDates(Club club, List<ClassSessionsDateTime> classdates, TimeOfClassFormation timeMode);
        void CheckProgramCapacity(IEnumerable<OrderItem> orderItems);
        string GetClassDaysSeperatedByCamma(Program program);
        List<KeyValuePair<string, string>> GetProgramChargesSpotsLeft(Program program, bool isTestMode);
        List<string> GetProgramChargesCapacities(Program program);
        IQueryable<Program> GetList();
        int? CapacityLeft(ProgramSchedule schedule, long? chargeId, bool isTestMode);
        IQueryable<Program> GetList(IEnumerable<long> programs);
        List<string> GetInstractorsInHomeSite(Program program);
        Program Get(long id);
        string GetStringGender(Program program);
        Program Get(long id, params Expression<Func<Program, object>>[] includes);
        Program Get(int clubId, string seasonDomain, string domain);
        int GetProgramWeeksNumber(Program program);
        Program GetByScheduleId(long scheduleId);
        IQueryable<Program> GetListWithOutSources(int clubId);
        IQueryable<Program> GetList(int clubId);

        IQueryable<Program> GetListBySeasonId(long seasonId);

        IQueryable<Program> GetListBySeasonId(List<long> seasonIds);

        IQueryable<Program> GetList(JbPSeasonCalendar programTypeCategory);

        IQueryable<Program> GetList(int clubId, string seasonDomain, string programName = "", DateTime? startDateCreated = null, DateTime? endDateCreated = null, SaveType? saveType = null, ProgramTypeCategory? typeCategory = null, int? instructorId = null);

        IQueryable<Program> GetInstructorPrograms(int instructorId);

        IQueryable<Program> GetInstructorPrograms(int instructorId, int clubId);

        OperationStatus Create(Program program);

        OperationStatus Update(Program program);

        OperationStatus Update(Program program, List<ProgramSchedule> schedules, List<SelectKeyValue<int>> numberOfClassDays = null);

        ProgramRegStatus GetRegStatus(Program program);

        ProgramStatusType GetStatus(Program program, bool isTestMode);

        ProgramRunningStatus GetRunningStatus(Program program);

        bool IsProgramExpired(DateTime deadline, Jumbula.Common.Enums.TimeZone timeZone);

        bool IsFull(Charge charge, bool isTestMode);

        bool IsFull(Program program, bool isTestMode, bool checkTuitions = false);

        bool IsConfirmed(Program program, bool isTestMode);

        bool IsProgramScheduleProrate(ProgramSchedule schedule, Charge charge);

        DateTime? StartDate(Program program);

        DateTime? EndDate(Program program);

        DateTime RegisterStartDate(Program program);

        DateTime RegisterEndDate(Program program);

        List<ScheduleSession> GenerateSessions(DateTime StartDate, DateTime? endDate, List<ProgramScheduleDay> days, ContinueType contineuType, int occurances);

        OperationStatus UpdateSessionItem(ScheduleSession sessionItem, long programId);

        OperationStatus DeleteSessionItem(int sessionItemId, long programId);

        OperationStatus UpdateSessions(List<ScheduleSession> sessions, long programId);

        List<ScheduleAttendance> GetSessionAttendance(long programId, int sessionId);

        List<ScheduleAttendance> GetSessionAttendance_V2_2_1(long programId, int sessionId, int page, int pageSize, ref int totalCount);

        List<StudentAttendance> GetSessionSubscriptionAttendance(long programId, int sessionId);

        List<StudentAttendance> GetSessionSubscriptionAttendance_V2_2_1(long programId, int sessionId, int page, int pageSize, ref int totalCount);

        List<StudentAttendance> GetBeforeAfterAttendance(long programId, DateTime date, int page, int pageSize, ref int totalCount);

        List<ScheduleAttendance> GetSessionAttendance(List<int> ids);

        AttendanceStatus? GetStudentAttendanceStatus(int profileId, long scheduleId, int sessionId);

        OperationStatus UpdateAttendances(List<ScheduleAttendance> attendances);

        OperationStatus UpdateAttendancesPickup(List<ScheduleAttendance> attendances);

        List<ScheduleAttendance> GetStudentAttendance(long programId, int profileId);

        IEnumerable<ScheduleSession> GetSessions(Program program);

        ScheduleSession GetSession(Program program, int sessionId);

        List<ScheduleSession> GetSessions(IEnumerable<ProgramSchedule> schedules);

        bool IsSessionsEdited(long scheduleId);

        List<ScheduleAttendance> GetProgramAttendance(long programId);

        List<ScheduleAttendance> GetProgramAttendance_V2_2_1(long programId, int page, int pageSize, ref int totalCount);

        List<ScheduleAttendance> GetProgramSubscriptionAttendance(long programId, DateTime? dateTime);

        List<ScheduleAttendance> GetProgramSubscriptionAttendance_V2_2_1(long programId, DateTime? dateTime, int page, int pageSize, ref int totalCount);

        List<ScheduleAttendance> GetProgramBeforeAfterAttendance_V2_2_1(long programId, DateTime? dateTime, int page, int pageSize, ref int totalCount);

        List<ScheduleAttendance> UpdateScheduleAttendance(List<ScheduleAttendance> attendances);

        List<ProgramSessionsItemViewModel> GenerateSessionItems(Program program, SchoolGradeType? schoolGradeType);

        IQueryable<ClubWaiver> GetWaivers(long id);

        OperationStatus Delete(long id);

        OperationStatus Freeze(long id);

        OperationStatus UnFreeze(long id);

        Program Copy(long id);

        JbForm GetJbForm(int id);

        string GenerateSubDomainName(string clubDomain, string seasonDomain, string name);

        bool CheckAgeRestriction(ProgramSchedule schedule, int age);

        bool CheckAgeRestriction(Program program, int age);

        string CheckGenderRestriction(ProgramSchedule schedule, Genders gender);

        bool CheckGenderRestriction(Program program, Genders gender);

        int? GetMaxAge(Program program);

        int? GetMinAge(Program program);

        SchoolGradeType? GetMinGrade(Program program);

        SchoolGradeType? GetMaxGrade(Program program);

        bool HasOrder(Program program, bool completedOnly = false);

        ProgramRegStatus GetScheduleRegStatus(ProgramSchedule schedule);

        IQueryable<Program> GetSeasonPrograms(long seasonId);

        decimal CalculateProratePrice(ProgramSchedule schedule, Charge charge);

        int CalculateTotalSessions(ProgramSchedule schedule);

        int CalculateLeftSessions(ProgramSchedule schedule);

        Genders GetGender(Program program);
        bool HasGenderRestriction(ProgramSchedule programSchedule);

        IQueryable<Charge> GetAllSchedulesCharges(List<long> programIDs);

        Program CopyInSeason(Program program, long seasonId, bool forwardDates, int? monthNum);

        int? CapacityLeft(Program program, bool isTestMode);

        int? CapacityLeft(ProgramSchedule schedule, bool isTestMode);

        int? CapacityLeft(Charge charge, bool isTestMode);

        string CapacityLeftFormated(Charge charge, bool isTestMode);

        ProgramStatusType GetProgramStatusType(Program program, bool isTestMode);

        bool IsProgramBelongsToClub(int clubId, long programId);
        OperationStatus InsertRespond(ProgramRespondToInvitation respond);

        IQueryable<Program> GetList(JbPSeasonGrid programTypeCategory);

        string GetEmProgramStatus(Program program, bool isTestMode);

        string GenerateGradeInfoLabel(Program program);
        string GenerateGradeInfoLabel(SchoolGradeType? min, SchoolGradeType? max);
        List<ClassSessionsDateTime> GetClassDates(ProgramSchedule programSchedule, TimeOfClassFormation timeMode = TimeOfClassFormation.None);
        List<ProgramScheduleDay> GetClassDays(Program program);
        string GenerateGradeInfoLabelForReport(Program program);
        List<ProgramScheduleDay> GetClassDays(Program program, DateTime date);
        List<ProgramScheduleDay> GetClassDays(ProgramSchedule programSchedule);
        List<ChargeItemViewModel> GetClassFees(Program program, CurrencyCodes currency);
        IQueryable<ClubStaff> GetProgramInstructor(int programId);
        bool CheckGradeRestriction(Program program, SchoolGradeType grade);
        string CheckGradeRestriction(ProgramSchedule scd, object gradeObj);
        int GetRegisteredCount(Program program, bool isTestMode);
        int GetRegisteredCount(ProgramSchedule schedule, bool isTestMode);

        int GetRegisteredCount(Charge charge, bool isTestMode);

        decimal GetTotalRegistration(Program program, bool isTestMode);

        int? GetEMRate(Program program);
        List<Coupon> GetProviderFundedCoupons(Program program, List<Coupon> coupons);

        List<Coupon> GetCashPaymentCoupons(Program program, List<Coupon> coupons);

        List<Coupon> GetPTAPTOFundedCoupons(Program program, List<Coupon> coupons);
        decimal GetTotalAmtProviderFundedScholarships(Program program, List<Coupon> coupons);

        decimal GetEmClassFee(Program program);

        int TotalRegistration(Program program, bool isTestMode);

        int TotalPaidRegistration(Program program, List<OrderItem> OrderItemProgram, bool isTestMode);

        decimal GetProviderNoShowFines(Program program);

        decimal GetProviderLatePenaltyFines(Program program);
        OperationStatus AssignRooms(Dictionary<long, string> programRooms);

        OperationStatus AssignInstructorsPrograms(Dictionary<long, List<int>> programModels, int clubId);

        List<ProgramScheduleDay> GetProgramScheduleDays(ProgramSchedule programSchedule);

        void AddImageGalleryToProgram(Program program, List<string> paths, string name, string domain);

        string GenerateGradeInfoLabelForFlyer(Program program);

        string GenerateGradeInfoLabelForFlyer(SchoolGradeType? min, SchoolGradeType? max);
        DateTime GetEndDateFromSessionWeeks(List<DayOfWeek> weekDays, DateTime startDate, int sessionWeeks);
        TimeOfClassFormation GetClassScheduleTime(Program program);
        List<ScheduleSession> GetAllClassSessions(Program program);
        List<ScheduleSession> GetClassHolidayDates(Program program);

        string GenerateAgeInfoLabel(Program program);
        string GenerateAgeInfoLabel(int? min, int? max);
        string GetInstractors(Program program, bool includePhone = false);
        string FormatDays(List<DayOfWeek> allDaysProgram);
        List<DateTime> GetDatesBetweenTwoIntervals(int schoolId, DateTime programStartDate, DateTime programEndDate);
        IQueryable<Program> GetList(List<int> clubIds);

        List<string> GetNoClassDates(Program program);
        ProgramAttributeValue GetAttribute(Program program, AttributeName attributeName);
        List<ProgramAttributeValue> GetAttributes(Program program);
        List<ProgramAttributeValue> GetAttributes(long programId);
        void SetAttributes(Program program, Dictionary<AttributeName, string> KeyValues);
        IEnumerable<OrderItem> GetProgramOrderitems(long programId);
        List<Program> GetProgramsToday(int clubId, long seasonId, DateTime? dateTime);
        Charge GetProgramApplicationFee(Program program);
        OperationStatus UpdateCharges(Program program, List<ProgramSchedule> schedules);
        OperationStatus CreateScheduleParts(Program program, DateTime startDate, DateTime endDate, string paymentMode, string monthlyType, int dueDateDay, bool SetPartsCharges);
        List<ProgramSchedulePart> GetScheduleParts(long programId);
        OperationStatus CreateSchedulePartCharges(List<Charge> programCharges, ProgramSchedulePart schedulePart);
        List<ProgramSchedulePartCharge> GetSchedulePartCharges(long schedulePartId);
        OperationStatus UpdateScheduleParts(List<ProgramSchedulePart> ProgramSchedulePart);
        List<ProgramSchedulePart> GenerateSchedulePart(long programId, DateTime startDate, DateTime endDate, string paymentMode, string monthlyType, int dueDateDay);
        bool IsProgramSchedulePartEdited(long programId);
        ProgramSchedule GetSchedule(long scheduleId);
        int GetScheduleCapacity(ProgramSchedule programSchedule);
        List<ProgramSchedulePartCharge> GetSchedulePartCharges(Charge charge);
        bool GetFirstInstallmentInRegistrationTime(Program program);
        bool IsProgramSchedulePartEdited(Program program, DateTime newStartDate, DateTime newEndDate, string scheduleMode, string scheduleMonthlyType, int paymentDuedate);
        List<ProgramSchedulePart> GenerateSubscriptionScheduleParts(DateTime startDate, DateTime endDate, decimal amount, int dueDate);
        string ProgramPaymentPriod(ProgramSchedule schedule);
        List<ScheduleTimesViewModel> GetTimesProgram(ProgramSchedule schedule);
        List<ProgramSchedulePartCharge> GetProgramPartCharges(long chargeId);
        bool IsCapacityFull(List<Charge> charges);

        List<string> GetComboTime(List<ProgramSchedule> schedules);
        List<string> GetScheduleTime(ProgramSchedule schedule);
        decimal GetDropInTotalFee(Program program, List<long> sessionIds, ProgramSchedule schedule = null);

        ProgramSchedule GetScheduleByDate(Program program, DateTime date);

        List<ScheduleSession> GetSessions(ProgramSchedule schedule);

        bool IsTeamChessTourney(Program program);

        string GetProgramDateWithScheduleTitleAndSchedulePart(ProgramSchedule schedule, ProgramSchedulePart programSchedulePart);


        bool HasActiveOrder(ProgramSchedule programSchedule);
        
        bool CheckAndAddSecheduleProgramSession(Program program);

        object GetProgramSchedules(long programId);
        List<SelectListItem> GetKeyValueSeasonPrograms(long seasonId);

        bool HasGradeRestriction(ProgramSchedule programSchedule);

        bool HasAgeRestriction(ProgramSchedule programSchedule);

        List<SelectListItem> GetSeasonProgramItems(long seasonId);

        IEnumerable<ProgramSchedulePart> GetProgramDeletedScheduleParts(long programId, DateTime dateTime, bool editEndDate);

        string GetProgramPinUniqueKey(Program program, string userName);

        IEnumerable<ProgramCalendarViewModel> GetProgramCalendarItems(List<long> seasonIds, int clubId, string baseUrl, DateTime startDate, DateTime endDate);

        IEnumerable<ProgramSchedulePart> GetUpcomingSchedules(List<ProgramSchedulePart> programScheduleParts, DateTime today);

        OperationStatus UnFreezeSchedule(long id);

        OperationStatus FreezeSchedule(long id);
    }
}

