﻿using Jb.Framework.Common.Forms.Validation;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Jb.Framework.Common.Forms.JsonFormatter
{
    public class JbElementConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {

            IJbBaseElement element = value as JbBaseElement ?? value as IJbBaseElement;
            var properties = element.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));

            writer.WriteStartObject();

            writer.WritePropertyName("Type");
            serializer.Serialize(writer, element.GetType().Name);

            if (this.GetType().Assembly.GetName().Name != element.GetType().Assembly.GetName().Name)
            {
                if (element is ICustomElement)
                {
                    writer.WritePropertyName("Type.FullName");
                    var fullName = string.Format("{0},{1}", value.GetType().FullName, value.GetType().Assembly.GetName().Name);
                    serializer.Serialize(writer, fullName);
                }
                else
                {
                    writer.WritePropertyName("Type.FullName");
                    serializer.Serialize(writer, element.GetType().FullName);
                    writer.WritePropertyName("Type.AssemblyName");
                    serializer.Serialize(writer, element.GetType().Assembly.GetName().Name);
                }
            }

            writer.WritePropertyName("IsCustomElement");
            serializer.Serialize(writer, element is ICustomElement ? true : false);

            foreach (var property in properties)
            {
                //if (property.PropertyType == element.CurrentMode.GetType())
                //{
                //    writer.WritePropertyName(property.Name);
                //    var val = ((AccessMode)property.GetValue(element)).ToString();//  .GetValue(element);
                //    serializer.Serialize(writer, val);
                //}

                if (Attribute.IsDefined(property, typeof(JsonConverterAttribute)))
                {
                    var jsonAttr = property.GetCustomAttribute<JsonConverterAttribute>(false);
                    if (jsonAttr.ConverterType == typeof(StringEnumConverter))
                    {
                        writer.WritePropertyName(property.Name);
                        serializer.Serialize(writer, Enum.ToObject(property.PropertyType, property.GetValue(element)).ToString());
                    }
                }
                else
                {
                    writer.WritePropertyName(property.Name);
                    serializer.Serialize(writer, property.GetValue(element));
                }

            }

            writer.WriteEndObject();

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JbBaseElement jbElement;
            JObject jsonObject = JObject.Load(reader);
            return GetJbElement(jsonObject);
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(JbBaseElement).IsAssignableFrom(objectType);
        }

        #region private

        internal IJbBaseElement GetJbElement(JObject jsonObject)
        {
            IJbBaseElement jbElement;
            var properties = jsonObject.Properties().ToList();
            if (properties.All(p => p.Name != "Type"))
                return null;
            var elementType = (string)properties.Single(p => p.Name == "Type").Value;
            var typeFull = properties.SingleOrDefault(p => p.Name == "Type.FullName");
            var assemblyName = properties.SingleOrDefault(p => p.Name == "Type.AssemblyName");
            if (elementType == typeof(JbTextBox).Name)
            {
                jbElement = new JbTextBox();
                (jbElement as JbTextBox).Value = (string)properties.Single(p => p.Name == "Value").Value;
                if (properties.Any(p => p.Name == "PlaceHolder"))
                    (jbElement as JbTextBox).PlaceHolder = (string)properties.Single(p => p.Name == "PlaceHolder").Value;
                else
                    (jbElement as JbTextBox).PlaceHolder = string.Empty;
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            else if (elementType == typeof(JbTextEditor).Name)
            {
                jbElement = new JbTextEditor();
                (jbElement as JbTextEditor).Value = (string)properties.Single(p => p.Name == "Value").Value;
            }
            else if (elementType == typeof(JbNumber).Name)
            {
                jbElement = new JbNumber();
                (jbElement as JbNumber).Value = (string)properties.Single(p => p.Name == "Value").Value;
                if (properties.Any(p => p.Name == "PlaceHolder"))
                    (jbElement as JbNumber).PlaceHolder = (string)properties.Single(p => p.Name == "PlaceHolder").Value;
                else
                    (jbElement as JbNumber).PlaceHolder = string.Empty;

                if (properties.Any(p => p.Name == "MinVal") && !string.IsNullOrEmpty((string)properties.Single(p => p.Name == "MinVal").Value))
                    (jbElement as JbNumber).MinVal = (int)properties.Single(p => p.Name == "MinVal").Value;

                if (properties.Any(p => p.Name == "MaxVal") && !string.IsNullOrEmpty((string)properties.Single(p => p.Name == "MaxVal").Value))
                    (jbElement as JbNumber).MaxVal = (int)properties.Single(p => p.Name == "MaxVal").Value;
            }
            else if (elementType == typeof(JbEmail).Name)
            {
                jbElement = new JbEmail();
                (jbElement as JbEmail).Value = (string)properties.Single(p => p.Name == "Value").Value;
                if (properties.Any(p => p.Name == "PlaceHolder"))
                    (jbElement as JbEmail).PlaceHolder = (string)properties.Single(p => p.Name == "PlaceHolder").Value;
                else
                    (jbElement as JbEmail).PlaceHolder = string.Empty;
            }
            else if (elementType == typeof(JbParagraph).Name)
            {
                jbElement = new JbParagraph();
                (jbElement as JbParagraph).Value = (string)properties.Single(p => p.Name == "Value").Value;
            }
            else if (elementType == typeof(JbHidden).Name)
            {
                jbElement = new JbHidden();
                (jbElement as JbHidden).Value = (string)properties.Single(p => p.Name == "Value").Value;
            }
            else if (elementType == typeof(JbCheckBox).Name)
            {
                jbElement = new JbCheckBox();
                (jbElement as JbCheckBox).Value = (bool)properties.Single(p => p.Name == "Value").Value;
            }
            else if (elementType == typeof(JbDropDown).Name)
            {
                jbElement = new JbDropDown();
                (jbElement as JbDropDown).Items =
                    properties.Single(p => p.Name == "Items").Value.ToObject<List<JbElementItem>>();

            }
            else if (elementType == typeof(JbRadioList).Name)
            {
                jbElement = new JbRadioList();
                (jbElement as JbRadioList).Items =
                    properties.Single(p => p.Name == "Items").Value.ToObject<List<JbElementItem>>();

            }
            else if (elementType == typeof(JbCheckBoxList).Name)
            {
                jbElement = new JbCheckBoxList();
                (jbElement as JbCheckBoxList).Items =
                      properties.Single(p => p.Name == "Items").Value.ToObject<List<JbSelectedItem>>();

            }
            else if (elementType == typeof(JbSection).Name)
            {
                jbElement = new JbSection();
                var innerElements = properties.Single(p => p.Name == "Elements").Value.ToObject<List<object>>();
                foreach (var element in innerElements)
                {
                    (jbElement as JbSection).Elements.Add(GetJbElement(element as JObject));
                }
            }
            else if (elementType == typeof(JbAddress).Name)
            {
                jbElement = new JbAddress();
            }
            else
            {
                Type typ;
                if (typeFull == null)
                    typ = Type.GetType(string.Format("{0}.Forms.{1}, {0}", this.GetType().Assembly.GetName().Name, elementType), true);
                else if (typeFull.Value.ToString().Contains("USCFInfo"))
                    typ = Type.GetType("Jb.Framework.Common.Forms.USCFInfo" + ",Jb.Framework.Common", true);
                else
                    typ = Type.GetType((string)typeFull.Value, true);

                jbElement = (IJbBaseElement)Activator.CreateInstance(typ);
            }

            if (jbElement is IInputElement)
            {
                //if (properties.Any(p => p.Name == "IsRequired"))
                //    (jbElement as IInputElement).IsRequired =
                //        (bool)properties.Single(p => p.Name == "IsRequired").Value;
                //else
                //    (jbElement as IInputElement).IsRequired = false;
            }

            foreach (var property in properties)
            {
                if (property.Name.Equals("Type"))
                    continue;
                var proInfo = jbElement.GetType().GetProperty(property.Name);
                if (proInfo == null)
                    continue;
                if (proInfo.PropertyType == jbElement.AccessRole.GetType())
                {
                    var accessRoles = property.Value.ToObject<Dictionary<string, AccessMode>>();
                    proInfo.SetValue(jbElement, accessRoles);
                }
                else if (proInfo.PropertyType == jbElement.Validations.GetType())
                {
                    var validations = property.Value.ToObject<List<BaseElementValidator>>();
                    proInfo.SetValue(jbElement, validations);
                }
                else if (Attribute.IsDefined(proInfo, typeof(JsonConverterAttribute)))
                {
                    var jsonAttr = proInfo.GetCustomAttribute<JsonConverterAttribute>(false);
                    if (jsonAttr.ConverterType == typeof(StringEnumConverter))
                    {
                        var mode = property.Value.ToObject(proInfo.PropertyType);
                        proInfo.SetValue(jbElement, Enum.ToObject(proInfo.PropertyType, mode));
                    }

                }
                else
                {
                    if (typeof(System.Collections.IList).IsAssignableFrom(proInfo.PropertyType))
                        continue;
                    if (!string.IsNullOrEmpty((string)property.Value))
                        proInfo.SetValue(jbElement, Convert.ChangeType(property.Value, proInfo.PropertyType));
                }

            }

            return jbElement as JbBaseElement ?? jbElement as IJbBaseElement;
        }

        #endregion
    }
}
