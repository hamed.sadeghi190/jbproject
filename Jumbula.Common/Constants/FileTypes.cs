﻿namespace Jumbula.Common.Constants
{
    public static class FileTypes
    {
        public const string PdfMimeType = "application/pdf";
        public const string ExcelMimeType = "application/vnd.ms-excel";
        public const string ExcelXlsxMimeType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public const string PdfExtension = "pdf";
    }

}
