﻿using System;
using Jumbula.Common.Base;
using Jumbula.Common.Constants;

namespace Jumbula.Core.Model.Order
{
    public class OrderitemWaiversViewModel : BaseViewModel
    {

        public string Name { get; set; }
        public string Text { get; set; }
        public long OrderItemId { get; set; }
        public string Status { get; set; }
        public string Signature { get; set; }
        public bool Agreed { set; get; }
        public DateTime CreateDate { get; set; }
        public string StrCreateDate => CreateDate.ToString(Constants.DateTime_Comma);
        public string ProgramName { get; set; }
        public bool IsRequired { get; set; }
        public string WaiverConfirmationType { get; set; }
    }
}


