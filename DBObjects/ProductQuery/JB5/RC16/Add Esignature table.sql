
CREATE TABLE [dbo].[JbESignatures](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[SenderId] [int] NOT NULL,
	[ReciverId] [int] NULL,
	[Status] [tinyint] NOT NULL,
	[Signature] [nvarchar](max) NULL,
	[DeadLine] [datetime] NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[DocumentName] [nvarchar](max) NULL,
	[Subject] [nvarchar](400) NULL,
	[Content] [nvarchar](2000) NULL,
	[MetaData_FileName] [nvarchar](max) NULL,
	[MetaData_FolderName] [nvarchar](max) NULL,
	[MessageId] [int] NULL,
	[SignatureDate] [datetime] NULL,
	[SignatureTitle] [nvarchar](max) NULL,
	[SignedBy] [nvarchar](max) NULL,
	[ReciverName] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.JbESignatures] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[JbESignatures]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbESignatures_dbo.Clubs_ReciverId] FOREIGN KEY([ReciverId])
REFERENCES [dbo].[Clubs] ([Id])
GO

ALTER TABLE [dbo].[JbESignatures] CHECK CONSTRAINT [FK_dbo.JbESignatures_dbo.Clubs_ReciverId]
GO

ALTER TABLE [dbo].[JbESignatures]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbESignatures_dbo.Clubs_SenderId] FOREIGN KEY([SenderId])
REFERENCES [dbo].[Clubs] ([Id])
GO

ALTER TABLE [dbo].[JbESignatures] CHECK CONSTRAINT [FK_dbo.JbESignatures_dbo.Clubs_SenderId]
GO

ALTER TABLE [dbo].[JbESignatures]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbESignatures_dbo.MessageHeaders_MessageId] FOREIGN KEY([MessageId])
REFERENCES [dbo].[MessageHeaders] ([Id])
GO

ALTER TABLE [dbo].[JbESignatures] CHECK CONSTRAINT [FK_dbo.JbESignatures_dbo.MessageHeaders_MessageId]
GO


