﻿namespace Jumbula.Common.Enums
{
    public enum EmailSendPriority : byte
    {
        High = 0,
        Normal = 1,
        Low = 2
    }
}
