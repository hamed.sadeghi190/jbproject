﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum BusinessType
    {
        [Description("Corporation")]
        Corporation = 1,
        [Description("Individual/sole proprietorship")]
        Individual = 2,
        [Description("Non-Profit")]
        Non_Profit = 3,
        [Description("Partnership")]
        Partnership = 4,
        [Description("LLC")]
        LLC = 5,
        [Description("Government Entity")]
        GovernmentEntity = 6
    }
}
