﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CoordinatorFinancialReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'report-add';
            window.app.globalObjects.initializeView();
            $scope.MC.collapseAllBlades();

            $scope.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            var baseUrl = window.location.origin + '/Dashboard/CustomReport';

            $scope.Model = customReportService.getModelFromStrorage();

            var model = $scope.Model;

            var myVar = 0;


            $http.post(baseUrl + '/CoordinatorFinancialRun', { model: $scope.Model })
          .success(function (data, status, headers, config) {

         $scope.TotalDonationsReceived = data.TotalDonationsReceived;
         $scope.TotalPTAActivityFees = data.TotalPTAActivityFees;
         $scope.Totalem = data.Totalem;



     }).error(function (data, status, headers, config) {
         //alert(data);

     });

            $scope.CoordinatorFinancialReportsDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/CoordinatorFinancialRun',
                            type: "POST",
                            data: function () {
                                return {

                                    model: model,
                                    term: $scope.searchTerm,

                                };

                            },

                        },

                    },

                change: function (e) {
                },

                schema: {
                    total: "Total",

                    data: "DataSource",
                    model: {
                        fields: {
                          
                            ClassFee: { type: "number" }
                        }
                    }
                },
                aggregate: [
                        { field: "TotalRegistrations", aggregate: "sum" },
                         { field: "TotalPTAFundedScholarships", aggregate: "sum" },
                         { field: "TotalCashPaymenttoPTA", aggregate: "sum" },
                         { field: "TotalProviderFundedScholarships", aggregate: "sum" },
                         { field: "TotalPaidEnrollments", aggregate: "sum" },
                         { field: "ClassFee", aggregate: "average" },
                         { field: "TotalPaymentsForClass", aggregate: "sum" },
                         { field: "TotalDonationsReceived", aggregate: "sum" },
                         { field: "TotalPTAActivityFeesAssessed", aggregate: "sum" },
                         { field: "TotalEMfeeassessed", aggregate: "sum" },

                ],
                serverSorting: false,
                serverPaging: true,
                pageSize: 10,
                batch: true,
                resizable: true,
            });

            $scope.CoordinatorFinancialReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Coordinator Financial Report.xlsx",
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();
                    var rows = e.workbook.sheets[0].rows;
                    rows.push({
                        cells: [{
                            value: ""
                        }]
                    });
                    if (true) {
                        rows.push({
                            cells: [{
                                value: "Total Donation Received: " + $scope.TotalDonationsReceived,
                            }]
                        },
                        {
                            cells: [{

                                value: "Total PTA Activity Fees assessed: " + $scope.TotalPTAActivityFees,
                            }]
                        },
                       {
                           cells: [{
                               value: "Total EM 5% Fee on Donations & Activity Fees: " + $scope.Totalem,
                           }]
                       });
                    }
                },
                dataSource: $scope.CoordinatorFinancialReportsDataSource,

                sortable: { mode: 'single' },
                resizable: true,
                scrollable: true,
                columns: [
                    {
                        field: "ClassName",
                        title: "Class name",
                        width: "500px",
                        footerTemplate: 'Total'
                    },
                    {
                        field: "ProviderName",
                        title: "Provider name",
                        width: "200px",
                    },
                    {
                        field: "ClassFee",
                        title: "Class fee",
                        format: "{0:c2}",
                        width: "200px",
                        footerTemplate: 'Average: #=kendo.toString(average, "c2")#'
                    },
                    {
                        field: "TotalRegistrations",
                        title: "Total # of registrations",
                        width: "200px",
                        footerTemplate: ' #= kendo.toString(sum) #'
                    },
                    {
                        field: "TotalPTAFundedScholarships",
                        title: "Total # of PTA-funded scholarships",
                        width: "290px",
                        footerTemplate: ' #= kendo.toString(sum) #'
                    },
                     {
                         field: "TotalCashPaymenttoPTA",
                         title: "Total # of cash payment to PTA/PTO",
                         width: "320px",
                         footerTemplate: ' #= kendo.toString(sum) #'
                     },
                      {
                          field: "TotalProviderFundedScholarships",
                          title: "Total # of provider-funded scholarships",
                          width: "300px",
                          footerTemplate: ' #= kendo.toString(sum) #'
                      },
                    {
                        field: "TotalPaidEnrollments",
                        title: "Total # of fully paid enrollments",
                        width: "250px",
                        footerTemplate: ' #= kendo.toString(sum) #'
                    },
                    {
                        field: "TotalPTAActivityFeesAssessed",
                        title: "Total PTA activity fees assessed",
                        width: "250px",
                        format: "{0:c2}",
                        footerTemplate: ' #= kendo.toString(sum, "c2") #'
                    }]
            }

            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                $("#CoordinatorFinancialReportGrid").getKendoGrid().saveAsExcel();
            }

        }]);
})();