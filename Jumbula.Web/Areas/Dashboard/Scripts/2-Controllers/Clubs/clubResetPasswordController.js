﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('clubResetPasswordController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'members-resetpass-view';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Clubs", order: 1, menu: "Clubs" });

        $scope.MC.stateParams = $stateParams;
        $scope.type = $stateParams.type;

        $http.get('Dashboard/Club/ChangeClubPassword', { params: { clubId: $stateParams.clubId } }).success(function (data, status, headers, config) {
            $scope.Model = data;
           
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });
        $scope.save = function () {

            $http.post('Dashboard/Club/ChangeClubPassword', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     jbToast.success(data.Message,"");
                     $state.go('Clubs');
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

    }])

})();