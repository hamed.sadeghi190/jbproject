﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class PreferencesViewModel
    {
        public string CurrentUserName{ get; set; }

        public ParentChangeEmailViewModel ChangeEmail { get; set; }

        public ParentChangePasswordViewModel ChangePassword { get; set; }
    }
}