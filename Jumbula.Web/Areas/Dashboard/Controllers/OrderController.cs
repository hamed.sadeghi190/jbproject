﻿using AuthorizeNet.Api.Contracts.V1;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Core.Model.Order;
using Jumbula.Core.Model.Form;
using Jumbula.Web.Areas.Dashboard.Services;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.ModelState;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.Register;
using Jumbula.Web.WebLogic.UserService;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using SportsClub.Models.Email;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using WebGrease.Css.Extensions;
using Serilog;
using Constants = Jumbula.Common.Constants.Constants;
using static Jumbula.Common.Constants.Payment;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class OrderController : DashboardBaseController
    {
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IJbFormBusiness _jbFormBusiness;
        private readonly IPlayerProfileBusiness _playerProfileBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;
        private readonly IOrderBusiness _orderBusiness;
        private readonly IChargeBusiness _chargeBusiness;
        private readonly ISubscriptionBusiness _subscriptionBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly ILotteryBusiness _lotteryBusiness;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        private readonly IOrderHistoryBusiness _orderHistoryBusiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;

        public OrderController(IProgramBusiness programBusiness, IOrderItemBusiness orderItemBusiness, IJbFormBusiness jbFormBusiness,
            IPlayerProfileBusiness playerProfileBusiness, IOrderSessionBusiness orderSessionBusiness, IOrderBusiness orderBusiness, IChargeBusiness chargeBusiness
            , ISubscriptionBusiness subscriptionBusiness, IClubBusiness clubBusiness, ILotteryBusiness lotteryBusiness, IOrderInstallmentBusiness orderInstallmentBusiness
            , IOrderHistoryBusiness orderHistoryBusiness, ITransactionActivityBusiness transactionActivityBusiness)
        {
            _programBusiness = programBusiness;
            _orderInstallmentBusiness = orderInstallmentBusiness;
            _orderItemBusiness = orderItemBusiness;
            _jbFormBusiness = jbFormBusiness;
            _playerProfileBusiness = playerProfileBusiness;
            _orderSessionBusiness = orderSessionBusiness;
            _orderBusiness = orderBusiness;
            _chargeBusiness = chargeBusiness;
            _subscriptionBusiness = subscriptionBusiness;
            _clubBusiness = clubBusiness;
            _lotteryBusiness = lotteryBusiness;
            _orderHistoryBusiness = orderHistoryBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
        }

        public virtual ActionResult PaymentHistory()
        {
            return View();
        }
        public virtual ActionResult MakePayment()
        {
            return View();
        }
        public virtual ActionResult Order()
        {
            return View();
        }
        public virtual ActionResult OrderItem()
        {
            return View();
        }
        public virtual ActionResult DisplayPaymentMessage()
        {
            return View();
        }
        public virtual ActionResult ConfirmMakePayment()
        {
            return View();
        }
        public virtual ActionResult OrderHistory()
        {
            return View();
        }
        public virtual ActionResult CancelSubscriptionOrder()
        {
            return View();
        }
        public virtual ActionResult ConfirmCancelSubscriptionItem()
        {
            return View();
        }

        public virtual ActionResult PartiallyRefundOrderItem()
        {
            return View();
        }

        public virtual ActionResult ConfirmPartiallyRefundOrderItem()
        {
            return View();
        }
        public virtual ActionResult WaiverView()
        {
            return View();
        }
        public virtual ActionResult EditCancelDropInOrder()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult Transfer()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult EditOrder()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult GetPunchCardAssignment()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult ChangeDays()
        {

            return View();
        }

        [HttpGet]
        public virtual ActionResult ChangeDesiredStartDate()
        {

            return View();
        }

        [HttpGet]
        public virtual ActionResult GetTransfer(long orderItemId)
        {
            var model = new TransferOrderViewModel();
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            model.OrderIsAutocharge = orderItem.Order.IsAutoCharge;
            CheckResourceForAccess(orderItem);

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription && orderItem.Installments != null && orderItem.Installments.Any(i => !i.IsDeleted && i.Type != InstallmentType.AddOn && i.ProgramSchedulePartId == null))
            {
                return JsonNet(new { Status = false });
            }

            var outsourceClubDomain = string.Empty;
            if (orderItem != null && orderItem.ProgramScheduleId.HasValue && orderItem.ProgramSchedule.Program.OutSourceSeasonId.HasValue)
            {
                outsourceClubDomain = orderItem.ProgramSchedule.Program.OutSourceSeason.Club.Domain;
            }
            if (orderItem != null && Ioc.OrderBusiness.IsUserOwnsTheOrder(this.GetCurrentUserId(), orderItem.Order.UserId, orderItem.Order.Club.Domain, outsourceClubDomain))
            {
                var sourceProgeam = orderItem.ProgramSchedule.Program;
                var sourceSchedule = string.Format("{0} - {1}", orderItem.ProgramSchedule.StartDate.ToString(Constants.DefaultDateFormat), orderItem.ProgramSchedule.EndDate.ToString(Constants.DefaultDateFormat));
                var sourceTution = orderItem.OrderChargeDiscounts.SingleOrDefault(t => !t.IsDeleted && t.Category == ChargeDiscountCategory.EntryFee);

                model.Id = orderItem.Id;
                model.ProgramType = orderItem.ProgramSchedule.Program.TypeCategory;
                model.SourceProgramId = orderItem.ProgramSchedule.Program.Id;
                model.ProgramName = _programBusiness.GetProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName);
                model.PaidAmount = orderItem.PaidAmount;
                model.OrderAmount = orderItem.TotalAmount;
                model.EntryFee = orderItem.EntryFee;
                model.PaymentPlanType = orderItem.PaymentPlanType;
                model.RefundOrPay = true;
                model.ConfirmationId = orderItem.Order.ConfirmationId;
                model.PlayerName = orderItem.Player.Contact.FullName;
                if (orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
                {
                    var gender = orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.Gender.ToString()).GetValue();
                    if (gender != null)
                    {
                        model.Gender = gender.ToString();
                    }
                }

                if (orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
                {
                    var dobDate = DateTime.UtcNow;
                    if (DateTime.TryParse(orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.DoB.ToString()).GetValue().ToString(), out dobDate))
                    {
                        model.Age = DateTime.Now.Year - dobDate.Year;
                    }
                }

                var programParts = new List<ProgramPartViewModel>();

                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription || orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                {
                    var programSchedulePart = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

                    model.ProgramParts = programSchedulePart.ToList().Select(s => new ProgramPartViewModel
                    {
                        StartDate = s.StartDate,
                        EndDate = s.EndDate,
                        DueDate = s.DueDate.Value,
                        Id = s.Id

                    }).ToList();
                }

                model.OrderInstallments = orderItem.Installments.Where(o => !o.IsDeleted).Select(installment =>
                      new OrderInstallmentViewModel(installment, programParts))
                    .ToList();


                if (orderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription && orderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                {
                    if (model.OrderInstallments != null && model.OrderInstallments.Any())
                    {
                        var firstInstallment = model.OrderInstallments.FirstOrDefault();
                        firstInstallment.IsDeposit = true;
                        firstInstallment.Description = "(Deposit)";
                    }
                }

                model.SourceCharges = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge && c.Category != ChargeDiscountCategory.EntryFee).Select(c =>
                    new ChargeDiscountItemViewModel
                    {
                        Amount = c.Amount,
                        Category = c.Category,
                        Name = c.Name,
                        SubCategory = c.Subcategory,
                    })
                    .ToList();

                model.SourceDiscounts = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount).Select(c =>
                    new ChargeDiscountItemViewModel
                    {
                        Amount = c.Amount,
                        Category = c.Category,
                        Name = c.Name,
                        SubCategory = c.Subcategory,
                    })
                    .ToList();

                string seasonDomain = orderItem.ProgramSchedule.Program.Season.Domain;

                var clubId = orderItem.ProgramSchedule.Program.Club.Id;

                var allPrograms = Ioc.SeasonBusiness.Get(seasonDomain, clubId).Programs.Where(p => p.Status != ProgramStatus.Deleted && p.SaveType == SaveType.Publish).ToList();

                if (sourceProgeam.TypeCategory == ProgramTypeCategory.Class || sourceProgeam.TypeCategory == ProgramTypeCategory.Camp || sourceProgeam.TypeCategory == ProgramTypeCategory.SeminarTour)
                {
                    allPrograms = allPrograms.Where(a => a.TypeCategory == ProgramTypeCategory.Class || a.TypeCategory == ProgramTypeCategory.Camp || a.TypeCategory == ProgramTypeCategory.SeminarTour).ToList();
                }
                else if (sourceProgeam.TypeCategory != ProgramTypeCategory.Subscription && sourceProgeam.TypeCategory != ProgramTypeCategory.BeforeAfterCare)
                {
                    allPrograms = allPrograms.Where(a => a.TypeCategory == sourceProgeam.TypeCategory).ToList();
                }
                else
                {
                    allPrograms = allPrograms.Where(a => a.Id == model.SourceProgramId).ToList();
                }

                model.AllPrograms = allPrograms.Select(p =>
                    new SelectKeyValue<string>
                    {
                        Text = p.Name,
                        Value = p.Id.ToString()
                    })
                    .ToList();
            }

            //Get information if program is subscription
            if (model.ProgramType == ProgramTypeCategory.Subscription || model.ProgramType == ProgramTypeCategory.BeforeAfterCare)
            {
                var tutionItemId = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge && c.Category == ChargeDiscountCategory.EntryFee).FirstOrDefault().ChargeId;
                model.SourceTutionId = tutionItemId;
                var lastSessiondate = orderItem.OrderSessions.OrderBy(s => s.ProgramSession.StartDateTime).ToList().LastOrDefault().ProgramSession.StartDateTime;
                model.CurrentDesiredStartDate = orderItem.DesiredStartDate.Value;
                model.TodayDate = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow).Date;

                model.LastSessionDate = lastSessiondate;
                model.ProgramDays = orderItem.Attributes.WeekDays != null ? orderItem.Attributes.WeekDays.Count : 0;

            }

            return JsonNet(model);
        }

        [HttpGet]
        public virtual ActionResult GetEdit(long orderItemId)
        {
            var model = new EditOrderViewModel()
            {
                OrderChargeDiscounts = new List<ChargeDiscountItemViewModel>(),
            };

            var program = new Program();
            var schedule = new ProgramSchedule();
            string seasonDomain = "";
            var tuition = new OrderChargeDiscount();
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var outsourceClubDomain = string.Empty;

            if (orderItem != null && orderItem.ProgramScheduleId.HasValue && orderItem.ProgramSchedule.Program.OutSourceSeasonId.HasValue)
            {
                outsourceClubDomain = orderItem.ProgramSchedule.Program.OutSourceSeason.Club.Domain;
            }
            if (orderItem != null && Ioc.OrderBusiness.IsUserOwnsTheOrder(this.GetCurrentUserId(), orderItem.Order.UserId, orderItem.Order.Club.Domain, outsourceClubDomain))
            {
                if (orderItem.ProgramScheduleId.HasValue)
                {
                    program = orderItem.ProgramSchedule.Program;
                    schedule = orderItem.ProgramSchedule;
                    seasonDomain = orderItem.ProgramSchedule.Program.Season.Domain;
                    tuition = orderItem.OrderChargeDiscounts.SingleOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee);
                    model.ProgramName = _programBusiness.GetProgramName(schedule, orderItem.EntryFeeName);
                    model.PlayerName = orderItem.Player.Contact.FullName;
                    model.ProgramType = orderItem.ProgramSchedule.Program.TypeCategory.ToString();
                    model.ProgramId = orderItem.ProgramSchedule.ProgramId;
                }
                else
                {
                    model.ProgramName = orderItem.Name;
                    model.PlayerName = orderItem.FirstName;
                    seasonDomain = orderItem.Season != null ? orderItem.Season.Domain : string.Empty;
                }

                model.Id = orderItem.Id;
                model.ConfirmationId = orderItem.Order.ConfirmationId;
                model.OrderAmount = orderItem.TotalAmount;
                model.PaidAmount = orderItem.PaidAmount;
                model.Mode = orderItem.Mode;
                model.PaymentPlanType = orderItem.PaymentPlanType;

                if (orderItem.ProgramScheduleId.HasValue && orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
                {
                    var gender = orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.Gender.ToString()).GetValue();
                    if (gender != null)
                    {
                        model.Gender = gender.ToString();
                    }
                }

                if (orderItem.ProgramScheduleId.HasValue && orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
                {
                    var dobDate = DateTime.UtcNow;
                    if (DateTime.TryParse(orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.DoB.ToString()).GetValue().ToString(), out dobDate))
                    {
                        model.Age = DateTime.Now.Year - dobDate.Year;
                    }
                }
                #region ChargeDiscounts
                if (orderItem.ProgramScheduleId.HasValue)
                {
                    decimal selectedTution = 0;

                    switch (orderItem.Mode)
                    {
                        case OrderItemMode.Noraml:
                            break;
                        case OrderItemMode.DropIn:
                            {
                                var defaultProgramSchedule = program.ProgramSchedules.Last(s => !s.IsDeleted);
                                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    var defaultScheduleAttribute = (ScheduleSubscriptionAttribute)defaultProgramSchedule.Attributes;
                                    selectedTution = defaultScheduleAttribute.DropInSessionPrice;
                                }
                                else if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                {
                                    var defaultScheduleAttribute = (ScheduleAfterBeforeCareAttribute)defaultProgramSchedule.Attributes;
                                    selectedTution = defaultScheduleAttribute.DropInSessionPrice;
                                }
                            }
                            break;
                        case OrderItemMode.PunchCard:
                            break;
                        default:
                            {
                                selectedTution = program.ProgramSchedules.SelectMany(s => s.Charges).SingleOrDefault(c => c.Id == tuition.ChargeId).Amount;
                            }
                            break;
                    }

                    var programDiscounts = new List<Discount>();

                    if (program.Discounts.Any(d => !d.IsDeleted))
                    {
                        programDiscounts.AddRange(program.Discounts.Where(d => !d.IsDeleted));
                    }

                    var discounts = Ioc.DiscountBusiness.GetList(program.SeasonId).Where(d => d.IsDeleted == false && d.IsAllProgram == true);
                    if (discounts != null && discounts.Any())
                    {
                        if (programDiscounts == null)
                        {
                            programDiscounts = new List<Discount>();
                        }
                        programDiscounts.AddRange(discounts);
                    }

                    model.OrderChargeDiscounts = orderItem.GetOrderChargeDiscounts().Select(o =>
                        new ChargeDiscountItemViewModel
                        {
                            Id = o.Id,
                            Amount = Math.Abs(o.Amount),
                            AmountType = ChargeDiscountType.Fixed,
                            Category = o.Category,
                            Checked = true,
                            CouponId = o.CouponId,
                            Name = o.Name,
                            SubCategory = o.Subcategory,
                            ChargeId = o.ChargeId,
                            DiscountId = o.DiscountId
                        })
                        .ToList();

                    var programCoupons = Ioc.CouponBusiness.GetProgramCoupons(program.Id);

                    foreach (var coupon in programCoupons.Where(c => !orderItem.GetOrderChargeDiscounts().Select(i => i.CouponId).Contains(c.Id)))
                    {
                        if (!model.OrderChargeDiscounts.Where(c => c.SubCategory == ChargeDiscountSubcategory.Discount).Select(c => c.CouponId).Contains(coupon.Id) && coupon.AmounType == ChargeDiscountType.Fixed)
                        {
                            model.OrderChargeDiscounts.Add(
                                new ChargeDiscountItemViewModel
                                {
                                    Id = coupon.Id,
                                    Amount = Math.Abs(coupon.Amount),
                                    AmountType = coupon.AmounType,
                                    Category = ChargeDiscountCategory.Coupon,
                                    Checked = false,
                                    Name = coupon.Name,
                                    SubCategory = ChargeDiscountSubcategory.Discount,
                                    CouponId = coupon.Id
                                });

                        }
                        else
                        {
                            if (coupon.CouponCalculateType == CalculationType.OnlyTuition)
                            {
                                var percentType = coupon.Amount / 100;
                                var amount = orderItem.EntryFee * percentType;

                                model.OrderChargeDiscounts.Add(
                                 new ChargeDiscountItemViewModel
                                 {
                                     Id = coupon.Id,
                                     Amount = Math.Abs(amount),
                                     AmountType = coupon.AmounType,
                                     Category = ChargeDiscountCategory.Coupon,
                                     Checked = false,
                                     Name = coupon.Name,
                                     SubCategory = ChargeDiscountSubcategory.Discount,
                                     CouponId = coupon.Id
                                 });

                            }
                            else
                            {
                                if (coupon.CouponCalculateType == CalculationType.TotalAmount)
                                {
                                    var percentType = coupon.Amount / 100;
                                    var amount = orderItem.TotalAmount * percentType;
                                    model.OrderChargeDiscounts.Add(
                                 new ChargeDiscountItemViewModel
                                 {
                                     Id = coupon.Id,
                                     Amount = Math.Abs(amount),
                                     AmountType = coupon.AmounType,
                                     Category = ChargeDiscountCategory.Coupon,
                                     Checked = false,
                                     Name = coupon.Name,
                                     SubCategory = ChargeDiscountSubcategory.Discount,
                                     CouponId = coupon.Id
                                 });
                                }
                            }

                        }
                    }

                    var currentOrderChargeDiscounts = model.OrderChargeDiscounts;
                    var chargesForEdit = _chargeBusiness.GetChargesForEdit(orderItem, program, currentOrderChargeDiscounts);

                    model.OrderChargeDiscounts.AddRange(chargesForEdit);

                    foreach (var discount in programDiscounts)
                    {
                        if (!model.OrderChargeDiscounts.Where(c => c.SubCategory == ChargeDiscountSubcategory.Discount).Select(c => c.DiscountId).Contains(discount.Id))
                        {
                            model.OrderChargeDiscounts.Add(
                                new ChargeDiscountItemViewModel
                                {
                                    Id = discount.Id,
                                    Amount = discount.AmountType == ChargeDiscountType.Fixed ? discount.Amount : discount.Amount * (selectedTution / 100),
                                    AmountType = ChargeDiscountType.Fixed,
                                    Category = discount.Category,
                                    Checked = false,
                                    Name = discount.Name,
                                    SubCategory = ChargeDiscountSubcategory.Discount,
                                    DiscountId = discount.Id,
                                });
                        }
                    }

                    var customCharge = model.OrderChargeDiscounts.FirstOrDefault(c => c.Category == ChargeDiscountCategory.CustomDiscount && c.DiscountId == null);

                    if (customCharge == null)
                        model.OrderChargeDiscounts.Add(new ChargeDiscountItemViewModel { Checked = false, Name = "Custom discount", Id = -100, SubCategory = ChargeDiscountSubcategory.Discount, Category = ChargeDiscountCategory.CustomDiscount });

                }
                else
                {
                    model.OrderChargeDiscounts = orderItem.GetOrderChargeDiscounts().Select(o =>
                                           new ChargeDiscountItemViewModel
                                           {
                                               Id = o.Id,
                                               Amount = Math.Abs(o.Amount),
                                               AmountType = ChargeDiscountType.Fixed,
                                               Category = o.Category,
                                               Checked = true,
                                               CouponId = o.CouponId,
                                               Name = o.Name,
                                               SubCategory = o.Subcategory,
                                               ChargeId = o.ChargeId,
                                               DiscountId = o.DiscountId
                                           })
                                           .ToList();
                }
                #endregion

                //get sessions for drop in item
                if (orderItem.Mode == OrderItemMode.DropIn)
                {
                    model.OrderSessions = new List<DropInSessionsViewModel>();
                    if (orderItem.OrderSessions != null && orderItem.OrderSessions.Any())
                    {
                        var sessionsCount = orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription ? orderItem.OrderSessions.Count : orderItem.OrderSessions.Select(p => p.ProgramSession).GroupBy(g => g.StartDateTime.Date).Count();
                        decimal amount = 0;

                        if (orderItem.OrderSessions.Any(s => s.Amount == null))
                        {
                            amount = orderItem.EntryFee / sessionsCount;

                            model.OrderSessions = orderItem.OrderSessions.Select(session =>
                                      new DropInSessionsViewModel(session, amount)).OrderBy(s => s.Date.Date).ToList();
                        }
                        else
                        {
                            var chargeDiscounts = orderItem.GetOrderChargeDiscounts().Where(c => c.Category != ChargeDiscountCategory.EntryFee).ToList();

                            foreach (var session in orderItem.OrderSessions.OrderBy(o => o.ProgramSession.StartDateTime.Date))
                            {
                                amount = session.Amount.Value;
                                model.OrderSessions.Add(new DropInSessionsViewModel(session, amount));
                            }
                        }

                        model.HasSessions = true;
                    }
                }

                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare && orderItem.PaymentPlanType == PaymentPlanType.FullPaid && orderItem.Mode == OrderItemMode.Noraml)
                {
                    model.OrderItemScheduleParts = new List<OrderItemSchedulePartsViewModel>();

                    model.OrderItemScheduleParts = orderItem.OrderItemScheduleParts.Where(p => !p.ProgramPart.IsDeleted && (!p.IsDeleted || (p.IsDeleted && p.DeleteReason == DeleteReason.SessionDelete))).Select(p => new OrderItemSchedulePartsViewModel
                    {
                        StartDate = p.ProgramPart.StartDate.ToString(Constants.DefaultDateFormat),
                        EndDate = p.ProgramPart.EndDate.ToString(Constants.DefaultDateFormat),
                        IsDeleted = p.IsDeleted,
                        Id = p.Id,
                        ProgramSchedulePartId = p.ProgramPartId.HasValue ? p.ProgramPartId.Value : 0

                    }).ToList();
                }

                if (orderItem.Installments != null && orderItem.Installments.Any())
                {
                    if (orderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription && orderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                    {
                        model.OrderInstallments = orderItem.Installments.Where(i => !i.IsDeleted).Select(installment =>
                              new OrderInstallmentViewModel(installment)).ToList();
                    }
                    else
                    {
                        var programParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

                        model.ProgramParts = programParts.ToList().Select(s => new ProgramPartViewModel
                        {
                            StartDate = s.StartDate,
                            EndDate = s.EndDate,
                            DueDate = s.DueDate.Value,
                            Id = s.Id

                        }).ToList();

                        model.OrderInstallments = orderItem.Installments.Where(i => (i.ProgramSchedulePartId.HasValue && !i.ProgramSchedulePart.IsDeleted || i.Type == InstallmentType.AddOn) && (!i.IsDeleted || (i.IsDeleted && (i.DeleteReason == DeleteReason.SessionDelete)))).Select(installment =>
                               new OrderInstallmentViewModel(installment, model.ProgramParts)).ToList();

                    }

                    if (program.TypeCategory != ProgramTypeCategory.Subscription && program.TypeCategory != ProgramTypeCategory.BeforeAfterCare)
                    {
                        var firstInstallment = model.OrderInstallments.FirstOrDefault();
                        firstInstallment.IsDeposit = true;
                        firstInstallment.Description = "(Deposit)";
                    }

                    if (orderItem.Order.IsAutoCharge)
                    {
                        model.IsAutoCharge = orderItem.Installments.Where(c => c.PendingPreapprovalTransactions != null && c.PendingPreapprovalTransactions.Count > 0).Any();
                    }
                }
            }

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult Edit(EditOrderViewModel model)
        {
            bool cancelMode = false;

            if (model.OrderChargeDiscounts == null || !model.OrderChargeDiscounts.Any(c => c.Category == ChargeDiscountCategory.EntryFee && c.Checked))
            {
                cancelMode = true;
            }
            var forDeleteIds = new List<long>();
            var forDeleteAndSessionsIds = new List<long>();
            var forDeletePaidInFullIds = new List<int>();
            var forActiveIds = new List<long>();
            var listAddProgramSession = new List<ProgramSession>();
            var itemDays = new List<DayOfWeek>();

            OrderItem orderItem = _orderItemBusiness.GetItem(model.Id);
            CheckResourceForAccess(orderItem);

            var club = orderItem.Order.Club;
            var selectedTuition = model.OrderChargeDiscounts != null ? model.OrderChargeDiscounts.SingleOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee) : null;

            OperationStatus res = new OperationStatus();
            var message = "";

            if (!cancelMode || !orderItem.ProgramScheduleId.HasValue)
            {

                var programId = orderItem.ProgramScheduleId.HasValue ? orderItem.ProgramSchedule.ProgramId : 0;
                var programCoupons = new List<Coupon>();

                if (programId != 0)
                {
                    programCoupons = Ioc.CouponBusiness.GetProgramCoupons(programId);
                }

                //var selectedCoupons = programCoupons.Where(c => model.OrderChargeDiscounts.Select(i => i.CouponId).Contains(c.Id));

                orderItem.OrderChargeDiscounts.Clear();

                if (model.EditFee.HasValue && model.EditFee != 0)
                {
                    if (model.OrderChargeDiscounts == null)
                    {
                        model.OrderChargeDiscounts = new List<ChargeDiscountItemViewModel>();
                    }

                    orderItem.OrderChargeDiscounts.Add(
                        new OrderChargeDiscount
                        {
                            Amount = model.EditFee.Value,
                            Category = ChargeDiscountCategory.EditFee,
                            Name = Constants.W_Edit_Fee,
                            Description = Constants.W_Edit_Fee,
                            Subcategory = ChargeDiscountSubcategory.Charge
                        }
                    );
                }

                foreach (var item in model.OrderChargeDiscounts.Where(c => c.Checked))
                {
                    if (item.Id == -100)
                    {
                        item.ChargeId = null;
                        item.DiscountId = null;
                    }

                    if (item.SubCategory == ChargeDiscountSubcategory.Charge || (item.SubCategory == ChargeDiscountSubcategory.Discount && !(item.Category == ChargeDiscountCategory.Coupon)))
                    {
                        orderItem.OrderChargeDiscounts.Add(
                        new OrderChargeDiscount
                        {
                            Amount = item.SubCategory == ChargeDiscountSubcategory.Discount ? -item.Amount : item.Amount,
                            Category = item.Category,
                            ChargeId = item.ChargeId,
                            DiscountId = item.DiscountId,
                            Name = item.Name,
                            Subcategory = item.SubCategory,
                            OrderItemId = orderItem.Id,
                            Description = item.SubCategory == ChargeDiscountSubcategory.Discount ? string.Format("{0} off for {1}", CurrencyHelper.FormatCurrencyWithPenny(item.Amount, club.Currency), item.Name) : null,
                            Coupon = item.Category == ChargeDiscountCategory.Coupon ? Ioc.CouponBusiness.Get(item.CouponId.Value) : null,
                            CouponId = item.CouponId,
                        });
                    }
                    else
                    {
                        if (item.Category == ChargeDiscountCategory.Coupon)
                        {
                            //var coupon = programCoupons.SingleOrDefault(c => c.Id == item.CouponId);
                            var coupon = Ioc.CouponBusiness.Get(item.CouponId.Value);

                            orderItem.OrderChargeDiscounts.Add(
                                new OrderChargeDiscount
                                {
                                    //Id = item.Id,
                                    Amount = item.Category == ChargeDiscountCategory.Coupon ? -item.Amount : item.Amount,
                                    Category = item.Category,
                                    Name = item.Name,
                                    Subcategory = item.SubCategory,
                                    OrderItemId = orderItem.Id,
                                    Description = item.SubCategory == ChargeDiscountSubcategory.Discount ? string.Format("{0} off for {1}", CurrencyHelper.FormatCurrencyWithPenny(item.Amount, club.Currency), item.Name) : null,
                                    //Coupon = item.Category == ChargeDiscountCategory.Coupon ? Ioc.CouponBusiness.Get(item.CouponId.Value) : null,
                                    CouponId = coupon.Id,
                                    Coupon = coupon
                                });
                        }
                    }
                }

                if (orderItem.ProgramScheduleId.HasValue)
                {
                    orderItem.EntryFee = selectedTuition.Amount;
                    orderItem.EntryFeeName = selectedTuition.Name;
                }

                orderItem.CalculateDiscounts();

                orderItem.CalculateTotalAmount();

                var userId = this.GetCurrentUserId();


                var createdInstallments = new List<OrderInstallment>();

                // Delete items
                forDeleteIds = model.OrderInstallments.Where(o => o.IsDeleted && o.Id != 0).Select(o => o.Id).ToList();

                // Delete installment for subscription item
                var listDeletedOrderSessionsItem = new List<OrderSession>();
                var listDeletedSchedule = new List<ProgramSchedulePart>();
                itemDays = orderItem.Attributes != null ? orderItem.Attributes.WeekDays : null;
                var programSubscriptionParts = new List<ProgramSchedulePart>();
                var allProgramSessionActivatedInstallment = new List<ProgramSession>();
                var listAvailableSession = new List<OrderSession>();
                var listActivatedSchedule = new List<ProgramSchedulePart>();

                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription || orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                {
                    programSubscriptionParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);
                }

                if (orderItem.PaymentPlanType == PaymentPlanType.Installment && (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription || orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare))
                {
                    forDeleteIds = model.OrderInstallments.Where(o => o.IsDeletedNormalItem && o.Id != 0).Select(o => o.Id).ToList();
                    forDeleteAndSessionsIds = model.OrderInstallments.Where(o => o.IsDeletedSessions && o.Id != 0).Select(o => o.Id).ToList();

                    var installmentsItem = orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted).ToList();

                    var listOrderSubscriptionItem = programSubscriptionParts.Where(p => installmentsItem.Select(i => i.ProgramSchedulePartId).ToList().Contains(p.Id)).ToList();

                    var listDeletedInstallment = model.OrderInstallments.Where(i => i.IsDeletedNormalItem || i.IsDeletedSessions && i.Type == InstallmentType.Noraml).ToList();

                    if (listDeletedInstallment.Count > 0)
                    {
                        var listDeletedInstallmentSessions = listDeletedInstallment.Where(i => i.IsDeletedSessions).ToList();
                        listDeletedSchedule = listDeletedInstallmentSessions != null ? listOrderSubscriptionItem.Where(s => listDeletedInstallmentSessions.Select(i => i.ProgramSchedulePartId).ToList().Contains(s.Id)).ToList() : null;

                        if (listDeletedSchedule.Count > 0)
                        {
                            foreach (var item in listDeletedSchedule)
                            {
                                var orderSessions = orderItem.OrderSessions.Where(o => o.ProgramSession.StartDateTime.Date >= item.StartDate && o.ProgramSession.EndDateTime.Date <= item.EndDate).ToList();

                                listDeletedOrderSessionsItem.AddRange(orderSessions);
                            }
                        }
                    }

                    var listActiveInstallmentItem = model.OrderInstallments.Where(i => i.IsActiveInstallment || i.IsActiveInstallmentAndSessions).ToList();
                    if (listActiveInstallmentItem.Count > 0)
                    {
                        forActiveIds = model.OrderInstallments.Where(o => o.IsActiveInstallment || o.IsActiveInstallmentAndSessions && o.Id != 0).Select(o => o.Id).ToList();

                        var listInstallmentSessions = model.OrderInstallments.Where(i => i.IsActiveInstallmentAndSessions).ToList();

                        listActivatedSchedule = programSubscriptionParts.Where(s => listInstallmentSessions.Select(i => i.ProgramSchedulePartId).ToList().Contains(s.Id)).ToList();
                    }
                }
                else if ((orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription || orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare))
                {
                    forDeletePaidInFullIds = model.OrderItemScheduleParts.Where(o => o.IsDeleted && o.Id != 0).Select(o => o.Id).ToList();

                    if (forDeletePaidInFullIds.Count > 0)
                    {
                        foreach (var item in model.OrderItemScheduleParts.Where(p => p.IsDeleted))
                        {
                            var startDate = DateTime.Parse(item.StartDate);
                            var endDate = DateTime.Parse(item.EndDate);
                            var orderSessions = orderItem.OrderSessions.Where(o => o.ProgramSession.StartDateTime.Date >= startDate && o.ProgramSession.EndDateTime.Date <= endDate).ToList();

                            listDeletedOrderSessionsItem.AddRange(orderSessions);
                        }
                    }

                    var activeScheduleParts = model.OrderItemScheduleParts.Where(i => i.IsRestored).ToList();

                    if (activeScheduleParts.Any())
                    {
                        var activateScheduleParts = model.OrderItemScheduleParts.Where(o => o.IsRestored && o.Id != 0);
                        forActiveIds = activateScheduleParts.Select(o => Convert.ToInt64(o.Id)).ToList();
                        listActivatedSchedule = programSubscriptionParts.Where(s => activateScheduleParts.Select(i => i.ProgramSchedulePartId).ToList().Contains(s.Id)).ToList();
                    }
                }

                if (listActivatedSchedule.Any())
                {
                    var programSessions = new List<ProgramSession>();

                    if (orderItem.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both)
                    {
                        var programschedules = orderItem.ProgramSchedule.Program.ProgramSchedules.Where(s => !s.IsDeleted && s.ScheduleMode != TimeOfClassFormation.Both);

                        foreach (var schedule in programschedules)
                        {
                            programSessions.AddRange(Ioc.ProgramSessionBusiness.GetList(schedule));
                        }
                    }
                    else
                    {
                        programSessions = Ioc.ProgramSessionBusiness.GetList(orderItem.ProgramSchedule);
                    }

                    foreach (var item in listActivatedSchedule)
                    {
                        var listProgramSessionsItem = programSessions.Where(p => p.StartDateTime.Date >= item.StartDate && p.EndDateTime.Date <= item.EndDate).ToList();
                        allProgramSessionActivatedInstallment.AddRange(listProgramSessionsItem);

                        var sessionsItem = orderItem.OrderSessions.Where(o => o.ProgramSession.StartDateTime.Date >= item.StartDate && o.ProgramSession.StartDateTime.Date <= item.EndDate).ToList();
                        listAvailableSession.AddRange(sessionsItem);
                    }
                }

                if (listAvailableSession.Any())
                {
                    listAddProgramSession = allProgramSessionActivatedInstallment.Where(p => !listAvailableSession.Select(s => s.ProgramSessionId).ToList().Contains(p.Id)).ToList();
                }
                else
                {
                    listAddProgramSession = allProgramSessionActivatedInstallment;
                }

                // Create items
                if (model.OrderInstallments != null && model.OrderInstallments.Any())
                {
                    var isOrderAutoCarge = orderItem.Order.IsAutoCharge && model.OrderInstallments.Any(i => !i.EnableReminder);
                    createdInstallments = model.OrderInstallments.Where(w => w.Id == 0 && !w.IsDeleted).Select(o =>
                        new OrderInstallment()
                        {
                            Id = o.Id,
                            Amount = o.Amount,
                            EnableReminder = isOrderAutoCarge ? false : true,
                            InstallmentDate = o.DueDate,
                            NoticeSent = o.NoticeSent,
                            PaidAmount = null,
                            Token = o.Token,
                            OrderItemId = orderItem.Id,
                            Status = o.ItemStatus
                        })
                        .ToList();


                    if (createdInstallments != null && createdInstallments.Any())
                    {
                        if (orderItem.Order.IsAutoCharge && orderItem.Order.PendingPreapprovalTransactions != null && orderItem.Order.PendingPreapprovalTransactions.Any())
                        {
                            foreach (var item in createdInstallments)
                            {
                                item.PendingPreapprovalTransactions = new List<PendingPreapprovalTransaction>();
                                item.PendingPreapprovalTransactions.Add(new PendingPreapprovalTransaction()
                                {
                                    Installment = item,
                                    Order = orderItem.Order,
                                    DueAmount = item.Amount,
                                    DueDate = item.InstallmentDate,
                                    OrderId = orderItem.Order_Id,
                                    Status = PendingPreapprovalTransactionStatus.Pending
                                });
                            }
                        }
                        orderItem.Installments.AddRange(createdInstallments);
                    }

                    foreach (var item in model.OrderInstallments.Where(w => w.Id != 0 && !w.IsDeleted))
                    {
                        var updateInstallment = orderItem.Installments.Single(o => o.Id == item.Id);
                        if (item.Amount != updateInstallment.Amount || item.DueDate.Date.CompareTo(updateInstallment.InstallmentDate.Date) != 0)
                        {
                            if (updateInstallment.PendingPreapprovalTransactions != null && updateInstallment.PendingPreapprovalTransactions.Any())
                            {
                                var deletedTran = updateInstallment.PendingPreapprovalTransactions.Where(c => c.Status != PendingPreapprovalTransactionStatus.Completed && c.Status != PendingPreapprovalTransactionStatus.PartiallyCompleted);

                                foreach (var pendingTran in deletedTran)
                                {
                                    pendingTran.Status = PendingPreapprovalTransactionStatus.Deleted;

                                }
                                decimal completeAmount = 0;
                                if (updateInstallment.PendingPreapprovalTransactions.Any(c => c.Status == PendingPreapprovalTransactionStatus.Completed))
                                {
                                    completeAmount = updateInstallment.PendingPreapprovalTransactions.Where(c => c.Status != PendingPreapprovalTransactionStatus.Completed).Sum(c => c.PaidAmount);
                                }
                                if (updateInstallment.PendingPreapprovalTransactions.Any(c => c.Status == PendingPreapprovalTransactionStatus.PartiallyCompleted))
                                {
                                    foreach (var partialTran in updateInstallment.PendingPreapprovalTransactions.Where(c => c.Status != PendingPreapprovalTransactionStatus.Completed))
                                    {
                                        partialTran.Status = PendingPreapprovalTransactionStatus.Completed;
                                        partialTran.DueAmount = partialTran.PaidAmount;
                                        completeAmount = partialTran.PaidAmount;
                                    }
                                }
                                if (completeAmount < item.Amount)
                                {
                                    updateInstallment.PendingPreapprovalTransactions.Add(new PendingPreapprovalTransaction()
                                    {
                                        Installment = updateInstallment,
                                        Order = orderItem.Order,
                                        DueAmount = (item.Amount - completeAmount),
                                        DueDate = item.DueDate,
                                        OrderId = orderItem.Order_Id,
                                        Status = PendingPreapprovalTransactionStatus.Pending
                                    });
                                }
                            }
                        }

                        // if after successfuly payment  club admin increase installment amount  we must set installment autochargeattemps  value zero .

                        if (updateInstallment.PaidAmount.HasValue && item.Amount > updateInstallment.PaidAmount
                            && updateInstallment.Amount == updateInstallment.PaidAmount // don't reset partial payment
                            && updateInstallment.Status == OrderStatusCategories.completed)
                        {
                            updateInstallment.AutoChargeAttemps = 0;

                            Log.Warning("{OrderItemId}: order, reset autocharge attemp for {InstallmentId}", orderItem.Id, updateInstallment.Id);
                        }

                        updateInstallment.Amount = item.Amount;
                        updateInstallment.InstallmentDate = item.DueDate;
                    }
                }

                if (!orderItem.ProgramScheduleId.HasValue)
                {
                    orderItem.ItemStatus = orderItem.TotalAmount == 0 ? OrderItemStatusCategories.changed : orderItem.ItemStatus;
                    orderItem.ItemStatusReason = orderItem.TotalAmount == 0 ? OrderItemStatusReasons.canceled : orderItem.ItemStatusReason;
                }

                res = _orderItemBusiness.Edit(orderItem, forDeleteIds, forDeleteAndSessionsIds, forActiveIds, this.GetCurrentUserId(), listDeletedOrderSessionsItem, model.CancelEditMemo, listAddProgramSession, itemDays, forDeletePaidInFullIds);
            }
            else
            {

                if (model.CancellationFee.HasValue && model.CancellationFee != 0)
                {
                    if (model.HasInstallment)
                    {
                        AddCancellationFee(model);
                    }
                    else
                    {
                        orderItem.OrderChargeDiscounts.Add(
                        new OrderChargeDiscount
                        {
                            Amount = model.CancellationFee.Value,
                            Category = ChargeDiscountCategory.CancellationFee,
                            Name = Constants.W_Cancel_Fee,
                            Description = Constants.W_Cancel_Fee,
                            Subcategory = ChargeDiscountSubcategory.Charge
                        });
                    }

                }

                if (model.ProrateFee != null && model.ProrateFee != 0)
                {
                    if (model.HasInstallment)
                    {
                        AddProrateFee(model);
                    }
                    else
                    {
                        orderItem.OrderChargeDiscounts.Add(
                        new OrderChargeDiscount
                        {
                            Amount = model.ProrateFee.Value,
                            Category = ChargeDiscountCategory.ProrateFee,
                            Name = Constants.W_Prorate_Fee,
                            Description = Constants.W_Prorate_Fee,
                            Subcategory = ChargeDiscountSubcategory.Charge
                        });
                    }

                }

                var cancellationFee = model.CancellationFee.HasValue ? model.CancellationFee.Value : 0;
                var prorateFee = model.ProrateFee.HasValue ? model.ProrateFee.Value : 0;
                res = _orderItemBusiness.Cancel(orderItem.Id, this.GetCurrentUserId(), model.CancelEditMemo, cancellationFee, prorateFee);
            }
            if (!orderItem.ProgramScheduleId.HasValue)
            {
                message = orderItem.TotalAmount > 0 ? "Donation/Edit" : "Donation/Cancel";

            }
            if (model.SendEmail)
            {
                ReservationEmailMode emailMode = new ReservationEmailMode();

                if (!orderItem.ProgramScheduleId.HasValue)
                {
                    emailMode = orderItem.TotalAmount > 0 ? ReservationEmailMode.Edit : ReservationEmailMode.Cancel;
                }
                else
                {
                    emailMode = cancelMode ? ReservationEmailMode.Cancel : ReservationEmailMode.Edit;
                }

                EmailService.Get(this.ControllerContext).SendReservationEmail(orderItem.Order.ConfirmationId, PaymentMethod.None, Request.Url.Host, orderItem.Id, emailMode);
            }

            message = message != null ? message : res.Message;

            return Json(new JResult { Status = res.Status, Message = message, RecordsAffected = res.RecordsAffected });
        }

        private void AddProrateFee(EditOrderViewModel model)
        {
            OrderItem orderItem = _orderItemBusiness.GetItem(model.Id);

            if (model.OrderChargeDiscounts == null)
            {
                model.OrderChargeDiscounts = new List<ChargeDiscountItemViewModel>();
            }

            var prorateFee = model.ProrateFee.HasValue ? model.ProrateFee.Value : 0;
            _orderItemBusiness.CreateAddOnChargeToOrderItem(orderItem, ChargeDiscountCategory.ProrateFee, prorateFee, Constants.W_Prorate_Fee, Constants.W_Prorate_Fee);
        }

        private void AddCancellationFee(EditOrderViewModel model)
        {
            OrderItem orderItem = _orderItemBusiness.GetItem(model.Id);

            if (model.OrderChargeDiscounts == null)
            {
                model.OrderChargeDiscounts = new List<ChargeDiscountItemViewModel>();
            }

            var cancellationFee = model.CancellationFee.HasValue ? model.CancellationFee.Value : 0;

            _orderItemBusiness.CreateAddOnChargeToOrderItem(orderItem, ChargeDiscountCategory.CancellationFee, cancellationFee, Constants.W_Cancel_Fee, Constants.W_Cancel_Fee);
        }

        public virtual void Cart()
        {
            var userId = 0;
            var order = Ioc.OrderBusiness.GetLastOfflineOrderForClub(this.ActiveClub.Id);
            if (order != null)
            {
                userId = order.UserId;
            }

            var url = string.Format("{0}://{1}/Cart?UserId=" + userId, Request.Url.Scheme, Request.Url.Host);

            Response.Redirect(url);
            Response.End();
        }

        public virtual JsonNetResult GetUserNameForLastOfflineOrder()
        {
            var order = Ioc.OrderBusiness.GetLastOfflineOrderForClub(this.ActiveClub.Id);
            if (order != null)
            {
                var playername = order.RegularOrderItems.Select(c => c.FullName);
                var result = string.Format("{0} ({1})", order.User.UserName, string.Join(",", playername));
                return JsonNet(result, new JsonSerializerSettings());

            }

            return JsonNet("", new JsonSerializerSettings());
        }
        public virtual JsonResult DoesOfflineOrderExist()
        {
            try
            {

                bool isExist = Ioc.OrderBusiness.DoesOfflineOrderExist(this.ActiveClub.Id);
                return Json(new JResult { Status = true, RecordsAffected = isExist ? 1 : 0 });
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = Constants.PlayerProfile_Ajax_Request_Failed, RecordsAffected = 0 });
            }
        }
        [HttpPost]
        public virtual ActionResult Transfer(TransferOrderViewModel model)
        {
            var sourceOrderItem = _orderItemBusiness.GetItem(model.Id);
            CheckResourceForAccess(sourceOrderItem);

            Club club = sourceOrderItem.Order.Club;

            var res = new OperationStatus();
            var paidAmount = sourceOrderItem.PaidAmount;

            var targetProgram = _programBusiness.Get(long.Parse(model.SelectedProgram));
            model.TargetProgram = targetProgram;

            var targetSchedule = targetProgram.ProgramSchedules.SingleOrDefault(t => t.Charges.Any(c => c.Id == model.SelectedTution));
            model.TargetSchedule = targetSchedule;

            var targetTution = targetSchedule.Charges.Single(s => s.Id == model.SelectedTution);
            model.TargetTution = targetTution;

            string itemName = _programBusiness.GetProgramName(targetSchedule, targetTution.Name);

            if (targetProgram.TypeCategory != ProgramTypeCategory.Calendar)
            {
                itemName += " " + targetTution.Amount.ToString().Replace(".00", "").Replace(".0", "");
            }
            model.ItemName = itemName;
            var followUpForms = new List<OrderItemFollowupForm>();

            foreach (var form in sourceOrderItem.FollowupForms)
            {
                if (targetProgram.ClubFormTemplates.Where(f => (f.FormType == FormType.FollowUp || f.FormType == FormType.UploadForm) && !f.IsDeleted).Any(f => f.Title.ToLower().Equals(form.FormName.ToLower())))
                {
                    followUpForms.Add(form);
                }
            }

            foreach (var form in targetProgram.ClubFormTemplates.Where(f => (f.FormType == FormType.FollowUp || f.FormType == FormType.UploadForm) && !f.IsDeleted))
            {
                if (!followUpForms.Any(f => f.FormName.ToLower().Equals(form.Title.ToLower())))
                {
                    followUpForms.Add(Ioc.FollowupBusiness.CreateOrderItemFollowUpForm(form, sourceOrderItem));
                }
            }

            model.FolloupForms = followUpForms;
            OrderItem targetOrderItem = new OrderItem();
            targetOrderItem.OrderChargeDiscounts = new List<OrderChargeDiscount>();


            #region
            targetOrderItem = new OrderItem()
            {
                Byes = sourceOrderItem.Byes,
                DateCreated = DateTime.UtcNow,
                End = targetSchedule.EndDate,
                Start = targetSchedule.StartDate,
                EntryFee = targetTution.Amount,
                EntryFeeName = targetTution.Name,
                FirstName = sourceOrderItem.FirstName,
                FollowupForms = followUpForms,
                Installments = model.OrderInstallments?.Where(i => !i.IsDeleted).Select(s =>
                      new OrderInstallment
                      {
                          Amount = s.Amount,
                          EnableReminder = s.EnableReminder,
                          Id = 0,
                          InstallmentDate = s.DueDate,
                          NoticeSent = s.NoticeSent,
                          OrderItemId = 0,
                          PaidAmount = s.PaidAmount,
                          PaidDate = s.PaidDateTime,
                          IsDeleted = s.IsDeleted,
                          PendingPreapprovalTransactions = Ioc.PendingPreapprovalTransactionBusiness.GetList().Where(t => t.InstallmentId == s.Id).ToList().Select(p =>
                                  new PendingPreapprovalTransaction
                                  {
                                      DueAmount = p.DueAmount,
                                      DueDate = p.DueDate,
                                      InstallmentId = 0,
                                      OrderId = p.OrderId,
                                      PaidAmount = p.PaidAmount,
                                      Status = p.Status,
                                  })
                                  .ToList(),

                          Status = s.ItemStatus,
                          Token = s.Token,
                          TransactionActivities = Ioc.TransactionActivityBusiness.GetList().Where(t => t.InstallmentId == s.Id).ToList().Select(a =>
                               new TransactionActivity
                               {
                                   OrderItemId = 0,
                                   Amount = a.Amount,
                                   ClubId = a.ClubId,
                                   Note = a.Note,
                                   OrderId = a.OrderId,
                                   SeasonId = a.SeasonId,
                                   Token = a.Token,
                                   TransactionCategory = a.TransactionCategory,
                                   TransactionDate = a.TransactionDate,
                                   TransactionStatus = a.TransactionStatus,
                                   TransactionType = a.TransactionType,
                                   PaymentDetail = a.PaymentDetail,
                                   MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                                   CheckId = a.CheckId,
                                   HandleMode = a.HandleMode,
                                   InstallmentId = 0,
                               })
                               .ToList(),
                      }
                )
                .ToList(),

                ISTS = sourceOrderItem.ISTS,
                JbFormId = sourceOrderItem.JbFormId,
                LastName = sourceOrderItem.LastName,
                Name = itemName,
                Options = sourceOrderItem.Options,
                Order_Id = sourceOrderItem.Order_Id,
                OrderItemChessId = sourceOrderItem.OrderItemChessId,
                PaymentPlanId = sourceOrderItem.PaymentPlanId,
                PaymentPlanType = sourceOrderItem.PaymentPlanType,
                PlayerId = sourceOrderItem.PlayerId,
                ProgramScheduleId = targetSchedule.Id,
                ProgramTypeCategory = targetProgram.TypeCategory,
                RegType = sourceOrderItem.RegType,
                SeasonId = targetProgram.SeasonId,
                Season = targetProgram.Season,
                Section = sourceOrderItem.Section,
                CreditCardId = sourceOrderItem.CreditCardId,
                ItemStatus = OrderItemStatusCategories.completed,
                ItemStatusReason = OrderItemStatusReasons.transferIn,
                PaidAmount = sourceOrderItem.PaidAmount,
                LastStep = sourceOrderItem.LastStep,
                IsMultiRegister = sourceOrderItem.IsMultiRegister,
                TransactionActivities = sourceOrderItem.TransactionActivities.Where(i => i.InstallmentId == null).Select(s =>
                 new TransactionActivity
                 {
                     OrderItemId = 0,
                     InstallmentId = null,
                     Amount = s.Amount,
                     ClubId = s.ClubId,
                     Note = s.Note,
                     OrderId = s.OrderId,
                     SeasonId = s.SeasonId,
                     Token = s.Token,
                     TransactionCategory = s.TransactionCategory,
                     TransactionDate = s.TransactionDate,
                     TransactionStatus = s.TransactionStatus,
                     TransactionType = s.TransactionType,
                     PaymentDetail = s.PaymentDetail,
                     MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                     CheckId = s.CheckId,
                     HandleMode = s.HandleMode
                 })
                 .ToList(),
            };
            #endregion

            if (model.OrderChargeDiscounts != null && model.OrderChargeDiscounts.Any())
            {
                targetOrderItem.OrderChargeDiscounts = model.OrderChargeDiscounts.Where(o => o.Checked).Select(c =>
                    new OrderChargeDiscount
                    {
                        Amount = c.SubCategory == ChargeDiscountSubcategory.Discount ? -Math.Abs(c.Amount) : c.Amount,
                        Category = c.Category,
                        Name = c.Name,
                        OrderId = sourceOrderItem.Order.Id,
                        Subcategory = c.SubCategory,
                        ChargeId = c.ChargeId,
                        DiscountId = c.DiscountId
                    })
                    .ToList();
            }

            targetOrderItem.OrderChargeDiscounts.Add(
                new OrderChargeDiscount
                {
                    Amount = targetTution.Amount,
                    Category = ChargeDiscountCategory.EntryFee,
                    ChargeId = targetTution.Id,
                    Name = targetTution.Name,
                    Description = targetTution.Description,
                    Subcategory = ChargeDiscountSubcategory.Charge
                });


            if (model.TransferFee.HasValue && model.TransferFee != 0)
            {
                targetOrderItem.OrderChargeDiscounts.Add(
                 new OrderChargeDiscount
                 {
                     Amount = model.TransferFee.Value,
                     Category = ChargeDiscountCategory.TransferFee,
                     Name = Constants.W_Transfer_Fee,
                     Description = Constants.W_Transfer_Fee,
                     Subcategory = ChargeDiscountSubcategory.Charge
                 });
            }

            var installments = sourceOrderItem.Installments.ToList();
            _orderInstallmentBusiness.SetStatusDeletedInstallment(installments, DeleteReason.Transfer);
            installments.ForEach(i => i.PaidAmount = 0);


            DeleteChargesAndUpdateTotalAmountSourceOrderItem(sourceOrderItem);

            targetOrderItem.ProgramSchedule = targetSchedule;

            targetOrderItem.ProgramScheduleId = targetSchedule.Id;

            targetOrderItem.CalculateDiscounts();
            targetOrderItem.CalculateTotalAmount();

            var userId = this.GetCurrentUserId();

            res = Ioc.OrderBusiness.Transfer(sourceOrderItem.Id, targetOrderItem, userId, model.TransferMemo);


            if (model.SendEmail)
            {
                EmailService.Get(this.ControllerContext).SendReservationEmail(targetOrderItem.Order.ConfirmationId, PaymentMethod.None, Request.Url.Host, targetOrderItem.Id, ReservationEmailMode.Transfer);
            }

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected, Data = targetOrderItem.Id.ToString() });
        }


        private void DeleteChargesAndUpdateTotalAmountSourceOrderItem(OrderItem orderItem)
        {
            orderItem.OrderChargeDiscounts.ToList().ForEach(c => c.IsDeleted = true);
            orderItem.TotalAmount = 0;
            orderItem.EntryFee = 0;
        }

        [HttpGet]
        public virtual ActionResult GetCancelSubscriptionOrder(long orderItemId)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var model = new CancelSubscriptionOrderViewModel();

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription && orderItem.Installments != null && orderItem.Installments.Any(i => !i.IsDeleted && i.Type != InstallmentType.AddOn && i.ProgramSchedulePartId == null))
            {
                return JsonNet(new { Status = false });
            }

            model.OrderItemId = orderItemId;
            model = GetInformationForCancelSubscription(model);

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult CheckValidationCancelSubscriptionItem(CancelSubscriptionOrderViewModel model)
        {
            OrderItem orderItem = _orderItemBusiness.GetItem(model.OrderItemId);
            var today = DateTime.UtcNow;
            if (!model.EffectiveDate.HasValue)
            {
                ModelState.AddModelError("model.EffectiveDate", "Effective date is required.");
            }
            else
            {
                //if (model.EffectiveDate.Value < today && orderItem.DesiredStartDate < today)
                //{
                //    ModelState.AddModelError("model.EffectiveDate", "Effective date is in the past.");
                //}
                //if (model.EffectiveDate.Value >= model.LastSessionDate)
                //{
                //    ModelState.AddModelError("model.EffectiveDate", "Effective date must be before the program end date.");
                //}
            }

            if (ModelState.IsValid)
            {
                return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });
            }

            return JsonFormResponse();
        }
        [HttpGet]
        public virtual ActionResult ConfirmCancelSubscriptionOrderInfo(long orderItemId, DateTime effectivedate, decimal cancelationFee, string memo)
        {
            var model = new CancelSubscriptionOrderViewModel();
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            model.OrderItemId = orderItemId;
            model = GetInformationForCancelSubscription(model);
            model.CancellationFee = cancelationFee;
            model.EffectiveDate = effectivedate;
            model.ShowCancellationFeeBox = model.CancellationFee > 0 ? true : false;

            if (memo != "null")
            {
                model.CancelMemo = memo;
            }
            var installmentsItem = orderItem.Installments.Where(i => !i.IsDeleted).ToList();

            if (orderItem.Installments != null && orderItem.PaymentPlanType == PaymentPlanType.Installment)
            {
                var paidInstallment = installmentsItem.Where(i => i.Status == OrderStatusCategories.completed).ToList();
                //if before program begins
                if (model.EffectiveDate.Value <= model.DesiredStartDate)
                {
                    model.IsEffectiveDateBeforeDesired = true;
                    model.PaidInstallmentAmount = paidInstallment.Sum(p => (decimal?)p.PaidAmount) ?? 0;
                    model.EntryFee = 0;

                    if (model.PaidInstallmentAmount > 0)
                    {
                        if (model.CancellationFee > orderItem.PaidAmount)
                        {
                            model.NewBalance = model.CancellationFee - orderItem.PaidAmount;
                        }
                        else
                        {
                            model.RefundFee = orderItem.PaidAmount - model.CancellationFee;
                        }
                    }
                    else
                    {
                        model.NewBalance = model.CancellationFee;
                    }
                    //update All cahrges and discount
                    foreach (var item in model.SourceCharges)
                    {
                        model.UpdatedChargeDiscounts.Add(new ChargeDiscountItemViewModel
                        {
                            Amount = 0,
                            Category = item.Category,
                            Name = item.Name,
                            Id = item.Id,
                            SubCategory = item.SubCategory
                        });
                    }
                    foreach (var item in model.SourceDiscounts)
                    {
                        model.UpdatedChargeDiscounts.Add(new ChargeDiscountItemViewModel
                        {
                            Amount = 0,
                            Category = item.Category,
                            Name = item.Name,
                            Id = item.Id,
                            SubCategory = item.SubCategory
                        });
                    }

                    if (cancelationFee != 0)
                    {
                        model.UpdatedChargeDiscounts.Add(
                            new ChargeDiscountItemViewModel
                            {
                                Amount = cancelationFee,
                                Category = ChargeDiscountCategory.CancellationFee,
                                Name = Constants.W_Cancel_Fee,
                            }
                        );
                    }
                    model.NewTotalAmount = model.UpdatedChargeDiscounts.Sum(p => (decimal?)p.Amount) ?? 0;
                    model.UpdatedCharges.AddRange(model.UpdatedChargeDiscounts.Where(d => d.SubCategory == ChargeDiscountSubcategory.Charge));
                    model.UpdatedDiscounts.AddRange(model.UpdatedChargeDiscounts.Where(d => d.SubCategory == ChargeDiscountSubcategory.Discount));

                }
                else
                {
                    model.IsEffectiveDateBeforeDesired = false;
                    model = cancelSubscriptionItem(model, orderItem);
                }
            }
            else
            {
                ConfirmCancelFullPaidItem(orderItem, model);
            }

            return JsonNet(model);

        }
        [HttpPost]
        public virtual ActionResult SubmitCancelSubscriptionOrder(CancelSubscriptionOrderViewModel model)
        {
            var result = new OperationStatus();
            var emailMode = ReservationEmailMode.Cancel;
            var orderItem = _orderItemBusiness.GetItem(model.OrderItemId);
            CheckResourceForAccess(orderItem);

            var installments = orderItem.Installments.Where(i => !i.IsDeleted).ToList();
            var orderItemSourceEntryFee = orderItem.EntryFee;
            var addOninstallment = new OrderInstallment();
            addOninstallment = installments.Where(i => i.Type == InstallmentType.AddOn).FirstOrDefault();

            if (orderItem.PaymentPlanType == PaymentPlanType.FullPaid)
            {
                CancelFullPaidItem(orderItem, model);
            }
            else
            {
                if (model.IsEffectiveDateBeforeDesired)
                {
                    if (model.CancellationFee > 0)
                    {
                        //if have installment with type AddOn
                        if (addOninstallment != null)
                        {
                            orderItem.Installments.Where(i => i.Id == addOninstallment.Id).FirstOrDefault().Amount = model.CancellationFee;
                            installments = installments.Where(i => i.Type != InstallmentType.AddOn).ToList();
                            Ioc.InstallmentBusiness.SetStatusDeletedInstallment(installments, DeleteReason.Cancel);
                            orderItem.GetOrderChargeDiscounts().ForEach(o => o.Amount = 0);
                            orderItem.Order.OrderAmount -= orderItem.TotalAmount;
                            orderItem.EntryFee = 0;
                        }
                        else
                        {
                            //if don't have installment with type AddOn we should add new installment
                            orderItem.Installments.Add(
                                new OrderInstallment
                                {
                                    Amount = model.CancellationFee,
                                    InstallmentDate = DateTime.UtcNow,
                                    Token = null,
                                    PaidAmount = 0,
                                    PaidDate = null,
                                    NoticeSent = false,
                                    EnableReminder = false,
                                    Type = InstallmentType.AddOn,
                                    Status = OrderStatusCategories.initiated,

                                });

                            installments = installments.Where(i => i.Type != InstallmentType.AddOn).ToList();
                            Ioc.InstallmentBusiness.SetStatusDeletedInstallment(installments, DeleteReason.Cancel);
                            orderItem.GetOrderChargeDiscounts().ForEach(o => o.Amount = 0);
                            orderItem.Order.OrderAmount -= orderItem.TotalAmount;
                            orderItem.EntryFee = 0;
                        }
                    }
                    else
                    {
                        Ioc.InstallmentBusiness.SetStatusDeletedInstallment(installments, DeleteReason.Cancel);
                        orderItem.GetOrderChargeDiscounts().ForEach(o => o.Amount = 0);
                        orderItem.Order.OrderAmount -= orderItem.TotalAmount;
                        orderItem.EntryFee = 0;
                    }
                }
                else
                {
                    if (model.CancellationFee > 0)
                    {
                        if (addOninstallment != null)
                        {
                            orderItem.Installments.Where(i => i.Id == addOninstallment.Id).FirstOrDefault().Amount += model.CancellationFee;
                        }
                        else
                        {
                            orderItem.Installments.Add(
                                new OrderInstallment
                                {
                                    Amount = model.CancellationFee,
                                    InstallmentDate = DateTime.UtcNow,
                                    Token = null,
                                    PaidAmount = 0,
                                    PaidDate = null,
                                    NoticeSent = false,
                                    EnableReminder = false,
                                    Type = InstallmentType.AddOn,
                                    Status = OrderStatusCategories.initiated,

                                });
                        }
                    }

                    installments = installments.Where(i => model.DeletedItemSchedules.Select(d => d.PaymentDueDate).ToList().Contains(i.InstallmentDate)).ToList();
                    Ioc.InstallmentBusiness.SetStatusDeletedInstallment(installments, DeleteReason.Cancel);
                    orderItem.EntryFee = model.EntryFee;

                    foreach (var item in orderItem.GetOrderChargeDiscounts())
                    {
                        if (item.Category == ChargeDiscountCategory.EntryFee)
                        {
                            item.Amount = model.EntryFee;
                        }
                        else
                        {
                            foreach (var updatedItem in model.UpdatedChargeDiscounts)
                            {
                                if (item.Id == updatedItem.Id)
                                {
                                    item.Amount = updatedItem.Amount;
                                    break;
                                }
                            }
                        }
                    }
                    //we should delete session after effective date
                    model.EffectiveDate = model.EffectiveDate;
                    var total = orderItem.TotalAmount;
                    var totalAmount = orderItem.GetOrderChargeDiscounts().Sum(p => (decimal?)p.Amount) ?? 0;
                    orderItem.Order.OrderAmount = (orderItem.Order.OrderAmount - total) + totalAmount;

                }
            }

            if (model.CancellationFee != 0)
            {
                orderItem.Order.OrderAmount += model.CancellationFee;
                orderItem.OrderChargeDiscounts.Add(
                    new OrderChargeDiscount
                    {
                        Amount = model.CancellationFee,
                        Category = ChargeDiscountCategory.CancellationFee,
                        Name = Constants.W_Cancel_Fee,
                        Description = Constants.W_Cancel_Fee,
                        Subcategory = ChargeDiscountSubcategory.Charge
                    }
                );
            }

            orderItem.CalculateTotalAmount(OrderItemStatusReasons.canceled);
            orderItem.ItemStatus = OrderItemStatusCategories.changed;
            orderItem.ItemStatusReason = OrderItemStatusReasons.canceled;
            orderItem.Attributes = new OrderItemAttributes() { WeekDays = orderItem.Attributes.WeekDays, TransferEffectiveDate = orderItem.Attributes.TransferEffectiveDate, CancelEffectiveDate = model.EffectiveDate };
            var historyDescription = string.Empty;
            var date = DateTime.UtcNow;
            historyDescription = string.Format(Constants.Order_History_Cancel_Desc,
               orderItem.ProgramSchedule.Program.Name, orderItem.EntryFeeName, orderItemSourceEntryFee.ToString("C2", CultureInfo.CurrentCulture));

            historyDescription += string.Format(" Memo: {0}", model.CancelMemo);

            orderItem.Order.OrderHistories.Add(
               new OrderHistory
               {
                   Action = OrderAction.Canceled,
                   ActionDate = date,
                   Description = historyDescription,
                   UserId = this.GetCurrentUserId(),
                   OrderItemId = orderItem.Id
               });

            result = _orderSessionBusiness.Create(orderItem, null, null, model.EffectiveDate);

            if (result.Status && model.SendEmail)
            {
                EmailService.Get(this.ControllerContext).SendReservationEmail(orderItem.Order.ConfirmationId, PaymentMethod.None, Request.Url.Host, orderItem.Id, emailMode);
            }

            return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });
        }
        [HttpPost]
        public virtual ActionResult TransferSubscription(TransferOrderViewModel model)
        {
            var result = new OperationStatus();

            OrderItem sourceOrderItem = _orderItemBusiness.GetItem(model.Id);
            CheckResourceForAccess(sourceOrderItem);

            Club club = sourceOrderItem.Order.Club;
            var res = new OperationStatus();
            var paidAmount = sourceOrderItem.PaidAmount;

            var targetProgram = _programBusiness.Get(long.Parse(model.SelectedProgram));
            model.TargetProgram = targetProgram;

            var targetSchedule = targetProgram.ProgramSchedules.SingleOrDefault(t => t.Charges.Any(c => c.Id == model.SelectedTution));
            model.TargetSchedule = targetSchedule;

            var targetTution = targetSchedule.Charges.Single(s => s.Id == model.SelectedTution);
            model.TargetTution = targetTution;

            string itemName = _programBusiness.GetProgramName(targetSchedule, targetTution.Name);
            var sourceTuition = sourceOrderItem.OrderChargeDiscounts.SingleOrDefault(s => !s.IsDeleted && s.Category == ChargeDiscountCategory.EntryFee);

            if (targetProgram.TypeCategory != ProgramTypeCategory.Calendar)
            {
                itemName += " " + targetTution.Amount.ToString().Replace(".00", "").Replace(".0", "");
            }
            model.ItemName = itemName;
            var followUpForms = new List<OrderItemFollowupForm>();

            foreach (var form in sourceOrderItem.FollowupForms)
            {
                if (targetProgram.ClubFormTemplates.Where(f => (f.FormType == FormType.FollowUp || f.FormType == FormType.UploadForm) && !f.IsDeleted).Any(f => f.Title.ToLower().Equals(form.FormName.ToLower())))
                {
                    followUpForms.Add(form);
                }
            }

            foreach (var form in targetProgram.ClubFormTemplates.Where(f => (f.FormType == FormType.FollowUp || f.FormType == FormType.UploadForm) && !f.IsDeleted))
            {
                if (!followUpForms.Any(f => f.FormName.ToLower().Equals(form.Title.ToLower())))
                {
                    followUpForms.Add(Ioc.FollowupBusiness.CreateOrderItemFollowUpForm(form, sourceOrderItem));
                }
            }
            var newInstallments = new List<OrderInstallment>();
            var deletedOldInstallment = new List<OrderInstallment>();
            model.FolloupForms = followUpForms;
            OrderItem targetOrderItem = new OrderItem();
            //var pastSchedules = new List<ProgramSubscriptionItem>();
            var pastSchedules = new List<ProgramSchedulePart>();
            targetOrderItem.OrderChargeDiscounts = new List<OrderChargeDiscount>();

            var listTargetAntSourceItem = GetNewOrderItemAndUpdateOrderItem(model);

            targetOrderItem = listTargetAntSourceItem.TargetItem;
            sourceOrderItem = listTargetAntSourceItem.SourceItem;
            pastSchedules = listTargetAntSourceItem.ListPastInstallmentSchedule;
            deletedOldInstallment = listTargetAntSourceItem.ListDeletedOldInstallments;
            newInstallments = listTargetAntSourceItem.NewInstallments;

            var targetOrderParts = new List<OrderItemSchedulePartModel>();

            if (targetOrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var part in targetOrderItem.OrderItemScheduleParts)
                {
                    targetOrderParts.Add(new OrderItemSchedulePartModel
                    {
                        Amount = part.PartAmount,
                        PartId = part.ProgramPartId.Value,
                    });
                }
            }

            sourceOrderItem.ItemStatus = OrderItemStatusCategories.changed;
            sourceOrderItem.ItemStatusReason = OrderItemStatusReasons.transferOut;

            model.NewDays = model.SelectedDays;
            targetOrderItem.Attributes = new OrderItemAttributes() { WeekDays = model.NewDays, TransferEffectiveDate = model.NewEffectiveDate };
            sourceOrderItem.Attributes = new OrderItemAttributes() { WeekDays = sourceOrderItem.Attributes.WeekDays, TransferEffectiveDate = model.NewEffectiveDate };

            if (model.TransferFee.HasValue && model.TransferFee != 0)
            {
                targetOrderItem.OrderChargeDiscounts.Add(
                new OrderChargeDiscount
                {
                    Amount = model.TransferFee.Value,
                    Category = ChargeDiscountCategory.TransferFee,
                    Name = Constants.W_Transfer_Fee,
                    Description = Constants.W_Transfer_Fee,
                    Subcategory = ChargeDiscountSubcategory.Charge
                });
                // shoulde equal total installments with total amount, so we shoulde add the installment for transfer fee 
                targetOrderItem.Installments.Add(new OrderInstallment
                {
                    Amount = model.TransferFee.Value,
                    InstallmentDate = DateTime.UtcNow,
                    Token = null,
                    PaidAmount = 0,
                    PaidDate = null,
                    NoticeSent = false,
                    EnableReminder = false,
                    Type = InstallmentType.AddOn,
                    Status = OrderStatusCategories.initiated,
                });
            }
            var Allinstallments = sourceOrderItem.Installments.Where(i => !i.IsDeleted).ToList();
            var installmentWithoutAppFee = Allinstallments.Where(i => i.Type != InstallmentType.AddOn).ToList();
            var applicationFee = sourceOrderItem.OrderChargeDiscounts.Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee).ToList();
            //Add charge discount new item
            if (sourceOrderItem.GetOrderChargeDiscounts() != null && sourceOrderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted))
            {
                foreach (var chargeDiscount in sourceOrderItem.OrderChargeDiscounts.OrderByDescending(c => c.Subcategory))
                {
                    if (chargeDiscount.Category == ChargeDiscountCategory.EntryFee)
                    {
                        targetOrderItem.OrderChargeDiscounts.Add(new OrderChargeDiscount
                        {
                            Amount = targetOrderItem.EntryFee,
                            Category = chargeDiscount.Category,
                            Name = model.TargetTution.Name,
                            OrderId = sourceOrderItem.Order.Id,
                            Subcategory = chargeDiscount.Subcategory,
                            ChargeId = model.SelectedTution,
                            DiscountId = chargeDiscount.DiscountId,
                            Charge = targetTution
                        });
                    }
                    else if (chargeDiscount.Category != ChargeDiscountCategory.ApplicationFee)
                    {
                        decimal amount = 0;
                        if (chargeDiscount.Category == ChargeDiscountCategory.Surcharge || chargeDiscount.Category == ChargeDiscountCategory.PartnerSurcharge)
                        {
                            foreach (var item in newInstallments)
                            {
                                var mainAmount = targetOrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare ? targetOrderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == item.ProgramSchedulePartId).FirstOrDefault().PartAmount : 0;
                                amount += _orderItemBusiness.CalculateOneSurCharge(sourceOrderItem, chargeDiscount, mainAmount);
                            }
                        }
                        if (chargeDiscount.Subcategory == ChargeDiscountSubcategory.Discount && (chargeDiscount.DiscountId.HasValue || chargeDiscount.CouponId.HasValue))
                        {
                            decimal percentAmount = 0;
                            if (chargeDiscount.CouponId.HasValue)
                            {
                                if (chargeDiscount.Coupon.CouponCalculateType == CalculationType.TotalAmount && applicationFee.Count > 0)
                                {
                                    if (chargeDiscount.Coupon.AmounType == ChargeDiscountType.Percent && sourceOrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                    {
                                        foreach (var item in newInstallments)
                                        {
                                            var partItem = targetOrderItem.OrderItemScheduleParts.Where(p => p.ProgramPartId == item.ProgramSchedulePartId).FirstOrDefault();
                                            var mainAmount = targetOrderParts.Where(p => p.PartId == item.ProgramSchedulePartId).FirstOrDefault();

                                            percentAmount = (chargeDiscount.Coupon.Amount * mainAmount.Amount) / 100;
                                            amount += (percentAmount) * -1;
                                            partItem.PartAmount -= (chargeDiscount.Coupon.Amount * mainAmount.Amount) / 100;

                                        }
                                    }
                                    else
                                    {

                                        if (chargeDiscount.Coupon.AmounType == ChargeDiscountType.Percent && sourceOrderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                        {
                                            decimal appAmount = 0;
                                            foreach (var app in applicationFee)
                                            {
                                                var percentApp = (chargeDiscount.Coupon.Amount * app.Amount) / 100;
                                                appAmount += percentApp;
                                            }

                                            amount = (chargeDiscount.Amount - appAmount) / installmentWithoutAppFee.Count;
                                        }
                                        else
                                        {
                                            amount = chargeDiscount.Amount / (installmentWithoutAppFee.Count + 1);
                                        }

                                        amount = (amount * newInstallments.Count);
                                    }
                                }
                                else
                                {
                                    if (chargeDiscount.Coupon.AmounType == ChargeDiscountType.Percent && sourceOrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                    {
                                        foreach (var item in newInstallments)
                                        {
                                            var partItem = targetOrderItem.OrderItemScheduleParts.Where(p => p.ProgramPartId == item.ProgramSchedulePartId).FirstOrDefault();
                                            var mainAmount = targetOrderParts.Where(p => p.PartId == item.ProgramSchedulePartId).FirstOrDefault();

                                            percentAmount = (chargeDiscount.Coupon.Amount * mainAmount.Amount) / 100;
                                            amount += (percentAmount) * -1;
                                            partItem.PartAmount -= (chargeDiscount.Coupon.Amount * mainAmount.Amount) / 100;
                                        }
                                    }
                                    else
                                    {
                                        amount = chargeDiscount.Amount / installmentWithoutAppFee.Count;
                                        amount = (amount * newInstallments.Count);
                                    }
                                }
                            }
                            else
                            {
                                if (chargeDiscount.Discount.AmountType == ChargeDiscountType.Percent && sourceOrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                {
                                    foreach (var item in newInstallments)
                                    {
                                        var partItem = targetOrderItem.OrderItemScheduleParts.Where(p => p.ProgramPartId == item.ProgramSchedulePartId).FirstOrDefault();
                                        var mainAmount = targetOrderParts.Where(p => p.PartId == item.ProgramSchedulePartId).FirstOrDefault();

                                        percentAmount = (chargeDiscount.Discount.Amount * mainAmount.Amount) / 100;
                                        amount += (percentAmount) * -1;

                                        partItem.PartAmount -= (chargeDiscount.Discount.Amount * mainAmount.Amount) / 100;
                                    }
                                }
                                else
                                {
                                    amount = chargeDiscount.Amount / (installmentWithoutAppFee.Count);
                                    amount = (amount * newInstallments.Count);
                                }
                            }
                        }

                        targetOrderItem.OrderChargeDiscounts.Add(new OrderChargeDiscount
                        {
                            Amount = amount,
                            Category = chargeDiscount.Category,
                            Name = chargeDiscount.Name,
                            OrderId = sourceOrderItem.Order.Id,
                            Subcategory = chargeDiscount.Subcategory,
                            ChargeId = chargeDiscount.ChargeId,
                            DiscountId = chargeDiscount.DiscountId,
                            CouponId = chargeDiscount.CouponId,
                            Coupon = chargeDiscount.Coupon,
                            Discount = chargeDiscount.Discount
                        });
                    }
                }
            }


            if (sourceOrderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
            {
                var orderItemEntryfee = _subscriptionBusiness.GetSelectedCharge(sourceOrderItem).Amount;
                decimal discountAmount = 0;
                decimal surCharge = 0;

                foreach (var item in sourceOrderItem.GetOrderChargeDiscounts().ToList())
                {
                    if (item.Category == ChargeDiscountCategory.EntryFee)
                    {
                        item.Amount -= (orderItemEntryfee * deletedOldInstallment.Count);
                        sourceOrderItem.EntryFee -= (orderItemEntryfee * deletedOldInstallment.Count);
                    }
                    if (item.Subcategory == ChargeDiscountSubcategory.Discount && (item.DiscountId.HasValue || item.CouponId.HasValue))
                    {
                        if (item.CouponId.HasValue)
                        {
                            if (item.Coupon.CouponCalculateType == CalculationType.TotalAmount && applicationFee.Count > 0)
                            {
                                discountAmount = item.Amount / (installmentWithoutAppFee.Count + 1);
                            }
                            else
                            {
                                discountAmount = item.Amount / installmentWithoutAppFee.Count;
                            }
                        }
                        else
                        {
                            discountAmount = item.Amount / installmentWithoutAppFee.Count;
                        }

                        item.Amount -= (discountAmount * deletedOldInstallment.Count);
                    }
                    if (item.Category == ChargeDiscountCategory.Surcharge || item.Category == ChargeDiscountCategory.PartnerSurcharge)
                    {
                        surCharge = _orderItemBusiness.CalculateOneSurCharge(sourceOrderItem, item);
                        item.Amount -= (surCharge * deletedOldInstallment.Count);
                    }
                }
            }
            else
            {
                //update charge discount old item
                UpdateItemOrderChargeDiscount(sourceOrderItem, deletedOldInstallment, targetOrderParts);
            }


            var totalAmount = sourceOrderItem.GetOrderChargeDiscounts().Sum(p => (decimal?)p.Amount) ?? 0;
            var total = sourceOrderItem.TotalAmount;
            sourceOrderItem.Order.OrderAmount = (sourceOrderItem.Order.OrderAmount - total) + totalAmount;

            targetOrderItem.ProgramSchedule = targetSchedule;

            if (targetOrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var item in targetOrderParts)
                {
                    targetOrderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == item.PartId).FirstOrDefault().PartAmount = item.Amount;
                }
            }

            targetOrderItem.ProgramScheduleId = targetSchedule.Id;
            targetOrderItem.DesiredStartDate = sourceOrderItem.DesiredStartDate;

            targetOrderItem.CalculateTotalAmount(OrderItemStatusReasons.transferIn);
            sourceOrderItem.CalculateTotalAmount(OrderItemStatusReasons.transferOut);

            var userId = this.GetCurrentUserId();
            // add history for orderItem
            var historyDescription = string.Format("Order {0} - {1}(${2}) transferred to {3} - {4}(${5})",
                  sourceOrderItem.ProgramSchedule.Program.Name, sourceTuition.Name, sourceTuition.Amount,
                  targetOrderItem.ProgramSchedule.Program.Name, targetTution.Name, targetTution.Amount);

            historyDescription += string.Format(" Memo: {0}", model.TransferMemo);

            Ioc.InstallmentBusiness.SetStatusDeletedInstallment(deletedOldInstallment, DeleteReason.Transfer);

            var order = Ioc.OrderBusiness.GetAllOrders().SingleOrDefault(o => o.OrderItems.Any(i => i.Id == sourceOrderItem.Id));
            order.OrderItems.Add(targetOrderItem);

            res = _orderSessionBusiness.CreateForTransfer(targetOrderItem, model.NewDays, sourceOrderItem, model.NewEffectiveDate);

            if (res.Status)
            {
                Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Transfer, historyDescription, sourceOrderItem.Order_Id, userId, targetOrderItem.Id);
                Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Transfer, historyDescription, sourceOrderItem.Order_Id, userId, sourceOrderItem.Id);
            }
            if (model.SendEmail)
            {
                EmailService.Get(this.ControllerContext).SendReservationEmail(targetOrderItem.Order.ConfirmationId, PaymentMethod.None, Request.Url.Host, targetOrderItem.Id, ReservationEmailMode.Transfer);
            }

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected, Data = targetOrderItem.Id.ToString() });
        }

        public dynamic GetNewOrderItemAndUpdateOrderItem(TransferOrderViewModel model)
        {
            dynamic result = new System.Dynamic.ExpandoObject();

            var orderInstallments = new List<OrderInstallment>();
            OrderItem targetOrderItem = new OrderItem();
            var listRemainingOldInstallments = new List<OrderInstallment>();
            var listDeletedOldInstallments = new List<OrderInstallment>();

            var listPastInstallmentSchedule = new List<ProgramSchedulePart>();
            var targetOrderItemScheduleParts = new List<OrderItemSchedulePart>();

            var sourceOrderItem = _orderItemBusiness.GetItem(model.Id);
            CheckResourceForAccess(sourceOrderItem);

            Club club = sourceOrderItem.Order.Club;

            var Allinstallments = sourceOrderItem.Installments.Where(i => !i.IsDeleted).ToList();
            var installmentWithoutAppFee = Allinstallments.Where(i => i.Type != InstallmentType.AddOn).ToList();

            if (sourceOrderItem.Installments.Count > 0)
            {
                var res = GetSubscriptionInstallmentGetPastInstallment(model.Id, model.SourceProgramId, model.SourceProgramId, model.SelectedTution, model.NewEffectiveDate);
                //get result
                orderInstallments = res.ListNewInstallment;
                listRemainingOldInstallments = res.ListRemainingOldInstallments;
                listDeletedOldInstallments = res.ListDeletedInstallments;
                listPastInstallmentSchedule = res.PastInstallmentSchedules;
                listPastInstallmentSchedule = listPastInstallmentSchedule.OrderByDescending(s => s.EndDate).ToList();
                targetOrderItemScheduleParts = res.ListOrderItemScheduleParts;

                var countdeleted = listDeletedOldInstallments.Count;

            }
            else
            {
                //if program is paid full
            }

            decimal entryFee = 0;
            if (sourceOrderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
            {
                entryFee = model.TargetTution.Amount * orderInstallments.Count;
            }

            #region Add new orderItem
            targetOrderItem = new OrderItem()
            {
                Byes = sourceOrderItem.Byes,
                DateCreated = DateTime.UtcNow,
                End = model.TargetSchedule.EndDate,
                Start = model.TargetSchedule.StartDate,
                EntryFee = entryFee,
                EntryFeeName = model.TargetTution.Name,
                FirstName = sourceOrderItem.FirstName,
                FollowupForms = model.FolloupForms,
                Installments = orderInstallments.Select(s =>
                    new OrderInstallment
                    {
                        Amount = s.Amount,
                        EnableReminder = s.EnableReminder,
                        Id = 0,
                        InstallmentDate = s.InstallmentDate,
                        NoticeSent = s.NoticeSent,
                        OrderItemId = 0,
                        PaidAmount = 0,
                        PaidDate = null,
                        Status = s.Status,
                        Token = s.Token,
                        ProgramSchedulePartId = s.ProgramSchedulePartId
                    }
                 )
                 .ToList(),

                ISTS = sourceOrderItem.ISTS,
                JbFormId = sourceOrderItem.JbFormId,
                LastName = sourceOrderItem.LastName,
                Name = model.ItemName,
                Options = sourceOrderItem.Options,
                Order_Id = sourceOrderItem.Order_Id,
                OrderItemChessId = sourceOrderItem.OrderItemChessId,
                PaymentPlanId = sourceOrderItem.PaymentPlanId,
                PaymentPlanType = sourceOrderItem.PaymentPlanType,
                PlayerId = sourceOrderItem.PlayerId,
                ProgramScheduleId = model.TargetSchedule.Id,
                ProgramTypeCategory = model.TargetProgram.TypeCategory,
                RegType = sourceOrderItem.RegType,
                SeasonId = model.TargetProgram.SeasonId,
                Season = model.TargetProgram.Season,
                Section = sourceOrderItem.Section,
                CreditCardId = sourceOrderItem.CreditCardId,
                ItemStatus = OrderItemStatusCategories.completed,
                ItemStatusReason = OrderItemStatusReasons.transferIn,
                PaidAmount = 0,
                LastStep = sourceOrderItem.LastStep,
                IsMultiRegister = sourceOrderItem.IsMultiRegister,
            };

            if (sourceOrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                targetOrderItem.OrderItemScheduleParts = new List<OrderItemSchedulePart>();

                foreach (var orderItemPart in targetOrderItemScheduleParts)
                {
                    targetOrderItem.OrderItemScheduleParts.Add(new OrderItemSchedulePart
                    {
                        PartAmount = orderItemPart.PartAmount,
                        ProgramPartId = orderItemPart.ProgramPartId,
                    });
                }

                targetOrderItem.EntryFee = targetOrderItemScheduleParts.Sum(p => (decimal?)p.PartAmount) ?? 0;
            }

            #endregion

            result.TargetItem = targetOrderItem;
            result.SourceItem = sourceOrderItem;
            result.ListDeletedOldInstallments = listDeletedOldInstallments;
            result.ListPastInstallmentSchedule = listPastInstallmentSchedule;
            result.NewInstallments = orderInstallments;
            return result;
        }

        public dynamic GetSubscriptionInstallmentGetPastInstallment(long sourceOrderItemId, long sourceProgramId, long targetProgramId, long tutionId, DateTime? effectiveDate)
        {
            dynamic result = new System.Dynamic.ExpandoObject();

            var listRemainingOldInstallments = new List<OrderInstallment>();
            var listDeletedOldInstallments = new List<OrderInstallment>();
            var subscriptionNewInstallments = new List<OrderInstallment>();

            var pastinstallmentSubscriptionItems = new List<ProgramSchedulePart>();
            var listNewOrderInstallments = new List<ProgramSchedulePart>();
            var targetSubscriptionItems = new List<ProgramSchedulePart>();

            var targetOrderItemScheduleParts = new List<OrderItemSchedulePart>();

            var targetProgram = _programBusiness.Get(targetProgramId);
            var orderItem = _orderItemBusiness.GetItem(sourceOrderItemId);
            CheckResourceForAccess(orderItem);

            var selectedTution = targetProgram.ProgramSchedules.SelectMany(s => s.Charges).SingleOrDefault(c => c.Id == tutionId);
            var installments = orderItem.Installments.Where(i => !i.IsDeleted).ToList();

            var installmentItemWithOutApplicationFee = installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted).ToList();
            var scheduleNewEffectiveDate = _subscriptionBusiness.GetMonthDesiredStartDate(orderItem.ProgramSchedule.ProgramId, effectiveDate.Value);

            var programParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);
            var listOrderItemSubscription = programParts.Where(p => installmentItemWithOutApplicationFee.Select(i => i.ProgramSchedulePartId).ToList().Contains(p.Id)).ToList();

            if (targetProgram.ProgramSchedules.Any() && targetProgram.ProgramSchedules.Last(p => !p.IsDeleted).Attributes != null)
            {
                targetSubscriptionItems = listOrderItemSubscription;
            }

            foreach (var schedulePart in listOrderItemSubscription)
            {
                foreach (var inst in installments)
                {
                    if (schedulePart.Id == inst.ProgramSchedulePartId)
                    {
                        if (schedulePart.StartDate < effectiveDate && schedulePart.EndDate >= effectiveDate || effectiveDate > schedulePart.EndDate)
                        {
                            listRemainingOldInstallments.Add(inst);
                            pastinstallmentSubscriptionItems.Add(schedulePart);
                        }
                        else
                        {
                            listDeletedOldInstallments.Add(inst);
                        }
                    }
                    else
                    {
                        if (inst.Type == InstallmentType.AddOn && inst.Status == OrderStatusCategories.completed)
                        {
                            var isPastAppFee = listRemainingOldInstallments.Select(i => i.InstallmentDate).ToList().Contains(inst.InstallmentDate);
                            if (!isPastAppFee)
                            {
                                listRemainingOldInstallments.Add(inst);
                            }
                        }
                    }
                }
            }

            decimal surCharge = 0;
            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
            {
                var surchargesAmount = _orderItemBusiness.CalculateOrderItemSurCharge(orderItem);
                var totalSurchargeAmount = surchargesAmount;
                surCharge = totalSurchargeAmount / installments.Count;
            }

            var Discounts = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount && c.Category != ChargeDiscountCategory.CustomDiscount).Select(c =>
                new ChargeDiscountItemViewModel
                {
                    Amount = c.Amount,
                    Category = c.Category,
                    Name = c.Name,
                    SubCategory = c.Subcategory,
                })
                .ToList();

            decimal totalDiscountAmount = 0;
            decimal discountAmount = 0;

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
            {
                totalDiscountAmount = Discounts.Sum(p => (decimal?)p.Amount) ?? 0;
                discountAmount = totalDiscountAmount / (installments.Count - 1);
            }

            // fill list of new installment
            listNewOrderInstallments.AddRange(targetSubscriptionItems.Where(s => !pastinstallmentSubscriptionItems.Select(sc => sc.StartDate).ToList().Contains(s.StartDate)));

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
            {
                var newInstallmentAmount = (selectedTution.Amount + discountAmount) + surCharge;
                foreach (var schedule in listNewOrderInstallments)
                {
                    subscriptionNewInstallments.Add(new OrderInstallment()
                    {
                        Amount = newInstallmentAmount,
                        InstallmentDate = schedule.DueDate.Value,
                        Token = null,
                        PaidAmount = 0,
                        PaidDate = null,
                        NoticeSent = false,
                        EnableReminder = false,
                        Type = InstallmentType.Noraml,
                        Status = OrderStatusCategories.initiated,
                        ProgramSchedulePartId = schedule.Id
                    });

                    //add new installment amount to order amount
                    orderItem.Order.OrderAmount += newInstallmentAmount;
                }
            }
            else
            {
                var charges = _programBusiness.GetProgramPartCharges(tutionId);
                var discounts = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount && c.Category != ChargeDiscountCategory.CustomDiscount).ToList();

                foreach (var schedule in listNewOrderInstallments)
                {
                    var mainAmount = charges.Where(c => c.SchedulePartId == schedule.Id).FirstOrDefault().ChargeAmount;
                    var amount = mainAmount;

                    discountAmount = _orderItemBusiness.calculateDiscountAmount(discounts, amount);
                    surCharge = 0;

                    foreach (var sur in orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge))
                    {
                        surCharge += _orderItemBusiness.CalculateOneSurCharge(orderItem, sur, amount - discountAmount);
                    }

                    subscriptionNewInstallments.Add(new OrderInstallment()
                    {
                        Amount = (amount - discountAmount) + surCharge,
                        InstallmentDate = schedule.DueDate.Value,
                        Token = null,
                        PaidAmount = 0,
                        PaidDate = null,
                        NoticeSent = false,
                        EnableReminder = false,
                        Type = InstallmentType.Noraml,
                        Status = OrderStatusCategories.initiated,
                        ProgramSchedulePartId = schedule.Id
                    });

                    targetOrderItemScheduleParts.Add(new OrderItemSchedulePart
                    {
                        PartAmount = mainAmount,
                        ProgramPartId = schedule.Id
                    });

                    //add new installment amount to order amount
                    orderItem.Order.OrderAmount += (amount - discountAmount) + surCharge;
                }
            }


            result.ListNewSchedule = listNewOrderInstallments;
            result.ListDeletedInstallments = listDeletedOldInstallments;
            result.ListRemainingOldInstallments = listRemainingOldInstallments;
            result.ListNewInstallment = subscriptionNewInstallments;
            result.PastInstallmentSchedules = pastinstallmentSubscriptionItems;
            result.ListOrderItemScheduleParts = targetOrderItemScheduleParts;

            return result;
        }

        [HttpGet]
        public virtual ActionResult GetSchedules(long programId, long orderItemId)
        {
            var model = new ProgramTransferViewModel();

            model.Schedules = new List<TransferScheduleViewModel>();
            var program = _programBusiness.Get(programId);
            var schedules = _programBusiness.Get(programId).ProgramSchedules.Where(s => !s.IsDeleted).ToList();
            model.IsFrozen = schedules.FirstOrDefault().Program.Status == ProgramStatus.Frozen;

            foreach (var schedule in schedules)
            {
                var scheduleModel = new TransferScheduleViewModel();

                scheduleModel.Id = schedule.Id;
                scheduleModel.Dates = string.Format("{0} - {1}", schedule.StartDate.ToString(Constants.DateTime_Comma), schedule.EndDate.ToString(Constants.DateTime_Comma));
                scheduleModel.Title = schedule.Title;

                scheduleModel.Tutions = new List<TransferTutionViewModel>();

                foreach (var tuition in schedule.Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted))
                {
                    var tuitionModel = new TransferTutionViewModel();

                    if (program.TypeCategory == ProgramTypeCategory.Subscription)
                    {
                        tuitionModel.WeekDay = ((SubscriptionChargeAttribute)tuition.Attributes).WeekDays;
                    }
                    else if (program.TypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        tuitionModel.WeekDay = int.Parse(((BeforeAfterCareChargeAttribute)tuition.Attributes).NumberOfClassDay);
                    }

                    tuitionModel.Id = tuition.Id;
                    tuitionModel.Name = tuition.Capacity == 0 ? tuition.Name : string.Format("{0} (Capacity: {1} - Registered: {2})", tuition.Name, tuition.Capacity, _orderItemBusiness.RegisteredCount(tuition.Id, tuition.ProgramSchedule.Program.Season.Status == SeasonStatus.Test));
                    tuitionModel.Price = tuition.Amount;
                    tuitionModel.IsProrate = tuition.Attributes.IsProrate;

                    tuitionModel.IsEarlyBird = tuition.HasEarlyBird;

                    scheduleModel.Tutions.Add(tuitionModel);
                }

                model.Schedules.Add(scheduleModel);
            }

            return JsonNet(model);
        }

        [HttpGet]
        public virtual ActionResult GetSubscriptionDays(long programId)
        {
            var model = new SubscriptionDaysViewModel();

            var program = _programBusiness.Get(programId);

            if (program.TypeCategory == ProgramTypeCategory.Subscription)
            {
                var attribute = program.ProgramSchedules.FirstOrDefault().Attributes as ScheduleSubscriptionAttribute;

                if (attribute != null)
                {
                    model.AllDays = attribute.Days.Select(s =>
                            new SubscriptionProgramDaysViewModel
                            {
                                Title = s.DayOfWeek.ToDescription(),
                                Day = s.DayOfWeek
                            })
                              .ToList();
                }
            }
            else
            {
                var attribute = program.ProgramSchedules.Where(s => !s.IsDeleted).FirstOrDefault().Attributes as ScheduleAfterBeforeCareAttribute;

                if (attribute != null)
                {
                    model.AllDays = attribute.Days.Select(s =>
                            new SubscriptionProgramDaysViewModel
                            {
                                Title = s.DayOfWeek.ToDescription(),
                                Day = s.DayOfWeek
                            })
                              .ToList();
                }
            }


            return JsonNet(model);
        }
        [HttpGet]
        public virtual ActionResult GetChargeDiscounts(long sourceOrderItemId, long sourceProgramId, long targetProgramId, long tutionId)
        {
            var model = new TransferChargeDiscountViewModel()
            {
                OrderChargeDiscounts = new List<ChargeDiscountItemViewModel>()
            };

            var selectedChargeDiscounts = new List<ChargeDiscountItemViewModel>();

            var sourceProgram = _programBusiness.Get(sourceProgramId);

            var targetProgram = _programBusiness.Get(targetProgramId);

            var sourceOrderItem = _orderItemBusiness.GetItem(sourceOrderItemId);

            var selectedTution = targetProgram.ProgramSchedules.SelectMany(s => s.Charges).SingleOrDefault(c => c.Id == tutionId);

            var targetDiscounts = Ioc.DiscountBusiness.GetProgramsDiscounts(targetProgram).Where(d => !d.IsDeleted).Select(p =>
                new ChargeDiscountItemViewModel
                {
                    Id = p.Id,
                    Amount = p.AmountType == ChargeDiscountType.Percent ? p.Amount * selectedTution.Amount / 100 : p.Amount,
                    Name = p.Name,
                    Category = p.Category,
                    SubCategory = ChargeDiscountSubcategory.Discount,
                    AmountType = p.AmountType,
                    DiscountId = p.Id
                })
                .ToList();

            foreach (OrderChargeDiscount sourceChargeDiscount in sourceOrderItem.OrderChargeDiscounts)
            {
                switch (sourceChargeDiscount.Subcategory)
                {
                    case ChargeDiscountSubcategory.Charge:
                        {

                        }
                        break;
                    case ChargeDiscountSubcategory.Discount:
                        {
                            foreach (var targetDiscount in targetDiscounts)
                            {
                                if (sourceChargeDiscount != null && sourceChargeDiscount.DiscountId == targetDiscount.Id)
                                {
                                    targetDiscount.Checked = true;
                                    selectedChargeDiscounts.Add(targetDiscount);
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            model.OrderChargeDiscounts.Add(new ChargeDiscountItemViewModel { Checked = false, Name = "Custom discount", Id = -100, SubCategory = ChargeDiscountSubcategory.Discount, Category = ChargeDiscountCategory.CustomDiscount });
            model.OrderChargeDiscounts.AddRange(targetDiscounts);

            model.OrderChargeDiscounts.AddRange(_chargeBusiness.GetChargesForTransfer(sourceOrderItem, targetProgram, tutionId));

            foreach (var chargeDiscount in selectedChargeDiscounts)
            {
                model.OrderChargeDiscounts.SingleOrDefault(a => a.Id == chargeDiscount.Id).Checked = true;
            }

            model.OrderChargeDiscounts.Distinct().ToList();
            return JsonNet(model);
        }

        public virtual JsonNetResult GetUserTransactions(int userId, string searchTerm, int participantId, string startDate, string endDate, int clubId = 0)
        {
            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id).IsPartner ? clubBusiness.Get(clubId) : clubBusiness.Get(ActiveClub.Id);
            var user = club.Users.Single(u => u.UserId == userId);

            this.EnsureUserIsAdminForClub(club.Domain);
            if (Ioc.ClubBusiness.IsUserBelongToTheClub(club.Id, userId))
            {

                var query = Ioc.TransactionActivityBusiness.GetAllClubTransactionByUserId(userId, club.Id);

                if (participantId > 0)
                {
                    query = query.Where(item => item.OrderItem.PlayerId == participantId);

                }
                if (!string.IsNullOrEmpty(searchTerm))
                {
                    if (new Regex(Constants.ConfirmationIdRegex).Match(searchTerm.Trim()).Success)
                    {
                        query = query.Where(item => item.Order.ConfirmationId == searchTerm.Trim());
                    }
                    else
                    {
                        query = query.Where(item => item.PaymentDetail.TransactionId == searchTerm.Trim());
                    }
                }

                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime date;
                    if (DateTime.TryParse(startDate, out date))
                    {
                        query = query.Where(item => DbFunctions.TruncateTime(item.TransactionDate) >= DbFunctions.TruncateTime(date));
                    }
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime date;
                    if (DateTime.TryParse(endDate, out date))
                    {

                        query = query.Where(item => DbFunctions.TruncateTime(item.TransactionDate) <= DbFunctions.TruncateTime(date));
                    }
                }
                var pageSize = int.Parse(Request.Params["PageSize"]);
                var page = int.Parse(Request.Params["Page"]);

                var model = new List<TransactionActivitiesViewModel>();
                if (query.Any())
                {

                    TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(club.TimeZone.ToDescription());
                    foreach (var trans in query.OrderByDescending(c => c.TransactionDate).ToList().GroupBy(c => c.PaymentDetailId))
                    {
                        var seasonId = trans.FirstOrDefault().SeasonId;
                        var programScheduleId = trans.FirstOrDefault().OrderItem.ProgramScheduleId;
                        if (seasonId != null && programScheduleId.HasValue)
                        {
                            var item = trans.First().ToViewModel<TransactionActivitiesViewModel>();
                            item.Currency = club.Currency;
                            item.ProgramId = trans.First().OrderItem.ProgramScheduleId.HasValue ? trans.First().OrderItem.ProgramSchedule.ProgramId : 0;
                            item.Amount = trans.Sum(c => c.Amount);
                            item.ConfirmationId = trans.First().Order.ConfirmationId;
                            item.Participant = trans.First().OrderItem.FullName;
                            item.TransactionDate = TimeZoneInfo.ConvertTimeFromUtc(trans.First().TransactionDate, localTimeZone);
                            item.SeasonDomain = trans.First().OrderItem.Season.Domain;
                            item.EnableUndoRefund = _transactionActivityBusiness.EnableUndoRefundToOrder(user, trans.First());
                            item.OrderItemId = trans.First().OrderItem.Id;
                            model.Add(item);
                        }
                        else
                        {
                            var seasondomain = trans.Select(c => c.Club).Last().Domain;
                            var participant = _playerProfileBusiness.GetList().Where(p => p.UserId == userId).First().Contact.FullName;
                            var item = trans.First().ToViewModel<TransactionActivitiesViewModel>();
                            item.Currency = club.Currency;
                            item.ProgramId = trans.First().OrderItem.ProgramScheduleId.HasValue ? trans.First().OrderItem.ProgramSchedule.ProgramId : 0;
                            item.Amount = trans.Sum(c => c.Amount);
                            item.ConfirmationId = trans.First().Order.ConfirmationId;
                            item.Participant = participant;
                            item.TransactionDate = TimeZoneInfo.ConvertTimeFromUtc(trans.First().TransactionDate, localTimeZone);
                            item.SeasonDomain = seasondomain;
                            item.EnableUndoRefund = _transactionActivityBusiness.EnableUndoRefundToOrder(user, trans.First());
                            item.OrderItemId = trans.First().OrderItem.Id;
                            model.Add(item);
                        }


                    }
                    return JsonNet(new { data = model.Skip((page - 1) * pageSize).Take(pageSize), total = model.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
                }

                return JsonNet(new { data = model, total = 0 }, new Newtonsoft.Json.JsonSerializerSettings(), "");
            }
            return JsonNet(null, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual JsonNetResult GetUserOrders(int userId, string confirmationId, int participantId, int statusReasonId, string startDate, string endDate, int clubId = 0)
        {
            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id).IsPartner ? clubBusiness.Get(clubId) : clubBusiness.Get(ActiveClub.Id);

            this.EnsureUserIsAdminForClub(club.Domain);

            if (clubBusiness.IsUserBelongToTheClub(club.Id, userId))
            {
                IQueryable<OrderItem> query = _orderItemBusiness.GetClubUserOrderItems(club.Id, userId, false, OrderItemStatusCategories.showAll).Where(c => c.ItemStatus == OrderItemStatusCategories.completed || c.ItemStatus == OrderItemStatusCategories.changed);

                if (!string.IsNullOrEmpty(confirmationId))
                {
                    query = query.Where(c => c.Order.ConfirmationId.ToLower() == confirmationId.ToLower().Trim());
                }
                if (statusReasonId == 0)
                {
                    query = query.Where(c => c.ItemStatus == OrderItemStatusCategories.completed);
                }
                else if (statusReasonId > 0)
                {
                    query = query.Where(c => c.ItemStatusReason == (OrderItemStatusReasons)statusReasonId);
                }
                if (participantId > 0)
                {
                    query = query.Where(c => c.PlayerId == participantId);

                }
                if (!string.IsNullOrEmpty(startDate))
                {
                    DateTime date;
                    if (DateTime.TryParse(startDate, out date))
                    {
                        query = query.Where(oitem => DbFunctions.TruncateTime(oitem.Order.CompleteDate) >= DbFunctions.TruncateTime(date));
                    }
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    DateTime date;
                    if (DateTime.TryParse(endDate, out date))
                    {

                        query = query.Where(oitem => DbFunctions.TruncateTime(oitem.Order.CompleteDate) <= DbFunctions.TruncateTime(date));
                    }
                }
                var pageSize = int.Parse(Request.Params["PageSize"]);
                var page = int.Parse(Request.Params["Page"]);
                var AccountService = Ioc.AccountingBusiness;

                TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(club.TimeZone.ToDescription());

                var model = query.OrderByDescending(item => item.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList()
                          .Select(item => new OrdersOfProgramViewModel()
                          {
                              OrderId = item.Order_Id,
                              Id = item.Id,
                              UserId = item.Order.UserId,
                              Attendee = item.FullName,
                              EntryFee = CurrencyHelper.FormatCurrencyWithPenny(item.EntryFee, club.Currency),
                              OrderDate = TimeZoneInfo.ConvertTimeFromUtc(item.Order.CompleteDate, localTimeZone),
                              ProgramType = item.ProgramScheduleId.HasValue ? item.ProgramSchedule.Program.TypeCategory.ToString() : string.Empty,
                              ConfirmationId = item.Order.ConfirmationId,
                              ItemStatus = (int)item.ItemStatus,
                              ItemStatusReason = (int)item.ItemStatusReason,
                              EntryFeeName = item.EntryFeeName,
                              ProgramName = item.ProgramScheduleId.HasValue ? _programBusiness.GetProgramName(item.ProgramSchedule, item.EntryFeeName) : "Donation",
                              ProgramId = item.ProgramScheduleId.HasValue ? item.ProgramSchedule.ProgramId : 0,
                              Season = item.SeasonId.HasValue ? item.Season.Title : Constants.S_Dash,
                              SeasonId = item.SeasonId,
                              SeasonDomain = item.SeasonId.HasValue ? item.Season.Domain : string.Empty,
                              Balance = AccountService.OrderItemBalance(item),
                              Installments = _orderInstallmentBusiness.GetOrderItemInstallments(item.Id, null, 0, int.MaxValue, this.GetCurrentUserRoleType(), InstallmentType.Noraml),
                              AddOnInstallments = _orderInstallmentBusiness.GetOrderItemInstallments(item.Id , null, 0, int.MaxValue, this.GetCurrentUserRoleType(), InstallmentType.AddOn),
                              IsDropIn = item.Mode == OrderItemMode.DropIn,
                              PaymentPlanType = item.PaymentPlanType,
                              IsPartner = Ioc.ClubBusiness.Get(ActiveClub.Id).IsPartner ? false : true
                          }).ToList();

                return JsonNet(new { data = model, total = query.Count() }, new Newtonsoft.Json.JsonSerializerSettings(),
               "");
            }
            return JsonNet(null, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual JsonResult SendReservation(long orderItemId)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            PaymentMethod? paymentMethod = null;

            if (orderItem != null)
            {
                EmailService.Get(this.ControllerContext).SendReservationEmail(orderItem.Order.ConfirmationId, paymentMethod, Request.Url.Host);
                return Json(new JResult { Status = true, Message = "", RecordsAffected = 1 });
            }
            else { return Json(new JResult { Status = false, Message = "", RecordsAffected = 1 }); }


        }

        #region Lottery
        public JsonNetResult GetLotteryItems(PaginationModel paginationModel, int programId)
        {
            var lotteryItems = _lotteryBusiness.GetLotteryItems(programId, paginationModel.Page, paginationModel.PageSize);

            return JsonNet(lotteryItems);
        }

        [HttpPost]
        public ActionResult SendEmailToLotteries(List<long> orderItemIds)
        {
            var result = Ioc.CampaignBusiness.MakeCampaignOnLottery(orderItemIds, ActiveClub.Domain);
            var campaignId = result.RecordsAffected;

            return JsonNet(new { result.Status, Data = campaignId, Message = result.ExceptionMessage });
        }

        [HttpPost]
        public JsonNetResult Draw(int programId)
        {
            var result = _lotteryBusiness.Draw(programId);

            return JsonNet(result);
        }

        [HttpPost]
        public JsonNetResult RemoveLotteryItem(long orderItemId)
        {
            var result = _lotteryBusiness.RemoveLotteryItem(orderItemId);

            return JsonNet(result);
        }

        [HttpPost]
        public JsonNetResult AddLotteryItem(long orderItemId)
        {
            var result = _lotteryBusiness.AddLotteryItem(orderItemId);

            return JsonNet(result);
        }

        [HttpPost]
        public JsonNetResult FinalizeLottery(int programId)
        {
            var result = _lotteryBusiness.FinalizeLottery(programId);

            return JsonNet(result);
        }

        public ActionResult GetLotteryDetails(int programId)
        {
            var result = _lotteryBusiness.GetLotteryDetails(programId);

            return JsonNet(result);
        }

        public ActionResult CompleteLotteryItem(long orderItemId)
        {
            var order = _lotteryBusiness.CompleteLotteryItem(orderItemId);

            EmailService.Get(this.ControllerContext).SendReservationEmail(order.ConfirmationId, null, Request.Url.Host, null, ReservationEmailMode.Normal, order.OrderMode);

            return JsonNet(true);
        }

        #endregion

        [JbAuthorize(JbAction.Orders_View)]
        public virtual JsonNetResult GetList(PaginationModel paginationModel, int programId, long? scheduleId, string term, string startDate, string endDate, string chessSchedule, string chessSection, decimal? entryFeeId, int? statusReasonId, bool makeCampaign = false, ParticipantSearchType? searchType = null, string firstName = null, string lastName = null)
        {
            var program = _programBusiness.Get(programId);

            var clubBusiness = Ioc.ClubBusiness;
            var club = clubBusiness.Get(ActiveClub.Id).IsPartner ? program.Club : clubBusiness.Get(ActiveClub.Id);

            if (_programBusiness.IsProgramBelongsToClub(club.Id, programId) || (program.OutSourceSeason != null && program.OutSourceSeason.ClubId == club.Id))
            {
                bool isTestMode = false;

                if (program.Season.Status == SeasonStatus.Test)
                {
                    isTestMode = true;
                }

                IQueryable<OrderItem> query = _orderItemBusiness.GetOrderItems(programId, isTestMode, OrderItemStatusCategories.showAll);

                if (statusReasonId.HasValue)
                {
                    if (statusReasonId.Value == 0)
                    {
                        query = query.Where(c => c.ItemStatus == OrderItemStatusCategories.completed);
                    }
                    else if (statusReasonId.Value == 7)
                    {
                        query = query.Where(c => c.ItemStatus == OrderItemStatusCategories.initiated);
                    }
                    else if (statusReasonId.Value > 0)
                    {
                        query = query.Where(c => c.ItemStatusReason == (OrderItemStatusReasons)statusReasonId.Value);
                    }
                }
                if (scheduleId.HasValue && scheduleId.Value > 0)
                {
                    query = query.Where(item => item.ProgramScheduleId == scheduleId);

                }

                if (searchType.HasValue)
                {
                    switch (searchType.Value)
                    {
                        case ParticipantSearchType.Email:
                            {
                                if (!string.IsNullOrWhiteSpace(term))
                                {
                                    var players = _playerProfileBusiness.GetList(email: term).Select(p => p.Id).ToList();

                                    var players2 = _playerProfileBusiness.GetList().Where(u => u.User.UserName == term).Select(p => p.Id);

                                    players.AddRange(players2);

                                    players = players.GroupBy(x => x).Select(y => y.First()).ToList();
                                    query = query.Where(item => players.Contains(item.PlayerId.Value));
                                }
                            }
                            break;
                        case ParticipantSearchType.ConfirmationId:
                            {
                                if (!string.IsNullOrWhiteSpace(term))
                                {
                                    query = query.Where(item => item.Order.ConfirmationId == term.Trim());
                                }
                            }
                            break;
                        case ParticipantSearchType.Name:
                            {
                                if (!string.IsNullOrWhiteSpace(firstName) || !string.IsNullOrWhiteSpace(lastName))
                                {
                                    query = query.Where(item => (string.IsNullOrEmpty(firstName) || item.FirstName.Contains(firstName)) && (string.IsNullOrEmpty(lastName) || item.LastName.Contains(lastName)));
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(startDate))
                {
                    var date = DateTime.Parse(startDate);
                    query = query.Where(oitem => oitem.Order.CompleteDate >= date);
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    var date = DateTime.Parse(endDate);
                    date = date.AddHours(24 - date.Hour);
                    query = query.Where(oitem => oitem.Order.CompleteDate < date);
                }
                if (!string.IsNullOrEmpty(chessSchedule))
                {
                    query = query.Where(oitem => oitem.OrderItemChess.Schedule == chessSchedule);
                }
                if (!string.IsNullOrEmpty(chessSection))
                {
                    query = query.Where(oitem => oitem.OrderItemChess.Section == chessSection);
                }
                if (entryFeeId.HasValue && entryFeeId.Value > 0)
                {
                    query = query.Where(oitem => oitem.OrderChargeDiscounts.Any(cd => !cd.IsDeleted && cd.ChargeId == entryFeeId));
                }

                if (!makeCampaign)
                {
                    var AccountService = Ioc.AccountingBusiness;
                    var model = query.ToList()
                              .Select(item => new OrdersOfProgramViewModel()
                              {
                                  IsSandbox = !item.Order.IsLive,
                                  OrderId = item.Order_Id,
                                  Id = item.Id,
                                  Attendee = item.FullName,
                                  EntryFee = CurrencyHelper.FormatCurrencyWithPenny(item.EntryFee, club.Currency),
                                  Schedule = _programBusiness.GetProgramScheduleTitleAndDate(item),
                                  OrderDate = !item.Order.IsDraft ? _clubBusiness.GetClubDateTime(club.Id, item.Order.CompleteDate) : (DateTime?)null,
                                  ConfirmationId = !string.IsNullOrEmpty(item.Order.ConfirmationId) ? item.Order.ConfirmationId : string.Empty,
                                  ItemStatus = (int)item.ItemStatus,
                                  ItemStatusReason = (int)item.ItemStatusReason,
                                  Installments = _orderInstallmentBusiness.GetOrderItemInstallments(item.Id, null, 0, int.MaxValue, this.GetCurrentUserRoleType(), InstallmentType.Noraml),
                                  AddOnInstallments = _orderInstallmentBusiness.GetOrderItemInstallments(item.Id, null, 0, int.MaxValue, this.GetCurrentUserRoleType(), InstallmentType.AddOn),
                                  EntryFeeName = item.EntryFeeName,
                                  Balance = AccountService.OrderItemBalance(item),
                                  ProgramType = item.ProgramSchedule.Program.TypeCategory.ToString(),
                                  ChessSchedule = item.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament ?
                                  _programBusiness.GetProgramScheduleTitleAndDate(item, item.OrderItemChess.Schedule) :
                                  _programBusiness.GetProgramScheduleTitleAndDate(item, ""),
                                  ChessSection =
                                  item.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament
                                     ? item.OrderItemChess.Section
                                     : "",
                                  UserId = item.Order.UserId,
                                  IsDropIn = item.Mode == OrderItemMode.DropIn,
                                  PaymentPlanType = item.PaymentPlanType
                              });

                    var itemsTotalAmount = query.Any() ? query.Sum(item => item.TotalAmount) : 0;
                    var datasource = model.Page(paginationModel);
                    return JsonNet(new { data = datasource, total = query.Count(), totalAmount = itemsTotalAmount }, new Newtonsoft.Json.JsonSerializerSettings(), string.Empty);
                }
                else
                {
                    var campaignId = 0;

                    query = query.Where(i => i.ItemStatusReason != OrderItemStatusReasons.transferOut && i.ItemStatusReason != OrderItemStatusReasons.canceled);
                    var recipients = (query.Any()) ? query.Select(o => o.Order.User.UserName).Distinct().ToList() : new List<string>();
                    var campaignName = (query.Any()) ? query.Select(o => o.ProgramSchedule).First().Program.Name : "Campaign";
                    var result = Ioc.CampaignBusiness.MakeCampaignOnReport(recipients, club.Domain, campaignName);
                    campaignId = result.RecordsAffected;
                    return JsonNet(new { result.Status, Data = campaignId, Message = result.ExceptionMessage });
                }
            }
            return JsonNet(null, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual ActionResult GetDetails(int orderId)
        {
            var model = new OrderViewModel();
            Order order = Ioc.OrderBusiness.Get(orderId);
            var club = base.ActiveClub;

            var outsourceClubDomain = string.Empty;

            if (order != null && order.OrderItems.Any() && order.OrderItems.First().ProgramScheduleId.HasValue && order.OrderItems.First().ProgramSchedule.Program.OutSourceSeasonId.HasValue)
            {
                outsourceClubDomain = order.OrderItems.First().ProgramSchedule.Program.OutSourceSeason.Club.Domain;
            }

            if (order != null && Ioc.OrderBusiness.IsUserOwnsTheOrder(this.GetCurrentUserId(), order.UserId, order.Club.Domain, outsourceClubDomain))
            {

                model = new OrderViewModel()
                {
                    Id = order.Id,
                    Currency = order.Club.Currency,
                    OrderMode = order.OrderMode,
                    UserName = order.User.UserName,
                    OrderDate = Ioc.ClubBusiness.GetClubDateTime(order.ClubId, order.CompleteDate).ToString(Constants.DefaultDateTimeFormatWithAMPM, new CultureInfo("en-US")),
                    ConfirmationId = order.ConfirmationId,
                    OrderAmount = order.OrderAmount,
                    OrderItems = order.RegularOrderItems.Select(item => new OrderItemsViewModel(order.Club.Currency)
                    {
                        FullName = item.FullName,
                        TotalAmount = item.TotalAmount,
                        Name = item.Name,
                        ItemStatusReason = item.ItemStatusReason,
                        Id = item.Id,
                        PaidAmount = item.PaidAmount,
                        Balance = Ioc.AccountingBusiness.OrderItemBalance(item),
                        PaymentPlanType = item.PaymentPlanType
                    }).ToList(),

                };

            }
            return JsonNet(model);
        }

        public virtual ActionResult GetPunchCardSessions(long orderItemId)
        {
            var model = new List<PunchCardSessionItemViewModel>();

            var clubBusiness = Ioc.ClubBusiness;

            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var club = orderItem.Order.Club;

            var programSchedule = orderItem.ProgramSchedule;

            var programScheduleAttribute = programSchedule.Attributes as ScheduleAfterBeforeCareAttribute;

            var sessionNumbers = programScheduleAttribute.PunchCard.SessionNumber;

            for (int i = 1; i <= sessionNumbers; i++)
            {
                var session = new PunchCardSessionItemViewModel()
                {
                    Id = -i,
                };

                model.Add(session);
            }

            var orderSessions = orderItem.OrderSessions;



            foreach (var item in orderSessions.OrderBy(o => o.ProgramSession.StartDateTime).GroupBy(o => o.ProgramSession.StartDateTime.Date))
            {
                var condidateItem = model.OrderByDescending(s => s.Id).First(s => s.Id < 0);

                condidateItem.Id = item.First().Id;
                condidateItem.StartDate = item.First().ProgramSession.StartDateTime;
                condidateItem.EndDate = item.First().ProgramSession.EndDateTime;
                condidateItem.DateCreated = clubBusiness.GetClubDateTime(club, item.First().MetaData.DateCreated);
                condidateItem.BeforeAfter = item.First().ProgramSession.ProgramSchedule.Title;
                condidateItem.Status = PunchcardAssignStatus.Assigned;

                if (item.Count() > 1)
                {
                    condidateItem.StartDate2 = item.Last().ProgramSession.StartDateTime;
                    condidateItem.EndDate2 = item.Last().ProgramSession.EndDateTime;
                    condidateItem.BeforeAfter2 = item.Last().ProgramSession.ProgramSchedule.Title;
                    condidateItem.HasTwoSessionInADay = true;
                }
            }

            model.Where(s => s.BeforeAfter == null).ToList().ForEach(s => s.BeforeAfter = string.Empty);
            model.Where(s => s.BeforeAfter2 == null).ToList().ForEach(s => s.BeforeAfter2 = string.Empty);

            model = model.OrderByDescending(o => o.StartDate.HasValue).ThenBy(o => o.StartDate).ThenByDescending(o => o.Id).ToList();

            for (int i = 1; i <= model.Count; i++)
            {
                model[i - 1].Title = string.Concat("Day #", i.ToString());
            }

            return JsonNet(new { data = model, total = model.Count() });
        }

        [JbAuthorize(JbAction.OrderDetail_View)]
        public virtual ActionResult GetOrderItem(long orderItemId, long? installmentId, int changeFormId = 0, bool editable = false, bool updateForm = false)
        {
            var model = GetOrderItemViewModel(orderItemId, installmentId, changeFormId, editable, updateForm);

            if (model == null)
                return JsonNet(false);

            return JsonNet(model);
        }

        public virtual ActionResult UndoSuspension(long installmentId)
        {
            var operationStatus = _orderInstallmentBusiness.UndoFailedInstallment(installmentId);

            if (operationStatus.Status)
                return Json(new JResult() { Status = true });

            return Json(new JResult() { Status = false, Message = operationStatus.Message });
        }
        public virtual ActionResult EditOrderForm(JbForm formData, long orderItemId)
        {
            try
            {
                var registerService = RegisterService.Get();
                PlayerProfile playerProfile = null;
                OperationStatus status = new OperationStatus();

                if (ModelState.IsValid)
                {
                    var orderItemBusiness = _orderItemBusiness;

                    var orderItem = orderItemBusiness.GetItem(orderItemId);
                    CheckResourceForAccess(orderItem);

                    var errorParent = registerService.SetFormToParentDashboard(formData, orderItem.PlayerId, orderItem.Order.UserId, ref playerProfile);

                    if (!errorParent.Status)
                    {
                        ManipulateParentDashboardValidations(formData, errorParent.Message);
                    }

                    if (ModelState.IsValid)
                    {
                        _playerProfileBusiness.Update(orderItem.Player);
                        status = orderItemBusiness.Update(orderItem);
                        var result = Ioc.JbFormBusiness.CreateEdit(formData); //2734

                        if (result.Status)
                        {
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.EditRegistrationForm, "Registration form is edited by admin.", orderItem.Order_Id, User.Identity.GetCurrentUserId(), orderItem.Id);
                        }

                        return Json(new JResult() { Status = true });
                    }
                    else
                    {
                        var errorMsg = string.Empty;
                        foreach (var modelState in ModelState.Values.Where(c => c.Errors.Any()))
                        {
                            errorMsg += modelState.Errors.First().ErrorMessage + Environment.NewLine;
                        }
                        return Json(new JResult() { Status = false, Message = errorMsg });
                    }
                }
                else { return Json(new JResult() { Status = false }); }
            }
            catch (Exception ex)
            {
                return Json(new JResult() { Status = false, Message = ex.Message });
            }
        }

        private void ManipulateParentDashboardValidations(JbForm jbForm, string message)
        {
            if (message == "Participant")
            {
                ModelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.FirstName, "The participant already exists.");
            }

            if (message == "Parent1")
            {
                ModelState.AddModelError(jbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName, "The parent you have entered already exists in the form.");
            }

            if (message == "Parent2")
            {
                ModelState.AddModelError(jbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName, "The parent you have entered already exists in the form.");
            }

            if (message == "authorizedPickup1")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup1, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }

            if (message == "authorizedPickup2")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup2, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }

            if (message == "authorizedPickup3")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup3, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }

            if (message == "authorizedPickup4")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup4, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }
        }

        public virtual JsonNetResult GetOrder(int orderId, long orderitemId = 0)
        {
            Order order = Ioc.OrderBusiness.Get(orderId);
            OrderViewModel model = new OrderViewModel();
            var outsourceClubDomain = string.Empty;

            if (order != null && order.OrderItems.Any() && order.OrderItems.First().ProgramScheduleId.HasValue && order.OrderItems.First().ProgramSchedule.Program.OutSourceSeasonId.HasValue)
            {
                outsourceClubDomain = order.OrderItems.First().ProgramSchedule.Program.OutSourceSeason.Club.Domain;
            }

            if (order != null && Ioc.OrderBusiness.IsUserOwnsTheOrder(this.GetCurrentUserId(), order.UserId, order.Club.Domain, outsourceClubDomain))
            {
                model = order.ToViewModel<OrderViewModel>();
                model.Currency = order.Club.Currency;
                model.OrderDate = Ioc.ClubBusiness.GetClubDateTime(order.ClubId, order.CompleteDate).ToString(Constants.DefaultDateTimeFormatWithAMPM, new CultureInfo("en-US"));
                if (orderitemId > 0)
                {
                    var accountService = Ioc.AccountingBusiness;
                    model.CancelAll = false;
                    model.OrderItems.ForEach(o => { o.IsSelected = false; o.Charges.ForEach(c => c.IsSelected = false); });
                    var orderItem = model.OrderItems.SingleOrDefault(o => o.Id == orderitemId);
                    if (orderItem != null)
                    {
                        orderItem.IsSelected = true;
                        orderItem.Charges.ForEach(c => c.IsSelected = true);
                    }
                }
                model.UserName = order.User.UserName;
            }
            return JsonNet(model);
        }

        public virtual JsonNetResult GetOrderItemTransactions(long orderItemId, long? installmentId = null)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var orderId = orderItem.Order_Id;
            var club = Ioc.ClubBusiness.Get(orderItem.Order.Club.Id);
            var transactions =
                Ioc.TransactionActivityBusiness.GetAllTransactionByOrderItemId(orderItemId)
                    .Where(
                        t =>
                            (t.TransactionStatus == TransactionStatus.Failure && t.TransactionCategory != TransactionCategory.Sale) ||
                           (t.TransactionStatus != TransactionStatus.Draft && t.TransactionStatus != TransactionStatus.Failure &&
                            t.TransactionStatus != TransactionStatus.Deleted && t.Amount != 0)
                            );

            if (installmentId.HasValue && installmentId.Value > 0)
            {
                transactions = transactions.Where(c => c.InstallmentId.HasValue && c.InstallmentId.Value == installmentId.Value);
            }

            var user = club.Users.Single(u => u.UserId == orderItem.Order.UserId);

            if (transactions.Any())
            {
                TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(club.TimeZone.ToDescription());
                var model = new List<TransactionActivitiesViewModel>();
                foreach (var trans in transactions.OrderBy(c => c.TransactionDate).ToList().GroupBy(c => c.PaymentDetailId))
                {
                    var item = trans.First().ToViewModel<TransactionActivitiesViewModel>();
                    item.Currency = club.Currency;
                    item.ProgramId = trans.First().OrderItem.ProgramScheduleId.HasValue ? trans.First().OrderItem.ProgramSchedule.ProgramId : 0;
                    item.Amount = trans.Sum(c => c.Amount);
                    item.TransactionDate = TimeZoneInfo.ConvertTimeFromUtc(trans.First().TransactionDate, localTimeZone);
                    item.IsAutoCharge = trans.First().IsAutoCharge;
                    item.PaymentPlan = PaymentPlanType.FullPaid;
                    var cardLastDigits = trans.First().PaymentDetail.CreditCard?.LastDigits;
                    var cardBrand = trans.First().PaymentDetail.CreditCard?.Brand;
                    item.CardNumber = !string.IsNullOrWhiteSpace(cardLastDigits) ? string.Format("{0} **** {1}", cardBrand, cardLastDigits) : string.Empty;
                    item.EnableUndoRefund = _transactionActivityBusiness.EnableUndoRefundToOrder(user, trans.First());

                    if (trans.First().TransactionCategory == TransactionCategory.Invoice && trans.First().PaymentDetail.invoice != null)
                    {
                        var invoiceDate = trans.First().PaymentDetail.invoice.InvoicingDate;
                        var dueDateValue = trans.First().PaymentDetail.invoice.DueDate;

                        item.InvoiceCreateDate = DateTimeHelper.FormatDate(DateTimeHelper.ConvertUtcDateTimeToLocal(invoiceDate, orderItem.Order.Club.TimeZone));

                        item.InvoicePaidDate = trans.First().TransactionStatus != TransactionStatus.Invoice ?
                            DateTimeHelper.FormatDate(DateTimeHelper.ConvertUtcDateTimeToLocal(trans.First().TransactionDate, orderItem.Order.Club.TimeZone)) : string.Empty;
                        item.InvoiceNumber = trans.First().PaymentDetail.invoice.InvoiceNumber;

                        if (dueDateValue > 0)
                        {
                            item.InvoiceDueDate = DateTimeHelper.FormatDate(DateTimeHelper.ConvertUtcDateTimeToLocal(invoiceDate.AddDays(dueDateValue.Value), orderItem.Order.Club.TimeZone));
                        }
                        else
                        {
                            item.InvoiceDueDate = DateTimeHelper.FormatDate(DateTimeHelper.ConvertUtcDateTimeToLocal(invoiceDate, orderItem.Order.Club.TimeZone));
                        }
                    }

                    if (item.InstallmentId.HasValue)
                    {
                        var installmentTransaction = orderItem.Installments.First(i => i.Id == item.InstallmentId.Value);
                        item.StrDueDate = installmentTransaction.InstallmentDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US"));
                        item.PaymentPlan = PaymentPlanType.Installment;
                    }

                    model.Add(item);
                }
                return JsonNet(new { data = model, total = model.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
            }

            return JsonNet(new { data = new List<TransactionActivitiesViewModel>(), total = 0 }, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual JsonNetResult GetOrderItemInstallments(long orderItemId)
        {
            var result = _orderInstallmentBusiness.GetOrderItemInstallments(orderItemId, null, 0, int.MaxValue, this.GetCurrentUserRoleType(), InstallmentType.Noraml);
            return JsonNet(new { data = result, total = result.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual JsonNetResult GetOrderItemAddOnInstallments(long orderItemId)
        {
            var result = _orderInstallmentBusiness.GetOrderItemInstallments(orderItemId, null, 0, int.MaxValue, this.GetCurrentUserRoleType(), InstallmentType.AddOn);
            return JsonNet(new { data = result, total = result.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");

        }

        public virtual JsonNetResult GetMakePaymentModel(long orderitemId, string token)
        {
            var clubBusiness = Ioc.ClubBusiness;
            OrderItem orderItem = _orderItemBusiness.GetItem(orderitemId);
            CheckResourceForAccess(orderItem);

            var isTestMode = !orderItem.Order.IsLive;

            var orderItemViewModel = GetOrderItemViewModel(orderitemId, null, 0);

            var orderItemInDb = _orderItemBusiness.GetItem(orderitemId);
            var club = orderItemInDb.Order.Club;
            var paymentMethods = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId);


            MakePaymentViewModel model = new MakePaymentViewModel()
            {
                OrderItem = orderItemViewModel,
                IsStripeEnabled = (paymentMethods.EnableStripePayment || paymentMethods.EnableAuthorizePayment) ? true : false,
                IsPaypalEnabled = paymentMethods.EnablePaypalPayment,
                Username = orderItem.Order.User.UserName,
                AvailableCredit = isTestMode ? clubBusiness.GetUserCredit(orderItem.Order.Club, orderItem.Order.UserId, true) : clubBusiness.GetUserCredit(orderItem.Order.Club, orderItem.Order.UserId, false),
                OrderItemId = orderitemId,
                PaymentDate = Ioc.ClubBusiness.GetClubDateTime(orderItem.Order.ClubId, DateTime.UtcNow),
                ProgramId = orderItem.ProgramScheduleId.HasValue ? orderItem.ProgramSchedule.ProgramId : 0,
                SeasonDomain = orderItem.Season != null ? orderItem.Season.Domain : null,
                isPayInstallment = false
            };

            model.HasProgramScheduleId = orderItem.ProgramScheduleId.HasValue ? true : false;

            if (orderItemViewModel.HasInstallments)
            {
                model.isPayInstallment = true;
                model.Installments = orderItem.Installments.Where(i => !i.IsDeleted).Select(c => c.ToViewModel<InstallmentViewModel>()).ToList();
                model.Installments.ForEach(c => c.Currency = orderItem.Order.Club.Currency);
                model.Installments.First().IsDeposit = true;
                model.Installments.ForEach(c => c.NewBalance = c.OldBalance = c.Balance);

                var typeCategory = orderItem.ProgramTypeCategory;

                for (int i = 0; i < model.Installments.Count; i++)
                {
                    if (i == 0)
                    {
                        model.Installments[i].IsDeposit = true;
                        model.Installments[i].StrInstallmentDate = string.Format("{0} {1}", model.Installments[i].InstallmentDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US")), typeCategory != ProgramTypeCategory.Subscription && typeCategory != ProgramTypeCategory.BeforeAfterCare ? "(Deposit)" : "");
                    }
                    else
                    {
                        model.Installments[i].StrInstallmentDate = string.Format("{0}", model.Installments[i].InstallmentDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US")));
                    }
                }
            }

            if (!string.IsNullOrEmpty(token))
            {
                var transactions = orderItem.TransactionActivities.Where(c => c.Token == token && c.TransactionStatus == TransactionStatus.Draft);
                if (transactions != null && transactions.Count() > 0)
                {
                    model.Amount = transactions.Sum(c => c.Amount);
                    model.PaymentDate = transactions.First().TransactionDate;
                    model.Note = transactions.First().Note;
                    if (!string.IsNullOrEmpty(token))
                    {
                        foreach (var tran in transactions.Where(c => c.InstallmentId.HasValue && c.InstallmentId.Value > 0))
                        {
                            model.Installments.Single(c => c.Id == tran.InstallmentId.Value).IsSelected = true;
                        }
                    }
                }
                else
                {
                    long paymentdetailId = 0;

                    if (long.TryParse(token, out paymentdetailId))
                    {
                        var transaction = orderItem.TransactionActivities.Where(t => t.PaymentDetailId == paymentdetailId);
                        if (transaction != null && transaction.Any())
                        {

                            model.Amount = transaction.Sum(c => c.Amount);
                            model.PaymentDate = transaction.First().TransactionDate;
                            model.Note = transaction.First().Note;
                            model.SelectedChargeMethod = "0";
                            model.IsEdit = true;
                            model.PaymentDetailId = paymentdetailId;
                            model.CheckId = transaction.First().CheckId;
                            model.SelectedPaymentMethod = ((int)transaction.First().PaymentDetail.PaymentMethod).ToString();
                            if (transaction.Any(c => c.InstallmentId.HasValue && c.InstallmentId > 0))
                            {
                                foreach (var tran in transaction)
                                {
                                    var inst = model.Installments.SingleOrDefault(c => c.Id == tran.InstallmentId);
                                    if (inst != null)
                                    {
                                        inst.IsSelected = true;
                                        inst.OldBalance += tran.Amount;
                                        inst.IsBelongTo = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return JsonNet(model);
        }

        public virtual RedirectResult MakePaymentReturnStatus(string token, string message = "", bool status = true)
        {
            var transaction = Ioc.TransactionActivityBusiness.GetList().FirstOrDefault(c => c.Token == token);
            var orderItem = transaction.OrderItem;
            var seasonDomain = orderItem.Season != null ? orderItem.Season.Domain : null;
            var urlToRedirect = string.Format("{0}/Dashboard#/Season/{1}/OrderItem/{2}/Status/{3}/Message/{4}", Request.Url.GetLeftPart(UriPartial.Authority), seasonDomain, orderItem.Id, status, message);
            return Redirect(urlToRedirect);
        }

        public virtual RedirectResult FamilyMakePaymentReturnStatus(string message = "", bool status = true)
        {

            var urlToRedirect = string.Format("{0}/Dashboard#/FamilyTakePayment/Status/{1}/Message/{2}", Request.Url.GetLeftPart(UriPartial.Authority), status, message);
            return Redirect(urlToRedirect);
        }

        public virtual JsonNetResult GetPaymentMessageModel(long orderItemId, bool status)
        {
            var model = new PaymentMessageViewModel() { IsSuceed = status };

            if (orderItemId > 0)
            {
                var orderItem = _orderItemBusiness.GetItem(orderItemId);
                CheckResourceForAccess(orderItem);

                var programId = orderItem.ProgramScheduleId.HasValue ? orderItem.ProgramSchedule.ProgramId : 0;

                model.Username = orderItem.Order.User.UserName;
                model.SeasonDomain = orderItem.Season != null ? orderItem.Season.Domain : null;
                model.ProgramId = programId;
                model.OrderItemId = orderItemId;
                model.ConfirmationId = orderItem.Order.ConfirmationId;
                model.FullName = orderItem.FullName;
            }
            return JsonNet(model);
        }

        public virtual ActionResult SubmitMakePayment(MakePaymentViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = false };

            try
            {
                var orderItem = _orderItemBusiness.GetItem(model.OrderItemId);
                CheckResourceForAccess(orderItem);

                var club = orderItem.Order.Club;
                decimal balance = (model.IsEdit) ? orderItem.TransactionActivities.Where(c => c.PaymentDetailId == model.PaymentDetailId).Sum(c => c.Amount) : 0;
                var currency = club.Currency;
                if (club.PartnerId.HasValue && club.IsSchool)
                {
                    currency = Ioc.ClubBusiness.Get(club.PartnerId.Value).Currency;
                }
                if (model.isPayInstallment)
                {
                    foreach (var inst in model.Installments.Where(c => c.IsSelected))
                    {
                        balance += orderItem.Installments.Single(c => c.Id == inst.Id).Balance;
                    }
                }
                else
                {
                    balance += orderItem.TotalAmount - orderItem.PaidAmount;
                }

                if (balance <= 0)
                {
                    return Json(new { Status = result.Status, result = "Refresh", Message = Constants.E_Balance_Zero, RecordsAffected = 0 });
                }

                if (model.Amount < 0 || model.Amount > balance)
                {
                    var errorMsg = string.Format(Constants.E_Amount_Between, CurrencyHelper.FormatCurrencyWithPenny(1, currency), CurrencyHelper.FormatCurrencyWithPenny(model.Balance, currency));
                    return Json(new { Status = result.Status, result = "Refresh", Message = errorMsg, RecordsAffected = 0 });
                }

                List<OrderInstallment> selectedInstallment = null;
                if (model.isPayInstallment && model.Installments != null && model.Installments.Count > 0)
                {
                    selectedInstallment = new List<OrderInstallment>();
                    foreach (var inst in model.Installments.Where(c => c.IsSelected))
                    {
                        selectedInstallment.Add(Ioc.InstallmentBusiness.Get(inst.Id));
                    }
                }

                if (!model.PaymentDate.HasValue)
                {
                    model.PaymentDate = DateTime.UtcNow;
                }
                var timezone = club.TimeZone > 0 ? club.TimeZone : Jumbula.Common.Enums.TimeZone.UTC;
                if (model.SelectedChargeMethod == ((int)ChargeMethod.ReceivedPayment).ToString())
                {
                    var paymentDetail = new PaymentDetail(model.Username, PaymentDetailStatus.COMPLETED, "", (PaymentMethod)(Convert.ToInt32(model.SelectedPaymentMethod)), currency, string.Empty, string.Empty, string.Empty, JbUserService.GetCurrentUserRoleType());

                    List<TransactionActivity> transactions = model.IsEdit ? orderItem.TransactionActivities.Where(c => c.PaymentDetailId == model.PaymentDetailId).ToList() : null;
                    result = OrderServices.Get()
                       .ReceivedPayment(model.OrderItemId, model.Amount, paymentDetail, model.Note,
                           DateTimeHelper.ConvertLocalDateTimeToUtc(model.PaymentDate.Value, timezone), model.CheckId,
                           selectedInstallment, transactions, HandleMode.Offline,
                           model.IsEdit);


                    if (result.Status && model.SendEmail)
                    {
                        EmailService.Get(this.ControllerContext).SendReceivedPaymentEmail(_orderItemBusiness.GetItem(model.OrderItemId), model.Amount, model.PaymentDate.Value, paymentDetail, model.IsEdit);
                    }

                }
                else if (model.SelectedChargeMethod == ((int)ChargeMethod.PayNowWithCreditCard).ToString())
                {
                    result = TakeAPaymentNowWithStripe(model, currency, orderItem, selectedInstallment);
                    return Json(new { Status = result.Status, result = "Redirect", url = result.Message, RecordsAffected = 0 });

                }
                else if (model.SelectedChargeMethod == ((int)ChargeMethod.PayWithCredit).ToString())
                {

                    var paymentDetail = new PaymentDetail(model.Username, PaymentDetailStatus.COMPLETED, "", PaymentMethod.Credit, currency, string.Empty, string.Empty, string.Empty, JbUserService.GetCurrentUserRoleType());

                    TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(this.ActiveClub.TimeZone.ToDescription());


                    if (!orderItem.Order.IsLive)
                    {
                        orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId).TestCredit -= model.Amount;
                    }
                    else
                    {
                        orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId).Credit -= model.Amount;
                    }

                    List<TransactionActivity> transactions = model.IsEdit ? orderItem.TransactionActivities.Where(c => c.PaymentDetailId == model.PaymentDetailId).ToList() : null;
                    result = OrderServices.Get()
                        .ReceivedPayment(model.OrderItemId, model.Amount, paymentDetail, model.Note,
                            TimeZoneInfo.ConvertTimeToUtc(model.PaymentDate.Value, localTimeZone), model.CheckId,
                            selectedInstallment, transactions, HandleMode.Offline,
                            model.IsEdit);

                    if (result.Status && model.SendEmail)
                    {
                        EmailService.Get(this.ControllerContext).SendReceivedPaymentEmail(_orderItemBusiness.GetItem(model.OrderItemId), model.Amount, model.PaymentDate.Value, paymentDetail, model.IsEdit);
                    }


                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                result = new OperationStatus() { Status = false, ExceptionMessage = Constants.M_E_CreateEdit };

            }
            return Json(new { Status = result.Status, result = "Refresh", Message = result.ExceptionMessage, RecordsAffected = 0 });
        }

        public virtual JsonNetResult GetRegisterredUsers(string term, string seasonDomain, ParticipantSearchType? searchType = null, string firstName = null, string lastName = null)
        {
            var club = ActiveClub;
            this.EnsureUserIsAdminForClub(club.Domain);

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }

            var pageSize = 10;
            int.TryParse(Request.Params["PageSize"], out pageSize);
            var page = 0;
            int.TryParse(Request.Params["Page"], out page);

            var season = Ioc.SeasonBusiness.Get(seasonDomain, club.Id);

            IQueryable<ClubUser> query = Ioc.ClubBusiness.Get(ActiveClub.Id).Users.AsQueryable();

            if (searchType.HasValue)
            {
                switch (searchType.Value)
                {
                    case ParticipantSearchType.Email:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                query = query.Where(item => item.User.UserName.ToLower() == term);
                            }
                        }
                        break;
                    case ParticipantSearchType.ConfirmationId:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                var orderItemsQuery = _orderItemBusiness.GetOrderItems(ActiveClub.Id, null, null, OrderItemStatusCategories.showAll);
                                var userOrderItem = orderItemsQuery.SingleOrDefault(o => o.Order.ConfirmationId.Equals(term, StringComparison.OrdinalIgnoreCase));
                                if (userOrderItem != null)
                                {
                                    var user = userOrderItem.Order.User;
                                    query = query.Where(item => item.UserId == user.Id);
                                }
                                else
                                {
                                    query = Enumerable.Empty<ClubUser>().AsQueryable();
                                }
                            }
                        }
                        break;
                    case ParticipantSearchType.Name:
                        {
                            if (!string.IsNullOrWhiteSpace(firstName) || !string.IsNullOrWhiteSpace(lastName))
                            {
                                var orderItemsQuery = _orderItemBusiness.GetOrderItems(clubId: ActiveClub.Id, term: null, isTestMode: season.Status == SeasonStatus.Test, itemStatus: OrderItemStatusCategories.showAll, instructorId: null, searchType: searchType, firstName: firstName, lastName: lastName);

                                var orderItemResult = Enumerable.Empty<OrderItem>();

                                orderItemResult = orderItemsQuery.Where(item => (string.IsNullOrEmpty(firstName) || item.FirstName.Contains(firstName)) && (string.IsNullOrEmpty(lastName) || item.LastName.Contains(lastName)));

                                var userIds = orderItemResult.Select(o => o.Order.UserId).ToList();

                                query = query.Where(u => userIds.Contains(u.UserId));
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            if (query.Any())
            {
                var groupbyQuery = query.GroupBy(u => u.UserId);
                var total = groupbyQuery.Count();
                groupbyQuery = groupbyQuery.OrderBy(c => c.Key).Skip((page - 1) * pageSize).Take(pageSize > 0 ? pageSize : 10);
                var model = new List<UserViewModel>();
                foreach (var item in groupbyQuery.ToList())
                {
                    var userPlayers = _playerProfileBusiness.GetList(item.Key).Where(p => p.Relationship != RelationshipType.Parent);
                    var clubUserPlayers = userPlayers.Where(p => p.OrderItems.Any(i => (i.ItemStatus == OrderItemStatusCategories.completed || i.ItemStatusReason == OrderItemStatusReasons.canceled || i.ItemStatusReason == OrderItemStatusReasons.transferOut) && i.Order.ClubId == club.Id && i.Order.IsLive));

                    model.Add(new UserViewModel()
                    {
                        Id = item.Key,
                        Players = clubUserPlayers,
                        UserOrdersItems = _orderItemBusiness.GetClubUserOrderItems(ActiveClub.Id, item.Key, season.Status == SeasonStatus.Test).Where(c => c.ItemStatus == OrderItemStatusCategories.completed || (c.ItemStatus == OrderItemStatusCategories.changed && c.ItemStatusReason == OrderItemStatusReasons.canceled)).ToList(),
                        UserName = item.ToList().FirstOrDefault().User.UserName
                    });
                }

                return JsonNet(new { Data = model, Total = total });
            }

            return JsonNet(new { Data = new List<UserViewModel>(), Total = 0 });
        }

        public virtual ActionResult RemoveTransaction(long orderItemId, long transactionId)
        {
            var result = OrderServices.Get().DeleteTransaction(orderItemId, transactionId);
            return JsonNet(result);
        }

        public virtual RedirectResult MakeaPaymentCancelUrl(string token)
        {
            var transactions = Ioc.TransactionActivityBusiness.GetList().First(c => c.Token == token);
            var orderItem = transactions.OrderItem;
            var urlToRedirect = string.Format("{0}/Dashboard#/Season/{1}/Program/{2}/OrderItem/{3}/MakePayment/{4}", Request.Url.GetLeftPart(UriPartial.Authority), orderItem.Season.Domain, orderItem.ProgramSchedule.ProgramId, orderItem.Id, token);
            return Redirect(urlToRedirect);
        }

        public virtual JsonNetResult GetOrderHistory(long orderId, long? orderItemId = null)
        {
            var model = new List<OrderHistoriesViewModel>();
            try
            {
                IQueryable<OrderHistory> orderHistories = null;
                if (!orderItemId.HasValue)
                {
                    orderHistories = Ioc.OrderHistoryBusiness.GetAllHistoryByOrderId(orderId);
                }
                else
                {
                    orderHistories = Ioc.OrderHistoryBusiness.GetAllHistoryByOrderItemId(orderId, orderItemId.Value);
                }
                if (orderHistories.Any())
                {
                    model.AddRange(orderHistories.ToList().Select(h => new OrderHistoriesViewModel
                    {
                        Email = Ioc.UserProfileBusiness.Get(h.UserId).UserName,
                        Action = h.Action.ToDescription(),
                        Description = h.Description,
                        ActionDate = DateTimeHelper.ConvertUtcDateTimeToLocal(h.ActionDate, ActiveClub.TimeZone).ToString(Constants.DefaultDateTimeFormat)
                    }));
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }

            return JsonNet(new { DataSource = model, TotalCount = model.Count() });
        }

        public virtual JsonNetResult SubmitRefundOrderItem(OrderItemsViewModel orderViewModel)
        {
            try
            {
                orderViewModel.CurrentRoleType = this.GetCurrentUserRoleType();
                orderViewModel.CurrentUserName = this.GetCurrentUserName();
                orderViewModel.CurrentUserId = this.GetCurrentUserId();

                var orderItem = _orderItemBusiness.GetItem(orderViewModel.Id);
                CheckResourceForAccess(orderItem);

                var resultRefund = _orderBusiness.Refund(orderViewModel);

                if (!resultRefund.Status && string.IsNullOrWhiteSpace(resultRefund.ErrorMessage))
                {
                    resultRefund.ErrorMessage = FailProcessRefund;
                }

                var result = new OperationStatus { Status = resultRefund.Status, Message = resultRefund.ErrorMessage };
                return JsonNet(result);
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                Log.Error(ex, "Error in refund {OrderItemId}", orderViewModel.Id);
                var result = new OperationStatus { Status = false, Message = FailProcessRefund, RecordsAffected = 0 };
                return JsonNet(result);
            }
        }

        [HttpPost]
        public virtual JsonResult StopInatallmentsPayment(int orderItemId)
        {
            var result = _orderItemBusiness.StopInatallmentsPayment(orderItemId);

            return Json(result);
        }

        [HttpPost]
        public virtual JsonResult ResumeInatallmentsPayment(int orderItemId)
        {
            var result = _orderItemBusiness.ResumeInatallmentsPayment(orderItemId);

            return Json(result);
        }

        private OperationStatus TakeAPaymentNowWithStripe(MakePaymentViewModel model, CurrencyCodes currency, OrderItem orderItem, List<OrderInstallment> selectedInstallment)
        {
            try
            {
                var orderDb = Ioc.OrderBusiness;

                var token = Guid.NewGuid().ToString("N");
                var paymentDetail = new PaymentDetail(model.Username, PaymentDetailStatus.PROCESSING, "", PaymentMethod.Card, currency, string.Empty, string.Empty, string.Empty, RoleCategoryType.Dashboard);
                if (selectedInstallment != null && selectedInstallment.Count > 0)
                {
                    var totalPaidAmount = model.Amount;
                    foreach (var inst in selectedInstallment)
                    {
                        if (totalPaidAmount <= 0)
                        {
                            break;
                        }
                        var instBalance = inst.Amount - (inst.PaidAmount.HasValue ? inst.PaidAmount.Value : 0);
                        if (instBalance > totalPaidAmount)
                        {
                            instBalance = totalPaidAmount;
                        }
                        totalPaidAmount -= instBalance;

                        orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Draft, null, model.PaymentDate, instBalance, orderItem, inst.Id, model.Note, token, model.CheckId, TransactionCategory.TakePayment);
                    }
                }
                else
                {
                    orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Draft, null, null, model.Amount, orderItem, null, model.Note, token, model.CheckId, TransactionCategory.TakePayment);
                }
                _orderItemBusiness.Update(orderItem);
                var urlToRedirect = string.Format("{0}/Cart?actionCaller={1}&token={2}&backToDetail={3}", Request.Url.GetLeftPart(UriPartial.Authority), JumbulaSubSystem.MakeaPayment, token, false);

                return new OperationStatus() { Status = true, Message = urlToRedirect };
            }
            catch
            {
                throw;
            }

        }

        public virtual ActionResult ValidateRefundOrderItem(OrderItemsViewModel model)
        {
            decimal balance = Ioc.AccountingBusiness.OrderItemBalance(_orderItemBusiness.GetItem(model.Id));
            var errorMessage = ValidateRefundStep1(model, balance);
            if (!string.IsNullOrEmpty(errorMessage))
            {
                ModelState.AddModelError("model.CustomRefund.Amount", errorMessage);
            }
            if (ModelState.IsValid)
            {
                return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });
            }
            return JsonFormResponse();
        }

        private string ValidateRefundStep1(OrderItemsViewModel model, decimal balance)
        {
            if (balance >= 0)
            {
                return string.Format(Constants.F_InvalidAmountForRefund, CurrencyHelper.FormatCurrencyWithPenny(0, ActiveClub.Currency));
            }
            if (model.CustomRefund.Amount > Math.Abs(balance))
            {
                return string.Format(Constants.F_InvalidAmountForRefund, CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(balance), ActiveClub.Currency));
            }
            return string.Empty;
        }

        public virtual ActionResult ValidateMakePayment(MakePaymentViewModel model)
        {
            var orderItem = _orderItemBusiness.GetItem(model.OrderItemId);

            decimal balance = 0;

            if (model.isPayInstallment && model.Installments != null && model.Installments.Count > 0)
            {
                if (!model.Installments.Any(c => c.IsSelected))
                {
                    ModelState.AddModelError("model.selectedInstallment", Constants.E_SelectedInstallment);
                }
                else
                {
                    balance = model.Installments.Where(c => c.IsSelected).Sum(c => c.OldBalance);


                    if (model.Amount <= 0 || model.Amount > balance)
                    {
                        ModelState.Remove("model.Amount");
                        ModelState.AddModelError("model.Amount", string.Format(Constants.E_Amount_Between, CurrencyHelper.FormatCurrencyWithPenny(1, orderItem.Order.Club.Currency), CurrencyHelper.FormatCurrencyWithPenny(balance, orderItem.Order.Club.Currency)));
                    }
                }
            }
            else
            {
                if (model.IsEdit)
                {
                    var transactions = Ioc.TransactionActivityBusiness.GetAllTransactionByOrderItemId(model.OrderItemId).Where(c => c.PaymentDetailId == model.PaymentDetailId);
                    if (transactions.Any())
                    {
                        balance = transactions.Sum(c => c.Amount);
                    }

                }
                balance += model.Balance;
                if (model.Amount <= 0 || model.Amount > balance)
                {
                    ModelState.Remove("model.Amount");
                    ModelState.AddModelError("model.Amount", string.Format(Constants.E_Amount_Between, CurrencyHelper.FormatCurrencyWithPenny(1, orderItem.Order.Club.Currency), CurrencyHelper.FormatCurrencyWithPenny(balance, orderItem.Order.Club.Currency)));
                }
            }
            if (model.SelectedChargeMethod == ((int)ChargeMethod.ReceivedPayment).ToString())
            {
                if (model.SelectedPaymentMethod == "0")
                {
                    ModelState.AddModelError("model.SelectedPaymentMethod", "Please select the payment method");

                }
                if (!model.PaymentDate.HasValue)
                {
                    ModelState.AddModelError("model.PaymentDate", "Payment date is required.");
                }
            }
            if (model.SelectedChargeMethod == ((int)ChargeMethod.PayWithCredit).ToString())
            {
                var maxamount = (balance >= model.AvailableCredit ? model.AvailableCredit : balance);
                if (model.Amount <= 0 || model.Amount > maxamount)
                {
                    ModelState.Remove("model.Amount");
                    ModelState.AddModelError("model.Amount", string.Format(Constants.E_Amount_Between, CurrencyHelper.FormatCurrencyWithPenny(1, orderItem.Order.Club.Currency), CurrencyHelper.FormatCurrencyWithPenny(maxamount, orderItem.Order.Club.Currency)));
                }
            }

            if (!ModelState.IsValid)
            {
                return JsonFormResponse();
            }

            return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });

        }

        public virtual ActionResult ValidateInstallments(EditOrderViewModel model)
        {
            if (model.OrderInstallments != null)
            {
                for (var i = 0; i < model.OrderInstallments.Count; i++)
                {
                    if (model.OrderInstallments[i].Amount < model.OrderInstallments[i].PaidAmount)
                    {
                        ModelState.AddModelError("model.OrderInstallments[" + i + "].Amount", "The installment amount cannot be less than the amount already paid.");
                    }
                    else
                    {
                        ModelState.Remove("model.OrderInstallments[" + i + "].Amount");
                    }
                }
            }

            if (!ModelState.IsValid)
            {
                return JsonFormResponse();
            }

            return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });

        }

        public virtual ActionResult ValidateTransferInstallments(TransferOrderViewModel model)
        {
            if (model.OrderInstallments != null)
            {
                for (var i = 0; i < model.OrderInstallments.Count; i++)
                {
                    if (model.OrderInstallments[i].Amount < model.OrderInstallments[i].PaidAmount)
                    {
                        ModelState.AddModelError("model.OrderInstallments[" + i + "].Amount", "The installment amount cannot be less than the amount already paid.");
                    }
                    else
                    {
                        ModelState.Remove("model.OrderInstallments[" + i + "].Amount");
                    }
                }
            }

            if (!ModelState.IsValid)
            {
                return JsonFormResponse();
            }

            return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });

        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private OrderItemsViewModel GetOrderItemViewModel(long orderItemId, long? installmentId, int changeFormId, bool editable = false, bool updateForm = false)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var club = orderItem.Order.Club;
            var orderUserId = orderItem.Order.UserId;
            var outsourceClubDomain = string.Empty;

            if (changeFormId > 0)
            {
                updateForm = true;
            }

            if (orderItem != null && orderItem.ProgramScheduleId.HasValue && orderItem.ProgramSchedule.Program.OutSourceSeasonId.HasValue)
            {
                outsourceClubDomain = orderItem.ProgramSchedule.Program.OutSourceSeason.Club.Domain;
            }
            if (orderItem != null && Ioc.OrderBusiness.IsUserOwnsTheOrder(this.GetCurrentUserId(), orderUserId, orderItem.Order.Club.Domain, outsourceClubDomain))
            {
                if (!updateForm && !editable && orderItem.ProgramScheduleId.HasValue)
                {
                    orderItem.JbForm.CurrentMode = AccessMode.ReadOnly;
                    orderItem.JbForm.SetElementsCurrentMode(AccessMode.ReadOnly);
                }

                var followupForms = (orderItem.ItemStatusReason != OrderItemStatusReasons.transferIn && orderItem.ItemStatusReason != OrderItemStatusReasons.transferOut && !orderItem.IsMultiRegister) ?
                        Ioc.FollowupBusiness.GetOrderItemFollowupForms(orderItem.ISTS, orderItem.ItemStatusReason).Select(m => new FollowUpFormViewModel() { FormName = m.FormName, Id = m.Id, Status = m.Status, FormType = m.FormType, FollowUpFormMode = m.FollowUpFormMode }).ToList() :
                        Ioc.FollowupBusiness.GetOrderItemFollowupForms(orderItem.ISTS, orderItem.ItemStatusReason, orderItemId).Select(m => new FollowUpFormViewModel() { FormName = m.FormName, Id = m.Id, Status = m.Status, FormType = m.FormType, FollowUpFormMode = m.FollowUpFormMode }).ToList();


                var waivers = orderItem.OrderItemWaivers.ToList();

                object gender = null;

                if (orderItem.ProgramScheduleId.HasValue && orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
                {
                    gender = orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.Gender.ToString()).GetValue();
                }

                int Age = 0;
                if (orderItem.ProgramScheduleId.HasValue && !_programBusiness.IsTeamChessTourney(orderItem.ProgramSchedule.Program) && orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
                {
                    var dobDate = DateTime.UtcNow;
                    if (DateTime.TryParse(orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.DoB.ToString()).GetValue().ToString(), out dobDate))
                    {
                        Age = DateTimeHelper.CalculateAge(dobDate);
                    }
                }

                var Grade = orderItem.JbForm != null && orderItem.JbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) ?
                    orderItem.JbForm.GetElement<JbSection>(SectionsName.SchoolSection.ToString()).Elements.Any(e => e.Name == "Grade") ?
                    orderItem.JbForm.GetElement<JbSection>(SectionsName.SchoolSection.ToString()).Elements.SingleOrDefault(e => e.Name == "Grade").GetValue() : "" : "";

                if (editable && orderItem.ProgramScheduleId.HasValue)
                {
                    foreach (var section in orderItem.JbForm.Elements)
                    {
                        section.CurrentMode = AccessMode.Edit;
                        foreach (var element in (section as JbSection).Elements)
                        {
                            element.CurrentMode = AccessMode.Edit;
                        }
                    }
                }

                var orderId = orderItem.Order_Id;
                var isLiveOrder = Ioc.OrderBusiness.Get(orderId).IsLive;

                var model = orderItem.ToViewModel<OrderItemsViewModel>();
                model.Waivers = new List<OrderitemWaiversViewModel>();
                model.Currency = club.Currency;
                model.EntryFee = orderItem.OrderChargeDiscounts.Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge && c.Category == ChargeDiscountCategory.EntryFee).Sum(c => c.Amount);
                model.HasProgramScheduleId = orderItem.ProgramScheduleId.HasValue ? true : false;
                model.UpdateForm = updateForm;
                model.ItemStatusReason = orderItem.ItemStatusReason;
                var chargeEntryFee = orderItem.OrderChargeDiscounts.SingleOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee);
                model.IsDeletedEntryFee = chargeEntryFee != null ? chargeEntryFee.IsDeleted : false;
                model.Forms = club.ClubFormTemplates.Where(f => !f.IsDeleted && f.FormType == FormType.Registration).Select(f =>
               new SelectListItem
               {
                   Text = f.Title,
                   Value = f.Id.ToString()
               }).ToList();

                var isTestMode = !orderItem.Order.IsLive;
                var allPlayerOrders = _orderItemBusiness.GetList().Where(p => p.ProgramScheduleId.HasValue && p.PlayerId == orderItem.PlayerId && p.ItemStatus == OrderItemStatusCategories.completed && p.Id != orderItem.Id && p.SeasonId == orderItem.SeasonId && p.Order.IsLive != isTestMode);

                var selectedForm = new ClubFormTemplate();
                model.IsSelectedForm = true;

                if (changeFormId > 0)
                {
                    selectedForm = club.ClubFormTemplates.FirstOrDefault(f => f.FormType == FormType.Registration && !f.IsDeleted && f.Id == changeFormId);

                    model.SelectedForm = selectedForm != null ? selectedForm.Id.ToString() : null;
                    model.UpdateForm = true;
                    model.IsSelectedForm = true;
                }
                else
                {
                    model.SelectedForm = "";
                    model.IsSelectedForm = false;
                    selectedForm = null;
                }

                if (updateForm && orderItem.JbForm != null && orderItem.JbForm.Elements.Any() && selectedForm != null)
                {
                    selectedForm.JbForm.DataBinding(orderItem.JbForm);
                    selectedForm.JbForm.Id = orderItem.JbForm.Id;
                }

                if (updateForm && orderItem.ProgramScheduleId.HasValue && selectedForm != null)
                {
                    foreach (var section in selectedForm.JbForm.Elements)
                    {
                        section.CurrentMode = AccessMode.Edit;
                        foreach (var element in (section as JbSection).Elements)
                        {
                            element.CurrentMode = AccessMode.Edit;
                        }
                    }
                }

                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.ChessTournament)
                    SetChessTournamentAdditionalInformation(orderItem, model);

                var desiredStartDate = string.Empty;
                var effectiveTransferredDate = string.Empty;
                var effectiveCanceledDate = string.Empty;

                if (orderItem.ProgramScheduleId.HasValue)
                {
                    if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription || orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        model.Name = orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare ?
                            _programBusiness.GetProgramNameWithScheduleTitleAndDate(orderItem, orderItem.EntryFeeName) :
                            _programBusiness.GetProgramName(orderItem.ProgramSchedule.Program.Name, orderItem.Start.Value, orderItem.End.Value, orderItem.EntryFeeName);

                        if (orderItem.Mode != OrderItemMode.DropIn)
                        {
                            desiredStartDate = orderItem.DesiredStartDate.HasValue ? orderItem.DesiredStartDate.Value.ToString("MMM dd, yyyy") : null;
                            effectiveTransferredDate = orderItem.Attributes.TransferEffectiveDate.HasValue ? orderItem.Attributes.TransferEffectiveDate.Value.ToString("MMM dd, yyyy") : null;
                            effectiveCanceledDate = orderItem.Attributes.CancelEffectiveDate.HasValue ? orderItem.Attributes.CancelEffectiveDate.Value.ToString("MMM dd, yyyy") : null;

                            var subscriptionAttribute = orderItem.Attributes.WeekDays;
                            if (subscriptionAttribute != null)
                            {
                                model.Days = subscriptionAttribute;
                            }

                            model.IsItemCanceled = orderItem.ItemStatusReason == OrderItemStatusReasons.canceled && !orderItem.OrderSessions.Any() ? true : false;
                        }
                        else
                        {
                            model.IsDropIn = orderItem.Mode == OrderItemMode.DropIn;

                            model.DropInSessions = orderItem.OrderSessions.OrderBy(c => c.ProgramSession.StartDateTime).Select(s =>
                                   s.ProgramSession.StartDateTime.ToString("MMM dd") + " " + s.ProgramSession.StartDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + "-" + s.ProgramSession.EndDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", ""))
                              .ToList();
                        }
                    }
                    else
                    {
                        if (orderItem.Mode == OrderItemMode.DropIn)
                        {
                            model.IsDropIn = orderItem.Mode == OrderItemMode.DropIn;
                            model.DropInSessions = _orderItemBusiness.ShowItemSessions(orderItem);
                        }

                        model.Name = orderItem.ProgramTypeCategory == ProgramTypeCategory.Camp ?
                            _programBusiness.GetProgramNameWithScheduleTitleAndDate(orderItem, orderItem.EntryFeeName, "") :
                             orderItem.ProgramTypeCategory == ProgramTypeCategory.ChessTournament ?
                            _programBusiness.GetProgramNameWithScheduleTitleAndDate(orderItem, orderItem.EntryFeeName, orderItem.OrderItemChess.Schedule)
                            : _programBusiness.GetProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName);
                    }

                    if (allPlayerOrders.Any())
                    {
                        model.AllOrdersThisPlayer = allPlayerOrders.Select(i => new SelectListItem
                        {
                            Text = i.Order.ConfirmationId + ": " + i.Name,
                            Value = i.Id.ToString()
                        }).ToList();
                    }

                    model.ProgramId = orderItem.ProgramSchedule.ProgramId;
                    model.JbFormHtml = RenderPartialViewToString("EditorTemplates/JbForm", orderItem.JbForm);

                    if (selectedForm != null)
                    {
                        //selectedForm.JbForm = new JbForm();
                        model.ProgramJbFormHtml = RenderPartialViewToString("EditorTemplates/JbForm", selectedForm.JbForm);

                    }
                    model.ProgramType = orderItem.ProgramTypeCategory;
                    model.JbFormId = orderItem.JbFormId;
                }
                else
                {
                    model.Name = orderItem.FirstName;
                }

                model.OrderInfo = new OrderInformationViewModel()
                {
                    ConfirmationId = orderItem.Order.ConfirmationId,
                    Gender = gender,
                    Age = Age,
                    Name = orderItem.FirstName,
                    FullName = orderItem.FullName,
                    DesiredStartDate = desiredStartDate,
                    EffectiveTransferredDate = effectiveTransferredDate,
                    EffectiveCanceledDate = effectiveCanceledDate,
                    HasProgramScheduleId = orderItem.ProgramScheduleId.HasValue ? true : false,
                    ItemStatusReson = orderItem.ItemStatusReason,
                    IsDropIn = orderItem.Mode == OrderItemMode.DropIn,
                    OrderAmount = orderItem.Order.OrderAmount,
                    ProgramType = orderItem.ProgramTypeCategory,
                    Days = model.Days,
                    Currency = model.Currency,
                    Balance = Ioc.AccountingBusiness.OrderItemBalance(orderItem),
                    TotalAmount = orderItem.TotalAmount,
                    PaidAmount = orderItem.PaidAmount,
                    //OrderItems = orderItem.Order.RegularOrderItems.Select(item => new OrderItemsViewModel(orderItem.Order.Club.Currency)).ToList()
                };

                model.HasProgramScheduleId = orderItem.ProgramScheduleId.HasValue ? true : false;
                model.OrderId = orderItem.Order_Id;
                model.ConfirmationId = orderItem.Order.ConfirmationId;
                model.HasTransactions = (orderItem.TransactionActivities != null && orderItem.TransactionActivities.Count > 0 && orderItem.TransactionActivities.Where(t => t.Amount != 0)
                    .Any(c => c.TransactionStatus == TransactionStatus.Success || c.TransactionStatus == TransactionStatus.Invoice ||
                    c.TransactionStatus == TransactionStatus.Pending ||
                            (c.TransactionStatus == TransactionStatus.Failure && c.TransactionDate > c.Order.CompleteDate)));

                model.HasInstallments = (orderItem.Installments != null && orderItem.Installments.Count > 0
                       && orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml
                      && i.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success && (t.TransactionStatus == TransactionStatus.Failure && t.TransactionDate > orderItem.Order.CompleteDate)) || !i.IsDeleted ||
                       (i.IsDeleted && i.PaidAmount > 0)).Any(c => c.Status == OrderStatusCategories.completed || c.Status == OrderStatusCategories.initiated || c.Status == OrderStatusCategories.error || c.Status == OrderStatusCategories.canceled));

                model.HasAddOnInstallments = (orderItem.Installments != null && orderItem.Installments.Count > 0 && orderItem.Installments.Where(i => i.Type == InstallmentType.AddOn).Where(i => i.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success && (t.TransactionStatus == TransactionStatus.Failure && t.TransactionDate > orderItem.Order.CompleteDate)) || !i.IsDeleted || (i.IsDeleted && i.PaidAmount > 0)).Any(c => c.Status == OrderStatusCategories.completed || c.Status == OrderStatusCategories.initiated || c.Status == OrderStatusCategories.error || c.Status == OrderStatusCategories.canceled));
                model.Gender = gender;
                model.Age = Age;
                model.Grade = Grade;
                model.FollowupForms = followupForms;
                model.UserId = orderItem.Order.UserId;
                model.Balance = Ioc.AccountingBusiness.OrderItemBalance(orderItem);
                model.PaymentPlanStatus = orderItem.PaymentPlanStatus;
                model.IsLiveOrder = isLiveOrder;
                model.IsInstallmentAutoCharge = orderItem.Installments != null && orderItem.Installments.Any(i => !i.EnableReminder) && orderItem.Order.IsAutoCharge;
                model.PaymentPlanType = orderItem.PaymentPlanType;
                model.IsPartner = Ioc.ClubBusiness.Get(orderItem.Order.Club.Id).IsPartner ? true : false;

                model.RefundModes = _orderInstallmentBusiness.GetRefundModes();
                model.AmountAutomaticallyTransactions = _orderItemBusiness.GetAmountAutomaticallyTransactions(orderItem);

                if (model.HasInstallments)
                {
                    model.Installments = _orderInstallmentBusiness.GetOrderItemInstallments(orderItemId, null, 0, int.MaxValue, this.GetCurrentUserRoleType(), InstallmentType.Noraml);
                    model.AddOnInstallments = _orderInstallmentBusiness.GetOrderItemInstallments(orderItemId, null, 0, int.MaxValue, this.GetCurrentUserRoleType(), InstallmentType.AddOn);

                    model.EnableInstallmentRefund = model.Installments.Any(i => i.RefundAmount > 0) || model.AddOnInstallments.Any(i => i.RefundAmount > 0);
                    model.EnableInstallmentTakePayment = model.Installments.Any(i => i.TakePaymentAmount > 0) || model.AddOnInstallments.Any(i => i.TakePaymentAmount > 0);

                    var installments = new List<ParentOrderItemInstallmentsViewModel>();
                    installments.AddRange(model.Installments);
                    installments.AddRange(model.AddOnInstallments);

                    if (installmentId.HasValue)
                    {
                        var installment = installments.Single(i => i.Id == installmentId);
                        var tempInstallments = new List<ParentOrderItemInstallmentsViewModel> { installment };

                        if (installment.Type == InstallmentType.AddOn)
                        {
                            model.AddOnInstallments = tempInstallments;
                            model.HasInstallments = false;
                        }
                        else { model.Installments = tempInstallments; model.HasAddOnInstallments = false; }            
                    }
                }

                if (waivers.Count > 0)
                {
                    var clubBusiness = Ioc.ClubBusiness;
                    foreach (var waiver in waivers)
                    {
                        model.Waivers.Add(new OrderitemWaiversViewModel()
                        {
                            Name = waiver.Name,
                            Text = waiver.Text,
                            Signature = waiver.Signature != null ? waiver.Signature : "",
                            Agreed = waiver.Agreed == true ? waiver.Agreed : false,
                            Id = waiver.Id,
                            IsRequired = waiver.IsRequired,
                            WaiverConfirmationType = waiver.WaiverConfirmationType.ToString()
                        });
                    }
                }
                return model;
            }
            return new OrderItemsViewModel();
        }
        [HttpGet]
        public virtual JsonNetResult GetWaiverView(int waiverId)
        {
            var waiver = Ioc.OrderItemWaiverBusiness.Get(waiverId);

            var model = new OrderitemWaiversViewModel();

            model.Agreed = waiver.Agreed;
            model.Name = waiver.Name;
            model.Text = waiver.Text;
            model.Signature = waiver.Signature;
            model.ProgramName = waiver.OrderItem.ProgramSchedule.Program.Name;
            model.CreateDate = DateTimeHelper.ConvertUtcDateTimeToLocal(waiver.OrderItem.Order.CompleteDate, waiver.OrderItem.ProgramSchedule.Program.Club.TimeZone);
            model.IsRequired = waiver.IsRequired;
            model.WaiverConfirmationType = waiver.WaiverConfirmationType.ToString();

            return JsonNet(model);
        }
        [HttpGet]
        public virtual JsonNetResult EditSubscriptionProgramDays(long orderItemId)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            var model = new ChangeDaysViewModel();
            model.OrderItemId = orderItemId;
            model.DesiredStartDate = orderItem.DesiredStartDate.HasValue ? orderItem.DesiredStartDate.Value.ToString("MMM dd, yyyy") : null;

            var subscriptionAttribute = orderItem.ProgramSchedule.Attributes as ScheduleSubscriptionAttribute;
            var beforeAfterCareAttribute = orderItem.ProgramSchedule.Attributes as ScheduleAfterBeforeCareAttribute;

            if (subscriptionAttribute != null && orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
            {
                model.AllProgramDays = subscriptionAttribute.Days.Select(s =>
                        new SubscriptionProgramDaysViewModel
                        {
                            Title = s.DayOfWeek.ToDescription(),
                            Day = s.DayOfWeek
                        })
                          .ToList();
            }
            else if (beforeAfterCareAttribute != null)
            {
                model.AllProgramDays = beforeAfterCareAttribute.Days.Select(s =>
                       new SubscriptionProgramDaysViewModel
                       {
                           Title = s.DayOfWeek.ToDescription(),
                           Day = s.DayOfWeek
                       })
                         .ToList();
            }

            model.Days = orderItem.Attributes.WeekDays != null ? orderItem.Attributes.WeekDays : null;

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult EditSubscriptionProgramDays(ChangeDaysViewModel model)
        {
            var orderItem = _orderItemBusiness.GetItem(model.OrderItemId);
            var result = new OperationStatus();
            var today = DateTime.UtcNow;
            var lastSessiondate = orderItem.OrderSessions.OrderBy(s => s.ProgramSession.StartDateTime).ToList().LastOrDefault().ProgramSession.StartDateTime;

            foreach (var item in model.AllProgramDays)
            {
                if (item.IsChecked)
                {
                    model.NewDays.Add(item.Day);
                }
            }

            if (model.Days.Count != model.NewDays.Count)
            {
                return Json(new JResult { Status = false, Message = "You must choose exactly " + model.Days.Count + " days." });
            }
            if (!model.EffectiveDate.HasValue)
            {
                ModelState.AddModelError("model.EffectiveDate", "Effective date is required.");
            }
            if (model.EffectiveDate.HasValue && model.EffectiveDate.Value < today)
            {
                ModelState.AddModelError("model.EffectiveDate", "Effective date must start from tomorrow.");
            }
            if (model.EffectiveDate.HasValue && model.EffectiveDate.Value > lastSessiondate)
            {
                ModelState.AddModelError("model.EffectiveDate", "Effective date must be before the program end date.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var isProgramOrderCombo = orderItem.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both ? true : false;
                    var historyDescription = string.Empty;
                    historyDescription += string.Format("{0}", "You chenged class days.");

                    orderItem.Order.OrderHistories.Add(
                       new OrderHistory
                       {
                           Action = OrderAction.ChangeDay,
                           ActionDate = DateTime.UtcNow,
                           Description = historyDescription,
                           UserId = this.GetCurrentUserId(),
                           OrderItemId = orderItem.Id
                       });

                    orderItem.Attributes = new OrderItemAttributes() { WeekDays = model.NewDays, TransferEffectiveDate = orderItem.Attributes.TransferEffectiveDate, CancelEffectiveDate = orderItem.Attributes.CancelEffectiveDate, ChangeDaysEffectiveDate = model.EffectiveDate };

                    result = _orderSessionBusiness.CreateSession(orderItem, model.NewDays, model.EffectiveDate, isProgramOrderCombo);

                    return Json(new JResult { Status = result.Status, Message = result.Message });
                }
                catch (Exception)
                {
                    return Json(new JResult { Status = false, Message = result.Message, });
                }
            }

            return JsonFormResponse();

        }

        [HttpGet]
        public virtual JsonNetResult EditSubscriptionDesiredStartDate(long orderItemId)
        {
            var model = new ChangeDesiredStartDateViewModel();
            var orderItem = _orderItemBusiness.GetItem(orderItemId);

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription && orderItem.Installments != null && orderItem.Installments.Any(i => !i.IsDeleted && i.Type != InstallmentType.AddOn && i.ProgramSchedulePartId == null))
            {
                return JsonNet(new { Status = false });
            }

            var lastSessiondate = orderItem.OrderSessions.OrderBy(s => s.ProgramSession.StartDateTime).ToList().LastOrDefault().ProgramSession.StartDateTime;
            var lastProgramSession = new ProgramSession();

            lastProgramSession = orderItem.ProgramSchedule.ScheduleMode != TimeOfClassFormation.Both ? orderItem.ProgramSchedule.ProgramSessions.ToList().OrderBy(s => s.StartDateTime).LastOrDefault(p => !p.IsDeleted)
                : orderItem.ProgramSchedule.Program.ProgramSchedules.FirstOrDefault(s => s.ScheduleMode != TimeOfClassFormation.Both).ProgramSessions.ToList().OrderBy(p => p.StartDateTime).LastOrDefault(p => !p.IsDeleted);

            model.OrderItemId = orderItem.Id;

            model.OldDesiredStartDate = orderItem.DesiredStartDate.HasValue ? orderItem.DesiredStartDate.Value.ToString("MMM dd, yyyy") : null;
            model.Days = orderItem.Attributes.WeekDays != null ? orderItem.Attributes.WeekDays : null;
            model.LastSessiondate = lastSessiondate;
            model.LastProgramSessiondate = lastProgramSession.StartDateTime;

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult EditSubscriptionDesiredStartDate(ChangeDesiredStartDateViewModel model)
        {
            var orderItem = _orderItemBusiness.GetItem(model.OrderItemId);
            var result = new OperationStatus();
            var club = orderItem.Order.Club;

            ValidationCheck(ModelState, model);

            if (ModelState.IsValid)
            {
                try
                {
                    var orderItemScheduleParts = new List<OrderItemSchedulePartModel>();

                    if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        foreach (var item in orderItem.OrderItemScheduleParts.Where(i => !i.IsDeleted))
                        {
                            orderItemScheduleParts.Add(new OrderItemSchedulePartModel
                            {
                                Amount = item.PartAmount,
                                PartId = item.ProgramPartId.Value
                            });
                        }
                    }

                    var newScheduleForNewDesiredStartDate = new ProgramSchedulePart();
                    var insallmentsItem = new List<OrderInstallment>();
                    DateTime date = DateTime.UtcNow;
                    var scheduleMonths = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

                    var scheduleOldDesiredDate = _subscriptionBusiness.GetMonthDesiredStartDate(orderItem.ProgramSchedule.ProgramId, orderItem.DesiredStartDate.Value);
                    var orderItemInstallmentCount = orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted).ToList().Count;
                    var entryfee = orderItem.EntryFee / orderItemInstallmentCount;

                    //If the new desired date is in the first month
                    var firstMonthEndDate = scheduleOldDesiredDate.EndDate;
                    var firstMonthStartDate = scheduleOldDesiredDate.StartDate;

                    var selectedCharge = _subscriptionBusiness.GetSelectedCharge(orderItem);
                    var charges = _programBusiness.GetProgramPartCharges(selectedCharge.Id);

                    decimal differenceAmount = 0;
                    decimal paidInstallmentAmount = 0;
                    var listPaidDeletedInstallmenst = new List<OrderInstallment>();
                    var oldInstallmentByDesiredStartDate = new OrderInstallment();

                    if (model.newDesiredStartDate <= firstMonthEndDate && model.newDesiredStartDate >= firstMonthStartDate)
                    {
                        orderItem.DesiredStartDate = model.newDesiredStartDate;

                        if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                        {
                            decimal newAmount = 0;
                            oldInstallmentByDesiredStartDate = orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted && i.ProgramSchedulePartId == scheduleOldDesiredDate.Id).FirstOrDefault();

                            var amountBeforeUpdate = oldInstallmentByDesiredStartDate.Amount;
                            var selectedDays = orderItem.Attributes.WeekDays;
                            newAmount = _orderItemBusiness.GetProgramSchedulePartAmount(scheduleOldDesiredDate, orderItem.ProgramSchedule, selectedCharge, selectedDays, model.newDesiredStartDate);

                            _orderItemBusiness.updateInstallmentAmount(orderItem, scheduleOldDesiredDate, orderItemScheduleParts, newAmount);
                            orderItem.OrderItemScheduleParts.FirstOrDefault(o => !o.IsDeleted && o.ProgramPartId == scheduleOldDesiredDate.Id).PartAmount = newAmount;

                            var oldInstallment = orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted && i.ProgramSchedulePartId == scheduleOldDesiredDate.Id).FirstOrDefault();
                            var amountAfterUpdate = oldInstallment.Amount;

                            if (oldInstallmentByDesiredStartDate.PaidAmount.HasValue && oldInstallmentByDesiredStartDate.PaidAmount > 0 && oldInstallment.PaidAmount > amountAfterUpdate)
                            {

                                differenceAmount = oldInstallment.PaidAmount.Value - amountAfterUpdate;

                                if (orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId) == null)
                                {
                                    Ioc.ClubBusiness.AddUserInClubUsers(orderItem.Order.UserId, orderItem.Order.ClubId);
                                }

                                if (orderItem.Order.IsLive)
                                {
                                    orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId).Credit += differenceAmount;
                                }
                                else
                                {
                                    orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId).TestCredit += differenceAmount;
                                }

                                orderItem.PaidAmount -= differenceAmount;
                                oldInstallment.PaidAmount -= differenceAmount;
                            }
                        }
                    }
                    // we should add new installment
                    else if (model.newDesiredStartDate <= firstMonthStartDate)
                    {
                        newScheduleForNewDesiredStartDate = _subscriptionBusiness.GetMonthDesiredStartDate(orderItem.ProgramSchedule.ProgramId, model.newDesiredStartDate.Value);
                        var newInstallmentParts = new List<ProgramSchedulePart>();

                        foreach (var item in scheduleMonths)
                        {
                            if (item.DueDate == newScheduleForNewDesiredStartDate.DueDate)
                            {
                                newInstallmentParts.Add(item);
                            }
                            if (item.DueDate < newScheduleForNewDesiredStartDate.DueDate)
                            {
                                continue;
                            }
                            else if (item.DueDate > newScheduleForNewDesiredStartDate.DueDate && item.DueDate < scheduleOldDesiredDate.DueDate)
                            {
                                newInstallmentParts.Add(item);
                            }
                        }

                        if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                        {
                            var discountChargesAmount = _orderItemBusiness.UpdateOrderItemChargeDiscounts(orderItem, newInstallmentParts.Count, model.newDesiredStartDate, null, newInstallmentParts) / newInstallmentParts.Count;
                            foreach (var part in newInstallmentParts)
                            {
                                var installment = new OrderInstallment();
                                installment.Amount = entryfee + discountChargesAmount;
                                installment.InstallmentDate = part.DueDate.Value;
                                installment.Status = OrderStatusCategories.initiated;
                                installment.OrderItemId = model.OrderItemId;
                                installment.Type = InstallmentType.Noraml;
                                installment.PaidAmount = 0;
                                installment.ProgramSchedulePartId = part.Id;

                                orderItem.EntryFee += entryfee;
                                orderItem.TotalAmount += entryfee;
                                orderItem.Order.OrderAmount += entryfee;
                                orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.EntryFee).FirstOrDefault().Amount += entryfee;
                                insallmentsItem.Add(installment);
                            }
                        }
                        else
                        {
                            foreach (var part in newInstallmentParts)
                            {
                                var listParts = new List<ProgramSchedulePart>();
                                listParts.Add(part);
                                var selectedDays = orderItem.Attributes.WeekDays;
                                var discountChargesAmount = _orderItemBusiness.UpdateOrderItemChargeDiscounts(orderItem, listParts.Count, model.newDesiredStartDate, orderItemScheduleParts, listParts);
                                decimal amount = _orderItemBusiness.GetProgramSchedulePartAmount(part, orderItem.ProgramSchedule, selectedCharge, selectedDays, model.newDesiredStartDate);

                                var installment = new OrderInstallment();
                                installment.Amount = amount + discountChargesAmount;
                                installment.InstallmentDate = part.DueDate.Value;
                                installment.Status = OrderStatusCategories.initiated;
                                installment.OrderItemId = model.OrderItemId;
                                installment.Type = InstallmentType.Noraml;
                                installment.PaidAmount = 0;
                                installment.ProgramSchedulePartId = part.Id;

                                orderItem.EntryFee += amount;
                                orderItem.TotalAmount += amount;
                                orderItem.Order.OrderAmount += amount;
                                orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.EntryFee).FirstOrDefault().Amount += amount;
                                insallmentsItem.Add(installment);

                                var availableOrderPart = orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == part.Id).FirstOrDefault();

                                if (availableOrderPart != null)
                                {
                                    orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == part.Id).FirstOrDefault().PartAmount = amount;
                                    orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == part.Id).FirstOrDefault().IsDeleted = false;
                                }
                                else
                                {
                                    orderItem.OrderItemScheduleParts.Add(new OrderItemSchedulePart
                                    {
                                        PartAmount = amount,
                                        ProgramPartId = part.Id

                                    });
                                }
                            }

                            //update old installment amounts
                            var newAmount = charges.Where(c => c.SchedulePartId == scheduleOldDesiredDate.Id).FirstOrDefault().ChargeAmount;
                            _orderItemBusiness.updateInstallmentAmount(orderItem, scheduleOldDesiredDate, orderItemScheduleParts, newAmount, true, newInstallmentParts.Count);
                        }

                        orderItem.Installments.AddRange(insallmentsItem);
                        orderItem.DesiredStartDate = model.newDesiredStartDate;
                    }
                    else if (model.newDesiredStartDate >= firstMonthEndDate)
                    {
                        newScheduleForNewDesiredStartDate = _subscriptionBusiness.GetMonthDesiredStartDate(orderItem.ProgramSchedule.ProgramId, model.newDesiredStartDate.Value);
                        var removeSchedules = new List<ProgramSchedulePart>();

                        foreach (var schedule in scheduleMonths)
                        {
                            if (schedule.EndDate < newScheduleForNewDesiredStartDate.StartDate)
                            {
                                removeSchedules.Add(schedule);
                            }
                            else
                            {
                                continue;
                            }
                        }

                        var listDeleteInstallments = orderItem.Installments.Where(c => !c.IsDeleted && removeSchedules.Select(d => d.DueDate).ToList().Contains(c.InstallmentDate)).ToList();
                        var installmentDeletedAmount = listDeleteInstallments.Sum(p => (decimal?)p.Amount) ?? 0;

                        listPaidDeletedInstallmenst = listDeleteInstallments.Where(inst => inst.Status == OrderStatusCategories.completed).ToList();
                        paidInstallmentAmount = listPaidDeletedInstallmenst.Count > 0 ? listPaidDeletedInstallmenst.Sum(p => (decimal?)p.PaidAmount) ?? 0 : 0;

                        orderItem.DesiredStartDate = model.newDesiredStartDate;

                        var deletedinstallCount = listDeleteInstallments.Count;

                        if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                        {
                            var chargeDiscountAmount = _orderItemBusiness.UpdateOrderItemChargeDiscounts(orderItem, deletedinstallCount, model.newDesiredStartDate, null, null, true);
                            orderItem.EntryFee -= (entryfee * deletedinstallCount);
                            orderItem.GetOrderChargeDiscounts().Where(o => o.Category == ChargeDiscountCategory.EntryFee).FirstOrDefault().Amount -= (entryfee * deletedinstallCount);
                            orderItem.TotalAmount -= ((entryfee * deletedinstallCount) + chargeDiscountAmount);
                            orderItem.Order.OrderAmount -= ((entryfee * deletedinstallCount) + chargeDiscountAmount);
                        }
                        else
                        {
                            var deletedInstallmentParts = listDeleteInstallments.Select(i => i.ProgramSchedulePart).ToList();

                            foreach (var part in deletedInstallmentParts)
                            {
                                var listDeletedParts = new List<ProgramSchedulePart>();
                                listDeletedParts.Add(part);

                                var chargeDiscountAmount = _orderItemBusiness.UpdateOrderItemChargeDiscounts(orderItem, listDeletedParts.Count, model.newDesiredStartDate, orderItemScheduleParts, listDeletedParts, true);
                                var partAmount = orderItemScheduleParts.Where(p => p.PartId == part.Id).FirstOrDefault().Amount;

                                orderItem.EntryFee -= partAmount;
                                orderItem.GetOrderChargeDiscounts().FirstOrDefault(o => o.Category == ChargeDiscountCategory.EntryFee).Amount -= (partAmount);
                                orderItem.TotalAmount -= (partAmount + chargeDiscountAmount);
                                orderItem.Order.OrderAmount -= (partAmount + chargeDiscountAmount);
                            }

                            //update new first month amount
                            var newDesiredStartDateInstallment = orderItem.Installments.FirstOrDefault(i => !i.IsDeleted && i.ProgramSchedulePartId == newScheduleForNewDesiredStartDate.Id);
                            var newDesiredStartDateInstallmentPaidAmount = newDesiredStartDateInstallment.PaidAmount;
                            var selectedDays = orderItem.Attributes.WeekDays;
                            decimal installmentAmount = _orderItemBusiness.GetProgramSchedulePartAmount(newScheduleForNewDesiredStartDate, orderItem.ProgramSchedule, selectedCharge, selectedDays, model.newDesiredStartDate);

                            oldInstallmentByDesiredStartDate = orderItem.Installments.FirstOrDefault(i => i.Type == InstallmentType.Noraml && !i.IsDeleted && i.ProgramSchedulePartId == newScheduleForNewDesiredStartDate.Id);
                            _orderItemBusiness.updateInstallmentAmount(orderItem, newScheduleForNewDesiredStartDate, orderItemScheduleParts, installmentAmount, false, deletedInstallmentParts.Count);
                            orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == newScheduleForNewDesiredStartDate.Id).FirstOrDefault().PartAmount = installmentAmount;
                            var updatedNewDesiredStartDateInstallmentAmount = newDesiredStartDateInstallment.Amount;

                            if (updatedNewDesiredStartDateInstallmentAmount < newDesiredStartDateInstallmentPaidAmount)
                            {
                                decimal diffrenceAmount = newDesiredStartDateInstallmentPaidAmount.Value - updatedNewDesiredStartDateInstallmentAmount;
                                newDesiredStartDateInstallment.PaidAmount = updatedNewDesiredStartDateInstallmentAmount;
                                paidInstallmentAmount += diffrenceAmount;
                            }

                            orderItem.PaidAmount -= paidInstallmentAmount;

                            foreach (var paidInstallment in listPaidDeletedInstallmenst)
                            {
                                paidInstallment.PaidAmount = 0;
                                paidInstallment.Status = OrderStatusCategories.refunded;
                            }

                            if (orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId) == null)
                            {
                                Ioc.ClubBusiness.AddUserInClubUsers(orderItem.Order.UserId, orderItem.Order.ClubId);
                            }

                            if (orderItem.Order.IsLive)
                            {
                                orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId).Credit += paidInstallmentAmount;
                            }
                            else
                            {
                                orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId).TestCredit += paidInstallmentAmount;
                            }
                        }

                        Ioc.InstallmentBusiness.SetStatusDeletedInstallment(listDeleteInstallments, DeleteReason.ChangeDesiredStartDate);
                    }

                    // refund amount to credit
                    if (listPaidDeletedInstallmenst.Count > 0 || differenceAmount > 0)
                    {
                        orderItem.TransactionActivities = new List<TransactionActivity>();

                        var paymentDetail = new PaymentDetail(this.GetCurrentUserName(), PaymentDetailStatus.COMPLETED, "", PaymentMethod.Credit, club.Currency, "", "", string.Empty, JbUserService.GetCurrentUserRoleType());

                        model.refundedAmount = listPaidDeletedInstallmenst.Count > 0 ? paidInstallmentAmount : differenceAmount;

                        long? installmentId = null;

                        if (listPaidDeletedInstallmenst.Count == 1)
                        {
                            installmentId = listPaidDeletedInstallmenst.First().Id;
                        }
                        else if (differenceAmount > 0)
                        {
                            installmentId = oldInstallmentByDesiredStartDate.Id;
                        }

                        orderItem.TransactionActivities.Add(_orderItemBusiness.SetRefundTransaction(orderItem, installmentId.Value, paymentDetail, TransactionStatus.Success, model.refundedAmount, date, "Refund to family credit"));
                    }

                    var isScheduleItemCombo = orderItem.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both ? true : false;
                    var historyDescription = string.Empty;
                    var strDesiredStartDate = model.newDesiredStartDate.Value.ToString("MMM dd,yyyy");
                    historyDescription += string.Format("{0}", "You changed the desired start date to " + strDesiredStartDate);

                    orderItem.Order.OrderHistories.Add(
                       new OrderHistory
                       {
                           Action = OrderAction.ChangeDesiredStartDate,
                           ActionDate = DateTime.UtcNow,
                           Description = historyDescription,
                           UserId = this.GetCurrentUserId(),
                           OrderItemId = orderItem.Id
                       });

                    result = _orderSessionBusiness.CreateSessionByChangeDesiredStartDate(orderItem, model.Days, model.newDesiredStartDate.Value, isScheduleItemCombo);

                    var StrRefundedAmount = CurrencyHelper.FormatCurrencyWithoutPenny(model.refundedAmount, orderItem.Order.Club.Currency);
                    result.Message = model.refundedAmount > 0 ? "New desired start date updated successfully. we refunded " + StrRefundedAmount + " to family credit" : "New desired start date updated successfully";

                    return Json(new JResult { Status = result.Status, Message = result.Message });
                }
                catch (Exception ex)
                {
                    return Json(new JResult { Status = false, Message = ex.Message });
                }
            }

            return JsonFormResponse();
        }

        [HttpPost]
        public virtual ActionResult CancelEditDropIn(EditOrderViewModel model)
        {
            bool cancelMode = false;
            var deletedSessions = new List<OrderSession>();
            var forDeleteIds = new List<long>();

            OrderItem orderItem = _orderItemBusiness.GetItem(model.Id);
            CheckResourceForAccess(orderItem);

            var club = orderItem.Order.Club;

            //if we want cancel sessions 
            forDeleteIds = model.OrderSessions.Where(o => o.IsDeleted && o.Id != 0).Select(o => o.Id).ToList();
            deletedSessions.AddRange(orderItem.OrderSessions.Where(s => forDeleteIds.Contains(s.Id)).ToList());

            //if we want change date 
            var updatedSessionIds = model.OrderSessions.Where(o => o.UpdatedDate.HasValue).Select(s => s.Id).ToList();
            deletedSessions.AddRange(orderItem.OrderSessions.Where(s => updatedSessionIds.Contains(s.Id)).ToList());

            var updatedSessions = new List<OrderSession>();
            if (updatedSessionIds.Count > 0)
            {
                var programSessions = Ioc.ProgramSessionBusiness.GetList(orderItem.ProgramSchedule.Program);

                var orderItemProgramSessionIds = orderItem.OrderSessions.Select(o => o.ProgramSessionId).ToList();

                programSessions = programSessions.Where(p => !orderItemProgramSessionIds.Contains(p.Id)).ToList();

                foreach (var session in model.OrderSessions.Where(s => s.UpdatedDate.HasValue))
                {
                    var programSession = programSessions.FirstOrDefault(p => p.StartDateTime.Date == session.UpdatedDate.Value.Date);
                    var orderSession = new OrderSession()
                    {
                        ProgramSessionId = programSession.Id,
                        Amount = session.Amount,
                        MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                        IsSameDayDropIn = session.IsSameDay
                    };

                    updatedSessions.Add(orderSession);
                }
            }

            if (deletedSessions.Count > 0 && updatedSessions.Count > 0)
            {
                cancelMode = false;
            }
            else if (model.OrderChargeDiscounts == null || !model.OrderChargeDiscounts.Any(c => c.Category == ChargeDiscountCategory.EntryFee && c.Checked))
            {
                cancelMode = true;
            }

            var selectedTuition = model.OrderChargeDiscounts != null ? model.OrderChargeDiscounts.SingleOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee) : null;

            OperationStatus res = new OperationStatus();
            var message = "";

            if (orderItem.ProgramScheduleId.HasValue && !cancelMode)
            {
                var programId = orderItem.ProgramScheduleId.HasValue ? orderItem.ProgramSchedule.ProgramId : 0;
                var programCoupons = new List<Coupon>();

                if (programId != 0)
                {
                    programCoupons = Ioc.CouponBusiness.GetProgramCoupons(programId);
                }

                orderItem.OrderChargeDiscounts.Clear();

                foreach (var item in model.OrderChargeDiscounts.Where(c => c.Checked))
                {
                    if (item.Id == -1 || item.Id == -2)
                    {
                        item.ChargeId = null;
                        item.DiscountId = null;
                    }

                    if (item.SubCategory == ChargeDiscountSubcategory.Charge || (item.SubCategory == ChargeDiscountSubcategory.Discount && !(item.Category == ChargeDiscountCategory.Coupon)))
                    {
                        orderItem.OrderChargeDiscounts.Add(
                        new OrderChargeDiscount
                        {
                            Amount = item.SubCategory == ChargeDiscountSubcategory.Discount ? -item.Amount : item.Amount,
                            Category = item.Category,
                            ChargeId = item.ChargeId,
                            DiscountId = item.DiscountId,
                            Name = item.Name,
                            Subcategory = item.SubCategory,
                            OrderItemId = orderItem.Id,
                            Description = item.SubCategory == ChargeDiscountSubcategory.Discount ? string.Format("{0} off for {1}", CurrencyHelper.FormatCurrencyWithPenny(item.Amount, club.Currency), item.Name) : null,
                            Coupon = item.Category == ChargeDiscountCategory.Coupon ? Ioc.CouponBusiness.Get(item.CouponId.Value) : null,
                            CouponId = item.CouponId,
                        });
                    }
                    else
                    {
                        if (item.Category == ChargeDiscountCategory.Coupon)
                        {
                            var coupon = Ioc.CouponBusiness.Get(item.CouponId.Value);

                            orderItem.OrderChargeDiscounts.Add(
                                new OrderChargeDiscount
                                {
                                    //Id = item.Id,
                                    Amount = item.Category == ChargeDiscountCategory.Coupon ? -item.Amount : item.Amount,
                                    Category = item.Category,
                                    Name = item.Name,
                                    Subcategory = item.SubCategory,
                                    OrderItemId = orderItem.Id,
                                    Description = item.SubCategory == ChargeDiscountSubcategory.Discount ? string.Format("{0} off for {1}", CurrencyHelper.FormatCurrencyWithPenny(item.Amount, club.Currency), item.Name) : null,
                                    //Coupon = item.Category == ChargeDiscountCategory.Coupon ? Ioc.CouponBusiness.Get(item.CouponId.Value) : null,
                                    CouponId = coupon.Id,
                                    Coupon = coupon

                                });
                        }
                    }
                }

                if (model.EditFee.HasValue && model.EditFee != 0)
                {
                    if (model.OrderChargeDiscounts == null)
                    {
                        model.OrderChargeDiscounts = new List<ChargeDiscountItemViewModel>();
                    }

                    orderItem.OrderChargeDiscounts.Add(
                        new OrderChargeDiscount
                        {
                            Amount = model.EditFee.Value,
                            Category = ChargeDiscountCategory.EditFee,
                            Name = Constants.W_Edit_Fee,
                            Description = Constants.W_Edit_Fee,
                            Subcategory = ChargeDiscountSubcategory.Charge
                        }
                    );
                }

                if (model.CancellationFee.HasValue && model.CancellationFee != 0)
                {
                    if (model.OrderChargeDiscounts == null)
                    {
                        model.OrderChargeDiscounts = new List<ChargeDiscountItemViewModel>();
                    }

                    orderItem.OrderChargeDiscounts.Add(
                        new OrderChargeDiscount
                        {
                            Amount = model.CancellationFee.Value,
                            Category = ChargeDiscountCategory.CancellationFee,
                            Name = Constants.W_Cancel_Fee,
                            Description = Constants.W_Cancel_Fee,
                            Subcategory = ChargeDiscountSubcategory.Charge
                        }
                    );
                }

                if (orderItem.ProgramScheduleId.HasValue)
                {
                    orderItem.EntryFee = selectedTuition.Amount;
                    orderItem.EntryFeeName = selectedTuition.Name;
                }

                orderItem.CalculateDiscounts();

                orderItem.CalculateTotalAmount();

                var userId = this.GetCurrentUserId();
                var allDeletedSessionsDate = model.OrderSessions.Where(o => o.IsDeleted && o.Id != 0).Select(s => s.Date.Date);
                var stringDates = string.Join(", ", allDeletedSessionsDate);

                if (cancelMode)
                {
                    orderItem.ItemStatus = orderItem.TotalAmount == 0 ? OrderItemStatusCategories.changed : orderItem.ItemStatus;
                    orderItem.ItemStatusReason = orderItem.TotalAmount == 0 ? OrderItemStatusReasons.canceled : orderItem.ItemStatusReason;
                }

                res = _orderItemBusiness.EditDropIn(orderItem, deletedSessions, updatedSessions, this.GetCurrentUserId(), stringDates, model.CancelEditMemo);
            }
            else
            {
                orderItem.GetOrderChargeDiscounts().Where(o => o.Category == ChargeDiscountCategory.CancellationFee).ToList().ForEach(c => c.IsDeleted = true);

                if (model.CancellationFee.HasValue && model.CancellationFee != 0)
                {
                    if (model.OrderChargeDiscounts == null)
                    {
                        model.OrderChargeDiscounts = new List<ChargeDiscountItemViewModel>();
                    }

                    orderItem.OrderChargeDiscounts.Add(
                        new OrderChargeDiscount
                        {
                            Amount = model.CancellationFee.Value,
                            Category = ChargeDiscountCategory.CancellationFee,
                            Name = Constants.W_Cancel_Fee,
                            Description = Constants.W_Cancel_Fee,
                            Subcategory = ChargeDiscountSubcategory.Charge
                        }
                    );
                }

                var cancellationFee = model.CancellationFee ?? 0;

                res = _orderItemBusiness.CancelDropIn(orderItem, this.GetCurrentUserId(), cancellationFee, model.CancelEditMemo);
            }

            if (model.SendEmail && res.Status)
            {
                var dropInEmailModel = new List<DropInSessionsViewModel>();

                foreach (var session in model.OrderSessions)
                {
                    if (session.IsDeleted)
                    {
                        session.Status = "Cancelled";
                    }
                    else if (session.UpdatedDate.HasValue)
                    {
                        if (session.UpdatedDate.Value == session.Date)
                        {
                            session.Status = "-";
                        }
                        else
                        {
                            session.Status = "Date modified";
                        }
                    }
                    else
                    {
                        session.Status = "-";
                    }

                    dropInEmailModel.Add(session);
                }

                ReservationEmailMode emailMode = new ReservationEmailMode();

                emailMode = !cancelMode ? ReservationEmailMode.Edit : ReservationEmailMode.Cancel;
                EmailService.Get(this.ControllerContext).SendEditCancelDropInEmail(orderItem, orderItem.Order.ConfirmationId, dropInEmailModel, club.Id, orderItem.Order.UserId, emailMode);
            }

            message = message != null ? message : res.Message;

            return Json(new JResult { Status = res.Status, Message = message, RecordsAffected = res.RecordsAffected });

        }

        [HttpGet]
        public virtual JsonNetResult GetCalendarForItem(int orderItemId)
        {
            var model = new DropInCalendarModel();
            var orderItem = _orderItemBusiness.GetItem(orderItemId);

            var program = orderItem.ProgramSchedule.Program;

            model.StartProgramDate = _programBusiness.StartDate(program).Value;
            model.EndProgramDate = _programBusiness.EndDate(program).Value;

            return JsonNet(model);

        }
        public virtual ActionResult GetCalendarItems(long programId, long orderItemId, long programSessionId, long oldSessionId)
        {
            var sessions = new List<DropInCalendarModel>();

            var program = _programBusiness.Get(programId);
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var club = program.Club;
            var schedules = program.ProgramSchedules.Where(s => !s.IsDeleted);

            var subscriptionSchedule = schedules.Last();

            var subscriptionAttribute = _subscriptionBusiness.GetSubscriptionAtribute(subscriptionSchedule);

            var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow);

            var programSessionBusiness = Ioc.ProgramSessionBusiness;

            IEnumerable<ProgramSession> programSessions = null;

            programSessions = programSessionBusiness.GetList(program);

            var allProgramSessions = programSessions.ToList();
            programSessionBusiness.ApplyHolidays(program, allProgramSessions);

            var currentSession = orderItem.OrderSessions.Where(o => o.ProgramSessionId == programSessionId).FirstOrDefault();

            sessions = allProgramSessions.Select(s =>
                new DropInCalendarModel
                {
                    Id = s.Id,
                    TaskID = s.Id,
                    Selected = false,
                    Start = DateTime.SpecifyKind(s.StartDateTime, DateTimeKind.Utc),
                    End = DateTime.SpecifyKind(s.EndDateTime, DateTimeKind.Utc),
                    DoDate = DateTime.SpecifyKind(s.StartDateTime, DateTimeKind.Utc),
                    IsAllDay = false,
                    Title = subscriptionAttribute.DropInSessionLabel,
                    ScheduleId = subscriptionSchedule.Id,
                    ScheduleTitle = subscriptionSchedule.Title,
                    Amount = subscriptionAttribute.DropInSessionPrice,
                    ChargeName = s.StartDateTime.ToString("MMM dd") + " " + s.StartDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + "-" + s.EndDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", ""),
                    ChargeCategory = ChargeDiscountCategory.EntryFee.ToString(),
                    Description2 = subscriptionSchedule.Title,
                    StartTimezone = "Etc/UTC",
                    EndTimezone = "Etc/UTC",
                    IsFull = programSessionBusiness.IsFull(s, subscriptionAttribute.Capacity),
                    IsChecked = false,
                })
                .ToList();

            var orderSessions = sessions.Where(s => orderItem.OrderSessions.Select(o => o.ProgramSessionId).ToList().Contains(s.Id));

            foreach (var item in sessions)
            {
                if (orderItem.OrderSessions.Select(o => o.ProgramSessionId).ToList().Contains(item.Id))
                {
                    if (item.Id == programSessionId)
                    {
                        item.IsChecked = true;
                    }
                    else
                    {
                        var session = orderItem.OrderSessions.Where(o => o.Id == oldSessionId).FirstOrDefault();
                        if (item.Id != session.ProgramSessionId)
                        {
                            item.IsChecked = true;
                            item.IsOrderSession = true;
                        }
                    }
                }
                else
                {
                    if (item.Id == programSessionId)
                    {
                        item.IsChecked = true;
                    }
                }
            }

            return JsonNet(sessions);
        }

        private void ValidationCheck(ModelStateDictionary modelState, ChangeDesiredStartDateViewModel model)
        {
            var orderItem = _orderItemBusiness.GetItem(model.OrderItemId);
            var today = DateTime.UtcNow;

            if (!model.newDesiredStartDate.HasValue)
            {
                ModelState.AddModelError("model.newDesiredStartDate", "Desired start date is required.");
            }
            else
            {
                if (!model.Days.Contains(model.newDesiredStartDate.Value.DayOfWeek))
                {
                    ModelState.AddModelError("model.newDesiredStartDate", "Desired start date should be one of the selected class days.");
                }
                else if (model.newDesiredStartDate > model.LastSessiondate)
                {
                    ModelState.AddModelError("model.newDesiredStartDate", "Desired start date (" + (model.newDesiredStartDate.Value.ToString(Constants.DefaultDateFormat)) + ")" + " must be before the order end date (" + (model.LastSessiondate.ToString(Constants.DefaultDateFormat)) + ")");
                }
                else if (model.newDesiredStartDate > model.LastProgramSessiondate)
                {
                    ModelState.AddModelError("model.newDesiredStartDate", "Desired start date (" + (model.newDesiredStartDate.Value.ToString(Constants.DefaultDateFormat)) + ")" + " must be before the program end date (" + (model.LastProgramSessiondate.ToString(Constants.DefaultDateFormat)) + ")");
                }
                else if (model.newDesiredStartDate < orderItem.ProgramSchedule.StartDate.Date)
                {
                    ModelState.AddModelError("model.newDesiredStartDate", "Desired start date cannot be before program start date (" + orderItem.ProgramSchedule.StartDate.Date.ToString(Constants.DefaultDateFormat) + ")");
                }
            }
        }

        private CancelSubscriptionOrderViewModel GetInformationForCancelSubscription(CancelSubscriptionOrderViewModel model)
        {
            var orderItem = _orderItemBusiness.GetItem(model.OrderItemId);
            CheckResourceForAccess(orderItem);

            object gender = null;

            if (orderItem.ProgramScheduleId.HasValue && orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                gender = orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.Gender.ToString()).GetValue();
            }

            int age = 0;
            if (orderItem.ProgramScheduleId.HasValue && !_programBusiness.IsTeamChessTourney(orderItem.ProgramSchedule.Program) && orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
            {
                var dobDate = DateTime.UtcNow;
                if (DateTime.TryParse(orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.DoB.ToString()).GetValue().ToString(), out dobDate))
                {
                    age = DateTime.Now.Year - dobDate.Year;
                }
            }
            model.HasProgramSchedule = orderItem.ProgramScheduleId.HasValue ? true : false;
            if (orderItem != null && orderItem.ProgramScheduleId.HasValue)
            {
                var installments = Ioc.InstallmentBusiness.GetListByOrderItemId(model.OrderItemId).Where(i => i.Type == InstallmentType.Noraml && i.InstallmentDate > DateTime.UtcNow);
                model.FullName = orderItem.FullName;
            }
            else
            {
                model.Name = orderItem.Name;
            }

            model.PaymentPlan = orderItem.PaymentPlanType;
            var orderSubscriptionItems = new List<ProgramSchedulePart>();
            model.ItemSchedules = new List<InstallmentandScheduleItemViewModel>();

            var orderInstallments = orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted).ToList();
            var programParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

            if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
            {
                orderSubscriptionItems = programParts.Where(p => orderInstallments.Select(i => i.ProgramSchedulePartId).ToList().Contains(p.Id)).ToList();

                foreach (var schedule in orderSubscriptionItems)
                {
                    foreach (var inst in orderInstallments)
                    {
                        if (schedule.Id == inst.ProgramSchedulePartId)
                        {
                            model.ItemSchedules.Add(new InstallmentandScheduleItemViewModel()
                            {
                                StartDate = schedule.StartDate,
                                EndDate = schedule.EndDate,
                                PaidAmount = inst.PaidAmount.HasValue ? inst.PaidAmount.Value : 0,
                                PaymentDueDate = schedule.DueDate.Value,
                                InstallmentId = inst.Id,
                                Amount = inst.Amount,
                                ProgramPartId = inst.ProgramSchedulePartId,
                            });
                        }
                    }
                }
            }
            else
            {
                orderSubscriptionItems = programParts.Where(p => orderItem.OrderItemScheduleParts.Where(i => !i.IsDeleted).Select(i => i.ProgramPartId).ToList().Contains(p.Id)).ToList();

                foreach (var schedule in orderSubscriptionItems)
                {
                    foreach (var part in orderItem.OrderItemScheduleParts)
                    {
                        if (schedule.Id == part.ProgramPartId)
                        {
                            model.ItemSchedules.Add(new InstallmentandScheduleItemViewModel()
                            {
                                StartDate = schedule.StartDate,
                                EndDate = schedule.EndDate,
                                //PaymentDueDate = schedule.DueDate.Value,
                                Amount = part.PartAmount,
                                ProgramPartId = part.ProgramPartId,
                            });
                        }
                    }
                }
            }

            var sessions = orderItem.OrderSessions.ToList();
            var lastSessiondate = sessions.Count > 0 ? sessions.LastOrDefault().ProgramSession.StartDateTime : (DateTime?)null;

            model.ProgramName = _programBusiness.GetProgramName(orderItem.ProgramSchedule.Program.Name, orderItem.Start.Value, orderItem.End.Value, orderItem.EntryFeeName);
            model.TotalAmount = orderItem.TotalAmount;
            model.DesiredStartDate = orderItem.DesiredStartDate;
            model.TodayDate = DateTime.UtcNow;
            model.Balance = Ioc.AccountingBusiness.OrderItemBalance(orderItem);
            model.PaidAmount = orderItem.PaidAmount;
            model.Gender = gender;
            model.Age = age;
            model.CancellationFee = 0;
            model.LastSessionDate = lastSessiondate.HasValue ? lastSessiondate.Value : (DateTime?)null;
            model.ConfirmationId = orderItem.Order.ConfirmationId;
            model.EntryFee = orderItem.EntryFee;
            model.SourceEntryFee = orderItem.EntryFee;
            model.DesiredStartDate = orderItem.DesiredStartDate;

            model.SourceCharges = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge && c.Category != ChargeDiscountCategory.EntryFee).Select(c =>
                 new ChargeDiscountItemViewModel
                 {
                     Amount = c.Amount,
                     Category = c.Category,
                     Name = c.Name,
                     SubCategory = c.Subcategory,
                     Id = c.Id,
                 })
                 .ToList();

            model.SourceDiscounts = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount).Select(c =>
                new ChargeDiscountItemViewModel
                {
                    Amount = c.Amount,
                    Category = c.Category,
                    Name = c.Name,
                    SubCategory = c.Subcategory,
                    Id = c.Id,
                })
                .ToList();

            return model;
        }

        private CancelSubscriptionOrderViewModel cancelSubscriptionItem(CancelSubscriptionOrderViewModel model, OrderItem orderItem)
        {
            var listUpdatedChargeDiscount = new List<OrderChargeDiscount>();
            decimal balance = 0;

            foreach (var schedule in model.ItemSchedules)
            {
                if (schedule.StartDate < model.EffectiveDate && schedule.EndDate >= model.EffectiveDate || model.EffectiveDate > schedule.EndDate)
                {
                    model.PastItemSchedules.Add(schedule);
                }
                else
                {
                    model.DeletedItemSchedules.Add(schedule);
                }
            }

            decimal deletedPaidInstallmentsAmount = model.DeletedItemSchedules.Sum(p => (decimal?)p.PaidAmount) ?? 0;

            if (deletedPaidInstallmentsAmount > 0)
            {
                if (model.CancellationFee > deletedPaidInstallmentsAmount)
                {
                    model.NewBalance = model.CancellationFee - deletedPaidInstallmentsAmount;
                }
                else
                {
                    model.RefundFee = deletedPaidInstallmentsAmount - model.CancellationFee;
                }
            }
            else
            {
                var unpaidPastScheduleAmount = model.PastItemSchedules.Where(s => s.Amount != s.PaidAmount).ToList();
                if (unpaidPastScheduleAmount != null)
                {
                    foreach (var pastItem in unpaidPastScheduleAmount)
                    {
                        balance += (pastItem.Amount - pastItem.PaidAmount) ?? 0;
                    }
                }
                model.NewBalance = model.CancellationFee + balance;
            }

            var installmentWithOutAppFee = orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted).ToList();

            decimal orderItemEntryfee = 0;

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
            {
                orderItemEntryfee = orderItem.EntryFee / installmentWithOutAppFee.Count;
                model.EntryFee -= (orderItemEntryfee * model.DeletedItemSchedules.Count);
            }
            else
            {
                foreach (var item in model.DeletedItemSchedules)
                {
                    decimal amount = 0;
                    var installment = orderItem.OrderItemScheduleParts.Where(i => i.ProgramPartId == item.ProgramPartId).FirstOrDefault();
                    amount = installment != null ? installment.PartAmount : 0;
                    model.EntryFee -= amount;
                }
            }

            listUpdatedChargeDiscount = UpdateOrderChargeDiscount(orderItem, model.DeletedItemSchedules, installmentWithOutAppFee.Count);

            foreach (var chargeDiscount in listUpdatedChargeDiscount)
            {
                model.UpdatedChargeDiscounts.Add(
                   new ChargeDiscountItemViewModel
                   {
                       Amount = chargeDiscount.Amount,
                       Category = chargeDiscount.Category,
                       Name = chargeDiscount.Name,
                       Id = chargeDiscount.Id,
                       SubCategory = chargeDiscount.Subcategory
                   }
               );
            }

            if (model.CancellationFee != 0)
            {
                model.UpdatedChargeDiscounts.Add(
                    new ChargeDiscountItemViewModel
                    {
                        Amount = model.CancellationFee,
                        Category = ChargeDiscountCategory.CancellationFee,
                        Name = Constants.W_Cancel_Fee,
                        SubCategory = ChargeDiscountSubcategory.Charge
                    }
                );
            }

            var totalAmount = model.EntryFee + model.UpdatedChargeDiscounts.Sum(p => (decimal?)p.Amount) ?? 0;
            model.NewTotalAmount = totalAmount;
            model.NewBalance = totalAmount - orderItem.PaidAmount;

            model.UpdatedCharges.AddRange(model.UpdatedChargeDiscounts.Where(d => d.SubCategory == ChargeDiscountSubcategory.Charge));
            model.UpdatedDiscounts.AddRange(model.UpdatedChargeDiscounts.Where(d => d.SubCategory == ChargeDiscountSubcategory.Discount));

            return model;
        }

        public ActionResult GetPunchCardCalendarItems(long orderItemId, int? orderSessionId, bool loadCapacity = true)
        {
            var model = new List<PunchcardCalendarItemViewModel>();
            var programSessionBusiness = Ioc.ProgramSessionBusiness;

            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var programSessions = Ioc.ProgramSessionBusiness.GetList(orderItem.ProgramSchedule.Program).ToList();

            var orderItemSessions = _orderSessionBusiness.GetOrderItemSessions(orderItemId).ToList();
            var orderItemProgramSessionIds = orderItemSessions.Select(o => o.ProgramSessionId).ToList();

            var selectedProgramSessionIds = new List<int>();

            if (orderSessionId.HasValue)
            {
                var selectedOrderItemSession = orderItemSessions.SingleOrDefault(o => o.Id == orderSessionId.Value);

                if (selectedOrderItemSession != null)
                {
                    selectedProgramSessionIds.Add(selectedOrderItemSession.ProgramSessionId.Value);

                    var sameDayProgramSession = Ioc.ProgramSessionBusiness.GetSameDayProgramSession(selectedOrderItemSession.ProgramSession);

                    if (sameDayProgramSession != null)
                    {
                        if (orderItemSessions.Any(o => o.ProgramSessionId == sameDayProgramSession.Id))
                        {
                            selectedProgramSessionIds.Add(sameDayProgramSession.Id);
                        }
                    }
                }
            }
            var allProgramSessions = programSessions.ToList();

            programSessionBusiness.ApplyHolidays(orderItem.ProgramSchedule.Program, allProgramSessions);

            model = allProgramSessions.Select(s =>
              new PunchcardCalendarItemViewModel
              {
                  ProgramSessionId = s.Id,
                  Start = DateTime.SpecifyKind(s.StartDateTime, DateTimeKind.Utc),
                  End = DateTime.SpecifyKind(s.EndDateTime, DateTimeKind.Utc),
                  Title = GetProgramSessionTitle(s),
                  IsAssigned = orderItemProgramSessionIds.Contains(s.Id) && !selectedProgramSessionIds.Contains(s.Id),
                  IsSelected = selectedProgramSessionIds.Contains(s.Id),
                  Date = s.StartDateTime.Date.ToShortDateString(),
                  StartTimezone = "Etc/UTC",
                  EndTimezone = "Etc/UTC",
                  IsFull = loadCapacity ? programSessionBusiness.IsFull(s) : false,
              })
              .ToList();

            var result = JsonNet(model);

            return result;
        }

        private string GetProgramSessionTitle(ProgramSession programSession)
        {
            var result = string.Format("{0}-{1}", programSession.StartDateTime.ToShortTimeString(), programSession.EndDateTime.ToShortTimeString());


            return result;
        }
        [HttpPost]
        public ActionResult CheckSessionsAreInFreezedSchedule(long orderItemId, List<long> programSessionIds)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            var getProgramsessionsAreInFreezedSchedule = _orderSessionBusiness.CheckSessionsAreInFreezedSchedule(orderItem.ProgramSchedule.Program, programSessionIds);

            var sessions = string.Empty;

            if (getProgramsessionsAreInFreezedSchedule.Any())
                sessions = string.Join(", ", getProgramsessionsAreInFreezedSchedule.Select(s => s.StartDateTime));

            return JsonNet(new { Sessions = sessions, status = getProgramsessionsAreInFreezedSchedule.Any() });
        }


        [HttpPost]
        public ActionResult AssignPunchCardSession(long orderItemId, List<int> programSessionIds)
        {
            var operationResult = _orderSessionBusiness.AssignPunchCardSession(orderItemId, programSessionIds, this.GetCurrentUserId());

            var result = new JResult(operationResult);

            return JsonNet(result);
        }

        [HttpPost]
        public ActionResult ReassignPunchCardSession(long orderItemId, int orderSessionId, List<int> programSessionIds)
        {
            var operationResult = _orderSessionBusiness.ReassignPunchCardSession(orderItemId, orderSessionId, programSessionIds, this.GetCurrentUserId());

            var result = new JResult(operationResult);

            return JsonNet(result);
        }

        public ActionResult UnassignPunchCardSession(int sessionId)
        {
            var operationResult = _orderSessionBusiness.UnassignPunchCardSession(sessionId, this.GetCurrentUserId());

            var result = new JResult(operationResult);

            return JsonNet(result);
        }

        public ActionResult GetReassignItems(int orderSessioinId)
        {
            var orderSession = _orderSessionBusiness.GetList().Single(o => o.Id == orderSessioinId);

            var programSession = orderSession.ProgramSession;

            var sameDayProgramSession = Ioc.ProgramSessionBusiness.GetSameDayProgramSession(programSession);

            var result = new List<object>();

            return JsonNet(result);
        }

        public virtual JsonNetResult DeleteDraftOrder(long orderItemId)
        {
            var result = _orderItemBusiness.Delete(orderItemId);
            return JsonNet(result.Status);
        }

        [HttpGet]
        public ActionResult GeneratePdfRegistrationForm(int jbFormId)
        {
            var pdfModel = _jbFormBusiness.GetAsPdf(jbFormId);

            var pdfGenerator = new PdfGenerator();

            var file = pdfGenerator.GenerateFromView(this.ControllerContext, "~/Views/Shared/PdfTemplates/JbFormTemplate.cshtml", pdfModel);

            return File(file, Common.Constants.FileTypes.PdfMimeType, "Registration form.Pdf");
        }

        public ActionResult SetNewFormToOrders(long orderItemId, List<string> selectedOrders)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);

            var anotherOrderItemIds = new List<long>();

            foreach (var id in selectedOrders)
            {
                anotherOrderItemIds.Add(Convert.ToInt64(id));
            }

            var result = _orderItemBusiness.AssignNewFormToOrders(orderItem, this.GetCurrentUserId(), anotherOrderItemIds);

            return Json(new JResult { Status = result.Status });
        }

        private decimal calculateDiscountAmount(OrderItem orderItem, OrderChargeDiscount discount, List<InstallmentandScheduleItemViewModel> DeletedinstallmentsOrParts, List<OrderItemSchedulePartModel> scheduleParts)
        {
            decimal result = 0;

            var orderItemScheduleParts = orderItem.OrderItemScheduleParts.Where(p => !p.IsDeleted);

            if (discount.DiscountId.HasValue && discount.Category != ChargeDiscountCategory.CustomDiscount)
            {
                foreach (var item in DeletedinstallmentsOrParts)
                {
                    var schedulePart = orderItemScheduleParts.Where(i => i.ProgramPartId == item.ProgramPartId).FirstOrDefault();

                    decimal amount = 0;
                    if (schedulePart != null)
                    {
                        amount = scheduleParts.Where(p => p.PartId == schedulePart.ProgramPartId).FirstOrDefault().Amount;
                        result += (amount * discount.Discount.Amount) / 100;

                        schedulePart.PartAmount -= (amount * discount.Discount.Amount) / 100;
                    }
                }
            }
            else if (discount.CouponId.HasValue)
            {
                foreach (var item in DeletedinstallmentsOrParts)
                {
                    var schedulePart = orderItemScheduleParts.Where(i => i.ProgramPartId == item.ProgramPartId).FirstOrDefault();
                    decimal amount = 0;

                    if (schedulePart != null)
                    {
                        amount = scheduleParts.Where(p => p.PartId == schedulePart.ProgramPartId).FirstOrDefault().Amount;
                        result += (amount * discount.Coupon.Amount) / 100;

                        schedulePart.PartAmount -= (amount * discount.Coupon.Amount) / 100;
                    }
                }
            }
            else if (discount.Category == ChargeDiscountCategory.FullPaySubscription)
            {
                var beforeAfterCareAttribute = _subscriptionBusiness.GetBeforAfterCareAtribute(orderItem.ProgramSchedule);
                var fullPaidDiscount = beforeAfterCareAttribute.FullPayDiscount == 0 ? discount.Amount / orderItemScheduleParts.Count() : beforeAfterCareAttribute.FullPayDiscount;

                foreach (var item in DeletedinstallmentsOrParts)
                {
                    var schedulePart = orderItemScheduleParts.Where(i => i.ProgramPartId == item.ProgramPartId).FirstOrDefault();
                    decimal amount = 0;

                    if (schedulePart != null)
                    {
                        amount = scheduleParts.Where(p => p.PartId == schedulePart.ProgramPartId).FirstOrDefault().Amount;
                        result += (amount * fullPaidDiscount) / 100;

                        schedulePart.PartAmount -= (amount * fullPaidDiscount) / 100;
                    }
                }

            }
            else
            {
                if (discount.DiscountId.HasValue)
                {
                    foreach (var item in DeletedinstallmentsOrParts)
                    {
                        if (discount.Discount.AmountType == ChargeDiscountType.Percent)
                        {
                            var schedulePart = orderItemScheduleParts.Where(i => i.ProgramPartId == item.ProgramPartId).FirstOrDefault();

                            decimal amount = 0;
                            if (schedulePart != null)
                            {
                                amount = scheduleParts.Where(p => p.PartId == schedulePart.ProgramPartId).FirstOrDefault().Amount;
                                result += (amount * discount.Discount.Amount) / 100;

                                schedulePart.PartAmount -= (amount * discount.Discount.Amount) / 100;
                            }
                        }
                        else
                        {
                            var customeAmount = discount.Amount / orderItemScheduleParts.Count();
                            var schedulePart = orderItemScheduleParts.Where(i => i.ProgramPartId == item.ProgramPartId).FirstOrDefault();
                            decimal amount = 0;

                            if (schedulePart != null)
                            {
                                amount = scheduleParts.Where(p => p.PartId == schedulePart.ProgramPartId).FirstOrDefault().Amount;
                                result += Math.Abs(customeAmount);

                                schedulePart.PartAmount -= Math.Abs(customeAmount);
                            }
                        }
                    }
                }
                else
                {
                    foreach (var item in DeletedinstallmentsOrParts)
                    {
                        var customeAmount = discount.Amount / orderItemScheduleParts.Count();

                        var schedulePart = orderItemScheduleParts.Where(i => i.ProgramPartId == item.ProgramPartId).FirstOrDefault();
                        decimal amount = 0;

                        if (schedulePart != null)
                        {
                            amount = scheduleParts.Where(p => p.PartId == schedulePart.ProgramPartId).FirstOrDefault().Amount;
                            result += customeAmount;

                            schedulePart.PartAmount -= Math.Abs(customeAmount);
                        }
                    }
                }
            }

            return result;
        }

        private List<OrderChargeDiscount> UpdateOrderChargeDiscount(OrderItem orderItem, List<InstallmentandScheduleItemViewModel> installments, int installmentCount)
        {
            var listDiscounts = new List<OrderChargeDiscount>();

            decimal discountAmount = 0;
            decimal couponAmount = 0;
            decimal customAmount = 0;
            var orderParts = new List<OrderItemSchedulePartModel>();
            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var part in orderItem.OrderItemScheduleParts)
                {
                    orderParts.Add(new OrderItemSchedulePartModel
                    {
                        Amount = part.PartAmount,
                        PartId = part.ProgramPartId.Value
                    });
                }
            }

            var application = orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee).FirstOrDefault();

            foreach (var item in orderItem.GetOrderChargeDiscounts().Where(c => c.Category != ChargeDiscountCategory.EntryFee).ToList())
            {
                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                {
                    decimal memberSurCharge = 0;
                    decimal partnerSurCharge = 0;
                    decimal customCharge = 0;
                    decimal customDiscount = 0;

                    if (item.Subcategory == ChargeDiscountSubcategory.Discount && item.DiscountId.HasValue)
                    {
                        discountAmount = item.Amount / installmentCount;
                        item.Amount -= (discountAmount * installments.Count);
                    }
                    if (item.Subcategory == ChargeDiscountSubcategory.Discount && item.CouponId.HasValue)
                    {
                        if (item.Coupon != null)
                        {
                            if (item.Coupon.CouponCalculateType == CalculationType.TotalAmount && application != null)
                            {
                                couponAmount = item.Amount / (installmentCount + 1);
                            }
                            else
                            {
                                couponAmount = item.Amount / installmentCount;
                            }
                        }
                        else
                        {
                            couponAmount = item.Amount / installmentCount;
                        }

                        item.Amount -= (couponAmount * installments.Count);
                    }
                    if (item.Category == ChargeDiscountCategory.Surcharge)
                    {
                        memberSurCharge = _orderItemBusiness.CalculateOneSurCharge(orderItem, item);
                        item.Amount -= (memberSurCharge * installments.Count);
                    }
                    if (item.Category == ChargeDiscountCategory.PartnerSurcharge)
                    {
                        partnerSurCharge = _orderItemBusiness.CalculateOneSurCharge(orderItem, item);
                        item.Amount -= (partnerSurCharge * installments.Count);
                    }
                    if (item.Category == ChargeDiscountCategory.CustomCharge)
                    {
                        customCharge = item.Amount / installmentCount;
                        item.Amount -= (customCharge * installments.Count);
                    }
                    if (item.Category == ChargeDiscountCategory.CustomDiscount)
                    {
                        customDiscount = item.Amount / installmentCount;
                        item.Amount -= (customDiscount * installments.Count);
                    }

                    listDiscounts.Add(item);
                }
                else
                {
                    if (item.Subcategory == ChargeDiscountSubcategory.Discount && item.DiscountId.HasValue)
                    {
                        if (item.Discount.AmountType == ChargeDiscountType.Percent)
                        {
                            discountAmount = calculateDiscountAmount(orderItem, item, installments, orderParts);
                            item.Amount += (discountAmount);
                        }
                        else
                        {
                            discountAmount = item.Amount / installmentCount;
                            item.Amount -= (discountAmount * installments.Count);
                        }
                    }
                    if (item.Subcategory == ChargeDiscountSubcategory.Discount && item.CouponId.HasValue)
                    {
                        if (item.Coupon != null)
                        {
                            if (item.Coupon.CouponCalculateType == CalculationType.TotalAmount && application != null)
                            {
                                if (item.Coupon.AmounType == ChargeDiscountType.Percent)
                                {
                                    couponAmount = calculateDiscountAmount(orderItem, item, installments, orderParts);
                                    item.Amount += (couponAmount);
                                }
                                else
                                {
                                    couponAmount = item.Amount / (installmentCount + 1);
                                    item.Amount -= (couponAmount * installments.Count);
                                }
                            }
                            else
                            {
                                if (item.Coupon.AmounType == ChargeDiscountType.Percent)
                                {
                                    couponAmount = calculateDiscountAmount(orderItem, item, installments, orderParts);
                                    item.Amount += (couponAmount);
                                }
                                else
                                {
                                    couponAmount = item.Amount / installmentCount;
                                    item.Amount -= (couponAmount * installments.Count);
                                }
                            }
                        }
                    }
                    if (item.Category == ChargeDiscountCategory.Surcharge || item.Category == ChargeDiscountCategory.PartnerSurcharge)
                    {
                        decimal amount = 0;
                        foreach (var inst in installments)
                        {
                            var mainAmount = orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare ? orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == inst.ProgramPartId).FirstOrDefault().PartAmount : 0;
                            amount += _orderItemBusiness.CalculateOneSurCharge(orderItem, item, mainAmount);
                        }

                        item.Amount -= amount;
                    }
                    if (item.Category == ChargeDiscountCategory.CustomCharge || item.Category == ChargeDiscountCategory.CustomDiscount)
                    {
                        customAmount = item.Amount / installmentCount;
                        item.Amount -= (customAmount * installments.Count);
                    }

                    listDiscounts.Add(item);
                }
            }
            return listDiscounts;
        }

        private void UpdateItemOrderChargeDiscount(OrderItem orderItem, List<OrderInstallment> installmentCount, List<OrderItemSchedulePartModel> partList)
        {
            var Allinstallments = orderItem.Installments.Where(i => !i.IsDeleted).ToList();
            var installmentWithoutAppFee = Allinstallments.Where(i => i.Type != InstallmentType.AddOn).ToList();
            var applicationFee = orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee).ToList();
            decimal discountAmount = 0;
            decimal surCharge = 0;
            decimal percentAmount = 0;

            var reminingInstallmentForOldOrderItem = orderItem.Installments.Where(i => !(!i.IsDeleted && installmentCount.Select(s => s.Id).ToList().Contains(i.Id)));

            var oldOrderPart = new List<OrderItemSchedulePartModel>();
            foreach (var item in installmentCount)
            {
                oldOrderPart.Add(new OrderItemSchedulePartModel
                {
                    Amount = orderItem.OrderItemScheduleParts.Where(p => p.ProgramPartId == item.ProgramSchedulePartId).FirstOrDefault().PartAmount,
                    PartId = item.ProgramSchedulePartId.Value
                });
            }

            //update orderChargediscount
            foreach (var item in orderItem.GetOrderChargeDiscounts().OrderByDescending(c => c.Subcategory))
            {
                if (item.Category == ChargeDiscountCategory.EntryFee)
                {
                    foreach (var oldInst in installmentCount)
                    {
                        decimal mainamount = 0;
                        mainamount = oldOrderPart.Where(p => p.PartId == oldInst.ProgramSchedulePartId).FirstOrDefault().Amount;

                        item.Amount -= (mainamount);
                        orderItem.EntryFee -= (mainamount);
                    }

                }
                if (item.Subcategory == ChargeDiscountSubcategory.Discount && (item.DiscountId.HasValue || item.CouponId.HasValue))
                {
                    if (item.CouponId.HasValue)
                    {
                        if (item.Coupon.AmounType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                        {
                            decimal amount = 0;
                            foreach (var inst in installmentCount)
                            {
                                var partItem = orderItem.OrderItemScheduleParts.Where(p => p.ProgramPartId == inst.ProgramSchedulePartId).FirstOrDefault();
                                var mainAmount = oldOrderPart.Where(p => p.PartId == inst.ProgramSchedulePartId).FirstOrDefault().Amount;

                                percentAmount = (item.Coupon.Amount * mainAmount) / 100;
                                amount += (percentAmount);
                                partItem.PartAmount -= (item.Coupon.Amount * mainAmount) / 100;

                            }

                            item.Amount += amount;
                        }
                        else
                        {
                            if (item.Coupon.CouponCalculateType == CalculationType.TotalAmount && applicationFee.Count > 0)
                            {
                                decimal appAmount = 0;
                                foreach (var app in applicationFee)
                                {
                                    var percentApp = (item.Coupon.Amount * app.Amount) / 100;
                                    appAmount += percentApp;
                                }

                                var couponWithoutApp = Math.Abs(item.Amount) - appAmount;
                                discountAmount = couponWithoutApp / (installmentCount.Count);
                                item.Amount += (discountAmount * installmentCount.Count);
                            }
                            else
                            {
                                discountAmount = item.Amount / (installmentWithoutAppFee.Count);
                                item.Amount -= (discountAmount * installmentCount.Count);
                            }

                        }
                    }
                    else
                    {
                        if (item.Discount.AmountType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                        {
                            decimal amount = 0;
                            foreach (var inst in installmentCount)
                            {
                                var partItem = orderItem.OrderItemScheduleParts.Where(p => p.ProgramPartId == inst.ProgramSchedulePartId).FirstOrDefault();
                                var mainAmount = oldOrderPart.Where(p => p.PartId == inst.ProgramSchedulePartId).FirstOrDefault().Amount;

                                percentAmount = (item.Discount.Amount * mainAmount) / 100;
                                amount += (percentAmount);
                                partItem.PartAmount -= (item.Discount.Amount * mainAmount) / 100;
                            }

                            item.Amount += amount;
                        }
                        else
                        {
                            discountAmount = item.Amount / installmentWithoutAppFee.Count;
                            item.Amount -= (discountAmount * installmentCount.Count);
                        }
                    }
                }
                if (item.Category == ChargeDiscountCategory.Surcharge || item.Category == ChargeDiscountCategory.PartnerSurcharge)
                {
                    if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                    {
                        surCharge = _orderItemBusiness.CalculateOneSurCharge(orderItem, item);
                        item.Amount -= (surCharge * installmentCount.Count);
                    }
                    else
                    {
                        decimal totalSurCharge = 0;
                        foreach (var inst in installmentCount)
                        {
                            var mainAmount = orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == inst.ProgramSchedulePartId).FirstOrDefault().PartAmount;
                            surCharge = _orderItemBusiness.CalculateOneSurCharge(orderItem, item, mainAmount);
                            totalSurCharge += surCharge;
                        }

                        item.Amount -= totalSurCharge;
                    }
                }
            }
        }
        private void ConfirmCancelFullPaidItem(OrderItem orderItem, CancelSubscriptionOrderViewModel model)
        {
            model.ShowCancellationFeeBox = model.CancellationFee > 0 ? true : false;
            model.PaidAmount = orderItem.PaidAmount;

            if (model.EffectiveDate.Value <= model.DesiredStartDate)
            {
                if (model.CancellationFee > orderItem.PaidAmount)
                {
                    model.NewBalance = model.CancellationFee - orderItem.PaidAmount;
                }
                else
                {
                    model.RefundFee = orderItem.PaidAmount - model.CancellationFee;
                }

                foreach (var item in model.SourceCharges)
                {
                    model.UpdatedChargeDiscounts.Add(new ChargeDiscountItemViewModel
                    {
                        Amount = 0,
                        Category = item.Category,
                        Name = item.Name,
                        Id = item.Id,
                        SubCategory = item.SubCategory
                    });
                }

                foreach (var item in model.SourceDiscounts)
                {
                    model.UpdatedChargeDiscounts.Add(new ChargeDiscountItemViewModel
                    {
                        Amount = 0,
                        Category = item.Category,
                        Name = item.Name,
                        Id = item.Id,
                        SubCategory = item.SubCategory
                    });
                }

                if (model.CancellationFee != 0)
                {
                    model.UpdatedChargeDiscounts.Add(
                        new ChargeDiscountItemViewModel
                        {
                            Amount = model.CancellationFee,
                            Category = ChargeDiscountCategory.CancellationFee,
                            Name = Constants.W_Cancel_Fee,
                        }
                    );
                }

                model.EntryFee = 0;
                model.IsEffectiveDateBeforeDesired = true;
                model.NewTotalAmount = model.UpdatedChargeDiscounts.Sum(p => (decimal?)p.Amount) ?? 0;
                model.UpdatedCharges.AddRange(model.UpdatedChargeDiscounts.Where(d => d.SubCategory == ChargeDiscountSubcategory.Charge));
                model.UpdatedDiscounts.AddRange(model.UpdatedChargeDiscounts.Where(d => d.SubCategory == ChargeDiscountSubcategory.Discount));
            }
            else
            {
                model.IsEffectiveDateBeforeDesired = false;
                var listUpdatedChargeDiscount = new List<OrderChargeDiscount>();

                foreach (var schedule in model.ItemSchedules)
                {
                    if (schedule.StartDate < model.EffectiveDate && schedule.EndDate >= model.EffectiveDate || model.EffectiveDate > schedule.EndDate)
                    {
                        model.PastItemSchedules.Add(schedule);
                    }
                    else
                    {
                        model.DeletedItemSchedules.Add(schedule);
                    }
                }

                foreach (var item in model.DeletedItemSchedules)
                {
                    decimal amount = 0;
                    var installment = orderItem.OrderItemScheduleParts.Where(i => !i.IsDeleted && i.ProgramPartId == item.ProgramPartId).FirstOrDefault();
                    amount = installment != null ? installment.PartAmount : 0;
                    model.EntryFee -= amount;
                }

                listUpdatedChargeDiscount = UpdateFullPaidOrderChargeDiscount(orderItem, model.DeletedItemSchedules, model.ItemSchedules.Count);

                foreach (var chargeDiscount in listUpdatedChargeDiscount)
                {
                    model.UpdatedChargeDiscounts.Add(
                       new ChargeDiscountItemViewModel
                       {
                           Amount = chargeDiscount.Amount,
                           Category = chargeDiscount.Category,
                           Name = chargeDiscount.Name,
                           Id = chargeDiscount.Id,
                           SubCategory = chargeDiscount.Subcategory
                       }
                   );
                }

                if (model.CancellationFee != 0)
                {
                    model.UpdatedChargeDiscounts.Add(
                        new ChargeDiscountItemViewModel
                        {
                            Amount = model.CancellationFee,
                            Category = ChargeDiscountCategory.CancellationFee,
                            Name = Constants.W_Cancel_Fee,
                            SubCategory = ChargeDiscountSubcategory.Charge
                        }
                    );
                }



                var totalAmount = model.EntryFee + model.UpdatedChargeDiscounts.Sum(p => (decimal?)p.Amount) ?? 0;
                model.NewTotalAmount = totalAmount;
                model.RefundFee = orderItem.PaidAmount - totalAmount;

                model.UpdatedCharges.AddRange(model.UpdatedChargeDiscounts.Where(d => d.SubCategory == ChargeDiscountSubcategory.Charge));
                model.UpdatedDiscounts.AddRange(model.UpdatedChargeDiscounts.Where(d => d.SubCategory == ChargeDiscountSubcategory.Discount));
            }
        }

        private void CancelFullPaidItem(OrderItem orderItem, CancelSubscriptionOrderViewModel model)
        {
            if (model.IsEffectiveDateBeforeDesired)
            {
                orderItem.GetOrderChargeDiscounts().ForEach(o => o.Amount = 0);
                orderItem.Order.OrderAmount -= orderItem.TotalAmount;
                orderItem.EntryFee = 0;
                orderItem.OrderItemScheduleParts.ForEach(i => i.IsDeleted = true);
            }
            else
            {
                foreach (var item in model.DeletedItemSchedules)
                {
                    orderItem.OrderItemScheduleParts.First(p => p.ProgramPartId == item.ProgramPartId).IsDeleted = true;
                }

                orderItem.EntryFee = model.EntryFee;

                foreach (var item in orderItem.GetOrderChargeDiscounts())
                {
                    if (item.Category == ChargeDiscountCategory.EntryFee)
                    {
                        item.Amount = model.EntryFee;
                    }
                    else
                    {
                        foreach (var updatedItem in model.UpdatedChargeDiscounts)
                        {
                            if (item.Id == updatedItem.Id)
                            {
                                item.Amount = updatedItem.Amount;
                                break;
                            }
                        }
                    }
                }
                //we should delete session after effective date
                model.EffectiveDate = model.EffectiveDate;
                var total = orderItem.TotalAmount;
                var totalAmount = orderItem.OrderChargeDiscounts.Sum(p => (decimal?)p.Amount) ?? 0;
                orderItem.Order.OrderAmount = (orderItem.Order.OrderAmount - total) + totalAmount;
            }
        }

        private List<OrderChargeDiscount> UpdateFullPaidOrderChargeDiscount(OrderItem orderItem, List<InstallmentandScheduleItemViewModel> DeletedScheduleParts, int countOrderItemScheduleParts)
        {
            var listChargeDiscounts = new List<OrderChargeDiscount>();

            decimal discountAmount = 0;
            decimal couponAmount = 0;
            decimal customAmount = 0;
            var orderScheduleParts = new List<OrderItemSchedulePartModel>();

            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                foreach (var part in orderItem.OrderItemScheduleParts.Where(p => !p.IsDeleted))
                {
                    orderScheduleParts.Add(new OrderItemSchedulePartModel
                    {
                        Amount = part.PartAmount,
                        PartId = part.ProgramPartId.Value
                    });
                }
            }

            var application = orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee).FirstOrDefault();

            foreach (var item in orderItem.GetOrderChargeDiscounts().Where(c => c.Category != ChargeDiscountCategory.EntryFee).ToList())
            {
                if (item.Subcategory == ChargeDiscountSubcategory.Discount && item.DiscountId.HasValue && item.Category != ChargeDiscountCategory.CustomDiscount)
                {
                    if (item.Discount.AmountType == ChargeDiscountType.Percent)
                    {
                        discountAmount = calculateDiscountAmount(orderItem, item, DeletedScheduleParts, orderScheduleParts);
                        item.Amount += (discountAmount);
                    }
                    else
                    {
                        discountAmount = item.Amount / countOrderItemScheduleParts;
                        item.Amount -= (discountAmount * DeletedScheduleParts.Count);
                    }
                }

                if (item.Subcategory == ChargeDiscountSubcategory.Discount && item.CouponId.HasValue)
                {
                    if (item.Coupon.CouponCalculateType == CalculationType.TotalAmount && application != null)
                    {
                        if (item.Coupon.AmounType == ChargeDiscountType.Percent)
                        {
                            couponAmount = calculateDiscountAmount(orderItem, item, DeletedScheduleParts, orderScheduleParts);
                            item.Amount += (couponAmount);
                        }
                        else
                        {
                            couponAmount = item.Amount / (countOrderItemScheduleParts + 1);
                            item.Amount -= (couponAmount * DeletedScheduleParts.Count);
                        }
                    }
                    else
                    {
                        if (item.Coupon.AmounType == ChargeDiscountType.Percent)
                        {
                            couponAmount = calculateDiscountAmount(orderItem, item, DeletedScheduleParts, orderScheduleParts);
                            item.Amount += (couponAmount);
                        }
                        else
                        {
                            couponAmount = item.Amount / countOrderItemScheduleParts;
                            item.Amount -= (couponAmount * DeletedScheduleParts.Count);
                        }
                    }
                }

                if (item.Subcategory == ChargeDiscountSubcategory.Discount && item.Category == ChargeDiscountCategory.FullPaySubscription)
                {
                    couponAmount = calculateDiscountAmount(orderItem, item, DeletedScheduleParts, orderScheduleParts);
                    item.Amount += (couponAmount);
                }

                if (item.Category == ChargeDiscountCategory.CustomCharge || (item.Category == ChargeDiscountCategory.CustomDiscount && !item.DiscountId.HasValue))
                {

                    customAmount = calculateDiscountAmount(orderItem, item, DeletedScheduleParts, orderScheduleParts);
                    item.Amount -= (customAmount * DeletedScheduleParts.Count);
                }

                if (item.Category == ChargeDiscountCategory.CustomDiscount && item.DiscountId.HasValue)
                {
                    customAmount = calculateDiscountAmount(orderItem, item, DeletedScheduleParts, orderScheduleParts);
                    item.Amount += (customAmount * DeletedScheduleParts.Count);
                }

                if (item.Category == ChargeDiscountCategory.Surcharge || item.Category == ChargeDiscountCategory.PartnerSurcharge)
                {
                    decimal amount = 0;
                    foreach (var inst in DeletedScheduleParts)
                    {
                        var mainAmount = orderItem.OrderItemScheduleParts.Where(o => !o.IsDeleted && o.ProgramPartId == inst.ProgramPartId).FirstOrDefault().PartAmount;
                        amount += _orderItemBusiness.CalculateOneSurCharge(orderItem, item, mainAmount);
                    }

                    item.Amount -= amount;
                }

                listChargeDiscounts.Add(item);
            }

            return listChargeDiscounts;
        }

        private void SetPlayerInfo(PlayerProfile player, JbForm formData)
        {

            var hasAllergeInForm = Ioc.JbFormBusiness.GetFormElement(formData, ElementsName.HasAllergiesInfo.ToString());
            bool? hasAllergy = null;
            var allergyDesCription = string.Empty;

            if (hasAllergeInForm != null && hasAllergeInForm is JbDropDown)
            {
                hasAllergy = Ioc.FormBusiness.GetPlayerHasAllergeis(formData);
                player.Info.HasAllergies = hasAllergy;
            }

            var allergyDesCriptionElement = Ioc.JbFormBusiness.GetFormElement(formData, ElementsName.AllergiesInfo.ToString());
            if (allergyDesCriptionElement != null && allergyDesCriptionElement is JbTextArea)
            {
                allergyDesCription = Ioc.FormBusiness.GetPlayerAllergies(formData);
                player.Info.Allergies = allergyDesCription;
            }

            var specialNeedsElement = Ioc.JbFormBusiness.GetFormElement(formData, ElementsName.HasMedicalConditions.ToString());
            bool? hasSpecialNeeds = null;
            var specialNeedsDescription = string.Empty;

            if (specialNeedsElement != null && specialNeedsElement is JbDropDown)
            {
                hasSpecialNeeds = Ioc.FormBusiness.GetPlayerHasSpecialNeeds(formData);
                player.Info.HasSpecialNeeds = hasSpecialNeeds;
            }

            var specialNeedsDescriptionElement = Ioc.JbFormBusiness.GetFormElement(formData, ElementsName.MedicalConditions.ToString());
            if (specialNeedsDescriptionElement != null && specialNeedsDescriptionElement is JbTextArea)
            {
                specialNeedsDescription = Ioc.FormBusiness.GetPlayerMedicalConditions(formData);
                player.Info.SpecialNeeds = specialNeedsDescription;
            }


            var selfAdministerMedicationElement = Ioc.JbFormBusiness.GetFormElement(formData, ElementsName.SelfAdministerMedication.ToString());
            bool? selfAdministerMedication = null;
            var selfAdministerMedicationDescription = string.Empty;

            if (selfAdministerMedicationElement != null && selfAdministerMedicationElement is JbDropDown)
            {
                selfAdministerMedication = Ioc.FormBusiness.GetPlayerSelfAdministerMedication(formData);
                player.Info.SelfAdministerMedication = selfAdministerMedication;
            }

            var selfAdministerMedicationDescriptionElement = Ioc.JbFormBusiness.GetFormElement(formData, ElementsName.SelfAdministerMedicationDescription.ToString());
            if (selfAdministerMedicationDescriptionElement != null && selfAdministerMedicationDescriptionElement is JbTextArea)
            {
                selfAdministerMedicationDescription = Ioc.FormBusiness.GetPlayerSelfAdministerMedicationDescription(formData);
                player.Info.SelfAdministerMedicationDescription = selfAdministerMedicationDescription;
            }


            var nutAllergyElement = Ioc.JbFormBusiness.GetFormElement(formData, ElementsName.NutAllergy.ToString());
            bool? nutAllergy = null;
            var nutAllergyDescription = string.Empty;

            if (nutAllergyElement != null && nutAllergyElement is JbDropDown)
            {
                nutAllergy = Ioc.FormBusiness.GetPlayerNutAllergy(formData);
                player.Info.NutAllergy = nutAllergy;

            }

            var nutAllergyDescriptionElement = Ioc.JbFormBusiness.GetFormElement(formData, ElementsName.NutAllergyDescription.ToString());
            if (nutAllergyDescriptionElement != null && nutAllergyDescriptionElement is JbTextArea)
            {
                nutAllergyDescription = Ioc.FormBusiness.GetPlayerNutAllergyDescription(formData);
                player.Info.NutAllergyDescription = nutAllergyDescription;
            }
        }

        public virtual JsonNetResult ValidateRefund(OrderItemsViewModel model)
        {
            var automaticallyBalance = 0m;
            var paymentAutomatically = new List<TransactionActivity>();

            if (model.Installments != null)
            {
                var installment = _orderInstallmentBusiness.GetInstallmentToBeRefund(model);
                paymentAutomatically = _transactionActivityBusiness.GetPaymentTransactions(installment).ToList();
                automaticallyBalance = _orderInstallmentBusiness.GetAmountAutomaticallyTransactions(installment);
            }
            else
            {
                var orderItem = _orderItemBusiness.GetItem(model.Id);
                paymentAutomatically = _transactionActivityBusiness.GetPaymentTransactions(orderItem).ToList();
                automaticallyBalance = _orderItemBusiness.GetAmountAutomaticallyTransactions(orderItem);
                var orderItemBalance = -1 * (orderItem.TotalAmount - orderItem.PaidAmount);

                if (automaticallyBalance > orderItemBalance)
                    automaticallyBalance = orderItemBalance;
            }

            paymentAutomatically = paymentAutomatically.Where(t => t.TransactionDate.AddDays(Constants.Refund_Period) > DateTime.UtcNow).ToList();

            for (var j = 0; j < model.RefundModes.Count; j++)
            {
                if (model.RefundModes[j].IsSelected && model.RefundModes[j].Amount.HasValue)
                {
                    if (model.RefundModes[j].Mode == RefundMode.Automatically)
                    {
                        if (model.RefundModes[j].Amount > Math.Abs(automaticallyBalance))
                        {
                            var errorMessage = string.Format(Constants.F_InvalidAmountForRefund, CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(automaticallyBalance), ActiveClub.Currency));
                            ModelState.AddModelError($"model.RefundModes[{j}].Amount", errorMessage);

                        }

                        if (!paymentAutomatically.Any())
                        {
                            var errorMessage = string.Format(Constants.F_RefundOrder_NotSupported_AfterRefundPeriod, Constants.Refund_Period);
                            ModelState.AddModelError($"model.RefundModes[{j}].Amount", errorMessage);

                        }
                    }
                }
                else if (model.RefundModes[j].IsSelected)
                {
                    ModelState.AddModelError($"model.RefundModes[{j}].Amount", RefundAmountRequiredMessage);
                }
            }

            return ModelState.IsValid ? JsonNet(true) : JsonNet(false);
        }

        public ActionResult UncancelOrder(int orderItemId)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var result = _orderItemBusiness.UncancelOrder(orderItem, this.GetCurrentUserId());

            return JsonNet(result);
        }
        [HttpGet]
        public virtual JsonNetResult GetUndoRefundToOrder(long orderItemId, long transacTionId)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            try
            {
                var model = _orderItemBusiness.GetUndoFamilyCreditViewModel(orderItem, transacTionId);

                Log.Warning("{TransactionId} undo family credit", transacTionId);

                return JsonNet(model);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "undo family credit {TransactionId}", transacTionId);
                return JsonNet(null);
            }
        }
        [HttpPost]
        public virtual ActionResult ValidateUndoFamilyCredit(UndoFamilyCreditToOrderViewModel model)
        {
            if (!model.ReturnedAmount.HasValue)
            {
                ModelState.AddModelError("model.ReturnedAmount", "Amount is required.");
            }
            else
            {
                if (model.ReturnedAmount > model.FamilyCredit)
                {
                    ModelState.AddModelError("model.ReturnedAmount", "Amount should be less than or equal than credit");
                }
                else if (model.ReturnedAmount > model.MaximumAmount)
                {
                    ModelState.AddModelError("model.ReturnedAmount", "Amount should be less than or equal transaction amount");
                }
            }

            if (!ModelState.IsValid) return JsonNet(false);

            return Json(new JResult { Status = true });
        }

        [HttpPost]
        public ActionResult SubmitUndoRefundToOrder(UndoFamilyCreditToOrderViewModel model)
        {
            var orderItem = _orderItemBusiness.GetItem(model.orderItemId);
            CheckResourceForAccess(orderItem);

            var result = _orderItemBusiness.UndoFamilyRefundToOrder(orderItem, model, this.GetCurrentUserId());

            return JsonNet(result);
        }

        [HttpPost]
        public ActionResult CompleteDraftOrder(long orderItemId)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var result = _orderItemBusiness.CompleteDraftItem(orderItemId, this.GetCurrentUserId());

            if (result.Status)
                EmailService.Get(this.ControllerContext).SendReservationEmail(orderItem.Order.ConfirmationId, null, Request.Url.Host, null, ReservationEmailMode.Normal, orderItem.Order.OrderMode);

            return JsonNet(result);
        }


        private void SetChessTournamentAdditionalInformation(OrderItem orderItem, OrderItemsViewModel model)
        {
            var chessOrderItemInformation = orderItem.OrderItemChess;
            model.ChessTournamentAtttribute = orderItem.ProgramSchedule.Program.ProgramSchedules.FirstOrDefault().Attributes as TournamentScheduleAttribute;
            model.SectionName = orderItem.OrderItemChess.Section;
            model.ChessScheduleName = orderItem.OrderItemChess.Schedule;

            if (model.ChessTournamentAtttribute != null)
            {
                int id = 0;
                foreach (var schedule in model.ChessTournamentAtttribute.Schedules)
                {
                    schedule.Title = schedule.GetSchFullName();
                    schedule.Id = id;

                    id++;
                }

                if (!string.IsNullOrWhiteSpace(model.SectionName) && model.ChessTournamentAtttribute.Sections != null && model.ChessTournamentAtttribute.Sections.Any(s => s.Name == model.SectionName))
                {
                    model.ChessTournamentAtttribute.Sections.First(s => s.Name == orderItem.OrderItemChess.Section).IsChecked = true;
                }

                if (!string.IsNullOrWhiteSpace(model.ChessScheduleName) && model.ChessTournamentAtttribute.Schedules != null && model.ChessTournamentAtttribute.Schedules.Any(s => s.GetSchName() == model.ChessScheduleName))
                {
                    model.ChessTournamentAtttribute.Schedules.First(s => s.GetSchName() == orderItem.OrderItemChess.Schedule).IsChecked = true;
                }
            }
        }

        [HttpPost]
        public virtual JsonNetResult EditChessTournament(long orderItemId, string chessInfo)
        {
            var orderItem = _orderItemBusiness.GetItem(orderItemId);
            var chessUpdatedInfo = JsonConvert.DeserializeObject<TournamentScheduleAttribute>(chessInfo);
            var result = new OperationStatus();

            if (chessUpdatedInfo.Sections.Any(s => s.IsChecked) && chessUpdatedInfo.Schedules.Any(s => s.IsChecked))
            {
                orderItem.OrderItemChess.Section = chessUpdatedInfo.Sections.First(s => s.IsChecked).Name;
                orderItem.OrderItemChess.Schedule = chessUpdatedInfo.Schedules.First(s => s.IsChecked).GetSchName();

                result = _orderItemBusiness.Update(orderItem);

                return JsonNet(result);
            }

            result.Status = false;
            result.Message = "You should choose section and schedule";
            return JsonNet(result);
        }
    }
}