﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Model.Email
{
    public class ResetPasswordSuccessBodyModel
    {
        public string HomeTitle { get; set; }

        public string HomePageUrl { get; set; }

        public string UserFullName { get; set; }

        public string LogoUrl { get; set; }

        public string ResetLink { get; set; }

        public RequestType RequestType { get; set; }
    }
}
