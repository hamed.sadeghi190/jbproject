

INSERT INTO [dbo].[ClientPaymentMethods]
         ([Id]
           ,[EnableStripePayment]
           ,[IsStripeTestMode]
          	,[StripeCustomerId] 
			,[StripeAccessToken]
			,[StripeResponse] 
           ,[EnablePaypalPayment]
           ,[IsPaypalTestMode]
           ,[PaypalEmail]
           ,[EnableCashPayment]
           ,[CashPaymentMessage]
           ,[CashPaymentInstruction]
           ,[EnableManualPayment]
           ,[ManualPaymentName]
           ,[ManualPaymentMessage]
           ,[ManualPaymentInstruction]
		   ,[EnableBankTransferPayment]
		   ,[BankTransferPaymentMessage]
		   ,[BankTransferPaymentInstruction])
     Select Id,CAST(
             CASE 
                  WHEN [StripeCustomerId] is null or [StripeCustomerId] = '' 
                     THEN 0 
                  ELSE 1 
             END AS bit) as [EnableStripePayment],IsStripeTestMode,
			 StripeCustomerId ,StripeAccessToken,StripeToken,
			 
			  CAST(

             CASE 
                  WHEN [PaypalEmail] is null or [PaypalEmail] = '' 
                     THEN 0 
                  ELSE 1 
             END AS bit) as [EnablePaypalPayment],IsSandBoxMode,PaypalEmail,EnableCashPayment,'','',0,'','','',0,'','' from Clients
          
GO


