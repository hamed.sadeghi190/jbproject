﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('changeDesiredStartDateController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }
        $scope.baseUrl = window.location.origin + "/Dashboard/Order/";
        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.orderItemId = $stateParams.orderId;

        $scope.Model = {};

        $http.get($scope.baseUrl + '/EditSubscriptionDesiredStartDate/?orderItemId=' + $stateParams.orderId).success(function (data, status, headers, config) {

            if (data.Status == false) {

                jbToast.error("We have encountered an internal error and cannot submit your changes. Please contact Jumbula support (support@jumbula.com) and provide a screenshot of this page.")

            } else {
                $scope.Model = data;

            }
    
        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.Save = function () {
            $http.post($scope.baseUrl + '/EditSubscriptionDesiredStartDate', { model: $scope.Model })
             .success(function (data, status, headers, config) {
                 if (data.Status) {
                     console.log(data.Message)
                     $scope.errors = null;
                     jbToast.success(data.Message);
                     $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderId })
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

    }]);
})();