﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ContactUsFormViewModel
    {
        [Required(ErrorMessage = "The Describe idea field is required.")]
        [DisplayName("Description")]
        public string Description { get; set; }

        [DisplayName("Subject")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "The Email address field is required.")]
        [EmailAddress(ErrorMessage = "The email must be a valid e-mail address.")]
        [DisplayName("What is your email address?")]
        public string Email { get; set; }

        [DisplayName("What is your name?")]
        public string Name { get; set; }
    }
}