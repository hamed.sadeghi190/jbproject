﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CapacityReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;

        $scope.filterData = function () {
            $scope.capacityReportGridOptions.dataSource.read();
        }

        $scope.capacityReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/CapacityReportRun',
                        type: "POST",
                        data: function () {
                            return {
                                model: model,
                            };
                        }
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            batch: true,
        });

        $scope.capacityReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "capacity (" + $scope.Model.SeasonDomain + ").xlsx",
                proxyURL: "/Dashboard/CustomReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.capacityReportsDataSource,
            //groupable: true,
            sortable: true,
            scrollable: true,
            resizable: true,
            //height: "600px",
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            dataBound: function () {
                var grid = this;
                $(".progress").each(function () {
                    var row = $(this).closest("tr");
                    var model = grid.dataItem(row);
                    $(this).kendoProgressBar({
                        type: "value",
                        animation: {
                            duration: 500
                        },
                        value: model.Filled,
                        max: (model.Capacity > 0) ? model.Capacity : (model.Filled * 2)
                    });
                });

                customReportService.initializeDoubleScroll();
            },
            columnMenu: true,
            columns: [
                {
                    field: "ProgramName",
                    title: "Program",
                    width: "150px",
                    locked: true,
                    lockable: true,
                },
                {
                    field: "Status",
                    title: "Status",
                    lockable: true,
                    width: "110px"
                },
                {
                    field: "MinimumEnrollment",
                    title: "Min capacity",
                    lockable: true,
                    width: "140px"
                },
                {
                    field: "Capacity",
                    title: "Max capacity",
                    lockable: true,
                    width: "140px"
                },
                {
                    field: "Filled",
                    title: "Registered",
                    lockable: true,
                    width: "120px"
                },
                {
                    field: "EnableWaitList",
                    title: "Waitlisted",
                    lockable: true,
                    width: "160px"
                },
                {
                    field: "WaitList",
                    title: "Waitlist #",
                    lockable: true,
                    width: "120px"
                },
                {
                    field: "Open",
                    title: "Remaining",
                    lockable: true,
                    width: "130px"
                },
                {
                    field: "Schedule",
                    title: "Scheduled date",
                    lockable: true,
                    width: "160px"
                },
                {
                    field: "DayOfWeek",
                    title: "Days",
                    lockable: true,
                    width: "160px"
                },
                {
                    template: "#if(data.Capacity == 'Unlimited') {#<div class='progress' style='width: 100%;text-align:right; padding-right:3px'; padding-top: 4px;> ∞ </div>#} " +
                                           "else {# <div class='progress' style='width: 100%;text-align:right; padding-right:3px; padding-top: 4px;'> #=Capacity# </div> #}#",
                    filed: "Filled",
                    lockable: true,
                    width: "150px"
                    
                },
                {
                    field: "",
                    title: "Actions",
                    width: "70px",
                    hidden:true,
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;position:absolute;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''  >\
                       <li> ... \
                           <ul>\
                                     <li  class='k-item k-state-default k-first k-last' role='menuitem' style='\"z-index:auto;\"' >\
                                           <span class='k-link jbi-angle-right'>Waitlist</span>\
                                     </li>\
                              \
                           </ul>\
                       </li>\
                   </ul>",
                },

            ],
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#capacityReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.shareReport = function () {
            if (is.undefined($scope.Model.Filters)) {
                $scope.Model.Filters = {};
                $scope.Model.Filters.Term = {};
            }
            $scope.Model.Filters.Term = "";
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "Capacity" });
        }

    }]);
})();