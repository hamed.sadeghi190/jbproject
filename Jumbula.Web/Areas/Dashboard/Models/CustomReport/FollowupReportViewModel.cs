﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text;


namespace SportsClub.Areas.Dashboard.Models
{
    public class FollowupReportViewModel
    {
        public string ProgramName { get; set; }

        public string ClubName { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public List<UserFollowupForm> FollowupForms { get; set; }

        public string RegDate { get; set; }

        public string ConfirmationId { get; set; }

        public long OrderItemId { get; set; }

        public long OrderId { get; set; }

        public int ItemStatusReason { get; set; }

        public string ToFollowupFromDetails()
        {
            var formDetails = string.Empty;

            foreach (var form in FollowupForms)
            {
                var name = form.FormName;
                var status = form.Status;
                formDetails += string.Format("{0} {1}, ", name, status);
            }

            formDetails = (formDetails.Length > 0)
                ? formDetails.Remove(formDetails.Length - 2)
                : formDetails;

            return formDetails;
        }

    }

    public enum FollowupFormFilter
    {
        None = 0,
        Completed = 1,
        Uncompleted = 2
    }

    public class UserFollowupForm
    {
        public string FormName { get; set; }

        public string Status { get; set; }

        public int FormId { get; set; }
    }

}