﻿
using System.Collections.Generic;
using Jumbula.Common.Enums;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models.Email
{
    public class EmailSubscriptionOrderViewModel
    {
        public EmailSubscriptionOrderViewModel()
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
            ClubUrl = RootSiteUrl + "/" + ClubDomain;
        }

        public string MemberName { get; set; }
        public string PayPalInfo { get; set; }
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string AppliedText { get; set; }
        public string ClubImageUrl { get; set; }
        public string ClubName { get; set; }
        public string ClubUrl { get; set; }
        public string ClubEmail { get; set; }
        public string ClubPhone { get; set; }
        public CurrencyCodes ClubCurrency { get; set; }
        public string InstallmentDate { get; set; }
        public string RootSiteUrl { get; set; }
        public string BodyMailTitle { get; set; }
        public string ClubLogo { get; set; }
        public bool HasPartner { get; set; }
        public List<EmailDropInSessionsViewModel> ReservationItems { get; set; }
        public string ConfirmationId { get; set; }
        public string Balance { get; set; }
        public string PaidAmount { get; set; }
        public bool isFail { get; set; }
        public string PlayerFullName { get; set; }
        public decimal EntryFee { get; set; }
        public string TotalAmount { get; set; }
        public bool HasSessions { get; set; }
        public List<ReservationItemDetail> OrderCharges { get; set; }
        public List<ReservationItemDetail> OrderDiscounts { get; set; }
    }
    public class EmailDropInSessionsViewModel
    {
        public EmailDropInSessionsViewModel()
        {

        }

        public EmailDropInSessionsViewModel(CurrencyCodes currency)
        {
            Currency = currency;
        }
        public CurrencyCodes Currency { get; set; }

        public string Date { get; set; }
        public string NewDate { get; set; }
        public string Status { get; set; }
        public string Amount { get; set; }

    }
}