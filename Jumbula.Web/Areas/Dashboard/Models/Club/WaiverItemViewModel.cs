﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class WaiverItemViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Text { get; set; }
    }
}