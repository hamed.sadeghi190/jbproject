﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.WebLogic.UserService;

namespace SportsClub.Models.Register
{
    public class RegisterProgramScheduleModel : BaseViewModel<ProgramSchedule, long>
    {
        public string Title
        {
            get;
            set;
        }

        [Display(Name = "Start date")]
        public DateTime StartDate { get; set; }
        [Display(Name = "End date")]
        public DateTime EndDate { get; set; }
        public bool IsProrate { get; set; }
        public List<RegisterChargeDiscountModel> Charges { get; set; }
        //public decimal SessionPrice { get; set; }
        public RegistrationType RegistrationType { get; set; }
        public ProgramRegStatus RegStatus { get; set; }
        public ProgramRunningStatus ProgramRunningStatus { get; set; }

        public List<ScheduleSession> ScheduleSections { get; set; }
        public int SelectedSection { get; set; }
        public bool Selected { get; set; }

        public int MinAge { get; set; }

        public int MaxAge { get; set; }
        public int Capacity { get; set; }
        public bool ShowCapacityLeft { get; set; }

        public bool IsFull { get; set; }
        public int ShowCapacityLeftNumber { get; set; }
        public int TotalNumberOfRegistrants
        {
            get { return Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee).Sum(c => c.NumberOfRegistration); }
        }
        public int TotalCapacityLeft
        {
            get
            {
                if (Capacity > 0)
                {
                    return (Capacity - TotalNumberOfRegistrants) > 0 ? (Capacity - TotalNumberOfRegistrants) : 0;
                }

                return -1;
            }
        }
        public bool IsRestricted { get; set; }
        public bool NotStarted { get; set; }
        public bool IsExpire { get; set; }
        public List<string> RestrictionMessages { get; set; }
        public string Color { get; set; }
        public bool EnabledWaitList { get; set; }

        public bool EnabledDropIn { get; set; }

        public bool IsTestMode { get; set; }

        public string DropInLabel { get; set; }
        public RegisterProgramScheduleModel()
        {
            Charges = new List<RegisterChargeDiscountModel>();
        }

        public RegisterProgramScheduleModel(ProgramSchedule schedule, Jumbula.Common.Enums .TimeZone? ClubTimeZone)
            : this()
        {
            if (schedule.Program.TypeCategory == ProgramTypeCategory.Calendar)
            {
                this.Title = schedule.Title + "(" + schedule.StartDate.ToShortDateString() + " - " + schedule.EndDate.ToShortDateString() + ")";
                Color = schedule.Charges.FirstOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee).Attributes.Color;
            }
            else
            {
                if (schedule.Program.TypeCategory == ProgramTypeCategory.Camp && !string.IsNullOrEmpty(schedule.Title))
                {
                    this.Title = string.Format("{0} ({1} - {2})", schedule.Title, schedule.StartDate.ToShortDateString(), schedule.EndDate.ToShortDateString());
                }
                else
                {
                    this.Title = schedule.StartDate.ToShortDateString() + " - " + schedule.EndDate.ToShortDateString();
                }
            }

            this.StartDate = schedule.StartDate;
            this.EndDate = schedule.EndDate;
            this.Id = schedule.Id;
            this.NotStarted = JbUserService.GetCurrentUserRoleType() != RoleCategoryType.Dashboard && Ioc.ProgramBusiness.GetScheduleRegStatus(schedule) == ProgramRegStatus.NotStarted;
            this.IsExpire = JbUserService.GetCurrentUserRoleType() != RoleCategoryType.Dashboard && Ioc.ProgramBusiness.GetScheduleRegStatus(schedule) == ProgramRegStatus.Closed;
            this.Capacity = schedule.Attributes.Capacity;
            this.ShowCapacityLeft = schedule.Attributes.ShowCapacityLeft;
            this.ShowCapacityLeftNumber = schedule.Attributes.ShowCapacityLeftNumber;
            this.IsTestMode = schedule.Program.Season.Status == SeasonStatus.Test;
            ScheduleSections = schedule.Sessions.ToList();

            if (schedule.Program.TypeCategory == ProgramTypeCategory.Camp)
            {
                EnabledDropIn = ((ScheduleAttribute)schedule.Attributes).EnableDropIn;
                DropInLabel = ((ScheduleAttribute)schedule.Attributes).DropInLable;
            }
            

            foreach (var charge in schedule.Charges.Where(c => !c.IsDeleted))
            {
                var chargeAttributes = charge.Attributes;

                var chargeViewModel = charge.ToViewModel<RegisterChargeDiscountModel>();
                chargeViewModel.IsFull = JbUserService.GetCurrentUserRoleType() != RoleCategoryType.Dashboard && Ioc.ProgramBusiness.IsFull(charge, schedule.Program.Season.Status == SeasonStatus.Test);
                chargeViewModel.IsMandatory = chargeAttributes.IsMandatory;
                chargeViewModel.Capacity = charge.Capacity;
                chargeViewModel.NumberOfRegistration = Ioc.OrderItemBusiness.RegisteredCount(charge.Id, schedule.Program.Season.Status == SeasonStatus.Test);

                chargeViewModel.HasEarlyBird = false;
                chargeViewModel.HasProrate = false;

                if (schedule.Program.TypeCategory != ProgramTypeCategory.Subscription)
                {
                    chargeViewModel.LeftSessions = Ioc.ProgramBusiness.CalculateLeftSessions(schedule);
                    chargeViewModel.TotalSessions = Ioc.ProgramBusiness.CalculateTotalSessions(schedule);
                }

                if (schedule.Program.TypeCategory != ProgramTypeCategory.Calendar && charge.Attributes.EarlyBirds != null)
                {
                    foreach (var earlyBrid in charge.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                    {
                        if (DateTimeHelper.GetCurrentLocalDateTime(ClubTimeZone.Value).Date <= earlyBrid.ExpireDate.Value.Date)
                        {
                            chargeViewModel.HasEarlyBird = true;
                            chargeViewModel.Amount = earlyBrid.Price;
                            chargeViewModel.EarlyBirdExpireDate = earlyBrid.ExpireDate.Value;
                            chargeViewModel.EarlyBirdPrice = (chargeViewModel.Amount != charge.Amount) ? charge.Amount : 0;
                            break;
                        }
                    }
                }

                chargeViewModel.HasProrate = Ioc.ProgramBusiness.IsProgramScheduleProrate(schedule, charge);
                if (chargeAttributes.IsProrate && chargeViewModel.HasProrate)
                {
                    chargeViewModel.Amount = Ioc.ProgramBusiness.CalculateProratePrice(schedule, charge);

                    chargeViewModel.ProratePrice = charge.Amount;
                    chargeViewModel.HasNoSession = Ioc.ProgramBusiness.CalculateLeftSessions(schedule) < 1;
                }

                if (schedule.Program.TypeCategory == ProgramTypeCategory.Camp && ((ScheduleAttribute)schedule.Attributes).EnableDropIn && charge.Category == ChargeDiscountCategory.EntryFee)
                {
                    chargeViewModel.DropInLable = charge.Attributes.DropInName + " " + CurrencyHelper.FormatCurrencyWithPenny(charge.Attributes.DropInPrice.Value, schedule.Program.Club.Currency);
                }

                Charges.Add(chargeViewModel);
            }
        }
    }
}