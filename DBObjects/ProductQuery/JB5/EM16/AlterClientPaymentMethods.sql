

alter TABLE [dbo].[ClientPaymentMethods] add [EnableAchPayment] [bit]  NULL
Go
update [dbo].[ClientPaymentMethods] set [EnableAchPayment]=0
GO
alter TABLE [dbo].[ClientPaymentMethods] alter column [EnableAchPayment] [bit]  NOT NULL
Go
alter TABLE [dbo].[ClientPaymentMethods] add 	[AchPaymentCheckoutLabel] [nvarchar](max) NULL
go
update [dbo].[ClientPaymentMethods] set [AchPaymentCheckoutLabel]='ECheck'
GO
alter TABLE [dbo].[ClientPaymentMethods] add 	[AchPaymentMessage] [nvarchar](max) NULL
GO
alter TABLE [dbo].[ClientPaymentMethods] add 	[AchPaymentInstruction] [nvarchar](max) NULL
GO
ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnablePaypalPayment]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [IsPaypalTestMode]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnableCashPayment]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnableManualPayment]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [ManualPaymentMethod]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnableBankTransferPayment]
GO

ALTER TABLE [dbo].[ClientPaymentMethods] ADD  DEFAULT ((0)) FOR [EnableAchPayment]
GO


