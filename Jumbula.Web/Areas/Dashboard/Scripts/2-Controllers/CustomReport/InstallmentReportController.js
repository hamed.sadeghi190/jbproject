﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('InstallmentReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var model = $scope.Model;

        $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model })
            .success(function (data, status, headers, config) {
                if (is.undefined($scope.Model.Filters)) {
                    $scope.Model.Filters = {};
                    $scope.Model.Filters.Status = "regular";
                    $scope.Model.Filters.Tuition = "";
                    $scope.Model.Filters.Tuitions = data.Tuitions;
                    $scope.Model.Filters.OrderStatus = data.OrderStatus;
                }
                
            }).error(function (data, status, headers, config) {
            });

        $scope.AutomaticFilter = [{ Value: 'All', Title: "All" }, { Value: 'AutoCharge', Title: "Autocharge installments" }, { Value: 'NoAutoCharge', Title: "Manual installments" }];
        $scope.AutoType = 'All';

        $scope.filterData = function () {
            $scope.installmentReportGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.Filters = {};
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.Status = "regular";
            $scope.Model.Filters.Tuition = "";
            $scope.searchTerm = "";
            $scope.AutoType = 'All';
            $scope.installmentReportGridOptions.dataSource.read();
            $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model }).success(function (data, status, headers, config) {
                $scope.Model.Filters.Tuitions = data.Tuitions;
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            });
        }

        $scope.installmentReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/InstallmentReportRun',
                        type: "POST",
                        data: function () {
                            return {
                                //modelStringify: $scope.ModelStr,
                                model: model,
                                term: $scope.searchTerm,
                                automaticType: $scope.AutoType,
                            };
                        },
                    },

                },
            change: function (e) {
            },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
            batch: true,
            resizable: true,
        });

        $scope.installmentReportGridOptions = {
            dataSource: $scope.installmentReportsDataSource,
            sortable: { mode: 'single' },
            resizable: true,
            scrollable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            dataBound: function () {
                this.expandRow(this.tbody.find("tr.k-master-row").first());
                var grid = $("#installmentReportGrid").data("kendoGrid");
                var data = grid.dataSource.data();

                grid.thead.closest('table').css({ "max-width": "initial", "min-width": "110px" });
                grid.tbody.closest('table').css({ "max-width": "initial", "min-width": "110px" });

                customReportService.initializeDoubleScroll(grid);

                $.each(data, function (i, row) {
                    var status = row.PastDue;
                    if (status == "Yes") {
                        $('tr[data-uid="' + row.uid + '"], tr[data-uid="' + row.uid + '"] table').css("background-color", "#ffc13c");
                    }
                });
            },
            columns: [
                {
                    field: "FullName",
                    title: "Full name",
                    width: "110px",
                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderItemDetail(#=OrderItemId#)'  class='block'> #= FullName # </a> #if(ItemStatusReason == 1) {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == 2) {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == 5) {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == 4){# <i class='grid-label label warning'>Canceled</i> #}#"
                },
                {
                    field: "Email",
                    title: "Email",
                    width: "130px"
                },
                {
                    field: "ProgramName",
                    title: "Program",
                    width: "110px"
                },
                {
                    field: "RegDate",
                    title: "Registration date",
                    width: "130px",
                },
                {
                    field: "ConfirmationId",
                    title: "Confirmation",
                    template: "<a style='overflow: hidden;text-overflow: ellipsis;' ng-click='goOrderDetail(#=OrderId#)' class='block'> #= ConfirmationId # </a>",
                    width: "130px"
                },
                {
                    field: "InstallmentsNum",
                    title: "Installments",
                    width: "110px"
                },
                {
                    title: "Installment details",
                    template: '' +
                        '<table>' +
                            '<tbody>' +
                                '<tr ng-repeat=\'row in dataItem.Installments\'>' +
                                    '<td>{{row.DueDate}}</td>' +
                                    '<td>{{row.Amount}}</td>' +
                                    '<td>{{row.PaymentStatus}}</td>' +
                                '</tr>' +
                            '</tbody>' +
                        '</table>',
                    filed: "Installments",
                    width: "220px"
                },
                {
                    field: "PaidInstallments",
                    title: "Paid installments",
                    width: "110px"
                },
                {
                    field: "Total",
                    title: "Total amount",
                    width: "110px"
                },
                {
                    field: "Balance",
                    title: "Total balance",
                    width: "110px"
                },
                {
                    field: "PaidAmount",
                    title: "Paid amount",
                    width: "110px"
                },
                {
                    field: "Preapproval",
                    title: "Preapproval",
                    width: "110px"
                },
                {
                    field: "PastDue",
                    title: "Past due",
                    width: "110px"
                }]
        }

        $scope.saveToExcel = function () {
            $http.post(baseUrl + '/InstallmentReportRun', { model: model, isExportToExcel: true, term: $scope.searchTerm, automaticType: $scope.AutoType })
                .success(function (data, status, headers, config) {
                    window.location.href = data.Data;
                });
        }

        $scope.installmentSearch = function () {
            $scope.installmentReportGridOptions.dataSource.read();
        }

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/InstallmentReportRun', { model: model, filters: model.Filters, makeCampaign: true, term: $scope.searchTerm, automaticType: $scope.AutoType })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        $scope.goOrderDetail = function (orderId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderDetail($scope.MC.stateParams.seasonDomain, orderId);
        }

        $scope.goOrderItemDetail = function (orderItemId) {
            customReportService.setModelInStorage($scope.Model);
            customReportService.goOrderItemDetail($scope.MC.stateParams.seasonDomain, orderItemId);
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            $scope.Model.Filters.AutomaticTerm = $scope.AutoType;
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment', { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "Installment" });
        }

    }]);
})();