﻿namespace SportsClub.Models
{
    public class ParentDetailsViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

       
        public string FullName
        { 
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        
        }
        public string CellPhone { get; set; }

        public string Work { get; set; }

        public string DOB { get; set; }

        public string NickName { get; set; }

        public string Occupation { get; set; }

        public string Employer { get; set; }

        public int? Age { get; set; }

        public string Gender { get; set; }

        public string EmailAddress { get; set; }

        public string Phone { get; set; }

        //public List<PlayerProfile> ChildList { get; set; }
    }
}