ALTER table CatalogItems Add ImageGallery_Id int null
Go

ALTER TABLE [dbo].[CatalogItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogItems_dbo.ImageGallerys_ImageGallery_Id] FOREIGN KEY([ImageGallery_Id])
REFERENCES [dbo].[ImageGallerys] ([Id])