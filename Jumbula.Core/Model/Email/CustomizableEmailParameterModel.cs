﻿namespace Jumbula.Core.Model.Email
{
    public class CustomizableEmailParameterModel
    {
        public string Body { get; set; } = string.Empty;
        public int ClubId { get; set; }
    }
}
