alter table clubs alter column [CategoryId] [int] NULL
GO
alter table seasons add [HasOutSourcePrograms] [bit]  NULL
Go 
update seasons set [HasOutSourcePrograms]=0
go 
alter table seasons alter column [HasOutSourcePrograms] [bit] NOT NULL
GO
 drop index IX_ClubId on seasons
go
alter table seasons alter column [ClubId] [int] NOT NULL
GO
CREATE NONCLUSTERED INDEX [IX_ClubId] ON [dbo].[Seasons]
(
	[ClubId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

alter table programs add 	[OutSourceSeasonId] [bigint] NULL
GO
ALTER TABLE [dbo].[Programs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Programs_dbo.Seasons_OutSourceSeasonId] FOREIGN KEY([OutSourceSeasonId])
REFERENCES [dbo].[Seasons] ([Id])
GO

ALTER TABLE [dbo].[Programs] CHECK CONSTRAINT [FK_dbo.Programs_dbo.Seasons_OutSourceSeasonId]
GO

alter table clubs drop column userid

Go