﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Jumbula.Common.Helper
{
    public class KendoHelper
    {
        public string ActionGridBuilder(List<string> actionNames, string state, Dictionary<string, object> parameters, string fieldName, string ngAction = "")
        {
            // "<ul kendo-menu style='display: inline-block; position: absolute' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''> <li> ...  <ul> <li> <span class='k-link jbi-angle-right'>View details</span> </li> </ul> </li> </ul> "
            var result = string.Empty;
            for (int i = 0; i < actionNames.Count; i++)
            {
                var action = actionNames.ElementAt(i);
                result += string.Format("<ul kendo-menu style='display: inline-block; position: absolute' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''> <li> ...  <ul> <li> <span class='k-link jbi-angle-right'> {0}</span> </li> </ul> </li> </ul>", action);
                // result += string.Format("<ul kendo-menu style='display: inline-block; position: absolute' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''> <li> ...  +<ul> <li> <span class='k-link jbi-angle-right'> {0}</span> </li> </ul> </li> </ul>", action);

                TagBuilder ul = new TagBuilder("ul");
                ul.Attributes.Add("kendo-menu", "");
                ul.Attributes["style"] = "display: inline-block; position: absolute";
                ul.Attributes["k-direction"] = "left";
                ul.Attributes["k-orientation"] = "horizontal";
                ul.Attributes["k-rebind"] = "horizontal";
                ul.Attributes["k-on-select"] = "";

                TagBuilder li = new TagBuilder("li");
                li.InnerHtml = "...";

                TagBuilder ul2 = new TagBuilder("ul");

                TagBuilder li2 = new TagBuilder("li");

                TagBuilder span = new TagBuilder("span");
                span.AddCssClass("k-link");
                span.AddCssClass("jbi-angle-right");
                span.InnerHtml = action;


                li2.InnerHtml = span.ToString();
                ul2.InnerHtml = li2.ToString();
                li.InnerHtml = ul2.ToString();
                ul.InnerHtml = li.ToString();

                result += ul.ToString();
            }

            return result;
        }
    }
}
