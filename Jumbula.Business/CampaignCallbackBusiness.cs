﻿
using Jb.Framework.Common;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using System.Collections.Generic;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class CampaignCallbackBusiness : ICampaignCallbackBusiness
    {
        private readonly IRepository<CampaignCallback> _campaignCallbackRepository;
        private readonly IEntitiesContext _dataContext;
        public CampaignCallbackBusiness(IRepository<CampaignCallback> campaignCallbackRepository, IEntitiesContext dataContext)
        {
            _campaignCallbackRepository = campaignCallbackRepository;
            _dataContext = dataContext;
        }

        public OperationStatus Create(CampaignCallback callback)
        {
            return _campaignCallbackRepository.Create(callback);

        }

        public  OperationStatus saveDetails(List<CampaignCallbackDetail> callbackDetails)
        {

            OperationStatus result = new OperationStatus { Status = false };
            _dataContext.Set<CampaignCallbackDetail>().AddRange(callbackDetails);
            return _campaignCallbackRepository.Save();
        }
    }
}
