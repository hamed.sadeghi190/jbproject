﻿using System;

namespace SportsClub.Areas.Dashboard.Models
{
    public class TeachersReportViewModel
    {
        public string ParticipantName { get; set; }
        public string  TeacherName { get; set; }

        public string ClassName { get; set; }
 
        public string ClassDays { get; set; }

        public string Room { get; set; }

    

    }

}