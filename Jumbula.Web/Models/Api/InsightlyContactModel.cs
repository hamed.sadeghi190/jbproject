﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace Insightly.Models.Api
{
    [JsonObject(MemberSerialization.OptIn)]
    public class InsightlyContact 
    {
        public InsightlyContact() {

            Tags = new List<Tag>();
            Addresses = new List<Address>();
            ContactInfos = new List<ContactInfo>();
            CustomFields = new List<CustomField>();
        }

        [JsonProperty(PropertyName = "CONTACT_ID", NullValueHandling = NullValueHandling.Ignore)]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "SALUTATION", NullValueHandling = NullValueHandling.Ignore)]
        public string Salutation { get; set; }

        [JsonProperty(PropertyName = "FIRST_NAME", NullValueHandling = NullValueHandling.Ignore)]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "LAST_NAME", NullValueHandling = NullValueHandling.Ignore)]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "BACKGROUND", NullValueHandling = NullValueHandling.Ignore)]
        public string Background { get; set; }

        [JsonProperty(PropertyName = "IMAGE_URL", NullValueHandling = NullValueHandling.Ignore)]
        public string ImageUrl { get; set; }

        [JsonProperty(PropertyName = "DEFAULT_LINKED_ORGANISATION", NullValueHandling = NullValueHandling.Ignore)]
        public int DefaultLinkedOrganisation { get; set; }

        [JsonProperty(PropertyName = "OWNER_USER_ID", NullValueHandling = NullValueHandling.Ignore)]
        public int OwnerUserId { get; set; }

        [JsonProperty(PropertyName = "DATE_CREATED_UTC", NullValueHandling = NullValueHandling.Ignore)]
        public string DateCreated { get; set; }

        [JsonProperty(PropertyName = "DATE_UPDATED_UTC", NullValueHandling = NullValueHandling.Ignore)]
        public string DateUpdated { get; set; }

        [JsonProperty(PropertyName = "VISIBLE_TO", NullValueHandling = NullValueHandling.Ignore)]
        public string VisibleTo { get; set; }

        [JsonProperty(PropertyName = "VISIBLE_TEAM_ID", NullValueHandling = NullValueHandling.Ignore)]
        public int VisibleTeamId { get; set; }

        [JsonProperty(PropertyName = "VISIBLE_USER_IDS", NullValueHandling = NullValueHandling.Ignore)]
        public string VisibleUserIds { get; set; }

        [JsonProperty(PropertyName = "CUSTOMFIELDS", NullValueHandling = NullValueHandling.Ignore)]
        public List<CustomField> CustomFields { get; set; }

        [JsonProperty(PropertyName = "ADDRESSES", NullValueHandling = NullValueHandling.Ignore)]
        public List<Address> Addresses { get; set; }

        [JsonProperty(PropertyName = "CONTACTINFOS", NullValueHandling = NullValueHandling.Ignore)]
        public List<ContactInfo> ContactInfos { get; set; }


        [JsonProperty(PropertyName = "TAGS", NullValueHandling = NullValueHandling.Ignore)]
        public List<Tag> Tags { get; set; }

        [JsonProperty(PropertyName = "LINKS", NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<Link> Links { get; set; }

        [JsonProperty(PropertyName = "EMAILLINKS", NullValueHandling = NullValueHandling.Ignore)]
        public IEnumerable<EmailLink> EmailLinks { get; set; }
    }
}