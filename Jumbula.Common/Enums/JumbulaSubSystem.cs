﻿

namespace Jumbula.Common.Enums
{
    public enum JumbulaSubSystem
    {
        None = 0,
        Order = 1,
        RequestNewPreapproval = 2,
        Installment = 3,
        PreapprovalPayment = 4,
        MakeaPayment = 5,
        Invoice = 6,
        ClientMonthlyCharge = 7,
        AutoChargeScheduler = 8
    }
}
