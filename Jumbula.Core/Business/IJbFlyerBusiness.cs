﻿using Jb.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IJbFlyerBusiness
    {
        OperationStatus SaveFlyer(JbFlyer template);

        IQueryable<JbFlyer> GetAllFlyers();

        JbFlyer Get(int id);

        List<JbFlyer> GetList(List<int> flyerId);

        OperationStatus DeleteFlyer(int flyerId);

        OperationStatus Update(JbFlyer flyer);
    }
}
