﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    /// <summary>
    /// to set in a profile primary or secondary
    /// </summary>
    public enum PlayerProfileType
    {
        Primary,
        Secondary
    }

    /// <summary>
    /// Player profile relation type. set player relation with owner account
    /// </summary>
    public enum RelationshipType
    {
        //Self,
        [Description("Parent or legal guardian")]
        Parent,
        //Sibling,
        //Spouse,
        Registrant
    }

    /// <summary>
    /// Player profile status type. set player in different modes
    /// </summary>
    public enum PlayerProfileStatusType
    {
        Active,
        //ChildNoParent,
        Deleted = 255 // to avoid changing after adding any item to this enum
    }

    public enum EducationalStatus:byte
    {
        //[Description ("Not Graduated")]
        //NotGraduated=0,

        graduated = 1,
    }

    /// <summary>
    /// Store player information in database
    /// </summary>
    public class PlayerProfile : BaseEntity, IProtectedResource
    {
        /// <summary>
        /// current player id
        /// </summary>
        //public int Id { get; set; }
        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }

        /// <summary>
        /// Owner id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// profile type ( primary / secondary )
        /// </summary>
        //[Required]
        //public PlayerProfileType Type { get; set; }

        /// <summary>
        /// Player gender
        /// </summary>
        [Required]
        public GenderCategories Gender { get; set; }

        /// <summary>
        /// school information such as grade and school name
        /// </summary>
        //public School School { get; set; }

        /// <summary>
        /// current player contact information
        /// </summary>
        public virtual ContactPerson Contact { get; set; }

        /// <summary>
        /// enable send sms to player
        /// </summary>
        public bool EnableSMS { get; set; }

        /// <summary>
        /// store current player doctor information
        /// </summary>
        //public ContactPerson DoctorContact { get; set; }

        /// <summary>
        /// store current player and primary profile relationship
        /// </summary>
        public RelationshipType Relationship { get; set; }

        /// <summary>
        /// sports that current player doing them
        /// </summary>
        //public virtual ICollection<SportInUserProfile> SportProfiles { get; set; }

        /// <summary>
        /// player status
        /// </summary>
        public PlayerProfileStatusType Status { get; set; }

        public EducationalStatus? EducationalStatus { get; set; }

        /// <summary>
        /// player avatar name
        /// </summary>
        [StringLength(128)]
        public string Avatar { get; set; }

        //public string AlergyInfo { get; set; }

        //public string HealthInfo { get; set; }

        /// <summary>
        /// Navigational property to access to player order items
        /// </summary>
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        public string PaypalPreapprovalID { get; set; }

        public DateTime? PaypalPreapprovalStartDate { get; set; }
        public DateTime? PaypalPreapprovalEndDate { get; set; }

        [ForeignKey("InfoId")]
        public virtual PlayerInfo Info { get; set; }

        public int? InfoId { get; set; }

        public PlayerProfile()
        {
        }

        public bool DoesTheClubOwnIt(int clubId)
        {
            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            if (UserId == userId)
                return true;

            return false;
        }
    }
}
