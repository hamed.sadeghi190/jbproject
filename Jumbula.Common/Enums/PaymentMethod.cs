﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum PaymentMethod
    {
        [Description("none")]
        None = 0,
        [Description("Paypal")]
        Paypal = 1,
        [Description("Credit card")]
        Card = 4,
        [Description("Cash")]
        Cash = 5,
        [Description("Check")]
        Check = 6,
        [Description("Bank deposit")]
        BankTransfer = 8,
        [Description("Wire transfer")]
        WireTransfer = 9,
        [Description("Debit card")]
        DebitCard = 10,
        [Description("Scholarship")]
        Scholarship = 12,
        [Description("Family credit")]
        Credit = 13,
        [Description("Echeck")]
        Echeck = 14,
        [Description("Other")]
        Other = 11,
        [Description("Credit card")]
        Express = 7,
        [Description("Financial aid")]
        FinancialAid = 15,

    }

}
