﻿using System.Web.Mvc;
using Jumbula.Web.Mvc.Controllers;

namespace Jumbula.Web.Controllers
{
    public class MarketsController : JbBaseController
    {
        // GET: Markets
        public virtual ActionResult Index()
        {
            return View("Markets");
        }
        public virtual ActionResult Chess()
        {
            return View("MarketsChess");
        }

        public virtual ActionResult Schools()
        {
            return View("MarketsSchools");
        }
        public virtual ActionResult EnterpriseSolution()
        {
            return View("MarketsEnterpriseSolution");
        }
        public virtual ActionResult Vendors()
        {
            return View("MarketsVendors");
        }
        public virtual ActionResult Arts()
        {
            return View("MarketsArts");
        }
        public virtual ActionResult WebDesign()
        {
            return View("MarketsWebdesign");
        }
        public virtual ActionResult Clubs()
        {
            return View("MarketsClubs");
        }
        public virtual ActionResult ChildCare()
        {
            return View("MarketsChildCare");
        }
    }
}