﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.SignalR;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class ESignatureController : DashboardBaseController
    {
        public virtual ActionResult ManageESignature()
        {
            return View();
        }
        public virtual ActionResult CreateEditESignature()
        {
            return View();
        }
        public virtual ActionResult ViewDocumentESignature()
        {
            return View();
        }
        public virtual ActionResult DocumentSignSetting()
        {
            return View();
        }
        [HttpGet]
        public virtual JsonNetResult ESignatureMainPage()
        {

            var model = new FilterESignatureViewModel();

            model.ESignatureTypes = DropdownHelpers.ToSelectList<ESignatureType>();

            return JsonNet(model);
        }
        [HttpGet]
        [JbAuthorize(JbAction.ESignature_Setting)]
        public virtual JsonNetResult GetSettings()
        {
            var model = new DocumentSignSettingViewModel();
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            model.HasPartner = club.PartnerId.HasValue ? true : false;

            model.ActiveStaffs = club.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted).Select(s =>
                             new SelectKeyValue<int>
                             {
                                 Text = s.JbUserRole.User.UserName,
                                 Value = s.Id
                             })
                         .ToList();

            var adminRole = RoleCategory.Admin.ToString();
            var defaultStaff = club.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && s.JbUserRole.Role.Name == adminRole).ToList().FirstOrDefault().Id;

            if (club.Setting != null)
            {
                model.SelectedStaffSignatory = club.Setting.StaffSignatory > 0 ? club.Setting.StaffSignatory : defaultStaff;
            }
            else
            {
                model.SelectedStaffSignatory = defaultStaff;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.ESignature_Setting)]
        public virtual ActionResult SaveSetting(DocumentSignSettingViewModel model)
        {
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            var staff = club.ClubStaffs.SingleOrDefault(c => c.Id == model.SelectedStaffSignatory);
            var message = "{0} updated successfully.";

            if (model.SelectedStaffSignatory >= 0)
            {
                club.Setting.StaffSignatory = model.SelectedStaffSignatory;
                message = string.Format(message, "Staff email");
            }
            else
            {
                return Json(new JResult { Status = false });
            }

            club.IsSettingChanged = true;
            var res = Ioc.ClubBusiness.Update(club);

            return Json(new JResult { Status = res.Status, Message = res.Status ? message : res.Message, RecordsAffected = res.RecordsAffected });
        }
        public virtual JsonNetResult GetAllDocumentSign(PaginationModel paginationModel, string term, string companyTerm, ESignatureType eSignaturType = ESignatureType.All)
        {
            try
            {
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                var documenSigns = new List<JbESignature>();

                if (club.IsPartner)
                {
                    documenSigns = Ioc.JbESignatureBusiness.GetList().Where(d => d.SenderId == club.Id).ToList();
                }
                else
                {
                    documenSigns = Ioc.JbESignatureBusiness.GetList().Where(d => d.ReciverId == club.Id).ToList();
                }


                switch (eSignaturType)
                {
                    case ESignatureType.All:
                        break;
                    case ESignatureType.Completed:
                        {
                            documenSigns = documenSigns.Where(d => d.Status == SignatureStatus.Completed).ToList();
                        }
                        break;
                    case ESignatureType.Inprogress:
                        {
                            documenSigns = documenSigns.Where(d => d.Status == SignatureStatus.Inprogress).ToList();
                        }
                        break;

                }
                if (!string.IsNullOrEmpty(companyTerm))
                {
                    documenSigns = club.PartnerId.HasValue ? documenSigns.Where(doc => doc.Sender.Name.ToLower() == companyTerm.ToLower()).ToList() : documenSigns.Where(doc => doc.Reciver.Name.ToLower() == companyTerm.ToLower()).ToList();
                }

                var model = documenSigns.OrderByDescending(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize).ToList().Select(c => new AllESignatureViewModel()
                {
                    Name = c.DocumentName,
                    Company = club.PartnerId.HasValue ? c.Sender.Name : c.Reciver.Name,
                    DeadLine = c.DeadLine.HasValue ? c.DeadLine.Value : (DateTime?)null,
                    CreateDocument = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), c.CreateDate),
                    Status = c.Status.ToDescription(),
                    Id = c.Id,
                    Subject = c.Subject,
                    DocumentUrl = StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + c.MetaData.FolderName + "/" + c.MetaData.FileName).AbsoluteUri,
                    IsMember = club.PartnerId.HasValue ? true : false,
                });

                var datasource = model.ToList();

                return JsonNet(new { data = datasource, total = documenSigns.Count() });
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        [HttpGet]
        public virtual ActionResult CreatEditEsignature(int? documentSignId)
        {

            var model = new EsignatureViewModel();
            model.PageMode = documentSignId.HasValue ? PageMode.Edit : PageMode.Create;

            var senderId = ActiveClub.Id;
            var members = Ioc.ClubBusiness.GetList().Where(c => c.PartnerId == senderId && !c.IsDeleted).OrderBy(c => c.Name).ToList();

            model.PartnerMembers = members.Select(l => new SelectKeyValue<int> { Text = l.Name, Value = l.Id }).ToList();

            switch (model.PageMode)

            {
                case PageMode.Create:
                    {
                        model.Subject = "Please sign the document";
                        model.PageTitle = "Send document";
                    }
                    break;
                case PageMode.Edit:
                    {
                        model.PageTitle = "Edit document";
                        var documentSign = Ioc.JbESignatureBusiness.Get(documentSignId.Value);

                        model.Subject = documentSign.Subject;
                        model.SelectedMember = documentSign.ReciverId.Value;
                        model.DeadLine = documentSign.DeadLine.Value;
                        model.Content = documentSign.Content;
                    }
                    break;

            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.ESignature_Add)]
        public virtual ActionResult CreatEditEsignature(EsignatureViewModel model)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var clubReciver = Ioc.ClubBusiness.Get(model.SelectedMember);

            if(clubReciver == null)
            {
                ModelState.AddModelError("model.SelectedMember", "Member is required.");
            }

            if (ModelState.IsValid)
            {
                var staffSignatory = Ioc.ClubBusiness.GetStaffs(clubReciver.Id).Where(s => s.Id == clubReciver.Setting.StaffSignatory).FirstOrDefault();
                var userId = 0;

                if (staffSignatory != null)
                {
                    userId = staffSignatory.JbUserRole.UserId;
                }
                else
                {
                    var adminRole = RoleCategory.Admin.ToString();
                    var defaultStaff = clubReciver.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && s.JbUserRole.Role.Name == adminRole).ToList().FirstOrDefault();
                    if (defaultStaff != null) userId = defaultStaff.JbUserRole.UserId;
                }

                OperationStatus res;
                try
                {
                    var uploadedFile = FileUploder(model.File, model.FileName, "");

                    var documentSign = new JbESignature()
                    {
                        Subject = model.Subject,
                        SenderId = club.Id,
                        ReciverId = model.SelectedMember,
                        Status = SignatureStatus.Inprogress,
                        DeadLine = model.DeadLine.HasValue ? model.DeadLine.Value : (DateTime?)null,
                        CreateDate = DateTime.UtcNow,
                        DocumentName = model.FileName,
                        Content = model.Content,
                        MetaData = new StorageMetaData { FileName = uploadedFile.Filename, FolderName = uploadedFile.Foldername },
                        Message = Ioc.MessageBusiness.GetChat(this.GetCurrentUserId(), this.GetCurrentClubId(), userId, model.SelectedMember, "E-Signatures" + ", " + model.FileName, null, MessageType.ESignature, null, null)
                    };

                    res = Ioc.JbESignatureBusiness.Create(documentSign);

                    if (res.Status)
                    {
                        var messageModel = new ESignatureMessageViewModel()
                        {
                            ChatId = documentSign.Message.ChatId,
                            Text = model.Subject + "<br/>" + model.Content,
                            Subject = model.FileName,
                            SenderId = this.GetCurrentUserId(),
                            SenderClubId = this.GetCurrentClubId(),
                            ReciverId = userId,
                            reciverClubId = model.SelectedMember,
                            messageType = MessageType.ESignature,
                        };

                        var sendMessage = SendMessage(messageModel);
                    }

                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }
                catch (Exception ex)
                {
                    return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
                }
            }

            return JsonFormResponse();
        }
        [HttpGet]
        public virtual JsonNetResult GetValidStaffForSign()
        {
            var status = false;
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            var staffSignatory = Ioc.ClubBusiness.GetStaffs(club.Id).Where(c=>!c.IsDeleted).Where(s => s.Id == club.Setting.StaffSignatory).FirstOrDefault();
            var userId = 0;

            if (staffSignatory != null)
            {
                userId = staffSignatory.JbUserRole.UserId;
            }
            else
            {
                var adminRole = RoleCategory.Admin.ToString();
                var defaultStaff = club.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && s.JbUserRole.Role.Name == adminRole && !s.IsDeleted).ToList().FirstOrDefault();
                userId = defaultStaff.JbUserRole.UserId;
            }


            if (club.Setting != null)
            {
                status = userId == this.GetCurrentUserId() ? true : false;

                return JsonNet(status);
            }
            else
            {
                return JsonNet(status);
            }

        }
        [HttpGet]
        public virtual ActionResult GetDocumentSigned(int documentId)
        {
            var model = new SignDocumentViewModel();
            var documentSign = Ioc.JbESignatureBusiness.Get(documentId);
            var club = documentSign.Reciver;
            var staffSignatory = Ioc.ClubBusiness.GetStaffs(club.Id).Where(s => s.Id == club.Setting.StaffSignatory).FirstOrDefault();
            var userId = 0;
            var name = "";

            if (staffSignatory != null)
            {
                userId = staffSignatory.JbUserRole.UserId;
                name = staffSignatory.Contact != null ? staffSignatory.Contact.FirstName : null;
            }
            else
            {
                var adminRole = RoleCategory.Admin.ToString();
                var defaultStaff = club.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && s.JbUserRole.Role.Name == adminRole).ToList().FirstOrDefault();
                userId = defaultStaff.JbUserRole.UserId;
                name = defaultStaff.Contact != null ? defaultStaff.Contact.FirstName : null;
            }

            model.PageTitle = documentSign.Status == SignatureStatus.Completed ? "View signature" : "Sign Document";
            model.Signature = documentSign.Signature;
            model.Id = documentId;
            model.IsSign = model.Signature != null ? true : false;
            model.Title = documentSign.SignatureTitle;

            if (documentSign.Status== SignatureStatus.Completed)
            {
                model.Name = documentSign.SignedBy;
                model.Company = documentSign.ReciverName;
                model.SignedDate = documentSign.SignatureDate.HasValue ? Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), documentSign.SignatureDate.Value).ToString("MMM dd, yyyy") : string.Empty;
            }
            else
            {
                model.Name = name;
                model.Company = this.GetCurrentClubName();
            }

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SaveSignatureDocument(SignDocumentViewModel model)
        {
            var res = new OperationStatus();
            var message = "";

            if (ModelState.IsValid)
            {
                try
                {
                    var document = Ioc.JbESignatureBusiness.Get(model.Id);

                    if (string.IsNullOrEmpty(model.Signature) || model.Signature == "image/jsignature;base30,")
                    {
                        message = "You need to sign the document.";
                    }
                    else
                    {
                        document.Signature = model.Signature;
                        document.Status = SignatureStatus.Completed;
                        document.SignedBy = model.Name;
                        document.SignatureDate = DateTime.UtcNow;
                        document.SignatureTitle = model.Title;
                        document.ReciverName = model.Company;

                        res = Ioc.JbESignatureBusiness.Update(document);

                        if (res.Status)
                        {
                            var club = document.Sender;
                            var staffSignatory = Ioc.ClubBusiness.GetStaffs(club.Id).Where(s => s.Id == club.Setting.StaffSignatory).FirstOrDefault();
                            var userId = 0;

                            if (staffSignatory != null)
                            {
                                userId = staffSignatory.JbUserRole.UserId;
                            }
                            else
                            {
                                var partnerRole = RoleCategory.Partner.ToString();
                                var defaultStaff = club.ClubStaffs.FirstOrDefault(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted && s.JbUserRole.Role.Name == partnerRole);
                                userId = defaultStaff.JbUserRole.UserId;
                            }

                            var messageModel = new ESignatureMessageViewModel()
                            {
                                ChatId = document.Message.ChatId,
                                Text = model.Name + " signed the document",
                                Subject = "Signed",
                                SenderId = this.GetCurrentUserId(),
                                SenderClubId = this.GetCurrentClubId(),
                                ReciverId = userId,
                                reciverClubId = document.SenderId,
                                messageType = MessageType.ESignature
                            };
                            message = res.Status == true ? "Your document was signed successfuly." : res.Message;

                            var sendMessage = SendMessage(messageModel);

                        }


                    }

                    return Json(new JResult { Status = res.Status, Message = message });
                }
                catch (Exception)
                {
                    return Json(new JResult { Status = res.Status, Message = res.Message });
                }
            }
            return JsonFormResponse();

        }
        [HttpPost]
        public virtual JsonNetResult UploadedDocument(HttpPostedFileBase uploadedList)
        {
            var uploadDocument = Jumbula.Common.Helper.FileHelper.ConvertStreamToByte(uploadedList.InputStream);
            return JsonNet(new { Data = "Add", uploadedList = uploadDocument, Status = true, FileName = uploadedList.FileName });

        }
        public virtual JsonNetResult DeleteUploadedDocument(string[] fileNames)
        {
            return JsonNet(new JResult { Data = "Remove", Status = true });
        }
        private bool SendMessage(ESignatureMessageViewModel model)
        {
            var messageBusiness = Ioc.MessageBusiness;
            int receiverId = 0;

            var result = messageBusiness.Send(model.ChatId, this.GetCurrentUserId(), this.GetCurrentClubId(), model.ReciverId, model.reciverClubId, model.Subject, model.Text);
            var context = GlobalHost.ConnectionManager.GetHubContext<SignalRHub>();

            var receiverUser = Ioc.MessageBusiness.GetAudienceUser(result, this.GetCurrentUserId());

            var chatAttriute = ((MessageEsignatureAttribute)(result.Attribute));

            var lastUserMessage = result.Messages.SelectMany(s => s.UserMessages).Last();

            var audience = messageBusiness.GetAudience(result, this.GetCurrentClubId());

            var messageResponse = new
            {
                Unread = Ioc.MessageBusiness.GetUnreadCount(model.ReciverId, lastUserMessage.ClubId),
                RefreshCurrentMessage = false,
                TargetClubDomain = audience.Domain,
                Chats = new List<object>()
                    {
                        new
                        {
                            Id = result.Id,
                            ChatId = result.ChatId,
                            Subject = result.Subject,
                            MessageType = result.Type,
                            MessageStatus = chatAttriute.Status.ToDescription(),
                            Summary = this.GetCurrentClubName(),
                            Audience = this.GetCurrentClubName(),
                            Logo = UrlHelpers.GetClubLogoUrl(audience.Domain, audience.Logo),
                            IsUnread = true,
                            DateTimeTicks = result.Messages.Max(m => m.CreatedDate).Ticks,
                            DateTime = result.Messages.Max(m => m.CreatedDate),
                            Unread = result.Messages.SelectMany(m => m.UserMessages).Count(m => !m.IsRead && m.ReceiverId == receiverId && m.ClubId == audience.Id),
                            Messages = new List<object>()
                            {
                                new
                                {
                                    Text = model.Text,
                                    DateTime = result.Messages.Last().CreatedDate,
                                    DateTimeTicks = result.Messages.Last().CreatedDate.Ticks,
                                }
                            }
                        }
                    },

            };

            context.Clients.User(receiverUser.UserName).refreshSignal(messageResponse);
            return true;

        }

        private string GetSenderClubName(MessageHeader messageHeader)
        {
            string result;

            result = messageHeader.Messages.Last().Club.Name;

            return result;
        }
        private dynamic FileUploder(byte[] fileData, string fileName, string location)
        {
            try
            {
                dynamic result = new ExpandoObject();

                var clubDomain = Request.Url.GetSubDomain();
                var fileAttribute = new StorageMetaData(StorageFileType.ESignature, fileName, clubDomain);
                var docName = fileAttribute.FileName;

                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                string contentType = string.Format(Constants.F_Image_MimeType, Path.GetExtension(fileName).Substring(1));

                Stream fileStream = new MemoryStream(fileData);

                sm.UploadBlob(fileAttribute.FolderName, docName, fileStream, contentType);
                fileStream.Close();

                var fileUrl = StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + fileAttribute.FolderName + "/" + docName).AbsoluteUri;

                result.Filename = docName;
                result.Foldername = fileAttribute.FolderName;
                result.FileUrl = fileUrl;

                return result;


            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return string.Empty;
            }
        }

    }
}