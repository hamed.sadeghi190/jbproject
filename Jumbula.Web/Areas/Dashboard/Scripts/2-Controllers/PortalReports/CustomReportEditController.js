﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CustomReportEditController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $http.get(baseUrl + '/CreateEditCustomReport', { params: { id: $scope.MC.stateParams.reportId } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
            });

        $scope.saveReport = function () {
            $http.post(baseUrl + '/CreateEditCustomReport', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (status == 200) {
                        $state.go('CustomReportPortal')
                    }
                });
        }

        $scope.checkAll = function (sectionCheckAllModel, sectionCheckModels) {
            angular.forEach(sectionCheckModels, function (item) {
                if (item.Name[0] != '%') {
                    item.Value = sectionCheckAllModel;
                }
            });
        };

    }]);
})();