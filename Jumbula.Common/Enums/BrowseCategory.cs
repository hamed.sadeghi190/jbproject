﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum BrowseCategory
    {
        [Display(Name = "Clubs")]
        [Description("Clubs")]
        Clubs,
        [Display(Name = "Events")]
        [Description("Events")]
        Events,
        SpecificClub,
        SpecificEvent,
        [Display(Name = "Clubs")]
        [Description("Clubs")]
        CustomText
    }
}
