﻿using Autofac;
using Jumbula.Business.EmailBuilder;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Bootstrapper.Modules
{
    public class EmailModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AutoChargeSuccessEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.AutoChargeSuccess);
            builder.RegisterType<AutoChargeFailEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.AutoChargeFail);
            builder.RegisterType<ResetPasswordSuccessEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.ResetPasswordSuccess);
            builder.RegisterType<AutoChargeSupportSummaryEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.AutoChargeSupportSummary);
            builder.RegisterType<AutoChargeSummaryEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.AutoChargeSummary);
            builder.RegisterType<RequestDemoEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.RequestDemo);
            builder.RegisterType<ResetPasswordEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.ResetPassword);
            builder.RegisterType<DelinquentEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.Delinquent);
            builder.RegisterType<RefundOrderEmailBuilder>().As<IEmailBuilder>().Keyed<IEmailBuilder>(EmailCategory.RefundOrder);
        }
    }
}
