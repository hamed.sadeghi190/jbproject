﻿using System;
using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SelectedPrograms
    {
        public SelectedPrograms()
        {
            // do some things good.
        }

        public List<long> _SelectedPrograms { get; set; }
        public DateTime DateProgram { get; set; }
        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public string SeasonDomain { get; set; }

        public bool IsAllPrograms { get; set; }

        public string Term { get; set; }
        public string AutomaticTerm { get; set; }

        public IncludeForms IncludeForms { get; set; }

        public ReportFilters Filters { get; set; }
    }

    public class IncludeForms
    {
        public bool Waivers { get; set; }

        public bool Followups { get; set; }

        public bool Registrations { get; set; }
    }

}