﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsScheduleController', ['$scope', '$http', 'jbToast', '$stateParams', function ($scope, $http, jbToast, $stateParams) {
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSetting", order: 2, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;
        $http.get('Dashboard/Season/EditSchoolSettingsScheduleInfo?seasonDomain=' + $stateParams.seasonDomain).success(function (data, status, headers, config) {
            $scope.Model = data;            

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });
        $scope.mondaydates = '';
        $scope.tuesdaydates = '';
        $scope.wednesdaydates = '';
        $scope.thursdaydates = '';
        $scope.fridaydates = '';
        $scope.noclassdates = '';
        $scope.datetimePickerChanged=function(e,dayOfWeek)
        {
            if (dayOfWeek == 'Monday') {
                $scope.Model.AllMondayDates.push({ Text: $scope.mondaydates, Value: $scope.mondaydates });
                $scope.Model.MondayDates.push($scope.mondaydates);
            }
            if (dayOfWeek == 'Tuesday') {
                $scope.Model.AllTuesdayDates.push({ Text: $scope.tuesdaydates, Value: $scope.tuesdaydates });
                $scope.Model.TuesdayDates.push($scope.tuesdaydates);
            }
            if (dayOfWeek == 'Wednesday') {
                $scope.Model.AllWednesdayDates.push({ Text: $scope.wednesdaydates, Value: $scope.wednesdaydates });
                $scope.Model.WednesdayDates.push($scope.wednesdaydates);
            }
            if (dayOfWeek == 'Thursday') {
                $scope.Model.AllThursdayDates.push({ Text: $scope.thursdaydates, Value: $scope.thursdaydates });
                $scope.Model.ThursdayDates.push($scope.thursdaydates);

            }
            if (dayOfWeek == 'Friday') {
                $scope.Model.AllFridayDates.push({ Text: $scope.fridaydates, Value: $scope.fridaydates });
                $scope.Model.FridayDates.push($scope.fridaydates);
            }
            if (dayOfWeek == 'NoClass') {
                $scope.Model.AllNoClassDates.push({ Text: $scope.noclassdates, Value: $scope.noclassdates });
                $scope.Model.NoClassDates.push($scope.noclassdates);
            }
        }
     
        $scope.save = function () {

         
            $http.post('Dashboard/Season/EditSchoolSettingsScheduleInfo', { model: $scope.Model, seasonDomain: $stateParams.seasonDomain })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     $scope.errors = null;
                     jbToast.success("Schedule information updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }
  
    }])

})();