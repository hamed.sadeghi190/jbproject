﻿using Jumbula.Common.Enums;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Core.Model.Order
{
    public class ChargeDiscountItemViewModel
    {
        public long Id { get; set; }

        [MaxLength(256, ErrorMessage = "{0} cannot be longer than 256 characters.")]
        public string Name { get; set; }

        public decimal Amount { get; set; }
        public string Type { get; set; }
        public bool Checked { get; set; }

        public int? CouponId { get; set; }

        public long? ChargeId { get; set; }

        public long? DiscountId { get; set; }

        public ChargeDiscountType AmountType { get; set; }

        public ChargeDiscountCategory Category { get; set; }

        public ChargeDiscountSubcategory SubCategory { get; set; }
    }
}
