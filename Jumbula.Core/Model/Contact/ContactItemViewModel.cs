﻿using System;

namespace Jumbula.Core.Model.Contact
{
    public class ContactItemViewModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string OrganizationName { get; set; }

        public string Email { get; set; }

        public string Nickname { get; set; }

        public string CellPhone { get; set; }

        public string WorkPhone { get; set; }

        public string Phone { get; set; }

        public string PhoneExtension { get; set; }

        public string Title { get; set; }

        public DateTime? DoB { get; set; }

        public bool IsStaff { get; set; }

        public bool IsLimitedForChange { get; set; }

        public bool IsPrimary { get; set; }

        public string StaffStatus { get; set; }
    }
}
