﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ShareAsAttachment
    {
        [Required(ErrorMessage = "{0} is required.")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string SentTo { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Email subject")]
        public string Subject { get; set; }

        [Required]
        [Display(Name = "Email body")]
        public string Message { get; set; }

        public string CustomMessage { get; set; }

    }
}