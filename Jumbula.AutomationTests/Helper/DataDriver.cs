﻿using System.Collections.Generic;

namespace Jumbula.AutomationTests.Helper
{
    public class DataDriver
    {
        #region URL
        public static string Domain
        {
            get
            {
                return "greenteaplanet.com";
            }
        }
        public static string CreateURL(string clubDomain)
        {
            return string.Format("https://{0}.{1}", clubDomain, Domain);
        }

        public static string CreateLoginURL(string clubDomain)
        {
            return string.Format("https://{0}.{1}/login", clubDomain, Domain);
        }

        public static string SportClubURL
        {
            get
            {
                return CreateURL("automationtest");
            }
        }
        public static string PartnerURL
        {
            get
            {
                return CreateURL("partnerautomationtest");
                
            }
        }
        public static string Partner2URL
        {
            get
            {
                return CreateURL("partner2automationtest");
                
            }
        }
        
        #endregion

        #region Partner
        public static string PartnerUsername
        {
            get
            {
                return "partner@jumbula.com";
            }
        }

        public static string PartnerPassword
        {
            get
            {
                return "12345678";
            }
        }
        
        
        #endregion Partner
        #region Admin
        public static string AdminUsername
        {
            get
            {
                return "adminstaff@jumbula.com";
            }
        }

        public static string AdminPassword
        {
            get
            {
                return "12345678";
            }
        }


#endregion Admin

        

        #region CreateProgram
        public static string CalssName
        {
            get
            {
                return "Test class ";
            }
        }

        public static string ProgramStartDate
        {
            get
            {
                return "12345678";
            }
        }


        #endregion
        public struct club
        {
            public string Domain;
            public string Name;
        }
        public static List<club> PartnerSchools
        {
            get
            {
                List<club> schools = new List<club>();
                
                schools.Add(new club { Domain = "jumbula-ashland", Name = "Ashland Elementary PTO" });
                schools.Add(new club { Domain = "jumbula-ashlawn", Name = "Ashlawn Elementary PTA" });
                schools.Add(new club { Domain = "jumbula-claremont", Name = "Claremont Immersion School PTA" });
                schools.Add(new club { Domain = "jumbula-discovery", Name = "Discovery Elementary PTA" });
                schools.Add(new club { Domain = "jumbula-hayfield", Name = "Hayfield Elementary PTA" });
                schools.Add(new club { Domain = "jumbula-barrett", Name = "Barrett Elementary PTA" });
                schools.Add(new club { Domain = "jumbula-jamestown", Name = "Jamestown Elementary PTA" });
                schools.Add(new club { Domain = "jumbula-pattie", Name = "John F. Pattie, Sr. Elementary PTCO" });
                schools.Add(new club { Domain = "jumbula-mckinley", Name = "McKinley Elementary PTA" });
                schools.Add(new club { Domain = "jumbula-oakton", Name = "Oakton Elementary PTA" });

                return schools;
            }
        }
        public static List<club> PartnerProviders
        {
            get
            {
                List<club> schools = new List<club>();

                schools.Add(new club { Domain = "jumbula-byh", Name = "Best Youth Hoops" });
                schools.Add(new club { Domain = "jumbula-canderson", Name = "Cathy Anderson" });
                schools.Add(new club { Domain = "jumbula-coretennis", Name = "Core Tennis" });
                schools.Add(new club { Domain = "jumbula-dmoore", Name = "Deborah Moore" });
                schools.Add(new club { Domain = "jumbula-llarco", Name = "Laura Larco" });

                return schools;
            }
        }
        public static List<club> PartnerDistricts
        {
            get
            {
                List<club> schools = new List<club>();

                schools.Add(new club { Domain = "schooldistrict2", Name = "School district2" });
                schools.Add(new club { Domain = "schooldistrict1", Name = "School district1" });
                
                return schools;
            }
        }
        public static List<club> Partners
        {
            get
            {
                List<club> schools = new List<club>();

                schools.Add(new club { Domain = "partnerautomationtest", Name = "Partner for automation test" });
                
                return schools;
            }
        }
        public static List<club> Parents
        {
            get
            {
                List<club> parents = new List<club>();

                parents.Add(new club { Domain = "automationtest", Name = "Automation Test" });
                parents.Add(new club { Domain = "automationtest2", Name = "Automation Test2" });

                return parents;
            }
        }
        public static List<club> SchoolsOfDistrict
        {
            get
            {
                List<club> SchoolsOfDistrict = new List<club>();

                SchoolsOfDistrict.Add(new club { Domain = "jumbula-discovery", Name = "Discovery Elementary PTA" });
                SchoolsOfDistrict.Add(new club { Domain = "jumbula-mckinley", Name = "McKinley Elementary PTA" });

                return SchoolsOfDistrict;
            }
        }
    }
}
