﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Model.Generic;
using Newtonsoft.Json;
using Constants=Jumbula.Common.Constants.Constants;


namespace Jumbula.Core.Domain
{
    public class ProgramSchedule : BaseEntity<long>
    {
        public ProgramSchedule()
        {
            OrderItems = new HashSet<OrderItem>();
            WaitLists = new HashSet<WaitList>();
        }

        private ScheduleBaseAttribute _attributes;

        [StringLength(100)]
        public string Title { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public RegistrationPeriod RegistrationPeriod { get; set; }

        public AttendeeRestriction AttendeeRestriction { get; set; }

        [ForeignKey("ProgramId")]
        public virtual Program Program { get; set; }

        public long ProgramId { get; set; }

        public virtual ICollection<Charge> Charges { get; set; }

        public virtual ICollection<WaitList> WaitLists { get; set; }

        [Column("Attributes")]
        public string AttributesSerialized { get; set; }

        [NotMapped]
        public ScheduleBaseAttribute Attributes
        {
            get
            {
                if (_attributes == null)
                {

                    if (this.Program == null)
                    {
                        _attributes = JsonConvert.DeserializeObject<ScheduleAttribute>(AttributesSerialized);
                    }
                    else
                    {
                        switch (this.Program.TypeCategory)
                        {
                            case ProgramTypeCategory.ChessTournament:
                                {
                                    _attributes = JsonConvert.DeserializeObject<TournamentScheduleAttribute>(AttributesSerialized);
                                }
                                break;
                            case ProgramTypeCategory.Subscription:
                                {
                                    _attributes = JsonConvert.DeserializeObject<ScheduleSubscriptionAttribute>(AttributesSerialized);
                                }
                                break;
                            case ProgramTypeCategory.BeforeAfterCare:
                                {
                                    _attributes = JsonConvert.DeserializeObject<ScheduleAfterBeforeCareAttribute>(AttributesSerialized);
                                }
                                break;
                            default:
                                {
                                    _attributes = JsonConvert.DeserializeObject<ScheduleAttribute>(AttributesSerialized);
                                }
                                break;
                        }
                    }
                }

                return _attributes;
            }

            set
            {
                AttributesSerialized = JsonConvert.SerializeObject(value);
            }
        }

        [Column("Sessions")]
        public string SectionsSerialized { get; set; }

        [NotMapped]
        public List<ScheduleSession> Sessions
        {
            get
            {

                return SectionsSerialized == null ? new List<ScheduleSession>() : JsonConvert.DeserializeObject<List<ScheduleSession>>(SectionsSerialized);
            }
            set
            {
                SectionsSerialized = JsonConvert.SerializeObject(value);
            }
        }

        public bool IsDeleted { get; set; }
        public bool IsCampOvernight { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public virtual ICollection<ProgramSession> ProgramSessions { get; set; }

        public TimeOfClassFormation ScheduleMode { get; set; }

        [StringLength(4000)]
        public string Description { get; set; }

        public bool IsFreezed { get; set; }
    }

    public class ScheduleBaseAttribute
    {
        public int MinimumEnrollment { get; set; }

        public int Capacity { get; set; }
        public bool ShowCapacityLeft { get; set; }
        public int ShowCapacityLeftNumber { get; set; }
    }



    public class ScheduleAttribute : ScheduleBaseAttribute
    {
        public List<ProgramScheduleDay> Days { get; set; }

        public ContinueType ContinueType { get; set; }

        public int Occurances { get; set; }

        public bool IsProrate { get; set; }

        public WeekDaysMode WeekDaysMode { get; set; }

        public bool EnableDropIn { get; set; }

        public string DropInLable { get; set; }

    }

    public class ScheduleSubscriptionAttribute : ScheduleBaseAttribute
    {
        public ScheduleSubscriptionAttribute()
        {
            EarlyBirds = new List<EarlyBird>();
        }

        public ContinueType ContinueType { get; set; }

        public int Occurances { get; set; }

        public bool IsProrate { get; set; }

        public DateTime ProrateTime { get; set; }

        public List<SubscriptionSchedule> Subscriptions { get; set; }

        public List<ProgramSubscriptionItem> SubscriptionItems { get; set; }

        public List<ProgramScheduleDay> Days { get; set; }

        public List<EarlyBird> EarlyBirds { get; set; }

        public int DueDate { get; set; }

        public bool HasPayInFullOption { get; set; }

        public decimal FullPayDiscount { get; set; }

        public string FullPayDiscountMessage { get; set; }

        public bool EnableDropIn { get; set; }

        public string DropInSessionLabel { get; set; }

        public decimal DropInSessionPrice { get; set; }
    }

    public class SubscriptionSchedule
    {
        public long Id { get; set; }

        [Display(Name = "Price for schedule")]
        [Required(ErrorMessage = "{0} is required.")]
        [Range(minimum: 0.01, maximum: double.MaxValue, ErrorMessage = "{0} is incorrect.")]
        public decimal Amount { get; set; }
        public RepeatType RepeatType { get; set; }
        public string Occurances { get; set; }
        public string DaysBeforeBilling { get; set; }

        [Display(Name = "Tuition label")]
        [Required(ErrorMessage = "{0} is required.")]
        public string PriceLabel { get; set; }

        public int? DueDate { get; set; }

        public int WeekDays { get; set; }

        public List<ProgramScheduleDay> Days { get; set; }
    }

    public class ProgramSubscriptionItem
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime DueDate { get; set; }
    }

    public class TournamentScheduleAttribute : ScheduleBaseAttribute
    {
        public TournamentType TournamentType { get; set; }

        public decimal? PrizeFund { get; set; }

        public int? FullEntries { get; set; }

        public string PrizeFundGuarantee { get; set; }

        public string Trophies { get; set; }

        public List<TournamentSection> Sections { get; set; }

        public List<TournamentSchedule> Schedules { get; set; }

        public int? MaxOfByes { get; set; }

        public int? LastRoundBye { get; set; }

        public int? NumberOfRounds { get; set; }

        public string ByeNote { get; set; }

        public bool HideViewEntries { get; set; }

    }

    public class TournamentSection
    {
        [Display(Name = "Section  name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }

        public string Rating { get; set; }

        public string Prize { get; set; }

        public bool IsChecked { get; set; }
    }

    public class TournamentSchedule
    {

        [Display(Name = "Start date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? EndDate { get; set; }

        public string Label { get; set; }

        public string Title { get; set; }

        public int Id { get; set; }
        public bool IsChecked { get; set; }
        public string GetSchName()
        {
            string schName = (EndDate.Value.Subtract(StartDate.Value).Days + 1) + "-day";
            if (!string.IsNullOrEmpty(Label))
                schName += string.Format(" {0}", Label);
            return schName;
        }

        public string GetSchFullName()
        {
            string name = GetSchName();
            string from = StartDate.Value.ToShortDateString();
            string to = EndDate.Value.ToShortDateString();
            return name + Constants.S_Space + Constants.S_OpenPran + from + Constants.S_Comma + Constants.S_Space + to + Constants.S_ClosePran;
        }

        public string GetSchMonDayName()
        {
            string s = StartDate.Value.ToString("MMMM ");
            s = s + StartDate.Value.Day;

            if (EndDate.Value.Subtract(StartDate.Value).Days > 0)
            {
                if (StartDate.Value.Month == EndDate.Value.Month)
                {
                    s = s + Constants.S_Dash + EndDate.Value.Day;
                }
                else
                {
                    s = s + Constants.S_Dash + EndDate.Value.ToString(" MMMM ") + EndDate.Value.Day;
                }
            }

            return s;
        }
    }

    public class ProgramScheduleDay
    {
        public DayOfWeek DayOfWeek { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }
    }

    public class EarlyBird
    {
        [Required(ErrorMessage = "Price is required.")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Expire date is required.")]
        [DataType(DataType.DateTime)]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? ExpireDate { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Prior day")]
        public int PriorDay { get; set; }
    }

    public class TutationOption
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsProrate { get; set; }
    }

   
    public class ScheduleSession
    {
        public int Id { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public bool IsDeleted { get; set; }
        public long ScheduleId { get; set; }

    }

    public class ScheduleAfterBeforeCareAttribute : ScheduleBaseAttribute
    {
        public ScheduleAfterBeforeCareAttribute()
        {
            EarlyBirds = new List<EarlyBird>();
            PunchCard = new PunchCardAttribute();
        }

        public ContinueType ContinueType { get; set; }

        public int Occurances { get; set; }

        public bool IsProrate { get; set; }

        public DateTime ProrateTime { get; set; }

        public List<BeforeAfterCareSchedule> Subscriptions { get; set; }

        public List<ProgramScheduleDay> Days { get; set; }

        public List<EarlyBird> EarlyBirds { get; set; }

        public int PaymentDueDate { get; set; }

        public bool HasPayInFullOption { get; set; }

        public decimal FullPayDiscount { get; set; }

        public string FullPayDiscountMessage { get; set; }

        public bool EnableDropIn { get; set; }

        public string DropInSessionLabel { get; set; }

        public decimal DropInSessionPrice { get; set; }

        public bool EnableSameDropInPrice { get; set; }
        public decimal DropInSameSessionPrice { get; set; }
        public bool GetFirstPaymentAtTheRegisterTime { get; set; }
        public string PaymentScheduleMode { get; set; }
        public string MonthlyScheduleType { get; set; }
        public List<SelectKeyValue<int>> NumberOfClassDays { get; set; }

        public PunchCardAttribute PunchCard { get; set; }
    }

    public class BeforeAfterCareSchedule
    {
        public long Id { get; set; }

        [Display(Name = "Price for schedule")]
        [Required(ErrorMessage = "{0} is required.")]
        [Range(minimum: 0.01, maximum: double.MaxValue, ErrorMessage = "{0} is incorrect.")]
        public decimal Amount { get; set; }
        public RepeatType RepeatType { get; set; }
        public string Occurances { get; set; }
        public string DaysBeforeBilling { get; set; }

        [Display(Name = "Tuition label")]
        [Required(ErrorMessage = "{0} is required.")]
        public string PriceLabel { get; set; }

        public int? DueDate { get; set; }

        public int WeekDays { get; set; }

        public List<ProgramScheduleDay> Days { get; set; }
    }

    public class PunchCardAttribute
    {
        public string Title { get; set; }

        public bool Enabled { get; set; }

        public decimal? Amount { get; set; }

        public int SessionNumber { get; set; }
    }

    #region Enums
    public enum TournamentType
    {
        Cash = 0,
        Scholastic = 1,
        Team = 2
    }
    public enum WeekDaysMode
    {
        Any,
        SpecialDay
    }

    public enum TimeOfClassFormation : byte
    {
        None,
        AM,
        PM,
        Both,
    }
    
    #endregion
}
