﻿namespace Jumbula.Common.Constants
{
    public class ExceptionMessage
    {
        public const string ResourceNotAccessibleMessage = "Resource is not accessible";
        public const string ResourceNotAccessibleMessageFormat = "The resource with type {0} is not accessible";
        public const string IllegalStaffChange = "Illegal staff change";
    }
}
