﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using Jumbula.Common.Constants;

namespace Jumbula.Business
{
    public class FormBusiness : IFormBusiness
    {
        #region Fields
        private IPlayerProfileBusiness _playerProfileBusiness;
        #endregion

        #region Constructors
        public FormBusiness()
        {
        }
        #endregion

        #region Public methods
        public IPlayerProfileBusiness PlayerProfileBusiness
        {
            get { return _playerProfileBusiness; }
            set { _playerProfileBusiness = value; }
        }

        public JbForm GetDefaultRegistrationForm(bool loadForFirstTimeGenerate = false)
        {
            var jbform = new JbForm();

            _playerProfileBusiness.ProfileSectionsGenerator(jbform);

            jbform.ChangeElementsMode(AccessMode.Edit, AccessMode.ReadOnly);
            jbform.ChangeElementsMode(AccessMode.Design, AccessMode.ReadOnly);
            //jbform.ChangeElementsMode(AccessMode.Hidden, AccessMode.ReadOnly);

            if (loadForFirstTimeGenerate)
            {
                RemoveHiddenItems(jbform);
            }
            else
            {
                jbform.ChangeElementsMode(AccessMode.Hidden, AccessMode.ReadOnly);
            }

            return jbform;
        }

        public JbForm GetDefaultSchoolRegistrationForm(bool loadForFirstTimeGenerate = false)
        {
            var jbform = new JbForm();

            _playerProfileBusiness.ProfileSectionsGenerator2(jbform);

            jbform.ChangeElementsMode(AccessMode.Edit, AccessMode.ReadOnly);
            jbform.ChangeElementsMode(AccessMode.Design, AccessMode.ReadOnly);

            if (loadForFirstTimeGenerate)
            {
                RemoveHiddenItems(jbform);
            }
            else
            {
                jbform.ChangeElementsMode(AccessMode.Hidden, AccessMode.ReadOnly);
            }

            return jbform;
        }

        public JbForm GetDefaultTeamTournamnetRegistrationForm()
        {
            var jbform = new JbForm();
            jbform.RefEntityName = "Program";

            #region
            jbform.JsonElements = @"[{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox276911636','TypeTitle':'TextBox','Name':'Team name_fXfzLf','Title':'Team name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[{'Type':'Required','Message':'Team name is required','IsChecked':true}]}],'ElementId':'JbSection539523990','TypeTitle':'Section','Name':'Team Information_vodlGD','Title':'Team Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox1661937505','TypeTitle':'TextBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'ReadOnly','AccessRole':{},'Validations':[{'Type':'Required','Message':'First name is required','IsChecked':true}]},{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox341564108','TypeTitle':'TextBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'ReadOnly','AccessRole':{},'Validations':[{'Type':'Required','Message':'Last name is required','IsChecked':true}]},{'Type':'JbEmail','IsCustomElement':false,'InvalidEmailMessage':'Invalid email address.','Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbEmail543777500','TypeTitle':'Email','Name':'Email','Title':'Email address','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'ReadOnly','AccessRole':{},'Validations':[{'Type':'Required','Message':'Email is required','IsChecked':true}]},{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox417978464','TypeTitle':'TextBox','Name':'Organization_SQFJGz','Title':'Organization','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[{'Type':'Required','Message':'Organization is required','IsChecked':true}]}],'ElementId':'JbSection1834159348','TypeTitle':'Section','Name':'ParticipantSection','Title':'Registrant Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'ReadOnly','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox598375857','TypeTitle':'TextBox','Name':'FirstName_1','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[{'Type':'Required','Message':'First name is required','IsChecked':true}]},{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox269997459','TypeTitle':'TextBox','Name':'LastName_1','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[{'Type':'Required','Message':'Last name is required','IsChecked':true}]},{'Type':'JbDropDown','IsCustomElement':false,'DefaultText':null,'Items':[{'Value':'1','Text':'Not in school'},{'Value':'2','Text':'Preschool'},{'Value':'3','Text':'Kindergarten'},{'Value':'4','Text':'1'},{'Value':'5','Text':'2'},{'Value':'6','Text':'3'},{'Value':'7','Text':'4'},{'Value':'8','Text':'5'},{'Value':'9','Text':'6'},{'Value':'10','Text':'7'},{'Value':'11','Text':'8'},{'Value':'12','Text':'9'},{'Value':'13','Text':'10'},{'Value':'14','Text':'11'},{'Value':'15','Text':'12'},{'Value':'16','Text':'College'}],'Value':null,'IsRequired':false,'ElementId':'JbDropDown2074864159','TypeTitle':'DropDown','Name':'Grade','Title':'Grade','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'USCFInfo','Type.FullName':'SportsClub.Models.FormModels.USCFInfo,SportsClub','IsCustomElement':true,'PlayerName':null,'UscfId':null,'UscfExpiration':null,'UscfRatingReg':null,'UscfRatingQuick':null,'IsRequired':false,'AccessRole':{},'CurrentMode':'Edit','ElementId':'USCFInfo417574348','HelpText':null,'Size':'Medium','Title':'USCF info','Validations':[],'VisibleMode':'Both','TypeTitle':'USCF Info','Name':'USCF info_mRAfUl'}],'ElementId':'JbSection585451194','TypeTitle':'Section','Name':'Participant_1','Title':'Participant 1','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox63594902','TypeTitle':'TextBox','Name':'FirstName_2','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[{'Type':'Required','Message':'First name is required','IsChecked':true}]},{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox348493292','TypeTitle':'TextBox','Name':'LastName_2','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[{'Type':'Required','Message':'LastName is required','IsChecked':true}]},{'Type':'JbDropDown','IsCustomElement':false,'DefaultText':null,'Items':[{'Value':'1','Text':'Not in school'},{'Value':'2','Text':'Preschool'},{'Value':'3','Text':'Kindergarten'},{'Value':'4','Text':'1'},{'Value':'5','Text':'2'},{'Value':'6','Text':'3'},{'Value':'7','Text':'4'},{'Value':'8','Text':'5'},{'Value':'9','Text':'6'},{'Value':'10','Text':'7'},{'Value':'11','Text':'8'},{'Value':'12','Text':'9'},{'Value':'13','Text':'10'},{'Value':'14','Text':'11'},{'Value':'15','Text':'12'},{'Value':'16','Text':'College'}],'Value':null,'IsRequired':false,'ElementId':'JbDropDown2074864159','TypeTitle':'DropDown','Name':'Grade','Title':'Grade','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'USCFInfo','Type.FullName':'SportsClub.Models.FormModels.USCFInfo,SportsClub','IsCustomElement':true,'PlayerName':null,'UscfId':null,'UscfExpiration':null,'UscfRatingReg':null,'UscfRatingQuick':null,'IsRequired':false,'AccessRole':{},'CurrentMode':'Edit','ElementId':'USCFInfo683191576','HelpText':null,'Size':'Medium','Title':'USCF info','Validations':[],'VisibleMode':'Both','TypeTitle':'USCF Info','Name':'USCF info_KnAsGc'}],'ElementId':'JbSection228151173','TypeTitle':'Section','Name':'Participant_2','Title':'Participant 2','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox171341356','TypeTitle':'TextBox','Name':'FirstName_3','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[{'Type':'Required','Message':'First name is required','IsChecked':true}]},{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':true,'PlaceHolder':null,'ElementId':'JbTextBox612835040','TypeTitle':'TextBox','Name':'LastName_3','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[{'Type':'Required','Message':'Last name is required','IsChecked':true}]},{'Type':'JbDropDown','IsCustomElement':false,'DefaultText':null,'Items':[{'Value':'1','Text':'Not in school'},{'Value':'2','Text':'Preschool'},{'Value':'3','Text':'Kindergarten'},{'Value':'4','Text':'1'},{'Value':'5','Text':'2'},{'Value':'6','Text':'3'},{'Value':'7','Text':'4'},{'Value':'8','Text':'5'},{'Value':'9','Text':'6'},{'Value':'10','Text':'7'},{'Value':'11','Text':'8'},{'Value':'12','Text':'9'},{'Value':'13','Text':'10'},{'Value':'14','Text':'11'},{'Value':'15','Text':'12'},{'Value':'16','Text':'College'}],'Value':null,'IsRequired':false,'ElementId':'JbDropDown2074864159','TypeTitle':'DropDown','Name':'Grade','Title':'Grade','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'USCFInfo','Type.FullName':'SportsClub.Models.FormModels.USCFInfo,SportsClub','IsCustomElement':true,'PlayerName':null,'UscfId':null,'UscfExpiration':null,'UscfRatingReg':null,'UscfRatingQuick':null,'IsRequired':false,'AccessRole':{},'CurrentMode':'Edit','ElementId':'USCFInfo725671374','HelpText':null,'Size':'Medium','Title':'USCF info','Validations':[],'VisibleMode':'Both','TypeTitle':'USCF Info','Name':'USCF info_jEhfbJ'}],'ElementId':'JbSection947029313','TypeTitle':'Section','Name':'Participant_3','Title':'Participant 3','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox468047272','TypeTitle':'TextBox','Name':'FirstName_4','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox123847880','TypeTitle':'TextBox','Name':'LastName_4','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbDropDown','IsCustomElement':false,'DefaultText':null,'Items':[{'Value':'1','Text':'Not in school'},{'Value':'2','Text':'Preschool'},{'Value':'3','Text':'Kindergarten'},{'Value':'4','Text':'1'},{'Value':'5','Text':'2'},{'Value':'6','Text':'3'},{'Value':'7','Text':'4'},{'Value':'8','Text':'5'},{'Value':'9','Text':'6'},{'Value':'10','Text':'7'},{'Value':'11','Text':'8'},{'Value':'12','Text':'9'},{'Value':'13','Text':'10'},{'Value':'14','Text':'11'},{'Value':'15','Text':'12'},{'Value':'16','Text':'College'}],'Value':null,'IsRequired':false,'ElementId':'JbDropDown2074864159','TypeTitle':'DropDown','Name':'Grade','Title':'Grade','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'USCFInfo','Type.FullName':'SportsClub.Models.FormModels.USCFInfo,SportsClub','IsCustomElement':true,'PlayerName':null,'UscfId':null,'UscfExpiration':null,'UscfRatingReg':null,'UscfRatingQuick':null,'IsRequired':false,'AccessRole':{},'CurrentMode':'Edit','ElementId':'USCFInfo158072024','HelpText':null,'Size':'Medium','Title':'USCF info','Validations':[],'VisibleMode':'Both','TypeTitle':'USCF Info','Name':'USCF info_BhrMdq'}],'ElementId':'JbSection774932809','TypeTitle':'Section','Name':'Participant_4','Title':'Participant 4','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox274202322','TypeTitle':'TextBox','Name':'FirstName_5','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox555606109','TypeTitle':'TextBox','Name':'LastName_5','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbDropDown','IsCustomElement':false,'DefaultText':null,'Items':[{'Value':'1','Text':'Not in school'},{'Value':'2','Text':'Preschool'},{'Value':'3','Text':'Kindergarten'},{'Value':'4','Text':'1'},{'Value':'5','Text':'2'},{'Value':'6','Text':'3'},{'Value':'7','Text':'4'},{'Value':'8','Text':'5'},{'Value':'9','Text':'6'},{'Value':'10','Text':'7'},{'Value':'11','Text':'8'},{'Value':'12','Text':'9'},{'Value':'13','Text':'10'},{'Value':'14','Text':'11'},{'Value':'15','Text':'12'},{'Value':'16','Text':'College'}],'Value':null,'IsRequired':false,'ElementId':'JbDropDown2074864159','TypeTitle':'DropDown','Name':'Grade','Title':'Grade','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'USCFInfo','Type.FullName':'SportsClub.Models.FormModels.USCFInfo,SportsClub','IsCustomElement':true,'PlayerName':null,'UscfId':null,'UscfExpiration':null,'UscfRatingReg':null,'UscfRatingQuick':null,'IsRequired':false,'AccessRole':{},'CurrentMode':'Edit','ElementId':'USCFInfo413351110','HelpText':null,'Size':'Medium','Title':'USCF info','Validations':[],'VisibleMode':'Both','TypeTitle':'USCF Info','Name':'USCF info_lEDOfb'}],'ElementId':'JbSection089150580','TypeTitle':'Section','Name':'Participant_5','Title':'Participant 5','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox306783236','TypeTitle':'TextBox','Name':'FirstName_6','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbTextBox','IsCustomElement':false,'Value':null,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox647336470','TypeTitle':'TextBox','Name':'LastName_6','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbDropDown','IsCustomElement':false,'DefaultText':null,'Items':[{'Value':'1','Text':'Not in school'},{'Value':'2','Text':'Preschool'},{'Value':'3','Text':'Kindergarten'},{'Value':'4','Text':'1'},{'Value':'5','Text':'2'},{'Value':'6','Text':'3'},{'Value':'7','Text':'4'},{'Value':'8','Text':'5'},{'Value':'9','Text':'6'},{'Value':'10','Text':'7'},{'Value':'11','Text':'8'},{'Value':'12','Text':'9'},{'Value':'13','Text':'10'},{'Value':'14','Text':'11'},{'Value':'15','Text':'12'},{'Value':'16','Text':'College'}],'Value':null,'IsRequired':false,'ElementId':'JbDropDown2074864159','TypeTitle':'DropDown','Name':'Grade','Title':'Grade','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'USCFInfo','Type.FullName':'SportsClub.Models.FormModels.USCFInfo,SportsClub','IsCustomElement':true,'PlayerName':null,'UscfId':null,'UscfExpiration':null,'UscfRatingReg':null,'UscfRatingQuick':null,'IsRequired':false,'AccessRole':{},'CurrentMode':'Edit','ElementId':'USCFInfo122227664','HelpText':null,'Size':'Medium','Title':'USCF info','Validations':[],'VisibleMode':'Both','TypeTitle':'USCF Info','Name':'USCF info_UWnQTj'}],'ElementId':'JbSection344372575','TypeTitle':'Section','Name':'Participant_6','Title':'Participant 6','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}]";
            jbform.JsonElements.Replace("'", "\"");
            #endregion

            return jbform;
        }

        public SchoolGradeType? GetPlayerGrade(JbForm jbForm)
        {
            SchoolGradeType? result = null;
            object gradeObject = null;
            IJbBaseElement gradeElement = null;


            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
            {
                gradeElement = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString());

            }
            else if (jbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
            {
                gradeElement = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString());
            }

            if (gradeElement != null)
            {
                gradeObject = gradeElement.GetValue();

                result = GetRightGradeFromDropDown(gradeObject);
            }

            return result;
        }

        public JbDropDown GetGradeElement(JbForm jbForm)
        {
            IJbBaseElement gradeElement = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
            {
                gradeElement = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString());

            }
            else if (jbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
            {
                gradeElement = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString());
            }

            return gradeElement != null ? gradeElement as JbDropDown : null;
        }

        public string GetPlayerGradeAsString(JbForm jbForm, bool showAbbreviationGrade = false)
        {
            string result = null;

            try
            {
                var grade = GetPlayerGrade(jbForm);

                if (grade.HasValue)
                {
                    result = showAbbreviationGrade ? grade.Value.ToAbbreviation() : grade.Value.ToDescription();
                }
            }
            catch
            {
                result = null;
            }

            if (string.IsNullOrEmpty(result))
            {
                if (jbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
                {
                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString()).GetValue() != null ? ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString()).GetValue().ToString() : string.Empty;
                }

                if (string.IsNullOrEmpty(result) && jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name.StartsWith(ElementsName.Grade.ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name.StartsWith(ElementsName.Grade.ToString(), StringComparison.OrdinalIgnoreCase)).GetValue() != null ? ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name.StartsWith(ElementsName.Grade.ToString(), StringComparison.OrdinalIgnoreCase)).GetValue().ToString() : string.Empty;
                }
            }


            return result;
        }

        public string GetPlayerGradeOrAgeAsString(JbForm jbForm)
        {
            string result = null;

            try
            {
                var grade = GetPlayerGrade(jbForm);

                if (grade.HasValue)
                {
                    result = grade.Value.ToDescription();
                }
            }
            catch
            {
                result = null;
            }

            if (string.IsNullOrEmpty(result))
            {
                if (jbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
                {
                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString()).GetValue() != null ? ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString()).GetValue().ToString() : string.Empty;
                }

                if (string.IsNullOrEmpty(result) && jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name.StartsWith(ElementsName.Grade.ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name.StartsWith(ElementsName.Grade.ToString(), StringComparison.OrdinalIgnoreCase)).GetValue() != null ? ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name.StartsWith(ElementsName.Grade.ToString(), StringComparison.OrdinalIgnoreCase)).GetValue().ToString() : string.Empty;
                }
            }
            if (string.IsNullOrEmpty(result))
            {
                if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
                {
                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.DoB.ToString()).GetValue() != null ? ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.DoB.ToString()).GetValue().ToString() : string.Empty;
                }

                if (result != null)
                {
                    DateTime dob = DateTime.Parse(result);
                    result = DateTimeHelper.CalculateAge(dob).ToString();
                }
            }

            return result;
        }

        public string GetPlayerFirstName(JbForm jbForm)
        {

            string result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
            {
                try
                {
                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue().ToString();
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public string GetPlayerLastName(JbForm jbForm)
        {
            string result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.LastName.ToString()))
            {
                try
                {
                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue().ToString();
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public Genders? GetPlayerGender(JbForm jbForm)
        {

            Genders? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                try
                {
                    var gender = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.Gender.ToString()).GetValue().ToString();
                    result = EnumHelper.ParseEnum<Genders>(gender);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public GenderCategories GetParticipantGender(JbForm jbForm)
        {

            GenderCategories result = GenderCategories.None;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                try
                {
                    var gender = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.Gender.ToString()).GetValue().ToString();
                    result = EnumHelper.ParseEnum<GenderCategories>(gender.Replace(" ", ""));
                }
                catch
                {
                    result = GenderCategories.None;
                }
            }

            return result;
        }
        public GenderCategories GetParent1Gender(JbForm jbForm)
        {

            GenderCategories result = GenderCategories.None;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParentGuardianSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParentGuardianSection.ToString())).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                try
                {
                    var gender = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParentGuardianSection.ToString())).Elements.Single(e => e.Name == ElementsName.Gender.ToString()).GetValue().ToString();
                    result = EnumHelper.ParseEnum<GenderCategories>(gender);
                }
                catch
                {
                    result = GenderCategories.None;
                }
            }

            return result;
        }
        public string GetPlayerSchoolName(JbForm jbForm)
        {

            var result = string.Empty;

            if (!string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.SchoolName, new List<string> { "Name" })))
            {
                result = GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.SchoolName, new List<string> { "Name" });
            }
            else
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetPlayerHomeRoom(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Any(e => e.Name == ElementsName.Homeroom.ToString()))
            {
                try
                {
                    var homeRoom = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.Homeroom.ToString()).GetValue().ToString();
                    result = homeRoom;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public bool? GetPlayerHasSpecialNeeds(JbForm jbForm)
        {

            bool? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.HasMedicalConditions.ToString()))
            {
                try
                {
                    var hasSpecialNeeds = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.HasMedicalConditions.ToString()).GetValue().ToString();

                    result = hasSpecialNeeds.ToLower() == "yes" ? true : false;
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public bool? GetPlayerHasAllergeis(JbForm jbForm)
        {

            bool? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.HasAllergiesInfo.ToString()))
            {
                try
                {
                    var hasAllergyInfo = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.HasAllergiesInfo.ToString()).GetValue().ToString();

                    result = hasAllergyInfo.ToLower() == "yes" ? true : false;
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public string GetPlayerAllergies(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.AllergiesInfo.ToString()))
            {
                try
                {
                    var allergiesInfo = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.AllergiesInfo.ToString()).GetValue().ToString();
                    result = allergiesInfo;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public string GetPlayerDoctorPhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.DoctorPhone.ToString()))
            {
                try
                {
                    var doctorPhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.DoctorPhone.ToString()).GetValue().ToString();
                    result = doctorPhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public string GetPlayerDoctorName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.DoctorName.ToString()))
            {
                try
                {
                    var doctorName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.DoctorName.ToString()).GetValue().ToString();
                    result = doctorName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public bool? GetPlayerSelfAdministerMedication(JbForm jbForm)
        {

            bool? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.SelfAdministerMedication.ToString()))
            {
                try
                {
                    var selfAdministerMedication = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.SelfAdministerMedication.ToString()).GetValue().ToString();

                    result = selfAdministerMedication.ToLower() == "yes" ? true : false;
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public string GetPlayerSelfAdministerMedicationDescription(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.SelfAdministerMedicationDescription.ToString()))
            {
                try
                {
                    var selfAdministerMedicationDescription = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.SelfAdministerMedicationDescription.ToString()).GetValue().ToString();
                    result = selfAdministerMedicationDescription;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public string GetPlayerNutAllergyDescription(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.NutAllergyDescription.ToString()))
            {
                try
                {
                    var nutAllergyDescription = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.NutAllergyDescription.ToString()).GetValue().ToString();
                    result = nutAllergyDescription;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public bool? GetPlayerNutAllergy(JbForm jbForm)
        {

            bool? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.NutAllergy.ToString()))
            {
                try
                {
                    var nutAllergy = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.NutAllergy.ToString()).GetValue().ToString();
                    result = nutAllergy.ToLower() == "yes" ? true : false;
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public string GetPlayerDoctorLocation(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.Address.ToString()))
            {
                try
                {
                    var doctorLocation = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.Address.ToString()).GetValue().ToString();
                    result = doctorLocation;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public string GetAuthorizePickup1FirstName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup1.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
            {
                try
                {
                    var authorizeFistName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue().ToString();
                    result = authorizeFistName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup1LastName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup1.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Any(e => e.Name == ElementsName.LastName.ToString()))
            {
                try
                {
                    var authorizeLastName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue().ToString();
                    result = authorizeLastName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup1PrimaryPhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup1.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Any(e => e.Name == ElementsName.Phone.ToString()))
            {
                try
                {
                    var primaryPhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Single(e => e.Name == ElementsName.Phone.ToString()).GetValue().ToString();
                    result = primaryPhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup1AlternatePhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup1.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Any(e => e.Name == ElementsName.AlternatePhone.ToString()))
            {
                try
                {
                    var alternatePhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Single(e => e.Name == ElementsName.AlternatePhone.ToString()).GetValue().ToString();
                    result = alternatePhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public Relationship? GetAuthorizePickup1Relationship(JbForm jbForm)
        {

            Relationship? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup1.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Any(e => e.Name == ElementsName.Relationship.ToString()))
            {
                try
                {
                    var relationShip = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Single(e => e.Name == ElementsName.Relationship.ToString()).GetValue().ToString();
                    var newRelationShip = relationShip != null ? relationShip.Replace("/", "").Replace(" ", "") : null;
                    result = EnumHelper.ParseEnum<Relationship>(newRelationShip);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public bool GetAuthorizePickup1IsEmergencyContact(JbForm jbForm)
        {

            bool result = false;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup1.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Any(e => e.Name == ElementsName.IsEmergencyContact.ToString()))
            {
                try
                {
                    var isEmergencyContact = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup1.ToString())).Elements.Single(e => e.Name == ElementsName.IsEmergencyContact.ToString()).GetValue().ToString();
                    result = Convert.ToBoolean(isEmergencyContact);
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }
        public string GetAuthorizePickup2FirstName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup2.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
            {
                try
                {
                    var authorizeFistName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue().ToString();
                    result = authorizeFistName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup2LastName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup2.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Any(e => e.Name == ElementsName.LastName.ToString()))
            {
                try
                {
                    var authorizeLastName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue().ToString();
                    result = authorizeLastName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup2PrimaryPhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup2.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Any(e => e.Name == ElementsName.Phone.ToString()))
            {
                try
                {
                    var primaryPhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Single(e => e.Name == ElementsName.Phone.ToString()).GetValue().ToString();
                    result = primaryPhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup2AlternatePhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup2.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Any(e => e.Name == ElementsName.AlternatePhone.ToString()))
            {
                try
                {
                    var alternatePhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Single(e => e.Name == ElementsName.AlternatePhone.ToString()).GetValue().ToString();
                    result = alternatePhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public Relationship? GetAuthorizePickup2Relationship(JbForm jbForm)
        {

            Relationship? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup2.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Any(e => e.Name == ElementsName.Relationship.ToString()))
            {
                try
                {
                    var relationShip = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Single(e => e.Name == ElementsName.Relationship.ToString()).GetValue().ToString();
                    var newRelationShip = relationShip != null ? relationShip.Replace("/", "").Replace(" ", "") : null;
                    result = EnumHelper.ParseEnum<Relationship>(newRelationShip);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public bool GetAuthorizePickup2IsEmergencyContact(JbForm jbForm)
        {

            bool result = false;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup2.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Any(e => e.Name == ElementsName.IsEmergencyContact.ToString()))
            {
                try
                {
                    var isEmergencyContact = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup2.ToString())).Elements.Single(e => e.Name == ElementsName.IsEmergencyContact.ToString()).GetValue().ToString();
                    result = Convert.ToBoolean(isEmergencyContact);
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }
        public string GetAuthorizePickup3FirstName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup3.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
            {
                try
                {
                    var authorizeFistName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue().ToString();
                    result = authorizeFistName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup3LastName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup3.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Any(e => e.Name == ElementsName.LastName.ToString()))
            {
                try
                {
                    var authorizeLastName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue().ToString();
                    result = authorizeLastName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup3PrimaryPhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup3.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Any(e => e.Name == ElementsName.Phone.ToString()))
            {
                try
                {
                    var primaryPhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Single(e => e.Name == ElementsName.Phone.ToString()).GetValue().ToString();
                    result = primaryPhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup3AlternatePhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup3.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Any(e => e.Name == ElementsName.AlternatePhone.ToString()))
            {
                try
                {
                    var alternatePhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Single(e => e.Name == ElementsName.AlternatePhone.ToString()).GetValue().ToString();
                    result = alternatePhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public Relationship? GetAuthorizePickup3Relationship(JbForm jbForm)
        {
            Relationship? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup3.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Any(e => e.Name == ElementsName.Relationship.ToString()))
            {
                try
                {
                    var relationShip = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Single(e => e.Name == ElementsName.Relationship.ToString()).GetValue().ToString();
                    var newRelationShip = relationShip != null ? relationShip.Replace("/", "").Replace(" ", "") : null;
                    result = EnumHelper.ParseEnum<Relationship>(newRelationShip);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public bool GetAuthorizePickup3IsEmergencyContact(JbForm jbForm)
        {

            bool result = false;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup3.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Any(e => e.Name == ElementsName.IsEmergencyContact.ToString()))
            {
                try
                {
                    var isEmergencyContact = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup3.ToString())).Elements.Single(e => e.Name == ElementsName.IsEmergencyContact.ToString()).GetValue().ToString();
                    result = Convert.ToBoolean(isEmergencyContact);
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }
        public string GetAuthorizePickup4FirstName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup4.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
            {
                try
                {
                    var authorizeFistName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue().ToString();
                    result = authorizeFistName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup4LastName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup4.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Any(e => e.Name == ElementsName.LastName.ToString()))
            {
                try
                {
                    var authorizeLastName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue().ToString();
                    result = authorizeLastName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup4PrimaryPhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup4.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Any(e => e.Name == ElementsName.Phone.ToString()))
            {
                try
                {
                    var primaryPhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Single(e => e.Name == ElementsName.Phone.ToString()).GetValue().ToString();
                    result = primaryPhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public string GetAuthorizePickup4AlternatePhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup4.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Any(e => e.Name == ElementsName.AlternatePhone.ToString()))
            {
                try
                {
                    var alternatePhone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Single(e => e.Name == ElementsName.AlternatePhone.ToString()).GetValue().ToString();
                    result = alternatePhone;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }
        public Relationship? GetAuthorizePickup4Relationship(JbForm jbForm)
        {
            Relationship? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup4.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Any(e => e.Name == ElementsName.Relationship.ToString()))
            {
                try
                {
                    var relationShip = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Single(e => e.Name == ElementsName.Relationship.ToString()).GetValue().ToString();
                    var newRelationShip = relationShip != null ? relationShip.Replace("/", "").Replace(" ", "") : null;
                    result = EnumHelper.ParseEnum<Relationship>(newRelationShip);
                }
                catch (Exception ex)
                {
                    result = null;
                }
            }

            return result;
        }

        public bool GetAuthorizePickup4IsEmergencyContact(JbForm jbForm)
        {

            bool result = false;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.AuthorizedPickup4.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Any(e => e.Name == ElementsName.IsEmergencyContact.ToString()))
            {
                try
                {
                    var isEmergencyContact = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.AuthorizedPickup4.ToString())).Elements.Single(e => e.Name == ElementsName.IsEmergencyContact.ToString()).GetValue().ToString();
                    result = Convert.ToBoolean(isEmergencyContact);
                }
                catch
                {
                    result = false;
                }
            }

            return result;
        }

        public string GetPlayerMedicalConditions(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.HealthSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Any(e => e.Name == ElementsName.MedicalConditions.ToString()))
            {
                try
                {
                    var medicalConditions = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.HealthSection.ToString())).Elements.Single(e => e.Name == ElementsName.MedicalConditions.ToString()).GetValue().ToString();
                    result = medicalConditions;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public bool? GetPermissionPhotography(JbForm jbForm)
        {

            bool? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.PhotographySection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.PhotographySection.ToString())).Elements.Any(e => e.Name == ElementsName.PermissionPhotography.ToString()))
            {
                try
                {
                    var permissionPhotography = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.PhotographySection.ToString())).Elements.Single(e => e.Name == ElementsName.PermissionPhotography.ToString()).GetValue().ToString();
                    result = permissionPhotography.ToLower() == "yes" ? true : false;
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public string GetInsuranceCompanyName(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.InsuranceSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.InsuranceSection.ToString())).Elements.Any(e => e.Name == ElementsName.CompanyName.ToString()))
            {
                try
                {
                    var companyName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.InsuranceSection.ToString())).Elements.Single(e => e.Name == ElementsName.CompanyName.ToString()).GetValue().ToString();
                    result = companyName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public string GetInsuranceCompanyPhone(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.InsuranceSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.InsuranceSection.ToString())).Elements.Any(e => e.Name == ElementsName.CompanyPhone.ToString()))
            {
                try
                {
                    var companyName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.InsuranceSection.ToString())).Elements.Single(e => e.Name == ElementsName.CompanyPhone.ToString()).GetValue().ToString();
                    result = companyName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public string GetInsurancePolicyNumber(JbForm jbForm)
        {

            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.InsuranceSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.InsuranceSection.ToString())).Elements.Any(e => e.Name == ElementsName.PolicyNumber.ToString()))
            {
                try
                {
                    var companyName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.InsuranceSection.ToString())).Elements.Single(e => e.Name == ElementsName.PolicyNumber.ToString()).GetValue().ToString();
                    result = companyName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public JbAddress GetPlayerAddress(JbForm jbForm)
        {

            JbAddress result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ContactSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ContactSection.ToString())).Elements.Any(e => e.Name == ElementsName.Address.ToString()))
            {
                try
                {
                    result = (JbAddress)((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ContactSection.ToString())).Elements.Single(e => e.Name == ElementsName.Address.ToString());
                }
                catch
                {
                    result = null;
                }
            }

            //if (result != null && string.IsNullOrEmpty(result.AddressLine1) && string.IsNullOrEmpty(result.AddressLine2) && string.IsNullOrEmpty(result.State) && string.IsNullOrEmpty(result.City) && string.IsNullOrEmpty(result.Zip))
            //{
            //    result = null;
            //}

            return result;
        }

        public object GetPlayerGradeObject(JbForm jbForm)
        {
            object result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
            {
                result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString()).GetValue();
            }

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
            {
                result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.Grade.ToString()).GetValue();
            }

            return result;
        }

        public JbAddress GetParentAddress(JbForm jbForm, SectionsName sectionName)
        {
            JbAddress result = null;

            if (sectionName == SectionsName.ContactSection && GetPlayerAddress(jbForm) != null)
            {
                result = GetPlayerAddress(jbForm);
            }
            else
            {
                if (jbForm.Elements.Any(s => s.Name == sectionName.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Any(e => e.Name.StartsWith(ElementsName.Address.ToString(), StringComparison.OrdinalIgnoreCase)))
                {
                    try
                    {
                        result = (JbAddress)((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Single(e => e.Name.StartsWith(ElementsName.Address.ToString(), StringComparison.OrdinalIgnoreCase));
                    }
                    catch
                    {
                        result = null;
                    }
                }
            }

            //if(result != null && string.IsNullOrEmpty(result.AddressLine1) && string.IsNullOrEmpty(result.AddressLine2) && string.IsNullOrEmpty(result.State) && string.IsNullOrEmpty(result.City) && string.IsNullOrEmpty(result.Zip))
            //{
            //    result = null;
            //}

            return result;
        }

        public JbAddress GetParentAddress(JbForm jbForm)
        {
            var result = new JbAddress();

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParentGuardianSection.ToString()) && GetParentAddress(jbForm, SectionsName.ParentGuardianSection) != null)
            {
                result = GetParentAddress(jbForm, SectionsName.ParentGuardianSection);
            }
            else if (jbForm.Elements.Any(s => s.Name == SectionsName.Parent2GuardianSection.ToString()) && GetParentAddress(jbForm, SectionsName.Parent2GuardianSection) != null)
            {
                result = GetParentAddress(jbForm, SectionsName.Parent2GuardianSection);
            }
            else
            {
                result = GetParentAddress(jbForm, SectionsName.ContactSection);
            }

            return result;
        }

        public JbAddress GetParent1Address(JbForm jbForm)
        {
            var result = new JbAddress();

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParentGuardianSection.ToString()) && GetParentAddress(jbForm, SectionsName.ParentGuardianSection) != null)
            {
                try
                {
                    result = (JbAddress)((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParentGuardianSection.ToString())).Elements.Single(e => e.Name.StartsWith(ElementsName.Address.ToString(), StringComparison.OrdinalIgnoreCase));
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public JbAddress GetParent2Address(JbForm jbForm)
        {
            var result = new JbAddress();

            if (jbForm.Elements.Any(s => s.Name == SectionsName.Parent2GuardianSection.ToString()) && GetParentAddress(jbForm, SectionsName.Parent2GuardianSection) != null)
            {
                try
                {
                    result = (JbAddress)((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.Parent2GuardianSection.ToString())).Elements.Single(e => e.Name.StartsWith(ElementsName.Address.ToString(), StringComparison.OrdinalIgnoreCase));
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public string GetFormValue(JbForm jbForm, SectionsName sectionName, ElementsName elementName, List<string> alternativeElementNames = null)
        {
            string result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == sectionName.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Any(e => e.Name == elementName.ToString()))
            {
                try
                {

                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Single(e => e.Name == elementName.ToString()).GetValue().ToString();
                }
                catch
                {
                    result = string.Empty;
                }
            }

            if (string.IsNullOrEmpty(result) && alternativeElementNames != null)
            {
                result = GetFormValue(jbForm, sectionName, alternativeElementNames);
            }

            return result;
        }

        public string GetFormValue(JbForm jbForm, SectionsName sectionName, string elementName, bool checkContins = false)
        {
            string result = string.Empty;

            try
            {
                if (checkContins)
                {
                    if (jbForm.Elements.Any(s => s.Name == sectionName.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Any(e => e.Name.ToLower().Contains(elementName.ToLower())))
                    {
                        result = ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.First(e => e.Name.ToLower().Contains(elementName.ToLower())).GetValue().ToString();
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name == sectionName.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Any(e => e.Name.StartsWith(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.First(e => e.Name.StartsWith(elementName, StringComparison.OrdinalIgnoreCase)).GetValue().ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetFormValue(JbForm jbForm, SectionsName sectionName, List<string> elementNames)
        {
            string result = string.Empty;

            try
            {
                foreach (var elementName in elementNames)
                {
                    if (jbForm.Elements.Any(s => s.Name == sectionName.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Any(e => e.Name.StartsWith(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.First(e => e.Name.StartsWith(elementName, StringComparison.OrdinalIgnoreCase)).GetValue().ToString();

                        if (!string.IsNullOrEmpty(result))
                        {
                            break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetFormValue(JbForm jbForm, string sectionName, string elementName, bool checkContins = false)
        {
            string result = string.Empty;

            try
            {
                if (checkContins)
                {
                    if (jbForm.Elements.Any(s => s.Name.StartsWith(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Any(e => e.Name.ToLower().Contains(elementName.ToLower())))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.StartsWith(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.ToLower().Contains(elementName.ToLower())).GetValue().ToString();
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.StartsWith(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.StartsWith(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.StartsWith(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.StartsWith(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.StartsWith(elementName, StringComparison.OrdinalIgnoreCase)).GetValue().ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetTeacher(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.TeacherName, new List<string> { "Teacher", "Morning Homeroom Teacher" }) != "Teacher not listed")
                {
                    result = GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.TeacherName, new List<string> { "Teacher", "Morning Homeroom Teacher" });
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.TeacherNotListed);
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            if (string.IsNullOrEmpty(result))
            {
                result = GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.TeacherNotListed);
            }
            return result;
        }

        public List<JbElementItem> GetTeacherItems(JbForm jbForm)
        {
            JbDropDown result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.SchoolSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Any(e => e.Name == ElementsName.TeacherName.ToString()))
            {
                try
                {
                    result = (JbDropDown)((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.SchoolSection.ToString())).Elements.Single(e => e.Name == ElementsName.TeacherName.ToString());
                }
                catch
                {
                    result = null;
                }
            }

            return result != null ? result.Items : null;


        }
        public string GetAuthorizedAdult(JbForm jbForm, SectionsName? sectionName)
        {
            string result = string.Empty;

            try
            {
                if (sectionName.HasValue)
                {
                    return string.Format("{0} {1}", GetFormValue(jbForm, sectionName.Value, ElementsName.FirstName), GetFormValue(jbForm, sectionName.Value, ElementsName.LastName));
                }
                else
                {
                    if (!string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Authorized Adult")))
                    {
                        return GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Authorized Adult");
                    }
                    else
                    {
                        return string.Format("{0} {1}", GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.FirstName), GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.LastName));
                    }
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;

        }

        public string GetAuthorizedAdultPhone(JbForm jbForm, SectionsName? sectionName)
        {
            string result = string.Empty;

            try
            {
                if (sectionName.HasValue)
                {
                    return GetFormValue(jbForm, sectionName.Value, ElementsName.Phone);
                }
                else
                {
                    if (!string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Authorized Adult Phone")))
                    {
                        return GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Authorized Adult Phone");
                    }
                    else
                    {
                        return GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Phone);
                    }
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;

        }

        public void RemoveHiddenItems(JbForm jbForm)
        {
            jbForm.Elements.RemoveAll(e => e.CurrentMode == AccessMode.Hidden);

            jbForm.Elements.ForEach(e => ((JbSection)e).Elements.RemoveAll(r => r.CurrentMode == AccessMode.Hidden));
        }

        public string GetFormValue(JbForm jbForm, string elementName)
        {
            string result = null;

            try
            {
                if (jbForm.Elements.Select(e => (JbSection)e)
                               .SelectMany(e => e.Elements)
                               .SingleOrDefault(e => e.Name.ToLower()
                               .Contains(elementName.ToLower())) != null)
                {
                    result = jbForm.Elements
                        .Select(e => e as JbSection)
                        .SelectMany(e => e.Elements)
                        .SingleOrDefault(e => e.Name.ToLower()
                        .Contains(elementName.ToLower()))
                        .GetValue().ToString();
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        public List<IJbBaseElement> GetFormElements(JbForm jbForm, string elementName)
        {
            List<IJbBaseElement> result = null;

            try
            {
                result = jbForm.Elements
                    .Select(e => e as JbSection)
                    .SelectMany(e => e.Elements)
                    .Where(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase))
                    .ToList();
            }
            catch
            {
                result = null;
            }

            return result;
        }

        private SchoolGradeType? GetRightGradeFromDropDown(object grade)
        {
            SchoolGradeType? result = null;

            try
            {
                var dic = new Dictionary<string, SchoolGradeType>();
                dic.Add("not in school", SchoolGradeType.NotInSchool);
                dic.Add("preschool - 3", SchoolGradeType.PreSchool3);
                dic.Add("preschool - 4", SchoolGradeType.PreSchool4);
                dic.Add("preschool", SchoolGradeType.PreSchool);
                dic.Add("kindergarten", SchoolGradeType.Kindergarten);
                dic.Add("1", SchoolGradeType.S1);
                dic.Add("2", SchoolGradeType.S2);
                dic.Add("3", SchoolGradeType.S3);
                dic.Add("4", SchoolGradeType.S4);
                dic.Add("5", SchoolGradeType.S5);
                dic.Add("6", SchoolGradeType.S6);
                dic.Add("7", SchoolGradeType.S7);
                dic.Add("8", SchoolGradeType.S8);
                dic.Add("9", SchoolGradeType.S9);
                dic.Add("10", SchoolGradeType.S10);
                dic.Add("11", SchoolGradeType.S11);
                dic.Add("12", SchoolGradeType.S12);
                dic.Add("college", SchoolGradeType.College);

                if (grade != null)
                {
                    result = dic[grade.ToString().ToLower()];
                }
            }
            catch
            {

            }

            return result;
        }

        public string GetParentPhone(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" });

            if (string.IsNullOrEmpty(result))
            {
                result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Home Phone", "Work Phone" });

                if (string.IsNullOrEmpty(result))
                {
                    result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Home Phone", "Work Phone" });
                }
            }

            return result;
        }
        public string GetParent2Phone(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Home Phone", "Work Phone" });

            if (string.IsNullOrEmpty(result))
            {
                result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Home Phone", "Work Phone" });
            }


            return result;
        }
        public string GetParentOccupation(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Occupation);

            if (string.IsNullOrEmpty(result))
            {
                result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Occupation);
            }

            return result;
        }
        public string GetParentEmployer(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Employer);

            if (string.IsNullOrEmpty(result))
            {
                result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Employer);
            }

            return result;
        }

        public string GetParent2Occupation(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Occupation);

            if (string.IsNullOrEmpty(result))
            {
                result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Occupation);
            }

            return result;
        }
        public string GetParent2Employer(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Employer);

            if (string.IsNullOrEmpty(result))
            {
                result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Employer);
            }

            return result;
        }
        public List<string> GetAuthorizedAdultNames(JbForm jbForm)
        {
            var result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Authorized Adult")))
                {
                    result.Add(GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Authorized Adult"));
                    return result;
                }
                else
                {
                    result.Add(string.Format("{0} {1}", GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.FirstName), GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.LastName)));
                    result.Add(string.Format("{0} {1}", GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.FirstName), GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.LastName)));
                    result.Add(string.Format("{0} {1}", GetFormValue(jbForm, SectionsName.AuthorizedPickup3, ElementsName.FirstName), GetFormValue(jbForm, SectionsName.AuthorizedPickup3, ElementsName.LastName)));
                    result.Add(string.Format("{0} {1}", GetFormValue(jbForm, SectionsName.AuthorizedPickup4, ElementsName.FirstName), GetFormValue(jbForm, SectionsName.AuthorizedPickup4, ElementsName.LastName)));
                    return result;
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return null;
        }


        public List<string> GetAuthorizedAdultPhones(JbForm jbForm)
        {
            var result = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Authorized Adult Phone")))
                {
                    result.Add(GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Authorized Adult Phone"));
                    return result;
                }
                else
                {
                    result.Add(PhoneNumberHelper.FormatPhoneNumber(GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Phone)));
                    result.Add(PhoneNumberHelper.FormatPhoneNumber(GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.Phone)));
                    result.Add(PhoneNumberHelper.FormatPhoneNumber(GetFormValue(jbForm, SectionsName.AuthorizedPickup3, ElementsName.Phone)));
                    result.Add(PhoneNumberHelper.FormatPhoneNumber(GetFormValue(jbForm, SectionsName.AuthorizedPickup4, ElementsName.Phone)));
                    return result;
                }
            }
            catch (Exception ex)
            {
                result = null;
            }

            return null;
        }
        public string GetPlayerEmail(JbForm jbForm)
        {

            string result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Any(e => e.Name == ElementsName.Email.ToString()))
            {
                try
                {
                    result = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.Email.ToString()).GetValue().ToString();

                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public string GetParentEmail(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Email);

            if (string.IsNullOrEmpty(result))
            {
                result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Email);
            }

            return result;
        }

        public string GetParent2Email(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Email);

            return result;
        }

        public string GetParentFirstName(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName);

            return result;
        }
        public string GetParentLastName(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.LastName);

            return result;
        }
        public string GetParent2FirstName(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName);

            return result;
        }

        public string GetParent2LastName(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName);

            return result;
        }
        public string GetParent2CellPhone(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone);

            return result;
        }
        public string GetParent2AlternatePhone(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.AlternatePhone);

            return result;
        }
        public string GetParent1AlternatePhone(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.AlternatePhone);

            return result;
        }
        public ParentRelationship? GetParent1RelationshipEnum(JbForm jbForm)
        {
            ParentRelationship? result = null;


            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParentGuardianSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParentGuardianSection.ToString())).Elements.Any(e => e.Name == ElementsName.ParentRelationship.ToString()))
            {
                try
                {
                    var parentRelationship = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParentGuardianSection.ToString())).Elements.Single(e => e.Name == ElementsName.ParentRelationship.ToString()).GetValue().ToString();
                    result = EnumHelper.ParseEnum<ParentRelationship>(parentRelationship);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public ParentRelationship? GetParent2RelationshipEnum(JbForm jbForm)
        {
            ParentRelationship? result = null;


            if (jbForm.Elements.Any(s => s.Name == SectionsName.Parent2GuardianSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.Parent2GuardianSection.ToString())).Elements.Any(e => e.Name == ElementsName.ParentRelationship.ToString()))
            {
                try
                {
                    var parentRelationship = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.Parent2GuardianSection.ToString())).Elements.Single(e => e.Name == ElementsName.ParentRelationship.ToString()).GetValue().ToString();
                    result = EnumHelper.ParseEnum<ParentRelationship>(parentRelationship);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }


        public string GetParentCellPhone(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Phone);

            return result;
        }


        public DateTime? GetParent2DoB(JbForm jbForm)
        {
            var result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.DoB, new List<string> { "DateOfBirth" });

                if (string.IsNullOrEmpty(result))
                {
                    return null;
                }
                else
                {
                    DateTime dob = DateTime.Parse(result);
                    return dob;
                }
            }
            catch (Exception)
            {
                return null;
            }

        }
        public DateTime? GetParentDoB(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.DoB, new List<string> { "DateOfBirth" });

            if (string.IsNullOrEmpty(result))
            {
                return null;
            }
            else
            {
                DateTime dob = DateTime.Parse(result);
                return dob;
            }

        }
        public DateTime? GetParticipantDoB(JbForm jbForm)
        {
            var result = string.Empty;

            result = GetFormValue(jbForm, SectionsName.ParticipantSection, ElementsName.DoB);

            if (string.IsNullOrEmpty(result))
            {
                return null;
            }
            else
            {
                try
                {
                    var dob = DateTime.Parse(result);

                    return dob;
                }
                catch (FormatException exception)
                {
                    if (DateTimeHelper.JbParse(result).HasValue)
                    {
                        return DateTimeHelper.JbParse(result).Value;
                    }

                    return null;
                }
            }
        }
        public Genders? GetParentGender(JbForm jbForm)
        {

            Genders? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParentGuardianSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParentGuardianSection.ToString())).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                try
                {
                    var gender = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParentGuardianSection.ToString())).Elements.Single(e => e.Name == ElementsName.Gender.ToString()).GetValue().ToString();
                    result = EnumHelper.ParseEnum<Genders>(gender);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public Genders? GetParent2Gender(JbForm jbForm)
        {

            Genders? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.Parent2GuardianSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.Parent2GuardianSection.ToString())).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                try
                {
                    var gender = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.Parent2GuardianSection.ToString())).Elements.Single(e => e.Name == ElementsName.Gender.ToString()).GetValue().ToString();
                    result = EnumHelper.ParseEnum<Genders>(gender);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public GenderCategories GetParent2GenderNew(JbForm jbForm)
        {

            GenderCategories result = GenderCategories.None;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.Parent2GuardianSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.Parent2GuardianSection.ToString())).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                try
                {
                    var gender = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.Parent2GuardianSection.ToString())).Elements.Single(e => e.Name == ElementsName.Gender.ToString()).GetValue().ToString();
                    result = EnumHelper.ParseEnum<GenderCategories>(gender);
                }
                catch
                {
                    result = GenderCategories.None;
                }
            }

            return result;
        }
        public string GetElementNameAttr(JbForm jbForm, SectionsName sectionName, ElementsName elementName)
        {
            string result = null;

            if (jbForm.Elements.Any(s => s.Name == sectionName.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == sectionName.ToString())).Elements.Any(e => e.Name == elementName.ToString()))
            {
                try
                {
                    var section = jbForm.Elements.Single(s => s.Name == sectionName.ToString()) as JbSection;
                    var element = section.Elements.Single(s => s.Name == elementName.ToString()) as JbBaseElement;

                    var sectionIndex = jbForm.Elements.FindIndex(s => s.Name == sectionName.ToString());

                    var elementIndex = GetElementIndexInSection(section, element);

                    result = string.Format("JbForm.Elements[{0}].Elements[{1}].Value", sectionIndex, elementIndex);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }

        public int GetElementIndexInSection(JbSection section, JbBaseElement element)
        {
            var result = 0;

            foreach (var item in section.Elements)
            {
                if (item.ElementId == element.ElementId)
                {
                    return result;
                }

                result++;
            }

            return result;
        }

        public void SetContactFullName(JbForm jbForm, PlayerProfile player)
        {
            var contactPerson = player.Contact;
            if (jbForm.Elements.Any(s => s.Name == SectionsName.ParticipantSection.ToString()))
            {
                try
                {
                    var participantSection = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString()));
                    var parentGuardianSection = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParentGuardianSection.ToString()));
                    var parent2GuardianSection = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.Parent2GuardianSection.ToString()));
                    var emergencyContactSection = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString()));

                    if (participantSection.Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
                    {
                        var firstNameElement = participantSection.Elements.Single(e => e.Name == ElementsName.FirstName.ToString());

                        firstNameElement.SetValue(contactPerson.FirstName);
                    }

                    if (participantSection.Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
                    {
                        var lastNameElement = participantSection.Elements.Single(e => e.Name == ElementsName.LastName.ToString());

                        lastNameElement.SetValue(contactPerson.LastName);
                    }

                    //if (participantSection.Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
                    //{
                    //    var dateOfBirthElement = participantSection.Elements.Single(e => e.Name == ElementsName.DoB.ToString());

                    //    dateOfBirthElement.SetValue(contactPerson.DoB);
                    //}

                    if (participantSection.Elements.Any(e => e.Name == ElementsName.Email.ToString()))
                    {
                        var emailElement = participantSection.Elements.Single(e => e.Name == ElementsName.Email.ToString());

                        emailElement.SetValue(contactPerson.Email);
                    }
                    if (participantSection.Elements.Any(e => e.Name == ElementsName.SchoolName.ToString()))
                    {
                        var schoolNameElement = participantSection.Elements.Single(e => e.Name == ElementsName.SchoolName.ToString());

                        schoolNameElement.SetValue(player.Info.SchoolName);
                    }
                    if (participantSection.Elements.Any(e => e.Name == ElementsName.Homeroom.ToString()))
                    {
                        var homeRoomElement = participantSection.Elements.Single(e => e.Name == ElementsName.Homeroom.ToString());

                        homeRoomElement.SetValue(player.Info.HomeRoom);
                    }
                    if (participantSection.Elements.Any(e => e.Name == ElementsName.Grade.ToString()))
                    {
                        var gradeElement = participantSection.Elements.Single(e => e.Name == ElementsName.Grade.ToString());

                        gradeElement.SetValue(player.Info.Grade);
                    }
                    if (participantSection.Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
                    {
                        var genderElement = participantSection.Elements.Single(e => e.Name == ElementsName.Gender.ToString());

                        genderElement.SetValue(player.Gender);
                    }

                    if (parentGuardianSection.Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
                    {
                        var firstNameElement = parentGuardianSection.Elements.Single(e => e.Name == ElementsName.FirstName.ToString());

                        firstNameElement.SetValue(contactPerson.FirstName);
                    }
                    //if (parentGuardianSection.Elements.Any(e => e.Name == ElementsName.LastName.ToString()))
                    //{
                    //    var lastNameElement = parentGuardianSection.Elements.Single(e => e.Name == ElementsName.LastName.ToString());

                    //    lastNameElement.SetValue(contactPerson.LastName);
                    //}
                }
                catch
                {
                }
            }
        }

        public JbSection GetSection(JbForm jbForm, SectionsName sectionName)
        {
            return GetSection(jbForm, sectionName.ToString());
        }

        public JbSection GetSection(JbForm jbForm, string sectionName)
        {
            var result = jbForm.Elements.Select(e => e as JbSection).SingleOrDefault(e => e.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase));

            return result;
        }

        public JbBaseElement GetElement(JbSection jbSection, ElementsName elementName)
        {
            return GetElement(jbSection, elementName.ToString());
        }

        public JbBaseElement GetElement(JbSection jbSection, string elementName)
        {
            var result = jbSection.Elements.Select(e => e as JbBaseElement).SingleOrDefault(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));

            return result;
        }

        public List<ElementsName> GetDefaultFormElements()
        {
            var result = new List<ElementsName>();

            var defaultFormElements = GetDefaultFormElements(GetDefaultRegistrationForm());
            var defaultSchoolFormElements = GetDefaultFormElements(GetDefaultSchoolRegistrationForm());

            result.AddRange(defaultFormElements);
            result.AddRange(defaultSchoolFormElements);

            result.Distinct();

            return result;
        }

        public List<ElementsName> GetDefaultFormElements(JbForm jbForm)
        {
            var result = new List<ElementsName>();

            var formElements = GetFormElements(jbForm);

            foreach (var element in formElements)
            {
                result.Add(EnumHelper.ParseEnum<ElementsName>(element.Name));
            }

            result.Distinct();

            return result;
        }

        public List<JbBaseElement> GetFormElements(JbForm jbForm)
        {
            var result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .Select(e => e as JbBaseElement)
                            .ToList();

            return result;
        }

        public void AddRequiredToGender(JbForm jbForm)
        {
            var participantSection = GetSection(jbForm, SectionsName.ParticipantSection);

            var element = GetElement(participantSection, ElementsName.Gender);

            if (element == null)
                return;

            var genderElement = element as JbDropDown;

            if (genderElement.Validations.Any(v => v.Type == Jb.Framework.Common.Forms.Validation.ElementValidationType.Required))
                return;

            genderElement.IsRequired = true;
            genderElement.Validations = new List<Jb.Framework.Common.Forms.Validation.BaseElementValidator>()
                        {
                            new Jb.Framework.Common.Forms.Validation.BaseElementValidator()
                            {
                                Type = Jb.Framework.Common.Forms.Validation.ElementValidationType.Required,
                                Message = string.Format( Validation.RequiredFormat, "Gender"),
                                IsChecked = true,
                            }
                        };
        }

        public void AddRequiredToGrade(JbForm jbForm)
        {
            var gradeElement = GetGradeElement(jbForm);

            if (gradeElement == null)
                return;

            if (gradeElement.Validations.Any(v => v.Type == Jb.Framework.Common.Forms.Validation.ElementValidationType.Required))
                return;

            gradeElement.IsRequired = true;
            gradeElement.Validations = new List<Jb.Framework.Common.Forms.Validation.BaseElementValidator>()
                        {
                            new Jb.Framework.Common.Forms.Validation.BaseElementValidator()
                            {
                                Type = Jb.Framework.Common.Forms.Validation.ElementValidationType.Required,
                                Message = string.Format( Validation.RequiredFormat, "Grade"),
                                IsChecked = true,
                            }
                        };
        }

        public void AddRequiredToDob(JbForm jbForm)
        {
            var participantSection = GetSection(jbForm, SectionsName.ParticipantSection);

            var element = GetElement(participantSection, ElementsName.DoB);

            if (element == null)
                return;

            var dobElement = element as JbTextBox;

            if (dobElement.Validations.Any(v => v.Type == Jb.Framework.Common.Forms.Validation.ElementValidationType.Required))
                return;

            dobElement.IsRequired = true;
            dobElement.Validations = new List<Jb.Framework.Common.Forms.Validation.BaseElementValidator>()
                        {
                            new Jb.Framework.Common.Forms.Validation.BaseElementValidator()
                            {
                                Type = Jb.Framework.Common.Forms.Validation.ElementValidationType.Required,
                                Message = string.Format( Validation.RequiredFormat, "Grade"),
                                IsChecked = true,
                            }
                        };
        }

        public void SetParticipantFieldsReadonly(JbForm jbForm)
        {
            var participantSection = GetSection(jbForm, SectionsName.ParticipantSection);
            var firstName = GetElement(participantSection, ElementsName.FirstName);
            var lastName = GetElement(participantSection, ElementsName.LastName);

            firstName.CurrentMode = AccessMode.ReadOnly;
            lastName.CurrentMode = AccessMode.ReadOnly;

        }

        public void SetElementEmpty(IJbBaseElement element)
        {

            if (element is JbAddress)
            {
                var address = element as JbAddress;

                address.AddressLine1 = null;
                address.AddressLine2 = null;
                address.City = null;
                address.Country = null;
                address.State = null;
                address.Zip = null;
            }
            else if (element is JbCheckBox)
            {
                ((JbCheckBox)element).SetValue(false);
            }
            else
            {
                element.SetValue(null);
            }
        }

        public string GetMorningProgramArrival(JbForm jbForm)
        {
            var result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, SectionsName.SchoolSection, new List<string> { ReportConstants.MorningProgramArrival });

                if (string.IsNullOrEmpty(result))
                {
                    return null;
                }
                else
                {
                    return result;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion

        #region Apollo

        public string GetApolloTeacher(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.TeacherName, new List<string> { "Teacher", "Name" }) != null)
                {
                    return GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.TeacherName, new List<string> { "Teacher", "Name" });
                }
                else
                {
                    return GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.Name);
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetDismissalFromEnrichment(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                return GetFormValue(jbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment);
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }



        #endregion

        #region Flex
        public string GetAllergies(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.HealthSection, ElementsName.AllergiesInfo)))
                {
                    result = GetFormValue(jbForm, "Additional Information", "Allergies (details)");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.HealthSection, ElementsName.AllergiesInfo);
                }
                return result;
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetMedicalConditions(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.HealthSection, ElementsName.MedicalConditions)))
                {
                    result = GetFormValue(jbForm, "Additional Information", "Health/medical issues/concerns (details)");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.HealthSection, ElementsName.MedicalConditions);
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetDeitary(JbForm jbForm)
        {

            string result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, "Additional Information", "Dietary (details)");
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetMedication(JbForm jbForm)
        {

            string result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, "Additional Information", "Medication (details)");
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetOtherInformation(JbForm jbForm)
        {

            string result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, "Additional Information", "Other (details)");
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetParent1Name(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.LastName)) && string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName)))
                {
                    result = string.Empty;
                }
                else
                {
                    result = string.Format("{0}, {1}", GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.LastName), GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName));
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetParent2Name(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName)) && string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName)))
                {
                    result = string.Empty;
                }
                else
                {
                    result = string.Format("{0}, {1}", GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName), GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName));
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetAuthorizedPickup1Name(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.LastName)) && string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.FirstName)))
                {
                    result = string.Format("{0}, {1}", GetFormValue(jbForm, "Authorized Pick Up 1", "Last Name"), GetFormValue(jbForm, "Authorized Pick Up 1", "First Name"));
                }
                else
                {
                    result = string.Format("{0}, {1}", GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.LastName), GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.FirstName));
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetAuthorizedPickup2Name(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.LastName)) && string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.FirstName)))
                {
                    result = string.Format("{0}, {1}", GetFormValue(jbForm, "Authorized Pick Up 2", "Last Name"), GetFormValue(jbForm, "Authorized Pick Up 2", "First Name"));
                }
                else
                {
                    result = string.Format("{0}, {1}", GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.LastName), GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.FirstName));
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetEmergencyContactName(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.LastName)) && string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName)))
                {
                    result = string.Format("{0}, {1}", GetFormValue(jbForm, "Emergency Contact 1", "Last Name"), GetFormValue(jbForm, "Emergency Contact 1", "First Name"));
                }
                else
                {
                    result = string.Format("{0}, {1}", GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.LastName), GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName));
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetEmergencyContact2Name(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                result = string.Format("{0}, {1}", GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.LastName), GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName));
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetParent1Phone(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Phone);

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetAuthorizedPickup1Phone(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Phone)))
                {
                    result = GetFormValue(jbForm, "Authorized Pick Up 1", "Phone");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Phone);

                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetAuthorizedPickup2Phone(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.Phone)))
                {
                    result = GetFormValue(jbForm, "Authorized Pick Up 2", "Phone");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.Phone);

                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetAuthuraizePikeUp1RelationShip(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Relationship)))
                {
                    result = GetFormValue(jbForm, "Authorized Pick Up 1", "Relationship (to the student)");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Relationship);
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;

        }
        public string GetAuthuraizePikeUp2RelationShip(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.Relationship)))
                {
                    result = GetFormValue(jbForm, "Authorized Pick Up 2", "Relationship (to the student)");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.Relationship);
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;

        }

        public string GetEmergency2RelationShip(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, "Emergency Contact 2", "Relationship (to the student)");
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetEmergency1RelationShip(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.Relationship)))
                {
                    result = GetFormValue(jbForm, "Emergency Contact 1", "Relationship (to the student)");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.Relationship);
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetEmergencyContactPhone(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.Phone)))
                {
                    result = GetFormValue(jbForm, "Emergency Contact 1", "Phone");

                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.Phone);
                }


            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetEmergencyContactFirstName(JbForm jbForm)
        {
            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.EmergencyContactSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Any(e => e.Name == ElementsName.FirstName.ToString()))
            {
                try
                {
                    var firstName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue().ToString();
                    result = firstName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public string GetEmergencyContactLastName(JbForm jbForm)
        {
            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.EmergencyContactSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Any(e => e.Name == ElementsName.LastName.ToString()))
            {
                try
                {
                    var lastName = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue().ToString();
                    result = lastName;
                }
                catch
                {
                    result = string.Empty;
                }
            }

            return result;
        }

        public string GetEmergencyContactPrimeryPhone(JbForm jbForm)
        {
            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.EmergencyContactSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Any(e => e.Name == ElementsName.Phone.ToString()))
            {
                try
                {
                    var phone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Single(e => e.Name == ElementsName.Phone.ToString()).GetValue().ToString();
                    result = phone;

                    if (string.IsNullOrEmpty(result))
                    {
                        result = GetFormValue(jbForm, SectionsName.EmergencyContactSection, "HomePhone");
                    }
                }
                catch
                {
                    result = string.Empty;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(result))
                {
                    result = GetFormValue(jbForm, SectionsName.EmergencyContactSection, "HomePhone");
                }
            }

            return result;
        }

        public string GetEmergencyContactWorkPhone(JbForm jbForm)
        {
            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.EmergencyContactSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Any(e => e.Name == ElementsName.WorkPhone.ToString()))
            {
                try
                {
                    var phone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Single(e => e.Name == ElementsName.WorkPhone.ToString()).GetValue().ToString();
                    result = phone;
                    if (string.IsNullOrEmpty(result))
                    {
                        result = GetFormValue(jbForm, SectionsName.EmergencyContactSection, "WorkPhone");
                    }

                }
                catch
                {
                    result = string.Empty;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(result))
                {
                    result = GetFormValue(jbForm, SectionsName.EmergencyContactSection, "WorkPhone");
                }
            }

            return result;
        }
        public string GetEmergencyContactAlternatePhone(JbForm jbForm)
        {
            var result = string.Empty;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.EmergencyContactSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Any(e => e.Name == ElementsName.AlternatePhone.ToString()))
            {
                try
                {
                    var phone = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Single(e => e.Name == ElementsName.AlternatePhone.ToString()).GetValue().ToString();
                    result = phone;
                }
                catch
                {
                    result = string.Empty;
                }
            }


            return result;
        }
        public Relationship? GetEmergencyContactRelationShip(JbForm jbForm)
        {
            Relationship? result = null;

            if (jbForm.Elements.Any(s => s.Name == SectionsName.EmergencyContactSection.ToString()) && ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Any(e => e.Name == ElementsName.Relationship.ToString()))
            {
                try
                {
                    var relationShip = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.EmergencyContactSection.ToString())).Elements.Single(e => e.Name == ElementsName.Relationship.ToString()).GetValue().ToString();
                    var newRelationShip = relationShip != null ? relationShip.Replace("/", "").Replace(" ", "") : null;
                    result = EnumHelper.ParseEnum<Relationship>(newRelationShip);
                }
                catch
                {
                    result = null;
                }
            }

            return result;
        }
        public string GetEmergencyContact2Phone(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, "Emergency Contact 2", "Phone");

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetParent1Email(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Email);

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }
        public string GetAuthorizedPickup1Email(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {


                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Email)))
                {
                    result = GetFormValue(jbForm, SectionsName.AuthorizedPickup1, "Email address");

                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.AuthorizedPickup1, ElementsName.Email);
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetAuthorizedPickup2Email(JbForm jbForm)
        {
            string result;

            try
            {
                result = string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.Email)) ? GetFormValue(jbForm, SectionsName.AuthorizedPickup2, "Email address") : GetFormValue(jbForm, SectionsName.AuthorizedPickup2, ElementsName.Email);
            }
            catch (Exception)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetAuthorizedPickup3Email(JbForm jbForm)
        {
            string result;

            try
            {
                result = string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.AuthorizedPickup3, ElementsName.Email)) ? GetFormValue(jbForm, SectionsName.AuthorizedPickup3, "Email address") : GetFormValue(jbForm, SectionsName.AuthorizedPickup3, ElementsName.Email);
            }
            catch (Exception)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetEmergencyContactEmail(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.Email)))
                {
                    result = GetFormValue(jbForm, SectionsName.EmergencyContactSection, "EmailAddress");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.EmergencyContactSection, ElementsName.Email);
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetParent2RelationShip(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Relationship)))
                {
                    result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, "Relationship (to the student)");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.Relationship);
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string GetParent1RelationShip(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Relationship)))
                {
                    result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Relationship (to the student)");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.Relationship);
                }


            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string Parent2HomePhone(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {

                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.HomePhone)))
                {
                    result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, "Home Phone");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.Parent2GuardianSection, ElementsName.HomePhone);
                }
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public string Parent1HomePhone(JbForm jbForm)
        {
            string result = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.HomePhone)))
                {
                    result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, "Home Phone");
                }
                else
                {
                    result = GetFormValue(jbForm, SectionsName.ParentGuardianSection, ElementsName.HomePhone);
                }

            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        #endregion

    }
}
