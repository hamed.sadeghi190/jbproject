﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using SportsClub.Models;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CalendarCreateEditModelStep2 : BaseViewModel<Program, long>, IProgramWizardModel
    {
        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }

        public ProgramPageStep PageStep { get; set;}

        public ProgramPageStep LastCreatedPage { get; set;}

        public SaveType SaveType { get; set;}

        public string ClubDomain { get; set;}

        public string SeasonDomain { get; set;}

        public string Domain { get; set;}

        public string Name { get; set;}

        public ProgramStatus Status { get; set;}

        public ProgramRegStatus RegStatus { get; set;}

        public ProgramRunningStatus RunningStatus { get; set;}

        public ProgramTypeCategory TypeCategory { get; set;}

        public List<ProgramScheduleViewModel> Schedules { get; set; }

        public List<SelectKeyValue<string>> GenderItems { get; set; }

        public Genders SelectedGender { get; set; }

        public List<SelectKeyValue<string>> Capacities { get; set; }

        public string Capacity { get; set; }

        public int SelectedCapacity { get; set; }

        public List<SelectKeyValue<string>> ContinueTypes { get; set; }

        public ContinueType SelectedContinueType { get; set; }

        public List<SelectKeyValue<string>> Sessions { get; set; }

        public string SelectedSession { get; set; }

        public List<SelectKeyValue<string>> RestrictionTypes { get; set; }

        public RestrictionType SelectedRestrictionType { get; set; }

        public List<SelectKeyValue<string>> Ages { get; set; }

        public List<SelectKeyValue<string>> Grades { get; set; }

        public string SelectedMinAge { get; set; }

        public string SelectedMaxAge { get; set; }

        public SchoolGradeType? SelectedMinGrade { get; set; }

        public SchoolGradeType? SelectedMaxGrade { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        public bool ViewCalendar { get; set; }
    }
}