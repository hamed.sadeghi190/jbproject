﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramTransferViewModel
    {
        public bool IsFrozen { get; set; }
        public List<TransferScheduleViewModel> Schedules { get; set; }
    }


    public class TransferScheduleViewModel
    {
        public long Id { get; set; }

        public string Dates { get; set; }

        public string Title { get; set; }

        public List<TransferTutionViewModel> Tutions { get; set; }
    }

    public class TransferTutionViewModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public bool IsProrate { get; set; }

        public bool IsEarlyBird { get; set; }
        public int WeekDay { get; set; }
    }
}