--@mn--2014/10/25--

IF OBJECT_ID('TourneyCoverages') IS NULL
BEGIN
	CREATE TABLE [dbo].[TourneyCoverages](
		[Id] [int] IDENTITY(1,1) NOT NULL,
		[RoundNo] [int] NOT NULL,
		[CoverageType] [int] NOT NULL,
		[Data] [ntext] NULL,
		[Tourney_Id] [int] NULL,
		[Section_Id] [int] NULL	)
END
GO

--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.TourneyCoverages')
BEGIN
	ALTER TABLE [dbo].[TourneyCoverages] ADD CONSTRAINT [PK_dbo.TourneyCoverages] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

--<< ADD FOREGIN KEYS >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.TourneyCoverages_dbo.Tourneys_Tourney_Id')
BEGIN
	ALTER TABLE [dbo].[TourneyCoverages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TourneyCoverages_dbo.Tourneys_Tourney_Id] FOREIGN KEY([Tourney_Id])
	REFERENCES [dbo].[Tourneys] ([Id])
END
GO

IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.TourneyCoverages_dbo.TourneySections_Section_Id')
BEGIN
	ALTER TABLE [dbo].[TourneyCoverages]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TourneyCoverages_dbo.TourneySections_Section_Id] FOREIGN KEY([Section_Id])
	REFERENCES [dbo].[TourneySections] ([Id])
END
GO

--<< ALTER COLUMN >>--
IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[TourneyCoverages]') AND NAME='Data')
BEGIN
	ALTER TABLE [dbo].[TourneyCoverages] 
	ALTER COLUMN [Data] [ntext] NULL
END
Go

