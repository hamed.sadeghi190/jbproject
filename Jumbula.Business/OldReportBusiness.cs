﻿using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Exceptions;
using Jumbula.Core.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class OldReportBusiness : IReportBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IJbFormBusiness _jbFormBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IRepository<EventRoaster> _eventRoaterRepository;
        private readonly IEntitiesContext _dataContext;

        public OldReportBusiness(IClubBusiness clubBusiness, IJbFormBusiness jbFormBusiness, IOrderItemBusiness orderItemBusness, IRepository<EventRoaster> eventRoaterRepository, IEntitiesContext dataContext)
        {
            _clubBusiness = clubBusiness;
            _jbFormBusiness = jbFormBusiness;
            _orderItemBusiness = orderItemBusness;
            _eventRoaterRepository = eventRoaterRepository;
            _dataContext = dataContext;
        }

        public EventRoaster GetReport(int reportId)
        {
            return _eventRoaterRepository.Get(e => e.Id == reportId);
        }

        public IQueryable<EventRoaster> GetReports(string clubDomain, long seasonId)
        {
            return _eventRoaterRepository.GetList(roaster => roaster.ClubDomain == clubDomain && (seasonId == 0 || roaster.SeasonId == seasonId));
        }
        public EventRoaster GetClubRosterReport(string clubDomain, long seasonId)
        {
            return _eventRoaterRepository.Get(roaster => roaster.ClubDomain == clubDomain && roaster.EventType == EventRoasterType.Roster && roaster.SeasonId == seasonId);
        }
        public IQueryable<EventRoaster> GetReports(string clubDomain)
        {
            return _eventRoaterRepository.GetList(roaster => roaster.ClubDomain == clubDomain);
        }

        public  OperationStatus Create(EventRoaster eventRoaster)
        {
            try
            {
                _eventRoaterRepository.Create(eventRoaster);
                if (eventRoaster.JbForm_Id.HasValue)
                {
                    eventRoaster.JbForm.RefEntityName = "Report";
                    eventRoaster.JbForm.RefEntityId = eventRoaster.Id;

                    _jbFormBusiness.CreateEdit(eventRoaster.JbForm);

                    foreach (var item in eventRoaster.FollowupForms)
                    {
                        item.RefEntityName = "Report";
                        item.RefEntityId = eventRoaster.Id;

                        _jbFormBusiness.CreateEdit(item);
                    }
                }
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }

            return new OperationStatus();
        }

        public OperationStatus Update(EventRoaster eventRoaster)
        {
            try
            {
                var followupForms = eventRoaster.FollowupForms;

                foreach (var item in followupForms.Where(f => f.Id == 0))
                {
                    _dataContext.Entry<JbForm>(item).State = System.Data.Entity.EntityState.Added;
                }

                var result = _eventRoaterRepository.Update(eventRoaster);

                return result;
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }

        }

        public OperationStatus Delete(int reportId)
        {
            var report = GetReport(reportId);

            var folloupForms = report.FollowupForms.ToList();

            _dataContext.Entry<EventRoaster>(report).State = System.Data.Entity.EntityState.Detached;

            foreach (var item in folloupForms)
            {
                _dataContext.Entry<JbForm>(item).State = System.Data.Entity.EntityState.Deleted;
            }

            var result = _eventRoaterRepository.Delete(reportId);

            return result;

        }

        public List<JbForm> GetFolloupFormReports(int clubId)
        {
            var result = new List<JbForm>();

            var followupTemplates = _clubBusiness.GetFormTemplates(clubId, FormType.FollowUp);

            followupTemplates.ForEach(f => f.JbForm.Title = f.Title);

            result = followupTemplates
                                    .Select(f => f.JbForm)
                                    .ToList();

            return result;
        }

        public JbForm GenerateReportFollowupForm(JbForm jbForm)
        {
            var result = new JbForm();

            foreach (var element in jbForm.Elements)
            {
                var lastSection = new JbSection();

                if (element is JbSection)
                {
                    lastSection = new JbSection { Name = element.Name, Title = element.Title };

                    foreach (var element2 in (element as JbSection).Elements.Where(e => !(e is JbHidden) && !(e is JbParagraph) && !(e is JbTextEditor)))
                    {
                        var checkbox = new JbCheckBox { ElementId = element2.ElementId, Name = element2.Name, Title = element2.Title };
                        lastSection.Elements.Add(checkbox);
                    }
                }
                else
                {
                    var checkbox = new JbCheckBox();
                    Jumbula.Common.Utilities.AutoMapper.Map(element, ref checkbox);
                    (lastSection as JbSection).Elements.Add(checkbox);
                }

                result.Elements.Add(lastSection);
            }

            return result;
        }


        private JbForm GetDefaultRosterReport()
        {
            JbForm result = null;

            var jsonElements = "[{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox2011448599','TypeTitle':'CheckBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox986298085','TypeTitle':'CheckBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbDropDown155163549','TypeTitle':'CheckBox','Name':'Gender','Title':'Gender','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox80634374','TypeTitle':'CheckBox','Name':'DoB','Title':'Date of birth','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbEmail1076256290','TypeTitle':'CheckBox','Name':'Email','Title':'Email address','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection1816708558','TypeTitle':'Section','Name':'ParticipantSection','Title':'Participant Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbAddress1631223834','TypeTitle':'CheckBox','Name':'Address','Title':'Address','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1174296125','TypeTitle':'CheckBox','Name':'Phone','Title':'Phone number','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1622301703','TypeTitle':'CheckBox','Name':'Phone','Title':'Primary phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone955075366','TypeTitle':'CheckBox','Name':'AlternatePhone','Title':'Alternate phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection2010624150','TypeTitle':'Section','Name':'ContactSection','Title':'Contact Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox904548860','TypeTitle':'CheckBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox680113752','TypeTitle':'CheckBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbEmail764076578','TypeTitle':'CheckBox','Name':'Email','Title':'Email address','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbDropDown1421090448','TypeTitle':'CheckBox','Name':'Gender','Title':'Gender','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox81152843','TypeTitle':'CheckBox','Name':'DateOfBirth','Title':'Date of birth','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone130902813','TypeTitle':'CheckBox','Name':'Phone','Title':'Cell phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox1606556686','TypeTitle':'CheckBox','Name':'Occupation','Title':'Occupation','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox503499193','TypeTitle':'CheckBox','Name':'Employer','Title':'Employer','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1194008593','TypeTitle':'CheckBox','Name':'Phone','Title':'Primary phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1095459738','TypeTitle':'CheckBox','Name':'AlternatePhone','Title':'Alternate phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection776804800','TypeTitle':'Section','Name':'ParentGuardianSection','Title':'Parent/Guardian Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox1321630566','TypeTitle':'CheckBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox698626539','TypeTitle':'CheckBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbEmail653490704','TypeTitle':'CheckBox','Name':'Email','Title':'Email address','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbDropDown381878674','TypeTitle':'CheckBox','Name':'Gender','Title':'Gender','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox382463109','TypeTitle':'CheckBox','Name':'DateOfBirth','Title':'Date of birth','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1725718169','TypeTitle':'CheckBox','Name':'Phone','Title':'Cell phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox1269820681','TypeTitle':'CheckBox','Name':'Occupation','Title':'Occupation','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox1078137361','TypeTitle':'CheckBox','Name':'Employer','Title':'Employer','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone333230559','TypeTitle':'CheckBox','Name':'Phone','Title':'Primary phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone26639627','TypeTitle':'CheckBox','Name':'AlternatePhone','Title':'Alternate phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection19573919','TypeTitle':'Section','Name':'Parent2GuardianSection','Title':'Parent 2/Guardian Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox535764096','TypeTitle':'CheckBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox2080449281','TypeTitle':'CheckBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbEmail521207705','TypeTitle':'CheckBox','Name':'EmailAddress','Title':'Email address','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1637488407','TypeTitle':'CheckBox','Name':'HomePhone','Title':'Home phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone164457063','TypeTitle':'CheckBox','Name':'WorkPhone','Title':'Work phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox1690939999','TypeTitle':'CheckBox','Name':'Relationship','Title':'Relationship','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone272657881','TypeTitle':'CheckBox','Name':'Phone','Title':'Primary phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1196458750','TypeTitle':'CheckBox','Name':'AlternatePhone','Title':'Alternate phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection98975068','TypeTitle':'Section','Name':'EmergencyContactSection','Title':'Emergency Contact Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbDropDown1843633711','TypeTitle':'CheckBox','Name':'Grade','Title':'Grade','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox675031491','TypeTitle':'CheckBox','Name':'Name','Title':'Name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox548285782','TypeTitle':'CheckBox','Name':'TeacherName','Title':'Teacher name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox1430214984','TypeTitle':'CheckBox','Name':'TeacherNotListed','Title':'Teacher not listed - write name here','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbDropDown92843816','TypeTitle':'CheckBox','Name':'StandardDismissal','Title':'Standard dismissal','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbDropDown2101210276','TypeTitle':'CheckBox','Name':'DismissalFromEnrichment','Title':'Dismissal from enrichment','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection149963881','TypeTitle':'Section','Name':'SchoolSection','Title':'School Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox202953800','TypeTitle':'CheckBox','Name':'CompanyName','Title':'Company name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox1122631298','TypeTitle':'CheckBox','Name':'PolicyNumber','Title':'Policy number','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection1084340328','TypeTitle':'Section','Name':'InsuranceSection','Title':'Insurance Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextArea1672593260','TypeTitle':'CheckBox','Name':'MedicalConditions','Title':'Medical conditions','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextArea1922908957','TypeTitle':'CheckBox','Name':'AllergiesInfo','Title':'Allergies or dietary restrictions','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection1484928630','TypeTitle':'Section','Name':'HealthSection','Title':'Medical/Allergy Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox1027677678','TypeTitle':'CheckBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox385002779','TypeTitle':'CheckBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1603354969','TypeTitle':'CheckBox','Name':'Phone','Title':'Primary phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1058270904','TypeTitle':'CheckBox','Name':'AlternatePhone','Title':'Alternate phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection119893728','TypeTitle':'Section','Name':'AuthorizedPickup1','Title':'Authorized Pickup 1 / Permission to Release Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox198999268','TypeTitle':'CheckBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox965198499','TypeTitle':'CheckBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1128381091','TypeTitle':'CheckBox','Name':'Phone','Title':'Primary phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone606453364','TypeTitle':'CheckBox','Name':'AlternatePhone','Title':'Alternate phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection912182745','TypeTitle':'Section','Name':'AuthorizedPickup2','Title':'Authorized Pickup 2 / Permission to Release Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextArea881725726','TypeTitle':'CheckBox','Name':'MedicalConditions','Title':'Medical conditions/special needs','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextArea1598096183','TypeTitle':'CheckBox','Name':'AllergiesInfo','Title':'Allergies or dietary restrictions','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection421461207','TypeTitle':'Section','Name':'HealthSection','Title':'Medical Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox716218934','TypeTitle':'CheckBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox48465148','TypeTitle':'CheckBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone2107015721','TypeTitle':'CheckBox','Name':'Phone','Title':'Primary phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone1141219797','TypeTitle':'CheckBox','Name':'AlternatePhone','Title':'Alternate phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbDropDown2134227113','TypeTitle':'CheckBox','Name':'Relationship','Title':'Relationship','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection906557894','TypeTitle':'Section','Name':'AuthorizedPickup3','Title':'Authorized Pickup 3 / Permission to Release Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox158358409','TypeTitle':'CheckBox','Name':'FirstName','Title':'First name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbTextBox849875551','TypeTitle':'CheckBox','Name':'LastName','Title':'Last name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':true,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone2058924461','TypeTitle':'CheckBox','Name':'Phone','Title':'Primary phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbPhone167834864','TypeTitle':'CheckBox','Name':'AlternatePhone','Title':'Alternate phone','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbDropDown2037193136','TypeTitle':'CheckBox','Name':'Relationship','Title':'Relationship','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection1450584903','TypeTitle':'Section','Name':'AuthorizedPickup4','Title':'Authorized Pickup 4 / Permission to Release Information','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbSection','IsCustomElement':false,'Elements':[{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1011908137','TypeTitle':'CheckBox','Name':'UserEmail','Title':'User email','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1624634121','TypeTitle':'CheckBox','Name':'Tuition','Title':'Tuition price','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1607046094','TypeTitle':'CheckBox','Name':'TuitionName','Title':'Tuition name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1991349748','TypeTitle':'CheckBox','Name':'ConfirmationId','Title':'Confirmation','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox646193904','TypeTitle':'CheckBox','Name':'OrderDate','Title':'Registration date','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox164359248','TypeTitle':'CheckBox','Name':'OrderType','Title':'Order type','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1923854102','TypeTitle':'CheckBox','Name':'OrderAmount','Title':'Order amount','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1820040990','TypeTitle':'CheckBox','Name':'OrderPaidAmount','Title':'Paid amount','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1541050729','TypeTitle':'CheckBox','Name':'Balance','Title':'Balance','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1651784977','TypeTitle':'CheckBox','Name':'DiscountTotal','Title':'Total discount','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox83213552','TypeTitle':'CheckBox','Name':'PaymentStatus','Title':'Payment status','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1977209905','TypeTitle':'CheckBox','Name':'PaymentMethod','Title':'Payment method','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1927693635','TypeTitle':'CheckBox','Name':'PaymentDate','Title':'Payment date','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1263989860','TypeTitle':'CheckBox','Name':'ScheduleStartEnd','Title':'Schedule start/end date','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox107875130','TypeTitle':'CheckBox','Name':'ProgramType','Title':'Program type','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1677725605','TypeTitle':'CheckBox','Name':'ProgramLocation','Title':'Program location','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1286132654','TypeTitle':'CheckBox','Name':'ProgramLocationName','Title':'Location name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox336691976','TypeTitle':'CheckBox','Name':'ProgramInstructor','Title':'Program instructor','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1016031315','TypeTitle':'CheckBox','Name':'ProgramRoomNumber','Title':'Program room assignment','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox141475153','TypeTitle':'CheckBox','Name':'ProgramDayOfWeek','Title':'Program day of week','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1361270975','TypeTitle':'CheckBox','Name':'SchoolName','Title':'School name','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox912933069','TypeTitle':'CheckBox','Name':'Charges','Title':'partner','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox864049151','TypeTitle':'CheckBox','Name':'Charges','Title':'On-Site Support Service & PTA Activity Fee','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox746352182','TypeTitle':'CheckBox','Name':'Charges','Title':'Enrichment Matters Service Fee','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]},{'Type':'JbCheckBox','IsCustomElement':false,'Value':false,'IsRequired':false,'PlaceHolder':null,'ElementId':'JbCheckBox1037351763','TypeTitle':'CheckBox','Name':'Charges','Title':'Late Registration Fee','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}],'ElementId':'JbSection1244765188','TypeTitle':'Section','Name':'MoreInfoSection','Title':'More Info','HelpText':null,'Size':0,'VisibleMode':'Both','CurrentMode':'Edit','AccessRole':{},'Validations':[]}] ";

            result = new JbForm()
            {
                JsonElements = jsonElements.Replace("'", "\""),
                Title = null,
                HelpText = null,
                RefEntityId = 0,
                RefEntityName = "Report",
                CreatedDate = DateTime.UtcNow,
                LastModifiedDate = DateTime.UtcNow,
            };

            return result;
        }

        public void setDefaultColumn(JbForm jbForm)
        {
            foreach (var item in jbForm.Elements)
            {
                if (item.Name == SectionsName.ParticipantSection.ToString())
                {
                    var participantFirstNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.FirstName.ToString());
                    var participantLastNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.LastName.ToString());
                    var participantGenders = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Gender.ToString());

                    if (participantFirstNames != null)
                    {
                        foreach (var participantFirstName in participantFirstNames)
                        {
                            participantFirstName.SetValue(true);
                        }
                    }
                    if (participantLastNames != null)
                    {
                        foreach (var participantLastName in participantLastNames)
                        {
                            participantLastName.SetValue(true);
                        }
                    }
                    if (participantGenders != null)
                    {
                        foreach (var participantGender in participantGenders)
                        {
                            participantGender.SetValue(true);
                        }
                    }
                }
                else if (item.Name == SectionsName.ParentGuardianSection.ToString())
                {
                    var parentFirstNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.FirstName.ToString());
                    var parentLastNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.LastName.ToString());
                    var parentEmails = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Email.ToString());

                    if (parentFirstNames != null)
                    {
                        foreach (var parentFirstName in parentFirstNames)
                        {
                            parentFirstName.SetValue(true);
                        }
                    }
                    if (parentLastNames != null)
                    {
                        foreach (var parentLastName in parentLastNames)
                        {
                            parentLastName.SetValue(true);
                        }
                    }
                    if (parentEmails != null)
                    {
                        foreach (var parentEmail in parentEmails)
                        {
                            parentEmail.SetValue(true);
                        }
                    }

                    if (((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString()) != null)
                    {
                        var phones = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString());
                        foreach (var phone in phones)
                        {
                            phone.SetValue(true);
                        }
                    }
                    if (((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.AlternatePhone.ToString()) != null)
                    {
                        var alternatePhones = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.AlternatePhone.ToString());
                        foreach (var phone in alternatePhones)
                        {
                            phone.SetValue(true);
                        }
                    }


                }
                else if (item.Name == SectionsName.Parent2GuardianSection.ToString())
                {
                    if (((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString()) != null)
                    {
                        var phones = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString());
                        foreach (var phone in phones)
                        {
                            phone.SetValue(true);
                        }
                    }
                }
                else if (item.Name == SectionsName.EmergencyContactSection.ToString())
                {
                    var emergencyFirstNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.FirstName.ToString());
                    var emergencyLastNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.LastName.ToString());
                    var emergencyPhones = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.HomePhone.ToString());

                    if (emergencyFirstNames != null)
                    {
                        foreach (var emergencyFirstName in emergencyFirstNames)
                        {
                            emergencyFirstName.SetValue(true);
                        }
                    }
                    if (emergencyLastNames  != null)
                    {
                        foreach (var emergencyLastName in emergencyLastNames)
                        {
                            emergencyLastName.SetValue(true);
                        }
                    }
                    if (emergencyPhones != null)
                    {
                        foreach (var emergencyPhone in emergencyPhones)
                        {
                            emergencyPhone.SetValue(true);
                        }
                    }
                    
                }
                else if (item.Name == SectionsName.SchoolSection.ToString())
                {
                    var schoolGrades = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Grade.ToString());
                    var schoolTeacherNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.TeacherName.ToString());
                    var schoolTeacherNotListeds = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.TeacherNotListed.ToString());

                    if (schoolGrades != null)
                    {
                        foreach (var schoolGrade in schoolGrades)
                        {
                            schoolGrade.SetValue(true);
                        }
                    }
                    if (schoolTeacherNames != null)
                    {
                        foreach (var schoolTeacherName in schoolTeacherNames)
                        {
                            schoolTeacherName.SetValue(true);
                        }
                     
                    }
                    if (schoolTeacherNotListeds != null)
                    {
                        foreach (var schoolTeacherNotListed in schoolTeacherNotListeds)
                        {
                            schoolTeacherNotListed.SetValue(true);
                        }
                    
                    }

                    if (((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.DismissalFromEnrichment.ToString()) != null)
                    {
                        var dismissalFromEnrichments = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.DismissalFromEnrichment.ToString());
                        foreach (var dismissal in dismissalFromEnrichments)
                        {
                            dismissal.SetValue(true);
                        }
                    }

                }
                else if (item.Name == SectionsName.HealthSection.ToString())
                {
                    var AllergiesInfos = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.AllergiesInfo.ToString());
                    var medicalConditions = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.MedicalConditions.ToString());

                    if (medicalConditions != null)
                    {
                        foreach (var medicalCondition in medicalConditions)
                        {
                            medicalCondition.SetValue(true);
                        }                
                    }
                    if (AllergiesInfos != null)
                    {
                       
                        foreach (var AllergiesInfo in AllergiesInfos)
                        {
                            AllergiesInfo.SetValue(true);
                        }
                        
                    }
                }
                else if (item.Name == SectionsName.AuthorizedPickup1.ToString())
                {
                    var authorizedPickup1FirstName = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.FirstName.ToString());
                    var authorizedPickup1LastName = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.LastName.ToString());

                    if (authorizedPickup1FirstName != null)
                    {
                        foreach (var authorizedPickup1 in authorizedPickup1FirstName)
                        {
                            authorizedPickup1.SetValue(true);
                        }
                        
                    }
                    if (authorizedPickup1LastName != null)
                    {
                        foreach (var authorizedPickup1 in authorizedPickup1LastName)
                        {
                            authorizedPickup1.SetValue(true);
                        }
                      
                    }

                    if (((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString()) != null)
                    {
                        var phones = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString());
                        foreach (var phone in phones)
                        {
                            phone.SetValue(true);
                        }
                    }
                }
                else if (item.Name == SectionsName.AuthorizedPickup2.ToString())
                {
                    var authorizedPickup2FirstNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.FirstName.ToString());
                    var authorizedPickup2LastNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.LastName.ToString());

                    if (authorizedPickup2FirstNames != null)
                    {
                        foreach (var authorizedPickup2 in authorizedPickup2FirstNames)
                        {
                            authorizedPickup2.SetValue(true);
                        }
                    }
                    if (authorizedPickup2LastNames != null)
                    {
                        foreach (var authorizedPickup2 in authorizedPickup2LastNames)
                        {
                            authorizedPickup2.SetValue(true);
                        }
                    }

                    if (((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString()) != null)
                    {
                        var phones = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString());
                        foreach (var phone in phones)
                        {
                            phone.SetValue(true);
                        }
                    }
                }
                else if (item.Name == SectionsName.AuthorizedPickup3.ToString())
                {
                    var authorizedPickup3FirstNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.FirstName.ToString());
                    var authorizedPickup3LastNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.LastName.ToString());

                    if (authorizedPickup3FirstNames != null)
                    {
                        foreach (var authorizedPickup3 in authorizedPickup3FirstNames)
                        {
                            authorizedPickup3.SetValue(true);
                        }
                    }
                    if (authorizedPickup3LastNames != null)
                    {
                        foreach (var authorizedPickup3 in authorizedPickup3LastNames)
                        {
                            authorizedPickup3.SetValue(true);
                        }
                    }

                    if (((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString()) != null)
                    {
                        var phones = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString());
                        foreach (var phone in phones)
                        {
                            phone.SetValue(true);
                        }
                    }
                }
                else if (item.Name == SectionsName.AuthorizedPickup4.ToString())
                {
                    var authorizedPickup4FirstNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.FirstName.ToString());
                    var authorizedPickup4LastNames = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.LastName.ToString());
                
                    if (authorizedPickup4FirstNames != null)
                    {
                        foreach (var authorizedPickup4 in authorizedPickup4FirstNames)
                        {
                            authorizedPickup4.SetValue(true);
                        }
                    }
                    if (authorizedPickup4LastNames != null)
                    {
                        foreach (var authorizedPickup4 in authorizedPickup4LastNames)
                        {
                            authorizedPickup4.SetValue(true);
                        }
                    }
                    if (((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString()) != null)
                    {
                        var phones = ((JbSection)item).Elements.Where(e => e is JbCheckBox && e.Name == ElementsName.Phone.ToString());
                        foreach (var phone in phones)
                        {
                            phone.SetValue(true);
                        }
                    }
                }
            }

        }

        public OperationStatus CopyCustomReportToSeason(int sourceReportId, List<long> destinationSeasonIds)
        {
            var sourceReport = GetReport(sourceReportId);

            var rosters = new List<EventRoaster>();

            foreach (var seasonId in destinationSeasonIds)
            {
                var eventRoaster = CreateEventRoaster(sourceReport, seasonId);

                rosters.Add(eventRoaster);
            }

            var result = _eventRoaterRepository.Create(rosters);

            return result;
        }

        private static EventRoaster CreateEventRoaster(EventRoaster eventRoster, long seasonId)
        {
            var roster = new EventRoaster
            {
                Name = eventRoster.Name,
                Description = eventRoster.Description,
                SeasonId = seasonId,
                ClubDomain = eventRoster.ClubDomain,
                LastUpdateDate = DateTime.Now,
                HasHeader = eventRoster.HasHeader,
                CustomType = eventRoster.CustomType
            };

            var utcDate = DateTime.UtcNow;

            roster.JbForm = new JbForm()
            {
                AccessRole = eventRoster.JbForm.AccessRole,
                CreatedDate = utcDate,
                CurrentMode = eventRoster.JbForm.CurrentMode,
                Elements = eventRoster.JbForm.Elements,
                HelpText = eventRoster.JbForm.HelpText,
                JsonElements = eventRoster.JbForm.JsonElements,
                LastModifiedDate = utcDate,
                ParentId = eventRoster.JbForm.ParentId,
                RefEntityId = eventRoster.JbForm.RefEntityId,
                RefEntityName = eventRoster.JbForm.RefEntityName,
                Title = eventRoster.JbForm.Title,
                VisibleMode = eventRoster.JbForm.VisibleMode,
            };

            return roster;
        }


    }
}
