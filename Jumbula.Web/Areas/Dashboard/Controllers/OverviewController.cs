﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Areas.Dashboard.Models.Overview;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class OverviewController : DashboardBaseController
    {
        #region Fields
        private readonly IProgramSessionBusiness _programSessionBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly ILocationBusiness _locationBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;

        #endregion

        #region Constractors
        public OverviewController(IProgramBusiness programBusiness, IProgramSessionBusiness programSessionBusiness, IOrderItemBusiness orderItemBusiness, ILocationBusiness locationBusiness, IOrderSessionBusiness orderSessionBusiness, IOldEmailBusiness emailBusiness)
        {
            _programBusiness = programBusiness;
            _programSessionBusiness = programSessionBusiness;
            _orderItemBusiness = orderItemBusiness;
            _locationBusiness = locationBusiness;
            _orderSessionBusiness = orderSessionBusiness;
        }
        #endregion

        [JbAuthorize(JbAction.Overview_View)]
        public virtual JsonNetResult GetRecentRegistration(ParticipantSearchType? searchType, string firstName = null, string lastName = null, List<long> clubIds = null)
        {
            var club = ActiveClub;

            var hasPartner = Ioc.ClubBusiness.Get(club.Id).IsPartner;

            var pageSize = int.Parse(Request.Params["PageSize"]);
            var page = int.Parse(Request.Params["Page"]);

            var term = Request.Params["term"];

            var model = new List<RecentRegistrationModel>();

            int? instructorId = null;

            var currentRole = this.GetCurrentUserRole();

            if (currentRole == RoleCategory.Instructor)
            {
                instructorId = Ioc.ClubBusiness.Get(ActiveClub.Id).ClubStaffs.Single(c => c.JbUserRole.UserId == this.GetCurrentUserId()).Id;
            }

            IQueryable<OrderItem> query = Ioc.OrderItemBusiness.GetOrderItems(clubId: club.Id, term: term, isTestMode: false, itemStatus: OrderItemStatusCategories.showAll, instructorId: instructorId, searchType: searchType, firstName: firstName, lastName: lastName, hasPartner: hasPartner).Where(item => (item.ItemStatus == OrderItemStatusCategories.changed || item.ItemStatus == OrderItemStatusCategories.completed) && item.ProgramScheduleId.HasValue && item.ProgramScheduleId > 0);

            if (clubIds != null)
            {
                query = query.Where(item => clubIds.Contains(item.Order.ClubId));
            }

            model = query.OrderByDescending(item => item.Id).Skip((page - 1) * pageSize).Take(pageSize).ToList()
            .Select(item => new RecentRegistrationModel
            {
                AttendeeName = item.FullName,
                ProgramName = item.ProgramSchedule != null ? item.ProgramSchedule.Program.Name : "Donation",
                PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(item.TotalAmount, club.Currency),
                DateTimeLable = DateTimeHelper.DateTimeLable(item.Order.CompleteDate, club.TimeZone),
                OrderItemId = item.Id,
                SeasonDomain = item.Season != null ? item.Season.Domain : "",
                ItemStatusReason = (Int16)item.ItemStatusReason,
                Schedule = item.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament ?
                _programBusiness.GetProgramScheduleTitleAndDate(item, item.OrderItemChess.Schedule) :
                _programBusiness.GetProgramScheduleTitleAndDate(item),
                TuitionName = item.EntryFeeName,
                ConfirmationId = item.Order.ConfirmationId,
                ClubName = item.Order.Club.Name,
                ClubId = item.Order.ClubId
            })
            .ToList();

            var dataSource = model;

            return JsonNet(new { DataSource = dataSource, TotalCount = query.Count() });
        }

        public virtual ActionResult GetTotalSales()
        {
            long clubId = ActiveClub.Id;

            var orderItems = Ioc.OrderItemBusiness.GetOrderItems(clubId: clubId, seasonId: null, isTestMode: false).Where(o => o.ItemStatus == OrderItemStatusCategories.completed);

            var model = new TotalinfoModel
            {
                TotalAttendees = orderItems.Any() ? orderItems.Count() : 0,
                TotalSales = orderItems.Any() ? orderItems.Sum(o => o.TotalAmount) : 0
            };

            return JsonNet(model);
        }

        public virtual JsonNetResult GetAccountInfo()
        {
            long clubId = ActiveClub.Id;
            dynamic model = new ExpandoObject();
            model.FreeTrialMessage = "";
            model.IsFreeTrialAccount = false;

            model.SearchTypes = DropdownHelpers.ToSelectList<ParticipantSearchType>();

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            model.HasPartner = club.ClubType.EnumType == ClubTypesEnum.Partner;
            model.EnableViewParentDashboard = Ioc.ClubBusiness.EnableViewParentDashboard(club);

            if (club != null && !club.PartnerId.HasValue && club.Client != null && club.Client.PricePlan != null)
            {
                if (club.Client.PricePlan.Type == PricePlanType.FreeTrial)
                {
                    model.IsFirstTime = false;

                    if (Request.Cookies.Get("FreeTrialViewFirstTimeLogin") != null)
                    {
                        model.IsFirstTime =  Request.Cookies.Get("FreeTrialViewFirstTimeLogin").Value;

                        HttpCookie firstTimeCookie = Request.Cookies["FreeTrialViewFirstTimeLogin"];
                        firstTimeCookie.Expires = DateTime.Now.AddDays(-10);
                        firstTimeCookie.Value = null;
                        Response.SetCookie(firstTimeCookie);
                    }

                    model.IsFreeTrialAccount = true;
                    model.FreeTrialMessage = Ioc.ClubBusiness.GetFreeTrialMessage(club.MetaData.DateCreated);
                }
            }

            return JsonNet(model);
        }



    }
}