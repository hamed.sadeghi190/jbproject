
If Object_ID('[dbo].[Coupons]') Is Null
BEGIN
	CREATE TABLE [dbo].[Coupons](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[CouponType] [int] NOT NULL,
	[Code] [nvarchar](256) NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[AmounType] [int] NOT NULL,
	[MetaData_DateCreated] [datetime] NOT NULL,
	[MetaData_DateUpdated] [datetime] NOT NULL,
	[Events] [varchar](max) NOT NULL,
	[Users] [nvarchar](max) NOT NULL,
	[ClubDomain] [nvarchar](128) NOT NULL,
	[Status] [int] NOT NULL,
	[NumberOfUse] [int] NULL,
	[SeasonId] bigint null)
END
Go
--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.Coupons')
BEGIN
	ALTER TABLE [dbo].[Coupons] ADD CONSTRAINT  [PK_dbo.Coupons] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
	)
END
Go


--<< ALTER COLUMN >>--
IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID('[dbo].[Coupons]') AND NAME='Events')
BEGIN
	ALTER TABLE [dbo].[Coupons]
	ALTER column events varchar(max) not null
END
Go

IF NOT EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[Coupons]') AND name='SeasonId')
BEGIN	
	ALTER TABLE [dbo].[Coupons]
	ADD [SeasonId] bigint null
END


--<< FOREIGNKEYS DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name='FK_dbo.Coupons_dbo.Seasons_SeasonId')
BEGIN
	ALTER TABLE [dbo].[Coupons]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Coupons_dbo.Seasons_SeasonId] FOREIGN KEY([SeasonId])
	REFERENCES [dbo].[Seasons] ([Id])
END
GO