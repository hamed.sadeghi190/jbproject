﻿using Jb.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IInvoiceBusiness
    {
        Invoice Get(int? InvoiceId);
        IQueryable<Invoice> GetList();
        OperationStatus Update(Invoice invoice);
        OperationStatus Create(Invoice invoice);
        IQueryable<Invoice> GetInvoiceByClubId(int clubId);
        IQueryable<Invoice> GetInvoicesUser(int UserId);
        string GetTokenForInvoice(int invoiceId);
        List<TransactionActivity> GetInvoiceTransactions(int invoiceId);
        OperationStatus UpdateInvoiceTransactions(PaymentDetail paymentDetail);
        OperationStatus AddMemoforInvoiceTransactions(int invoiceId, string memo);
    }
}
