﻿(function () {
    'use strict';

    angular.module('jbTools').service("addNewUserService", [function () {

        var _model;


       
        function setModel(Data) {
            _model = Data;
        }

        function getModel() {
            return _model;
        }

        return {
            set: setModel,
            get: getModel
        }
    }]);
})();