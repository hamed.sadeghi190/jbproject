﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models.CustomReport
{
    public class CouponReportViewModel
    {
        public string FullName { get; set; }

        public string ProgramName { get; set; }

        public string Email { get; set; }

        public string RegDate { get; set; }

        public long OrderItemId { get; set; }

        public long OrderId { get; set; }

        public int CouponId { get; set; }

        public string CouponName { get; set; }

        public string CouponCode { get; set; }

        public string ConfirmationId { get; set; }

        public string Amount { get; set; }
    }
}