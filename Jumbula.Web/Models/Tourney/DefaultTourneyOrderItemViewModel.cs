﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class DefaultTourneyOrderItemViewModel : OrderItemViewModelBase
    {
        public string Section { get; set; }

        public string Schedule { get; set; }

        public string Byes { get; set; }

        public string Options { get; set; }
    }
}