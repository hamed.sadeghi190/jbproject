﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Web.Infrastructure;
using SportsClub.Models;

namespace Jumbula.Web.Mvc.ModelState
{
    public static class ModelStateHelper
    {
        public static void AddModelError(this ModelStateDictionary modelState, JbForm jbForm, SectionsName sectionName, ElementsName elementName, string errorMessage)
        {
            var elementNameAttr = Ioc.FormBusiness.GetElementNameAttr(jbForm, sectionName, elementName);

            if(!string.IsNullOrEmpty(elementNameAttr))
            {
                modelState.AddModelError(elementNameAttr, errorMessage);
            }
        }

        public static JResult GetErrorList(this ModelStateDictionary modelStateDictionary)
        {
            var errorList = new List<FormValidationError>();

            foreach (var key in modelStateDictionary.Keys)
            {
                if (modelStateDictionary.TryGetValue(key, out var modelState))
                {
                    if (modelState.Errors.Any())
                    {
                        var error = new FormValidationError
                        {
                            Key = key.StartsWith("model.") ? key : "model." + key,
                            Messages = new List<string>()
                        };

                        foreach (var item in modelState.Errors)
                        {
                            error.Messages.Add(item.ErrorMessage);
                        }

                        errorList.Add(error);
                    }
                }
            }

            var result = new JResult
            {
                FormErrors = errorList
            };

            return result;
        }
    }
}