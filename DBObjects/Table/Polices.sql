
If Object_ID('[dbo].[Policies]') Is Null
BEGIN
CREATE TABLE [dbo].[Policies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Field1] [int] NOT NULL,
	[Field2] [decimal](18, 2) NOT NULL,
	[Category] [int] NOT NULL,
	[TTTourney_Id] [int] NULL,
	[Camp_Id] [int] NULL,
	[Course_Id] [int] NULL,
	[Description] [nvarchar](2048) NULL,
	[Event_Id] [int] NULL)

END
Go

---<<ADD PRIMARY>>--
IF NOT EXISTS (select 1 from sys.objects where name='PK_dbo.Policies')
BEGIN 
	alter table [dbo].[Policies] add constraint [PK_dbo.Policies] primary key clustered
	(
		Id ASC
	)	
END
Go

---<<ADD COLUMNS>>--
IF NOT EXISTS(select 1 from sys.COLUMNS where object_id=object_id('[dbo].[Policies]') and name='Event_Id')
BEGIN
	ALTER TABLE [dbo].[Policies]
	ADD [Event_Id] int null
END

---<< ADD FOREIGN KEYS >>--
IF NOT EXISTS(select 1 FROM sys.objects WHERE name='FK_dbo.Policies_dbo.Events_Event_Id')
BEGIN
	ALTER TABLE [dbo].[Policies]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Policies_dbo.Events_Event_Id] FOREIGN KEY([Event_Id])
	REFERENCES [dbo].[Events] ([Id])
END