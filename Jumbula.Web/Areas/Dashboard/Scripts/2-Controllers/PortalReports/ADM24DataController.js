﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ADM24DataController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();



        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = {};
        $scope.Model.checkedIds = {};

        var nameModel = customReportService.getModelFromStrorage();
        if (is.not.undefined(nameModel) && nameModel != null) {
            var seasonId = nameModel.seasonId;
            var schoolId = nameModel.schoolId;
            $http.get('Dashboard/PortalReport/GetReoprtNameWithSeason', { params: { schoolId: schoolId, seasonId: seasonId } }).success(function (data, status, headers, config) {
                $scope.reportName = data;
            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });
        }

        $scope.ADM24DataReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetADM24Data?seasonId=' + $scope.MC.stateParams.seasonId,
                       
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
          
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
        });

        $scope.ADM24DataReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "ADM24 Report",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.ADM24DataReportDataSource,
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5
            },
           
            sortable: { mode: 'single' },
            columns: [
                
                {
                    field: "ProviderName",
                    title: "Provider name",
                    width: "180px"
                }, {
                    field: "ClassName",
                    title: "Class name",
                    width: "180px"
                },
                {
                    field: "Max",
                    title: "Maximum",
                    width: "200px"
                },
                {
                    field: "Day",
                    title: "Day of week",
                    width: "200px"
                },
                {
                    field: "ClassTimes",
                    title: "Class times",
                    width: "200px"
                },
                {
                    field: "Sessions",
                    title: "Meeting Dates",
                    width: "200px"
                },
                {
                    field: "SpaceReq",
                    title: "Space Requirements",
                    width: "200px"
                },
                {
                    
                    field: "Fee",
                    title: "Fee",
                    width: "200px"
                }
            ]
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ADM24DataReportGrid").getKendoGrid().saveAsExcel();
        }

       
    }
    ]);

})();