﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CatalogPriceGroupViewModel
    {
        public int Id { get; set; }

        public string StringId
        {
            get
            {
                return Id == 0 ? Guid.NewGuid().ToString() : Id.ToString();
            }
        }
        public string Title { get; set; }

        public int Duration { get; set; }

        public bool IsChecked
        {
            get
            {
                return Prices != null && Prices.Where(p => p.IsChecked).Any();
            }
        }

        public bool IsDefault { get; set; }
        public List<CatalogPriceViewModel> Prices { get; set; }
    }
}