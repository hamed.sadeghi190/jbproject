﻿using System;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class StripeWebhook : BaseEntity<long>
    {
        public StripeWebhook()
        {
            IsProceed = false;
            IsActionTaken = false;
            CreatedDate = DateTime.UtcNow;
        }
       public string EventId { get; set; }
        public bool livemode { get; set; }
        public string Created { get; set; }
        public string Type { get; set; }
        public string Data { get; set; }
        public string Webhook { get; set; }
        public bool IsProceed { get; set; }
        public bool IsActionTaken { get; set; }
        public DateTime CreatedDate { get; set; }
    }

}
