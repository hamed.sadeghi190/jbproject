alter table  [dbo].[Clients] drop DF__Clients__EnableC__4C413C06
go
alter table  [dbo].[Clients] drop DF__Clients__IsSandB__6EEB59C5
go
alter table  [dbo].[Clients] drop DF__Clients__IsStrip__746F28F1
go
alter TABLE [dbo].[Clients] drop column [PaypalEmail] 
go
alter TABLE [dbo].[Clients] drop column [EnableCashPayment] 
go
alter TABLE [dbo].[Clients] drop column [IsSandBoxMode] 
go
alter TABLE [dbo].[Clients] drop column [IsStripeTestMode] 
go
alter TABLE [dbo].[Clients] drop column [StripeCustomerId] 
go
alter TABLE [dbo].[Clients] drop column [StripeToken] 
go
alter TABLE [dbo].[Clients] drop column [StripeAccessToken] 

