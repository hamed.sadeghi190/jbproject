﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemChargeForClubController', ['$scope', '$http', '$stateParams', '$state', 'chargeClubService', function ($scope, $http, $stateParams, $state, chargeClubService) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.Model = {};
        $scope.Model.clubId = $stateParams.clubId;
       

        $scope.back = function () {
            $state.go('SystemClubBillingHistory', { clubId: $stateParams.clubId });
        }

        $http.get($scope.baseUrl + '/InfoForAutoChargeClubBilling?clubId=' + $stateParams.clubId).success(function (data, status, headers, config) {
            $scope.Model = data;

        });


        $scope.confirmChargeInfo = function () {

            $http.post($scope.baseUrl + '/ConfirmChargeInfo', { model: $scope.Model })

                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        chargeClubService.set($scope.Model);

                        $state.go('SystemConfirmChargeClub', { clubId: $stateParams.clubId }, 'SystemChargeClub', { clubId: $stateParams.clubId });

                    }
                    else {


                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

    }])

})();