﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SportsClub.Domain;
using SportsClub.Infrastructure;
using System.IO;
using SportsClub.Db;
using SportsClub.Infrastructure.Helper;

namespace SportsClub.Models
{
    public class CampPageViewModel
    {
        public bool IsLoggedIn { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                // Get cart
                var cartDb = Ioc.ICartDbService;

                // Get domain of orders in the cart(if there is no order in cart return empty)
                string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                if (!string.IsNullOrEmpty(currentDomain))
                {
                    return !(currentDomain == this.ClubDomain);
                }

                return false;
            }
        }

        public RoleCategories CurrentUserRole { get; set; }

        public bool IUnderstandChecked { get; set; }

        public bool OfflineRegChecked { get; set; }

        public Nullable<int> MinAge { get; set; }

        public Nullable<int> MaxAge { get; set; }

        public SchoolGradeType? MinGrade { get; set; }

        public SchoolGradeType? MaxGrade { get; set; }

        public string Name { get; set; }

        public string Name1 { get; set; }

        public string ChiefCoach { get; set; }

        public string AssistantCoaches { get; set; }

        public string Description { get; set; }

        public string Notes { get; set; }

        public string LocationName { get; set; }

        public string RegistrationUrl { get; set; }

        public DateTime EarlyBirdEndDate { get; set; }

        public PostalAddress PostalAddress { get; set; }

        public CampSectionViewModel AmSection { get; set; }

        public CampSectionViewModel MdSection { get; set; }

        public CampSectionViewModel PmSection { get; set; }

        public CampSectionViewModel FullSection { get; set; }

        public ChargeDiscountViewModel EarlyBird { get; set; }

        public ChargeDiscountViewModel MultipleWeek { get; set; }

        public ChargeDiscountViewModel Sibling { get; set; }

        public ChargeDiscountViewModel Membership { get; set; }

        public ChargeDiscountViewModel Lunch { get; set; }

        public ChargeDiscountViewModel MorningChildCare { get; set; }

        public ChargeDiscountViewModel AfternoonChildCare { get; set; }

        public PolicyViewModel Refund { get; set; }

        public PolicyViewModel Waiver { get; set; }

        public PolicyViewModel MedicalRelease { get; set; }

        public OptionalRegDataViewModel School { get; set; }

        public OptionalRegDataViewModel ParentGuardian { get; set; }

        public OptionalRegDataViewModel Insurance { get; set; }

        public OptionalRegDataViewModel SkillLevel { get; set; }

        public OptionalRegDataViewModel EmergencyContact { get; set; }

        public OptionalRegDataViewModel Doctor { get; set; }

        public ICollection<CampSchedule> Schedules { get; set; }
        public List<CampPageScheduleViewModel> Events { get; set; }

        public ICollection<CampSection> Sections { get; set; }

        public List<ChargeDiscount> ChargeDiscounts { get; set; }

        public List<ChargeDiscount> Services { get; set; }
        public List<string> ServicesOutput { get; set; }

        public bool CaptureRestriction { get; set; }

        public string ClubDomain { get; set; }
        public string Domain { get; set; } // set after post back
        public int CategoryId { get; set; }
        public string ClubName { get; set; }
        public bool Expired { get; set; }
        // add-on properties

        public GoogleMapsViewModel Map { get; set; }

        public List<CampPageImageGalleryItemViewModel> Gallery { get; set; }

        public string PdfFileName { get; set; }

        public string LogoUri { get; set; }

        public bool IsAdmin { get; set; }

        // affiliator 
        public bool HasAffiliator { get; set; }

        public string AffiliatorLogoUri { get; set; }

        public string AffiliatorDomain { get; set; }

        public string AffiliatorName { get; set; }

        public PageMetaDataViewModel MetaData { get; set; }

        public string Room { get; set; }

        public Domain.TimeZone TimeZone { get; set; }

        public string MinAgeMessage
        {
            get
            {
                if (MinAge.HasValue)
                {
                    if (MaxAge.HasValue)
                    {
                        return string.Format(Constants.Age_Restriction_Message, MinAge, MaxAge);
                    }

                    return string.Format(Constants.Min_Age_Restriction_Message, MinAge);
                }

                if (MaxAge != null)
                {
                    return string.Format(Constants.Max_Age_Restriction_Message, MaxAge);
                }

                return "";
            }
        }

        public string GradeMessage
        {
            get
            {
                if (MinGrade != null)
                {

                    if (MaxGrade != null)
                    {
                        return string.Format(Constants.Grade_Restriction_Message, MinGrade.Value.ToDescription(), MaxGrade.Value.ToDescription());
                    }
                    return string.Format(Constants.Min_Grade_Restriction_Message, MinGrade.Value.ToDescription());
                }
                if (MaxGrade != null)
                {
                    return string.Format(Constants.Max_Grade_Abbreviation, MaxGrade.Value.ToDescription());
                }
                return "";
            }
        }

        public Level Level { get; set; }
        public Genders Gender { get; set; }


        public CampPageViewModel()
        {
            Gallery = new List<CampPageImageGalleryItemViewModel>();
            Map = new GoogleMapsViewModel();
            MetaData = new PageMetaDataViewModel();
        }

        public EventStatusCategories EventStaus { get; set; }

        public bool IsOutsourcerEvent { get; set; }
        public string OutsourcerClubDomain { get; set; }
        public string OutsourcerClubName { get; set; }

        public CampPageViewModel(string domain, Camp camp)
            : this()
        {
            Domain = domain;
            Name = camp.Name;
            Name1 = camp.Name1;
            OfflineRegChecked = camp.OfflineRegChecked;
            LocationName = camp.ClubLocation.Name;
            ClubName = Ioc.IClubDbService.GetClubName(camp.ClubDomain);
            ClubDomain = camp.ClubDomain;
            PdfFileName = camp.FileName;
            PostalAddress = camp.ClubLocation.PostalAddress;
            CategoryId = camp.CategoryId;
            Room = camp.Room;
            this.TimeZone = camp.TimeZone;

            EventStaus = camp.EventStatus;

            DateTime expireDate = Utilities.CampExpireDate(camp.Schedules);
            Expired = Utilities.IsEventExpired(expireDate, camp.TimeZone);

            // schedules ( timeline )
            Schedules = camp.Schedules;

            Events = new List<CampPageScheduleViewModel>();

            int counter = 1;

            // Meta tag
            MetaData = new PageMetaDataViewModel()
            {
                PageTitle =
                    this.Name +
                    Constants.C_Space +
                    "by" +
                    Constants.C_Space +
                    this.ClubName +
                    Constants.S_SpaceDashSpace +
                    CategoryModel.GetCategoryName(this.CategoryId) +
                    Constants.C_Space + SubDomainCategories.Camp,

                MetaKeywords = string.Join(Constants.S_CommaSpace, new string[]
                        {
                            this.Name,
                            this.ClubName,
                            CategoryModel.GetCategoryName(this.CategoryId) + Constants.C_Space +
                            SubDomainCategories.Camp,
                            Utilities.StringNullHandle(this.PostalAddress.City, string.Empty),
                            Utilities.GetAbbreviationState(this.PostalAddress.State)
                        }),

                MetaDesc = this.Description,
            };

            foreach (CampSchedule schedule in camp.Schedules.OrderBy(s => s.Start))
            {
                Events.Add(new CampPageScheduleViewModel(schedule) { Position = counter });

                counter++;
            }

            Sections = camp.Sections;

            // charge discounts & services
            ChargeDiscounts = new List<ChargeDiscount>();
            //ChargeDiscountsOutput = new List<string>();

            Services = new List<ChargeDiscount>();
            ServicesOutput = new List<string>();

            foreach (ChargeDiscount discount in camp.ChargeDiscounts)
            {
                switch (discount.Category)
                {
                    // All discounts except earlybird ( end date needed )
                    case ChargeDiscountCategories.MultipleWeek:
                    case ChargeDiscountCategories.Sibling:
                    case ChargeDiscountCategories.ClubMembership:
                    case ChargeDiscountCategories.EarlyBird:
                        {
                            ChargeDiscounts.Add(discount);
                        }
                        break;
                    // All Services ( no exception )
                    case ChargeDiscountCategories.Lunch:
                    case ChargeDiscountCategories.MorningChildCare:
                    case ChargeDiscountCategories.AfternoonChildCare:
                        {
                            Services.Add(discount);
                        }
                        break;
                }
            }



            // description
            Description = camp.Description;

            // coach
            ChiefCoach = camp.ChiefCoach;
            AssistantCoaches = camp.AssistantCoaches;

            // notes section
            if (camp.EventSearchField.MinAge != null)
            {
                MinAge = camp.EventSearchField.MinAge.Value;
            }


            MaxAge = camp.EventSearchField.MaxAge;
            MaxGrade = camp.EventSearchField.MaxGrade;
            MinGrade = camp.EventSearchField.MinGrade;
            Level = camp.EventSearchField.Level.HasValue ? (Level)(camp.EventSearchField.Level) : Level.None;
            Gender = camp.EventSearchField.Gender.HasValue ? (Genders)camp.EventSearchField.Gender : Genders.BoyAndGirl;

            Notes = camp.Notes;

            if (camp.OfflineRegChecked)
            {
                RegistrationUrl = camp.ExternalRegUrl;
            }

            // map properties
            Map.Name = camp.ClubLocation.Name;
            Map.PostalAddress = camp.ClubLocation.PostalAddress;
            Map.Room = camp.Room;

            // load image gallery

            string galleryPath = string.Format("{1}/{0}", Domain, ClubDomain);

            Gallery = new List<CampPageImageGalleryItemViewModel>();


            List<Domain.ImageGalleryItem> images = Ioc.IImageGalleryDbService.GetImageGalleryEvent(camp.Domain, camp.ClubDomain);
            foreach (var item in images)
            {
                Gallery.Add(new CampPageImageGalleryItemViewModel()
                {
                    OriginUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, camp.ClubDomain, item.Path).AbsoluteUri,
                    ThumbnailUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, camp.ClubDomain, camp.Domain + "/ImageGallery/" + Path.GetFileNameWithoutExtension(item.Path) + "_Thumbnail" + Path.GetExtension(item.Path)).AbsoluteUri
                });
            }


            Club club = Ioc.IClubDbService.GetClub(ClubDomain, false);
            if (club != null)
            {
                LogoUri = Utilities.GetClubLogoURL(club.Domain, club.Logo, camp.CategoryId);
            }
            else
            {
                LogoUri = string.Empty;
            }

            // check if club has affiliator
            bool hasAffilator = false;
            string affiliatorLogoUri = string.Empty;
            string affiliatorDomain = string.Empty;
            string affiliatorName = string.Empty;
            if (club.AffiliatorID.HasValue)
            {
                hasAffilator = true;
                IClubDb clubDb = Infrastructure.Helper.Ioc.IClubDbService;
                Club affiliatorClub = clubDb.GetAffiliatorClubByAffiliatorID(club.AffiliatorID.Value);
                if (affiliatorClub != null)
                {
                    affiliatorLogoUri = Utilities.GetClubLogoURL(affiliatorClub.Domain, affiliatorClub.Logo, affiliatorClub.CategoryId);
                    affiliatorDomain = affiliatorClub.Domain;
                    affiliatorName = affiliatorClub.Name;
                }
            }
            HasAffiliator = hasAffilator;
            AffiliatorLogoUri = affiliatorLogoUri;
            AffiliatorDomain = affiliatorDomain;
            AffiliatorName = affiliatorName;

            IsAdmin = WebMatrix.WebData.WebSecurity.CurrentUserId == club.UserId;

            // Realize event is outsourcer or not
            IsOutsourcerEvent = !string.IsNullOrEmpty(camp.OutSourcerClubDomain);

            if (IsOutsourcerEvent)
            {
                OutsourcerClubDomain = camp.OutSourcerClubDomain;

                //Get outsourcer club domain from db
                OutsourcerClubName = Ioc.IClubDbService.GetClubName(OutsourcerClubDomain);
            }

            CaptureRestriction = camp.CaptureRestriction;
        }
    }

    public class CampPageImageGalleryItemViewModel
    {
        public string OriginUrl { get; set; }
        public string ThumbnailUrl { get; set; }
    }

    public class CampPageScheduleViewModel
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public EventStatusType Status { get; set; }
        public string Caption { get; set; }

        public CampPageScheduleViewModel() { }

        public CampPageScheduleViewModel(CampSchedule schedule)
        {
            Id = schedule.Id;
            StartDate = schedule.Start;
            FinishDate = schedule.End;
            Caption = schedule.Caption;

            if (DateTime.Now <= StartDate)
            {
                Status = EventStatusType.Open;
            }
            else if (DateTime.Now >= StartDate && DateTime.Now <= FinishDate)
            {
                Status = EventStatusType.OpenInProgress;
            }
            else
            {
                Status = EventStatusType.Closed;
            }
        }
    }
}