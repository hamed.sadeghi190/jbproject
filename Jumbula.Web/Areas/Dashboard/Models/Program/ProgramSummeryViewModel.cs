﻿using Jumbula.Common.Enums;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramSummeryViewModel : BaseViewModel<Program, long>
    {
        public string Name { get; set; }
        public ProgramTypeCategory TypeCategory { get; set; }
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string Domain { get; set; }
        public List<string> ChessSchedules
        {
            get
            {
                if (TypeCategory == ProgramTypeCategory.ChessTournament && Schedules != null && Schedules.Any())
                {
                    return (Schedules.First().Attributes as TournamentScheduleAttribute).Schedules.Select(s => s.GetSchName()).ToList();

                }
                return null;

            }
        }

        public List<string> ChessSections
        {
            get
            {
                if (TypeCategory == ProgramTypeCategory.ChessTournament && Schedules != null && Schedules.Any())
                {
                    return (Schedules.First().Attributes as TournamentScheduleAttribute).Sections.Select(s => s.Name).ToList();

                }
                return null;

            }
        }

        public List<JbTitleValue> StatusReason { get; set; }

        public int OrderItemsCount { get; set; }
        public decimal TotalAmount { get; set; }
        public string DisplayLink { get; set; }


        public List<SelectKeyValue<string>> SearchTypes { get; set; }
        public List<ScheduleViewModel> Schedules { get; set; }
        public List<ChargesViewModel> ProgramCharges
        {
            get

            {
                return Schedules.SelectMany(sch => sch.Charges.Where(c => c.IsDeleted == false)).ToList();
            }
        }
    }

    public class ScheduleViewModel : BaseViewModel<ProgramSchedule, long>
    {

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Title { get; set; }
        public string StrStartDate
        {
            get { return StartDate.ToString(Constants.DefaultDateFormat); }
        }
        public string StrEndDate
        {
            get { return EndDate.ToString(Constants.DefaultDateFormat); }
        }

        public string Lable
        {
            get
            {
                if (Title != null)
                {
                    return string.Format("{0} ({1}-{2})", Title, StrStartDate, StrEndDate);
                }
                else
                {
                    return string.Format("{0}-{1}", StrStartDate, StrEndDate);
                }
            }
        }
        public List<ChargesViewModel> Charges { get; set; }
        public ScheduleBaseAttribute Attributes { get; set; }
        public bool IsDeleted { get; set; }

    }
    public class ChargesViewModel : BaseViewModel<Charge, long>
    {
        public ChargeDiscountCategory Category { get; set; }
        public long ProgramScheduleId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public bool IsDeleted { get; set; }
    }


}