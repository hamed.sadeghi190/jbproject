﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('selectedSchoolReportRunController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', 'reminderInvoicesService', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, reminderInvoicesService) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();


        $scope.MC.stateParams = $stateParams;
       

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var reportName = $scope.MC.stateParams.reportName;

        $http.get(baseUrl + '/GetAllSchools')
            .success(function (data, status, headers, config) {
                $scope.Model = data;

            });
      
        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };

        $scope.runReport = function () {
            $scope.Model.ReportId = $scope.MC.stateParams.reportId;
            $scope.Model.SeasonDomain = $scope.MC.stateParams.seasonDomain;
            $scope.schools = $scope.Model._SelectedSchools;
            $scope.Model.ReportName = $scope.MC.stateParams.reportName;
            $scope.isAllProviders = $scope.Model.IsAllSchools;

            customReportService.setModelInStorage($scope.Model);

            $state.go('InvoicesPortalReports', { Model: $scope.Model });
        }

    }]);
})();
