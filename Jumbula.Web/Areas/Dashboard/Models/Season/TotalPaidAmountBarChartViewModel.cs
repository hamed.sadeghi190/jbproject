﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models.Season
{
    public class TotalPaidAmountBarChartViewModel
    {
        public DateTime DueDate { get; set; }
        public string strDueDate { get; set; }
        public decimal Amount { get; set; }
    }
}