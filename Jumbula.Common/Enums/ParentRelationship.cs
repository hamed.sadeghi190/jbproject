﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum ParentRelationship
    {
        [Description("Mother")]
        [Display(Name = "Mother")]
        Mother = 1,

        [Description("Father")]
        [Display(Name = "Father")]
        Father = 2,

        [Description("Guardian")]
        [Display(Name = "Guardian")]
        Guardian = 3,

        [Description("Grandmother")]
        [Display(Name = "Grandmother")]
        Grandmother = 4,

        [Description("Grandfather")]
        [Display(Name = "Grandfather")]
        Grandfather = 5,

        [Description("Other")]
        [Display(Name = "Other")]
        Other = 100,
    }

}
