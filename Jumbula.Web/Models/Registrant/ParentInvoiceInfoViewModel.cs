﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Generic;
using MailChimp.Ecomm;


namespace SportsClub.Models
{
    public class ParentInvoiceInfoViewModel
    {
        public ParentInvoiceInfoViewModel()
        {
            InvoiceDueDate = SeedDueDate();

        }
        public int invoiceId { get; set; }
        public DateTime Date { get; set; }
        public CurrencyCodes Currency { get; set; }
        public string StrDate
        {
            get
            {

                return Date.ToString("MMM dd, yyyy, h:mm tt");
            }
        }
        public string Description { get; set; }
        public long InvoiceNumber { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal AmountDue { get; set; }
        public string ClubSite { get; set; }
        public string ContactClubEmail { get; set; }
        public string UserEmail { get; set; }
        public string Memo { get; set; }
        public string NoteToRecipient { get; set; }
        public string MemberName { get; set; }
        public string ClubInfo { get; set; }
        public string Url { get; set; }
        public string EmailCc { get; set; }
        public string Refrense { get; set; }
        public string TimeZone { get; set; }
        public string ClubName { get; set; }
        public string ClubAddress { get; set; }
        public string ClubUrlLogo { get; set; }
        public int SelectDueDate { get; set; }
        public DateTime DueDate { get; set; }
        public string StatusInvoice { get; set; }
        public bool HasPartner { get; set; }
        public string StrDueDate
        {
            get
            {
                return DueDate.ToString("MMM dd, yyyy");
            }
        }
        public List<SelectKeyValue<int>> InvoiceDueDate { get; set; }

        List<SelectKeyValue<int>> SeedDueDate()
        {

            var result = new List<SelectKeyValue<int>>()
            {
                new SelectKeyValue<int> { Value = 0, Text = "Due on receipt"},
                new SelectKeyValue<int> { Value = 5, Text = "Due in 5 days"},
                new SelectKeyValue<int> { Value = 10, Text = "Due in 10 days"},
                new SelectKeyValue<int> { Value = 15, Text = "Due in 15 days"},
                new SelectKeyValue<int> { Value = 30, Text = "Due in 30 days"},
                new SelectKeyValue<int> { Value = 45, Text = "Due in 45 days"},
                new SelectKeyValue<int> { Value = 60, Text = "Due in 60 days"},
                new SelectKeyValue<int> { Value = 90, Text = "Due in 90 days"},
            };

            return result;
        }

    }
}