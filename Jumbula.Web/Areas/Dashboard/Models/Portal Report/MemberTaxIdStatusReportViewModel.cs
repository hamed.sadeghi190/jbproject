﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class MemberTaxIdStatusReportViewModel
    {
        public string Name { get; set; }

        public string TaxIdFEIN { get; set; }

        public string TaxIdSSN { get; set; }

        public bool Provider1099 { get; set; }

        public DateTime? AgreementExpirationDate { get; set; }
    }
}