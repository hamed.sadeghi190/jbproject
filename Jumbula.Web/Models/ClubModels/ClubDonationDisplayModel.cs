﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;

namespace SportsClub.Models.ClubModels
{
    public class ClubDonationDisplayModel
    {

        public int Id { get; set; }
        public bool IsClubDonation { get; set; }

        public string ClubDomain { get; set; }

        public int CategoryId { get; set; }

        public int OrderId { get; set; }

        public string Domain { get; set; }

        public SubDomainCategories SubDomainCategories { get; set; }
        
        public string DonationReceiveType { get; set; }

        public string Email { get; set; }

        public int? ProfileId { get; set; }
        
        public string UserName { get; set; }

        public decimal Amount { get; set; }

        public string Date { get; set; }

        public string Description { get; set; }

        public string ConfirmationId { get; set; }
    }
}