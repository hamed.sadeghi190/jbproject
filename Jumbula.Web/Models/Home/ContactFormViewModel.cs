﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace SportsClub.Models.Home
{
    public class ContactFormViewModel
    {
        [Required]
        [Display(Name = "First name")]
        [DataMember(Name = "FNAME")]
        public string FName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        [DataMember(Name = "LNAME")]
        public string LName { get; set; }

        [Required]
        [Display(Name = "Email")]
        [DataMember(Name = "EMAIL")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Phone number")]
        [DataMember(Name = "PHONE")]
        public string Phone { get; set; }

        [Required]
        [Display(Name = "Organization")]
        [DataMember(Name = "BIZNAME")]
        public string BizName { get; set; }

        [Required]
        [Display(Name = "Message")]
        [DataMember(Name = "MESSAGE")]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }

        [Required]
        [Display(Name = "Newsletter")]
        [DataMember(Name = "NEWSLETTER")]
        public bool Newsletter { get; set; }
    }
}