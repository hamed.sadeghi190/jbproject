﻿using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class SchoolDistrictElements : BaseElements
    {
        public SchoolDistrictElements(IWebDriver driver)
            : base(driver)
        {

        }
        #region Header
        public IWebElement BackLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@class='icon-arrow-left']"));
            }
        }
        public IWebElement PageTitle
        {
            get
            {
                return Driver.FindElement(By.XPath("//div[@class='title']/h1[1]"));
            }
        }

        public IWebElement AddDistrictLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@data-original-title='Add district']"));
            }
        }
        #endregion Header

        #region Filtering
        public IWebElement SearchTerm
        {
            get
            {
                return Driver.FindElement(By.Id("searchClubName"));
            }
        }
        public IWebElement SearchButton
        {
            get
            {
                return Driver.FindElement(By.Id("btn_search"));
            }
        }

        public IWebElement ClearFilterLink
        {
            get
            {
                return Driver.FindElement(By.Id("btn_clear"));
            }
        }
        #endregion Filtering

        #region GridRow1
        public bool Row1Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[1]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row1Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[1]/td[1]/span"));
            }
        }
        public IWebElement Row1Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[1]/td[2]/span"));
            }
        }
        public IWebElement Row1Address
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[1]/td[3]/span"));
            }
        }
        public IWebElement Row1Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[1]/td[4]/ul/li/span"));
            }
        }
        public IWebElement Row1ViewEditActions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[1]/td[4]/ul/li/div/ul/li/span"));
            }
        }
        #endregion GridRow1

        #region GridRow2
        public bool Row2Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[2]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row2Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[2]/td[1]/span"));
            }
        }
        public IWebElement Row2Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[2]/td[2]/span"));
            }
        }
        public IWebElement Row2Address
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[2]/td[3]/span"));
            }
        }
        public IWebElement Row2Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[2]/td[4]/ul/li/span"));
            }
        }
        public IWebElement Row2ViewEditActions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[2]/td[4]/ul/li/div/ul/li/span"));
            }
        }
        #endregion GridRow2

        #region GridRow3
        public bool Row3Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[3]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement Row3Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[3]/td[1]/span"));
            }
        }
        public IWebElement Row3Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[3]/td[2]/span"));
            }
        }
        public IWebElement Row3Address
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[3]/td[3]/span"));
            }
        }
        public IWebElement Row3Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[3]/td[4]/ul/li/span"));
            }

            
        }
        public IWebElement Row3ViewEditActions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='District']/div[2]/table/tbody/tr[3]/td[4]/ul/li/div/ul/li/span"));
            }
        }
        #endregion GridRow3

        #region AddDistrictElement
        public IWebElement Name
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Name"));
            }
        }
        public IWebElement Address
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Address"));
            }
        }
        public IWebElement Domain
        {
            get
            {
                return Driver.FindElement(By.Id("Model.Domain"));
            }
        }
        public IWebElement TimeZone
        {
            get
            {
                return Driver.FindElement(By.Id("SelectTimeZone"));
            }
        }
        public IWebElement TimeZoneDiv
        {
            get
            {
                return Driver.FindElement(By.Id("timeZoneDiv2"));
            }
        }
        public IWebElement UTCOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='UTC']"));
            }
        }
        public IWebElement PSTOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Pacific Standard Time']"));
            }
        }
        public IWebElement TimeXZoneEmptyOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='Select the organization time zone']"));
            }
        }
        public IWebElement SuperintendentName
        {
            get
            {
                return Driver.FindElement(By.Id("Model.SuperintendentName"));
            }
        }
        public IWebElement SuperintendentEmail
        {
            get
            {
                return Driver.FindElement(By.Id("Model.SuperintendentEmail"));
            }
        }
        public IWebElement SuperintendentPhone
        {
            get
            {
                return Driver.FindElement(By.Id("Model.SuperintendentPhone"));
            }
        }
        public IWebElement AreaManagerList
        {
            get
            {
                return Driver.FindElement(By.Id("SelectedAreaManager"));
            }
        }
        #region  Area Manager options
        public IWebElement AreaManagerAreaManagerStaffOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedAreaManager']/option[@label='AreaManager@jumbula.com']"));
            }
        }
        public IWebElement AreaManagerActiveStaffOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedAreaManager']/option[@label='ActiveStaff@jumbula.com']"));
            }
        }
        public bool AreaManagerPendingStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedAreaManager']/option[@label='PendingStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool AreaManagerActiveStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedAreaManager']/option[@label='ActiveStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool AreaManagerFrozenStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedAreaManager']/option[@label='FrozenStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool AreaManagerDeletedStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedAreaManager']/option[@label='DeletedStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }

        #endregion Area Manager options
        public IWebElement RegionalDirectorList
        {
            get
            {
                return Driver.FindElement(By.Id("SelectedRegionalDirector"));
            }
        }
        #region  Regional Director options
        public IWebElement RegionalDirectoradminStaffOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedRegionalDirector']/option[@label='adminstaff@jumbula.com']"));
            }
        }
        public IWebElement RegionalDirectorRegionalDirectorStaffOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedRegionalDirector']/option[@label='RegionalDirector@jumbula.com']"));
            }
        }
        public bool RegionalDirectorPendingStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedRegionalDirector']/option[@label='PendingStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool RegionalDirectorActiveStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedRegionalDirector']/option[@label='ActiveStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool RegionalDirectorFrozenStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedRegionalDirector']/option[@label='FrozenStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool RegionalDirectorDeletedStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedRegionalDirector']/option[@label='DeletedStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion Regional Director options
        public IWebElement SalesContactList
        {
            get
            {
                return Driver.FindElement(By.Id("SelectedSalesContact"));
            }
        }
        #region  Sales Contact options
        public IWebElement SalesContactPartnerStaffOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSalesContact']/option[@label='partner@jumbula.com']"));
            }
        }
        public IWebElement SalesContactSalesContactStaffOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSalesContact']/option[@label='SalesContact@jumbula.com']"));
            }
        }
        public bool SalesContactPendingStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedSalesContact']/option[@label='PendingStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool SalesContactActiveStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedSalesContact']/option[@label='ActiveStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool SalesContactFrozenStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedSalesContact']/option[@label='FrozenStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public bool SalesContactDeletedStaffOptionExist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='SelectedSalesContact']/option[@label='DeletedStaff@jumbula.com']")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion Sales Contact options
        public IWebElement PartnerOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='partner@jumbula.com']"));
            }
        }
        public IWebElement AdminOption
        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='admin@jumbula.com']"));
            }
        }
        public IWebElement NCESID
        {
            get
            {
                return Driver.FindElement(By.Id("Model.NcesId"));
            }
        }
        public IWebElement RevenueShare
        {
            get
            {
                return Driver.FindElement(By.Id("Model.RevenueShare"));
            }
        }
        public IWebElement StaffingRatio

        {
            get
            {
                return Driver.FindElement(By.XPath("//select[@ng-model='Model.SelectedStaffingRatio']"));
            }
        }
        public IWebElement StaffingRatioOption11

        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='11']"));
            }
        }
        public IWebElement StaffingRatioOption12

        {
            get
            {
                return Driver.FindElement(By.XPath("//option[@label='12']"));
            }
        }
        public IWebElement AddSubsidyBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn_AddSubsidy"));
            }
        }
        public IWebElement AddSchoolBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn_AddSchool"));
            }
        }
        public IWebElement SubsidyList
        {
            get
            {
                return Driver.FindElement(By.Id("SelectedSubsidies_chosen"));
            }
        }
        public IWebElement ChosenResultsSubsidy1
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[1]/input"));
            }
        }
        public IWebElement ChosenResultsSubsidy1Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[1]/a"));
            }
        }
        public IWebElement ChosenChoicesSubsidy1
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/div/ul/li[1]"));
            }
        }
        public IWebElement ChosenResultsSubsidy2
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[2]/input"));
            }
        }
        public IWebElement ChosenResultsSubsidy2Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[2]/a"));
            }
        }
        public IWebElement ChosenChoicesSubsidy2
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/div/ul/li[2]"));
            }
        }
        public IWebElement ChosenResultsSubsidy3
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[3]/input"));
            }
        }
        public IWebElement ChosenResultsSubsidy3Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[3]/a"));
            }
        }
        public IWebElement ChosenChoicesSubsidy3
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/div/ul/li[3]"));
            }
        }
        public IWebElement ChosenResultsSubsidy4
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[4]/input"));
            }
        }
        public IWebElement ChosenResultsSubsidy4Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[4]/a"));
            }
        }
        public IWebElement ChosenChoicesSubsidy4
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/div/ul/li[4]"));
            }
        }
        public IWebElement ChosenResultsSubsidy5
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[5]/input"));
            }
        }
        public IWebElement ChosenResultsSubsidy5Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/ul/li[5]/a"));
            }
        }
        public IWebElement ChosenChoicesSubsidy5
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSubsidies_chosen']/div/ul/li[5]"));
            }
        }
        public IWebElement CancelSubsidyBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn_cancelSubsidy"));
            }
        }
        public IWebElement SaveSubsidyBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn_saveSubsidy"));
            }
        }
        public IWebElement SchoolList
        {
            get
            {
                return Driver.FindElement(By.Id("SelectedSchools_chosen"));
            }
        }
        public IWebElement ChosenResultsSchool1
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[1]/input"));
            }
        }
        public IWebElement ChosenResultsSchool1Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[1]/a"));
            }
        }
        public IWebElement ChosenChoicesSchool1
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/div/ul/li[1]"));
            }
        }
        public IWebElement ChosenResultsSchool2
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[2]/input"));
            }
        }
        public IWebElement ChosenResultsSchool2Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[2]/a"));
            }
        }
        public IWebElement ChosenChoicesSchool2
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/div/ul/li[2]"));
            }
        }
        public IWebElement ChosenResultsSchool3
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[3]/input"));
            }
        }
        public IWebElement ChosenResultsSchool3Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[3]/a"));
            }
        }
        public IWebElement ChosenChoicesSchool3
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/div/ul/li[3]"));
            }
        }
        public IWebElement ChosenResultsSchool4
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[4]/input"));
            }
        }
        public IWebElement ChosenResultsSchool4Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[4]/a"));
            }
        }
        public IWebElement ChosenChoicesSchool4
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/div/ul/li[4]"));
            }
        }
        public IWebElement ChosenResultsSchool5
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[5]/input"));
            }
        }
        public IWebElement ChosenResultsSchool5Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[5]/a"));
            }
        }
        public IWebElement ChosenChoicesSchool5
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/div/ul/li[5]"));
            }
        }
        public IWebElement ChosenResultsSchool6
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[6]/input"));
            }
        }
        public IWebElement ChosenResultsSchool6Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[6]/a"));
            }
        }
        public IWebElement ChosenChoicesSchool6
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/div/ul/li[6]"));
            }
        }
        public IWebElement ChosenResultsSchool7
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[7]/input"));
            }
        }
        public IWebElement ChosenResultsSchool7Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[7]/a"));
            }
        }
        public IWebElement ChosenChoicesSchool7
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/div/ul/li[7]"));
            }
        }
        public IWebElement ChosenResultsSchool8
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[8]/input"));
            }
        }
        public IWebElement ChosenResultsSchool8Delete
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/ul/li[8]/a"));
            }
        }
        public IWebElement ChosenChoicesSchool8
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='SelectedSchools_chosen']/div/ul/li[8]"));
            }
        }
        public IWebElement CancelSchoolBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn_cancelSchool"));
            }
        }
        public IWebElement SaveSchoolBtn
        {
            get
            {
                return Driver.FindElement(By.Id("btn_saveSchool"));
            }
        }
        public IWebElement Submit
        {
            get
            {
                return Driver.FindElement(By.Id("btn_save"));
            }
        }

        #region Subsidies Row1
        public bool SubsidiesRow1Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SubsidiesRow1Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[1]/div"));
                                                    
            }
        }
        public IWebElement SubsidiesRow1State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[2]/span"));
                                                    
            }
        }
        public IWebElement SubsidiesRow1Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[3]/span"));
            }
        }
        public IWebElement SubsidiesRow1Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[4]/ul/li/span"));
            }
        }
        public IWebElement SubsidiesRow1DeleteActions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[1]/td[4]/ul/li/div/ul/li/span"));
                                                    //*[@id="Subsidy"]/div[2]/table/tbody/tr[1]/td[4]/ul/li/div/ul/li/span
            }
        }

        #endregion Subsidies Row1
        #region Subsidies Row2
        public bool SubsidiesRow2Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SubsidiesRow2Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[1]/div"));
            }
        }
        public IWebElement SubsidiesRow2State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[2]/span"));
            }
        }
        public IWebElement SubsidiesRow2Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[3]/span"));
            }
        }
        public IWebElement SubsidiesRow2Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[4]/ul/li/span"));
            }
        }
        public IWebElement SubsidiesRow2DeleteActions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[2]/td[4]/ul/li/div/ul/li/span"));
                                                    
            }
        }

        #endregion Subsidies Row2
        #region Subsidies Row3
        public bool SubsidiesRow3Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SubsidiesRow3Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[1]/div"));
            }
        }
        public IWebElement SubsidiesRow3State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[2]/span"));
            }
        }
        public IWebElement SubsidiesRow3Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[3]/span"));
            }
        }
        public IWebElement SubsidiesRow3Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[4]/ul/li/span"));
            }
        }
        public IWebElement SubsidiesRow3DeleteActions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[3]/td[4]/ul/li/div/ul/li/span"));
            }
        }

        #endregion Subsidies Row3
        #region Subsidies Row1
        public bool SubsidiesRow4Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SubsidiesRow4Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[1]/div"));

            }
        }
        public IWebElement SubsidiesRow4State
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[2]/span"));

            }
        }
        public IWebElement SubsidiesRow4Website
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[3]/span"));
            }
        }
        public IWebElement SubsidiesRow4Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[4]/ul/li/span"));
            }
        }
        public IWebElement SubsidiesRow4DeleteActions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='Subsidy']/div[2]/table/tbody/tr[4]/td[4]/ul/li/div/ul/li/span"));
                //*[@id="Subsidy"]/div[2]/table/tbody/tr[1]/td[4]/ul/li/div/ul/li/span
            }
        }

        #endregion Subsidies Row4
        #region Schools Row1
        public bool SchoolsRow1Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='School']/tbody/tr[1]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SchoolsRow1Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[1]/span"));
            }
        }
        public IWebElement SchoolsRow1Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[2]/span"));
            }
        }
        public IWebElement SchoolsRow1NCESID
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[3]/div"));
                                                    
            }
        }
        public IWebElement SchoolsRow1NCESIDInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[3]/div/input"));

            }
        }
        public IWebElement SchoolsRow1ProviderID1
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[4]/div"));
            }
        }
        public IWebElement SchoolsRow1ProviderID1Input
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[4]/div/input"));
            }
        }
        public IWebElement SchoolsRow1ProviderID2
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[5]/div"));
            }
        }
        public IWebElement SchoolsRow1ProviderID2Input
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[5]/div/input"));
            }
        }
        public IWebElement SchoolsRow1Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[6]/div/ul/li/span"));
            }
        }
        public IWebElement SchoolsRow1EditProviderID1Action
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[6]/div/ul/li/div[2]/ul/li[1]/span"));
            }
        }
        public IWebElement SchoolsRow1DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[6]/div/ul/li/div[2]/ul/li[2]/span"));
            }
        }
        public IWebElement SchoolsRow1Save
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[6]/div/button[1]"));
            }
        }
        public IWebElement SchoolsRow1Cancel
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[1]/td[6]/div/button[2]"));
            }
        }

        #endregion Schools Row1
        #region Schools Row2
        public bool SchoolsRow2Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='School']/tbody/tr[2]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SchoolsRow2Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[1]/span"));
            }
        }
        public IWebElement SchoolsRow2Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[2]/span"));
            }
        }
        public IWebElement SchoolsRow2NCESID
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[3]/div"));
            }
        }
        public IWebElement SchoolsRow2ProviderID1
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[4]/div"));
            }
        }
        public IWebElement SchoolsRow2ProviderID2
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[5]/div"));
            }
        }
        public IWebElement SchoolsRow2Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[6]/div/ul/li/span"));
            }
        }
        public IWebElement SchoolsRow2EditProviderID1Action
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[6]/div/ul/li/div[2]/ul/li[1]/span"));
            }
        }
        public IWebElement SchoolsRow2DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[6]/div/ul/li/div[2]/ul/li[2]/span"));
            }
        }
        public IWebElement SchoolsRow2Save
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[6]/div/button[1]"));
            }
        }
        public IWebElement SchoolsRow2Cancel
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[2]/td[6]/div/button[2]"));
            }
        }

        #endregion Schools Row2
        #region Schools Row3
        public bool SchoolsRow3Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='School']/tbody/tr[3]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SchoolsRow3Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[1]/span"));
            }
        }
        public IWebElement SchoolsRow3Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[2]/span"));
            }
        }
        public IWebElement SchoolsRow3NCESID
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[3]/div"));
                                                         
            }
        }
        public IWebElement SchoolsRow3ProviderID1
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[4]/div"));
            }
        }
        public IWebElement SchoolsRow3ProviderID2
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[5]/div"));
            }
        }
        public IWebElement SchoolsRow3Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[6]/div/ul/li/span"));
            }
        }
        public IWebElement SchoolsRow3EditProviderID1Action
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[6]/div/ul/li/div[2]/ul/li[1]/span"));
            }
        }
        public IWebElement SchoolsRow3DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[6]/div/ul/li/div[2]/ul/li[2]/span"));
            }
        }
        public IWebElement SchoolsRow3Save
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[6]/div/button[1]"));
            }
        }
        public IWebElement SchoolsRow1Cancel3
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[3]/td[6]/div/button[2]"));
            }
        }

        #endregion Schools Row3
        #region Schools Row4
        public bool SchoolsRow4Exist
        {
            get
            {
                if (Driver.FindElements(By.XPath("//*[@id='School']/tbody/tr[4]")).FirstOrDefault() != null)
                {
                    return true;
                }
                return false;
            }
        }
        public IWebElement SchoolsRow4Name
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[1]/span"));
            }
        }
        public IWebElement SchoolsRow4Domain
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[2]/span"));
            }
        }
        public IWebElement SchoolsRow4NCESID
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[3]/div"));

            }
        }
        public IWebElement SchoolsRow4NCESIDInput
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[3]/div/input"));

            }
        }
        public IWebElement SchoolsRow4ProviderID1
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[4]/div"));
            }
        }
        public IWebElement SchoolsRow4ProviderID1Input
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[4]/div/input"));
            }
        }
        public IWebElement SchoolsRow4ProviderID2
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[5]/div"));
            }
        }
        public IWebElement SchoolsRow4ProviderID2Input
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[5]/div/input"));
            }
        }
        public IWebElement SchoolsRow4Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[6]/div/ul/li/span"));
            }
        }
        public IWebElement SchoolsRow4EditProviderID1Action
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[6]/div/ul/li/div[2]/ul/li[1]/span"));
            }
        }
        public IWebElement SchoolsRow4DeleteAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[6]/div/ul/li/div[2]/ul/li[2]/span"));
            }
        }
        public IWebElement SchoolsRow4Save
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[6]/div/button[1]"));
            }
        }
        public IWebElement SchoolsRow4Cancel
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='School']/tbody/tr[4]/td[6]/div/button[2]"));
            }
        }

        #endregion Schools Row4
        #endregion AddDistrictElement
    }
}
