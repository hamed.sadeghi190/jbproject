﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Enums;

namespace SportsClub.Models
{
    public class OrderItemFollowUpFormViewModel
    {
        public long OrderItemId { get; set; }
        public OrderItemStatusReasons ItemStatusReasons { get; set; }
        public List<UserFollowupFormDetails> FollowUpForms { get; set; }
        public JbForm Form { get; set; }
    }

}