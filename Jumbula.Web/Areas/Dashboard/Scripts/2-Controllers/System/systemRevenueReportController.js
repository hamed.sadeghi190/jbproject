﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemRevenueReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.stateParams = $stateParams;
   
        $scope.reportType = $scope.stateParams.reportType;

        $scope.MC.stateParams = $stateParams;


        $http.get($scope.baseUrl + '/GetSystemRevenue').success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });
        

        var baseUrl = window.location.origin + '/Dashboard/System';

        var totalAmount;

        $scope.Model = customReportService.getModelFromStrorage();

        var colors = ["#e892a4", "#db530e", "#9e0020"];

        function getTotalTransaction() {
            $http.get(baseUrl + '/GetTotalTransaction', { params: { start: $scope.Model.UpdateFrom, end: $scope.Model.UpdateTo, PaymentMethod: $scope.Model.PaymentMethod } }).success(function (data, status, headers, config) {

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        getTotalTransaction();
     $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: baseUrl + '/GetSystemRevenue',
                    type: "POST",
                    data: function () {
                        return {
                            end: $scope.Model.UpdateTo,
                            start: $scope.Model.UpdateFrom
                        };
                    },
                },
            },
            pageSize: 50,
            batch: true,
            scrollable: true,
            serverPaging: true,
            serverSorting: true,

            schema: {
                data: "data",
                total: "total"
            },
            aggregate: [
                        { field: "ChargeWithCredit", aggregate: "sum" },
                         { field: "ChareWithManualy", aggregate: "sum" },
                         { field: "Percent", aggregate: "sum" },
                         { field: "TotalRevenue", aggregate: "sum" },

            ],

        });
        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            resizable: false,
            columns: [
                {
                    field: "DateFormat",
                    title: "Date",
                    width: "200px",
                   
                },
                {
                    field: "ChargeWithCredit",
                    title: "Charge",
                    width: "200px",
                    format: "{0:c2}",
                    footerTemplate: ' #= kendo.toString(sum, "c2") #'

                },

                {
                    field: "ChareWithManualy",
                    title: "Manual Charge",
                    width: "200px",
                    format: "{0:c2}",
                    footerTemplate: ' #= kendo.toString(sum, "c2") #'
                },
                {
                    field: "Percent",
                    title: "Percent",
                    width: "200px",
                    format: "{0:c2}",
                   footerTemplate: ' #= kendo.toString(sum, "c2") #'
                },

                 {
                     field: "TotalRevenue",
                     title: "Total Revenue",
                     width: "200px",
                     format: "{0:c2}",
                     footerTemplate: ' #= kendo.toString(sum, "c2") #'
                 },
                  {
                      title: "Actions",
                      width: "70px",
                      headerTemplate: "<span class='actions-title'>Actions</span>",
                      template: '<ul kendo-menu style="display: inline-block;position:absolute" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li  ui_sref="SystemRevenueViewDetails({reportType:  \'Revenue\', date:\'#=Date#\'})">\
                                          <span class="k-link">View Details</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>'
                  }
            ],
        }

        $scope.filterData = function () {
            getTotalTransaction();
            $scope.mainGridOptions.dataSource.read();
            $scope.pieChartOption.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.UpdateFrom = "";
            $scope.Model.UpdateTo = "";
            $scope.Model.PaymentMethod = "";
            $scope.mainGridOptions.dataSource.read();
            $scope.pieChartOption.dataSource.read();
        }
    }
    ]);

})()
