﻿using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
using Jumbula.Core.Model.Generic;
using System.Web.Mvc;

namespace Jumbula.Business
{
    public class SeasonBusiness : ISeasonBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IStorageBusiness _storageBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IRepository<Season, long> _seasonRepository;
        private readonly ISeasonPaymentPlanBusiness _seasonPaymentPlanBusiness;
        public SeasonBusiness(IClubBusiness clubBusiness, IStorageBusiness storageBusiness, IProgramBusiness programBusiness, IRepository<Season, long> seasonRepository , ISeasonPaymentPlanBusiness seasonPaymentPlanBusiness)
        {
            _clubBusiness = clubBusiness;
            _storageBusiness = storageBusiness;
            _programBusiness = programBusiness;
            _seasonRepository = seasonRepository;
            _seasonPaymentPlanBusiness = seasonPaymentPlanBusiness;
        }

        public Season Get(long id)
        {
            return _seasonRepository.Get(s => s.Id == id);
        }
        public Season Get(string seasonDomain, int clubId)
        {
            return _seasonRepository.GetList().SingleOrDefault(s => s.Status != SeasonStatus.Deleted && s.Domain == seasonDomain && s.ClubId == clubId);
        }
        public IQueryable<Season> GetList()
        {
            return _seasonRepository.GetList().Where(s => s.Status != SeasonStatus.Deleted);
        }
        public IQueryable<Season> GetList(int clubId)
        {
            return GetList().Where(s => s.ClubId == clubId);
        }

        public IQueryable<Season> GetList(long clubId)
        {
            return GetList().Where(s => s.ClubId == clubId);
        }

        public OperationStatus Create(Season season)
        {
            season.MetaData.DateCreated = DateTime.UtcNow;
            season.MetaData.DateUpdated = DateTime.UtcNow;

            return _seasonRepository.Create(season);
        }

        public OperationStatus Update(Season season)
        {
            season.MetaData.DateUpdated = DateTime.UtcNow;
            if (season.IsSettingChanged)
            {
                season.SettingSerialized = JsonConvert.SerializeObject(season.Setting);
            }
            return _seasonRepository.Update(season);
        }

        public OperationStatus Delete(Season season)
        {
            throw new NotImplementedException();
        }

        public OperationStatus Delete(long seasonId)
        {
            return _seasonRepository.Delete(seasonId);
        }

        public OperationStatus SaveEmailTemplate(EmailTemplate template, long seasonId)
        {
            try
            {
                var season = _seasonRepository.Get(s => s.Id == seasonId);
                season.Template = template;
                _seasonRepository.Update(season);
                return new OperationStatus { Status = true };
            }
            catch (Exception e)
            {
                return new OperationStatus { Status = false, Message = e.Message, ExceptionInnerMessage = e.InnerException.Message };
            }

        }

        public OperationStatus UpdateEmailTemplate(EmailTemplate template, long seasonId)
        {
            try
            {
                var season = _seasonRepository.Get(s => s.Id == seasonId);
                var templateDb = season.Template;

                templateDb.Template = template.Template;
                templateDb.TemplateName = template.TemplateName;

                _seasonRepository.Update(season);
                return new OperationStatus { Status = true };
            }
            catch (Exception e)
            {
                return new OperationStatus { Status = false, Message = e.Message, ExceptionInnerMessage = e.InnerException.Message };
            }
        }

        public EmailTemplate GetSeasonEmailTemplate(long seasonId)
        {
            return _seasonRepository.Get(s => s.Id == seasonId).Template;
        }

        public string GenerateSeasonDomain(string seasonTitle, int clubId)
        {
            string domain = string.Empty;
            bool success = false;
            var i = 0;

            domain = JumbulaDomainHelper.GenerateRandomDomain(seasonTitle, i);
            while (DoesSeasonDomainExist(domain, clubId))
            {
                i++;
                domain = JumbulaDomainHelper.GenerateRandomDomain(seasonTitle, i);
            }

            if (!DoesSeasonDomainExist(domain, clubId))
                success = true;

            if (success == false)
                throw new Exception("Dmain generation failed");

            return domain;
        }


        public bool DoesSeasonDomainExist(string domain, int clubId)
        {
            return GetList().Any(s => s.Domain.ToLower() == domain.ToLower() && s.ClubId == clubId);
        }

        public bool DoesSeasonTitleExist(string title, int clubId, long seasonId)
        {
            return GetList().Any(s => s.Id != seasonId && s.Title.ToLower() == title.ToLower() && s.ClubId == clubId);
        }

        public DateTime GetFirstStartDateClassInSeason(string seasonDomain)
        {
            DateTime startDate;
            var season = _seasonRepository.GetList().Single(s => s.Domain == seasonDomain);
            if (season.Setting != null && ((SeasonSchoolSetting)season.Setting).SchoolSeasonGeneralSetting != null &&
                ((SeasonSchoolSetting)season.Setting).SeasonProgramInfoSetting.StartDate.HasValue)
            {
                startDate = ((SeasonSchoolSetting)season.Setting).SeasonProgramInfoSetting.StartDate.Value;
            }
            else
            {
                startDate = season.Programs.SelectMany(p => p.ProgramSchedules).Min(ps => ps.StartDate);
            }

            return startDate;
        }

        public SeasonRegStatus GetRegStatus(Season season)
        {
            var clubDateTime = _clubBusiness.GetClubDateTime(season.ClubId, DateTime.UtcNow);

            var result = SeasonRegStatus.Closed;

            bool earlyNotOpened = false;
            bool generalNotOpened = false;
            bool lateNotOpened = false;
            bool earlyClosed = false;
            bool generalClosed = false;
            bool preRegistrationOpened = false;
            bool lotteryOpened = false;
            bool closed = false;

            if (season.Setting != null)
            {
                var seasonProgramInfoSettings = season.Setting.SeasonProgramInfoSetting;

                if (seasonProgramInfoSettings != null)
                {
                    var earlyRegistrationOpens = seasonProgramInfoSettings.EarlyRegistrationOpens.HasValue ? seasonProgramInfoSettings.EarlyRegistrationOpens.Value : DateTime.MinValue;
                    var earlyRegistrationCloses = seasonProgramInfoSettings.EarlyRegistrationCloses.HasValue ? seasonProgramInfoSettings.EarlyRegistrationCloses.Value : DateTime.MaxValue;

                    var generalRegistrationOpens = seasonProgramInfoSettings.GeneralRegistrationOpens.HasValue ? seasonProgramInfoSettings.GeneralRegistrationOpens.Value : DateTime.MinValue;
                    var generalRegistrationCloses = seasonProgramInfoSettings.GeneralRegistrationCloses.HasValue ? seasonProgramInfoSettings.GeneralRegistrationCloses.Value : DateTime.MaxValue;

                    var lateRegistrationOpens = seasonProgramInfoSettings.LateRegistrationOpens.HasValue ? seasonProgramInfoSettings.LateRegistrationOpens.Value : DateTime.MinValue;
                    var lateRegistrationCloses = seasonProgramInfoSettings.LateRegistrationCloses.HasValue ? seasonProgramInfoSettings.LateRegistrationCloses.Value : DateTime.MaxValue;

                    var preRegistrationOpens = seasonProgramInfoSettings.PreRegistrationOpens.HasValue ? seasonProgramInfoSettings.PreRegistrationOpens.Value : DateTime.MinValue;
                    var preRegistrationCloses = seasonProgramInfoSettings.PreRegistrationCloses.HasValue ? seasonProgramInfoSettings.PreRegistrationCloses.Value : DateTime.MaxValue;

                    var lotteryOpens = seasonProgramInfoSettings.LotteryOpens.HasValue ? seasonProgramInfoSettings.LotteryOpens.Value : DateTime.MinValue;
                    var lotteryCloses = seasonProgramInfoSettings.LotteryCloses.HasValue ? seasonProgramInfoSettings.LotteryCloses.Value : DateTime.MaxValue;


                    // Early 
                    if (seasonProgramInfoSettings.HasEarlyRegistration)
                    {
                        if (earlyRegistrationOpens <= clubDateTime && earlyRegistrationCloses >= clubDateTime)
                        {
                            return SeasonRegStatus.EarlyOpen;
                        }
                        else
                        {
                            if (earlyRegistrationOpens > clubDateTime)
                            {
                                earlyNotOpened = true;
                            }
                            else if (earlyRegistrationCloses < clubDateTime)
                            {
                                earlyClosed = true;
                            }
                        }
                    }

                    // General 
                    if (seasonProgramInfoSettings.GeneralRegistrationOpens.HasValue && seasonProgramInfoSettings.GeneralRegistrationCloses.HasValue)
                    {
                        if (generalRegistrationOpens <= clubDateTime && generalRegistrationCloses >= clubDateTime)
                        {
                            return SeasonRegStatus.GeneralOpen;
                        }
                        else
                        {
                            if (generalRegistrationOpens > clubDateTime)
                            {
                                generalNotOpened = true;
                            }
                            else if (generalRegistrationCloses < clubDateTime)
                            {
                                generalClosed = true;
                            }
                        }
                    }
                    else
                    {

                        if (seasonProgramInfoSettings.HasPreRegistration)
                        {
                            if (preRegistrationOpens <= clubDateTime && preRegistrationCloses >= clubDateTime)
                            {
                                return SeasonRegStatus.PreRegisterOpen;
                            }
                        }

                        return SeasonRegStatus.GeneralOpen;
                    }

                    // Late 
                    if (seasonProgramInfoSettings.HasLateRegistration)
                    {
                        if (lateRegistrationOpens <= clubDateTime && lateRegistrationCloses >= clubDateTime)
                        {
                            return SeasonRegStatus.LateOpen;
                        }
                        else
                        {
                            if (lateRegistrationOpens > clubDateTime)
                            {
                                lateNotOpened = true;
                            }
                            else if (lateRegistrationCloses < clubDateTime)
                            {
                                closed = true;
                            }
                        }
                    }

                    // Pre register 
                    if (seasonProgramInfoSettings.HasPreRegistration)
                    {
                        if (preRegistrationOpens <= clubDateTime && preRegistrationCloses >= clubDateTime)
                        {
                            preRegistrationOpened = true;
                        }
                    }

                    // Lottery
                    if (seasonProgramInfoSettings.HasLottery)
                    {
                        if (lotteryOpens <= clubDateTime && lotteryCloses >= clubDateTime)
                        {
                            lotteryOpened = true;
                        }
                    }

                    if ((generalClosed && !seasonProgramInfoSettings.HasLateRegistration) || closed)
                    {
                        return SeasonRegStatus.Closed;
                    }
                    else if (generalClosed)
                    {
                        return SeasonRegStatus.GeneralClosed;
                    }
                    else if (earlyClosed)
                    {
                        return SeasonRegStatus.EarlyClosed;
                    }
                }
                else
                {
                    return SeasonRegStatus.GeneralOpen;
                }

                if (preRegistrationOpened)
                {
                    return SeasonRegStatus.PreRegisterOpen;
                }

                if (lotteryOpened)
                {
                    return SeasonRegStatus.LotteryOpen;
                }

                if (earlyNotOpened || generalNotOpened || lateNotOpened)
                {
                    return SeasonRegStatus.NotOpened;
                }
            }
            else
            {
                return SeasonRegStatus.GeneralOpen;
            }

            return result;
        }

        public bool IsPreRegistrationMode(Season season)
        {
            return GetRegStatus(season) == SeasonRegStatus.PreRegisterOpen;
        }

        public bool IsLotteryMode(Season season)
        {
            return GetRegStatus(season) == SeasonRegStatus.LotteryOpen;
        }

        public bool IsGeneralRegistrationOpen(Season season)
        {
            return GetRegStatus(season) == SeasonRegStatus.GeneralOpen;
        }

        public ClubFormTemplate GetSeasonRegistrationForm(Season season)
        {
            ClubFormTemplate result = null;

            var club = season.Club;

            if (season.Setting != null)
            {
                var seasonFromSettings = season.Setting.SeasonFormsSetting;
                if (seasonFromSettings != null)
                {
                    var formTemplate = club.ClubFormTemplates.SingleOrDefault(c => c.Id == seasonFromSettings.Form);

                    if (formTemplate != null)
                    {
                        result = formTemplate;
                    }
                }
            }

            if (result == null)
            {
                var defaultForm = club.ClubFormTemplates.SingleOrDefault(f => f.FormType == FormType.Registration && !f.IsDeleted && f.Title.Equals(Constants.W_Default));

                if (defaultForm == null)
                {
                    _clubBusiness.AddDefaultTemplates(club.Id);

                    defaultForm = club.ClubFormTemplates.SingleOrDefault(f => f.FormType == FormType.Registration && !f.IsDeleted && f.Title.Equals(Constants.W_Default));
                }

                result = defaultForm;
            }

            return result;
        }

        public List<ClubWaiver> GetSeasonWaivers(Season season)
        {
            var result = new List<ClubWaiver>();

            if (season.Setting != null)
            {
                var seasonFromSettings = season.Setting.SeasonFormsSetting;
                if (seasonFromSettings != null)
                {
                    var clubWaiverIds = season.Setting.SeasonFormsSetting.Waivers;

                    if (clubWaiverIds != null && clubWaiverIds.Any())
                    {
                        result = season.Club.ClubWaivers.Where(c => !c.IsDeleted && clubWaiverIds.Contains(c.Id)).ToList();
                    }
                }
            }

            return result;
        }

        public bool HasSeasonWaiver(Season season)
        {
            return GetSeasonWaivers(season).Any();
        }

        public List<PaymentPlan> GetMultRegisterPaymentPlan(Season season , int userId)
        {
            var paymentPlanLists = new List<PaymentPlan>();

            List<PaymentPlan> paymentPlanList = null;

            paymentPlanList = _seasonPaymentPlanBusiness.GetPaymentPlansByUserIdForMultiRegister(season, userId).OrderBy(c => c.NumOfInstallments).ToList();

            foreach (var paymentPlan in paymentPlanList)
            {
                var numOfInstallment = paymentPlan.ListOfInstallmentDates.Count(d => Convert.ToDateTime(d).CompareTo(DateTime.UtcNow.Date) > 0);

                if (numOfInstallment > 0)
                {
                    paymentPlanLists.Add(paymentPlan);
                }
            }

            return paymentPlanLists;
        }

        public bool HasSeasonMultRegisterPaymentPlan(Season season , int userId)
        {
            return GetMultRegisterPaymentPlan(season , userId).Any();
        }
        public DateTime? GetGeneralRegOpenDate(Season season)
        {
            if (season == null)
            {
                return null;
            }

            if (season.Setting == null || season.Setting.SeasonProgramInfoSetting == null)
            {
                return null;
            }

            var seasonProgramInfoSetting = season.Setting.SeasonProgramInfoSetting;

            return seasonProgramInfoSetting.GeneralRegistrationOpens;
        }

        public DateTime? GetGeneralRegCloseDate(Season season)
        {
            if (season == null)
            {
                return null;
            }

            if (season.Setting == null || season.Setting.SeasonProgramInfoSetting == null)
            {
                return null;
            }

            var seasonProgramInfoSetting = season.Setting.SeasonProgramInfoSetting;

            return seasonProgramInfoSetting.GeneralRegistrationCloses;
        }

        public DateTime? GetLotteryCloseDate(Season season)
        {
            if (season == null)
            {
                return null;
            }

            if (season.Setting == null || season.Setting.SeasonProgramInfoSetting == null)
            {
                return null;
            }

            var seasonProgramInfoSetting = season.Setting.SeasonProgramInfoSetting;

            return seasonProgramInfoSetting.LotteryCloses;
        }

        public Charge GetApplicationFee(Season season)
        {
            var charge = new Charge();
            charge = season.Charges.SingleOrDefault(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted);

            return charge;
        }

        #region Replicate Seasons

        public OperationStatus ReplicateDistrictSeasonToMembers(int districtId, long seasonId, int? memberId)
        {
            var members = _clubBusiness.GetRelatedClubs(districtId).Where(d => !d.IsDeleted).ToList();

            if (memberId.HasValue)
            {
                members = members.Where(m => m.Id == memberId.Value).ToList();
            }

            var season = Get(seasonId);

            foreach (var member in members)
            {
                List<ClubFormTemplate> clubFormTemplates = _clubBusiness.GetFormTemplates(districtId, null).ToList();

                foreach (var clubFormTemplate in clubFormTemplates)
                {
                    if (!member.ClubFormTemplates.Where(c => !c.IsDeleted && !string.IsNullOrWhiteSpace(c.Title)).Select(s => s.Title.ToLower()).Contains(clubFormTemplate.Title.ToLower()))
                    {
                        member.ClubFormTemplates.Add(GetReplicateClubFormTemplate(clubFormTemplate, member));
                    }
                }

                List<ClubWaiver> clubWaivers = _clubBusiness.GetWaivers(districtId).ToList();

                foreach (var clubWaiver in clubWaivers)
                {
                    if (!member.ClubWaivers.Where(w => !w.IsDeleted).Select(s => s.Name.ToLower()).Contains(clubWaiver.Name.ToLower()))
                    {
                        member.ClubWaivers.Add(GetReplicateWaiver(clubWaiver, member));
                    }
                }

                var newSeasonDomain = GenerateSeasonDomain(season.Title, member.Id);

                GetReplicateSeason(season, member, newSeasonDomain);
            }

            var result = _seasonRepository.Save();

            return result;

        }

        private void GetReplicateSeason(Season season, Club club, string newSeasonDomain)
        {
            Season newSeason = null;

            if (IsSeasonNotExistInMember(club, season))
            {
                newSeason = GetSeasonForReplicate(season, club, newSeasonDomain);

                club.Seasons.Add(newSeason);
            }
            else
            {
                newSeason = club.Seasons.FirstOrDefault(s => s.ParentId == season.Id);
                newSeasonDomain = newSeason.Domain;
            }

            var newDiscounts = GetDiscountsForReplicate(season.Discounts.Where(d => !d.IsDeleted).ToList());

            foreach (var discount in newDiscounts)
            {
                if (!newSeason.Discounts.Any(d => !d.IsDeleted && d.Name.Equals(discount.Name, StringComparison.OrdinalIgnoreCase)))
                {
                    newSeason.Discounts.Add(discount);
                }
            }

            var newPaymentPlans = GetPaymentPlanForReplicate(season.PaymentPlans.Where(p => !p.IsDeleted).ToList());

            foreach (var paymentPlan in newPaymentPlans)
            {
                if (!newSeason.PaymentPlans.Where(p => !p.IsDeleted && !string.IsNullOrWhiteSpace(p.Name)).Any(d => d.Name.Equals(paymentPlan.Name, StringComparison.OrdinalIgnoreCase)))
                {
                    newSeason.PaymentPlans.Add(paymentPlan);
                }
            }

            var newCoupons = GetCouponForReplicate(season.Coupons.Where(c => !c.Deleted).ToList());

            foreach (var coupon in newCoupons)
            {
                if (!newSeason.Coupons.Where(c => !c.Deleted && !string.IsNullOrWhiteSpace(c.Name)).Any(d => d.Name.Equals(coupon.Name, StringComparison.OrdinalIgnoreCase)))
                {
                    newSeason.Coupons.Add(coupon);
                }
            }

            if (newSeason.Programs == null)
            {
                newSeason.Programs = new List<Program>();
            }

            foreach (var program in season.Programs.Where(p => p.Status != ProgramStatus.Deleted))
            {
                if (IsProgramNotExistInSeason(newSeason, program.Id))
                {
                    newSeason.Programs.Add(GetReplicateProgram(program, club, newSeason, newSeasonDomain));
                }
            }

        }

        private bool IsProgramNotExistInSeason(Season season, long programId)
        {
            return !season.Programs.Any(p => p.Status != ProgramStatus.Deleted && p.ParentId == programId);
        }

        private Season GetSeasonForReplicate(Season season, Club club, string newSeasonDomain)
        {
            var now = DateTime.UtcNow;

            var result = new Season()
            {
                ClubId = club.Id,
                Description = season.Description,
                Domain = newSeasonDomain,
                HasOutSourcePrograms = season.HasOutSourcePrograms,
                IsSandBox = season.IsSandBox,
                IsSettingChanged = season.IsSettingChanged,
                MetaData = new MetaData
                {
                    DateCreated = now,
                    DateUpdated = now
                },
                Name = season.Name,
                Note = season.Note,
                Status = season.Status,
                Title = season.Title,
                Year = season.Year,
                ParentId = season.Id,
                Template = GetEmailTemplate(season.Template, club)
            };

            return result;
        }

        private EmailTemplate GetEmailTemplate(EmailTemplate sourceEmailTemplate, Club destinationClub)
        {
            if (sourceEmailTemplate == null)
            {
                return null;
            }

            var result = new EmailTemplate();

            result.Club_Id = destinationClub.Id;
            result.TemplateName = sourceEmailTemplate.TemplateName;
            result.Type = sourceEmailTemplate.Type;

            string jsonModel = null;
            #region FillJsonTemplate

            var sourceEmailTemplateModel = new EmailTemplateModel();
            if (!string.IsNullOrEmpty(sourceEmailTemplate.Template))
            {
                sourceEmailTemplateModel = JsonConvert.DeserializeObject<EmailTemplateModel>(sourceEmailTemplate.Template);
            }

            var emailTemplateModel = new EmailTemplateModel()
            {
                Body = sourceEmailTemplateModel.Body,
                BodyBackColor = sourceEmailTemplateModel.BodyBackColor,
                ChangeTemplate = sourceEmailTemplateModel.ChangeTemplate,
                ClubDomain = destinationClub.Domain,
                CongratulationMessage = sourceEmailTemplateModel.CongratulationMessage,
                CustomMessage = sourceEmailTemplateModel.CustomMessage,
                FooterBackground = sourceEmailTemplateModel.FooterBackground,
                FooterText = sourceEmailTemplateModel.FooterText,
                HasFooter = sourceEmailTemplateModel.HasFooter,
                HasFooterText = sourceEmailTemplateModel.HasFooterText,
                HasHeader = sourceEmailTemplateModel.HasHeader,
                HasHeaderText = sourceEmailTemplateModel.HasHeaderText,
                HasLogo = sourceEmailTemplateModel.HasLogo,
                HasMap = sourceEmailTemplateModel.HasMap,
                HasMoreQuestions = sourceEmailTemplateModel.HasMoreQuestions,
                HeaderBackground = sourceEmailTemplateModel.HeaderBackground,
                HeaderText = sourceEmailTemplateModel.HeaderText,
                IsRegisterationEmail = sourceEmailTemplateModel.IsRegisterationEmail,
                Locations = sourceEmailTemplateModel.Locations,
                MapLocation = sourceEmailTemplateModel.MapLocation,
                MapZoom = sourceEmailTemplateModel.MapZoom,
                MoreQuestionsEmail = sourceEmailTemplateModel.MoreQuestionsEmail,
                MoreQuestionsPhone = sourceEmailTemplateModel.MoreQuestionsPhone,
                Name = sourceEmailTemplateModel.Name,
                NewName = sourceEmailTemplateModel.NewName,
                UseProgramLocation = sourceEmailTemplateModel.UseProgramLocation,

            };

            if (destinationClub.PartnerId.HasValue)
            {
                emailTemplateModel.ClubLogo = _clubBusiness.GetClubLogoURL(destinationClub.PartnerClub.Domain, destinationClub.PartnerClub.Logo);
            }
            else
            {
                emailTemplateModel.ClubLogo = _clubBusiness.GetClubLogoURL(destinationClub.Domain, "");
            }

            #endregion
            jsonModel = JsonConvert.SerializeObject(emailTemplateModel);

            result.Template = jsonModel;

            return result;
        }

        private List<PaymentPlan> GetPaymentPlanForReplicate(List<PaymentPlan> paymentPlans)
        {
            var now = DateTime.UtcNow;
            return paymentPlans.Select(p =>
                                    new PaymentPlan()
                                    {
                                        Deposit = p.Deposit,
                                        DepositType = p.DepositType,
                                        ExpiryDate = p.ExpiryDate,
                                        InstallmentDueDates = p.InstallmentDueDates,
                                        InstallmentFee = p.InstallmentFee,
                                        IsAllProgram = p.IsAllProgram,
                                        IsAllUser = p.IsAllUser,
                                        IsAutoChargeMandatory = p.IsAutoChargeMandatory,
                                        IsDeleted = p.IsDeleted,
                                        MetaData = new MetaData { DateCreated = now, DateUpdated = now },
                                        Name = p.Name,
                                        NumOfInstallments = p.NumOfInstallments,
                                        Users = p.Users.Select(u =>
                                                        new UserPaymentPlan
                                                        {
                                                            UserId = u.UserId,
                                                        })
                                                        .ToList(),
                                    })
                                    .ToList();
        }

        private List<Coupon> GetCouponForReplicate(List<Coupon> coupons)
        {
            var now = DateTime.UtcNow;
            return coupons.Select(c =>
                                    new Coupon()
                                    {
                                        AddType = c.AddType,
                                        Amount = c.Amount,
                                        AmounType = c.AmounType,
                                        ClubDomain = c.ClubDomain,
                                        Code = c.Code,
                                        CouponCalculateType = c.CouponCalculateType,
                                        CouponType = c.CouponType,
                                        Deleted = c.Deleted,
                                        EndDate = c.EndDate,
                                        IsAllProgram = c.IsAllProgram,
                                        IsForAllUsers = c.IsForAllUsers,
                                        IsOneTimeUse = c.IsOneTimeUse,
                                        MetaData = new MetaData { DateCreated = now, DateUpdated = now },
                                        Name = c.Name,
                                        NumberOfUse = c.NumberOfUse,
                                        StartDate = c.StartDate,
                                    })
                                    .ToList();
        }

        private List<Discount> GetDiscountsForReplicate(List<Discount> discounts)
        {
            var now = DateTime.UtcNow;
            return discounts.Select(d =>
                                    new Discount()
                                    {
                                        Amount = d.Amount,
                                        AmountType = d.AmountType,
                                        ApplyType = d.ApplyType,
                                        BenefitType = d.BenefitType,
                                        Category = d.Category,
                                        Description = d.Description,
                                        EligibleMode = d.EligibleMode,
                                        IsAllProgram = d.IsAllProgram,
                                        IsDeleted = d.IsDeleted,
                                        MetaData = new MetaData { DateCreated = now, DateUpdated = now },
                                        Name = d.Name,
                                        NOP = d.NOP,
                                    })
                                    .ToList();
        }

        private ClubWaiver GetReplicateWaiver(ClubWaiver clubWaiver, Club club)
        {
            var now = DateTime.UtcNow;
            var result = new ClubWaiver()
            {
                Club_Id = club.Id,
                DateCreated = now,
                IsDeleted = clubWaiver.IsDeleted,
                IsRequired = clubWaiver.IsRequired,
                Name = clubWaiver.Name,
                Text = clubWaiver.Text,
                WaiverConfirmationType = clubWaiver.WaiverConfirmationType
            };

            return result;
        }

        private ClubFormTemplate GetReplicateClubFormTemplate(ClubFormTemplate clubFormTemplate, Club club)
        {
            var now = DateTime.UtcNow;

            var result = new ClubFormTemplate()
            {
                ClubId = club.Id,
                DateCreated = now,
                DefaultFormType = clubFormTemplate.DefaultFormType,
                FormType = clubFormTemplate.FormType,
                IsDeleted = clubFormTemplate.IsDeleted,
                Title = clubFormTemplate.Title,
                JbForm = new Jb.Framework.Common.Forms.JbForm()
                {
                    AccessRole = clubFormTemplate.JbForm.AccessRole,
                    CreatedDate = now,
                    CurrentMode = clubFormTemplate.JbForm.CurrentMode,
                    HelpText = clubFormTemplate.JbForm.HelpText,
                    JsonElements = clubFormTemplate.FormType == FormType.UploadForm ? GetSublamentalForm(clubFormTemplate.JbForm.Elements, club.Domain) : clubFormTemplate.JbForm.JsonElements,
                    LastModifiedDate = clubFormTemplate.JbForm.LastModifiedDate,
                    RefEntityName = clubFormTemplate.JbForm.RefEntityName,
                    Title = clubFormTemplate.JbForm.Title,
                    VisibleMode = clubFormTemplate.JbForm.VisibleMode,
                },
            };

            return result;
        }

        private string GetSublamentalForm(List<IJbBaseElement> elements, string targetClubDomian)
        {
            string result = null;
            var resultElements = new List<IJbBaseElement>();

            foreach (var secElement in elements.Where(e => e is JbSection))
            {
                var section = secElement as JbSection;

                foreach (var item in section.Elements.Where(e => e is JbUpload))
                {
                    var element = item as JbUpload;
                    var sourceFileUrl = element.GetValue();
                    string targetFileUrl = null;

                    if (!string.IsNullOrWhiteSpace(sourceFileUrl))
                    {
                        var firstPart = sourceFileUrl.Substring(0, sourceFileUrl.LastIndexOf(Constants.Path_ClubFilesContainer + "/")) + Constants.Path_ClubFilesContainer + "/";
                        var middlePart = targetClubDomian;
                        var lastPart = sourceFileUrl.Substring(sourceFileUrl.LastIndexOf("/JbFormUploadedDocs"), sourceFileUrl.Length - sourceFileUrl.LastIndexOf("/JbFormUploadedDocs"));

                        targetFileUrl = firstPart + middlePart + lastPart;

                        UploadSuplementalForm(sourceFileUrl, targetFileUrl);
                    }

                    element.SetValue(targetFileUrl);
                }

                resultElements.Add(section);
            }

            result = JsonConvert.SerializeObject(resultElements);

            return result;
        }

        private void UploadSuplementalForm(string sourceUrl, string targerUrl)
        {
            sourceUrl = sourceUrl.Substring(sourceUrl.LastIndexOf(Constants.Path_ClubFilesContainer + "/") + 6, sourceUrl.Length - (sourceUrl.LastIndexOf(Constants.Path_ClubFilesContainer + "/") + 6));
            targerUrl = targerUrl.Substring(targerUrl.LastIndexOf(Constants.Path_ClubFilesContainer + "/") + 6, targerUrl.Length - (targerUrl.LastIndexOf(Constants.Path_ClubFilesContainer + "/") + 6));

            _storageBusiness.ReadFile(sourceUrl, targerUrl, Constants.Path_ClubFilesContainer);
        }

        private Program GetReplicateProgram(Program program, Club club, Season newSeason, string seasonDomain)
        {
            var now = DateTime.UtcNow;

            var result = new Program()
            {
                ClubId = club.Id,
                CustomFieldsSerialized = program.CustomFieldsSerialized,
                IsCustomFieldChanged = program.IsCustomFieldChanged,
                IsWaitListAvailable = program.IsWaitListAvailable,
                LastCreatedPage = program.LastCreatedPage,
                MetaData = new MetaData { DateCreated = now, DateUpdated = now },
                Description = program.Description,
                Name = program.Name,
                Note = program.Note,
                OutSourceProgramStatus = program.OutSourceProgramStatus,
                Room = program.Room,
                Status = program.Status,
                TypeCategory = program.TypeCategory,
                WaitlistPolicy = program.WaitlistPolicy,
                ProgramSchedules = program.ProgramSchedules.Select(s => GetReplicateProgramSchedule(s, club)).ToList(),
                ClubFormTemplates = GetClubFormTemplatesForReplicate(club.ClubFormTemplates.ToList(), program),
                RegistrationMode = program.RegistrationMode,
                ClubWaivers = GetWaiversForReplicate(club.ClubWaivers.ToList(), program),
                Domain = _programBusiness.GenerateSubDomainName(club.Domain, seasonDomain, program.Name),
                Flyer = new Flyer { FileName = "New flyer", FlyerType = FlyerType.None },
                ClubLocation = club.ClubLocations.First(),
                ParentId = program.Id,
                Discounts = GetReplicateProgramDiscount(program, newSeason),
                Coupons = GetReplicateProgramCoupon(program, newSeason),
                PaymentPlans = GetReplicateProgramPaymentPlan(program, newSeason),
                ImageGallery = GetReplicateProgramImageGallery(program.ImageGallery, club),
                HasRegisterPIN = program.HasRegisterPIN,
                RegisterPINMessage = program.RegisterPINMessage,
                RegisterPIN = program.RegisterPIN,
                CustomFields = program.CustomFields != null ?
                                            new ProgramCustomFields()
                                            {
                                                CommisionRate = program.CustomFields.CommisionRate,
                                                LateCheckInPenalty = program.CustomFields.LateCheckInPenalty,
                                                Noshowpenalty = program.CustomFields.Noshowpenalty,
                                                SpecialRequests = program.CustomFields.SpecialRequests,
                                            }
                                            : null,
            };

            result.ScheduleParts = GetReplicateScheduleParts(program.ScheduleParts.ToList(), result);

            result.Categories.AddEntities(program.Categories.ToList());

            return result;
        }

        private ImageGallery GetReplicateProgramImageGallery(ImageGallery imageGallery, Club club)
        {

            if (imageGallery == null)
            {
                return null;
            }

            var now = DateTime.UtcNow;

            var result = new ImageGallery()
            {
                ClubDomain = club.Domain,
                MetaData = new MetaData { DateCreated = now, DateUpdated = now },
                Name = imageGallery.Name,
                Status = imageGallery.Status,
                Type = imageGallery.Type
            };

            var images = new List<ImageGalleryItem>();

            var sourceImageNames = imageGallery.Images.Select(i => i.Path).ToList();

            var imagesForUpload = new Dictionary<string, string>();

            foreach (var item in imageGallery.Images)
            {
                var path = Guid.NewGuid().ToString();

                var sourceImageUrl = GetProgramImageURL(item.Path, imageGallery.ClubDomain);
                var targetImageUrl = GetProgramImageURL(path, club.Domain);
                _storageBusiness.ReadFile(sourceImageUrl, targetImageUrl, Constants.Path_ClubFilesContainer);

                var sourceLargeImageUrl = GetProgramImageURL(string.Format("large-{0}", item.Path), imageGallery.ClubDomain);
                var targetLargImageUrl = GetProgramImageURL(string.Format("large-{0}", path), club.Domain);
                _storageBusiness.ReadFile(sourceLargeImageUrl, targetLargImageUrl, Constants.Path_ClubFilesContainer);

                var image = new ImageGalleryItem()
                {
                    Description = item.Description,
                    MetaData = new MetaData { DateCreated = now, DateUpdated = now },
                    Status = item.Status,
                    Title = item.Title,
                    Path = path
                };

                images.Add(image);
            }

            result.Images = images;

            return result;
        }

        private string GetProgramImageURL(string fileName, string clubDomain)
        {
            string imageUri = string.Format(@"{0}/{1}/{2}", clubDomain, Constants.Path_CatalogItems_Folder, fileName);

            if (string.IsNullOrEmpty(fileName))
            {
                imageUri = string.Empty;
            }

            return imageUri;
        }

        private List<PaymentPlan> GetReplicateProgramPaymentPlan(Program program, Season newSeason)
        {
            var programPaymentPlanNames = program.PaymentPlans.Where(p => !string.IsNullOrWhiteSpace(p.Name)).Select(d => d.Name.ToLower()).ToList();

            return newSeason.PaymentPlans.Where(d => !string.IsNullOrWhiteSpace(d.Name) && programPaymentPlanNames.Contains(d.Name.ToLower())).ToList();
        }

        private List<Coupon> GetReplicateProgramCoupon(Program program, Season newSeason)
        {
            var programCouponNames = program.Coupons.Select(d => d.Name.ToLower()).ToList();

            return newSeason.Coupons.Where(d => programCouponNames.Contains(d.Name.ToLower())).ToList();
        }

        private List<Discount> GetReplicateProgramDiscount(Program program, Season newSeason)
        {
            var programDiscountNames = program.Discounts.Select(d => d.Name.ToLower()).ToList();

            return newSeason.Discounts.Where(d => programDiscountNames.Contains(d.Name.ToLower())).ToList();
        }

        private List<ClubFormTemplate> GetClubFormTemplatesForReplicate(List<ClubFormTemplate> clubFormTemplates, Program sourceProgram)
        {
            var sourceFormTitles = sourceProgram.ClubFormTemplates.Select(s => s.Title.ToLower()).ToList();

            var result = clubFormTemplates.Where(c => !string.IsNullOrWhiteSpace(c.Title) && sourceFormTitles.Contains(c.Title.ToLower())).ToList();

            return result;
        }

        private List<ClubWaiver> GetWaiversForReplicate(List<ClubWaiver> clubWaivers, Program sourceProgram)
        {
            var sourceFormTitles = sourceProgram.ClubWaivers.Select(s => s.Name.ToLower()).ToList();

            var result = clubWaivers.Where(c => !string.IsNullOrWhiteSpace(c.Name) && sourceFormTitles.Contains(c.Name.ToLower())).ToList();

            return result;
        }

        private ProgramSchedule GetReplicateProgramSchedule(ProgramSchedule programSchedule, Club club)
        {
            var result = new ProgramSchedule()
            {
                AttendeeRestriction = new AttendeeRestriction() { Gender = programSchedule.AttendeeRestriction.Gender, MaxAge = programSchedule.AttendeeRestriction.MaxAge, MaxGrade = programSchedule.AttendeeRestriction.MaxGrade, MinAge = programSchedule.AttendeeRestriction.MinAge, MinGrade = programSchedule.AttendeeRestriction.MinGrade, RestrictionType = programSchedule.AttendeeRestriction.RestrictionType },
                AttributesSerialized = programSchedule.AttributesSerialized,
                EndDate = programSchedule.EndDate,
                IsDeleted = programSchedule.IsDeleted,
                RegistrationPeriod = new RegistrationPeriod { RegisterEndDate = programSchedule.RegistrationPeriod.RegisterEndDate, RegisterEndTime = programSchedule.RegistrationPeriod.RegisterEndTime, RegisterStartDate = programSchedule.RegistrationPeriod.RegisterStartDate, RegisterStartTime = programSchedule.RegistrationPeriod.RegisterStartTime },
                SectionsSerialized = programSchedule.SectionsSerialized,
                StartDate = programSchedule.StartDate,
                Title = programSchedule.Title,
                ScheduleMode = programSchedule.ScheduleMode,
                IsCampOvernight = programSchedule.IsCampOvernight,
                Charges = programSchedule.Charges.Select(c =>
                                                          new Charge
                                                          {
                                                              Amount = c.Amount,
                                                              AmountType = c.AmountType,
                                                              ApplyType = c.ApplyType,
                                                              AttributesSerialized = c.AttributesSerialized,
                                                              Capacity = c.Capacity,
                                                              Category = c.Category,
                                                              Description = c.Description,
                                                              IsDeleted = c.IsDeleted,
                                                              HasEarlyBird = c.HasEarlyBird,
                                                              MetaData = new MetaData() { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                                                              Name = c.Name,
                                                              ProgramSessions = c.ProgramSessions.Select(s => new ProgramSession
                                                              {
                                                                  EndDateTime = s.EndDateTime,
                                                                  IsDeleted = s.IsDeleted,
                                                                  StartDateTime = s.StartDateTime,
                                                                  Status = s.Status,
                                                                  WeeklyAgenda = s.WeeklyAgenda
                                                              }).ToList(),
                                                          })
                                                          .ToList(),
                ProgramSessions = programSchedule.ProgramSessions.Select(s =>
                                                 new ProgramSession()
                                                 {
                                                     EndDateTime = s.EndDateTime,
                                                     IsDeleted = s.IsDeleted,
                                                     StartDateTime = s.StartDateTime,
                                                     Status = s.Status,
                                                     WeeklyAgenda = s.WeeklyAgenda
                                                 })
                                                  .ToList(),
                Description = programSchedule.Description
            };

            return result;
        }

        public List<ProgramSchedulePart> GetReplicateScheduleParts(List<ProgramSchedulePart> scheduleParts, Program program)
        {
            if (scheduleParts == null)
            {
                return null;
            }

            return scheduleParts.Select(p =>
                                            new ProgramSchedulePart()
                                            {
                                                EndDate = p.EndDate,
                                                IsDeleted = p.IsDeleted,
                                                StartDate = p.StartDate,
                                                DueDate = p.DueDate,
                                                Charges = p.Charges.Select(c =>
                                                         new ProgramSchedulePartCharge
                                                         {
                                                             ChargeAmount = c.ChargeAmount,
                                                             Charge = program.ProgramSchedules
                                                                  .SelectMany(s => s.Charges)
                                                                  .FirstOrDefault(g => g.Amount == c.Charge.Amount && g.Name == c.Charge.Name && g.Category == c.Charge.Category)
                                                         }
                                                         )
                                                         .ToList()
                                            })
                                            .ToList();
        }

        bool IsSeasonNotExistInMember(Club club, Season season)
        {
            return !club.Seasons.Any(s => s.Status != SeasonStatus.Deleted && s.ParentId == season.Id);
        }

        #endregion

        public List<SelectListItem> GetKeyValueClubSeasons(int clubId,bool includeAllOption)
        {
            var result = GetList(clubId).Select(s => new SelectListItem { Text = s.Title, Value = s.Id.ToString() }).ToList();

            if (includeAllOption)
            {
                result.AddItemToFirst(DefaultDropdownItem.All);
            }

            return result;

        }

        public List<SelectKeyValue<long>> GetLiveSeasonsClub(int clubId, string exceptedSeasonDomains = null)
        {
            return GetList(clubId).Where(s => s.Status == SeasonStatus.Live && exceptedSeasonDomains != s.Domain)
                .Select(s => new SelectKeyValue<long>() {Text = s.Title, Value = s.Id})
                .OrderBy(s => s.Text)
                .ToList();
        }
    }
}
