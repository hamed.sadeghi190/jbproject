﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('waiverViewController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;
        $scope.MC.stateParams = $stateParams;

        $scope.Model = {};

        $http.get(window.location.origin + '/Dashboard/Order/GetWaiverView', { params: { waiverId: $stateParams.waiverId } }).success(function (data, status, headers, config) {
            $scope.Model = data;
            
           //for show signture
            var data = $("#" + "JbSignature480796241").text("").jSignature({
                lineWidth: 1,
                height: 120,
            }).jSignature("setData", $scope.Model.Signature, "base30").jSignature("getData");
            $("#Image_JbSignature480796241").attr("src", data);

        });

    }]);
})();

