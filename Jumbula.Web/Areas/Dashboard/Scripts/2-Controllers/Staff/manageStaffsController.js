﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('manageStaffsController', [
        '$scope', '$http', 'jbToast', 'jbConfirmation', 'cfpLoadingBar', function ($scope, $http, jbToast, jbConfirmation, cfpLoadingBar) {
            window.app.globalObjects.pageClass = '';
            window.app.globalObjects.initializeView();

            $scope.Model = {};
            $http.get('/Dashboard/Staff/GetSettings').success(function (data, status, headers, config) {

                $scope.Model = data;

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });


            var dataInfo = {};
            $http.get('Dashboard/CustomReport/GetStaffListReportHeader').success(function (data, status, headers, config) {

                dataInfo = data;

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });

            $scope.roleDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport: {
                    read:
                        {
                            url: window.location.origin + '/Dashboard/Staff/GetStaffs/',
                            contentType: "application/json; charset=utf-8", // tells the web method to serialize JSON
                            type: "POST" //use HTTP POST or get
                        },
                    parameterMap: function (data, operation) {
                        if (operation == "read") {
                            data = $.extend(
                                {
                                    sort: null,
                                    filter: null
                                }, data);

                            return JSON.stringify(data);
                        }
                    }
                },
                schema:
                    {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                pageSize: 100,
                batch: true,
                serverFiltering: true,
                serverPaging: true,
                serverSorting: true,
                requestStart: function (e) {
                    kendo.ui.progress($('[kendo-grid]'), true);
                },
                requestEnd: function (e) {
                    kendo.ui.progress($('[kendo-grid]'), false);
                }
            });

            $scope.roleGridOptions = {
                dataSource: $scope.roleDataSource,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                columns: [
                    {
                        field: "FullName",
                        title: "Name",
                    },
                    {
                        field: "Title",
                        title: "Title",
                        width: "180px"
                    },
                    {
                        field: "Email",
                        title: "Email",
                    }, {
                        field: "Role",
                        title: "Access role",
                        width: "160px"
                    }, {
                        field: "Status",
                        title: "Status",
                        width: "100px"
                    },
                    {
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                    <li ngaction='#=Manageable#' ui-sref='EditStaff({staffId: \"#=Id#\"})'>\
                                          <span class='k-link jbi-pencil'>Edit</span>\
                                    </li>\
									#if(!IsFrozen){#\
										<li ngaction='#=Manageable#' ng-click='freeze(#= Id #)''>\
											  <span class='k-link jbi-ice'>Freeze</span>\
										</li>\
									 #}#\
									 #if(IsFrozen){#\
                                        <li ngaction='#=Manageable#' ng-click='unfreeze(#= Id #)''>\
                                           <span class='k-link jbi-ice'>Unfreeze</span>\
                                         </li>\
                                      #}#\
									   #if(Status == 'Pending'){#\
                                        <li ngaction='#=Manageable#'  ng-click='resendInvitation(#= Id #)''>\
                                           <span  class='k-link jbi-email'>Resend invitation</span>\
                                         </li>\
                                      #}#\
                                     #if(Status == 'Pending'){#\
                                        <li ngaction='#=Manageable#'  ng-click='ManualActivation(#= Id #)''>\
                                           <span  class='k-link jbi-check'>Activate manually</span>\
                                         </li>\
                                      #}#\
                                    <li ngaction='#=Manageable#' ng-click='delete(#= Id #)'>\
                                          <span  class='k-link jbi-remove'>Delete</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>"
                    }
                ],

                excel: {
                    allPages: true,
                    fileName: $scope.MC.account.name + " (ManageStaff).xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Manage staff - " + $scope.MC.account.name
                        }]
                    });

                    cfpLoadingBar.complete();
                },
            };

            $scope.delete = function (id) {
                jbConfirmation
                    .yes(function () {
                        $http.post('Dashboard/Staff/Delete', { id: id })
                            .success(function (data) {

                                if (data.Status) {

                                    jbToast.success(data.Message);

                                    $scope.roleGridOptions.dataSource.read();
                                } else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });
                    })
                    .confirm();
            }

            $scope.freeze = function (id) {
                jbConfirmation
                    .yes(function () {
                        $http.post('Dashboard/Staff/Freeze', { id: id })
                            .success(function (data) {

                                if (data.Status) {

                                    jbToast.success(data.Message);

                                    $scope.roleGridOptions.dataSource.read();
                                }
                                else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });
                    })
                    .confirm();
            }


            $scope.unfreeze = function (id) {
                jbConfirmation
                    .yes(function () {
                        $http.post('Dashboard/Staff/Unfreeze', { id: id })
                            .success(function (data) {

                                if (data.Status) {

                                    jbToast.success(data.Message);

                                    $scope.roleGridOptions.dataSource.read();
                                } else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });
                    })
                    .confirm();
            }

            $scope.resendInvitation = function (id) {
                jbConfirmation
                    .yes(function () {
                        $http.post('Dashboard/Staff/ResendInvitation', { id: id })
                            .success(function (data) {

                                if (data.Status) {

                                    jbToast.success(data.Message);

                                    $scope.roleGridOptions.dataSource.read();
                                } else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });
                    })
                    .confirm();
            }

            $scope.ManualActivation = function (id) {
                jbConfirmation
                    .yes(function () {
                        $http.post('Dashboard/Staff/ManualActivation', { id: id })
                            .success(function (data) {

                                if (data.Status) {

                                    jbToast.success(data.Message);

                                    $scope.roleGridOptions.dataSource.read();
                                }
                                else {
                                    $scope.MC.handleErrors($scope, data);
                                }
                            });
                    })
                    .title('Manual activation')
                    .message('Are you sure you want to manually activate this staff member? Note: If this person does not already have an account, the Forgot Password feature can be used to recover the password.')
                    .confirm();
            }

            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                $("#roleGrid").getKendoGrid().saveAsExcel();

            }
        }
    ]);

})();