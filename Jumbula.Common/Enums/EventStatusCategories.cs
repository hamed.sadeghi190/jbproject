﻿

namespace Jumbula.Common.Enums
{
    public enum EventStatusCategories
    {
        open = 0,
        frozen = 1,
        deleted = 2,

        Invisiable = 3,
        //ComingSoon = 4,
    }

}
