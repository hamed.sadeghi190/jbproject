﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class FillterCampViewModel
    {
        public long CampId { get; set; }
        public string Name { get; set; }

        public List<DayOfWeek> NameDay { get; set; }
        public long SeasonId { get; set; }
    }
}