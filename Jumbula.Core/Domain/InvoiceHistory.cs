﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class InvoiceHistory : BaseEntity
    {
        public int? InvoiceId { get; set; }
        [ForeignKey("InvoiceId")]
        public virtual Invoice invoice { get; set; }

        public DateTime ActionDate { get; set; }

        public Invoicestatus Action { get; set; }

        public string Description { get; set; }
    }

    public enum Invoicestatus : byte
    {
        [Description("Canceled")]
        Canceled = 0,
        [Description("Initiated")]
        Initiated = 1,
        [Description("Paid")]
        Paid = 2,
        [Description("Delete")]
        Deleted = 3,

    }

}
