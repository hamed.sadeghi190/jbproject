﻿namespace Jumbula.Common.Base
{
    public interface IBaseViewModel<TPrimaryKey>
    {
        TPrimaryKey Id { get; set; }
    }
}
