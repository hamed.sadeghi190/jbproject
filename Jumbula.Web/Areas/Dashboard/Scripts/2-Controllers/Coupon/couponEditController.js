﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('couponEditController', ['$scope', '$http', '$log', '$stateParams', '$state', 'couponAddEditService', function ($scope, $http, $log, $stateParams, $state, couponAddEditService) {
        window.app.globalObjects.pageClass = 'seasons-view step1';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });

        couponAddEditService($scope, $http, $stateParams);

        $scope.pageTitle = function () {
            if ($scope.MC.stateParams.couponId) {
                return "Edit Coupon";
            } else {
                return "Add New Coupon";
            }
    }        
}]);
})();