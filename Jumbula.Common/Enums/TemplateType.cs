﻿

namespace Jumbula.Common.Enums
{
    public enum TemplateType : int
    {
        Email = 0,
        Registration = 1
    }

}
