﻿using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ParentInvoiceOrderViewModel : BaseViewModel<OrderItem, long>
    {
     
        public DateTime OrderDate { get; set; }
        public string StrOrderDate
        {
            get
            {
                return OrderDate.ToString("MMM dd, yyyy");
            }

        }
        public string ConfirmationId { get; set; }
        public decimal Balance { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal OrderAmount { get; set; }
    }
}