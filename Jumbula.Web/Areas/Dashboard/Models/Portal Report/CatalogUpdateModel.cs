﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CatalogUpdateViewModel
    {
        public string ClubName { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string UpdateStr { get; set; }

        public int ClubId { get; set; }
    }

    public class CatalogUpdateItemsViewModel
    {
        public string CatalogItem { get; set; }

        public DateTime UpdateDate { get; set; }

        public string UpdateDateStr { get; set; }
    }
}