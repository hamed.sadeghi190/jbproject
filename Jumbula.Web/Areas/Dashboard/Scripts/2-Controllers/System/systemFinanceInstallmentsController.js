﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemFinanceInstallmentsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $http.get($scope.baseUrl + '/GetSystemInstallments').success(function (data, status, headers, config) {
            $scope.Model = data;
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });
        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetSystemInstallments',
                    type: "POST",
                    data: function () {
                        return {
                            end: $scope.Model.UpdateTo,
                            start: $scope.Model.UpdateFrom
                        };
                    },
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            term: $scope.searchTerm,
                            skip: data.skip,
                            take: data.take,
                            page: data.page,
                            pageSize: data.pageSize,
                            sort: data.sort
                        }
                    }
                }
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "StrInstallmentDate",
                    title: "Installment Date",
                    width: "200px"

                },
                {
                    field: "ClubName",
                    title: "Club",
                    width: "200px"
                },

                {
                    field: "StrTransactionDate",
                    title: "Transaction Date",
                    width: "200px"
                },
                {
                    field: "StrAmount",
                    title: "Amount",
                    width: "200px"
                },

                 {
                     field: "StrStatus",
                     title: "Status",
                     width: "200px"
                 },
                  {
                      field: "ResponseMessage",
                      title: "Response Message",
                      width: "200px"
                  },
            ],
        }

        $scope.filterData = function () {
            $scope.mainGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Model.UpdateFrom = "";
            $scope.Model.UpdateTo = "";
            $scope.mainGridOptions.dataSource.read();
        }
    }
    ]);

})()
