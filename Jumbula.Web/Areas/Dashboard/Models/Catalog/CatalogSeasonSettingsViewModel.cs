﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Utilities;
using Newtonsoft.Json;

namespace Jumbula.Web.Areas.Dashboard.Models.Catalog
{
    public class CatalogSeasonSettingsViewModel
    {
        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "Start time")]
        public DateTime? StartTime { get; set; }

        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "End time")]
        public DateTime? EndTime { get; set; }
    }
}