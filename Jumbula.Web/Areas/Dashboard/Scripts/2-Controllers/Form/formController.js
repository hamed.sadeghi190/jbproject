﻿(function () {
    'use strict';
    angular.module('dashboardApp').controller('formController', ['$scope', '$http', 'cfpLoadingBar', '$stateParams', '$state', 'jbToast', 'jbRandom', 'programService', 'formService' , function ($scope, $http, cfpLoadingBar, $stateParams, $state, jbToast, jbRandom, programService, formService) {
        window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();

        $scope.initial = function () {
            $('[data-role=accordion]').accordion();
            $('[data-role=accordion] > .accordion-frame > .content').unbind("mousedown.sortable").bind("mousedown.sortable", function () {
                $(this).addClass("active");
            }).unbind("mouseup.sortable").bind("mouseup.sortable", function () {
                $(this).removeClass("active");
            });
        };

        
        $scope.MC.stateParams = $stateParams;

        $scope.formType = $stateParams.formType;

        var baseUrl = window.location.origin;

        var sortableX, sortableY;
        $scope.SortableOptions = {
            Section: {
                start: function (e, ui) {
                    $scope.RefreshKendoEditor();
                    angular.element(".JbSection.ui-sortable-placeholder").height(ui.item.height());
                    if (ui.item.find(".content.active").length > 0) {
                        ui.item.sortable.cancel();
                    }
                    ui.item.find(".heading").addClass("grabbing");

                    sortableX = e.pageX;
                    sortableY = e.pageY;
                    
                },
                stop: function (e, ui) {
                    ui.item.find(".heading").removeClass("grabbing");

                    if (is.within(sortableX, e.pageX - 3, e.pageX + 3) && is.within(sortableY, e.pageY - 3, e.pageY + 3)) {
                        $(e.toElement).trigger("click");
                    }
                    sortableX = null;
                    sortableY = null;
                    $scope.RefreshKendoEditor();
            },
                
            },
            Element: {
                start: function (e, ui) {
                    $scope.RefreshKendoEditor();
                    angular.element(".JbElement.ui-sortable-placeholder").height(ui.item.height());
                    ui.item.find(".heading").addClass("grabbing");

                    sortableX = e.pageX;
                    sortableY = e.pageY;
                   
                },
                stop: function (e, ui) {
                    ui.item.find(".heading").removeClass("grabbing");

                    if (is.within(sortableX, e.pageX - 3, e.pageX + 3) && is.within(sortableY, e.pageY - 3, e.pageY + 3)) {
                        if ($(e.toElement).find(":checkbox").size() > 0) {
                            $(e.toElement).find(":checkbox").each(function () {
                                $(this).val($(this).val() != "true").trigger("input");
                            });
                        } else if ($(e.toElement).find(":radio").size() > 0) {
                            $("[name='" + $(e.toElement).find(":radio").attr('name') + "']").not(e.toElement).prop("checked", "").trigger("input");
                            $(e.toElement).find(":radio").prop("checked", "checked").trigger("input");
                        } else {
                            $(e.toElement).click();
                        }
                    }
                    sortableX = null;
                    sortableY = null;
                    
                    $scope.RefreshKendoEditor();
                }
            },
            DropdownOption: {
                start: function (e, ui) {
                    angular.element(".drop-down-option.ui-sortable-placeholder").height(ui.item.height());
                    ui.item.children(0).addClass("grabbing");

                    sortableX = e.pageX;
                    sortableY = e.pageY;
                },
                stop: function (e, ui) {
                    ui.item.children(0).removeClass("grabbing");

                    if (is.within(sortableX, e.pageX - 3, e.pageX + 3) && is.within(sortableY, e.pageY - 3, e.pageY + 3)) {
                        $(e.toElement).click();
                    }
                    sortableX = null;
                    sortableY = null;
                }
            }
        }

        var requiredByJbElementNames = {
            ParticipantSection: {
                requiredSection: true,
                children: [
                "FirstName",
                "LastName"
               
                ]
            },
            ContactSection: {
                requiredSection: false,
                children: [
                "Address",
                "Phone",
                "Cell"
                ]
            },
            ParentGuardianSection: {
                requiredSection: false,
                children: [
                    "FirstName",
                    "LastName",
                ]
            },
            Parent2GuardianSection: {
                requiredSection: false,
                children: [
                    "FirstName",
                    "LastName",
                ]
            },
            EmergencyContactSection: {
                requiredSection: false,
                children: [
                    "FirstName",
                    "LastName",
                    "EmailAddress"
                ]
            },
            SchoolSection: {
                requiredSection: false,
                children: [
                ]
            },
            InsuranceSection: {
                requiredSection: false,
                children: [
                ]
            },
            HealthSection: {
                requiredSection: false,
                children: [
                ]
            },
            AuthorizedPickup1: {
                requiredSection: false,
                children: [
                    "FirstName",
                    "LastName",
                ]
            },
            AuthorizedPickup2: {
            requiredSection: false,
            children: [
                "FirstName",
                "LastName",
             ]
            },
            AuthorizedPickup3: {
                requiredSection: false,
                children: [
                    "FirstName",
                    "LastName",
                ]
            },
            AuthorizedPickup4: {
                requiredSection: false,
                children: [
                    "FirstName",
                    "LastName",
                ]
            },
       
        }
        
        $scope.RefreshKendoEditor = function () {
            var editor = $(".kendo-text-editor").data("kendoEditor");
            //editor.wrapper.appendTo("#container");
            if(editor != undefined) editor.refresh();
        }
        $scope.getUrl = function () {
            $http.get("KendoEditorFiles/FileUrl").success(function (data, status, headers, config) {

                $scope.editorOptions = {
                    tools: [
                        "bold",
                        "italic",
                        "underline",
                        "strikethrough",
                        "justifyLeft",
                        "justifyCenter",
                        "justifyRight",
                        "justifyFull",
                        "insertUnorderedList",
                        "insertOrderedList",
                        "indent",
                        "outdent",
                        "createLink",
                        "unlink",
                        "insertImage",
                        //"insertFile",
                        "subscript",
                        "superscript",
                        "createTable",
                        "addRowAbove",
                        "addRowBelow",
                        "addColumnLeft",
                        "addColumnRight",
                        "deleteRow",
                        "deleteColumn",
                        "formatting",
                        "cleanFormatting",
                        "fontName",
                        "fontSize",
                        "foreColor",
                        "backColor",
                        //"viewHtml",
                        //"print"
                    ],

                    imageBrowser: {
                        messages: {
                            dropFilesHere: "Drop files here"
                        },

                        transport: {
                            type: "imagebrowser-aspnetmvc",
                            read: "/KendoEditorFiles/Read",
                            uploadUrl: "/KendoEditorFiles/Upload",
                            imageUrl: data + "{0}",
                        }
                    }
                }

            });
        }


        $scope.JbElement = {}

        $scope.JbElement.Base = function () {
            return {
                "ElementId": "JbBaseElement" + jbRandom.Generate(9, true, false, false, false, false),
                "Name": null,
                "Title": "",
                "Value": "",
                "Type": null,
                "HelpText": null,
                "Width": null,
                "IsCustomElement": false,
                "Size": 0,
                "Items": [
                    {
                        Text: "Option 1",
                        Value: "Option1"
                    }
                ],
                "VisibleMode": "Both",
                "CurrentMode": "Design",
                "AccessRole": {},
                "Validations": []
            }
        }

        $scope.JbElement.GetBaseByType = function (type) {
            for (var i = 0; i < $scope.JbElement.DefaultTemplates.length; i++) {
                if (type == $scope.JbElement.DefaultTemplates[i].Type) {
                    return angular.copy($scope.JbElement.DefaultTemplates[i]);
                }
            }
        }

        $scope.JbElement.DefaultTemplates = [];

        $http.get("FormBuilder/JbElamentTypes").success(function (data, status, headers, config) {
            $scope.JbElement.DefaultTemplates = data;
        });

        $http.get("FormBuilder/GetFormConstants").success(function (data, status, headers, config) {
            $scope.JbSchoolGrade.Operate.Grades = data.Grades;
            $scope.JbSchoolGender.Operate.Genders = data.Genders;
            $scope.JbParentsRelationships.Operate.Relationships = data.ParentRelationships;
            $scope.JbRelationships.Operate.Relationships = data.Relationships;
            $scope.JbHearAboutUs.Operate.HearAboutUs = data.HearAboutUs;
            $scope.JbSelfAdministerMedication.Operate.SelfAdministerMedication = data.SelfAdministeredMedication;
            $scope.JbNutAllergy.Operate.NutAllergy = data.NutAllergy;
            $scope.JbPermissionPhotography.Operate.PermissionPhotography = data.PermissionPhotography;

        });

        $scope.JbElement.Active = {};
        $scope.JbElement.Operate = {
            Title: function (element) {
                try {
                    element.Title = element.Title.replace(/'/g, "’").replace(/[^a-zA-Z0-9\!\@\#\$\:\;\"\-\%\^\&\*\|\(\)\/\>\<\_\\\.\=,?`\+\~\n\ ’]/g, "");
                } catch (e) { }
                if (element.CurrentMode == "Design") {
                    element.Name = element.Title + "_" + jbRandom.Generate(6, false, true, true, true, false);
                }
            },
            Value: function (element) {
                element.Value = element.Value.replace(/[^a-zA-Z0-9\!\@\#\$\%\^\&\:\;\"\-\*\|\(\)\/\>\<\_\\\.\=\+\~\n\ ]/g, "");
            },
            IsSectionTitleEditing: function (section) {
                return is.existy(section._titleIsEditing) && section._titleIsEditing;
            },
            EditSectionTitle: function (section) {
                section._titleIsEditing = true;
                section._Title = section.Title;
                setTimeout(function () {
                    $("#sectionTitle" + section.Name).focus().unbind("keyup.edit").bind("keyup.edit", function (e) {
                        if (e.which == 13) {
                            $scope.JbElement.Operate.SaveEditSectionTitle(section);
                            $scope.$apply();
                        } else if (e.which == 27) {
                            $scope.JbElement.Operate.CancelEditSectionTitle(section);
                            $scope.$apply();
                        }
                    });
                });
            },
            CancelEditSectionTitle: function (section) {
                section._titleIsEditing = false;
                section.Title = section._Title;
            },
            SaveEditSectionTitle: function (section) {
                if (is.not.empty(section.Title)) {
                    section._Title = '';
                } else {
                    section.Title = section._Title;
                }
                section._titleIsEditing = false;
            },
            ToggleHiddenReadOnlyModes: function (element) {
                if (element.CurrentMode == "ReadOnly") {
                    element.CurrentMode = "Hidden";
                    return true;
                } else if (element.CurrentMode == "Hidden") {
                    element.CurrentMode = "ReadOnly";
                    return true;
                } else {
                    return false;
                }
            },
            CanShowHide: function (section, element) {
                if (is.not.propertyDefined(requiredByJbElementNames, section.Name) || (section && section.CurrentMode == "Design") || (element && element.CurrentMode == "Design")) {
                    return true;
                } else {
                    return ((section && is.propertyDefined(requiredByJbElementNames, section.Name) && !element) ? requiredByJbElementNames[section.Name].requiredSection == false : is.not.inArray(element.Name, requiredByJbElementNames[section.Name].children));
                }
            },
            IsRequiredByJB: function (section, element) {
                if (is.propertyDefined(requiredByJbElementNames, section.Name)) {
                    if (element) {
                    
                        return is.inArray(element.Name, requiredByJbElementNames[section.Name].children);

                    } else {
                        return requiredByJbElementNames[section.Name].requiredSection;
                    }
                } else {
                    return false;
                }
            },
            HasNoElement: function (section) {
                return section.Elements.length == 0;
            },
            GetBase: function (type) {
                var result = {};
                angular.forEach($scope.DefaultElementsTemplate, function (value, key) {
                    if (value.Type == type) {
                        result = value;
                    }
                });
                return result;
            },
            GetTypeTitle: function (type) {
                var typeTitles = {
                    "JbSection": "Section",
                    "JbTextBox": "Text",
                    "JbTextArea": "Paragraph text",
                    "JbDropDown": "Choose from a list",
                    "JbRadioList": "Select from a list",
                    "JbCheckBox": "Check box",
                    "JbEmail": "Email",
                    "JbNumber": "Number",
                    "JbPhone": "Phone number",
                    "JbAddress": "Address",
                    "USCF": "USCF info",
                    "JbParagraph": "Text display",
                    "JbTextEditor": "Text Editor",
                }
                return typeTitles[type];
            },
            Add: {
                Section: function () {
                    $scope.Model.JbForm.Elements.push({
                        "Type": "JbSection",
                        "IsCustomElement": false,
                        "Elements": [],
                        "ElementId": "JbSection" + jbRandom.Generate(9, true, false, false, false, false),
                        "TypeTitle": "Section",
                        "Name": "CustomSection_" + jbRandom.Generate(6, true, true, true, true, false),
                        "Title": "Custom section",
                        "HelpText": null,
                        "Width": null,
                        "Size": 0,
                        "VisibleMode": "Both",
                        "CurrentMode": "Design",
                        "AccessRole": {},
                        "Validations": []
                    });
                },
                Element: {
                    ShowTypes: function (section) {
                        angular.forEach($scope.Model.JbForm.Elements, function (section, skey) {
                            angular.forEach(section.Elements, function (element, ekey) {
                                if (element._IsNewElement) {
                                    section.Elements.removeByIndex(ekey);
                                }
                            });
                        });
                        var newElement = $scope.JbElement.Base();
                        newElement._IsNewElement = true;
                        newElement._IsInAddProcess = true;
                        section.Elements.push(newElement);
                        $scope.JbElement.Operate.Edit.SetAsActiveElement(section, newElement);
                    },
                    SelectType: function (element, type) {
                        element.Type = type;
                        angular.extend(element, $scope.JbElement.GetBaseByType(type));
                        element._IsNewElement = false;
                        element.ElementId = type + jbRandom.Generate(9, true, false, false, false, true);
                        element.Name = element.Type + "_" + jbRandom.Generate(6, false, true, true, true, false);
                       
                        element.Width = type == "JbTextArea"  ? "Full" : "Third";

                    },
                    CanAddNew: function () {
                        return is.empty($scope.JbElement.Active);
                    }
                }
            },
            Edit: {
                SetAsActiveElement: function (section, element) {
                    $scope.JbElement.Active.Element = element;
                    $scope.JbElement.Active._Element = angular.copy(element);
                    $scope.JbElement.Active.Section = section;
                    //element.Width = type == "JbTextArea" || type == "JbSignature" ? "Full" : "Third";

                    //console.log(element);
                    //console.log(element.Width);
                },
                SaveActiveElement: function (element, activeForm) {
                    if (activeForm.$invalid) {
                        if (activeForm._submited) {
                            jbToast.error("Please check your inputs again, something is wrong with the inputs.");
                        }
                        activeForm._submited = true;
                        setTimeout(function () {
                            $("[validation-error-tooltip]").tooltip("show");
                        }, 20);
                    }
                    else {
                        //if (element.Type == 'JbTextEditor') {
                        //    alert(element.Value);
                        //    element.Value = $sce.trustAsHtml(element.Value)
                        //    //element.Value = decodeHtml(element.Value);
                        //    alert(element.Value); 
                        //    }
                        activeForm._submited = false;
                        $scope.JbElement.Active = {};
                        if (element._IsInAddProcess) {
                            element._IsInAddProcess = false;
                        }
                    }
                },
                CancelActiveElement: function (section, elementIndex) {
                    section.Elements[elementIndex] = $scope.JbElement.Active._Element;
                    $scope.JbElement.Active = {};
                    if (section.Elements[elementIndex]._IsInAddProcess) {
                        $scope.JbElement.Operate.Delete.Element(section, elementIndex);
                    }
                },
                IsActiveElement: function (element) {
                    return $scope.JbElement.Active.Element == element;
                },
                CanEdit: function (section, element) {
                    return is.empty($scope.JbElement.Active);
                },
            },
            Delete: {
                Section: function (sectionIndex) {
                    $scope.Model.JbForm.Elements.removeByIndex(sectionIndex);
                },
                Element: function (section, elementIndex) {
                    section.Elements.removeByIndex(elementIndex);
                }
            },
            IsRequired: function (element) {
                var hasRequiredValidation;
                if (element.IsRequired) {
                    hasRequiredValidation = false;
                    angular.forEach(element.Validations, function (validation, key) {
                        if (validation.Type == "Required") {
                            hasRequiredValidation = true;
                        }
                    });
                    if (!hasRequiredValidation) {
                        element.Validations.push({
                            "Type": "Required",
                            "Message": element.Title + " is required",
                            "IsChecked": true
                        });
                    }
                } else {
                    hasRequiredValidation = false;
                    var validationIndex = -1;
                    angular.forEach(element.Validations, function (validation, key) {
                        if (validation.Type == "Required") {
                            hasRequiredValidation = true;
                            validationIndex = key;
                        }
                    });
                    if (hasRequiredValidation) {
                        element.Validations.removeByIndex(validationIndex);
                    }
                }
            }
        }

        $scope.JbForm = {};

        $scope.JbForm.Default = {};

        $scope.JbForm.EditMode = false;

        $scope.JbForm.goBack = function () {

            if ($scope.JbForm.ConfirmMode) {
                $scope.JbForm.ConfirmMode = false;
            }
            else {
                window.history.back();
            }
        }

        $scope.JbForm.Save = function () {

            if ($scope.formType.toLowerCase() == "donation") {
                $http.post('/FormBuilder/Edit', { formJson: JSON.stringify($scope.Model.JbForm) })
                             .success(function (data, status, headers, config) {
                                 if (data.Status == true) {
                                     $state.go("CheckOutSetting");
                                 }
                                 else {
                                     $scope.MC.handleErrors($scope, data);
                                 }
                             });
            }
            else {
                if (!$scope.JbForm.EditMode) {

                    $scope.Model.FormTemplate = $stateParams.templateId;

                    formService.setModel($scope.Model);

                    $state.back();

                } else {

                    var formElements = $scope.Model.JbForm.Elements;
                    // si => Section Index
                    for (var si = 0; si < formElements.length; si++) {
                        if (formElements[si].CurrentMode == "Hidden") {
                            formElements.removeByIndex(si--);
                        } else {
                            // ei => Element Index
                            for (var ei = 0; ei < formElements[si].Elements.length; ei++) {
                                if (formElements[si].Elements[ei].CurrentMode == "Hidden") {
                                    formElements[si].Elements.removeByIndex(ei--);
                                }
                            }
                        }
                    }

                    if (!$scope.JbForm.ConfirmMode) {
                        $scope.JbForm.ConfirmMode = true;
                    } else {

                        if ($stateParams.templateId == 0) {
                            $scope.JbForm.SaveAsMode = true;
                        }

                        if ($scope.JbForm.SaveAsMode === true || $scope.JbForm.SaveAsMode === 'true') {
                            // Save club form template
                            $http.post('/FormBuilder/Create', { formJson: JSON.stringify($scope.Model.JbForm) })
                              .success(function (data, status, headers, config) {
                                  if (data.Status == 'Success') {
                                      $http.post('/FormBuilder/AddTemplate', { jbFormId: data.FormId, templateName: $scope.Model.NewTemplateName, formType: $stateParams.formType, defaultFormType: $scope.Model.DefaultFormType })
                                      .success(function (data2, status, headers, config) {
                                          if (data2.Status == true) {

                                              $scope.Model.FormTemplate = data2.templateId;

                                              $scope.Model.FormTemplates.push({ Text: $scope.Model.NewTemplateName, Value: data2.templateId });

                                              formService.setModel($scope.Model);

                                              $state.back();
                                          }
                                          else {
                                              $scope.MC.handleErrors($scope, data2);
                                          }

                                      });
                                  }
                              });
                        }
                        else {
                            /* SAVE FORM */
                            $http.post('/FormBuilder/Edit', { formJson: JSON.stringify($scope.Model.JbForm) })
                               .success(function (data, status, headers, config) {
                                   if (data.Status == true) {

                                       if ($scope.MC.stateParams.formType == "Registration") {
                                           var _programModel = programService.getModel();

                                           _programModel.FormTemplate = $stateParams.templateId;

                                           programService.setModel(_programModel);
                                       }

                                       $state.back();
                                   }
                                   else {
                                       $scope.MC.handleErrors($scope, data);
                                   }
                               });
                        }
                    }
                }
            }
        };

        $scope.JbForm.Load = function () {
            if ($stateParams.formType.toLowerCase() == "donation") {

                $http.get("Dashboard/Club/GetForm?formId=" + $stateParams.templateId)
                    .success(function (data, status, headers, config) {
                        $scope.Model = data;
                        $scope.JbForm.EditMode = true;
                    });
            }
            else
                $http.get("Dashboard/Club/GetJbForm", { params: { templateId: $stateParams.templateId, formType: $stateParams.formType } }).success(function (data, status, headers, config) {
                    $scope.Model = data;
                    if ($stateParams.templateId == '0') {
                        $scope.JbForm.EditMode = true;
                    }

                    formService.setModel(data);
                    if ($scope.MC.stateParams.formType == "Registration") {
                        $http.get("FormBuilder/GetDefaultJbForm", { params: { defaultFormType: $scope.Model.DefaultFormType } }).success(function (data, status, headers, config) {
                            $scope.JbForm.Default = data;
                            var defaultForm = $scope.JbForm.Default;
                            var model = $scope.Model.JbForm;
                            model.CurrentMode = "Design";
                            // dsi => DefaultForm Section Index
                            for (var dsi = 0; dsi < defaultForm.Elements.length; dsi++) {
                                var modelHasThisSection = false;
                                // msi => Model Section Index
                                for (var msi = 0; msi < model.Elements.length; msi++) {
                                    if (defaultForm.Elements[dsi].Name == model.Elements[msi].Name) {
                                        modelHasThisSection = true;
                                        model.Elements[msi].CurrentMode = defaultForm.Elements[dsi].CurrentMode;
                                        // dei => DefaultForm Element Index
                                        for (var dei = 0; dei < defaultForm.Elements[dsi].Elements.length; dei++) {
                                            var modelHasThisElement = false;
                                            // mei => Model Element Index
                                            for (var mei = 0; mei < model.Elements[msi].Elements.length; mei++) {
                                                if (defaultForm.Elements[dsi].Elements[dei].Name == model.Elements[msi].Elements[mei].Name) {
                                                    modelHasThisElement = true;
                                                    model.Elements[msi].Elements[mei].CurrentMode = defaultForm.Elements[dsi].Elements[dei].CurrentMode;
                                                    break;
                                                }
                                            }
                                            if (!modelHasThisElement) {
                                                var newElement = angular.copy(defaultForm.Elements[dsi].Elements[dei]);
                                                newElement.CurrentMode = "Hidden";
                                                model.Elements[msi].Elements.push(newElement);
                                            }
                                        }
                                    }
                                }
                                if (!modelHasThisSection) {
                                    var newSection = angular.copy(defaultForm.Elements[dsi]);
                                    newSection.CurrentMode = "Hidden";
                                    model.Elements.push(newSection);
                                }
                            }
                        });
                    }
                    $scope.getUrl();
                });
        };

        $scope.JbForm.ChangeTemplate = function (templateId) {
            $state.go('Form', { formType: $stateParams.formType, templateId: templateId });
        };

        $scope.JbForm.EnableEditing = function () {
            $scope.JbForm.EditMode = true;
        };

        $scope.JbForm.GetTemplateName = function (templateId) {
            if ($stateParams.formType.toLowerCase() == "donation") {
                return "Donation";
            }
            if ($scope.Model && $scope.Model.FormTemplates) {
                for (var i = 0; i < $scope.Model.FormTemplates.length; i++) {
                    if ($scope.Model.FormTemplates[i].Value == templateId)
                        return $scope.Model.FormTemplates[i].Text;
                };
            }
            return null;
        };

        $scope.JbNumber = {
            Operate: {
                Value: function (element) {
                    element.Value = element.Value.replace(/[^0-9\.]/g, "");
                }
            },
            Validate: {
                Value: function (value) {
                    return value / 1 == value;
                }
            }
        }

        $scope.JbEmail = {
            Operate: {
                Value: function (element) {
                    element.Value = element.Value.toLowerCase().replace(/[^a-z0-9\.\-\_\@]/g, "");
                }
            },
            Validate: {
                Value: function (value) {
                    return is.email(value) || is.empty(value);
                }
            }
        }

        $scope.JbDropDown = {
            Operate: {
                SetOptionValue: function (items, item) {
                    if (is.not.empty(item.Text)) {
                        item.Text = item.Textreplace(/'/g, "’").replace(/[^a-zA-Z0-9\!\@\#\$\:\;\"\-\%\^\&\*\|\(\)\/\>\<\_\\\.\=,?`\+\~\n\ ’]/g, "");
                        item.Value = item.Text.replace(/[^a-zA-Z0-9 ]/g, "");
                    }
                    var sameValues = [];
                    angular.forEach(items, function (value, key) {
                        if (value.Value == item.Value) {
                            sameValues.push(key);
                        }
                    });
                    if (sameValues.length > 1) {
                        sameValues.removeByIndex(0);
                        angular.forEach(sameValues, function (value, key) {
                            items[value].Value = items[value].Value + jbRandom.Generate(9, true, false, false, false, true);
                        });
                    }
                },
                AddOption: function (element) {
                    if (is.null(element.Items) || is.undefined(element.Items)) {
                        element.Items = [];
                    }
                    element.Items.push({
                        Text: "Option " + (element.Items.length + 1),
                        Value: "Option" + jbRandom.Generate(9, true, false, false, false, true)
                    });
                    setTimeout(function () {
                        $("#optionInput" + element.ElementId + (element.Items.length - 1)).select();
                    }, 1);
                },
                RemoveOption: function (element, index) {
                    element.Items.removeByIndex(index);
                }
            }
        }

        $scope.JbSchoolGrade = {
            Operate: {
                Grades: [],
                SetVisibleGradesAsItems: function (element) {
                    var filtered = []; angular.forEach(element.Grades, function (value, key) {
                        if (value.Visible) {
                            filtered.push({ Text: value.Title, Value: value.Value });
                        }
                    });
                    element.Items = filtered;
                },
                Initialize: function (element) {
                  
                    if (is.not.existy(element.Grades)) {
                        element.Grades = angular.copy($scope.JbSchoolGrade.Operate.Grades);

                        var grades = {};
                        for (var gi = 0; gi < element.Grades.length; gi++) {
                            grades[element.Grades[gi].Value] = { Title: element.Grades[gi].Title, Value: element.Grades[gi].Value };
                        }

                        var items = {};
                        for (var ii = 0; ii < element.Items.length; ii++) {
                            items[element.Items[ii].Value] = { Text: element.Items[ii].Text, Value: element.Items[ii].Value };
                        }

                        for (gi = 0; gi < element.Grades.length; gi++) {
                            if (is.not.existy(items[element.Grades[gi].Value])) {
                              
                                element.Grades[gi].Visible = false;
                            }
                        }
                    }
                }
            }
        }

        $scope.JbSchoolGender = {
            Operate: {
                Genders: [],
                SetVisibleGendersAsItems: function (element) {
                    var filtered = []; angular.forEach(element.Genders, function (value, key) {
                        if (value.Visible) {
                            filtered.push({ Text: value.Title, Value: value.Value });
                        }
                    });
                    element.Items = filtered;
                },
                Initialize: function (element) {
                    if (is.not.existy(element.Genders)) {
                        element.Genders = angular.copy($scope.JbSchoolGender.Operate.Genders);

                        var genders = {};
                        for (var gi = 0; gi < element.Genders.length; gi++) {
                            genders[element.Genders[gi].Value] = { Title: element.Genders[gi].Title, Value: element.Genders[gi].Value };
                        }

                        var items = {};
                        for (var ii = 0; ii < element.Items.length; ii++) {
                            items[element.Items[ii].Value] = { Text: element.Items[ii].Text, Value: element.Items[ii].Value };
                        }

                        for (gi = 0; gi < element.Genders.length; gi++) {
                            if (is.not.existy(items[element.Genders[gi].Value])) {
                              
                                element.Genders[gi].Visible = false;
                            }
                            if (is.existy(items[element.Genders[gi].Value])) {

                                element.Genders[gi].Visible = true;
                            }
                        }
                    }
                }
            }
        }

        $scope.JbParentsRelationships = {
            Operate: {
                Relationships: [],
                SetVisibleRelationshipsAsItems: function (element) {
                    var filtered = []; angular.forEach(element.Relationships, function (value, key) {
                        if (value.Visible) {
                            filtered.push({ Text: value.Title, Value: value.Value });
                        }
                    });
                    element.Items = filtered;
                  
                },
                Initialize: function (element) {
                  
                    if (is.not.existy(element.Relationships)) {
                        element.Relationships = angular.copy($scope.JbParentsRelationships.Operate.Relationships);

                        var relationships = {};
                        for (var gi = 0; gi < element.Relationships.length; gi++) {
                            relationships[element.Relationships[gi].Value] = { Title: element.Relationships[gi].Title, Value: element.Relationships[gi].Value };
                        }

                        var items = {};
                        for (var ii = 0; ii < element.Items.length; ii++) {
                            items[element.Items[ii].Value] = { Text: element.Items[ii].Text, Value: element.Items[ii].Value };
                        }

                        for (gi = 0; gi < element.Relationships.length; gi++) {
                            if (is.not.existy(items[element.Relationships[gi].Value])) {
                            
                                element.Relationships[gi].Visible = false;
                            }
                            if (is.existy(items[element.Relationships[gi].Value])) {
                             
                                element.Relationships[gi].Visible = true;
                            }
                        }
                    }
                }
            }
        }

        $scope.JbRelationships = {
            Operate: {
                Relationships: [],
                SetVisibleRelationshipsAsItems: function (element) {
                    var filtered = []; angular.forEach(element.Relationships, function (value, key) {
                        if (value.Visible) {
                            filtered.push({ Text: value.Title, Value: value.Value });
                        }
                    });
                    element.Items = filtered;

                },
                Initialize: function (element) {

                    if (is.not.existy(element.Relationships)) {
                        element.Relationships = angular.copy($scope.JbRelationships.Operate.Relationships);

                        var relationships = {};
                        for (var gi = 0; gi < element.Relationships.length; gi++) {
                            relationships[element.Relationships[gi].Value] = { Title: element.Relationships[gi].Title, Value: element.Relationships[gi].Value };
                        }

                        var items = {};
                        for (var ii = 0; ii < element.Items.length; ii++) {
                            items[element.Items[ii].Value] = { Text: element.Items[ii].Text, Value: element.Items[ii].Value };
                        }

                        for (gi = 0; gi < element.Relationships.length; gi++) {
                            if (is.not.existy(items[element.Relationships[gi].Value])) {

                                element.Relationships[gi].Visible = false;
                            }
                            if (is.existy(items[element.Relationships[gi].Value])) {

                                element.Relationships[gi].Visible = true;
                            }
                        }
                    }
                }
            }
        }

        $scope.JbHearAboutUs = {
            Operate: {
                HearAboutUs: [],
                SetVisibleHearAboutUsAsItems: function (element) {
                    var filtered = []; angular.forEach(element.HearAboutUs, function (value, key) {
                        if (value.Visible) {
                            filtered.push({ Text: value.Title, Value: value.Value });
                        }
                    });
                    element.Items = filtered;
                },
                Initialize: function (element) {
                    if (is.not.existy(element.HearAboutUs)) {
                        element.HearAboutUs = angular.copy($scope.JbHearAboutUs.Operate.HearAboutUs);

                        var HearAboutUs = {};
                        for (var gi = 0; gi < element.HearAboutUs.length; gi++) {
                            HearAboutUs[element.HearAboutUs[gi].Value] = { Title: element.HearAboutUs[gi].Title, Value: element.HearAboutUs[gi].Value };
                        }

                        var items = {};
                        for (var ii = 0; ii < element.Items.length; ii++) {
                            items[element.Items[ii].Value] = { Text: element.Items[ii].Text, Value: element.Items[ii].Value };
                        }

                        for (gi = 0; gi < element.HearAboutUs.length; gi++) {
                            if (is.not.existy(items[element.HearAboutUs[gi].Value])) {
                                element.HearAboutUs[gi].Visible = false;
                            }
                            if (is.existy(items[element.HearAboutUs[gi].Value])) {
                                element.HearAboutUs[gi].Visible = true;
                            }
                        
                        }
                    }
                }
            }
        }

        $scope.JbSelfAdministerMedication = {
            Operate: {
                SelfAdministeredMedication: [],
                SetVisibleSelfAdministeredMedicationAsItems: function (element) {
                    var filtered = []; angular.forEach(element.SelfAdministerMedication, function (value, key) {
                        if (value.Visible) {
                            filtered.push({ Text: value.Title, Value: value.Value });
                        }
                    });
                    element.Items = filtered;
                },
                Initialize: function (element) {
                    if (is.not.existy(element.SelfAdministerMedication)) {
                        element.SelfAdministerMedication = angular.copy($scope.JbSelfAdministerMedication.Operate.SelfAdministerMedication);

                        var SelfAdministerMedication = {};
                        for (var gi = 0; gi < element.SelfAdministerMedication.length; gi++) {
                            SelfAdministerMedication[element.SelfAdministerMedication[gi].Value] = { Title: element.SelfAdministerMedication[gi].Title, Value: element.SelfAdministerMedication[gi].Value };
                        }

                        var items = {};
                        for (var ii = 0; ii < element.Items.length; ii++) {
                            items[element.Items[ii].Value] = { Text: element.Items[ii].Text, Value: element.Items[ii].Value };
                        }

                        for (gi = 0; gi < element.SelfAdministerMedication.length; gi++) {
                            if (is.not.existy(items[element.SelfAdministerMedication[gi].Value])) {
                                element.SelfAdministerMedication[gi].Visible = false;
                            }
                            if (is.existy(items[element.SelfAdministerMedication[gi].Value])) {
                                element.SelfAdministerMedication[gi].Visible = true;
                            }

                        }
                    }
                }
            }
        }

        $scope.JbNutAllergy = {
            Operate: {
                NutAllergy: [],
                SetVisibleNutAllergyAsItems: function (element) {
                    var filtered = []; angular.forEach(element.NutAllergy, function (value, key) {
                        if (value.Visible) {
                            filtered.push({ Text: value.Title, Value: value.Value });
                        }
                    });
                    element.Items = filtered;
                },
                Initialize: function (element) {
                    if (is.not.existy(element.NutAllergy)) {
                        element.NutAllergy = angular.copy($scope.JbNutAllergy.Operate.NutAllergy);

                        var NutAllergy = {};
                        for (var gi = 0; gi < element.NutAllergy.length; gi++) {
                            NutAllergy[element.NutAllergy[gi].Value] = { Title: element.NutAllergy[gi].Title, Value: element.NutAllergy[gi].Value };
                        }

                        var items = {};
                        for (var ii = 0; ii < element.Items.length; ii++) {
                            items[element.Items[ii].Value] = { Text: element.Items[ii].Text, Value: element.Items[ii].Value };
                        }

                        for (gi = 0; gi < element.NutAllergy.length; gi++) {
                            if (is.not.existy(items[element.NutAllergy[gi].Value])) {
                                element.NutAllergy[gi].Visible = false;
                            }
                            if (is.existy(items[element.NutAllergy[gi].Value])) {
                                element.NutAllergy[gi].Visible = true;
                            }

                        }
                    }
                }
            }
        }

        $scope.JbPermissionPhotography = {
            Operate: {
                PermissionPhotography: [],
                SetVisiblePermissionPhotographyAsItems: function (element) {
                    var filtered = []; angular.forEach(element.PermissionPhotography, function (value, key) {
                        if (value.Visible) {
                            filtered.push({ Text: value.Title, Value: value.Value });
                        }
                    });
                    element.Items = filtered;
                },
                Initialize: function (element) {
                    if (is.not.existy(element.PermissionPhotography)) {
                        element.PermissionPhotography = angular.copy($scope.JbPermissionPhotography.Operate.PermissionPhotography);

                        var PermissionPhotography = {};
                        for (var gi = 0; gi < element.PermissionPhotography.length; gi++) {
                            PermissionPhotography[element.PermissionPhotography[gi].Value] = { Title: element.PermissionPhotography[gi].Title, Value: element.PermissionPhotography[gi].Value };
                        }

                        var items = {};
                        for (var ii = 0; ii < element.Items.length; ii++) {
                            items[element.Items[ii].Value] = { Text: element.Items[ii].Text, Value: element.Items[ii].Value };
                        }

                        for (gi = 0; gi < element.PermissionPhotography.length; gi++) {
                            if (is.not.existy(items[element.PermissionPhotography[gi].Value])) {
                                element.PermissionPhotography[gi].Visible = false;
                            }
                            if (is.existy(items[element.PermissionPhotography[gi].Value])) {
                                element.PermissionPhotography[gi].Visible = true;
                            }

                        }
                    }
                }
            }
        }

        $scope.OpenSection = function (sectionId) {
            setTimeout(function () {
                $("#" + sectionId + " .accordion-frame > .heading.collapsed").removeClass("collapsed");
                $("#" + sectionId + " .accordion-frame > .content").css("display", "block");
                try {
                    $('#bodyPanel').animate({
                        scrollTop: $(".new-elemet").offset().top - $('#bodyPanel > .view').offset().top - 100
                    }, 'slow');
                } catch (e) {
                    try {
                        $('#bodyPanel').animate({
                            scrollTop: $(".edit-mode:not(.ng-hide)").offset().top - $('#bodyPanel > .view').offset().top - 100
                        }, 'slow');
                    } catch (e) { }
                }
            }, 50);
        }

        $scope.JbForm.Load();


        $scope.isDefaultFormSelected = function () {

            for (var i = 0; i < $scope.Model.FormTemplates.length; i++) {
                if ($scope.Model.FormTemplates[i].Value == $scope.Model.FormTemplate) {
                    if ($scope.Model.FormTemplates[i].Text == 'Default' || $scope.Model.FormTemplates[i].Text == 'Default school') {

                        $scope.JbForm.SaveAsMode = true;

                        return true;
                    }
                }
            }

            return false;
        }
    }]);
})();