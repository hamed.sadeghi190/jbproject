﻿using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using System.Linq;

namespace Jumbula.Business
{
    public class FamilyBusiness : IFamilyBusiness
    {
        private readonly IRepository<Family> _familyRepository;
        public FamilyBusiness(IRepository<Family> familyRepository)
        {
            _familyRepository = familyRepository;
        }

        public Family GetFamily(int userId)
        {
            return _familyRepository.Get(p => p.UserId == userId);
        }

        public  OperationStatus Update(Family entity)
        {
            var currentFamily = GetFamily(entity.UserId);

            return _familyRepository.Update(currentFamily);
        }

        public OperationStatus Create(Family entity)
        {
            return _familyRepository.Create(entity);
        }

        public Family AddUserFamily(int userId)
        {
            var newFamily = new Family {UserId = userId};
            _familyRepository.Create(newFamily);

            return newFamily;
        }

        public IQueryable<Family> GetList()
        {
            return _familyRepository.GetList();
        }
    }
}
