﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models.Season
{
    public class SaleOverTimeModel
    {
        public string Total { get; set; }

        public decimal Price { get; set; }

        public string Date { get; set; }

        public DateTime SortDate { get; set; }

        public decimal Revenue { get; set; }
    }
}