﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class ClientPaymentMethod : BaseEntity<long>
    {
        public ClientPaymentMethod()
        {
            EnableStripePayment = false;
            EnablePaypalPayment = false;
            EnableManualPayment = false;
            EnableCashPayment = false;
            CreditCardPreferredLabel = "Express payment methods";
        }

        [Key, ForeignKey("Client")]
        public override long Id { get; set; }
        public bool EnableStripePayment { get; set; }
        public bool IsCreditCardTestMode { get; set; }
        public string StripeCustomerId { get; set; }
        public string StripeAccessToken { get; set; }
        public string StripeResponse { get; set; }
        public string CreditCardCheckoutLabel { get; set; }

        public bool EnablePaypalPayment { get; set; }
        public bool IsPaypalTestMode { get; set; }
        public string PaypalEmail { get; set; }
        public string PaypalCheckoutLabel { get; set; }
        public bool EnableAuthorizePayment { get; set; }

        [StringLength(128)]
        public string AuthorizeLoginId { get; set; }

        [StringLength(128)]
        public string AuthorizeTransactionKey { get; set; }
        public PaymentGateway DefaultPaymentGateway { get; set; }

        public bool EnableCashPayment { get; set; }
        public string CashPaymentMessage { get; set; }
        public string CashPaymentInstruction { get; set; }
        public string CashCheckoutLabel { get; set; }
        public bool EnableBankTransferPayment { get; set; }
        public string BankTransferPaymentMessage { get; set; }
        public string BankTransferPaymentInstruction { get; set; }
        public string BankTransferCheckoutLabel { get; set; }
        public bool EnableManualPayment { get; set; }
        public string ManualPaymentName { get; set; }
        public string ManualPaymentMessage { get; set; }
        public string ManualPaymentInstruction { get; set; }
        public bool EnableAchPayment { get; set; }
        public string AchPaymentCheckoutLabel { get; set; }
        public string AchPaymentMessage { get; set; }
        public string AchPaymentInstruction { get; set; }
        public PaymentMethod ManualPaymentMethod { get; set; }
        public virtual Client Client { get; set; }
        public string CreditCardPreferredLabel { get; set; }
    }

    #region Enums
    public enum PaymentGateway
    {
        Stripe,
        AuthorizeNet
    }
    #endregion
}
