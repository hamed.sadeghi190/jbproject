﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class PlayerProgramNote : BaseEntity
    {
        public PlayerProgramNote()
        {
            MetaData = new MetaData();
        }

        [StringLength(4000)]
        public string Note { get; set; }

        [ForeignKey("ProgramId")]
        public virtual Program Program { get; set; }

        public long ProgramId { get; set; }

        [ForeignKey("ClubStaffId")]
        public virtual ClubStaff ClubStaff { get; set; }

        public int ClubStaffId { get; set; }

        public MetaData MetaData { get; set; }

        [ForeignKey("ProfileId")]
        public virtual PlayerProfile PlayerProfile { get; set; }
        public int ProfileId { get; set; }

        [StringLength(100)]
        public string Title { get; set; }

        [StringLength(10)]
        public string Color { get; set; }

        public bool IsDeleted { get; set; }

    }
}
