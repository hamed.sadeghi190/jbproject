﻿using Jumbula.Common.Enums;
using System;

namespace Jumbula.Core.Model.Catalog
{
    public class SpaceRequirementsViewModel
    {
        public int Id { get; set; }

        public string StringId => Id == 0 ? Guid.NewGuid().ToString() : Id.ToString();
        public string Title { get; set; }
        public SpaceRequirementType Type { get; set; }
        public bool IsChecked { get; set; }

        public bool IsDefault { get; set; }
    }
}
