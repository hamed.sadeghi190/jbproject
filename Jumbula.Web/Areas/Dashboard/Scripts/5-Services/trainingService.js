﻿(function () {
    'use strict';

    angular.module('dashboardApp').service("trainingService", [function () {
        var _model;
        function setModel(Data) {
            _model = Data;
        }
        function getModel() {
            return _model;
        }
        var btnShowTrainingBox = angular.element('#showTrainingBox'),
            trainingBox = angular.element('#trainingBox'),
            search = "";
        this.init = function () {
            btnShowTrainingBox = angular.element('#showTrainingBox');
            btnShowTrainingBox.on('click', function () {
            
                trainingBox.html("")
                trainingBox.slideToggle(500);

                trainingBox.append('<div class="loader more centeral-pos"></div>');
                trainingBox.append('<div class="training-box-bar"><i class="icon-cancel-2" title="Close"></i> <i class="jbi-maximize" title="Maximize"></i> <i class="jbi-minus" title="Minimize"></i></div>');
                trainingBox.append('<div id="searchTrainings" class="input-control text" data-role="input-control"> <input type="text" placeholder="How can we help you?"> <button class="btn-search"></button> </div>')
                trainingBox.append('<div id="wrapperTranings"></div>');

                //Bar actions start
                trainingBox.find(".training-box-bar>.icon-cancel-2").on('click', function () {
                    trainingBox.removeClass("training-box-expand");
                    trainingBox.slideUp(500);
                });
                trainingBox.find(".training-box-bar>.jbi-maximize").on('click', function () {
                    trainingBox.addClass("training-box-expand");
                });
                trainingBox.find(".training-box-bar>.jbi-minus").on('click', function () {
                    trainingBox.removeClass("training-box-expand");
                });
                //Bar actions end

                trainingBox.css({ 'backgroundColor': '#002d4e' })
                var loader = trainingBox.find(".loader"),
                    wrapperTranings = trainingBox.find("#wrapperTranings"),
                    searchInput = trainingBox.find("#searchTrainings input"),
                        searchBtn = trainingBox.find("#searchTrainings .btn-search");
                wrapperTranings.append('<p id="suggestsTitle">Suggested articles</p>')
                loader.css("display", "block");

                searchInput.keypress(function (e) {
                    if (e.keyCode == 13) {

                        searchBtn.click()
                    }

                });
                searchBtn.on('click', function () {
                    wrapperTranings.html("");
                    trainingBox.css({ 'backgroundColor': '#002d4e' })
                    loader.css("display", "block");
                    find(searchInput.val());
                    wrapperTranings.append('<button id="backToSuggests">Back</button>');
                    wrapperTranings.find("#backToSuggests").on('click', function () {
                        searchInput.val("");
                        wrapperTranings.html("");
                        trainingBox.css({ 'backgroundColor': '#002d4e' })
                        loader.css("display", "block");
                        wrapperTranings.append('<p id="suggestsTitle">Suggested articles</p>')
                        find(search);
                    });
                })
                find(search);

            })
        }
        //Training component start

        this.setting = function (setting) {

            if (setting.show == false) {
                //btnShowTrainingBox.hide();
                trainingBox.hide();
            }
            if (setting.searchKeyword) {
                search = setting.searchKeyword;
            }
        }
        var find = function (search) {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "https://kb.jumbula.com/wp-json/wp/v2/posts?search=" + search, true);
            xhr.onreadystatechange = function () {
        
                if (xhr.readyState == 4) {
                    trainingBox.css({ 'backgroundColor': '#f5f5f5' });
                    trainingBox.find(".loader").css({ 'display': 'none' })
                    //trainingBox.find("#wrapperTranings").html("");
                    if (xhr.status >= 200 && xhr.status < 304) {
                        var response = JSON.parse(xhr.responseText);
                        if (response.length == 0) {
                            trainingBox.find("#wrapperTranings").append('<p class="tranings-error">No matching articles found.</p>')
                        }
                        $.each(response, function (index, post) {

                            var tm = "<article>";
                            tm += "<header>";
                            tm += " <h3>" + post.title.rendered + "</h3>";
                            tm += "</header>";
                            tm += "<div>" + post.content.rendered + "</div>";
                            tm += "</article>";
                            trainingBox.find("#wrapperTranings").append(tm);

                        });
                        trainingBox.find('header').on('click', function (e) {
                            angular.element(e.currentTarget).toggleClass('active');
                            angular.element(e.currentTarget).next().slideToggle(400)
                        });
                    } else {

                        trainingBox.find("#wrapperTranings").append('<p class="tranings-error">Internet connection lost, please try again later.</p>')

                    }

                }
            }
            xhr.send();
        }
      

        //Training component end

    }]);
})();

