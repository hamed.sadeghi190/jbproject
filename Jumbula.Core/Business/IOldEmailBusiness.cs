﻿using Jumbula.Core.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IOldEmailBusiness
    {
        void SendEmail(int clubId, string resiver, string subject, string content);
        void SendReservationEmail(List<string> toPrimaryPlayerEmail, List<string> toSecondaryPlayerEmail, List<string> toClubOwner, List<string> toOutSourcerCLubOwner, string subject, string body, string clubOwnerName);

        void SendEmail(string from, string to, string subj, string body, bool isBodyHtml, bool sendToSupport = false);

        void SendEmail(MailAddress from, MailAddress to, string subj, string body, bool isBodyHtml, MailAddress bcc = null, List<MailAddress> cc = null, string clubDomain = null, string fileNames = null);

        void SendEmail(MailAddress from, MailAddress to, MailAddress replyTo, string subj, string body, bool isBodyHtml);

        void SendEmailAsync(MailAddress from, MailAddress to, MailAddress replyTo, string subj, string body, bool isBodyHtml);
        void SendInstallmentReminderEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName);
        void SendInvoicePaymentForParentEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string outSourcerClubOwnerEmail, string emailSubject, string body, string clubName, List<MailAddress> cc = null);
        void SendInvoicePaymentForAdminEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string outSourcerClubOwnerEmail, string emailSubject, string body, string clubName);
        void SendPreApprovalEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName);
        void SendInstallmentConfirmationEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string outSourcerClubOwnerEmail, string emailSubject, string body, string clubName);

        void SendConfirmationEmail(MailAddress from, MailAddress to, string subject, string body);

        void SendErrorMessages(string primaryemail, string body);
        void SendProviderRespondToInvitation(List<string> clubOwnerEmail, string emailSubject, string body, string clubName);
        void SendECheckClearedNotification(List<string> clubOwnerEmail, string emailSubject, string body, string clubName);
        void SendClientBillingNotification(List<string> clubOwnerEmail, string emailSubject, string body);

        void SendInvoiceForAdminEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName);
        void SendInvoiceForParentEmail(string primaryPlayerEmail, string emailSubject, string body, string clubName, List<MailAddress> cc = null);
        void SendCancellationOrReminderEmailToAdmin(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName);
        void SendCancellationOrReminderEmailToParent(string primaryPlayerEmail, string emailSubject, string body, string clubName, List<MailAddress> cc = null);
        void SendWaitListForAdmin(MailAddress fromClubOwner, List<string> clubOwnerEmail, string emailSubject, string body);
        void SendEmailForErrorSendGrid(string emailSubject, string body, string clubName);
        void SendTakePaymentForParentEmail(string primaryPlayerEmail, string emailSubject, string body, string clubName, List<MailAddress> cc = null);
        void SendTakePaymentForAdminEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName);

        void SendRegisterErrorEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string outSourcerClubOwnerEmail, string emailSubject, string body, string clubName);

        OperationStatus Create(Email email);
        OperationStatus Create(List<Email> emails);
        IQueryable<Email> GetList();
       
        void SendProgramParentsEmail(List<JbUser> programParents, MailAddress sender, string subject, string body, IEnumerable<OrderItem> programOrderItems);
        void SendSubscriptionForAdminEmail(string primaryPlayerEmail, List<string> clubOwnerEmail, string emailSubject, string body, string clubName);
        string GetParametersFromContact(string body, string className, string seasonName, string childName, string clubName, string providerName, string partnerName);
        string FineWithParemetersInContent(string body, string param, string className, string seasonName, string childName, string clubName, string providerName, string partnerName);
        void SendUpdateRasInstallment(string primaryemail, string body);
    }
}
