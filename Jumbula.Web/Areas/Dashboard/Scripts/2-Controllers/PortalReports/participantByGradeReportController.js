﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('participantByGradeReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'customReportService', function ($scope, $http, $log, $stateParams, $state, jbToast,customReportService) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });
        $scope.baseUrl = window.location.origin + "/Dashboard/PortalReport/";

        $scope.Model = {};
        $scope.Model.checkedIds = {};

            $scope.AllParticipantReportsDataSource = new kendo.data.DataSource({
                type: "json",
                transport: {
                    read: {
                        url: $scope.baseUrl + '/GetAllParticipantByGrade/',
                        contentType: "application/json; charset=utf-8",
                        type: "POST",
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            data = $.extend(
                               {
                                   grade: $stateParams.grade,
                               }, data);
                            return JSON.stringify(data);
                        }
                    },
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.ParticipantReportGridOptions = {
                dataSource: $scope.AllParticipantReportsDataSource,
                resizable: true,
                serverPaging: true,
                scrollable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if ($scope.Model.checkedIds[view[i].PlayerId]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                        }
                    }
                },
                sortable: { mode: 'single' },
                columns: [

                    {
                        template: "<input type='checkbox' class='checkbox' />",
                        width: "30px",
                        headerTemplate: '<input type="checkbox" class="checkAll" />'
                    },
                    {
                        field: "ParticipantName",
                        title: "Participant",
                        width: "180px",
                    },
                    {
                        field: "UserName",
                        title: "Account",
                        width: "210px",
                    },
                    {
                        field: "StrDate",
                        title: "Last order date",
                        width: "170px",
                    },
                ]
            }
     

        $(document.body).on('click', '#ParticipantReportGrid table .checkbox', selectRow);
        $(document.body).on('click', '#ParticipantReportGrid table .checkAll', checkAll);

        function selectRow() {
            var checked = this.checked,
            row = $(this).closest("tr"),
            grid = $("#ParticipantReportGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);
            $scope.Model.checkedIds[dataItem.PlayerId] = checked;

            if (checked) {
                //-select the row
                row.addClass("k-state-selected");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
            }
        }

        function checkAll() {
            var grid = $("#ParticipantReportGrid").data("kendoGrid");
            var state = $("#ParticipantReportGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
            var ckbox;
            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                ckbox = $this.find("td .checkbox");
                ckbox.prop("checked", state);
                var dataItem = grid.dataItem($this);
                $scope.Model.checkedIds[dataItem.PlayerId] = state;
                if (state) {
                    //-select the row
                    $this.addClass("k-state-selected");
                } else {
                    //-remove selection
                    $this.removeClass("k-state-selected");
                }
            });
        }

        $scope.SetGraduateds = function () {
            var checked = [];
            for (var i in $scope.Model.checkedIds) {
                if ($scope.Model.checkedIds[i]) {
                    checked.push(i);
                }
            }
            if (checked.length > 0) {
                $http.post($scope.baseUrl + "/SaveGraduateds", { playerIds: checked })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            jbToast.success("Participants status updated successfully.")
                        } else {
                            jbToast.error(data.Message);
                        }
                    }).error(function (data, status, headers, config) {
                     
                    });
            } else {
                jbToast.error("Please select at least one participant.");
            }
        };

    }]);
})();