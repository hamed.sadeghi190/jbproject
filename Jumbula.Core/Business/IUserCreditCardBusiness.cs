﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.CreditCard;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;
using Jumbula.Core.Model.Generic;

namespace Jumbula.Core.Business
{
    public interface IUserCreditCardBusiness
    {
        UserCreditCard Get(int cradId);
        UserCreditCard Get(string CustomerId);

        UserCreditCard Get(ParentBillingMethodsViewModel source, int userId, PaymentMethod method, int creditId,
            PaymentGateway typePayment, PostalAddress postalAddress = null);
        UserCreditCard GetWithCardId(string creditCardId);
        IQueryable<UserCreditCard> GetParentCreditCards(int userId);
        UserCreditCard GetLastUserCreditCards(int userId);
        OperationStatus Update(UserCreditCard userCreditCard);
        OperationStatus Create(UserCreditCard userCreditCard);
        OperationStatus Delete(int userCreditCardId);
        OperationStatus ActivateUserCreditCard(int userId, int cardId);
        bool IsExist(string cardNumber, int userId, PaymentGateway typePayment);
        UserCreditCard GetListCard(int userId);
        IQueryable<UserCreditCard> GetAllList(int userId);
        IQueryable<UserCreditCard> GetAllList(int userId, PaymentGateway typeGateway);
        UserCreditCard GetActiveUserCreditCards(int userId, PaymentGateway typeGateway);
        IQueryable<UserCreditCard> GetListwithCustomerId();
        IQueryable<UserCreditCard> GetCreditCards(string cardNumber, int userId, PaymentGateway typePayment);
        bool IsExistAll(string cardNumber, int userId, PaymentGateway typeGateway);
        IQueryable<UserCreditCard> GetUserCreditCards(int userId, PaymentGateway typePayment);
        UserCreditCard GetExist(int Id, int userId, PaymentGateway typePayment);
        IQueryable<UserCreditCard> GetUserCreditCards(int userId);
        int CountWords(string strInput);
        IQueryable<UserCreditCard> GetUserCreditCards(List<int> userIds);
        List<SelectListItem> GetKeyValueUserCreditCards(int userId, int clubId);
        IQueryable<UserCreditCard> GetUserCreditCards(int userId, int clubId);
        string GetLastDigit(string cardNumber);
    }
}
