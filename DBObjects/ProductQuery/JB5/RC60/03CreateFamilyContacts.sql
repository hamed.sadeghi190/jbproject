
CREATE TABLE [dbo].[FamilyContacts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FamilyId] [int] NOT NULL,
	[ContactId] [int] NOT NULL,
	[Type] [tinyint] NOT NULL,
	[Relationship] [int] NULL,
	[Status] [int] NOT NULL DEFAULT ((0)),
	[IsEmergencyContact] [bit] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.FamilyContacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[FamilyContacts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.FamilyContacts_dbo.ContactPersons_ContactId] FOREIGN KEY([ContactId])
REFERENCES [dbo].[ContactPersons] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[FamilyContacts] CHECK CONSTRAINT [FK_dbo.FamilyContacts_dbo.ContactPersons_ContactId]
GO

ALTER TABLE [dbo].[FamilyContacts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.FamilyContacts_dbo.Families_FamilyId] FOREIGN KEY([FamilyId])
REFERENCES [dbo].[Families] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[FamilyContacts] CHECK CONSTRAINT [FK_dbo.FamilyContacts_dbo.Families_FamilyId]
GO


