﻿
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Data.DbHelpers;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
using Jumbula.Core.Model.Order;
using System.Web;
using PaymentConstants = Jumbula.Common.Constants.Payment;
using static Jumbula.Common.Constants.Payment;
using Jb.Framework.Common.Forms;
using Serilog;

namespace Jumbula.Business
{
    public class OrderItemBusiness : IOrderItemBusiness
    {

        private readonly IClubBusiness _clubBusiness;
        private readonly IPendingPreapprovalTransactionBusiness _pendingPreapprovalTransactionBusiness;
        private IPlayerProfileBusiness _playerProfileBussiness;
        private IProgramBusiness _programBusiness;
        private ISubscriptionBusiness _subscriptionBusiness;

        private readonly IUserProfileBusiness _userProfileBusienss;
        private ISeasonBusiness _seasonBusiness;
        private IOrderSessionBusiness _orderSessionBusiness;
        private readonly IRepository<OrderItem, long> _orderItemRepository;
        private readonly IEntitiesContext _dataContext;
        private readonly IJbDbFunctions _jbDbFunctions;
        private IOrderInstallmentBusiness _orderInstallmentBusiness;
        private IOrderBusiness _orderBusiness;
        private IProgramSessionBusiness _programSessionBusiness;
        private readonly IClubSettingBusiness _clubSettingBusiness;
        private readonly IFormBusiness _formBusiness;
        private readonly IAccountingBusiness _accountingBusiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;
        private readonly IPaymentBusiness _paymentBusiness;
        private readonly IOrderHistoryBusiness _orderHistoryBusiness;

        public OrderItemBusiness(IClubBusiness clubBusiness,
        IPendingPreapprovalTransactionBusiness pendingPreapprovalTransactionBusiness,
        IUserProfileBusiness userProfileBusienss,
        IEntitiesContext dataContext,
        IRepository<OrderItem, long> orderItemRepository, IClubSettingBusiness clubSettingBusiness,
        IFormBusiness formBusiness, IAccountingBusiness accountingBusiness,
        IJbDbFunctions jbDbFunctions, ITransactionActivityBusiness transactionActivityBusiness,
         IPaymentBusiness paymentBusiness, IOrderHistoryBusiness orderHistoryBusiness)
        {
            _clubBusiness = clubBusiness;
            _pendingPreapprovalTransactionBusiness = pendingPreapprovalTransactionBusiness;
            _userProfileBusienss = userProfileBusienss;
            _dataContext = dataContext;
            _orderItemRepository = orderItemRepository;
            _jbDbFunctions = jbDbFunctions;
            _clubSettingBusiness = clubSettingBusiness;
            _formBusiness = formBusiness;
            _accountingBusiness = accountingBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
            _paymentBusiness = paymentBusiness;
            _orderHistoryBusiness = orderHistoryBusiness;
        }


        public IPlayerProfileBusiness PlayerProfileBusiness
        {
            get { return _playerProfileBussiness; }
            set { _playerProfileBussiness = value; }
        }
        public IOrderBusiness OrderBusiness
        {
            get { return _orderBusiness; }
            set { _orderBusiness = value; }
        }

        public IProgramSessionBusiness ProgramSessionBusiness
        {
            get { return _programSessionBusiness; }
            set { _programSessionBusiness = value; }
        }

        public IOrderInstallmentBusiness OrderInstallmentBusiness
        {
            get { return _orderInstallmentBusiness; }
            set { _orderInstallmentBusiness = value; }
        }

        public IProgramBusiness ProgramBusiness
        {
            get { return _programBusiness; }
            set { _programBusiness = value; }
        }

        public ISubscriptionBusiness SubscriptionBusiness
        {
            get { return _subscriptionBusiness; }
            set { _subscriptionBusiness = value; }
        }

        public ISeasonBusiness SeasonBusiness
        {
            get { return _seasonBusiness; }
            set { _seasonBusiness = value; }
        }

        public IOrderSessionBusiness OrderSessionBusiness
        {
            get => _orderSessionBusiness;
            set => _orderSessionBusiness = value;
        }


        public OperationStatus DeleteByISTS(long ists)
        {
            var operationStatus = new OperationStatus() { Status = true };
            return DeleteItems(GetOrderItemByISTS(ists).ToList());

        }

        public OperationStatus DeleteItems(List<OrderItem> orderItems)
        {
            var operationStatus = new OperationStatus() { Status = true };
            foreach (var item in orderItems)
            {
                item.ItemStatus = OrderItemStatusCategories.deleted;

                item.OrderSessions.ToList().ForEach(s => _dataContext.Entry(s).State = EntityState.Deleted);

                operationStatus = _orderItemRepository.Update(item);

            }
            return operationStatus;
        }

        public OperationStatus Delete(long orderItemId)
        {
            var orderItem = GetItem(orderItemId);
            orderItem.ItemStatus = OrderItemStatusCategories.deleted;

            orderItem.Order.OrderAmount = orderItem.Order.CalculateOrderAmount;

            _dataContext.Set<OrderSession>().RemoveRange(orderItem.OrderSessions);

            if (!orderItem.Order.OrderItems.Any(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.notInitiated))
            {
                orderItem.Order.OrderStatus = OrderStatusCategories.deleted;
            }

            var result = _orderItemRepository.Update(orderItem);
            return result;

        }
        public IQueryable<OrderItem> GetList()
        {
            return _orderItemRepository.GetList().Where(o => o.ItemStatus != OrderItemStatusCategories.deleted);
        }
        public IQueryable<OrderItem> GetComplatedOrderitems()
        {
            return GetList().Where(o => o.ItemStatus == OrderItemStatusCategories.completed || (o.ItemStatus == OrderItemStatusCategories.changed && (o.ItemStatusReason == OrderItemStatusReasons.canceled || o.ItemStatusReason == OrderItemStatusReasons.transferOut) && (o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || o.ProgramTypeCategory == ProgramTypeCategory.Subscription) && o.TotalAmount > 0));
        }

        private IEnumerable<OrderItem> GetClubUserCompletedOrCanceledOrderItems(int userId, int clubId)
        {
            var complatedOrChangedOrderItems = GetList().
                Where(o => o.ItemStatus != OrderItemStatusCategories.deleted
            && o.ItemStatus != OrderItemStatusCategories.initiated
            && o.ItemStatus != OrderItemStatusCategories.notInitiated);

            var withValidPaidOrTotalAmount = complatedOrChangedOrderItems.
                Where(o => o.TotalAmount > 0 || (o.TotalAmount <= 0 && o.PaidAmount > 0));

            return withValidPaidOrTotalAmount.Where(o => o.Order.IsLive && o.Order.UserId == userId);

        }

        private bool CheckIsPayable(OrderItem orderItem)
        {
            return GetPayableBalance(orderItem) > 0;
        }

        public decimal GetPayableBalance(OrderItem orderItem)
        {
            var balance = orderItem.TotalAmount - orderItem.PaidAmount;


            return balance > 0 ? balance : 0;
        }

        public OrderChargeDiscount GetLastCancellationCharge(OrderItem orderItem)
        {
            var cancelOrderItem = orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.CancellationFee).ToList();

            return cancelOrderItem.Any() ? cancelOrderItem.OrderBy(c => c.Id).ToList().LastOrDefault() : null;
        }

        public IQueryable<OrderItem> GetDelinquentFullPaidItems(int clubId, DelinquentPolicy delinquentPolicy)
        {
            if (delinquentPolicy.FamilyBalanceNoLimitDate != null)
            {
                var familyBalanceNoLimitDate = delinquentPolicy.FamilyBalanceNoLimitDate.Value.AddDays(1).Date;

                return GetValidOrderItems().Include(x => x.Season).Include(x => x.Order).Include(x => x.Order.Club).Include(x => x.Order.Club.ClubType).Where(x =>
                          (x.Order.ClubId == clubId || x.Order.Club.PartnerId == clubId) &&
                          x.PaymentPlanType == PaymentPlanType.FullPaid &&
                          x.ProgramScheduleId != null &&
                          x.Order.IsLive &&
                          x.Order.CompleteDate <= familyBalanceNoLimitDate &&
                          x.TotalAmount != x.PaidAmount &&
                          x.TotalAmount - x.PaidAmount >= delinquentPolicy.FamilyBalanceMinPrice).AsNoTracking();
            }

            return null;
        }
        public IQueryable<OrderInstallment> GetDelinquentOrderInstallmentItems(int clubId, DelinquentPolicy delinquentPolicy)
        {
            if (delinquentPolicy.AutoChargeFailAttemptNumber != 0)
            {
                return _orderInstallmentBusiness.GetDelinquentInstallmentItemsAsNoTracking(clubId,
                    delinquentPolicy.AutoChargeFailAttemptNumber);
            }

            return null;
        }

        public IQueryable<OrderItem> GetComplatedOrInitiatedOrderitems()
        {
            return GetList().Where(o => o.ItemStatus == OrderItemStatusCategories.completed && o.Order.IsLive);
        }
        public IQueryable<OrderItem> GetCompletetCancelOrderItems(long orderId)
        {
            return GetList().Where(o => (o.ItemStatus == OrderItemStatusCategories.completed || o.ItemStatus == OrderItemStatusCategories.changed && (o.ItemStatusReason == OrderItemStatusReasons.canceled || ((o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || o.ProgramTypeCategory == ProgramTypeCategory.Subscription) && o.ItemStatusReason == OrderItemStatusReasons.transferOut))) && o.Order_Id == orderId);
        }

        public IQueryable<OrderItem> GetCompletetCancelOrderItems(List<int> clubIds)
        {
            return GetList().Where(o => clubIds.Contains(o.Order.ClubId) && (o.ItemStatus == OrderItemStatusCategories.completed || o.ItemStatus == OrderItemStatusCategories.changed && (o.ItemStatusReason == OrderItemStatusReasons.canceled || (o.ProgramTypeCategory == ProgramTypeCategory.Subscription && o.ItemStatusReason == OrderItemStatusReasons.transferOut))));
        }

        public IQueryable<OrderItem> GetClubCompletetCancelOrderItems(int clubId)
        {
            return GetList().Where(o => clubId == o.Order.ClubId && (o.ItemStatus == OrderItemStatusCategories.completed || o.ItemStatus == OrderItemStatusCategories.changed && (o.ItemStatusReason == OrderItemStatusReasons.canceled || (o.ProgramTypeCategory == ProgramTypeCategory.Subscription && o.ItemStatusReason == OrderItemStatusReasons.transferOut))));
        }

        public IQueryable<OrderItem> GetInitiatedScheduleOrderitems(ProgramSchedule schedule)
        {
            return GetList().Where(o => o.ProgramScheduleId == schedule.Id && o.ItemStatus == OrderItemStatusCategories.initiated && o.Order.IsLive);
        }
        public IQueryable<OrderItem> GetClubOrderItems(int clubId)
        {
            return _orderItemRepository.GetList().Where(o => o.ItemStatus == OrderItemStatusCategories.completed && o.Order.ClubId == clubId);
        }
        public IEnumerable<OrderItem> GetProgramOrderItems(long programId, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetNotDonationItems(programId).AsEnumerable();

            if (itemStatus == OrderItemStatusCategories.completed)
            {
                query = query.Where(item => item.IsCompleted);
            }
            else
            {
                query =
                    query.Where(
                        item =>
                            item.ItemStatus == OrderItemStatusCategories.completed ||
                            item.ItemStatus == OrderItemStatusCategories.changed);
            }
            return query;
        }

        public List<OrderItem> GetSubscriptionReportOrderItems(long programId, DateTime date)
        {
            var result = GetProgramOrderItems(programId, OrderItemStatusCategories.showAll)
                .ToList()
                .Where(o => o.ItemStatus == OrderItemStatusCategories.completed || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.CancelEffectiveDate > date) || (o.ItemStatus == OrderItemStatusCategories.changed && o.Attributes.TransferEffectiveDate > date))
                .ToList();

            return result;
        }

        public IQueryable<ChessTourneyEntryItem> ChessTourneyEntries(long programId, long seasonId, int clubId)
        {
            var query = GetList().Where(item => item.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.ChessTournament
                && item.Order.ClubId == clubId && item.ProgramSchedule.ProgramId == programId && item.SeasonId == seasonId
                && (item.ItemStatus == OrderItemStatusCategories.completed));

            var result = query.Select(i => new ChessTourneyEntryItem
            {
                OrderItemId = i.Id,
                FirstName = i.FirstName,
                LastName = i.LastName,
                Date = i.Order.CompleteDate,
                Section = i.OrderItemChess.Section,
                Schedule = i.OrderItemChess.Schedule,
                Byes = i.OrderItemChess.Byes,
                Options = i.OrderItemChess.Options,
                JbForm = i.JbForm,
                ProfileId = i.PlayerId,
                UscfId = i.OrderItemChess.UscfId,
                UscfExpiration = i.OrderItemChess.UscfExpiration,
                UscfRatingQuick = i.OrderItemChess.UscfRatingQuick,
                UscfRatingReg = i.OrderItemChess.UscfRatingReg
            });

            return result;
        }
        public IQueryable<OrderItem> GetReportOrderItems(List<long> programIds, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetList().Where(item => item.ProgramScheduleId.HasValue &&
                                                item.ProgramScheduleId > 0 &&
                                                programIds.Contains(item.ProgramSchedule.ProgramId) &&
                                                (item.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == item.Order.IsLive);

            if (itemStatus == OrderItemStatusCategories.completed)
            {
                query = query.Where(item => item.ItemStatus == OrderItemStatusCategories.completed);
            }
            else
            {
                query =
                    query.Where(
                        item =>
                            item.ItemStatus == OrderItemStatusCategories.completed ||
                            item.ItemStatus == OrderItemStatusCategories.changed);
            }
            return query;
        }

        public IQueryable<OrderItem> GetReportOrderItemsBySchedule(long scheduleId, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetList().Where(item => item.ProgramScheduleId.HasValue &&
                                                scheduleId == item.ProgramScheduleId.Value &&
                                                (item.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == item.Order.IsLive);

            if (itemStatus == OrderItemStatusCategories.completed)
            {
                query = query.Where(item => item.ItemStatus == OrderItemStatusCategories.completed || (item.ProgramTypeCategory == ProgramTypeCategory.Subscription && (item.ItemStatusReason == OrderItemStatusReasons.transferOut || item.ItemStatusReason == OrderItemStatusReasons.canceled)));
            }
            else
            {
                query =
                    query.Where(
                        item =>
                            item.ItemStatus == OrderItemStatusCategories.completed ||
                            item.ItemStatus == OrderItemStatusCategories.changed);
            }
            return query;
        }

        public IQueryable<OrderItem> GetCustomReportOrderItems(List<long> scheduleIds, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetList().Where(item => item.ProgramScheduleId.HasValue &&
                                                item.ProgramScheduleId > 0 &&
                                                scheduleIds.Contains(item.ProgramScheduleId.Value) &&
                                                (item.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == item.Order.IsLive);

            if (itemStatus == OrderItemStatusCategories.completed)
            {
                query = query.Where(item => item.ItemStatus == OrderItemStatusCategories.completed);
            }
            else
            {
                query =
                    query.Where(
                        item =>
                            item.ItemStatus == OrderItemStatusCategories.completed ||
                            item.ItemStatus == OrderItemStatusCategories.changed);
            }
            return query;
        }

        public IQueryable<OrderItem> GetOrderItems(long clubId, string term, bool isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed, int? instructorId = null, ParticipantSearchType? searchType = null, string firstName = null, string lastName = null, bool hasPartner = false)
        {
            IQueryable<OrderItem> query = GetOrderItems(clubId: clubId, seasonId: null, isTestMode: isTestMode, itemStatus: itemStatus, hasPartner: hasPartner);

            if (instructorId.HasValue)
            {
                query = query.Where(o => o.ProgramSchedule.Program.Instructors != null && o.ProgramSchedule.Program.Instructors.Any(c => c.Id == instructorId));
            }

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }

            if (searchType.HasValue)
            {
                switch (searchType.Value)
                {
                    case ParticipantSearchType.Email:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                var user = _userProfileBusienss.GetList().SingleOrDefault(u => u.UserName.ToLower() == term);

                                if (user != null)
                                {
                                    query = query.Where(item => item.Order.UserId == user.Id);
                                }
                                else
                                {
                                    var players = _playerProfileBussiness.GetList(email: term).Select(p => p.Id).ToList();

                                    query = query.Where(item => players.Contains(item.PlayerId.Value));
                                }
                            }
                        }
                        break;
                    case ParticipantSearchType.ConfirmationId:
                        {
                            if (!string.IsNullOrWhiteSpace(term))
                            {
                                var mainOrderClub = GetList().FirstOrDefault(i => i.Order.ConfirmationId == term)?.Order.Club;
                                long correctClubId;

                                if (mainOrderClub != null && mainOrderClub.Id == clubId)
                                {
                                    correctClubId = clubId;
                                    query = GetList().Where(item => item.Order.ClubId == correctClubId && item.Order.IsLive != isTestMode && item.Order.ConfirmationId == term).AsQueryable();
                                }
                                else if (mainOrderClub?.PartnerId != null && mainOrderClub.PartnerClub.Id == clubId)
                                {
                                    correctClubId = mainOrderClub.Id;
                                    query = GetList().Where(item => item.Order.ClubId == correctClubId && item.Order.IsLive != isTestMode && item.Order.ConfirmationId == term).AsQueryable();
                                }
                                else
                                {
                                    query = GetList().Where(item => item.Order.ClubId == clubId && item.Order.IsLive != isTestMode && item.Order.ConfirmationId == term).AsQueryable();
                                }
                            }
                        }
                        break;
                    case ParticipantSearchType.Name:
                        {
                            if (!string.IsNullOrWhiteSpace(firstName) || !string.IsNullOrWhiteSpace(lastName))
                            {
                                var orderItemResult = Enumerable.Empty<OrderItem>().AsQueryable();

                                orderItemResult = query.Where(item => (string.IsNullOrEmpty(firstName) || item.FirstName.Contains(firstName)) && (string.IsNullOrEmpty(lastName) || item.LastName.Contains(lastName)));

                                query = orderItemResult;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            return query;
        }

        public IQueryable<OrderItem> GetOrderItems(List<long> orderItemIds)
        {
            return GetList().Where(x => orderItemIds.Contains(x.Id));
        }

        public int RegisteredCount(long chargeId, bool isTestMode)
        {

            int registeredCount = GetList().Where(item => item.ItemStatus == OrderItemStatusCategories.completed && item.Order.IsLive != isTestMode)
                    .SelectMany(item => item.OrderChargeDiscounts)
                    .Count(i => !i.IsDeleted && i.ChargeId == chargeId);

            return registeredCount;

        }

        public int SchedulelRegisteredCount(long scheduleId, bool isTestMode)
        {
            int count = GetList().Where(item => item.ProgramScheduleId.HasValue && item.ProgramScheduleId == scheduleId && item.ItemStatus == OrderItemStatusCategories.completed && item.Order.IsLive != isTestMode).Count();

            return count;
        }

        public IQueryable<OrderItem> GetOrderItems(long orderId, int userId, bool? isTestMode)
        {
            var query = GetList().Where(o => o.Order_Id == orderId && o.Order.UserId == userId && (o.ItemStatus == OrderItemStatusCategories.changed || o.ItemStatus == OrderItemStatusCategories.completed));

            if (isTestMode.HasValue)
            {
                query = query.Where(o => o.Order.IsLive != isTestMode.Value);
            }

            return query;
        }

        public IQueryable<OrderItem> GetOrderItems(long programId, bool isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetList().Where(item => (item.ItemStatus == OrderItemStatusCategories.completed || item.ItemStatus == OrderItemStatusCategories.changed || item.ItemStatus == OrderItemStatusCategories.initiated) && item.ProgramScheduleId.HasValue && item.ProgramScheduleId > 0 && item.ProgramSchedule.ProgramId == programId && item.Order.IsLive != isTestMode);
            if (itemStatus != OrderItemStatusCategories.showAll)
            {
                query = query.Where(item => item.ItemStatus == itemStatus);
            }
            return query;
        }



        public IQueryable<OrderItem> GetOrderItems(long clubId, long? seasonId, bool? isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed, bool hasPartner = false)
        {
            IQueryable<OrderItem> query = null;
            if (!seasonId.HasValue)
            {
                query = GetList().Where(item => item.Order.ClubId == clubId && item.ProgramScheduleId.HasValue && item.ProgramScheduleId > 0 && item.Order.IsLive != isTestMode);
            }
            else
            {
                query = GetList().Where(item => item.SeasonId == seasonId && item.ProgramScheduleId.HasValue && item.ProgramScheduleId > 0 && item.Order.IsLive != isTestMode);
            }

            if (hasPartner)
            {
                var provider = _clubBusiness.GetAllPartnerProviders((int)clubId);
                var schools = _clubBusiness.GetAllPartnerSchools((int)clubId).ToList();
                var schoolProviderIds = schools.Select(s => s.Id).ToList();
                schoolProviderIds.AddRange(provider.Select(s => s.Id));

                query = GetList().Where(item => schoolProviderIds.Contains(item.Order.ClubId) && item.ProgramScheduleId.HasValue && item.ProgramScheduleId > 0 && item.Order.IsLive != isTestMode);
            }

            if (isTestMode.HasValue)
            {
                query = query.Where(item => item.Order.IsLive != isTestMode.Value);
            }

            if (query != null && itemStatus != OrderItemStatusCategories.showAll)
            {
                query = query.Where(item => item.ItemStatus == itemStatus);
            }
            return query;
        }

        public IQueryable<OrderItem> GetOrderItems(List<int> clubIds, bool? isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            IQueryable<OrderItem> query = null;

            query = GetList().Where(item => clubIds.Contains(item.Order.ClubId) && item.ProgramScheduleId.HasValue && item.ProgramScheduleId > 0 && item.Order.IsLive != isTestMode);

            if (isTestMode.HasValue)
            {
                query = query.Where(item => item.Order.IsLive != isTestMode.Value);
            }

            if (query != null && itemStatus != OrderItemStatusCategories.showAll)
            {
                query = query.Where(item => item.ItemStatus == itemStatus);
            }
            return query;
        }

        public IQueryable<OrderItem> GetOrderItems(long programId, int userId, bool isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetList().Where(item => item.Order.UserId == userId && item.ProgramScheduleId.HasValue && item.ProgramSchedule.ProgramId == programId && item.Order.IsLive != isTestMode);
            if (query != null && itemStatus != OrderItemStatusCategories.showAll)
            {
                query = query.Where(item => item.ItemStatus == itemStatus);
            }
            return query;
        }

        public IQueryable<OrderItem> GetOrderItems(int userId, long seasonId, bool isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetList().Where(item => item.Order.UserId == userId && item.Order.IsLive != isTestMode && item.SeasonId == seasonId && item.ProgramScheduleId.HasValue && item.ProgramScheduleId.Value > 0);
            if (query != null && itemStatus != OrderItemStatusCategories.showAll)
            {
                query = query.Where(item => item.ItemStatus == itemStatus);
            }
            return query;
        }
        public IQueryable<OrderItem> GetClubUserOrderItems(int clubId, int userId, bool? isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetList().Where(o => o.Order.UserId == userId && o.Order.ClubId == clubId);

            var itemCompleted = query.Where(o => o.ItemStatus != OrderItemStatusCategories.showAll);
            var itemCanceled = query.Where(o => o.ItemStatus == OrderItemStatusCategories.changed && o.ItemStatusReason == OrderItemStatusReasons.canceled);
            var itemCanceledHaveFee = itemCanceled.SelectMany(i => i.OrderChargeDiscounts.Where(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.CancellationFee).Select(item => item.OrderItem));


            if (query != null)
            {
                query = itemCompleted.Concat(itemCanceledHaveFee).Distinct();
            }

            if (isTestMode.HasValue)
            {
                query = query.Where(o => o.Order.IsLive != isTestMode);
            }


            return query;
        }
        public IQueryable<OrderItem> GetUserOrderItems(int userId, bool? isTestMode, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.completed)
        {
            var query = GetList().Where(o => o.Order.UserId == userId);

            if (isTestMode.HasValue)
            {
                query = query.Where(o => o.Order.IsLive != isTestMode.Value);
            }

            if (query != null && itemStatus != OrderItemStatusCategories.showAll)
            {
                query = query.Where(item => item.ItemStatus == itemStatus);
            }
            return query;
        }
        public IQueryable<OrderItem> GetUserOrderItems(int userId)
        {
            var query = GetList().Where(o => o.Order.UserId == userId).Where(i =>
                           !i.Order.IsDraft && i.ProgramScheduleId.HasValue &&
                           (i.ItemStatus == OrderItemStatusCategories.completed ||
                           (((i.ProgramTypeCategory == ProgramTypeCategory.Subscription || i.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && i.Mode == OrderItemMode.Noraml) && (i.ItemStatus == OrderItemStatusCategories.changed && (i.ItemStatusReason == OrderItemStatusReasons.canceled || i.ItemStatusReason == OrderItemStatusReasons.transferOut))) || ((i.ProgramTypeCategory == ProgramTypeCategory.Subscription || i.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && (i.ItemStatus == OrderItemStatusCategories.changed && i.ItemStatusReason != OrderItemStatusReasons.transferOut &&
                            i.ItemStatusReason != OrderItemStatusReasons.canceled))));
            return query;
        }

        public IQueryable<OrderItem> GetOrderItemByISTS(long ists, int userId, OrderItemStatusCategories itemStatus = OrderItemStatusCategories.showAll)
        {
            var query = GetList().Where(item => item.ISTS == ists && item.Order.UserId == userId);

            if (query != null && itemStatus != OrderItemStatusCategories.showAll)
            {
                query = query.Where(item => item.ItemStatus == itemStatus);
            }
            return query;
        }
        public IQueryable<OrderItem> GetOrderItemByTransactionDate(List<long> ids, DateTime startDate, DateTime endDate, TransactionStatus transactionStatus = TransactionStatus.Success)
        {
            var query = GetList().Where(i => ids.Contains(i.Id) && i.TransactionActivities.Any(t => t.TransactionStatus == transactionStatus &&
                         t.TransactionDate >= startDate && t.TransactionDate <= endDate));
            return query;
        }


        public IQueryable<OrderItem> GetClubUserOrderItemsInSeason(int clubId, int userId, long seasonId, bool? isTestMode)
        {
            var query = GetList().Where(o => o.Order.UserId == userId && o.Order.ClubId == clubId && o.SeasonId == seasonId && o.ItemStatus != OrderItemStatusCategories.deleted);

            if (isTestMode.HasValue)
            {
                query = query.Where(o => o.Order.IsLive != isTestMode);
            }

            return query;
        }
        public IQueryable<OrderItem> GetParticipantItemsInSeason(long playerId, long seasonId, bool? isTestMode)
        {
            var query = GetList().Where(o => o.PlayerId == playerId && o.SeasonId == seasonId && o.ItemStatus != OrderItemStatusCategories.deleted);

            if (isTestMode.HasValue)
            {
                query = query.Where(o => o.Order.IsLive != isTestMode);
            }

            return query;
        }
        public IQueryable<OrderItem> GetOrderItemByISTS(long ists)
        {
            return GetList().Where(item => item.ISTS == ists && (item.ItemStatus == OrderItemStatusCategories.notInitiated || item.ItemStatus == OrderItemStatusCategories.initiated));
        }

        public IQueryable<OrderChargeDiscount> GetChargeDiscounts(long orderItemId)
        {
            return _dataContext.Set<OrderChargeDiscount>().Where(c => c.OrderItemId == orderItemId);
        }

        public OrderItem GetItem(long orderItemId)
        {
            return GetList().SingleOrDefault(item => item.Id == orderItemId);
        }

        public OperationStatus Edit(OrderItem orderItem, List<long> forDeleteIds, List<long> forDeleteSessionsIds, List<long> forActiveIds, int userId, List<OrderSession> deletedOrderSessions, string editMemo, List<ProgramSession> addProrgramSessions, List<DayOfWeek> days, List<int> forDeletePaidInFullIds)
        {
            if (orderItem.Order.OrderHistories == null)
            {
                orderItem.Order.OrderHistories = new List<OrderHistory>();
            }

            var historyDescription = "";

            if (orderItem.ProgramScheduleId.HasValue)
            {
                historyDescription = string.Format("Order {0} - {1}(${2}) is edited.",
                   orderItem.ProgramSchedule.Program.Name, orderItem.EntryFeeName, orderItem.EntryFee);
            }
            else
            {
                historyDescription = string.Format("Order {0} - {1}(${2}) is edited.",
                   orderItem.Name, orderItem.EntryFeeName, orderItem.TotalAmount);
            }


            historyDescription += string.Format(" Memo: {0}", editMemo);

            orderItem.Order.OrderHistories.Add(
               new OrderHistory
               {
                   Action = OrderAction.EditChargeDiscount,
                   ActionDate = DateTime.UtcNow,
                   Description = historyDescription,
                   UserId = userId,
                   OrderItemId = orderItem.Id
               });


            List<OrderInstallment> deletedInstallmentItem = new List<OrderInstallment>();
            var listForDeleted = new List<long>();
            listForDeleted.AddRange(forDeleteIds);
            listForDeleted.AddRange(forDeleteSessionsIds);

            foreach (var item in orderItem.Installments.Where(o => listForDeleted.Contains(o.Id)))
            {
                deletedInstallmentItem.Add(item);
                if (item.PendingPreapprovalTransactions != null && item.PendingPreapprovalTransactions.Any())
                {
                    foreach (var pendingTran in item.PendingPreapprovalTransactions)
                    {
                        pendingTran.Status = PendingPreapprovalTransactionStatus.Deleted;
                    }
                }
            }

            //if item is subscription we should remove session's installments that are deleted
            if (deletedOrderSessions.Count > 0)
            {
                var today = DateTime.UtcNow;
                var deletedSession = deletedOrderSessions.Where(o => o.ProgramSession.StartDateTime >= today);

                _dataContext.Set<OrderSession>().RemoveRange(deletedSession);
            }

            //if we have installment that is undo we should Add sessions for it schedule
            if (addProrgramSessions.Count > 0)
            {
                foreach (var programSession in addProrgramSessions)
                {
                    if (days.Contains(programSession.StartDateTime.DayOfWeek))
                    {
                        var orderSession = new OrderSession()
                        {
                            ProgramSessionId = programSession.Id,
                            MetaData = new MetaData { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow }
                        };

                        orderItem.OrderSessions.Add(orderSession);
                    }
                }
            }

            //if orderItem is pay-in full 
            orderItem.OrderItemScheduleParts.Where(i => forDeletePaidInFullIds.Contains(i.Id)).ToList().ForEach(o => o.IsDeleted = true);
            orderItem.OrderItemScheduleParts.Where(i => forDeletePaidInFullIds.Contains(i.Id)).ToList().ForEach(o => o.DeleteReason = DeleteReason.SessionDelete);

            //if orderItem is installment
            orderItem.Installments.Where(i => listForDeleted.Contains(i.Id)).ToList().ForEach(o => o.IsDeleted = true);
            orderItem.Installments.Where(i => forDeleteIds.Contains(i.Id)).ToList().ForEach(o => o.DeleteReason = DeleteReason.NormalDelete);
            orderItem.Installments.Where(i => forDeleteSessionsIds.Contains(i.Id)).ToList().ForEach(o => o.DeleteReason = DeleteReason.SessionDelete);

            //if installment is activated
            orderItem.Installments.Where(i => forActiveIds.Contains(i.Id)).ToList().ForEach(o => o.IsDeleted = false);
            orderItem.Installments.Where(i => forActiveIds.Contains(i.Id)).ToList().ForEach(o => o.DeleteReason = null);

            // if schedule part is activates
            orderItem.OrderItemScheduleParts.Where(i => forActiveIds.Contains(i.Id)).ToList().ForEach(o => o.IsDeleted = false);
            orderItem.OrderItemScheduleParts.Where(i => forActiveIds.Contains(i.Id)).ToList().ForEach(o => o.DeleteReason = null);

            orderItem.Order.OrderAmount = orderItem.Order.CalculateOrderAmount;
            var res = _orderItemRepository.Save();// Update(orderItem);
            if (res.Status)
            {
                _pendingPreapprovalTransactionBusiness.DeleteWhere(c => c.Status == PendingPreapprovalTransactionStatus.Deleted && c.OrderId == orderItem.Order_Id);
            }

            return res;
        }

        public OperationStatus EditDropIn(OrderItem orderItem, List<OrderSession> deletedSessions, List<OrderSession> UpdatedSessions, int userId, string stringDates, string editMemo)
        {
            if (orderItem.Order.OrderHistories == null)
            {
                orderItem.Order.OrderHistories = new List<OrderHistory>();
            }

            var historyDescription = "";
            if (orderItem.ProgramScheduleId.HasValue)
            {
                historyDescription = string.Format("Order {0} - {1}(${2}) is edited.",
                   orderItem.ProgramSchedule.Program.Name, orderItem.EntryFeeName, orderItem.EntryFee);

                if (!string.IsNullOrEmpty(stringDates))
                {
                    historyDescription = historyDescription + "You cancelled these sessions:" + stringDates;
                }
            }
            else
            {
                historyDescription = string.Format("Order {0} - {1}(${2}) is edited.",
                   orderItem.Name, orderItem.EntryFeeName, orderItem.TotalAmount);
            }

            if (!string.IsNullOrEmpty(editMemo))
            {
                historyDescription += string.Format(" Memo: {0}", editMemo);
            }

            orderItem.Order.OrderHistories.Add(
              new OrderHistory
              {
                  Action = OrderAction.EditChargeDiscount,
                  ActionDate = DateTime.UtcNow,
                  Description = historyDescription,
                  UserId = userId,
                  OrderItemId = orderItem.Id
              });
            var updatedDateList = new List<DateTime>();


            foreach (var orderSession in UpdatedSessions)
            {
                orderItem.OrderSessions.Add(orderSession);
            }

            if (deletedSessions.Count > 0)
            {
                var today = DateTime.UtcNow;
                _dataContext.Set<OrderSession>().RemoveRange(deletedSessions);
            }

            orderItem.Order.OrderAmount = orderItem.Order.CalculateOrderAmount;
            return _orderItemRepository.Save();
        }
        public OperationStatus CancelDropIn(OrderItem orderItem, int userId, decimal cancellationFee, string cancelMemo)
        {
            var isTestMode = !orderItem.Order.IsLive;

            var historyDescription = string.Empty;
            var date = DateTime.UtcNow;

            historyDescription = string.Format(Constants.Order_History_Cancel_Desc,
               orderItem.ProgramSchedule.Program.Name, orderItem.EntryFeeName, orderItem.EntryFee.ToString("C2", CultureInfo.CurrentCulture));
            if (!string.IsNullOrEmpty(cancelMemo))
            {
                historyDescription += string.Format(" Memo: {0}", cancelMemo);
            }

            orderItem.Order.OrderHistories.Add(
              new OrderHistory
              {
                  Action = OrderAction.Canceled,
                  ActionDate = date,
                  Description = historyDescription,
                  UserId = userId,
                  OrderItemId = orderItem.Id
              });

            var today = DateTime.UtcNow;
            var deletedSession = orderItem.OrderSessions;//.Where(o => o.ProgramSession.StartDateTime >= today);
            _dataContext.Set<OrderSession>().RemoveRange(deletedSession);

            foreach (var chargeDiscount in orderItem.GetOrderChargeDiscounts())
            {
                if (chargeDiscount.Category == ChargeDiscountCategory.CancellationFee)
                {
                    continue;
                }
                else
                {
                    chargeDiscount.IsDeleted = true;
                }
            }

            var total = orderItem.TotalAmount;
            orderItem.EntryFee = 0;
            orderItem.TotalAmount = cancellationFee;
            orderItem.Order.OrderAmount = (orderItem.Order.OrderAmount - total) + cancellationFee;
            orderItem.ItemStatus = OrderItemStatusCategories.changed;
            orderItem.ItemStatusReason = OrderItemStatusReasons.canceled;

            OperationStatus result = Update(orderItem);

            return result;
        }
        public TransactionActivity SetRefundTransaction(OrderItem orderItem, long? installmentId, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, DateTime date, string note, HandleMode handleMode = HandleMode.Online)
        {
            return new TransactionActivity()
            {
                Amount = transactionAmount,
                TransactionCategory = TransactionCategory.Refund,
                ClubId = orderItem.Order.ClubId,
                OrderId = orderItem.Order_Id,
                OrderItemId = orderItem.Id,
                InstallmentId = installmentId,
                PaymentDetail = paymentDetail,
                TransactionDate = date,
                TransactionStatus = transactionStatus,
                TransactionType = TransactionType.Refund,
                SeasonId = orderItem.SeasonId,
                HandleMode = handleMode,
                MetaData = new MetaData
                {
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now
                },

                Note = note
            };
        }

        public TransactionActivity SetPayTransaction(OrderItem orderItem, long? installmentId, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, DateTime date, string note)
        {
            return new TransactionActivity()
            {
                Amount = transactionAmount,
                TransactionCategory = TransactionCategory.TakePayment,
                ClubId = orderItem.Order.ClubId,
                OrderId = orderItem.Order_Id,
                OrderItemId = orderItem.Id,
                InstallmentId = installmentId,
                PaymentDetail = paymentDetail,
                TransactionDate = date,
                TransactionStatus = transactionStatus,
                TransactionType = TransactionType.Payment,
                SeasonId = orderItem.SeasonId,
                HandleMode = HandleMode.Online,
                MetaData = new MetaData
                {
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now
                },
                Note = note
            };
        }

        public decimal CalculateTotalAmount(OrderItem orderItem, bool realResultIfNegative = false)
        {
            decimal result = 0;

            var chargeAmount = orderItem.GetOrderChargeDiscounts().Where(cd => (int)cd.Category > 5).Sum(cd => cd.Amount);
            decimal discountAmount = orderItem.CalculateDiscounts();

            decimal couponTotalDiscount = 0;
            if (orderItem.OrderChargeDiscounts.Any(r => !r.IsDeleted && r.Category == ChargeDiscountCategory.Coupon))
            {
                foreach (var coupon in orderItem.GetOrderChargeDiscounts().Where(cd => cd.Category == ChargeDiscountCategory.Coupon))
                {
                    if (coupon.Coupon.CouponCalculateType == CalculationType.OnlyTuition)
                    {
                        discountAmount += Math.Abs(coupon.Amount);
                    }
                    else
                    {
                        couponTotalDiscount += Math.Abs(coupon.Amount);
                    }
                }
            }


            if (orderItem.ProgramSchedule != null && orderItem.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Calendar)
            {
                result = orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category > 0).Sum(c => c.Amount) - Math.Abs(orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category < 0).Sum(c => c.Amount));
            }
            else
            {
                result = (orderItem.EntryFee - discountAmount + chargeAmount) - couponTotalDiscount;
            }

            if (result < 0 && !realResultIfNegative)
            {
                result = 0;
            }

            return result;
        }

        public OperationStatus Cancel(long orderItemId, int userId, string cancelMemo, decimal refundFee, decimal prorateFee)
        {
            var orderSessions = _orderSessionBusiness.GetOrderItemSessions(orderItemId);

            if (orderSessions.Any())
            {
                var remainingOrderSessions = orderSessions.Where(s => s.ProgramSession.StartDateTime > DateTime.UtcNow);
                _dataContext.Set<OrderSession>().RemoveRange(remainingOrderSessions);
            }
            var orderItem = GetItem(orderItemId);
            var isTestMode = !orderItem.Order.IsLive;

            var historyDescription = string.Empty;
            var date = DateTime.UtcNow;

            historyDescription = string.Format(Constants.Order_History_Cancel_Desc,
               orderItem.ProgramSchedule.Program.Name, orderItem.EntryFeeName, orderItem.EntryFee.ToString("C2", CultureInfo.CurrentCulture));

            historyDescription += string.Format(" Memo: {0}", cancelMemo);

            orderItem.Order.OrderHistories.Add(
               new OrderHistory
               {
                   Action = OrderAction.Canceled,
                   ActionDate = date,
                   Description = historyDescription,
                   UserId = userId,
                   OrderItemId = orderItemId
               });

            orderItem.ItemStatus = OrderItemStatusCategories.changed;

            orderItem.ItemStatusReason = OrderItemStatusReasons.canceled;

            if (orderItem.Installments != null && orderItem.Installments.Any())
            {

                var pendingTrans = orderItem.Installments.Where(c => c.PendingPreapprovalTransactions != null && c.PendingPreapprovalTransactions.Count > 0).SelectMany(c => c.PendingPreapprovalTransactions);
                if (pendingTrans != null && pendingTrans.Any())
                {
                    foreach (var tran in pendingTrans)
                    {
                        switch (tran.Status)
                        {
                            case PendingPreapprovalTransactionStatus.Completed:
                                break;
                            case PendingPreapprovalTransactionStatus.PartiallyCompleted:
                                tran.DueAmount = tran.PaidAmount;
                                tran.Status = PendingPreapprovalTransactionStatus.Completed;
                                break;
                            default:
                                tran.Status = PendingPreapprovalTransactionStatus.Deleted;
                                break;
                        }

                    }
                }
            }

            var currentUser = _userProfileBusienss.Get(userId);

            if (orderItem.Installments != null)
            {
                var normalInstallments = orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted).ToList();
                _orderInstallmentBusiness.SetStatusDeletedInstallment(normalInstallments, DeleteReason.Cancel);

                var enabledRefundAutomatically = _clubSettingBusiness.GetRefundAutomatically(orderItem.Order.ClubId);

                if (normalInstallments.Any(i => i.PaidAmount > 0) && enabledRefundAutomatically)
                {
                    SetCancellationProrateFeeToInstallments(orderItem.Installments, currentUser.UserName);
                }

            }


            DeleteChargesAndUpdateTotalAmount(orderItem, refundFee, prorateFee);

            OperationStatus result = Update(orderItem);

            if (result.Status)
            {
                _pendingPreapprovalTransactionBusiness.DeleteWhere(c => c.Status == PendingPreapprovalTransactionStatus.Deleted && c.OrderId == orderItem.Order_Id);
            }

            return result;
        }


        private static void DeleteChargesAndUpdateTotalAmount(OrderItem orderItem, decimal cancellationFee, decimal prorateFee)
        {
            var cancelAmount = cancellationFee + prorateFee;
            orderItem.OrderChargeDiscounts.Where(i => i.Category != ChargeDiscountCategory.CancellationFee && i.Category != ChargeDiscountCategory.ProrateFee).ToList().ForEach(c => c.IsDeleted = true);
            orderItem.TotalAmount = cancelAmount;
            orderItem.EntryFee = 0;
            orderItem.Order.OrderAmount = orderItem.Order.CalculateOrderAmount;

        }

        public OperationStatus StopInatallmentsPayment(int id)
        {
            var orderItem = GetList().Single(o => o.Id == id);

            orderItem.PaymentPlanStatus = PaymentPlanStatus.Stopped;
            orderItem.PaymentPlanStatusUpdateDate = DateTime.UtcNow;

            return _orderItemRepository.Update(orderItem);
        }

        public OperationStatus ResumeInatallmentsPayment(int id)
        {
            var orderItem = GetList().Single(o => o.Id == id);

            orderItem.PaymentPlanStatus = PaymentPlanStatus.Started;
            orderItem.PaymentPlanStatusUpdateDate = DateTime.UtcNow;

            return _orderItemRepository.Update(orderItem);
        }

        public void InitiateOrderItem(OrderItem orderItem, bool hasWaiver = false, bool hasPaymentPlant = false, bool hasFollowUpForm = false)
        {
            if (hasFollowUpForm && orderItem.LastStep == RegistrationStep.Step3)
            {
                orderItem.ItemStatus = OrderItemStatusCategories.notInitiated;
            }
            else if (hasWaiver && orderItem.LastStep == RegistrationStep.Step3)
            {
                orderItem.ItemStatus = OrderItemStatusCategories.notInitiated;
            }
            else if (hasWaiver && orderItem.LastStep == RegistrationStep.Step4)
            {
                orderItem.ItemStatus = OrderItemStatusCategories.notInitiated;
            }
            else if (hasPaymentPlant && orderItem.LastStep == RegistrationStep.Step3)
            {
                orderItem.ItemStatus = OrderItemStatusCategories.notInitiated;
            }
            else if (hasPaymentPlant && orderItem.LastStep == RegistrationStep.Step4)
            {
                orderItem.ItemStatus = OrderItemStatusCategories.notInitiated;
            }
            else if (hasPaymentPlant && orderItem.LastStep == RegistrationStep.Step5)
            {
                orderItem.ItemStatus = OrderItemStatusCategories.notInitiated;
            }
            else
            {
                var isPreregistrationOpenned = _seasonBusiness.IsPreRegistrationMode(orderItem.Season);

                if (isPreregistrationOpenned)
                {
                    orderItem.PreRegistrationStatus = PreRegistrationStatus.Initiated;
                    orderItem.ItemStatus = OrderItemStatusCategories.notInitiated;
                }
                else
                {
                    orderItem.ItemStatus = OrderItemStatusCategories.initiated;
                }
            }
        }

        public OperationStatus UpdateOrderItems(List<OrderItem> orderItems)
        {
            OperationStatus result = new OperationStatus { Status = false };

            return _orderItemRepository.Save();
        }


        public OperationStatus RemoveOrderItemCharges(List<OrderChargeDiscount> charges)
        {
            var result = new OperationStatus();

            _dataContext.Set<OrderChargeDiscount>().RemoveRange(charges);

            return _orderItemRepository.Save();
        }

        public decimal UpdateOrderItemChargeDiscounts(OrderItem orderItem, int installmentCount, DateTime? newDesiredStartDate, List<ProgramSchedulePart> listItemsParts = null, bool installmentIsDeleted = false)
        {
            var installments = orderItem.Installments.Where(i => !i.IsDeleted).ToList();
            var surChargesItem = orderItem.GetOrderChargeDiscounts().Where(s => s.Category == ChargeDiscountCategory.Surcharge || s.Category == ChargeDiscountCategory.PartnerSurcharge).ToList();
            var discounts = orderItem.GetOrderChargeDiscounts().Where(d => d.DiscountId != null).ToList();
            var coupons = orderItem.GetOrderChargeDiscounts().Where(c => c.CouponId != null).ToList();

            var club = orderItem.Order.Club;
            decimal totalChargeDiscountAmount = 0;

            decimal totalDiscount = 0;

            var selectedCharges = _subscriptionBusiness.GetSelectedCharge(orderItem);
            var charges = _programBusiness.GetProgramPartCharges(selectedCharges.Id);

            var itemApplicationFee = orderItem.GetOrderChargeDiscounts().Where(o => o.Category == ChargeDiscountCategory.ApplicationFee).FirstOrDefault();
            var allInstallmentCount = installments.Where(i => i.Type == InstallmentType.Noraml).ToList().Count;

            if (discounts.Count > 0)
            {
                decimal discountAmount = 0;
                foreach (var discount in discounts)
                {
                    var isMulToInstallment = true;

                    if (discount.Discount.AmountType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        foreach (var item in listItemsParts)
                        {
                            decimal percentAmount = 0;
                            var part = orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == item.Id).FirstOrDefault();

                            if (part != null)
                            {
                                percentAmount = (discount.Discount.Amount * part.PartAmount) / 100;

                                part.PartAmount -= (discount.Discount.Amount * part.PartAmount) / 100;
                            }
                            else
                            {
                                decimal amount = 0;
                                if (_subscriptionBusiness.IsProrateProgramPart(item, orderItem.ProgramSchedule, orderItem.DesiredStartDate))
                                {
                                    var selectedDays = orderItem.Attributes.WeekDays;
                                    amount = _subscriptionBusiness.CalculateBeforeAfterCareProrateAmount(item, orderItem.DesiredStartDate.Value, selectedCharges, selectedDays);
                                }
                                else
                                {
                                    amount = charges.Where(c => c.SchedulePartId == item.Id).FirstOrDefault().ChargeAmount;
                                }

                                percentAmount = (discount.Discount.Amount * amount) / 100;

                                totalDiscount += (discount.Discount.Amount * amount) / 100;

                            }

                            isMulToInstallment = false;
                            discountAmount += percentAmount;
                        }
                    }
                    else
                    {
                        discountAmount = Math.Abs(discount.Amount) / allInstallmentCount;
                    }

                    if (!installmentIsDeleted)
                    {
                        orderItem.OrderChargeDiscounts.Where(s => !s.IsDeleted && s.Id == discount.Id).FirstOrDefault().Amount += isMulToInstallment ? -(discountAmount) * installmentCount : -(discountAmount);
                        orderItem.TotalAmount = isMulToInstallment ? orderItem.TotalAmount - (discountAmount * installmentCount) : orderItem.TotalAmount - discountAmount;
                        orderItem.Order.OrderAmount = isMulToInstallment ? orderItem.Order.OrderAmount - (discountAmount * installmentCount) : orderItem.Order.OrderAmount - discountAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? -(discountAmount) * installmentCount : -(discountAmount);
                    }
                    else
                    {
                        orderItem.OrderChargeDiscounts.Where(s => !s.IsDeleted && s.Id == discount.Id).FirstOrDefault().Amount += isMulToInstallment ? (discountAmount) * installmentCount : (discountAmount);
                        totalChargeDiscountAmount += isMulToInstallment ? -(discountAmount) * installmentCount : -(discountAmount);
                    }
                }
            }
            decimal couponAmount = 0;
            if (coupons.Count > 0)
            {
                foreach (var coupon in coupons)
                {
                    var isMulToInstallment = true;
                    if (coupon.Coupon != null)
                    {
                        if (coupon.Coupon.CouponCalculateType == CalculationType.TotalAmount && itemApplicationFee != null)
                        {
                            if (coupon.Coupon.AmounType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                            {
                                foreach (var item in listItemsParts)
                                {
                                    decimal percentAmount = 0;
                                    var part = orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == item.Id).FirstOrDefault();
                                    if (part != null)
                                    {
                                        percentAmount = (coupon.Coupon.Amount * part.PartAmount) / 100;
                                        part.PartAmount -= (coupon.Coupon.Amount * part.PartAmount) / 100;
                                    }
                                    else
                                    {
                                        decimal amount = 0;
                                        if (_subscriptionBusiness.IsProrateProgramPart(item, orderItem.ProgramSchedule, newDesiredStartDate))
                                        {
                                            var selectedDays = orderItem.Attributes.WeekDays;
                                            amount = _subscriptionBusiness.CalculateBeforeAfterCareProrateAmount(item, orderItem.DesiredStartDate.Value, selectedCharges, selectedDays);
                                        }
                                        else
                                        {
                                            amount = charges.Where(c => c.SchedulePartId == item.Id).FirstOrDefault().ChargeAmount;
                                        }
                                        percentAmount = (coupon.Coupon.Amount * amount) / 100;
                                        totalDiscount += (coupon.Coupon.Amount * amount) / 100;
                                    }

                                    isMulToInstallment = false;
                                    couponAmount += percentAmount;
                                }
                            }
                            else
                            {
                                if (coupon.Coupon.AmounType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    var percentApp = (coupon.Coupon.Amount * itemApplicationFee.Amount) / 100;

                                    var couponWithoutApp = Math.Abs(coupon.Amount) - percentApp;

                                    couponAmount = couponWithoutApp / (allInstallmentCount);
                                }
                                else
                                {
                                    couponAmount = Math.Abs(coupon.Amount) / (allInstallmentCount + 1);
                                }
                            }
                        }
                        else
                        {
                            if (coupon.Coupon.AmounType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                            {
                                foreach (var item in listItemsParts)
                                {
                                    decimal percentAmount = 0;
                                    var part = orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == item.Id).FirstOrDefault();
                                    if (part != null)
                                    {
                                        percentAmount = (coupon.Coupon.Amount * part.PartAmount) / 100;
                                        part.PartAmount -= (coupon.Coupon.Amount * part.PartAmount) / 100;
                                    }
                                    else
                                    {
                                        decimal amount = 0;
                                        if (_subscriptionBusiness.IsProrateProgramPart(item, orderItem.ProgramSchedule, newDesiredStartDate))
                                        {
                                            var selectedDays = orderItem.Attributes.WeekDays;
                                            amount = _subscriptionBusiness.CalculateBeforeAfterCareProrateAmount(item, newDesiredStartDate.Value, selectedCharges, selectedDays);
                                        }
                                        else
                                        {
                                            amount = charges.Where(c => c.SchedulePartId == item.Id).FirstOrDefault().ChargeAmount;
                                        }
                                        percentAmount = (coupon.Coupon.Amount * amount) / 100;

                                        totalDiscount += (coupon.Coupon.Amount * amount) / 100;
                                    }

                                    isMulToInstallment = false;
                                    couponAmount += percentAmount;
                                }
                            }
                            else
                            {
                                couponAmount = Math.Abs(coupon.Amount) / allInstallmentCount;
                            }
                        }
                    }

                    if (!installmentIsDeleted)
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == coupon.Id).FirstOrDefault().Amount += isMulToInstallment ? -(couponAmount) * installmentCount : -(couponAmount);
                        orderItem.TotalAmount = isMulToInstallment ? orderItem.TotalAmount - (couponAmount * installmentCount) : orderItem.TotalAmount - couponAmount;
                        orderItem.Order.OrderAmount = isMulToInstallment ? orderItem.Order.OrderAmount - (couponAmount * installmentCount) : orderItem.Order.OrderAmount - couponAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? -(couponAmount) * installmentCount : -(couponAmount);
                    }
                    else
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == coupon.Id).FirstOrDefault().Amount += isMulToInstallment ? couponAmount * installmentCount : couponAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? -(couponAmount) * installmentCount : -(couponAmount);
                    }

                }
            }
            if (surChargesItem.Count > 0)
            {
                Charge clubSurcharge = null;
                Charge partnerSurcharge = null;

                var applicationFee = orderItem.GetOrderChargeDiscounts().Where(a => a.Category == ChargeDiscountCategory.ApplicationFee).FirstOrDefault();
                var applicationFeeAmount = applicationFee != null ? applicationFee.Amount : 0;
                decimal surAmount = 0;
                foreach (var surCharge in surChargesItem)
                {
                    var isMulToInstallment = true;
                    if (surCharge.Category == ChargeDiscountCategory.Surcharge)
                    {
                        if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                        {
                            clubSurcharge = club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);
                            if (clubSurcharge.AmountType == ChargeDiscountType.Fixed)
                            {
                                surAmount = surCharge.Amount / installments.Count;
                            }
                            else
                            {
                                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    var percentSurCharge = clubSurcharge.Amount;
                                    var percentAppFee = (applicationFeeAmount * percentSurCharge) / 100;
                                    surAmount = (surCharge.Amount - percentAppFee) / allInstallmentCount;
                                }
                                else
                                {
                                    foreach (var item in listItemsParts)
                                    {
                                        decimal surchargeApplayAmount = 0;
                                        decimal percentAmount = 0;
                                        var part = orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == item.Id).FirstOrDefault();

                                        if (part != null)
                                        {
                                            percentAmount = (clubSurcharge.Amount * part.PartAmount) / 100;
                                        }
                                        else
                                        {
                                            decimal amount = 0;
                                            if (_subscriptionBusiness.IsProrateProgramPart(item, orderItem.ProgramSchedule, newDesiredStartDate))
                                            {
                                                var selectedDays = orderItem.Attributes.WeekDays;
                                                amount = _subscriptionBusiness.CalculateBeforeAfterCareProrateAmount(item, newDesiredStartDate.Value, selectedCharges, selectedDays);
                                            }
                                            else
                                            {
                                                amount = charges.Where(c => c.SchedulePartId == item.Id).FirstOrDefault().ChargeAmount;
                                            }

                                            surchargeApplayAmount = amount - totalDiscount;

                                            percentAmount = (clubSurcharge.Amount * surchargeApplayAmount) / 100;
                                        }

                                        isMulToInstallment = false;
                                        surAmount += percentAmount;
                                    }
                                }
                            }
                        }
                        else //if club remove surCharge from setting
                        {
                            surAmount = surCharge.Amount / installments.Count;
                        }
                    }
                    else
                    {
                        if (club.PartnerClub != null && club.PartnerClub.Charges != null && club.PartnerClub.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                        {
                            partnerSurcharge = club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);
                            if (partnerSurcharge.AmountType == ChargeDiscountType.Fixed)
                            {
                                surAmount = surCharge.Amount / installments.Count;

                            }
                            else
                            {
                                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    var percentSurCharge = partnerSurcharge.Amount;
                                    var percentAppFee = (applicationFeeAmount * percentSurCharge) / 100;
                                    surAmount = (surCharge.Amount - percentAppFee) / allInstallmentCount;
                                }
                                else
                                {
                                    foreach (var item in listItemsParts)
                                    {
                                        decimal surchargeApplayAmount = 0;
                                        decimal percentAmount = 0;
                                        var part = orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == item.Id).FirstOrDefault();

                                        if (part != null)
                                        {
                                            percentAmount = (partnerSurcharge.Amount * part.PartAmount) / 100;
                                        }
                                        else
                                        {
                                            decimal amount = 0;
                                            if (_subscriptionBusiness.IsProrateProgramPart(item, orderItem.ProgramSchedule, newDesiredStartDate))
                                            {
                                                var selectedDays = orderItem.Attributes.WeekDays;
                                                amount = _subscriptionBusiness.CalculateBeforeAfterCareProrateAmount(item, newDesiredStartDate.Value, selectedCharges, selectedDays);
                                            }
                                            else
                                            {
                                                amount = charges.Where(c => c.SchedulePartId == item.Id).FirstOrDefault().ChargeAmount;
                                            }

                                            surchargeApplayAmount = amount - totalDiscount;
                                            percentAmount = (partnerSurcharge.Amount * amount) / 100;
                                        }

                                        isMulToInstallment = false;
                                        surAmount += percentAmount;
                                    }
                                }
                            }
                        }
                        else //if partner remove surCharge from setting
                        {
                            surAmount = surCharge.Amount / installments.Count;
                        }
                    }

                    if (!installmentIsDeleted)
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == surCharge.Id).FirstOrDefault().Amount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                        orderItem.TotalAmount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                        orderItem.Order.OrderAmount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                    }
                    else
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == surCharge.Id).FirstOrDefault().Amount -= isMulToInstallment ? surAmount * installmentCount : surAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                    }
                }
            }

            return totalChargeDiscountAmount;
        }

        public decimal CalculateOrderItemSurCharge(OrderItem orderItem)
        {
            decimal totalSurCharge = 0;
            var club = orderItem.Order.Club;
            var surChargesItem = orderItem.GetOrderChargeDiscounts().Where(s => s.Category == ChargeDiscountCategory.Surcharge || s.Category == ChargeDiscountCategory.PartnerSurcharge).ToList();
            var allInstallment = orderItem.Installments.Where(i => !i.IsDeleted).ToList();
            var installmentWithOutAppFee = orderItem.Installments.Where(i => !i.IsDeleted && i.Type == InstallmentType.Noraml).ToList();
            decimal surAmount = 0;

            if (surChargesItem.Count > 0)
            {
                Charge clubSurcharge = null;
                Charge partnerSurcharge = null;
                var applicationFee = orderItem.GetOrderChargeDiscounts().Where(a => a.Category == ChargeDiscountCategory.ApplicationFee).FirstOrDefault();
                var applicationFeeAmount = applicationFee != null ? applicationFee.Amount : 0;
                foreach (var surCharge in surChargesItem)
                {
                    if (surCharge.Category == ChargeDiscountCategory.Surcharge)
                    {
                        if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                        {
                            clubSurcharge = club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);
                            if (clubSurcharge.AmountType == ChargeDiscountType.Fixed)
                            {
                                surAmount = surCharge.Amount / allInstallment.Count;
                            }
                            else
                            {
                                var percentSurCharge = clubSurcharge.Amount;
                                var percentAppFee = (applicationFeeAmount * percentSurCharge) / 100;
                                surAmount = (surCharge.Amount - percentAppFee) / installmentWithOutAppFee.Count;
                            }
                        }
                        else //if club remove surCharge from setting
                        {
                            surAmount = surCharge.Amount / allInstallment.Count;
                        }
                    }
                    else
                    {
                        if (club.Charges != null && club.PartnerClub.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                        {
                            partnerSurcharge = club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);
                            if (partnerSurcharge.AmountType == ChargeDiscountType.Fixed)
                            {
                                surAmount = surCharge.Amount / allInstallment.Count;
                            }
                            else
                            {
                                var percentSurCharge = partnerSurcharge.Amount;
                                var percentAppFee = (applicationFeeAmount * percentSurCharge) / 100;
                                surAmount = (surCharge.Amount - percentAppFee) / installmentWithOutAppFee.Count;
                            }
                        }
                        else //if partner remove surCharge from setting
                        {
                            surAmount = surCharge.Amount / allInstallment.Count;
                        }
                    }

                    totalSurCharge += (surAmount * allInstallment.Count);
                }

            }
            return totalSurCharge;
        }

        public IQueryable<OrderItem> GetFSAReport(string filterType, int userId, ref int? year, ref DateTime? startDate, ref DateTime? endDate)
        {

            var orderItems = GetUserOrderItems(userId);

            if (!orderItems.Any())
            {
                return null;
            }

            if (filterType == "Year")
            {
                year = year == 0 ? DateTime.Now.Year : year;

                startDate = new DateTime(year.Value, 1, 1);
                endDate = new DateTime(year.Value + 1, 1, 1).AddDays(-1);
            }
            else if (filterType == "DateRange")
            {
                if (!startDate.HasValue && !endDate.HasValue)
                {
                    startDate = new DateTime(DateTime.Now.Year, 1, 1);
                    endDate = DateTime.Now;
                }
                else if (startDate.HasValue && !endDate.HasValue)
                {
                    startDate = startDate.Value;
                    endDate = DateTime.Now;
                }
                else if (!startDate.HasValue && endDate.HasValue)
                {
                    startDate = new DateTime(DateTime.Now.Year, 1, 1);
                    endDate = endDate.Value;
                }
                else
                {
                    startDate = startDate.Value;
                    endDate = endDate.Value;
                }

            }

            var orderItemIds = orderItems.Select(o => o.Id).ToList();

            orderItems = GetOrderItemByTransactionDate(orderItemIds, startDate.Value, endDate.Value, TransactionStatus.Success);

            return orderItems;
        }

        public List<OrderItemSchedulePart> SetOrderItemsScheduleParts(OrderItem orderItem, DateTime? desiredStartDate, long selectedChargeId = 0)
        {
            var result = new List<OrderItemSchedulePart>();
            if (orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {

                var programParts = _programBusiness.GetScheduleParts(orderItem.ProgramSchedule.ProgramId);

                var programSchedule = orderItem.ProgramSchedule;

                var selectedCharge = _subscriptionBusiness.GetSelectedCharge(programSchedule, selectedChargeId);

                var programPartCharges = selectedCharge.SchdulePartCharges;

                var appliedProgramParts = _subscriptionBusiness.GetAppliedProgramParts(programParts, programSchedule, orderItem.DesiredStartDate);

                orderItem.OrderItemScheduleParts = new List<OrderItemSchedulePart>();

                foreach (var programPart in appliedProgramParts)
                {
                    decimal amount = 0;
                    if (_subscriptionBusiness.IsProrateProgramPart(programPart, programSchedule, orderItem.DesiredStartDate))
                    {
                        var selectedDays = orderItem.Attributes.WeekDays;
                        amount = _subscriptionBusiness.CalculateBeforeAfterCareProrateAmount(programPart, orderItem.DesiredStartDate.Value, selectedCharge, selectedDays);
                    }
                    else
                    {
                        amount = programPartCharges.Where(c => c.SchedulePartId == programPart.Id).FirstOrDefault().ChargeAmount;
                    }

                    orderItem.OrderItemScheduleParts.Add(new OrderItemSchedulePart
                    {
                        ProgramPartId = programPart.Id,
                        PartAmount = amount,

                    });
                }

                result = orderItem.OrderItemScheduleParts.ToList();
            }

            return result;
        }

        public decimal CalculateOneSurCharge(OrderItem orderItem, OrderChargeDiscount surCharge, decimal partAmount = 0)
        {
            decimal surChargeAmount = 0;
            var club = orderItem.Order.Club;
            Charge clubSurcharge = null;
            Charge partnerSurcharge = null;
            var allInstallment = orderItem.Installments.Where(i => !i.IsDeleted).ToList();
            var itemPartCount = orderItem.OrderItemScheduleParts.Where(p => !p.IsDeleted).Count();
            var installmentWithOutAppFee = orderItem.Installments.Where(i => !i.IsDeleted && i.Type == InstallmentType.Noraml).ToList();
            var applicationFee = orderItem.GetOrderChargeDiscounts().Where(a => a.Category == ChargeDiscountCategory.ApplicationFee).FirstOrDefault();
            var applicationFeeAmount = applicationFee != null ? applicationFee.Amount : 0;

            itemPartCount = applicationFee != null ? itemPartCount + 1 : itemPartCount;

            if (surCharge.Category == ChargeDiscountCategory.Surcharge)
            {
                if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                {
                    clubSurcharge = club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);
                    if (clubSurcharge.AmountType == ChargeDiscountType.Fixed)
                    {
                        surChargeAmount = orderItem.PaymentPlanType == PaymentPlanType.Installment ? surCharge.Amount / allInstallment.Count : surCharge.Amount / itemPartCount;
                    }
                    else
                    {
                        var percentSurCharge = clubSurcharge.Amount;

                        if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                        {
                            var percentAppFee = (applicationFeeAmount * percentSurCharge) / 100;
                            surChargeAmount = (surCharge.Amount - percentAppFee) / installmentWithOutAppFee.Count;
                        }
                        else
                        {
                            var amount = partAmount > 0 ? (partAmount * percentSurCharge) / 100 : 0;
                            surChargeAmount = amount;
                        }
                    }
                }
                else //if club remove surCharge from setting
                {
                    surChargeAmount = orderItem.PaymentPlanType == PaymentPlanType.Installment ? surCharge.Amount / allInstallment.Count : surCharge.Amount / itemPartCount;
                }
            }
            else
            {
                if (club.PartnerClub != null && club.PartnerClub.Charges != null && club.PartnerClub.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                {
                    partnerSurcharge = club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);
                    if (partnerSurcharge.AmountType == ChargeDiscountType.Fixed)
                    {
                        surChargeAmount = orderItem.PaymentPlanType == PaymentPlanType.Installment ? surCharge.Amount / allInstallment.Count : surCharge.Amount / itemPartCount;
                    }
                    else
                    {
                        var percentSurCharge = partnerSurcharge.Amount;
                        var percentAppFee = (applicationFeeAmount * percentSurCharge) / 100;
                        surChargeAmount = orderItem.PaymentPlanType == PaymentPlanType.Installment ? (surCharge.Amount - percentAppFee) / installmentWithOutAppFee.Count : (surCharge.Amount - percentAppFee) / itemPartCount;
                    }
                }
                else //if partner remove surCharge from setting
                {
                    surChargeAmount = orderItem.PaymentPlanType == PaymentPlanType.Installment ? surCharge.Amount / allInstallment.Count : surCharge.Amount / itemPartCount;
                }
            }

            return surChargeAmount;

        }

        public decimal calculateDiscountAmount(List<OrderChargeDiscount> discounts, decimal amount)
        {
            decimal result = 0;

            foreach (var discount in discounts)
            {
                if (discount.DiscountId.HasValue)
                {
                    if (discount.Discount.AmountType == ChargeDiscountType.Percent)
                    {
                        result += (discount.Discount.Amount * amount) / 100;
                    }
                    else
                    {
                        result += discount.Discount.Amount;
                    }
                }
                else
                {
                    if (discount.Coupon.AmounType == ChargeDiscountType.Percent)
                    {
                        result += (discount.Coupon.Amount * amount) / 100;
                    }
                    else
                    {
                        result += discount.Coupon.Amount;
                    }
                }
            }

            return result;
        }

        public decimal CalculateSessionAmount(List<OrderChargeDiscount> chargeDiscounts, OrderSession session, int sessionsCount)
        {
            decimal amount = session.Amount.Value;

            foreach (var chargeDiscount in chargeDiscounts)
            {
                if (chargeDiscount.DiscountId.HasValue)
                {
                    if (chargeDiscount.Discount.AmountType == ChargeDiscountType.Percent)
                    {
                        amount -= (chargeDiscount.Discount.Amount * amount) / 100;
                    }
                    else
                    {
                        amount -= chargeDiscount.Discount.Amount / sessionsCount;
                    }
                }
                else
                {
                    if (chargeDiscount.Coupon.AmounType == ChargeDiscountType.Percent)
                    {
                        amount -= (chargeDiscount.Coupon.Amount * amount) / 100;
                    }
                    else
                    {
                        amount -= chargeDiscount.Coupon.Amount / sessionsCount;
                    }
                }
            }

            return Math.Floor(amount * 100) / 100;
        }

        public bool HasTutionOrderItems(Charge charge)
        {
            var items = GetComplatedOrInitiatedOrderitems().Where(i => i.ProgramScheduleId == charge.ProgramScheduleId).SelectMany(i => i.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee && c.ChargeId == charge.Id);
            var chargeHaveOrder = items.Any() ? true : false;

            if (!chargeHaveOrder)
            {
                var DropInItems = GetComplatedOrInitiatedOrderitems().Where(i => i.ProgramScheduleId == charge.ProgramScheduleId && i.Mode == OrderItemMode.DropIn);

                var programsessions = DropInItems.SelectMany(i => i.OrderSessions.Select(o => o.ProgramSession));

                chargeHaveOrder = programsessions.Any(p => p.ChargeId == charge.Id);
            }

            return chargeHaveOrder;
        }

        public List<string> ShowItemSessions(OrderItem orderItem)
        {
            var viewSessions = new List<string>();

            var sessions = orderItem.OrderSessions;

            if (sessions.Any())
            {
                foreach (var programSession in sessions.Select(p => p.ProgramSession).ToList().OrderBy(s => s.StartDateTime.Day))
                {
                    var charge = programSession.Charge;
                    var strSession = string.Format("{0} - {1}", charge.Attributes.DropInName, programSession.StartDateTime.ToString("MMM dd"));
                    viewSessions.Add(strSession);
                }
            }

            return viewSessions;
        }

        public OperationStatus Update(OrderItem orderItem)
        {
            return _orderItemRepository.Update(orderItem);
        }

        public string GetOrderItemConfirmationIdMessage(long programId, List<ProgramSchedulePart> deletedProgramScheduleParts, DateTime newProgramDate, bool editEndDate, bool onlyDropInPunchcard = false)
        {
            var allOrderItems = GetComplatedOrderitems().Where(o => o.ProgramSchedule.ProgramId == programId);
            var confirmationIds = string.Empty;
            var dropInPunchcardMessage = string.Empty;
            var normalOrdersToBeUpdated = new List<OrderItem>();
            var dropInPunchcardOrdersToBeUpdated = new List<OrderItem>();
            var errorMessage = string.Empty;

            var programPartIds = deletedProgramScheduleParts.Select(p => p.Id).ToList();

            if (allOrderItems.Any())
            {
                var installmentOrders = allOrderItems.Where(o => o.PaymentPlanType == PaymentPlanType.Installment);
                var dropInPunchcardOrders = allOrderItems.Where(o => o.Mode != OrderItemMode.Noraml);
                var paidInFullOrders = allOrderItems.Where(o => o.PaymentPlanType == PaymentPlanType.FullPaid && o.Mode == OrderItemMode.Noraml);
                var orderItemIds = new List<long>();

                if (installmentOrders.Any() && !onlyDropInPunchcard)
                {
                    var orderItemInstallments = installmentOrders.SelectMany(o => o.Installments).Where(i => !i.IsDeleted && i.ProgramSchedulePartId.HasValue && programPartIds.Contains(i.ProgramSchedulePartId.Value)).ToList();
                    orderItemIds = orderItemInstallments.Select(o => o.OrderItemId).ToList();
                    normalOrdersToBeUpdated.AddRange(installmentOrders.Where(i => orderItemIds.Contains(i.Id)));
                }

                if (paidInFullOrders.Any() && !onlyDropInPunchcard)
                {
                    var orderItemScheduleParts = paidInFullOrders.SelectMany(o => o.OrderItemScheduleParts).Where(i => !i.IsDeleted && programPartIds.Contains(i.ProgramPartId.Value)).ToList();
                    orderItemIds = orderItemScheduleParts.Select(o => o.OrderItemId).ToList();
                    normalOrdersToBeUpdated.AddRange(paidInFullOrders.Where(i => orderItemIds.Contains(i.Id)));
                }

                if (dropInPunchcardOrders.Any())
                {
                    IEnumerable<OrderSession> dropInPunchcardSessions;
                    dropInPunchcardSessions = editEndDate ? dropInPunchcardOrders.SelectMany(o => o.OrderSessions).ToList().Where(p => p.ProgramSession.StartDateTime.Date > newProgramDate.Date)
                        : dropInPunchcardOrders.SelectMany(o => o.OrderSessions).ToList().Where(p => p.ProgramSession.StartDateTime.Date < newProgramDate.Date);

                    orderItemIds = dropInPunchcardSessions.Select(o => o.OrderItemId).ToList();
                    dropInPunchcardOrdersToBeUpdated.AddRange(dropInPunchcardOrders.Where(i => orderItemIds.Contains(i.Id)));
                }

                var normalConfirmationIds = normalOrdersToBeUpdated.Select(i => i.Order.ConfirmationId).Distinct();
                var dropInPunchcardConfirmationIds = dropInPunchcardOrdersToBeUpdated.Select(i => i.Order.ConfirmationId).Distinct();

                confirmationIds = string.Join(", ", normalConfirmationIds);
                dropInPunchcardMessage = string.Join(", ", dropInPunchcardConfirmationIds);
            }


            if (editEndDate && (!string.IsNullOrWhiteSpace(confirmationIds) || !string.IsNullOrWhiteSpace(dropInPunchcardMessage)))
            {
                if (!onlyDropInPunchcard)
                {
                    var scheduleDates = new List<string>();
                    foreach (var item in deletedProgramScheduleParts)
                    {
                        scheduleDates.Add($"{item.StartDate.Date.ToString(Constants.DefaultDateFormat)}-{item.EndDate.Date.ToString(Constants.DefaultDateFormat)}");
                    }

                    var deletedSchedules = string.Join(", ", scheduleDates);

                    errorMessage = Jumbula.Common.Constants.Messages.EditEndDateProgramForSubscriptionOrders + "</br> Schedules " + "<" + deletedSchedules + ">";

                    if (!string.IsNullOrWhiteSpace(confirmationIds))
                    {
                        errorMessage = errorMessage + " must be deleted from these orders: " + confirmationIds;
                    }
                    if (!string.IsNullOrWhiteSpace(dropInPunchcardMessage))
                    {
                        errorMessage = errorMessage + "</br> The following orders are the affected drop-in and punch card orders: " + "<" + dropInPunchcardMessage + ">";
                    }
                }
                else
                {
                    errorMessage = Jumbula.Common.Constants.Messages.EditEndDateProgramForDorpInPunchcard + "</br>" + "<" + dropInPunchcardMessage + ">";
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(confirmationIds) || !string.IsNullOrWhiteSpace(dropInPunchcardMessage))
                {
                    if (!string.IsNullOrWhiteSpace(confirmationIds) && !string.IsNullOrWhiteSpace(confirmationIds))
                    {
                        errorMessage = Jumbula.Common.Constants.Messages.EditStartDateProgramForSubscriptionOrders + confirmationIds;
                        errorMessage = errorMessage + "</br> The following orders are the affected drop-in and punch cards: " + "<" + dropInPunchcardMessage + ">";
                    }

                    if ((!string.IsNullOrWhiteSpace(dropInPunchcardMessage) && string.IsNullOrWhiteSpace(confirmationIds)) || onlyDropInPunchcard)
                    {
                        errorMessage = Jumbula.Common.Constants.Messages.EditEndDateProgramForDorpInPunchcard + "</br>" + "<" + dropInPunchcardMessage + ">";
                    }
                    else if (string.IsNullOrWhiteSpace(dropInPunchcardMessage) && !string.IsNullOrWhiteSpace(confirmationIds))
                    {
                        errorMessage = Jumbula.Common.Constants.Messages.EditStartDateProgramForSubscriptionOrders + confirmationIds;
                    }
                }
            }

            return errorMessage;
        }

        public void UpdateOrderItemSchedulePart(OrderItem orderItem, ProgramSchedulePart schedulePart, List<OrderItemSchedulePartModel> orderParts, decimal newScheduleAmount, bool hasAddedInstallment = false, int deletedInstallment = 0)
        {
            var newEntryFee = newScheduleAmount;
            var schedulePartAmount = orderParts.FirstOrDefault(o => o.PartId == schedulePart.Id).Amount;
            var mainScheduleAmount = orderItem.OrderItemScheduleParts.FirstOrDefault(o => o.ProgramPartId == schedulePart.Id).PartAmount;

            var draftOrderChargeDiscount = new List<OrderChargeDiscount>();

            draftOrderChargeDiscount = orderItem.GetOrderChargeDiscounts().Where(o => o.Category != ChargeDiscountCategory.EntryFee && (o.Category != ChargeDiscountCategory.ApplicationFee && o.Category != ChargeDiscountCategory.CancellationFee && o.Category != ChargeDiscountCategory.EditFee && o.Category != ChargeDiscountCategory.TransferFee && o.Category != ChargeDiscountCategory.ProrateFee)).OrderByDescending(c => c.Subcategory).Select(c => new OrderChargeDiscount
            {
                Amount = c.Amount,
                Id = c.Id,
                OrderItem = c.OrderItem,
                OrderItemId = c.OrderItemId,
                Subcategory = c.Subcategory,
                Category = c.Category
            }).ToList();

            orderItem.EntryFee -= schedulePartAmount;
            orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.EntryFee).FirstOrDefault().Amount -= schedulePartAmount;

            foreach (var item in orderItem.GetOrderChargeDiscounts().Where(o => o.Category != ChargeDiscountCategory.EntryFee && (o.Category != ChargeDiscountCategory.ApplicationFee && o.Category != ChargeDiscountCategory.CancellationFee && o.Category != ChargeDiscountCategory.EditFee && o.Category != ChargeDiscountCategory.TransferFee && o.Category != ChargeDiscountCategory.ProrateFee)).OrderByDescending(c => c.Subcategory))
            {
                var chargeDiscountAmount = CalculatScheduleChargeDiscount(item, schedulePartAmount, hasAddedInstallment, deletedInstallment);

                if (item.Subcategory == ChargeDiscountSubcategory.Discount)
                {
                    item.Amount += chargeDiscountAmount;
                    mainScheduleAmount -= chargeDiscountAmount;
                }
                else
                {
                    item.Amount -= chargeDiscountAmount;
                    orderItem.TotalAmount -= chargeDiscountAmount;
                    orderItem.Order.OrderAmount -= chargeDiscountAmount;
                }
            }

            orderItem.TotalAmount -= mainScheduleAmount;
            orderItem.Order.OrderAmount -= mainScheduleAmount;
            orderItem.OrderItemScheduleParts.FirstOrDefault(o => o.ProgramPartId == schedulePart.Id).PartAmount = newScheduleAmount;

            foreach (var item in orderItem.GetOrderChargeDiscounts().Where(o => o.Category != ChargeDiscountCategory.EntryFee && (o.Category != ChargeDiscountCategory.ApplicationFee && o.Category != ChargeDiscountCategory.CancellationFee && o.Category != ChargeDiscountCategory.EditFee && o.Category != ChargeDiscountCategory.TransferFee && o.Category != ChargeDiscountCategory.ProrateFee)).OrderByDescending(c => c.Subcategory))
            {
                decimal chargeDiscountAmount = 0;
                if (item.Category == ChargeDiscountCategory.CustomCharge || item.Category == ChargeDiscountCategory.CustomDiscount)
                {
                    foreach (var chargeDiscount in draftOrderChargeDiscount)
                    {
                        if (item.Id == chargeDiscount.Id)
                        {
                            chargeDiscountAmount = CalculatScheduleChargeDiscount(chargeDiscount, schedulePartAmount, hasAddedInstallment, deletedInstallment);
                            break;
                        }
                    }
                }
                else
                {
                    chargeDiscountAmount = CalculatScheduleChargeDiscount(item, schedulePartAmount, hasAddedInstallment, deletedInstallment);
                }

                if (item.Subcategory == ChargeDiscountSubcategory.Discount)
                {
                    item.Amount -= chargeDiscountAmount;
                    orderItem.TotalAmount -= chargeDiscountAmount;
                    orderItem.Order.OrderAmount -= chargeDiscountAmount;
                    newScheduleAmount -= chargeDiscountAmount;
                }
                else
                {
                    item.Amount += chargeDiscountAmount;
                    orderItem.TotalAmount += chargeDiscountAmount;
                    orderItem.Order.OrderAmount += chargeDiscountAmount;
                }
            }

            orderItem.EntryFee += newEntryFee;
            orderItem.TotalAmount += newEntryFee;
            orderItem.Order.OrderAmount += newEntryFee;
            orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.EntryFee).FirstOrDefault().Amount += newEntryFee;
        }

        public decimal CalculatScheduleChargeDiscount(OrderChargeDiscount chargeDiscount, decimal amount, bool hasAddedInstallment = false, int countInstallment = 0)
        {
            decimal result = 0;

            var club = chargeDiscount.OrderItem.Order.Club;
            int installmentCount = 0;

            if (chargeDiscount.OrderItem.PaymentPlanType == PaymentPlanType.Installment)
            {
                var installments = chargeDiscount.OrderItem.Installments.Where(i => !i.IsDeleted && i.Type == InstallmentType.Noraml).ToList();
                installmentCount = hasAddedInstallment ? installments.Count + countInstallment : installments.Count - countInstallment;
            }
            else
            {
                var installments = chargeDiscount.OrderItem.OrderItemScheduleParts.Where(i => !i.IsDeleted).ToList();
                installmentCount = hasAddedInstallment ? installments.Count + countInstallment : installments.Count - countInstallment;
            }

            if (chargeDiscount.Subcategory == ChargeDiscountSubcategory.Discount)
            {
                if (chargeDiscount.DiscountId.HasValue)
                {
                    if (chargeDiscount.Discount.AmountType == ChargeDiscountType.Fixed)
                    {
                        result = chargeDiscount.Discount.Amount;
                    }
                    else
                    {
                        result = (amount * chargeDiscount.Discount.Amount) / 100;
                    }
                }
                else if (chargeDiscount.CouponId.HasValue)
                {
                    if (chargeDiscount.Coupon.AmounType == ChargeDiscountType.Fixed)
                    {
                        result = chargeDiscount.Coupon.Amount;
                    }
                    else
                    {
                        result = (amount * chargeDiscount.Coupon.Amount) / 100;
                    }
                }
                else
                {
                    result = Math.Abs(chargeDiscount.Amount / installmentCount);
                }
            }
            else
            {
                if (chargeDiscount.ChargeId.HasValue && chargeDiscount.Category == ChargeDiscountCategory.Surcharge)
                {
                    if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                    {
                        var clubSurcharge = club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);
                        if (clubSurcharge.AmountType == ChargeDiscountType.Fixed)
                        {
                            result = clubSurcharge.Amount;
                        }
                        else
                        {
                            result = (amount * clubSurcharge.Amount) / 100;
                        }
                    }
                    else
                    {
                        result = chargeDiscount.Amount / installmentCount;
                    }
                }
                else if (chargeDiscount.ChargeId.HasValue && chargeDiscount.Category == ChargeDiscountCategory.PartnerSurcharge)
                {
                    if (club.PartnerClub != null && club.PartnerClub.Charges != null && club.PartnerClub.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                    {
                        var partnerSurcharge = club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);
                        if (partnerSurcharge.AmountType == ChargeDiscountType.Fixed)
                        {
                            result = partnerSurcharge.Amount;
                        }
                        else
                        {
                            result = (amount * partnerSurcharge.Amount) / 100;
                        }
                    }
                    else
                    {
                        result = chargeDiscount.Amount / installmentCount;
                    }
                }
                else
                {
                    result = chargeDiscount.Amount / installmentCount;
                }
            }

            return (result * 100) / 100;
        }

        public void updateInstallmentAmount(OrderItem orderItem, ProgramSchedulePart installmentPart, List<OrderItemSchedulePartModel> orderParts, decimal newAmount, bool hasAddedInstallment = false, int deletedInstallment = 0)
        {
            var newEntryFee = newAmount;
            var oldInstallmentByDesiredStartDate = orderItem.Installments.FirstOrDefault(i => i.Type == InstallmentType.Noraml && !i.IsDeleted && i.ProgramSchedulePartId == installmentPart.Id);

            var oldInstallmentAmount = oldInstallmentByDesiredStartDate.Amount;
            var mainInstallmentAmount = orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == installmentPart.Id).FirstOrDefault().PartAmount;
            var installmentPartAmount = orderParts.Where(o => o.PartId == installmentPart.Id).FirstOrDefault().Amount;

            orderItem.EntryFee -= installmentPartAmount;
            orderItem.TotalAmount -= mainInstallmentAmount;
            orderItem.Order.OrderAmount -= mainInstallmentAmount;
            orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.EntryFee).FirstOrDefault().Amount -= installmentPartAmount;

            var draftOrderChargeDiscount = new List<OrderChargeDiscount>();

            draftOrderChargeDiscount = orderItem.GetOrderChargeDiscounts().Where(o => o.Category != ChargeDiscountCategory.EntryFee && (o.Category != ChargeDiscountCategory.ApplicationFee && o.Category != ChargeDiscountCategory.CancellationFee && o.Category != ChargeDiscountCategory.EditFee && o.Category != ChargeDiscountCategory.TransferFee && o.Category != ChargeDiscountCategory.ProrateFee)).OrderByDescending(c => c.Subcategory).Select(c => new OrderChargeDiscount
            {
                Amount = c.Amount,
                Id = c.Id,
                OrderItem = c.OrderItem,
                OrderItemId = c.OrderItemId,
                Subcategory = c.Subcategory,
                Category = c.Category
            }).ToList();

            foreach (var item in orderItem.GetOrderChargeDiscounts().Where(o => o.Category != ChargeDiscountCategory.EntryFee && (o.Category != ChargeDiscountCategory.ApplicationFee && o.Category != ChargeDiscountCategory.CancellationFee && o.Category != ChargeDiscountCategory.EditFee && o.Category != ChargeDiscountCategory.TransferFee && o.Category != ChargeDiscountCategory.ProrateFee)).OrderByDescending(c => c.Subcategory))
            {
                var chargeDiscountAmount = CalculatScheduleChargeDiscount(item, installmentPartAmount, hasAddedInstallment, deletedInstallment);

                if (item.Subcategory == ChargeDiscountSubcategory.Discount)
                {
                    item.Amount += chargeDiscountAmount;
                    orderItem.TotalAmount += chargeDiscountAmount;
                    orderItem.Order.OrderAmount += chargeDiscountAmount;
                    mainInstallmentAmount -= chargeDiscountAmount;
                }
                else
                {
                    item.Amount -= chargeDiscountAmount;
                    orderItem.TotalAmount -= chargeDiscountAmount;
                    orderItem.Order.OrderAmount -= chargeDiscountAmount;
                }
            }

            //update installment
            orderItem.OrderItemScheduleParts.Where(o => o.ProgramPartId == installmentPart.Id).FirstOrDefault().PartAmount = newAmount;
            oldInstallmentByDesiredStartDate.Amount = newAmount;

            foreach (var item in orderItem.GetOrderChargeDiscounts().Where(o => o.Category != ChargeDiscountCategory.EntryFee && (o.Category != ChargeDiscountCategory.ApplicationFee && o.Category != ChargeDiscountCategory.CancellationFee && o.Category != ChargeDiscountCategory.EditFee && o.Category != ChargeDiscountCategory.TransferFee && o.Category != ChargeDiscountCategory.ProrateFee)).OrderByDescending(c => c.Subcategory))
            {
                decimal chargeDiscountAmount = 0;
                if (item.Category == ChargeDiscountCategory.CustomCharge || item.Category == ChargeDiscountCategory.CustomDiscount)
                {
                    foreach (var chargeDiscount in draftOrderChargeDiscount)
                    {
                        if (item.Id == chargeDiscount.Id)
                        {
                            chargeDiscountAmount = CalculatScheduleChargeDiscount(chargeDiscount, newAmount, hasAddedInstallment, deletedInstallment);
                            break;
                        }
                    }
                }
                else
                {
                    chargeDiscountAmount = CalculatScheduleChargeDiscount(item, newAmount, hasAddedInstallment, deletedInstallment);
                }

                if (item.Subcategory == ChargeDiscountSubcategory.Discount)
                {
                    item.Amount -= chargeDiscountAmount;
                    orderItem.TotalAmount -= chargeDiscountAmount;
                    orderItem.Order.OrderAmount -= chargeDiscountAmount;
                    oldInstallmentByDesiredStartDate.Amount -= chargeDiscountAmount;
                    newAmount -= chargeDiscountAmount;
                }
                else
                {
                    item.Amount += chargeDiscountAmount;
                    orderItem.TotalAmount += chargeDiscountAmount;
                    orderItem.Order.OrderAmount += chargeDiscountAmount;
                    oldInstallmentByDesiredStartDate.Amount += chargeDiscountAmount;
                }
            }

            orderItem.EntryFee += newEntryFee;
            orderItem.TotalAmount += newEntryFee;
            orderItem.Order.OrderAmount += newEntryFee;
            orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.EntryFee).FirstOrDefault().Amount += newEntryFee;
        }

        public decimal GetProgramSchedulePartAmount(ProgramSchedulePart schedulePart, ProgramSchedule schedule, Charge selectedCharge, List<DayOfWeek> days, DateTime? desiredStartDate)
        {
            decimal result = 0;
            var schedulePartcharges = _programBusiness.GetProgramPartCharges(selectedCharge.Id);

            if (_subscriptionBusiness.IsProrateProgramPart(schedulePart, schedule, desiredStartDate))
            {
                result = _subscriptionBusiness.CalculateBeforeAfterCareProrateAmount(schedulePart, desiredStartDate.Value, selectedCharge, days);
                result = Math.Floor(result * 100) / 100;
            }
            else
            {
                result = schedulePartcharges.FirstOrDefault(c => c.SchedulePartId == schedulePart.Id).ChargeAmount;
            }

            return result;
        }

        public decimal UpdateOrderItemChargeDiscounts(OrderItem orderItem, int installmentCount, DateTime? newDesiredStartDate, List<OrderItemSchedulePartModel> orderItemScheduleParts = null, List<ProgramSchedulePart> listItemsParts = null, bool installmentIsDeleted = false)
        {
            // var installments = new List<OrderInstallment>();
            var countInastallmentPart = 0;
            var allInstallmentPartCount = 0;
            var allCharges = orderItem.GetOrderChargeDiscounts().Where(s => s.Subcategory == ChargeDiscountSubcategory.Charge && s.Category != ChargeDiscountCategory.EntryFee && s.Category != ChargeDiscountCategory.ApplicationFee && s.Category != ChargeDiscountCategory.CancellationFee && s.Category != ChargeDiscountCategory.EditFee && s.Category != ChargeDiscountCategory.TransferFee && s.Category != ChargeDiscountCategory.ProrateFee).ToList();
            var discounts = orderItem.GetOrderChargeDiscounts().Where(d => d.DiscountId != null || d.Category == ChargeDiscountCategory.CustomDiscount).ToList();
            var coupons = orderItem.GetOrderChargeDiscounts().Where(c => c.CouponId != null).ToList();

            var club = orderItem.Order.Club;
            decimal totalChargeDiscountAmount = 0;

            decimal totalDiscount = 0;

            var selectedCharges = _subscriptionBusiness.GetSelectedCharge(orderItem);
            var charges = _programBusiness.GetProgramPartCharges(selectedCharges.Id);

            var itemApplicationFee = orderItem.OrderChargeDiscounts.FirstOrDefault(o => !o.IsDeleted && o.Category == ChargeDiscountCategory.ApplicationFee || o.Category == ChargeDiscountCategory.PartnerApplicationFee);
            if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
            {
                var installments = orderItem.Installments.Where(i => !i.IsDeleted).ToList();
                allInstallmentPartCount = installments.Where(i => i.Type == InstallmentType.Noraml).ToList().Count;
                countInastallmentPart = installments.Count;
            }
            else
            {
                var orderScheduleParts = orderItem.OrderItemScheduleParts.Where(p => !p.IsDeleted).ToList();
                countInastallmentPart = itemApplicationFee != null ? orderScheduleParts.Count + 1 : orderScheduleParts.Count;
                allInstallmentPartCount = orderScheduleParts.Count;
            }


            if (discounts.Count > 0)
            {
                decimal discountAmount = 0;
                foreach (var discount in discounts)
                {
                    var isMulToInstallment = true;

                    if (discount.DiscountId.HasValue && discount.Discount.AmountType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        foreach (var item in listItemsParts)
                        {
                            decimal percentAmount = 0;
                            var part = orderItem.OrderItemScheduleParts.Where(o => !o.IsDeleted && o.ProgramPartId == item.Id).FirstOrDefault();
                            var mainAmount = orderItemScheduleParts.Any(o => o.PartId == item.Id) ? orderItemScheduleParts.FirstOrDefault(o => o.PartId == item.Id).Amount : 0;

                            if (part != null)
                            {
                                percentAmount = (discount.Discount.Amount * mainAmount) / 100;

                                part.PartAmount -= (discount.Discount.Amount * mainAmount) / 100;
                            }
                            else
                            {
                                var selectedDays = orderItem.Attributes.WeekDays;
                                decimal amount = GetProgramSchedulePartAmount(item, orderItem.ProgramSchedule, selectedCharges, selectedDays, newDesiredStartDate);

                                percentAmount = (discount.Discount.Amount * amount) / 100;

                                totalDiscount += (discount.Discount.Amount * amount) / 100;
                            }

                            isMulToInstallment = false;
                            discountAmount += percentAmount;
                        }
                    }
                    else
                    {
                        discountAmount = Math.Abs(discount.Amount) / allInstallmentPartCount;
                    }

                    if (!installmentIsDeleted)
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == discount.Id).FirstOrDefault().Amount += isMulToInstallment ? -(discountAmount) * installmentCount : -(discountAmount);
                        orderItem.TotalAmount = isMulToInstallment ? orderItem.TotalAmount - (discountAmount * installmentCount) : orderItem.TotalAmount - discountAmount;
                        orderItem.Order.OrderAmount = isMulToInstallment ? orderItem.Order.OrderAmount - (discountAmount * installmentCount) : orderItem.Order.OrderAmount - discountAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? -(discountAmount) * installmentCount : -(discountAmount);
                    }
                    else
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == discount.Id).FirstOrDefault().Amount += isMulToInstallment ? (discountAmount) * installmentCount : (discountAmount);
                        totalChargeDiscountAmount += isMulToInstallment ? -(discountAmount) * installmentCount : -(discountAmount);
                    }
                }
            }
            decimal couponAmount = 0;
            if (coupons.Count > 0)
            {
                foreach (var coupon in coupons)
                {
                    var isMulToInstallment = true;
                    if (coupon.Coupon != null)
                    {
                        if (coupon.Coupon.CouponCalculateType == CalculationType.TotalAmount && itemApplicationFee != null)
                        {
                            if (coupon.Coupon.AmounType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                            {
                                foreach (var item in listItemsParts)
                                {
                                    decimal percentAmount = 0;
                                    var part = orderItem.OrderItemScheduleParts.Where(o => !o.IsDeleted && o.ProgramPartId == item.Id).FirstOrDefault();
                                    var mainAmount = orderItemScheduleParts.Any(o => o.PartId == item.Id) ? orderItemScheduleParts.FirstOrDefault(o => o.PartId == item.Id).Amount : 0;

                                    if (part != null)
                                    {
                                        percentAmount = (coupon.Coupon.Amount * mainAmount) / 100;
                                        part.PartAmount -= (coupon.Coupon.Amount * mainAmount) / 100;
                                    }
                                    else
                                    {
                                        var selectedDays = orderItem.Attributes.WeekDays;
                                        decimal amount = GetProgramSchedulePartAmount(item, orderItem.ProgramSchedule, selectedCharges, selectedDays, newDesiredStartDate);

                                        percentAmount = (coupon.Coupon.Amount * amount) / 100;
                                        totalDiscount += (coupon.Coupon.Amount * amount) / 100;
                                    }

                                    isMulToInstallment = false;
                                    couponAmount += percentAmount;
                                }
                            }
                            else
                            {
                                if (coupon.Coupon.AmounType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    var percentApp = (coupon.Coupon.Amount * itemApplicationFee.Amount) / 100;

                                    var couponWithoutApp = Math.Abs(coupon.Amount) - percentApp;

                                    couponAmount = couponWithoutApp / (allInstallmentPartCount);
                                }
                                else
                                {
                                    couponAmount = Math.Abs(coupon.Amount) / (allInstallmentPartCount + 1);
                                }
                            }
                        }
                        else
                        {
                            if (coupon.Coupon.AmounType == ChargeDiscountType.Percent && orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                            {
                                foreach (var item in listItemsParts)
                                {
                                    decimal percentAmount = 0;
                                    var part = orderItem.OrderItemScheduleParts.Where(o => !o.IsDeleted && o.ProgramPartId == item.Id).FirstOrDefault();
                                    var mainAmount = orderItemScheduleParts.Any(o => o.PartId == item.Id) ? orderItemScheduleParts.FirstOrDefault(o => o.PartId == item.Id).Amount : 0;

                                    if (part != null)
                                    {
                                        percentAmount = (coupon.Coupon.Amount * mainAmount) / 100;
                                        part.PartAmount -= (coupon.Coupon.Amount * mainAmount) / 100;
                                    }
                                    else
                                    {
                                        var selectedDays = orderItem.Attributes.WeekDays;
                                        decimal amount = GetProgramSchedulePartAmount(item, orderItem.ProgramSchedule, selectedCharges, selectedDays, newDesiredStartDate);

                                        percentAmount = (coupon.Coupon.Amount * amount) / 100;

                                        totalDiscount += (coupon.Coupon.Amount * amount) / 100;
                                    }

                                    isMulToInstallment = false;
                                    couponAmount += percentAmount;
                                }
                            }
                            else
                            {
                                couponAmount = Math.Abs(coupon.Amount) / allInstallmentPartCount;
                            }
                        }
                    }

                    if (!installmentIsDeleted)
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == coupon.Id).FirstOrDefault().Amount += isMulToInstallment ? -(couponAmount) * installmentCount : -(couponAmount);
                        orderItem.TotalAmount = isMulToInstallment ? orderItem.TotalAmount - (couponAmount * installmentCount) : orderItem.TotalAmount - couponAmount;
                        orderItem.Order.OrderAmount = isMulToInstallment ? orderItem.Order.OrderAmount - (couponAmount * installmentCount) : orderItem.Order.OrderAmount - couponAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? -(couponAmount) * installmentCount : -(couponAmount);
                    }
                    else
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == coupon.Id).FirstOrDefault().Amount += isMulToInstallment ? couponAmount * installmentCount : couponAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? -(couponAmount) * installmentCount : -(couponAmount);
                    }
                }
            }
            if (allCharges.Count > 0)
            {
                Charge clubSurcharge = null;
                Charge partnerSurcharge = null;

                var applicationFee = orderItem.GetOrderChargeDiscounts().Where(a => a.Category == ChargeDiscountCategory.ApplicationFee).FirstOrDefault();
                var applicationFeeAmount = applicationFee != null ? applicationFee.Amount : 0;
                decimal surAmount = 0;
                foreach (var charge in allCharges)
                {
                    var isMulToInstallment = true;
                    if (charge.Category == ChargeDiscountCategory.Surcharge)
                    {
                        if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge))
                        {
                            clubSurcharge = club.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge);
                            if (clubSurcharge.AmountType == ChargeDiscountType.Fixed)
                            {
                                surAmount = charge.Amount / countInastallmentPart;
                            }
                            else
                            {
                                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    var percentSurCharge = clubSurcharge.Amount;
                                    var percentAppFee = (applicationFeeAmount * percentSurCharge) / 100;
                                    surAmount = (charge.Amount - percentAppFee) / allInstallmentPartCount;
                                }
                                else
                                {
                                    foreach (var item in listItemsParts)
                                    {
                                        decimal surchargeApplayAmount = 0;
                                        decimal percentAmount = 0;
                                        var part = orderItem.OrderItemScheduleParts.Where(o => !o.IsDeleted && o.ProgramPartId == item.Id).FirstOrDefault();

                                        if (part != null)
                                        {
                                            percentAmount = (clubSurcharge.Amount * part.PartAmount) / 100;
                                        }
                                        else
                                        {
                                            var selectedDays = orderItem.Attributes.WeekDays;
                                            decimal amount = GetProgramSchedulePartAmount(item, orderItem.ProgramSchedule, selectedCharges, selectedDays, newDesiredStartDate);

                                            surchargeApplayAmount = amount - totalDiscount;
                                            percentAmount = (clubSurcharge.Amount * surchargeApplayAmount) / 100;
                                        }

                                        isMulToInstallment = false;
                                        surAmount += percentAmount;
                                    }
                                }
                            }
                        }
                        else //if club remove surCharge from setting
                        {
                            surAmount = charge.Amount / countInastallmentPart;
                        }
                    }
                    else
                    {
                        if (club.PartnerClub != null && club.PartnerClub.Charges != null && club.PartnerClub.Charges.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                        {
                            partnerSurcharge = club.PartnerClub.Charges.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.PartnerSurcharge);
                            if (partnerSurcharge.AmountType == ChargeDiscountType.Fixed)
                            {
                                surAmount = charge.Amount / countInastallmentPart;

                            }
                            else
                            {
                                if (orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                {
                                    var percentSurCharge = partnerSurcharge.Amount;
                                    var percentAppFee = (applicationFeeAmount * percentSurCharge) / 100;
                                    surAmount = (charge.Amount - percentAppFee) / allInstallmentPartCount;
                                }
                                else
                                {
                                    foreach (var item in listItemsParts)
                                    {
                                        decimal surchargeApplayAmount = 0;
                                        decimal percentAmount = 0;
                                        var part = orderItem.OrderItemScheduleParts.Where(o => !o.IsDeleted && o.ProgramPartId == item.Id).FirstOrDefault();

                                        if (part != null)
                                        {
                                            percentAmount = (partnerSurcharge.Amount * part.PartAmount) / 100;
                                        }
                                        else
                                        {
                                            var selectedDays = orderItem.Attributes.WeekDays;
                                            decimal amount = GetProgramSchedulePartAmount(item, orderItem.ProgramSchedule, selectedCharges, selectedDays, newDesiredStartDate);

                                            surchargeApplayAmount = amount - totalDiscount;
                                            percentAmount = (partnerSurcharge.Amount * amount) / 100;
                                        }

                                        isMulToInstallment = false;
                                        surAmount += percentAmount;
                                    }
                                }
                            }
                        }
                        else //if partner remove surCharge from setting
                        {
                            surAmount = charge.Amount / countInastallmentPart;
                        }
                    }

                    if (!installmentIsDeleted)
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == charge.Id).FirstOrDefault().Amount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                        orderItem.TotalAmount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                        orderItem.Order.OrderAmount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                    }
                    else
                    {
                        orderItem.GetOrderChargeDiscounts().Where(s => s.Id == charge.Id).FirstOrDefault().Amount -= isMulToInstallment ? surAmount * installmentCount : surAmount;
                        totalChargeDiscountAmount += isMulToInstallment ? surAmount * installmentCount : surAmount;
                    }
                }
            }

            return totalChargeDiscountAmount;
        }

        public OperationStatus AssignNewFormToOrders(OrderItem orderItem, int userId, List<long> orderItemIds)
        {
            var confirMationId = orderItem.Order.ConfirmationId;
            var updatedOrderItems = new List<OrderItem>();

            foreach (var id in orderItemIds)
            {
                var newOrder = GetItem(id);
                newOrder.JbForm.JsonElements = orderItem.JbForm.JsonElements;
                newOrder.JbForm.ParentId = orderItem.JbForm.ParentId;

                newOrder.Order.OrderHistories.Add(
                    new OrderHistory
                    {
                        Action = OrderAction.ReassignRegistrationForm,
                        ActionDate = DateTime.UtcNow,
                        Description = Jumbula.Common.Constants.Messages.ReassignRegistrationForm + confirMationId,
                        UserId = userId,
                        OrderItemId = newOrder.Id
                    });

                updatedOrderItems.Add(newOrder);
            }

            return UpdateOrderItems(updatedOrderItems);
        }

        public IQueryable<OrderItem> GetList(Order order)
        {
            return order.OrderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted).AsQueryable();
        }

        public IQueryable<OrderItem> GetNonDonationItems(Order order)
        {
            return GetList(order).Where(o => o.ProgramScheduleId.HasValue).AsQueryable();
        }

        public IQueryable<OrderItem> GetDonationItems(Order order)
        {
            return GetList(order).Where(o => !o.ProgramScheduleId.HasValue).AsQueryable();
        }

        public string GetSubscriptionSchedulePartDateRange(OrderInstallment installment)
        {
            if (installment.Type == InstallmentType.AddOn || !installment.ProgramSchedulePartId.HasValue) return string.Empty;

            return $"{installment.ProgramSchedulePart.StartDate.ToString(Constants.DateTime_Comma)} - {installment.ProgramSchedulePart.EndDate.ToString(Constants.DateTime_Comma)}";
        }

        public IQueryable<OrderItem> GetRegisteredProfileBySeason(int? playerId, long? seasonId)
        {
            var query = GetList().Where(item => item.PlayerId == playerId && item.SeasonId == seasonId && item.ProgramScheduleId.HasValue && item.Order.IsLive &&
                                                (item.ItemStatus == OrderItemStatusCategories.completed || (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && (item.ItemStatusReason == OrderItemStatusReasons.transferOut || item.ItemStatusReason == OrderItemStatusReasons.canceled)));
            return query;
        }

        public IQueryable<OrderItem> GetRegisteredProfile(int playerId)
        {
            var query = GetList().Where(item => item.PlayerId.Value == playerId && item.ProgramScheduleId.HasValue && item.Order.IsLive &&
                                                (item.ItemStatus == OrderItemStatusCategories.completed || (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && (item.ItemStatusReason == OrderItemStatusReasons.transferOut || item.ItemStatusReason == OrderItemStatusReasons.canceled)));
            return query;
        }

        private IQueryable<OrderItem> GetNotDonationItems(long programId)
        {
            return GetList().Where(item => item.ProgramScheduleId.HasValue &&
                                    item.ProgramSchedule.ProgramId == programId &&
                                    (item.ProgramSchedule.Program.Season.Status == SeasonStatus.Live) == item.Order.IsLive);
        }


        public IEnumerable<UserPaymentsItemViewModel> GetUserPaymentItems(int userId, int clubId)
        {
            var clubUserCompletedOrCanceledOrderItems = GetClubUserCompletedOrCanceledOrderItems(userId, clubId).ToList();

            var fullPaidOrderItems = clubUserCompletedOrCanceledOrderItems.Where(o => o.PaymentPlanType == PaymentPlanType.FullPaid).ToList();
            var installmentOrderItems = clubUserCompletedOrCanceledOrderItems.Where(o => o.PaymentPlanType == PaymentPlanType.Installment).ToList();

            var installments = _orderInstallmentBusiness.GetCalendarOrderItemInstallments(installmentOrderItems).ToList();

            var model = new List<UserPaymentsItemViewModel>();

            if (installments.Any())
            {
                model = installments.Select(MapUserPaymentsItemViewModel).ToList();
            }

            if (fullPaidOrderItems.Any())
            {
                model.AddRange(fullPaidOrderItems.Select(MapUserPaymentsItemViewModel).ToList());
            }

            return model.Where(m => m.IsPresentable); //Just show items in calendar that Is Presentable
        }

        private UserPaymentsItemViewModel MapUserPaymentsItemViewModel(OrderInstallment installment)
        {
            return new UserPaymentsItemViewModel
            {
                Id = installment.Id,
                Currency = installment.OrderItem.Order.Club.Currency,
                Start = installment.InstallmentDate.ToZeroTime(),
                End = installment.InstallmentDate.ToZeroTime(),
                IsAutoCharge = !installment.EnableReminder,
                IsManually = installment.EnableReminder,
                PaidAmount = installment.PaidAmount ?? 0,
                Title = Common.Constants.Payment.ParentPaymentSchedulerTitle,
                PaymentMessage = string.Format(Common.Constants.Payment.ParentPaymentSchedulerMessage, installment.OrderItem.Player.Contact.FirstName, installment.OrderItem.Player.Contact.LastName, CurrencyHelper.FormatCurrencyWithPenny(installment.Amount, installment.OrderItem.Order.Club.Currency), CurrencyHelper.FormatCurrencyWithPenny(installment.PaidAmount ?? 0, installment.OrderItem.Order.Club.Currency)),
                TooltipMessage = string.Format(Common.Constants.Payment.ParentPaymentSchedulerTooltipMessage, installment.OrderItem.Player.Contact.FirstName, installment.OrderItem.Player.Contact.LastName, CurrencyHelper.FormatCurrencyWithPenny(installment.Amount, installment.OrderItem.Order.Club.Currency), CurrencyHelper.FormatCurrencyWithPenny(installment.PaidAmount ?? 0, installment.OrderItem.Order.Club.Currency), GetProgramName(installment.OrderItem), installment.OrderItem.Order.ConfirmationId),
                PaymentType = PaymentPlanType.Installment.ToString(),
                IsPayable = _orderInstallmentBusiness.CheckIsPayable(installment)
            };
        }

        private UserPaymentsItemViewModel MapUserPaymentsItemViewModel(OrderItem orderItem)
        {
            return new UserPaymentsItemViewModel
            {
                Id = orderItem.Id,
                Currency = orderItem.Order.Club.Currency,
                Title = Common.Constants.Payment.ParentPaymentSchedulerTitle,
                PaidAmount = orderItem.PaidAmount,
                Start = GetStartDate(orderItem),
                End = GetStartDate(orderItem),
                PaymentMessage = string.Format(Common.Constants.Payment.ParentPaymentSchedulerMessage, orderItem.Player.Contact.FirstName, orderItem.Player.Contact.LastName, CurrencyHelper.FormatCurrencyWithPenny(orderItem.TotalAmount, orderItem.Order.Club.Currency), CurrencyHelper.FormatCurrencyWithPenny(orderItem.PaidAmount, orderItem.Order.Club.Currency)),
                TooltipMessage = string.Format(Common.Constants.Payment.ParentPaymentSchedulerTooltipMessage, orderItem.Player.Contact.FirstName, orderItem.Player.Contact.LastName, CurrencyHelper.FormatCurrencyWithPenny(orderItem.TotalAmount, orderItem.Order.Club.Currency), CurrencyHelper.FormatCurrencyWithPenny(orderItem.PaidAmount, orderItem.Order.Club.Currency), GetProgramName(orderItem), orderItem.Order.ConfirmationId),
                PaymentType = PaymentPlanType.FullPaid.ToString(),
                IsCancel = orderItem.ItemStatusReason == OrderItemStatusReasons.canceled,
                IsPayable = CheckIsPayable(orderItem)
            };
        }

        private DateTime GetStartDate(OrderItem orderItem)
        {
            var result = _programSessionBusiness.GetFirstSession(orderItem);

            return result.StartTime;
        }

        public bool AddItemToCart(ConfirmPaymentAmountViewModel model)
        {
            model.PayableAmount = model.AmountPayType == AmountPayType.FullPay ? model.Balance : model.PayableAmount;
            model.EncodedReturnUrl = HttpUtility.UrlEncode(model.ReturnUrl);
            model.Token = Guid.NewGuid().ToString("N");

            if (model.PaymentType == PaymentPlanType.Installment) // if parent select one installment for pay
            {
                var installment = _orderInstallmentBusiness.Get(model.Id);

                //Set payment details and transaction for pay
                if (installment == null) return false;

                var paymentDetail = new PaymentDetail(installment.OrderItem.Order.User.UserName, PaymentDetailStatus.PROCESSING, "", PaymentMethod.Card, installment.OrderItem.Order.Club.Currency, string.Empty, string.Empty, string.Empty, RoleCategoryType.Parent);
                _orderBusiness.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Draft, installment, null, model.PayableAmount, null, installment.Id, string.Empty, model.Token, string.Empty, TransactionCategory.TakePayment);

                var result = _orderInstallmentBusiness.Update(installment);

                return result.Status;
            }
            else // if parent select one orderItem for pay
            {
                var orderItem = GetItem(model.Id);

                //Set payment details and transaction for pay
                if (orderItem == null) return false;

                var paymentDetail = new PaymentDetail(orderItem.Order.User.UserName, PaymentDetailStatus.PROCESSING, "", PaymentMethod.Card, orderItem.Order.Club.Currency, string.Empty, string.Empty, string.Empty, RoleCategoryType.Parent);
                _orderBusiness.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Draft, null, null, model.PayableAmount, orderItem, null, string.Empty, model.Token, string.Empty, TransactionCategory.TakePayment);

                return Update(orderItem).Status;
            }

        }

        public Tuple<bool, string> CheckPayableAmountValidation(decimal payableAmount, decimal totalAmount)
        {
            var result = new Tuple<bool, string>(true, string.Empty);

            if (payableAmount > totalAmount)
            {
                result = new Tuple<bool, string>(false, Common.Constants.Payment.PaymentAmountValidationMoreThanBalance);
            }

            if (payableAmount <= 0)
            {
                result = new Tuple<bool, string>(false, Common.Constants.Payment.PaymentAmountValidationLessThanZero);
            }

            return result;
        }

        public ConfirmPaymentAmountViewModel ConfirmPaymentAmount(long id, PaymentPlanType paymentType, string returnUrl)
        {
            var model = new ConfirmPaymentAmountViewModel();

            switch (paymentType)
            {
                case PaymentPlanType.Installment:
                    {
                        var installment = _orderInstallmentBusiness.Get(id);

                        if (installment == null) return model;

                        model = MapConfirmPaymentAmountViewModel(installment, paymentType, returnUrl);

                        break;
                    }
                case PaymentPlanType.FullPaid:
                    {
                        var orderItem = GetItem(id);

                        if (orderItem == null) return model;

                        model = MapConfirmPaymentAmountViewModel(orderItem, paymentType, returnUrl);

                        break;
                    }
                default:
                    throw new ArgumentOutOfRangeException(nameof(paymentType), paymentType, null);
            }

            return model;
        }

        private ConfirmPaymentAmountViewModel MapConfirmPaymentAmountViewModel(OrderInstallment installment, PaymentPlanType paymentType,
            string returnUrl)
        {
            var result = new ConfirmPaymentAmountViewModel
            {
                Id = installment.Id,
                PayableAmount = installment.Balance,
                Balance = installment.Balance,
                DueDate = installment.InstallmentDate,
                FullName = $"{installment.OrderItem.Player.Contact.FirstName} {installment.OrderItem.Player.Contact.LastName}",
                ProgramName = GetProgramName(installment.OrderItem),
                AmountPayType = AmountPayType.FullPay,
                PaymentType = paymentType,
                IsAutoChargeNotPastDue = !installment.EnableReminder && installment.InstallmentDate > DateTime.UtcNow, //For show text note for autoCharge in popup view
                ReturnUrl = returnUrl
            };
            return result;
        }

        private ConfirmPaymentAmountViewModel MapConfirmPaymentAmountViewModel(OrderItem orderItem, PaymentPlanType paymentType,
            string returnUrl)
        {

            var result = new ConfirmPaymentAmountViewModel
            {
                Id = orderItem.Id,
                PayableAmount = GetPayableBalance(orderItem),
                Balance = GetPayableBalance(orderItem),
                AmountPayType = AmountPayType.FullPay,
                DueDate = GetStartDate(orderItem),
                FullName = $"{orderItem.Player.Contact.FirstName} {orderItem.Player.Contact.LastName}",
                ProgramName = GetProgramName(orderItem),
                PaymentType = paymentType,
                ReturnUrl = returnUrl
            };

            return result;
        }

        public UserPaymentsItemViewModel GetParentPayments(long? id, PaymentPlanType? paymentType, bool? status, int userId)
        {
            var calendarDefaultDate = GetCalendarDefaultDate(id, paymentType);

            var model = new UserPaymentsItemViewModel
            {
                Start = calendarDefaultDate,
                UserId = userId,
                Status = status,
            };

            return model;
        }

        private DateTime GetCalendarDefaultDate(long? id, PaymentPlanType? paymentType)
        {
            if (!id.HasValue) return DateTime.UtcNow;

            switch (paymentType)
            {
                case PaymentPlanType.Installment:
                    {
                        var installment = _orderInstallmentBusiness.Get(id.Value);

                        if (installment != null)
                            return installment.InstallmentDate; // for return to date of the installment paid
                        break;
                    }
                case PaymentPlanType.FullPaid:
                    {
                        var orderItem = GetItem(id.Value);

                        if (orderItem != null)
                            return GetStartDate(orderItem);
                        break;
                    }
                case null:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(paymentType), paymentType, null);
            }

            return DateTime.UtcNow;
        }

        private string GetProgramName(OrderItem orderItem)
        {
            if (orderItem == null)
            {
                throw new ArgumentNullException(nameof(orderItem));
            }

            return orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare ?
                _programBusiness.GetProgramNameWithScheduleTitleAndDate(orderItem, orderItem.EntryFeeName) :
                  _programBusiness.GetProgramName(orderItem.ProgramSchedule != null ? orderItem.ProgramSchedule.Program.Name : "Donation", orderItem.Start ?? DateTime.Now, orderItem.End ?? DateTime.Now, orderItem.EntryFeeName);
        }

        public void CreateAddOnChargeToOrderItem(OrderItem orderItem, ChargeDiscountCategory category, decimal amount, string name, string description, long? chargeId = null)
        {
            var newInstallment = _orderInstallmentBusiness.CreateAddOnInstallment(amount);
            orderItem.Installments.Add(newInstallment);

            orderItem.OrderChargeDiscounts.Add(
                new OrderChargeDiscount
                {
                    Amount = amount,
                    Category = category,
                    Name = name,
                    Description = description,
                    Subcategory = ChargeDiscountSubcategory.Charge,
                    OrderInstallment = newInstallment,
                    ChargeId = chargeId
                }
            );
        }

        private void SetCancellationProrateFeeToInstallments(List<OrderInstallment> installments, string userName)
        {
            var normalInstallments = installments.Where(i => i.Type == InstallmentType.Noraml).ToList();
            var addOnInstallments = installments.Where(i => i.Type == InstallmentType.AddOn).ToList();
            var installmentsWithPaidAmount = normalInstallments.Where(i => i.PaidAmount > 0);

            var charges = installments.First().OrderItem.OrderChargeDiscounts;
            var cancelInstallment = charges.SingleOrDefault(i => addOnInstallments.Contains(i.OrderInstallment) && i.Category == ChargeDiscountCategory.CancellationFee);
            var prorateInstallment = charges.SingleOrDefault(i => addOnInstallments.Contains(i.OrderInstallment) && i.Category == ChargeDiscountCategory.ProrateFee);

            foreach (var item in installmentsWithPaidAmount)
            {
                var cancellationFee = cancelInstallment?.OrderInstallment.Balance ?? 0;
                var prorateFee = prorateInstallment?.OrderInstallment.Balance ?? 0;

                if (item.PaidAmount > 0 && cancellationFee > 0)
                {
                    if (cancelInstallment != null)
                        UpdateAddOnInstallment(item, cancelInstallment.OrderInstallment, userName);
                }

                if (item.PaidAmount > 0 && prorateFee > 0)
                {
                    if (prorateInstallment != null)
                        UpdateAddOnInstallment(item, prorateInstallment.OrderInstallment, userName);
                }
            }
        }

        public void UpdateAddOnInstallment(OrderInstallment item, OrderInstallment installmentCharge, string userName)
        {
            var amountFee = installmentCharge?.Balance ?? 0;

            var amount = item.PaidAmount <= amountFee ? item.PaidAmount.Value : amountFee;

            if (amount > 0)
            {
                SetRefundTransactionToInstallment(item, amount, userName);
                SetTakePaymentTransactionToInstallment(item.OrderItem, installmentCharge, amount, userName);

                if (installmentCharge != null)
                {
                    var cancelPaid = installmentCharge.PaidAmount ?? 0;
                    installmentCharge.PaidAmount = cancelPaid + amount;
                    installmentCharge.Status = OrderStatusCategories.completed;
                }

                item.PaidAmount = item.PaidAmount - amount;

            }
        }
        private void SetRefundTransactionToInstallment(OrderInstallment installment, decimal amount, string userName)
        {
            if (installment.OrderItem.TransactionActivities == null)
            {
                installment.OrderItem.TransactionActivities = new List<TransactionActivity>();
            }

            var transactionId = _transactionActivityBusiness.GeneratGuidTransactionId();

            var paymentDetail = new PaymentDetail(userName, PaymentDetailStatus.COMPLETED, "", PaymentMethod.Credit, installment.OrderItem.Order.Club.Currency, "", "", transactionId, RoleCategoryType.System, null, null, ActionReason.Cancel);

            installment.OrderItem.TransactionActivities.Add(SetRefundTransaction(installment.OrderItem, installment.Id, paymentDetail, TransactionStatus.Success, amount, DateTime.UtcNow, PaymentConstants.SystemRefundNote, HandleMode.Offline));
        }

        private void SetTakePaymentTransactionToInstallment(OrderItem orderitem, OrderInstallment installment, decimal amount, string userName)
        {
            if (orderitem.TransactionActivities == null)
            {
                orderitem.TransactionActivities = new List<TransactionActivity>();
            }

            var transactionId = _transactionActivityBusiness.GeneratGuidTransactionId();
            var paymentDetail = new PaymentDetail(userName, PaymentDetailStatus.COMPLETED, "", PaymentMethod.Credit, orderitem.Order.Club.Currency, string.Empty, string.Empty, transactionId, RoleCategoryType.System, null, null, ActionReason.Cancel);

            orderitem.TransactionActivities.Add(new TransactionActivity
            {
                Amount = amount,
                TransactionCategory = TransactionCategory.TakePayment,
                ClubId = orderitem.Order.ClubId,
                OrderId = orderitem.Order_Id,
                OrderItemId = orderitem.Id,
                Installment = installment,
                PaymentDetail = paymentDetail,
                TransactionDate = DateTime.UtcNow,
                TransactionStatus = TransactionStatus.Success,
                TransactionType = TransactionType.Payment,
                SeasonId = orderitem.SeasonId,
                HandleMode = HandleMode.Offline,
                MetaData = new MetaData
                {
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now
                },
                Note = PaymentConstants.SystemTakeAPaymentNote
            });
        }

        public OperationStatus UncancelOrder(OrderItem orderItem, int userId)
        {
            var cancelProrateFee = orderItem.OrderChargeDiscounts.Where(c => c.Category == ChargeDiscountCategory.CancellationFee || c.Category == ChargeDiscountCategory.ProrateFee).ToList();
            var orderItemEntryFee = orderItem.OrderChargeDiscounts.First(c => c.Category == ChargeDiscountCategory.EntryFee);
            var deletedInstallments = orderItem.Installments.Where(i => i.IsDeleted && i.DeleteReason == DeleteReason.Cancel);

            if (orderItem.Installments.Any())
            {
                var deletedInstallmentIds = deletedInstallments.Select(i => i.Id).ToList();
                UndoCancelProrateFeeAmount(orderItem, cancelProrateFee);

                orderItem.Installments.Where(i => deletedInstallmentIds.Contains(i.Id)).ToList().ForEach(o => o.IsDeleted = false);
                orderItem.Installments.Where(i => deletedInstallmentIds.Contains(i.Id)).ToList().ForEach(o => o.DeleteReason = null);
            }
            else
            {
                orderItem.OrderChargeDiscounts.ToList().ForEach(c => c.IsDeleted = false);

                if (cancelProrateFee.Any())
                {

                    orderItem.OrderChargeDiscounts.Where(c => cancelProrateFee.Select(i => i.Id).ToList().Contains(c.Id)).ToList().ForEach(c => c.IsDeleted = true);
                    orderItem.TotalAmount -= cancelProrateFee.Sum(c => c.Amount);
                    orderItem.Order.OrderAmount -= cancelProrateFee.Sum(c => c.Amount);
                }
            }

            if (orderItemEntryFee.ChargeId.HasValue && orderItem.ProgramTypeCategory == ProgramTypeCategory.Camp)
            {
                _orderSessionBusiness.CreateSession(orderItem, orderItemEntryFee.ChargeId.Value);
            }

            orderItem.TotalAmount = orderItem.GetOrderChargeDiscounts().Sum(c => c.Amount);
            orderItem.EntryFee = orderItemEntryFee.Amount;
            orderItem.Order.OrderAmount += orderItem.TotalAmount;
            orderItem.ItemStatus = OrderItemStatusCategories.completed;
            orderItem.ItemStatusReason = OrderItemStatusReasons.regular;

            //add history
            var historyDescription = string.Format(Common.Constants.Messages.UndoCancelOrder,
              orderItem.ProgramSchedule.Program.Name, orderItem.EntryFeeName, orderItem.EntryFee.ToString("C2", CultureInfo.CurrentCulture));

            AddHistory(orderItem, OrderAction.UndoCancel, historyDescription, userId);


            _dataContext.Set<OrderChargeDiscount>().RemoveRange(cancelProrateFee);

            return _orderItemRepository.Save();
        }

        private void UndoCancelProrateFeeAmount(OrderItem orderItem, List<OrderChargeDiscount> cancelProrateFee)
        {
            if (cancelProrateFee.Any())
            {
                var cancelProrateFeeInstallments = cancelProrateFee.Select(c => c.OrderInstallment).ToList();
                foreach (var installment in cancelProrateFeeInstallments.Where(i => i.PaidAmount.HasValue && i.PaidAmount > 0))
                {
                    installment.PaidAmount = 0;
                    installment.IsDeleted = true;
                    installment.TransactionActivities.Where(t => t.PaymentDetail.PayerType == RoleCategoryType.System && t.PaymentDetail.ActionReason == ActionReason.Cancel).ToList().ForEach(t => t.TransactionStatus = TransactionStatus.Deleted);
                }

                foreach (var installment in orderItem.Installments.Where(i => !cancelProrateFeeInstallments.Select(inst => inst.Id).Contains(i.Id)))
                {
                    var installmentTransactions = installment.TransactionActivities.Where(t => t.TransactionStatus != TransactionStatus.Deleted && t.PaymentDetail.PayerType == RoleCategoryType.System && t.PaymentDetail.ActionReason == ActionReason.Cancel).ToList();
                    installment.PaidAmount += installmentTransactions.Sum(t => t.Amount);
                    installment.Status = OrderStatusCategories.completed;
                    installmentTransactions.ForEach(t => t.TransactionStatus = TransactionStatus.Deleted);
                }

                orderItem.OrderChargeDiscounts.Where(c => !cancelProrateFee.Select(d => d.Id).ToList().Contains(c.Id)).ToList().ForEach(c => c.IsDeleted = false);
                orderItem.TotalAmount -= cancelProrateFee.Sum(c => c.Amount);
                orderItem.Order.OrderAmount -= cancelProrateFee.Sum(c => c.Amount);
                orderItem.OrderChargeDiscounts.Where(c => cancelProrateFee.Select(d => d.Id).ToList().Contains(c.Id)).ToList().ForEach(c => c.IsDeleted = true);
                orderItem.Installments.Where(i => cancelProrateFeeInstallments.Select(inst => inst.Id).Contains(i.Id)).ToList().ForEach(i => i.IsDeleted = true);

            }
            else
                orderItem.OrderChargeDiscounts.ToList().ForEach(c => c.IsDeleted = false);
        }

        private void AddHistory(OrderItem orderItem, OrderAction orderAction, string description, int userId)
        {
            orderItem.Order.OrderHistories.Add(
             new OrderHistory
             {
                 Action = orderAction,
                 ActionDate = DateTime.UtcNow,
                 Description = description,
                 UserId = userId,
                 OrderItemId = orderItem.Id
             });
        }

        public UndoFamilyCreditToOrderViewModel GetUndoFamilyCreditViewModel(OrderItem orderItem, long transactionId)
        {
            var model = MapUndoCreditToViewModel(orderItem, transactionId);

            return model;
        }

        private UndoFamilyCreditToOrderViewModel MapUndoCreditToViewModel(OrderItem orderItem, long transactionId)
        {
            var club = orderItem.Order.Club;
            var user = club.Users.Single(u => u.UserId == orderItem.Order.UserId);
            var refundTransaction = orderItem.TransactionActivities.First(t => t.Id == transactionId);
            var desiredStartDate = orderItem.DesiredStartDate.HasValue ? orderItem.DesiredStartDate.Value.ToString(Constants.DateTime_Comma) : null;
            var effectiveTransferredDate = orderItem.Attributes.TransferEffectiveDate.HasValue ? orderItem.Attributes.TransferEffectiveDate.Value.ToString(Constants.DateTime_Comma) : null;
            var effectiveCanceledDate = orderItem.Attributes.CancelEffectiveDate.HasValue ? orderItem.Attributes.CancelEffectiveDate.Value.ToString(Constants.DateTime_Comma) : null;
            var orderDays = new List<DayOfWeek>();
            var subscriptionAttribute = orderItem.Attributes.WeekDays;
            var gender = GetGender(orderItem);
            var MaximumAmount = GetMaximumUndoAmount(orderItem, user, refundTransaction);

            if (subscriptionAttribute != null)
                orderDays = subscriptionAttribute;

            return new UndoFamilyCreditToOrderViewModel
            {
                FamilyCredit = orderItem.Order.IsLive ? user.Credit : user.TestCredit,
                Currency = club.Currency,
                TransactionAmount = refundTransaction.Amount,
                MaximumAmount = MaximumAmount,
                OrderInfo = new OrderInformationViewModel
                {
                    ConfirmationId = orderItem.Order.ConfirmationId,
                    Gender = gender != GenderCategories.None ? gender.ToString() : string.Empty,
                    Age = GetAge(orderItem),
                    Name = orderItem.FirstName,
                    FullName = orderItem.FullName,
                    DesiredStartDate = desiredStartDate,
                    EffectiveTransferredDate = effectiveTransferredDate,
                    EffectiveCanceledDate = effectiveCanceledDate,
                    HasProgramScheduleId = orderItem.ProgramScheduleId.HasValue,
                    ItemStatusReson = orderItem.ItemStatusReason,
                    IsDropIn = orderItem.Mode == OrderItemMode.DropIn,
                    OrderAmount = orderItem.Order.OrderAmount,
                    ProgramType = orderItem.ProgramTypeCategory,
                    Days = orderDays,
                    Balance = _accountingBusiness.OrderItemBalance(orderItem),
                    TotalAmount = orderItem.TotalAmount,
                    PaidAmount = orderItem.PaidAmount,
                },
                TransactionId = transactionId,
                orderItemId = orderItem.Id,
                TransactionActivity = refundTransaction.ToViewModel<TransactionActivitiesViewModel>(),
            };
        }

        private decimal GetMaximumUndoAmount(OrderItem orderItem, ClubUser clubUser, TransactionActivity transaction)
        {
            decimal result = 0;
            var totalUndoAmount = orderItem.TransactionActivities.Where(s => s.TransactionCategory == TransactionCategory.FamilyCreditTransfer && s.PaymentDetail.RelatedTransactionId == transaction.PaymentDetail.TransactionId).Sum(t => t.Amount);

            var differenceTrnsferredAmount = transaction.Amount - totalUndoAmount;

            if (orderItem.Order.IsLive)
                result = clubUser.Credit > differenceTrnsferredAmount ? differenceTrnsferredAmount : clubUser.Credit;
            else
                result = clubUser.TestCredit > differenceTrnsferredAmount ? differenceTrnsferredAmount : clubUser.TestCredit;

            return result;
        }
        public OperationStatus UndoFamilyRefundToOrder(OrderItem orderItem, UndoFamilyCreditToOrderViewModel model, int userId)
        {
            if (!model.ReturnedAmount.HasValue) return new OperationStatus() { Status = false };

            var club = orderItem.Order.Club;
            var user = club.Users.Single(u => u.UserId == orderItem.Order.UserId);

            var refundTransaction = orderItem.TransactionActivities.Single(t => t.Id == model.TransactionId);

            orderItem.PaidAmount += model.ReturnedAmount.Value;

            if (orderItem.Order.IsLive)
                user.Credit -= model.ReturnedAmount.Value;
            else
                user.TestCredit -= model.ReturnedAmount.Value;

            //set transaction for the undo credit
            var transactionId = _transactionActivityBusiness.GeneratGuidTransactionId();
            var currentUser = _userProfileBusienss.Get(userId);
            var paymentDetail = new PaymentDetail(currentUser.UserName, PaymentDetailStatus.COMPLETED, "", PaymentMethod.Credit, orderItem.Order.Club.Currency, string.Empty, string.Empty, transactionId, RoleCategoryType.Dashboard, null, null, null, refundTransaction.PaymentDetail.TransactionId);

            orderItem.TransactionActivities.Add(new TransactionActivity
            {
                Amount = model.ReturnedAmount.Value,
                TransactionCategory = TransactionCategory.FamilyCreditTransfer,
                ClubId = orderItem.Order.ClubId,
                OrderId = orderItem.Order_Id,
                OrderItemId = orderItem.Id,
                InstallmentId = refundTransaction.InstallmentId ?? null,
                PaymentDetail = paymentDetail,
                TransactionDate = DateTime.UtcNow,
                TransactionStatus = TransactionStatus.Success,
                TransactionType = TransactionType.Payment,
                SeasonId = orderItem.SeasonId,
                HandleMode = HandleMode.Offline,
                MetaData = new MetaData
                {
                    DateCreated = DateTime.Now,
                    DateUpdated = DateTime.Now
                },
            });

            if (refundTransaction.InstallmentId.HasValue)
                refundTransaction.Installment.PaidAmount += model.ReturnedAmount.Value;

            var historyDescription = string.Format(Common.Constants.Messages.UndoFamilyRefunToOrder, model.ReturnedAmount.Value);

            AddHistory(orderItem, OrderAction.UndoFamilyCreditTransfer, historyDescription, userId);

            return _orderItemRepository.Save();
        }

        public int GetAge(OrderItem orderItem)
        {
            var age = 0;

            if (orderItem.ProgramScheduleId.HasValue && !_programBusiness.IsTeamChessTourney(orderItem.ProgramSchedule.Program) && orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.DoB.ToString()))
            {
                var dobDate = DateTime.UtcNow;
                if (DateTime.TryParse(orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.SingleOrDefault(e => e.Name == ElementsName.DoB.ToString())?.GetValue().ToString(), out dobDate))
                {
                    age = DateTime.Now.Year - dobDate.Year;
                }
            }

            return age;
        }

        public GenderCategories GetGender(OrderItem orderItem)
        {
            GenderCategories gender = GenderCategories.None;

            if (orderItem.ProgramScheduleId.HasValue && orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(e => e.Name == ElementsName.Gender.ToString()))
            {
                gender = _formBusiness.GetParticipantGender(orderItem.JbForm);
            }

            return gender;
        }

        public OperationStatus CompleteDraftItem(long orderItemId, int userId)
        {
            var orderItem = GetItem(orderItemId);

            orderItem.ItemStatus = OrderItemStatusCategories.completed;

            var order = orderItem.Order;
            order.ConfirmationId = _orderBusiness.GenerateConfirmationId();
            order.IsDraft = false;
            order.OrderMode = OrderMode.Offline;
            order.OrderAmount = orderItem.TotalAmount;
            order.OrderStatus = OrderStatusCategories.completed;
            order.CompleteDate = DateTime.UtcNow;
            order.SubmitDate = DateTime.UtcNow;

            var userClub = order.Club.Users.SingleOrDefault(u => u.UserId == order.UserId);

            if (userClub == null)
                order.Club.Users.Add(new ClubUser() { ClubId = order.Club.Id, UserId = order.UserId });

            var historyDescription = "Order" + order.ConfirmationId + "is placed by admin.";
            AddHistory(orderItem, OrderAction.Initiated, historyDescription, userId);

            var result = _orderItemRepository.Save();

            if (result.Status)
                result = _orderBusiness.SeparateOrderItemFromCartOrder(order, orderItem.Id);

            return result;
        }

        private IQueryable<OrderItem> GetValidOrderItems()
        {
            return GetList().Where(o => o.ItemStatus != OrderItemStatusCategories.initiated && o.ItemStatus != OrderItemStatusCategories.notInitiated);
        }

        public string GetClassDays(OrderItem orderItem)
        {
            switch (orderItem.ProgramTypeCategory)
            {
                case ProgramTypeCategory.Class:
                    {
                        return string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(_programBusiness.GetClassDays(orderItem.ProgramSchedule).Select(s => s.DayOfWeek).ToList()));
                    }
                case ProgramTypeCategory.Camp:
                case ProgramTypeCategory.Subscription:
                case ProgramTypeCategory.BeforeAfterCare:
                    {
                        return string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(_orderSessionBusiness.GetOrderItemSessions(orderItem.Id).ToList().Select(o => o.ProgramSession.StartDateTime.DayOfWeek).Distinct().ToList()));
                    }
                default:
                    {
                        return string.Empty;
                    }
            }
        }

        public RefundResultModel Refund(OrderItemsViewModel model, ref decimal refundedAmount)
        {
            var orderItem = GetItem(model.Id);
            var historyDescription = string.Empty;

            //options that can picked(Automatically, manually, AddToCredit)
            //first AddToCredit, second manually and in last automatically to do
            var refundOptionsPicked = model.RefundModes.Where(i => i.IsSelected && i.Amount > 0)
                .OrderBy(p => p.Priority);

            var result = RefundByModes(refundOptionsPicked, model, orderItem, ref refundedAmount);
            var fullRefundAmount = refundOptionsPicked.Sum(m => m.Amount);

            if (refundedAmount > 0 && fullRefundAmount == refundedAmount)
            {
                historyDescription = string.Format(Constants.Order_Refund, CurrencyHelper.FormatCurrencyWithPenny(refundedAmount, orderItem.Order.Club.Currency), model.CustomRefund.Name);
            }
            else if (refundedAmount > 0 && fullRefundAmount != refundedAmount)
            {
                historyDescription = string.Format(OrderPartiallyRefund, CurrencyHelper.FormatCurrencyWithPenny(refundedAmount, orderItem.Order.Club.Currency), CurrencyHelper.FormatCurrencyWithPenny(fullRefundAmount - refundedAmount, orderItem.Order.Club.Currency), model.CustomRefund.Name);
            }

            // Update refund installment
            var updateRefund = UpdateRefund(result.Status, orderItem, refundedAmount, model, historyDescription);

            if (!result.Status || updateRefund.Status) return result;
            Log.Error("Error in save refund {ErrorMessage} {OrderItemId}", updateRefund.Message,
                orderItem.Id);
            result.Status = updateRefund.Status;
            result.ErrorMessage = RefundInstallmentFailedSaveDb;

            return result;
        }

        public decimal GetAmountAutomaticallyTransactions(OrderItem orderItem)
        {
            var paymentAutomaticallyAmount = _transactionActivityBusiness.GetPaymentTransactions(orderItem).Sum(t => t.Amount);
            var refundAutomaticallyAmount = _transactionActivityBusiness.GetRefundAutomaticallyTransactions(orderItem).Sum(t => t.Amount);
            return paymentAutomaticallyAmount - refundAutomaticallyAmount;
        }

        public OperationStatus UpdateRefund(bool status, OrderItem orderItem, decimal refundedAmount, OrderItemsViewModel model, string historyDescription)
        {
            if (status)
            {
                _orderHistoryBusiness.AddOrderHistory(OrderAction.PartiallyRefunded, historyDescription, orderItem.Order_Id, model.CurrentUserId, orderItem.Id);
            }

            orderItem.PaidAmount -= refundedAmount;

            return Update(orderItem);
        }

        public decimal GetRefundAmount(TransactionActivity transaction, OrderItem orderItem, decimal remainAmount, bool isTestMode, IEnumerable<TransactionActivity> refunded, bool isPaypalTransaction)
        {
            var refundedAmount = 0m;

            if (refunded.Any(r => r.PaymentDetail.PayKey == transaction.PaymentDetail.PayKey))
                 refundedAmount = refunded.Where(r => r.PaymentDetail.PayKey == transaction.PaymentDetail.PayKey).Sum(t => t.Amount);

            var canRefund =  transaction.Amount - refundedAmount;

            if (canRefund == 0)
                return 0;

            var tmpAmount = remainAmount;

            if (canRefund < tmpAmount)
                tmpAmount = canRefund;

            if (transaction.Amount <= tmpAmount)
                tmpAmount = transaction.Amount;

            return tmpAmount;
        }

        public void SetToUserCredit(OrderItem orderItem, decimal refundAmount)
        {
            var isTestMode = !orderItem.Order.IsLive;

            if (orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId) == null)
            {
                _clubBusiness.AddUserInClubUsers(orderItem.Order.UserId, orderItem.Order.ClubId);
            }

            var clubUser = orderItem.Order.Club.Users.SingleOrDefault(u => u.UserId == orderItem.Order.UserId);

            if (clubUser != null)
            {
                if (isTestMode)
                {
                    clubUser.TestCredit += Math.Abs(refundAmount);
                }
                else
                {
                    clubUser.Credit += Math.Abs(refundAmount);
                }
            }
        }

        #region private
        private RefundResultModel RefundByModes(IEnumerable<RefundInformation> refundOptionsPicked, OrderItemsViewModel model, OrderItem orderItem, ref decimal refundedAmount)
        {
            var result = new RefundResultModel();
            var refundAmount = 0m;

            foreach (var item in refundOptionsPicked)
            {
                if (item.Amount != null) refundAmount = item.Amount.Value;
                var remainAmount = refundAmount;
                result.TransactionId = string.Empty;

                switch (item.Mode)
                {
                    case RefundMode.Automatically:

                        //Refund by Paypal, stripe, authorize
                        result = RefundAutomatically(orderItem, model.CurrentUserName, model.CurrentRoleType, model.CustomRefund.Name, ref remainAmount);
                        refundedAmount += result.Status ? result.RefundedAmount : 0;

                        break;

                    case RefundMode.Manually:

                        result = RefundManually(orderItem, model.CurrentUserName, model.CurrentRoleType, model.CustomRefund.Name, refundAmount);
                        refundedAmount += result.Status ? result.RefundedAmount : 0;

                        break;

                    case RefundMode.AddToCredit:

                        result = RefundToFamilyCredit(orderItem, model.CurrentUserName, model.CurrentRoleType, model.CustomRefund.Name, refundAmount);
                        refundedAmount += result.Status ? result.RefundedAmount : 0;

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (!result.Status)
                {
                    break;
                }
            }

            return result;
        }

        private RefundResultModel RefundAutomatically(OrderItem orderItem, string currentUserName, RoleCategoryType currentRoleType, string refundNote, ref decimal remainingAmount)
        {
            var club = orderItem.Order.Club;
            var oldTransactions = _transactionActivityBusiness.GetPaymentTransactions(orderItem).ToList();
            var oldRefunded = _transactionActivityBusiness.GetRefundAutomaticallyTransactions(orderItem).ToList();
            var refundResult = new RefundResultModel();
            var refundedAmount = 0m;
            var tempRefundedAmount = 0m;

            foreach (var transactionActivity in oldTransactions)
            {
                if (transactionActivity.PaymentDetail.PaymentMethod == null || remainingAmount == 0) continue;

                var paymentMethod = transactionActivity.PaymentDetail.PaymentMethod.Value;
                var isPaypal = paymentMethod == PaymentMethod.Paypal;
                var refundAmount = GetRefundAmount(transactionActivity, orderItem, remainingAmount, !orderItem.Order.IsLive, oldRefunded, isPaypal);

                if (refundAmount == 0)
                {
                    refundResult.Status = false;
                    refundResult.ErrorMessage = NotRefundWithAutomatically;
                    continue;
                }

                refundResult = _paymentBusiness.Refund(transactionActivity, null, orderItem, club.Domain, refundAmount, !orderItem.Order.IsLive);

                if (refundResult.Status)
                {
                    SetOrderItemRefundedToBeSuccess(refundResult, ref remainingAmount, ref refundedAmount);
                }

                tempRefundedAmount = refundedAmount;

                //Add Payment detail and transaction
                SetPaymentTransaction(paymentMethod, refundResult, orderItem, transactionActivity, currentUserName, currentRoleType, refundNote);
            }

            refundResult.RefundedAmount = tempRefundedAmount;

            return refundResult;
        }

        private RefundResultModel RefundManually(OrderItem orderItem, string currentUserName, RoleCategoryType currentRoleType, string refundNote, decimal refundAmount)
        {
            var model = new RefundResultModel();

            var refundedAmount = 0m;
            var remainingAmount = 0m;

            try
            {
                model.Status = true;
                model.RefundAmount = refundAmount;

                SetOrderItemRefundedToBeSuccess(model, ref remainingAmount, ref refundedAmount);
                SetPaymentTransaction(PaymentMethod.Cash, model, orderItem, null, currentUserName, currentRoleType, refundNote);
                model.RefundedAmount = refundedAmount;
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.ErrorMessage = FailProcessRefund;
                Log.Error(ex, "Refund is failed in {LogCategory} {orderItemId}", "Manually", orderItem.Id);
            }

            return model;
        }

        private RefundResultModel RefundToFamilyCredit(OrderItem orderItem, string currentUserName, RoleCategoryType currentRoleType, string refundNote, decimal refundAmount)
        {
            var model = new RefundResultModel();
            var refundedAmount = 0m;
            var remainingAmount = 0m;

            try
            {
                model.Status = true;
                model.RefundAmount = refundAmount;
                model.TransactionId = _transactionActivityBusiness.GeneratGuidTransactionId();

                SetToUserCredit(orderItem, model.RefundAmount);

                SetOrderItemRefundedToBeSuccess(model, ref remainingAmount, ref refundedAmount);
                SetPaymentTransaction(PaymentMethod.Credit, model, orderItem, null, currentUserName, currentRoleType, refundNote);
                model.RefundedAmount = refundedAmount;
            }
            catch (Exception ex)
            {
                model.Status = false;
                model.PaymentStatus = PaymentDetailStatus.ERROR;
                model.TransactionStatus = TransactionStatus.Failure;
                model.ErrorMessage = FailProcessRefund;
                Log.Error(ex, "Refund is failed in {LogCategory} {OrderItemId}", "Add to Credit", orderItem.Id);

                SetPaymentTransaction(PaymentMethod.Credit, model, orderItem, null, currentUserName, currentRoleType, refundNote);
            }

            return model;
        }

        private void SetOrderItemRefundedToBeSuccess(RefundResultModel model, ref decimal remainingAmount, ref decimal refundedAmount)
        {
            remainingAmount -= model.RefundAmount;
            refundedAmount += model.RefundAmount;
        }

        private void SetPaymentTransaction(PaymentMethod method, RefundResultModel model, OrderItem orderItem, TransactionActivity transaction, string currentUserName, RoleCategoryType currentRoleType, string refundNote)
        {
            var payKey = transaction != null ? transaction.PaymentDetail.PayKey : string.Empty;
            var creditCardId = transaction?.PaymentDetail.CreditCardId;
            var paymentStatus = model.Status ? PaymentDetailStatus.COMPLETED : PaymentDetailStatus.ERROR;
            var transactionStatus = model.Status ? TransactionStatus.Success : TransactionStatus.Failure;

            var paymentDetail = new PaymentDetail(currentUserName, paymentStatus, payKey, method, orderItem.Order.Club.Currency, model.Response, model.ErrorMessage, model.TransactionId, currentRoleType, creditCardId);
            orderItem.TransactionActivities.Add(SetRefundTransaction(orderItem, null, paymentDetail, transactionStatus, model.RefundAmount, DateTime.UtcNow, refundNote));
        }
        #endregion
    }
}



