﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Identity
{
    public interface ICurrentUser
    {
        int Id { get; set; }

        string UserName { get; set; }

        string FullName { get; set; }

        RoleCategory Role { get; set; }
    }
}