﻿using FluentAssertions;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Enums;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class OrderHistoryBusinessTest
    {

        private Mock<IRepository<OrderHistory>> _mockOrderHistoryRepository;
        private Mock<IEntitiesContext> _mockDataContext;

        [TestMethod]
        public void GetCancelHistory_OrderHistoryCancelIsOneInList_ShouldReturnCountOne()
        {
            //Arrange
            var service = CreateService();
            var order = FactoryGirl.Build<Order>();
            var orderHistory1 = FactoryGirl.Build<OrderHistory>(p =>
            {
                p.Id = 3;
                p.OrderId = 1;
                p.OrderItemId = 3;
                p.Action = OrderAction.Canceled;
            });

            var orderHistory2 = FactoryGirl.Build<OrderHistory>(p =>
            {
                p.Id = 3;
                p.OrderId = 1;
                p.OrderItemId = 3;
                p.Action = OrderAction.Failed;
            });

            var orderHistories = new List<OrderHistory>() { orderHistory1, orderHistory2 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.Club = FactoryGirl.Build<Club>();
                p.Player = FactoryGirl.Build<PlayerProfile>();
                p.Player.Contact = FactoryGirl.Build<ContactPerson>();
            });

            _mockOrderHistoryRepository.Setup(x => x.GetList()).Returns(orderHistories.AsQueryable());

            //Act
            var result = service.GetCancelHistory(order.Id, orderItem.Id);

            //Assert
            result.Count().Should().Be(1);

        }

        [TestMethod]
        public void GetCancelHistory_OrderHistoryCancelIsTwoInList_ShouldReturnCountTwo()
        {
            //Arrange
            var service = CreateService();
            var order = FactoryGirl.Build<Order>();
            var orderHistory1 = FactoryGirl.Build<OrderHistory>(p =>
            {
                p.Id = 3;
                p.OrderId = 1;
                p.OrderItemId = 3;
                p.Action = OrderAction.Canceled;
            });

            var orderHistory2 = FactoryGirl.Build<OrderHistory>(p =>
            {
                p.Id = 3;
                p.OrderId = 1;
                p.OrderItemId = 3;
                p.Action = OrderAction.Canceled;
            });

            var orderHistories = new List<OrderHistory>() { orderHistory1, orderHistory2 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.Club = FactoryGirl.Build<Club>();
                p.Player = FactoryGirl.Build<PlayerProfile>();
                p.Player.Contact = FactoryGirl.Build<ContactPerson>();
            });

            _mockOrderHistoryRepository.Setup(x => x.GetList()).Returns(orderHistories.AsQueryable());

            //Act
            var result = service.GetCancelHistory(order.Id, orderItem.Id);

            //Assert
            result.Count().Should().Be(2);

        }

        [TestMethod]
        public void GetCancelHistory_OrderHistoryCancelIsNotInList_ShouldReturnEmpty()
        {
            //Arrange
            var service = CreateService();
            var order = FactoryGirl.Build<Order>();
            var orderHistory1 = FactoryGirl.Build<OrderHistory>(p =>
            {
                p.Id = 3;
                p.OrderId = 1;
                p.OrderItemId = 3;
                p.Action = OrderAction.Initiated;
            });

            var orderHistory2 = FactoryGirl.Build<OrderHistory>(p =>
            {
                p.Id = 3;
                p.OrderId = 1;
                p.OrderItemId = 3;
                p.Action = OrderAction.Failed;
            });

            var orderHistories = new List<OrderHistory>() { orderHistory1, orderHistory2 };

            var orderItem = FactoryGirl.Build<OrderItem>(p => { p.Id = 3; });

            _mockOrderHistoryRepository.Setup(x => x.GetList()).Returns(orderHistories.AsQueryable());

            //Act
            var result = service.GetCancelHistory(order.Id, orderItem.Id);

            //Assert
            result.Should().BeEmpty();

        }

        #region Private methods
        private OrderHistoryBusiness CreateService()
        {

            _mockOrderHistoryRepository = new Mock<IRepository<OrderHistory>>();
            _mockDataContext = new Mock<IEntitiesContext>();


            var result = new OrderHistoryBusiness(

               _mockOrderHistoryRepository.Object,
               _mockDataContext.Object
               );

            return result;
        }
        #endregion
    }
}
