﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;

namespace Jumbula.Web.Mvc.UIHelpers
{
    public static class UrlHelpers
    {
        #region UrlHelpers
        public static string ClubDomain(this UrlHelper url, string clubDomain, bool externalMode)
        {

            if (externalMode)
            {
                return "/";
            }
            else
            {
                return $"/{clubDomain}";
            }
        }
        public static string GetSubDomain(this Uri uri)
        {
            string result = null;

            var segments = uri.Host.Split('.');

            if (segments.Count() > 2)
            {
                if (segments[0].ToLower().Equals("www"))
                {
                    return null;
                }

                return segments[0];
            }

            return result;
        }
        public static bool HasSubDaomin(this Uri uri)
        {
            if (!String.IsNullOrEmpty(uri.GetSubDomain()) && !uri.AbsoluteUri.ToLower().Contains("cloudapp"))
            {
                return true;
            }

            return false;
        }
        public static string GetBaseUrl(this UrlHelper url, string clubDomain)
        {
            string result = String.Empty;

            string hostUrl = HttpContext.Current.Request.Url.Host;

            if (!String.IsNullOrEmpty(clubDomain) && !hostUrl.ToLower().Contains(clubDomain.ToLower()))
            {
                hostUrl = $"{clubDomain.ToLower()}.{hostUrl}";
            }

            result = $"{HttpContext.Current.Request.Url.Scheme}://{hostUrl}";

            return result;
        }
        public static string EventRegister(this UrlHelper url, string clubDomain, string seasonDomain, string eventDomain, int? userId, bool externalMode)
        {
            string result;

            if (externalMode)
            {
                result = $"/{seasonDomain}/{eventDomain}/Register";
            }
            else
            {
                result = $"/{clubDomain}/{seasonDomain}/{eventDomain}/Register";
            }

            if (userId.HasValue)
            {
                result = $"{result}?userId={userId}";
            }

            return result;
        }

        public static string EventDomain(this UrlHelper url, string seasonDomain, string eventDomain, bool viewOnly = false)
        {
            if (viewOnly)
            {
                return $"/{seasonDomain}/{eventDomain}/View";
            }

            return $"/{seasonDomain}/{eventDomain}";
        }
        public static string EventDomain(this UrlHelper url, string seasonDomain, string clubDomain, string eventDomain, bool viewOnly = false)
        {
            string hostUrl = HttpContext.Current.Request.Url.Host;
            hostUrl = hostUrl.Substring(hostUrl.IndexOf('.') + 1);

            if (!String.IsNullOrEmpty(clubDomain) && !hostUrl.ToLower().Contains(clubDomain.ToLower()))
            {
                hostUrl = $"{clubDomain.ToLower()}.{hostUrl}";
            }

            var baseUrl = $"{HttpContext.Current.Request.Url.Scheme}://{hostUrl}";

            string eventUrl;

            if (viewOnly)
            {
                eventUrl = $"{seasonDomain}/{eventDomain}/View";
            }
            else
            {
                eventUrl = $"{seasonDomain}/{eventDomain}";
            }

            var result = $"{baseUrl}/{eventUrl}";

            return result;
        }
        #endregion


        public static string GetClubLogoUrl(string domain, string logo, bool returnAbsolutePath = false)
        {
            string logoUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, domain, logo).AbsoluteUri;

            if (String.IsNullOrEmpty(logo) || !StorageManager.IsExist(logoUri))
            {
                logoUri = Constants.Club_NoSport_Default_Image_Large;

                if (returnAbsolutePath)
                {
                    logoUri =
                        $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Host}{logoUri}";
                }
            }

            var rnd = new Random(Environment.TickCount);

            logoUri += "?v=" + rnd.Next();

            return logoUri;
        }


        public static string GetProperUrl(ClubRedirectUrl clubRedirectUrl, string site, string requestUrl)
        {
            if (clubRedirectUrl == null)
            {
                if (String.IsNullOrEmpty(site))
                {
                    return requestUrl;
                }

                return site;
            }

            switch (clubRedirectUrl.RedirectUrlType)
            {
                case RedirectUrlTypes.JumbulaPage:
                    return requestUrl;
                case RedirectUrlTypes.OrganizationWebsite:
                    return site;
                case RedirectUrlTypes.Other:
                    return clubRedirectUrl.RedirectUrl;
                default:
                    return site;
            }
        }

        public static string GetOutSourceUrl(string host, string clubdomain, string seasonDomain, string programDomain)
        {
            var segments = host.Split('.');
            string returnUrl;
            if (segments.Count() > 2)
            {
                if (segments[0].ToLower().Equals("www"))
                {
                    returnUrl = String.Format("{0}.{2}.{3}", clubdomain, segments[1], segments[2]);
                }
                returnUrl = $"{clubdomain}.{segments[1]}.{segments[2]}";
            }
            else
            {
                returnUrl = $"{clubdomain}.{host}";
            }


            return $"https://{returnUrl}/{seasonDomain}/{programDomain}/View";
        }

        public static Uri HomeUrl()
        {
            var url = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Host}";

            return new Uri(url);
        }

        public static string GetBaseUrl(string clubDomain)
        {
            var url = HttpContext.Current.Request.Url;

            string hostUrl = url.Host;

            var subDomain = url.GetSubDomain();

            if (!String.IsNullOrEmpty(subDomain))
            {
                if (!subDomain.Equals(clubDomain, StringComparison.OrdinalIgnoreCase))
                {
                    hostUrl = hostUrl.Replace(subDomain, clubDomain);
                }
            }
            if (!String.IsNullOrEmpty(clubDomain) && !hostUrl.ToLower().Contains(clubDomain.ToLower()))
            {
                hostUrl = $"{clubDomain.ToLower()}.{hostUrl}";
            }

            var result = $"{HttpContext.Current.Request.Url.Scheme}://{hostUrl}";

            return result;
        }

        public static string ClubDomain(string clubDomain)
        {
            string basixUrl = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Host}";
            return $"{basixUrl}/{clubDomain}";
        }
    }
}