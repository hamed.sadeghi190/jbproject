﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('mainController', ['$scope', '$http', 'jbUtils', '$state', 'prototypeFunctionRegistrations', 'jbToast', 'scopeServise', '$timeout', 'jbConfirmation', function ($scope, $http, jbUtils, $state, prototypeFunctionRegistrations, jbToast, scopeServise, $timeout, jbConfirmation, trainingService) {
        window.app.globalObjects.initializeView();

        $scope.pageClass = function () { return window.app.globalObjects.pageClass; };

        var MC = this;
        scopeServise.set(MC);
        var SeasonUrl = window.location.origin + "/Dashboard/Season";

        MC.utils = jbUtils;

        var forEach = angular.forEach,
            extend = angular.extend,
            copy = angular.copy;

        MC.allowedActions = [];

        MC.getAllowedActions = function () {
            $http.get(window.location.origin + "/Dashboard/Home/GetAllowedActions").success(function (data, status, headers, config) {
                MC.allowedActions = data;
            });
        }

        MC.getSeasons = function () {
            $http.get(window.location.origin + '/dashboard/season/GetList').success(function (data, status, headers, config) {

                MC.archivedSeasons  = (data).filter(function (entry) {
                    return entry.Archived === true;
                });
                MC.seasons = (data).filter(function (entry) {
                    return entry.Archived === false;
                });
            });
        }

        $scope.unarchiveSeason = function (seasonDomain, seasonTitle) {

            jbConfirmation
            .title("Unarchive season")
                .yes(function () {
                    $http.get(SeasonUrl + '/UnArchive?seasonDomain=' + seasonDomain, { seasonDomain: seasonDomain})
                        .success(function (data, status, headers, config) {

                            if (data.Status == true) {
                                $scope.MC.getSeasons();
                                $scope.MC.currentSeason.Title = seasonTitle;
                                $scope.MC.stateParams.seasonDomain = seasonDomain;
                                $state.go('Season', { seasonDomain: seasonDomain});
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                })
                .confirm();
        };

        MC.getAllowedActions();

        MC.isAllowedAction = function (action) {

            for (var i = 0; i < MC.allowedActions.length; i++) {
                if (MC.allowedActions[i] == action) {
                    return true;
                }
            }

            return false;
        }

        MC.getSeasons();

        MC.models = {
            Season: {
                Id: 0,
                Title: null,
                Domain: null,
                ClubDomain: null,
                Description: null
            }
        };

        MC.steps = [];

        MC.tempModels = {};

        MC.addNewSeasonFromTempModels = function () {
            $state.go('Season', { seasonDomain: MC.tempModels.Season.Domain });
            var maxId = 0;
            forEach(MC.seasons, function (value, key) {
                if (value.Id > maxId)
                    maxId = value.Id;
            });
            MC.tempModels.Season.Id = ++maxId;
            MC.seasons.push(copy(MC.tempModels.Season));
            copy(MC.models.Season, MC.tempModels.Season);
        };

        MC.addNewSeasonFromExistingSeason = function () {
            forEach(MC.seasons, function (value, key) {
                if (value.Domain === MC.tempModels.Season.Domain)
                    MC.tempModels.Season = copy(value);
            });
            $state.go('Season', { seasonDomain: MC.tempModels.Season.Domain });
            var maxId = 0;
            forEach(MC.seasons, function (value, key) {
                if (value.Id > maxId)
                    maxId = value.Id;
            });
            MC.tempModels.Season.Id = ++maxId;
            MC.seasons.push(copy(MC.tempModels.Season));
            copy(MC.models.Season, MC.tempModels.Season);
        };

        MC.closeBlade = function (blade) {
            forEach(MC.steps, function (value, key) {
                if (value == blade)
                    MC.steps.splice(key, 1);
            });
        }

        MC.collapseAllBlades = function () {
            for (var i = 0; i < MC.steps.length; i++) {
                MC.steps[i].isExpanded = false;
            }
        }

        MC.closeAllBlades = function () {
            MC.steps = [];
        }
        //var changeBladeTasks = function () {  
        //    //hide Training Box start
        //    var settingTrainingBox = {
        //        show: false
        //    }
        //    trainingService.setting(settingTrainingBox);
        //    //hide Training Box End
        //}
        //angular.element("#Overview,#Help").on('click', function (e) {
        //    //hide Training Box start
        //    var settingTrainingBox = {
        //        show: false
        //    };
        //    trainingService.setting(settingTrainingBox);
        //    //hide Training Box End
        //});
        MC.openBlade = function (blade) {
            //changeBladeTasks();
            blade = extend({ url: null, isExpanded: true, order: 9, menu: null }, blade);
            var found = false, oldIndex;
            if (MC.steps[0] && (MC.steps[0].menu != blade.menu))
                MC.steps = [];
            else
                forEach(MC.steps, function (value, key) {
                    if (value.order > blade.order)
                        MC.steps.removeByIndex(key);
                    if (value.url == blade.url) {
                        found = true;
                        oldIndex = key;
                    } else
                        value.isExpanded = false;
                });
            if (found) {
                MC.steps[oldIndex].isExpanded = true;
                MC.steps.moveByIndex(oldIndex, MC.steps.length - 1);
            }
            else
                MC.steps.push(blade);
        }

        MC.signOut = function () {
            $http.post("/Account/LogOff").success(function () {
                window.location = "/";
            });
        }

        MC.notification = {}

        MC.notification.defaultOptions = {
            position: {
                pinned: true,
                top: 30,
                right: 30
            },
            animation: {
                open: {
                    effects: "slideIn:left"
                },
                close: {
                    effects: "slideIn:left",
                    reverse: true
                }
            },
            autoHideAfter: 10000,
            stacking: "down",
            templates: [{
                type: "info",
                template: '<div class="toast toast-info"><i class="toast-close-button jbi-remove"></i>\
                              <div>\
                                <span class="toast-title">#= Title # </span>\
                                <p class="toast-message">#= Message #</p>\
                              </div>\
                            </div>'
            }, {
                type: "error",
                template: '<div class="toast toast-error"><i class="toast-close-button jbi-remove"></i>\
                              <div>\
                                <span class="toast-title">#= Title # </span>\
                                <p class="toast-message">#= Message #</p>\
                              </div>\
                            </div>'
            }, {
                type: "success",
                template: '<div class="toast toast-success"><i class="toast-close-button jbi-remove"></i>\
                              <div>\
                                <span class="toast-title">#= Title # </span>\
                                <p class="toast-message">#= Message #</p>\
                              </div>\
                            </div>'
            }, {
                type: "warning",
                template: '<div class="toast toast-warning"><i class="toast-close-button jbi-remove"></i>\
                              <div>\
                                <span class="toast-title">#= Title # </span>\
                                <p class="toast-message">#= Message #</p>\
                              </div>\
                            </div>'
            }]

        };

        var notification = $("#notification").kendoNotification(extend(MC.notification.defaultOptions)).data("kendoNotification");
        MC.notification.error = function (message, title, options) {
            extend(notification.options, options || {});
            notification.show({
                Title: title,
                Message: message
            }, 'error');
        };

        MC.notification.info = function (message, title, options) {
            extend(notification.options, options || {});
            notification.show({
                Title: title,
                Message: message
            }, 'info');
        };

        MC.notification.success = function (message, title, options) {
            extend(notification.options, options || {});
            notification.show({
                Title: title,
                Message: message
            }, 'success');
        };

        MC.notification.warning = function (message, title, options) {
            extend(notification.options, options || { autoHideAfter: 20000});
            notification.show({
                Title: title,
                Message: message
            }, 'warning');
        };

        var hiddenParameters = {};
        MC.setHiddenParam = function (key, value) {
            key = "HiddenParam_" + key;
            if (is.not.object(value))
                throw "You can send an Object as parameter.";
            sessionStorage[key] = JSON.stringify(value);
            return hiddenParameters[key] = value;
        };

        MC.popHiddenParam = function (key) {
            var result;
            if (is.not.undefined(hiddenParameters[key]) && is.not.null(hiddenParameters[key])) {
                result = hiddenParameters[key];
                hiddenParameters[key] = null;
            } else {
                result = JSON.parse(sessionStorage[key]);
            }
            //localStorage.removeItem(Key);
            return result;
        }

        MC.backQueueItems = [];

        $state.forward = function (to, toParams, from, fromParams, callBack) {

            var stackItem = {
                from: from ? from : $state.current.name,
                fromParams: fromParams ? fromParams : scope.MC.stateParams,
                callBack: callBack
            };

            MC.backQueueItems.push(stackItem);

            $state.go(to, toParams);

            if (is.not.null(MC.backQueueItems) && MC.backQueueItems.length == 1) {
                var confirmExit = function () {
                    return "You have attempted to leave this page. It means you may lose some data!\n Are you sure?";
                }
                window.onbeforeunload = confirmExit;
            }
        }

        $state.returnByBack = function () {
            if (is.not.null(MC.backQueueItems) && MC.backQueueItems.length > 0) {
                var lastStack = MC.backQueueItems[MC.backQueueItems.length - 1];

                return lastStack.from == $state.current.name;
            }

            return false;
        }

        $state.back = function () {
            var to,
                toParams,
                stateStack = MC.backQueueItems[MC.backQueueItems.length - 1];

            to = stateStack.from;
            toParams = stateStack.fromParams;

            $state.go(to, toParams);

            stateStack.callBack();

            if (is.not.null(MC.backQueueItems) && MC.backQueueItems.length < 1) {
                window.onbeforeunload = null;
            }
        };

        MC.updateFormErrors = function (scope, errors) {

            scope.errors = {};
            scope.errors.formErrors = {};

            if (errors) {
                for (var i = 0; i < errors.length; i++) {
                    scope.errors.formErrors[errors[i].Key] = errors[i];
                    scope.errors.formErrors[errors[i].Key].Messages = errors[i].Messages;
                }
            }
        }

        MC.handleErrors = function (scope, data) {
            if (data.Message) {
                jbToast.error(data.Message);
            } else if (data.FormErrors && data.FormErrors.length > 0) {
                MC.updateFormErrors(scope, data.FormErrors);
            } else {
                jbToast.error("An unexpected error has occurred.");
            }
        };

        MC.datePickerOptions = {
            format: 'dd MMM yyyy',
            parseFormats: ["yyyy-MM-ddTHH:mm:ss"]
        }

        MC.dateTimePickerOptions = {
            format: 'dd MMM yyyy h:mm tt',
            parseFormats: ["yyyy-MM-ddTHH:mm:ss"]
        }

        MC.datePickerOptions2 = {
            format: 'MM/dd/yyyy',
            parseFormats: ["yyyy-MM-ddTHH:mm:ss"]
        }

        MC.timePickerOptions = {
            format: 'h:mm tt',
            min: new Date(0, 0, 0, 6, 0, 0),
            max: new Date(0, 0, 0, 23, 45, 0),
            interval: 15
        }
        MC.timePickerOptionsAllTime5MinInterval = {
            format: 'h:mm tt',
            min: new Date(0, 0, 0, 0, 0, 0),
            max: new Date(0, 0, 0, 24, 0, 0),
            interval: 5
        }
        MC.timePickerOptionsAllTime5MinInterval59 = {
            format: 'h:mm tt',
            max: new Date(0, 0, 0, 23, 59, 0),
            interval: 5
        }
        MC.timePickerOptions5MinInterval = {
            format: 'h:mm tt',
            min: new Date(0, 0, 0, 6, 0, 0),
            max: new Date(0, 0, 0, 24, 0, 0),
            interval: 5
        }
        MC.timePickerOptionsAM = {
            format: 'h:mm tt',
            min: new Date(0, 0, 0, 6, 0, 0),
            max: new Date(0, 0, 0, 12, 0, 0),
            interval: 5
        }
        MC.timePickerOptionsPM = {
            format: 'h:mm tt',
            min: new Date(0, 0, 0, 12, 0, 0),
            max: new Date(0, 0, 0, 24, 0, 0),
            interval: 5
        }
        MC.timePickerOptionsPMInterval30 = {
            format: 'h:mm tt',
            min: new Date(0, 0, 0, 12, 0, 0),
            max: new Date(0, 0, 0, 23, 30, 0),
            interval: 30
        }
        MC.joinStringsWithBreak = function (errorMessages) {
            return errorMessages ? errorMessages.join("<br>") : "";
        }

        MC.isEmpty = function (object) {
            return object ? is.not.empty(object) : false;
        }

        MC.dateTimePickerOpen = function (e) {
            try {
                $(e.currentTarget).data("kendoDateTimePicker").open();
            }
            catch (ex) {
                try {
                    $(e.currentTarget).data("kendoDatePicker").open();
                }
                catch (ex) {
                    try {
                        $(e.currentTarget).data("kendoTimePicker").open();
                    }
                    catch (ex) {
                        throw "No kendo picker found to open";
                    }
                }
            }
        };

        MC.dateTimePickerClose = function (e) {
            try {
                $(e.currentTarget).data("kendoDateTimePicker").close();
            }
            catch (ex) {
                try {
                    $(e.currentTarget).data("kendoDatePicker").close();
                }
                catch (ex) {
                    try {
                        $(e.currentTarget).data("kendoTimePicker").close();
                    }
                    catch (ex) {
                        throw "No kendo picker found to close";
                    }
                }
            }
        };

        MC.account = {};

        MC.getClubbaseInfo = function () {
            $http.get(window.location.origin + "/Dashboard/Club/GetClubInfo").success(function (data, status, headers, config) {

                var model = data;

                MC.account.Id = model.Id;
                MC.account.name = model.Name;
                MC.account.domain = model.Domain;
                MC.account.logo = model.Logo;
                MC.account.singOut = "/Account/LogOff";
                MC.account.hasPartner = model.HasPartner;
                MC.account.isPartner = model.IsPartner;
                MC.account.partnerLogo = model.PartnerLogo;
                MC.account.Role = model.Role;
                MC.account.ShowWelcomeToJumbula = model.ShowWelcomeToJumbula;
                MC.account.ShowSchoolsInHome = model.ShowSchoolsInHome;
                MC.Tour = model.Tour;

                if (model.IsPartner) {
                    MC.PortalParameters = model.PortalParameters;
                }

                MC.IsMultipleClubAdmin = model.IsMultipleClubAdmin;
                MC.Clubs = model.Clubs;

                $timeout(function () {
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                })
            });
        }


        MC.getMessagesSummary = function () {

            $http.get(window.location.origin + "/Message/GetMessagesSummary").success(function (data, status, headers, config) {

                MC.Unread = data.Unread;
            });
        }

        MC.getClubbaseInfo();
        MC.getMessagesSummary();

        var chat = $.connection.signalRHub;

        $.connection.hub.start();

        chat.client.refreshSignal = function (messageResponse) {

            $scope.refreshRecentMessages();

            MC.updateMessages(messageResponse);

            if (messageResponse.RefreshCurrentMessage) {

                MC.getChat(MC.stateParams.chatId);
            }

            var messageBox = angular.element(document.querySelector('#messageBox'));

            if (messageBox != null && messageBox != 'undefined') {
                messageBox[0].scrollTop = messageBox[0].scrollHeight;
            }
        };



        MC.updateMessages = function (messageResponse) {

            if (MC.stateParams.chatId) {

                for (var i = 0; i < messageResponse.Chats.length; i++) {

                    if (messageResponse.Chats[i].ChatId == MC.stateParams.chatId) {

                        if (MC.CurrentChat) {
                            for (var k = 0; k < messageResponse.Chats[i].Messages.length; k++) {
                                //if (messageResponse.Chats[i].Messages[k].RecieverClubId == MC.account.Id) {

                                MC.CurrentChat.Messages.push(messageResponse.Chats[i].Messages[k]);
                                //}
                            }
                        }
                    }
                }
            }

            for (var i = 0; i < messageResponse.Chats.length; i++) {

                for (var m = 0; m < MC.Chats.length; m++) {
                    if (messageResponse.Chats[i].ChatId == MC.Chats[m].ChatId) {
                        messageResponse.Chats[i].IsAlreadyExist = true;
                        MC.Chats[m].IsUnread = true;
                        MC.Chats[m].DateTimeTicks = messageResponse.Chats[i].DateTimeTicks;
                        MC.Chats[m].DateTime = messageResponse.Chats[i].DateTime;
                        MC.Chats[m].Unread = messageResponse.Chats[i].Unread;
                    }
                }

                if (!messageResponse.Chats[i].IsAlreadyExist && messageResponse.TargetClubDomain == MC.account.domain) {
                    MC.Chats.push(messageResponse.Chats[i]);
                }
            }

            MC.Unread = messageResponse.Unread;

            $scope.$apply();
        }

        MC.getChat = function (chatId) {
            $http.get('/Message/GetChat?chatId=' + chatId).success(function (data, status, headers, config) {

                MC.CurrentChat = data;
                MC.Unread = data.Unread;

                $scope.setCurrentMessageAsRead();
            })

            setTimeout(function () {
                var messageBox = angular.element(document.querySelector('#messageBox'));

                if (messageBox != null && messageBox != 'undefined') {
                    messageBox[0].scrollTop = messageBox[0].scrollHeight;
                }
            }, 1000);
        };

        MC.bladeIsWide = function () {
            if ($state.current.name == 'Messaging' || $state.current.name == 'MessagingInbox') {
                return true;
            }

            return false;
        }

        $scope.setCurrentMessageAsRead = function () {

            var currentChatId = MC.stateParams.chatId;

            if (MC.Chats != null && MC.Chats != 'undefined') {
                for (var i = 0; i < MC.Chats.length; i++) {
                    if (MC.Chats[i].ChatId == currentChatId) {
                        MC.Chats[i].IsUnread = false;
                        MC.Chats[i].Unread = 0;
                    }
                }
            }
        }

        $scope.refreshRecentMessages = function () {

            var messagesGrid = $("#messagesGrid");

            if (messagesGrid.length > 0) {

                messagesGrid.data("kendoGrid").dataSource.read();
            }
        }

    }]);
})();