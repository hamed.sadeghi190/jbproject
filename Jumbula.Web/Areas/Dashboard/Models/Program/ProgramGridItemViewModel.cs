﻿using System;
using System.Collections.Generic;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Charge;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramGridItemViewModel
    {
        public long ProgramId { get; set; }
        public string Title { get; set; }
        public int CountClassFees { get; set; }
        public string Start { get; set; }

        public string End { get; set; }

        public string SecondStart { get; set; }

        public string SecondEnd { get; set; }

        public string SecondStartEndTime { get; set; }
        public int TotalCapacity { get; set; }
        public List<string> Programs { get; set; }

        public bool EnablePopUp { get; set; }
        public bool EnabledRegistrationPopup { get; set; }
        public string GenderRestrictions { get; set; }
        public string CalendarScheduleMinGrade { get; set; }
        public string CalendarScheduleMaxGrade { get; set; }
        public int CalendarTaskId { get; set; }
        public bool CalendarIsAllDay { get; set; }
        public List<JbPScheduleViewModel> Schedules { get; set; }
        public JbPageProgramLinkType ProgramRegBtnLinkTo { get; set; }

        public string StrSchedules
        {
            get
            {
                return ((int)DayOfWeek) + DayOfWeek.ToString();
            }
        }
        public string RegistrationPeriod { get; set; }
        public string Dates { get; set; }
        public string RegistrationDate { get; set; }
        public string GradeAge { get; set; }

        public string ProgramType { get; set; }
        public TimeSpan Time { get; set; }
        public int Order { get; set; }
        public int StartOrder { get; set; }
        public int EndOrder { get; set; }
        public string ProgramRegStatus { get; set; }

        public List<KeyValuePair<string, string>> CapacityLeft { get; set; }

        public string Domain { get; set; }

        public string SeasonDomain { get; set; }
        public List<ChargeItemViewModel> Classfee { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public string IntDayOfWeek => ((int)DayOfWeek) + DayOfWeek.ToString();
        public string StartEndTime { get; set; }

        public string DisplayUrl { get; set; }

        public string SingleRegisterUrl { get; set; }
        public string RegisterUrl { get; set; }
        public string Room { get; set; }
        public List<string> Instructors { get; set; }

        public string Provider { get; set; }
        public string Location { get; set; }
        public List<string> Capacity { get; set; }
        public string ProgramRegBtnBackgroundColor { get; set; }
        public string ProgramRegBtnLabelColor { get; set; }
        public string ProgramRegBtnTitle { get; set; }
        public string PMAM { get; set; }
        public string DaysTimes
        {
            get
            {
                return  Schedules != null ? Schedules[0].strDays : string.Empty ;
            }

        }
        public string CalenderProgramTextColor { get; set; }
        public string CalenderProgramBackgroundColor { get; set; }
     
    }
}