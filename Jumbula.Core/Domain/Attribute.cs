﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class JbAttribute : BaseEntity
    {
        public AttributeName Name { get; set; }

        public string Label { get; set; }

        public AttributeType Type { get; set; }

        public AttributeReferenceType ReferenceType { get; set; }

        public bool ShowInGrid { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsRequired { get; set; }

        public int ClubId { get; set; }

        [ForeignKey("ClubId")]
        public virtual Club Club { get; set; }
    }


    public class JbAttributeOption : BaseEntity
    {
        public string Value { get; set; }

        public int AttributeId { get; set; }

        [ForeignKey("AttributeId")]
        public virtual JbAttribute Attribute { get; set; }
    }

    public enum AttributeReferenceType : byte
    {
        Club,
        ContactPerson,
        Program
    }

    public enum AttributeName : byte
    {
        Custom = 0,
        PTAFee = 1,
        LeadGeneration = 2,
    }

    public enum AttributeType : byte
    {
        Text,
        LongText,
        Currency,
        Percent,
        Email,
        Phone,
        YesNo,
        DropDown
    }
}
