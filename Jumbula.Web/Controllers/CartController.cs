﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Exceptions;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    [Authorize(Roles = "Parent, Support, Admin, Owner, Manager")]
    public class CartController : JbBaseController
    {
        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private readonly ICartBusiness _cartBusiness;
        private readonly IClientBusiness _clientBusiness;
        private readonly IUserCreditCardBusiness _userCreditCardBusiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;
        private readonly IOrderBusiness _orderBusiness;
        private readonly IRegisterBusiness _registerBusiness;
        #endregion

        #region Constractor
        public CartController(IClientBusiness clientBusiness, IUserCreditCardBusiness userCreditCardBusiness, IClubBusiness clubBusiness, ITransactionActivityBusiness transactionActivityBusiness, ICartBusiness cartBusiness, IOrderBusiness orderBusiness, IRegisterBusiness registerBusiness)
        {
            _clubBusiness = clubBusiness;
            _cartBusiness = cartBusiness;
            _clientBusiness = clientBusiness;
            _orderBusiness = orderBusiness;
            _userCreditCardBusiness = userCreditCardBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
            _registerBusiness = registerBusiness;
        }
        #endregion

        [ChildActionOnly]
        public virtual PartialViewResult CartSummary()
        {
            ShoppingCartViewModel model = null;

            try
            {
                var cart = _cartBusiness.Get(this.GetCurrentUserId(), this.GetCurrentClubId());

                model = new ShoppingCartViewModel()
                {
                    Total = cart.Total,
                    Count = cart.Count,
                    Currency = cart.Currency
                };
            }
            catch (JumbulaCartSessionExpiredException)
            {
                model = new ShoppingCartViewModel()
                {
                    Total = 0,
                    Count = 0,
                    Currency = CurrencyCodes.USD
                };
            }

            return PartialView("_ShoppingCart", model);
        }


        public virtual ActionResult Index(int? userId, int? invoiceId, string returnUrl, bool? backToDetail, JumbulaSubSystem? actionCaller, string message, string token, bool isNotValid = false, bool isFamilyTakePayment = false)
        {

            //try
            //{
            var clubBusiness = _clubBusiness;
            actionCaller = actionCaller.HasValue ? actionCaller.Value : JumbulaSubSystem.Order; //when url was just /cart 

            var displayCartViewModel = new DisplayCartViewModel();
            displayCartViewModel.PaymentModel = new StripePaymentModels();

            //Fill dropdown month and Year
            ViewBag.MonthList = DateTimeHelper.GetMonthForDropdownlist();
            ViewBag.Year2CharList = DateTimeHelper.GetYearForDropdownlist();

            #region Check Error in Cart
            if (!string.IsNullOrEmpty(message))
            {

            }

            var name = "";
            //get cookie
            if (userId == null && invoiceId == null)
            {
                name = token;
            }
            else if (userId.HasValue)
            {
                name = userId.ToString();
            }
            else if (invoiceId.HasValue)
            {
                name = invoiceId.ToString();
            }

            var jsonError = Request.Cookies.Get(name);

            if (isNotValid)
            {

                if (jsonError != null && !string.IsNullOrWhiteSpace(jsonError.Value))
                {
                    var decriptedJson = StringCipher.Decrypt(jsonError.Value, DateTime.UtcNow.ToShortDateString().Replace("/", ""));
                    var dictionary = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(decriptedJson);

                    var Data = dictionary["Data"];
                    var errors = dictionary["Errors"];

                    if (Data != null)
                    {
                        var year = Data["Year2Char"];
                        displayCartViewModel.PaymentModel.CardHolderName = Data["CardHolderName"];
                        displayCartViewModel.PaymentModel.CardNumber = Data["CardNumber"];
                        displayCartViewModel.PaymentModel.Month = Data["Month"];
                        displayCartViewModel.PaymentModel.Year2Char = int.Parse(year);
                        displayCartViewModel.PaymentModel.CVV = Data["Cvv"];

                        displayCartViewModel.PaymentModel.ZipCode = Data["ZipCode"];
                        displayCartViewModel.PaymentModel.Street = Data["Street"];
                        displayCartViewModel.PaymentModel.City = Data["City"];
                    }

                    foreach (var item in errors)
                    {
                        ModelState.AddModelError(item.Key, item.Value);
                    }
                }

            }
            else
            {
                if (jsonError != null && !string.IsNullOrWhiteSpace(jsonError.Value))
                {
                    var decriptedJson = StringCipher.Decrypt(jsonError.Value, DateTime.UtcNow.ToShortDateString().Replace("/", ""));
                    var dictionary = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, string>>>(decriptedJson);

                    var Data = dictionary["Data"];
                    var errors = dictionary["Errors"];

                    //delete cookie
                    jsonError.Expires = DateTime.Now.AddMinutes(30);

                    if (Data != null)
                    {
                        Response.Cookies[name].Expires = DateTime.Now.AddDays(-1);
                    }
                    if (errors != null)
                    {
                        Response.Cookies[name].Expires = DateTime.Now.AddDays(-1);
                    }
                }
            }

            #endregion

            Club club = null;

            club = _clubBusiness.Get(this.GetCurrentClubId());

            if (actionCaller.HasValue && actionCaller == JumbulaSubSystem.MakeaPayment && club.ClubType.EnumType == ClubTypesEnum.Partner)
            {
                var partnerSetting = _clubBusiness.GetPartnerSetting(club.Id);

                if (partnerSetting != null && partnerSetting.PortalParameters.ReadStripeSettingsFromMember)
                {
                    var transactions = _transactionActivityBusiness.GetDraftByToken(token);

                    var transaction = transactions != null && transactions.Any() ? transactions.FirstOrDefault() : null;

                    club = transaction != null ? transaction.Order.Club : _clubBusiness.Get(this.GetCurrentClubId());
                }
            }

            ClientPaymentMethod clientPaymentMethod = null;

            if (club.PartnerId.HasValue && club.PartnerClub != null && club.IsSchool && (club.Domain.Equals("bc-alx-georgemason", StringComparison.OrdinalIgnoreCase) || (club.Domain.Equals("bc-alx-barrett", StringComparison.OrdinalIgnoreCase)) || (club.Domain.Equals("bc-arl-claremont", StringComparison.OrdinalIgnoreCase)) || (club.Domain.Equals("bc-alx-macarthur", StringComparison.OrdinalIgnoreCase)) || (club.Domain.Equals("bc-arl-arlingtonsciencefocus", StringComparison.OrdinalIgnoreCase)) || club.Domain.Equals("bc-arl-campbell", StringComparison.OrdinalIgnoreCase)))
            {
                var client = _clientBusiness.Get(club.Id);
                clientPaymentMethod = client.PaymentMethods;
            }
            else
            {
                clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId);
            }

            displayCartViewModel.ClientPaymentMethods = clientPaymentMethod;
            displayCartViewModel.ClientName = club.PartnerClub != null ? $"{club.PartnerClub.Name} ({club.Name})" : club.Name;
            displayCartViewModel.EnableExpressPaymentInOfflineMode = _clubBusiness.IsExpressPaymentEnabledInOfflineMode(club);

            var roleType = this.GetCurrentUserRoleType();

            switch (actionCaller)
            {
                case JumbulaSubSystem.Order:
                    {

                        #region order

                        var cartBusiness = _cartBusiness;
                        var cart = cartBusiness.Get(userId ?? this.GetCurrentUserId(), club.Id);

                        cartBusiness.ManipulatePreregisterationStutus(cart);
                        cartBusiness.ManipulateLotteryStutus(cart);

                        Season season = null;

                        if (cart.Order.PreregisterItems.Any())
                        {
                            season = cart.Order.PreregisterItems.First().Season;

                            var opentDate = Ioc.SeasonBusiness.GetGeneralRegOpenDate(season);

                            var clubTimeZone = season.Club.TimeZone;

                            var clubDateTime = DateTimeHelper.ConvertLocalDateTimeToUtc(opentDate.Value, clubTimeZone); // _clubBusiness.GetClubDateTime(season.ClubId, opentDate.Value);
                            displayCartViewModel.PreRegisterTime = clubDateTime;
                            displayCartViewModel.GeneralOpenDate = opentDate;
                        }

                        if (cart.Order.LotteryItems.Any())
                        {
                            season = cart.Order.LotteryItems.First().Season;

                            var lotteryCloseDate = Ioc.SeasonBusiness.GetLotteryCloseDate(season);

                            displayCartViewModel.LotteryCloseDate = lotteryCloseDate;
                        }

                        var user = Ioc.UserProfileBusiness.Get(userId ?? this.GetCurrentUserId());

                        var payableAmount = _orderBusiness.CalculateOrderPayableAmount(cart.Order);

                        if (cart.Count == 0)
                        {
                            displayCartViewModel.PaymentModel.ActionCaller = JumbulaSubSystem.Order;
                            displayCartViewModel.Cart.IsEmpty = true;

                        }

                        if (cart.Order != null && cart.Order.RegularOrderItems.Any() || cart.Order.PreregisterItems.Any() || cart.Order.LotteryItems.Any())
                        {
                            var currentUserId = this.GetCurrentUserId();

                            if (userId.HasValue && !_cartBusiness.IsCartBelongsToUser(currentUserId, cart.Order.UserId, this.IsUserAdminForClub(cart.Order.Club.Domain, currentUserId)))
                            {
                                return View(displayCartViewModel);
                            }

                            _orderBusiness.UpdateLiveTestMode(cart.Order);

                            displayCartViewModel.AvailableCredit = cart.Order.IsLive ? clubBusiness.GetUserCredit(cart.Order.Club, user.Id, false) : clubBusiness.GetUserCredit(cart.Order.Club, user.Id, true);

                            displayCartViewModel.EnableDonation = club.Client.EnableCartDonation;

                            displayCartViewModel.DonationMessage = club.Client.DonationMessage;
                            displayCartViewModel.DonationDescription = club.Client.DonationDescription != null ? club.Client.DonationDescription : string.Empty;


                            displayCartViewModel.PaymentModel.Currency = cart.Order.Club.Currency;
                            displayCartViewModel.PaymentModel.ConfirmationId = cart.Order.ConfirmationId;
                            displayCartViewModel.PaymentModel.UserId = cart.Order.UserId;
                            displayCartViewModel.PaymentModel.PayableAmount = payableAmount;
                            displayCartViewModel.PaymentModel.IsTestMode = (!cart.Order.IsLive || clientPaymentMethod.IsCreditCardTestMode);
                            displayCartViewModel.PaymentModel.ActionCaller = JumbulaSubSystem.Order;
                            displayCartViewModel.PaymentModel.OrderId = cart.Order.Id;


                            displayCartViewModel.ClientPaymentMethods = clientPaymentMethod;

                            displayCartViewModel.EnablePayByCash = (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard) ? true : displayCartViewModel.ClientPaymentMethods.EnableCashPayment;

                            CartCalculation(cart, false, displayCartViewModel.EnableCreditCardPayment);
                            displayCartViewModel.Cart = cart;

                            cart.PayableAmount = _orderBusiness.CalculateOrderPayableAmount(cart.Order);
                            displayCartViewModel.PaymentModel.PayableAmount = cart.PayableAmount;

                            if (club.Setting != null)
                            {
                                displayCartViewModel.WebSiteUrl = UrlHelpers.GetProperUrl(club.Setting.BrowseMoreRedirectUrl, club.Site, Request.Url.GetLeftPart(UriPartial.Authority));
                                displayCartViewModel.HideProgramTime = club.Setting.AppearanceSetting.HideProgramDatesInCart;
                                displayCartViewModel.ShowCombinedPriceOnlyForClub = club.Setting.ShowCombinedPriceOnly;
                                displayCartViewModel.HideCoupon = club.Setting.HideCouponInCart;
                                displayCartViewModel.HiddenPricePrograms = club.Setting.ClubProgram.EnablePriceHidden ? club.Setting.ClubProgram.HiddenPricePrograms : null;
                                displayCartViewModel.HideAllProgramsPrice = club.Setting.ClubProgram.EnablePriceHidden ? club.Setting.ClubProgram.HideAllProgramsPrice : false;

                                if (club.PartnerId.HasValue)
                                {
                                    var partner = club.PartnerClub;
                                }
                            }
                            else
                            {
                                displayCartViewModel.WebSiteUrl = UrlHelpers.GetProperUrl(new ClubRedirectUrl(), club.Site, Request.Url.GetLeftPart(UriPartial.Authority));
                            }
                            if (club.Setting == null || club.Setting.BrowseMoreText == null || club.Setting.BrowseMoreText.IsNullOrWhiteSpace())
                            {
                                displayCartViewModel.WebSiteText = "Select classes for a new participant";
                            }
                            else
                            {
                                displayCartViewModel.WebSiteText = club.Setting.BrowseMoreText;
                            }

                            if (club.Setting == null || club.Setting.AnotherRegisterButtonText == null || club.Setting.AnotherRegisterButtonText.IsNullOrWhiteSpace())
                            {
                                displayCartViewModel.RegisterSameProgramText = "Register for same program";
                            }
                            else
                            {
                                displayCartViewModel.RegisterSameProgramText = club.Setting.AnotherRegisterButtonText;
                            }

                            if (cart.Order.RegularOrderItems.Any())
                            {
                                CheckCartItemValidation(cart.Order.RegularOrderItems);

                                var errors = new Dictionary<long, string>();
                                var validationResult = _cartBusiness.CheckCartItemsValidation(cart, ref errors);

                                if (!validationResult)
                                {
                                    foreach (var item in cart.Order.RegularOrderItems)
                                    {
                                        item.ValidationMessages = errors.Where(e => e.Key == item.Id).Select(e => e.Value).ToList();
                                    }
                                }

                                displayCartViewModel.IsNotValid = !validationResult;

                            }
                        }
                        else if (this.GetCurrentRequestType() == RequestType.Branded)
                        {
                            var externalClub = _clubBusiness.Get(ClubSubDomain);

                            if (externalClub.Setting != null)
                            {
                                displayCartViewModel.WebSiteUrl = UrlHelpers.GetProperUrl(externalClub.Setting.BrowseMoreRedirectUrl, externalClub.Site, Request.Url.GetLeftPart(UriPartial.Authority));
                            }
                            else
                            {
                                displayCartViewModel.WebSiteUrl = UrlHelpers.GetProperUrl(new ClubRedirectUrl(), externalClub.Site, Request.Url.GetLeftPart(UriPartial.Authority));
                            }
                        }

                        if (string.IsNullOrEmpty(displayCartViewModel.WebSiteUrl) && cart.Order != null)
                        {
                            club = _clubBusiness.Get(club.Id);
                            displayCartViewModel.WebSiteUrl = club.Site;
                        }

                        displayCartViewModel.UserId = userId;
                        #endregion

                        switch (roleType)
                        {
                            case RoleCategoryType.Dashboard:
                                {
                                    try
                                    {
                                        if (cart.Order != null && cart.Order.RegularOrderItems.Any())
                                        {
                                            displayCartViewModel.PaymentModel.IsPreferrd = false;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogHelper.LogException(ex);
                                    }
                                }
                                break;
                            case RoleCategoryType.Parent:
                                {
                                    #region Fill CreditCards For Express
                                    var paymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
                                    var clubId = _clubBusiness.GetClubByClientId(paymentMethod.Id).Id;

                                    displayCartViewModel.CreditCards = _userCreditCardBusiness.GetKeyValueUserCreditCards(this.GetCurrentUserId(), clubId);

                                    #endregion

                                    try
                                    {
                                        if (cart.Order != null && cart.Order.RegularOrderItems.Any())
                                        {
                                            displayCartViewModel.PaymentModel.IsPreferrd = _userCreditCardBusiness.GetUserCreditCards(this.GetCurrentUserId(), clubId).Any(c => c.IsDefault);
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        LogHelper.LogException(ex);
                                    }
                                }
                                break;
                            default:
                                {
                                    throw new NotSupportedException("Role Category Type default Not Supported in Installmet");
                                }
                        }

                    }
                    break;
                case JumbulaSubSystem.Installment:
                    {
                        #region Installment
                        List<OrderInstallment> installment = null;

                        if (!string.IsNullOrEmpty(token))
                        {
                            installment = Ioc.InstallmentBusiness.GetListByToken(token).ToList();

                            if (installment != null && installment.Count > 0)
                            {
                                club = _clubBusiness.Get(installment.FirstOrDefault().OrderItem.Order.ClubId);
                                var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                                displayCartViewModel.ClientPaymentMethods = clinetPaymentMethod;
                                displayCartViewModel.PaymentModel.Currency = club.Currency;
                                displayCartViewModel.PaymentModel.ConfirmationId = token;
                                displayCartViewModel.PaymentModel.PayableAmount = installment.Sum(c => c.Balance);
                                displayCartViewModel.PaymentModel.IsTestMode = !installment.First().OrderItem.Order.IsLive || clinetPaymentMethod.IsCreditCardTestMode;
                                displayCartViewModel.PaymentModel.ActionCaller = JumbulaSubSystem.Installment;

                                displayCartViewModel.CreditCards = _userCreditCardBusiness.GetKeyValueUserCreditCards(installment.FirstOrDefault().OrderItem.Order.UserId, club.Id);


                                displayCartViewModel.PaymentModel.IsPreferrd = _userCreditCardBusiness.GetUserCreditCards(installment.FirstOrDefault().OrderItem.Order.UserId, club.Id).Any(c => c.IsDefault);

                            }
                        }
                        #endregion

                        switch (roleType)
                        {
                            case RoleCategoryType.Dashboard:
                                {
                                    try
                                    {
                                        if (!string.IsNullOrEmpty(token))
                                        {
                                            if (installment != null && installment.Count > 0)
                                            {
                                                displayCartViewModel.PaymentModel.IsPreferrd = _userCreditCardBusiness.GetUserCreditCards(installment.FirstOrDefault().OrderItem.Order.UserId, club.Id).Any(c => c.IsDefault);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogHelper.LogException(ex);
                                    }
                                }
                                break;
                            case RoleCategoryType.Parent:
                                {
                                    #region Fill CreditCards For Express


                                    var paymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
                                    var clubId = _clubBusiness.GetClubByClientId(paymentMethod.Id).Id;

                                    displayCartViewModel.CreditCards = _userCreditCardBusiness.GetKeyValueUserCreditCards(this.GetCurrentUserId(), clubId);


                                    #endregion
                                    try
                                    {

                                        if (!string.IsNullOrEmpty(token))
                                        {
                                            if (installment != null && installment.Count > 0)
                                            {
                                                displayCartViewModel.PaymentModel.IsPreferrd = _userCreditCardBusiness.GetUserCreditCards(this.GetCurrentUserId(), clubId).Any(c => c.IsDefault);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        LogHelper.LogException(ex);
                                    }
                                }
                                break;
                                {
                                    throw new NotSupportedException("Role Category Type default Not Supported in Installmet");
                                }
                        }
                    }
                    break;
                case JumbulaSubSystem.MakeaPayment:
                    {
                        #region makeapayment

                        var isOrderTestMode = true;
                        var transactionactivities = _transactionActivityBusiness.GetDraftByToken(token).ToList();
                        var firstTransaction = transactionactivities.First();


                        if (firstTransaction.OrderId.HasValue)
                        {
                            isOrderTestMode = !firstTransaction.Order.IsLive;
                        }
                        else if (firstTransaction.OrderItemId.HasValue)
                        {
                            isOrderTestMode = !firstTransaction.OrderItem.Order.IsLive;
                        }

                        if (transactionactivities != null && transactionactivities.Count > 0)
                        {
                            club = _clubBusiness.Get(transactionactivities.First().ClubId);
                            var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                            displayCartViewModel.PaymentModel.Currency = club.Currency;
                            displayCartViewModel.PaymentModel.ConfirmationId = token;
                            displayCartViewModel.PaymentModel.PayableAmount = transactionactivities.Sum(c => c.Amount);
                            displayCartViewModel.PaymentModel.IsTestMode = isOrderTestMode || clinetPaymentMethod.IsCreditCardTestMode;
                            displayCartViewModel.PaymentModel.ActionCaller = JumbulaSubSystem.MakeaPayment;
                            displayCartViewModel.PaymentModel.IsFamilyTakePayment = isFamilyTakePayment;

                        }
                        #endregion

                        switch (roleType)
                        {
                            case RoleCategoryType.Dashboard:
                                {
                                    var order = firstTransaction.OrderItem.Order;

                                    displayCartViewModel.IsAutoCharge = _orderBusiness.IsTakePaymentOrderIsAutoCharge(order);
                                    displayCartViewModel.CreditCards = _userCreditCardBusiness.GetKeyValueUserCreditCards(order.UserId, order.ClubId);

                                    if (transactionactivities != null && transactionactivities.Count > 0)
                                    {
                                        displayCartViewModel.PaymentModel.IsPreferrd = _userCreditCardBusiness.GetUserCreditCards(order.UserId, order.ClubId).Any(c => c.IsDefault);
                                    }

                                }
                                break;
                            case RoleCategoryType.Parent:
                                {
                                    #region Fill CreditCards For Express
                                    var paymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
                                    var clubId = _clubBusiness.GetClubByClientId(paymentMethod.Id).Id;

                                    displayCartViewModel.CreditCards = _userCreditCardBusiness.GetKeyValueUserCreditCards(this.GetCurrentUserId(), clubId);

                                    #endregion

                                    if (transactionactivities != null && transactionactivities.Count > 0)
                                    {
                                        displayCartViewModel.PaymentModel.IsPreferrd = _userCreditCardBusiness.GetUserCreditCards(this.GetCurrentUserId(), clubId).Any(c => c.IsDefault);
                                    }
                                }
                                break;
                            default:
                                {
                                    throw new NotSupportedException("Role Category Type Not Supported in MakeaPayment");
                                }
                        }
                    }
                    break;
                case JumbulaSubSystem.Invoice:
                    {
                        #region Invoices
                        switch (roleType)
                        {
                            case RoleCategoryType.Dashboard:
                                {
                                    if (!string.IsNullOrEmpty(token))
                                    {
                                        var isOrderTestMode = true;
                                        var transactions = _transactionActivityBusiness.GetInvoicedByToken(token).ToList();

                                        var firstTransaction = transactions.First();

                                        if (firstTransaction.OrderId.HasValue)
                                        {
                                            isOrderTestMode = !firstTransaction.Order.IsLive;
                                        }
                                        else if (firstTransaction.OrderItemId.HasValue)
                                        {
                                            isOrderTestMode = !firstTransaction.OrderItem.Order.IsLive;
                                        }

                                        if (transactions != null && transactions.Count > 0)
                                        {
                                            club = _clubBusiness.Get(transactions.FirstOrDefault().ClubId);
                                            var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                                            displayCartViewModel.PaymentModel.Currency = club.Currency;
                                            displayCartViewModel.PaymentModel.ConfirmationId = token;
                                            displayCartViewModel.PaymentModel.PayableAmount = transactions.Sum(c => c.Amount);
                                            displayCartViewModel.PaymentModel.IsTestMode = isOrderTestMode || clinetPaymentMethod.IsCreditCardTestMode;
                                            displayCartViewModel.PaymentModel.ActionCaller = JumbulaSubSystem.Invoice;
                                            displayCartViewModel.PaymentModel.BackToDetail = backToDetail;

                                            displayCartViewModel.PaymentModel.IsPreferrd = _userCreditCardBusiness.GetUserCreditCards(transactions.FirstOrDefault().OrderItem.Order.UserId, club.Id).Any(c => c.IsDefault);

                                            #region Fill CreditCards For Express

                                            displayCartViewModel.CreditCards = _userCreditCardBusiness.GetKeyValueUserCreditCards(transactions.FirstOrDefault().Order.UserId, club.Id);

                                            #endregion
                                        }
                                    }
                                }
                                break;
                            case RoleCategoryType.Parent:
                                {
                                    var cart = _cartBusiness.Get(userId.HasValue ? userId.Value : this.GetCurrentUserId(), club.Id);

                                    var invoice = Ioc.InvoiceBusiness.Get(invoiceId);

                                    #region Fill CreditCards For Express

                                    var paymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
                                    var clubId = _clubBusiness.GetClubByClientId(paymentMethod.Id).Id;

                                    displayCartViewModel.CreditCards = _userCreditCardBusiness.GetKeyValueUserCreditCards(this.GetCurrentUserId(), clubId);

                                    #endregion

                                    if (!userId.HasValue)
                                    {
                                        userId = invoice.UserId;
                                    }

                                    displayCartViewModel.InvoiceId = invoiceId;
                                    displayCartViewModel.PaymentModel.ActionCaller = JumbulaSubSystem.Invoice;
                                    displayCartViewModel.PaymentModel.PayableAmount = invoice.Amount;
                                    displayCartViewModel.PaymentModel.InvoiceId = invoiceId;
                                    displayCartViewModel.PaymentModel.IsPreferrd = _userCreditCardBusiness.GetUserCreditCards(this.GetCurrentUserId(), clubId).Any(c => c.IsDefault);
                                    displayCartViewModel.PaymentModel.IsTestMode = (!cart.Order.IsLive || clientPaymentMethod.IsCreditCardTestMode);

                                    cart.PayableAmount = invoice.Amount;
                                    displayCartViewModel.Cart = cart;

                                    displayCartViewModel.CreditCards = _userCreditCardBusiness.GetKeyValueUserCreditCards(this.GetCurrentUserId(), clubId);

                                }
                                break;
                            default:
                                {
                                    throw new NotSupportedException("Role Category Type for default Not Supported Invoice");
                                }
                        }
                        #endregion
                    }
                    break;
                default:
                    {
                        throw new NotSupportedException(string.Format(Constants.F_ActionCallerNotSupported, actionCaller));
                    }
            }

            return View(displayCartViewModel);
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
        }


        [HttpPost]
        public virtual ActionResult Pay(PayViewModel model)
        {
            //offlinePayNow for select radio pay later or pay now in offline register - if offlinePayNow=true is select pay now
            var offlinePayNow = model.OfflinePaymentNow;

            try
            {
                var utcNow = DateTime.UtcNow;

                var state = GetState(model.Country, model.State);

                if (string.IsNullOrEmpty(state))
                {
                    state = model.State;
                }

                var address = new PostalAddress("", model.Street, model.City, state, model.Country.ToDescription(), model.ZipCode);

                CheckCardValidation(ModelState, model, ref address);

                model.Address = address;

                PaymentResponseModel payResponse;
                int? invoiceId = 0;
                invoiceId = model.InvoiceId;
                var invoice = new Invoice();
                var token = model.Token;

                var club = _clubBusiness.Get(this.GetCurrentClubId());

                if ((model.ActionCaller == JumbulaSubSystem.MakeaPayment || model.ActionCaller == JumbulaSubSystem.Installment) && model.ConfirmationId != null && club.ClubType.EnumType == ClubTypesEnum.Partner)
                {
                    var partnerSetting = _clubBusiness.GetPartnerSetting(club.Id);

                    if (club.Setting != null && partnerSetting.PortalParameters.ReadStripeSettingsFromMember)
                    {
                        if (model.ActionCaller == JumbulaSubSystem.MakeaPayment)
                        {
                            var transactions = _transactionActivityBusiness.GetDraftByToken(model.ConfirmationId);
                            var transaction = transactions != null && transactions.Any() ? transactions.FirstOrDefault() : null;

                            club = transaction != null ? transaction.Order.Club : _clubBusiness.Get(this.GetCurrentClubId());
                        }
                        else
                        {
                            var installments = Ioc.InstallmentBusiness.GetListByToken(model.ConfirmationId).ToList();
                            var installment = installments != null && installments.Any() ? installments.FirstOrDefault() : null;

                            club = installments != null ? installment.OrderItem.Order.Club : _clubBusiness.Get(this.GetCurrentClubId());
                        }

                        model.clubId = club.Id;
                    }

                }

                var roleType = this.GetCurrentUserRoleType(); // for parent or Admin

                switch (model.ActionCaller)  // check error in Info Card
                {
                    case JumbulaSubSystem.Order:
                        {
                            #region Order Parent and Offline register

                            model.Cart = _cartBusiness.Get(model.UserId.HasValue ? model.UserId.Value : JbUserService.GetCurrentUserId(), this.GetCurrentClubId());
                            model.Cart.Order.SubmitDate = utcNow;

                            if (model.Cart != null && model.Cart.Order != null && model.Cart.Order.OrderItems != null && model.Cart.Order.RegularOrderItems.Any() && this.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                            {
                                Ioc.ProgramBusiness.CheckProgramCapacity(model.Cart.Order.RegularOrderItems);

                                _registerBusiness.CheckDuplicateEnrollment(model.Cart.Order.RegularOrderItems.ToList());

                                var errors = new Dictionary<long, string>();
                                var validationResult = _cartBusiness.CheckCartItemsValidation(model.Cart, ref errors);

                                if (!validationResult && !CurrentUserCanDoOfflineRegister())
                                {
                                    return RedirectToAction("Index", "cart", new { userId = model.UserId });
                                }

                                if (model.Cart.Order.RegularOrderItems.Any(c => c.IsProgramFull))
                                {
                                    return RedirectToAction("Index", "cart", new { userId = model.UserId }); // In Index.cshtml handled
                                }

                                if (model.PaymentMethod == PaymentMethod.Card)
                                {
                                    if (!ModelState.IsValid)
                                    {
                                        SetValidationToCookie(ModelState, model);
                                        return RedirectToAction("Index", "cart", new { userId = model.UserId, isNotValid = true });
                                    }
                                }
                            }

                            if (model.Cart.Order == null)
                            {
                                throw new Exception(string.Format(Constants.F_NoCartItems, model.Cart.Count));
                            }
                            else if (!model.Cart.Order.RegularOrderItems.Any())
                            {
                                if (model.PaymentMethod != PaymentMethod.None)
                                {
                                    throw new Exception(string.Format(Constants.F_Amount0WrongCartType, model.PaymentMethod));
                                }

                            }

                            if (string.IsNullOrEmpty(model.Cart.Order.ConfirmationId)) //Generate ConfirmationId
                            {
                                model.Cart.Order.ConfirmationId = _orderBusiness.GenerateConfirmationId();
                                model.ConfirmationId = model.Cart.Order.ConfirmationId;
                            }

                            model.Cart.PayableAmount = _orderBusiness.CalculateOrderPayableAmount(model.Cart.Order);

                            #region Create order in completed total cart was zero

                            if (model.Cart.Total <= 0)
                            {
                                if (!model.Cart.Order.PreregisterItems.Any())
                                {
                                    model.Cart.Order.OrderStatus = OrderStatusCategories.completed;
                                    model.Cart.Order.IsDraft = false;
                                }

                                foreach (var item in model.Cart.Order.RegularOrderItems)
                                {
                                    item.ItemStatus = OrderItemStatusCategories.completed;
                                    item.PaidAmount = 0;
                                }

                                _orderBusiness.Update(model.Cart.Order);

                                _orderBusiness.OrderCompleted(model.Cart.Order, Constants.Order_Initiated, model.UserId, this.GetCurrentUserId());

                                if (model.Cart == null || model.Cart.Order == null || !(model.Cart.Order.RegularOrderItems.Any()))
                                {
                                    return RedirectToAction("Index", "cart", new { ActionCaller = JumbulaSubSystem.Order, IsEmpty = true });
                                }

                                return RedirectToAction("FinalizeConfirmation", "Cart", new { cid = model.Cart.Order.ConfirmationId, paymentMethod = PaymentMethod.None });
                            }
                            #endregion


                            #region Create order in completed Payable Amount cart was zero

                            if (model.Cart.Total >= 0 && model.Cart.PayableAmount <= 0)
                            {
                                if (!model.Cart.Order.PreregisterItems.Any())
                                {
                                    model.Cart.Order.OrderStatus = OrderStatusCategories.completed;
                                    model.Cart.Order.IsDraft = false;
                                }

                                foreach (var item in model.Cart.Order.RegularOrderItems)
                                {
                                    item.ItemStatus = OrderItemStatusCategories.completed;
                                }

                                _orderBusiness.Update(model.Cart.Order);

                                _orderBusiness.OrderCompleted(model.Cart.Order, Constants.Order_Initiated, model.UserId, this.GetCurrentUserId());

                                if (model.Cart == null || model.Cart.Order == null || !(model.Cart.Order.RegularOrderItems.Any()))
                                {
                                    return RedirectToAction("Index", "cart", new { ActionCaller = JumbulaSubSystem.Order, IsEmpty = true });
                                }

                                return RedirectToAction("FinalizeConfirmation", "Cart", new { cid = model.Cart.Order.ConfirmationId, paymentMethod = PaymentMethod.None });
                            }
                            #endregion

                            if (model.Cart.Order.OrderMode == OrderMode.Online)
                            {
                                model.Cart.Order.PaymentMethod = model.PaymentMethod;
                            }

                            _orderBusiness.Update(model.Cart.Order); // update orderstatus=initiated,IsDraft = true


                            #endregion
                        }
                        break;
                    case JumbulaSubSystem.Installment:
                        {
                            if (model.PaymentMethod == PaymentMethod.Card)
                            {
                                if (!ModelState.IsValid)
                                {
                                    SetValidationToCookie(ModelState, model);
                                    return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Installment, token = model.ConfirmationId, backToDetail = false, isNotValid = true });
                                }
                            }
                        }
                        break;
                    case JumbulaSubSystem.MakeaPayment:
                        {
                            if (model.PaymentMethod == PaymentMethod.Card)
                            {
                                if (!ModelState.IsValid) // check error in Info Card
                                {
                                    if (model.IsFamilyTakePayment)
                                    {
                                        SetValidationToCookie(ModelState, model);
                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, isNotValid = true, isFamilyTakePayment = true });
                                    }
                                    else
                                    {
                                        SetValidationToCookie(ModelState, model);

                                        if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                                        {
                                            return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, isNotValid = true });
                                        }
                                        else
                                        {
                                            return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, isNotValid = true, isFamilyTakePayment = false });

                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case JumbulaSubSystem.Invoice:
                        {
                            if (invoiceId.HasValue)
                            {
                                invoice = Ioc.InvoiceBusiness.Get(invoiceId);
                                token = Ioc.InvoiceBusiness.GetTokenForInvoice(invoice.Id);
                            }

                            #region Invoices
                            switch (roleType)
                            {
                                case RoleCategoryType.Dashboard:
                                    {
                                        if (model.PaymentMethod == PaymentMethod.Card)
                                        {
                                            if (!ModelState.IsValid)
                                            {
                                                SetValidationToCookie(ModelState, model);
                                                return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Invoice, token = model.ConfirmationId, backToDetail = true, isNotValid = true });
                                            }
                                        }

                                    }
                                    break;
                                case RoleCategoryType.Parent:
                                    {
                                        if (model.PaymentMethod == PaymentMethod.Card)
                                        {
                                            if (!ModelState.IsValid)
                                            {
                                                SetValidationToCookie(ModelState, model);
                                                return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Invoice, invoiceId = invoiceId, backToDetail = true, isNotValid = true });
                                            }
                                        }
                                        if (invoice.Status == InvoiceStatus.Paid || invoice.Status == InvoiceStatus.Canceled)
                                        {
                                            return RedirectToAction("Index", "cart", new { invoiceId = invoiceId, isPaidOrCanceled = true });
                                        }
                                    }
                                    break;
                                default:
                                    { throw new NotSupportedException("Role Category Not Supported in Invoice"); }

                            }
                            #endregion
                        }
                        break;
                    default:
                        {
                            throw new NotSupportedException(string.Format(Constants.F_ActionCallerNotSupported, model.ActionCaller));
                        }
                }


                var paymentService = new JbPaymentService();
                payResponse = paymentService.Pay(model, this.ControllerContext, this.GetCurrentClubId());


                if (ModelState.IsValid)
                {
                    if (payResponse.cardErrors != null)
                    {
                        foreach (var item in payResponse.cardErrors)
                        {
                            ModelState.AddModelError(item.Key, item.Value);
                        }
                    }
                }

                if (!ModelState.IsValid)
                {
                    SetValidationToCookie(ModelState, model);
                }

                switch (payResponse.PaymentMethod) // check result from paymentService
                {
                    case PaymentMethod.Paypal:
                        {
                            #region switch Action caller

                            switch (model.ActionCaller)
                            {
                                case JumbulaSubSystem.Order:
                                    {
                                        if (!ModelState.IsValid)
                                        {
                                            return RedirectToAction("Index", "cart", new { userId = model.UserId, isNotValid = true, message = "paypal: " + payResponse.errorMsg });
                                        }

                                        _orderBusiness.Update(model.Cart.Order);

                                        if (model.Cart.Order.IsAutoCharge)
                                        {
                                            return RedirectToAction("SetPreapproval", "Paypal", new { token = model.Cart.Order.ConfirmationId, actionCaller = JumbulaSubSystem.Order });
                                        }
                                        else
                                        {
                                            return RedirectToAction("PaypalCheckOut", "Paypal", new { confirmationID = model.Cart.Order.ConfirmationId });
                                        }
                                    }
                                case JumbulaSubSystem.Installment:
                                    {
                                        if (!ModelState.IsValid)
                                        {
                                            return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Installment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "paypal: " + payResponse.errorMsg });
                                        }
                                        else
                                        {
                                            return RedirectToAction("InstallmentPayment", "Paypal", new { token = token });
                                        }

                                    }
                                case JumbulaSubSystem.MakeaPayment:
                                    {
                                        if (!ModelState.IsValid)
                                        {
                                            if (model.IsFamilyTakePayment)
                                            {
                                                return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, isNotValid = true, isFamilyTakePayment = true, message = "paypal: " + payResponse.errorMsg });
                                            }
                                            else
                                            {
                                                if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                                                {
                                                    return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, isNotValid = true, message = "paypal: " + payResponse.errorMsg });
                                                }
                                                else
                                                {
                                                    return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, isNotValid = true, message = "paypal: " + payResponse.errorMsg });
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (model.IsFamilyTakePayment)
                                            {
                                                if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                                                {
                                                    return RedirectToAction("MakePayment", "Paypal", new { token = token, parentReturnUrl = model.ReturnUrl, isFamilyTakePayment = true });
                                                }
                                                else
                                                {
                                                    return RedirectToAction("MakePayment", "Paypal", new { token = token, isFamilyTakePayment = true });
                                                }
                                            }
                                            else
                                            {
                                                if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                                                {
                                                    return RedirectToAction("MakePayment", "Paypal", new { token = token, parentReturnUrl = model.ReturnUrl, isFamilyTakePayment = false });
                                                }
                                                else
                                                {
                                                    return RedirectToAction("MakePayment", "Paypal", new { token = token });
                                                }
                                            }
                                        }
                                    }
                                case JumbulaSubSystem.Invoice:
                                    {
                                        if (invoiceId.HasValue)
                                        {
                                            invoice = Ioc.InvoiceBusiness.Get(invoiceId);
                                            token = Ioc.InvoiceBusiness.GetTokenForInvoice(invoice.Id);
                                        }

                                        var payRole = this.GetCurrentUserRoleType();

                                        switch (payRole)
                                        {
                                            case RoleCategoryType.Dashboard:
                                                {
                                                    if (!ModelState.IsValid)
                                                    {
                                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Invoice, token = token, isNotValid = true, message = "paypal: " + payResponse.errorMsg });
                                                    }
                                                    else
                                                    {
                                                        return RedirectToAction("InvoicePayment", "Paypal", new { token = token, IsTakePaymentInvoice = true });
                                                    }
                                                }
                                            case RoleCategoryType.Parent:
                                                {
                                                    if (!ModelState.IsValid)
                                                    {
                                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Invoice, token = token, isNotValid = true, message = "paypal: " + payResponse.errorMsg });
                                                    }
                                                    else
                                                    {
                                                        return RedirectToAction("InvoicePayment", "Paypal", new { token = token, IsTakePaymentInvoice = false });

                                                    }
                                                }

                                            default:
                                                {
                                                    throw new NotSupportedException("Role Category Type Not Supported");
                                                }
                                        }
                                    }
                                default:
                                    {
                                        throw new NotSupportedException(string.Format(Constants.F_CardTypeCategoryNotSupported, model.PaymentMethod));
                                    }
                            }
                            #endregion
                        }
                    case PaymentMethod.Card:
                        {
                            #region Action Caller

                            switch (model.ActionCaller)
                            {
                                case JumbulaSubSystem.Order:
                                    {
                                        if (model.Cart == null || model.Cart.Order == null || !(model.Cart.Order.RegularOrderItems.Any()))
                                        {
                                            return RedirectToAction("Index", "cart", new { ActionCaller = JumbulaSubSystem.Order, IsEmpty = true });
                                        }

                                        if (!ModelState.IsValid)
                                        {
                                            return RedirectToAction("Index", "cart", new { userId = model.UserId, isNotValid = true, message = "card: " + payResponse.errorMsg });
                                        }
                                        else
                                        {
                                            return RedirectToAction("FinalizeConfirmation", "Cart", new { cid = model.Cart.Order.ConfirmationId, paymentMethod = model.PaymentMethod, orderMode = model.Cart.Order.OrderMode, offlinePayNow = offlinePayNow, PayableAmount = model.PayableAmount });
                                        }
                                    }
                                case JumbulaSubSystem.Installment:
                                    {
                                        if (!ModelState.IsValid)
                                        {
                                            if (payResponse.cardErrors != null)
                                            {
                                                return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Installment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });
                                            }
                                            return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Installment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });

                                        }
                                        else
                                        {
                                            return RedirectToAction("DisplaySucceedOperation", "Cart");
                                        }
                                    }

                                case JumbulaSubSystem.MakeaPayment:
                                    {
                                        switch (roleType)
                                        {
                                            case RoleCategoryType.Dashboard:
                                                {
                                                    if (!ModelState.IsValid)
                                                    {
                                                        if (payResponse.cardErrors != null)
                                                        {
                                                            if (!string.IsNullOrWhiteSpace(model.ReturnUrl))
                                                                return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });

                                                            if (model.IsFamilyTakePayment == false)
                                                            {
                                                                return RedirectToAction("MakePaymentReturnStatus", "Order", new { token = model.ConfirmationId, message = payResponse.errorMsg, status = false });
                                                            }
                                                            else
                                                            {
                                                                return RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = payResponse.errorMsg, status = false });
                                                            }
                                                        }

                                                        if (!string.IsNullOrWhiteSpace(model.ReturnUrl))
                                                            return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });


                                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });
                                                    }
                                                    else
                                                    {
                                                        if (!string.IsNullOrWhiteSpace(model.ReturnUrl) && string.IsNullOrEmpty(payResponse.errorMsg))
                                                        {
                                                            return Redirect(model.ReturnUrl + "&status=true");
                                                        }
                                                        else
                                                        {
                                                            if (string.IsNullOrEmpty(payResponse.errorMsg))
                                                            {
                                                                if (!string.IsNullOrWhiteSpace(model.ReturnUrl))
                                                                    return Redirect(model.ReturnUrl + "&status=true");

                                                                if (model.IsFamilyTakePayment == false)
                                                                {
                                                                    return RedirectToAction("MakePaymentReturnStatus", "Order", new { token = model.ConfirmationId, message = payResponse.errorMsg, status = true });
                                                                }
                                                                else
                                                                {
                                                                    return RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = payResponse.errorMsg, status = true });
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (!string.IsNullOrWhiteSpace(model.ReturnUrl))
                                                                    return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });

                                                                if (model.IsFamilyTakePayment == false)
                                                                {
                                                                    return RedirectToAction("MakePaymentReturnStatus", "Order", new { token = model.ConfirmationId, message = payResponse.errorMsg, status = false });
                                                                }
                                                                else
                                                                {
                                                                    return RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = payResponse.errorMsg, status = false });
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            case RoleCategoryType.Parent:
                                                {
                                                    if (!ModelState.IsValid)
                                                    {
                                                        if (payResponse.cardErrors != null)
                                                        {
                                                            if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                                                            {
                                                                return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });
                                                            }
                                                            else
                                                            {
                                                                return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });
                                                            }
                                                        }

                                                        if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                                                        {
                                                            return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });
                                                        }
                                                        else
                                                        {
                                                            return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });

                                                        }
                                                    }
                                                    else
                                                    {
                                                        return Redirect(model.ReturnUrl + "&status=true");
                                                    }
                                                }
                                            default: { throw new NotSupportedException("Role Category Not Supported in Invoice Parent"); }

                                        }

                                    }
                                case JumbulaSubSystem.Invoice:
                                    {
                                        if (invoiceId.HasValue)
                                        {
                                            invoice = Ioc.InvoiceBusiness.Get(invoiceId);
                                            token = Ioc.InvoiceBusiness.GetTokenForInvoice(invoice.Id);
                                        }
                                        switch (roleType)
                                        {
                                            case RoleCategoryType.Dashboard:
                                                {
                                                    var transactions = _transactionActivityBusiness.GetTransactionsByToken(model.ConfirmationId).ToList();
                                                    invoiceId = transactions.First().PaymentDetail.InvoiceId;

                                                    if (ModelState.IsValid)
                                                    {
                                                        if (model.BackToDetail)
                                                        {
                                                            var urlToRedirect =
                                                                $"{Request.Url.GetLeftPart(UriPartial.Authority)}/Dashboard#/Invoicing/Invoice/Details/{invoiceId}";
                                                            return Redirect(urlToRedirect);
                                                        }
                                                        else
                                                        {
                                                            return RedirectToAction("DisplaySucceedOperation", "Cart");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Invoice, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });

                                                    }
                                                }
                                            case RoleCategoryType.Parent:
                                                {
                                                    if (ModelState.IsValid)
                                                    {
                                                        if (!string.IsNullOrEmpty(payResponse.errorMsg))
                                                        {
                                                            return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Payment_Failed, msg = payResponse.errorMsg, status = false, cid = token });
                                                        }
                                                        return RedirectToAction("InvoiceDetail", "Registrant", new { invoiceId = invoiceId, status = true });
                                                    }
                                                    else
                                                    {
                                                        if (!string.IsNullOrEmpty(payResponse.errorMsg))
                                                        {
                                                            return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Payment_Failed, msg = payResponse.errorMsg, status = false, cid = token });

                                                        }
                                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Invoice, invoiceId = model.InvoiceId, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });
                                                    }

                                                }
                                            default:
                                                {
                                                    throw new NotSupportedException("Role Category Not Supported in Invoice Parent");
                                                }
                                        }
                                    }
                                default:
                                    {
                                        throw new NotSupportedException(string.Format(Constants.F_ActionCallerNotSupported, model.ActionCaller));
                                    }
                            }
                            #endregion
                        }
                    case PaymentMethod.Express:
                        {
                            #region Action caller
                            switch (model.ActionCaller)
                            {
                                case JumbulaSubSystem.Order:
                                    {
                                        if (model.Cart?.Order == null || !model.Cart.Order.RegularOrderItems.Any())
                                        {
                                            model.ActionCaller = JumbulaSubSystem.Order;
                                            model.Cart.IsEmpty = true;
                                        }
                                        if (ModelState.IsValid)
                                        {
                                            return string.IsNullOrEmpty(payResponse.errorMsg) ? RedirectToAction("FinalizeConfirmation", "Cart", new { cid = model.Cart.Order.ConfirmationId, paymentMethod = model.PaymentMethod, orderMode = model.Cart.Order.OrderMode, offlinePayNow = offlinePayNow, PayableAmount = model.PayableAmount }) : RedirectToAction("Index", "cart", new { userId = model.UserId, isNotValid = true, message = "express: " + payResponse.errorMsg });
                                        }

                                        return RedirectToAction("Index", "cart", new { userId = model.UserId, isNotValid = true, message = "express: " + payResponse.errorMsg });
                                    }
                                case JumbulaSubSystem.MakeaPayment:
                                    {
                                        switch (roleType)
                                        {
                                            case RoleCategoryType.Dashboard:
                                                {
                                                    if (!ModelState.IsValid)
                                                    {
                                                        if (payResponse.cardErrors != null)
                                                        {
                                                            if (!string.IsNullOrWhiteSpace(model.ReturnUrl))
                                                                return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, backToDetail = false, isNotValid = true, message = "express: " + payResponse.errorMsg });
                                                            
                                                            return model.IsFamilyTakePayment == false ? RedirectToAction("MakePaymentReturnStatus", "Order", new { token = model.ConfirmationId, message = payResponse.errorMsg, status = false }) : RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = payResponse.errorMsg, status = false });
                                                        }

                                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "express: " + payResponse.errorMsg });
                                                    }
                                                    else
                                                    {
                                                        if (!string.IsNullOrWhiteSpace(model.ReturnUrl) && string.IsNullOrEmpty(payResponse.errorMsg))
                                                            return Redirect(model.ReturnUrl + "&status=true");

                                                    }

                                                    if (string.IsNullOrEmpty(payResponse.errorMsg))
                                                    {
                                                        return model.IsFamilyTakePayment == false ? RedirectToAction("MakePaymentReturnStatus", "Order", new { token = model.ConfirmationId, message = payResponse.errorMsg, status = true }) : RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = payResponse.errorMsg, status = true });
                                                    }

                                                    if (!string.IsNullOrWhiteSpace(model.ReturnUrl))
                                                    {
                                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, isNotValid = true, message = "express: " + payResponse.errorMsg });
                                                    }

                                                    return model.IsFamilyTakePayment == false ?
                                                      RedirectToAction("MakePaymentReturnStatus", "Order", new { token = model.ConfirmationId, message = payResponse.errorMsg, status = false }) :
                                                      RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = payResponse.errorMsg, status = false });
                                                }
                                            case RoleCategoryType.Parent:
                                                {
                                                    if (ModelState.IsValid && !string.IsNullOrWhiteSpace(model.ReturnUrl) && string.IsNullOrEmpty(payResponse.errorMsg)) return Redirect(model.ReturnUrl + "&status=true");

                                                    if (payResponse.cardErrors == null)
                                                    {
                                                        return RedirectToAction("Index", "cart",
                                                            new
                                                            {
                                                                actionCaller = JumbulaSubSystem.MakeaPayment,
                                                                token = model.ConfirmationId,
                                                                isNotValid = true,
                                                                message = "express: " + payResponse.errorMsg
                                                            });
                                                    }

                                                    return JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent ?
                                                        RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, returnUrl = model.ReturnUrl, isNotValid = true, message = "express: " + payResponse.errorMsg }) :
                                                        RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.MakeaPayment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "express: " + payResponse.errorMsg });

                                                }
                                            default: { throw new NotSupportedException("Role Category Not Supported in Invoice Parent"); }

                                        }

                                    }
                                case JumbulaSubSystem.Invoice:
                                    {
                                        if (invoiceId.HasValue)
                                        {
                                            invoice = Ioc.InvoiceBusiness.Get(invoiceId);
                                            token = Ioc.InvoiceBusiness.GetTokenForInvoice(invoice.Id);
                                        }

                                        var payRole = this.GetCurrentUserRoleType();

                                        switch (payRole)
                                        {
                                            case RoleCategoryType.Parent:
                                                {
                                                    if (!ModelState.IsValid)
                                                    {
                                                        return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Invoice, invoiceId = invoiceId, isNotValid = true, message = "express: " + payResponse.errorMsg });
                                                    }
                                                    return string.IsNullOrEmpty(payResponse.errorMsg) ? RedirectToAction("InvoiceDetail", "Registrant", new { invoiceId = invoiceId, status = true }) : RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Payment_Failed, msg = payResponse.errorMsg, status = false, cid = token });
                                                }
                                            case RoleCategoryType.Dashboard:
                                                {
                                                    var transactions = _transactionActivityBusiness.GetTransactionsByToken(model.ConfirmationId).ToList();
                                                    invoiceId = transactions.First().PaymentDetail.InvoiceId;

                                                    if (!ModelState.IsValid)
                                                        return RedirectToAction("Index", "cart",
                                                            new
                                                            {
                                                                actionCaller = JumbulaSubSystem.Invoice,
                                                                token = model.ConfirmationId,
                                                                backToDetail = false,
                                                                isNotValid = true,
                                                                message = "card: " + payResponse.errorMsg
                                                            });

                                                    if (model.BackToDetail)
                                                    {
                                                        var urlToRedirect =
                                                            $"{Request.Url.GetLeftPart(UriPartial.Authority)}/Dashboard#/Invoicing/Invoice/Details/{invoiceId}";
                                                        return Redirect(urlToRedirect);
                                                    }

                                                    return RedirectToAction("DisplaySucceedOperation", "Cart");

                                                }
                                            default:
                                                {
                                                    throw new NotSupportedException("Role Category Type Not Supported");
                                                }
                                        }
                                    }
                                case JumbulaSubSystem.Installment:
                                    {
                                        if (!ModelState.IsValid)
                                        {
                                            return RedirectToAction("Index", "cart", new { actionCaller = JumbulaSubSystem.Installment, token = model.ConfirmationId, backToDetail = false, isNotValid = true, message = "card: " + payResponse.errorMsg });
                                        }

                                        return RedirectToAction("DisplaySucceedOperation", "Cart");
                                    }
                                default:
                                    {
                                        throw new NotSupportedException(string.Format(Constants.F_ActionCallerNotSupported, model.ActionCaller));
                                    }

                            }
                            #endregion
                        }
                    case PaymentMethod.Cash:
                    case PaymentMethod.Scholarship:
                    case PaymentMethod.FinancialAid:
                    case PaymentMethod.BankTransfer:
                    case PaymentMethod.Other:
                        {
                            if (!ModelState.IsValid)
                            {
                                return RedirectToAction("Index", "cart", new { userId = model.UserId, isNotValid = true, message = "cash: " + payResponse.errorMsg });
                            }

                            if (model.Cart == null || model.Cart.Order == null || !(model.Cart.Order.RegularOrderItems.Any()))
                            {
                                return RedirectToAction("Index", "cart", new { ActionCaller = JumbulaSubSystem.Order, IsEmpty = true });
                            }

                            if (string.IsNullOrEmpty(payResponse.errorMsg))
                            {
                                return RedirectToAction("FinalizeConfirmation", "Cart", new { cid = model.Cart.Order.ConfirmationId, paymentMethod = model.PaymentMethod, orderMode = model.Cart.Order.OrderMode, offlinePayNow = offlinePayNow, PayableAmount = model.Cart.Order.OrderAmount });
                            }
                            else
                            {
                                return RedirectToAction("Index", "cart", new { userId = model.UserId, isNotValid = true, message = "cash: " + payResponse.errorMsg });
                            }
                        }
                    default:
                        {
                            throw new NotSupportedException(string.Format(Constants.F_CardTypeCategoryNotSupported, model.PaymentMethod));
                        }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public virtual JsonResult CheckCartValidation(int? userId)
        {
            var cart = _cartBusiness.Get(userId.HasValue ? userId.Value : this.GetCurrentUserId(), this.GetCurrentClubId());
            if (cart != null && cart.Order != null && cart.Order.RegularOrderItems != null && cart.Order.RegularOrderItems.Any() && cart.Order.OrderMode == OrderMode.Online)
            {
                var validateStatus = CheckCartItemValidation(cart.Order.RegularOrderItems);

                if (validateStatus.Key)
                {
                    var errors = new Dictionary<long, string>();
                    var status = _cartBusiness.CheckCartItemsValidation(cart, ref errors);

                    if (status)
                    {
                        return Json(new JResult() { Status = true, Data = string.Empty }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var error = errors.Any() ? errors.First().Value : string.Empty;

                        return Json(new JResult() { Status = false, Data = error }, JsonRequestBehavior.AllowGet);
                    }
                }

                var message = !string.IsNullOrWhiteSpace(validateStatus.Value) ? validateStatus.Value : "This program is now full, please delete it from your cart. You can select an alternate program or submit the payment to complete your registration for the remaining programs.";

                return Json(new JResult() { Status = validateStatus.Key, Data = message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new JResult() { Status = true, Message = "" }, JsonRequestBehavior.AllowGet);
        }

        private bool CurrentUserCanDoOfflineRegister()
        {
            var currentUserRole = this.GetCurrentUserRole();

            if (currentUserRole == RoleCategory.Admin || currentUserRole == RoleCategory.Manager || currentUserRole == RoleCategory.Contributor)
            {
                return true;
            }

            return false;
        }

        public virtual JsonResult CheckActiveCreditCard(int? creditCardId)
        {
            var creditCards = _userCreditCardBusiness.GetUserCreditCards(this.GetCurrentUserId(), PaymentGateway.Stripe);
            if (creditCards.ToList().Count > 1)
            {
                var userCreditCard = _userCreditCardBusiness.Get(creditCardId.HasValue ? creditCardId.Value : 0);
                if (userCreditCard.IsDefault == false)
                {
                    return Json(new JResult() { Status = false }, JsonRequestBehavior.AllowGet);
                }
                else { return Json(new JResult() { Status = true, Message = "" }, JsonRequestBehavior.AllowGet); }
            }
            else
            {
                return Json(new JResult() { Status = true, Message = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public KeyValuePair<bool, string> CheckCartItemValidation(IEnumerable<OrderItem> orderItems)
        {
            Ioc.ProgramBusiness.CheckProgramCapacity(orderItems);
            _registerBusiness.CheckDuplicateEnrollment(orderItems.ToList());

            var programCapacityResult = !(orderItems.Any(c => c.IsProgramFull));
            var seasonModeResult = (orderItems.Where(c => c.ProgramScheduleId.HasValue).All(c => c.Season.Status == SeasonStatus.Live) ||
                orderItems.Where(c => c.ProgramScheduleId.HasValue).All(c => c.Season.Status == SeasonStatus.Test));

            return new KeyValuePair<bool, string>(programCapacityResult && seasonModeResult, seasonModeResult ? "" : Constants.M_LiveAndSandBoxItemInCart);
        }


        public virtual JsonResult AddDonationToCart(string donationAmount, int? userId)
        {
            decimal donate = 0;
            if (!decimal.TryParse(donationAmount, out donate))
            {
                return Json(new JResult() { Status = false, Message = "Please enter the amount you would like to donate." }, JsonRequestBehavior.AllowGet);
            }

            long? seasonId = null;

            var clubDomain = Request.Url.GetSubDomain();
            var club = _clubBusiness.Get(clubDomain);
            var user = userId.HasValue ? Ioc.UserProfileBusiness.Get(userId.Value) : Ioc.UserProfileBusiness.Get(this.GetCurrentUserId());

            var players = Ioc.UserProfileBusiness.GetPlayerProfiles(user.Id);

            var player = players.Any(p => p.Relationship == RelationshipType.Parent) ? players.First(p => p.Relationship == RelationshipType.Parent) : new PlayerProfile();


            var order = _orderBusiness.GetOrderInCart(user.Id, club.Id) ?? new Order();

            if (order.RegularOrderItems.Any(o => o.SeasonId.HasValue))
            {
                seasonId = order.RegularOrderItems.Last(o => o.SeasonId.HasValue).SeasonId.Value;
            }

            order.ClubId = club.Id;
            order.UserId = user.Id;
            order.OrderMode = (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Dashboard) ? OrderMode.Offline : OrderMode.Online;
            order.CompleteDate = DateTime.UtcNow;

            var orderItem = new OrderItem();
            orderItem.LastStep = RegistrationStep.Step4;
            orderItem.SeasonId = seasonId;
            orderItem.Name = "Donation";
            orderItem.DateCreated = DateTime.UtcNow;
            orderItem.ISTS = DateTimeHelper.DateTimetoTimeStamp(DateTime.UtcNow);
            if (player.Id > 0)
            {
                orderItem.PlayerId = player.Id;
            }
            orderItem.ItemStatus = OrderItemStatusCategories.initiated;
            orderItem.FirstName = player.Contact != null ? player.Contact.FirstName : this.GetCurrentUserName();
            orderItem.LastName = player.Contact != null ? player.Contact.LastName : this.GetCurrentUserName();
            orderItem.EntryFeeName = "Donation";
            //orderItem.EntryFee = amount;
            var ordercharge = new OrderChargeDiscount()
            {
                Id = 0,
                Amount = donate,
                Category = ChargeDiscountCategory.Donation,
                Subcategory = ChargeDiscountSubcategory.Charge,
                Name = "Donation",
            };
            if (orderItem.GetOrderChargeDiscounts().ToList().Count > 0)
            {
                orderItem.OrderChargeDiscounts.Clear();
            }
            orderItem.OrderChargeDiscounts.Add(ordercharge);

            if (orderItem.Id == 0)
            {
                order.OrderItems.Add(orderItem);
            }
            OperationStatus res = null;
            if (order.Id > 0)
            {
                res = _orderBusiness.Update(order);
            }
            else
            {
                res = _orderBusiness.Create(order);
            }
            if (res.Status)
            {
                var url = Url.Action("Index", "Cart");
                return Json(new JResult() { Status = true, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            LogHelper.LogException(res.Exception);
            return Json(new JResult() { Status = res.Status, Message = Constants.PlayerProfile_Ajax_Request_Failed }, JsonRequestBehavior.AllowGet);

        }

        public virtual JsonResult ApplyCouponToCart(string couponCode, int? userId)
        {
            var cart = _cartBusiness.Get(userId.HasValue ? userId.Value : this.GetCurrentUserId(), this.GetCurrentClubId());
            var coupons = Ioc.CouponBusiness.GetListId(cart.Order.ClubId, couponCode, !cart.Order.IsLive);

            if (coupons != null && coupons.Any())
            {
                Ioc.CouponBusiness.CalulateCoupon(cart, coupons);
                return Json(new JResult() { Status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new JResult() { Status = false, Message = "This coupon code is invalid or has expired" }, JsonRequestBehavior.AllowGet);
            }
        }

        public virtual ActionResult RemoveAppliedCoupon()
        {
            return View("Index");
        }

        [AllowAnonymous]
        public virtual ActionResult FinalizeConfirmation(string cid, PaymentMethod paymentMethod, OrderMode orderMode = OrderMode.Online, bool offlinePayNow = false, decimal PayableAmount = 0)
        {
            try
            {
                var order = _orderBusiness.Get(cid);

                order.CompleteDate = DateTime.UtcNow;

                try
                {
                    var res = _clubBusiness.AddUserInClubUsers(order);

                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }

                if (paymentMethod != PaymentMethod.Paypal)
                {
                    EmailService.Get(this.ControllerContext).SendReservationEmail(cid, paymentMethod, Request.Url.Host, null, SportsClub.Models.Email.ReservationEmailMode.Normal, orderMode);

                    var allOrderitems = _orderBusiness.Get(cid).RegularOrderItems;
                    if (orderMode == OrderMode.Offline && offlinePayNow == true && allOrderitems.Where(c => c.ItemStatus != OrderItemStatusCategories.deleted).Count() == 1) // One Item go to Orderitem take payment
                    {
                        var orderItem = _orderBusiness.Get(cid).RegularOrderItems.First();

                        var urlToRedirect = string.Format("{0}/Dashboard#/Season/{1}/Program/{2}/OrderItem/{3}/MakePayment/", Request.Url.GetLeftPart(UriPartial.Authority), orderItem.Season.Domain, orderItem.ProgramSchedule.ProgramId, orderItem.Id);
                        return Redirect(urlToRedirect);
                    }
                    else if (orderMode == OrderMode.Offline && offlinePayNow == true && allOrderitems.Where(c => c.ItemStatus != OrderItemStatusCategories.deleted).Count() > 1) //More One Item go to Family take payment
                    {
                        var orderItem = _orderBusiness.Get(cid).RegularOrderItems.First();

                        var urlToRedirect = string.Format("{0}/Dashboard#/People/Family/{1}/{2}/{3}/0//", Request.Url.GetLeftPart(UriPartial.Authority), orderItem.Order.UserId, "TakePayment", orderItem.Order.Id);
                        return Redirect(urlToRedirect);
                    }
                    else
                    {

                        return RedirectToAction("DisplayConfirmation", new { paymentMethod, PayableAmount });
                    }

                }
                else
                {
                    return RedirectToAction("DisplayConfirmation", new { paymentMethod, PayableAmount });
                }


            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual ActionResult DisplayConfirmation(PaymentMethod? paymentMethod, decimal? PayableAmount)
        {


            ViewBag.RedirectUrl = (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard) ? Constants.Jumbula_Dashbaord_Url :
                Url.Action("Dashboard", "Registrant");

            var club = _clubBusiness.Get(this.GetCurrentClubId());

            if (paymentMethod != null && PayableAmount != null)
            {
                ClientPaymentMethod clientPaymentMethod = null;

                if (club.PartnerId.HasValue && club.PartnerClub != null && club.IsSchool && (club.Domain.Equals("bc-alx-georgemason", StringComparison.OrdinalIgnoreCase) || (club.Domain.Equals("bc-alx-barrett", StringComparison.OrdinalIgnoreCase)) || (club.Domain.Equals("bc-arl-claremont", StringComparison.OrdinalIgnoreCase)) || (club.Domain.Equals("bc-alx-macarthur", StringComparison.OrdinalIgnoreCase)) || (club.Domain.Equals("bc-arl-arlingtonsciencefocus", StringComparison.OrdinalIgnoreCase)) || club.Domain.Equals("bc-arl-campbell", StringComparison.OrdinalIgnoreCase)))
                {
                    var client = _clientBusiness.Get(club.Id);
                    clientPaymentMethod = client.PaymentMethods;
                }
                else
                {
                    clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId);
                }


                if (paymentMethod == PaymentMethod.Cash)
                {
                    ViewBag.messageInstruction = clientPaymentMethod.CashPaymentInstruction;
                }
                if (paymentMethod == PaymentMethod.Echeck)
                {
                    ViewBag.messageInstruction = clientPaymentMethod.AchPaymentInstruction;
                }
                if (paymentMethod == PaymentMethod.BankTransfer)
                {
                    ViewBag.messageInstruction = clientPaymentMethod.BankTransferPaymentInstruction;
                }
                if (paymentMethod == PaymentMethod.None)
                {
                    ViewBag.messageInstruction = clientPaymentMethod.ManualPaymentInstruction;
                }
                if (paymentMethod == PaymentMethod.Scholarship || paymentMethod == PaymentMethod.FinancialAid)
                {
                    ViewBag.messageInstruction = clientPaymentMethod.ManualPaymentInstruction;
                }
                if (paymentMethod == PaymentMethod.Other)
                {
                    ViewBag.messageInstruction = clientPaymentMethod.ManualPaymentInstruction;
                }

                ViewBag.PayableAmount = PayableAmount;

            }
            else
            {
                ViewBag.PayableAmount = 0;
            }

            var url = "~/Views/Order/DisplayConfirmation.cshtml";
            var customUrl = string.Empty;

            if (club.Setting != null && club.Setting.ConfirmationRedirectUrl != null && club.Setting.ConfirmationRedirectUrl.RedirectUrl != null)
            {
                customUrl = club.Setting.ConfirmationRedirectUrl.RedirectUrl;

                return Redirect(customUrl);
            }

            return View(url);
        }

        [AllowAnonymous]
        public virtual ActionResult DisplaySucceedOperation()
        {
            return View("DisplaySucceedOperation");
        }

        public virtual ActionResult RemovePack(long ists, int? userId)
        {
            var order = _cartBusiness.Get(userId.HasValue ? userId.Value : this.GetCurrentUserId(), this.GetCurrentClubId()).Order;

            Ioc.OrderItemBusiness.DeleteByISTS(ists);

            var clubId = _orderBusiness.GetOrderClubIdFromItems(order.OrderItems.Where(i => i.ItemStatus != OrderItemStatusCategories.deleted && i.ItemStatus != OrderItemStatusCategories.notInitiated).ToList());

            if (clubId.HasValue)
            {
                order.ClubId = clubId.Value;
                _orderBusiness.Update(order);
            }

            if (!order.RegularOrderItems.Any() && !order.PreregisterItems.Any())
            {
                _orderBusiness.Delete(order.Id);
            }

            return RedirectToAction("Index", new { userId = userId });
        }

        public virtual ActionResult RemoveItem(long id, int? userId)
        {
            var order = _cartBusiness.Get(userId.HasValue ? userId.Value : this.GetCurrentUserId(), this.GetCurrentClubId()).Order;

            var orderItem = Ioc.OrderItemBusiness.GetItem(id);

            if (orderItem.ItemStatus == OrderItemStatusCategories.notInitiated || orderItem.ItemStatus == OrderItemStatusCategories.initiated)
                Ioc.OrderItemBusiness.Delete(id);

            var clubId = _orderBusiness.GetOrderClubIdFromItems(order.OrderItems.Where(i => i.ItemStatus != OrderItemStatusCategories.deleted && i.ItemStatus != OrderItemStatusCategories.notInitiated).ToList());

            if (clubId.HasValue)
            {
                order.ClubId = clubId.Value;
                _orderBusiness.Update(order);
            }

            if (!order.RegularOrderItems.Any() && !order.PreregisterItems.Any())
            {
                var orderResult = _orderBusiness.Delete(order.Id);
            }

            return RedirectToAction("Index", new { userId = userId });
        }

        [AllowAnonymous]
        public virtual ActionResult PaymentGeneralError(string subject, string msg, bool status, string cid)
        {
            var errorModel = new PaymentErrorViewModel(subject, msg, cid, status);
            return View(errorModel);
        }

        [AllowAnonymous]
        public virtual ActionResult PaymentGeneralErrorWithModel(PaymentErrorViewModel model)
        {
            return View("PaymentGeneralError", model);
        }

        [AllowAnonymous]
        public virtual ActionResult ConfirmInstallmentPayment(string Token)
        {
            try
            {
                if (!string.IsNullOrEmpty(Token))
                {
                    var installments = Ioc.InstallmentBusiness.GetListByToken(Token);
                    if (installments != null && installments.Count() > 0)
                    {
                        return RedirectToAction("PreapprovalRequest", "Cart", new { token = installments.FirstOrDefault().OrderItem.Order.ConfirmationId });
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }


            return HttpNotFound();
        }

        [AllowAnonymous]
        public virtual ActionResult InstallmentPayment(string token)
        {
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    return RedirectToAction("MakePayment", "Cart", new { token = token, actionCaller = JumbulaSubSystem.Installment });
                }

                return HttpNotFound();
            }
            catch
            {
                throw;
            }
        }

        [AllowAnonymous]
        public virtual ActionResult MakePayment(string token, JumbulaSubSystem actionCaller = JumbulaSubSystem.Invoice)
        {
            try
            {
                GeneralPaymentViewModel model = new GeneralPaymentViewModel()
                {
                    Token = token,
                    ActionCaller = actionCaller,
                    Amount = 0,
                    BackToDetail = false
                };
                if (!string.IsNullOrEmpty(token))
                {
                    switch (actionCaller)
                    {
                        case JumbulaSubSystem.Installment:
                            {
                                var installment = Ioc.InstallmentBusiness.GetListByToken(token).ToList();
                                if (installment != null)
                                {
                                    var club = installment.First().OrderItem.Order.Club;
                                    var paymentSettings = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                                    model.Amount = installment.Sum(c => c.Balance);
                                    model.Currency = club.Currency;
                                    model.EnablePaypalPayment = paymentSettings.EnablePaypalPayment;
                                    model.EnableCreditCardPayment = paymentSettings.EnableStripePayment;

                                    if (club.PartnerId.HasValue && club.IsSchool)
                                    {
                                        var partnerClub = club.PartnerClub;
                                        model.Currency = partnerClub.Currency;
                                    }
                                }

                                return View(model);
                            }
                        case JumbulaSubSystem.Invoice:
                            {
                                var transactions = _transactionActivityBusiness.GetInvoicedByToken(token).ToList();
                                if (transactions.Count == 0)
                                {
                                    model.IsPaid = true;
                                    return View("MakePayment", model);
                                }

                                else
                                {
                                    var club = transactions.First().Club;
                                    model.Amount = transactions.Sum(c => c.Amount);

                                    var paymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId);
                                    model.IsPaid = false;
                                    model.Currency = club.Currency;
                                    model.EnablePaypalPayment = paymentMethod.EnablePaypalPayment;
                                    model.EnableCreditCardPayment = paymentMethod.EnableStripePayment;
                                    if (club.PartnerId.HasValue && club.IsSchool)
                                    {
                                        var partnerClub = club.PartnerClub;

                                        model.Currency = partnerClub.Currency;
                                    }
                                }
                                return View(model);
                            }
                    }

                }


            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return HttpNotFound();
        }

        [AllowAnonymous]
        public virtual ActionResult PreapprovalRequest(string token)
        {
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    var order = _orderBusiness.GetOrdersByConfirmationID(token);
                    if (order != null)
                    {
                        var paidAmount = _transactionActivityBusiness.GetPaidAmountForOrder(order.Id);
                        var balance = order.OrderAmount - order.PaidAmount > 0 ? order.OrderAmount - order.PaidAmount : 0;
                        ViewBag.Balance = CurrencyHelper.FormatCurrencyWithPenny(balance, order.Club.Currency);
                        ViewBag.ConfirmationId = order.ConfirmationId;
                        return View();
                    }
                }

                return HttpNotFound();
            }
            catch
            {
                throw;
            }
        }


        public virtual ActionResult GetUtcDateTime()
        {
            return JsonNet(new { UtcDateTime = DateTime.UtcNow });
        }

        public ActionResult States(Countries country)
        {
            dynamic model = new ExpandoObject();

            var states = GetStates(country);
            var stateLabel = "State";

            if (country == Countries.Canada)
            {
                stateLabel = "Province";
            }

            model.States = states;
            model.StateLabel = stateLabel;

            return View("_States", model);
        }


        #region Private Method

        private string GetState(Countries country, string state)
        {
            string result = null;

            try
            {
                switch (country)
                {
                    case Countries.Canada:
                        {
                            var canadaState = EnumHelper.ParseEnum<CanadaStates>(state);
                            result = canadaState.ToDescription();
                        }
                        break;
                    case Countries.UnitedStates:
                        {
                            var usState = EnumHelper.ParseEnum<USStates>(state);
                            result = usState.ToDescription();
                        }
                        break;
                }
            }
            catch { }

            return result;
        }

        private List<SelectListItem> GetStates(Countries? country = Countries.UnitedStates)
        {
            var states = new List<SelectListItem>();

            switch (country)
            {
                case Countries.Canada:
                    {
                        states = Enum.GetValues(typeof(CanadaStates)).Cast<CanadaStates>().Select(d => new SelectListItem { Text = d.ToDescription(), Value = d.ToString() }).ToList();
                    }
                    break;
                case Countries.UnitedStates:
                    {
                        states = Enum.GetValues(typeof(USStates)).Cast<USStates>().Select(d => new SelectListItem { Text = d.ToDescription(), Value = d.ToString() }).ToList();
                    }
                    break;
            }

            return states;
        }

        private void CartCalculation(Cart cart, bool payByCash = false, bool isStripeEnbaled = false)
        {
            // bool needsUpdate = false;
            var discountMode = DiscountMode.OrdersInSeason;
            if (cart.Order.Club.PartnerId.HasValue)
            {
                discountMode = _clientBusiness.Get(cart.Order.Club.PartnerId.Value).DiscountMode;
            }
            else
            {
                discountMode = _clientBusiness.Get(cart.Order.ClubId).DiscountMode;
            }

            var orderItemCount = cart.Order.RegularOrderItems.Count();
            Ioc.DiscountBusiness.CalculateDiscount(cart.Order, discountMode);
            Ioc.discountEngine.CalculateDiscount(cart.Order, discountMode);
            Ioc.FollowupBusiness.OrderFollowUpForms(cart.Order);
            CalculateLateRegistrationFee(cart.Order);
            Ioc.CouponBusiness.CalulateCoupon(cart);

            var orderHasSubscriptionAndBeforeAfterCareItem = cart.Order.RegularOrderItems.Any(c => c.ProgramTypeCategory == ProgramTypeCategory.Subscription || c.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare);

            _cartBusiness.CalculateRegistrationFee(cart.Order);

            if (!orderHasSubscriptionAndBeforeAfterCareItem)
            {
                Ioc.InstallmentBusiness.CalculateInstallments(cart.Order, isStripeEnbaled);
            }

            if (orderHasSubscriptionAndBeforeAfterCareItem)
            {
                Ioc.DiscountBusiness.CalculateDiscount(cart.Order, discountMode);
                Ioc.discountEngine.CalculateDiscount(cart.Order, discountMode);
                Ioc.CouponBusiness.CalulateCoupon(cart);
                Ioc.InstallmentBusiness.CalculateInstallments(cart.Order, isStripeEnbaled);
            }

            CalculateSurChargeFee(cart.Order);

            if (cart.Order.OrderMode == OrderMode.Online)
            {
                CalculateConenienceFee(cart, payByCash);
            }

            if (cart.Order.RegularOrderItems.Any(o => !o.ProgramScheduleId.HasValue))
            {
                long? seasonId = null;

                if (cart.Order.RegularOrderItems.Any(o => o.SeasonId.HasValue && o.ProgramScheduleId.HasValue))
                {
                    seasonId = cart.Order.RegularOrderItems.Last(o => o.SeasonId.HasValue && o.ProgramScheduleId.HasValue).SeasonId;
                }

                foreach (var item in cart.Order.RegularOrderItems.Where(o => !o.ProgramScheduleId.HasValue))
                {
                    item.SeasonId = seasonId;
                }
            }

            var res = _orderBusiness.Update(cart.Order);
        }

        private void CalculateSurChargeFee(Order order)
        {

            if (order == null || order.OrderItems == null || !order.RegularOrderItems.Any())
            {
                return;
            }

            var club = _clubBusiness.Get(order.ClubId);

            List<Charge> surCharges = new List<Charge>();

            var showCombinedPriceOnlyForClub = false;

            if (club.Setting != null)
            {
                showCombinedPriceOnlyForClub = club.Setting.ShowCombinedPriceOnly;
            }

            if (club.Charges != null && club.Charges.Any(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.Surcharge))
            {
                surCharges.Add(club.Charges.FirstOrDefault(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.Surcharge));
            }

            if (club.PartnerClub != null && club.PartnerClub.Charges != null && club.PartnerClub.Charges.Any(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.PartnerSurcharge))
            {

                surCharges.Add(club.PartnerClub.Charges.FirstOrDefault(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.PartnerSurcharge));

            }

            var isDeleteSurCharge = false;
            foreach (var orderItem in order.RegularOrderItems.Where(c => c.ProgramScheduleId.HasValue))
            {
                if (orderItem.GetOrderChargeDiscounts() != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge))
                {
                    foreach (var charge in orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Surcharge || c.Category == ChargeDiscountCategory.PartnerSurcharge).ToList())
                    {
                        orderItem.OrderChargeDiscounts.Remove(charge);
                        orderItem.TotalAmount -= charge.Amount;
                        if (orderItem.PaymentPlanType == PaymentPlanType.Installment && orderItem.Installments.Any() && orderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription && orderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                        {
                            var firstInst = orderItem.Installments.First();
                            firstInst.Amount -= charge.Amount;
                        }
                    }

                }

                if (orderItem.GetOrderChargeDiscounts() != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience))
                {
                    foreach (var convenience in orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).ToList())
                    {
                        orderItem.OrderChargeDiscounts.Remove(convenience);
                        orderItem.TotalAmount -= convenience.Amount;
                    }
                }

            }

            if (surCharges.Any())
            {
                var countOrderItem = 0;

                foreach (var orderItem in order.RegularOrderItems.Where(c => c.ProgramScheduleId.HasValue))
                {

                    if (!order.RegularOrderItems.Any(o => o.TotalAmount != 0))
                        countOrderItem++;

                    if (orderItem.TotalAmount != 0)
                    {
                        countOrderItem++;
                    }

                    decimal addCoveFee = 0;
                    var timeType = Ioc.ProgramBusiness.GetClassScheduleTime(orderItem.ProgramSchedule.Program);

                    if (orderItem.OrderChargeDiscounts.Any(c => c.Category == ChargeDiscountCategory.Convenience))
                    {
                        addCoveFee = orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).Sum(c => c.Amount);
                    }

                    decimal addchargeFee = 0;

                    foreach (var surcharge in surCharges)
                    {
                        var couponCharege = orderItem.GetOrderChargeDiscounts().FirstOrDefault(c => c.Category == ChargeDiscountCategory.Coupon);

                        decimal orderItemsurchargeFee = 0;
                        decimal surchargeAmount = surcharge.Amount;

                        if (surcharge.DisableForZeroAmount && surcharge.AmountType == ChargeDiscountType.Fixed)
                        {
                            switch (surcharge.ApplyType)
                            {
                                case ChargeApplyType.Cart:

                                    if (order.CalculateOrderAmount == 0)
                                    {
                                        surchargeAmount = 0;
                                    }

                                    break;
                                case ChargeApplyType.OrderItem:

                                    if (orderItem.TotalAmount == 0)
                                    {
                                        surchargeAmount = 0;
                                    }

                                    break;

                                default:
                                    break;
                            }
                        }

                        if (club.PartnerId.HasValue && club.Domain.Equals("em-waplesmill", StringComparison.OrdinalIgnoreCase) && club.PartnerClub.Charges != null && club.PartnerClub.Charges.Any(c => c.IsDeleted == false && c.Category == ChargeDiscountCategory.PartnerSurcharge))
                        {
                            if (surcharge.Category == ChargeDiscountCategory.PartnerSurcharge && (!surcharge.DisableForZeroAmount || (surcharge.DisableForZeroAmount && surcharge.AmountType == ChargeDiscountType.Percent)))
                            {
                                surchargeAmount = 6;
                            }
                        }

                        if (orderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription && orderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                        {
                            if (couponCharege != null)
                            {
                                if (couponCharege.Coupon.CouponCalculateType == CalculationType.TotalAmount)
                                {
                                    var netFee = (surcharge.AmountType == ChargeDiscountType.Fixed) ? surchargeAmount : (((orderItem.TotalAmount - addCoveFee) * surchargeAmount) / 100);
                                    orderItemsurchargeFee = showCombinedPriceOnlyForClub && orderItem.ProgramTypeCategory == ProgramTypeCategory.Class ? CurrencyHelper.RoundCurrencyCeilingWithoutPenny(netFee) : CurrencyHelper.RoundCurrencyWithPenny(netFee);
                                }
                                else
                                {
                                    var netFee = (surcharge.AmountType == ChargeDiscountType.Fixed) ? surchargeAmount : (((orderItem.TotalAmount - addCoveFee) * surchargeAmount) / 100);
                                    orderItemsurchargeFee = showCombinedPriceOnlyForClub && orderItem.ProgramTypeCategory == ProgramTypeCategory.Class ? CurrencyHelper.RoundCurrencyCeilingWithoutPenny(netFee) : CurrencyHelper.RoundCurrencyWithPenny(netFee);

                                }
                            }
                            else
                            {
                                var netFee = (surcharge.AmountType == ChargeDiscountType.Fixed) ? surchargeAmount : ((orderItem.TotalAmount - addCoveFee) * surchargeAmount) / 100;

                                orderItemsurchargeFee = showCombinedPriceOnlyForClub && orderItem.ProgramTypeCategory == ProgramTypeCategory.Class ? CurrencyHelper.RoundCurrencyCeilingWithoutPenny(netFee) : CurrencyHelper.RoundCurrencyWithPenny(netFee);
                            }
                        }
                        else
                        {
                            var netFee = Ioc.SubscriptionBusiness.CalclulateSurcharge(orderItem, surcharge);
                            orderItemsurchargeFee = showCombinedPriceOnlyForClub && orderItem.ProgramTypeCategory == ProgramTypeCategory.Class ? CurrencyHelper.RoundCurrencyCeilingWithoutPenny(netFee) : CurrencyHelper.RoundCurrencyWithPenny(netFee);

                        }

                        if (orderItem.Mode == OrderItemMode.Noraml && orderItemsurchargeFee != 0)
                        {
                            if ((surcharge.ApplyType == ChargeApplyType.OrderItem) || (surcharge.AmountType == ChargeDiscountType.Fixed && surcharge.ApplyType == ChargeApplyType.Cart && countOrderItem == 1))
                            {
                                var charge = new OrderChargeDiscount()
                                {
                                    Amount = orderItemsurchargeFee,
                                    Category = surcharge.Category,
                                    Description = surcharge.Description,
                                    Name = surcharge.Name,
                                    Subcategory = ChargeDiscountSubcategory.Charge,
                                    OrderItemId = orderItem.Id,
                                    OrderItem = orderItem,
                                    Message = surcharge.Description,
                                    ChargeId = surcharge.Id
                                };

                                orderItem.OrderChargeDiscounts.Add(charge);
                                addchargeFee += orderItemsurchargeFee;
                            }
                        }
                    }

                    countOrderItem = countOrderItem == 1 ? countOrderItem += 1 : countOrderItem;
                    orderItem.TotalAmount += addchargeFee;

                    if (orderItem.PaymentPlanType == PaymentPlanType.Installment && orderItem.Installments.Any() && orderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription && orderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                    {
                        var firstInst = orderItem.Installments.First();

                        firstInst.Amount += addchargeFee;
                    }
                }
            }
        }

        private void CalculateLateRegistrationFee(Order order)
        {
            if (order == null || order.OrderItems == null || !order.RegularOrderItems.Any())
            {
                return;
            }


            foreach (var orderItem in order.RegularOrderItems.Where(c => c.ProgramScheduleId.HasValue))
            {
                if (orderItem.OrderChargeDiscounts != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.LateRegistrationFee))
                {
                    foreach (var charge in orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.LateRegistrationFee).ToList())
                    {
                        orderItem.OrderChargeDiscounts.Remove(charge);
                        orderItem.TotalAmount -= charge.Amount;
                        if (orderItem.PaymentPlanType == PaymentPlanType.Installment && orderItem.Installments.Any())
                        {
                            var firstInst = orderItem.Installments.First();
                            firstInst.Amount -= charge.Amount;
                        }
                    }

                }

                var seasonSetting = orderItem.Season == null ? Ioc.SeasonBusiness.Get(orderItem.SeasonId.Value).Setting : orderItem.Season.Setting;

                if (seasonSetting != null && seasonSetting.SeasonProgramInfoSetting != null && seasonSetting.SeasonProgramInfoSetting.LateRegistrationOpens != null && seasonSetting.SeasonProgramInfoSetting.LateRegistrationCloses != null && seasonSetting.SeasonProgramInfoSetting.LateRegistrationFee > 0 && seasonSetting.SeasonProgramInfoSetting.HasLateRegistration)
                {
                    var now = DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, order.Club.TimeZone);
                    if (now >= seasonSetting.SeasonProgramInfoSetting.LateRegistrationOpens && now <= seasonSetting.SeasonProgramInfoSetting.LateRegistrationCloses)
                    {
                        var charge = new OrderChargeDiscount()
                        {
                            Amount = seasonSetting.SeasonProgramInfoSetting.LateRegistrationFee,
                            Category = ChargeDiscountCategory.LateRegistrationFee,
                            Description = seasonSetting.SeasonProgramInfoSetting.LateRegistrationFeeMessage,
                            Name = string.IsNullOrEmpty(seasonSetting.SeasonProgramInfoSetting.LateRegistrationFeeMessage) ? ChargeDiscountCategory.LateRegistrationFee.ToDescription() : seasonSetting.SeasonProgramInfoSetting.LateRegistrationFeeMessage,
                            Subcategory = ChargeDiscountSubcategory.Charge,
                            OrderItemId = orderItem.Id,
                            OrderItem = orderItem
                        };
                        orderItem.OrderChargeDiscounts.Add(charge);
                        orderItem.TotalAmount += charge.Amount;
                        if (orderItem.PaymentPlanType == PaymentPlanType.Installment && orderItem.Installments.Any())
                        {
                            var firstInst = orderItem.Installments.First();

                            firstInst.Amount += charge.Amount;
                        }

                    }
                }
            }
        }

        private bool CalculateConenienceFee(Cart cart, bool payByCash)
        {
            var result = false;
            var order = cart.Order;

            foreach (var orderItem in order.RegularOrderItems)
            {
                if (orderItem.OrderChargeDiscounts != null && orderItem.OrderChargeDiscounts.Any(ch => !ch.IsDeleted))
                {
                    foreach (var charge in orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).ToList())
                    {
                        orderItem.OrderChargeDiscounts.Remove(charge);
                        orderItem.TotalAmount -= charge.Amount;
                        if (orderItem.PaymentPlanType == PaymentPlanType.Installment && orderItem.Installments.Any())
                        {
                            var firstInst = orderItem.Installments.First();
                            firstInst.Amount -= charge.Amount;
                        }
                    }
                    result = true;
                }
            }

            var club = _clubBusiness.Get(order.ClubId);
            var client = club.Client;
            var currency = club.Currency;

            var hasConvenienceFee = client.ConvenienceFee > 0;
            if (hasConvenienceFee && !payByCash)
            {
                AddConvToOrderItems(client, cart, currency);


            }
            if (club.PartnerId.HasValue && club.IsSchool)
            {
                client = _clientBusiness.Get(club.PartnerId.Value);
                currency = club.PartnerClub.Currency;
                hasConvenienceFee = client.ConvenienceFee > 0;
                if (hasConvenienceFee && !payByCash)
                {
                    AddConvToOrderItems(client, cart, currency);
                }


            }
            return result;
        }

        private bool AddConvToOrderItems(Client client, Cart cart, CurrencyCodes currency)
        {
            var convenienceFee = client.ConvenienceFee;
            decimal totalConvFee = 0;
            var order = cart.Order;
            foreach (var orderItem in order.RegularOrderItems.Where(c => c.ProgramScheduleId.HasValue && c.ProgramTypeCategory != ProgramTypeCategory.Subscription && c.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare))
            {
                if (orderItem.TotalAmount <= 0)
                {
                    continue;
                }
                decimal orderItemConFee = 0;
                decimal addCoveFee = 0;

                if (orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience))
                {
                    addCoveFee = orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).Sum(c => c.Amount);
                }

                if (orderItem.PaymentPlanType == PaymentPlanType.Installment && orderItem.Installments.Any())
                {
                    var firstInst = orderItem.Installments.First();
                    var netFee = client.ConvenienceFeeType == ChargeDiscountType.Percent ? (((firstInst.Amount - addCoveFee) * convenienceFee) / 100) : convenienceFee;
                    orderItemConFee = CurrencyHelper.RoundCurrencyWithPenny(netFee);
                    firstInst.Amount += orderItemConFee;
                }
                else
                {
                    var netFee = client.ConvenienceFeeType == ChargeDiscountType.Percent ? (((orderItem.TotalAmount - addCoveFee) * convenienceFee) / 100) : convenienceFee;
                    orderItemConFee = CurrencyHelper.RoundCurrencyWithPenny(netFee);

                }

                totalConvFee += orderItemConFee;
                orderItem.OrderChargeDiscounts.Add(new OrderChargeDiscount
                {
                    Message = client.ConvenienceFeeMessage,
                    Amount = orderItemConFee,
                    Category = ChargeDiscountCategory.Convenience,
                    Subcategory = ChargeDiscountSubcategory.Charge,
                    Description = string.Format("{0}: {1}", ChargeDiscountCategory.Convenience.ToDescription(), CurrencyHelper.FormatCurrencyWithPenny(orderItemConFee, currency)),
                    Name = string.IsNullOrEmpty(client.ConvenienceFeeLabel) ? ChargeDiscountCategory.Convenience.ToDescription() : client.ConvenienceFeeLabel
                });
                orderItem.TotalAmount += orderItemConFee;
            }

            cart.Order.OrderAmount = totalConvFee;
            return true;
        }

        //private void OrderCompleted(Order order, string orderHistoryDescription, int? userId,int CurrentUserId)
        //{
        //    Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Initiated, String.Format(orderHistoryDescription, order.ConfirmationId), order.Id, userId.HasValue ? userId.Value : CurrentUserId);
        //    Ioc.OrderSessionBusiness.CreateSessions(order);
        //}

        #endregion

        private void SetValidationToCookie(ModelStateDictionary modelState, PayViewModel model)
        {
            var errors = new Dictionary<string, string>();
            var Data = new Dictionary<string, string>();
            var directPaymentModel = new Dictionary<string, Dictionary<string, string>>();

            //set cookie
            foreach (var item in modelState)
            {
                foreach (var error in item.Value.Errors)
                {
                    errors.Add(item.Key, error.ErrorMessage);
                }
            }

            Data.Add("CardHolderName", model.CardHolderName);
            Data.Add("Month", model.Month);
            Data.Add("Year2Char", model.Year2Char.ToString());
            Data.Add("CardNumber", model.CardNumber);
            Data.Add("Cvv", model.CVV);

            Data.Add("Street", model.Street);
            Data.Add("ZipCode", model.ZipCode);
            Data.Add("City", model.City);
            Data.Add("State", model.State.ToString());

            directPaymentModel.Add("Errors", errors);
            directPaymentModel.Add("Data", Data);

            var jsonDirectPaymentModel = JsonConvert.SerializeObject(directPaymentModel);
            var name = "";
            if (model.UserId == null && model.InvoiceId == null)
            {
                name = model.ConfirmationId;
            }
            else if (model.UserId.HasValue)
            {
                name = model.UserId.ToString();
            }
            else if (model.InvoiceId.HasValue)
            {
                name = model.InvoiceId.ToString();
            }

            //set errors and data to cookie
            Response.SetCookie((new System.Web.HttpCookie(name, StringCipher.Encrypt(jsonDirectPaymentModel, DateTime.UtcNow.ToShortDateString().Replace("/", "")))));

        }

        private void CheckCardValidation(ModelStateDictionary modelState, PayViewModel model, ref PostalAddress address)
        {
            switch (model.PaymentMethod)
            {
                case PaymentMethod.None:
                    break;

                case PaymentMethod.Card:
                    {
                        if (address != null && !string.IsNullOrEmpty(address.Address) & address.Address.Length > 256)
                        {
                            ModelState.AddModelError("Address", "Address is too long.");
                        }

                        //try
                        //{
                        //Utilities.GetGEO(model.StrAddress, ref address);
                        //}
                        //catch
                        //{
                        //    throw new Exception("Problem in auto complete with google api");
                        //    //ModelState.AddModelError("StrAddress", "We could not autocomplete your address. Please try another address or contact your organization administrator.");
                        //}

                        //if (address == null)
                        //{
                        //    ModelState.AddModelError("StrAddress", "We could not autocomplete your address. Please try another address or contact your organization administrator.");
                        //}
                    }
                    break;
                case PaymentMethod.Paypal:
                case PaymentMethod.BankTransfer:
                case PaymentMethod.Check:
                case PaymentMethod.Echeck:
                case PaymentMethod.Express:
                case PaymentMethod.Scholarship:
                case PaymentMethod.FinancialAid:
                case PaymentMethod.Other:
                case PaymentMethod.Cash:

                    {
                        modelState.Remove("CardNumber");
                        modelState.Remove("CardHolderName");
                        modelState.Remove("StrAddress");
                        modelState.Remove("CVV");
                        modelState.Remove("Month");
                        modelState.Remove("Year2Char");

                        modelState.Remove("State");
                        modelState.Remove("ZipCode");
                        modelState.Remove("City");
                        modelState.Remove("Street");

                    }
                    break;

            }

            if (!string.IsNullOrEmpty(model.CardHolderName))
            {
                var count = _userCreditCardBusiness.CountWords(model.CardHolderName);

                if (count < 2)
                {
                    ModelState.AddModelError("CardHolderName", "Please enter first name and last name.");
                }
            }
        }
    }
}