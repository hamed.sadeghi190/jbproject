﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AddDistrictViewModel : BaseViewModel<Club>
    {
        public AddDistrictViewModel()
        {
            OrganizationTimeZone = DropdownHelpers.SeedTimeZones();
            SelectedTimeZone = "-1";
        }
        [Display(Name = "Domain")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 3)]
        [RegularExpression("[A-Za-z0-9-]+",
             ErrorMessage = "{0} cannot contain any spaces or special characters such as *, &, !, _, etc")]
        public string Domain { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }
        [Display(Name = "Address")]
        [Required(ErrorMessage = "{0} is required.")]
        public string Address { get; set; }
        public int ClubId { get; set; }
        public SelectKeyValue<int> SelectedState { get; set; }
        public List<SelectKeyValue<int>> Countries { get; set; }
        public SelectKeyValue<int> SelectedCountry { get; set; }
        public string SelectedTimeZone { get; set; }
        public List<SelectKeyValue<string>> OrganizationTimeZone { get; set; }

    }
}