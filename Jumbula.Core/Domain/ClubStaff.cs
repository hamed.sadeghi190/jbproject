﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class ClubStaff : BaseEntity, IProtectedResource
    {
        public ClubStaff()
        {
            Programs = new HashSet<Program>();
        }

        [ForeignKey("JbUserRole")]
        public int UserRoleId { get; set; }
        public virtual JbUserRole JbUserRole { get; set; }

        [ForeignKey("Club")]
        public int ClubId { get; set; }

        public virtual Club Club { get; set; }

        [ForeignKey("ContactId")]
        public virtual ContactPerson Contact { get; set; }

        public int? ContactId { get; set; }

        public StaffStatus Status { get; set; }

        public DateTime? DateOfBackgroundCheck { get; set; }
        public virtual ICollection<Program> Programs { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsFrozen { get; set; }

        private RoleCategory? _roleOnClub;

        [NotMapped]
        public RoleCategory RoleOnClub
        {
            get
            {
                if (!_roleOnClub.HasValue)
                {
                    _roleOnClub = EnumHelper.ParseEnum<RoleCategory>(this.JbUserRole.Role.Name);
                }

                return _roleOnClub.Value;
            }
            set
            {
                _roleOnClub = value;
            }
        }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if(ClubId == clubId || (Club.PartnerId.HasValue && Club.PartnerId == clubId))
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

    public enum StaffStatus
    {
        Pending,
        Active,
    }
}
