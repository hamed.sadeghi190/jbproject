
ALTER TABLE [dbo].[UserCheckins] add [AddressId] [int] NOT NULL
GO

ALTER TABLE [dbo].[UserCheckins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserCheckins_dbo.PostalAddresses_AddressId] FOREIGN KEY([AddressId])
REFERENCES [dbo].[PostalAddresses] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[UserCheckins] CHECK CONSTRAINT [FK_dbo.UserCheckins_dbo.PostalAddresses_AddressId]
GO

ALTER TABLE [dbo].[UserCheckins] DROP COLUMN Lat
GO

ALTER TABLE [dbo].[UserCheckins] DROP COLUMN Lng
GO
