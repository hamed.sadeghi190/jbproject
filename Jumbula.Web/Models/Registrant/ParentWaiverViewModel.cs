﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ParentWaiverViewModel
    {
        public int Id { get; set; }

        public string Signiture { get; set; }

        public string Text { get; set; }
        public DateTime Date { get; set; }

        public string Name { get; set; }

        public bool Agreed { get; set; }

        public int WaiverId { get; set; }
        public string ProgramName { get; set; }
        public WaiverConfirmationType WaiverConfirmationType { get; set; }
    }

}