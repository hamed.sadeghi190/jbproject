﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FinancePortalViewModel
    {
        public string ClubName { get; set; }
        public string FullName { get; set; }
        public string SeasonName { get; set; }
        public string ProgramName { get; set; }
        public string UserEmail { get; set; }
        public string Date { get; set; }
        public string PayerEmail { get; set; }
        public string Confirmation { get; set; }
        public TransactionType TransactionType { get; set; }
        public TransactionCategory TransactionCategory { get; set; }
        public string CheckId { get; set; }
        public PaymentMethod? PaymentMethod { get; set; }
        public string PaypalHandler { get; set; }
        public string Note { get; set; }
        public decimal Payment { get; set; }
        public decimal Charge { get; set; }
    }
}