﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class ClubRevenue : BaseEntity
    {
       
      public int Minimum { get; set; }
        public int Maximum { get; set; }
        public decimal Price { get; set; }
        [NotMapped]
        public string StrPrice => $"${Price:n0}";

        [NotMapped]
        public string Description
        {
            get
            {
                if(Minimum== 0 && Maximum==0)
                {
                    return "I'm just getting started";
                }
                 if(Minimum==0 && Maximum >0)
                {
                    return "Less than $"+Maximum;
                }
                return string.Format("${0:n0}", Minimum) + " - " + string.Format("${0:n0}", Maximum); 
            }
        }

        [NotMapped]
        public string FreeTrialDesc
        {
            get
            {
                if (Maximum < 1000000)
                {
                    return Description;
                }
                else
                    return "Over $1,000,000";

            }
        }
    }
}
