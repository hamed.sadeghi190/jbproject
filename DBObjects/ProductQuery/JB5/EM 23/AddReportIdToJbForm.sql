ALTER TABLE [dbo].[JbForms] Add [EventRoaster_Id] [int] NULL

ALTER TABLE [dbo].[JbForms]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbForms_dbo.EventRoasters_EventRoaster_Id] FOREIGN KEY([EventRoaster_Id])
REFERENCES [dbo].[EventRoasters] ([Id])
GO

ALTER TABLE [dbo].[JbForms] CHECK CONSTRAINT [FK_dbo.JbForms_dbo.EventRoasters_EventRoaster_Id]
GO
