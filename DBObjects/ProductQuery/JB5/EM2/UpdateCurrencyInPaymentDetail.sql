
update PaymentDetails set Currency='0' where Currency='USD'
Go
update PaymentDetails set Currency='1' where Currency='GBP'
Go
update PaymentDetails set Currency='2' where Currency='CNY'
Go
update PaymentDetails set Currency='3' where Currency='CAD'
Go
update PaymentDetails set Currency='0' where Currency=''
Go
update PaymentDetails set Currency='0' where Currency is Null
Go

alter TABLE [dbo].[PaymentDetails] alter column [Currency] [smallint] NOT NULL

