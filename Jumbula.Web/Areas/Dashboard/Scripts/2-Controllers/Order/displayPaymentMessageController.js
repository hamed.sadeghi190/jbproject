﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('displayPaymentMessageController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/Order";
        if ("comesFromOrderItem" in window) {

            window.comesFromOrderItem = false;
        }
            
        $http.get($scope.baseUrl + '/GetPaymentMessageModel/?orderItemId=' + $stateParams.orderItemId + "&status=" + $stateParams.status).success(function (data, status, headers, config) {
            $scope.Model = data;
            if ($stateParams.message!=null){
                $scope.Model.Message = $stateParams.message;
            }
           
          
        }).error(function (data, status, headers, config) {
            $scope.MC.handleErrors($scope, data);
        });

    }]);
})();