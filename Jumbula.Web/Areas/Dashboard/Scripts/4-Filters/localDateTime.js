﻿(function () {
    'use strict';

    var options = { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric' };
    angular.module('dashboardApp').filter('localDateTime', [function () {
        return function (val) {
            return (new Date(val)).toLocaleString('en-US', options);
        };
    }]);

    angular.module('dashboardApp').filter('utcDateTime', [function () {
        return function (val) {
            return (new Date(val)).toGMTString();
        };
    }]);

})();