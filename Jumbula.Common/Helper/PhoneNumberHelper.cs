﻿using System;


namespace Jumbula.Common.Helper
{
    public static class PhoneNumberHelper
    {
        //TODO: Add an extension method for IsNullOrEmpty() to simlfy the usage
        public static string FormatPhoneNumber(string phone)
        {
            if (string.IsNullOrEmpty(phone)) return phone;

            phone = string.Join("", phone.Split('-', '(', ')'));

            if (phone.Length != 10) return phone;

            string result;
            try
            {
                result = Convert.ToInt64(phone).ToString(Constants.Constants.FormatConstants.UsPhoneNumber);
            }
            catch (Exception)
            {
                result = phone;
            }

            return result;
        }
    }
}