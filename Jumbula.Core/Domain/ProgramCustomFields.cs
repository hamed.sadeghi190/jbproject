﻿namespace Jumbula.Core.Domain
{

    public class ProgramCustomFields
    {
        public int? CommisionRate { get; set; }
        public decimal? Noshowpenalty { get; set; }
        public decimal? LateCheckInPenalty { get; set; }
        public string SpecialRequests { get; set; }
    }
    
}
