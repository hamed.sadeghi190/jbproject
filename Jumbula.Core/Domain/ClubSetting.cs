﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Constants = Jumbula.Common.Constants.Constants;


namespace Jumbula.Core.Domain
{

    public class ClubSetting
    {
        public ClubSetting()
        {
            IsDiscountApplyOnOriginPrice = true;
            ShowSchoolLogo = true;
            RegistrationPageLogoRedirectUrl = new ClubRedirectUrl();
            LogOutRedirectUrl = new ClubRedirectUrl();
            BrowseMoreRedirectUrl = new ClubRedirectUrl();
            IsNewFamily = false;
            AppearanceSetting = new ClubAppearanceSetting();
            Notifications = new ClubNotificationSetting();
            Agenda = new AgendaSettings();
            ClubProgram = new ClubProgramSettings();
            OnSiteInfo = new SchoolSettingOnSiteInfo();
        }

        public List<string> InstructorBackgroundCheckCertificate { get; set; }
        public List<string> InstructorBackgroundCheckCertificateDate { get; set; }
        public CertificateOfInsurances CertificateOfInsurances { get; set; }
        public BusinessType BusinessType { get; set; }
        public ClubRedirectUrl RegistrationPageLogoRedirectUrl { get; set; }
        public ClubRedirectUrl LogOutRedirectUrl { get; set; }

        public ClubRedirectUrl ConfirmationRedirectUrl { get; set; }
        public ClubRedirectUrl BrowseMoreRedirectUrl { get; set; }
        public string BrowseMoreText { get; set; }

        public string AnotherRegisterButtonText { get; set; }
        public List<string> NotificationEmails { get; set; }
        public int StaffResponder { get; set; }
        public int StaffSignatory { get; set; }
        public int? PartnerMessageResponder { get; set; }
        public string LegalName { get; set; }
        public DateTime? MemberSince { get; set; }
        public DateTime? AgreementExpirationDate { get; set; }
        public string TaxIDFEIN { get; set; }
        public string TaxIDSSN { get; set; }
        public string EMDiscountSchools { get; set; }

        public bool Provider1099 { get; set; }

        public bool TaxExemptNonprofit { get; set; }

        public bool IsNewFamily { get; set; }
        public string NewFamilyNotification { get; set; }
        public bool IsDiscountApplyOnOriginPrice { get; set; }
        public bool ShowCombinedPriceOnly { get; set; }
        public bool HideEnrollmentCapacityInClassPage { get; set; }

        public bool HideCouponInCart { get; set; }
        public ClubAppearanceSetting AppearanceSetting { get; set; }
        public List<string> ListOfHolidayDates { get; set; }
        public ClubNotificationSetting Notifications { get; set; }
        public List<string> ListOfHolidayAMDates { get; set; }
        public List<string> ListOfHolidayPMDates { get; set; }
        public int? SalesContactUserId { get; set; }
        public string RevenueShare { get; set; }
        public int? StaffingRatio { get; set; }
        public int? NCESID { get; set; }
        public string FirstProviderId { get; set; }
        public string SecondProviderId { get; set; }

        public AgendaSettings Agenda { get; set; }
        public ClubProgramSettings ClubProgram { get; set; }
        public bool ShowSchoolLogo { get; set; }

        public bool EnableExpressPaymentInOfflineMode { get; set; }

        public bool EnableViewParentDashboard { get; set; }

        public SchoolSettingOnSiteInfo OnSiteInfo { get; set; }

        public string OnSiteCoordinatorName { get; set; }

        public string OnSiteCoordinatorEmail { get; set; }
        public AutoChargePolicy AutoChargePolicy { get; set; }
        public DelinquentPolicy DelinquentPolicy { get; set; }
        public DuplicateEnrollmentPolicy DuplicateEnrollmentPolicy { get; set; }

        public ProgramsPolicy ProgramsPolicy { get; set; }

    }
    public class ClubRedirectUrl
    {
        public ClubRedirectUrl()
        {
            RedirectUrlType = RedirectUrlTypes.JumbulaPage;
        }
        public ClubRedirectUrl(string redirectTypeId, string redirectUrl)
        {
            RedirectUrlType = (RedirectUrlTypes)(Convert.ToInt16(redirectTypeId));


            if (RedirectUrlType == RedirectUrlTypes.Other)
            {
                RedirectUrl = redirectUrl;
            }
        }
        public RedirectUrlTypes RedirectUrlType { get; set; }
        public string RedirectUrl { get; set; }
    }

    public class Certificate
    {
        public Certificate()
        {
            CertificateFileName = new List<string>();
        }
        public CertificateOfInsuranceTypes CertificateOfInsuranceType { get; set; }
        public List<string> CertificateFileName { get; set; }
    }
    public class CertificateOfInsurances
    {
        public CertificateOfInsurances()
        {
            CertificateList = new List<Certificate>();
        }
        public List<Certificate> CertificateList { get; set; }
        public DateTime? PolicyExpirationDate { get; set; }
    }

    public class ClubNotificationSetting
    {
        public ClubNotificationSetting()
        {
            Email = new EmailNotificationSetting();
            Report = new ReportNotificationSetting();
            Catalog = new CatalogNotificationSetting();
        }

        public EmailNotificationSetting Email { get; set; }
        public ReportNotificationSetting Report { get; set; }
        public CatalogNotificationSetting Catalog { get; set; }
    }

    public class EmailNotificationSetting
    {
        private string _contentConfirmationEmail;

        public string ContentConfirmationEmail
        {
            get
            {
                return !string.IsNullOrEmpty(_contentConfirmationEmail) ? _contentConfirmationEmail : Constants.Notification_Confirmation_Content;
            }

            set
            {
                _contentConfirmationEmail = value;
            }
        }
        private string _contentCancellationEmail;

        public string ContentCancellationEmail
        {
            get
            {
                return !string.IsNullOrEmpty(_contentCancellationEmail) ? _contentCancellationEmail : Constants.Notification_Cancellation_Content;
            }

            set
            {
                _contentCancellationEmail = value;
            }
        }
        public string ContentAbsentee { get; set; }
    }

    public class ReportNotificationSetting
    {
        private string _dependentCareDescription;

        public string DependentCareDescription
        {
            get
            {
                return !string.IsNullOrEmpty(_dependentCareDescription) ? _dependentCareDescription : Constants.Notification_DependentCareDescription;
            }

            set
            {
                _dependentCareDescription = value;
            }
        }
    }

    public class CatalogNotificationSetting
    {
        public bool SendMessageToProviderOnClassSchedule { get; set; }
    }
    public class ClubAppearanceSetting
    {
        public bool HideProgramDates { get; set; }
        public bool HidePhoneNumber { get; set; }
        public string ContactBoxBackColor { get; set; }
        public string TestimonialsBoxBackColor { get; set; }
        public string TestimonialsBoxColor { get; set; }
        public string TestimonialsTitle { get; set; }
        public string TuitionLabelTextColor { get; set; }
        public string PriceTextColor { get; set; }
        public string PriceBackColor { get; set; }
        public string ERButtonTextColor { get; set; }
        public string ERButtonBackColor { get; set; }
        public string ERButtonTextLabel { get; set; }
        public bool HideProgramDatesInCart { get; set; }
        public bool ShowProviderNameInClassPage { get; set; }
        public bool HideGoogleMapInClassPage { get; set; }
    }

    public class AgendaSettings
    {
        public bool IsEnabled { get; set; }
        public bool IsExactDay { get; set; }
        public DateTime? ExactTime { get; set; }
        public int HourAfterClassEnds { get; set; }
        public string Subject { get; set; }
        public string Wrapper { get; set; }
    }

    public class ClubProgramSettings
    {
        public bool EnablePriceHidden { get; set; }
        public bool HideAllProgramsPrice { get; set; }
        public List<long> HiddenPricePrograms { get; set; }
    }
    public class AutoChargePolicy
    {
        public AutoChargePolicy()
        {
        }

        public AutoChargePolicy(bool attemptsNotSet)
        {
            if (!attemptsNotSet) return;

            Attempts = new List<AutoChargeAttempt>();

            var attempt = new AutoChargeAttempt
            {
                AttemptOrder = 1,
                IntervalDays = 0,
                AdminEmailTemplate = Common.Constants.ClubSetting.AutoChargeAttemptOneDefaultAdminFailEmailTemplate,
                ParentEmailTemplate = Common.Constants.ClubSetting.AutoChargeAttemptOneDefaultParentFailedEmailTemplate,
            };

            Attempts.Add(attempt);

            for (var i = 2; i <= Common.Constants.ClubSetting.CountOfAttempts; i++)
            {
                Attempts.Add(new AutoChargeAttempt() { AttemptOrder = i });
            }

            Attempts.Last().Enabled = false;
        }

        public List<AutoChargeAttempt> Attempts { get; set; }
        public bool SuccessEmailToAdmin { get; set; } = true;
        public string SuccessAdminEmailTemplate { get; set; } = Common.Constants.ClubSetting.AutoChargeDefaultAdminSuccessEmailTemplate;
        public string SuccessParentEmailTemplate { get; set; } = Common.Constants.ClubSetting.AutoChargeDefaultParentSuccessEmailTemplate;
        public bool SendSummeryEmail { get; set; }
        public bool IsCombinePayments { get; set; }

        public bool HasReplyTo { get; set; }

        public string ReplyToEmailAddress { get; set; }
    }

    public class DuplicateEnrollmentPolicy
    {
        public bool PreventSameClass { get; set; }
        public bool PreventOtherClasses { get; set; }
        public bool PreventSameCampSchedule { get; set; }
        public bool PreventMultipleCampDropIn { get; set; }
        public bool PreventSameProgramBeforeAfterWeekDay { get; set; }
    }

    public class AutoChargeAttempt
    {
        public int IntervalDays { get; set; } = 2;
        public int AttemptOrder { get; set; }
        public string AdminEmailTemplate { get; set; } = Common.Constants.ClubSetting.AutoChargeDefaultAdminFailEmailTemplate;
        public string ParentEmailTemplate { get; set; } = Common.Constants.ClubSetting.AutoChargeDefaultParentFailedEmailTemplate;
        public bool FailedEmailToParent { get; set; } = true;
        public bool FailedEmailToAdmin { get; set; } = true;
        public bool Enabled { get; set; } = true;
    }

    public class ProgramsPolicy
    {
        public SubscriptionProgramPolicy SubscriptionProgramPolicy { get; set; }
        public ProgramGeneralPolicy GeneralPolicy { get; set; }

    }

    public class SubscriptionProgramPolicy
    {
        public SubscriptionProgramPolicy()
        {
            TransferOrderPolicy = new TransferOrderPolicy();
        }

        private bool? _enabledDesiredStartDateProrate;
        public bool EnabledDesiredStartDateProrate
        {
            get
            {
                return _enabledDesiredStartDateProrate.HasValue ? _enabledDesiredStartDateProrate.Value : true;
            }

            set
            {
                _enabledDesiredStartDateProrate = value;
            }
        }

        public bool EnabledCancellationProrate { get; set; }

        public TransferOrderPolicy TransferOrderPolicy { get; set; }

    }

    public class ProgramGeneralPolicy
    {
        private bool? _enabledRefundAutomaticallyInCancellationOrder;
        public bool EnabledRefundAutomaticallyInCancellationOrder
        {
            get
            {
                return _enabledRefundAutomaticallyInCancellationOrder.HasValue ? _enabledRefundAutomaticallyInCancellationOrder.Value : true;
            }

            set
            {
                _enabledRefundAutomaticallyInCancellationOrder = value;
            }
        }

        private bool? _enabledCancellationFee;
        public bool EnabledCancellationFee
        {
            get
            {
                return _enabledCancellationFee.HasValue ? _enabledCancellationFee.Value : true;
            }

            set
            {
                _enabledCancellationFee = value;
            }
        }

        private bool? _enabledTransferFee;
        public bool EnabledTransferFee
        {
            get
            {
                return _enabledTransferFee.HasValue ? _enabledTransferFee.Value : true;
            }

            set
            {
                _enabledTransferFee = value;
            }
        }

        private ProrationMethod? _perorationMethod;
        public ProrationMethod PerorationMethod
        {
            get
            {
                return _perorationMethod.HasValue ? _perorationMethod.Value : ProrationMethod.CalculateBySessions;
            }

            set
            {
                _perorationMethod = value;
            }
        }

    }

    public class TransferOrderPolicy
    {
        private bool? _enabledDowngradeProrate;
        public bool EnabledDowngradeProrate
        {
            get
            {
                return _enabledDowngradeProrate.HasValue ? _enabledDowngradeProrate.Value : true;

            }

            set
            {
                _enabledDowngradeProrate = value;
            }

        }

        public bool EnabledDowngradeExpensivePart { get; set; }

        private bool? _enabledUpgradeProrate;
        public bool EnabledUpgradeProrate
        {
            get
            {
                return _enabledUpgradeProrate.HasValue ? _enabledUpgradeProrate.Value : true;
            }
        }
    }

    public class DelinquentPolicy
    {
        public int AutoChargeFailAttemptNumber { get; set; } = 0;
        public int ManualInstallmentDelayDays { get; set; } = 0;
        public DateTime? FamilyBalanceNoLimitDate { get; set; } = null;

        [RegularExpression(Validation.PriceRegex, ErrorMessage = Validation.PriceRegexErrorMessage)]
        [Display(Name = "Minimum unpaid balance")]
        public decimal FamilyBalanceMinPrice { get; set; } = decimal.Zero;
        public bool AllowEnrollOnDelinquent { get; set; }
        public string NotAllowEnrollTemplate { get; set; } = Delinquent.NotAllowEnrollTemplate;
        public string AllowEnrollTemplate { get; set; } = Delinquent.AllowEnrollTemplate;
        public bool ExpelChildOnDelinquent { get; set; }
        public string ExpelChildTemplate { get; set; } = Delinquent.ExpelChildTemplate;
    }

}
