﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public static class CategoryModel
    {


        public static int ChessId
        {
            get
            {
                return (int)SportCategories.Chess;
            }
        }

        public static int TableTennisId
        {
            get
            {
                return (int)SportCategories.TableTennis;
            }
        }

        public static int WrestlingId
        {
            get
            {
                return (int)SportCategories.Wrestling;
            }
        }


    }



    public class CategoryViewModel
    {
        public int Id { get; set; }

        public int? ParentId { get; set; }

        public string Name { get; set; }

        public string Label { get; set; }

        public string Route { get; set; }

        public string LogoUri { get; set; }

        public bool SupportTourney { get; set; }

        public bool IsPrimary { get; set; }

        public CategoryConfigurationModels Configuration { get; set; }

        public SubDomainCategories Type { get; set; }

        public BrowseCategory BrowseCategory { get; set; }

        public int? Order { get; set; }
    }

 

    public class DisplayCategoryViewModel
    {
        public ICollection<Category> Categories { get; set; }

        public int CategoryId { get; set; }

        public int ClubCategoryId { get; set; }
    }
}