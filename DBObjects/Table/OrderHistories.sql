
IF OBJECT_ID('[dbo].[OrderHistories]') IS NULL
BEGIN
CREATE TABLE [dbo].[OrderHistories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[ActionDate] [datetime] NOT NULL,
	[Action] [tinyint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[UserId] [int] NOT NULL)
END
GO

---<<ADD PRIMARY KEY>>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.OrderHistories')
BEGIN
	ALTER TABLE [dbo].[OrderHistories] ADD CONSTRAINT [PK_dbo.OrderHistories] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

ALTER TABLE [dbo].[OrderHistories] ADD  DEFAULT ((0)) FOR [UserId]
GO

ALTER TABLE [dbo].[OrderHistories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderHistories_dbo.Orders_OrderId] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OrderHistories] CHECK CONSTRAINT [FK_dbo.OrderHistories_dbo.Orders_OrderId]
GO


