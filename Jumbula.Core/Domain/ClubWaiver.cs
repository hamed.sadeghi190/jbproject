﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class ClubWaiver : BaseEntity
    {
        [Required]
        [MaxLength(128)]
        public string Name { get; set; }

        [Required]
        public string Text { get; set; }

        public DateTime DateCreated { get; set; }

        [ForeignKey("Club_Id")]
        public virtual Club Club { get; set; }

        public int Club_Id { get; set; }

        public bool IsDeleted { get; set; }
        public virtual ICollection<Program> Programs { get; set; }
        public bool IsRequired { get; set; }
        [Required]
        public WaiverConfirmationType WaiverConfirmationType { get; set; }
        public int Order { get; set; }
    }

    public enum WaiverConfirmationType : byte
    {

        [Description("Agreed")]
        Agree = 0,
        [Description("Singed")]
        Signature = 1,
    }
}
