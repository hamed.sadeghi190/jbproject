﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FamilyCreditCardsListViewModel
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public int UserClubRoleId { get; set; }
        public string UserClubRole { get; set; }
        [JsonIgnore]
        public IEnumerable<PlayerProfile> Players { get; set; }


        public bool Status { get; set; }
        public string PlayersName
        {
            get
            {
                if (Players != null && Players.Any())
                {
                    return string.Join(", ", Players.Select(c => c.Contact.FullName));
                }
                return string.Empty;
            }
        }

        public string LastDigits { get; set; }

        public string Year { get; set; }

        public string Month { get; set; }
        public string ExpiryDate
        {
            get { return Month != null ? string.Format("{0}/{1}", Month, Year) : "-"; }
        }
        public string Name { get; set; }

        public string Brand { get; set; }
        public bool Isdefault { get; set; }



    }
}