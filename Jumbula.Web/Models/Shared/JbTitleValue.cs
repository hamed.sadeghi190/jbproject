﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class JbTitleValue
    {
        public JbTitleValue()
        {

        }      
        public JbTitleValue(string title, int value)
            : this()
        {
            this.Title = title;
            this.Value = value;
        }
        public string Title { get; set; }
        public int Value { get; set; }
    }

    public class JbTitleValue<T>
    {
        public JbTitleValue()
        {

        }
        public JbTitleValue(string title, T value)
            : this()
        {
            this.Title = title;
            this.Value = value;
        }
        public string Title { get; set; }
        public T Value { get; set; }
    }

}