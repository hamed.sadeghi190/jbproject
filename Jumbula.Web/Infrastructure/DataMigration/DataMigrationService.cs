﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Newtonsoft.Json;
using NPOI.Util;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace Jumbula.Web.Infrastructure.DataMigration
{
    public class DataManipulationService : IDataManipulationService
    {
        IApplicationUserManager<JbUser, int> _userManager;

        public DataManipulationService(IEntitiesContext context, IApplicationUserManager<JbUser, int> userManager)
        {
            DataContext = context;
            _userManager = userManager;
        }

        public void SetControllerContext(ControllerContext controllerContext)
        {
            ControllerContext = controllerContext;
        }

        private ControllerContext ControllerContext { get; set; }

        private IEntitiesContext DataContext { get; set; }

        public List<Core.Domain.DataMigration> GetList()
        {
            return DataContext.Set<Core.Domain.DataMigration>().ToList();
        }

        public List<Core.Domain.DataMigration> GetResults(string name)
        {
            return DataContext.Set<Core.Domain.DataMigration>().Where(d => d.RunDate.HasValue && d.Data.Equals(name, StringComparison.OrdinalIgnoreCase)).ToList();
        }

        public Core.Domain.DataMigration Get(int id)
        {
            return DataContext.Set<Core.Domain.DataMigration>().SingleOrDefault(d => d.Id == id);
        }

        public List<Core.Domain.DataMigration> GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool Create(Core.Domain.DataMigration dataMigration)
        {
            DataContext.Set<Core.Domain.DataMigration>().Add(dataMigration);

            var result = DataContext.SaveChanges();

            return result > 0;
        }

        public bool Edit(Core.Domain.DataMigration dataMigration)
        {
            var result = DataContext.SaveChanges();

            return result > 0;
        }

        public bool SaveSource(Stream stream, int id)
        {
            byte[] fileBytes = IOUtils.ToByteArray(stream);

            var dataMigration = Get(id);

            dataMigration.Source = fileBytes;

            var result = Edit(dataMigration);

            return result;
        }

        public DataManipulationGeneralResult Manipulate(int id, int? from = null, int? endPoint = null)
        {
            DataManipulationGeneralResult result = null;

            var dataMigration = Get(id);
            var dataManipultaionClass = GetDataManipulationClass(dataMigration.Data);

            dataManipultaionClass.DataContext = DataContext;
            dataManipultaionClass.UserManager = _userManager;
            dataManipultaionClass.From = from;
            dataManipultaionClass.EndPoint = endPoint;

            var runDateTime = DateTime.UtcNow;
            result = dataManipultaionClass.Operate();

            SaveResult(id, runDateTime, result.Log);

            result.Log = null;

            return result;
        }

        private void SaveResult(int id, DateTime runDateTime, DataManipulationLog log)
        {
            try
            {
                var dataMigration = Get(id);

                dataMigration.RunDate = runDateTime;

                var currentJsonLog = new DataManipulationLogResult();

                var currentByteLog = dataMigration.Result;

                if (currentByteLog != null)
                {
                    var currentStringLog = ConvertByteToString(currentByteLog);
                    currentJsonLog = JsonConvert.DeserializeObject<DataManipulationLogResult>(currentStringLog);
                }

                currentJsonLog.Items.Add(log);

                var jsonLog = JsonConvert.SerializeObject(currentJsonLog);

                var byteLog = ConvertStringToByte(jsonLog);

                dataMigration.Result = byteLog;

                DataContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var message = string.Empty;

                foreach (var eve in e.EntityValidationErrors)
                {
                    message = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                             ve.PropertyName, ve.ErrorMessage);
                    }
                }

                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(message));
            }
            catch
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception("Error in saving data manipulation result."));
            }
        }

        public DataManipulationLogResult RunReport(int id, string token = null)
        {
            var result = new DataManipulationLogResult();

            var dataMigration = Get(id);

            var dataMigrationResult = dataMigration.Result;
            var jsonDataMigration = ConvertByteToString(dataMigrationResult);

            result = JsonConvert.DeserializeObject<DataManipulationLogResult>(jsonDataMigration);

            if (!string.IsNullOrEmpty(token))
            {
                result.Items.RemoveAll(i => i.Id != token);
            }

            return result;
        }

        private byte[] ConvertStringToByte(string text)
        {
            var result = Encoding.UTF8.GetBytes(text);

            return result;
        }

        private string ConvertByteToString(byte[] file)
        {
            string result = System.Text.Encoding.UTF8.GetString(file);

            return result;
        }

        public List<IDataManipulation> GetDataManipulationClasses()
        {
            var result = new List<IDataManipulation>();

            var allTypes = Assembly.GetAssembly(typeof(IDataManipulation)).GetTypes()
                .Where(t => t.IsClass &&
                t.Namespace == "Jumbula.Web.Infrastructure.DataMigration.DataManipulations" &&
                !t.AssemblyQualifiedName.Contains("c__DisplayClass")
                && !t.AssemblyQualifiedName.Contains("<>c")).ToList();

            foreach (var item in allTypes)
            {
                result.Add((IDataManipulation)Activator.CreateInstance(item));
            }

            return result.ToList();
        }

        public IDataManipulation GetDataManipulationClass(string name)
        {
            var result = GetDataManipulationClasses().Single(m => m.Name == name);

            return result;
        }

        public int GetIdOfLastItem(string name)
        {
            var dataMigration = DataContext.Set<Core.Domain.DataMigration>().OrderByDescending(o => o.Id).FirstOrDefault(d => !d.RunDate.HasValue && d.Data.Equals(name, StringComparison.OrdinalIgnoreCase));

            if (dataMigration == null)
            {
                dataMigration = new Core.Domain.DataMigration()
                {
                    CreatedDate = DateTime.UtcNow,
                    Data = name,
                };

                var res = Create(dataMigration);
            }

            var result = dataMigration.Id;

            return result;
        }





        public string GetWrongMedicalElements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, "Number reg forms ", allRasClubRegForms.Count().ToString());

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var medicalElement = GetFormElement(form, "MedicalConditions", "HealthSection");

                if (medicalElement != null && medicalElement.Title.ToLower().Contains("allerg"))
                {
                    SetReportValue(table, $"{form.Id}", $"MedicalConditions -> {medicalElement.Title}");
                }

                var alergyElement = GetFormElement(form, "AllergiesInfo", "HealthSection");

                if (alergyElement != null && alergyElement.Title.ToLower().Contains("medi"))
                {
                    SetReportValue(table, $"{form.Id}", $"AllergiesInfo -> {alergyElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongMedicalNutElements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();


            var table = new TagBuilder("table");

            SetReportValue(table, "Number reg forms ", allRasClubRegForms.Count().ToString());

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var administerMedicationDescriptionmElement = GetFormElement(form, "SelfAdministerMedicationDescription", "HealthSection");

                if (administerMedicationDescriptionmElement != null && !administerMedicationDescriptionmElement.Title.ToLower().Equals("self-administered medication description".ToLower()))
                {
                    SetReportValue(table, $"{form.Id}", $"{administerMedicationDescriptionmElement.Title}");
                }

            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongSchoolElements(int from = 1, int to = int.MaxValue)
        {
            string result = null;

            //var emPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "em").Select(p => p.Id);
            //var emClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && emPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
            var allEmClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school").OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            var allEmClubRegForms = DataContext.Set<JbForm>().Where(f => allEmClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, "Number of reg forms ", allEmClubRegForms.Count().ToString());

            var allItems = new List<string>();

            foreach (var form in allEmClubRegForms)
            {
                var teacherDropDownElement = GetFormElement(form, "TeacherName", "SchoolSection");

                if (teacherDropDownElement != null && !teacherDropDownElement.Title.Equals("Teacher name"))
                {
                    SetReportValue(table, $"{form.Id}", $"Teacher name was changed to {teacherDropDownElement.Title}");
                }

                var teacherNotListedElement = GetFormElement(form, "TeacherNotListed", "SchoolSection");

                if (teacherNotListedElement != null && !teacherNotListedElement.Title.Equals("Teacher not listed - write name here"))
                {
                    SetReportValue(table, $"{form.Id}", $"Teacher not listed was changed to {teacherNotListedElement.Title}");
                }

                var standardDismissalElement = GetFormElement(form, "StandardDismissal", "SchoolSection");

                if (standardDismissalElement != null && !standardDismissalElement.Title.Equals("Standard dismissal"))
                {
                    SetReportValue(table, $"{form.Id}", $"Standard dismissal was changed to {standardDismissalElement.Title}");
                }

            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }


        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }


        public string GetWrongParent1Elements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var firstNameElement = GetFormElement(form, "FirstName", SectionsName.ParentGuardianSection.ToString());

                if (firstNameElement != null && firstNameElement.Title.ToLower() != "first name")
                {
                    SetReportValue(table, $"{form.Id} firstname ->", $" {firstNameElement.Title}");
                }

                var lastNameElement = GetFormElement(form, "LastName", SectionsName.ParentGuardianSection.ToString());

                if (lastNameElement != null && lastNameElement.Title.ToLower() != "last name")
                {
                    SetReportValue(table, $"{form.Id} last name ->", $"{lastNameElement.Title}");
                }


                var emailElement = GetFormElement(form, "Email", SectionsName.ParentGuardianSection.ToString());

                if (emailElement != null && emailElement.Title.ToLower() != "email address" && emailElement.Title.ToLower() != "email")
                {
                    SetReportValue(table, $"{form.Id} email ->", $"{emailElement.Title}");
                }

                var gendarElement = GetFormElement(form, "Gendar", SectionsName.ParentGuardianSection.ToString());

                if (gendarElement != null && gendarElement.Title.ToLower() != "gendar")
                {
                    SetReportValue(table, $"{form.Id} gender ->", $"{gendarElement.Title}");
                }

                var dateOfBirthElement = GetFormElement(form, "DateOfBirth", SectionsName.ParentGuardianSection.ToString());

                if (dateOfBirthElement != null && dateOfBirthElement.Title.ToLower() != "date of birth")
                {
                    SetReportValue(table, $"{form.Id} date of birth ->", $"{dateOfBirthElement.Title}");
                }


                var phoneElement = GetFormElement(form, "Phone", SectionsName.ParentGuardianSection.ToString());

                if (phoneElement != null && phoneElement.Title.ToLower() != "cell phone" && phoneElement.Title.ToLower() != "primery phone")
                {
                    SetReportValue(table, $"{form.Id} phone ->", $"{phoneElement.Title}");
                }

                var occupationElement = GetFormElement(form, "Occupation", SectionsName.ParentGuardianSection.ToString());

                if (occupationElement != null && occupationElement.Title.ToLower() != "occupation")
                {
                    SetReportValue(table, $"{form.Id} occupation ->", $"{occupationElement.Title}");
                }

                var employerElement = GetFormElement(form, "Employer", SectionsName.ParentGuardianSection.ToString());

                if (employerElement != null && employerElement.Title.ToLower() != "employer")
                {
                    SetReportValue(table, $"{form.Id} Employer ->", $"{employerElement.Title}");
                }


                var alternatePhoneElement = GetFormElement(form, "AlternatePhone", SectionsName.ParentGuardianSection.ToString());

                if (alternatePhoneElement != null && alternatePhoneElement.Title.ToLower() != "alternate phone")
                {
                    SetReportValue(table, $"{form.Id} alternate phone ->", $"{alternatePhoneElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongParent2Elements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var firstNameElement = GetFormElement(form, "FirstName", SectionsName.Parent2GuardianSection.ToString());

                if (firstNameElement != null && firstNameElement.Title.ToLower() != "first name")
                {
                    SetReportValue(table, $"{form.Id} firstname ->", $" {firstNameElement.Title}");
                }

                var lastNameElement = GetFormElement(form, "LastName", SectionsName.Parent2GuardianSection.ToString());

                if (lastNameElement != null && lastNameElement.Title.ToLower() != "last name")
                {
                    SetReportValue(table, $"{form.Id} last name ->", $"{lastNameElement.Title}");
                }


                var emailElement = GetFormElement(form, "Email", SectionsName.Parent2GuardianSection.ToString());

                if (emailElement != null && emailElement.Title.ToLower() != "email address" && emailElement.Title.ToLower() != "email")
                {
                    SetReportValue(table, $"{form.Id} email ->", $"{emailElement.Title}");
                }

                var gendarElement = GetFormElement(form, "Gendar", SectionsName.Parent2GuardianSection.ToString());

                if (gendarElement != null && gendarElement.Title.ToLower() != "gendar")
                {
                    SetReportValue(table, $"{form.Id} gender ->", $"{gendarElement.Title}");
                }

                var dateOfBirthElement = GetFormElement(form, "DateOfBirth", SectionsName.Parent2GuardianSection.ToString());

                if (dateOfBirthElement != null && dateOfBirthElement.Title.ToLower() != "date of birth")
                {
                    SetReportValue(table, $"{form.Id} date of birth ->", $"{dateOfBirthElement.Title}");
                }


                var phoneElement = GetFormElement(form, "Phone", SectionsName.Parent2GuardianSection.ToString());

                if (phoneElement != null && phoneElement.Title.ToLower() != "cell phone" && phoneElement.Title.ToLower() != "primery phone")
                {
                    SetReportValue(table, $"{form.Id} phone ->", $"{phoneElement.Title}");
                }

                var occupationElement = GetFormElement(form, "Occupation", SectionsName.Parent2GuardianSection.ToString());

                if (occupationElement != null && occupationElement.Title.ToLower() != "occupation")
                {
                    SetReportValue(table, $"{form.Id} occupation ->", $"{occupationElement.Title}");
                }

                var employerElement = GetFormElement(form, "Employer", SectionsName.Parent2GuardianSection.ToString());

                if (employerElement != null && employerElement.Title.ToLower() != "employer")
                {
                    SetReportValue(table, $"{form.Id} Employer ->", $"{employerElement.Title}");
                }


                var alternatePhoneElement = GetFormElement(form, "AlternatePhone", SectionsName.Parent2GuardianSection.ToString());

                if (alternatePhoneElement != null && alternatePhoneElement.Title.ToLower() != "alternate phone")
                {
                    SetReportValue(table, $"{form.Id} alternate phone ->", $"{alternatePhoneElement.Title}");
                }

            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }


        public string GetWrongEmergencyElements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var firstNameElement = GetFormElement(form, "FirstName", SectionsName.EmergencyContactSection.ToString());

                if (firstNameElement != null && firstNameElement.Title.ToLower() != "first name")
                {
                    SetReportValue(table, $"{form.Id}  firstname ->", $"{firstNameElement.Title}");
                }

                var lastNameElement = GetFormElement(form, "LastName", SectionsName.EmergencyContactSection.ToString());

                if (lastNameElement != null && lastNameElement.Title.ToLower() != "last name")
                {
                    SetReportValue(table, $"{form.Id} last name ->", $"{lastNameElement.Title}");
                }


                var emailElement = GetFormElement(form, "Email", SectionsName.EmergencyContactSection.ToString());

                if (emailElement != null && emailElement.Title.ToLower() != "email address" && emailElement.Title.ToLower() != "email")
                {
                    SetReportValue(table, $"{form.Id} email ->", $"{emailElement.Title}");
                }


                var phoneElement = GetFormElement(form, "Phone", SectionsName.EmergencyContactSection.ToString());

                if (phoneElement != null && phoneElement.Title.ToLower() != "primery phone")
                {
                    SetReportValue(table, $"{form.Id} phone ->", $"{phoneElement.Title}");
                }

                var relationshipElement = GetFormElement(form, "Ralationship", SectionsName.EmergencyContactSection.ToString());

                if (relationshipElement != null && relationshipElement.Title.ToLower() != "Ralationship")
                {
                    SetReportValue(table, $"{form.Id} Ralationship ->", $"{relationshipElement.Title}");
                }

                var alternatePhoneElement = GetFormElement(form, "AlternatePhone", SectionsName.EmergencyContactSection.ToString());

                if (alternatePhoneElement != null && alternatePhoneElement.Title.ToLower() != "alternate phone")
                {
                    SetReportValue(table, $"{form.Id} alternate phone ->", $"{alternatePhoneElement.Title}");
                }

                var homePhoneElement = GetFormElement(form, "HomePhone", SectionsName.EmergencyContactSection.ToString());

                if (homePhoneElement != null && homePhoneElement.Title.ToLower() != "home phone")
                {
                    SetReportValue(table, $"{form.Id} home phone ->", $"{homePhoneElement.Title}");
                }

                var workPhoneElement = GetFormElement(form, "WorkPhone", SectionsName.EmergencyContactSection.ToString());

                if (workPhoneElement != null && workPhoneElement.Title.ToLower() != "work phone")
                {
                    SetReportValue(table, $"{form.Id} work phone ->", $"{workPhoneElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongAuthorizePickup1Elements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var firstNameElement = GetFormElement(form, "FirstName", SectionsName.AuthorizedPickup1.ToString());

                if (firstNameElement != null && firstNameElement.Title.ToLower() != "first name")
                {
                    SetReportValue(table, $"{form.Id} firstname ->", $"{firstNameElement.Title}");
                }

                var lastNameElement = GetFormElement(form, "LastName", SectionsName.AuthorizedPickup1.ToString());

                if (lastNameElement != null && lastNameElement.Title.ToLower() != "last name")
                {
                    SetReportValue(table, $"{form.Id} last name ->", $"{lastNameElement.Title}");
                }

                var phoneElement = GetFormElement(form, "Phone", SectionsName.AuthorizedPickup1.ToString());

                if (phoneElement != null && phoneElement.Title.ToLower() != "primery phone")
                {
                    SetReportValue(table, $"{form.Id} phone ->", $"{phoneElement.Title}");
                }

                var relationshipElement = GetFormElement(form, "Ralationship", SectionsName.AuthorizedPickup1.ToString());

                if (relationshipElement != null && relationshipElement.Title.ToLower() != "Ralationship")
                {
                    SetReportValue(table, $"{form.Id} Ralationship ->", $"{relationshipElement.Title}");
                }

                var alternatePhoneElement = GetFormElement(form, "AlternatePhone", SectionsName.AuthorizedPickup1.ToString());

                if (alternatePhoneElement != null && alternatePhoneElement.Title.ToLower() != "alternate phone")
                {
                    SetReportValue(table, $"{form.Id} alternate phone ->", $"{alternatePhoneElement.Title}");
                }

                var isEmergencyContactElement = GetFormElement(form, "IsEmergencyContact", SectionsName.AuthorizedPickup1.ToString());

                if (isEmergencyContactElement != null && isEmergencyContactElement.Title.ToLower() != "this person is also an emergency contact")
                {
                    SetReportValue(table, $"{form.Id} is emergency contact ->", $"{isEmergencyContactElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongAuthorizePickup2Elements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var firstNameElement = GetFormElement(form, "FirstName", SectionsName.AuthorizedPickup2.ToString());

                if (firstNameElement != null && firstNameElement.Title.ToLower() != "first name")
                {
                    SetReportValue(table, $"{form.Id} firstname ->", $"{firstNameElement.Title}");
                }

                var lastNameElement = GetFormElement(form, "LastName", SectionsName.AuthorizedPickup2.ToString());

                if (lastNameElement != null && lastNameElement.Title.ToLower() != "last name")
                {
                    SetReportValue(table, $"{form.Id} last name ->", $"{lastNameElement.Title}");
                }

                var phoneElement = GetFormElement(form, "Phone", SectionsName.AuthorizedPickup2.ToString());

                if (phoneElement != null && phoneElement.Title.ToLower() != "primery phone")
                {
                    SetReportValue(table, $"{form.Id} phone ->", $"{phoneElement.Title}");
                }

                var relationshipElement = GetFormElement(form, "Ralationship", SectionsName.AuthorizedPickup2.ToString());

                if (relationshipElement != null && relationshipElement.Title.ToLower() != "Ralationship")
                {
                    SetReportValue(table, $"{form.Id} Ralationship ->", $"{relationshipElement.Title}");
                }

                var alternatePhoneElement = GetFormElement(form, "AlternatePhone", SectionsName.AuthorizedPickup2.ToString());

                if (alternatePhoneElement != null && alternatePhoneElement.Title.ToLower() != "alternate phone")
                {
                    SetReportValue(table, $"{form.Id} alternate phone ->", $"{alternatePhoneElement.Title}");
                }

                var isEmergencyContactElement = GetFormElement(form, "IsEmergencyContact", SectionsName.AuthorizedPickup2.ToString());

                if (isEmergencyContactElement != null && isEmergencyContactElement.Title.ToLower() != "this person is also an emergency contact")
                {
                    SetReportValue(table, $"{form.Id} is emergency contact ->", $"{isEmergencyContactElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongAuthorizePickup3Elements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var firstNameElement = GetFormElement(form, "FirstName", SectionsName.AuthorizedPickup3.ToString());

                if (firstNameElement != null && firstNameElement.Title.ToLower() != "first name")
                {
                    SetReportValue(table, $"{form.Id} firstname ->", $"{firstNameElement.Title}");
                }

                var lastNameElement = GetFormElement(form, "LastName", SectionsName.AuthorizedPickup3.ToString());

                if (lastNameElement != null && lastNameElement.Title.ToLower() != "last name")
                {
                    SetReportValue(table, $"{form.Id} last name ->", $"{lastNameElement.Title}");
                }

                var phoneElement = GetFormElement(form, "Phone", SectionsName.AuthorizedPickup3.ToString());

                if (phoneElement != null && phoneElement.Title.ToLower() != "primery phone")
                {
                    SetReportValue(table, $"{form.Id} phone ->", $"{phoneElement.Title}");
                }

                var relationshipElement = GetFormElement(form, "Ralationship", SectionsName.AuthorizedPickup3.ToString());

                if (relationshipElement != null && relationshipElement.Title.ToLower() != "Ralationship")
                {
                    SetReportValue(table, $"{form.Id} Ralationship ->", $"{relationshipElement.Title}");
                }

                var alternatePhoneElement = GetFormElement(form, "AlternatePhone", SectionsName.AuthorizedPickup3.ToString());

                if (alternatePhoneElement != null && alternatePhoneElement.Title.ToLower() != "alternate phone")
                {
                    SetReportValue(table, $"{form.Id} alternate phone ->", $"{alternatePhoneElement.Title}");
                }

                var isEmergencyContactElement = GetFormElement(form, "IsEmergencyContact", SectionsName.AuthorizedPickup3.ToString());

                if (isEmergencyContactElement != null && isEmergencyContactElement.Title.ToLower() != "this person is also an emergency contact")
                {
                    SetReportValue(table, $"{form.Id} is emergency contact ->", $"{isEmergencyContactElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongAuthorizePickup4Elements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var firstNameElement = GetFormElement(form, "FirstName", SectionsName.AuthorizedPickup4.ToString());

                if (firstNameElement != null && firstNameElement.Title.ToLower() != "first name")
                {
                    SetReportValue(table, $"{form.Id} firstname ->", $"{firstNameElement.Title}");
                }

                var lastNameElement = GetFormElement(form, "LastName", SectionsName.AuthorizedPickup4.ToString());

                if (lastNameElement != null && lastNameElement.Title.ToLower() != "last name")
                {
                    SetReportValue(table, $"{form.Id} last name ->", $"{lastNameElement.Title}");
                }

                var phoneElement = GetFormElement(form, "Phone", SectionsName.AuthorizedPickup4.ToString());

                if (phoneElement != null && phoneElement.Title.ToLower() != "primery phone")
                {
                    SetReportValue(table, $"{form.Id} phone ->", $"{phoneElement.Title}");
                }

                var relationshipElement = GetFormElement(form, "Ralationship", SectionsName.AuthorizedPickup4.ToString());

                if (relationshipElement != null && relationshipElement.Title.ToLower() != "Ralationship")
                {
                    SetReportValue(table, $"{form.Id} Ralationship ->", $"{relationshipElement.Title}");
                }

                var alternatePhoneElement = GetFormElement(form, "AlternatePhone", SectionsName.AuthorizedPickup4.ToString());

                if (alternatePhoneElement != null && alternatePhoneElement.Title.ToLower() != "alternate phone")
                {
                    SetReportValue(table, $"{form.Id} alternate phone ->", $"{alternatePhoneElement.Title}");
                }

                var isEmergencyContactElement = GetFormElement(form, "IsEmergencyContact", SectionsName.AuthorizedPickup4.ToString());

                if (isEmergencyContactElement != null && isEmergencyContactElement.Title.ToLower() != "this person is also an emergency contact")
                {
                    SetReportValue(table, $"{form.Id} is emergency contact ->", $"{isEmergencyContactElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongMedical2Elements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var nutAllergyElement = GetFormElement(form, "NutAllergy", SectionsName.HealthSection.ToString());

                if (nutAllergyElement != null && nutAllergyElement.Title.ToLower() != "nut allergy")
                {
                    SetReportValue(table, $"{form.Id} nut allergy ->", $"{nutAllergyElement.Title}");
                }

                var nutAllergyDescriptionElement = GetFormElement(form, "NutAllergyDescription", SectionsName.HealthSection.ToString());

                if (nutAllergyDescriptionElement != null && nutAllergyDescriptionElement.Title.ToLower() != "nut allergy description")
                {
                    SetReportValue(table, $"{form.Id} nut allergy description ->", $"{nutAllergyDescriptionElement.Title}");
                }

                var doctorNameElement = GetFormElement(form, "DoctorName", SectionsName.HealthSection.ToString());

                if (doctorNameElement != null && doctorNameElement.Title.ToLower() != "doctor name")
                {
                    SetReportValue(table, $"{form.Id} doctor name ->", $"{doctorNameElement.Title}");
                }

                var doctorPhoneElement = GetFormElement(form, "DoctorPhone", SectionsName.HealthSection.ToString());

                if (doctorPhoneElement != null && doctorPhoneElement.Title.ToLower() != "doctor phone")
                {
                    SetReportValue(table, $"{form.Id} doctor phone ->", $"{doctorPhoneElement.Title}");
                }

                var doctorLocationElement = GetFormElement(form, "DoctorLocation", SectionsName.HealthSection.ToString());

                if (doctorLocationElement != null && doctorLocationElement.Title.ToLower() != "doctor location")
                {
                    SetReportValue(table, $"{form.Id} doctor location ->", $"{doctorLocationElement.Title}");
                }

            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongInsuranceElements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var companyNameElement = GetFormElement(form, "CompanyName", SectionsName.InsuranceSection.ToString());

                if (companyNameElement != null && companyNameElement.Title.ToLower() != "company name")
                {
                    SetReportValue(table, $"{form.Id} company name ->", $"{companyNameElement.Title}");
                }

                var policyNumberElement = GetFormElement(form, "PolicyNumber", SectionsName.InsuranceSection.ToString());

                if (policyNumberElement != null && policyNumberElement.Title.ToLower() != "policy number")
                {
                    SetReportValue(table, $"{form.Id} policy number ->", $"{policyNumberElement.Title}");
                }

                var companyPhoneElement = GetFormElement(form, "CompanyPhone", SectionsName.InsuranceSection.ToString());

                if (companyPhoneElement != null && companyPhoneElement.Title.ToLower() != "company phone")
                {
                    SetReportValue(table, $"{form.Id} company phone ->", $"{companyPhoneElement.Title}");
                }

            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongPermissionPhotographyElements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var permissionPhotographyElement = GetFormElement(form, "PermissionPhotography", SectionsName.PhotographySection.ToString());

                if (permissionPhotographyElement != null && permissionPhotographyElement.Title.ToLower() != "I give permission for my child to be photographed and videotaped during program activities")
                {
                    SetReportValue(table, $"{form.Id} permission photography ->", $"{permissionPhotographyElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetWrongSchool2Elements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var teacherNameElement = GetFormElement(form, "TeacherName", SectionsName.SchoolSection.ToString());

                if (teacherNameElement != null && teacherNameElement.Title.ToLower() != "teacher name")
                {
                    SetReportValue(table, $"{form.Id} teacher name ->", $"{teacherNameElement.Title}");
                }

                var teacherNotListedElement = GetFormElement(form, "TeacheNotListed", SectionsName.SchoolSection.ToString());

                if (teacherNotListedElement != null && teacherNotListedElement.Title.ToLower() != "teacher not listed - write name here")
                {
                    SetReportValue(table, $"{form.Id} teacher not listed ->", $"{teacherNotListedElement.Title}");
                }

                var dismissalFromEnrichmentElement = GetFormElement(form, "DismissalFromEnrichment", SectionsName.SchoolSection.ToString());

                if (dismissalFromEnrichmentElement != null && dismissalFromEnrichmentElement.Title.ToLower() != "dismissal from enrichment")
                {
                    SetReportValue(table, $"{form.Id} dismissal from enrichment ->", $"{dismissalFromEnrichmentElement.Title}");
                }


                var schoolNameElement = GetFormElement(form, "SchoolName", SectionsName.SchoolSection.ToString());

                if (schoolNameElement != null && schoolNameElement.Title.ToLower() != "school name")
                {
                    SetReportValue(table, $"{form.Id} school name ->", $"{schoolNameElement.Title}");
                }

                var homeroomElement = GetFormElement(form, "Homeroom", SectionsName.SchoolSection.ToString());

                if (homeroomElement != null && homeroomElement.Title.ToLower() != "homeroom")
                {
                    SetReportValue(table, $"{form.Id} homeroom ->", $"{homeroomElement.Title}");
                }

                var nameElement = GetFormElement(form, "Name", SectionsName.SchoolSection.ToString());

                if (nameElement != null && nameElement.Title.ToLower() != "name")
                {
                    SetReportValue(table, $"{form.Id} name ->", $"{nameElement.Title}");
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        public string GetAllEmFormElements()
        {
            string result = null;

            var emPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "em").Select(p => p.Id);


            var allEmClubRegForms = DataContext.Set<ClubFormTemplate>().Where(i => i.FormType==FormType.Registration && i.Title != "Default" && i.Title != "Default school" && (i.Club.PartnerId.HasValue && emPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).ToList();

            var table = new TagBuilder("table");

            SetReportValue(table, $"Number reg forms", $"{allEmClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var clubForm in allEmClubRegForms)
            {
                var form = clubForm.JbForm;

                foreach (var item in form.Elements)
                {
                    var section = item as JbSection;

                    foreach (var element in section.Elements)
                    {
                        var defaultElementNames = Common.Helper.EnumHelper.GetEnumList<ElementsName>().Select(d => d.Value);

                        var isDefault = defaultElementNames.Contains(element.Name);

                        SetReportValue(table,"EM", $"{clubForm.Club.Name} ## {clubForm.Title} ## {section.Title} ## {element.Title} ## {(isDefault? "Default" : "Custom")}");
                    }
                }
            }

            var html = new TagBuilder("html");
            var head = new TagBuilder("head");
            var body = new TagBuilder("body");

            body.InnerHtml += table.ToString();
            html.InnerHtml += head.ToString() + body.ToString();
            result = html.ToString();

            return result;
        }

        private void SetReportValue(TagBuilder table, string key, string value)
        {
            var tr = new TagBuilder("tr");
            var td1 = new TagBuilder("td");
            var td2 = new TagBuilder("td");

            td1.SetInnerText(key);
            td2.SetInnerText(value);

            tr.InnerHtml += td1.ToString();
            tr.InnerHtml += td2.ToString();

            table.InnerHtml += tr.ToString();
        }
    }
}