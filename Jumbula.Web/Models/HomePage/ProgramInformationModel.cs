﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Domain.Generic;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models
{
    public class ProgramInformationModel : BaseViewModel<Program, long>
    {
        public ProgramInformationModel()
        {
            ImageUrls = new List<ImageUrls>();
        }
        public string ClubDomain { get; set; }

        public string ClubName{ get; set; }
        public string SeasonDomain { get; set; }

        public string Domain { get; set; }
        public string WeeklyEmail { get; set; }
        public string MaterialsNeeded { get; set; }
        public string Name { get; set; }
        public string MinEnrollment { get; set; }
        public string MaxEnrollment { get; set; }
        public string Description { get; set; }
        public string RoomAssignment { get; set; }
        public DateTime? StartDate { get; set; }
        public string StrStartDate { get; set; }
        public string StrEndDate { get; set; }
        public List<ImageUrls> ImageUrls { get; set; }
        public List<ClassSessionsDateTime> ClassDates { get; set; }
        public bool HasProvider { get; set; }
        public string ProviderName { get; set; }
        public string ProviderContact { get; set; }
        public string ProviderPhone { get; set; }
        public string ProviderEmail { get; set; }
        public bool HasPartner { get; set; }
        public string strClassDates
        {
            get

            {
                return GenerateClassDateStrings();
            }

        }
        public List<ClassSessionsDateTime> ClassDatesTimeChanges
        {
            get

            {
                return ClassDates.Where(c=>c.TimeChanges).ToList();
            }

        }
        public ProgramRestrictionMessag RestrictionMessages { get; set; }
        public DateTime? EndDate { get; set; }

        public PostalAddress Address { get; set; }

        public string LocationName { get; set; }

        public string ClubLogo { get; set; }

        public Jumbula.Common.Enums.TimeZone  ClubTimeZone { get; set; }
        public CurrencyCodes Currency { get; set; }
        public ProgramBodySchedule Schedule { get; set; }
        public string Testimonials { get; set; }
        public bool HasTestimonials { get; set; }

        public List<ProgramDays> ProgramDays { get; set; }
        public List<CategoryViewModel> Categories { get; set; }
        public string ClubEmail { get; set; }
        public string ClubPhone { get; set; }
        public string ClubPhoneExtension { get; set; }
        public string Question { get; set; }
        public bool HidePhoneNumber { get; set; }
        public ClubAppearanceSetting AppearanceSetting { get; set; }

        public string RegistrationPriodStartDate { get; set; }
        public string RegistrationPriodEndDate { get; set; }
        private string GenerateClassDateStrings()
        {
            if (ClassDates != null && ClassDates.Any())
            {
                var fisrtItem = true;
                var result = string.Empty;
                foreach (var item in ClassDates)
                {
                    if (!fisrtItem)
                    {
                        result += ", ";
                    }
                    result += item.SessionDate.ToString(Constants.DefaultDateFormat);
                    if (item.TimeChanges)
                    {
                        result += "*";
                    }
                    fisrtItem = false;
                }
                return result;
            }
            return string.Empty;
        }
    }
    public class ProgramRestrictionMessag
    {
        public string AgeMessage { get; set; }
        public string GradeMessage { get; set; }
        public string GenderMessage { get; set; }

    }
    public class ProgramDays
    {
        public DayOfWeek DayOfWeek { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }
        public string StrTimes { get; set; }
    }

    public class ImageUrls
    {
        public string SmallUrl { get; set; }
        public string LargeUrl { get; set; }
    }
}