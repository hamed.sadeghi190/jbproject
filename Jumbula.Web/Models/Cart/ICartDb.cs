﻿using System.Web;
using SportsClub.Domain;

namespace SportsClub.Models.Cart
{
    public interface ICartDb
    {
        Cart Get(HttpContextBase context);
        Cart GetEditCart(HttpContextBase context, bool InitialIfNull);
       // void Add(HttpContextBase context, CartOrderViewModel item, bool collapse);
        //void AddEditItem(HttpContextBase context, CartOrderViewModel order, bool collapase);
        void Remove(HttpContextBase context, string orderID);
        void Empty(HttpContextBase context);
        string CurrentDomain(HttpContext context);

        string SibilingForUser(HttpContextBase httpContextBase, string domain, string subDomain);
    }
}

