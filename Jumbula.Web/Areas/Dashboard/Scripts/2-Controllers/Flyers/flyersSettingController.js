﻿// start of file
(function () {
    'use strict';
    angular.module('dashboardApp').controller('flyersSettingController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {
        window.app.globalObjects.pageClass = 'campaign-templates';
        window.app.globalObjects.initializeView();

        var baseUrl = window.location.origin + '/Dashboard/Flyer';



        $http.get('Dashboard/Flyer/GetFlyersSetting').success(function (data, status, headers, config) {
            $scope.Model = data;


        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.save = function () {
            if (checkValidation($scope.Model.Email, $scope.Model.Phone)) {
                $http.post('Dashboard/Flyer/SaveFlyersSetting', { model: $scope.Model })
                 .success(function (data, status, headers, config) {

                     if (data.Status) {

                         $scope.errors = null;

                         jbToast.success('Setting updated successfully.');
                     }
                     else {
                         $scope.MC.handleErrors($scope, data);
                     }
                 })
            }
        }

        var checkValidation = function (email, phone) {

            if (email == null || email == "") {
                jbToast.error("Please enter email.");
                return false;
            }
            else if (phone == null || phone == "") {
                jbToast.error("Please enter phone.");
                return false;
            }
            else if (!validateEmail(email)) {
                jbToast.error("Email is not valid.");
                return false;
            } else {
                return true;
            }
        }

        function validateEmail(email) {
            var re = /\S+@\S+\.\S+/;
            return re.test(email);
        }

    }]);
})();


