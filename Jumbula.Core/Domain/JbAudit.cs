﻿using Jumbula.Common.Base;
using System;

namespace Jumbula.Core.Domain
{
    public class JbAudit : BaseEntity<Guid>
    {
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public override Guid Id { get; set; }

        public string UserName { get; set; }

        public string ClubDomain { get; set; }

        public string IpAddress { get; set; }

        public string Url { get; set; }

        public string Browser { get; set; }

        public DateTime DateTime { get; set; }

        public string Action { get; set; }

        public string Description { get; set; }

        public string Device { get; set; }

        public int? StatusCode { get; set; }

        public bool Status { get; set; }

        public string Zone { get; set; }

        public string OwnerKey { get; set; }
    }


}
