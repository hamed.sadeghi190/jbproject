﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Jumbula.Core.Identity
{
    public interface IApplicationRoleManager<TRole>
    {

        Task<IdentityResult> CreateAsync(TRole role);

        Task<IdentityResult> DeleteAsync(TRole role);

        Task<TRole> FindByNameAsync(string roleName);
        
        Task<IdentityResult> UpdateAsync(TRole role);
    }
}
