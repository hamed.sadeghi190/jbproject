﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('allergyMedicalConditionReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();
            $scope.MC.collapseAllBlades();

            var baseUrl = window.location.origin + '/Dashboard/CustomReport';

            var timer = false;

            $scope.Model = {};
            $scope.rowNumber = 1;

            $scope.stateParams = $stateParams;
            $scope.MC.stateParams = $stateParams;

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };
            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get(baseUrl + '/GetAllProgramsSignOutSheetReport', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllPrograms = data;
                $scope.AllPrograms.unshift({ Text: "All", Value: null });
                $scope.Model.ScheduleId = null;

                $scope.runReport();

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });



            $scope.runReport = function () {

                $http.get(baseUrl + '/GetAllergyMedicalConditionReport', { params: { seasonDomain: $scope.stateParams.seasonDomain, scheduleId: $scope.Model.ScheduleId } }).success(function (data, status, headers, config) {
                    $scope.ModelSections = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error("error " + data);
                });

            }


            $scope.saveToPdf = function () {

                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = baseUrl + '/GenerateAllergyMedicalConditionReportPdf?seasonDomain=' + $scope.stateParams.seasonDomain + '&scheduleId=' + $scope.Model.ScheduleId;
                a.download = "AllergyMedicalConditionReport.Pdf";
                a.click();
            };
        }
    ]);


})();