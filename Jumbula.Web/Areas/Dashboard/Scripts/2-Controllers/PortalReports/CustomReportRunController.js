﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CustomReportRunController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', 'jbToast', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, jbToast, $timeout) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();


        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.IsReportRun = false;
        $scope.Model = [];
        $scope.Model.HasHeader = false;
        $scope.Model.SingleProgramMode = false;

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.ReportId = $scope.MC.stateParams.reportId;
        $scope.Model.ReportName = $scope.MC.stateParams.reportName;
        $scope.Model.reportType = $scope.MC.stateParams.reportType;

        ////////////////
        $scope.Model.Filters = {};
        $scope.Model.Year = 0;
        $scope.Model._SelectedClubs = [];
        $scope.ClubTypes = [{ Value: "school", Title: "Schools" }, { Value: "provider", Title: "Providers" }, { Value: "district", Title: "School districts" }];

        $scope.Model.IsAllPrograms = "true";

        $scope.Model.ClubType = "school";
        $scope.Model.IsAllClubs = "true";

        $scope.updateChosenRadio = function () {
            $scope.Model.IsAllClubs = "true";
        }


        $scope.getClubs = function (clubType) {
            if (clubType === "school")
                getAllSchools();
            else if (clubType === "provider")
                getAllProviders();
            else
                getAllDistricts();
        }

        function getAllSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data) {
                    $scope.AllClubs = data.AllSchools;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllProviders() {
            $http.get(baseUrl + "/GetAllProvider")
                .success(function (data) {
                    $scope.AllClubs = data;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch('AllClubs', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        function getAllDistricts() {
            $http.get(baseUrl + '/GetAllPartnerSchoolDistricts')
                .success(function (data) {
                    $scope.AllClubs = data.AllDisctricts;
                    var timer = false;

                    $scope.updateChosenScope = function () {
                        $("select[chosen]").trigger('chosen:updated');
                    };

                    $scope.$watch("AllClubs", function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                });
        }

        $http.get(baseUrl + '/GetFullSeasonClassListReportHeader').success(function (data) {

            $scope.Years = data.Years;
            $scope.Model.AllSeason = data.Seasons;

            //$scope.Model.Year = new Date().getFullYear().toString();
            $scope.Model.Year = data.Years[0].Value;
            $scope.Model.SeasonName = data.Seasons[0].Value;
        }).error(function (data) {
            jbToast.error("error " + data);
            });

        $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model, reportType: $stateParams.reportType })
            .success(function (data, status, headers, config) {
                $scope.Model.Filters.Status = "regular";
                $scope.Model.Filters.Tuition = "";
                $scope.Model.Filters.Tuitions = data.Tuitions;
                $scope.Model.Filters.OrderStatus = data.OrderStatus;
            }).error(function (data, status, headers, config) {
            });

        //////////////////

        $scope.prepareGrid = function () {

            if ($stateParams.reportType == 'Parent') {

                if ($scope.Model.IsAllPrograms == 'false' && $scope.Model._SelectedPrograms != null) {

                    $scope.Model.Filters._SelectedPrograms = [];
                    for (var i = 0; i < $scope.Model._SelectedPrograms.length; i++) {

                        var scheduleIds = $scope.Model._SelectedPrograms[i];
                        if (scheduleIds.length > 0) {
                            for (var j = 0; j < scheduleIds.length; j++) {
                                $scope.Model.Filters._SelectedPrograms.push(scheduleIds[j]);
                            }
                        } else {
                            $scope.Model.Filters._SelectedPrograms.push(scheduleIds);
                        }

                    }
                    $scope.Model._SelectedPrograms = $scope.Model.Filters._SelectedPrograms;
                }
            }

            
            //$http.post(baseUrl + '/GetAllSeasonProgramsCustomReport', { model: $scope.Model })
            //    .success(function (data) {
            //        $scope.Model.Filters.Programs = data.AllPrograms;
            //        var timer = false;

            //        $scope.updateChosenScope = function () {
            //            $("select[chosen]").trigger('chosen:updated');
            //        };

            //        $scope.$watch('Model.Filters.Programs',
            //            function () {
            //                if (timer) {
            //                    $timeout.cancel(timer);
            //                }
            //                timer = $timeout(function () {
            //                    $scope.updateChosenScope();
            //                },
            //                    1500);
            //            });
            //    });



            if (!$scope.IsReportRun) {
                
                $scope.customReportsDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport:
                        {
                            read: {
                                url: baseUrl + '/RunReportCustomReport',
                                type: "POST",
                                data: function () {
                                    return {
                                        model: $scope.Model
                                    };
                                }
                            },
                        },
                    schema: {
                        total: "TotalCount",
                        data: function (response) {

                            $scope.Titles = response.Titles;
                            return response.DataSource;
                        },
                    },
                    serverPaging: true,
                    pageSize: 100,
                    batch: true,
                    requestStart: function () {
                        kendo.ui.progress($("[kendo-grid]"), true);
                    },
                    requestEnd: function () {
                        kendo.ui.progress($("[kendo-grid]"), false);
                    }
                });

                $scope.oldColumns = [];

                $scope.customReportGridOptions = {
                    excel: {
                        allPages: true,
                        fileName: $stateParams.reportName + '.xlsx',
                        proxyURL: baseUrl + '/KendoExport',
                    },
                    excelExport: function (e) {

                        if ($scope.Model.HasHeader) {

                            var rows = e.workbook.sheets[0].rows;

                            if ($scope.Model.IsAllPrograms == 'true' ||
                                ($scope.Model._SelectedPrograms != null &&
                                    $scope.Model._SelectedPrograms != 'undefined' &&
                                    $scope.Model._SelectedPrograms.length > 1)) {

                                rows.unshift({
                                    cells: [{ value: "" }]
                                });

                                rows.unshift({
                                    cells: [{ value: $stateParams.reportName + ", " + $scope.Model.Header.Date }]
                                });
                            }

                        }

                        cfpLoadingBar.complete();
                    },
                    dataSource: $scope.customReportsDataSource,
                    scrollable: true,
                    resizable: true,
                    sortable: true,
                    columnMenu: true,
                    pageable: {
                        refresh: true,
                        pageSizes:
                            [
                                5, 10, 20, 30, 50, 100
                            ],
                        buttonCount: 5
                    },
                    dataBound: function (e) {
                        customReportService.initializeDoubleScroll(e);
                        $scope.oldColumns = $("#customReportGrid").data("kendoGrid").columns;

                        setHeader();

                        //Rename columns when select another program with different columns
                        for (var j = 0; j < e.sender.columns.length; j++) {

                            var column = e.sender.columns[j];

                            var newTitle = $scope.Titles[j].replace(/_/g, " ").trimLeft().replace(/sDash/g, "<br>");

                            $("#customReportGrid thead [data-field=" + column.field + "] .k-link").html(newTitle);
                        }

                        resizeGrid();
                    }
                }

                $scope.IsReportRun = true;

            } else {
                
                var grid = $("#customReportGrid").data("kendoGrid");

                grid.setOptions({
                    columns: []
                });

                $scope.customReportGridOptions.dataSource.page(1);
                grid.dataSource.read();
            }

            var columns = [];
            $scope.oldColumns = [];

            $(document).bind("ajaxComplete.changeColumns",
                function () {
                    if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                        for (var i = 0; i < $scope.oldColumns.length; i++) {
                            var column = [];
                            column = {
                                field: $scope.oldColumns[i].field,
                                title: $scope.Titles[i].replace(/_/g, " ").trimLeft().replace(/sDash/g, "<br>")
                                    .trimLeft()
                            }
                            columns.push(column);
                        }
                        $("#customReportGrid").data("kendoGrid").setOptions({
                            columns: columns
                        });
                    }
                    $(document).unbind("ajaxComplete.changeColumns");
                });

        }


        $scope.filterData = function () {
            $("#customReportGrid").data("kendoGrid").setOptions({
                columns: []
            });
            setHeader();
            $scope.customReportGridOptions.dataSource.page(1);
            $("#customReportGrid").data("kendoGrid").dataSource.read();
           
        }

        $scope.clearFilters = function () {
            //$scope.Model.Filters = {};
            console.log($scope.IsReportRun);
            if (!$scope.IsReportRun) {
                return;
            }
            $scope.Model.Filters.RegistrationStart = "";
            $scope.Model.Filters.RegistrationEnd = "";
            $scope.Model.Filters.Status = "regular";
            $scope.Model.Filters.Tuition = "";
            $scope.Model.Filters._SelectedPrograms = [];
            $scope.Model.ClubType = "school";
            $scope.Model.IsAllClubs = "true";
            $scope.Model._SelectedClubs = [];
            $scope.Model.Year = "-1";
            $scope.Model.SeasonName = "-1";
            $scope.Model.Filters.IsAllPrograms = "true";

            $http.post(baseUrl + '/FillBaseFilters', { model: $scope.Model }).success(
                function (data, status, headers, config) {
                    $scope.Model.Filters.Tuitions = data.Tuitions;
                    $scope.Model.Filters.OrderStatus = data.OrderStatus;
                });

            setHeader();
            $("#customReportGrid").data("kendoGrid").setOptions({
                columns: []
            });
            $scope.customReportGridOptions.dataSource.page(1);
            $("#customReportGrid").data("kendoGrid").dataSource.read();
            resizeGrid();

        }

        $scope.getReportHeader = function (programId) {
            if ($stateParams.reportType == 'Registration') {
                $http.get(baseUrl + '/GetReportHeader',
                    { params: { reportId: $scope.MC.stateParams.reportId, programId: programId } })
                    .success(function (data, status, headers, config) {
                        $scope.Model.Header = data;

                        var hasReportHeader = false;

                        if (data != null && data != 'undefined' && data != '') {
                            hasReportHeader = true;
                        }

                        $scope.Model.HasHeader = hasReportHeader
                    });
            } else {
                $http.get(baseUrl + '/GetCustomReportHeader',
                    { params: { reportId: $scope.MC.stateParams.reportId, scheduleId: programId } })
                    .success(function (data, status, headers, config) {
                        $scope.Model.Header = data;

                        var hasReportHeader = false;

                        if (data != null && data != 'undefined' && data != '') {
                            hasReportHeader = true;
                        }

                        $scope.Model.HasHeader = hasReportHeader;
                    });
            }
        }

        $scope.saveToExcel = function () {

            cfpLoadingBar.start();

            $http.post(baseUrl + '/RunReportCustomReport', {
                model: $scope.Model,
                makeCampaign: false,
                isExportExcel: true
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/excel" });

                    var fileUrl = URL.createObjectURL(blob);
                    a.href = fileUrl;
                    a.download = "custom_report.xlsx";
                    a.click();
                    cfpLoadingBar.complete();

                }).error(function (data, status, headers, config) {
                    cfpLoadingBar.complete();

                });

        }

        $scope.MakeCampaign = function () {
            $http.post(baseUrl + '/RunReportCustomReport',
                { model: $scope.Model, makeCampaign: true, term: $scope.searchTerm })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        //$scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                    } else {
                        jbToast.error(data.Message);
                    }
                });
        }

        function setHeader() {
            if ($scope.Model.IsAllPrograms != 'true' &&
                ($scope.Model._SelectedPrograms != null &&
                    $scope.Model._SelectedPrograms != 'undefined' &&
                    $scope.Model._SelectedPrograms.length == 1)) {
                $scope.Model.SingleProgramMode = true;
                $scope.getReportHeader($scope.Model._SelectedPrograms[0]);
            }

            if ($scope.Model.IsAllPrograms == 'true' ||
                ($scope.Model._SelectedPrograms != null &&
                    $scope.Model._SelectedPrograms != 'undefined' &&
                    $scope.Model._SelectedPrograms.length > 1)) {
                $scope.Model.SingleProgramMode = false;
                $scope.getReportHeader();
            }
        }

        $scope.shareReport = function () {
            $scope.Model.Filters.Term = $scope.searchTerm;
            customReportService.setModelInStorage($scope.Model);
            $state.go('ShareAsAttchment',
                { seasonDomain: $scope.MC.stateParams.seasonDomain, reportName: "CustomParentPortal" });
        }

        function resizeGrid() {
            $("#customReportGrid").data("kendoGrid").resize();
        }
        

    }]);
})();
