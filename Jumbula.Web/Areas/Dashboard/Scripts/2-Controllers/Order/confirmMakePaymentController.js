﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('confirmMakePaymentController', ['$scope', '$http', '$log', '$stateParams', '$state', 'makePaymentService', '$filter', function ($scope, $http, $log, $stateParams, $state, makePaymentService, $filter) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/Order";


        $scope.Model = makePaymentService.get();

        $scope.Model.OrderItems = $scope.OrderitemModel;
 
        $scope.cancelAction = function () {
            makePaymentService.set($scope.Model);
            $state.back();
        }
        $scope.formatDate = function () {
            return new Date($scope.Model.PaymentDate).format('mmm d, yyyy');

        }

        $scope.makePayment = function () {
            $('#submitMakeAPayment').prop("disabled", true);
            $http.post($scope.baseUrl + '/SubmitMakePayment', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                 
                    if (data.Status == true) {
                        $scope.MC.backQueueItems.pop();
                        if (data.result == "Refresh") {
                            $state.go("DisplayPaymentMessage", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId, status: true, message: '' });
                        }
                        if (data.result == "Redirect") {
                            window.onbeforeunload = null;
                            window.location.replace(data.url);
                        }
                    }
                    else {
                        $('#submitMakeAPayment').prop("disabled", true);
                        $scope.MC.handleErrors($scope, data);
                    }
                });

        };

        $scope.getPaymentMethodText = function () {
            var paymentMethodValue = $scope.Model.SelectedPaymentMethod;
            var paymentMethod = $filter('getById')($scope.Model.PaymentMethods, paymentMethodValue);
            return paymentMethod.Text;
        }

    }]).filter('getById', function () {
        return function (input, id) {
            var i = 0, len = input.length;
            for (; i < len; i++) {
                if (+input[i].Value == +id) {
                    return input[i];
                }
            }
            return null;
        }
    });
})();