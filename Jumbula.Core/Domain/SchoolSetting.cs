﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Core.Domain
{

    public class SchoolSetting : ClubSetting
    {
        public SchoolSetting()
        {
            SchoolInfo = new SchoolSettingSchoolInfo();
            ProgramInfo = new SchoolSettingProgramInfo();
            RegistrationInfo = new SchoolSettingRegistrationInfo();
        }

        public SchoolSettingSchoolInfo SchoolInfo { get; set; }
        public SchoolSettingProgramInfo ProgramInfo { get; set; }
        public SchoolSettingRegistrationInfo RegistrationInfo { get; set; }

    }
    public class SchoolSettingSchoolInfo
    {
        public string MinGrade { get; set; }
        public string MaxGrade { get; set; }
        [StringLength(256)]
        public string PTASite { get; set; }
        public string Note { get; set; }
    }
    public class SchoolSettingProgramInfo
    {
        public int SelectedCoordinator { get; set; }
        public Int16? NumberOfSeasons { get; set; }
        public Int16? NumberOfClassesPerSeason { get; set; }

        public Int16? MaxClassesPerDay { get; set; }
        public List<string> DaysClassesMeet { get; set; }

        public bool BeforeSchool { get; set; }
        public string NumberOfStudents { get; set; }
        public string NumberOfRegistrationsPerSeason { get; set; }

        public bool OpenToNonStudents { get; set; }

        public string TranslationNeeded { get; set; }
        public string FolderDay { get; set; }
        public string Note { get; set; }
    }

    public class SchoolSettingOnSiteInfo
    {
        public string OnSiteCancellationContactInfo { get; set; }
        public DateTime? SchoolStartTime { get; set; }
        public DateTime? SchoolDismissalTime { get; set; }

        public DateTime? EnrichmentStartTimeAM { get; set; }
        public DateTime? EnrichmentStartTimePM { get; set; }

        public DateTime? InstructorArrivalTimeAM { get; set; }
        public DateTime? InstructorArrivalTimePM { get; set; }

        public string EarlyReleaseDay { get; set; }
        public DateTime? EarlyReleaseTime { get; set; }

        public List<SpaceRequirement> AvailableSpaces { get; set; }

        public int OnSiteCancellationContactId { get; set; }
        public string ParkingForProviders { get; set; }
        public string TransitionInstructions { get; set; }
        public string AttendanceProcedure { get; set; }
        public List<string> DismissalOption { get; set; }
        public List<string> EnrichmentDismissalOption { get; set; }

        public string CheckInProceduresForInstructors { get; set; }
        public string EnrichmentDismissalInstructions { get; set; }
        public string LatePickUpPolicy { get; set; }
        public string PermitResponsibility { get; set; }
        public string Note { get; set; }
        public string Title { get; set; }
    }
    public class SpaceRequirement
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
    public class SchoolSettingRegistrationInfo
    {
        public string Colors { get; set; }
        public DateTime? RegistrationStartTime { get; set; }
        public DateTime? RegistrationEndTime { get; set; }
        public List<string> LateRegistrationOption { get; set; }
        public decimal PTARegFee { get; set; }

        public string ScholarshipRequirement { get; set; }
        public string ScholarshipCodes { get; set; }
        public string SpecialRosterRequirements { get; set; }
        public List<string> ReconciliationDetail { get; set; }
        public string Note { get; set; }
    }
}
