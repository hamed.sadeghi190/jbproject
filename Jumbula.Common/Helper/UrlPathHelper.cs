﻿using System;
using System.Web;

namespace Jumbula.Common.Helper
{
    public static class UrlPathHelper
    {
        public static bool IsRelationalUrl(string path)
        {
            return !string.IsNullOrEmpty(path) && !path.StartsWith("http:") && !path.StartsWith("https:");
        }
        public static Uri HomeUrl()
        {
            var url = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Host}";

            return new Uri(url);
        }

    }
}