﻿using Jumbula.Core.Model.Order;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ChangeDaysViewModel
    {
        public List<DayOfWeek> Days { get; set; }
        public List<DayOfWeek> NewDays { get; set; }
        public string DaysName
        {
            get
            {
                if (Days != null && Days.Any())
                {
                    return string.Join(", ", Days);
                }
                return string.Empty;
            }
        }
        public string DesiredStartDate { get; set; }
        public List<SubscriptionProgramDaysViewModel> AllProgramDays { get; set; }
        public long OrderItemId { get; set; }

        public DateTime? EffectiveDate { get; set; }
    }
}