﻿using System.Web.Mvc;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using SportsClub.Models;

namespace Jumbula.Web.Controllers
{
    public class ClubMembersController : JbBaseController
    {
        public ClubMembersController()
            :base()
        {
            if (HttpContext.Request.Url.HasSubDaomin())
            {
                this._clubSubDoamin = HttpContext.Request.Url.GetSubDomain();
            }
        }

        private string _clubSubDoamin;
        #region Admin Authenticated Calls

        [HttpGet]
        [Authorize(Roles = "Admin, SysAdmin, Support, Manager")]
        public virtual ActionResult DisplayMembers()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin, SysAdmin, Support, Manager")]
        public virtual JsonResult DisplayMembersPaged([DataSourceRequest] DataSourceRequest request)
        {
            var activeclub = Ioc.ClubBusiness.Get(_clubSubDoamin);
            var clubDomain = activeclub.Domain;
            AllClubMembersViewModel allClubMembers = new AllClubMembersViewModel(clubDomain, request.PageSize, request.Page);
            request.Page = 1;
            var result = allClubMembers.ClubMemberItems.ToDataSourceResult(request);
            result.Total = allClubMembers.Total;
            return Json(result);
        }

        [HttpGet]
        [Authorize(Roles = "Admin, SysAdmin, Support, Manager")]
        public virtual ActionResult DiplayMemberOrders(int? userId, int? playerId)
        {
            // for merge to method see all user orders and see player orders
            // if playerid has value then this class called from player view orders
            
            ClubMemberOrdersModel clubMemberOrders = new ClubMemberOrdersModel(userId,playerId);
            return PartialView("_ClubMemberOrders",clubMemberOrders);
        }

       

        #endregion
    }
}
