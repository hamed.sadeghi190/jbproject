﻿using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Web.Mvc.Controllers;
using SportsClub.Models;
using System.Web.Mvc;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class FormController : DashboardBaseController
    {
        private readonly IFollowupFormBusiness _followupFormBusiness;

        public FormController(IFollowupFormBusiness followupFormBusiness)
        {
            _followupFormBusiness = followupFormBusiness;
        }
        public virtual ActionResult ChangeFollowUpFormMode(int id)
        {
            OperationStatus res = _followupFormBusiness.ChangeFollowUpFormMode(id);

            return Json(res);
        }

        public virtual ActionResult AssignedTemplateToPrograms(int id)
        {
            var message = string.Empty;
            var res = _followupFormBusiness.AssignedTemplateToPrograms(id, ActiveClub.Id);

            if (res)
            {
                message = _followupFormBusiness.GetNamesOfProgramsThatAssignedTemplate(id, ActiveClub.Id);
            }

            return Json(new JResult { Status = res , Message = message });
        }
    }
}