﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('displayInvoiceMessageController', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', 'familyOrderItemsService', 'familyOrdersService', 'informationInvoiceService', function ($scope, $http, jbToast,$log, $stateParams, $state, familyOrderItemsService, familyOrdersService, informationInvoiceService) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/People";
        
        $scope.Model = familyOrdersService.get();
        $scope.OrderitemModel = familyOrderItemsService.get();
        $scope.InfoModel = informationInvoiceService.get();
        

        $scope.BackToCreateInvoice = function () {
            informationInvoiceService.set($scope.InfoModel);
            $state.back();
        }

        
         
        $scope.ModelDate = {};
        $scope.selectDuedate = 0;
        $scope.selectDuedate = $scope.InfoModel.SelectDueDate

        $http.get($scope.baseUrl + '/GetInvoiceMessage?selectDueDate=' + $scope.selectDuedate + '&orderItemId=' + $stateParams.orderItemId).success(function (data, status, headers, config) {
            $scope.ModelDate = data;
        });
       
        $scope.totalAmount = 0;
        for (var i = 0; i < $scope.Model.length; i++) {

            if ($scope.Model[i].Checked) {

                $scope.Model[i].orginalAmount = $scope.Model[i].InvoiceAmount;
                $scope.totalAmount += $scope.Model[i].orginalAmount;
            }

        }

        $scope.SendInvoice = function () {
            $('#submitSendInvoice').prop("disabled", true);

            var orderItemModel = [];

            for (var i = 0; i < $scope.Model.length; i++) {

                if ($scope.Model[i].Checked) {

                    for (var j = 0; j < $scope.OrderitemModel[$scope.Model[i].Id].length; j++) {

                        if ($scope.OrderitemModel[$scope.Model[i].Id][j].Checked) {

                            orderItemModel.push($scope.OrderitemModel[$scope.Model[i].Id][j]);

                        }

                    }

                }
            }

            $http.post(window.location.origin + '/Dashboard/People/SubmitSendInvoice', { model: orderItemModel, Infomodel: $scope.InfoModel, orderItemId: $stateParams.orderItemId })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.backQueueItems.pop();
                        jbToast.success("Your invoice was sent successfully.", "");

                        if ($stateParams.orderItemId != null && $stateParams.orderItemId > 0) {

                            console.log($stateParams.orderItemId)

                            $state.go('OrderItem', { seasonDomain: $scope.MC.stateParams.seasonDomain, orderItemId: $stateParams.orderItemId });

                        } else {
                            $state.go("Invoices", null);
                        }
                        }
                        else {
                            $('#submitSendInvoice').prop("disabled", true);
                            $scope.MC.handleErrors($scope, data);
                        }


                });

        };

    }]);
})();