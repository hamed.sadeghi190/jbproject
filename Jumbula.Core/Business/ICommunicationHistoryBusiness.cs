﻿using System.Linq;
using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.CommunicationHistory;

namespace Jumbula.Core.Business
{
    public interface ICommunicationHistoryBusiness
    {
        CommunicationHistoryViewModel Get(int id, int clubId);


        IQueryable<CommunicationHistory> GetListOfContact(int contactId);

        IQueryable<CommunicationHistory> GetListOfEmail(int emailId);
        
        OperationStatus Delete(int id);

        OperationStatus Save(CommunicationHistoryViewModel communicationHistoryViewModel);

        PaginatedList<CommunicationHistoryViewModel> GetViewHistoryList(PaginationModel paginationModel, CommunicationHistoryFilterViewModel filters, int clubId);
    }
}
