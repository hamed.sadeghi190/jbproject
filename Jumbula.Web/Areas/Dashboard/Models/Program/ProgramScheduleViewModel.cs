﻿using Newtonsoft.Json;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Http.ModelBinding;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramScheduleViewModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public List<ProgramScheduleDayViewModel> Days { get; set; }

        [Display(Name = "Start date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Start time")]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? StartTime { get; set; }

        [Display(Name = "End time")]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? EndTime { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        public bool IsProrate { get; set; }

        public List<SelectKeyValue<string>> GenderItems { get; set; }

        public Genders SelectedGender { get; set; }

        [Display(Name = "Minimum enrollment")]
        public int MinimumEnrollment { get; set; }

        public string Capacity { get; set; }

        public int SelectedCapacity { get; set; }
        [Display(Name = "show capacity left number")]
        public bool ShowCapacityLeft { get; set; }
        [Display(Name = "capacity left number")]
        public int ShowCapacityLeftNumber { get; set; }
        public List<SelectKeyValue<int>> CapacityLeftNumbers { get; set; }

        public List<SelectKeyValue<string>> ContinueTypes { get; set; }

        public ContinueType SelectedContinueType { get; set; }

        public string SelectedSession { get; set; }

        public List<SelectKeyValue<string>> RestrictionTypes { get; set; }

        public RestrictionType SelectedRestrictionType { get; set; }

        public List<SelectKeyValue<string>> Grades { get; set; }

        public string SelectedMinAge { get; set; }

        public string SelectedMaxAge { get; set; }

        public SchoolGradeType? SelectedMinGrade { get; set; }

        public SchoolGradeType? SelectedMaxGrade { get; set; }

        public List<Tuition> Tuitions { get; set; }

        public List<Charges> Charges { get; set; }

        public bool IsScheduleChanged { get; set; }
        public bool IsCampOvernight { get; set; }

        public bool HasOrder { get; set; }

        public bool EnableDropIn { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Drop-in lable")]
        public string DropInLable { get; set; }

        public bool AppliesRestrictionAtTheProgramStart { get; set; }
    }

    public class Tuition
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Tuition label")]
        [StringLength(256, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Label { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Price")]
        public decimal? Price { get; set; }

        public string Color { get; set; }

        public string Capacity { get; set; }

        public bool HasEarlyBird { get; set; }

        [Display(Name = "Is prorate")]
        public bool IsProrate { get; set; }
        [Display(Name = "prorate start date")]
        public DateTime? ProrateStartDate { get; set; }
        [Display(Name = "missing session price")]
        public decimal? ProrateSessionPrice { get; set; }
        [Display(Name = "Tuition description")]
        [StringLength(1024, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Description { get; set; }

        [Display(Name = "Drop-in price")]
        [Required(ErrorMessage = "{0} is required.")]
        public decimal? DropInPrice { get; set; }

        [Display(Name = "Drop-in label")]
        [Required(ErrorMessage = "{0} is required.")]
        public string DropInName { get; set; }

        //public DropIn DropInInformation { get; set; }
        public List<EarlyBird> EarlyBirds { get; set; }

        public bool HasOrder { get; set; }
    }

    public class Charges
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Charge label")]
        [StringLength(128, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Label { get; set; }

        [Required(ErrorMessage = "Price is required.")]
        public decimal Price { get; set; }

        public string Capacity { get; set; }

        [Display(Name = "Charge description")]
        [StringLength(1024, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Description { get; set; }

        public ChargeDiscountCategory Category { get; set; }

        [Display(Name = "Start tiem")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? StartTime { get; set; }

        [Display(Name = "End time")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? EndTime { get; set; }

        public bool IsMandatory { get; set; }
    }
}