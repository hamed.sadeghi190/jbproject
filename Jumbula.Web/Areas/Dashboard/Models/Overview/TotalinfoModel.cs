﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models.Overview
{
    public class TotalinfoModel
    {
        public decimal TotalSales { get; set; }
        public string SeasonTitel { get; set; }
        public int TotalAttendees { get; set; }
    }
}