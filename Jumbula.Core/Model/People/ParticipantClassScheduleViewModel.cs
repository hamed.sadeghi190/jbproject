﻿using System.Collections.Generic;

namespace Jumbula.Core.Model.People
{
    public class ParticipantClassScheduleViewModel
    {
        public List<ParticipantClassScheduleItemViewModel> ClassesInfo { get; set; }

        public string Date { get; set; }
        public string ClubName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string SchoolLogo { get; set; }
    }

    public class ParticipantClassScheduleItemViewModel
    {
        public string ProgramName { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartEndTime { get; set; }
        public string Days { get; set; }
        public string TeacherName { get; set; }
        public string Room { get; set; }
        public string Location { get; set; }
    }
}
