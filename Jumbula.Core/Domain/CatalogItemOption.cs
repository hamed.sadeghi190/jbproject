﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class CatalogItemOption : BaseEntity
    {
        public string Title { get; set; }

        public string Value { get; set; }

        public string Group { get; set; }

        public CatalogOptionType Type { get; set; }

        [ForeignKey("CatalogItemId")]
        public virtual CatalogItem CatalogItem { get; set; }

        public int? CatalogItemId { get; set; }


        [ForeignKey("CatalogSettingId")]
        public virtual CatalogSetting CatalogSetting { get; set; }

        public int? CatalogSettingId { get; set; }

    }

    public enum CatalogOptionType
    {
        GradeGrouping = 0,
        SpaceRequrement = 1
    }
}
