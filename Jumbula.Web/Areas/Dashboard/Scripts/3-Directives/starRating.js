﻿(function (angular) {
    'use strict';

    angular
      .module('dashboardApp')
.directive('starRating',
function () {
    return {
        restrict: 'A',
        template: '<ul class="rating rating-custom">' + ' <li ng-repeat="star in stars"  ng-class="star" ng-click="toggle($index)">'
       + '  <i class="jbi-star"></i>'
       + ' </li>'
       + '</ul>',
        scope: {
            ratingValue: '=ngModel',
            max: '=',
            onRatingSelected: '&',
            readonly: '=?'
        },
        link: function (scope, element, attributes) {
            if (scope.max == undefined) {
                scope.max = 5;
            }
            function updateStars() {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };
            scope.toggle = function (index) {
                if (scope.readonly == undefined || scope.readonly === false) {
                    scope.ratingValue = index + 1;
                }
            };
            scope.$watch('ratingValue', function (oldValue, newValue) {
                if (newValue || newValue === 0) {
                    updateStars();
                }
            });
        }
    };
}
);

})(angular);