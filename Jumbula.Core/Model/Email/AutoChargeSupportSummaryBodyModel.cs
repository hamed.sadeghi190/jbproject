﻿using System;
using System.Collections.Generic;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeSupportSummaryBodyModel
    {
        public List<long> SuccessInstallments { get; set; } = new List<long>();
        public List<Tuple<long, string>> FailedInstallments { get; set; } = new List<Tuple<long, string>>();
        public List<Tuple<long, string>> SystemFailedInstallments { get; set; } = new List<Tuple<long, string>>();
        public List<long> NotAttemptedInstallments { get; set; } = new List<long>();
        public List<long> OtherErrorInstallments { get; set; } = new List<long>();
        
    }
}
