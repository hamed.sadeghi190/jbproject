﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.CreditCard;
using Jumbula.Web.Areas.Dashboard.Services;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Serilog;
using Constants = Jumbula.Common.Constants.Constants;
using static Jumbula.Common.Constants.Payment;
using Jumbula.Core.Model.Order;
using Jumbula.Core.Business;

namespace Jumbula.Web.WebLogic.Payment
{
    public class JbStripeService : JbBasePaymentGateway, IPaymentGatewayService
    {
        public string CallAPI(string payLoad, string requestEndPoint)
        {

            HttpWebRequest objRequest = null;
            string responseString = string.Empty;
            try
            {
                objRequest = (HttpWebRequest)WebRequest.Create(requestEndPoint);
                objRequest.UseDefaultCredentials = true;
                objRequest.Method = REQUESTMETHOD;
                objRequest.ContentType = "application/json";
                using (StreamWriter myWriter = new StreamWriter(objRequest.GetRequestStream()))
                {
                    myWriter.Write(payLoad);
                }

                using (WebResponse response = objRequest.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        responseString = sr.ReadToEnd();
                        return responseString;
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objRequest = null;
            }


        }

        public JbStripeService(string clubDomain, bool isTestMode, long clientId = 0)
            : base(PaymentGateway.Stripe)
        {


            if (!string.IsNullOrEmpty(clubDomain))
            {
                Club club = Ioc.ClubBusiness.Get(clubDomain);


                ClientPaymentMethod clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);

                connectedAccessToken = (clientPaymentMethod.IsCreditCardTestMode || isTestMode) ? WebConfigHelper.Stripe_TestPayment_AccessToken : clientPaymentMethod.StripeAccessToken;


                IsTestMode = clientPaymentMethod.IsCreditCardTestMode || isTestMode;
                AccountId = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.Stripe_TestPayment_ConnectedId : clientPaymentMethod.StripeCustomerId;
                SecretKey = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.Stripe_SecretKey_Test : WebConfigHelper.Stripe_SecretKey_Live;
                PublishableKey = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.Stripe_PublishableKey_Test : WebConfigHelper.Stripe_PublishableKey_Live;
                ClientKey = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.Stripe_ClientId_Test : WebConfigHelper.Stripe_ClientId_Live;

            }
            else if (clientId > 0)
            {
                ClientPaymentMethod clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(clientId);

                //var client = Ioc.ClientBusiness.GetById(clientId);
                //connectedId = (clientPaymentMethod.IsStripeTestMode || isTestMode) ? WebConfigHelper.Stripe_TestPayment_ConnectedId : clientPaymentMethod.StripeCustomerId;
                connectedAccessToken = (clientPaymentMethod.IsCreditCardTestMode || isTestMode) ? WebConfigHelper.Stripe_TestPayment_AccessToken : clientPaymentMethod.StripeAccessToken;

                IsTestMode = clientPaymentMethod.IsCreditCardTestMode || isTestMode;
                AccountId = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.Stripe_TestPayment_ConnectedId : clientPaymentMethod.StripeCustomerId;
                SecretKey = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.Stripe_SecretKey_Test : WebConfigHelper.Stripe_SecretKey_Live;
                PublishableKey = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.Stripe_PublishableKey_Test : WebConfigHelper.Stripe_PublishableKey_Live;
                ClientKey = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.Stripe_ClientId_Test : WebConfigHelper.Stripe_ClientId_Live;

            }


        }

        public bool ChargeClient(Client client, string clubName, DateTime today, string billingId, string description = Constants.F_ClientMonthlyBillingDescription, decimal? chargeFee = null)
        {
            JbPaymentService paymentService = new JbPaymentService();
            var dueAmount = chargeFee.HasValue ? chargeFee.Value : Ioc.AccountingBusiness.GetClientBalance(client);
            var errorMsg = string.Empty;
            var token = string.Empty;
            var ipn = string.Empty;
            var clientDb = Ioc.ClientBusiness;
            var transaction = new BillingTransactionActivity() { Amount = dueAmount, PaymentMethod = PaymentMethod.Card, TransactionDate = DateTime.UtcNow, TransactionType = TransactionType.Payment };
            try
            {

                if (dueAmount > 0)
                {

                    if (client.CreditCards != null && client.CreditCards.Any(c => c.IsDefault && c.IsDeleted == false))
                    {
                        var userCreditCard = client.CreditCards.First(c => c.IsDefault && c.IsDeleted == false);
                        if (userCreditCard != null)
                        {
                            transaction.CreditCardId = userCreditCard.Id;
                            transaction.CreditCard = userCreditCard;


                            var metadata = paymentService.GenerateMetaData(JumbulaSubSystem.ClientMonthlyCharge, null, null, null, client, billingId);
                            var myCharge = new StripeChargeCreateOptions
                            {
                                Amount = (int)(dueAmount * 100),
                                Currency = CurrencyCodes.USD.ToString(),
                                Description = string.Format(Constants.F_Client_Pay_Memo, today.ToString("MMM"), clubName),
                                Metadata = metadata,
                                CustomerId = userCreditCard.StripeCustomerId

                            };


                            var chargeService = new StripeChargeService(WebConfigHelper.Stripe_SecretKey);
                            var stripeCharge = chargeService.Create(myCharge);
                            transaction.TransactionId = stripeCharge.Id;

                            transaction.TransactionResponse = Newtonsoft.Json.JsonConvert.SerializeObject(stripeCharge);
                            if (stripeCharge.Status.ToLower() == "succeeded" && stripeCharge.Paid)
                            {
                                var history = new ClientBillingHistory()
                                {
                                    BillingDate = today,
                                    BillingId = billingId,
                                    ClientId = client.Id,
                                    Debit = 0,
                                    Description = string.Format(description, userCreditCard.Brand, userCreditCard.LastDigits),
                                    Credit = dueAmount
                                };

                                transaction.TransactionStatus = TransactionStatus.Success;
                                transaction.TransactionDate = stripeCharge.Created;
                                history.TransactionActivities = new List<BillingTransactionActivity>();
                                history.TransactionActivities.Add(transaction);
                                client.LastMonthBillingPaid = today.Month;
                                client.BillingHistories.Add(history);
                                clientDb.Update(client);
                                return true;
                            }
                            else
                            {
                                errorMsg = stripeCharge.FailureMessage;
                            }
                        }

                    }

                }
            }
            catch (StripeException stripeException)
            {
                transaction.TransactionResponse = stripeException.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(stripeException);
            }
            catch (Exception ex)
            {
                transaction.TransactionResponse = ex.Message;

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            transaction.TransactionStatus = TransactionStatus.Failure;
            var lastBilling = client.BillingHistories.First(c => c.BillingId == billingId);
            if (lastBilling.TransactionActivities == null)
            {
                lastBilling.TransactionActivities = new List<BillingTransactionActivity>();
            }
            lastBilling.TransactionActivities.Add(transaction);
            clientDb.Update(client);
            return false;

        }

        public string GetPlaidBankAccountTokenAsync(string token, string accountId)
        {

            var baseUrl = IsTestMode ? WebConfigHelper.Plaid_LocalEndPoint : WebConfigHelper.Plaid_ProdEndPoint;
            string json = JsonConvert.SerializeObject(new
            {
                client_id = WebConfigHelper.Plaid_ClientId,
                secret = WebConfigHelper.Plaid_SecretKey,
                public_token = token
            });
            var accessTokenRes = CallAPI(json, baseUrl + WebConfigHelper.Plaid_ExchangeApi);

            var accessTokenObj = JsonConvert.DeserializeObject<dynamic>(accessTokenRes);
            var bankAccountJson = JsonConvert.SerializeObject(new
            {
                client_id = WebConfigHelper.Plaid_ClientId,
                secret = WebConfigHelper.Plaid_SecretKey,
                accessTokenObj.access_token,
                account_id = accountId
            });

            var bankAccountRes = CallAPI(bankAccountJson, baseUrl + WebConfigHelper.Plaid_BankAccountTokenApi);

            var bankAccountObj = JsonConvert.DeserializeObject<dynamic>(bankAccountRes);
            return bankAccountObj.stripe_bank_account_token;
        }

        private bool IsTestMode = true;
        private const string REQUESTMETHOD = "POST";
        private const int DEFAULT_TIMEOUT = 3600000;
        private string AccountId { get; set; }
        private string connectedAccessToken { get; set; }
        private string SecretKey { get; set; }
        private string PublishableKey { get; set; }
        private string ClientKey { get; set; }


        public StripeRefund Refund(string chargeId, decimal refundAmount, Dictionary<string, string> metaData)
        {
            var refundService = new StripeRefundService(SecretKey);
            var stripeRefundOption = new StripeRefundCreateOptions()
            {
                Amount = (int)(refundAmount * 100),
                Reason = "requested_by_customer",
                Metadata = metaData,
            };
            var stripeRequestOption = new StripeRequestOptions()
            {
                ApiKey = PublishableKey,
                StripeConnectAccountId = AccountId
            };
            return refundService.Create(chargeId, stripeRefundOption, stripeRequestOption);

        }

        public StripeRefund RefundJumbulaClient(string chargeId, decimal refundAmount)
        {
            var refundService = new StripeRefundService(SecretKey);
            var stripeRefundOption = new StripeRefundCreateOptions()
            {
                Amount = (int)(refundAmount * 100),
                Reason = "requested_by_customer"
            };
            var stripeRequestOption = new StripeRequestOptions()
            {
                ApiKey = PublishableKey
            };
            return refundService.Create(chargeId, stripeRefundOption, stripeRequestOption);

        }
        public void UpdateCreditCard(int creditCardId, string userCustomerId, string userCustomerResponse)
        {
            var creditcard = Ioc.UserCreditCardBusiness.Get(creditCardId);
            creditcard.CustomerId = userCustomerId;
            creditcard.CustomerResponse = userCustomerResponse;
            Ioc.UserCreditCardBusiness.Update(creditcard);
        }


        public StripeCharge paymentDetails(string chargeId)
        {
            var chargeService = new StripeChargeService(SecretKey);
            var stripeRequestOption = new StripeRequestOptions()
            {
                ApiKey = PublishableKey,
                StripeConnectAccountId = AccountId
            };
            return chargeService.Get(chargeId, stripeRequestOption);
        }

        public string StripeConnect(string code)
        {
            HttpWebRequest objRequest = null;
            string responseString = string.Empty;
            try
            {

                string baseUrl = "https://connect.stripe.com/oauth/token?client_secret=" + SecretKey + "&grant_type=authorization_code&client_id=" + ClientKey + "&code=" + code;

                objRequest = (HttpWebRequest)WebRequest.Create(baseUrl);
                objRequest.Method = REQUESTMETHOD;

                using (WebResponse response = objRequest.GetResponse())
                {
                    using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                    {
                        return sr.ReadToEnd();

                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                objRequest = null;
            }


        }

        public string GetSharedTokenId(string customerId, string secretKey, string cardId = "")
        {

            var myToken = new StripeTokenCreateOptions
            {
                CustomerId = customerId,

            };
            if (!string.IsNullOrEmpty(cardId))
            {
                myToken.Card = new StripeCreditCardOptions()
                {
                    TokenId = cardId
                };
            }
            var tokenService = new StripeTokenService(secretKey);
            var stripeToken = tokenService.Create(myToken);

            return stripeToken.Id;

        }


        public StripeCustomer CreateStripeCustomerId(string token, Order order, string secretKey)
        {
            try
            {
                var myCustomer = new StripeCustomerCreateOptions();
                myCustomer.Email = order.User.UserName;
                myCustomer.Description = string.Format("ConfirmationId: {0}", order.ConfirmationId);

                myCustomer.SourceToken = token;

                var customerService = new StripeCustomerService(secretKey);
                return customerService.Create(myCustomer);


            }
            catch
            {
                throw;
            }
        }

        public StripeCharge Charge(long clientId, decimal amount, CurrencyCodes currency, string token, Dictionary<string, string> metaData, string cid, string connectedId, string connectedAccessToken, long installmentId = 0, List<OrderInstallment> installments = null)
        {
            var commission = GetCommission(clientId, amount);

            var myCharge = new StripeChargeCreateOptions
            {
                Amount = (int)(amount * 100),
                Currency = currency.ToString(),
                Description = GetPaymentDescription(cid, installments),
                Metadata = metaData,
            };

            if (commission > 0)
            {
                myCharge.ApplicationFee = commission;
                // myCharge.Amount -= commission;

            }

            myCharge.SourceTokenOrExistingSourceId = token;

            var chargeService = new StripeChargeService(connectedAccessToken);
            var stripeRequestOption = new StripeRequestOptions()
            {
                StripeConnectAccountId = connectedId

            };

            return chargeService.Create(myCharge, stripeRequestOption);
        }

        private string GetPaymentDescription(string confirmationId, List<OrderInstallment> scheduleInstallments = null)
        {
            var parents = Ioc.PlayerProfileBusiness.GetParents(userId);
            var firstparent = parents.Any() ? parents.FirstOrDefault()?.Contact : null;
            var parentFullName = $"{firstparent?.FirstName} {firstparent?.LastName}";

            if (transactions != null)
            {
                var tranFirst = transactions.FirstOrDefault();

                if (tranFirst != null)
                    order = tranFirst.Order;
            }

            if (scheduleInstallments != null)
            {
                var instFirst = scheduleInstallments.FirstOrDefault(i => !i.IsDeleted);

                if (instFirst != null)
                    order = instFirst.OrderItem.Order;
            }

            if (installments != null)
            {
                var instFirst = installments.FirstOrDefault(i => !i.IsDeleted);

                if (instFirst != null)
                    order = instFirst.OrderItem.Order;
            }

            if (order == null)
                return string.Empty;

            var programNames = Ioc.OrderItemBusiness.GetNonDonationItems(order).Select(o => o.ProgramSchedule.Program.Name).Distinct().ToList();
            var donationName = Ioc.OrderItemBusiness.GetDonationItems(order);

            if (donationName.Any())
            {
                programNames.Add("Donation");
            }

            var joinedProgramNames = string.Join(", ", programNames);

            return !string.IsNullOrWhiteSpace(parentFullName) ? $"{Common.Constants.Payment.JumbulaConfirmation}{confirmationId}, {parentFullName} ({joinedProgramNames}) {order.Club.Domain} {DateTimeHelper.FormatShortDateTimeSeperetedByCamma(order.CompleteDate)}" :
                   $"{Common.Constants.Payment.JumbulaConfirmation}{confirmationId} ({joinedProgramNames}) {order.Club.Domain} {DateTimeHelper.FormatShortDateTimeSeperetedByCamma(order.CompleteDate)}";

        }

        public string UpdateInstallmentBasedOnPayment(List<OrderInstallment> installmentList, StripeCharge stripeCharge, string ipn, int? creditcardId, DateTime date, RoleCategoryType currentUserRoleType, decimal? payableAmount = null)
        {
            var result = string.Empty;

            try
            {
                var installmentDB = Ioc.InstallmentBusiness;
                if (installmentList == null && installmentList.SelectMany(c => c.TransactionActivities).Any(x => x.PaymentDetail.PayKey == stripeCharge.Id))
                {
                    Log.Warning("{LogCategory} {StripeChargeId} is not exist", "Auto Charge", stripeCharge.Id);
                    return "";
                }
                OperationStatus updateStatus = new OperationStatus() { Status = true };
                var currencyCode = CurrencyCodes.USD;
                Enum.TryParse<CurrencyCodes>(stripeCharge.Currency, out currencyCode);

                PaymentDetail paymentDetail = new PaymentDetail(stripeCharge.ReceiptEmail, PaymentDetailStatus.COMPLETED, stripeCharge.Id, PaymentMethod.Card, currencyCode, ipn, string.Empty, stripeCharge.BalanceTransactionId, currentUserRoleType, creditcardId);

                // var date = DateTime.UtcNow;
                var itemInstallment = installmentList.GroupBy(i => i.OrderItemId);
                var orderDb = Ioc.OrderBusiness;
                foreach (var installments in itemInstallment)
                {
                    var orderItem = installments.First().OrderItem;
                    var currency = orderItem.Order.Club.Currency;
                    decimal totalpaidAmount = 0;
                    foreach (var item in installments)
                    {
                        var paidAmount = payableAmount.HasValue ? payableAmount.Value : item.Balance;
                        totalpaidAmount += paidAmount;
                        item.PaidAmount = paidAmount + (item.PaidAmount.HasValue ? item.PaidAmount.Value : 0);
                        item.PaidDate = date;
                        item.Status = OrderStatusCategories.completed;
                        orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Success, item, date, paidAmount, null, null, "", "", "", TransactionCategory.Sale, HandleMode.Online);
                        var res = installmentDB.Update(item);

                        var historyInstallment = Constants.OrderInstallment_Paid;

                        if (currentUserRoleType == RoleCategoryType.Parent)
                        {
                            historyInstallment = Common.Constants.Payment.ParentOrderInstallmentPaid;
                        }

                        Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Paid, string.Format(historyInstallment, CurrencyHelper.FormatCurrencyWithPenny(item.PaidAmount, currency), item.InstallmentDate.ToString(Constants.DefaultDateFormat)), item.OrderItem.Order_Id, userId, item.OrderItemId);
                    }

                    orderItem.PaidAmount += totalpaidAmount;

                    var dbResult = orderDb.Update(orderItem.Order);

                }

                return string.Empty;
            }


            catch (Exception e)
            {
                LogHelper.LogException(e);
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return e.Message;
            }
        }

        public StripeCustomer CreateStripeCustomerForClient(AccountBillingViewModel card, PostalAddress address)
        {
            try
            {
                var club = Ioc.ClubBusiness.Get(JbUserService.GetCurrentClubId());
                var myCustomer = new StripeCustomerCreateOptions();
                if (club.ContactPersons != null && club.ContactPersons.Any() && !string.IsNullOrEmpty(club.ContactPersons.First().Email))
                {
                    myCustomer.Email = club.ContactPersons.First().Email;
                }
                else
                {
                    myCustomer.Email = username;
                }
                myCustomer.Description = string.Format("{0} ({1})", club.Name, club.Domain);
                var source = new SourceCard();

                source.Number = card.CardNumber;
                source.ExpirationYear = Convert.ToInt32(card.Year);
                source.ExpirationMonth = Convert.ToInt32(card.Month);
                source.AddressCountry = address.Country;
                source.AddressLine1 = address.StreetNumber + " " + address.Street;

                source.AddressCity = address.City;
                source.AddressState = address.State;
                source.AddressZip = address.Zip;
                source.Name = card.CardHolderName;
                source.Cvc = card.CVV;
                myCustomer.SourceCard = source;
                var customerService = new StripeCustomerService(WebConfigHelper.Stripe_SecretKey);
                return customerService.Create(myCustomer);
            }
            catch
            {
                throw;
            }
        }
        public StripeCard CreateStripeCreditCard(PaymentModels card, PostalAddress address, string customerId, string secretKey)
        {
            try
            {
                var myCustomer = new StripeCustomerCreateOptions();
                var source = new SourceCard();
                myCustomer.Email = username;

                if (customerId != "")
                {
                    var myCard = new StripeCardCreateOptions();
                    Stripe.StripeCardCreateOptions CardCreateOptions = new Stripe.StripeCardCreateOptions();

                    source.Number = card.CardNumber;
                    source.ExpirationYear = Convert.ToInt32(card.Year);
                    source.ExpirationMonth = Convert.ToInt32(card.Month);
                    source.AddressCountry = address.Country;
                    source.AddressLine1 = address.StreetNumber + " " + address.Street;
                    source.AddressCity = address.City;
                    source.AddressState = address.State;
                    source.AddressZip = address.Zip;
                    source.Name = card.CardHolderName;
                    source.Cvc = card.CVV;

                    myCard.SourceCard = source;

                    var cardService = new StripeCardService(secretKey);
                    StripeCard stripeCard = cardService.Create(customerId, myCard);
                    return cardService.Get(customerId, stripeCard.Id);
                }
                else
                {
                    myCustomer.Description = string.Format("{0} ({1})", username, userId);
                    source.Number = card.CardNumber;
                    source.ExpirationYear = Convert.ToInt32(card.Year);
                    source.ExpirationMonth = Convert.ToInt32(card.Month);
                    source.AddressCountry = address.Country;
                    source.AddressLine1 = address.StreetNumber + " " + address.Street;
                    source.AddressCity = address.City;
                    source.AddressState = address.State;
                    source.AddressZip = address.Zip;
                    source.Name = card.CardHolderName;
                    source.Cvc = card.CVV;
                    myCustomer.SourceCard = source;
                    var cardService = new StripeCardService(secretKey);
                    var customerService = new StripeCustomerService(secretKey);
                    StripeCustomer stripeCustomer = customerService.Create(myCustomer);
                    return cardService.Get(stripeCustomer.Id, stripeCustomer.DefaultSourceId);
                }
            }
            catch
            {
                throw;
            }
        }
        public StripeCard CreateStripeCustomerForUser(ParentBillingMethodsViewModel card, PostalAddress address, string customerId, string secretKey)
        {
            try
            {

                var myCustomer = new StripeCustomerCreateOptions();
                var source = new SourceCard();
                myCustomer.Email = username;

                if (customerId != null)
                {
                    var myCard = new StripeCardCreateOptions();
                    Stripe.StripeCardCreateOptions CardCreateOptions = new Stripe.StripeCardCreateOptions();

                    source.Number = card.CardNumber;
                    source.ExpirationYear = Convert.ToInt32(card.Year);
                    source.ExpirationMonth = Convert.ToInt32(card.Month);
                    source.AddressCountry = address.Country;
                    source.AddressLine1 = address.StreetNumber + " " + address.Street;
                    source.AddressCity = address.City;
                    source.AddressState = address.State;
                    source.AddressZip = address.Zip;
                    source.Name = card.CardHolderName;
                    source.Cvc = card.CVV;

                    myCard.SourceCard = source;
                    var cardService = new StripeCardService(secretKey);
                    StripeCard stripeCard = cardService.Create(customerId, myCard);
                    return cardService.Get(customerId, stripeCard.Id);
                }
                else
                {
                    myCustomer.Description = string.Format("{0} ({1})", username, userId);
                    source.Number = card.CardNumber;
                    source.ExpirationYear = Convert.ToInt32(card.Year);
                    source.ExpirationMonth = Convert.ToInt32(card.Month);
                    source.AddressCountry = address.Country;
                    source.AddressLine1 = address.StreetNumber + " " + address.Street;
                    source.AddressCity = address.City;
                    source.AddressState = address.State;
                    source.AddressZip = address.Zip;
                    source.Name = card.CardHolderName;
                    source.Cvc = card.CVV;
                    myCustomer.SourceCard = source;
                    var cardService = new StripeCardService(secretKey);
                    var customerService = new StripeCustomerService(secretKey);
                    StripeCustomer stripeCustomer = customerService.Create(myCustomer);
                    return cardService.Get(stripeCustomer.Id, stripeCustomer.DefaultSourceId);
                }
            }
            catch
            {
                throw;
            }
        }
        public StripeCustomer UpdateStripeCustomerForClient(AccountBillingViewModel card, PostalAddress address, string customerId)
        {
            try
            {
                var source = new SourceCard();
                var club = Ioc.ClubBusiness.Get(JbUserService.GetCurrentClubId());
                var myCustomer = new StripeCustomerUpdateOptions();
                myCustomer.Email = !string.IsNullOrEmpty(club.ContactPersons.First().Email) ? club.ContactPersons.First().Email : username;
                myCustomer.Description = string.Format("{0} ({1})", club.Name, club.Domain);
                //myCustomer.Source = new StripeSourceOptions();
                source.Number = card.CardNumber;
                source.ExpirationYear = Convert.ToInt32(card.Year);
                source.ExpirationMonth = Convert.ToInt32(card.Month);
                source.AddressCountry = address.Country;
                source.AddressLine1 = address.StreetNumber + " " + address.Street;
                source.AddressCity = address.City;
                source.AddressState = address.State;
                source.AddressZip = address.Zip;
                source.Name = card.CardHolderName;
                source.Cvc = card.CVV;
                myCustomer.SourceCard = source;
                var customerService = new StripeCustomerService(WebConfigHelper.Stripe_SecretKey);
                return customerService.Update(customerId, myCustomer);
            }
            catch
            {
                throw;
            }
        }

        public StripeCard UpdateStripeCustomerForUser(ParentBillingMethodsViewModel card, PostalAddress address, UserCreditCard userCreditCard, string customerId, string secretKey)
        {
            StripeCard stripeCard = null;

            try
            {
                var myCustomer = new StripeCustomerCreateOptions { Email = username };

                customerId = userCreditCard?.CustomerId;

                var cardId = GetCardId(userCreditCard);
                var deletedStatus = DeleteCard(customerId, cardId);

                if (!deletedStatus.Deleted) return null;

                var model = new PaymentModels
                {
                    CVV = card.CVV,
                    Brand = card.Brand,
                    CardNumber = card.CardNumber,
                    CardHolderName = card.CardHolderName,
                    Month = card.Month,
                    Year = card.Year,
                };

                stripeCard = CreateStripeCreditCard(model, address, customerId, secretKey);
                return stripeCard;
            }
            catch (Exception ex)
            {
                var stripeMessage = stripeCard?.StripeResponse?.ResponseJson;

                throw new Exception(!string.IsNullOrEmpty(stripeMessage) ? stripeMessage : ex.Message, ex);



            }

        }

        public StripeCustomer RetrievingCustomer(string customerId)
        {
            try
            {
                var customerService = new StripeCustomerService(WebConfigHelper.Stripe_SecretKey);
                return customerService.Get(customerId);
            }
            catch
            {
                throw;
            }
        }
        public PaymentResponseModel DirectPay(ControllerContext controllerContext, PayViewModel model)
        {

            var isUpdate = false;
            var creditCards = Ioc.UserCreditCardBusiness;
            IQueryable<UserCreditCard> userCreditCardNotLast;
            resultResponse.cardErrors = new Dictionary<string, string>();
            bool isCardCreated = false;
            ipn = model.CreateTokenResponse;
            StripeCharge stripeCharge = null;

            StripeCard stripeCard = null;
            StripeCustomer res;

            var selectedCreditCard = creditCards.Get(model.CreditId);
            CurrencyCodes currency = CurrencyCodes.USD;

            try
            {

                var isCreditExist = false;

                JbPaymentService paymentService = new JbPaymentService();

                SetParameters(model);
                var userCreditCard = creditCards.GetUserCreditCards(userId, PaymentGateway.Stripe);
                isCreditExist = isCreditCardExist(model, _paymentGateway, userId);
                UserCreditCard CreditCardExistWithCustomer = null;
                var modelParentBilling = new ParentBillingMethodsViewModel();

                if (model.PaymentMethod == PaymentMethod.Card)
                {
                    CreditCardExistWithCustomer = Ioc.UserCreditCardBusiness.GetCreditCards(model.CardNumber, userId, _paymentGateway).Where(c => c.CustomerId != "").FirstOrDefault();

                    modelParentBilling = GetModelParentBilling(model);
                }

                userCreditCard = creditCards.GetUserCreditCards(userId, _paymentGateway);
                if (club != null)
                {
                    var client = club.Client;
                    currency = club.Currency;
                    var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                    if (club.PartnerId.HasValue && club.IsSchool)
                    {
                        client = club.PartnerClub.Client;
                        currency = club.PartnerClub.Currency;
                    }

                    var connectedId = (clinetPaymentMethod.IsCreditCardTestMode || isOrderTestMode) ? WebConfigHelper.Stripe_TestPayment_ConnectedId : clinetPaymentMethod.StripeCustomerId;
                    var connectedAccessToken = (clinetPaymentMethod.IsCreditCardTestMode || isOrderTestMode) ? WebConfigHelper.Stripe_TestPayment_AccessToken : clinetPaymentMethod.StripeAccessToken;
                    var secretKey = (clinetPaymentMethod.IsCreditCardTestMode || isOrderTestMode) ? WebConfigHelper.Stripe_SecretKey_Test : WebConfigHelper.Stripe_SecretKey_Live;

                    var userCustomerId = string.Empty;
                    var userCustomerResponse = string.Empty;
                    var token = model.Token;

                    if ((model.ActionCaller == JumbulaSubSystem.Order || model.ActionCaller == JumbulaSubSystem.MakeaPayment) && order.IsAutoCharge)
                    {
                        if (model.PaymentMethod == PaymentMethod.Card) // No select express payment methods available
                        {
                            if (userCreditCard != null && userCreditCard.Any())
                            {
                                var firstCard = userCreditCard.ToList().First();
                                var lastCard = userCreditCard.ToList().LastOrDefault();

                                //show express payment methods available if select express payment methods  just create token in card
                                if (firstCard.CustomerId == lastCard.CustomerId)
                                {
                                    userCustomerId = lastCard.CustomerId;
                                }
                                //Token in state create in javascript
                            }
                            else
                            {
                                res = CreateStripeCustomerId(model.Token, order, secretKey);
                                var cardService = new StripeCardService(secretKey);
                                userCustomerId = res.Id;
                                stripeCard = cardService.Get(userCustomerId, res.DefaultSourceId);
                                userCustomerResponse = JsonConvert.SerializeObject(res);
                                token = GetSharedTokenId(userCustomerId, connectedAccessToken);
                                isCardCreated = true;
                            }
                        }
                        else //select express payment methods available
                        {
                            if (userCreditCard != null && userCreditCard.Any())
                            {
                                var cardId = "";
                                userCustomerId = selectedCreditCard.CustomerId;
                                cardId = GetCardId(selectedCreditCard);

                                if (!userCreditCard.Any(u => u.IsDefault)) // If no preferred in express payment methods available
                                {
                                    selectedCreditCard.IsDefault = true;
                                    selectedCreditCard.DefaultDate = DateTime.UtcNow;
                                    creditCards.Update(selectedCreditCard); //update payment methods for preferred payment methods
                                }

                                token = GetSharedTokenId(userCustomerId, connectedAccessToken, cardId);
                            }
                        }
                    }
                    else
                    {
                        if (model.PaymentMethod != PaymentMethod.Card) //select express payment methods available
                        {
                            var cardId = "";
                            cardId = GetCardId(selectedCreditCard);
                            token = GetSharedTokenId(selectedCreditCard.CustomerId, connectedAccessToken, cardId);//token for payment method selected
                        }
                    }

                    //charge first installment by token created

                    var confirmationIds = new List<string>();
                    if (model.ActionCaller == JumbulaSubSystem.Invoice)
                    {
                        var transactionsGroup = transactions.GroupBy(t => t.OrderId);
                        foreach (var tran in transactionsGroup)
                        {
                            var confermation = tran.First().Order.ConfirmationId;
                            confirmationIds.Add(confermation);
                        }
                        model.ConfirmatiocIds = confirmationIds;
                        stripeCharge = Charge(client.Id, payableAmount, currency, token, metadata, model.StrConfirmatiocIds, connectedId, connectedAccessToken);
                    }
                    else
                    {

                        stripeCharge = Charge(client.Id, payableAmount, currency, token, metadata, model.ConfirmationId, connectedId, connectedAccessToken);
                    }

                    if (stripeCharge.Status.ToLower().Equals(StripeChargeStatus.succeeded.ToString()) && stripeCharge.Paid)
                    {
                        try
                        {
                            ipn = Newtonsoft.Json.JsonConvert.SerializeObject(stripeCharge);

                            var modelCard = new PaymentModels();

                            if (model.PaymentMethod == PaymentMethod.Card)
                            {
                                var brand = GetBrandCard(model.CardNumber);

                                modelCard = GetPaymentModels(model);
                                modelCard.Brand = brand;
                            }

                            if (model.ActionCaller == JumbulaSubSystem.Order || model.ActionCaller == JumbulaSubSystem.MakeaPayment)
                            {

                                #region Register Order

                                var isMakeaPayment = false;
                                var isOnline = false;

                                if (model.ActionCaller == JumbulaSubSystem.MakeaPayment)
                                {
                                    isMakeaPayment = true;
                                }

                                if (order.OrderMode == OrderMode.Online)
                                {
                                    isOnline = true;
                                }

                                if (model.PaymentMethod == PaymentMethod.Card)
                                {
                                    var countActive = 0;
                                    var countInstallment = 0;
                                    var isSubscription = false;
                                    var orderItems = new List<OrderItem>();

                                    if (model.ActionCaller != JumbulaSubSystem.MakeaPayment)
                                    {
                                        orderItems = model.Cart.Order.OrderItems.Where(o => o.Order.IsAutoCharge).ToList();

                                        foreach (var item in orderItems)
                                        {
                                            countInstallment += item.Installments.Where(i => !i.IsDeleted).Count();
                                            if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                            {
                                                isSubscription = true;
                                            }
                                        }
                                    }
                                    countActive = GetCountActive(userCreditCard);
                                    SpecifyDefault(countActive, model, countInstallment);


                                    if ((countActive < 1 && model.IsDefault == false && (countInstallment > 0 || orderItems.Count > 0)) || isSubscription)//in installment
                                    {
                                        model.IsDefault = true;

                                    }
                                    if (model.IsDefault)
                                    {
                                        if (CreditCardExistWithCustomer != null)
                                        {
                                            creditCard = CreditCardExistWithCustomer;
                                        }
                                        else
                                        {
                                            creditCard = Ioc.UserCreditCardBusiness.Get(modelParentBilling, userId, model.PaymentMethod, model.CreditId, _paymentGateway, model.Address);
                                        }
                                    }
                                    else
                                    {
                                        creditCard = Ioc.UserCreditCardBusiness.Get(modelParentBilling, userId, model.PaymentMethod, model.CreditId, _paymentGateway, model.Address);
                                    }

                                    if (creditCard != null && isCreditExist) //Exist Lastdigit in all credit cards in db 
                                    {
                                        var cardId = creditCard.Id;

                                        if (model.IsDefault)
                                        {
                                            if (string.IsNullOrEmpty(creditCard.CustomerId)) // if credit card exist in db not have StripeCustomerId
                                            {
                                                if (!isCardCreated)
                                                {

                                                    stripeCard = CreateStripeCreditCard(modelCard, model.Address, userCustomerId, secretKey);

                                                }

                                            } //if credit card exist in db have StripeCustomerId we update a creditcard
                                            else
                                            {
                                                stripeCard = UpdateStripeCustomerForUser(modelParentBilling, model.Address, creditCard, userCustomerId, secretKey);

                                            }

                                            SetCreditCardForUpdate(model);
                                            creditCard.Brand = stripeCard.Brand;

                                            creditCard.CardId = stripeCard.Id;
                                            creditCard.CustomerId = stripeCard.CustomerId;
                                            creditCard.CustomerResponse = userCustomerResponse;

                                            creditCard.TypePaymentGateway = PaymentGateway.Stripe;
                                            creditCard.Id = cardId;
                                            //update in stripe & db
                                            foreach (var CreditCard in userCreditCard)
                                            {
                                                if (CreditCard.IsDefault)
                                                {
                                                    CreditCard.IsDefault = false;
                                                    creditCards.Update(CreditCard);
                                                }
                                            }
                                            creditCard.IsDefault = true;
                                            creditCard.DefaultDate = DateTime.UtcNow;
                                            creditCard.UpdatedDate = DateTime.UtcNow;

                                            creditCards.Update(creditCard);
                                        }
                                        else
                                        {
                                            creditCard.Id = cardId;
                                            // just charge 
                                        }

                                    }
                                    else if (creditCard == null && isCreditExist) // one information is changed
                                    {

                                        if (model.IsDefault)
                                        {
                                            if (!isCardCreated && CreditCardExistWithCustomer == null)
                                            {

                                                stripeCard = CreateStripeCreditCard(modelCard, model.Address, userCustomerId, secretKey);
                                                userCustomerResponse = JsonConvert.SerializeObject(stripeCard);
                                                modelCard.CardId = stripeCard.Id;
                                                foreach (var CreditCard in userCreditCard)
                                                {
                                                    if (CreditCard.IsDefault)
                                                    {
                                                        CreditCard.IsDefault = false;
                                                        creditCards.Update(CreditCard);
                                                    }
                                                }

                                                creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, stripeCard.CustomerId, modelCard.CardId, userCustomerResponse, true, _paymentGateway, isMakeaPayment, isOnline);
                                            }
                                            else if ((isCardCreated && CreditCardExistWithCustomer != null) || (!isCardCreated && CreditCardExistWithCustomer != null))
                                            {
                                                modelParentBilling = GetModelParentBilling(model);
                                                modelParentBilling.Id = CreditCardExistWithCustomer.Id;

                                                stripeCard = UpdateStripeCustomerForUser(modelParentBilling, model.Address, creditCard, userCustomerId, secretKey);

                                                creditCard = CreditCardExistWithCustomer;

                                                SetCreditCardForUpdate(model);
                                                creditCard.Brand = stripeCard.Brand;

                                                creditCard.CardId = stripeCard.Id;
                                                creditCard.CustomerId = stripeCard.CustomerId;
                                                creditCard.CustomerResponse = userCustomerResponse;

                                                creditCard.Brand = stripeCard.Brand;
                                                creditCard.Name = stripeCard.Name;
                                                creditCard.UpdatedDate = DateTime.UtcNow;
                                                creditCard.TypePaymentGateway = PaymentGateway.Stripe;

                                                creditCard.Id = CreditCardExistWithCustomer.Id;
                                                //update in stripe & db
                                                foreach (var CreditCard in userCreditCard)
                                                {
                                                    if (CreditCard.IsDefault)
                                                    {
                                                        CreditCard.IsDefault = false;

                                                        creditCards.Update(CreditCard);
                                                    }
                                                }
                                                creditCard.IsDefault = true;

                                                creditCard.UpdatedDate = DateTime.UtcNow;
                                                creditCards.Update(creditCard);
                                            }
                                            else if (isCardCreated && CreditCardExistWithCustomer == null)
                                            {
                                                creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, stripeCard.Id, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                            }
                                        }
                                        else
                                        {

                                            userCustomerId = "";
                                            //creditCard = CreateCreditCardjustdb(modelCard, userId, postalAddress1, userCustomerId, userCustomerResponse, newDefault);
                                            creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, modelCard.CardId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);

                                        }
                                    }
                                    else
                                    {
                                        creditCard = Ioc.UserCreditCardBusiness.Get(modelParentBilling, userId, model.PaymentMethod, model.CreditId, PaymentGateway.Stripe, model.Address);

                                        if (userId > 0 && creditCard != null) // full Similar card in db
                                        {
                                            if (!model.IsDefault)
                                            {

                                                if (string.IsNullOrEmpty(creditCard.CustomerId) && model.ActionCaller == JumbulaSubSystem.Order && order.IsAutoCharge)
                                                {
                                                    creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, modelCard.CardId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);

                                                }

                                            }
                                            if (model.IsDefault)
                                            {
                                                if (string.IsNullOrEmpty(creditCard.CustomerId))
                                                {

                                                    if (userCreditCard != null && userCreditCard.Any())
                                                    {

                                                        stripeCard = CreateStripeCreditCard(modelCard, model.Address, userCustomerId, secretKey);


                                                        foreach (var CreditCard in userCreditCard)
                                                        {
                                                            if (CreditCard.IsDefault)
                                                            {
                                                                CreditCard.IsDefault = false;
                                                                creditCards.Update(CreditCard);
                                                            }
                                                        }

                                                        creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, stripeCard.CustomerId, stripeCard.Id, userCustomerResponse, true, _paymentGateway, isMakeaPayment, isOnline);

                                                    }
                                                }
                                                else
                                                {

                                                    if (!string.IsNullOrEmpty(creditCard.CustomerId) && model.ActionCaller != JumbulaSubSystem.Order && !order.IsAutoCharge)
                                                    {
                                                        foreach (var CreditCard in userCreditCard)
                                                        {
                                                            if (CreditCard.IsDefault)
                                                            {
                                                                CreditCard.IsDefault = false;
                                                                creditCards.Update(CreditCard);
                                                            }
                                                        }

                                                        creditCard.IsDefault = true;
                                                        creditCards.Update(creditCard); //update payment methods for preferred payment methods}
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (userCustomerId == "") // No select express payment methods available and no installment just save in db
                                            {
                                                creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, stripeCharge.Id, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                            }
                                            else if (userCustomerId != "" && model.PaymentMethod != PaymentMethod.Express)
                                            {
                                                if (userCreditCard != null && userCreditCard.Any())
                                                {

                                                    if (newDefault)
                                                    {
                                                        foreach (var CreditCard in userCreditCard) // remove default previuse
                                                        {
                                                            if (CreditCard.IsDefault)
                                                            {
                                                                CreditCard.IsDefault = false;
                                                                creditCards.Update(CreditCard);
                                                            }
                                                        }
                                                        stripeCard = CreateStripeCreditCard(modelCard, model.Address, userCustomerId, secretKey);
                                                        creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, stripeCard.Id, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);

                                                        if (isUpdate)
                                                        {
                                                            if (userCreditCard != null && userCreditCard.Any())
                                                            {
                                                                userCreditCardNotLast = userCreditCard.Take(userCreditCard.Count() - 1);//select items Except Last
                                                                foreach (var CreditCard in userCreditCardNotLast)
                                                                {
                                                                    if (CreditCard.IsDefault)
                                                                    {
                                                                        CreditCard.IsDefault = false;
                                                                        creditCards.Update(CreditCard);
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                    else  // for just save in db
                                                    {
                                                        userCustomerId = "";
                                                        creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, null, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                    }

                                                }
                                                else  //no express payment method available and select pay by card and is installment(create in stripe first action) 
                                                {
                                                    if (model.IsDefault)
                                                    {
                                                        creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, stripeCard.Id, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                    }

                                                    if (isUpdate)
                                                    {
                                                        if (userCreditCard != null && userCreditCard.Any())
                                                        {

                                                            userCreditCardNotLast = userCreditCard.Take(userCreditCard.Count() - 1);//select items Except Last
                                                            foreach (var CreditCard in userCreditCardNotLast)
                                                            {
                                                                if (CreditCard.IsDefault)
                                                                {
                                                                    CreditCard.IsDefault = false;
                                                                    creditCards.Update(CreditCard);
                                                                }
                                                            }

                                                        }
                                                    }

                                                }

                                            }

                                        }
                                    }
                                }
                                #endregion
                            }
                            else  //add for invoice  -Installment
                            {
                                if (model.PaymentMethod == PaymentMethod.Card)
                                {
                                    creditCard = Ioc.UserCreditCardBusiness.Get(modelParentBilling, userId, model.PaymentMethod, model.CreditId, PaymentGateway.Stripe);

                                    if (userId > 0 && creditCard == null)
                                    {

                                        if (userCustomerId == "")
                                        {
                                            creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, string.Empty, userCustomerResponse, false, _paymentGateway, false, false);
                                        }
                                    }
                                }
                            }

                            //update in db

                            resultResponse = SuccessPayment(model, stripeCharge, payableAmount, controllerContext, selectedCreditCard);
                            return resultResponse;

                        }
                        catch (Exception ex)
                        {
                            if (stripeCharge.Status.ToLower().Equals(StripeChargeStatus.succeeded.ToString()) && stripeCharge.Paid)
                            {
                                resultResponse.errorMsg = "You have successfully charged the user, but the balance may not reflect the correct amount. Do not charge the user again. Please contact Jumbula at support@jumbula.com and report this issue.";

                                if (ex.InnerException == null)
                                {
                                    resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);
                                    resultResponse.cardErrors.Add("error", resultResponse.errorMsg);

                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                                else if (ex.InnerException != null)
                                {
                                    resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.InnerException.Message);
                                    resultResponse.cardErrors.Add("error", resultResponse.errorMsg);

                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                }

                                if (WebConfigHelper.IsProductionMode)
                                {
                                    var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
                                    string emailSubject = string.Concat("Error in Create Credit Card", " ", model.ConfirmationId);
                                    string emailBody = ViewHelper.RenderViewToString(controllerContext, @"~/Views/Shared/Email/ErrorInTakePayment.cshtml", null);
                                    Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, emailBody, true);
                                }
                            }

                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    else
                    {
                        resultResponse.cardErrors.Add("error", stripeCharge.FailureMessage);
                        resultResponse.cardErrors.Add("errorex" + rnd.Next(52), stripeCharge.FailureMessage);
                        resultResponse.errorMsg = stripeCharge.FailureMessage;
                    }
                }

            }
            catch (StripeException stripeException)
            {
                resultResponse.errorMsg = stripeException.Message;
                resultResponse.cardErrors.Add("error", stripeException.Message);
                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), stripeException.Message);

                Elmah.ErrorSignal.FromCurrentContext().Raise(stripeException);
            }
            catch (Exception ex)
            {
                resultResponse.errorMsg = ex.Message;
                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            FailurePayment(model);

            return resultResponse;
        }

        public string UpdateOrderBasedOnPayment(ControllerContext controllerContext, Order order, StripeCharge stripeCharge, string ipn, int? creditcardId, PaymentDetailStatus paymentStaus = PaymentDetailStatus.COMPLETED, TransactionStatus transactionStatus = TransactionStatus.Success, PaymentMethod paymentMethod = PaymentMethod.Card, List<TransactionActivity> transactions = null)
        {
            try
            {
                if (transactions == null)
                {

                    PaymentDetail paymentDetail = new PaymentDetail(stripeCharge.ReceiptEmail, paymentStaus, stripeCharge.Id, paymentMethod, order.Club.Currency, ipn, string.Empty, stripeCharge.BalanceTransactionId, JbUserService.GetCurrentUserRoleType(), creditcardId);
                    if (order.IsDraft == false && order.TransactionActivities != null && order.TransactionActivities.Count() > 0)
                    {
                        return "";
                    }
                    order.IsDraft = false;
                    if (order.OrderStatus == OrderStatusCategories.deleted)
                    {
                        order.OrderStatus = OrderStatusCategories.completed;
                    }

                    order.OrderStatus = OrderStatusCategories.completed;
                    var installmentToken = string.Empty;
                    var date = DateTime.UtcNow;
                    foreach (OrderItem item in order.RegularOrderItems)
                    {
                        item.CreditCardId = creditcardId;
                        item.ItemStatus = OrderItemStatusCategories.completed;
                        item.PaidAmount = paymentStaus == PaymentDetailStatus.COMPLETED ? item.TotalAmount : 0;
                        if (string.IsNullOrEmpty(installmentToken))
                        {
                            installmentToken = Guid.NewGuid().ToString("N");
                        }
                        if (item.PaymentPlanType == PaymentPlanType.Installment && item.Installments.Any())
                        {
                            if (item.ProgramTypeCategory != ProgramTypeCategory.Subscription && item.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                            {
                                var firstInst = item.Installments.Where(i => !i.IsDeleted).FirstOrDefault();
                                item.PaidAmount = paymentStaus == PaymentDetailStatus.COMPLETED ? firstInst.Amount : 0;
                                firstInst.PaidDate = date;
                                firstInst.PaidAmount = paymentStaus == PaymentDetailStatus.COMPLETED ? firstInst.Amount : 0;
                                firstInst.Status = OrderStatusCategories.completed;
                                firstInst.NoticeSent = true;
                                firstInst.Token = installmentToken;
                            }
                            else
                            {
                                Ioc.SubscriptionBusiness.SetInstallmentsStatus(item, date);
                            }
                        }
                    }

                    var orderHistory = Constants.Order_Paid;

                    if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                    {
                        orderHistory = Common.Constants.Payment.ParentOrderPaid;
                    }

                    Ioc.OrderBusiness.SetPaymentTransaction(order, paymentDetail, transactionStatus, null, date);
                    Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Paid, string.Format(orderHistory, order.ConfirmationId), order.Id, (JbUserService.IsUserAuthenticated()) ? userId : -1);


                    Ioc.OrderBusiness.Update(order);

                }

                //add for update invoice transactions
                else
                {
                    var tranDb = Ioc.TransactionActivityBusiness;
                    PaymentDetail paymentDetail = new PaymentDetail(stripeCharge.ReceiptEmail, paymentStaus, stripeCharge.Id, paymentMethod, transactions.FirstOrDefault().Club.Currency, ipn, string.Empty, stripeCharge.BalanceTransactionId, RoleCategoryType.Parent, creditcardId);
                    var result = tranDb.UpdateTransactions(transactions, paymentDetail);
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return ex.Message;
            }
        }

        public void DeleteCustomer(string customerId)
        {
            try
            {
                var customerService = new StripeCustomerService(WebConfigHelper.Stripe_SecretKey);

                customerService.Delete(customerId);

            }
            catch
            {
                throw;

            }
        }
        public StripeDeleted DeleteCard(string customerId, string cardId)
        {
            try
            {
                var CardService = new StripeCardService(WebConfigHelper.Stripe_SecretKey);
                return CardService.Delete(customerId, cardId);
            }
            catch
            {
                throw;

            }
        }

        public StripeCard GetCard(string customerId, string cardId)
        {

            var cardService = new StripeCardService(WebConfigHelper.Stripe_SecretKey);
            StripeCard stripeCard = cardService.Get(customerId, cardId);  ////Get Card

            return stripeCard;
        }

        //add method for update invoice
        public string SucceedInvoicePayment(ControllerContext controllerContext, List<TransactionActivity> transactions, StripeCharge stripeCharge, string ipn, int? creditcardId, bool backToDetail = false)
        {
            try
            {
                var club = transactions.First().Club;

                var tranDb = Ioc.TransactionActivityBusiness;
                if (transactions == null || transactions.Count == 0)
                {
                    return "Invalid Token";
                }
                var paidAmount = transactions.Sum(c => c.Amount);
                var orderItem = Ioc.OrderItemBusiness.GetItem(transactions.FirstOrDefault().OrderItemId.Value);
                var invoiceCategory = transactions.Select(t => t.TransactionCategory).First();

                if (Ioc.TransactionActivityBusiness.IsPayKeyExist(stripeCharge.Id))
                {
                    return string.Empty;
                }

                PaymentDetail paymentDetail = new PaymentDetail(stripeCharge.ReceiptEmail, PaymentDetailStatus.COMPLETED, stripeCharge.Id, PaymentMethod.Card, transactions.FirstOrDefault().Club.Currency, ipn, string.Empty, stripeCharge.BalanceTransactionId, JbUserService.GetCurrentUserRoleType(), creditcardId);

                var date = DateTime.UtcNow;

                var invoice = transactions.First().PaymentDetail.invoice;
                var userId = invoice.UserId;
                invoice.PaidAmount = paidAmount;
                invoice.Status = InvoiceStatus.Paid;
                Ioc.InvoiceBusiness.Update(invoice);

                foreach (var transaction in transactions)
                {
                    transaction.PaymentDetail.Currency = paymentDetail.Currency;
                    transaction.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                    transaction.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                    transaction.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                    transaction.PaymentDetail.Status = paymentDetail.Status;
                    transaction.PaymentDetail.PayKey = paymentDetail.PayKey;
                    transaction.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                    transaction.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                    transaction.PaymentDetail.CreditCardId = paymentDetail.CreditCardId;
                    transaction.PaymentDetail.PayerType = paymentDetail.PayerType;
                    transaction.TransactionStatus = TransactionStatus.Success;
                    transaction.TransactionDate = date;
                    tranDb.Update(transaction);

                    if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                    {
                        var installment = Ioc.InstallmentBusiness.Get(transaction.InstallmentId.Value);
                        installment.PaidAmount = (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0) + transaction.Amount;
                        installment.PaidDate = date;
                        installment.Status = OrderStatusCategories.completed;
                        Ioc.InstallmentBusiness.Update(installment);

                    }

                }

                var invoiceHistoryDescription = Constants.Order_InvoiceHistory;
                var invoiceHistoryDescriptionPaided = Constants.Invoice_Paided;

                if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                {
                    invoiceHistoryDescription = Common.Constants.Payment.ParentInvoiceDescription;
                    invoiceHistoryDescriptionPaided = Common.Constants.Payment.ParentInvoiceDescriptionPaided;
                }

                foreach (var transaction in transactions)
                {
                    if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                    {
                        var orderItemInInvoice = transaction.OrderItem;
                        var instalment = transaction.Installment;
                        var InstalmentordeItamBalance = transaction.Amount;
                        orderItemInInvoice.PaidAmount += transaction.Amount;
                        Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                        Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(invoiceHistoryDescription, InstalmentordeItamBalance), orderItemInInvoice.Order_Id, userId, orderItemInInvoice.Id);

                    }
                    else
                    {
                        var orderItemInInvoice = transaction.OrderItem;
                        var ordeItamBalance = transaction.Amount;
                        orderItemInInvoice.PaidAmount += transaction.Amount;
                        Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                        Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(invoiceHistoryDescription, ordeItamBalance), orderItemInInvoice.Order_Id, userId, orderItemInInvoice.Id);
                    }

                }
                var invoiceId = invoice.Id;
                var userName = invoice.User.UserName;
                var today = DateTime.UtcNow;
                var clubTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, today);
                var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(invoiceHistoryDescriptionPaided, userName, CurrencyHelper.FormatCurrencyWithPenny(paidAmount, club.Currency)) };
                Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);

                var totalPaidAmount = transactions.Sum(c => c.Amount);

                EmailService.Get(controllerContext).SendReceivedFamilyInvoicePaymentEmail(transactions, totalPaidAmount, DateTime.UtcNow, paymentDetail, invoice);

                return string.Empty;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);
                return ex.Message;

            }

        }

        public string SucceedTakePayment(ControllerContext controllerContext, List<TransactionActivity> transactions, StripeCharge charge, string ipn, int? creditCardId, decimal? payableAmount, bool isFamilyTakePayment = false)
        {
            if (transactions != null && transactions.Count > 0)
            {
                var errorMsg = string.Empty;

                if (Ioc.TransactionActivityBusiness.IsPayKeyExist(charge.Id))
                {
                    return string.Empty;
                }
                CurrencyCodes currency = CurrencyCodes.USD;
                Enum.TryParse(charge.Currency, out currency);
                PaymentDetail paymentDetail = new PaymentDetail(username, PaymentDetailStatus.COMPLETED, charge.Id, PaymentMethod.Card, transactions.First().Club.Currency, ipn, string.Empty, charge.BalanceTransactionId, JbUserService.GetCurrentUserRoleType(), creditCardId);

                var orderItem = transactions.First().OrderItem;
                var orderItems = transactions.Select(t => t.OrderItem).GroupBy(i => i.Id).ToList();

                decimal totalPaidAmount = payableAmount.HasValue ? payableAmount.Value : transactions.Sum(c => c.Amount);


                var selectedInstallment = new List<OrderInstallment>();

                foreach (var tran in transactions)
                {
                    if (tran.InstallmentId.HasValue && tran.InstallmentId.Value > 0)
                    {
                        selectedInstallment.Add(Ioc.InstallmentBusiness.Get(tran.InstallmentId.Value));
                    }
                }
                OperationStatus result = null;
                if (isFamilyTakePayment == false)
                {
                    result = OrderServices.Get().ReceivedPayment(orderItem.Id, totalPaidAmount, paymentDetail, transactions.First().Note, transactions.First().TransactionDate, transactions.First().CheckId, selectedInstallment, transactions, HandleMode.Online, false);
                    if (result.Status)
                    {
                        EmailService.Get(controllerContext).SendReceivedPaymentEmail(orderItem, totalPaidAmount, transactions.First().TransactionDate, paymentDetail);
                    }
                    return result.Status == false ? result.ExceptionMessage : errorMsg;
                }
                else
                {
                    var listOderItems = new List<OrderItem>();

                    listOderItems.AddRange(orderItems.Select(o => o.FirstOrDefault()));

                    result = OrderServices.Get().ReceivedPaymentFromMultiOrder(listOderItems, null, totalPaidAmount, paymentDetail, transactions.First().Note, transactions.First().TransactionDate, transactions.First().CheckId, selectedInstallment, transactions);

                    if (result.Status)
                    {
                        EmailService.Get(controllerContext).SendReceivedFromCreditCardFamilyTakePaymentEmail(transactions, transactions.First().TransactionDate, paymentDetail, transactions.First().Order.UserId, transactions.First().ClubId);
                    }
                    return result.Status == false ? result.ExceptionMessage : errorMsg;
                }


            }

            return "No Element Found";
        }

        public UserCreditCard CreateCreditCardjustdb(PaymentModels source, int userId, PostalAddress Address, string customerId, string customerResponse, bool isDefault)
        {

            if (customerId == "")
            {
                isDefault = false;
            }

            var creditcard = new UserCreditCard()
            {
                ClubId = JbUserService.GetCurrentClubId(),
                Brand = source.Brand,
                ExpiryMonth = source.Month.Length == 1 ? ("0" + source.Month) : source.Month,
                ExpiryYear = source.Year,
                UserId = userId,
                LastDigits = source.CardNumber.Substring((source.CardNumber.Length) - 4, 4),
                Name = source.CardHolderName,
                CustomerId = customerId,
                CardId = source.CardId,
                CustomerResponse = customerResponse,
                IsDefault = isDefault,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = null,
                DefaultDate = null,
                DeletedDate = null,
                TypePaymentGateway = PaymentGateway.Stripe,
                TypeCreditCard = UserCreditCardType.OnlineHistory

            };

            creditcard.Address = new PostalAddress
            {
                Country = Address.Country,
                State = Address.State,
                City = Address.City,
                Zip = Address.Zip,
                Street = Address.State,
                Address = Address.Address,
                Lng = Address.Lng,
                Lat = Address.Lat,
                Route = Address.Route,
                TimeZone = Address.TimeZone,
                StreetNumber = Address.StreetNumber,
                County = Address.County,
                AutoCompletedAddress = Address.AutoCompletedAddress
            };

            Ioc.UserCreditCardBusiness.Create(creditcard);

            return creditcard;
        }

        private PaymentResponseModel SuccessPayment(PayViewModel model, StripeCharge stripeCharge, decimal payableAmount, ControllerContext controllerContext, UserCreditCard selectedCreditCard)
        {
            int? creditCardId;
            creditCardId = creditCard != null ? creditCardId = creditCard.Id : selectedCreditCard != null ? creditCardId = selectedCreditCard.Id : null;
            switch (model.ActionCaller)
            {
                case JumbulaSubSystem.Installment:
                    var date = DateTime.UtcNow;
                    resultResponse.errorMsg = UpdateInstallmentBasedOnPayment(installments, stripeCharge, ipn, creditCardId, date, JbUserService.GetCurrentUserRoleType(), payableAmount);


                    if (string.IsNullOrEmpty(resultResponse.errorMsg))
                    {
                        EmailService.Get(controllerContext).SendInstallmentPaymentConfirmation(installments.First().Token, date, false);

                    }
                    return resultResponse;
                case JumbulaSubSystem.Order:

                    resultResponse.errorMsg = UpdateOrderBasedOnPayment(controllerContext, order, stripeCharge, ipn, creditCardId);

                    return resultResponse;// if select express payment method 

                case JumbulaSubSystem.Invoice:

                    if (!string.IsNullOrEmpty(model.Token))
                    {
                        resultResponse.errorMsg = SucceedInvoicePayment(controllerContext, transactions, stripeCharge, ipn, creditCardId, model.BackToDetail);
                        var invoiceId = transactions.First().PaymentDetail.InvoiceId;

                    }
                    else
                    {
                        if (model.PaymentMethod == PaymentMethod.Card)
                        {
                            resultResponse.errorMsg = SucceedInvoicePayment(controllerContext, transactions, stripeCharge, ipn, creditCardId);
                        }
                        else
                        {

                            resultResponse.errorMsg = SucceedInvoicePayment(controllerContext, transactions, stripeCharge, ipn, creditCardId);
                        }
                    }

                    return resultResponse;

                case JumbulaSubSystem.MakeaPayment:
                    try
                    {
                        resultResponse.errorMsg = SucceedTakePayment(controllerContext, transactions, stripeCharge, ipn, creditCardId, model.PayableAmount, model.IsFamilyTakePayment);

                        if (!string.IsNullOrEmpty(resultResponse.errorMsg))
                        {
                            resultResponse.errorMsg = "You have successfully charged the user, but the balance may not reflect the correct amount. Do not charge the user again. Please contact Jumbula at support@jumbula.com and report this issue.";

                        }
                        return resultResponse;
                    }
                    catch (Exception ex)
                    {
                        resultResponse.errorMsg = "You have successfully charged the user, but the balance may not reflect the correct amount. Do not charge the user again. Please contact Jumbula at support@jumbula.com and report this issue.";

                        if (ex.InnerException == null)
                        {
                            resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);
                        }
                        else
                        {
                            resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.InnerException.Message);
                        }
                        if (WebConfigHelper.IsProductionMode)
                        {
                            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
                            string emailSubject = string.Concat("Error in TakePayment update", " ", club.Name);

                            string emailBody = ViewHelper.RenderViewToString(controllerContext, @"~/Views/Shared/Email/ErrorInTakePayment.cshtml", null);
                            Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, emailBody, true);
                        }
                        return resultResponse;
                    }


            }

            throw new NotSupportedException("This type of payment don't supported.");
        }

        public RefundResultModel Refund(TransactionActivity transactionActivity, string clubDomain, decimal refundAmount, bool isTestMode, Dictionary<string, string> metaData)
        {
            var result = new RefundResultModel();
            StripeRefund response = null;

            try
            {
                result.RemainAmount = refundAmount;
                var stripe = new JbStripeService(clubDomain, isTestMode);

                response = stripe.Refund(transactionActivity.PaymentDetail.PayKey, refundAmount, metadata);
                result.Response = JsonConvert.SerializeObject(response);

                result.Status = true;
                result.TransactionId = response.Id;
                result.RefundAmount = refundAmount;

            }
            catch (StripeException ex)
            {
                result.Status = false;
                result.ErrorMessage = string.IsNullOrWhiteSpace(ex.StripeError.Error) ? ex.Message : ex.StripeError.Error;
                if (response != null) result.TransactionId = response.Id;
                Log.Error(ex, "Refund is failed {TransactionId}", result.TransactionId);
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.ErrorMessage = FailProcessRefund;
                if (response != null) result.TransactionId = response.Id;
                Log.Error(ex, "Refund is failed {TransactionId}", result.TransactionId);
            }

            return result;
        }

        public PaymentDetailModel GetPaymentDetails(string clubDomain, string payKey, IEnumerable<TransactionActivity> oldRefunded, bool isPaypalTransaction, bool isTestMode)
        {
            var stripe = new JbStripeService(clubDomain, isTestMode);

            var payDetail = stripe.paymentDetails(payKey);

            if (payDetail != null)
            {
                return new PaymentDetailModel
                {
                    Amount = payDetail.Amount > 0 ? payDetail.Amount / 100 : payDetail.Amount,
                    AmountRefunded = payDetail.AmountRefunded > 0 ? payDetail.AmountRefunded / 100 : payDetail.AmountRefunded,
                    Status = payDetail.Status == "succeeded"
                };
            }

            return new PaymentDetailModel
            {
                Amount = 0,
                AmountRefunded = 0,
                Status = false
            };
        }
    }
}