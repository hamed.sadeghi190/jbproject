﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('DailyDismissalReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};
        var isBegin = false;

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.printMode = false;

        $http.get(baseUrl + '/GetInstructorListReportDetail').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            dataInfo = data;
            //manipulateMemberListGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $http.get(baseUrl + '/GetAllProgramsDailyDismissalReport', { params: { seasonDomain: $scope.seasonName } }).success(function (data, status, headers, config) {
            $scope.programs = data;
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.getProgramSessions = function(programId) {
            $http.get(baseUrl + '/GetSessionsDailyDismissalReport', { params: { programId: programId } }).success(function (data, status, headers, config) {
                $scope.sessions = [];
                $scope.sessions = data;
                $scope.Model.sessionId = data[data.length -1].Value;
            }).error(function (data, status, headers, config) {
                jbToast.error(data);
            });
        }

        var prepareGrid = function () {

            isBegin = true;

            $scope.DailyDismissalDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetDailyDismissalReport",
                        dataType: "json",
                        type: "POST",
                        data: function () {
                            return {
                                programId: $scope.Model.programId,
                                sessionId: $scope.Model.sessionId,
                                printMode: $scope.Model.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.DailyDismissalGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Daily dismissal).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Daily dismissal - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.DailyDismissalDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#DailyDismissalGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [
                    {
                        field: "IsPickedUp",
                        title: "Status",
                        width: "50px",
                    },
                    {
                        field: "StudentName",
                        title: "Participant",
                        width: "190px"
                    }, {
                        field: "DismissalFromEnrichment",
                        title: "Dismissal",
                        width: "120px"
                    }, {
                        field: "PickupTime",
                        title: "Dismissal time",
                        width: "120px"
                    }, {
                        field: "PickupName",
                        title: "Picked up by",
                        width: "120px"
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $scope.Model.printMode = false;
            $("#DailyDismissalGrid").getKendoGrid().saveAsExcel();

        }
        $scope.saveToPDF = function () {
            $scope.Model.printMode = true;
            createPDF();

        }
        $scope.print = function () {
            $scope.Model.printMode = true;
            createPDF();
        }

        $scope.search = function () {
            if (!isBegin) {
                prepareGrid();
            } else {
                $scope.Model.printMode = false;
                $("#DailyDismissalGrid").data("kendoGrid").dataSource.read();
            }

        };

        $scope.clearFilters = function () {
            $scope.Model.programId = 0;
            $scope.Model.sessionId = 0;
            $scope.filter.printMode = false;
            $("#DailyDismissalGrid").data("kendoGrid").dataSource.read();
        }

        function createPDF() {
            $http.post(baseUrl + '/GetDailyDismissalReport', {
                programId: $scope.Model.programId,
                sessionId: $scope.Model.sessionId,
                printMode: $scope.Model.printMode
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/pdf" });

                    var fileURL = URL.createObjectURL(blob);
                    a.href = fileURL;
                    a.download = "DailyDismissalReport.pdf";
                    a.click();
                });
        }
    }
    ]);

})();