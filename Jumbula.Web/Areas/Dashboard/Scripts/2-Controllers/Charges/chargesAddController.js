﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('chargesAddController', ['$scope', '$http', '$stateParams', '$state', 'jbConfirmation', '$sce', function ($scope, $http, $stateParams, $state, jbConfirmation, $sce) {
        window.app.globalObjects.pageClass = 'season-setup';
        window.app.globalObjects.initializeView();
        $scope.baseUrl = window.location.origin + "/Dashboard/Season";

        $scope.MC.openBlade({ url: "Dashboard/Blade/Charges", order: 2, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;

    }])
})();