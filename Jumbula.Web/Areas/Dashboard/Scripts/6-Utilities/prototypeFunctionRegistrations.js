﻿(function () {
    'use strict';

    angular.module('dashboardApp').factory("prototypeFunctionRegistrations", [function () {

        Array.prototype.removeByValue = function () {
            var what, a = arguments, l = a.length, ax;
            while (l && this.length) {
                what = a[--l];
                while ((ax = this.indexOf(what)) !== -1) {
                    this.splice(ax, 1);
                }
            }
            return this;
        };

        Array.prototype.removeByIndex = function (index, shift) {
            this.splice(index, shift ? shift : 1);
            return this;
        };

        Array.prototype.removeByPropertyValue = function (propertyName, value, shift) {
            for (var object in this) {

                var keyNames = Object.keys(this[object]);
                for (var i in keyNames) {
                    if (keyNames[i] == propertyName && this[object][propertyName] == value)
                        this.splice(object, shift ? shift : 1);
                }
            }
            return this;
        };

        Array.prototype.findByPropertyValue = function (propertyName, value, returnValueName, rturnObject) {
            for (var object in this) {
                var keyNames = Object.keys(this[object]);
                for (var i in keyNames) {
                    if (keyNames[i] == propertyName && this[object][propertyName] == value)
                        if (rturnObject) {
                            return this[object];
                        } else {
                            return this[object][returnValueName];
                        }
                }
            }
            return this;
        };

        Array.prototype.moveByIndex = function (oldIndex, newIndex) {
            if (newIndex >= this.length) {
                var k = newIndex - this.length;
                while ((k--) + 1) {
                    this.push(undefined);
                }
            }
            this.splice(newIndex, 0, this.splice(oldIndex, 1)[0]);
            return this;
        };

        Number.prototype.toDayOfWeek = function () {
            switch (this) {
                case 0:
                    {
                        return 'Sunday';
                    }
                case 1:
                    {
                        return 'Monday';
                    }
                case 2:
                    {
                        return 'Tuesday';
                    }
                case 3:
                    {
                        return 'Wednesday';
                    }
                case 4:
                    {
                        return 'Thursday';
                    }
                case 5:
                    {
                        return 'Friday';
                    }
                case 6:
                    {
                        return 'Saturday';
                    }
            }
        }

        Number.prototype.toDayOfWeekAbbreviation = function () {
            switch (this) {
                case 0:
                    {
                        return 'Sun';
                    }
                case 1:
                    {
                        return 'Mon';
                    }
                case 2:
                    {
                        return 'Tue';
                    }
                case 3:
                    {
                        return 'Wed';
                    }
                case 4:
                    {
                        return 'Thu';
                    }
                case 5:
                    {
                        return 'Fri';
                    }
                case 6:
                    {
                        return 'Sat';
                    }
            }
        }

        var formatCurrencyWithPenny = function (n, x) {
            var v = this,
                notValidErrorMessage = 'Input is not valid. expected types: string(e.g. \'$-21.15\') and number(e.g. 58)';
            if (typeof v === 'string') {
                v = v.replace(/[^0-9.-]/g, '');
                if (v === '') {
                    console.error(notValidErrorMessage)
                }
                v = Number(v);
                if (v === NaN) {
                    console.error(notValidErrorMessage)
            }
            } else if (typeof v !== 'number') {
                console.error(notValidErrorMessage)
            }

            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : symbol) + ')';
            if (v >= 0) {
                return symbol + v.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
            }
            else
            {
                v = v * -1;
                return '-' + symbol + v.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
            }
        };

        var toOrdinal = function () {
            var s = ["th", "st", "nd", "rd"],
                v = this,
                notValidErrorMessage = 'Input is not valid. expected types: string(e.g. \'21\') and number(e.g. 58)';
            if (typeof v === 'string') {
                v = v.replace(/[^0-9]/g, '');
                if (v === '') {
                    console.error(notValidErrorMessage)
                }
                v = Number(v);
                if (v === NaN) {
                    console.error(notValidErrorMessage)
                }
            } else if (typeof v !== 'number') {
                console.error(notValidErrorMessage)
            }
                v = this % 100;

            return this + (s[(v - 20) % 10] || s[v] || s[0]);
        }
        String.prototype.toOrdinal = toOrdinal;
        Number.prototype.toOrdinal = toOrdinal;
        String.prototype.formatCurrencyWithPenny = formatCurrencyWithPenny;
        Number.prototype.formatCurrencyWithPenny = formatCurrencyWithPenny;
        String.prototype.format = function () {
            var args = arguments;
            return this.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined'
                  ? args[number]
                  : match
                ;
            });
        };
        Date.prototype.defaultDateformat = function () {
            return new Date(this).format('mm/dd/yyyy');
        };
        //Date.prototype.defaultDateTimeformat = function () {
        //    return new Date(this).format('mm/dd/yyyy, HH:MM');
        //};
        String.prototype.defaultDateTimeformat = function () {
            return new Date(this).format('mm/dd/yyyy, HH:MM');
        };
        Date.prototype.defaultDateFormatWith3CharMonth = function () {
            return new Date(this).format('mmm dd, yyyy');
        };
        return "Prototypes registered.";
    }]);
})();