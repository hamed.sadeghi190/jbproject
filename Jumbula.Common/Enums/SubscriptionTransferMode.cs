﻿using System.ComponentModel;


namespace Jumbula.Common.Enums
{
    public enum SubscriptionTransferMode
    {
        [Description("TransferOut")]
        Out = 0,
        [Description("TransferIn")]
        In = 1,
        [Description("Both")]
        Both = 2,
    }
}
