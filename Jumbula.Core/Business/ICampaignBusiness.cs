﻿using Jb.Framework.Common;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Campaign;
using System;
using System.Collections.Generic;
using System.Linq;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ICampaignBusiness
    {
        Campaign Get(int campaignId);
        IQueryable<Campaign> GetAllCampaigns(int clubId);
        OperationStatus SaveCampaignData(Campaign campaign);
        OperationStatus UpdateCampaign(Campaign campaign);
        OperationStatus UpdateCampaign(Campaign campaign, List<CampaignRecipients> recipients);
        OperationStatus UpdateCampaignRecipientsEvents(List<CampaignRecipients> recipients);
        int GetCampaignRecipientsNum(int campaignId);
        bool IsClicked(int campaignId, int recipientId);
        bool IsOpened(int campaignId, int recipientId);
        DateTime GetOpenDate(int campaignId, int recipientId);
        DateTime GetClickDate(int campaignId, int recipientId);
        CampaignRatesModel GetCampaignRates(int campaignId);
        double GetCampaignClickRate(int campaignId);
        double GetCampaignOpenRate(int campaignId);
        double GetCampaignEmailMarkAsSpam(int campaignId);
        List<object> GetOpenedClickedRateInRang(int campaignId);
        IQueryable<CampaignRecipients> GetCampaignRecipients(int campaignId);
        OperationStatus DeleteCampaign(int campaignId);
        OperationStatus DeleteCampaignRecipient(int campId, int recipientId);
        OperationStatus AddNewCampaignRecipient(int campId, string email, string firstName, string lastName, RecipeientType type);
        OperationStatus AddNewCampaignRecipients(int campaignId, List<CampaignRecipients> newRecipients);
        OperationStatus MakeCampaignOnReport(IEnumerable<ReportRecipient> recipients, string clubDomain,
            string campaignName);
        OperationStatus MakeCampaignOnReport(IEnumerable<string> emails, string clubDomain, string campaignName);
        OperationStatus MakeCampaignOnLottery(List<long> orderItemIds, string clubDomain);
        OperationStatus DeleteCcRecipients(List<CampaignRecipients> deletedRecipients);
        ClassConfirmationCampaignModel GetClassConfirmationModel(int clubId);
        PaginatedList<CampaignClassConfirmationItemModel> GetClassConfirmationItems(ClassConfirmationCampaignModel model, int partnerId, int pageIndex, int pageSize);
        PaginatedList<CampaignClassConfirmationDetailItemModel> GetClassConfirmationDetails(long programId, int pageIndex, int pageSize);
        OperationStatus MakeCampaignForConfirmationClass(ClassConfirmationCampaignModel model, int partnerId);
    }
}
