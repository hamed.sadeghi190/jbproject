﻿namespace Jumbula.Core.Model.Lottery
{
    public class LotteryDetailsViewModel
    {
        public string ProgramName { get; set; }
        public int NumberOfParticipants { get; set; }
        public int TotalCapacity { get; set; }
        public bool IsLotteryOpenForAdmin { get; set; }
        public bool EnableDraw { get; set; }
        public bool EnableFinalize { get; set; }
        public string DrawDescription { get; set; }
        public string FinalizeDescription { get; set; }
    }
}
