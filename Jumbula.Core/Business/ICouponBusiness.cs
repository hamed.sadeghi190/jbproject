﻿using Jb.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ICouponBusiness
    {
        Coupon Get(int id);
        List<int> GetListId(int clubId, string couponCode, bool isTestMode);
        IQueryable<Coupon> GetList();
        IQueryable<Coupon> GetList(long seasonId);
        IQueryable<Coupon> GetList(long seasonId, bool isDeleted);
        IQueryable<Coupon> GetList(List<long> seasonIds);
        IQueryable<Coupon> GetList(List<long> seasonIds, bool isDeleted);
        List<Coupon> GetProgramCoupons(long programId);
        bool IsExistCouponCode(long seasonId, string couponCode);
        bool IsExistCouponCode(long seasonId, string couponCode, int couponId);
        OperationStatus Create(Coupon coupon);
        OperationStatus Update(Coupon coupon);
        OperationStatus Delete(Coupon coupon);
        OperationStatus Delete(int couponId);
        IEnumerable<Coupon> GetAllBy(string clubDomain, int pageSize, int currentPage, out int total);
        Coupon GetBy(int couponId);
        //bool IsExistCouponCode(string clubDomain, string code, int? id);
        void CalulateCoupon(Cart cart);
        void CalulateCoupon(Cart cart, List<int> couponsId);

        OperationStatus CopyInSeason(long couponId, long seasonId, int clubId, string newSeasonDomain, bool forwardDates, int? monthNum);
        OperationStatus CreateList(List<Coupon> coupons);
    }
}
