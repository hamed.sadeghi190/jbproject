﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsClub.Models
{
    public class OrderItemRegistrationFormViewModel
    {
        public long OrderItemId { get; set; }
        public bool Editable { get; set; }
        public JbForm Form { get; set; }
    }
    
}