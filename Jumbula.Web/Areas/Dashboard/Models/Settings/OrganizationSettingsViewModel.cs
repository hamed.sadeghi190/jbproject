﻿using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{
    public class OrganizationSettingsViewModel
    {

        public OrganizationSettingsViewModel()
        {
            OrganizationTimeZone = DropdownHelpers.SeedTimeZones();
            OrganizationTimeZone.RemoveAt(0);
            OrganizationBusinessTypes = SeedBusinessTypes();
            SelectedBusinessType = "0";
            IsNotificationEmailsSet = false;
            AllStaffTitles = new List<SelectKeyValue<string>>();
        }


        [Required(ErrorMessage = "Organization name is required.")]
        public string Name { get; set; }
        public string Domain { get; set; }

        [Url(ErrorMessage = "Website is not a valid fully-qualified http or https URL.")]
        [DataType(DataType.Url)]
        [Display(Name = "Website")]
        public string Site { get; set; }
        [Required(ErrorMessage = "Address is required.")]
        public string Address { get; set; }

        public string SelectedTimeZone { get; set; }
        public List<SelectKeyValue<string>> OrganizationTimeZone { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Last name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete last name.")]
        public string LastName { get; set; }

        [EmailAddress(ErrorMessage = "Email is not a valid email address.")]
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is required")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string Email { get; set; }

        [Display(Name = "Title")]
        public StaffTitle? Title { get; set; }

        public List<SelectKeyValue<string>> AllStaffTitles { get; set; }

        public List<SelectKeyValue<int>> ActiveStaffs { get; set; }

        //[Required(ErrorMessage = "Staff member is required")]
        public int SelectedStaffResponder { get; set; }

        [Display(Name = "Phone number")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$",
            ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string Phone { get; set; }

        public string ExtensionPhone { get; set; }
        public string Logo { get; set; }

        public string SelectedBusinessType { get; set; }
        public List<SelectKeyValue<string>> OrganizationBusinessTypes { get; set; }

        [Display(Name = "Legal name")]
        public string LegalName { get; set; }

        public string TaxIDFEIN { get; set; }
        public string TaxIDSSN { get; set; }

        #region Change Password
        public string Username { get; set; }
        [Display(Name = "Current password")]
        [Required(ErrorMessage = "{0} is required.")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }


        [Display(Name = "New password")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "Confirm password")]
        [Required(ErrorMessage = "{0} is required.")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        #endregion

        #region notificationEmail

        public bool IsNotificationEmailsSet { get; set; }
        public List<SelectKeyValue<string>> AllCCEmails { get; set; }

        public List<string> SelectedCCEmails { get; set; }
        #endregion

        #region Onsite coordinator
        public string OnSiteCoordinatorName { get; set; }

        [EmailAddress(ErrorMessage = "Email is not a valid email address.")]
        public string OnSiteCoordinatorEmail { get; set; }

        #endregion
        List<SelectKeyValue<string>> SeedBusinessTypes()
        {
            var result = new List<SelectKeyValue<string>>();

            foreach (BusinessType type in Enum.GetValues(typeof(BusinessType)))
            {
                result.Add(new SelectKeyValue<string>() { Text = type.ToDescription(), Value = ((int)type).ToString() });
            }

            result = result.OrderBy(o => o.Text).ToList();
            result.Insert(0, new SelectKeyValue<string>() { Text = "Select business type", Value = "0" });

            return result;
        }
    }

    public enum OrganizationSettingsSection
    {
        Basic = 0,
        Logo = 1,
        Formation = 2,
        Contact = 3,
        ChangePass = 4,
        NotificationEmail = 5,
        StaffResponder = 6,
        OnSiteCoordinator = 7
    }


}