﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Core.Data
{
    public interface IEmailRepository : IRepository<Email>
    {
        IQueryable<Email> GetPendingEmails(EmailCategory? emailCategory = null);
        OperationStatus UpdateEmailRecipientEvents(IEnumerable<EmailRecipient> recipients);
    }
}
