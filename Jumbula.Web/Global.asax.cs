﻿using Elmah;
using FluentValidation.Mvc;
using Jb.Framework.Common.Forms;
using Jb.Framework.Common.Forms.Validation;
using Jumbula.Common.Exceptions;
using Jumbula.Core.Domain;
using Jumbula.Web.Mvc.Factories;
using Jumbula.Web.Mvc.ModelBinder;
using System;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Serilog;

namespace Jumbula.Web
{
    public class MvcApplication : HttpApplication
    {
        public static byte[] DataManipulationSourceFile { get; set; }

        protected void Application_Start()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls;

            //add fluentvalidation
            FluentValidationModelValidatorProvider.Configure();

            ViewEngines.Engines.Clear();

            ViewEngines.Engines.Add(new RazorViewEngine());


            // CustomModelBinder to trim strings
            ModelBinders.Binders.DefaultBinder = new CustomModelBinder();

            ModelBinders.Binders.Add(typeof(ICustomElement), new CustomElementModelBinder());

            //add custom model binder for binding template elements(JB.Framework.Web)
            ModelBinders.Binders.Add(typeof(JbBaseElementViewModel), new JbElementModelBinder());
            ModelBinders.Binders.Add(typeof(JbBaseElement), new JbElementModelBinder());
            ModelBinders.Binders.Add(typeof(IJbBaseElement), new JbElementModelBinder());
            ModelBinders.Binders.Add(typeof(BaseElementValidator), new JbElementValidatorModelBinder());

            ModelBinders.Binders.Add(typeof(JbPBaseElement), new JbPageBinder());

            // HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();
            AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configure(WebApiConfig.Register);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var config = GlobalConfiguration.Configuration;
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Not sure if needed or how they are used
            InitializeJbJsonProviderFactory();
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var stackTrace = new StackTrace();
            var stackFrame = stackTrace.GetFrame(0);

            Log.Error(Server.GetLastError(), "Grabbed in {GrabPoint}", stackFrame.GetMethod().Name);
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {

            Response.ClearHeaders();
            Response.BufferOutput = true;

            string redirectUrl = string.Empty;

            if (HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains("www."))
            {
                redirectUrl = HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Replace("www.", "");
            }

            if (HttpContext.Current.Request.IsSecureConnection.Equals(false) && HttpContext.Current.Request.IsLocal.Equals(false))
            {
                if (!string.IsNullOrEmpty(redirectUrl))
                {
                    redirectUrl = "https://" + Request.ServerVariables["HTTP_HOST"] + HttpContext.Current.Request.RawUrl;
                }
            }

            if (!string.IsNullOrEmpty(redirectUrl))
            {
                Response.RedirectPermanent(redirectUrl);
            }
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
        }

        public void ErrorMail_Filtering(object sender, ExceptionFilterEventArgs e)
        {
            var exception = e.Exception;
            if (exception.GetType() == typeof(JumbulaResourceNotFoundException))
            {
                e.Dismiss();
            }

        }

        public void ErrorLog_Filtering(object sender, ExceptionFilterEventArgs e)
        {
            var httpException = e.Exception;
            if (httpException.GetType() == typeof(JumbulaResourceNotFoundException))
            {
                e.Dismiss();
            }
            else if (httpException.ToString().Contains("parameter 'eventType' of non-nullable type 'SportsClub.Models.TimeLineFilter'"))
            {
                e.Dismiss();
            }
        }

        private void InitializeJbJsonProviderFactory()
        {
            JsonValueProviderFactory jsonValueProviderFactory = null;

            foreach (var factory in ValueProviderFactories.Factories)
            {
                if (factory is JsonValueProviderFactory providerFactory)
                {
                    jsonValueProviderFactory = providerFactory;
                }
            }

            //remove the default JsonVAlueProviderFactory
            if (jsonValueProviderFactory != null) ValueProviderFactories.Factories.Remove(jsonValueProviderFactory);

            //add the custom one
            ValueProviderFactories.Factories.Add(new JbJsonValueProviderFactory());

        }
    }
}