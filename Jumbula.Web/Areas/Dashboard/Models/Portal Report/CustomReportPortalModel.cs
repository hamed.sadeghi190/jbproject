﻿using System;
using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CustomReportPortalModel
    {
        public CustomReportPortalModel()
        {
            // do some things good.
        }

        public List<long> _SelectedPrograms { get; set; }
        public List<long> _SelectedClubs { get; set; }
        public DateTime DateProgram { get; set; }
        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public SeasonNames SeasonName { get; set; }
        public int Year { get; set; }

        public bool IsAllPrograms { get; set; }

        public string Term { get; set; }
        public string AutomaticTerm { get; set; }
        public string ClubType { get; set; }
        public string IsAllClubs { get; set; }

        public IncludeForms IncludeForms { get; set; }

        public ReportFilters Filters { get; set; }
    }

}