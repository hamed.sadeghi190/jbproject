﻿using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Mvc.WebConfig;


namespace SportsClub.Areas.Dashboard.Models
{
    public class PaymentSettingsViewModel
    {

        public PaymentSettingsViewModel()
        {
            PaymentMethods = SeedMakePaymentMethods();
            SelectedPaymentMethod = "0";
        }

        public int UserId { get; set; }
        [MaxLength(32, ErrorMessage = "The maximum length for checkout label is 32 characters")]
        public string StripePaymentLabel { get; set; }
        [MaxLength(32, ErrorMessage = "The maximum length for checkout label is 32 characters")]
        public string PaypalPaymentLabel { get; set; }
        [MaxLength(32, ErrorMessage = "The maximum length for checkout label is 32 characters")]
        public string CashPaymentLabel { get; set; }
        [MaxLength(32, ErrorMessage = "The maximum length for checkout label is 32 characters")]
        public string BankTransferPaymentLabel { get; set; }

        public bool EnablePayByCash { get; set; }
        public bool EnableManualPayment { get; set; }
        [MaxLength(35, ErrorMessage = "The maximum length for checkout label is 32 characters")]
        public string ManualPaymentLabel { get; set; }
        public string ManualPaymentMessage { get; set; }
        public string ManualPaymentInstruction { get; set; }
        [EmailAddress(ErrorMessage = "Paypal email is not a valid email address.")]
        [DataType(DataType.EmailAddress)]
        public string PaypalEmail { get; set; }
        public string ReturnUrl { get; set; }
        public string StripeClientId
        {
            get
            {
                return IsCreditCardTestMode ? WebConfigHelper.Stripe_ClientId_Test : WebConfigHelper.Stripe_ClientId_Live;
            }
        }
        public bool IsCreditCardTestMode { get; set; }
        public string PayByCashMessage { get; set; }
        public string PayByCashInstruction { get; set; }

        public bool EnableBankTransfer { get; set; }
        public string BankTransferMessage { get; set; }
        public string BankTransferInstruction { get; set; }

        public string StripeCustomerId { get; set; }
        public string Token { get; set; }

        public bool EnableCreditCard { get; set; }

        public bool IsStripe { get; set; }
        public bool EnableAchPayment { get; set; }
        [MaxLength(32, ErrorMessage = "The maximum length for checkout label is 32 characters")]
        public string AchPaymentLabel { get; set; }
        public string AchPaymentMessage { get; set; }
        public string AchPaymentInstruction { get; set; }

        public bool EnablePaypalPayment { get; set; }
        public bool EnableStripePayment { get; set; }
        public bool EnableAuthorizePayment { get; set; }


        [Required(ErrorMessage = "{0} is required")]
        [DisplayName("Login ID")]
        public string LoginId { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [DisplayName("Transaction key")]
        public string TransactionKey { get; set; }
        public long ClientId { get; set; }
        public string SelectedPaymentMethod { get; set; }
        public List<SelectKeyValue<string>> PaymentMethods { get; set; }

        public CurrencyCodes Currency { get; set; }
        public List<SelectKeyValue<string>> Currencies
        {
            get
            {
                var result = new List<SelectKeyValue<string>>()
                {
                     new SelectKeyValue<string> { Value = CurrencyCodes.USD.ToString(), Text = "U.S. Dollar"  },
                     new SelectKeyValue<string> { Value = CurrencyCodes.CAD.ToString(), Text = "Canadian Dollar" },
                     new SelectKeyValue<string> { Value = CurrencyCodes.GBP.ToString(), Text = "United Kingdom Pound"  },
                     new SelectKeyValue<string> { Value = CurrencyCodes.AUD.ToString(), Text = "Australian Dollar"  },
                     new SelectKeyValue<string> { Value = CurrencyCodes.MYR.ToString(), Text = "Malaysia Ringgit"  },
                };

                return result;
            }
        }

        List<SelectKeyValue<string>> SeedMakePaymentMethods()
        {
            var result = new List<SelectKeyValue<string>>();

            result.Add(new SelectKeyValue<string>() { Text = PaymentMethod.Scholarship.ToDescription(), Value = ((int)PaymentMethod.Scholarship).ToString() });
            result.Add(new SelectKeyValue<string>() { Text = PaymentMethod.FinancialAid.ToDescription(), Value = ((int)PaymentMethod.FinancialAid).ToString() });
            result.Add(new SelectKeyValue<string>() { Text = PaymentMethod.Other.ToDescription(), Value = ((int)PaymentMethod.Other).ToString() });

            return result;
        }
    }


}