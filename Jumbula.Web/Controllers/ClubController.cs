﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.ClubsService;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.Identity.Owin;
using SportsClub.Models;
using SportsClub.Models.Home;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    public class ClubController : JbBaseController
    {

        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly IApplicationSignInManager _applicationSignInManager;
        private readonly IClientBusiness _clientBusiness;

        public ClubController(IApplicationUserManager<JbUser, int> applicationUserManager, IApplicationSignInManager applicationSignInManager, IClientBusiness clientBusiness)
        {
            _applicationUserManager = applicationUserManager;
            _applicationSignInManager = applicationSignInManager;
            _clientBusiness = clientBusiness;
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult CreateAccountStep1(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                PageNotFound();
            }
            if (!Ioc.VerificationBusiness.IsTokenValid(token))
            {
                PageNotFound();
            }
            var model = new CreateClubStep1ViewModel(token);
            return View(model);
        }
        [RequireHttps]
        [AllowAnonymous]
        public virtual JsonResult CheckIsUserNameExist(string userName)
        {
            if (!_applicationUserManager.IsUserExist(userName))
            {
                return Json(new { Status = "UserIsNotExist" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Status = "UserIsExist", Message = string.Format("Sorry, it looks like {0} belongs to an existing user.", userName) }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult CreateAccountStep1(CreateClubStep1ViewModel model)
        {

            if (!string.IsNullOrEmpty(model.Domain) && Ioc.ClubBusiness.IsClubDomainExist(model.Domain, 0))
            {
                ModelState.AddModelError("Domain", "Domain already exists.");
            }
            if (!string.IsNullOrEmpty(model.Password) && !(model.Password.Length >= 8 && model.Password.Any(c => char.IsUpper(c)) && model.Password.Any(c => char.IsLower(c)) && !model.Password.All(c => char.IsLetterOrDigit(c))))
            {
                ModelState.AddModelError("Password", "Password is invalid.");
            }
            if (!string.IsNullOrEmpty(model.Email) && _applicationUserManager.IsUserExist(model.Email))
            {
                ModelState.AddModelError("Email", string.Format("Sorry, it looks like {0} belongs to an existing user.", model.Email));
            }

            var verification = Ioc.VerificationBusiness.GetByToken(model.Token);

            if (verification == null)
            {
                PageNotFound();
            }

            if (ModelState.IsValid)
            {
                var clubAdmin = Ioc.UserProfileBusiness.Get(model.Email);
                if (clubAdmin == null || clubAdmin.Id < 1)
                {
                    clubAdmin = new JbUser() { UserName = model.Email, Email = model.Email };

                    var res = _applicationUserManager.Create(clubAdmin, model.Password);
                    _applicationUserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));
                    string token = _applicationUserManager.GenerateEmailConfirmationTokenAsync(clubAdmin.Id).Result;
                    _applicationUserManager.ConfirmEmail(clubAdmin.Id, token);
                }
                Club club = new Club();

                var contactPerson = new ContactPerson()
                {
                    Email = model.Email,
                    //Phone = verification.PhoneNumber,
                    Phone = model.Phone,
                    IsPrimary = true
                };
                club.ContactPersons.Add(contactPerson);
                var date = DateTime.UtcNow;
                club.MetaData = new MetaData()
                {
                    DateCreated = date,
                    DateUpdated = date
                };
                club.Currency = CurrencyCodes.USD;
                club.TimeZone = Jumbula.Common.Enums.TimeZone .PST;
                club.Client = new Client();
                var paymentMethod = new ClientPaymentMethod()
                {
                    Client = club.Client,
                    EnableCashPayment = false,
                    EnablePaypalPayment = false,
                    EnableStripePayment = false,
                    CreditCardPreferredLabel = "Express payment methods",
                };
                club.Client.PaymentMethods = paymentMethod;
                var priceplan = Ioc.PricePlanBusiness.GetFreeTrialPlan();
                club.Client.PricePlan = priceplan;
                club.Client.PricePlanId = priceplan.Id;
                club.Name = model.Name.Replace(Constants.S_DoubleQuote, string.Empty).Replace(Constants.S_BackSlash, string.Empty);
                club.Domain = model.Domain;
                club.Site = model.Site;
                club.TypeId = (int)ClubTypesEnum.SportClub;

                var result = Ioc.ClubBusiness.Create(club);

                if (result.Status)
                {
                    var contact = club.ContactPersons.FirstOrDefault();
                    club.ClubStaffs = new List<ClubStaff>();
                    var selectedRole = Ioc.UserProfileBusiness.GetRole(RoleCategory.Admin);
                    club.ClubStaffs.Add(new ClubStaff() { Status = StaffStatus.Active, ContactId = contact.Id, JbUserRole = new JbUserRole() { UserId = clubAdmin.Id, RoleId = selectedRole.Id } });

                    Ioc.ClubBusiness.Update(club);

                    verification.ClubDomain = club.Domain;
                    Ioc.VerificationBusiness.Update(verification);
                    return RedirectToAction("CreateAccountStep2", "Club", new { token = verification.Token });
                }


            }
            return View(model);
        }
        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult CreateAccountStep2(string token)
        {
            var verification = Ioc.VerificationBusiness.GetByToken(token);

            if (verification == null || string.IsNullOrEmpty(verification.ClubDomain))
            {
                PageNotFound();
            }

            var model = new CreateClubStep2ViewModel(token);
            return View(model);
        }
        [HttpPost]
        public virtual JsonResult IsClubDomainValid(string domain)
        {
            var status = false;
            if (!string.IsNullOrEmpty(domain) && domain.Length > 2)
            {
                status = !(Ioc.ClubBusiness.IsClubDomainExist(domain));
            }
            return Json(new { status = status });
        }
        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult CreateAccountStep2(CreateClubStep2ViewModel model)
        {
            var verification = Ioc.VerificationBusiness.GetByToken(model.Token);

            if (verification == null || string.IsNullOrEmpty(verification.ClubDomain))
            {
                PageNotFound();
            }
            var club = Ioc.ClubBusiness.Get(verification.ClubDomain);
            if (club == null)
            {
                PageNotFound();
            }
            var address = new PostalAddress();
            if (!string.IsNullOrEmpty(model.Address))
            {
                try
                {
                    GoogleMap.GetGeo(model.Address, ref address);
                }
                catch
                {
                    ModelState.AddModelError("Address", "Address is not valid.");
                }
            }

            if (string.IsNullOrEmpty(model.SelectedRevenue))
            {
                ModelState.AddModelError("SelectedRevenue", "Annual revenue is required.");
            }
            if (string.IsNullOrEmpty(model.SelectedHowDidYouHear))
            {
                ModelState.AddModelError("SelectedHowDidYouHear", "How did you hear about us?");
            }
            if (ModelState.IsValid)
            {

                //club.Site = model.Site;
                club.Address = address;
                club.ClubLocations = new List<ClubLocation>()
                {
                    new ClubLocation()
                    {
                        Name = string.Empty,
                        PostalAddress = address
                    }
                };

                club.TimeZone = model.TimeZone;
                var contactPerson = club.ContactPersons.First();
                contactPerson.FirstName = model.FirstName;
                contactPerson.LastName = model.LastName;

                club.ClubRevenueId = Convert.ToInt32(model.SelectedRevenue);

                var result = Ioc.ClubBusiness.Update(club);
                if (result.Status)
                {

                    try
                    {

                        var page = Ioc.PageBusiness.GetDefaultPage(club);

                        page.Header.LogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);

                        if (string.IsNullOrEmpty(page.Header.LogoUrl))
                        {
                            page.Header.LogoUrl = "~/Images/club-nosport.png";
                        }



                        page.SaveType = SaveType.Publish;
                        page.ClubId = club.Id;
                        Ioc.PageBusiness.CreateEdit(page);
                    }
                    catch (Exception ex)
                    {
                        LogHelper.LogException(ex);
                    }


                    FreeTrialViewModel emailmodel = new FreeTrialViewModel()
                    {
                        BizName = club.Name,
                        BizWeb = club.Domain,
                        Email = club.ContactPersons.First().Email,
                        FullName = string.Format(Constants.F_FullName, club.ContactPersons.First().FirstName, club.ContactPersons.First().LastName),
                        Revenue = club.ClubRevenue.Description,
                        TimeZone = club.TimeZone,
                        Phone = club.ContactPersons.First().Phone,
                        HowHear = model.SelectedHowDidYouHear,
                        ClubDomain = club.Domain,
                        WebSite = club.Site,
                    };

                    if (club.ClubLocations.Any())
                    {
                        emailmodel.Address = club.ClubLocations.FirstOrDefault().PostalAddress.AutoCompletedAddress;
                    }
                    EmailService.Get(this.ControllerContext).SendGretingEmail(emailmodel);

                }

                return Redirect(string.Format("{0}://{1}.{2}/Club/RedirectToDashboard?token={3}", Request.Url.Scheme, club.Domain, Request.Url.Host, model.Token));
            }
            return View(model);
        }
        [AllowAnonymous]
        [HttpGet]
        public virtual async Task<ActionResult> RedirectToDashboard(string token)
        {
            var verification = Ioc.VerificationBusiness.GetByToken(token);

            if (verification == null || string.IsNullOrEmpty(verification.ClubDomain))
            {
                PageNotFound();
            }
            var club = Ioc.ClubBusiness.Get(verification.ClubDomain);
            if (club == null)
            {
                PageNotFound();
            }


            var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);

            var jbuser = club.ClubStaffs.First().JbUserRole.User;
            CurrentUser userBaseInfo = new CurrentUser()
            {
                UserName = jbuser.UserName,
                Id = jbuser.Id,
                Role = EnumHelper.ParseEnum<RoleCategory>(club.ClubStaffs.First().JbUserRole.Role.Name),
            };


            await _applicationSignInManager.SignInAsync(jbuser, clubBaseInfo, userBaseInfo, false, false).ContinueWith(c =>
            {
                if (userBaseInfo.Role == RoleCategory.Admin)
                {
                    SetCookieForFreeTrialViewFirstTimeLogin(club);
                }

            });
            return Redirect(Constants.Jumbula_Dashbaord_Url);

        }

        public void SetCookieForFreeTrialViewFirstTimeLogin(Club club)
        {
            if (club.Client.PricePlan.Type == PricePlanType.FreeTrial)
            {
                Response.Cookies["FreeTrialViewFirstTimeLogin"].Value = "true";
            }
        }

        [ChildActionOnly]
        [AllowAnonymous]
        public virtual ActionResult JbNavbar(bool isDisplayPage = false, bool isSwitchedAsAdmin = false)
        {
            var clubId = this.GetCurrentClubId();
            var club = Ioc.ClubBusiness.Get(clubId);
            var clubDomain = club.Domain;

            var model = new NavBarViewModel();

            model.ShowLogo = true;
            model.LogoUrl = UrlHelpers.GetClubLogoUrl(clubDomain, club.Logo);
            model.LogoRedirecturl = club.Site;
            model.HeaderTitle = club.Name;

            if (club.Setting != null)
            {
                model.ShowLogo = club.Setting.ShowSchoolLogo;
            }

            if (this.IsUserAuthenticated() && this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)
            {
                model.IsAdminSwitchedAsParent = isSwitchedAsAdmin;

                if (isSwitchedAsAdmin)
                {
                    var currentUserId = JbUserService.GetSwitchedParentUserId(this.GetCurrentUserId());

                    var user = Ioc.UserProfileBusiness.Get(currentUserId);

                    model.SwitchedParentUserName = user.UserName;
                }
            }

            if (club.PartnerId.HasValue)
            {
                model.HasPartner = true;
                model.PartnerLogoUrl = UrlHelpers.GetClubLogoUrl(club.PartnerClub.Domain, club.PartnerClub.Logo);
                model.PartnerLogoRedirectUrl = club.PartnerClub.Site;
                model.IsSchool = club.IsSchool;
            }

            model.Clubs = GetMemeberClubs(clubDomain);
            model.IsClassPage = isDisplayPage;
            var order = Ioc.OrderBusiness.GetOrderInCart(this.GetCurrentUserId(), club.Id);

            if (order != null)
            {
                model.CountOrder = order.RegularOrderItems.Count() + order.PreregisterItems.Count() + order.LotteryItems.Count();
                model.HasPreregisteredOrder = order.PreregisterItems.Any();
                model.HasAddedPreregisteredOrder = order.RegularOrderItems.Any(o => o.PreRegistrationStatus == PreRegistrationStatus.Added);
                model.HasLotteryOrder = order.LotteryItems.Any();
                model.HasWonLotteryOrder = order.LotteryItems.Any(l => l.LotteryStatus == LotteryStatus.Won);
            }
            else
            {
                model.CountOrder = 0;
                model.HasPreregisteredOrder = false;
            }

            model.GoogleAnalytics = _clientBusiness.GetToolboxGoogleAnalytics(this.GetCurrentClubId());

            return PartialView("Navbar/_JbNavbarPartial", model);
        }

        public virtual ActionResult GetJsonMemeberClubs()
        {
            var clubDomain = this.GetCurrentClubDomain();

            var clubs = GetMemeberClubs(clubDomain);

            return JsonNet(clubs);
        }

        private List<MemberClubsModel> GetMemeberClubs(string clubDomain)
        {
            var result = new List<MemberClubsModel>();

            RoleCategory? roleCategory = null;
            if (this.IsUserAuthenticated())
            {
                roleCategory = this.GetCurrentUserRole();
            }

            var clubs = Ioc.ClubBusiness.GetMemeberClubs(clubDomain, this.GetCurrentUserName(), roleCategory);

            if (clubs.Any())
            {
                result = clubs.Select(c =>
                    new MemberClubsModel
                    {
                        Name = c.Name,
                        Domain = c.Domain,
                        Role = c.Role
                    })
                    .ToList();
            }

            return result;
        }

        [ChildActionOnly]
        [AllowAnonymous]
        public virtual ActionResult Navbar()
        {
            var club = this.GetCurrentClubBaseInfo();

            var model = new NavBarViewModel()
            {
                Name = club.Name,
                ClubDomain = club.Domain,
                ClubWebsiteURL = club.Site,
                LogoRedirecturl = club.Site
            };

            return PartialView("Navbar/_Navbar", model);
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult Donation(string clubDomain, long ists = 0)
        {
            ViewBag.ISTS = ists;
            if (ists > 0)
            {

                var orderitem = Ioc.OrderItemBusiness.GetOrderItemByISTS(ists);
                if (orderitem != null && orderitem.Count() > 0)
                {
                    ViewBag.ClubDomain = orderitem.FirstOrDefault().Order.Club.Domain;

                    return View(orderitem.FirstOrDefault().JbForm);
                }
            }
            var club = Ioc.ClubBusiness.Get(clubDomain);
            if (club.Client.EnableDonation && club.Client.DonationFormId.HasValue)
            {
                ViewBag.ClubDomain = club.Domain;

                return View(club.Client.DonationForm);
            }
            return HttpNotFound();
        }
        [HttpPost]
        [Authorize]
        public virtual JsonResult Donation(string clubDomain, long ists, JbForm form)
        {
            form.Id = 0;
            form.RefEntityName = typeof(OrderItem).Name;
            var club = Ioc.ClubBusiness.Get(clubDomain);
            var user = Ioc.UserProfileBusiness.Get(this.GetCurrentUserId());

            var players = Ioc.UserProfileBusiness.GetPlayerProfiles(user.Id);

            var player = players.Any(p => p.Relationship == RelationshipType.Parent) ? players.First(p => p.Relationship == RelationshipType.Parent) : new PlayerProfile();
            var amountElement = form.GetElement<JbSection>(Constants.Donation.Section_Amount_Name).Elements.SingleOrDefault(e => e.Name == Constants.Donation.Element_Amount_Name);
            if (amountElement == null)
            {
                return Json(new JResult() { Status = false, Message = "Amount not found" }, JsonRequestBehavior.AllowGet);
            }
            decimal amount = 0;
            decimal.TryParse(((JbRadioList)amountElement).Value, out amount);
            if (amount <= 0)
            {
                return Json(new JResult() { Status = false, Message = "Please select the amount you would like to donate." }, JsonRequestBehavior.AllowGet);
            }
            var order = Ioc.OrderBusiness.GetOrderInCart(user.Id, club.Id) ?? new Order();
            order.ClubId = club.Id;
            order.UserId = user.Id;
            order.OrderMode = OrderMode.Online;
            order.CompleteDate = DateTime.UtcNow;

            var orderItem = ists > 0 ? order.RegularOrderItems.FirstOrDefault(c => c.ISTS == ists) : new OrderItem();
            if (orderItem == null)
            {
                orderItem = new OrderItem();
            }

            orderItem.Name = "Donation";
            orderItem.DateCreated = DateTime.UtcNow;
            orderItem.JbForm = form;
            orderItem.JbFormId = 0;
            orderItem.ISTS = ists > 0 ? ists : DateTimeHelper.DateTimetoTimeStamp(DateTime.UtcNow);
            if (player.Id > 0)
            {
                orderItem.PlayerId = player.Id;
            }
            orderItem.FirstName = player.Contact != null ? player.Contact.FirstName : this.GetCurrentUserName();
            orderItem.LastName = player.Contact != null ? player.Contact.LastName : this.GetCurrentUserName();
            orderItem.EntryFeeName = "Donation";
            orderItem.ItemStatus = OrderItemStatusCategories.initiated;
            //orderItem.EntryFee = amount;
            var ordercharge = new OrderChargeDiscount()
            {
                Id = 0,
                Amount = amount,
                Category = ChargeDiscountCategory.Donation,
                Subcategory = ChargeDiscountSubcategory.Charge,
                Name = "Donation",
            };
            if (orderItem.GetOrderChargeDiscounts().ToList().Count > 0)
            {
                orderItem.OrderChargeDiscounts.Clear();
            }
            orderItem.OrderChargeDiscounts.Add(ordercharge);

            if (orderItem.Id == 0)
            {
                order.OrderItems.Add(orderItem);
            }
            OperationStatus res = null;
            if (order.Id > 0)
            {
                res = Ioc.OrderBusiness.Update(order);
            }
            else
            {
                res = Ioc.OrderBusiness.Create(order);
            }
            if (res.Status)
            {
                var url = Url.Action("Index", "Cart");
                return Json(new JResult() { Status = true, Data = url }, JsonRequestBehavior.AllowGet);
            }
            LogHelper.LogException(res.Exception);
            return Json(new JResult() { Status = res.Status, Message = Constants.PlayerProfile_Ajax_Request_Failed }, JsonRequestBehavior.AllowGet);
        }

    }
}