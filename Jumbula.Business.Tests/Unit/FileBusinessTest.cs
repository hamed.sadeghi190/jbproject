﻿using System;
using FluentAssertions;
using Jumbula.Core.Business;
using Jumbula.Core.Model.Email;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.IO;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class FileBusinessTest
    {
        #region Fields
        private Mock<FileStream> _mockFileStream;
        private Mock<IStorageBusiness> _mockStorageBusiness;
        #endregion

        #region Constants
        private const int LowerThanValidSize = 9 * 1024 * 1024;
        private const int EqualToValidSize = 10 * 1024 * 1024;
        private const int GreaterThanValidSize = 20 * 1024 * 1024;
        private const string ValidFileAddress = "c:\\xy.pdf";
        private const string NotValidFileAddress = "c:\\xy.exe";
        private const string ContentType = "image/jpeg";
        private const string ClubDomain = "qaclubsand";
        private const string FolderName = "club";
        private const string ImagePathIsNotValidMessage = "Image path is not valid.";
        #endregion

        #region Test methods
        [TestMethod]
        public void GetLimitExtensions_GetList_ShouldReturnItems()
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.GetLimitExtensions();

            // Assert
            result.Should().NotBeNullOrEmpty();
        }

        [TestMethod]
        public void IsAllowedFile_ValidSizeWithNotValidExtension_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();
            _mockFileStream.Setup(x => x.Length).Returns(LowerThanValidSize);

            // Act
            var result = service.IsAllowedFile(new FileStream(NotValidFileAddress, FileMode.Append));

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsAllowedFile_ValidExtensionWithSizeGreaterThanMaxSize_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();
            _mockFileStream.Setup(x => x.Length).Returns(GreaterThanValidSize);
            //_mockFileStream.Setup(x => x.Name).Returns(ValidFileAddress);

            // Act
            var result = service.IsAllowedFile(_mockFileStream.Object);

            // Assert
            result.Should().BeFalse();
        }

        [TestMethod]
        public void IsAllowedFile_ValidExtensionWithSizeEqualToMaxSize_ShouldReturnTrue()
        {
            // Arrange
            var service = CreateService();
            _mockFileStream.Setup(x => x.Length).Returns(EqualToValidSize);

            // Act
            var result = service.IsAllowedFile(new FileStream(ValidFileAddress, FileMode.Append));

            // Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void IsAllowedFile_ValidExtensionWithSizeLowerThanMaxSize_ShouldReturnTrue()
        {
            // Arrange
            var service = CreateService();
            _mockFileStream.Setup(x => x.Length).Returns(LowerThanValidSize);

            // Act
            var result = service.IsAllowedFile(new FileStream(ValidFileAddress, FileMode.Append));

            // Assert
            result.Should().BeTrue();
        }

        [TestMethod]
        public void ValidateAttachment_NotValidExtensions_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.ValidateAttachment(FillNotValidExtensionAttachment());

            // Assert
            result.Status.Should().BeFalse();
        }

        [TestMethod]
        public void ValidateAttachment_SumOfSizeGreaterThanMaxSize_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.ValidateAttachment(FillSumSizeIsGreaterThanMaxSizeAttachment());

            // Assert
            result.Status.Should().BeFalse();
        }

        [TestMethod]
        public void ValidateAttachment_SumOfSizeEqualToMaxSize_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.ValidateAttachment(FillSumSizeIsEqualToMaxSizeAttachment());

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        public void ValidateAttachment_SumOfSizeLowerThanMaxSize_ShouldReturnFalse()
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.ValidateAttachment(FillSumSizeIsLowerThanMaxSizeAttachment());

            // Assert
            result.Status.Should().BeTrue();
        }

        [TestMethod]
        [ExpectedException(typeof(Exception), ImagePathIsNotValidMessage)]
        public void UploadImage_IsNotRelationalUrl_ShouldReturnEmpty()
        {
            // Arrange
            var service = CreateService();

            // Act
            service.UploadImage(string.Empty, It.IsAny<string>(), It.IsAny<string>());

            // Assert
        }

        [TestMethod]
        public void UploadImage_IsRelationalUrl_ShouldReturnEmpty()
        {
            // Arrange
            var service = CreateService();
            _mockStorageBusiness.Setup(x => x.UploadBlob($@"{ClubDomain}\{FolderName}", It.IsAny<string>(), It.IsAny<Stream>(), ContentType));
            var imageData = new FileStream(ValidFileAddress, FileMode.Append).ToString();

            // Act
            var result = service.UploadImage(imageData, ClubDomain, FolderName);

            // Assert
            result.Should().NotBeNullOrEmpty();
        }
        #endregion

        #region Private methods
        private FileBusiness CreateService()
        {
            _mockFileStream = new Mock<FileStream>();
            _mockStorageBusiness = new Mock<IStorageBusiness>();

            return new FileBusiness(_mockStorageBusiness.Object);
        }

        private static List<AttachmentModel> FillNotValidExtensionAttachment() => new List<AttachmentModel>
        {
            new AttachmentModel {FileName = ValidFileAddress, FileSize = LowerThanValidSize},
            new AttachmentModel {FileName = NotValidFileAddress, FileSize = LowerThanValidSize}
        };

        private static List<AttachmentModel> FillSumSizeIsGreaterThanMaxSizeAttachment() => new List<AttachmentModel>
        {
            new AttachmentModel {FileName = ValidFileAddress, FileSize = LowerThanValidSize},
            new AttachmentModel {FileName = ValidFileAddress, FileSize = LowerThanValidSize}
        };

        private static List<AttachmentModel> FillSumSizeIsEqualToMaxSizeAttachment() => new List<AttachmentModel>
        {
            new AttachmentModel {FileName = ValidFileAddress, FileSize = EqualToValidSize},
            new AttachmentModel {FileName = ValidFileAddress, FileSize = 0}
        };

        private static List<AttachmentModel> FillSumSizeIsLowerThanMaxSizeAttachment() => new List<AttachmentModel>
        {
            new AttachmentModel {FileName = ValidFileAddress, FileSize = LowerThanValidSize},
            new AttachmentModel {FileName = ValidFileAddress, FileSize = 0}
        };
        //private FileStream ReturnValidFile()
        //{
        //    return new FileStream(); { };
        //}

        #endregion
    }
}
