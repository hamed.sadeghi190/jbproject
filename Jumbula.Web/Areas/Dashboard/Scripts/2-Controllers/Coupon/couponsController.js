﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('couponsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'couponAddEditService', function ($scope, $http, $log, $stateParams, $state, couponAddEditService) {
        window.app.globalObjects.pageClass = 'seasons-view step1';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;

        couponAddEditService($scope, $http, $stateParams);

        $scope.baseUrl = window.location.origin + "/Dashboard/Coupon";
        $scope.baseModel = null;
        $http.get($scope.baseUrl + '/GetBaseData').success(function (data, status, headers, config) {
            $scope.baseModel = data;
        }).error(function (data, status, headers, config) {
            alert("error " + data);
        });

        $scope.mainCouponsGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + '/GetJsonData?seasonDomain=' + $scope.MC.stateParams.seasonDomain,
                        //cache: false,
                        type: "POST",
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchCoupon,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainCouponsGridDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [{
                field: "Name",
                title: "Name",
                template: "<strong class='grid-row-title'>#= Name #</strong>"
            }, {
                field: "Code",
                title: "Code",
                width: "200px",
                template: "<span class='label info text-italic text-bold'>#= Code #</span>"
            }, {
                field: "ValidityDates",
                title: "Validity dates",
                width: "220px",
                template: "<span> #=StartDate# </span> - <span> #=EndDate# </span>"
            }, {
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                    <li ui_sref='CouponEdit({seasonDomain: \"#=SeasonDomain#\", couponId: #=Id#})'>\
                                          <span class='k-link jbi-pencil'>Edit</span>\
                                    </li>\
                                    <li ng-click='deleteRow(#= Id #)'>\
                                          <span class='k-link jbi-remove'>Delete</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>"
            }],
        };

        $scope.couponsSearch = function () {
            $scope.mainGridOptions.dataSource.page(1);
            $scope.mainGridOptions.dataSource.read();
        }
        $scope.Upload = function () {

            $state.forward('CouponsUpload', { templateId: 0 }, 'Coupons', { seasonDomain: MC.stateParams.seasonDomain }
                , function () {

                });
        }

        $scope.toolbarOptions = {
            items: [
                { type: "button", text: "Add new coupon", click: function () { location.href = baseUrl + "/createedit/0"; } },
            ]
        };
    }]);
})();