﻿using Jumbula.Core.Domain;
using Jumbula.Core.Model.Toolbox;
using System.Linq;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IClientBusiness
    {
        ClientBillingHistory GetClientBillingHistory(long billingId);
        Client Get(int clubId);
        OperationStatus Update(Client client);
        Client GetById(long clientId);
        IQueryable<Client> GetList();
        OperationStatus ActivateClientCreditCard(long clientId, long cardId);
        PaymentGateway GetPaymentGatewayClub(int clubId);
        string GenerateBillingId();
        ToolboxGoogleAnalyticsViewModel GetToolboxGoogleAnalytics(int clubId);
        OperationStatus SaveToolboxGoogleAnalytics(ToolboxGoogleAnalyticsViewModel toolboxGoogleAnalytics, int clubId);
    }
}
