﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Core.Model.Email;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web;

namespace Jumbula.Business.EmailBuilder
{
    public class ResetPasswordEmailBuilder : BaseEmailBuilder<ResetPasswordParameterModel>
    {
        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private readonly IUserProfileBusiness _userProfileBusiness;
        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private string _userName;
        private Club _club;
        private RequestType _requestType;
        private ResetPasswordParameterModel _parameters;
        #endregion

        #region Constants

        #endregion

        #region Constructors
        public ResetPasswordEmailBuilder(IClubBusiness clubBusiness, IUserProfileBusiness userProfileBusiness, IApplicationUserManager<JbUser, int> applicationUserManager)
        {
            _clubBusiness = clubBusiness;
            _userProfileBusiness = userProfileBusiness;
            _applicationUserManager = applicationUserManager;
        }
        #endregion

        #region Properties
        public override EmailCategory Category => EmailCategory.ResetPassword;

        public override string TemplateName => "ResetPasswordEmail";

        public override EmailCallerType CallerType => EmailCallerType.User;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.High;

        public override bool ShouldSendToSupport => true;

        #endregion

        #region Public Methods

        public override void Initiate(ResetPasswordParameterModel parameters)
        {
            _parameters = parameters;
            _requestType = parameters.RequestType;
            _userName = _parameters.UserName;

            if(parameters.ClubId.HasValue)
            _club = _clubBusiness.Get(parameters.ClubId.Value);
        }

        public override void InitiatePreview(ResetPasswordParameterModel parameters)
        {
        }

        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            ResetPasswordEmailBodyModel result;

            var user = _userProfileBusiness.Get(_userName);
            string token = _applicationUserManager.GeneratePasswordResetToken(user.Id);

            var resetLink = _parameters.ResetPasswordUrl.Replace("jbtoken", HttpUtility.UrlEncode(token)).Replace("jbemail", HttpUtility.UrlEncode(_userName));

            if (_requestType == RequestType.Branded)
            {
                result = new ResetPasswordEmailBodyModel
                {
                    RequestType = _requestType,
                    HomePageUrl = _club.Site,
                    LogoUrl = _clubBusiness.GetClubLogo(_club),
                    HomeTitle = _club.Name,
                    UserFullName = _userName,
                    ResetLink = resetLink
                };

            }
            else
            {
                result = new ResetPasswordEmailBodyModel
                {
                    RequestType = _requestType,
                    HomePageUrl = Constants.JumbulaDotCom_Url,
                    LogoUrl = $"{Constants.JumbulaDotCom_Url}Images/jb-logo.png",
                    HomeTitle = Constants.W_Jumbula,
                    UserFullName = _userName,
                    ResetLink = resetLink
                };
            }

            return result;
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            var result = new MailAddress(Constants.W_NoReplyEmailAddress, Constants.W_Jumbula);

            if (_requestType == RequestType.Branded)
                result = new MailAddress(Constants.W_NoReplyEmailAddress, _club.Name);

            return result;
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return _requestType == RequestType.Branded ? _club.Name : Constants.W_Jumbula;
        }

        public override string GetReplyTo(string subCategory = null)
        {
            return Constants.W_NoReplyEmailAddress;
        }

        public override string GetSubject(string subCategory = null)
        {
            var result = Constants.F_ResetPasswordSubject;

            if (_requestType == RequestType.Branded)
                result = $"Reset your {_club.Name} password {_userName}";

            return result;
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            return new List<MailAddress> { new MailAddress(_userName) };
        }

        public override List<CommunicationHistory> GetHistories()
        {
            var user = _userProfileBusiness.Get(_userName);

            return new List<CommunicationHistory>()
            {
                new CommunicationHistory{ RelatedEntity = nameof(JbUser), RelatedEntityId = user.Id},
            };
        }

        #endregion
    }
}
