﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum CalculationType : byte
    {
        [Description("Only tuition label")]
        OnlyTuition = 0,

        [Description("Total amount: tuition amount and all additional charges and services (except application fee and surcharge).")]
        TotalAmount = 1,

        [Description("Only the extra services")]
        OnlyExtraServices = 2
    }

}
