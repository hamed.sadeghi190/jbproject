﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum SubDomainCategories
    {
        Tourney = 0,
        Camp = 1,
        [Display(Name = "Class")]
        [Description("Class")]
        Course = 2, /* used instead of class, which is a researved word :) */
        League = 3,
        Coach = 4,
        News = 5,
        [Display(Name = "None")]
        None = 6,
        ImageGalley = 7,
        Calendar = 8,
        [Display(Name = "Class")]
        [Description("Class")]
        NewRegular = 9,
        [Display(Name = "Class")]
        [Description("Class")]
        Pack = 10,
        Donation = 11,
        [Display(Name = "SimpleProgram")]
        [Description("Simple Program")]
        SimpleProgram
    }
}
