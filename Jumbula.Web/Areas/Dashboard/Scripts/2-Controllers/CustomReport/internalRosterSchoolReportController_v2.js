﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('internalRosterSchoolReportController_v2', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            var baseUrl = window.location.origin + '/Dashboard/CustomReport';

            var timer = false;

            $scope.Model = {};

            $scope.stateParams = $stateParams;
            $scope.MC.stateParams = $stateParams;

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };
            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get(baseUrl + '/GetAllProgramsSignOutSheetReport', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllPrograms = data;
                $scope.AllPrograms.unshift({ Text: "All", Value: null });
                $scope.Model.ScheduleId = null;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });



            $scope.runReport = function () {

                customReportService.saveSchoolSeasonId($scope.Model.schoolId, $scope.Model.SeasonId);

                $http.get(baseUrl + '/GetInternalRosterSchoolReport_v2', { params: { seasonDomain: $scope.stateParams.seasonDomain, scheduleId: $scope.Model.ScheduleId } }).success(function (data, status, headers, config) {
                    $scope.ModelPrograms = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error("error " + data);
                });

            }


            $scope.saveToPdf = function () {

                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = baseUrl + '/GenerateInternalRosterSchoolReportPdf_v2?seasonDomain=' + $scope.stateParams.seasonDomain + '&scheduleId=' + $scope.Model.ScheduleId;
                a.download = "InternalRosterReport.Pdf";
                a.click();
            };
        }
    ]);


})();