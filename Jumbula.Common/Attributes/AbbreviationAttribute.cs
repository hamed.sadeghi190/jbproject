﻿using System;

namespace Jumbula.Common.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class AbbreviationAttribute : Attribute
    {
        public static readonly AbbreviationAttribute Default = new AbbreviationAttribute();

        public AbbreviationAttribute() : this(string.Empty)
        {
        }

        public AbbreviationAttribute(string abbreviation)
        {
            AbbreviationValue = abbreviation;
        }

        public virtual string Abbreviation => AbbreviationValue;

        protected string AbbreviationValue { get; set; }
    }
}
