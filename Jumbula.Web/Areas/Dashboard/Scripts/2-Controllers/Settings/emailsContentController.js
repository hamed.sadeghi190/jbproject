﻿(function () {
    "use strict";

    angular.module("dashboardApp").controller("emailsContentController",
        [
            "$scope", "$http", "jbToast", function ($scope, $http, jbToast) {
            
                window.app.globalObjects.initializeView();

                $http.get('Dashboard/Setting/GetEmailContents').success(function (data, status, headers, config) {
                    $scope.Model = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error("error " + data);
                });

                $scope.save = function () {

                    $http.post('Dashboard/Setting/SaveEmailContents', { model: $scope.Model })
                        .success(function (data, status, headers, config) {

                            if (data.Status) {

                                $scope.errors = null;

                                jbToast.success('Notifications updated successfully.');
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        })
                }

                $scope.setConfirmationdefault = function (value) {
                    $scope.Model.ContentConfirmationEmail = value;
                }
                $scope.setCancellationdefault = function (value) {
                    $scope.Model.ContentCancellationEmail = value;
                }

                $scope.toggleTooltip = function (containerId) {
                    if (arguments[3])
                        uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2], arguments[3]);
                    else if (arguments[2])
                        uiComponentsService.toggleTooltip(containerId, arguments[1], arguments[2]);
                    else if (arguments[1])
                        uiComponentsService.toggleTooltip(containerId, arguments[1]);
                    else
                        uiComponentsService.toggleTooltip(containerId);
                }
            }
        ]);
})();