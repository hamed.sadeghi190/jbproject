﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models.Register
{
    public class OrderChargeDiscountModel : BaseViewModel<OrderChargeDiscount, long>
    {
        public long ChargeId { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public ChargeDiscountCategory Category { get; set; }
        public ChargeDiscountSubcategory Subcategory { get; set; }
        public long ParentChargeId { get; set; }
        public int ScheduleId { get; set; }
        public bool IsMandatory { get; set; }
        public string ScheduleTitle { get; set; }
    }
}