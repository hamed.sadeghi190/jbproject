﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SelectSeasonController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'select-county-view';
            window.app.globalObjects.initializeView();

            $scope.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.AllSeasons = [];
            $scope.Model = {};
            var timer = false;

            $scope.$watch('AllSeasons', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get('Dashboard/PortalReport/GetAllSchoolsSeasons').success(function (data, status, headers, config) {
                $scope.AllSeasons = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            var checkValidation = function (seasonTitle) {
                if (is.undefined(seasonTitle)) {
                    jbToast.error("Please select a season.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {
                if (checkValidation($scope.Model.SeasonTitle)) {
                    $state.go('SeasonProcessingDates', { seasonTitle: $scope.Model.SeasonTitle });
                }
            }
        }
    ]);

})();