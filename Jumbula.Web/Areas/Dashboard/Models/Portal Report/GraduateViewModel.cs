﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class GraduateViewModel
    {
      public string ParticipantName { get; set; }
        public string UserName { get; set; }
        public DateTime LastOrderDate { get; set; }

        public string StrDate {
            get
            {
                return LastOrderDate.ToString("MMM dd, yyyy");
            }
        }
        public int PlayerId { get; set; }
    }
   
}