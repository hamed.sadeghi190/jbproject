﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class BillingTransactionActivityConfig : EntityTypeConfiguration<BillingTransactionActivity>
    {
        public BillingTransactionActivityConfig()
        {
            HasRequired(c => c.Billing).WithMany(p => p.TransactionActivities).HasForeignKey(c => c.BillingId);
            HasRequired(c => c.CreditCard).WithMany(p => p.TransactionActivities).HasForeignKey(c => c.CreditCardId).WillCascadeOnDelete(false);
        }
    }
}
