﻿using Jb.Framework.Common;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class JbFlyerBusiness : IJbFlyerBusiness
    {
        private readonly IRepository<JbFlyer> _jbFlyerRepository;
        public JbFlyerBusiness(IRepository<JbFlyer> jbFlyerRepository)
        {
            _jbFlyerRepository = jbFlyerRepository;
        }

        public OperationStatus SaveFlyer(JbFlyer template)
        {
            return _jbFlyerRepository.Create(template);
        }

        public IQueryable<JbFlyer> GetAllFlyers()
        {
            return _jbFlyerRepository.GetList();
        }
        public JbFlyer Get(int id)
        {
            return _jbFlyerRepository.GetList().SingleOrDefault(F => F.Id == id);
        }

        public List<JbFlyer> GetList(List<int> flyerId)
        {
            return _jbFlyerRepository.GetList().Where(f => flyerId.Contains(f.Id)).ToList();

        }

        public OperationStatus DeleteFlyer(int flyerId)
        {
            return _jbFlyerRepository.Delete(flyerId);
        }

        public OperationStatus Update(JbFlyer flyer)
        {
            return _jbFlyerRepository.Update(flyer);
        }

    }
}
