﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramCreateEditModelStep3 : BaseViewModel<Program, long>, IProgramWizardModel
    {
        public string ClubDomain{get; set;}

        public string SeasonDomain{get; set;}

        public string Domain{get; set;}

        public string Name{get; set;}

        public ProgramStatus Status { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public ProgramTypeCategory TypeCategory { get; set; }

        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }

        public ProgramPageStep PageStep{get; set;}

        public ProgramPageStep LastCreatedPage { get; set; }


        public string FormTemplate { get; set; }

        public List<SelectKeyValue<string>> FormTemplates { get; set; }

        public List<SelectKeyValue<string>> AllFollowUpForms { get; set; }

        public List<string> SelectedForms { get; set; }

        public List<SelectKeyValue<string>> AllWaivers { get; set; }

        public List<string> SelectedWaivers { get; set; }

        public SaveType SaveType { get; set; }
    }
}