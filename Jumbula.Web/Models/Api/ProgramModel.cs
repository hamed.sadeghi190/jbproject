﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models.Api
{
    public class SessionAttendanceModel
    {
        public string Id { get; set; }
        public AttendanceStatus? Status { get; set; }
        public string FullName { get; set; }
        public GenderCategories Gender { get; set; }
        public string ImageUrl { get; set; }
    }

    public class SeasonAttendanceModel
    {
        public string ProgramName { get; set; }
        public List<SessionAttendanceModel> SessionAttendances { get; set; }
    }

    public class SeasonAttendancesModel
    {
        public int ClubId { get; set; }
        public long SeasonId { get; set; }
        public DateTime? LocalDateTime { get; set; }
    }
}