﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Core.Model.Report
{
    public class ReportGridSort
    {
        public string field { get; set; }
        public string dir { get; set; }
        public string sortPath { get; set; }

    }
}
