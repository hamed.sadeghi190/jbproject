﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('SignOutSheetAdvancedReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();
            $scope.MC.collapseAllBlades();

            var baseUrl = window.location.origin + '/Dashboard/CustomReport';

            var timer = false;

            $scope.Model = {};

            $scope.stateParams = $stateParams;
            
            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.Model.Sorts = [{ Value: "Participant", Title: "Participant" }, { Value: "Grade", Title: "Grade" }];
            $scope.Model.SortBy = "Participant";

            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get(baseUrl + '/GetAllProgramsSignOutSheetReport', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete
                $scope.AllPrograms = data;
                $scope.AllPrograms.unshift({ Text: "All", Value: null });
                $scope.Model.ScheduleId = null;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            var checkValidation = function (schoolId, seasonId) {
                if ($scope.Model.DateProgram == "") {
                    jbToast.error("Please select a session start date.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $http.get(baseUrl + '/GetReportDate').success(function (data, status, headers, config) {

                $scope.Model.Date = data.Date;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $http.get(baseUrl + '/GetDate').success(function (data, status, headers, config) {

                $scope.Model.DateProgram = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            $scope.runReport = function () {
                if (checkValidation()) {
                    $http.get(baseUrl + '/ProgramsListSignOutSheetReport', { params: { seasonDomain: $scope.stateParams.seasonDomain, scheduleId: $scope.Model.ScheduleId, date: $scope.Model.DateProgram, sortBy: $scope.Model.SortBy } }).success(function (data, status, headers, config) {

                        $scope.ModelPrograms = data;
                        showEmptyDataMessage(data);

                    }).error(function (data, status, headers, config) {
                        jbToast.error("error " + data);
                    });
                }
            }
            $scope.saveToPdf = function () {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = baseUrl + '/GenerateSignOutSheetPdf?seasonDomain=' + $scope.stateParams.seasonDomain + '&scheduleId=' + $scope.Model.ScheduleId + '&date=' + $scope.Model.DateProgram + '&sortBy=' + $scope.Model.SortBy;
                a.download = "SignOutSheetReport.Pdf";
                a.click();
            };
            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = '/Dashboard/CustomReport/GenerateSignOutSheetExcel?seasonDomain=' + $scope.stateParams.seasonDomain + '&scheduleId=' + $scope.Model.ScheduleId + '&date=' + $scope.Model.DateProgram + '&sortBy=' + $scope.Model.SortBy;
                a.download = "SignOutSheetReport.xlsx";
                a.click();
                cfpLoadingBar.complete();
            };

            function showEmptyDataMessage(data) {
                var isEmptyData = true;
                for (var i = 0; i < data.length; i++) {
                    if (data[i].ProgramParticipants != null) {
                        isEmptyData = false;
                    }
                }

                if (isEmptyData) {
                    jbToast.info("There is no sign-out information for " + $scope.Model.DateProgram);
                }
            }
        }
    ]);


})();