﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Stripe;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    [Authorize(Roles = "SysAdmin")]
    public class SystemController : DashboardBaseController
    {
        #region Fields
        private readonly object _thisLock = new object();
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IUserCreditCardBusiness _userCreditCardBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;
        #endregion

        #region Constractors
        public SystemController(IOrderInstallmentBusiness orderInstallmentBusiness,
            IClubBusiness clubBusiness,
            IUserCreditCardBusiness userCreditCardBusiness,
            IProgramBusiness programBusiness,
            ISeasonBusiness seasonBusiness,
            ITransactionActivityBusiness transactionActivityBusiness)
        {
            _orderInstallmentBusiness = orderInstallmentBusiness;
            _clubBusiness = clubBusiness;
            _userCreditCardBusiness = userCreditCardBusiness;
            _programBusiness = programBusiness;
            _seasonBusiness = seasonBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
        }
        #endregion

        // GET: Dashboard/System
        public virtual ActionResult Clubs()
        {
            return View();
        }
        public virtual ActionResult ClubManage()
        {
            return View();
        }

        public virtual ActionResult ShowPlan()
        {
            return View();
        }
        public virtual ActionResult InstallmentReport()
        {
            return View();
        }
        public virtual ActionResult SelectTransaction()
        {
            return View();
        }
        public virtual ActionResult BillingHistory()
        {
            return View();
        }
        public virtual ActionResult BillingReceipt()
        {
            return View();
        }
        public virtual ActionResult RefundTransaction()
        {
            return View();
        }
        public virtual ActionResult ConfirmRefundTransaction()
        {
            return View();
        }
        public virtual ActionResult AddDebit()
        {
            return View();
        }
        public virtual ActionResult ChargeClub()
        {
            return View();
        }
        public virtual ActionResult Credit()
        {
            return View();
        }
        public virtual ActionResult ConfirmCredit()
        {
            return View();
        }
        public virtual ActionResult ConfirmChargeClub()
        {
            return View();
        }
        public virtual ActionResult ConfirmDebit()
        {
            return View();
        }
        public virtual ActionResult SystemRevenue()
        {
            return View();
        }
        public virtual ActionResult SystemClientCreditCards()
        {
            return View();
        }
        public virtual ActionResult Installments()
        {
            return View();
        }

        public virtual ActionResult RevenueDetailes()
        {
            return View();
        }
        public virtual ActionResult RevenueMatrix()
        {
            return View();
        }
        public virtual JsonNetResult GetAllClubs(PaginationModel paginationModel, string term, string typeId,
           string startDate, string endDate, ClubAccountType accountType = ClubAccountType.Active)
        {
            try
            {
                var clubs = _clubBusiness.GetList().Where(c => !c.IsDeleted);
                if (this.GetCurrentUserRole() != RoleCategory.SysAdmin)
                {
                    PageNotFound();
                }
                if (!string.IsNullOrEmpty(term))
                {
                    clubs = clubs.Where(c => c.Name.ToLower().StartsWith(term.ToLower()));
                }
                if (!string.IsNullOrEmpty(typeId) && typeId != "-1")
                {
                    var inttypeId = (int.Parse(typeId));
                    clubs = clubs.Where(c => c.ClubType.ParentId.Value == inttypeId);
                }

                switch (accountType)
                {
                    case ClubAccountType.All:
                        break;
                    case ClubAccountType.Active:
                        {
                            clubs = clubs.Where(c => c.Client != null && c.Client.CreditCards.Any());
                        }
                        break;
                    case ClubAccountType.FreeTrial:
                        {
                            clubs = clubs.Where(c => c.Client != null && c.Client.PricePlan.Type == PricePlanType.FreeTrial);
                        }
                        break;
                    default:
                        break;
                }

                if (!string.IsNullOrEmpty(startDate))
                {
                    var date = DateTime.Parse(startDate);
                    clubs =
                        clubs.Where(
                            oitem =>
                                oitem.ExpirationDate.HasValue && oitem.ExpirationDate.Value >= date);
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    var date = DateTime.Parse(endDate);
                    date = date.AddHours(24 - date.Hour);
                    clubs =
                        clubs.Where(
                            oitem =>
                                oitem.ExpirationDate.HasValue && oitem.ExpirationDate.Value < date);
                }
                var model = clubs.OrderBy(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize).ToList().Select(c => new SysAdminClubItemViewModel()
                {
                    County = c.Address != null ? (!string.IsNullOrEmpty(c.Address.County) ? c.Address.County : "-") : "-",
                    //Description = c.Description,
                    Domain = c.Domain,
                    AgreementExpirationDate = c.ExpirationDate,
                    Id = c.Id,
                    Name = c.Name,
                    Phone = c.ContactPersons.Any() ? c.ContactPersons.First().Phone : "-",
                    Email = c.ContactPersons.Any() ? c.ContactPersons.First().Email : "-",
                    FirstName = c.ContactPersons.Any() ? c.ContactPersons.First().FirstName : "",
                    LastName = c.ContactPersons.Any() ? c.ContactPersons.First().LastName : "",
                    typeMerge = c.ClubType.ParentType != null ? string.Format(Constants.F_CategoryFormat, c.ClubType.ParentType.Name, c.ClubType.Name) : "-",
                    //CommissionRateString = !c.IsSchool ? c.PartnerCommisionRate.ToString() : "-"

                });


                var datasource = model.ToList();
                return JsonNet(new { data = datasource, total = clubs.Count() },
                    new Newtonsoft.Json.JsonSerializerSettings(), "");

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return JsonNet(null, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual JsonResult DeleteClub(int clubId)
        {
            var res = new OperationStatus() { Status = false, Message = Constants.F_Club_Delete };
            if (
                !Ioc.OrderBusiness.GetList()
                    .Any(c => c.ClubId == clubId && c.OrderStatus == OrderStatusCategories.completed))
            {
                res = _clubBusiness.Delete(clubId);
            }

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        //public virtual ActionResult SaveClub(SysAdminClubViewModel model)
        //{
        //    try
        //    {
        //        var address = new PostalAddress();
        //        CheckModelIntegrity(ModelState, model, ref address);


        //        if (ModelState.IsValid)
        //        {

        //            OperationStatus res;

        //            if (model.Id > 0)
        //            {
        //                res = EditClub(model, address);
        //            }
        //            else
        //            {
        //                res = CreateClub(model, address);
        //            }

        //            return
        //                Json(new JResult
        //                {
        //                    Status = res.Status,
        //                    Message = res.Message,
        //                    RecordsAffected = res.RecordsAffected
        //                });
        //        }
        //        return JsonFormResponse();
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilities.LogException(ex);
        //    }
        //    return Json(new JResult { Status = false, Message = Constants.M_E_CreateEdit, RecordsAffected = 0 });
        //}

        [HttpGet]
        public virtual JsonNetResult ClubPage()
        {

            var model = new SysAdminClubViewModel();
            model.OrganizationTypes = new List<JbTitleValue>();
            model.OrganizationTypes.Add(new JbTitleValue() { Title = "All", Value = -1 });
            model.OrganizationTypes.AddRange(
                _clubBusiness.GetClubTypeList()
                    .Where(c => c.ParentId.HasValue)
                    .GroupBy(c => new { id = c.ParentId.Value, name = c.ParentType.Name })
                    .Select(v => new JbTitleValue() { Title = v.Key.name, Value = v.Key.id })
                    .ToList());

            model.AccountTypes = DropdownHelpers.ToSelectList<ClubAccountType>();

            model.PaymentMethods = DropdownHelpers.ToSelectList<PaymentMethod>();

            return JsonNet(model);
        }

        public virtual JsonNetResult GetCreditCard(int clubId, PaginationModel paginationModel)
        {

            var model = new List<PlanItemViewModel>();
            try
            {
                var clientId = _clubBusiness.Get(clubId).ClientId;

                var clintCards = Ioc.ClientCreditCardBusiness.GetClientCards(clientId.Value);


                model = clintCards.Select(p =>
                  new PlanItemViewModel
                  {
                      Last4Digit = p.LastDigits,
                      CardHolderName = p.CardHolderName,
                      Brand = p.Brand,
                      ExpiryYear = p.ExpiryYear,
                      ExpiryMonth = p.ExpiryMonth.Length == 1 ? ("0" + p.ExpiryMonth) : p.ExpiryMonth,
                      Address = p.Address.Address,
                      IsDefault = p.IsDefault,

                  })
                  .ToList();
            }

            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }


            var dataSource = model.ToList().Page(paginationModel);
            return JsonNet(new { data = dataSource, total = model.Count() },
                     new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual ActionResult GetShowPlan(int clubId)
        {
            var model = new PlanItemViewModel();

            var club = _clubBusiness.Get(clubId);

            if (club.ClientId.HasValue)
            {
                var client = Ioc.ClientBusiness.GetById(club.ClientId.Value);
                var priceid = client.PricePlanId;
                var plan = Ioc.PricePlanBusiness.GetList().Where(c => c.Id == priceid).First();

                model.PlanName = plan.Type.ToString();
                model.ClubName = club.Name;
                model.MonthlyCharge = CurrencyHelper.FormatCurrencyWithPenny(club.Client.PricePlan.MonthlyCharges, club.Currency);
                model.TransactionRate = club.Client.PricePlan.TransactionFee + "% ";
            }
            return JsonNet(model);
        }
        public virtual JsonNetResult TransactionPage()
        {

            var model = new SysAdminClubViewModel();
            model.PaymentMethods = DropdownHelpers.ToSelectList<PaymentMethod>();

            return JsonNet(model);
        }

        public virtual ActionResult GetSystemInstallments(DateTime? start, DateTime? end)
        {
            var model = new List<SysInstallmentReportViewModel>();
            var orderInstallments = _orderInstallmentBusiness.GetAllInstallmentsByDate(start, end, 0);
            if (orderInstallments.Any())
            {
                model = orderInstallments.Select(o => new SysInstallmentReportViewModel
                {
                    ClubName = o.OrderItem.Order.Club.Name,
                    InstallmentDate = o.InstallmentDate,
                    Amount = o.Amount,
                    Status = o.Status,
                    //TransactionDate =  o.TransactionActivities.Any() ? (DateTime?) o.TransactionActivities.LastOrDefault().TransactionDate : null,
                    //ResponseMessage = o.TransactionActivities != null && o.TransactionActivities.Any() ? o.TransactionActivities.Last().PaymentDetail.TransactionMessage : string.Empty,
                }).ToList();
            }
            return JsonNet(model);
        }

        public virtual ActionResult GetTotalTransaction(DateTime? start, DateTime? end)
        {

            DateTime startDate;
            DateTime endDate;
            if (!start.HasValue && !end.HasValue)
            {
                startDate = new DateTime(DateTime.Now.Year, 1, 1);
                endDate = DateTime.Now;
            }
            else if (start.HasValue && !end.HasValue)
            {
                startDate = start.Value;
                endDate = DateTime.Now;
            }
            else if (!start.HasValue && end.HasValue)
            {
                startDate = new DateTime(DateTime.Now.Year, 1, 1);
                endDate = end.Value;
            }
            else
            {
                startDate = start.Value;
                endDate = end.Value;
            }

            var query =
                Ioc.OrderItemBusiness.GetList().Where(o => o.ItemStatus == OrderItemStatusCategories.completed)
                    .Where(
                        oi =>
                            DbFunctions.TruncateTime(oi.Order.CompleteDate) >= startDate &&
                            DbFunctions.TruncateTime(oi.Order.CompleteDate) <= endDate);

            decimal totalAmount = query.SelectMany(o => o.TransactionActivities).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal totalRevenue = query.Sum(t => (decimal?)t.TotalAmount) ?? 0;
            decimal creditCard = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Card)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal cash = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Cash)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal check = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Check)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal wireTransfer = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.WireTransfer)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal debitCard = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.DebitCard)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal scholarship = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Scholarship)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal credit = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Credit)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal echeck = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Echeck)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal paypal = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Paypal)).Sum(t => (decimal?)t.Amount) ?? 0;
            var other = query.SelectMany(t => t.TransactionActivities.Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Other)).Sum(t => (decimal?)t.Amount) ?? 0;

            var dataSource = new List<object>()
            {
                new
                {
                    TotalAmount = totalAmount,
                    TotalRevenue = totalRevenue,
                    Creditcard=creditCard,
                    PayPal=paypal,
                    Cash=cash,
                    Check=check,
                    WireTransfer=wireTransfer,
                    DebitCard=debitCard,
                    Scholarship=scholarship,
                    Credit=credit,
                    Echeck=echeck,
                    Other=other,
                }
            }
            .ToList();


            return JsonNet(new { data = dataSource, total = 1 },
                   new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual ActionResult GetPieChartTransactionReport(DateTime? start, DateTime? end)
        {
            DateTime startDate;
            DateTime endDate;
            if (!start.HasValue && !end.HasValue)
            {
                startDate = new DateTime(DateTime.Now.Year, 1, 1);
                endDate = DateTime.Now;
            }
            else if (start.HasValue && !end.HasValue)
            {
                startDate = start.Value;
                endDate = DateTime.Now;
            }
            else if (!start.HasValue && end.HasValue)
            {
                startDate = new DateTime(DateTime.Now.Year, 1, 1);
                endDate = end.Value;
            }
            else
            {
                startDate = start.Value;
                endDate = end.Value;
            }

            var query = Ioc.OrderItemBusiness.GetList().Where(oi => oi.ItemStatus == OrderItemStatusCategories.completed);

            decimal livePayPalTransaction = query.SelectMany(t => t.TransactionActivities).Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Paypal && !string.IsNullOrEmpty(p.PaymentDetail.PaypalIPN)).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal liveCreditcardTransaction = query.SelectMany(t => t.TransactionActivities).Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Credit).Sum(t => (decimal?)t.Amount) ?? 0;
            decimal liveEcheckTransaction = query.SelectMany(t => t.TransactionActivities).Where(p => p.PaymentDetail.PaymentMethod == PaymentMethod.Echeck).Sum(t => (decimal?)t.Amount) ?? 0;



            var reportModel = new List<object>();


            reportModel.Add(new { ProgramName = "CreditCard", Price = liveCreditcardTransaction });
            reportModel.Add(new { ProgramName = "Paypal", Price = livePayPalTransaction });
            reportModel.Add(new { ProgramName = "Echeck", Price = liveEcheckTransaction });

            return Json(reportModel, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult GetSystemRevenue()
        {
            decimal amountWithCredit;
            decimal amountWitoutCrdit;
            decimal totalReveneue;
            decimal percent;

            var year = 2016;

            var billingHistories = Ioc.ClientBusiness.GetList().SelectMany(b => b.BillingHistories);

            var months = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            var dates = new List<DateTime>();

            dates = months.Select(m =>
                new DateTime(year, m, 1)).ToList();

            var result = new List<object>();

            var clubs = _clubBusiness.GetList().Where(c => c.Client.BillingHistories.Any() && billingHistories.Select(b => b.ClientId).Contains(c.Client.Id) && c.Client.PricePlan.Type == PricePlanType.PayAsYouGo);
            var allOrderItemclubs = Ioc.OrderItemBusiness.GetList().Where(i => clubs.Select(c => c.Id).Contains(i.Order.ClubId) && i.ItemStatus == OrderItemStatusCategories.completed);

            foreach (var date in dates)
            {
                var startDate = date;
                var endDate = date.AddMonths(1).AddDays(-1);
                var percentAmount = allOrderItemclubs.SelectMany(t => t.TransactionActivities).Where(d => d.TransactionDate >= startDate && d.TransactionDate <= endDate).Sum(s => (decimal?)s.Amount) / 100 ?? 0;

                amountWithCredit = billingHistories.SelectMany(b => b.TransactionActivities).Where(p => p.TransactionStatus == TransactionStatus.Success && p.TransactionType == TransactionType.Payment && (p.PaymentMethod == PaymentMethod.Card || p.PaymentMethod == PaymentMethod.Credit) && p.TransactionDate >= startDate && p.TransactionDate <= endDate).Sum(t => (decimal?)t.Amount) ?? 0;
                amountWitoutCrdit = billingHistories.SelectMany(c => c.TransactionActivities).Where(p => p.TransactionStatus == TransactionStatus.Success && p.TransactionType == TransactionType.Payment && (p.PaymentMethod != PaymentMethod.Card && p.PaymentMethod != PaymentMethod.Credit) && p.TransactionDate >= startDate && p.TransactionDate <= endDate).Sum(t => (decimal?)t.Amount) ?? 0;
                percent = percentAmount;
                totalReveneue = amountWithCredit + amountWitoutCrdit + percent;

                if (amountWitoutCrdit != 0 || amountWithCredit != 0 || percent != 0)
                {
                    result.Add(new { DateFormat = date.ToString("MMMM yyyy"), Date = date, ChargeWithCredit = amountWithCredit, ChareWithManualy = amountWitoutCrdit, Percent = percent, TotalRevenue = totalReveneue });
                }
            }


            return JsonNet(new { data = result, total = result.Count() },
                   new Newtonsoft.Json.JsonSerializerSettings(), "");
        }



        [HttpGet]
        public virtual ActionResult GetRevenueDate(DateTime date)
        {

            var model = new ReveneuReportViewModel()
            {
                Date = date.ToString("MMMM yyyy")
            };


            return JsonNet(model);
        }





        [HttpPost]
        public virtual ActionResult GetClientMonthlyAmount(DateTime date, PaginationModel paginationModel)
        {

            var startDate = date;
            var endDate = date.AddMonths(1).AddDays(-1);
            var total = 0;
            decimal chargeWithCredit = 0;
            decimal chargeWithManualy = 0;
            decimal totalCharge = 0;
            decimal percent = 0;
            var model = new List<ReveneuReportViewModel>();
            var billingHistories = Ioc.ClientBusiness.GetList().SelectMany(b => b.BillingHistories);


            var clubs = _clubBusiness.GetList().Where(c => c.Client.BillingHistories.Any() && billingHistories.Select(b => b.ClientId).Contains(c.Client.Id));

            foreach (var club in clubs)
            {
                var billings = club.Client.BillingHistories;
                var clubName = club.Name;
                var clubId = club.Id;
                var pricePlan = club.Client.PricePlan.Type;


                if (pricePlan == PricePlanType.PayAsYouGo)
                {
                    var orders = Ioc.OrderBusiness.GetAllOrders().Where(c => c.ClubId == clubId);

                    var orderItems = orders.SelectMany(oi => oi.OrderItems).Where(oi => oi.ItemStatus == OrderItemStatusCategories.completed);
                    var percentAmount = orderItems.SelectMany(t => t.TransactionActivities).Where(d => d.TransactionDate >= startDate && d.TransactionDate <= endDate).Sum(s => (decimal?)s.Amount) / 100 ?? 0;
                    percent = percentAmount;
                }
                else
                {
                    percent = 0;
                }

                chargeWithCredit = billings.SelectMany(t => t.TransactionActivities).Where(p => p.TransactionStatus == TransactionStatus.Success && p.TransactionType == TransactionType.Payment && (p.PaymentMethod == PaymentMethod.Card || p.PaymentMethod == PaymentMethod.Credit) && p.TransactionDate >= startDate && p.TransactionDate <= endDate).Sum(t => (decimal?)t.Amount) ?? 0;
                chargeWithManualy = billings.SelectMany(t => t.TransactionActivities).Where(p => p.TransactionStatus == TransactionStatus.Success && p.TransactionType == TransactionType.Payment && (p.PaymentMethod != PaymentMethod.Card && p.PaymentMethod != PaymentMethod.Credit) && p.TransactionDate >= startDate && p.TransactionDate <= endDate).Sum(t => (decimal?)t.Amount) ?? 0;
                totalCharge = chargeWithCredit + chargeWithManualy + percent;

                model.Add(new ReveneuReportViewModel()
                {
                    Name = clubName,
                    ChargeWithCredit = chargeWithCredit,
                    ChareWithManualy = chargeWithManualy,
                    Percent = percent,
                    TotalCharge = totalCharge
                });
            }
            var dataSource = model.Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize);
            total = model.Count;
            return JsonNet(new { DataSource = dataSource, TotalCount = total });
        }

        [HttpPost]
        public virtual ActionResult GetRevenueMatrix(PaginationModel paginationModel)
        {
            decimal chargeWithCredit = 0;
            decimal chargeWithManualy = 0;
            decimal percent = 0;
            decimal totalCharge = 0;

            var result = new List<Dictionary<string, string>>();

            var months = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            var dates = new List<DateTime>();

            dates = months.Select(m =>
                new DateTime(2016, m, 1)).ToList();

            var billingHistories = Ioc.ClientBusiness.GetList().SelectMany(b => b.BillingHistories);
            var clubs = _clubBusiness.GetList().Where(c => c.Client.BillingHistories.Any() && billingHistories.Select(b => b.ClientId).Contains(c.Client.Id)).ToList();

            var clubCounts = clubs.Count();

            var lastClubId = 0;
            if (clubs.Any())
            {
                lastClubId = clubs.LastOrDefault().Id;
            }

            decimal[] totalOfAllRows = new decimal[12];


            foreach (var club in clubs)
            {
                var row = new Dictionary<string, string>();
                var billings = club.Client.BillingHistories;
                var clubId = club.Id;
                var pricePlan = club.Client.PricePlan.Type;

                row.Add("Name", club.Name);

                var orders = Ioc.OrderBusiness.GetAllOrders().Where(c => c.ClubId == clubId);

                var orderItems = orders.SelectMany(oi => oi.OrderItems).Where(oi => oi.ItemStatus == OrderItemStatusCategories.completed);
                decimal total = 0;
                foreach (var month in dates)
                {
                    var startDate = month;
                    var endDate = month.AddMonths(1).AddDays(-1);

                    if (pricePlan == PricePlanType.PayAsYouGo)
                    {
                        var percentAmount = orderItems.SelectMany(t => t.TransactionActivities).Where(d => d.TransactionDate >= startDate && d.TransactionDate <= endDate).Sum(s => (decimal?)s.Amount) / 100 ?? 0;
                        percent = percentAmount;
                    }
                    else
                    {
                        percent = 0;
                    }

                    chargeWithCredit = billings.SelectMany(t => t.TransactionActivities).Where(p => p.TransactionStatus == TransactionStatus.Success && p.TransactionType == TransactionType.Payment && (p.PaymentMethod == PaymentMethod.Card || p.PaymentMethod == PaymentMethod.Credit) && p.TransactionDate >= startDate && p.TransactionDate <= endDate).Sum(t => (decimal?)t.Amount) ?? 0;
                    chargeWithManualy = billings.SelectMany(t => t.TransactionActivities).Where(p => p.TransactionStatus == TransactionStatus.Success && p.TransactionType == TransactionType.Payment && (p.PaymentMethod != PaymentMethod.Card && p.PaymentMethod != PaymentMethod.Credit) && p.TransactionDate >= startDate && p.TransactionDate <= endDate).Sum(t => (decimal?)t.Amount) ?? 0;
                    totalCharge = chargeWithCredit + chargeWithManualy + percent;

                    row.Add(month.ToString("MMM yyyy").Replace(" ", "_"), CurrencyHelper.FormatCurrencyWithPenny(totalCharge, CurrencyCodes.USD).ToString());

                    total += totalCharge;

                    totalOfAllRows[month.Month - 1] += totalCharge;
                }

                row.Add("Total", CurrencyHelper.FormatCurrencyWithPenny(total, CurrencyCodes.USD).ToString());

                result.Add(row);

                if (club.Id == lastClubId)
                {
                    var totalRow = new Dictionary<string, string>();

                    foreach (var month in dates)
                    {
                        totalRow.Add(month.ToString("MMM yyyy").Replace(" ", "_"), CurrencyHelper.FormatCurrencyWithPenny(totalOfAllRows[month.Month - 1], CurrencyCodes.USD).ToString());
                    }

                    result.Add(totalRow);
                }
            }



            var model = result.Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize);
            return JsonNet(new { DataSource = model, TotalCount = result.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        [HttpPost]
        public virtual JsonNetResult GetClientCreditCards(int? partnerId, int? clubId, int? gateway, bool? isDefault, string UserName, string mode, int? lastOrderMode, DateTime? orderDate, DateTime? installmentDate, DateTime? installmentEndDate, PaginationModel paginationModel)
        {
            Club club;
            var dataSource = new List<CreditCardsListViewModel>();
            var orders = Ioc.OrderBusiness.GetAllOrdersNeedCreditCard();

            if (clubId.HasValue)
            {
                club = _clubBusiness.Get(clubId.Value);
                orders = orders.Where(c => c.ClubId == clubId);
            }
            else if (partnerId.HasValue)
            {
                club = _clubBusiness.Get(partnerId.Value);
                orders = orders.Where(c => c.Club.PartnerId == partnerId);
            }

            if (orderDate.HasValue)
            {
                orders = orders.Where(c => c.CompleteDate >= orderDate.Value);
            }

            var orderOrderItems = Ioc.OrderItemBusiness.GetComplatedOrderitems().Where(o => orders.Contains(o.Order) && o.ItemStatus == OrderItemStatusCategories.completed);
            var orderItemInstallments = _orderInstallmentBusiness.GetOrderItemInstallments(orderOrderItems);


            if (installmentDate.HasValue && installmentEndDate.HasValue)
            {
                orderItemInstallments = orderItemInstallments.Where(i => i.InstallmentDate >= installmentDate.Value && i.InstallmentDate <= installmentEndDate.Value);
            }
            else if (installmentDate.HasValue)
            {
                orderItemInstallments = orderItemInstallments.Where(i => i.InstallmentDate >= installmentDate.Value);
            }
            else if (installmentEndDate.HasValue)
            {
                orderItemInstallments = orderItemInstallments.Where(i => i.InstallmentDate <= installmentEndDate.Value);
            }

            var users = orderItemInstallments.OrderByDescending(u => u.OrderItem.Order.CompleteDate).Select(c => c.OrderItem.Order.User).Distinct();
            var userIds = users.Select(u => u.Id).ToList();
            var userCreditCards = _userCreditCardBusiness.GetUserCreditCards(userIds);

            var secondUserIds = new List<int>();

            if (!string.IsNullOrEmpty(UserName))
            {
                userCreditCards = userCreditCards.Where(c => c.User.UserName.Contains(UserName));
                userIds = userCreditCards.Select(c => c.UserId).ToList();
                users = users.Where(u => userIds.Contains(u.Id));
            }

            PaymentGateway? _paymentGateway = null;

            if (gateway.HasValue)
            {
                if (gateway.Value == 0)
                {
                    _paymentGateway = PaymentGateway.Stripe;
                }
                else
                {
                    _paymentGateway = PaymentGateway.AuthorizeNet;
                }

                userCreditCards = userCreditCards.Where(c => (int)c.TypePaymentGateway.Value == gateway);
                userIds = userCreditCards.Select(c => c.UserId).ToList();
                users = users.Where(u => userIds.Contains(u.Id));
            }

            if (!string.IsNullOrEmpty(mode))
            {
                if (mode == "Preferred") // Preferrd
                {
                    userCreditCards = userCreditCards.Where(c => c.IsDefault);
                    userIds = userCreditCards.Select(c => c.UserId).ToList();
                    users = users.Where(u => userIds.Contains(u.Id));
                }
                else if (mode == "NotPreferred") // have Credit card but no Preferrd and no Credit card
                {
                    var temp = new List<JbUser>();
                    var hasCreditCardUserIds = userCreditCards.Select(u => u.UserId).ToList();
                    temp.AddRange(Ioc.UserProfileBusiness.GetList(userIds).Where(u => !hasCreditCardUserIds.Contains(u.Id)));

                    userIds = new List<int>();
                    var groupUserCedit = userCreditCards.GroupBy(c => c.UserId);

                    foreach (var group in groupUserCedit)
                    {
                        var userCredit = group.ToList();

                        if (!userCredit.Any(c => c.IsDefault))
                        {
                            userIds = userCredit.Select(c => c.UserId).ToList();
                        }
                    }

                    temp.AddRange(users.Where(u => userIds.Contains(u.Id)));

                    users = temp.AsQueryable();
                }
                else if (mode == "NoCard") // no Credit card
                {
                    var hasCreditCardUserIds = userCreditCards.Select(u => u.UserId).ToList();
                    users = Ioc.UserProfileBusiness.GetList(userIds).Where(u => !hasCreditCardUserIds.Contains(u.Id));

                    userIds = users.Select(c => c.Id).ToList();
                }
                else if (mode == "HasCardNotPreferred") // have Credit card but no Preferrd
                {
                    var groupUserCedit = userCreditCards.GroupBy(c => c.UserId);

                    foreach (var group in groupUserCedit)
                    {
                        var userCredit = group.ToList();

                        if (!userCredit.Any(c => c.IsDefault))
                        {
                            userIds = userCredit.Select(c => c.UserId).ToList();
                        }
                    }

                    users = users.Where(u => userIds.Contains(u.Id));
                }
            }

            var pagedUsers = users.Pagination(paginationModel);

            foreach (var item in pagedUsers)
            {
                var userCards = new List<UserCreditCard>();

                if (_paymentGateway.HasValue)
                {
                    userCards = _userCreditCardBusiness.GetUserCreditCards(item.Id, _paymentGateway.Value).OrderByDescending(c => c.IsDefault).ToList();// userCreditCards.Where(c => c.User.Id == item.Id).OrderByDescending(c => c.IsDefault).ToList();
                }
                else
                {
                    userCards = _userCreditCardBusiness.GetUserCreditCards(item.Id).OrderByDescending(c => c.IsDefault).ToList();
                }
                var lastOrder = orders.Where(o => o.UserId.Equals(item.Id)).OrderByDescending(o => o.Id).First();

                var firstUserCard = userCards.Count() > 0 ? userCards[0] : new UserCreditCard();
                var secondUserCard = userCards.Count() > 1 ? userCards[1] : new UserCreditCard();
                var thirdUserCard = userCards.Count() > 2 ? userCards[2] : new UserCreditCard();
                var lastUserOrder = Ioc.OrderBusiness.GetLastUserOrder(item.Id);
                var userOrdersClub = orders.Where(o => o.UserId.Equals(item.Id)).Select(o => o.Club).Distinct();
                var userClubDomains = GetUserClubs(userOrdersClub);

                bool hasError = false;
                bool hasNoCreditCardError = false;

                if (userCards.Any() && userCards.Count() > 0)
                {
                    if (!userCards.Any(c => c.IsDefault))
                    {
                        hasError = true;
                    }
                }
                else
                {
                    hasNoCreditCardError = true;
                }


                dataSource.Add(new CreditCardsListViewModel()
                {
                    User = item.UserName,
                    HasError = hasError,
                    HasNoCreditCardError = hasNoCreditCardError,
                    userCardsCount = userCards.Count(),
                    LastOrderDate = lastOrder.CompleteDate,
                    ClubDomain = userClubDomains,
                    CustomerId = firstUserCard.CustomerId,
                    LastDigits = firstUserCard.LastDigits,
                    Expiry = !string.IsNullOrEmpty(firstUserCard.ExpiryMonth) && !string.IsNullOrEmpty(firstUserCard.ExpiryYear) ? string.Format("{0}/{1}", firstUserCard.ExpiryMonth, firstUserCard.ExpiryYear) : string.Empty,
                    Name = firstUserCard.Name,
                    Brand = firstUserCard.Brand,
                    IsDefault = firstUserCard.IsDefault,
                    TypeCreditcard = firstUserCard.TypeCreditCard != null ? firstUserCard.TypeCreditCard.ToDescription() : string.Empty,
                    CreateDate = firstUserCard.CreatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(firstUserCard.CreatedDate.Value) : string.Empty,
                    UpdateDate = firstUserCard.UpdatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(firstUserCard.UpdatedDate.Value) : string.Empty,
                    DefaultDate = firstUserCard.DefaultDate != null ? DateTimeHelper.GetIsoDateTimeFormat(firstUserCard.DefaultDate.Value) : string.Empty,
                    LastOrderMode = lastUserOrder.OrderMode.ToDescription(),
                    CustomerId2 = secondUserCard.CustomerId,
                    LastDigits2 = secondUserCard.LastDigits,
                    Expiry2 = !string.IsNullOrEmpty(secondUserCard.ExpiryMonth) && !string.IsNullOrEmpty(secondUserCard.ExpiryYear) ? string.Format("{0}/{1}", secondUserCard.ExpiryMonth, secondUserCard.ExpiryYear) : string.Empty,
                    Name2 = secondUserCard.Name,
                    Brand2 = secondUserCard.Brand,
                    IsDefault2 = secondUserCard.IsDefault,
                    TypeCreditcard2 = secondUserCard.TypeCreditCard != null ? secondUserCard.TypeCreditCard.ToDescription() : string.Empty,
                    CreateDate2 = secondUserCard.CreatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(secondUserCard.CreatedDate.Value) : string.Empty,
                    UpdateDate2 = secondUserCard.UpdatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(secondUserCard.UpdatedDate.Value) : string.Empty,
                    DefaultDate2 = secondUserCard.DefaultDate != null ? DateTimeHelper.GetIsoDateTimeFormat(secondUserCard.DefaultDate.Value) : string.Empty,
                    CustomerId3 = thirdUserCard.CustomerId,
                    LastDigits3 = thirdUserCard.LastDigits,
                    Expiry3 = !string.IsNullOrEmpty(thirdUserCard.ExpiryMonth) && !string.IsNullOrEmpty(thirdUserCard.ExpiryYear) ? string.Format("{0}/{1}", thirdUserCard.ExpiryMonth, thirdUserCard.ExpiryYear) : string.Empty,
                    Name3 = thirdUserCard.Name,
                    Brand3 = thirdUserCard.Brand,
                    IsDefault3 = thirdUserCard.IsDefault,
                    TypeCreditcard3 = thirdUserCard.TypeCreditCard != null ? thirdUserCard.TypeCreditCard.ToDescription() : string.Empty,
                    CreateDate3 = thirdUserCard.CreatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(thirdUserCard.CreatedDate.Value) : string.Empty,
                    UpdateDate3 = thirdUserCard.UpdatedDate != null ? DateTimeHelper.GetIsoDateTimeFormat(thirdUserCard.UpdatedDate.Value) : string.Empty,
                    DefaultDate3 = thirdUserCard.DefaultDate != null ? DateTimeHelper.GetIsoDateTimeFormat(thirdUserCard.DefaultDate.Value) : string.Empty,
                });
            }

            return JsonNet(new { data = dataSource, total = users.Count() });
        }
        private string GetUserClubs(IQueryable<Club> clubs)
        {
            if (clubs != null && clubs.Any())
            {
                return string.Join(", ", clubs.Select(c => c.Domain));
            }
            return string.Empty;
        }

        public virtual JsonNetResult GetClubSeasons(int clubId, bool includeAllOption = false)
        {
            var result = _seasonBusiness.GetKeyValueClubSeasons(clubId, includeAllOption);
            return JsonNet(result);
        }

        public virtual JsonNetResult GetSeasonPrograms(long seasonId)
        {
            var result = _programBusiness.GetKeyValueSeasonPrograms(seasonId);
            return JsonNet(result);
        }
        public virtual JsonNetResult GetProgramSchedules(long programId)
        {
            var result = _programBusiness.GetProgramSchedules(programId);
            return JsonNet(result);
        }

        [HttpPost]
        public virtual JsonNetResult GetInstallments(string term, PeopleSearchType? searchType, int? partnerId, int? clubId, long? seasonId, long? programId, long? programScheduleId, int? orderMode, DateTime? start, DateTime? endDate, DateTime? transactionStartDate, DateTime? transactionEndDate, string mode, string Status, PaginationModel paginationModel, string customeStripeErrorMessage, List<GridSort> sort = null, string firstName = null, string lastName = null)
        {
            var startDate = (DateTime?)null;
            var dataSource = new List<InstallmentsViewModel>();
            var end = (DateTime?)null;

            if (endDate.HasValue)
            {
                end = endDate.Value.Date;
            }

            if (start.HasValue)
            {
                startDate = start.Value.Date;
            }

            if (!string.IsNullOrEmpty(term))
            {
                term = term.ToLower();
            }

            var installments = _orderInstallmentBusiness.GetAllInstallments(startDate, end);

            if (transactionEndDate.HasValue || transactionStartDate.HasValue)
            {
                var transactionEnd = transactionEndDate?.Date;
                var transactionStart = transactionStartDate?.Date;

                installments = _orderInstallmentBusiness.GetInstallmentOfTransactionDate(transactionStart, transactionEnd, installments);
            }

            if (clubId.HasValue && clubId != 0)
            {
                installments = installments.Where(c => c.OrderItem.Order.ClubId == clubId);
            }
            else if (partnerId.HasValue)
            {
                installments = installments.Where(c => c.OrderItem.Order.Club.PartnerId == partnerId);
            }

            if (seasonId.HasValue && seasonId != 0)
            {
                installments = installments.Where(c => c.OrderItem.SeasonId == seasonId);
            }

            if (programId.HasValue && programId != 0)
            {
                installments = installments.Where(c => c.OrderItem.ProgramSchedule.ProgramId == programId);
            }

            if (programScheduleId.HasValue && programScheduleId != 0)
            {
                installments = installments.Where(c => c.OrderItem.ProgramSchedule.Id == programScheduleId);
            }

            if (orderMode.HasValue)
            {
                installments = installments.Where(c => (int)c.OrderItem.Order.OrderMode == orderMode);
            }

            installments = _orderInstallmentBusiness.GetInstallmentsByStatus(Status, installments);
            installments = _orderInstallmentBusiness.GetInstallmentsByMode(mode, installments);

            installments = _orderInstallmentBusiness.GetInstallmentsByParticipantInfo(searchType, installments, term, customeStripeErrorMessage, firstName, lastName);

            var pagedInstallments = installments.AsEnumerable().OrderByDescending(i => i.InstallmentDate).Pagination(paginationModel);

            foreach (var installment in pagedInstallments)
            {
                var userCreditCards = _userCreditCardBusiness.GetUserCreditCards(installment.OrderItem.Order.User.Id);
                var defaultCreditCard = userCreditCards?.FirstOrDefault(c => c.IsDefault);
                var installmentLastDigit = defaultCreditCard != null ? defaultCreditCard.LastDigits : string.Empty;
                //Get Installment payment details of payment
                var installmentTransaction = _transactionActivityBusiness.GetTransactionsForInstallmentReport(installment);

                var installmentPaymentDetail = installmentTransaction?.PaymentDetail;

                dataSource.Add(new InstallmentsViewModel()
                {
                    Id = installment.Id,
                    ClubDomain = installment.OrderItem.Order.Club?.Domain,
                    Season = installment.OrderItem.Season.Title,
                    Program = installment.OrderItem.ProgramSchedule.Program.Name,
                    ScheduleAndDate = _programBusiness.GetProgramDateWithScheduleTitleAndSchedulePart(installment.OrderItem.ProgramSchedule, installment.ProgramSchedulePart),
                    ParticipantName = installment.OrderItem.FirstName + " " + installment.OrderItem.LastName,
                    TransactionDate = installmentTransaction != null ? DateTimeHelper.GetIsoDateFormat(installmentTransaction.TransactionDate) : string.Empty,
                    Stopped = _orderInstallmentBusiness.GetInstallmentPaymentMode(installment),
                    TransactionId = installmentPaymentDetail != null ? installmentPaymentDetail.TransactionId : string.Empty,
                    OrderMode = installment.OrderItem.Order.OrderMode.ToDescription(),
                    LastDigit = installmentLastDigit,
                    ConfirmationId = installment.OrderItem.Order.ConfirmationId,
                    SendReminder = installment.NoticeSent,
                    Expiry = defaultCreditCard != null ? string.Format(Constants.ExpiryDateFormat, defaultCreditCard.ExpiryMonth, defaultCreditCard.ExpiryYear) : string.Empty,
                    Amount = CurrencyHelper.FormatCurrencyWithPenny(installment.Amount, installment.OrderItem.Order.Club.Currency),
                    AmountPaid = CurrencyHelper.FormatCurrencyWithPenny(installment.PaidAmount, installment.OrderItem.Order.Club.Currency),
                    Balance = CurrencyHelper.FormatCurrencyWithPenny(installment.Amount - (installment.PaidAmount ?? 0), installment.OrderItem.Order.Club.Currency),
                    DueDate = DateTimeHelper.GetIsoDateFormat(installment.InstallmentDate),
                    PaidDate = installment.PaidDate != null ? DateTimeHelper.GetIsoDateFormat(installment.PaidDate.Value) : null,
                    Phone = installment.OrderItem.Order.User.PhoneNumber,
                    Email = installment.OrderItem.Order.User.Email,
                    Status = _orderInstallmentBusiness.GetInstallmentStatus(installment),
                    Description = installmentPaymentDetail != null ? installmentPaymentDetail.TransactionMessage : string.Empty,
                    SeasonDomain = installment.OrderItem.Season.Domain,
                    ClubId = installment.OrderItem.Order.Club.Id,
                    OrderItemId = installment.OrderItem.Id,

                });
            }

            decimal totalAmount = 0;
            decimal totalPaidAmount = 0;
            decimal totalBalance = 0;

            if (installments.Any())
            {
                totalAmount = installments.Sum(i => i.Amount);
                totalPaidAmount = installments.Sum(i => i.PaidAmount ?? 0);
                totalBalance = installments.Sum(i => i.Amount - (i.PaidAmount ?? 0));
            }

            var clubCurrency = Ioc.ClubBusiness.GetClubCurrency(this.GetCurrentClubDomain());
            return JsonNet(new { data = dataSource.OrderBy(c => c.User), total = installments.Count(), TotalPastInstallment = installments.Count(), TotalAmountPastInstallment = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalAmount), clubCurrency), TotalPaidAmountPastInstallment = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalPaidAmount), clubCurrency), TotalBalancePastInstallment = CurrencyHelper.FormatCurrencyWithPenny(Math.Abs(totalBalance), clubCurrency) });
        }

        public virtual ActionResult UndoFailedInstallment(long id)
        {
            var res = _orderInstallmentBusiness.UndoFailedInstallment(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }
        public virtual JsonNetResult GetSchoolsAndProviders()
        {
            var schoolsAndProviders = _clubBusiness.GetKeyValueSchoolsAndProvider();

            return JsonNet(schoolsAndProviders);
        }

        [HttpGet]
        public virtual JsonNetResult GetAllPartners()
        {
            var allPartners = _clubBusiness.GetAllPartners();

            return JsonNet(allPartners);
        }

        [HttpGet]
        public virtual ActionResult GetClubName(int clubId)
        {
            var client = Ioc.ClientBusiness.Get(clubId);
            var clubName = _clubBusiness.Get(clubId).Name;

            var model = new BillingHistoryClubViewModel()
            {
                ClubName = clubName
            };


            return JsonNet(model);
        }
        [HttpPost]
        public virtual JsonNetResult GetAllClubBillingHistories(int clubId, int skip, int take, int page, int pageSize)
        {
            var model = new List<BillingHistoryClubViewModel>();
            var clubName = _clubBusiness.Get(clubId).Name;
            var total = 0;
            try
            {
                var client = Ioc.ClientBusiness.Get(clubId);

                decimal balance = 0;
                foreach (var item in client.BillingHistories)
                {
                    balance = balance + item.Debit - item.Credit;
                    model.Add(new BillingHistoryClubViewModel()
                    {
                        BillingDate = item.BillingDate,
                        BillingId = item.BillingId,
                        Credit = item.Credit,
                        Debit = item.Debit,
                        Balance = balance,
                        Description = item.Description,
                        Id = item.Id,

                    });
                }

                total = model.Count;
                model = model.Skip(skip).Take(take).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return JsonNet(new { DataSource = model, TotalCount = total });
        }
        public virtual JsonNetResult GetClubBillingHistoryDetail(long billingId)
        {
            var billing = Ioc.ClientBusiness.GetClientBillingHistory(billingId);
            var paymentMethod = billing.TransactionActivities.First().PaymentMethod;


            var model = new object();

            if (paymentMethod == PaymentMethod.Card)
            {
                model = new
                {
                    Amount = CurrencyHelper.FormatCurrencyWithPenny(billing.Credit, CurrencyCodes.USD),
                    Description = billing.Description,
                    BillingPeriod = billing.BillingDate.ToString(Constants.DateTime_Comma) + " - " + billing.BillingDate.AddMonths(1).ToString(Constants.DateTime_Comma),
                    TransactionDate = billing.TransactionActivities.First().TransactionDate.ToString(Constants.DefaultDateFormat),
                    CreditCardNumber = billing.TransactionActivities.First().CreditCard.Brand + "  ***" + billing.TransactionActivities.First().CreditCard.LastDigits,
                    BillingId = billing.BillingId,
                    IsCreditMode = false

                };
            }
            else
            {
                model = new
                {
                    Amount = CurrencyHelper.FormatCurrencyWithPenny(billing.Credit, CurrencyCodes.USD),
                    Description = billing.Description,
                    BillingPeriod = billing.BillingDate.ToString(Constants.DateTime_Comma) + " - " + billing.BillingDate.AddMonths(1).ToString(Constants.DateTime_Comma),
                    TransactionDate = billing.TransactionActivities.First().TransactionDate.ToString(Constants.DefaultDateFormat),
                    BillingId = billing.BillingId,
                    IsCreditMode = true,
                    PaymentMethod = paymentMethod.ToDescription()
                };
            }

            return JsonNet(model, new JsonSerializerSettings());
        }

        public virtual JsonNetResult GetClubBillingHistories(int billingId)
        {
            var billing = Ioc.ClientBusiness.GetClientBillingHistory(billingId);
            var clientId = billing.ClientId;
            var clubName = _clubBusiness.GetClubByClientId(clientId).Name;
            var model = new SysAdminRefundConfirmationViewModel
            {

                Balance = CurrencyHelper.FormatCurrencyWithPenny(billing.Debit - billing.Credit, CurrencyCodes.USD),
                BillingDate = billing.BillingDate.ToString(Constants.DefaultDateFormat),
                BillingId = billing.BillingId,
                Credit = CurrencyHelper.FormatCurrencyWithPenny(billing.Credit, CurrencyCodes.USD),
                Debit = CurrencyHelper.FormatCurrencyWithPenny(billing.Debit, CurrencyCodes.USD),
                Description = billing.Description,
                Id = billing.Id,
                ClubName = clubName
            };

            return JsonNet(model, new JsonSerializerSettings());
        }

        [HttpPost]
        public virtual ActionResult ClubBillingHistories(SysAdminRefundConfirmationViewModel model)
        {
            var billingHistories = Ioc.ClientBusiness.Get(model.ClubId).BillingHistories;

            var balance = billingHistories.First().Debit - billingHistories.First().Credit;


            var errorMessage = ValidateRefundStep1(model, ModelState, balance);
            if (ModelState.IsValid)
            {
                return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });
            }

            return JsonFormResponse();
        }
        private string ValidateRefundStep1(SysAdminRefundConfirmationViewModel model, ModelStateDictionary modelState, decimal balance)
        {
            if (model.RefundAmount > balance)
            {
                ModelState.AddModelError("model.RefundAmount", "Refund amount invalid");

            }
            return string.Empty;
        }

        public virtual ActionResult SubmitRefundClub(SysAdminRefundConfirmationViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = true, Message = string.Empty, RecordsAffected = 0 };
            try
            {

                var newTransaction = new BillingTransactionActivity();
                var errorMessage = string.Empty;

                var club = _clubBusiness.Get(model.ClubId);
                var clientBillingHistory = Ioc.ClientBusiness.GetClientBillingHistory(model.Id);
                DateTime date = DateTime.UtcNow;
                var periodDate = date.ToString(Constants.DateTime_Comma) + " - " + date.AddMonths(1).ToString(Constants.DateTime_Comma);
                var billingid = clientBillingHistory.BillingId;
                var balance = clientBillingHistory.Credit;
                bool refundstatus;
                List<BillingTransactionActivity> transactions = new List<BillingTransactionActivity>();
                if (clientBillingHistory.TransactionActivities != null && clientBillingHistory.TransactionActivities.Any())
                {
                    transactions = clientBillingHistory.TransactionActivities.Where(t => t.Billing.Credit >= 0 && t.TransactionType == TransactionType.Payment && t.TransactionStatus == TransactionStatus.Success && t.PaymentMethod == PaymentMethod.Card).ToList();
                }

                var amountTobeRefund = model.RefundAmount;
                if (balance < amountTobeRefund)
                {
                    amountTobeRefund = balance;
                }
                var note = model.RefundReason != null ? model.RefundReason : "Refunded";
                decimal refundedAmount = 0;

                result.Status = true;


                if (amountTobeRefund > 0)
                {

                    var remainAmount = amountTobeRefund;

                    if (remainAmount > 0 && transactions.Any(c => c.PaymentMethod == PaymentMethod.Card))
                    {
                        var refundedResult = RefundCLubByStripe(transactions, clientBillingHistory, date, remainAmount, note, ref newTransaction);
                        refundedAmount += refundedResult.Value;

                        if (!string.IsNullOrEmpty(refundedResult.Text))
                        {
                            result.Status = false;
                            result.Message += Environment.NewLine + refundedResult.Text;
                        }

                    }
                }

                refundstatus = result.Status;
                //Add new billing 
                var description = model.RefundReason;
                var credit = balance - refundedAmount;
                var clientId = clientBillingHistory.ClientId;

                if (refundedAmount > 0)
                {

                    var billingHisory = new ClientBillingHistory()
                    {
                        BillingDate = date,
                        Debit = 0,
                        Description = description,
                        Credit = credit,
                        BillingId = billingid,
                        ClientId = clientId,

                    };
                    var client = clientBillingHistory.Client;
                    newTransaction.PaymentMethod = PaymentMethod.Card;
                    newTransaction.Amount = refundedAmount;
                    newTransaction.TransactionDate = date;
                    newTransaction.TransactionType = TransactionType.Refund;
                    billingHisory.TransactionActivities = new List<BillingTransactionActivity>();
                    billingHisory.TransactionActivities.Add(newTransaction);
                    client.BillingHistories.Add(billingHisory);

                    var res = Ioc.ClientBusiness.Update(client);
                    if (result.Status)
                    {

                        refundstatus = result.Status;
                        EmailService.Get(this.ControllerContext).SendClientRefundBillingNotification(billingid, clientId, club, refundstatus, periodDate, description);
                    }
                }
                return Json(new JResult { Status = result.Status, Message = result.Message, RecordsAffected = 0 });
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = Constants.PlayerProfile_Ajax_Request_Failed, RecordsAffected = 0 });
            }
        }
        private SelectKeyValue<decimal> RefundCLubByStripe(List<BillingTransactionActivity> transactions, ClientBillingHistory clientBillingHistory, DateTime date, decimal remainAmount, string refundNote, ref BillingTransactionActivity newTransaction)
        {
            var clientId = clientBillingHistory.ClientId;

            var stripe = new JbStripeService(string.Empty, false, clientId);
            var result = new SelectKeyValue<decimal>();

            decimal refundedAmount = 0;
            var note = refundNote;

            foreach (var billingtran in transactions)
            {
                var canRefund = billingtran.Amount;
                var creditId = billingtran.CreditCardId;


                var chargeId = billingtran.TransactionId;



                var payDetail = stripe.paymentDetails(chargeId);

                if (payDetail != null && payDetail.Status == "succeeded")
                {
                    canRefund = payDetail.Amount - payDetail.AmountRefunded;
                }
                if (canRefund == 0)
                {
                    continue;
                }
                var tmpAmount = remainAmount;

                if (canRefund < tmpAmount)
                {
                    tmpAmount = canRefund;
                }
                if (billingtran.Amount <= tmpAmount)
                {
                    tmpAmount = billingtran.Amount;
                }
                var transactionStatus = TransactionStatus.Success;
                var refundStatus = true;
                var correlationId = string.Empty;
                StripeRefund response = null;
                try
                {
                    response = stripe.RefundJumbulaClient(chargeId, tmpAmount);

                    var transactionId = response.Id;
                    //var amount = response.Amount;

                    refundStatus = true;
                    correlationId = string.Format(", refund transaction id: {0}", response.Id);
                    note = refundNote;

                    newTransaction = new BillingTransactionActivity()
                    {
                        //Amount = amount,
                        TransactionId = transactionId,
                        TransactionStatus = transactionStatus,
                        CreditCardId = creditId,
                    };
                }
                catch (StripeException ex)
                {

                    transactionStatus = TransactionStatus.Failure;
                    refundStatus = false;
                    result.Text = ex.StripeError.Error;
                }
                catch (Exception ex)
                {
                    transactionStatus = TransactionStatus.Failure;
                    refundStatus = false;
                    result.Text = ex.Message;
                }
                if (refundStatus)
                {
                    remainAmount -= tmpAmount;
                    refundedAmount += tmpAmount;
                    result.Value = refundedAmount;
                }

            }

            return result;
        }
        [HttpGet]
        public virtual ActionResult GetInfoForDebit(int clubId)
        {
            var client = Ioc.ClientBusiness.Get(clubId);
            var clubName = _clubBusiness.Get(clubId).Name;
            var today = DateTime.UtcNow;
            var model = new AddDebitViewModel()
            {
                ClubId = clubId,
                ClubName = clubName,
                Date = today
            };


            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult ConfirmInfoDebit(AddDebitViewModel model)
        {
            if (model.Date == null)
            {
                ModelState.AddModelError("model.Date", "Please select Date ");

            }
            if (model.Description == null)
            {
                ModelState.AddModelError("model.Description", "Please Inter Description ");

            }
            if (model.DebitAmount <= 0)
            {
                ModelState.AddModelError("model.DebitAmount", "Please Inter Amount ");

            }


            if (ModelState.IsValid)
            {
                return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });
            }
            else
                return JsonFormResponse();

        }
        [HttpPost]
        public virtual ActionResult CreateDebit(AddDebitViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = false };
            try
            {
                var description = model.Description;
                var date = model.Date;
                var debitAmount = model.DebitAmount;
                var clubName = model.ClubName;


                var client = Ioc.ClientBusiness.Get(model.ClubId);
                var billingId = Ioc.ClientBusiness.GenerateBillingId();
                var billingHisory = new ClientBillingHistory()
                {
                    BillingDate = date,
                    Debit = debitAmount,
                    Description = description,
                    Credit = 0,
                    BillingId = billingId,

                };

                client.BillingHistories.Add(billingHisory);

                Ioc.ClientBusiness.Update(client);
                result.Status = true;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                result = new OperationStatus() { Status = false, ExceptionMessage = Constants.M_E_CreateEdit };

            }

            return Json(new { Status = result.Status, result = "Refresh", Message = result.ExceptionMessage, RecordsAffected = 0 });
        }

        [HttpGet]
        public virtual ActionResult InfoForAutoChargeClubBilling(int clubId)
        {
            var client = Ioc.ClientBusiness.Get(clubId);
            var clubName = _clubBusiness.Get(clubId).Name;
            var accountService = Ioc.AccountingBusiness;
            var today = DateTime.UtcNow;
            var balance = accountService.GetClientBalance(client) > 0 ? accountService.GetClientBalance(client) : 0;
            var billingId = client.BillingHistories.Last(c => c.BillingDate.Month == today.Month).BillingId;
            var model = new AutoChargeClubViewModel()
            {
                Balance = balance,
                ClubId = clubId,
                Date = today,
                BillingId = billingId,
                ClubName = clubName,
                ChargeAmount = balance
            };

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult ConfirmChargeInfo(AutoChargeClubViewModel model)
        {
            if (model.Description == null)
            {
                ModelState.AddModelError("model.Description", "Please Inter Description ");

            }
            if (model.ChargeAmount > model.Balance || model.ChargeAmount <= 0)
            {
                ModelState.AddModelError("model.CreditAmount", "Please Inter Amount ");

            }
            var amount = model.ChargeAmount;
            if (amount > model.Balance)
            {
                ModelState.AddModelError("model.ChargeAmount", "Amount is invalid");

            }

            var description = model.Description;
            var date = model.Date;



            if (ModelState.IsValid)
            {
                return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });
            }
            else
                return JsonFormResponse();
        }

        [HttpPost]
        public virtual ActionResult AutoChargeClubBilling(AutoChargeClubViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = false };
            try
            {
                var today = model.Date;
                var periodDate = today.ToString(Constants.DateTime_Comma) + " - " + today.AddMonths(1).ToString(Constants.DateTime_Comma);
                var billingId = model.BillingId;
                var club = _clubBusiness.Get(model.ClubId);
                var clientId = club.Client.Id;
                var description = model.Description;
                var chargeFee = model.ChargeAmount;
                var balance = model.Balance;
                var stripeService = new JbStripeService(string.Empty, false);

                if (balance > 0)
                {

                    var billingStatus = stripeService.ChargeClient(club.Client, club.Name, today, billingId, description, chargeFee);

                    var subject = Constants.F_ClientBillingSubject;
                    var text = "charge";
                    EmailService.Get(this.ControllerContext).SendClientBillingNotification(billingId, clientId, club, billingStatus, periodDate, subject, text, description);

                }

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                result = new OperationStatus() { Status = false, ExceptionMessage = Constants.M_E_CreateEdit };

            }

            return Json(new { Status = true, Message = result.ExceptionMessage, RecordsAffected = 0 });
        }
        [HttpGet]
        public virtual ActionResult GetCreditInfo(int clubId)
        {
            var client = Ioc.ClientBusiness.Get(clubId);
            var clientid = client.Id;
            var ClubName = _clubBusiness.Get(clubId).Name;
            var today = DateTime.UtcNow;
            var accountService = Ioc.AccountingBusiness;
            var balance = accountService.GetClientBalance(client);

            var model = new CreditViewModel()
            {
                Balance = balance,
                ClubId = clubId,
                ClubName = ClubName,
                Date = today
            };


            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult ConfirmCreditInfo(CreditViewModel model)
        {
            if (string.IsNullOrEmpty(model.Description))
            {
                ModelState.AddModelError("model.Description", "Please Inter Description ");

            }
            if (model.CreditAmount <= 0)
            {
                ModelState.AddModelError("model.CreditAmount", "Please Inter Amount ");

            }
            if (model.SelectedPaymentMethod == "0")
            {
                ModelState.AddModelError("model.SelectedPaymentMethod", "Please select the payment method");

            }

            var description = model.Description;
            var date = model.Date;
            var paymentmethod = model.SelectedPaymentMethod;
            var amount = model.CreditAmount;

            if (ModelState.IsValid)
            {
                return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });
            }
            else
                return JsonFormResponse();
        }
        public virtual ActionResult SubmitCreditClub(CreditViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = false };
            try
            {
                var club = _clubBusiness.Get(model.ClubId);
                var client = Ioc.ClientBusiness.Get(model.ClubId);
                var clientid = client.Id;
                var currency = club.Currency;
                var description = model.Description;
                var oldBalance = model.Balance;
                var credit = model.CreditAmount;
                var date = model.Date;
                var periodDate = date.ToString(Constants.DateTime_Comma) + " - " + date.AddMonths(1).ToString(Constants.DateTime_Comma);
                var paymentmethodvalue = model.SelectedPaymentMethod;
                PaymentMethod paymentmethodText = (PaymentMethod)int.Parse(paymentmethodvalue);

                if (model.CreditAmount < 0)
                {
                    var errorMsg = string.Format(Constants.E_Amount_Between, CurrencyHelper.FormatCurrencyWithPenny(1, currency), CurrencyHelper.FormatCurrencyWithPenny(model.Balance, currency));
                    return Json(new { Status = result.Status, result = "Refresh", Message = errorMsg, RecordsAffected = 0 });
                }


                var transactonPaymenth = paymentmethodvalue;
                var billingHisory = new ClientBillingHistory()
                {
                    BillingDate = date,
                    Debit = 0,
                    Description = description,
                    Credit = credit,
                    BillingId = "-",
                    ClientId = clientid,

                };

                var transaction = new BillingTransactionActivity() { Amount = credit, PaymentMethod = EnumHelper.ParseEnum<PaymentMethod>(paymentmethodvalue), TransactionDate = date, TransactionType = TransactionType.Payment };

                transaction.TransactionStatus = TransactionStatus.Success;
                transaction.Billing = billingHisory;
                billingHisory.TransactionActivities = new List<BillingTransactionActivity>();
                billingHisory.TransactionActivities.Add(transaction);
                client.BillingHistories.Add(billingHisory);


                Ioc.ClientBusiness.Update(client);

                var billingid = billingHisory.Id;
                result.Status = true;

                EmailService.Get(this.ControllerContext).SendClientBillingByCreditNotification(billingid, clientId: clientid, club: club, billingStatus: true, periodDate: periodDate, description: description, paymentmethod: paymentmethodText.ToString());

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                result = new OperationStatus() { Status = false, ExceptionMessage = Constants.M_E_CreateEdit };

            }

            return Json(new { Status = result.Status, result = "Refresh", Message = result.ExceptionMessage, RecordsAffected = 0 });
        }

        public virtual JsonNetResult GetRangeInstallmentDate()
        {
            var endDate = _clubBusiness.GetClubDateTime(ActiveClub.Id, DateTime.UtcNow);
            var startDate = endDate.AddDays(-7);

            var model = new { StartDate = startDate, EndDate = endDate };
            return JsonNet(model);
        }

        public ActionResult GetStripeErrorMessage()
        {
            var result = _orderInstallmentBusiness.GetStripeErrorMessage();

            result.AddItemToFirst(DefaultDropdownItem.All);
            result.AddItemAtEnd(DefaultDropdownItem.NotListed);

            return JsonNet(result);
        }

    }
}
