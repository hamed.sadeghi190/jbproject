Drop Table DataMigrations

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DataMigrations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [tinyint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[RunDate] [datetime] NULL,
	[Source] [varbinary](max) NULL,
	[Result] [varbinary](max) NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.DataMigrations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


