﻿using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SportsClub.Areas.Dashboard.Models
{
    public class SubscriptionParticipantInternalViwModel
    {
        public SubscriptionParticipantInternalViwModel()
        {
            Sessions = new List<SubscriptionSessionItem>();
        }
        public string StudentName { get; set; }
        public string TeacherName { get; set; }
        public string ProgramName { get; set; }
        public string StrStartTime { get; set; }
        public string StrEndTime { get; set; }
        public string Grade { get; set; }
        public int GradeOrder { get; set; }

        public string Gender { get; set; }
        public List<SubscriptionSessionItem> Sessions { get; set; }
        public string HealthMedical { get; set; }
        public string IssueConcems { get; set; }
        public string Allergies { get; set; }
        public string Deitary { get; set; }
        public string Medication { get; set; }
        public int Number { get; set; }
        public string Other { get; set; }
        public string TuitionName { get; set; }
        public OrderItemMode orderMode { get; set; }
        public long ProgramId {get; set;}
        public TimeOfClassFormation ScheduleMode { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }

        public List<int> sessionId { get; set; }

        //parent
        public string FirstParentName { get; set; }
        public string FirstParentHome { get; set; }
        public string FirstParentPhone { get; set; }
        public string FirstParentRelationShip { get; set; }
        public string FirstParentEmail { get; set; }
        public string SecoundParentName { get; set; }
        public string SecoundParentHome { get; set; }
        public string SecoundParentPhone { get; set; }
        public string SecoundParentRelationShip { get; set; }
        public string SecoundParentEmail { get; set; }
        //emergency contact
        public string FirstEmergencyName { get; set; }
        public string FirstEmergencyHome { get; set; }
        public string FirstEmergencyPhone { get; set; }
        public string FirstEmergencyRelationShip { get; set; }
        public string FirstEmergencyEmail { get; set; }
        public string SecoundEmergencyName { get; set; }
        public string SecoundEmergencyHome { get; set; }
        public string SecoundEmergencyPhone { get; set; }
        public string SecoundEmergencyRelationShip { get; set; }
        public string SecoundEmergencyEmail { get; set; }

        //Authuraize pick up

        public string FirstAuthuraizePikeUpName { get; set; }
        public string FirstAuthuraizePikeUpHome { get; set; }
        public string FirstAuthuraizePikeUpPhone { get; set; }
        public string FirstAuthuraizePikeUpRelationShip { get; set; }
        public string FirstAuthuraizePikeUpEmail { get; set; }
        public string SecoundAuthuraizePikeUpName { get; set; }
        public string SecoundAuthuraizePikeUpHome { get; set; }
        public string SecoundAuthuraizePikeUpPhone { get; set; }
        public string SecoundAuthuraizePikeUpRelationShip { get; set; }
        public string SecoundAuthuraizePikeUpEmail { get; set; }
    }

    public class SubscriptionSessionItem
    {
        public DateTime SessionDateTime { get; set; }

        public bool IsPresent { get; set; }
        public string SessionTitle { get; set; }

    }
}
