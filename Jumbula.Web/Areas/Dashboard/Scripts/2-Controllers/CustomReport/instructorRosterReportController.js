﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('instructorRosterReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};

        $scope.Model = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/GetRosterReportDetail', { params: { programId: $scope.Model._SelectedPrograms } }).success(function (data, status, headers, config) {

            dataInfo = data;
            $scope.ProgramName = data.ProgramName;
            $scope.ClubName = data.ClubName;
            $scope.RoomAssignment = data.Room;
            $scope.DayOfWeek = data.Days;
            $scope.Date = data.Date;

            manipulateRosterGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        var model = $scope.Model;

        var manipulateRosterGrid = function () {

            $scope.RosterReportDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/InstructorRosterReportRun',
                            type: "POST",
                            data: function () {
                                return {
                                    programId: model._SelectedPrograms,
                                };
                            },

                        },

                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },

                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
            });

            $scope.RosterReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: dataInfo.ProgramName + "_" + $scope.seasonName + " (Roster).xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;
                    rows.unshift({
                        cells: [{ value: $scope.seasonName + ": " + dataInfo.ProgramName + " " + dataInfo.ClubName }]
                    });
                    rows.push({
                        cells: [{ value: "" }]
                    });
                    rows.push({
                        cells: [{
                            value: dataInfo.Days
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: "Room: " + dataInfo.Room
                        }]
                    });
                    rows.push({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.RosterReportDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5
                },

                sortable: true,
                columnMenu: true,
                columns: [

                    {
                        field: "ParticipantFirstName",
                        title: "First name",
                        width: "230px"
                    }, {
                        field: "ParticipantLastName",
                        title: "Last name",
                        width: "230px"
                    }, {
                        field: "ParticipantGender",
                        title: "Gender",
                        width: "230px"
                    },
                    {
                        field: "SchoolGrade",
                        title: "Grade",
                        width: "230px"
                    },
                    {
                        field: "ParentFirstName",
                        title: "Parent first name",
                        width: "230px"
                    }, {
                        field: "ParentLastName",
                        title: "Last name",
                        width: "230px"
                    }, {
                        field: "ParentEmail",
                        title: "Email",
                        width: "230px"
                    },
                    {
                        field: "ParentPrimaryPhone",
                        title: "Primary phone",
                        width: "230px"
                    },
                    {
                        field: "EmergencyContactFirstName",
                        title: "Emergency contact first name",
                        width: "230px"
                    }, {
                        field: "EmergencyContactLastName",
                        title: "Last name",
                        width: "230px"
                    }, {
                        field: "EmergencyContactPhone",
                        title: "Phone",
                        width: "230px"
                        } 
                ]
            }
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#RosterReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();