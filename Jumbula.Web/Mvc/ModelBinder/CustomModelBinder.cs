﻿using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;
using OfficeOpenXml.FormulaParsing.Utilities;

namespace Jumbula.Web.Mvc.ModelBinder
{
    /// <summary>
    /// Trim string input fields
    /// </summary>
    /// <remarks>
    /// Password fields are also trimmed, ok?
    /// If not desirable, perhaps use (propertyDescriptor.Attributes[typeof(PasswordAttribute)] != null to prevent
    /// </remarks>
    public class CustomModelBinder : DefaultModelBinder
    {
        protected override void SetProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, object value)
        {
            if (propertyDescriptor.PropertyType == typeof(string))
            {
                if (value != null)
                {
                    value = ((string)value).Trim();

                    // Replace tab character with space
                    if (((string)value).Contains("\t"))
                    {
                        value = ((string)value).Replace("\t", " ");
                    }

                    // Reset the value to null if we end up with a empty string
                    // This behaviour can also be changed as a configuration option?
                    // Is this a good idea to change to null?
                    if ((string)value == string.Empty)
                    {
                        value = null;
                    }
                }
            }

            if (propertyDescriptor.PropertyType == typeof(decimal))
            {
                if (!bindingContext.ModelState.IsValid)
                {
                    if (bindingContext.ModelState.TryGetValue($"model.{propertyDescriptor.Name}", out var priceValue))
                    {
                        if (priceValue != null && priceValue.Errors.Count > 0 && !priceValue.Value.IsNumeric())
                        {
                            bindingContext.ModelState.Remove(propertyDescriptor.Name);
                            bindingContext.ModelState.AddModelError(propertyDescriptor.Name, string.Format(
                                Common.Constants.Validation.PriceRegexErrorMessage,
                                bindingContext.PropertyMetadata.First(x => x.Key == propertyDescriptor.Name).Value.DisplayName));
                        }
                    }
                }
            }

            base.SetProperty(controllerContext, bindingContext, propertyDescriptor, value);
        }
    }
}