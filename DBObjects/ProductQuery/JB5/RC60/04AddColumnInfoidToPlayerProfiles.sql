 
alter table PlayerProfiles add [InfoId] [int] NULL
GO
ALTER TABLE [dbo].[PlayerProfiles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.PlayerProfiles_dbo.PlayerInfoes_InfoId] FOREIGN KEY([InfoId])
REFERENCES [dbo].[PlayerInfoes] ([Id])
GO