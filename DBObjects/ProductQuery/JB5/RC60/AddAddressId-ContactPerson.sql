ALTER table ContactPersons Add AddressId int null
Go

ALTER TABLE [dbo].[ContactPersons]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ContactPersons_dbo.PostalAddresses_AddressId] FOREIGN KEY([AddressId])
REFERENCES [dbo].[PostalAddresses] ([Id])