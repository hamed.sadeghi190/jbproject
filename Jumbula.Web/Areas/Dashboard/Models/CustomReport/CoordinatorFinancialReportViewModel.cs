﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SportsClub.Models;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CoordinatorFinancialReportViewModel
    {
        public string ClassName { get; set; }
        public string ProviderName { get; set; }
        public decimal ClassFee { get; set; }
        public int TotalRegistrations { get; set; }
        public int TotalPTAFundedScholarships { get; set; }
        public int TotalCashPaymenttoPTA { get; set; }
        public int TotalProviderFundedScholarships { get; set; }
        public int TotalPaidEnrollments { get { return TotalRegistrations - (TotalPTAFundedScholarships+ TotalProviderFundedScholarships); } }   
        public decimal TotalPTAActivityFeesAssessed { get; set; }
 

    }

}