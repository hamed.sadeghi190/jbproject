﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('subscriptionAddEditStep2Point5Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation) {

        $scope.baseUrl = window.location.origin + "/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.MC.stateParams = $stateParams;

        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;

        $http.get('Dashboard/Program/SubscriptionCreateEditStep2Point5', { params: { programId: $stateParams.programId } })
             .success(function (data, status, headers, config) {

                 $scope.Model = data;
             });

        $scope.removeSubscriptinItem = function (subscrtiptionItemId) {
            jbConfirmation.message('Are you sure you want to delete this item?').yes(function () {

                for (var i = 0; i < $scope.Model.length; i++) {
                    if ($scope.Model[i].Id == subscrtiptionItemId) {
                        $scope.Model.splice(i, 1);
                        break;
                    }
                }
            })
            .confirm();
        }

        $scope.addSubscriptionItem = function () {
            $scope.Model.push({ Id: (Math.random() * 100000), StartDate: '', EndDate: '', DueDate: '', Amount: 0, editMode: true });
        }

        $scope.save = function () {
            $http.post('Dashboard/Program/SubscriptionCreateEditStep2Point5', { model: $scope.Model, programId: $stateParams.programId })
                .success(function (data, status, headers, config) {
                    if (data.Status) {
                        $state.go('ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                    }
                })
        }

        $scope.editSubscriptionItem = function (subscriptionItem) {

            if (subscriptionItem.editMode) {
                subscriptionItem.editMode = false
            }
            else {
                subscriptionItem.editMode = true;
                subscriptionItem._Amount = subscriptionItem.Amount;
                subscriptionItem._DueDate = subscriptionItem.DueDate;
                subscriptionItem._StartDate = subscriptionItem.StartDate;
                subscriptionItem._EndDate = subscriptionItem.EndDate;
            }
        }

        $scope.cancelEditItem = function (subscriptionItem) {
            subscriptionItem.Amount = subscriptionItem._Amount;
            subscriptionItem.DueDate = subscriptionItem._DueDate;
            subscriptionItem.StartDate = subscriptionItem._StartDate;
            subscriptionItem.EndDate = subscriptionItem._EndDate;

            subscriptionItem.editMode = false;
        }

        $scope.saveSubscriptionItem = function (subscriptionItem) {

            $http.post('Dashboard/Program/RegenerateSubscriptionItems', { model: $scope.Model, programId: $stateParams.programId, editedSubscriptionItemId: subscriptionItem.Id })
               .success(function (data, status, headers, config) {
                   if (data.Status) {
                       $scope.Model = data.SubscriptionItems;
                   }
                   else {
                       jbToast.error("Some inputs are invalid.");
                   }
               })
        }

        $scope.formatDate = function (date) {

            if (date != '' && date != null && date != 'undefined') {
                return new Date(date).format('mm/dd/yyyy');
            }
        }

        $scope.back = function () {
            $state.go('ProgramAddEditStep2', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
        }

        if ($state.returnByBack()) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();

