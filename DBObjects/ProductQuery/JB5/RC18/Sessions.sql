
/****** Object:  Table [dbo].[ProgramScheduleParts]    Script Date: 7/27/2017 11:30:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProgramScheduleParts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramScheduleId] [bigint] NOT NULL,
	[StartDate] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[EndDate] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
 CONSTRAINT [PK_dbo.ProgramScheduleParts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProgramScheduleParts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramScheduleParts_dbo.ProgramSchedules_ProgramScheduleId] FOREIGN KEY([ProgramScheduleId])
REFERENCES [dbo].[ProgramSchedules] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ProgramScheduleParts] CHECK CONSTRAINT [FK_dbo.ProgramScheduleParts_dbo.ProgramSchedules_ProgramScheduleId]
GO




Drop table [dbo].[ProgramSessions]


go

/****** Object:  Table [dbo].[ProgramSessions]    Script Date: 7/26/2017 6:39:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ProgramSessions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StartDateTime] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[EndDateTime] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[ProgramSchedulePartId] [int] NOT NULL DEFAULT ((0)),
 CONSTRAINT [PK_dbo.ProgramSessions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ProgramSessions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramSessions_dbo.ProgramScheduleParts_ProgramSchedulePartId] FOREIGN KEY([ProgramSchedulePartId])
REFERENCES [dbo].[ProgramScheduleParts] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ProgramSessions] CHECK CONSTRAINT [FK_dbo.ProgramSessions_dbo.ProgramScheduleParts_ProgramSchedulePartId]
GO

Drop Table OrderSessions

Go

/****** Object:  Table [dbo].[OrderSessions]    Script Date: 7/26/2017 6:44:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrderSessions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderItemId] [bigint] NOT NULL,
	[ProgramSessionId] [int] NULL,
 CONSTRAINT [PK_dbo.OrderSessions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OrderSessions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderSessions_dbo.OrderItems_OrderItemId] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[OrderItems] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OrderSessions] CHECK CONSTRAINT [FK_dbo.OrderSessions_dbo.OrderItems_OrderItemId]
GO

ALTER TABLE [dbo].[OrderSessions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderSessions_dbo.ProgramSessions_ProgramSessionId] FOREIGN KEY([ProgramSessionId])
REFERENCES [dbo].[ProgramSessions] ([Id])
GO

ALTER TABLE [dbo].[OrderSessions] CHECK CONSTRAINT [FK_dbo.OrderSessions_dbo.ProgramSessions_ProgramSessionId]
GO


