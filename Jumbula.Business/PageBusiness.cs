﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class PageBusiness : IPageBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IRepository<JbPage> _jbPageRepository;

        public PageBusiness(IClubBusiness clubBusiness, IRepository<JbPage> jbPageRepository)
        {
            _clubBusiness = clubBusiness;
            _jbPageRepository = jbPageRepository;
        }

        public JbPage Get(int clubId, SaveType? saveType)
        {
            var page = _jbPageRepository.GetList().SingleOrDefault(p => p.ClubId == clubId && p.SaveType == (saveType.HasValue ? saveType : SaveType.Publish));

            if (page != null)
            {
                var fakePage = JsonHelper.JsonComplexDeserialize<JbPage>(page.JsonBody);

                if (fakePage.Id == 0)
                    fakePage.Id = page.Id;

                fakePage.Id = page.Id; //MGH: for publish page

                return fakePage;
            }
            return new JbPage();
        }

        public JbPage Get(string clubDomain, SaveType? saveType)
        {
            var page = _jbPageRepository.GetList().SingleOrDefault(p => p.Club.Domain == clubDomain && p.SaveType == (saveType.HasValue ? saveType : SaveType.Publish));
            if (page == null)
                return null;

            var fakePage = JsonHelper.JsonComplexDeserialize<JbPage>(page.JsonBody);
            if (fakePage.Id == 0)
                fakePage.Id = page.Id;

            fakePage.Id = page.Id;//MGH: for publish page

            return fakePage;
        }

        public IQueryable<JbPage> GetList()
        {
            return _jbPageRepository.GetList();
        }
        public OperationStatus CreateEdit(JbPage page)
        {
            page.JsonBody = JsonHelper.JsonSerializer(page, new JsonSerializerSettings() { Formatting = Formatting.None });

            var jbPages = GetList().Where(p => p.ClubId == page.ClubId);

            var isUpdate = false;

            if (jbPages.Any(p => p.SaveType == page.SaveType))
            {
                isUpdate = true;

                var PageBiz = this.Get(page.ClubId, page.SaveType);
                if (PageBiz.Id != 0)
                {
                    page.Id = PageBiz.Id;//update published record

                    //update page id in JsonBody in published record
                    if (page.SaveType == SaveType.Publish)
                    {
                        page.JsonBody = JsonHelper.JsonSerializer(page, new JsonSerializerSettings() { Formatting = Formatting.None });
                    }
                }
            }

            if (page.Id == 0 || !isUpdate)
            {
                page.CreateDate = DateTime.UtcNow;
                var res = _jbPageRepository.Create(page);

                return res;
            }
            else
            {
                var r = _jbPageRepository.Update(page);

                return r;
            }
        }
        public  OperationStatus Delete(int id)
        {
            return _jbPageRepository.Delete(id);
        }

        public OperationStatus DeleteClubPage(int clubId)
        {
            var result = _jbPageRepository.GetList().ToList().RemoveAll(c => c.ClubId == clubId);

            return new OperationStatus { Status = result > 0 };
        }

        public JbPage GetDefaultPage(Club club)
        {
            var page = new JbPage();

            page.Title = club.Name + " Jumbula Home";
            //page.SeasonId = season.Id;
            page.Header.Title = club.Name;
            page.Header.ShowLogo = true;
            page.ClubId = club.Id;

            // page.Header.LogoUrl = ServiceFactory.Get<ClubBusiness>().GetClubLogoURL(club.Domain, club.Logo, 1);

            page.CreateDate = DateTime.UtcNow;
            page.Name = "Jumbula_Home_" + club.Id;

            page.Footer.Address = club.ClubLocations.Any() ? club.ClubLocations.First().PostalAddress.Address : string.Empty;
            page.Footer.Email = club.ContactPersons.Any() ? club.ContactPersons.First().Email : string.Empty;
            page.Footer.Phone = club.ContactPersons.Any() ? club.ContactPersons.First().Phone : string.Empty;
            page.Footer.Website = club.Site;
            page.Footer.Visible = false;

            page.Setting = new JbPSetting()
            {
                RegLiksAvailableColor = "#005f2e",
                RegLiksUnavailableColor = "#006933"
            };
            if (club.ClubLocations.Any())
            {
                page.Address = new PostalAddress
                {
                    Lat = club.ClubLocations.First().PostalAddress.Lat,
                    Lng = club.ClubLocations.First().PostalAddress.Lng
                };
            }
            else
            {
                page.Address = new PostalAddress();
            }

            //page.Elements.Add(new JbPTitlePack() { Title = season.Title, Subtitle = "", Paragraph = season.Description });
            page.Elements.Add(
                new JbPTab()
                {
                    Title = "Home",
                    TabLink = "Home",
                    TypeTitle = "Tab",

                    Cover = new JbPCover()
                    {

                        Title = club.Name,
                        //Paragraph = club.Name,
                        Visible = true,
                        BackgroundMode = JbPCoverBGMode.Gallery,
                        Background = "/Images/Covers/gray-banner.png",
                        TypeTitle = "Cover"
                    },

                    Appearance = new JbPAppearance()
                    {
                        EnabledAppearance = false,
                        TabTextColor = "#01a3f1",
                        TabHoverTextColor = "#333",
                        TabBackgroundColor = "#fff",
                        TabBorderColor = "#01a3f1",
                        TabSelectedBackgroundColor = "#01a3f1",
                        TabSelectedTextColor = "#fff",
                        TabSelectedBorderColor = "#000",

                    },
                    Elements = new List<JbPBaseElement>()
                    {

                        //new JbPTitlePack() { Title = club.Name, Subtitle = "", Paragraph = "This is your paragraph" },

                        /* new JbPSeasonCalendar()
                        {
                            ElementId = "SeasonCalendar0",
                            TypeTitle = "Calendar",
                        },

                        new JbPSeasonCalendar()
                        {
                            ElementId = "SeasonCalendar1",
                            TypeTitle = "Calendar",
                            //Title = season.Title,

                            //SeasonId = season.Id
                        }*/
                    }


                });

            return page;
        }

        #region Replicate

        public OperationStatus Replicate(int districtId, int? memberId)
        {
            var members = _clubBusiness.GetRelatedClubs(districtId).Where(d => !d.IsDeleted);

            if (memberId.HasValue)
            {
                members = members.Where(m => m.Id == memberId.Value);
            }

            var sourcePage = Get(districtId, SaveType.Publish);

            var result = Replicate(sourcePage, members.ToList());

            return result;
        }

        public OperationStatus Replicate(JbPage sourcePage, List<Club> destinationClubs)
        {
            OperationStatus result = null;

            foreach (var club in destinationClubs)
            {
                result = Replicate(sourcePage, club);
            }

            return result;
        }


        public OperationStatus Replicate(JbPage sourcePage, Club destinationClub)
        {
            var destinationPage = GetReplicate(sourcePage, destinationClub);

            if (destinationClub.Pages.Any(p => p.SaveType == SaveType.Draft))
            {
                destinationPage.Id = destinationClub.Pages.Single(p => p.SaveType == SaveType.Draft).Id;
            }

            destinationPage.SaveType = SaveType.Draft;
            var result = CreateEdit(destinationPage);

            if (result.Status)
            {

                if (destinationClub.Pages.Any(p => p.SaveType == SaveType.Publish))
                {
                    var puplishedPage = destinationClub.Pages.Single(p => p.SaveType == SaveType.Publish);

                    puplishedPage = GetReplicate(sourcePage, destinationClub);

                    destinationPage = puplishedPage;
                }

                destinationPage.SaveType = SaveType.Publish;
                result = CreateEdit(destinationPage);
            }

            return result;
        }

        public JbPage GetReplicate(JbPage sourcePage, Club destinationClub)
        {
            var now = DateTime.UtcNow;
            var result = new JbPage();

            result.Address = destinationClub.ClubLocations.First().PostalAddress;

            result.ClubId = destinationClub.Id;
            result.CreateDate = now;
            result.Name = sourcePage.Name;
            result.PageMode = sourcePage.PageMode;
            result.SaveType = sourcePage.SaveType;
            result.Title = destinationClub.Name;
            result.Elements = GetReplicatePageElements(sourcePage.Elements, destinationClub);
            result.Header = GetReplicatePageHeader(sourcePage.Header, destinationClub);
            result.Footer = GetReplicatePageFooter(sourcePage.Footer, destinationClub);
            result.Setting = sourcePage.Setting;

            result.JsonBody = JsonHelper.JsonSerializer(result, new JsonSerializerSettings() { Formatting = Formatting.Indented });

            return result;
        }

        private List<JbPBaseElement> GetReplicatePageElements(List<JbPBaseElement> sourceElements, Club destinationClub)
        {
            var result = new List<JbPBaseElement>();

            foreach (var item in sourceElements.Where(e => e is JbPTab))
            {
                var tab = ((JbPTab)item);

                tab.Elements.RemoveAll(e => (e is JbPGrid || e is JbPGridSetting || e is JbPSeasonGrid || e is JbPRegBtn || e is JbPSeasonCalendar) || e is JbPSection || e is JbPSectionColumn);

                result.Add(item);
            }

            return result;
        }

        private JbPHeader GetReplicatePageHeader(JbPHeader sourceHeader, Club destinationClub)
        {
            var result = new JbPHeader();

            result.ElementId = sourceHeader.ElementId;
            result.Menu = sourceHeader.Menu;
            result.ShowLogo = sourceHeader.ShowLogo;
            result.Title = destinationClub.Name;
            result.TitleColor = sourceHeader.TitleColor;
            result.TypeTitle = sourceHeader.TypeTitle;

            result.LogoRedirecturl = sourceHeader.LogoRedirecturl;
            result.LogoUrl = _clubBusiness.GetClubLogoURL(destinationClub.Domain, destinationClub.Logo);

            return result;
        }

        private JbPFooter GetReplicatePageFooter(JbPFooter sourceFooter, Club destinationClub)
        {
            var result = new JbPFooter();

            result.Address = destinationClub.ClubLocations.First().PostalAddress.Address;
            result.BackgroundColor = sourceFooter.BackgroundColor;
            result.ElementId = sourceFooter.ElementId;

            var contact = destinationClub.ContactPersons.FirstOrDefault(c => c.IsPrimary);

            if (contact != null)
            {
                result.Email = contact.Email;
                result.Phone = contact.Phone;
            }

            result.TypeTitle = sourceFooter.TypeTitle;
            result.Visible = sourceFooter.Visible;
            result.Website = destinationClub.Site;
            result.Title = sourceFooter.Title;

            return result;
        }

        public JbPage Get(int id)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
