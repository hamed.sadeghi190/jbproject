﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemConfirmRefundController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cancelOrderService', 'jbToast', function ($scope, $http, $log, $stateParams, $state, cancelOrderService, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.Model = cancelOrderService.get();

        $scope.cancelAction = function () {
            cancelOrderService.set($scope.Model);
            $state.back();
        }
        $scope.cancelAndRefundOrder = function () {
            
            $('#submitCancelAndRefundOrder').prop("disabled", true);
            $http.post($scope.baseUrl + '/SubmitRefundClub', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        jbToast.success("The refund operation was successful.", "");
                        $state.go('SystemClubBillingHistory', { clubId: $stateParams.clubId });
                        $scope.MC.backQueueItems.pop();
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                        jbToast.error("The refund faild");
                    }
                    $('#submitCancelAndRefundOrder').prop("disabled", false);
                });

        };
    }]);
})();