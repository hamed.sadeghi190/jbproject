
IF OBJECT_ID('[dbo].[EventOrderItems]') IS NULL
BEGIN
	CREATE TABLE [dbo].[EventOrderItems](
		[Id] [int] NOT NULL,
		[OrderItemId] [nvarchar](128) NULL,
		[Start] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
		[End] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'))

END
GO

---<< ADD PRIMARY KEY >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='PK_dbo.EventOrderItems')
BEGIN
	ALTER TABLE [dbo].[EventOrderItems] ADD CONSTRAINT [PK_dbo.EventOrderItems] PRIMARY KEY CLUSTERED
	(
	Id ASC
	)
END
GO

--<< EDIT COLUMNS >>--
IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=OBJECT_ID('[dbo].[EventOrderItems]') AND name='End')
BEGIN
	ALTER TABLE [dbo].[EventOrderItems] 
	ALTER COLUMN [End] [datetime] NULL
END

---<< ADD FOREIGN KEYS >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE name='FK_dbo.EventOrderItems_dbo.OrderItems_Id')
BEGIN
	ALTER TABLE [dbo].[EventOrderItems]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EventOrderItems_dbo.OrderItems_Id] FOREIGN KEY([Id])
		REFERENCES [dbo].[OrderItems] ([Id])
END
GO

