﻿using System.Collections.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SelectedSchoolsViewModel
    {
        public SelectedSchoolsViewModel()
        {
            // do some things good.
        }

        public List<int> _SelectedSchools { get; set; }

        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public string SeasonDomain { get; set; }

        public bool IsAllSchools { get; set; }

        public string Term { get; set; }

    }

}