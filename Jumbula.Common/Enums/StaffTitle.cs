﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum StaffTitle : byte
    {
        [Description("Principal")]
        SchoolPrincipal = 1,
        [Description("Vice principal")]
        SchoolVicePrincipal = 2,
        [Description("Regional director")]
        SchoolRegionalDirector = 3,
        [Description("Area manager")]
        AreaManager = 26,
        //[Description("Administrative staff")]
        //SchoolAdministrativeStaff = 6,
        [Description("Assistant director")]
        SchoolAssistantDirector = 5,
        [Description("PTA president")]
        SchoolPTAPresident = 7,
        [Description("PTA vice president")]
        SchoolPTAVicePresident = 8,
        [Description("PTA enrichment coordinator")]
        SchoolPTAEnrichmentCordinator = 9,
        [Description("Onsite manager")]
        SchoolOnsiteManager = 10,
        [Description("Onsite coordinator")]
        SchoolOnsiteCoordinator = 11,
        [Description("Owner")]
        ProviderOwner = 21,
        [Description("Director")]
        ProviderDirector = 22,
        [Description("Manager ")]
        ProviderManager = 23,
        [Description("Administrative assistant")]
        ProviderAdministrativeAssistant = 24,
        [Description("Instructor")]
        ProviderInstructor = 25,
        [Description("Sales contact")]
        SalesContact = 27,
        [Description("Superintendent")]
        SchoolSuperintendent = 50,
        [Description("Other")]
        Other = 51,
    }
}
