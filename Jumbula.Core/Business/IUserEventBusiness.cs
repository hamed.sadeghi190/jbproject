﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IUserEventBusiness
    {
        UserEvent Get(int id);

        UserEvent Get(long? programId, int sessionId, int userId);

        UserEvent Get(long? programId, DateTime dateTime, int userId, UserEventType type);

        UserEvent Get(long? programId, int? clubId, DateTime dateTime, int userId, UserEventType type);

        IQueryable<UserEvent> GetList(Expression<Func<UserEvent, bool>> predicate);

        IQueryable<UserEvent> GetList();

        IQueryable<UserEvent> GetList(List<long> programIds);

        OperationStatus CreateEdit(UserEvent userEvent);

        OperationStatus Create(UserEvent userEvent);

        OperationStatus Update(UserEvent userEvent);
    }
}
