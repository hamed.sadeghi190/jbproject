--@mn--2014/09/15--

If Object_ID('[dbo].[JbForms]') Is Null
BEGIN
CREATE TABLE [dbo].[JbForms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JsonElements] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,	
	[HelpText] [nvarchar](max) NULL,
	[RefEntityName] [nvarchar] (100) NULL,
	[RefEntityId] int NOT NULL,
	[CreatedDate] datetime NOT NULL default(getdate()),
	[LastModifiedDate] datetime NOT NULL)
END
GO

--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.JbForms')
BEGIN
	ALTER TABLE [dbo].[JbForms] ADD CONSTRAINT [PK_dbo.JbForms] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

--<< ADD COLUMNS >>--
IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='RefEntityName')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD RefEntityName nvarchar(100) NULL;

END
GO

IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='RefEntityId')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD RefEntityId int NOT NULL;
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='CreatedDate')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD CreatedDate datetime NULL default(getdate());
	EXEC('update [dbo].[JbForms] set CreatedDate=getdate()')
	ALTER TABLE [dbo].[JbForms]
	Alter Column CreatedDate datetime NOT NULL;
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='LastModifiedDate')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD LastModifiedDate datetime NULL;
	EXEC('update [dbo].[JbForms] set LastModifiedDate=getdate()')
	ALTER TABLE [dbo].[JbForms]
	Alter Column LastModifiedDate datetime NOT NULL;
END
GO

--<< Edit COLUMNS >>--
IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='RefEntityName')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	Alter Column RefEntityName nvarchar(100) NULL;

END
GO


--<< Drop COLUMNS >>--
IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='Size')
BEGIN	
	EXEC [dbo].[JbDropColumn] @tablename='dbo.JbForms' ,@fieldname='Size';
END
GO

IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='Name')
BEGIN
	EXEC [dbo].[JbDropColumn] @tablename='dbo.JbForms' ,@fieldname='Name';
END
GO

IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='Event_Id')
BEGIN	
	EXEC [dbo].[JbDropColumn] @tablename='dbo.JbForms' ,@fieldname='Event_Id';
END
GO






