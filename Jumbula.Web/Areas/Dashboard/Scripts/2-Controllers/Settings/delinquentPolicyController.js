﻿(function () {
    "use strict";

    angular.module("dashboardApp").controller("delinquentPolicyController",
        [
            "$scope", "$http", "jbToast", function ($scope, $http, jbToast) {
            
                window.app.globalObjects.initializeView();

                $http.get("Dashboard/Setting/GetDelinquentPolicy").success(
                    function (data) {
                        $scope.Model = data;
                    }).error(function (data) {
                        jbToast.error("error " + data);
                    });

                $scope.save = function () {
                    $http.post("Dashboard/Setting/SaveDelinquentPolicy", { model: $scope.Model })
                        .success(function (data) {

                            if (data.Status) {
                                $scope.errors = null;
                                jbToast.success("Delinquent account policy updated successfully.", "");
                            } else {
                                $scope.MC.handleErrors($scope, data);
                            }
                        });
                };
            }
        ]);
})();