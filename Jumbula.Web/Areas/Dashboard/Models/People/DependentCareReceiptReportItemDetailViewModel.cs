﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DependentCareReceiptReportItemDetailViewModel
    {
        public string PartnerName { get; set; }

        public string PartnerAddress { get; set; }

        public string PartnerPhone { get; set; }

        public string PartnerSite { get; set; }

        public string PartnerEmail { get; set; }

        public string Date { get; set; }

        public string Description { get; set; }
    }
}