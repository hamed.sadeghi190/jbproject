﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('beforeAfterCareAddEditStep2FeesController', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', 'ProgramSchedulePartsService', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation, ProgramSchedulePartsService) {

        $scope.baseUrl = window.location.origin + "/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.MC.stateParams = $stateParams;

        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;

        var isSchedulePartsChanged = ProgramSchedulePartsService.get();

        if (isSchedulePartsChanged != false && isSchedulePartsChanged != true) {
            isSchedulePartsChanged = false;
        }

        $scope.Model = {};
        $http.get('Dashboard/Program/GetBeforeAfterCareCreateEditStep2Fees', { params: { programId: $stateParams.programId, isSchedulePartsChanged: isSchedulePartsChanged } })
             .success(function (data, status, headers, config) {

                 $scope.Model = data;
             });

        $scope.Model.LastCreatedPage = "Step2.5";

        var IsSchedulePartChanged = false;
        var IsScheduleChargeChanged = false;

        $scope.AllScheduleFees = [];

        $scope.save = function () {

            for (var i = 0; i < $scope.Model.BeforeFees.length; i++) {
                $scope.AllScheduleFees.push($scope.Model.BeforeFees[i]);
            }
            
            for (var i = 0; i < $scope.Model.AfterFees.length; i++) {
                $scope.AllScheduleFees.push($scope.Model.AfterFees[i]);
            }

            for (var i = 0; i < $scope.Model.ComboFees.length; i++) {
                $scope.AllScheduleFees.push($scope.Model.ComboFees[i]);
            }
          

            $scope.CheckScheduleChargeChanged($stateParams.programId, $scope.AllScheduleFees);
        }


        $scope.CheckScheduleChargeChanged = function (programId, NewFees) {

            $http.post('Dashboard/Program/IsProgramSchedulesChargeChanged', { programId: programId, newScheduleCharges: NewFees })
                         .success(function (data, status, headers, config) {

                             if (data.status) {
                                 IsScheduleChargeChanged = data.Changed;
                                 $scope.nextStep();
                             }

                         });

        }

        $scope.nextStep = function () {
            if (IsScheduleChargeChanged) {

                jbConfirmation
                   .yes(function () {

                       $http.post('Dashboard/Program/BeforeAfterCareCreateEditStep2Fees', { model: $scope.Model, programId: $stateParams.programId, ScheduleChargesChanged: IsScheduleChargeChanged })
                           .success(function (data, status, headers, config) {
                               if (data.Status) {
                                   $state.go('BeforeAfterCareAddEditStep2ScheduleList', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                               } else {

                                   $scope.MC.handleErrors($scope, data);
                               }
                           })
                   })
                  .title('Overwrite prices?')
                  .message('it will overwrite the edits you had previously made to the "Schedule list" step. Are you sure you want to continue?')
                  .confirm();
            }
            else {
                $http.post('Dashboard/Program/BeforeAfterCareCreateEditStep2Fees', { model: $scope.Model, programId: $stateParams.programId, ScheduleChargesChanged: IsScheduleChargeChanged })
                         .success(function (data, status, headers, config) {
                             if (data.Status) {
                                 $state.go('BeforeAfterCareAddEditStep2ScheduleList', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                             } else {

                                 $scope.MC.handleErrors($scope, data);
                             }
                         })
            }

        }


        $scope.back = function () {
            $state.go('ProgramAddEditStep2', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
        }

        if ($state.returnByBack()) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();

