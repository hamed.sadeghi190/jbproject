﻿(function (angular) {
    'use strict';

    angular
        .module('dashboardApp').directive('preventDefault', function () {
            return {
                restrict: 'A',
                scope: {
                    preventDefault: '='
                },
                link: function ($scope, element) {
                    angular.element(element).off("click.disabled").on("click.disabled", function (e) {
                        if ($scope.preventDefault) {
                            e.preventDefault();
                        }
                    })
                }
            };
        });
})(angular);
