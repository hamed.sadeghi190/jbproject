﻿
namespace Jumbula.Core.Model.ClubSetting
{
    public class ReportsSettingViewModel
    {
        public string DependentCareDescription { get; set; }
        public string DefaultDependentCareDescription { get; set; }
        public bool IsPartner { get; set; }
    }
 
}