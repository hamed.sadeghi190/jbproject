﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonAssignRoomForProgramsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {

        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        $scope.init = function () {
            app.globalObjects.initializeView();
        }

        $http.get(window.location.origin + '/Dashboard/Season/GetAllPrograms', { params: { seasonDomain: $scope.seasonDomain } }).success(function (data, status, headers, config) {

            $scope.Model = data;
        });
      

        $scope.save = function () {
            $http.post(window.location.origin + '/Dashboard/Season/SaveAssignRoom', {model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status) {

                        jbToast.success("Rooms assigned successfully.");

                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                })
        }

    }]);
})();