﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Jumbula.Core.Model.Email;
using System.Text.RegularExpressions;
using System.Web;
using Email = Jumbula.Core.Domain.Email;

namespace Jumbula.Business.EmailBuilder
{
    public abstract class BaseEmailBuilder<TEmailParameter> : IEmailBuilder<TEmailParameter> where TEmailParameter : class
    {

        #region Constants
        #endregion

        #region Properties
        public abstract EmailCategory Category { get; }
        public abstract string TemplateName { get; }
        public virtual IEnumerable<string> SubCategories => new List<string> { Common.Constants.Email.PrimarySubCategory };

        public virtual bool ShouldSendToSupport => false;

        public abstract EmailCallerType CallerType { get; }

        public abstract EmailSendPriority EmailSendPriority { get; }

        public TEmailParameter Parameters { get; set; }
        #endregion

        #region Public Methods
        public abstract void Initiate(TEmailParameter parameters);

        public abstract void InitiatePreview(TEmailParameter parameters);

        public abstract string GetSubject(string subCategory = null);

        public abstract MailAddress GetFrom(string subCategory = null);

        public abstract string GetFromDisplayName(string subCategory = null);

        public abstract string GetReplyTo(string subCategory = null);

        public virtual IEnumerable<MailAddress> GetCCs(string subCategory = null)
        {
            return new List<MailAddress>();
        }

        public abstract IEnumerable<MailAddress> GetTos(string subCategory = null);

        public virtual IEnumerable<MailAddress> GetBcCs(string subCategory = null)
        {
            return new List<MailAddress>();
        }

        public abstract object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal);

        public virtual object BuildPreviewBodyModel(TEmailParameter parameters, string subCategory)
        {
            return new object();
        }

        public virtual string GenerateBody(Email email, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            var parameters = GetParameter(email.Parameters);

            Initiate(parameters);

            return GenerateBody(BuildBodyModel(email.SubCategory, previewMode));
        }

        public virtual string GeneratePreviewBody(TEmailParameter parameters, string subCategory)
        {
            InitiatePreview(parameters);

            return GenerateBody(BuildPreviewBodyModel(parameters, subCategory));
        }

        public virtual string GenerateBody(object bodyModel)
        {
            var result = ManipulateBodyParameters(ViewHelper.RenderViewToString($"{Common.Constants.Email.TemplatesPath}/{TemplateName}.cshtml", bodyModel));

            return result;
        }

        public virtual Email GetEmail(TEmailParameter parameter, string subCategory = Common.Constants.Email.PrimarySubCategory)
        {
            Initiate(parameter);

            var result = new Email
            {
                DateCreated = DateTime.UtcNow,
                Category = Category,
                FromEmail = GetFrom(subCategory).Address,
                FromDisplayName = GetFrom(subCategory).DisplayName,
                ReplyTo = GetReplyTo(subCategory),
                Priority = EmailSendPriority,
                Subject = GetSubject(subCategory),
                Parameters = SerializeParameter(parameter),
                CallerType = CallerType,
                SubCategory = subCategory
            };

            var recipients = new List<EmailRecipient>();

            var too = new List<MailAddress>();

            if (subCategory.Equals(Common.Constants.Email.SupportSubCategory))
                too.Add(GetSupportRecipient());
            else
                too = GetTos(subCategory).ToList();

            recipients.AddRange(too.Select(e => new EmailRecipient
            {
                EmailAddress = e.Address,
                DisplayName = e.DisplayName,
                Type = EmailRecipientType.To
            }));

            recipients.AddRange(GetCCs(subCategory).Select(e => new EmailRecipient
            {
                EmailAddress = e.Address,
                DisplayName = e.DisplayName,
                Type = EmailRecipientType.Cc
            }));

            recipients.AddRange(GetBcCs(subCategory).Select(e => new EmailRecipient
            {
                EmailAddress = e.Address,
                DisplayName = e.DisplayName,
                Type = EmailRecipientType.Bcc
            }));

            result.Recipients = recipients;

            if (!subCategory.Equals(Common.Constants.Email.SupportSubCategory))
                result.Histories = GetHistories();

            return result;
        }

        public virtual IEnumerable<Email> GetEmails(TEmailParameter parameter, bool isProductionMode)
        {
            var result = new List<Email>();

            foreach (var subCategory in SubCategories)
            {
                var email = GetEmail(parameter, subCategory);

                if (email.Recipients.Any(e => e.Type == EmailRecipientType.To))
                    result.Add(email);
            }

            if (isProductionMode && ShouldSendToSupport)
                result.Add(GetEmail(parameter, Common.Constants.Email.SupportSubCategory));

            return result;
        }

        public virtual string ManipulateBodyParameters(string body)
        {
            var bodyParameters = Regex.Matches(body, @"%(.+?)%|%date <([1-9][0-9]) days>%")
                .Cast<Match>()
                .Select(m => m.Groups[1].Value)
                .ToArray();

            foreach (var bodyParameter in bodyParameters)
            {
                var parameter = HttpUtility.HtmlDecode(bodyParameter);

                var replaced = ManipulateBodyParameter(parameter);

                if (!string.IsNullOrWhiteSpace(replaced))
                    body = body.Replace($"%{bodyParameter}%", replaced);
            }

            return body;
        }

        public virtual string ManipulateBodyParameter(string parameter)
        {
            return null;
        }

        public virtual string SerializeParameter(TEmailParameter parameters)
        {
            return JsonHelper.JsonSerializer(parameters);
        }

        public virtual TEmailParameter GetParameter(string parameter)
        {
            if (string.IsNullOrEmpty(parameter))
                return default(TEmailParameter);

            return JsonHelper.JsonDeserialize<TEmailParameter>(parameter);
        }

        public abstract List<CommunicationHistory> GetHistories();

        public virtual EmailViewModel GetEmailViewModel(TEmailParameter parameter)
        {
            return new EmailViewModel();
        }

        public virtual Email GetEmail(EmailViewModel emailViewModel)
        {
            return new Email();
        }

        public virtual string GeneratePreviewBody(EmailViewModel emailViewModel)
        {
            return string.Empty;
        }

        public virtual string GenerateBody(EmailTemplateModel template, string emailBody, object bodyModel)
        {
            return string.Empty;
        }
        #endregion

        #region Protected Methods

        protected static MailAddress GetSupportRecipient()
        {
            return new MailAddress(Common.Constants.Email.JumbulaSupportEmailAddress, Common.Constants.Email.JumbulaSupportEmailName);
        }

        #endregion

        #region Private Methods


        #endregion

    }
}
