﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class BladeViewModel
    {
        public string Title { get; set; }
        public List<DashboardMenuItemViewModel> MenuItems { get; set; }
    }
}