
go
Alter table ClientPaymentMethods add StripePreferredLabel nvarchar(max)  null 

go
update ClientPaymentMethods set StripePreferredLabel = 'Express payment methods'