﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CatalogSettingsViewModel
    {
        public CatalogSettingsViewModel()
        {
            CatalogSearchTags = new CatalogSearchTags();
        }

        public int Id { get; set; }

        public CatalogSearchTags CatalogSearchTags { get; set; }

        public bool WeeklyEmail { get; set; }

        public bool FairFaxCounty { get; set; }

        public string Image { get; set; }

        public bool VirtusCertify { get; set; }

        public bool JumbulaMobileApp { get; set; }
        public bool HasTitle1SchoolDiscounts { get; set; }

        public string Title1SchoolDiscountType { get; set; }

        public List<int> Counties { get; set; }

        public List<int> CountiesMayBe { get; set; }

        public List<SelectKeyValue<int>> AllCounties{ get; set; }

    }
}