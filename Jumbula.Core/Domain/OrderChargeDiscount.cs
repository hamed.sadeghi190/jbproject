﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class OrderChargeDiscount : BaseEntity<long>
    {
        [MaxLength(256)]
        public string Name { get; set; }

        [Required]
        public decimal Amount { get; set; }
        [StringLength(256)]
        public string Description { get; set; }
        [Required]

        [DefaultValue(ChargeDiscountCategory.EntryFee)]
        public ChargeDiscountCategory Category { get; set; }

        [Required]
        public ChargeDiscountSubcategory Subcategory { get; set; }

        public long? OrderId { get; set; }
        public long? OrderItemId { get; set; }
        public long? ChargeId { get; set; }
        public long? DiscountId { get; set; }
        public int? CouponId { get; set; }
        //public int? DonationId { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        [ForeignKey("OrderItemId")]
        public virtual OrderItem OrderItem { get; set; }
        [ForeignKey("ChargeId")]
        public virtual Charge Charge { get; set; }
        [ForeignKey("DiscountId")]
        public virtual Discount Discount { get; set; }

        [ForeignKey("CouponId")]
        public virtual Coupon Coupon { get; set; }
        [NotMapped]
        public string Message { get; set; }
        public Discount ToDiscount()
        {
            var discount = new Discount
            {
                Amount = this.Amount,
                Category = this.Category,
            };
            return discount;
        }

        [ForeignKey("InstallmentId")]
        public virtual OrderInstallment OrderInstallment { get; set; }
        public long? InstallmentId { get; set; }
        public bool IsDeleted { get; set; }

    }
}