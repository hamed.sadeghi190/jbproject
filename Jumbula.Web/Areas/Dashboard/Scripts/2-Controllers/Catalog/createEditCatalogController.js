﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('createEditCatalogController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', 'jbRandom', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, jbRandom) {
        window.app.globalObjects.pageClass = 'catalog-create-view';
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        var pageMode = null;

        var catalogId = $stateParams.catalogId;


        $scope.arrayCropper = [];
        $scope.arraySourceCropper = [];
        $scope.cropperImages = {};
        $scope.cropperImages.sourceImage = null;
        $scope.cropperImages.croppedImage = null;

        $scope.cropper = {};
        $scope.cropper.sourceImage = null;
        $scope.cropper.croppedImage = null;
        $scope.bounds = {};
        $scope.bounds.left = 0;
        $scope.bounds.right = 0;
        $scope.bounds.top = 0;
        $scope.bounds.bottom = 0;

        $scope.AddTo = function (cropImgs, sourcImg) {

            $scope.arrayCropper.push(cropImgs);

            $scope.Model.Images = $scope.arrayCropper;
            $scope.arraySourceCropper.push(sourcImg);
            $scope.Model.SourceImages = $scope.arraySourceCropper;

            $scope.cropperImages.sourceImage = null;


            if ($scope.Model.Images.length == 4) {

                document.getElementById("btn_Add").disabled = true;
            }

        };
        $scope.ShowImage = false;

        $scope.AddToImage = function (cropImg, sourceImg) {
            $scope.Model.Image = cropImg;
            $scope.Model.SourceImage = sourceImg;
            $scope.ShowImage = true;
            document.getElementById("btn_Add2").disabled = true;

        }

        $scope.remove = function () {
            $scope.Model.Image = null;
            $scope.ShowImage = false;
            document.getElementById("btn_Add2").disabled = false;
        }

        if (catalogId == 'undefined' || catalogId == '' || catalogId == 0 || catalogId == null || catalogId == '0') {
            pageMode = 'Create';
        }
        else {
            pageMode = 'Edit';
        }

        if (pageMode == 'Create') {
            $http.get('Dashboard/Catalog/GetCreateEdit').success(function (data, status, headers, config) {
                $scope.Model = data;
                $scope.Model.Rating = 1;
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }
        else {
            $http.get('Dashboard/Catalog/GetCreateEdit', { params: { catalogId: catalogId } }).success(function (data, status, headers, config) {
                $scope.Model = data;
                if ($scope.Model.Image != "")
                {
                    $scope.cropper.croppedImage = $scope.Model.Image;
                    $scope.ShowImage = true;
                }
                else
                {
                    $scope.ShowImage = false;
                }
               
                if ($scope.Model.Images.length > 0) {
                    for (var i = 0; i < $scope.Model.Images.length; i++) {
                        $scope.arrayCropper.push($scope.Model.Images[i])
                    }

                }

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $scope.save = function () {
            if ($scope.Model.Image!=null) {
                $scope.Model.Image = $scope.cropper.croppedImage;
            }
            
            $scope.Model.Images = $scope.arrayCropper;

            $scope.Model.SourceImage = $scope.cropper.sourceImage;
            $scope.Model.SourceImages = $scope.arraySourceCropper;

            $http.post('Dashboard/Catalog/CreateEdit', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {

                     $scope.errors = null;

                     if (pageMode == 'Create') {
                         jbToast.success("Catalog created successfully.", "");
                     }
                     else {
                         jbToast.success("Catalog edited successfully.", "");
                     }

                     $state.go("Catalog");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }



        //removeImages
        $scope.removeImage = function (index) {
            $scope.arrayCropper.splice(index, 1);
            $scope.Model.Images = $scope.arrayCropper;
            document.getElementById("btn_Add").disabled = false;
            $scope.disable = false;

        }
       
        $scope.showCanvac = false;
        $scope.disable = false;
        $scope.Canvac2 = function () {
            $scope.showCanvac = true;
            $scope.cropperImages.sourceImage = null;

            if ($scope.Model.Images.length == 4) {
                $scope.disable = true;
            }

        }
        $scope.Canvac1 = function () {

            $scope.showCanvac = false;
        }
        $scope.loadCounties = function () {

            $http.get('Dashboard/Catalog/GetCounties', { params: { states: $scope.Model.SelectedStates } }).success(function (data, status, headers, config) {

                $scope.Model.Counties = data;

                if (!$scope.Model.SelectedCounties) {
                    $scope.Model.SelectedCounties = [];
                }

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $scope.addDuration = function () {
            $scope.Model.PriceGroups.push(
                {
                    TempTitle: 'New duration',
                    EditMode: true,
                    Id: 0,
                    StringId: jbRandom.Generate(10, true, false, false, false, false),
                    Prices: [{ Title: 'New schedule', EditMode: false, Id: 0, StringId: jbRandom.Generate(10, true, false, false, false, false) }]
                });
        }

        $scope.addPriceOption = function (priceGroupId) {
            for (var i = 0; i < $scope.Model.PriceGroups.length; i++) {
                if ($scope.Model.PriceGroups[i].StringId == priceGroupId) {
                    if ($scope.Model.PriceGroups[i].Prices == null || $scope.Model.PriceGroups[i].Prices == 'undefined') {
                        $scope.Model.PriceGroups[i].Prices = [];
                    }

                    $scope.Model.PriceGroups[i].Prices.push({ TempTitle: 'New schedule', EditMode: true, GroupId: priceGroupId, Id: 0, StringId: jbRandom.Generate(10, true, false, false, false, false) });
                };
            }
        }

        $scope.addGradeGrouping = function () {
            $scope.Model.RecomendedGradeGroups.push({ TempTitle: 'New grade grouping', EditMode: true, StringId: jbRandom.Generate(10, true, false, false, false, false) })
        }

        $scope.editItemTitle = function (item) {
            item.TempTitle = item.Title;
            item.EditMode = true;
        }

        $scope.cancelItemSave = function (item, allItems) {

            var index = -1;

            if ((item.Title == null || item.Title == '') || (item.TempTitle == null || item.TempTitle == '')) {
                for (var i = 0; i < allItems.length; i++) {

                    if (allItems[i].StringId == item.StringId) {
                        index = i;
                    }
                }

                allItems.splice(index, 1);
            }
            else {
                item.TempTitle = '';
                item.EditMode = false;
            }
        }

        $scope.saveItemTitle = function (item, allItems) {

            if (item.TempTitle == null || item.TempTitle == '') {
                return;
            }

            for (var i = 0; i < allItems.length; i++) {
                if (allItems[i].StringId != item.StringId) {
                    if (allItems[i].Title == item.TempTitle) {
                        jbToast.error('An item with this name exists.');
                        return;
                    }
                }
            }

            item.Title = item.TempTitle;
            item.TempTitle = '';
            item.EditMode = false;
        }

        $scope.cancelPriceOptionSave = function (item, allItems) {
            var index = -1;

            if ((item.TempTitle == null || item.TempTitle == '') || (item.Title == null || item.Title == '')) {
                for (var i = 0; i < allItems.length; i++) {

                    if (allItems[i].StringId == item.StringId) {
                        index = i;
                    }
                }

                allItems.splice(index, 1);
            }
            else {
                item.TempTitle = '';
                item.TempWeeks = null;
                item.EditMode = false;
            }
        }

        $scope.savePriceOption = function (item, allItems) {

            if ((item.TempTitle == null || item.TempTitle == '')) {
                return;
            }

            for (var i = 0; i < allItems.length; i++) {

                if (allItems[i].StringId != item.StringId) {

                    if (allItems[i].Title == item.TempTitle) {
                        jbToast.error('An item with this name exists.');
                        return;
                    }
                }
            }

            item.Title = item.TempTitle;
            item.Weeks = item.TempWeeks;
            item.TempTitle = '';
            item.TempWeeks = null;
            item.EditMode = false;
        }

        $scope.changePriceGroup = function (priceGroup) {

            for (var i = 0; i < priceGroup.Prices.length; i++) {
                priceGroup.Prices[i].IsChecked = priceGroup.IsChecked;
            }
        }

        $scope.addSpaceRequirement = function (spaceRequirementType) {

            var tempTitle = '';

            if (spaceRequirementType == 'Indoor') {
                tempTitle = 'New indoor space';
            }
            else if (spaceRequirementType == 'Outdoor') {
                tempTitle = 'New outdoor space';
            }

            $scope.Model.SpaceRequirements.push({ TempTitle: tempTitle, EditMode: true, Type: spaceRequirementType, StringId: jbRandom.Generate(10, true, false, false, false, false) });
        }

        $scope.onSelect = function (e) {
            var message = $.map(e.files, function (file) { return file.name; }).join(", ");
            kendoConsole.log("event :: select (" + message + ")");
        }

        $scope.removePrice = function (index) {

            jbConfirmation
             .yes(function () {
                 $scope.Model.Prices.removeByIndex(index);
             })
            .title('Price option delete')
            .message('Are you sure you want to delete this price option?')
            .confirm();
        }

        $scope.OnUploaderSuccess = function (e) {

            if (e.response.Status == true && e.response.actionType == 'Upload') {
                jbToast.success("File uploaded.");
                $scope.showListName = true;
                $scope.Model.ImageFileName = e.response.imageFileName;

            } else if (e.response.Status == true && e.response.actionType == 'Remove') {
                jbToast.success("Remove uploaded file completed.");
            }
        }

        $scope.onUpload = function (e) {

            var files = e.files;
            $.each(files, function (index, value) {
                if (value.extension !== '.jpg' && value.extension !== '.jpeg' && value.extension !== '.bmp') {
                    jbToast.success("Extention not allowed!");
                    e.preventDefault();
                }
            });
        }

    }]);
})();

