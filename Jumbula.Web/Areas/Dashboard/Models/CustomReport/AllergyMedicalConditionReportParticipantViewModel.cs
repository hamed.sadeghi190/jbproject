﻿namespace SportsClub.Areas.Dashboard.Models
{
    public class AllergyMedicalConditionReportParticipantViewModel
    {
        public string ParticipantName { get; set; }
        public string Grade { get; set; }
        public string ProgramName { get; set; }
        public int Number { get; set; }
        public string DescriptionMedical { get; set; }
        public string DescriptionAllergies { get; set; }
        public string DescriptionSelfAdministered { get; set; }
        public string DescriptionNutAllergy { get; set; }
        public string Description { get; set; }
        public bool HasMedicalCondition { get; set; }
        public bool HasAllergies { get; set; }
        public bool HasSelfAdministered { get; set; }
        public bool HasNutAllergy { get; set; }
        public int PlayerId { get; set; }
    }
}