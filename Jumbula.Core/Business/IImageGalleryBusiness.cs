﻿using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface IImageGalleryBusiness
    {
        ImageGallery Get(int id);
    }
}
