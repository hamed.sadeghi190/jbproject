﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('reviewSentCampaignController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'templateEditorService', 'CampaignService', function ($scope, $http, jbToast, $stateParams, $state, templateEditorService, CampaignService) {
        window.app.globalObjects.pageClass = 'campaign-review';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $scope.campaignId = $scope.MC.stateParams.campaignId;

        $scope.Model = {};
        $scope.Model.MailTemplate = {};

        $scope.Model.Attachments = [];

        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        }

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        }

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        }

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        }

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        }


        $http.get(baseUrl + '/GetSentCampaign', { params: { campaignId: $scope.campaignId } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
                if ($scope.Model.TemplateNames && $scope.Model.SelectedTemplateId != 0) {
                    $http.get(baseUrl + '/GetTemplate', { params: { id: $scope.Model.SelectedTemplateId } })
                        .success(function (data, stats, headers, config) {
                            $scope.Model.MailTemplate = data;
                            if ($scope.Model.MailTemplate.Name == "") {
                                $scope.Model.MailTemplate.ChangeTemplate = true;
                            } else {
                                $scope.Model.MailTemplate.ChangeTemplate = false;
                            }
                        });
                }
            });

    }]);
})();