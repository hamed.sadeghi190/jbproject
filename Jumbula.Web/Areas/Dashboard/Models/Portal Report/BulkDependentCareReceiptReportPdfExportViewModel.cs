﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class BulkDependentCareReceiptReportPdfExportViewModel
    {
        public string Date { get; set; }

        public List<List<BulkDependentCareReceiptReportViewModel>> Data { get; set; }

        public string HeaderInfoPartnerName { get; set; }

        public string HeaderInfoPartnerAddress { get; set; }

        public string HeaderInfoPartnerPhone { get; set; }

        public string HeaderInfoPartnerSite { get; set; }

        public string HeaderInfoPartnerEmail { get; set; }
        public string Description { get; set; }
        public string HeaderInfoDate { get; set; }

        public List<string> TotalAmount { get; set; }
    }
}