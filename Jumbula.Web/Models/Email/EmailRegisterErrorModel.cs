﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Email
{
    public class EmailRegisterErrorModel
    {
        public string CardNumber { get; set; }
        public string CardHolderName { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int Userid { get; set; }
        public string Response { get; set; }
        public string ClubName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string UserCustomerId { get; set; }
        
    }

   
}