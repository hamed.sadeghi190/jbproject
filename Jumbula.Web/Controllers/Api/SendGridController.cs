﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Jumbula.Web.Controllers.Api
{
    public class SendGridController : ApiController
    {
        [HttpPost]
        public async Task<HttpResponseMessage> Post()
        {
            HttpResponseMessage response = null;
            string jsonData = await Request.Content.ReadAsStringAsync();


            try
            {
                if (string.IsNullOrEmpty(jsonData))
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Result = new { Status = true } });
                    return response;
                }

                var dataLists = JArray.Parse(jsonData);

                var receivedWebHooks = new List<Webhook>();

                try
                {
                    receivedWebHooks = JsonConvert.DeserializeObject<List<Webhook>>(dataLists.ToString());
                }
                catch
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Result = new { Status = true } });
                    return response;
                }

                #region Campaign
                var campaignReceivedWebHooks = receivedWebHooks.Where(r => r.CampaignId > 0);
               
                var campaignRecipients = new List<CampaignRecipients>();

                foreach (var item in campaignReceivedWebHooks)
                {
                    var recipient = new CampaignRecipients
                    {
                        CampaignId = item.CampaignId,
                        EmailAddress = item.Email != null ? item.Email.ToLower() : "",
                        EmailStatus = (EmailStatus)Enum.Parse(typeof(EmailStatus), item.Event),
                        Reason = item.Reason,
                    };

                    switch (recipient.EmailStatus)
                    {
                        case EmailStatus.click:
                            {
                                recipient.IsClicked = true;
                                recipient.ClickDate = DateTimeHelper.ConvertTimespanToDateTime(item.Timestamp);
                                break;
                            }
                        case EmailStatus.open:
                            {
                                recipient.IsOpened = true;
                                recipient.OpenDate = DateTimeHelper.ConvertTimespanToDateTime(item.Timestamp);
                                break;
                            }
                    }
                    campaignRecipients.Add(recipient);
                }

                Ioc.CampaignBusiness.UpdateCampaignRecipientsEvents(campaignRecipients);
                #endregion


                #region Email
                var updatedEmailRecipients = new List<EmailRecipient>();
                var emailReceivedWebHooks = receivedWebHooks.Where(r => r.EmailId > 0);

                foreach (var item in emailReceivedWebHooks)
                {
                    var emailRecipient = new EmailRecipient();

                    emailRecipient.EmailId = item.EmailId;
                    emailRecipient.EmailAddress = item.Email != null? item.Email.ToLower(): string.Empty;

                    UpdateEvent(emailRecipient, item.Event, item.Timestamp);

                    updatedEmailRecipients.Add(emailRecipient);
                }

                Ioc.EmailRespository.UpdateEmailRecipientEvents(updatedEmailRecipients);
                #endregion

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        private void UpdateEvent(EmailRecipient recipient, string emailEvent, long timeStamp)
        {
            var dateTime = DateTimeHelper.ConvertTimespanToDateTime(timeStamp);

            var emailStatus = (EmailStatus) Enum.Parse(typeof(EmailStatus), emailEvent);

            switch (emailStatus)
            {
                case EmailStatus.processed:
                    break;
                case EmailStatus.dropped:
                    recipient.Dropped = true;
                    recipient.DropDate = dateTime;
                    break;
                case EmailStatus.delivered:
                    recipient.Delivered = true;
                    recipient.DeliverDate = dateTime;
                    break;
                case EmailStatus.open:
                    recipient.Opened = true;
                    recipient.OpenDate = dateTime;
                    break;
                case EmailStatus.click:
                    recipient.Clicked = true;
                    recipient.ClickDate = dateTime;
                    break;
                case EmailStatus.bounce:
                    recipient.Bounced = true;
                    recipient.BounceDate = dateTime;
                    break;
                case EmailStatus.deferred:
                    recipient.Deferred = true;
                    recipient.DefereDate = dateTime;
                    break;
                case EmailStatus.spamreport:
                    recipient.SpamReport = true;
                    recipient.SpamReportDate = dateTime;
                    break;
                case EmailStatus.unsubscribe:
                    recipient.Unsubscribe = true;
                    recipient.UnsubscribeDate = dateTime;
                    break;
                default:
                    break;
            }
        }
    }

}
