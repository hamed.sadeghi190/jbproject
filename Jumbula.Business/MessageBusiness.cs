﻿using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Catalog;

namespace Jumbula.Business
{
    public class MessageBusiness : IMessageBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IUserProfileBusiness _userProfileBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IRepository<Message> _messageRepository;
        private readonly IEntitiesContext _dataContext;

        public MessageBusiness(IClubBusiness clubBusiness, IUserProfileBusiness userProfileBusiness, ISeasonBusiness seasonBusiness, IRepository<Message> messageRepository, IEntitiesContext dataContext)
        {
            _clubBusiness = clubBusiness;
            _seasonBusiness = seasonBusiness;
            _userProfileBusiness = userProfileBusiness;
            _messageRepository = messageRepository;
            _dataContext = dataContext;
        }

        public MessageHeader Get(int id)
        {
            var result = _dataContext.Set<MessageHeader>().SingleOrDefault(u => u.Id == id);

            return result;
        }

        public List<MessageHeader> GetInboxUserMessages(int userId, int clubId, int start, int take, ref int total)
        {
            var messages = _dataContext.Set<MessageHeader>().Where(u => u.Messages.Any(s => s.SenderId == userId && s.ClubId == clubId) || u.Messages.Any(m => m.UserMessages.Any(t => t.ReceiverId == userId && t.ClubId == clubId)));

            total = messages.Count();

            var result = messages.OrderByDescending(o => o.Messages.Max(m => m.CreatedDate)).Skip(start).Take(take).ToList();

            return result;
        }

        public IQueryable<MessageHeader> GetRecentMessages(int userId, int clubId)
        {
            var result = _dataContext.Set<MessageHeader>().Where(u => u.Messages.Any(m => m.UserMessages.Any(t => t.ReceiverId == userId && t.ClubId == clubId)));

            return result;
        }


        public IQueryable<Message> GetList()
        {
            return _messageRepository.GetList();
        }

        public JbUser GetClubMessageReciever(Club club)
        {
            var clubSetting = club.Setting;

            var recieverId = clubSetting.StaffResponder;

            var staff = club.ClubStaffs.SingleOrDefault(c => c.Status == StaffStatus.Active && c.Id == recieverId);

            if (staff == null)
            {
                throw new Exception("You must access the provider dashboard-> Settings-> General menu and set the staff member who should receive the communication messages before you can schedule a draft class.");
            }

            return staff.JbUserRole.User;
        }

        public JbUser GetPartnerMessageReciever(Club partner, Club school, long outSourceSeasonId)
        {
            var schoolSetting = school.Setting;

            var partnerSetting = partner.Setting;

            if (schoolSetting.PartnerMessageResponder.HasValue && schoolSetting.PartnerMessageResponder != 0)
            {
                return partner.ClubStaffs.Single(s => s.Id == schoolSetting.PartnerMessageResponder).JbUserRole.User;
            }

            var outSourceSeason = _seasonBusiness.Get(outSourceSeasonId);
            var providerSetting = outSourceSeason.Club.Setting;

            if (providerSetting.PartnerMessageResponder.HasValue && providerSetting.PartnerMessageResponder != 0)
            {
                return partner.ClubStaffs.Single(s => s.Id == providerSetting.PartnerMessageResponder).JbUserRole.User;
            }

            if (partnerSetting.StaffResponder != 0)
            {
                return partner.ClubStaffs.Single(s => s.Id == partnerSetting.StaffResponder).JbUserRole.User;
            }

            throw new Exception("You must access the Settings-> General menu and set the staff member who should receive the communication messages before you can schedule a draft class.");
        }

        public MessageHeader Send(string chatId, int currentUserId, int currentClubId, int? senderId, int? senderClubId, int? receiverId, int? receiverClubId, ref int realReceiverId, string subject = "", string text = "", MessageType messageType = MessageType.Ordinary, string attribute = "", long? programId = null)
        {

            var reciverIds = new Dictionary<int?, int?>();

            if (receiverId.HasValue)
            {
                reciverIds.Add(receiverClubId, receiverId);
            }

            var result = Send(chatId, currentUserId, currentClubId, senderId, senderClubId, reciverIds, ref realReceiverId, subject, text, messageType, attribute, programId);

            return result;
        }

        public MessageHeader Send(string chatId, int senderId, int senderClubId, int recieverId, int recieverClubId, string subject, string text)
        {
            var reciverIds = new Dictionary<int?, int?>();

            var chat = new MessageHeader();

            if (!string.IsNullOrEmpty(chatId))
            {
                chat = GetChat(chatId);

                reciverIds.Add(recieverClubId, recieverId);

            }
            else
            {
                throw new Exception("ChatId can not be null or empty.");
            }

            int reciverId = 0;

            return Send(chatId, senderId, senderClubId, senderId, senderClubId, reciverIds, ref reciverId, subject, text, chat.Type, null, null);
        }

        public MessageHeader Send(string chatId, int senderId, int senderClubId, string text)
        {
            var reciverIds = new Dictionary<int?, int?>();

            var chat = new MessageHeader();

            if (!string.IsNullOrEmpty(chatId))
            {
                chat = GetChat(chatId);

                foreach (var item in chat.Messages.Where(m => m.SenderId != senderId))
                {
                    reciverIds.Add(item.ClubId, item.SenderId);
                }

                foreach (var item in chat.Messages.SelectMany(u => u.UserMessages).Where(m => m.ReceiverId != senderId))
                {
                    reciverIds.Add(item.ClubId, item.ReceiverId);
                }
            }
            else
            {
                throw new Exception("ChatId can not be null or empty.");
            }

            int reciverId = 0;

            return Send(chatId, senderId, senderClubId, senderId, senderClubId, reciverIds, ref reciverId, "", text, chat.Type, null, null);
        }

        public bool Reply(int headerId, int clubId, string replyText, int senderId, int receiverId)
        {
            var messageHeader = _dataContext.Set<MessageHeader>().SingleOrDefault(u => u.Id == headerId);

            var message = new Message { SenderId = senderId, CreatedDate = DateTime.UtcNow, MeassageHeaderId = headerId, Text = replyText };

            messageHeader.Messages.Add(message);

            var userMessage = new UserMessage { ClubId = clubId, MessageId = message.Id, ReceiverId = receiverId };

            _dataContext.Set<UserMessage>().Add(userMessage);


            return _dataContext.SaveChanges() > 0;
        }

        public IQueryable<Message> GetRecieved(int userId, int clubId)
        {
            return _messageRepository.GetList(m => m.UserMessages.Any(u => u.ReceiverId == userId && u.ClubId == clubId));
        }

        public int GetUnreadCount(int userId, int clubId)
        {
            return GetRecieved(userId, clubId).Where(m => m.UserMessages.Any(u => !u.IsRead)).GroupBy(m => m.MeassageHeaderId).Count();
        }

        public MessageHeader GetChat(int senderId, int clubSenderId, int receiverId, int clubRecieverId, string subject, string text, MessageType messageType = MessageType.Ordinary, string attribute = "", long? programId = null)
        {
            var result = InitilizeNewChat(senderId, clubSenderId, receiverId, clubRecieverId, subject, text, messageType, attribute, programId);

            return result;
        }

        public MessageHeader GetChat(string chatId)
        {
            MessageHeader result = null;

            result = _dataContext.Set<MessageHeader>().Single(h => h.ChatId.Equals(chatId));

            return result;
        }

        public MessageHeader GetChat(string chatId, int usreId)
        {
            MessageHeader result = null;

            result = _dataContext.Set<MessageHeader>().Single(h => h.ChatId.Equals(chatId));

            SetAsRead(result, usreId);

            return result;
        }

        public MessageHeader InitilizeNewChat(int senderId, int clubSenderId, int receiverId, int clubRecieverId, string subject, string text, MessageType messageType = MessageType.Ordinary, string attribute = "", long? programId = null)
        {
            var result = new MessageHeader()
            {
                Subject = subject,
                AttributeSerialized = attribute,
                Type = messageType,
                ProgramId = programId,
                ChatId = Guid.NewGuid().ToString(),
                Messages = new List<Message>(),
            };

            return result;
        }

        public Club GetAudience(MessageHeader messageHeader, int currentClubId)
        {
            if (messageHeader.Messages.Any(m => m.ClubId != currentClubId))
            {
                return messageHeader.Messages.Last(m => m.ClubId != currentClubId).Club;
            }

            if (messageHeader.Messages.SelectMany(s => s.UserMessages).Any(u => u.ClubId != currentClubId))
            {
                var userMessage = messageHeader.Messages.SelectMany(s => s.UserMessages).Last(u => u.ClubId != currentClubId);

                var club = _clubBusiness.Get(userMessage.ClubId);

                return club;
            }

            return null;
        }

        public JbUser GetAudienceUser(MessageHeader messageHeader, int currentUserId)
        {
            if (messageHeader.Messages.Any(m => m.SenderId != currentUserId))
            {
                return messageHeader.Messages.Last(m => m.SenderId != currentUserId).Sender;
            }

            if (messageHeader.Messages.SelectMany(s => s.UserMessages).Any(u => u.ReceiverId != currentUserId))
            {
                var userMessage = messageHeader.Messages.SelectMany(s => s.UserMessages).Last(u => u.ReceiverId != currentUserId);

                var user = _userProfileBusiness.Get(userMessage.ReceiverId);

                return user;
            }

            return null;
        }

        private MessageHeader Send(string chatId, int currentUserId, int currentClubId, int? senderId, int? senderClubId, Dictionary<int?, int?> receiverIds, ref int finalReceiverId, string subject = "", string text = "", MessageType messageType = MessageType.Ordinary, string attribute = "", long? programId = null)
        {
            MessageHeader messageHeader = null;

            if (string.IsNullOrEmpty(chatId))
            {
                var recieverId = receiverIds.Last();

                messageHeader = GetChat(senderId.Value, senderClubId.Value, recieverId.Value.Value, recieverId.Key.Value, subject, text, messageType, attribute, programId);
            }
            else
            {
                messageHeader = _dataContext.Set<MessageHeader>().Single(s => s.ChatId.Equals(chatId));
            }

            var realSenderId = currentUserId;
            var realSenderClubId = senderClubId.HasValue ? senderClubId : currentClubId;

            var realReceiverId = 0;
            var realReceiverClubId = 0;

            if (!string.IsNullOrEmpty(chatId) && messageHeader.Messages.Any())
            {
                if (messageHeader.Messages.Any(m => m.SenderId != realSenderId))
                {
                    realReceiverId = messageHeader.Messages.Last(m => m.SenderId != realSenderId).SenderId;
                    realReceiverClubId = messageHeader.Messages.Last(m => m.SenderId != realSenderId).ClubId;
                }
                else if (messageHeader.Messages.Any(m => m.UserMessages.Any(u => u.ReceiverId != realSenderId)))
                {
                    realReceiverId = messageHeader.Messages.SelectMany(m => m.UserMessages).Last(u => u.ReceiverId != realSenderId).ReceiverId;
                    realReceiverClubId = messageHeader.Messages.SelectMany(m => m.UserMessages).Last(u => u.ReceiverId != realSenderId).ClubId;
                }
                //else if (messageHeader.Messages.Any(m => m.UserMessages.Any(u => u.ReceiverId == realSenderId && u.ClubId != realSenderClubId)))
                //{
                //    realReceiverId = messageHeader.Messages.SelectMany(m => m.UserMessages).Last(u => u.ReceiverId == realSenderId && u.ClubId != realSenderClubId).ReceiverId;
                //    realReceiverClubId = messageHeader.Messages.SelectMany(m => m.UserMessages).Last(u => u.ReceiverId == realSenderId && u.ClubId != realSenderClubId).ClubId;
                //}
                //else if (messageHeader.Messages.Any(m => m.SenderId != realSenderId || m.ClubId != realSenderClubId))
                //{
                //    realReceiverId = messageHeader.Messages.Last(m => m.SenderId == realSenderId && m.ClubId != realSenderClubId).SenderId;
                //    realReceiverClubId = messageHeader.Messages.Last(m => m.SenderId == realSenderId && m.ClubId != realSenderClubId).ClubId;
                //}
                else
                {
                    throw new Exception("There is no receiver to send message.");
                }
            }
            else
            {
                realReceiverId = receiverIds.Last().Value.Value;
                realReceiverClubId = receiverIds.Last().Key.Value;
            }

            var message = new Message { ClubId = realSenderClubId.Value, SenderId = realSenderId, CreatedDate = DateTime.UtcNow, Text = text };

            var userMessage = new UserMessage()
            {
                ClubId = realReceiverClubId,
                Message = message,
                ReceiverId = realReceiverId,
            };

            message.UserMessages.Add(userMessage);

            messageHeader.Messages.Add(message);

            if (!string.IsNullOrEmpty(attribute))
            {
                messageHeader.AttributeSerialized = attribute;
            }

            finalReceiverId = realReceiverId;

            if (messageHeader.Id < 1)
            {
                _dataContext.Set<MessageHeader>().Add(messageHeader);
            }

            var result = _dataContext.SaveChanges() > 0;

            return messageHeader;
        }

        public void SetAsRead(string chatId, int userId)
        {
            var messages = _messageRepository.GetList(m => m.MessageHeader.ChatId == chatId);

            messages.SelectMany(m => m.UserMessages).Where(u => u.ReceiverId == userId).ToList().ForEach(m => m.IsRead = true);
        }

        public void SetAsRead(MessageHeader messageHeader, int userId)
        {
            messageHeader.Messages.SelectMany(m => m.UserMessages).Where(u => u.ReceiverId == userId).ToList().ForEach(m => m.IsRead = true);

            _dataContext.SaveChanges();
        }

        public int AddProgramIdForOldMessages()
        {
            var messageHeaders = _dataContext.Set<MessageHeader>();

            foreach (var messageHeaer in messageHeaders)
            {
                if (messageHeaer.Type == MessageType.ScheduleDraftSession)
                {
                    var attribute = messageHeaer.Attribute as MessageScheduleDraftSessionAttribute;

                    if (attribute.ProgramId > 0)
                    {
                        messageHeaer.ProgramId = attribute.ProgramId;
                    }
                }
            }

            return _dataContext.SaveChanges();
        }

    }
}
