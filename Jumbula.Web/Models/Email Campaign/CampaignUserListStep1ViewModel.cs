﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Email_Campaign
{
    public class CampaignUserListStep1ViewModel
    {
        public List<UploadedListModel> UploadedList { get; set; }

        public string ListName { get; set; }
    }
}