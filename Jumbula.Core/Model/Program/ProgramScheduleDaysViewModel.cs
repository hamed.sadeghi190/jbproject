﻿using System;

namespace Jumbula.Core.Model.Program
{
    public class ProgramScheduleDaysViewModel
    {
        public DayOfWeek DayOfWeek { get; set; }

        public TimeSpan? FirstStartTime { get; set; }

        public TimeSpan? FirstEndTime { get; set; }

        public TimeSpan? SecondStartTime { get; set; }

        public TimeSpan? SecondEndTime { get; set; }

    }
}
