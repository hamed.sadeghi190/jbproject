using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Jumbula.Bootstrapper.Startup))]
namespace Jumbula.Bootstrapper
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureLog();
        }
    }
}
