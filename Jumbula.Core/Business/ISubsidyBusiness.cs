﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ISubsidyBusiness
    {
        OperationStatus Create(Subsidy subsidy);

        Subsidy Get(int subsidyId);
        OperationStatus Update(Subsidy subsidy);
        IQueryable<Subsidy> GetAllSubsidiesClub(int clubId);
        IQueryable<Subsidy> GetList();
    }
}
