﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Model.Generic;


namespace SportsClub.Areas.Dashboard.Models
{
    public class FilterClubInvoicesViewModel
    {

        public List<SelectKeyValue<string>> InvoiceTypes { get; set; }
    }
    public enum InvoiceType : byte
    {
        All,
        Paid,
        Unpaid,
        [Description("Past due")]
        PastDue,
        Canceled
    }
}