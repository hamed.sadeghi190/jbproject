﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class ProgramRespondToInvitationConfig : EntityTypeConfiguration<ProgramRespondToInvitation>
    {
        public ProgramRespondToInvitationConfig()
        {

            HasRequired(d => d.Program).WithMany(p => p.RespondToInvitation);
        }
    }
}
