﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{

    public enum AutoChargeResultType
    {
        [Description("Installment due date not reached.")]
        NotAttempted = 1,
        [Description("Error conditions other than payment.")]
        AttemptError = 2,
        [Description("Charge was Succeed.")]
        Succsess = 3,
        [Description("Charge was failed.")]
        Faild = 4,
        [Description("Charge was failed by system error.")]
        SystemFaild = 5

    }
}
