﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Models
{
    public class MessageHomeViewModel
    {
        public List<MessageItemViewModel> Messages { get; set; }

        public bool AdminMode { get; set; }
    }

    public class MessageItemViewModel
    {
        public int Id { get; set; }

        public string ChatId { get; set; }
        public string Subject { get; set; }

        public string Sender { get; set; }

        public string Summary { get; set; }

        public string Logo { get; set; }

        public bool Starred { get; set; }

        public string Audience { get; set; }

        public bool IsUnread { get; set; }

        public DateTime DateTime { get; set; }

        public long DateTimeTicks { get; set; }

        public int Unread { get; set; }
    }

    public class RecentMessageViewModel
    {
        public string ClassName { get; set; }

        public string SchoolSeasonName { get; set; }

        public string Day { get; set; }

        public string Grades { get; set; }

        public string ChatId { get; set; }

        public bool IsUnread { get; set; }
    }

    public class MessageComposeViewModel
    {
        
        public string ChatId { get; set; }


        public string Text { get; set; }
    }

    public class MessageReadViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public List<MessageReadItemViewModel> Messages { get; set; }

        public string Sender { get; set; }

    }

    public class MessageReadItemViewModel
    {
        public string Sender{ get; set; }

        public string Title { get; set; }

        public DateTime CreateDate { get; set; }

    }

    public class MessageReplyViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public string Receiver { get; set; }

    }

    public class ChatViewModel
    {
        public ChatViewModel(MessageHeader chat, int currentUserId, Club currentClub)
        {
            var messageBusiness = Ioc.MessageBusiness;

            //MessageAttributeBase chatAttriute = null;
            switch (chat.Type)
            {
                case MessageType.Ordinary:
                    break;
                case MessageType.ScheduleDraftSession:
                    {
                        var scheduleDraftSessionAttribute = ((MessageScheduleDraftSessionAttribute)(chat.Attribute));

                        ShowActions = chat.Type == MessageType.ScheduleDraftSession && (scheduleDraftSessionAttribute.Status == ScheduleDraftSessionStatus.Draft || scheduleDraftSessionAttribute.Status == ScheduleDraftSessionStatus.Modified);

                        ProgramId = scheduleDraftSessionAttribute.ProgramId;

                        MessageStatus = scheduleDraftSessionAttribute.Status.ToString();
                    }
                    break;
                case MessageType.ESignature:
                    {
                        var scheduleDraftSessionAttribute = ((MessageEsignatureAttribute)(chat.Attribute));
                        ShowActions = false;
                        MessageStatus = scheduleDraftSessionAttribute.Status.ToString();
                    }
                    break;
                default:
                    break;
            }

            Unread = messageBusiness.GetUnreadCount(currentUserId, currentClub.Id);
            Subject = chat.Subject;
            Logo = UrlHelpers.GetClubLogoUrl(messageBusiness.GetAudience(chat, currentClub.Id).Domain, messageBusiness.GetAudience(chat, currentClub.Id).Logo);
            MessageType = chat.Type;
            Audience = messageBusiness.GetAudience(chat, currentClub.Id).Name;

            ClubType = currentClub.ClubType.EnumType;
            Messages = chat.Messages.Select(s =>
                new ChatItemViewModel
                {
                    Text = s.Text,
                    //IsFromMe = (s.SenderId == currentUserId && s.ClubId == currentClub.Id),
                    IsFromMe = s.SenderId == currentUserId,
                    DateTime = s.CreatedDate,
                    DateTimeTicks = s.CreatedDate.Ticks
                })
                .ToList();
        }

        public string ChatId { get; set; }
        public string Subject { get; set; }

        public string Logo { get; set; }

        public MessageType MessageType { get; set; }

        public bool ShowActions { get; set; }

        public long ProgramId { get; set; }

        public string MessageStatus { get; set; }

        public string Audience { get; set; }

        public int Unread { get; set; }

        public ClubTypesEnum ClubType { get; set; }

        public List<ChatItemViewModel> Messages { get; set; }
    }

    public class ChatItemViewModel
    {
        public DateTime DateTime { get; set; }

        public long DateTimeTicks { get; set; }

        public string Text { get; set; }

        public bool IsFromMe { get; set; }
    }

    public class MessageActionViewModel
    {
        public MessageActionViewModel()
        {
            ListOfRespondTypes = DropdownHelpers.ToSelectListWithIntValue<ProgramRespondType>();
            ListOfDeclineResaons = DropdownHelpers.ToSelectListWithIntValue<PorgramRespondDeclineResaon>();
            ListOfFirstDaysOfWeek = DropdownHelpers.ToSelectListWithIntValue<DayOfWeek>("Select 1st day preference", -1);
            //FirstDayPreference = DayOfWeek.;
            //SecondDayPreference = -1;
            ListOfSecondDaysOfWeek = DropdownHelpers.ToSelectListWithIntValue<DayOfWeek>("Select 2nd day preference", -1);
            WeekDays = DropdownHelpers.ToSelectList<DayOfWeek>();
            Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);
        }
        public MessageActionType RespondType { get; set; }

        public long ProgramId { get; set; }
        public string Note { get; set; }

        public DayOfWeek? FirstDayPreference { get; set; }
        public DayOfWeek? SecondDayPreference { get; set; }
        public PorgramRespondDeclineResaon DeclineResaon { get; set; }

        public List<SelectKeyValue<int>> ListOfRespondTypes { get; set; }
        public string ProgramName { get; set; }
        public bool AvailableAnyDay { get; set; }

        public List<SelectKeyValue<string>> Grades { get; set; }

        [Required(ErrorMessage = "Min Grade is required.")]
        public SchoolGradeType? SelectedMinGrade { get; set; }

        [Required(ErrorMessage = "Max Grade is required.")]
        public SchoolGradeType? SelectedMaxGrade { get; set; }

        public List<SelectKeyValue<int>> ListOfFirstDaysOfWeek { get; set; }
        //public int FirstDayPreference { get; set; }
        public List<SelectKeyValue<int>> ListOfSecondDaysOfWeek { get; set; }
        public List<SelectKeyValue<int>> ListOfDeclineResaons { get; set; }

        public WeekDaysMode WeekDaysMode { get; set; }

        public List<SelectKeyValue<string>> WeekDays { get; set; }

        public List<string> SelectedWeekDays { get; set; }
    }

    public enum MessageActionType
    {
        Accept = 1,
        ModifyRequest = 2,
        Decline = 3,
        Modify = 4
    }
}