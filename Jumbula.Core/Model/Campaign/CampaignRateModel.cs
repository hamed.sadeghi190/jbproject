﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Core.Model.Campaign
{
    public class CampaignRatesModel
    {
        public decimal OpenedRate { get; set; }

        public decimal ClickedRate { get; set; }

        public decimal DeliveredRate { get; set; }

        public int OpenedNum { get; set; }

        public int ClickedNum { get; set; }

        public int DeliveredNum { get; set; }

        public DateTime? LastClicked { get; set; }

        public DateTime? LastOpened { get; set; }
    }
}
