﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SeasonSurveyDatesReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};
        var isBegin = false;
        $scope.searchTerm = "";

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.filter.printMode = false;

        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            dataInfo = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

       
        var manipulateMemberListGrid = function () {

            isBegin = true;

            $scope.SeasonSurveyDatesDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetSeasonSurveyDatesReportInfo",
                        dataType: "json",
                        //type: "POST",
                        data: function () {
                            return {                               
                                term: $scope.searchTerm,
                                printMode: $scope.filter.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.SeasonSurveyDatesGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Season survey dates).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Season survey dates - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.SeasonSurveyDatesDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#SeasonSurveyDatesGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "Season",
                        title: "Session schedule name",
                        width: "190px"
                    }, {
                        field: "SurveyOpen",
                        title: "Survey opens",
                        width: "120px",
                        sortable: false
                    }, {
                        field: "SurveyClose",
                        title: "Survey closes",
                        width: "120px",
                        sortable: false
                    }, {
                        field: "SurveyResultsDue",
                        title: "Survey results delivered",
                        width: "120px",
                        sortable: false
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $scope.filter.printMode = false;
            $("#SeasonSurveyDatesGrid").getKendoGrid().saveAsExcel();

        }
        $scope.saveToPDF = function () {
            $scope.filter.printMode = true;
            $("#SeasonSurveyDatesGrid").data("kendoGrid").dataSource.read();
            $scope.filter.printMode = false;

        }
        $scope.print = function () {
            $scope.filter.printMode = true;
            $("#SeasonSurveyDatesGrid").data("kendoGrid").dataSource.read();
            $scope.filter.printMode = false;
        }

        $scope.search = function () {
            if (!isBegin) {
                manipulateMemberListGrid();
            } else {
                $scope.filter.printMode = false;
                $("#SeasonSurveyDatesGrid").data("kendoGrid").dataSource.read();
            }

        };

        $scope.clearFilters = function () {
            $scope.searchTerm = "";
            $scope.filter.printMode = false;
            $("#SeasonSurveyDatesGrid").data("kendoGrid").dataSource.read();
        }
    }
    ]);

})();