﻿using Newtonsoft.Json;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeSuccessEmailParameterModel 
    {
        public long InstallmentId { get; set; }

        public long TransactionActivityId { get; set; }

        [JsonIgnore]
        public int ClubId { get; set; }

        [JsonIgnore]
        public string SuccessMessage{ get; set; }
    }
}
