﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Jumbula.Common.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Jumbula.Core.Domain.Generic
{
    public class JbPElementConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {

            var element = value as JbPBaseElement;
            var properties = element.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));

            writer.WriteStartObject();

            writer.WritePropertyName("Type");
            serializer.Serialize(writer, element.GetType().Name);

            if (this.GetType().Assembly.GetName().Name != element.GetType().Assembly.GetName().Name)
            {
                writer.WritePropertyName("Type.FullName");
                serializer.Serialize(writer, element.GetType().FullName);
                writer.WritePropertyName("Type.AssemblyName");
                serializer.Serialize(writer, element.GetType().Assembly.GetName().Name);
            }

            foreach (var property in properties)
            {
                if (Attribute.IsDefined(property, typeof(JsonConverterAttribute)))
                {
                    var jsonAttr = property.GetCustomAttribute<JsonConverterAttribute>(false);
                    if (jsonAttr.ConverterType == typeof(StringEnumConverter))
                    {
                        writer.WritePropertyName(property.Name);
                        serializer.Serialize(writer, Enum.ToObject(property.PropertyType, property.GetValue(element)).ToString());
                    }

                }
                else
                {
                    writer.WritePropertyName(property.Name);
                    serializer.Serialize(writer, property.GetValue(element));
                }

            }

            writer.WriteEndObject();

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JbPBaseElement jbElement;
            JObject jsonObject = JObject.Load(reader);
            return GetJbElement(jsonObject);
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(JbPBaseElement).IsAssignableFrom(objectType);
        }

        #region private

        internal JbPBaseElement GetJbElement(JObject jsonObject)
        {
            var jbElement = new JbPBaseElement();
            if (jsonObject != null)
            {
                var properties = jsonObject.Properties().ToList();

                if (properties.All(p => p.Name != "Type"))
                    return null;
                var elementType = (string)properties.Single(p => p.Name == "Type").Value;
                var typeFull = properties.SingleOrDefault(p => p.Name == "Type.FullName");
                var assemblyName = properties.SingleOrDefault(p => p.Name == "Type.AssemblyName");
                if (elementType == typeof(JbPTitlePack).Name)
                {
                    jbElement = new JbPTitlePack();
                }
                else if (elementType == typeof(JbPDivider).Name)
                {
                    jbElement = new JbPDivider();
                }
                else if (elementType == typeof(JbPRegBtn).Name)
                {
                    jbElement = new JbPRegBtn();
                }
                else if (elementType == typeof(JbPContact).Name)
                {
                    jbElement = new JbPContact();
                }
                else if (elementType == typeof(JbPTab).Name)
                {
                    jbElement = new JbPTab();
                    var innerElements = properties.Single(p => p.Name == "Elements").Value.ToObject<List<object>>();

                    foreach (var element in innerElements)
                    {
                        (jbElement as JbPTab).Elements.Add(GetJbElement(element as JObject));
                    }

                    var innerCover = properties.Single(p => p.Name == "Cover").Value;


                    (jbElement as JbPTab).Cover = GetJbElement(innerCover as JObject) as JbPCover;


                    var innerAppearance = properties.SingleOrDefault(p => p.Name == "Appearance") != null ? properties.SingleOrDefault(p => p.Name == "Appearance").Value : null;

                    if (innerAppearance != null)
                    {
                        (jbElement as JbPTab).Appearance = GetJbElement(innerAppearance as JObject) as JbPAppearance;
                    }
                    else
                    {
                        (jbElement as JbPTab).Appearance = new JbPAppearance();
                    }

                }
                else if (elementType == typeof(JbPSection).Name)
                {
                    jbElement = new JbPSection();
                    var innerElements = properties.Single(p => p.Name == "Columns").Value.ToObject<List<object>>();
                    foreach (var element in innerElements)
                    {
                        (jbElement as JbPSection).Columns.Add(GetJbElement(element as JObject) as JbPSectionColumn);
                    }
                }
                else if (elementType == typeof(JbPSectionColumn).Name)
                {
                    jbElement = new JbPSectionColumn();
                    var innerElements = properties.Single(p => p.Name == "RegLinks").Value.ToObject<List<object>>();
                    foreach (var element in innerElements)
                    {
                        (jbElement as JbPSectionColumn).RegLinks.Add(GetJbElement(element as JObject) as JbPRegLink);
                    }
                }
                else if (elementType == typeof(JbPFooter).Name)
                {
                    jbElement = new JbPFooter();
                }
                else if (elementType == typeof(JbPHeader).Name)
                {
                    jbElement = new JbPHeader();
                    var innerElements = properties.Single(p => p.Name == "Menu").Value.ToObject<List<object>>();
                    foreach (var element in innerElements)
                    {
                        (jbElement as JbPHeader).Menu.Add(GetJbElement(element as JObject) as JbPMenuItem);
                    }
                }
                else if (elementType == typeof(JbPRegLink).Name)
                {
                    jbElement = new JbPRegLink();
                }
                else if (elementType == typeof(JbPMenuItem).Name)
                {
                    jbElement = new JbPMenuItem();
                }
                //cover moved to tab section
                else if (elementType == typeof(JbPCover).Name)
                {
                    jbElement = new JbPCover();
                }
                else if (elementType == typeof(JbPAppearance).Name)
                {
                    jbElement = new JbPAppearance();
                }
                else if (elementType == typeof(JbPMap).Name)
                {
                    jbElement = new JbPMap();
                }
                else if (elementType == typeof(JbPGrid).Name)
                {
                    jbElement = new JbPGrid();
                    var innerElements = properties.Single(p => p.Name == "RegLinks").Value.ToObject<List<object>>();
                    foreach (var element in innerElements)
                    {
                        (jbElement as JbPGrid).RegLinks.Add(GetJbElement(element as JObject) as JbPRegLink);
                    }
                }
                else if (elementType == typeof(JbPSeasonCalendar).Name)
                {
                    jbElement = new JbPSeasonCalendar();

                }
                else if (elementType == typeof(JbPSeasonGrid).Name)
                {
                    jbElement = new JbPSeasonGrid();

                    var innerFilter = properties.SingleOrDefault(p => p.Name == "Filter") != null ? properties.SingleOrDefault(p => p.Name == "Filter").Value : null;

                    if (innerFilter != null)
                    {
                        (jbElement as JbPSeasonGrid).Filter = GetJbElement(innerFilter as JObject) as JbPFilter;
                    }
                    else
                    {
                        (jbElement as JbPSeasonGrid).Filter = new JbPFilter(true);
                    }
                }
                else if (elementType == typeof(JbPVideo).Name)
                {
                    jbElement = new JbPVideo();
                }
                else if (elementType == typeof(JbPBeforeAfterCareRegLinks).Name)
                {
                    jbElement = new JbPBeforeAfterCareRegLinks();

                    var innerSetting = properties.SingleOrDefault(p => p.Name == "ColorSetting") != null ? properties.SingleOrDefault(p => p.Name == "ColorSetting").Value : null;

                    if (innerSetting != null)
                    {
                        (jbElement as JbPBeforeAfterCareRegLinks).ColorSetting = GetJbElement(innerSetting as JObject) as JbPBeforeAfterCareSetting;
                    }
                    else
                    {
                        (jbElement as JbPBeforeAfterCareRegLinks).ColorSetting = new JbPBeforeAfterCareSetting(true);
                    }


                }
                else if (elementType == typeof(JbPImageGallery).Name)
                {
                    jbElement = new JbPImageGallery();

                    var innerElements = properties.Single(p => p.Name == "Images").Value.ToObject<List<object>>();
                    foreach (var element in innerElements)
                    {
                        (jbElement as JbPImageGallery).Images.Add(GetJbElement(element as JObject) as JbPImage);
                    }

                }
                else if (elementType == typeof(JbPImageSlider).Name)
                {
                    jbElement = new JbPImageSlider();

                    var innerElements = properties.Single(p => p.Name == "Images").Value.ToObject<List<object>>();
                    foreach (var element in innerElements)
                    {
                        (jbElement as JbPImageSlider).Images.Add(GetJbElement(element as JObject) as JbPImage);
                    }

                }
                else if (elementType == typeof(JbPImage).Name)
                {
                    jbElement = new JbPImage();
                }
                else if (elementType == typeof(JbPFilter).Name)
                {
                    jbElement = new JbPFilter();

                    var innerElements = properties.SingleOrDefault(p => p.Name == "Grades") != null ? properties.SingleOrDefault(p => p.Name == "Grades").Value.ToObject<List<JbPFilterGradeItem>>() : null;
                    foreach (var element in innerElements)
                    {
                        var gradeItem = element;//as JbPFilterGradeItem;

                        (jbElement as JbPFilter).Grades.Add(gradeItem);
                    }

                    var innerSetting = properties.SingleOrDefault(p => p.Name == "Setting") != null ? properties.SingleOrDefault(p => p.Name == "Setting").Value : null;


                    (jbElement as JbPFilter).Setting = GetJbElement(innerSetting as JObject) as JbPGridSetting;

                }
                else if (elementType == typeof(JbPFilterGradeItem).Name)
                {
                    jbElement = new JbPFilterGradeItem();
                }
                else if (elementType == typeof(JbPGridSetting).Name)
                {
                    jbElement = new JbPGridSetting();
                }
                else if (elementType == typeof(JbPBeforeAfterCareSetting).Name)
                {
                    jbElement = new JbPBeforeAfterCareSetting();
                }
                else
                {
                    Type typ;
                    if (typeFull == null)
                        typ =
                            Type.GetType(
                                string.Format("{0}.Forms.{1}, {0}", this.GetType().Assembly.GetName().Name, elementType),
                                true);
                    else
                        typ = Type.GetType((string)typeFull.Value + "," + (string)assemblyName.Value, true);

                    jbElement = (JbPBaseElement)Activator.CreateInstance(typ);
                }

                foreach (var property in properties)
                {
                    if (property.Name.Equals("Type"))
                        continue;
                    var proInfo = jbElement.GetType().GetProperty(property.Name);
                    if (proInfo == null)
                        continue;
                    else if (Attribute.IsDefined(proInfo, typeof(JsonConverterAttribute)))
                    {
                        var jsonAttr = proInfo.GetCustomAttribute<JsonConverterAttribute>(false);
                        if (jsonAttr.ConverterType == typeof(StringEnumConverter))
                        {
                            var mode = property.Value.ToObject(proInfo.PropertyType);
                            proInfo.SetValue(jbElement, Enum.ToObject(proInfo.PropertyType, mode));
                        }

                    }
                    else
                    {
                        if (typeof(System.Collections.IList).IsAssignableFrom(proInfo.PropertyType)  || typeof(RegistrationPeriod) == proInfo.PropertyType || typeof(JbPGridSetting) == proInfo.PropertyType || typeof(JbPBeforeAfterCareSetting) == proInfo.PropertyType || typeof(JbPBeforeAfterCareRegLinks) == proInfo.PropertyType || typeof(Genders?) == proInfo.PropertyType || typeof(SchoolGradeType?) == proInfo.PropertyType || typeof(SchoolGradeType) == proInfo.PropertyType || typeof(JbPAppearance) == proInfo.PropertyType || typeof(JbPCover) == proInfo.PropertyType || typeof(JbPMap) == proInfo.PropertyType || typeof(JbPFilter) == proInfo.PropertyType || typeof(JbPFilterGradeItem) == proInfo.PropertyType || typeof(DropInPunchCardInformation) == proInfo.PropertyType)
                            continue;
                        if (!string.IsNullOrEmpty((string)property.Value))
                            proInfo.SetValue(jbElement, Convert.ChangeType(property.Value, proInfo.PropertyType));
                    }

                }
            }

            return jbElement as JbPBaseElement;
        }

        #endregion
    }
}