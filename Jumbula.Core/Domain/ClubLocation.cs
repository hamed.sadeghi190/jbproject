﻿using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class ClubLocation : BaseEntity
    {

        public virtual PostalAddress PostalAddress { get; set; }

        public virtual Club Club { get; set; }

        public string Name { get; set; }

        public bool IsDeleted { get; set; }
    }
}
