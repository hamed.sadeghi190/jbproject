﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaAccountAlreadyExistsException : JumbulaClubImportException
    {
        public JumbulaAccountAlreadyExistsException(Exception e)
            : base(e)
        {

        }
    }
}
