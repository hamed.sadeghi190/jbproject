﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programAddBladeController', ['$scope', '$http', '$log','$stateParams','$state', function ($scope, $http, $log,$stateParams,$state) {
        window.app.globalObjects.pageClass = 'seasons-view delete-program';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.MC.openBlade({ url: "Dashboard/Blade/ProgramAdd", order: 3, menu: "SEASONS" });

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.programName = $stateParams.programDomain;

    }]);
})();