﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('familyOrdersController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'family-orders-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });

        $scope.MC.stateParams = $stateParams;

        $scope.filter = {};
        $scope.filter.confirmationId = "";
        $scope.filter.participantId = -1;
        $scope.filter.statusReasonId = -1;
        $scope.filter.startDate = "";
        $scope.filter.endDate = "";
        $scope.pageTitle = "";
        $scope.orderDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: window.location.origin + '/Dashboard/Order/GetUserOrders/?userId=' + $scope.MC.stateParams.userId + '&clubId=' + $scope.MC.stateParams.clubId,
                    cache: false
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            confirmationId: $scope.filter.confirmationId,
                            participantId: $scope.filter.participantId,
                            statusReasonId: $scope.filter.statusReasonId,
                            startDate:$scope.filter.startDate ,
                            endDate: $scope.filter.endDate,
                            skip: data.skip,
                            take: data.take,
                            page: data.page,
                            pageSize: data.pageSize,
                            sort: data.sort
                        }
                    }
                }
            },
            pageSize: 10,
            batch: true,
            serverPaging: true,
            serverSorting: false,
            schema: {
                data: "data",
                total: "total"
            }
        });

    
        $http.get(window.location.origin + '/dashboard/people/GetUserListDetail/?userId=' + $scope.MC.stateParams.userId + '&clubId=' + $scope.MC.stateParams.clubId).success(function (data, status, headers, config) {
            $scope.Model = data;
            if ($scope.Model.TagName != "") {
                $scope.pageTitle = $scope.Model.TagName;
            }
            else
            {
                $scope.pageTitle = $scope.Model.UserName;
            }

        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.orderGridOptions = {
            dataSource: $scope.orderDataSource,
            sortable: true,
            serverPaging: true,
            columns: [{
                field: "Attendee",
                title: "Participant",
                width: "150px",
                template: "<a ng-click='viewOrderItem(#=Id#,\"#=SeasonDomain#\")'  class='block'> #= Attendee # #if(ItemStatusReason == '1') {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == '2') {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == '5') {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == '4'){# <i class='grid-label label warning'>Canceled</i> #}#"
            }, {
                field: "StrOrderDate",
                title: "Order date",
                width: "140px",
                
            }, {
                field: "ConfirmationId",
                title: "Confirmation",
                width: "120px",
                template: "<a ui_sref='Order({seasonDomain:\"#=SeasonDomain#\",orderId:  #=OrderId#})'  class='block'> #= ConfirmationId # </a>"
            },
            {
                field: "Season",
                title: 'Season',
                width: '120px',
                template: '<span>#=Season#</span>'
            },
            
            {
                field: "ProgramName",
                title: 'Program',

            }
            , {
                field: "Balance",
                title: 'Balance',
                width: '70px',
                template: "<span>#if(ItemStatusReason == '2'){# <p>-</p> #} else if(Balance >0) {# <p class='fg-red'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance <0) {# <p class='fg-green'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance ==0) {# <p>#=Balance#</p> #}#"
            }, {
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li ng-click="viewOrderItem(#=Id#,\'#=SeasonDomain#\')">\
                                          <span class="k-link jbi-angle-right">View details</span>\
                                    </li>\
                                    <li ng-hide="#=ItemStatus!=3 || ProgramType ==\'Subscription\' || ProgramType ==\'BeforeAfterCare\' || ProgramType ==\'Calendar\' #" ng-click="edit(#=Id#,\'#=SeasonDomain#\')">\
                                          <span class="k-link jbi-pencil">Edit/Cancel</span>\
                                    </li>\
                                    <li ngaction="Order_Edit" ng-hide="#=ItemStatus!=3 ||  IsDropIn == false && ProgramType !=\'BeforeAfterCare\' || IsDropIn == false && PaymentPlanType != \'Installment\' #" ng-click="editDropIn(#=Id#)">\
                                          <span class="k-link jbi-pencil">Edit/Cancel</span>\
                                    </li>\
                                    <li ng-hide="#=ItemStatus!=3 && ItemStatus!=4  || ProgramType !=\'Subscription\' ||  PaymentPlanType != 1|| IsDropIn != false ||  ProgramType ==\'Calendar\' #" ng-click="edit(#=Id#,\'#=SeasonDomain#\')">\
                                          <span class="k-link jbi-pencil">Edit</span>\
                                    </li>\
                                    <li ng-show="#=(Balance > 0 && PaymentPlanType==0 ) || (EnableInstallmentTakePayment > 0 && PaymentPlanType==1)#" ng-click="makePayment(#= Id#,\'#=SeasonDomain#\',#=ProgramId#)">\
                                          <span class="k-link jbi-coins">Take a payment</span>\
                                    </li>\
                                        <li ngaction="invoice_View" ng-show="#=Balance>0 && IsSandbox !=true#" ui_sref="FamilyInvoicesOrTakePayment({userId:#=UserId#,type:\'Invoice\',orderId:#=OrderId#})">\
                                          <span class="k-link jbi-invoic">Invoice</span>\
                                    </li>\
                                    <li ng-show="#=(Balance < 0 && PaymentPlanType==0 ) || (EnableInstallmentRefund >0 && PaymentPlanType==1)#" ng-click="refund(#= Id#,\'#=SeasonDomain#\',#=ProgramId#)">\
                                          <span title="Refund" class="k-link jbi-fully-refund">Refund</span>\
                                    </li>\
                                    <li ng-hide="#=ItemStatus!=3 || ProgramType ==\'ChessTournament\' || ProgramType ==\'Subscription\' || ProgramType ==\'BeforeAfterCare\' || ProgramName == \'Donation\' || IsDropIn != false || ProgramType ==\'Calendar\'#" ng-click="transfer(#=Id#,\'#=SeasonDomain#\' )">\
                                          <span class="k-link jbi-transfer">Transfer</span>\
                                    </li>\
                                    <li ng-hide="#=ItemStatus!=3 || ProgramType ==\'ChessTournament\' || PaymentPlanType != 1 || ProgramType !=\'Subscription\' || ProgramType ==\'BeforeAfterCare\' || ProgramName == \'Donation\' || IsDropIn != false || ProgramType ==\'Calendar\'#" ng-click="transfer(#=Id#,\'#=SeasonDomain#\' )">\
                                          <span class="k-link jbi-transfer">Transfer</span>\
                                    </li>\
                                    <li ng-click="history(#=Id#,#=OrderId#,\'#=SeasonDomain#\')">\
                                          <span class="k-link icon-history">History</span>\
                                    </li>\
                                     #if(ProgramType ==\'Subscription\' && IsDropIn == false){#\
                                     <hr/>\
                                      <li ng-click="ChangeDays(#:Id#)" >\
                                          <span class="k-link">Change class days</span>\
                                      </li>\
                                      <li ng-hide="#=PaymentPlanType != 1#"  ng-click="ChangeDesiredStart(#:Id#)" >\
                                          <span class="k-link">Change desired start date</span>\
                                      </li>\
                                   #}#\
                            </ul>\
                        </li>\
                    </ul>'
            }],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
        };

        $scope.search = function () {
            $("#orderGrid").data("kendoGrid").dataSource.read();
        };

        $scope.viewOrderItem = function (id, seasonDomain) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            $state.go('OrderItem', { seasonDomain: seasonDomain, orderItemId: id });
        };

        $scope.transfer = function (id, seasonDomain) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            $state.go('Transfer', { seasonDomain: seasonDomain, orderId: id });
        };

        $scope.edit = function (id, seasonDomain) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            window.clubId = $scope.MC.stateParams.clubId;
            $state.go('EditOrder', { seasonDomain: seasonDomain, orderId: id });
        };

        $scope.history = function (id, orderId, seasonDomain) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            $state.go('OrderHistory', { seasonDomain: seasonDomain, orderId: orderId, orderItemId: id });
        };
        $scope.makePayment = function (id, seasonDomain, programId) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            $state.go("MakePayment", { seasonDomain: seasonDomain, programId: programId, orderItemId: id });
        }
        $scope.refund = function (id, seasonDomain, programId) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            window.clubId = $scope.MC.stateParams.clubId;
            $state.go("PartiallyRefundOrderItem", { seasonDomain: seasonDomain, programId: programId, orderItemId: id });
        }
        $scope.ChangeDays = function (id) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            $state.go("OrderChandgeDays", { seasonDomain: $stateParams.seasonDomain, orderId: id });
        }
        $scope.ChangeDesiredStart = function (id) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            $state.go("OrderChandgeDesiredStartDate", { seasonDomain: $stateParams.seasonDomain, orderId: id });
        }
        $scope.cancelSubscription = function (id) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            $state.go("CancelSubscriptionOrder", { seasonDomain: $stateParams.seasonDomain, orderId: id });
        }
        $scope.editDropIn = function (id) {
            $state.go("EditCancelDropInOrder", { seasonDomain: $stateParams.seasonDomain, orderId: id });
        }
        $scope.clearFilters = function () {
            $scope.filter.confirmationId = "";
            $scope.filter.participantId = -1;
            $scope.filter.statusReasonId = -1;
            $scope.filter.startDate = "";
            $scope.filter.endDate = "";
            $("#orderGrid").data("kendoGrid").dataSource.read();
        }
    }])
})();
