﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.Api
{

    public class PickupStatusModel_V2_2_1
    {
        public string Id { get; set; }
        public string DismissalFromEnrichment { get; set; }
        public string Phone { get; set; }
        public bool IsPickedUp { get; set; }
        public DateTime LocalDateTime { get; set; }
        public string FullName { get; set; }
        public int? ClubStaffId { get; set; }

    }

    public class PickupStatusModel
    {
        public string Id { get; set; }
        public string DismissalFromEnrichment { get; set; }
        public string Phone { get; set; }
        public bool IsPickedUp { get; set; }
        public DateTime LocalDateTime { get; set; }
        public string FullName { get; set; }

    }

    public class SessionPickupModel
    {
        public string Id { get; set; }
        public string StudentName { get; set; }
        public string DismissalFromEnrichment { get; set; }
        public List<string> AuthorizeAdultPickupName { get; set; }
        public List<string> AuthorizeAdultPickupPhone { get; set; }
        public string Parent1FirstName { get; set; }
        public string Parent1LastName { get; set; }
        public string Parent1PrimaryPhone { get; set; }
        public string Parent2FirstName { get; set; }
        public string Parent2LastName { get; set; }
        public string Parent2PrimaryPhone { get; set; }
        public bool IsPickedUp { get; set; }
        public string PickupTime { get; set; }
        public string PickupName { get; set; }
        public object AdditionalInfo { get; set; }
        public string ProgramName { get; set; }
        public long ProgramId { get; set; }
    }
    
    public class SessionAbsentee
    {
        public string Id { get; set; }
        public string StudentName { get; set; }
        public string ParentFullName { get; set; }
        public string ParentPhone { get; set; }
        public string ProgramName { get; set; }
        public long ProgramId { get; set; }
    }
    



}