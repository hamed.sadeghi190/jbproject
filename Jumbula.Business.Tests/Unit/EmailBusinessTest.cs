﻿using Autofac.Features.Indexed;
using FluentAssertions;
using Jumbula.Common.Delegates;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Infrastructure;
using Jumbula.Core.Infrastructure.Email;
using Jumbula.Core.Model.Email;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using Jumbula.Common.Helper;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class EmailBusinessTest
    {
        #region Constants
        private const string NoReplyEmailAddress = "noreply@jumbula.com";

        private const string FromEmailAddress = "from@jumbula.com";
        private const string ToEmailAddress = "email@jumbula.com";
        private const string CcEmailAddress = "ccemail@jumbula.com";
        private const string BccEmailAddress = "bccemail@jumbula.com";

        private const string FromEmailDisplayName = "From jumbula";
        private const string ToEmailDisplayName = "Jumbula To recipient";
        private const string CcEmailDisplayName = "Jumbula Cc recipient";
        private const string BccEmailDisplayName = "Jumbula Bcc recipient";

        private const string EmailSubject = "Test email";
        private const string EmailBody = "Hello all recipients";

        private const int EmailId = 100;
        private const int ClubId = 10;

        #endregion

        #region Fields
        private Mock<IEmailRepository> _mockEmailRepository;
        private Mock<IIndex<EmailCategory, IEmailBuilder>> _mockBuilder;
        private Mock<IJbDateTime> _mockJbDateTime;
        private Mock<IJbEmailService> _mockJbEmailService;
        private Mock<IJbLocker> _mockJbLocker;
        private bool _isProductionMode;

        private DateTime _utcNow;

        private EmailBusiness _service;
        #endregion

        #region Public Methods
        [TestMethod]
        public void Send_whenEmailCategoryChanged_GetProperCategory()
        {
            //Arrange
            var service = CreateService();

            var parameter = FillAutoChargeSuccessEmailParameterModel();
            var emailCategory = EmailCategory.AutoChargeSuccess;
            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[emailCategory]).Returns(mockEmailBuilder.Object);
            mockEmailBuilder.Setup(e => e.Category).Returns(emailCategory);

            //Act
            service.Send(emailCategory, parameter);

            //Assert
            mockEmailBuilder.Object.Category.Should().Be(emailCategory);
        }

        [TestMethod]
        public void Send_PriorityIsNormal_ShouldQueueEmail()
        {
            //Arrange
            var service = CreateService();
            var parameter = FillAutoChargeSuccessEmailParameterModel();
            var emails = CreateEmails().Where(e => e.Priority == EmailSendPriority.Normal).ToList();
            List<Email> backedEmails = null;
            var mailMessage = CreateMailMessage();
            var emailCompletedEventHandler = CreateCompletedEventHandler();

            _mockEmailRepository.Setup(e => e.Create(emails)).Returns(new OperationStatus() { Status = true, RecordsAffected = 1 }).Callback<List<Email>>(e => backedEmails = e);

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            mockEmailBuilder.Setup(e => e.GetEmails(parameter, _isProductionMode)).Returns(emails);
            mockEmailBuilder.Setup(e => e.EmailSendPriority).Returns(EmailSendPriority.Normal);

            _mockJbEmailService.Setup(e => e.Send(mailMessage, 1, emailCompletedEventHandler, false));

            //Act
            service.Send(EmailCategory.AutoChargeSuccess, parameter);

            //Assert
            _mockEmailRepository.Verify(e => e.Create(emails), Times.Once);
            _mockJbEmailService.Verify(e => e.Send(It.IsAny<MailMessage>(), 1, It.IsAny<EmailCompletedEventHandler>(), false), Times.Never);
            backedEmails.Should().Equal(emails);
        }

        [TestMethod]
        public void Send_PriorityIsHigh_ShouldQueueEmailAndSend()
        {
            //Arrange
            var service = CreateService();
            var parameter = FillAutoChargeSuccessEmailParameterModel();
            var emails = CreateEmails().Where(e => e.Priority == EmailSendPriority.High).ToList();
            List<Email> backedEmails = null;
            var emailCompletedEventHandler = CreateCompletedEventHandler();
            var mailMessage = CreateMailMessage();

            _mockEmailRepository.Setup(e => e.Create(emails)).Returns(new OperationStatus() { Status = true, RecordsAffected = 1 }).Callback<List<Email>>(e => backedEmails = e);

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            mockEmailBuilder.Setup(e => e.GetEmails(parameter, _isProductionMode)).Returns(emails);
            mockEmailBuilder.Setup(e => e.EmailSendPriority).Returns(EmailSendPriority.High);

            _mockJbEmailService.Setup(e => e.Send(mailMessage, 1, emailCompletedEventHandler, false));


            //Act
            service.Send(EmailCategory.AutoChargeSuccess, parameter);

            //Assert
            _mockEmailRepository.Verify(e => e.Create(emails), Times.Once);
            _mockJbEmailService.Verify(e => e.Send(It.Is<MailMessage>(m => m.To.All(a => a.Address.Equals(ToEmailAddress))), 0, It.IsAny<EmailCompletedEventHandler>(), false), Times.Exactly(emails.Count()));
            backedEmails.Should().Equal(emails);
        }

        [TestMethod]
        public void Send_ScheduleDateTimeIsNull_StatusShouldBeImmediateSend()
        {
            //Arrange
            var service = CreateService();

            var parameter = FillAutoChargeSuccessEmailParameterModel();
            var emails = CreateEmails();
            List<Email> backedEmails = null;
            var emailCompletedEventHandler = CreateCompletedEventHandler();
            var mailMessage = CreateMailMessage();

            _mockEmailRepository.Setup(e => e.Create(emails)).Returns(new OperationStatus() { Status = true, RecordsAffected = 1 }).Callback<List<Email>>(e => backedEmails = e);

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            mockEmailBuilder.Setup(e => e.GetEmails(parameter, _isProductionMode)).Returns(emails);
            mockEmailBuilder.Setup(e => e.EmailSendPriority).Returns(EmailSendPriority.Normal);

            _mockJbEmailService.Setup(e => e.Send(mailMessage, 1, emailCompletedEventHandler, false));

            //Act
            service.Send(EmailCategory.AutoChargeSuccess, parameter);

            //Assert
            backedEmails.ForEach(b => b.ScheduleType.Should().Be(EmailScheduleType.ImmediateSend));
        }

        [TestMethod]
        public void Send_ScheduleDateIsNotNull_StatusShouldBeScheduled()
        {
            //Arrange
            var service = CreateService();

            var parameter = FillAutoChargeSuccessEmailParameterModel();
            var emails = CreateEmails();
            List<Email> backedEmails = null;
            var mailMessage = CreateMailMessage();
            var emailCompletedEventHandler = CreateCompletedEventHandler();


            _mockEmailRepository.Setup(e => e.Create(emails)).Returns(new OperationStatus() { Status = true, RecordsAffected = 1 }).Callback<List<Email>>(e => backedEmails = e);

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            mockEmailBuilder.Setup(e => e.GetEmails(parameter, _isProductionMode)).Returns(emails);
            mockEmailBuilder.Setup(e => e.EmailSendPriority).Returns(EmailSendPriority.Normal);

            _mockJbEmailService.Setup(e => e.Send(mailMessage, 1, emailCompletedEventHandler, false));

            var scheduleDate = DateTime.UtcNow.AddDays(1);

            //Act
            service.Send(EmailCategory.AutoChargeSuccess, parameter, scheduleDate);

            //Assert
            backedEmails.ForEach(b => b.ScheduleType.Should().Be(EmailScheduleType.Scheduled));
        }

        [TestMethod]
        public void Send_SendToSupportIsDisabled_EmailsShouldBeEqualsToSubCategories()
        {
            //Arrange
            var service = CreateService();

            var parameter = FillAutoChargeSuccessEmailParameterModel();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var emails = new List<Email>();

            var subCategories = new List<string> { "Parent", "Admin" };


            for (var i = 0; i < subCategories.Count; i++)
                emails.Add(FactoryGirl.Build<Email>());

            mockEmailBuilder.Setup(e => e.SubCategories).Returns(subCategories);
            mockEmailBuilder.Setup(e => e.ShouldSendToSupport).Returns(false);
            mockEmailBuilder.Setup(e => e.GetEmails(parameter, false)).Returns(emails);

            var backedEmails = new List<Email>();
            _mockEmailRepository.Setup(e => e.Create(emails)).Returns(new OperationStatus() { Status = true, RecordsAffected = 1 }).Callback<List<Email>>(e => backedEmails = e);

            //Act
            service.Send(EmailCategory.AutoChargeSuccess, parameter);

            //Assert
            backedEmails.Count.Should().Be(subCategories.Count);

        }

        [TestMethod]
        public void Send_SendToSupportIsEnabled_EmailsShouldBeEqualsToSubCategoriesPlusOne()
        {
            //Arrange
            _isProductionMode = true;
            var service = CreateService(_isProductionMode);

            var parameter = FillAutoChargeSuccessEmailParameterModel();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var emails = new List<Email>();

            var subCategories = new List<string> { "Parent", "Admin" };

            mockEmailBuilder.Setup(e => e.SubCategories).Returns(subCategories);
            mockEmailBuilder.Setup(e => e.ShouldSendToSupport).Returns(true);

            for (var i = 0; i < subCategories.Count; i++)
                emails.Add(FactoryGirl.Build<Email>());

            if (_isProductionMode)
                emails.Add(FactoryGirl.Build<Email>());

            mockEmailBuilder.Setup(e => e.GetEmails(parameter, _isProductionMode)).Returns(emails);

            var backedEmails = new List<Email>();
            _mockEmailRepository.Setup(e => e.Create(emails)).Returns(new OperationStatus() { Status = true, RecordsAffected = 1 }).Callback<List<Email>>(e => backedEmails = e);

            //Act
            service.Send(EmailCategory.AutoChargeSuccess, parameter);

            //Assert
            backedEmails.Count.Should().Be(subCategories.Count + 1);

        }

        [TestMethod]
        public void SendAllQueued_OneThreadIsRunning_ShouldNotAllowAnotherThread()
        {
            //Arrange
            var service = CreateSingletonService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            _mockEmailRepository.Setup(e => e.GetPendingEmails(null)).Returns(new List<Email>() { FactoryGirl.Build<Email>() }.AsQueryable());

            var inProgressThread = new Thread(RunSendAllQueued);
            inProgressThread.Start();

            //Act
            service.SendAllQueued();

            inProgressThread.Join();

            //Assert
            _mockEmailRepository.Verify(e => e.GetPendingEmails(null), Times.Once);
        }


        [TestMethod]
        public void SendAllQueued_ThereIsNoAnyQueued_ShouldNotSendAny()
        {
            //Arrange
            var service = CreateService();
            var email = FactoryGirl.Build<Email>();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            _mockEmailRepository.Setup(e => e.GetPendingEmails(null))
                        .Returns(new List<Email>() { email }.AsQueryable());

            //Act
            service.SendAllQueued();

            //Assert
            _mockJbEmailService.Verify(e => e.Send(It.IsAny<MailMessage>(), 1, It.IsAny<EmailCompletedEventHandler>(), false), Times.Never);
        }

        [TestMethod]
        public void SendAllQueued_FilterOnEmailCategory_ShouldSendOnlyEmailsFromFilteredCategory()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var emailCategory = EmailCategory.AutoChargeSuccess;
            var emails = CreateEmails().Where(e => e.Category == emailCategory && e.Priority != EmailSendPriority.High).ToList();

            _mockEmailRepository.Setup(e => e.GetPendingEmails(emailCategory))
                        .Returns(emails.Where(e => !e.ScheduleDate.HasValue || e.ScheduleDate <= _utcNow).AsQueryable());

            //Act
            service.SendAllQueued(emailCategory);

            //Assert
            _mockJbEmailService.Verify(e => e.Send(It.Is<MailMessage>(m => m.To.All(a => a.Address.Equals(ToEmailAddress))), 0, It.IsAny<EmailCompletedEventHandler>(), false), Times.Exactly(emails.Count(e => !e.ScheduleDate.HasValue || e.ScheduleDate <= _utcNow)));
        }

        [TestMethod]
        public void SendAllQueued_ScheduleDateIsBeforeNow_ShouldSend()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var email = FactoryGirl.Build<Email>();
            email.Priority = EmailSendPriority.Normal;
            email.ScheduleDate = _utcNow.AddDays(-1);

            _mockEmailRepository.Setup(r => r.GetPendingEmails(null))
                             .Returns(new List<Email> { email }.AsQueryable());

            //Act
            service.SendAllQueued();

            //Assert
            _mockJbEmailService.Verify(e => e.Send(It.Is<MailMessage>(m => m.To.First().Address.Equals(ToEmailAddress)), 1, It.IsAny<EmailCompletedEventHandler>(), false), Times.Once);
        }

        [TestMethod]
        public void SendAllQueued_ScheduleDateIsEqualsNow_ShouldSend()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var email = FactoryGirl.Build<Email>();
            email.Priority = EmailSendPriority.Normal;
            email.ScheduleDate = _utcNow;

            _mockEmailRepository.Setup(r => r.GetPendingEmails(null))
                             .Returns(new List<Email> { email }.AsQueryable());

            //Act
            service.SendAllQueued();

            //Assert
            _mockJbEmailService.Verify(e => e.Send(It.Is<MailMessage>(a => a.To.First().Address.Equals(ToEmailAddress)), 1, It.IsAny<EmailCompletedEventHandler>(), false), Times.Once);
        }

        [TestMethod]
        public void SendAllQueued_ScheduleDateIsAfterNow_ShouldNotSend()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var email = FactoryGirl.Build<Email>();
            email.Priority = EmailSendPriority.Normal;
            email.ScheduleDate = _utcNow.AddDays(1);

            _mockEmailRepository.Setup(r => r.GetPendingEmails(null))
                              .Returns(new List<Email> { email }.AsQueryable());

            //Act
            service.SendAllQueued();

            //Assert
            _mockJbEmailService.Verify(e => e.Send(It.IsAny<MailMessage>(), It.IsAny<int>(), It.IsAny<EmailCompletedEventHandler>(), false), Times.Never);
        }

        [TestMethod]
        public void SendAllQueued_EmailHasOneRecipient_ShouldSendWithCorrectSubjectAndBodyEtc()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var email = FactoryGirl.Build<Email>();

            email.Subject = EmailSubject;
            email.Priority = EmailSendPriority.Normal;
            email.FromEmail = FromEmailAddress;
            email.FromDisplayName = FromEmailDisplayName;
            email.ReplyTo = NoReplyEmailAddress;
            email.ScheduleDate = _utcNow;

            var toRecipient = FactoryGirl.Build<EmailRecipient>();
            toRecipient.Type = EmailRecipientType.To;
            toRecipient.EmailAddress = ToEmailAddress;
            toRecipient.DisplayName = ToEmailDisplayName;

            email.Recipients = new List<EmailRecipient> { toRecipient };

            mockEmailBuilder.Setup(e => e.GenerateBody(email, EmailBodyGenerationMode.Normal)).Returns(EmailBody);
            _mockEmailRepository.Setup(r => r.GetPendingEmails(null))
                                   .Returns(new List<Email> { email }.AsQueryable());

            var mailMessage = new MailMessage
            {
                Subject = EmailSubject,
                Body = EmailBody,
                From = new MailAddress(FromEmailAddress, FromEmailDisplayName),
                IsBodyHtml = true,

            };

            mailMessage.To.Add(new MailAddress(ToEmailAddress, ToEmailDisplayName));
            mailMessage.ReplyToList.Add(new MailAddress(NoReplyEmailAddress, NoReplyEmailAddress));
            mailMessage.Priority = MailPriority.Normal;

            MailMessage backedMailMessage = null;
            _mockJbEmailService.Setup(r => r.Send(It.Is<MailMessage>(d => d.To.First().Address == ToEmailAddress), 1, It.IsAny<EmailCompletedEventHandler>(), false)).Callback<MailMessage, int, EmailCompletedEventHandler>((e, f, g) => backedMailMessage = e);

            //Act
            service.SendAllQueued();

            //Assert
            backedMailMessage.Subject.Should().Be(EmailSubject);
            backedMailMessage.Body.Should().Be(EmailBody);
            backedMailMessage.From.Address.Should().Be(FromEmailAddress);
            backedMailMessage.From.DisplayName.Should().Be(FromEmailDisplayName);
            backedMailMessage.ReplyToList.First().Should().Be(NoReplyEmailAddress);

            backedMailMessage.To.Should().HaveCount(1);
            backedMailMessage.To.First().Address.Should().Be(ToEmailAddress);
            backedMailMessage.To.First().DisplayName.Should().Be(ToEmailDisplayName);
        }


        [TestMethod]
        public void SendAllQueued_EmailHasNotCc_ShouldNotSendCc()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var email = FactoryGirl.Build<Email>();
            email.Priority = EmailSendPriority.Normal;
            email.ScheduleDate = _utcNow;

            var toRecipient = FactoryGirl.Build<EmailRecipient>();
            toRecipient.Type = EmailRecipientType.To;

            email.Recipients = new List<EmailRecipient> { toRecipient };

            _mockEmailRepository.Setup(r => r.GetPendingEmails(null))
                              .Returns(new List<Email> { email }.AsQueryable());

            MailMessage backedMailMessage = null;
            _mockJbEmailService.Setup(r => r.Send(It.Is<MailMessage>(m => m.To.First().Address.Equals(ToEmailAddress)), 1, It.IsAny<EmailCompletedEventHandler>(), false)).Callback<MailMessage, int, EmailCompletedEventHandler>((e, f, g) => backedMailMessage = e);

            //Act
            service.SendAllQueued();

            //Assert
            backedMailMessage.CC.Should().HaveCount(0);
        }

        [TestMethod]
        public void SendAllQueued_EmailHasCc_ShouldSendCcAsCcAndWithCorrectNameAndAddress()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var email = FactoryGirl.Build<Email>();
            email.Priority = EmailSendPriority.Normal;

            var toRecipient = FactoryGirl.Build<EmailRecipient>();
            toRecipient.Type = EmailRecipientType.To;

            var ccRecipient = FactoryGirl.Build<EmailRecipient>();
            ccRecipient.Type = EmailRecipientType.Cc;
            ccRecipient.EmailAddress = CcEmailAddress;
            ccRecipient.DisplayName = CcEmailDisplayName;

            var emailEventHandler = CreateCompletedEventHandler();
            var mailMessage = new MailMessage
            {
                From = new MailAddress(FromEmailAddress, FromEmailDisplayName),
            };
            mailMessage.CC.Add(new MailAddress(CcEmailAddress, CcEmailDisplayName));
            mailMessage.To.Add(new MailAddress(ToEmailAddress, ToEmailDisplayName));

            email.Recipients = new List<EmailRecipient> { toRecipient, ccRecipient };

            _mockEmailRepository.Setup(r => r.GetPendingEmails(null))

                .Returns(new List<Email> { email }.AsQueryable());

            _mockJbDateTime.Setup(e => e.UtcNow).Returns(_utcNow.AddDays(1));

            MailMessage backedMailMessage = null;
            _mockJbEmailService.Setup(r => r.Send(It.Is<MailMessage>(m => m.CC.First().Address == CcEmailAddress), 1, It.IsAny<EmailCompletedEventHandler>(), false)).Callback<MailMessage, int, EmailCompletedEventHandler>((e, f, g) => backedMailMessage = e);

            //Act
            service.SendAllQueued();

            //Assert
            backedMailMessage.CC.Should().HaveCount(1);
            backedMailMessage.CC.First().Address.Should().Be(CcEmailAddress);
            backedMailMessage.CC.First().DisplayName.Should().Be(CcEmailDisplayName);
        }

        [TestMethod]
        public void SendAllQueued_EmailHasNotBCC_ShouldNotSendBCC()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var email = FactoryGirl.Build<Email>();
            email.Priority = EmailSendPriority.Normal;
            email.ScheduleDate = null;

            var toRecipient = FactoryGirl.Build<EmailRecipient>();
            toRecipient.Type = EmailRecipientType.To;

            email.Recipients = new List<EmailRecipient> { toRecipient };

            _mockEmailRepository.Setup(r => r.GetPendingEmails(null))
                              .Returns(new List<Email> { email }.AsQueryable());

            MailMessage backedMailMessage = null;
            _mockJbEmailService.Setup(r => r.Send(It.Is<MailMessage>(s => s.To.First().Address == ToEmailAddress), 1, It.IsAny<EmailCompletedEventHandler>(), false)).Callback<MailMessage, int, EmailCompletedEventHandler>((e, f, g) => backedMailMessage = e);

            //Act
            service.SendAllQueued();

            //Assert
            backedMailMessage.Bcc.Should().HaveCount(0);
        }

        [TestMethod]
        public void SendAllQueued_EmailHasBCC_ShouldSendBCCAsBCCAndWithCorrectNameAndAddress()
        {
            //Arrange
            var service = CreateService();

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            var email = FactoryGirl.Build<Email>();
            email.ScheduleDate = null;
            email.Priority = EmailSendPriority.Normal;

            var toRecipient = FactoryGirl.Build<EmailRecipient>();
            toRecipient.Type = EmailRecipientType.To;

            var ccRecipient = FactoryGirl.Build<EmailRecipient>();
            ccRecipient.Type = EmailRecipientType.Bcc;
            ccRecipient.EmailAddress = BccEmailAddress;
            ccRecipient.DisplayName = BccEmailDisplayName;

            email.Recipients = new List<EmailRecipient> { toRecipient, ccRecipient };

            _mockEmailRepository.Setup(r => r.GetPendingEmails(null))
                             .Returns(new List<Email> { email }.AsQueryable());

            MailMessage backedMailMessage = null;
            _mockJbEmailService.Setup(r => r.Send(It.Is<MailMessage>(s => s.Bcc.First().Address == BccEmailAddress), 1, It.IsAny<EmailCompletedEventHandler>(), false)).Callback<MailMessage, int, EmailCompletedEventHandler>((e, f, g) => backedMailMessage = e);

            //Act
            service.SendAllQueued();

            //Assert
            backedMailMessage.Bcc.Should().HaveCount(1);
            backedMailMessage.Bcc.First().Address.Should().Be(BccEmailAddress);
            backedMailMessage.Bcc.First().DisplayName.Should().Be(BccEmailDisplayName);
        }

        [TestMethod]
        public void Send_EmailViewModelPriorityNormal_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();
            var emailViewModel = FillEmailViewModel();

            var emailCategory = emailViewModel.Category;
            var mockEmailBuilder = new Mock<IEmailBuilder<DelinquentEmailParameterModel>>();
            var mockEmailBuilderObject = mockEmailBuilder.Object;

            _mockBuilder.Setup(b => b[emailCategory]).Returns(mockEmailBuilderObject);
            _mockEmailRepository.Setup(x => x.Create(It.Is<Email>(e => e == FillEmailAsDelinquentCategory()))).Returns(new OperationStatus { Status = true });

            //Act
            service.Send(emailViewModel);

            //Assert
        }

        [TestMethod]
        public void Send_EmailViewModelPriorityHigh_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();

            _mockEmailRepository.Setup(x => x.Create(It.Is<Email>(e => e == FillEmailAsDelinquentCategory()))).Returns(new OperationStatus { Status = true });

            var mockEmailBuilder = new Mock<IEmailBuilder<AutoChargeSuccessEmailParameterModel>>();
            _mockBuilder.Setup(b => b[EmailCategory.AutoChargeSuccess]).Returns(mockEmailBuilder.Object);

            //Act
            service.Send(FillEmailViewModelAsDelinquentCategory());

            //Assert
        }

        [TestMethod]
        public void GetPreview_GetId_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();
            var emailAsDelinquentCategory = FillEmailAsDelinquentCategory();

            _mockEmailRepository.Setup(x => x.Get(EmailId)).Returns(emailAsDelinquentCategory);

            _mockBuilder.Setup(b => b[EmailCategory.Delinquent].GenerateBody(emailAsDelinquentCategory, It.Is<EmailBodyGenerationMode>(x => x == EmailBodyGenerationMode.Preview))).Returns(string.Empty);

            //Act
            var result = service.GetPreview(emailAsDelinquentCategory.Id);

            //Assert
            result.Should().NotBeNull();
        }

        [TestMethod]
        public void GetPreview_GetViewModel_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();
            var emailViewModel = FillEmailViewModel();

            _mockBuilder.Setup(b => b[EmailCategory.Delinquent].GeneratePreviewBody(emailViewModel)).Returns(string.Empty);

            //Act
            var result = service.GetPreview(emailViewModel);

            //Assert
            result.Should().NotBeNull();
        }

        [TestMethod]
        public void GetViewModel_GenerateViewModel_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();
            var emailViewModel = FillEmailViewModel();

            var parameter = FillDelinquentEmailParameterModel();
            const EmailCategory emailCategory = EmailCategory.Delinquent;
            var mockEmailBuilder = new Mock<IEmailBuilder<DelinquentEmailParameterModel>>();
            _mockBuilder.Setup(b => b[emailCategory]).Returns(mockEmailBuilder.Object);
            mockEmailBuilder.Setup(e => e.GetEmailViewModel(parameter)).Returns(emailViewModel);

            //Act
            var result = service.GetViewModel(emailCategory,parameter);

            //Assert
            result.Should().NotBeNull();
        }

        #endregion

        #region Private methods

        private EmailBusiness CreateService(bool isProductionMode = false)
        {
            _isProductionMode = isProductionMode;
            _utcNow = DateTime.UtcNow;
            _mockEmailRepository = new Mock<IEmailRepository>();
            _mockBuilder = new Mock<IIndex<EmailCategory, IEmailBuilder>>();
            _mockJbDateTime = new Mock<IJbDateTime>();
            _mockJbEmailService = new Mock<IJbEmailService>();
            _mockJbLocker = new Mock<IJbLocker>();

            _mockJbDateTime.Setup(s => s.UtcNow).Returns(_utcNow);

            return new EmailBusiness(_mockEmailRepository.Object, _mockBuilder.Object, DeploymentEnvironment.Local, _mockJbDateTime.Object, _mockJbEmailService.Object, _mockJbLocker.Object);
        }

        private EmailBusiness CreateSingletonService(bool isProductionMode = false)
        {
            if (_service == null)
                _service = CreateService(isProductionMode);

            return _service;
        }

        private void RunSendAllQueued()
        {
            var service = CreateSingletonService();

            service.SendAllQueued();
        }


        private List<Email> CreateEmails()
        {
            var afterNow = _utcNow.AddDays(1);
            var beforeNow = _utcNow.AddDays(-1);

            return new List<Email>
            {
                new Email { FromEmail = NoReplyEmailAddress, FromDisplayName = NoReplyEmailAddress, ReplyTo = NoReplyEmailAddress , Status = EmailSendStatus.Accepted, Category = EmailCategory.AutoChargeSuccess, Recipients = CreateRecipients() , ScheduleDate = _utcNow  },
                new Email { FromEmail = NoReplyEmailAddress, FromDisplayName = NoReplyEmailAddress, ReplyTo = NoReplyEmailAddress, Status = EmailSendStatus.Processed, Category = EmailCategory.AutoChargeSuccess, Recipients = CreateRecipients()  , ScheduleDate = _utcNow  },
                new Email { FromEmail = NoReplyEmailAddress, FromDisplayName = NoReplyEmailAddress, ReplyTo = NoReplyEmailAddress, Status = EmailSendStatus.Failed, Category = EmailCategory.AutoChargeSuccess, Recipients = CreateRecipients()  , ScheduleDate = afterNow  },
                new Email { FromEmail = NoReplyEmailAddress, FromDisplayName = NoReplyEmailAddress, ReplyTo = NoReplyEmailAddress, Status = EmailSendStatus.Canceled, Category = EmailCategory.AutoChargeSuccess, Recipients = CreateRecipients(), ScheduleDate = afterNow  },
                new Email { FromEmail = NoReplyEmailAddress, FromDisplayName = NoReplyEmailAddress, ReplyTo = NoReplyEmailAddress, Priority = EmailSendPriority.Normal, Category = EmailCategory.AutoChargeSuccess, Recipients = CreateRecipients()  , ScheduleDate = beforeNow },
                new Email { FromEmail = NoReplyEmailAddress, FromDisplayName = NoReplyEmailAddress, ReplyTo = NoReplyEmailAddress, Priority = EmailSendPriority.High, Category = EmailCategory.AutoChargeSuccess, Recipients = CreateRecipients()  , ScheduleDate = beforeNow},
                new Email { FromEmail = NoReplyEmailAddress, FromDisplayName = NoReplyEmailAddress, ReplyTo = NoReplyEmailAddress, Priority = EmailSendPriority.Low, Category = EmailCategory.AutoChargeSuccess, Recipients = CreateRecipients()  , ScheduleDate = beforeNow},
            };
        }

        private List<EmailRecipient> CreateRecipients()
        {
            return new List<EmailRecipient>
            {
                new EmailRecipient{ Type = EmailRecipientType.To, EmailAddress = ToEmailAddress  }
            };
        }

        private MailMessage CreateMailMessage()
        {
            return new MailMessage(FromEmailAddress, ToEmailAddress, EmailSubject, EmailBody);
        }

        private EmailCompletedEventHandler CreateCompletedEventHandler()
        {
            return new EmailCompletedEventHandler(EmailCompleted);
        }

        private void EmailCompleted(object sender, EmailCompletedEventArgs args)
        {

        }

        private AutoChargeSuccessEmailParameterModel FillAutoChargeSuccessEmailParameterModel()
        {
            return new AutoChargeSuccessEmailParameterModel()
            {
                InstallmentId = 1
            };
        }

        private EmailViewModel FillEmailViewModelAsDelinquentCategory()
        {
            return new EmailViewModel
            {
                TemplateId = null,
                Category = EmailCategory.Delinquent,
                Parameters = "{}",
                Recipients = new List<EmailRecipient> { new EmailRecipient { EmailAddress = "hamid@jumbula.com" } },
                Body = "this is for unit test",
                Subject = "test",
                From = "hamid@jumbula.com",
                IsConfirmation = true
            };
        }

        private static Email FillEmailAsDelinquentCategory()
        {
            return new Email
            {
                Id = EmailId,
                TemplateId = null,
                Category = EmailCategory.Delinquent,
                Parameters = "",
                Recipients = new List<EmailRecipient> { new EmailRecipient { EmailAddress = "hamid@jumbula.com" } },
                Subject = "test",
                FromEmail = "hamid@jumbula.com",
            };
        }

        private static EmailViewModel FillEmailViewModel()
        {
            return new EmailViewModel
            {
                TemplateId = null,
                Category = EmailCategory.Delinquent,
                Parameters = JsonHelper.JsonSerializer(FillDelinquentEmailParameterModel()),
                Recipients = new List<EmailRecipient> { new EmailRecipient { EmailAddress = "hamid@jumbula.com" } },
                Subject = "test",
                From = "hamid@jumbula.com",
            };
        }

        private static DelinquentEmailParameterModel FillDelinquentEmailParameterModel()
        {
           return new DelinquentEmailParameterModel { ClubId = ClubId, Body = "This is body of email!", RelatedEntity = new List<DelinquentEmailParameterRelatedEntityItemModel> { new DelinquentEmailParameterRelatedEntityItemModel { RelatedEntityName = nameof(OrderItem), RelatedEntityId = 120 } } };
        }
        #endregion
    }
}
