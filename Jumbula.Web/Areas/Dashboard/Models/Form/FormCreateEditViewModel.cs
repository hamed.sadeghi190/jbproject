﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FormCreateEditViewModel
    {
        public int EntityId { get; set; }

        public JbForm JbForm { get; set; }

        public DefaultFormType? DefaultFormType { get; set; }

        public string FormTemplate { get; set; }

        public List<SelectKeyValue<string>> FormTemplates { get; set; }
    }
}