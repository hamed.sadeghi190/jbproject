﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolAddressListReportViewModel
    {
        public string SchoolName { get; set; }

        public string CoordinatorName { get; set; }

        public string StreetAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public DateTime? AgreementExpirationDate { get; set; }
    }
}