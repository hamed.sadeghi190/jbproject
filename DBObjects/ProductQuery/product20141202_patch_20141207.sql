--@mn--2014/12/07--

--Kashian--
update  chargediscounts set detail='<SiblingDiscountMetaData>
  <PerEvent>false</PerEvent>
</SiblingDiscountMetaData>' where Category=505 and detail is null
GO
print 'success execute KASHIAN query';

IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.ChargeDiscounts') AND name='Migrate')
BEGIN
	ALTER TABLE [dbo].[ChargeDiscounts] ADD [Migrate] [bit] NULL
END
GO

print 'success execute MILAD query';