﻿using Jumbula.Common.Enums;
using System;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FamilyOrderItemInstalmentsViewModel
    {
        public DateTime DueDate { get; set; }
        
        public string StrDueDate { get
            {

                return DueDate.ToString("MMM dd, yyyy");
            } }
        public DateTime? LastDueDate { get; set; }

        public string StrPaidDate
        {
            get
            {
                return LastDueDate.HasValue ? LastDueDate.Value.ToString("MMM dd, yyyy") : "-";
            }
        }
        public int UserId { get; set; }
        public decimal? PaidAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal? Balance
        {
            get
            {

                return Amount - PaidAmount;
            }
        }

        public bool IsInvoiced { get; set; }
        public long id { get; set; }
        public OrderStatusCategories Status { get; set; }
        public decimal InvoiceAmount { get; set; }
        public bool Checked { get; set; }
    }



}