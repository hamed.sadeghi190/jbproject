﻿using HtmlAgilityPack;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using Jumbula.Web.Areas.Dashboard.Controllers;
using Jumbula.Web.Infrastructure.DataMigration.DataManipulationItems;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Register;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace Jumbula.Web.Infrastructure.DataMigration.DataManipulations
{
    public class AutoChargeStressTest : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {

        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();


        public override string Title
        {
            get { return "Create auto charge stress test data "; }
        }
        public override int? NumbersInCycle
        {
            get { return 1; }
        }
        public override int GetNumberOfElements()
        {
            return 200;
        }

        public override DataManipulationGeneralResult Operate()
        {

            Initialize(GetNumberOfElements());
            AddInstallment();
            FinalizeM();
            return Reslult;

        }

        private void AddInstallment()
        {


            //// online 
            Dictionary<int, List<long>> clubProgramPair = new Dictionary<int, List<long>>
            {
                {11963, new List<long>() {75078}}
            };

            Dictionary<int, int> clubUsers = new Dictionary<int, int>
            {
                {11963, 26712}
            };

            var jbFromId = 45551;

            ////local
            //Dictionary<int, List<long>> clubProgramPair = new Dictionary<int, List<long>>
            //{
            //    {11963, new List<long>() {375855}}
            //};

            //Dictionary<int, int> clubUsers = new Dictionary<int, int>
            //{
            //    {11963, 77706}
            //};

            //var jbFromId = 205927;

            foreach (var club in clubProgramPair)
            {
                foreach (var programId in club.Value)
                {
                    var userId = clubUsers.Where(d => d.Key == club.Key).First().Value;
                    var player = DataContext.Set<PlayerProfile>().First(p => p.UserId == userId);

                    var program = DataContext.Set<Program>().Single(p => p.Id == programId);
                    var programSchedule = DataContext.Set<ProgramSchedule>().First(p => p.ProgramId == programId && !p.IsDeleted);

                    for (int i = 0; i < 1; i++)
                    {
                        var order = new Order()
                        {
                            ClubId = club.Key,
                            IsAutoCharge = true,
                            IsLive = true,
                            OrderAmount = 1000,
                            PaymentPlanType = PaymentPlanType.Installment,
                            OrderStatus = OrderStatusCategories.completed,
                            UserId = userId,
                            ConfirmationId = GenerateConfirmationId(),
                            CompleteDate = DateTime.UtcNow,
                            OrderItems = new List<OrderItem>()
                        };

                        var orderItem = new OrderItem()
                        {
                            PaymentPlanType = PaymentPlanType.Installment,
                            ItemStatus = OrderItemStatusCategories.completed,
                            PaymentPlanStatus = PaymentPlanStatus.Started,
                            EntryFee = 100,
                            EntryFeeName = "EntryFee",
                            TotalAmount = 200,
                            SeasonId = program.SeasonId,
                            ISTS = 100,
                            JbFormId = jbFromId,
                            PlayerId = player.Id,
                            Name = program.Name,
                            FirstName = player.Contact.FirstName,
                            LastName = player.Contact.LastName,
                            DateCreated = DateTime.UtcNow,
                            DesiredStartDate = DateTime.UtcNow,
                            PaymentPlanStatusUpdateDate = DateTime.UtcNow,
                            ProgramScheduleId = programSchedule.Id,
                            Installments = new List<OrderInstallment>()

                        };

                        order.OrderItems.Add(orderItem);

                        for (int j = 0; j < 1; j++)
                        {
                            var installment = new OrderInstallment()
                            {
                                AutoChargeAttemps = 0,
                                Amount = 220,
                                PaidAmount = 0,
                                InstallmentDate = DateTime.UtcNow.Date,
                                Status = OrderStatusCategories.initiated,
                                IsDeleted = false,
                                EnableReminder = false,
                            };

                            orderItem.Installments.Add(installment);
                        }

                        DataContext.Set<Order>().Add(order);
                    }
                }
            }
        }

        public string GenerateConfirmationId()
        {
            string s;
            string ss;
            string sss;

            lock (syncLock)
            {
                byte[] buffer = new byte[5];

                random.NextBytes(buffer);

                s = BitConverter.ToString(buffer, 0, 1) + BitConverter.ToString(buffer, 1, 1);
                ss = BitConverter.ToString(buffer, 2, 1);
                sss = BitConverter.ToString(buffer, 3, 1) + BitConverter.ToString(buffer, 4, 1);
            }

            var result = $"{s}-{ss}-{sss}";

            return result;
        }
    }

    public class SetDefualtAutoChargePolicyForClubs : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get { return "Set Defualt Auto Charge Policy For Clubs"; }
        }
        public override int? NumbersInCycle
        {
            get { return 250; }
        }
        public override int GetNumberOfElements()
        {
            return DataContext.Set<Club>().Count();
        }
        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;
            List<Club> clubs;
            var numberOfClubForms = GetNumberOfElements();
            if (NumbersInCycle.HasValue)
            {
                clubs = DataContext.Set<Club>().OrderBy(c => c.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                clubs = DataContext.Set<Club>().ToList();
            }
            Initialize(numberOfClubForms);
            SetClubsDefualtPolicy(clubs);
            FinalizeM();
            return Reslult;
        }
        private void SetClubsDefualtPolicy(List<Club> clubs)
        {
            foreach (var club in clubs)
            {
                SetClubDefualtPolicy(club);
            }
        }
        private void SetClubDefualtPolicy(Club club)
        {
            club.Setting.AutoChargePolicy = new AutoChargePolicy(true);
            club.SettingSerialized = JsonConvert.SerializeObject(club.Setting);
        }
    }

    public class SetDefualtDelinquentPolicyForClubs : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get { return "Set Defualt Delinquent Policy For Clubs"; }
        }
        public override int? NumbersInCycle
        {
            get { return 250; }
        }
        public override int GetNumberOfElements()
        {
            return DataContext.Set<Club>().Count();
        }
        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;
            List<Club> clubs;
            var numberOfClubForms = GetNumberOfElements();
            if (NumbersInCycle.HasValue)
            {
                clubs = DataContext.Set<Club>().OrderBy(c => c.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                clubs = DataContext.Set<Club>().ToList();
            }
            Initialize(numberOfClubForms);
            SetClubsDefualtPolicy(clubs);
            FinalizeM();
            return Reslult;
        }
        private void SetClubsDefualtPolicy(List<Club> clubs)
        {
            foreach (var club in clubs)
            {
                SetClubDefualtPolicy(club);
            }
        }
        private void SetClubDefualtPolicy(Club club)
        {
            club.Setting.DelinquentPolicy = new DelinquentPolicy();
            club.SettingSerialized = JsonConvert.SerializeObject(club.Setting);
        }
    }

    public class ReplaceRelationshipInClubFormsWithNewEnum : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Replace Relationship In Club Forms With New Enum";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 1000;
            }
        }

        public override int GetNumberOfElements()
        {
            return DataContext.Set<Club>().SelectMany(p => p.ClubFormTemplates).Count();
        }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            List<JbForm> clubForms;

            var numberOfClubForms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                clubForms = DataContext.Set<Club>().SelectMany(p => p.ClubFormTemplates).OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).Select(f => f.JbForm).ToList();
            }
            else
            {
                clubForms = DataContext.Set<Club>().SelectMany(p => p.ClubFormTemplates).OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).Select(f => f.JbForm).ToList();
            }

            Initialize(numberOfClubForms);

            replaceProgramFormsWithNewEnum(clubForms);

            FinalizeM();

            return this.Reslult;
        }

        private void replaceProgramFormsWithNewEnum(List<JbForm> allClubForms)
        {
            var elementName = ElementsName.Relationship.ToString();
            string sectionName = null;

            Log(string.Format("Number of club forms ", allClubForms.Count().ToString()));

            var allDropDownItems = new List<string>();

            foreach (var form in allClubForms)
            {
                var element = GetFormElement(form, elementName, sectionName);

                if (element != null && element is JbDropDown)
                {
                    var dropDownElement = element as JbDropDown;

                    dropDownElement.Items = null;
                    dropDownElement.Items = Enum.GetValues(typeof(Relationship))
                        .Cast<Relationship>()
                        .Select(d => new JbElementItem { Text = EnumHelper.GetEnumDescription(d), Value = EnumHelper.GetEnumDescription(d) })
                        .ToList();
                }


                //Emergency Contact relationship
                var emergencyRealtionship = GetFormElement(form, ElementsName.Relationship.ToString(), SectionsName.EmergencyContactSection.ToString());

                if (emergencyRealtionship != null && emergencyRealtionship is JbTextBox)
                {
                    var emergencyRealtionshipText = emergencyRealtionship as JbTextBox;
                    var dropDownElement = new JbDropDown();

                    dropDownElement.IsRequired = emergencyRealtionshipText.IsRequired;
                    dropDownElement.AccessRole = emergencyRealtionshipText.AccessRole;
                    dropDownElement.CurrentMode = emergencyRealtionshipText.CurrentMode;
                    dropDownElement.ElementId = emergencyRealtionshipText.ElementId;
                    dropDownElement.HelpText = emergencyRealtionshipText.HelpText;
                    dropDownElement.Name = emergencyRealtionshipText.Name;
                    dropDownElement.Size = emergencyRealtionshipText.Size;
                    dropDownElement.Title = emergencyRealtionshipText.Title;
                    dropDownElement.TypeTitle = emergencyRealtionshipText.TypeTitle;
                    dropDownElement.Validations = emergencyRealtionshipText.Validations;
                    dropDownElement.VisibleMode = emergencyRealtionshipText.VisibleMode;
                    dropDownElement.AccessRole = emergencyRealtionshipText.AccessRole;
                    dropDownElement.CurrentMode = emergencyRealtionshipText.CurrentMode;
                    dropDownElement.Width = emergencyRealtionshipText.Width;

                    dropDownElement.Items = Enum.GetValues(typeof(Relationship))
                        .Cast<Relationship>()
                        .Select(d => new JbElementItem { Text = EnumHelper.GetEnumDescription(d), Value = EnumHelper.GetEnumDescription(d) })
                        .ToList();

                    ReplaceEmergencyRelationshipWithDropdown(form, dropDownElement);
                }

                form.JsonElements = JsonConvert.SerializeObject(form.Elements);
            }

        }

        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        private void ReplaceEmergencyRelationshipWithDropdown(JbForm jbForm, JbDropDown dropdown)
        {
            if (jbForm.Elements.Any(s => s.Name.Equals(SectionsName.EmergencyContactSection.ToString(), StringComparison.OrdinalIgnoreCase)))
            {
                var emergencySection = (JbSection)jbForm.Elements.First(s => s.Name.Equals(SectionsName.EmergencyContactSection.ToString(), StringComparison.OrdinalIgnoreCase));

                emergencySection.Elements.RemoveAll(r => r.Name == ElementsName.Relationship.ToString());

                emergencySection.Elements.Add(dropdown);
            }
        }
    }

    public class UpdateClubFormsForNewConditionalElements : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Update club forms for new conditional elements";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 1000;
            }
        }

        public override int GetNumberOfElements()
        {
            return DataContext.Set<Club>().SelectMany(p => p.ClubFormTemplates).Count(c => c.FormType == FormType.Registration && c.Title != "Default" && c.Title != "Default school");
        }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            List<JbForm> clubForms;

            var numberOfClubForms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                clubForms = DataContext.Set<Club>().SelectMany(p => p.ClubFormTemplates).Where(c => c.FormType == FormType.Registration && c.Title != "Default" && c.Title != "Default school").OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).Select(f => f.JbForm).ToList();
            }
            else
            {
                clubForms = DataContext.Set<Club>().SelectMany(p => p.ClubFormTemplates).Where(c => c.FormType == FormType.Registration && c.Title != "Default" && c.Title != "Default school").OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).Select(f => f.JbForm).ToList();
            }

            Initialize(numberOfClubForms);

            UpdateSaveAsForms(clubForms);

            FinalizeM();

            return this.Reslult;
        }

        private void UpdateSaveAsForms(List<JbForm> allClubForms)
        {
            var teacherElementName = ElementsName.TeacherName.ToString();
            var teacherNotListedName = ElementsName.TeacherNotListed.ToString();

            var dismissalName = ElementsName.StandardDismissal.ToString();

            var medicalConditionDescriptionName = ElementsName.MedicalConditions.ToString();
            var allergiesInfoName = ElementsName.AllergiesInfo.ToString();
            var selfAdministerMedicationDescriptionName = ElementsName.SelfAdministerMedicationDescription.ToString();
            var nutAllergyName = ElementsName.NutAllergy.ToString();

            Log(string.Format("Number of club forms ", allClubForms.Count().ToString()));

            var allDropDownItems = new List<string>();

            foreach (var form in allClubForms)
            {
                try
                {
                    #region Teacher

                    var teacherelement = GetFormElement(form, teacherElementName, SectionsName.SchoolSection.ToString());

                    var dropdownHasTeacherNotListedElement = false;

                    if (teacherelement is JbDropDown)
                    {
                        var dropDownElement = teacherelement as JbDropDown;

                        if (dropDownElement.Items.Any(d => d.Value == ElementsName.TeacherNotListed.ToDescription()))
                        {
                            dropdownHasTeacherNotListedElement = true;
                            dropDownElement.Items.FirstOrDefault(d => d.Value == ElementsName.TeacherNotListed.ToDescription()).ReadOnly = true;
                        }
                    }
                    else
                    {
                        dropdownHasTeacherNotListedElement = true;
                    }

                    var teacherNotlistedElement = GetFormElement(form, teacherNotListedName, SectionsName.SchoolSection.ToString());

                    if (teacherNotlistedElement is JbTextBox && dropdownHasTeacherNotListedElement)
                    {
                        teacherNotlistedElement.IsConditional = true;
                        teacherNotlistedElement.RelatedElementId = ElementsName.TeacherName.ToString();
                        teacherNotlistedElement.RelatedElementValue = ElementsName.TeacherNotListed.ToDescription();
                    }

                    #endregion

                    #region Dismissal

                    var schoolSection = form.Elements.FirstOrDefault(e => e.Name == SectionsName.SchoolSection.ToString());

                    if (schoolSection != null)
                    {
                        var currentSection = schoolSection as JbSection;

                        var dismissalElement = GetFormElement(form, dismissalName, SectionsName.SchoolSection.ToString());

                        if (dismissalElement != null && dismissalElement is JbDropDown)
                        {
                            var dropDownElement = dismissalElement as JbDropDown;

                            if (dropDownElement.Items.Any(d => d.Value == "Bus rider"))
                            {
                                dropDownElement.Items.FirstOrDefault(d => d.Value == "Bus rider").ReadOnly = true;
                            }
                        }

                    }

                    #endregion

                    #region Medical

                    var healthSection = form.Elements.FirstOrDefault(e => e.Name == SectionsName.HealthSection.ToString());

                    if (healthSection != null)
                    {
                        var currentHealthSection = healthSection as JbSection;

                        //MedicalCondition
                        var medicalConditionDescriptionElement = GetFormElement(form, medicalConditionDescriptionName, SectionsName.HealthSection.ToString());

                        if (medicalConditionDescriptionElement is JbTextArea)
                        {
                            var medicalConditionDescriptionElementTextArea =
                                medicalConditionDescriptionElement as JbTextArea;

                            var relatedElement = GetFormElement(form, ElementsName.HasMedicalConditions.ToString(), SectionsName.HealthSection.ToString());
                            if (relatedElement == null)
                            {
                                var itemsMedicalConditions = Enum.GetValues(typeof(YesNoDropDownOptions)).Cast<YesNoDropDownOptions>().Select(v => new JbElementItem
                                {
                                    Text = ((YesNoDropDownOptions)v).ToDescription(),
                                    Value = ((int)v).ToString()
                                }).ToList();

                                var indexOfTextArea =
                                    currentHealthSection.Elements.IndexOf(medicalConditionDescriptionElementTextArea);

                                currentHealthSection.Elements.Insert(indexOfTextArea, new JbDropDown()
                                {
                                    Name = ElementsName.HasMedicalConditions.ToString(),
                                    Title = ElementsName.HasMedicalConditions.ToDescription(),
                                    Items = itemsMedicalConditions,
                                    IsRequired = medicalConditionDescriptionElementTextArea.IsRequired,
                                    VisibleMode = medicalConditionDescriptionElementTextArea.VisibleMode,
                                });

                                medicalConditionDescriptionElementTextArea.IsConditional = true;
                                medicalConditionDescriptionElementTextArea.RelatedElementId = ElementsName.HasMedicalConditions.ToString();
                                medicalConditionDescriptionElementTextArea.RelatedElementValue = 1.ToString();
                            }
                        }

                        //Allergy
                        var allergiesInfoElement = GetFormElement(form, allergiesInfoName, SectionsName.HealthSection.ToString());

                        if (allergiesInfoElement is JbTextArea)
                        {
                            var allergiesInfoElementTextArea =
                                allergiesInfoElement as JbTextArea;

                            var relatedElement = GetFormElement(form, ElementsName.HasAllergiesInfo.ToString(), SectionsName.HealthSection.ToString());
                            if (relatedElement == null)
                            {
                                var itemsAllergiesInfo = Enum.GetValues(typeof(YesNoDropDownOptions)).Cast<YesNoDropDownOptions>().Select(v => new JbElementItem
                                {
                                    Text = ((YesNoDropDownOptions)v).ToDescription(),
                                    Value = ((int)v).ToString()
                                }).ToList();

                                var indexOfTextArea =
                                    currentHealthSection.Elements.IndexOf(allergiesInfoElementTextArea);

                                currentHealthSection.Elements.Insert(indexOfTextArea, new JbDropDown()
                                {
                                    Name = ElementsName.HasAllergiesInfo.ToString(),
                                    Title = ElementsName.HasAllergiesInfo.ToDescription(),
                                    Items = itemsAllergiesInfo,
                                    IsRequired = allergiesInfoElementTextArea.IsRequired,
                                    VisibleMode = allergiesInfoElementTextArea.VisibleMode,
                                });

                                allergiesInfoElementTextArea.IsConditional = true;
                                allergiesInfoElementTextArea.RelatedElementId = ElementsName.HasAllergiesInfo.ToString();
                                allergiesInfoElementTextArea.RelatedElementValue = 1.ToString();
                            }
                        }

                        //SelfAdminister
                        var selfAdministerMedicationDescriptionElement = GetFormElement(form, selfAdministerMedicationDescriptionName, SectionsName.HealthSection.ToString());

                        if (selfAdministerMedicationDescriptionElement is JbTextArea)
                        {
                            selfAdministerMedicationDescriptionElement.IsConditional = true;
                            selfAdministerMedicationDescriptionElement.RelatedElementId = ElementsName.SelfAdministerMedication.ToString();
                            selfAdministerMedicationDescriptionElement.RelatedElementValue = 1.ToString();
                        }

                        //Nut Allergy
                        var nutAllergyElement = GetFormElement(form, nutAllergyName, SectionsName.HealthSection.ToString());

                        if (nutAllergyElement is JbDropDown)
                        {

                            var nutAllergyElementDropDown =
                                nutAllergyElement as JbDropDown;

                            var relatedElement = GetFormElement(form, ElementsName.NutAllergyDescription.ToString(), SectionsName.HealthSection.ToString());
                            if (relatedElement == null)
                            {
                                var indexOfJbDropDown =
                                       currentHealthSection.Elements.IndexOf(nutAllergyElementDropDown);

                                currentHealthSection.Elements.Insert(indexOfJbDropDown + 1, new JbTextArea()
                                {
                                    Name = ElementsName.NutAllergyDescription.ToString(),
                                    Title = ElementsName.NutAllergyDescription.ToDescription(),
                                    IsConditional = true,
                                    RelatedElementId = ElementsName.NutAllergy.ToString(),
                                    RelatedElementValue = 1.ToString(),
                                    CurrentMode = AccessMode.Hidden
                                });
                            }
                        }
                    }

                    #endregion

                    form.JsonElements = JsonConvert.SerializeObject(form.Elements);
                }
                catch
                {
                    Log(string.Format("Form {0} has error", form.Id));
                }
            }

        }


        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }
    }

    public class SetNewValuesForPlayer : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Set new information for palyers";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 1000;
            }
        }

        public override int GetNumberOfElements()
        {
            return DataContext.Set<PlayerInfo>().Count();
        }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            List<PlayerInfo> playersInformation;

            var numberOfClubForms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                playersInformation = DataContext.Set<PlayerInfo>().OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                playersInformation = DataContext.Set<PlayerInfo>().OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfClubForms);

            SetPlayerinformation(playersInformation);

            FinalizeM();

            return this.Reslult;
        }

        private void SetPlayerinformation(List<PlayerInfo> playersInformation)
        {
            var negativeAnswers = getNegativeAnswers();

            foreach (var playerInfo in playersInformation)
            {
                if (!string.IsNullOrWhiteSpace(playerInfo.Allergies))
                {
                    if (!negativeAnswers.Contains(playerInfo.Allergies.ToLower()))
                        playerInfo.HasAllergies = true;
                    else
                    {
                        playerInfo.HasAllergies = false;
                        playerInfo.Allergies = null;
                    }

                }
                else
                {
                    playerInfo.HasAllergies = null;
                    playerInfo.Allergies = null;
                }

                if (!string.IsNullOrWhiteSpace(playerInfo.SpecialNeeds))
                {
                    if (!negativeAnswers.Contains(playerInfo.SpecialNeeds.ToLower()))
                        playerInfo.HasSpecialNeeds = true;
                    else
                    {
                        playerInfo.HasSpecialNeeds = false;
                        playerInfo.SpecialNeeds = null;
                    }
                }
                else
                {
                    playerInfo.HasSpecialNeeds = null;
                    playerInfo.SpecialNeeds = null;
                }

            }
        }

        private List<string> getNegativeAnswers()
        {
            var result = new List<string>() { "none", "n/a", "-", "no", "nine", "none known", "non", "n.a.", "na", "(none)", "no problem", "nka", "n:a", "not known", "nothing", "--", "n", "n-a", "n/as", "none.", "n/aww", "n/x", "n/an", "/", "n/a???", "no known", "0", "nkf/da", "knd/fa", "n/", "n/.a", "n.a", "no allergies. . . . . .", "no allergies. . . . .", "no allergies. .. . . .", "no allergies. . . . . . .", "no allergies. . . . . .", "no allergies. . . . . . . .", "no allergies. . . . . . . .", "no allergies. . . .", "nonr", "nil", "*", "none!", "'none'", "no.", "nonoe", "done", "no known allergies", "./.", "?n/a", "n/as", "—-", "n/4", "nona", "n/", "now", "?n/a", "a/a", "n’a", "n?a", "na/", "n/a??", "njne", "n.a", "noe", "n/o", "n/al", "n/-", "none to my knowledge.", "nome", "n / a", "nil", "??", "n/-", "noene", "?", "noone", "nono", "nonw", "a/a", "no e", "`n/a", "n/1", "--none--", "no special needs", "no know allergies", "none at this time", "none st this time", "negative", "none that impede her running", "none available", "none that we know of", "none to note", "no known", "fine", "excellent", "well child", "n-", "no medical issues", "none n/a", "oh he goood", "no special needs", "no conditions", "none to my knowledge", "none`", "normal", "n/.a", "do not have any", "none that we are aware of.", "good", "good health", "well", "very well", "healthy", "no special things", "nothing special", "healthy / n/a", "no special needs", "normal", "`", "`none", "no medical conditions. foster child", "good health", "nonwe", "nonenone", "very goof", "good conditions. no special needs", "all good", "not applicable", "n'a", "n/a/", "no medical conditions", "nond", "all is good", "none listed", "not any", "not aware", "dont have any medical condition", "noen", "good/no", "healthy child", "no medical conditions", "nons", "non so far.", "non so far", "-----------", "none n", "(non)", "no known allergies or medical conditions", "none noted", "none known at this time", "nonn", "no special needs.", "????", "nonei", "none none", "no issues", "n a", "good.", "none", "", "n/a.", "great", "a/n", "nonena", "none", "known.", "no conditions, no allergies", "not listed", "nothing special . . .", "no mergency conditions. . . . .", "no medical. . . . .", "no emg conditions. . . . .", "no emg conditions. . . . . .", "no emg conditions. . . .", "no emg condiotions. . . . . .", "no emg conditios. . . . .", "no emg conditions. . . . . . . .", "no emg conditions. . . . . .", "no emg conditions. . . . . . .", "none yah", "mo", "nons", "n/a????", "n/a?????", "no one", "n/z", "no special needs or medications", "no problem.", "n/a at this time", "no known medical conditions. no known special needs.", "very good", "no medical conditions, nka", "none known,.", "nonie",
                "n/a (thankfully!)", "no allergies or medical issues", "none currently", "healthy kid!", "perfect", "no medical problems", "none thank god", "in health", "healthy.", "thanks.", "no known conditions", "all ok", "he is healthy", "none as of now", "very good health", "good / none", "no medical needs", "no ne", "no allergies known yet", "not registered", "nothi special", "very good.", "none!:)", "none identified", "nothing major", "very healthy", "none to our knowledge", "none that we know of", "none of which we are aware", "nione", "none known.", "no medical or special needs", "health is good", "not aware of any", "none aware of", "no allergies."
            };

            result = result.Select(f => f.ToLower()).ToList();
            return result;

        }
    }

    public class DataManipulationAddNewClubs : DataManipulationBaseModel<DataManipulationAddNewClubsItem>
    {
        public override string Title
        {
            get
            {
                return "Add new clubs";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 10;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            List<DataManipulationAddNewClubsItem> sourceItems;

            var allElementNumbers = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                sourceItems = SourceItems.OrderBy(s => s.Name).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                sourceItems = SourceItems;
            }

            Initialize(allElementNumbers);

            AddClubs(sourceItems);

            FinalizeM();

            return Reslult;
        }

        private void AddClubs(List<DataManipulationAddNewClubsItem> sourceClubItems)
        {
            string partnerDomain = string.Empty;

            try
            {
                partnerDomain = sourceClubItems.First().PartnerDomain;
            }
            catch
            {
                Log("Can't get partner domain");
                return;
            }

            Club partnerClub = null;
            if (HttpContext.Current.Request.IsLocal)
            {
                partnerClub = DataContext.Set<Club>().Single(c => c.Domain.Equals("em", StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                partnerClub = DataContext.Set<Club>().Single(c => c.Domain.Equals(partnerDomain, StringComparison.OrdinalIgnoreCase));
            }


            foreach (var item in sourceClubItems)
            {
                AddClub(item, partnerClub);
            }
        }

        private void AddClub(DataManipulationAddNewClubsItem sourceClubItem, Club partnerClub)
        {
            int index = 0;

            try
            {
                index = sourceClubItem.Index;
            }
            catch
            {
                Log($"Can't get index");
            }

            if (string.IsNullOrEmpty(sourceClubItem.Name) || string.IsNullOrEmpty(sourceClubItem.Domain))
            {
                Log($"{index}  name or domain are empty");
            }

            try
            {
                string phoneNumber;
                var result = new Club();

                try
                {
                    result.PartnerCommisionRate = 0;

                    try
                    {
                        result.Setting = new ClubSetting();
                        if (!string.IsNullOrEmpty(sourceClubItem.AgreementExpirationDate))
                        {
                            result.Setting.AgreementExpirationDate = DateTime.Parse(sourceClubItem.AgreementExpirationDate);
                        }
                        if (!string.IsNullOrEmpty(sourceClubItem.MemberSince))
                        {
                            result.Setting.MemberSince = DateTime.Parse(sourceClubItem.MemberSince);
                        }
                        result.Setting.TaxIDFEIN = sourceClubItem.TaxIDFEIN;
                        result.Setting.TaxIDSSN = sourceClubItem.TaxIDSSN;
                        result.Setting.Provider1099 = false;

                        result.SettingSerialized = JsonConvert.SerializeObject(result.Setting);

                    }
                    catch
                    {
                        Log($"{index}  club setting is not valid.");
                        return;
                    }

                    var setting = result.Setting;


                    if (!string.IsNullOrEmpty(sourceClubItem.FirstName1) && !string.IsNullOrEmpty(sourceClubItem.LastName1) && !string.IsNullOrEmpty(sourceClubItem.PhoneNumber1) && !string.IsNullOrEmpty(sourceClubItem.Email1))
                    {
                        //Add contact for club
                        var contactPerson1 = new ContactPerson();
                        if (IsNameValid(sourceClubItem.FirstName1))
                        {
                            contactPerson1.FirstName = sourceClubItem.FirstName1;
                        }
                        else
                        {
                            Log($"{index}  First name1 is not valid.");
                            return;
                        }

                        if (IsNameValid(sourceClubItem.LastName1))
                        {
                            contactPerson1.LastName = sourceClubItem.LastName1;
                        }
                        else
                        {
                            Log($"{index} Last name is not valid.");

                            return;
                        }

                        phoneNumber = FormatPhone(sourceClubItem.PhoneNumber1);
                        if (IsPhoneValid(phoneNumber))
                        {
                            contactPerson1.Phone = phoneNumber;
                        }
                        else
                        {
                            Log($"{index} Phone number1 is not valid.");
                            return;

                        }

                        contactPerson1.PhoneExtension = sourceClubItem.Extension1;
                        contactPerson1.Email = sourceClubItem.Email1;
                        contactPerson1.Title = GetStaffTitle(sourceClubItem.Title1);
                        contactPerson1.IsPrimary = true;
                        result.ContactPersons.Add(contactPerson1);

                        if (sourceClubItem.HasAccess1.ToLower() == "yes")
                        {
                            var userName = sourceClubItem.Email1.Replace(" ", string.Empty).Trim();
                            var user = DataContext.Set<JbUser>().SingleOrDefault(p => p.UserName == userName);

                            if (user == null)
                            {

                                UserManager.PasswordValidator = new PasswordValidator
                                {
                                    RequiredLength = 8,
                                    RequireNonLetterOrDigit = false,
                                    RequireDigit = false,
                                    RequireLowercase = false,
                                    RequireUppercase = false,
                                };

                                var password = userName.ToLower();

                                user = new JbUser() { UserName = userName, Email = userName };

                                var res = UserManager.Create(user, password);

                            }


                            if (!UserManager.IsEmailConfirmed(user.Id))
                            {
                                // confirm email
                                UserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));

                                string token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                                UserManager.ConfirmEmail(user.Id, token);
                            }
                            ClubStaff staff = new ClubStaff();
                            JbUserRole userrole = new JbUserRole();
                            ContactPerson contactpersonStaff1 = new ContactPerson();
                            contactpersonStaff1 = contactPerson1;
                            staff.Club = result;
                            staff.Contact = contactpersonStaff1;
                            staff.Status = StaffStatus.Active;
                            userrole.Role = GetStaffRole(sourceClubItem.AccessRole1);
                            userrole.UserId = user.Id;
                            staff.JbUserRole = userrole;
                            result.ClubStaffs = new List<ClubStaff>();
                            result.ClubStaffs.Add(staff);
                        }
                    }
                    else
                    {
                        Log($"{index} contact info1 is not valid.");
                        return;
                    }

                    // Contact2
                    if (!string.IsNullOrEmpty(sourceClubItem.FirstName2) && !string.IsNullOrEmpty(sourceClubItem.LastName2))
                    {

                        var contactPerson2 = new ContactPerson();
                        if (IsNameValid(sourceClubItem.FirstName2))
                        {
                            contactPerson2.FirstName = sourceClubItem.FirstName2;
                        }
                        else
                        {
                            Log($"{index} First name2 is not valid.");
                            return;

                        }

                        if (IsNameValid(sourceClubItem.LastName2))
                        {
                            contactPerson2.LastName = sourceClubItem.LastName2;
                        }
                        else
                        {
                            Log($"{index} Last name 2 is not valid.");
                            return;
                        }

                        phoneNumber = FormatPhone(sourceClubItem.PhoneNumber2);
                        if (!string.IsNullOrEmpty(sourceClubItem.PhoneNumber2))
                        {
                            if (IsPhoneValid(phoneNumber))
                            {
                                contactPerson2.Phone = phoneNumber;
                            }
                        }

                        contactPerson2.PhoneExtension = sourceClubItem.Extension2;
                        contactPerson2.Email = sourceClubItem.Email2;
                        contactPerson2.Title = GetStaffTitle(sourceClubItem.Title2);
                        contactPerson2.IsPrimary = false;
                        result.ContactPersons.Add(contactPerson2);

                        if (sourceClubItem.HasAccess2.ToLower() == "yes")
                        {
                            var userName = sourceClubItem.Email2.Replace(" ", string.Empty).Trim();
                            var user = DataContext.Set<JbUser>().SingleOrDefault(p => p.UserName == userName);

                            if (user == null)
                            {
                                UserManager.PasswordValidator = new PasswordValidator
                                {
                                    RequiredLength = 8,
                                    RequireNonLetterOrDigit = false,
                                    RequireDigit = false,
                                    RequireLowercase = false,
                                    RequireUppercase = false,
                                };

                                var password = userName.ToLower();

                                user = new JbUser() { UserName = userName, Email = userName };

                                var res = UserManager.Create(user, password);

                            }

                            if (!UserManager.IsEmailConfirmed(user.Id))
                            {
                                // confirm email
                                UserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));

                                string token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                                UserManager.ConfirmEmail(user.Id, token);
                            }
                            ClubStaff staff = new ClubStaff();
                            JbUserRole userrole = new JbUserRole();
                            ContactPerson contactpersonStaff2 = new ContactPerson();
                            contactpersonStaff2 = contactPerson2;
                            staff.Club = result;
                            staff.Contact = contactpersonStaff2;
                            staff.Status = StaffStatus.Active;
                            userrole.Role = GetStaffRole(sourceClubItem.AccessRole2);
                            userrole.UserId = user.Id;
                            staff.JbUserRole = userrole;
                            if (result.ClubStaffs == null)
                            {
                                result.ClubStaffs = new List<ClubStaff>();
                            }
                            result.ClubStaffs.Add(staff);
                        }
                    }

                    // Contact3
                    if (!string.IsNullOrEmpty(sourceClubItem.FirstName3) && !string.IsNullOrEmpty(sourceClubItem.LastName3))
                    {

                        var contactPerson3 = new ContactPerson();
                        if (IsNameValid(sourceClubItem.FirstName3))
                        {
                            contactPerson3.FirstName = sourceClubItem.FirstName3;
                        }
                        else
                        {
                            Log($"{index} First name3 is not valid.");
                            return;

                        }

                        if (IsNameValid(sourceClubItem.LastName3))
                        {
                            contactPerson3.LastName = sourceClubItem.LastName3;
                        }
                        else
                        {
                            Log($"{index} Last name3  is not valid.");
                            return;

                        }

                        phoneNumber = FormatPhone(sourceClubItem.PhoneNumber3);
                        if (!string.IsNullOrEmpty(sourceClubItem.PhoneNumber3))
                        {
                            if (IsPhoneValid(phoneNumber))
                            {
                                contactPerson3.Phone = phoneNumber;
                            }
                        }

                        contactPerson3.PhoneExtension = sourceClubItem.Extension3;
                        contactPerson3.Email = sourceClubItem.Email3;
                        contactPerson3.Title = GetStaffTitle(sourceClubItem.Title3);
                        contactPerson3.IsPrimary = false;
                        result.ContactPersons.Add(contactPerson3);

                        if (sourceClubItem.HasAccess3.ToLower() == "yes")
                        {
                            var userName = sourceClubItem.Email3.Replace(" ", string.Empty).Trim();
                            var user = DataContext.Set<JbUser>().SingleOrDefault(p => p.UserName == userName);

                            if (user == null)
                            {
                                UserManager.PasswordValidator = new PasswordValidator
                                {
                                    RequiredLength = 8,
                                    RequireNonLetterOrDigit = false,
                                    RequireDigit = false,
                                    RequireLowercase = false,
                                    RequireUppercase = false,
                                };

                                var password = userName.ToLower();

                                user = new JbUser() { UserName = userName, Email = userName };

                                var res = UserManager.Create(user, password);

                            }

                            if (!UserManager.IsEmailConfirmed(user.Id))
                            {
                                // confirm email
                                UserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));

                                string token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                                UserManager.ConfirmEmail(user.Id, token);
                            }
                            ClubStaff staff = new ClubStaff();
                            JbUserRole userrole = new JbUserRole();
                            ContactPerson contactpersonStaff3 = new ContactPerson();
                            contactpersonStaff3 = contactPerson3;
                            staff.Club = result;
                            staff.Contact = contactpersonStaff3;
                            staff.Status = StaffStatus.Active;
                            userrole.Role = GetStaffRole(sourceClubItem.AccessRole3);
                            userrole.UserId = user.Id;
                            staff.JbUserRole = userrole;
                            if (result.ClubStaffs == null)
                            {
                                result.ClubStaffs = new List<ClubStaff>();
                            }
                            result.ClubStaffs.Add(staff);
                        }
                    }

                    var date = DateTime.UtcNow;
                    result.MetaData = new MetaData()
                    {
                        DateCreated = date,
                        DateUpdated = date
                    };
                    try
                    {

                        PostalAddress postalAddress = GetClubPostalAddress(sourceClubItem.Address);

                        result.ClubLocations = new List<ClubLocation>()
                        {
                            new ClubLocation()
                            {
                                Name = string.Empty,
                                PostalAddress = postalAddress
                            }
                        };

                        result.Address = postalAddress;
                    }
                    catch
                    {
                        Log($"{index} Address is not valid.");
                        return;

                    }

                    if (!string.IsNullOrEmpty(sourceClubItem.Website))
                    {
                        if (IsWebsiteValid(sourceClubItem.Website))
                        {
                            result.Site = sourceClubItem.Website;
                        }
                        else
                        {
                            Log($"{index} website is not valid.");
                            return;

                        }
                    }
                    result.Currency = CurrencyCodes.USD;
                    result.TimeZone = GetTimezone(sourceClubItem.TimeZone);
                    result.PartnerId = partnerClub.Id;
                    result.Client = new Client();
                    result.Client.PaymentMethods = new ClientPaymentMethod();
                    var priceplan = GetFreeTrialPlan();
                    result.Client.PricePlan = priceplan;
                    result.Client.PricePlanId = priceplan.Id;
                    result.Name = sourceClubItem.Name.Replace(Common.Constants.Constants.S_DoubleQuote, string.Empty)
                    .Replace(Jumbula.Common.Constants.Constants.S_BackSlash, string.Empty);

                    if (IsClubDomainValid(sourceClubItem.Domain))
                    {
                        result.Domain = sourceClubItem.Domain.ToLower();
                    }
                    else
                    {
                        Log($"{index} Domain is not valid.");
                        return;
                    }

                    if (GetTypeId(sourceClubItem.Type) > 0)
                    {
                        result.TypeId = GetTypeId(sourceClubItem.Type);
                    }
                    else
                    {
                        Log($"{index} Type is not valid.");
                        return;

                    }

                    result.Description = sourceClubItem.Description;

                    try
                    {
                        var page = GetDefaultPage(result);
                        DataContext.Set<JbPage>().Add(page);

                    }
                    catch
                    {
                        Log($"{index} error in creating page.");
                        return;

                    }

                    DataContext.Set<Club>().Add(result);
                }
                catch (Exception ex)
                {
                    Log($"{index} {ex.Message}");
                    return;
                }

                StaticValueClass.Add(sourceClubItem.Domain.ToLower());
            }
            catch
            {
                Log($"Club with index {index} has error");
            }
        }


        private int GetTypeId(string type)
        {
            switch (type)
            {
                case "Provider --> Individual":
                    {
                        return 4;
                    }
                case "Provider --> Corp":
                    {
                        return 5;
                    }
                case "School --> Public":
                    {
                        return 6;
                    }
                case "School --> Private":
                    {
                        return 7;
                    }
                case "School --> Title 1":
                    {
                        return 8;
                    }
                case "School --> Charter":
                    {
                        return 9;
                    }
                default:
                    {
                        return 0;
                    }
            }
        }
        private StaffTitle? GetStaffTitle(string title)
        {

            switch (title)
            {
                case "Principal":
                    {
                        return StaffTitle.SchoolPrincipal;
                    }
                case "Regional director":
                    {
                        return StaffTitle.SchoolRegionalDirector;
                    }
                case "Vice principal":
                    {
                        return StaffTitle.SchoolVicePrincipal;
                    }
                case "Assistant director":
                    {
                        return StaffTitle.SchoolAssistantDirector;
                    }
                case "PTA president":
                    {
                        return StaffTitle.SchoolPTAPresident;
                    }
                case "PTA vice president":
                    {
                        return StaffTitle.SchoolPTAVicePresident;
                    }
                case "PTA enrichment coordinator":
                    {
                        return StaffTitle.SchoolPTAEnrichmentCordinator;
                    }
                case "Onsite manager":
                    {
                        return StaffTitle.SchoolOnsiteManager;
                    }
                case "Onsite coordinator":
                    {
                        return StaffTitle.SchoolOnsiteCoordinator;
                    }
                case "Owner":
                    {
                        return StaffTitle.ProviderOwner;
                    }
                case "Director":
                    {
                        return StaffTitle.ProviderDirector;
                    }
                case "Manager":
                    {
                        return StaffTitle.ProviderManager;
                    }
                case "Administrative assistant":
                    {
                        return StaffTitle.ProviderAdministrativeAssistant;
                    }

                case "Instructor":
                    {
                        return StaffTitle.ProviderInstructor;
                    }

                case "Other":
                    {
                        return StaffTitle.Other;
                    }

                case "":
                    {
                        return null;
                    }
            }

            throw new Exception("title is invalid");
        }

        private JbRole GetStaffRole(string role)
        {
            return DataContext.Set<JbRole>().Single(r => r.Name.ToLower() == role.ToLower());
        }
        private string FormatPhone(string phoneNumber)
        {
            return phoneNumber.Replace("-", "").Replace("(", "").Replace(")", "").Replace(".", "").Replace(" ", "").Trim();
        }
        private Common.Enums.TimeZone GetTimezone(string timezone)
        {

            switch (timezone)
            {
                case "Eastern Standard Time":
                    {
                        return Common.Enums.TimeZone.EST;
                    }
                case "Central Standard Time":
                    {
                        return Common.Enums.TimeZone.CST;
                    }
                case "Pacific Standard Time":
                    {
                        return Common.Enums.TimeZone.PST;
                    }

            }

            throw new Exception("time zone i invalid");
        }

        private PricePlan GetFreeTrialPlan()
        {
            var plan = GetbyType(PricePlanType.FreeTrial);
            if (plan == null)
            {
                plan = new PricePlan()
                {
                    Deposit = 0,
                    Description = "Free trial",
                    MonthlyCharges = 0,
                    TransactionFee = 0,
                    Type = PricePlanType.FreeTrial

                };

            }
            return GetbyType(PricePlanType.FreeTrial);
        }

        private PricePlan GetbyType(PricePlanType planType)
        {
            if (DataContext.Set<PricePlan>().Any(c => !c.IsDeleted && c.Type == planType))
            {
                return DataContext.Set<PricePlan>().FirstOrDefault(c => !c.IsDeleted && c.Type == planType);
            }
            return null;
        }

        private JbPage GetDefaultPage(Club club)
        {
            var page = new JbPage();

            page.Title = club.Name + " Jumbula Home";
            page.Header.Title = club.Name;
            page.Header.ShowLogo = true;
            page.Club = club;


            page.CreateDate = DateTime.UtcNow;
            page.Name = "Jumbula_Home_" + club.Id;

            page.Footer.Address = club.ClubLocations.Any() ? club.ClubLocations.First().PostalAddress.Address : string.Empty;
            page.Footer.Email = club.ContactPersons.Any() ? club.ContactPersons.First().Email : string.Empty;
            page.Footer.Phone = club.ContactPersons.Any() ? club.ContactPersons.First().Phone : string.Empty;
            page.Footer.Website = club.Site;
            page.Footer.Visible = false;

            page.Setting = new JbPSetting()
            {
                RegLiksAvailableColor = "#005f2e",
                RegLiksUnavailableColor = "#006933"
            };
            if (club.ClubLocations.Any())
            {
                page.Address = new PostalAddress
                {
                    Lat = club.ClubLocations.First().PostalAddress.Lat,
                    Lng = club.ClubLocations.First().PostalAddress.Lng
                };
            }
            else
            {
                page.Address = new PostalAddress();
            }

            page.Elements.Add(
                new JbPTab()
                {
                    Title = "Home",
                    TabLink = "Home",
                    TypeTitle = "Tab",

                    Cover = new JbPCover()
                    {

                        Title = club.Name,
                        Visible = true,
                        BackgroundMode = JbPCoverBGMode.Gallery,
                        Background = "/Images/Covers/gray-banner.png",
                        TypeTitle = "Cover"
                    },

                    Elements = new List<JbPBaseElement>()
                    {


                    }


                });

            page.Header.LogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);

            if (string.IsNullOrEmpty(page.Header.LogoUrl))
            {
                page.Header.LogoUrl = "~/Images/club-nosport.png";
            }

            page.SaveType = SaveType.Publish;
            page.CreateDate = DateTime.UtcNow;

            page.JsonBody = JsonHelper.JsonSerializer(page, new JsonSerializerSettings() { Formatting = Formatting.Indented });

            return page;

        }
        private PostalAddress GetClubPostalAddress(string address)
        {

            var postalAddress = new PostalAddress();

            GoogleMap.GetGeo(address, ref postalAddress);

            return postalAddress;
        }
        private bool IsClubDomainValid(string clubDomain)
        {
            if (string.IsNullOrWhiteSpace(clubDomain))
            {
                return false;
            }

            var isExist = DataContext.Set<Club>().Any(c => c.Domain.Equals(clubDomain, StringComparison.OrdinalIgnoreCase));
            if (!isExist)
            {
                isExist = StaticValueClass.IsExist(clubDomain.ToLower());
            }

            Regex regex = new Regex("[A-Za-z0-9-]+");
            Match match = regex.Match(clubDomain);
            var isValid = match.Success && match.Index == 0 && match.Length == clubDomain.Length && clubDomain.Length >= 3 && clubDomain.Length < 64;

            return isValid && !isExist;
        }
        private bool IsNameValid(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return false;
            }

            return name.Length >= 2 && name.Length <= 64;
        }
        private bool IsPhoneValid(string phone)
        {
            if (string.IsNullOrWhiteSpace(phone))
            {
                return false;
            }

            return Regex.IsMatch(phone, "[0-9]+$") && phone.Length >= 8 && phone.Length <= 15;
        }

        private bool IsEmailValid(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            return Regex.IsMatch(email, Common.Constants.Constants.EmailRegex);
        }

        private bool IsWebsiteValid(string website)
        {
            Uri uriResult;
            bool result = Uri.TryCreate(website, UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            return result;
        }
    }

    public class DataManipulationRegformElementTitle : DataManipulationBaseModel<DataManipulationAddNewClubsItem>
    {
        public override string Title
        {
            get
            {
                return "Registration form Element Title";
            }
        }


        public override int GetNumberOfElements()
        {
            return DataContext.Set<ClubFormTemplate>().Count(c => c.Title != "Default");
        }

        public override DataManipulationGeneralResult Operate()
        {
            var from = 0;
            var to = int.MaxValue;
            var allElementNumbers = GetNumberOfElements();

            Initialize(allElementNumbers);

            GetWrongMedicalElements(true, from, to);

            FinalizeM(false);

            return Reslult;
        }

        public string GetWrongMedicalElements(bool rasOnly, int from = 1, int to = int.MaxValue)
        {
            string result = null;

            var allRasClubRegFormIds = new List<int>();

            var rassPartnerIds = DataContext.Set<Club>().Where(c => c.Domain == "rightatschool" || c.Domain == "rightatschoolcanada").Select(p => p.Id);

            if (rasOnly)
            {
                var rassClubIds = DataContext.Set<Club>().Where(c => c.PartnerId.HasValue && rassPartnerIds.Contains(c.PartnerId.Value)).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(c => c.Id).ToList();
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => rassClubIds.Contains(i.ClubId)).Select(f => f.JbForm_Id).ToList();
            }
            else
            {
                allRasClubRegFormIds = DataContext.Set<ClubFormTemplate>().Where(i => i.Title != "Default" && i.Title != "Default school" && (!i.Club.PartnerId.HasValue || i.Club.PartnerId.HasValue && !rassPartnerIds.Contains(i.Club.PartnerId.Value))).OrderBy(o => o.Id).Skip(from).Take(to - from).Select(f => f.JbForm_Id).ToList();
            }

            var allRasClubRegForms = DataContext.Set<JbForm>().Where(f => allRasClubRegFormIds.Contains(f.Id)).ToList();

            Log($"Number reg forms {allRasClubRegForms.Count().ToString()}");

            var allItems = new List<string>();

            foreach (var form in allRasClubRegForms)
            {
                var medicalElement = GetFormElement(form, "MedicalConditions", "HealthSection");

                if (medicalElement != null && medicalElement.Title.ToLower().Contains("allerg"))
                {
                    Log($"{form.Id} Medical conditions was changed");
                }

                var alergyElement = GetFormElement(form, "AllergiesInfo", "HealthSection");

                if (alergyElement != null && alergyElement.Title.ToLower().Contains("medi"))
                {
                    Log($"{form.Id} Allergies info was changed");
                }
            }

            return result;
        }

        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }


    }

    public class DataManipulationCreateProgramFromCatalog : DataManipulationBaseModel<DataManipulationCreateProgramFromCatalogItem>
    {
        public override string Title
        {
            get
            {
                return "Create program from catalog";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 30;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        public List<DataManipulationCreateProgramFromCatalogItem> CurrentPrograms { get; set; }
        public DataManipulationCreateProgramFromCatalogItem CurrentProgram { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentPrograms = SourceItems;

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentPrograms = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);

            BuildProgramsFromCatalog();

            FinalizeM();

            return this.Reslult;
        }

        private void BuildProgramsFromCatalog()
        {
            foreach (var sourceProgram in CurrentPrograms)
            {
                try
                {

                    CurrentProgram = sourceProgram;
                    BuildProgramFromCatalog();
                }
                catch (Exception ex)
                {
                    Log($"{CurrentProgram.Index} Has fatal error: {ex.Message}");
                }
            }
        }

        private void BuildProgramFromCatalog()
        {
            var seasonName = EnumHelper.ParseEnum<SeasonNames>(CurrentProgram.Season);
            var year = int.Parse(CurrentProgram.Year);
            switch (CurrentProgram.Action)
            {
                case "Nothing":
                    {

                    }
                    break;
                case "Add":
                case "Update":
                    {
                        AddUpdateProgram();
                        break;
                    }
                case "Delete":
                    {
                        if (string.IsNullOrEmpty(CurrentProgram.OldUniqueKey))
                        {
                            Log($"{CurrentProgram.Index} UniqueKey cannot be null ");
                            return;
                        }
                        var program = DataContext.Set<Program>().SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.OldUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName);
                        if (program == null)
                        {
                            program = DataContext.Set<Program>().Local.SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.OldUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName);
                        }
                        if (program == null)
                        {
                            Log($"{CurrentProgram.Index} Program doesn't exist in Db with unique key: {CurrentProgram.OldUniqueKey}"); return;
                        }
                        else
                        {
                            program.Status = ProgramStatus.Deleted;
                        }


                    }
                    break;
                case "Update Price":
                    {
                        if (string.IsNullOrEmpty(CurrentProgram.OldUniqueKey))
                        {
                            Log($"{CurrentProgram.Index} UniqueKey cannot be null ");
                            return;
                        }
                        var program = DataContext.Set<Program>().SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.OldUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName);
                        if (program == null)
                        {
                            program = DataContext.Set<Program>().Local.SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.OldUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName);
                        }
                        if (program == null)
                        {
                            Log($"{CurrentProgram.Index} Program doesn't exist in Db with unique key: {CurrentProgram.OldUniqueKey}");
                            return;
                        }
                        else
                        {
                            var tutions = program.ProgramSchedules.First(s => !s.IsDeleted).Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).ToList();
                            if (tutions == null || tutions.Count == 0)
                            {
                                Log($"{CurrentProgram.Index} program doesn't have any tuitions. Unique key: {CurrentProgram.OldUniqueKey} ");
                                return;
                            }
                            else
                            {
                                foreach (var tuition in tutions)
                                {
                                    tuition.Amount = Convert.ToDecimal(CurrentProgram.Price);
                                    tuition.MetaData.DateUpdated = DateTime.UtcNow;
                                    if (tuition.Name.ToLower().Trim() != CurrentProgram.TuitionLabel.ToLower().Trim())
                                    {
                                        Log($"{CurrentProgram.Index} Tuition label is diffirent. Unique key: {CurrentProgram.OldUniqueKey} {tuition.Name.ToLower().Trim()} -> {CurrentProgram.TuitionLabel.ToLower().Trim()}");

                                    }
                                }
                                if (tutions.Count > 1)
                                {
                                    Log($"{CurrentProgram.Index} More than one tution exist. Unique key: {CurrentProgram.OldUniqueKey} ");

                                }
                            }
                        }


                    }
                    break;
                default:
                    Log($"{CurrentProgram.Index} Action is invalid");
                    break;

            }


        }

        private bool AlreadyAdded()
        {
            if (CurrentProgram.AlreadyAdded.ToLower() == "x" || CurrentProgram.AlreadyAdded.ToLower() == "yes")
                return true;
            return false;
        }
        private List<ScheduleSession> GenerateSessions(DateTime StartDate, DateTime? endDate, List<ProgramScheduleDay> days, ContinueType contineuType, int occurances)
        {
            DateTime? scheduleEndDate = null;

            switch (contineuType)
            {
                case ContinueType.Until:
                    {
                        scheduleEndDate = endDate;
                    }
                    break;
                case ContinueType.For:
                    {

                        if (endDate.HasValue)
                        {
                            TimeSpan time = endDate.Value - StartDate;
                            var countDays = time.TotalDays;
                            scheduleEndDate = StartDate.AddDays(countDays);
                        }
                        else
                        {
                            scheduleEndDate = StartDate.AddDays(7 * occurances);
                        }

                    }
                    break;
            }

            var scheduleSections = new List<ScheduleSession>();

            DateTime currentDate = StartDate;

            var random = new Random();

            // Add selected days to schedule list
            while (currentDate <= scheduleEndDate)
            {
                DayOfWeek dayOfWeek = currentDate.DayOfWeek;

                foreach (var item in days)
                {
                    if (dayOfWeek == item.DayOfWeek)
                    {
                        scheduleSections.Add(new
                            ScheduleSession()
                        {
                            Id = random.Next(1000000, 9999999),
                            Start = currentDate.Date.Add(item.StartTime.Value),
                            End = currentDate.Date.Add(item.EndTime.Value),
                        });
                    }
                }

                currentDate = currentDate.AddDays(1);
            }

            return scheduleSections;
        }
        private SchoolGradeType? GetGrade(string grade)
        {

            switch (grade.ToLower())
            {
                case "not in school":
                    return SchoolGradeType.NotInSchool;
                case "preschool":
                    return SchoolGradeType.PreSchool;
                case "preschool - 3":
                    return SchoolGradeType.PreSchool3;
                case "preschool - 4":
                    return SchoolGradeType.PreSchool4;
                case "kindergarten":
                    return SchoolGradeType.Kindergarten;
                case "1":
                    return SchoolGradeType.S1;
                case "2":
                    return SchoolGradeType.S2;
                case "3":
                    return SchoolGradeType.S3;
                case "4":
                    return SchoolGradeType.S4;
                case "5":
                    return SchoolGradeType.S5;
                case "6":
                    return SchoolGradeType.S6;
                case "7":
                    return SchoolGradeType.S7;
                case "8":
                    return SchoolGradeType.S8;
                case "9":
                    return SchoolGradeType.S9;
                case "10":
                    return SchoolGradeType.S10;
                case "11":
                    return SchoolGradeType.S11;
                case "12":
                    return SchoolGradeType.S12;
                case "college":
                    return SchoolGradeType.College;
            }
            return null;
        }
        private void AddUpdateProgram()
        {
            Program program;
            var seasonName = EnumHelper.ParseEnum<SeasonNames>(CurrentProgram.Season);
            var year = int.Parse(CurrentProgram.Year);
            if (CurrentProgram.Action == "Add")
            {
                if (string.IsNullOrEmpty(CurrentProgram.NewUniqueKey))
                {
                    Log($"{CurrentProgram.Index} UniqueKey cannot be null ");
                    return;
                }
                
                program = DataContext.Set<Program>().SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.NewUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName);
                if (program == null)
                {
                    program = DataContext.Set<Program>().Local.SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.NewUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName);
                }
                if (AlreadyAdded())
                {
                    if (program == null)
                    {
                        Log($"{CurrentProgram.Index} Program is not already added with unique key: {CurrentProgram.NewUniqueKey}"); return;
                    }
                    else
                    {
                        return;
                    }
                }
                else if (program != null)
                {
                    Log($"{CurrentProgram.Index} Program exists in Db with unique key: {CurrentProgram.NewUniqueKey}"); return;
                }
                program = new Program();

            }
            else
            {
                if (string.IsNullOrEmpty(CurrentProgram.NewUniqueKey) || string.IsNullOrEmpty(CurrentProgram.OldUniqueKey))
                {
                    Log($"{CurrentProgram.Index} UniqueKey cannot be null ");
                    return;
                }
                program = DataContext.Set<Program>().SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.OldUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName);
                if (program == null)
                {
                    program = DataContext.Set<Program>().Local.SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.OldUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName);
                }
                if (program == null)
                {
                    Log($"{CurrentProgram.Index} Program doesn't exist in Db with unique key: {CurrentProgram.OldUniqueKey}");
                    return;
                }
                if (DataContext.Set<Program>().SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.NewUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName && p.Season.Status != SeasonStatus.Deleted && p.Season.Title.Equals(CurrentProgram.SchoolSeasonTitle, StringComparison.OrdinalIgnoreCase) && p.Id != program.Id) != null
                    || DataContext.Set<Program>().Local.SingleOrDefault(p => p.UniqueKey.Equals(CurrentProgram.NewUniqueKey, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted && p.Season.Year == year && p.Season.Name == seasonName && p.Season.Status != SeasonStatus.Deleted && p.Season.Title.Equals(CurrentProgram.SchoolSeasonTitle, StringComparison.OrdinalIgnoreCase) && p.Id != program.Id) != null
                    )
                {
                    Log($"{CurrentProgram.Index} Unique key cannot be duplicate: {CurrentProgram.NewUniqueKey}");
                    return;
                }

            }
            program.UniqueKey = CurrentProgram.NewUniqueKey;
            #region Season
            Club school;
            Club provider;
            Season schoolSeason;
            Season providerSeason;
            if (program.Id == 0)
            {
                var schoolTypeIds = new List<int> { 6, 7, 8, 9 };
                var schools = DataContext.Set<Club>().Where(c => c.Name.ToLower() == CurrentProgram.School.ToLower() && c.PartnerId.HasValue && c.PartnerClub.Domain.ToLower() == CurrentProgram.PartnerDomain.ToLower() && !c.IsDeleted && schoolTypeIds.Contains(c.TypeId)).ToList();

                if (schools.Count() == 0)
                {
                    Log($"{CurrentProgram.Index} School doesn't exist"); return;
                }
                else if (schools.Count() > 1)
                {
                    Log($"{CurrentProgram.Index} more than one scholl exist"); return;
                }
                else
                {
                    school = schools[0];
                }


                var providerTypeIds = new List<int> { 4, 5 };
                var providers = DataContext.Set<Club>().Where(c => c.Name.ToLower() == CurrentProgram.Provider.ToLower() && c.PartnerId.HasValue && c.PartnerClub.Domain.ToLower() == CurrentProgram.PartnerDomain.ToLower() && !c.IsDeleted && providerTypeIds.Contains(c.TypeId)).ToList();

                if (providers.Count() == 0)
                {
                    Log($"{CurrentProgram.Index} Provider doesn't exist"); return;
                }
                else if (providers.Count() > 1)
                {
                    Log($"{CurrentProgram.Index} more than one provider exist"); return;
                }
                else
                {
                    provider = providers[0];
                }
                


                var schoolSeasons = DataContext.Set<Season>().Where(s => s.ClubId == school.Id && s.Title.ToLower() == CurrentProgram.SchoolSeasonTitle.ToLower() && s.Status != SeasonStatus.Deleted && s.Name == seasonName && s.Year == year).ToList();

                if (schoolSeasons.Count() > 1)
                {
                    Log($"{CurrentProgram.Index} more than one scholl season exist"); return;
                }
                else if (schoolSeasons.Count() == 1)
                {
                    schoolSeason = schoolSeasons[0];
                }
                else
                {
                    try
                    {
                        schoolSeason = DataContext.Set<Season>().Local.SingleOrDefault(s => s.ClubId == school.Id && s.Title.ToLower() == CurrentProgram.SchoolSeasonTitle.ToLower() && s.Status != SeasonStatus.Deleted && s.Name == seasonName && s.Year == year);
                    }
                    catch
                    {
                        Log($"{CurrentProgram.Index} Local school season has error"); return;
                    }
                }

                if (schoolSeason == null)
                {
                    schoolSeason = new Season()
                    {
                        MetaData = new MetaData()
                        {
                            DateCreated = DateTime.UtcNow,
                            DateUpdated = DateTime.UtcNow

                        },
                        ClubId = school.Id,
                        Domain = Ioc.SeasonBusiness.GenerateSeasonDomain(CurrentProgram.SchoolSeasonTitle.ToLower().Trim().Replace(" ", ""), school.Id),
                        Setting = new SeasonSettings(),
                        Status = SeasonStatus.Live,
                        Title = CurrentProgram.SchoolSeasonTitle,
                        Year = int.Parse(CurrentProgram.Year),
                        Name = EnumHelper.ParseEnum<SeasonNames>(CurrentProgram.Season),
                        HasOutSourcePrograms = true
                    };
                }

                var seasonTitle = $"{CurrentProgram.Season} {CurrentProgram.Year}";

                var providerSeasons = DataContext.Set<Season>().Where(s => s.ClubId == provider.Id && s.Status != SeasonStatus.Deleted && s.Name == seasonName && s.Year == year && seasonTitle.ToLower() == seasonTitle.ToLower()).ToList();

                if (providerSeasons.Count() > 1)
                {
                    Log($"{CurrentProgram.Index} more than one provider season exist"); return;
                }
                else if (providerSeasons.Count() == 1)
                {
                    providerSeason = providerSeasons[0];
                }
                else
                {
                    try
                    {
                        providerSeason = DataContext.Set<Season>().Local.SingleOrDefault(s => s.ClubId == provider.Id && s.Status != SeasonStatus.Deleted && s.Name == seasonName && s.Year == year && seasonTitle.ToLower() == seasonTitle.ToLower());
                    }
                    catch
                    {
                        Log($"{CurrentProgram.Index} Local provider season has error"); return;
                    }
                }

                if (providerSeason == null)
                {
                    providerSeason = new Season()
                    {
                        MetaData = new MetaData()
                        {
                            DateCreated = DateTime.UtcNow,
                            DateUpdated = DateTime.UtcNow

                        },
                        ClubId = provider.Id,
                        Domain = Ioc.SeasonBusiness.GenerateSeasonDomain(seasonTitle.ToLower().Trim().Replace(" ", ""), provider.Id),
                        Setting = new SeasonSettings(),
                        Status = SeasonStatus.Live,
                        Title = seasonTitle,
                        Year = int.Parse(CurrentProgram.Year),
                        Name = seasonName
                    };
                }
                #endregion Season
            }
            else
            {
                school = program.Club;
                schoolSeason = program.Season;
                provider = program.OutSourceSeason.Club;
                providerSeason = program.OutSourceSeason;
            }
            #region Main
            if (program.Id == 0)
            {
                program.MetaData = new MetaData()
                {
                    DateCreated = DateTime.UtcNow,
                };
                program.ClubId = school.Id;
                program.Season = schoolSeason;
                program.RegistrationMode = RegistrationMode.Open;
                program.Status = ProgramStatus.Open;
                program.LastCreatedPage = ProgramPageStep.Step2;
                program.OutSourceSeason = providerSeason;
                program.OutSourceProgramStatus = OutSourceProgramStatus.Invitation;
                program.Flyer = new Core.Domain.Flyer()
                {
                    FileName = "testFlyer",
                    FlyerType = FlyerType.Generate
                };

                program.TypeCategory = ProgramTypeCategory.Class;
                program.Name = CurrentProgram.Class;
                program.Domain = Ioc.ProgramBusiness.GenerateSubDomainName(school.Domain, schoolSeason.Domain, CurrentProgram.Class);
            }

            program.MetaData.DateUpdated = DateTime.UtcNow;


            #endregion

            #region Catalog
            CatalogItem catalog;
            if (program.Id == 0)
            {
                var catalogs = DataContext.Set<CatalogItem>().Where(c => c.ClubId == provider.Id && c.Status != CatalogStatus.Deleted && c.Name.ToLower().Replace("  ", "  ") == CurrentProgram.Class.ToLower().Replace("  ", "  ")).ToList();
                if (catalogs == null || catalogs.Count() == 0)
                {
                    Log($"{CurrentProgram.Index} ({provider.Domain} --> {CurrentProgram.Class}) Catalog doesn't exist"); return;
                }
                else if (catalogs.Count() > 1)
                {
                    Log($"{CurrentProgram.Index} ({provider.Domain} --> {CurrentProgram.Class}) more than one catalog exist"); return;
                }
                else
                {
                    catalog = catalogs[0];
                }
                program.Description = catalog.Description;
                program.CatalogId = catalog.Id;
            }
            else
            {
                catalog = program.Catalog;
            }
            #endregion

            #region Category

            if (program.Id == 0)
            {
                program.Categories = catalog.Categories;
            }

            #endregion Category

            #region Location
            if (program.Id == 0)
            {
                var loc = school.ClubLocations.Any(l => !l.IsDeleted) ? school.ClubLocations.First(l => !l.IsDeleted) : null;
                if (loc == null)
                {
                    Log($"{CurrentProgram.Index} school doesn't have any locations"); return;
                }

                var programLocation = school.ClubLocations.First(l => !l.IsDeleted);
                program.ClubLocationId = programLocation.Id;

                if (!provider.ClubLocations.Any(c => c.PostalAddress.Id == programLocation.PostalAddress.Id))
                {
                    provider.ClubLocations.Add(new ClubLocation()
                    {
                        PostalAddress = programLocation.PostalAddress
                    });
                }
            }
            #endregion Location

            #region Dates
            ProgramSchedule programSchedule;
            if (program.Id == 0)
            {
                programSchedule = new ProgramSchedule();
            }
            else
            {
                programSchedule = program.ProgramSchedules.First(p => !p.IsDeleted);
            }
            if (!string.IsNullOrEmpty(CurrentProgram.StartDate))
            {
                try
                {
                    programSchedule.StartDate = DateTime.Parse(CurrentProgram.StartDate);
                }
                catch
                {
                    Log($"{CurrentProgram.Index} Program start date is wrong");
                    return;
                }
            }
            else
            {
                Log("Program start date is required");
                return;
            }

            if (!string.IsNullOrEmpty(CurrentProgram.EndDate))
            {
                try
                {
                    programSchedule.EndDate = DateTime.Parse(CurrentProgram.EndDate);
                }
                catch
                {
                    Log($"{CurrentProgram.Index} Program end date is wrong");
                    return;
                }
            }
            else
            {
                Log($"{CurrentProgram.Index} Program end date is required");
                return;
            }
            if (program.Id == 0)
            {
                programSchedule.RegistrationPeriod = new RegistrationPeriod();
            }
            if (!string.IsNullOrEmpty(CurrentProgram.RegStartDate))
            {
                try
                {
                    programSchedule.RegistrationPeriod.RegisterStartDate = DateTime.Parse(CurrentProgram.RegStartDate);
                }
                catch
                {
                    Log($"{CurrentProgram.Index} Program reg start date is wrong");
                    return;
                }
            }
            else
            {
                programSchedule.RegistrationPeriod.RegisterStartDate = DateTime.Parse(CurrentProgram.StartDate);
            }

            if (!string.IsNullOrEmpty(CurrentProgram.RegEndDate))
            {
                try
                {
                    programSchedule.RegistrationPeriod.RegisterEndDate = DateTime.Parse(CurrentProgram.RegEndDate);
                }
                catch
                {
                    Log($"{CurrentProgram.Index} Program reg end date is wrong");
                    return;
                }
            }
            else
            {
                programSchedule.RegistrationPeriod.RegisterEndDate = DateTime.Parse(CurrentProgram.EndDate);
            }

            if (!string.IsNullOrEmpty(CurrentProgram.RegStartTime))
            {
                try
                {
                    programSchedule.RegistrationPeriod.RegisterStartTime = TimeSpan.Parse(CurrentProgram.RegStartTime);
                }
                catch
                {
                    Log($"{CurrentProgram.Index} Reg start time is wrong");
                    return;
                }
            }
            else
            {
                programSchedule.RegistrationPeriod.RegisterStartTime = TimeSpan.Parse("00:00");
            }

            if (!string.IsNullOrEmpty(CurrentProgram.RegEndTime))
            {
                try
                {
                    programSchedule.RegistrationPeriod.RegisterStartTime = TimeSpan.Parse(CurrentProgram.RegEndTime);
                }
                catch
                {
                    Log($"{CurrentProgram.Index} Reg end time is wrong");
                    return;
                }
            }
            else
            {
                programSchedule.RegistrationPeriod.RegisterEndTime = TimeSpan.Parse("23:59");
            }
            #endregion

            #region Day & Capaciy
            ScheduleAttribute scheduleAttribute = new ScheduleAttribute();
            if (program.Id == 0)
            {
                var catalogCapacity = catalog.MaximumEnrollment;
                var catalogMinimumEnrollment = catalog.MinimumEnrollment;

                if (!catalogCapacity.HasValue)
                {
                    scheduleAttribute.Capacity = 0;
                }
                else
                {
                    scheduleAttribute.Capacity = catalogCapacity.Value;
                }
                if (!catalogMinimumEnrollment.HasValue)
                {
                    scheduleAttribute.MinimumEnrollment = 0;
                }
                else
                {
                    scheduleAttribute.MinimumEnrollment = catalogMinimumEnrollment.Value;
                }
            }
            else
            {
                scheduleAttribute.Capacity = programSchedule.Attributes.Capacity;
                scheduleAttribute.MinimumEnrollment = programSchedule.Attributes.MinimumEnrollment;
            }
            scheduleAttribute.WeekDaysMode = WeekDaysMode.SpecialDay;
            scheduleAttribute.ContinueType = ContinueType.Until;
            scheduleAttribute.Days = new List<ProgramScheduleDay>();

            if (!string.IsNullOrEmpty(CurrentProgram.StartTime) && !string.IsNullOrEmpty(CurrentProgram.EndTime))
            {
                try
                {
                    DateTime programStartTime = new DateTime();
                    programStartTime = DateTime.Parse(CurrentProgram.StartTime);
                    DateTime programEndTime = new DateTime();
                    programEndTime = DateTime.Parse(CurrentProgram.EndTime);
                    if (CurrentProgram.PMAM.ToLower() == "pm")
                    {
                        programStartTime = programStartTime.AddHours(12);
                        programEndTime = programEndTime.AddHours(12);
                    }
                    DayOfWeek dayOfWeek = new DayOfWeek();
                    try
                    {
                        dayOfWeek = EnumHelper.ParseEnum<DayOfWeek>(CurrentProgram.Day);
                    }
                    catch
                    {
                        Log($"{CurrentProgram.Index}  day is invalid");
                    }
                    var day = new ProgramScheduleDay
                    {
                        DayOfWeek = dayOfWeek,
                        StartTime = programStartTime.TimeOfDay,
                        EndTime = programEndTime.TimeOfDay
                    };

                    scheduleAttribute.Days.Add(day);
                }
                catch (Exception e)
                {
                    Log($"{CurrentProgram.Index}  start time or end time is invalid");
                    return;
                }
            }
            else
            {
                Log($"{CurrentProgram.Index} start time or end time is required");
                return;
            }

            #endregion Day

            #region Tuition
            if (program.Id == 0)
            {
                Charge tuition = new Charge();
                if (!string.IsNullOrEmpty(CurrentProgram.Price) && !string.IsNullOrEmpty(CurrentProgram.TuitionLabel))
                {
                    tuition.Name = CurrentProgram.TuitionLabel;

                    try
                    {
                        tuition.Amount = decimal.Parse(CurrentProgram.Price);
                        tuition.AmountType = ChargeDiscountType.Fixed;
                        tuition.Category = ChargeDiscountCategory.EntryFee;
                        tuition.MetaData = new MetaData()
                        {
                            DateCreated = DateTime.UtcNow,
                            DateUpdated = DateTime.UtcNow
                        };

                        programSchedule.Charges = new List<Charge>()
                    {
                        tuition
                    };
                    }
                    catch
                    {
                        Log($"{CurrentProgram.Index} price amount is not valid");
                        return;
                    }

                }
                else
                {
                    Log($"{CurrentProgram.Index} price {nameof(CurrentProgram.Price)} and tuition lable is required");
                    return;
                }
            }
            else
            {
                var tutions = program.ProgramSchedules.First(s => !s.IsDeleted).Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).ToList();
                if (tutions == null || tutions.Count == 0)
                {
                    Log($"{CurrentProgram.Index} program doesn't have any tuitions. Unique key: {CurrentProgram.OldUniqueKey} ");
                    return;
                }
                else
                {
                    foreach (var tuition in tutions)
                    {
                        tuition.Amount = Convert.ToDecimal(CurrentProgram.Price);
                        tuition.MetaData.DateUpdated = DateTime.UtcNow;
                        if (tuition.Name.ToLower().Trim() != CurrentProgram.TuitionLabel.ToLower().Trim())
                        {
                            Log($"{CurrentProgram.Index} Tuition label is diffirent. Unique key: {CurrentProgram.OldUniqueKey} {tuition.Name.ToLower().Trim()} -> {CurrentProgram.TuitionLabel.ToLower().Trim()}");

                        }
                    }
                    if (tutions.Count > 1)
                    {
                        Log($"{CurrentProgram.Index} More than one tution exist. Unique key: {CurrentProgram.OldUniqueKey} ");

                    }
                }
            }
            #endregion

            #region Restriction
            if (program.Id == 0)
            {
                programSchedule.AttendeeRestriction = new AttendeeRestriction();
            }
            if (CurrentProgram.GradesAges == "Grades")
            {
                var min = GetGrade(CurrentProgram.Min);
                var max = GetGrade(CurrentProgram.Max);
                if (min == null || max == null)
                {
                    Log($"{CurrentProgram.Index} restrictions are invalid");
                    return;
                }
                else
                {

                    programSchedule.AttendeeRestriction.RestrictionType = RestrictionType.Grade;
                    programSchedule.AttendeeRestriction.MinGrade = min;
                    programSchedule.AttendeeRestriction.MaxGrade = max;

                }
            }
            else if (CurrentProgram.GradesAges == "Ages")
            {
                try
                {
                    programSchedule.AttendeeRestriction.RestrictionType = RestrictionType.Age;
                    programSchedule.AttendeeRestriction.MinAge = Convert.ToInt16(CurrentProgram.Min);
                    programSchedule.AttendeeRestriction.MaxAge = Convert.ToInt16(CurrentProgram.Max);

                }
                catch
                {
                    Log($"{CurrentProgram.Index} restrictions are invalid");
                    return;
                }
            }
            else
            {
                Log($"{CurrentProgram.Index} restrictions are invalid");
                return;
            }
            #endregion Restriction

            #region Sessions
            programSchedule.Sessions = GenerateSessions(programSchedule.StartDate, programSchedule.EndDate, scheduleAttribute.Days, scheduleAttribute.ContinueType, 1);
            #endregion

            programSchedule.AttributesSerialized = JsonConvert.SerializeObject(scheduleAttribute);

            if (program.Id == 0)
            {
                program.ProgramSchedules = new List<ProgramSchedule>() { programSchedule };
                DataContext.Set<Program>().Add(program);
            }

        }
    }

    public class DataManipulationCreateProgramSendMessage : DataManipulationBaseModel<DataManipulationCreateProgramFromCatalogItem>
    {
        public override string Title
        {
            get
            {
                return "Send program message";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 20;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        public List<DataManipulationCreateProgramFromCatalogItem> CurrentPrograms { get; set; }
        public DataManipulationCreateProgramFromCatalogItem CurrentProgram { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentPrograms = SourceItems;

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentPrograms = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);

            SendMessages();

            FinalizeM();

            return this.Reslult;
        }

        private void SendMessages()
        {
            foreach (var program in SourceItems)
            {
                CurrentProgram = program;
                SendMessage();
            }
        }

        private bool AlreadyAdded()
        {
            if (CurrentProgram.AlreadyAdded.ToLower() == "x" || CurrentProgram.AlreadyAdded.ToLower() == "yes")
                return true;
            return false;
        }

        private void SendMessage()
        {
            try
            {
                if (CurrentProgram.Action == "Add" && !AlreadyAdded())
                {
                    var program = DataContext.Set<Program>().SingleOrDefault(p => p.Status != ProgramStatus.Deleted && p.UniqueKey.Equals(CurrentProgram.NewUniqueKey));

                    if (program == null)
                    {
                        Log($"{CurrentProgram.Index} Program doesn't exist"); return;
                    }

                    if (program.MessageSent)
                    {
                        Log($"{CurrentProgram.Index} Message allready sent");
                        return;
                    }

                    if (!program.CatalogId.HasValue)
                    {
                        Log($"{CurrentProgram.Index} program doesn't have catalog");
                        return;
                    }

                    SendInviteMessage(program);

                    program.MessageSent = true;
                }
            }
            catch (Exception ex)
            {
                Log($"{CurrentProgram.Index} has total error and message not sent: {(ex != null ? ex.Message : string.Empty)}");
            }
        }

        private void SendInviteMessage(Program program)
        {

            var messageBusiness = Ioc.MessageBusiness;

            var outsourceSeason = program.OutSourceSeason;
            var club = outsourceSeason.Club;
            var school = program.Club;
            var partner = club.PartnerClub;

            var catalog = program.Catalog;

            //Program start time and end time
            //var startTime = program.ProgramSchedules.First().st
            var inviteMessageModel = Ioc.CatalogBusiness.BuildInviteMessageBody(program, outsourceSeason, null, null);
            var text = ViewHelper.RenderViewToString("~/Areas/Dashboard/Views/Catalog/InviteMessage.cshtml", inviteMessageModel);

            var subject = string.Format("Session request: {0}", program.Name);

            int realReceiverId = 0;


            var sender = Ioc.MessageBusiness.GetPartnerMessageReciever(partner, school, outsourceSeason.Id);

            var receiver = Ioc.MessageBusiness.GetClubMessageReciever(club);

            var attribute = new MessageScheduleDraftSessionAttribute()
            {
                Status = ScheduleDraftSessionStatus.Draft,
                ProgramId = program.Id,
            };

            var attributeSerialized = JsonConvert.SerializeObject(attribute);

            var result = SendMessage(null, sender.Id, club.Id, sender.Id, partner.Id, receiver.Id, club.Id, ref realReceiverId, subject, text, MessageType.ScheduleDraftSession, attributeSerialized, program.Id);

            var receiverUser = Ioc.UserProfileBusiness.Get(realReceiverId);

            EmailService.Get(ControllerHelper.CreateController<CatalogController>().ControllerContext).SendMessagingEmail(receiverUser.UserName, sender.UserName, text, partner.Domain, result.ChatId, partner.Logo, program.Name, partner.Name, club.Domain, true);
        }

        private MessageHeader SendMessage(string chatId, int currentUserId, int currentClubId, int? senderId, int? senderClubId, int? receiverId, int? receiverClubId, ref int realReceiverId, string subject = "", string text = "", MessageType messageType = MessageType.Ordinary, string attribute = "", long? programId = null)
        {

            var reciverIds = new Dictionary<int?, int?>();

            if (receiverId.HasValue)
            {
                reciverIds.Add(receiverClubId, receiverId);
            }

            var result = SendMessage(chatId, currentUserId, currentClubId, senderId, senderClubId, reciverIds, ref realReceiverId, subject, text, messageType, attribute, programId);

            return result;
        }

        private MessageHeader SendMessage(string chatId, int currentUserId, int currentClubId, int? senderId, int? senderClubId, Dictionary<int?, int?> receiverIds, ref int finalReceiverId, string subject = "", string text = "", MessageType messageType = MessageType.Ordinary, string attribute = "", long? programId = null)
        {
            MessageHeader messageHeader = null;

            if (string.IsNullOrEmpty(chatId))
            {
                var recieverId = receiverIds.Last();

                messageHeader = Ioc.MessageBusiness.GetChat(senderId.Value, senderClubId.Value, recieverId.Value.Value, recieverId.Key.Value, subject, text, messageType, attribute, programId);
            }
            else
            {
                messageHeader = DataContext.Set<MessageHeader>().Single(s => s.ChatId.Equals(chatId));
            }

            var realSenderId = currentUserId;
            var realSenderClubId = senderClubId.HasValue ? senderClubId : currentClubId;

            var realReceiverId = 0;
            var realReceiverClubId = 0;

            if (!string.IsNullOrEmpty(chatId) && messageHeader.Messages.Any())
            {
                if (messageHeader.Messages.Any(m => m.SenderId != realSenderId))
                {
                    realReceiverId = messageHeader.Messages.Last(m => m.SenderId != realSenderId).SenderId;
                    realReceiverClubId = messageHeader.Messages.Last(m => m.SenderId != realSenderId).ClubId;
                }
                else if (messageHeader.Messages.Any(m => m.UserMessages.Any(u => u.ReceiverId != realSenderId)))
                {
                    realReceiverId = messageHeader.Messages.SelectMany(m => m.UserMessages).Last(u => u.ReceiverId != realSenderId).ReceiverId;
                    realReceiverClubId = messageHeader.Messages.SelectMany(m => m.UserMessages).Last(u => u.ReceiverId != realSenderId).ClubId;
                }
                else
                {
                    throw new Exception("There is no receiver to send message.");
                }
            }
            else
            {
                realReceiverId = receiverIds.Last().Value.Value;
                realReceiverClubId = receiverIds.Last().Key.Value;
            }

            var message = new Message { ClubId = realSenderClubId.Value, SenderId = realSenderId, CreatedDate = DateTime.UtcNow, Text = text };

            var userMessage = new UserMessage()
            {
                ClubId = realReceiverClubId,
                Message = message,
                ReceiverId = realReceiverId,
            };

            message.UserMessages.Add(userMessage);

            messageHeader.Messages.Add(message);

            if (!string.IsNullOrEmpty(attribute))
            {
                messageHeader.AttributeSerialized = attribute;
            }

            finalReceiverId = realReceiverId;

            if (messageHeader.Id < 1)
            {
                DataContext.Set<MessageHeader>().Add(messageHeader);
            }

            return messageHeader;
        }
    }

    public class DataManipultaionCreateOrderSessioins : DataManipulationBaseModel<DataManipultaionCreateOrderSessioinsItem>
    {
        public override string Title
        {
            get
            {
                return "Data Manipultaion Create Order Sessioins";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        public List<DataManipultaionCreateOrderSessioinsItem> CurrentOrderItems { get; set; }
        public DataManipultaionCreateOrderSessioinsItem CurrentOrderItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentOrderItems = SourceItems;

            var numberOfOrderItems = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfOrderItems);

            AddSessions();

            FinalizeM();

            return this.Reslult;
        }

        private void AddSessions()
        {
            foreach (var item in CurrentOrderItems)
            {
                CurrentOrderItem = item;

                AddSession();
            }
        }

        private void AddSession()
        {
            try
            {
                var orderItemId = long.Parse(CurrentOrderItem.OrderItemId);
                var orderItem = DataContext.Set<OrderItem>().SingleOrDefault(o => o.Id == orderItemId);

                if (orderItem == null)
                {
                    Log($"{CurrentOrderItem.Index} Order is not exist in db.");
                    return;
                }

                var days = GetWeekDays();

                if (days == null || !days.Any())
                {

                    Log("Error in get days or days is empty.");
                    return;
                }

                try
                {
                    orderItem.Attributes = new OrderItemAttributes() { WeekDays = days };
                }
                catch
                {
                    Log("Error in set attibute.");
                    return;
                }

                try
                {
                    orderItem.OrderSessions = Create(orderItem, days);
                }
                catch
                {
                    Log("Error in creating sessions.");
                    return;
                }
            }
            catch (Exception ex)
            {

                Log(ex.Message);
                return;
            }
        }

        private List<DayOfWeek> GetWeekDays()
        {
            var result = new List<DayOfWeek>();

            if (!string.IsNullOrEmpty(CurrentOrderItem.Mon) && CurrentOrderItem.Mon.Equals("Y"))
            {
                result.Add(DayOfWeek.Monday);
            }
            if (!string.IsNullOrEmpty(CurrentOrderItem.Tue) && CurrentOrderItem.Tue.Equals("Y"))
            {
                result.Add(DayOfWeek.Tuesday);
            }
            if (!string.IsNullOrEmpty(CurrentOrderItem.Wed) && CurrentOrderItem.Wed.Equals("Y"))
            {
                result.Add(DayOfWeek.Wednesday);
            }
            if (!string.IsNullOrEmpty(CurrentOrderItem.Thu) && CurrentOrderItem.Thu.Equals("Y"))
            {
                result.Add(DayOfWeek.Thursday);
            }
            if (!string.IsNullOrEmpty(CurrentOrderItem.Fri) && CurrentOrderItem.Fri.Equals("Y"))
            {
                result.Add(DayOfWeek.Friday);
            }

            return result;
        }

        private List<OrderSession> Create(OrderItem orderItem, List<DayOfWeek> days)
        {
            var result = new List<OrderSession>();
            var programSessions = new List<ProgramSession>();

            var desiredStartDate = orderItem.DesiredStartDate;

            if (orderItem.ProgramSchedule.ScheduleMode != TimeOfClassFormation.Both)
            {
                programSessions = Ioc.ProgramSessionBusiness.GetList(orderItem.ProgramSchedule);
            }
            else
            {
                var beforeAfterSchedules = orderItem.ProgramSchedule.Program.ProgramSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both);

                foreach (var schedule in beforeAfterSchedules)
                {
                    programSessions.AddRange(Ioc.ProgramSessionBusiness.GetList(schedule));
                }
            }


            foreach (var programSession in programSessions.Where(p => p.StartDateTime >= desiredStartDate))
            {
                if (days.Contains(programSession.StartDateTime.DayOfWeek))
                {
                    var orderSession = new OrderSession()
                    {
                        ProgramSessionId = programSession.Id,
                        MetaData = new MetaData()
                        {
                            DateCreated = DateTime.UtcNow,
                            DateUpdated = DateTime.UtcNow,
                        }
                    };

                    orderItem.OrderSessions.Add(orderSession);
                    result.Add(orderSession);
                }
            }

            return result;
        }
    }

    public class DataManipulationCompleteAcceptedPrograms : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Complete accepted programs";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return AllPrograms.Count();
        }

        public List<Program> CurrentPrograms { get; set; }
        public Program CurrentProgram { get; set; }

        public IEnumerable<Program> AllPrograms { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            AllPrograms = DataContext.Set<Program>().Where(p =>
                p.Status != ProgramStatus.Deleted
                && p.Club.PartnerId.HasValue && p.Club.PartnerClub.Domain.Equals("flexacademies")
                && p.Season.Name.HasValue && p.Season.Year.HasValue
                && ((p.Season.Name == SeasonNames.WinterInterim || p.Season.Name == SeasonNames.Spring) && p.Season.Year == 2019));

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentPrograms = AllPrograms.OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);

            CompleteAcceptedPrograms();

            FinalizeM();

            return this.Reslult;
        }

        private void CompleteAcceptedPrograms()
        {
            foreach (var item in CurrentPrograms)
            {
                CurrentProgram = item;
                CompleteAcceptedProgram();
            }
        }

        private void CompleteAcceptedProgram()
        {
            var lastNotModifiedResponse = DataContext.Set<ProgramRespondToInvitation>().OrderByDescending(o => o.Id).FirstOrDefault(r => r.RespondType != ProgramRespondType.Modify && r.ProgramId == CurrentProgram.Id);

            if ((CurrentProgram.LastCreatedPage == ProgramPageStep.Step2 || CurrentProgram.LastCreatedPage == ProgramPageStep.Step3) && lastNotModifiedResponse != null && lastNotModifiedResponse.RespondType == ProgramRespondType.Accept)
            {

                if (CurrentProgram.ClubFormTemplates == null)
                    CurrentProgram.ClubFormTemplates = new List<ClubFormTemplate>();

                if (!CurrentProgram.ClubFormTemplates.Any(c => c.FormType == FormType.Registration))
                {
                    var ClubFormTemplate = CurrentProgram.Club.ClubFormTemplates.SingleOrDefault(c => !c.IsDeleted && c.FormType == FormType.Registration && c.Title.ToLower().Equals("default"));
                    int? seasonFormId = null;
                    if (CurrentProgram.Season.Setting != null && CurrentProgram.Season.Setting.SeasonFormsSetting != null && CurrentProgram.Season.Setting.SeasonFormsSetting.Form != null)
                    {
                        seasonFormId = CurrentProgram.Season.Setting.SeasonFormsSetting.Form;
                    }
                    if(seasonFormId == null || seasonFormId == 0)
                    {
                        Log($"{CurrentProgram.Id} season form is not set.");
                    }
                    else
                    {
                        ClubFormTemplate = CurrentProgram.Club.ClubFormTemplates.SingleOrDefault(c => !c.IsDeleted && c.FormType == FormType.Registration && c.Id == seasonFormId);
                    }
                                       

                    if (ClubFormTemplate == null)
                    {
                        Log($"{CurrentProgram.Id} defalt club form doesn't exist.");
                        return;
                    }
                    else
                    {
                        CurrentProgram.ClubFormTemplates.Add(ClubFormTemplate);
                        CurrentProgram.LastCreatedPage = ProgramPageStep.Step4;
                    }
                }

                Log($"Program '{CurrentProgram.Season.Name} {CurrentProgram.Name}({CurrentProgram.Id})' completed.");
            }

        }
    }

    public class DataManipulationUpdateClassPrice : DataManipulationBaseModel<DataManipultaionUpdateClassPriceItem>
    {
        public override string Title
        {
            get
            {
                return "Update class prices";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        public List<DataManipultaionUpdateClassPriceItem> CurrentCharges { get; set; }
        public DataManipultaionUpdateClassPriceItem CurrentCharge { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentCharges = SourceItems;

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentCharges = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);

            UpdateCharges();

            FinalizeM();

            return this.Reslult;
        }

        private void UpdateCharges()
        {
            foreach (var item in CurrentCharges)
            {
                CurrentCharge = item;
                UpdateCharge();
            }
        }

        private void UpdateCharge()
        {
            try
            {
                #region Provider
                Club provider;
                var providerTypeIds = new List<int> { 4, 5 };
                var providers = DataContext.Set<Club>().Where(c => c.Name.ToLower() == CurrentCharge.Provider.ToLower() && c.PartnerId.HasValue && c.PartnerClub.Domain.ToLower() == CurrentCharge.PartnerDomain.ToLower() && !c.IsDeleted && providerTypeIds.Contains(c.TypeId)).ToList();

                if (providers.Count() == 0)
                {
                    Log($"{CurrentCharge.Index} Provider doesn't exist");
                    return;
                }
                else if (providers.Count() > 1)
                {
                    Log($"{CurrentCharge.Index} more than one provider exist");
                    return;
                }
                else
                {
                    provider = providers[0];
                }
                #endregion

                #region Catalog
                CatalogItem catalog;
                var catalogs = DataContext.Set<CatalogItem>().Where(c => c.ClubId == provider.Id && c.Status != CatalogStatus.Deleted && c.Name.ToLower().Replace("  ", " ") == CurrentCharge.Class.ToLower().Replace("  ", " ")).ToList();
                if (catalogs.Count() == 0)
                {
                    Log($"{CurrentCharge.Index} ({provider.Domain} --> {CurrentCharge.Class}) Catalog doesn't exist");
                    return;
                }
                else if (catalogs.Count() > 1)
                {
                    Log($"{CurrentCharge.Index} ({provider.Domain} --> {CurrentCharge.Class}) more than one catalog exist");
                    return;
                }
                else
                {
                    catalog = catalogs[0];
                }
                #endregion

                #region Programs
                var programs = DataContext.Set<Program>().Where(p => p.CatalogId == catalog.Id && p.Status != ProgramStatus.Deleted && (((p.Season.Name == SeasonNames.Fall || p.Season.Name == SeasonNames.FallInterim) && p.Season.Year == 2018) || (((p.Season.Name == SeasonNames.Winter || p.Season.Name == SeasonNames.Spring || p.Season.Name == SeasonNames.WinterInterim) && p.Season.Year == 2019)))).ToList();

                foreach (var program in programs)
                {
                    var schedule = program.ProgramSchedules.LastOrDefault(s => !s.IsDeleted);

                    if (schedule == null)
                    {
                        Log($"{CurrentCharge.Index} schedule doesn't exist");
                        return;
                    }

                    Charge charge;

                    var charges = schedule.Charges.Where(s => !s.IsDeleted && s.Category == ChargeDiscountCategory.EntryFee).ToList();

                    if (charges.Count() == 0)
                    {
                        Log($"{CurrentCharge.Index} hasn't any charges");
                        return;
                    }
                    else if (charges.Count() > 1)
                    {
                        Log($"{CurrentCharge.Index} more than one charge exist");
                        return;
                    }
                    else
                    {
                        charge = charges[0];
                    }

                    decimal price = 0;

                    if (decimal.TryParse(CurrentCharge.Price.Replace("$", "").Trim(), out price))
                    {
                        charge.Amount = price;
                        Log($"Price for program: '{program.UniqueKey}' was updated");
                    }
                    else
                    {
                        Log($"{CurrentCharge.Index} input price is not valid");
                        return;
                    }
                }

                #endregion
            }
            catch
            {
                Log($"{CurrentCharge.Index} has total error");
            }
        }
    }

    public class DataManipulationUpdateSeasonSetting : DataManipulationBaseModel<DataManipultaionUpdateSeasonSettingItem>
    {
        public override string Title
        {
            get
            {
                return "Update season settings";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        public DateTime NotValidDateTime
        {
            get { return new DateTime(1900, 01, 01); }
        }

        public List<DataManipultaionUpdateSeasonSettingItem> CurrentSeasons { get; set; }
        public DataManipultaionUpdateSeasonSettingItem CurrentSeason { get; set; }

        private const string BothAction = "Both";
        private const string DatesAction = "Dates";
        private const string FormsAction = "Forms";

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentSeasons = SourceItems;

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentSeasons = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);

            UpdateSeasons();

            FinalizeM();

            return this.Reslult;
        }

        private void UpdateSeasons()
        {
            foreach (var item in CurrentSeasons)
            {
                CurrentSeason = item;
                UpdateSeason();
            }
        }

        private void UpdateSeason()
        {
            try
            {
                var action = CurrentSeason.Action;
                if (string.IsNullOrWhiteSpace(action) || (!action.EqualsTrim(DatesAction) && !action.EqualsTrim(FormsAction) && !action.EqualsTrim(BothAction)))
                {
                    Log($"{CurrentSeason.Index} action is not correct");
                    return;
                }

                var school = DataContext.Set<Club>().SingleOrDefault(c => !c.IsDeleted && c.Domain.Equals(CurrentSeason.SchoolDomain, StringComparison.OrdinalIgnoreCase) && c.PartnerId.HasValue && c.PartnerClub.Domain.Equals(CurrentSeason.PartnerDomain, StringComparison.OrdinalIgnoreCase));

                if (school == null)
                {
                    Log($"{CurrentSeason.Index} school doesn't exist");
                    return;
                }

                Season season;

                var seasonName = EnumHelper.ParseEnum<SeasonNames>(CurrentSeason.Season);
                var seasonYear = int.Parse(CurrentSeason.Year);

                var seasons = DataContext.Set<Season>().Where(s => s.Status != SeasonStatus.Deleted && s.ClubId == school.Id && s.Title.Equals(CurrentSeason.SchoolSeasonTitle, StringComparison.OrdinalIgnoreCase) && s.Year == seasonYear && s.Name == seasonName).ToList();

                if (seasons.Count() == 0)
                {
                    Log($"{CurrentSeason.Index} Season doesn't exist");
                    return;
                }
                else if (seasons.Count() > 1)
                {
                    Log($"{CurrentSeason.Index} more than one season exist");
                    return;
                }
                else
                {
                    season = seasons[0];
                }

                if (season.Setting == null)
                    season.Setting = new SeasonSettings();

                if (season.Setting.SeasonProgramInfoSetting == null)
                    season.Setting.SeasonProgramInfoSetting = new SeasonProgramInfoSetting();

                var programInfo = season.Setting.SeasonProgramInfoSetting;

                if (action.Equals(BothAction) || action.Equals(DatesAction))
                {
                    #region Date times
                    if (IsDateTimeValid(CurrentSeason.GeneralRegistrationStartDate, CurrentSeason.GeneralRegistrationStartTime))
                        programInfo.GeneralRegistrationOpens = GetDateTime(CurrentSeason.GeneralRegistrationStartDate, CurrentSeason.GeneralRegistrationStartTime);
                    else
                    {
                        Log($"{CurrentSeason.Index} {nameof(CurrentSeason.GeneralRegistrationStartDate)} is not in correct format");
                        return;
                    }

                    if (IsDateTimeValid(CurrentSeason.GeneralRegistrationEndDate, CurrentSeason.GeneralRegistrationEndTime))
                        programInfo.GeneralRegistrationCloses = GetDateTime(CurrentSeason.GeneralRegistrationEndDate, CurrentSeason.GeneralRegistrationEndTime);
                    else
                    {
                        Log($"{CurrentSeason.Index} {nameof(CurrentSeason.GeneralRegistrationEndDate)} is not in correct format");
                        return;
                    }

                    if (string.IsNullOrWhiteSpace(CurrentSeason.HasLateRegistration))
                    {
                        Log($"{CurrentSeason.Index} {nameof(CurrentSeason.HasLateRegistration)} is empty");
                        return;
                    }

                    if (CurrentSeason.HasLateRegistration.Equals("yes", StringComparison.OrdinalIgnoreCase))
                    {
                        programInfo.HasLateRegistration = true;

                        if (IsDateTimeValid(CurrentSeason.LateRegistrationStartDate, CurrentSeason.LateRegistrationStartTime))
                            programInfo.LateRegistrationOpens = GetDateTime(CurrentSeason.LateRegistrationStartDate, CurrentSeason.LateRegistrationStartTime);
                        else
                        {
                            Log($"{CurrentSeason.Index} {nameof(CurrentSeason.LateRegistrationStartDate)} is not in correct format");
                            return;
                        }

                        if (IsDateTimeValid(CurrentSeason.LateRegistrationEndDate, CurrentSeason.LateRegistrationEndTime))
                            programInfo.LateRegistrationCloses = GetDateTime(CurrentSeason.LateRegistrationEndDate, CurrentSeason.LateRegistrationEndTime);
                        else
                        {
                            Log($"{CurrentSeason.Index} {nameof(CurrentSeason.LateRegistrationEndDate)} is not in correct format");
                            return;
                        }
                    }

                    if (string.IsNullOrWhiteSpace(CurrentSeason.HasPreRegistration))
                    {
                        Log($"{CurrentSeason.Index} {nameof(CurrentSeason.HasPreRegistration)} is empty");
                        return;
                    }

                    if (CurrentSeason.HasPreRegistration.Equals("yes", StringComparison.OrdinalIgnoreCase))
                    {
                        programInfo.HasPreRegistration = true;

                        if (IsDateTimeValid(CurrentSeason.PreRegistrationStartDate, CurrentSeason.PreRegistrationStartTime))
                            programInfo.PreRegistrationOpens = GetDateTime(CurrentSeason.PreRegistrationStartDate, CurrentSeason.PreRegistrationStartTime);
                        else
                        {
                            Log($"{CurrentSeason.Index} {nameof(CurrentSeason.PreRegistrationStartDate)} is not in correct format");
                            return;
                        }

                        if (IsDateTimeValid(CurrentSeason.PreRegistrationEndDate, CurrentSeason.PreRegistrationEndTime))
                            programInfo.PreRegistrationCloses = GetDateTime(CurrentSeason.PreRegistrationEndDate, CurrentSeason.PreRegistrationEndTime);
                        else
                        {
                            Log($"{CurrentSeason.Index} {nameof(CurrentSeason.PreRegistrationStartDate)} is not in correct format");
                            return;
                        }
                    }

                    if (IsDateTimeValid(CurrentSeason.SeasonStartDate, new TimeSpan(0).ToString()))
                        programInfo.StartDate = GetDateTime(CurrentSeason.SeasonStartDate, new TimeSpan(0).ToString());
                    else
                    {
                        Log($"{CurrentSeason.Index} {nameof(CurrentSeason.SeasonStartDate)} is not in correct format");
                        return;
                    }

                    if (IsDateTimeValid(CurrentSeason.SeasonEndDate, new TimeSpan(0).ToString()))
                        programInfo.EndDate = GetDateTime(CurrentSeason.SeasonEndDate, new TimeSpan(0).ToString());
                    else
                    {
                        Log($"{CurrentSeason.Index} {nameof(CurrentSeason.SeasonEndDate)} is not in correct format");
                        return;
                    }
                    #endregion

                    if (string.IsNullOrWhiteSpace(CurrentSeason.HasLateRegistrationCharge))
                    {
                        Log($"{CurrentSeason.Index} {nameof(CurrentSeason.HasLateRegistrationCharge)} is empty");
                        return;
                    }

                    if (CurrentSeason.HasLateRegistrationCharge.Equals("yes", StringComparison.OrdinalIgnoreCase))
                    {
                        programInfo.LateRegistrationFeeMessage = CurrentSeason.LateRegistrationLabel;

                        decimal lateRegistrationFee = 0;
                        if (decimal.TryParse(CurrentSeason.LateRegistrationCharge, out lateRegistrationFee))
                            programInfo.LateRegistrationFee = lateRegistrationFee;
                        else
                        {
                            Log($"{CurrentSeason.Index} {nameof(CurrentSeason.LateRegistrationCharge)} is not in valid format");
                            return;
                        }
                    }

                    #region Additional
                    if (season.Setting.SeasonAdditionalSetting == null)
                        season.Setting.SeasonAdditionalSetting = new SeasonAdditionalSetting();

                    var additionalSettings = season.Setting.SeasonAdditionalSetting;

                    additionalSettings.IsWaitListAvailable = true;
                    additionalSettings.WaitlistPolicy = "Thank you for your interest in the Enrichment Program. If a space opens up in this class, everyone selecting notification for this class will be informed via email. Space will be filled on a first-come first-serve basis once the notification is sent out.  Payment will be required to confirm your registration. Please enter your student’s information below to be added to the waitlist.";
                    additionalSettings.ShowRemainingSpots = true;
                    additionalSettings.CapacityLeftNumbers = 3;

                    #endregion
                }

                if (action.Equals(BothAction) || action.Equals(FormsAction))
                {
                    #region RegForm
                    var regFormTemplate = GetClubFormTemplate(school);

                    if (regFormTemplate == null)
                    {
                        Log($"{CurrentSeason.Index}  regform has error");
                        return;
                    }

                    if (season.Setting.SeasonFormsSetting == null)
                        season.Setting.SeasonFormsSetting = new SeasonFormsSetting();

                    season.Setting.SeasonFormsSetting.Form = regFormTemplate.Id;

                    #endregion

                    #region Waivers
                    var clubWaivers = GetClubWaivers(school, new List<string>() { DataManipulationAddFormAndWaivers.Waiver1Name, DataManipulationAddFormAndWaivers.Waiver2Name });

                    if (!clubWaivers.Any())
                    {
                        Log($"{school.Domain} not found any waiver");
                        return;
                    }
                    else
                    {
                        if (clubWaivers.Count != 2)
                        {
                            Log($"{school.Domain} number of waivers is not 2");
                            return;
                        }

                        if (season.Setting.SeasonFormsSetting == null)
                            season.Setting.SeasonFormsSetting = new SeasonFormsSetting();

                        season.Setting.SeasonFormsSetting.Waivers = clubWaivers.Select(w => w.Id).ToList();
                    }

                    #endregion
                }

                #region Serialize
                season.MetaData.DateUpdated = DateTime.UtcNow;

                season.SettingSerialized = JsonConvert.SerializeObject(season.Setting);
                #endregion

            }
            catch
            {
                Log($"{CurrentSeason.Index} has total error");
            }
        }

        private bool IsDateTimeValid(string date, string time)
        {
            var dateTime = GetDateTime(date, time);

            if (dateTime.HasValue && dateTime == NotValidDateTime)
                return false;

            return true;
        }

        private DateTime? GetDateTime(string date, string time)
        {
            if (string.IsNullOrWhiteSpace(date))
                return null;

            DateTime result = DateTime.Now;

            if (DateTime.TryParse(date, out result))
            {

                DateTime timeResult = DateTime.UtcNow;

                if (DateTime.TryParse(time, out timeResult))
                {
                    result = result.Add(timeResult.TimeOfDay);

                    return result;
                }
                else
                {
                    return NotValidDateTime;
                }

                return result;
            }
            else
            {
                return NotValidDateTime;
            }

            return null;
        }

        private ClubFormTemplate GetClubFormTemplate(Club school)
        {
            var formTemplateName = CurrentSeason.FormTemplateName;

            if (string.IsNullOrWhiteSpace(formTemplateName))
                formTemplateName = $"{school.Name} Registration Form 2018-19";

            return school.ClubFormTemplates.SingleOrDefault(c => !c.IsDeleted && c.FormType == FormType.Registration && c.Title.Equals(formTemplateName, StringComparison.OrdinalIgnoreCase));
        }
        //private int? GetClubFormTemplateFomWinter(Club school)
        // {
        //     var winterSeason = school.Seasons.FirstOrDefault(s => s.Year == 2019 && s.Name == SeasonNames.Winter);
        //     var winterInterimSeason = school.Seasons.FirstOrDefault(s => s.Year == 2019 && s.Name == SeasonNames.WinterInterim);
        //     if(winterSeason == null && winterInterimSeason == null)
        //     {
        //         Log($"{CurrentSeason.Index}  winter and winter interim don't exist");
        //         return 0;
        //     }
        //     else if(winterSeason == null && winterInterimSeason != null)
        //     {
        //         var form = winterInterimSeason.Setting.SeasonFormsSetting.Form;
        //         if (form == null)
        //         {
        //             Log($"{CurrentSeason.Index}  Winter interim doesn't have form");
        //             return 0;
        //         }
        //         else
        //         {
        //             Log($"{CurrentSeason.Index} winter interim form: {school.ClubFormTemplates.Single(f => f.Id == form)}");
        //             return form;
        //         }
        //     }
        //     else if (winterSeason != null && winterInterimSeason == null)
        //     {
        //         var form = winterSeason.Setting.SeasonFormsSetting.Form;
        //         if (form == null)
        //         {
        //             Log($"{CurrentSeason.Index}  Winter doesn't have form");
        //             return 0;
        //         }
        //         else
        //         {
        //             Log($"{CurrentSeason.Index} winter form: {school.ClubFormTemplates.Single(f => f.Id == form)}");
        //             return form;
        //         }
        //     }
        //     else
        //     {

        //     }

        // }

        private List<ClubWaiver> GetClubWaivers(Club school, List<string> waiverNames)
        {
            return school.ClubWaivers.Where(w => waiverNames.Contains(w.Name)).ToList();
        }
    }

    public class DataManipulationCreateFamilyProfile : DataManipulationBaseModel<DataManipultaionFamilyProfileItem>
    {
        public override string Title
        {
            get
            {
                return "Create family profile";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipultaionFamilyProfileItem> CurrentFamilies { get; set; }
        public DataManipultaionFamilyProfileItem CurrentFamily { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentFamilies = SourceItems;

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentFamilies = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);

            CreateFamilies();

            FinalizeM();

            return this.Reslult;
        }

        private void CreateFamilies()
        {
            foreach (var item in CurrentFamilies)
            {
                CurrentFamily = item;
                CreateFamily();
            }
        }

        private void CreateFamily()
        {
            try
            {
                var clubDomain = CurrentFamily.ClubDomain.Trim().ToLower();

                if (HttpContext.Current.Request.IsLocal)
                {
                    clubDomain = "qaclubsand";
                }

                var club = DataContext.Set<Club>().Single(c => c.Domain.ToLower() == clubDomain.ToLower());

                #region User

                var userName = CurrentFamily.AccountEmail.Replace(" ", string.Empty).Trim().ToLower();
                if (!IsEmailValid(userName, true))
                {

                    Log($"{CurrentFamily.Index} Account email is invalid");
                    return;
                }

                var user = DataContext.Set<JbUser>().SingleOrDefault(p => p.UserName.Equals(userName, StringComparison.OrdinalIgnoreCase));
                if (user == null)
                {
                    UserManager.PasswordValidator = new PasswordValidator
                    {
                        RequiredLength = 8,
                        RequireNonLetterOrDigit = false,
                        RequireDigit = false,
                        RequireLowercase = false,
                        RequireUppercase = false,
                    };

                    var password = userName.ToLower();
                    user = new JbUser() { UserName = userName, Email = userName };
                    var res = UserManager.Create(user, password);
                }

                if (!UserManager.IsEmailConfirmed(user.Id))
                {
                    // confirm email
                    UserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));

                    string token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                    UserManager.ConfirmEmail(user.Id, token);
                }

                var family = DataContext.Set<Family>().SingleOrDefault(f => f.UserId == user.Id);
                if (family == null)
                    family = DataContext.Set<Family>().Local.SingleOrDefault(f => f.UserId == user.Id);
                if (family == null)
                    family = new Family() { UserId = user.Id };
                family.InsuranceCompanyName = CurrentFamily.CompanyName;
                family.InsurancePhone = CurrentFamily.CompanyPhone;
                family.InsurancePolicyNumber = CurrentFamily.PolicyNumber;

                if (!club.Users.Any(c => c.UserId == user.Id))
                {
                    club.Users.Add(new ClubUser() { ClubId = club.Id, UserId = user.Id });
                }

                #endregion User

                #region Participant 
                var participant = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id && u.Relationship == RelationshipType.Registrant)
                            .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentFamily.StudentFirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.StudentLastName.ToLower());

                if (participant == null)
                {
                    participant = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id && u.Relationship == RelationshipType.Registrant)
                            .FirstOrDefault(p =>
                            p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                            && p.Contact.FirstName.ToLower() == CurrentFamily.StudentFirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.StudentLastName.ToLower());
                }

                if (participant == null)
                {
                    participant = new PlayerProfile
                    {
                        Contact = new ContactPerson
                        {
                            FirstName = CurrentFamily.StudentFirstName,
                            LastName = CurrentFamily.StudentLastName
                        },

                        Relationship = RelationshipType.Registrant,
                        Status = PlayerProfileStatusType.Active,
                        UserId = user.Id,
                    };

                    DataContext.Set<PlayerProfile>().Add(participant);
                }

                if (!string.IsNullOrWhiteSpace(CurrentFamily.StudentDOB))
                {
                    try
                    {
                        participant.Contact.DoB = DateTime.Parse(CurrentFamily.StudentDOB);
                    }
                    catch
                    {

                        Log($"{CurrentFamily.Index} DOB is invalid");
                        return;
                    }
                }
                var studentEmail = CurrentFamily.StudentEmail.Replace(" ", string.Empty).Trim().ToLower();
                if (IsEmailValid(studentEmail, false))
                {
                    participant.Contact.Email = studentEmail;
                }
                else
                {

                    Log("Student email is invalid");
                    return;
                }

                participant.Gender = FormatGender(CurrentFamily.StudentGender);

                if (participant.Info == null)
                {
                    participant.Info = new PlayerInfo();
                }
                participant.Info.HasAllergies = FormatBoolNullable(CurrentFamily.HasAllergiesAndDietaryRestrictions);
                participant.Info.Allergies = CurrentFamily.AllergiesAndDietaryRestrictions;
                participant.Info.DoctorLocation = CurrentFamily.DoctorLocation;
                participant.Info.DoctorName = CurrentFamily.DoctorName;
                participant.Info.DoctorPhone = CurrentFamily.DoctorPhone;
                participant.Info.Grade = FormatGrade(CurrentFamily.StudentGrade);
                participant.Info.HasPermissionPhotography = FormatBoolNullable(CurrentFamily.PhotographyVideoReleasePermission);
                participant.Info.HomeRoom = CurrentFamily.Homeroom;
                participant.Info.NutAllergy = FormatBoolNullable(CurrentFamily.HasNutAllergy);
                participant.Info.NutAllergyDescription = CurrentFamily.NutAllergyDescription;
                participant.Info.SchoolName = CurrentFamily.SchoolName;
                participant.Info.SelfAdministerMedication = FormatBoolNullable(CurrentFamily.SelfAdministeredMedication);
                participant.Info.SelfAdministerMedicationDescription = CurrentFamily.SelfAdministeredMedicationDescription;
                participant.Info.HasSpecialNeeds = FormatBoolNullable(CurrentFamily.HasMedicalConditionsAndSpecialNeeds);
                participant.Info.SpecialNeeds = CurrentFamily.MedicalConditionsAndSpecialNeeds;

                #endregion Participant

                #region Parent1
                if (!string.IsNullOrEmpty(CurrentFamily.Parent1FirstName) && !string.IsNullOrEmpty(CurrentFamily.Parent1LastName))
                {
                    var parent1 = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id && u.Relationship == RelationshipType.Parent)
                            .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentFamily.Parent1FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.Parent1LastName.ToLower());
                    if (parent1 == null)
                    {
                        parent1 = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id && u.Relationship == RelationshipType.Parent)
                            .FirstOrDefault(p =>
                                             p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                                            && p.Contact.FirstName.ToLower() == CurrentFamily.Parent1FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.Parent1LastName.ToLower());
                    }
                    if (parent1 == null)
                    {
                        parent1 = new PlayerProfile
                        {
                            Contact = new ContactPerson
                            {
                                FirstName = CurrentFamily.Parent1FirstName,
                                LastName = CurrentFamily.Parent1LastName
                            },

                            Relationship = RelationshipType.Parent,
                            Status = PlayerProfileStatusType.Active,
                            UserId = user.Id,
                        };

                        DataContext.Set<PlayerProfile>().Add(parent1);
                    }

                    if (!string.IsNullOrEmpty(CurrentFamily.Parent1DOB))
                    {
                        try
                        {
                            parent1.Contact.DoB = Convert.ToDateTime(CurrentFamily.Parent1DOB);
                        }
                        catch
                        {

                            Log("Parent1 DOB is invalid");
                            return;
                        }
                    }

                    if (!string.IsNullOrEmpty(CurrentFamily.Parent1AddressLine1) || !string.IsNullOrEmpty(CurrentFamily.Parent1AddressLine2) || !string.IsNullOrEmpty(CurrentFamily.Parent1City) || !string.IsNullOrEmpty(CurrentFamily.Parent1State) || !string.IsNullOrEmpty(CurrentFamily.Parent1ZipCode))
                    {
                        if (parent1.Contact.Address == null)
                            parent1.Contact.Address = new PostalAddress();

                        var parent1Address = parent1.Contact.Address;

                        parent1Address.City = CurrentFamily.Parent1City;
                        parent1Address.State = CurrentFamily.Parent1State;
                        parent1Address.Street = CurrentFamily.Parent1AddressLine1;
                        parent1Address.Street2 = CurrentFamily.Parent1AddressLine2;
                        parent1Address.Zip = CurrentFamily.Parent1ZipCode;
                        parent1Address.Country = CurrentFamily.Parent1Country;

                        parent1Address.Address = string.Format("{0}, {1}, {2}, {3} {4}", string.Join(" ", new List<string>() { parent1Address.Street, parent1Address.Street2 }), parent1Address.City, parent1Address.State, parent1Address.Country, parent1Address.Zip);
                        parent1Address.AutoCompletedAddress = parent1Address.Address;
                    }

                    parent1.Contact.Employer = CurrentFamily.Parent1Employer;
                    parent1.Contact.Occupation = CurrentFamily.Parent1Occupation;
                    var Parent1Email = CurrentFamily.Parent1Email.Replace(" ", string.Empty).Trim().ToLower();
                    if (IsEmailValid(Parent1Email, false))
                    {
                        parent1.Contact.Email = Parent1Email;
                    }
                    else
                    {

                        Log("Parent1 email is invalid");
                        return;
                    }
                    if (!string.IsNullOrWhiteSpace(CurrentFamily.Parent1MobilePhoneAlternatePhone))
                        parent1.Contact.Cell = FormatPhone(CurrentFamily.Parent1MobilePhoneAlternatePhone);

                    if (!string.IsNullOrWhiteSpace(CurrentFamily.Parent1HomePhonePrimaryPhone))
                        parent1.Contact.Phone = FormatPhone(CurrentFamily.Parent1HomePhonePrimaryPhone);

                    if (!string.IsNullOrWhiteSpace(CurrentFamily.Parent1WorkPhone))
                        parent1.Contact.Work = FormatPhone(CurrentFamily.Parent1WorkPhone);


                    parent1.Contact.Relationship = FormatParentRelationship(CurrentFamily.Parent1Relationship);
                    parent1.Gender = FormatGender(CurrentFamily.Parent1Gender);
                }
                #endregion Parent1

                #region Parent2
                if (!string.IsNullOrEmpty(CurrentFamily.Parent2FirstName) && !string.IsNullOrEmpty(CurrentFamily.Parent2LastName))
                {
                    var parent2 = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id && u.Relationship == RelationshipType.Parent)
                            .FirstOrDefault(p =>
                                            p.Contact.FirstName.ToLower() == CurrentFamily.Parent2FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.Parent2LastName.ToLower());
                    if (parent2 == null)
                    {
                        parent2 = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id && u.Relationship == RelationshipType.Parent)
                            .FirstOrDefault(p =>
                                             p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                                             && p.Contact.FirstName.ToLower() == CurrentFamily.Parent2FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.Parent2LastName.ToLower());
                    }
                    if (parent2 == null)
                    {
                        parent2 = new PlayerProfile
                        {
                            Contact = new ContactPerson
                            {
                                FirstName = CurrentFamily.Parent2FirstName,
                                LastName = CurrentFamily.Parent2LastName
                            },

                            Relationship = RelationshipType.Parent,
                            Status = PlayerProfileStatusType.Active,
                            UserId = user.Id,
                        };

                        DataContext.Set<PlayerProfile>().Add(parent2);

                    }

                    if (!string.IsNullOrEmpty(CurrentFamily.Parent2DOB))
                    {
                        try
                        {
                            parent2.Contact.DoB = Convert.ToDateTime(CurrentFamily.Parent2DOB);
                        }
                        catch
                        {

                            Log("Parent2 DOB is invalid");
                            return;
                        }
                    }



                    if (!string.IsNullOrEmpty(CurrentFamily.Parent2AddressLine1) || !string.IsNullOrEmpty(CurrentFamily.Parent2AddressLine2) || !string.IsNullOrEmpty(CurrentFamily.Parent2City) || !string.IsNullOrEmpty(CurrentFamily.Parent2State) || !string.IsNullOrEmpty(CurrentFamily.Parent2ZipCode))
                    {

                        if (parent2.Contact.Address == null)
                            parent2.Contact.Address = new PostalAddress();

                        var parent2Address = parent2.Contact.Address;

                        parent2Address.City = CurrentFamily.Parent2City;
                        parent2Address.State = CurrentFamily.Parent2State;
                        parent2Address.Street = CurrentFamily.Parent2AddressLine1;
                        parent2Address.Street2 = CurrentFamily.Parent2AddressLine2;
                        parent2Address.Zip = CurrentFamily.Parent2ZipCode;
                        parent2Address.Country = CurrentFamily.Parent2Country;

                        parent2Address.Address = string.Format("{0}, {1}, {2}, {3} {4}", string.Join(" ", new List<string>() { parent2Address.Street, parent2Address.Street2 }), parent2Address.City, parent2Address.State, parent2Address.Country, parent2Address.Zip);
                        parent2Address.AutoCompletedAddress = parent2Address.Address;

                    }

                    parent2.Contact.Employer = CurrentFamily.Parent2Employer;
                    parent2.Contact.Occupation = CurrentFamily.Parent2Occupation;
                    var Parent2Email = CurrentFamily.Parent2Email.Replace(" ", string.Empty).Trim().ToLower();
                    if (IsEmailValid(Parent2Email, false))
                    {
                        parent2.Contact.Email = Parent2Email;
                    }
                    else
                    {

                        Log($"{CurrentFamily.Index} Parent2 email is invalid");
                        return;
                    }
                    if (!string.IsNullOrWhiteSpace(CurrentFamily.Parent2MobilePhoneAlternatePhone))
                        parent2.Contact.Cell = FormatPhone(CurrentFamily.Parent2MobilePhoneAlternatePhone);

                    if (!string.IsNullOrWhiteSpace(CurrentFamily.Parent2HomePhonePrimaryPhone))
                        parent2.Contact.Phone = FormatPhone(CurrentFamily.Parent2HomePhonePrimaryPhone);

                    if (!string.IsNullOrWhiteSpace(CurrentFamily.Parent2WorkPhone))
                        parent2.Contact.Work = FormatPhone(CurrentFamily.Parent2WorkPhone);

                    parent2.Contact.Relationship = FormatParentRelationship(CurrentFamily.Parent2Relationship);
                    parent2.Gender = FormatGender(CurrentFamily.Parent2Gender);

                }
                #endregion Parent2

                #region Emergency1
                if (!string.IsNullOrEmpty(CurrentFamily.EmergencyContact1FirstName) && !string.IsNullOrEmpty(CurrentFamily.EmergencyContact1LastName))
                {
                    var Emergency1 = DataContext.Set<FamilyContact>().Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.Emergency)
                            .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentFamily.EmergencyContact1FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.EmergencyContact1LastName.ToLower());
                    if (Emergency1 == null)
                    {
                        Emergency1 = DataContext.Set<FamilyContact>().Local.Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.Emergency)
                            .FirstOrDefault(p =>
                               p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                               && p.Contact.FirstName.ToLower() == CurrentFamily.EmergencyContact1FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.EmergencyContact1LastName.ToLower());
                    }
                    if (Emergency1 == null)
                    {
                        Emergency1 = new FamilyContact
                        {
                            Contact = new ContactPerson
                            {
                                FirstName = CurrentFamily.EmergencyContact1FirstName,
                                LastName = CurrentFamily.EmergencyContact1LastName
                            },

                            Status = FamilyContactStatusType.Active,
                            Type = FamilyContactType.Emergency,
                            Family = family
                        };

                        DataContext.Set<FamilyContact>().Add(Emergency1);

                    }

                    var Emergency1Email = CurrentFamily.EmergencyContact1Email.Replace(" ", string.Empty).Trim().ToLower();
                    if (IsEmailValid(Emergency1Email, false))
                    {
                        Emergency1.Contact.Email = Emergency1Email;
                    }
                    else
                    {

                        Log("Emergency1 email is invalid");
                        return;
                    }

                    Emergency1.Contact.Phone = FormatPhone(CurrentFamily.EmergencyContact1PrimaryPhone);
                    Emergency1.Contact.Cell = FormatPhone(CurrentFamily.EmergencyContact1AlternatePhone);
                    Emergency1.Relationship = FormatRelationship(CurrentFamily.EmergencyContact1Relationship);
                }
                #endregion Emergency1

                #region Emergency2
                if (!string.IsNullOrEmpty(CurrentFamily.EmergencyContact2FirstName) && !string.IsNullOrEmpty(CurrentFamily.EmergencyContact2LastName))
                {
                    var Emergency2 = DataContext.Set<FamilyContact>().Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.Emergency)
                            .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentFamily.EmergencyContact2FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.EmergencyContact2LastName.ToLower());
                    if (Emergency2 == null)
                    {
                        Emergency2 = DataContext.Set<FamilyContact>().Local.Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.Emergency)
                            .FirstOrDefault(p =>
                                             p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                                             && p.Contact.FirstName.ToLower() == CurrentFamily.EmergencyContact2FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.EmergencyContact2LastName.ToLower());
                    }

                    if (Emergency2 == null)
                    {
                        Emergency2 = new FamilyContact
                        {
                            Contact = new ContactPerson
                            {
                                FirstName = CurrentFamily.EmergencyContact2FirstName,
                                LastName = CurrentFamily.EmergencyContact2LastName
                            },

                            Status = FamilyContactStatusType.Active,
                            Type = FamilyContactType.Emergency,
                            Family = family
                        };

                        DataContext.Set<FamilyContact>().Add(Emergency2);
                    }

                    var Emergency2Email = CurrentFamily.EmergencyContact2Email.Replace(" ", string.Empty).Trim().ToLower();
                    if (IsEmailValid(Emergency2Email, false))
                    {
                        Emergency2.Contact.Email = Emergency2Email;
                    }
                    else
                    {

                        Log("Emergency2 email is invalid");
                        return;
                    }

                    Emergency2.Contact.Phone = FormatPhone(CurrentFamily.EmergencyContact2PrimaryPhone);
                    Emergency2.Contact.Cell = FormatPhone(CurrentFamily.EmergencyContact2AlternatePhone);
                    Emergency2.Relationship = FormatRelationship(CurrentFamily.EmergencyContact2Relationship);
                }
                #endregion Emergency2

                #region Authorized pick up1
                if (!string.IsNullOrEmpty(CurrentFamily.AuthorizedPickUp1FirstName) && !string.IsNullOrEmpty(CurrentFamily.AuthorizedPickUp1LastName))
                {
                    var Authorized1 = DataContext.Set<FamilyContact>().Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.AuthorizedPickup)
                            .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentFamily.AuthorizedPickUp1FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.AuthorizedPickUp1LastName.ToLower());
                    if (Authorized1 == null)
                    {
                        Authorized1 = DataContext.Set<FamilyContact>().Local.Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.AuthorizedPickup)
                            .FirstOrDefault(p =>
                                             p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                                             && p.Contact.FirstName.ToLower() == CurrentFamily.AuthorizedPickUp1FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.AuthorizedPickUp1LastName.ToLower());
                    }
                    if (Authorized1 == null)
                    {
                        Authorized1 = new FamilyContact
                        {
                            Contact = new ContactPerson
                            {
                                FirstName = CurrentFamily.AuthorizedPickUp1FirstName,
                                LastName = CurrentFamily.AuthorizedPickUp1LastName,
                            },

                            Status = FamilyContactStatusType.Active,
                            Type = FamilyContactType.AuthorizedPickup,
                            Family = family
                        };

                        DataContext.Set<FamilyContact>().Add(Authorized1);

                    }

                    Authorized1.Contact.Phone = FormatPhone(CurrentFamily.AuthorizedPickUp1PrimaryPhone);
                    Authorized1.Contact.Cell = FormatPhone(CurrentFamily.AuthorizedPickUp1AlternatePhone);
                    Authorized1.Relationship = FormatRelationship(CurrentFamily.AuthorizedPickUp1Relationship);
                    Authorized1.IsEmergencyContact = FormatBool(CurrentFamily.AuthorizedPickUp1IsEmergencyContact);
                    var Authorized1Email = CurrentFamily.AuthorizedPickUp1Email.Replace(" ", string.Empty).Trim().ToLower();
                    if (IsEmailValid(Authorized1Email, false))
                    {
                        Authorized1.Contact.Email = Authorized1Email;
                    }
                    else
                    {

                        Log("Authorized1 email is invalid");
                        return;
                    }
                }
                #endregion Authorized pick up1

                #region Authorized pick up2
                if (!string.IsNullOrEmpty(CurrentFamily.AuthorizedPickUp2FirstName) && !string.IsNullOrEmpty(CurrentFamily.AuthorizedPickUp2LastName))
                {
                    var Authorized2 = DataContext.Set<FamilyContact>().Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.AuthorizedPickup)
                            .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentFamily.AuthorizedPickUp2FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.AuthorizedPickUp2LastName.ToLower());
                    if (Authorized2 == null)
                    {
                        Authorized2 = DataContext.Set<FamilyContact>().Local.Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.AuthorizedPickup)
                            .FirstOrDefault(p =>
                                             p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                                             && p.Contact.FirstName.ToLower() == CurrentFamily.AuthorizedPickUp2FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.AuthorizedPickUp2LastName.ToLower());
                    }
                    if (Authorized2 == null)
                    {
                        Authorized2 = new FamilyContact
                        {
                            Contact = new ContactPerson
                            {
                                FirstName = CurrentFamily.AuthorizedPickUp2FirstName,
                                LastName = CurrentFamily.AuthorizedPickUp2LastName,
                            },

                            Status = FamilyContactStatusType.Active,
                            Type = FamilyContactType.AuthorizedPickup,
                            Family = family
                        };

                        DataContext.Set<FamilyContact>().Add(Authorized2);

                    }

                    Authorized2.Contact.Phone = FormatPhone(CurrentFamily.AuthorizedPickUp2PrimaryPhone);
                    Authorized2.Contact.Cell = FormatPhone(CurrentFamily.AuthorizedPickUp2AlternatePhone);
                    Authorized2.Relationship = FormatRelationship(CurrentFamily.AuthorizedPickUp2Relationship);
                    Authorized2.IsEmergencyContact = FormatBool(CurrentFamily.AuthorizedPickUp2IsEmergencyContact);
                    var Authorized2Email = CurrentFamily.AuthorizedPickUp2Email.Replace(" ", string.Empty).Trim().ToLower();
                    if (IsEmailValid(Authorized2Email, false))
                    {
                        Authorized2.Contact.Email = Authorized2Email;
                    }
                    else
                    {

                        Log($"{CurrentFamily.Index} Authorized2 email is invalid");
                        return;
                    }
                }
                #endregion Authorized pick up2

                #region Authorized pick up3
                if (!string.IsNullOrEmpty(CurrentFamily.AuthorizedPickUp3FirstName) && !string.IsNullOrEmpty(CurrentFamily.AuthorizedPickUp3LastName))
                {
                    var Authorized3 = DataContext.Set<FamilyContact>().Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.AuthorizedPickup)
                            .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentFamily.AuthorizedPickUp3FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.AuthorizedPickUp3LastName.ToLower());
                    if (Authorized3 == null)
                    {
                        Authorized3 = DataContext.Set<FamilyContact>().Local.Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.AuthorizedPickup)
                            .FirstOrDefault(p =>
                                             p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                                             && p.Contact.FirstName.ToLower() == CurrentFamily.AuthorizedPickUp3FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.AuthorizedPickUp3LastName.ToLower());
                    }
                    if (Authorized3 == null)
                    {
                        Authorized3 = new FamilyContact
                        {
                            Contact = new ContactPerson
                            {
                                FirstName = CurrentFamily.AuthorizedPickUp3FirstName,
                                LastName = CurrentFamily.AuthorizedPickUp3LastName,
                            },

                            Status = FamilyContactStatusType.Active,
                            Type = FamilyContactType.AuthorizedPickup,
                            Family = family
                        };

                        DataContext.Set<FamilyContact>().Add(Authorized3);

                    }

                    Authorized3.Contact.Phone = FormatPhone(CurrentFamily.AuthorizedPickUp3PrimaryPhone);
                    Authorized3.Contact.Cell = FormatPhone(CurrentFamily.AuthorizedPickUp3AlternatePhone);
                    Authorized3.Relationship = FormatRelationship(CurrentFamily.AuthorizedPickUp3Relationship);
                    Authorized3.IsEmergencyContact = FormatBool(CurrentFamily.AuthorizedPickUp3IsEmergencyContact);
                    var Authorized3Email = CurrentFamily.AuthorizedPickUp3Email.Replace(" ", string.Empty).Trim().ToLower();
                    if (IsEmailValid(Authorized3Email, false))
                    {
                        Authorized3.Contact.Email = Authorized3Email;
                    }
                    else
                    {

                        Log("Authorized3 email is invalid");
                        return;
                    }
                }
                #endregion Authorized pick up3

                #region Authorized pick up4
                if (!string.IsNullOrEmpty(CurrentFamily.AuthorizedPickUp4FirstName) && !string.IsNullOrEmpty(CurrentFamily.AuthorizedPickUp4LastName))
                {
                    var Authorized4 = DataContext.Set<FamilyContact>().Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.AuthorizedPickup)
                            .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentFamily.AuthorizedPickUp4FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.AuthorizedPickUp4LastName.ToLower());
                    if (Authorized4 == null)
                    {
                        Authorized4 = DataContext.Set<FamilyContact>().Local.Where(f => f.Family.UserId == user.Id && f.Type == FamilyContactType.AuthorizedPickup)
                            .FirstOrDefault(p =>
                                             p.Contact != null && !string.IsNullOrWhiteSpace(p.Contact.FirstName) && !string.IsNullOrWhiteSpace(p.Contact.LastName)
                                             && p.Contact.FirstName.ToLower() == CurrentFamily.AuthorizedPickUp4FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentFamily.AuthorizedPickUp4LastName.ToLower());
                    }
                    if (Authorized4 == null)
                    {
                        Authorized4 = new FamilyContact
                        {
                            Contact = new ContactPerson
                            {
                                FirstName = CurrentFamily.AuthorizedPickUp4FirstName,
                                LastName = CurrentFamily.AuthorizedPickUp4LastName,
                            },

                            Status = FamilyContactStatusType.Active,
                            Type = FamilyContactType.AuthorizedPickup,
                            Family = family
                        };

                        DataContext.Set<FamilyContact>().Add(Authorized4);

                    }

                    Authorized4.Contact.Phone = FormatPhone(CurrentFamily.AuthorizedPickUp4PrimaryPhone);
                    Authorized4.Contact.Cell = FormatPhone(CurrentFamily.AuthorizedPickUp4AlternatePhone);
                    Authorized4.Relationship = FormatRelationship(CurrentFamily.AuthorizedPickUp4Relationship);
                    Authorized4.IsEmergencyContact = FormatBool(CurrentFamily.AuthorizedPickUp4IsEmergencyContact);
                    var Authorized4Email = CurrentFamily.AuthorizedPickUp4Email.Replace(" ", string.Empty).Trim().ToLower();
                    if (IsEmailValid(Authorized4Email, false))
                    {
                        Authorized4.Contact.Email = Authorized4Email;
                    }
                    else
                    {

                        Log($"{CurrentFamily.Index} Authorized4 email is invalid");
                        return;
                    }
                }
                #endregion Authorized pick up4

            }
            catch
            {
                Log($"{CurrentFamily.Index} has total error");
            }
        }

        private SchoolGradeType FormatGrade(string grade)
        {
            SchoolGradeType result = (SchoolGradeType)0;
            grade = grade.ToLower();
            if (grade == "not in school")
            {
                result = SchoolGradeType.NotInSchool;
            }
            else if (grade == "preschool - 3")
            {
                result = SchoolGradeType.PreSchool3;
            }
            else if (grade == "preschool - 4")
            {
                result = SchoolGradeType.PreSchool4;
            }
            else if (grade == "preschool")
            {
                result = SchoolGradeType.PreSchool;
            }
            else if (grade == "kindergarten")
            {
                result = SchoolGradeType.Kindergarten;
            }
            else if (grade == "1")
            {
                result = SchoolGradeType.S1;
            }
            else if (grade == "2")
            {
                result = SchoolGradeType.S2;
            }
            else if (grade == "3")
            {
                result = SchoolGradeType.S3;
            }
            else if (grade == "4")
            {
                result = SchoolGradeType.S4;
            }
            else if (grade == "5")
            {
                result = SchoolGradeType.S5;
            }
            else if (grade == "6")
            {
                result = SchoolGradeType.S6;
            }
            else if (grade == "7")
            {
                result = SchoolGradeType.S7;
            }
            else if (grade == "8")
            {
                result = SchoolGradeType.S8;
            }
            else if (grade == "9")
            {
                result = SchoolGradeType.S9;
            }
            else if (grade == "10")
            {
                result = SchoolGradeType.S9;
            }
            else if (grade == "college")
            {
                result = SchoolGradeType.College;
            }

            return result;
        }
        private GenderCategories FormatGender(string gender)
        {
            var result = GenderCategories.None;
            gender = gender.ToLower();
            if (gender == "male" || gender == "m")
            {
                result = GenderCategories.Male;
            }
            else if (gender == "female" || gender == "f")
            {
                result = GenderCategories.Female;
            }

            return result;
        }
        private bool? FormatBoolNullable(string boolvalue)
        {
            boolvalue = boolvalue.ToLower();
            bool? result = null;

            if (boolvalue == "yes")
            {
                result = true;
            }
            else if (boolvalue == "no")
            {
                result = false;
            }

            return result;
        }
        private bool FormatBool(string boolvalue)
        {
            if (boolvalue != null)
            {
                boolvalue = boolvalue.ToLower();
            }

            var result = false;

            if (boolvalue == "yes")
            {
                result = true;
            }

            return result;
        }
        private Relationship? FormatRelationship(string relationship)
        {
            Relationship? result = null;
            relationship = relationship.ToLower();
            if (relationship == "brother")
            {
                result = Relationship.Brother;
            }
            else if (relationship == "father")
            {
                result = Relationship.Father;
            }
            else if (relationship == "friendneighbor")
            {
                result = Relationship.FriendNeighbor;
            }
            else if (relationship == "gaurdian")
            {
                result = Relationship.Gaurdian;
            }
            else if (relationship == "grandfather")
            {
                result = Relationship.Grandfather;
            }
            else if (relationship == "grandmother")
            {
                result = Relationship.Grandmother;
            }
            else if (relationship == "mother")
            {
                result = Relationship.Mother;
            }
            else if (relationship == "nannyaupair")
            {
                result = Relationship.Nannyaupair;
            }
            else if (relationship == "other")
            {
                result = Relationship.Other;
            }
            else if (relationship == "sister")
            {
                result = Relationship.Sister;
            }
            return result;
        }
        private ParentRelationship? FormatParentRelationship(string relationship)
        {
            ParentRelationship? result = null;
            relationship = relationship.ToLower();
            if (relationship == "father")
            {
                result = ParentRelationship.Father;
            }
            else if (relationship == "grandfather")
            {
                result = ParentRelationship.Grandfather;
            }
            else if (relationship == "grandmother")
            {
                result = ParentRelationship.Grandmother;
            }
            else if (relationship == "gaurdian")
            {
                result = ParentRelationship.Guardian;
            }
            else if (relationship == "mother")
            {
                result = ParentRelationship.Mother;
            }
            else if (relationship == "other")
            {
                result = ParentRelationship.Other;
            }

            return result;
        }
        private string FormatPhone(string phoneNumber)
        {
            return phoneNumber.Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "").Replace(".", "").Trim();
        }
        private bool IsEmailValid(string email, bool mandatory)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                if (mandatory)
                {
                    return false;
                }
                if (!mandatory)
                {
                    return true;
                }
            }
            return Regex.IsMatch(email, Jumbula.Common.Constants.Constants.EmailRegex);
        }

    }


    public class DataManipulationAddFormAndWaivers : DataManipulationBaseModel<DataManipulationAddFormAndWaiverItem>
    {
        public override string Title
        {
            get { return "Add forms and waivers"; }
        }

        public override int? NumbersInCycle
        {
            get { return 100; }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationAddFormAndWaiverItem> CurrentSchools { get; set; }
        public DataManipulationAddFormAndWaiverItem CurrentSchool { get; set; }

        public const string SourceClubDomainOfWaiver = "flexacademieshillside";

        public ClubWaiver SourceWaiver1 { get; set; }
        public ClubWaiver SourceWaiver2 { get; set; }

        public static string Waiver1Name = "Permission and Waiver Agreement";
        public static string Waiver2Name = "Parent Responsibilities";

        private const string FlexDomain = "flexacademies";

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentSchools = SourceItems;

            var numberOfSchools = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentSchools = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfSchools);

            UpdateSchools();

            FinalizeM();

            return this.Reslult;
        }

        private void UpdateSchools()
        {

            SourceWaiver1 = DataContext.Set<ClubWaiver>().SingleOrDefault(s => !s.IsDeleted && s.Club.Domain.Equals(SourceClubDomainOfWaiver) && s.Name.Equals(Waiver1Name, StringComparison.OrdinalIgnoreCase));

            if (SourceWaiver1 == null)
            {
                Log($"Can't find source waiver 1");
            }

            SourceWaiver2 = DataContext.Set<ClubWaiver>().SingleOrDefault(s => !s.IsDeleted && s.Club.Domain.Equals(SourceClubDomainOfWaiver) && s.Name.Equals(Waiver2Name, StringComparison.OrdinalIgnoreCase));

            if (SourceWaiver2 == null)
            {
                Log($"Can't find source waiver 2");
            }

            foreach (var item in CurrentSchools)
            {
                CurrentSchool = item;
                UpdateSchool();
            }
        }

        private void UpdateSchool()
        {
            try
            {
                var school = DataContext.Set<Club>().SingleOrDefault(c =>
                    !c.IsDeleted && c.Domain.Equals(CurrentSchool.SchoolDomain, StringComparison.OrdinalIgnoreCase) &&
                    c.PartnerId.HasValue &&
                    c.PartnerClub.Domain.Equals(CurrentSchool.PartnerDomain, StringComparison.OrdinalIgnoreCase));

                if (school == null)
                {
                    Log($"{CurrentSchool.Index} school doesn't exist");
                    return;
                }

                var formTemplate = ManipulateRegForm(school);

                if (formTemplate == null)
                {
                    Log($"{CurrentSchool.Index} form has error");
                }

                if (!string.IsNullOrWhiteSpace(CurrentSchool.PartnerDomain) && CurrentSchool.PartnerDomain.Equals(FlexDomain))
                {
                    var waiver1 = ManipulateWaiver(school, Waiver1Name);

                    if (waiver1 == null)
                    {
                        Log($"{CurrentSchool.Index} waiver {Waiver1Name} has error");
                    }

                    var waiver2 = ManipulateWaiver(school, Waiver2Name);

                    if (waiver2 == null)
                    {
                        Log($"{CurrentSchool.Index} waiver {Waiver2Name} has error");
                    }
                }
            }
            catch
            {
                Log($"{CurrentSchool.Index} has total error");
            }
        }

        private ClubFormTemplate ManipulateRegForm(Club school)
        {
            ClubFormTemplate result = null;
            try
            {
                var formTemplateName = CurrentSchool.FormTemplateName;

                if (string.IsNullOrWhiteSpace(formTemplateName))
                    formTemplateName = $"{school.Name} Registration Form 2018-19";

                var schoolId = school.Id;
                var clubFormTemplates = DataContext.Set<ClubFormTemplate>().Where(c =>
                    c.ClubId == schoolId && !c.IsDeleted && c.FormType == FormType.Registration).ToList();

                if (!clubFormTemplates.Any(c =>
                    !c.IsDeleted && c.FormType == FormType.Registration &&
                    c.Title.Equals(formTemplateName, StringComparison.OrdinalIgnoreCase)))
                {
                    var sourceFormTemplate = DataContext.Set<ClubFormTemplate>().SingleOrDefault(c =>
                        !c.IsDeleted && c.FormType == FormType.Registration &&
                        c.Title.Equals(CurrentSchool.SourceFormName, StringComparison.OrdinalIgnoreCase) &&
                        c.Club.Domain.Equals(CurrentSchool.SourceFormClub, StringComparison.OrdinalIgnoreCase));

                    if (sourceFormTemplate == null)
                    {
                        Log("Source form doesn't exist");
                        return null;
                    }

                    result = new ClubFormTemplate()
                    {
                        DateCreated = DateTime.UtcNow,
                        DefaultFormType = sourceFormTemplate.DefaultFormType,
                        FollowUpFormMode = sourceFormTemplate.FollowUpFormMode,
                        FormType = FormType.Registration,
                        Title = formTemplateName,
                        Club = school,
                        JbForm = new JbForm()
                        {
                            AccessRole = sourceFormTemplate.JbForm.AccessRole,
                            CreatedDate = DateTime.UtcNow,
                            CurrentMode = sourceFormTemplate.JbForm.CurrentMode,
                            HelpText = sourceFormTemplate.JbForm.HelpText,
                            LastModifiedDate = DateTime.UtcNow,
                            RefEntityId = sourceFormTemplate.JbForm.RefEntityId,
                            RefEntityName = sourceFormTemplate.JbForm.RefEntityName,
                            Title = sourceFormTemplate.JbForm.Title,
                            VisibleMode = sourceFormTemplate.JbForm.VisibleMode,
                            JsonElements = sourceFormTemplate.JbForm.JsonElements,
                        }
                    };

                    DataContext.Set<ClubFormTemplate>().Add(result);
                }
                else
                {
                    var formTemplate = clubFormTemplates.SingleOrDefault(c =>
                        !c.IsDeleted && c.FormType == FormType.Registration &&
                        c.Title.Equals(formTemplateName, StringComparison.OrdinalIgnoreCase));

                    return formTemplate;
                }
            }
            catch (Exception ex)
            {
                Log($"{school.Domain} form creation has total error {ex.Message}");
                return null;
            }

            return result;
        }

        private ClubWaiver ManipulateWaiver(Club school, string waiverName)
        {
            ClubWaiver result = null;
            try
            {
                var clubWaivers = DataContext.Set<ClubWaiver>().Where(c =>
                          c.Club_Id == school.Id && !c.IsDeleted && !c.IsDeleted && c.Name.Equals(waiverName, StringComparison.OrdinalIgnoreCase)).ToList();

                if (clubWaivers.Any())
                {
                    if (clubWaivers.Count > 1)
                    {
                        Log($"{school.Domain} there is more than one waiver exist with name {waiverName}");
                        return clubWaivers.First();
                    }

                    Log($"{school.Domain} already has waiver: {waiverName}");
                    result = clubWaivers.Single();

                    result.Text = waiverName == Waiver1Name ? SourceWaiver1.Text : SourceWaiver2.Text;
                }
                else
                {
                    result = new ClubWaiver()
                    {
                        Name = waiverName,
                        Club_Id = school.Id,
                        DateCreated = DateTime.UtcNow,
                        IsRequired = true,
                        Text = waiverName == Waiver1Name ? SourceWaiver1.Text : SourceWaiver2.Text,
                    };

                    DataContext.Set<ClubWaiver>().Add(result);
                }

            }
            catch (Exception ex)
            {
                Log($"{school.Domain} exception: {ex.Message}");
                return null;
            }


            return result;
        }
    }

    public class DataManipulationUpdateSchoolHoliday : DataManipulationBaseModel<DataManipultaionClubHolidaysItem>
    {
        public override string Title
        {
            get
            {
                return "Update shool holidays";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipultaionClubHolidaysItem> CurrentSchools { get; set; }
        public DataManipultaionClubHolidaysItem CurrentSchool { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentSchools = SourceItems;

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentSchools = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);

            ManipulateSchools();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateSchools()
        {
            foreach (var item in CurrentSchools)
            {
                CurrentSchool = item;
                ManipulateSchool();
            }
        }

        private void ManipulateSchool()
        {
            try
            {
                var school = DataContext.Set<Club>().SingleOrDefault(c => !c.IsDeleted && c.Domain.Equals(CurrentSchool.SchoolDomain, StringComparison.OrdinalIgnoreCase) && c.PartnerId.HasValue && c.PartnerClub.Domain.Equals(CurrentSchool.PartnerDomain, StringComparison.OrdinalIgnoreCase));

                if (school == null)
                {
                    Log($"{CurrentSchool.Index} school doesn't exist");
                    return;
                }

                if (school.Setting == null)
                    school.Setting = new ClubSetting();

                var schoolSetting = school.Setting;

                var amResult = new List<string>();
                if (GetDates(CurrentSchool.AM, nameof(CurrentSchool.AM), ref amResult))
                    schoolSetting.ListOfHolidayAMDates = amResult;


                var pmResult = new List<string>();
                if (GetDates(CurrentSchool.PM, nameof(CurrentSchool.PM), ref pmResult))
                    schoolSetting.ListOfHolidayPMDates = pmResult;


                var amPmResult = new List<string>();
                if (GetDates(CurrentSchool.AMPM, nameof(CurrentSchool.AMPM), ref amPmResult))
                    schoolSetting.ListOfHolidayDates = amPmResult;

                #region Serialize
                school.SettingSerialized = JsonConvert.SerializeObject(schoolSetting);
                #endregion

            }
            catch
            {
                Log($"{CurrentSchool.Index} has total error");
            }
        }

        private bool GetDates(string sourceDates, string holidayMode, ref List<string> result)
        {
            try
            {
                var amDates = sourceDates.Split(',');

                foreach (var item in amDates)
                {
                    DateTime date;

                    if (string.IsNullOrWhiteSpace(item))
                    {
                        Log($"{CurrentSchool.Index} ({holidayMode}: {item}) date is empty");
                        return false;
                    }

                    if (DateTime.TryParse(item.Trim(), out date))
                    {
                        result.Add(date.ToString("dd MMM yyyy"));
                    }
                    else
                    {
                        Log($"{CurrentSchool.Index} ({holidayMode}: {item}) date format is invalid");
                        return false;
                    }

                }

                return true;
            }
            catch
            {
                Log($"{CurrentSchool.Index} ({holidayMode}) error in converting dates");
                return false;
            }
        }
    }

    public class DataManipulationUpdateHomepages : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Update home pages";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return AllSchools.Count();
        }

        public List<Club> CurrentSchools { get; set; }
        public Club CurrentSchool { get; set; }

        public IEnumerable<Club> AllSchools { get; set; }

        #region Constants
        public List<string> TabTitlesForDelete
        {
            get
            {
                return new List<string> { "Fall 2017 Activities",
                    "Winter 2018 Activities", "Spring 2018 Activities",
                    "Summer 2018", "Upper School Spring 2018 Ativities",
                    "Primary School Spring 2018 Activities",
                    "Spring 2018 Mini Session Activities", "Spring 2018 Addition",
                    "Activities", "Summer 2018 Day Camp",
                    "Upper School Spring 2018 Activities"};
            }
        }

        public List<string> WelcomePageTitles { get { return new List<string> { "welcome!", "welcome" }; } }
        public string PartnerDomain { get { return "flexacademies"; } }
        #endregion

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            AllSchools = DataContext.Set<Club>().Where(p =>
                !p.IsDeleted
                && p.PartnerId.HasValue
                //&& p.Domain.Equals("flexacademiescamps", StringComparison.OrdinalIgnoreCase)
                && p.PartnerClub.Domain.Equals(PartnerDomain, StringComparison.OrdinalIgnoreCase)).ToList()
                .Where(c => c.IsSchool)
                .ToList();

            var numberOfSchools = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentSchools = AllSchools.OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfSchools);

            UpdateHomePages();

            FinalizeM();

            return this.Reslult;
        }

        private void UpdateHomePages()
        {
            foreach (var item in CurrentSchools)
            {
                CurrentSchool = item;
                UpdateHomePage();
            }
        }

        private void UpdateHomePage()
        {
            var homePages = CurrentSchool.Pages.AsQueryable().AsNoTracking();
            var draftPage = homePages.SingleOrDefault(h => h.SaveType == SaveType.Draft);
            var publishedPage = homePages.SingleOrDefault(h => h.SaveType == SaveType.Publish);

            Log($"{CurrentSchool.Domain} started----------------------------------------------------------------");

            if (publishedPage != null)
            {
                Log($"{CurrentSchool.Domain} publish page ------------------");

                var publishedPageId = publishedPage.Id;
                publishedPage = JsonHelper.JsonComplexDeserialize<JbPage>(publishedPage.JsonBody);
                publishedPage.Id = publishedPageId;

                var elements = publishedPage.Elements;
                UpdateTabs(ref elements, "Publish");

                if (!TryUpdateParagraph(ref elements))
                {
                    Log($"{CurrentSchool.Domain} (publish) paragraph has error");
                }

                publishedPage.Elements = elements;
            }
            else
            {
                Log($"{CurrentSchool.Domain} hasn't publish page");
                //return;
            }


            if (draftPage != null)
            {
                Log($"{CurrentSchool.Domain} Draft page ------------------");

                var draftPageId = draftPage.Id;
                draftPage = JsonHelper.JsonComplexDeserialize<JbPage>(draftPage.JsonBody);
                draftPage.Id = draftPageId;

                var elements = draftPage.Elements;
                UpdateTabs(ref elements, "Draft");

                if (!TryUpdateParagraph(ref elements))
                {
                    Log($"{CurrentSchool.Domain} (draft) paragraph has error");
                }

                draftPage.Elements = elements;
            }
            else
            {
                Log($"{CurrentSchool.Domain} hasn't draft page");
                return;
            }

            var homePages2 = CurrentSchool.Pages;
            var draftPage2 = homePages2.SingleOrDefault(h => h.SaveType == SaveType.Draft);
            var publishedPage2 = homePages2.SingleOrDefault(h => h.SaveType == SaveType.Publish);

            if (draftPage != null)
                draftPage2.JsonBody = JsonHelper.JsonSerializer(draftPage);

            if (publishedPage != null)
            {
                publishedPage2.JsonBody = JsonHelper.JsonSerializer(publishedPage);
                Log($"{CurrentSchool.Domain} Done");
            }

        }

        void UpdateTabs(ref List<JbPBaseElement> elements, string pageSaveType)
        {
            var elementsForDelete = elements.Where(e => e is JbPTab && TabTitlesForDelete.Contains((e as JbPTab).Title)).ToList();

            var forDelelteTitles = elementsForDelete.Select(e => (e as JbPTab).Title).ToList();

            var notDoneTabs = TabTitlesForDelete.Where(e => !forDelelteTitles.Contains(e));

            if (notDoneTabs.Any())
                Log($"{CurrentSchool.Domain} can't find tabs {string.Join(", ", notDoneTabs.Select(n => n).ToList())} in page: {pageSaveType}");

            foreach (var item in elementsForDelete)
            {
                elements.RemoveAt(elements.IndexOf(item));
            }
        }

        bool TryUpdateParagraph(ref List<JbPBaseElement> elements)
        {
            try
            {
                var welcomeTab = elements.SingleOrDefault(s => s is JbPTab && !string.IsNullOrWhiteSpace(s.Title) && WelcomePageTitles.Contains(s.Title.ToLower()));

                if (welcomeTab != null)
                {
                    var tab = welcomeTab as JbPTab;
                    var fisrtTitlePack = tab.Elements.FirstOrDefault(e => e is JbPTitlePack && !string.IsNullOrWhiteSpace((e as JbPTitlePack).Title) && (e as JbPTitlePack).Title.Contains("Welcome to Registration"));

                    if (fisrtTitlePack != null)
                    {
                        var titlePack = fisrtTitlePack as JbPTitlePack;

                        if (!string.IsNullOrWhiteSpace(titlePack.Paragraph))
                        {
                            HtmlDocument html = new HtmlDocument();

                            //titlePack.Paragraph = @"<strong style=""font-size:large;text-align:center;""><p>For information about the activities being offered this year, and to register for activities, select the relevant ""Activities"" tab above. Activities are offered for three 8-week sessions throughout the year. Approximate dates are:</p><table class=""sessched k-table"" style=""width:932px;""><tbody><tr><td style=""width:233px;""></td><td style=""width:233px;""><span style=""text-decoration-line:underline;"">Season</span></td><td style=""width:233px;""><span style=""text-decoration-line:underline;"">Dates</span></td><td style=""width:232px;""></td></tr><tr><td style=""width:233px;""></td><td>Fall</td><td>Sept. 25th - Dec. 1st</td><td style=""width:232px;""></td></tr><tr><td style=""width:233px;""></td><td>Winter</td><td>Jan. 16th - Mar. 19th</td><td style=""width:232px;""></td></tr><tr><td style=""width:233px;""></td><td>Spring</td><td>Apr. 10th - June 1st</td><td style=""width:232px;""></td></tr></tbody></table><p><br /></p><p>Please note that dates are subject to change. Exact dates will be announced throughout the year as necessary.</p></strong>";

                            html.LoadHtml(titlePack.Paragraph);

                            var table = html.DocumentNode.Descendants("table").FirstOrDefault();

                            if (table != null)
                            {
                                List<HtmlNode> trs = new List<HtmlNode>();
                                int seasonPlace = 0;
                                int datePlace = 0;

                                try
                                {
                                    var tBody = table.ChildNodes.Single(t => t.Name.Equals("tbody", StringComparison.OrdinalIgnoreCase));

                                    trs = tBody.ChildNodes.Where(t => t.Name.Equals("tr", StringComparison.OrdinalIgnoreCase)).ToList();

                                    var firstTr = trs.First();
                                    var firstTrTds = firstTr.ChildNodes.Where(c => c.Name.Equals("td", StringComparison.OrdinalIgnoreCase)).ToList();
                                    for (int i = 1; i <= firstTrTds.Count; i++)
                                    {
                                        if (!string.IsNullOrWhiteSpace(firstTrTds[i - 1].InnerText) && firstTrTds[i - 1].InnerText.Trim().Equals("Season", StringComparison.OrdinalIgnoreCase))
                                        {
                                            seasonPlace = i;
                                        }

                                        if (!string.IsNullOrWhiteSpace(firstTrTds[i - 1].InnerText) && firstTrTds[i - 1].InnerText.Trim().Equals("Dates", StringComparison.OrdinalIgnoreCase))
                                        {
                                            datePlace = i;
                                        }
                                    }

                                    if (seasonPlace == 0)
                                    {
                                        Log($"{CurrentSchool.Domain} can't find season column");
                                        return false;
                                    }

                                    if (datePlace == 0)
                                    {
                                        Log($"{CurrentSchool.Domain} can't find date column");
                                        return false;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log($"{CurrentSchool.Domain} error in table proccesing: {ex.Message}");
                                }

                                var notDoneSeasons = new List<SeasonNames> { SeasonNames.Fall, SeasonNames.Spring, SeasonNames.Winter };

                                foreach (var tr in trs.Skip(1))
                                {
                                    var season = tr.ChildNodes.Where(s => s.Name.Equals("td")).ToList()[seasonPlace - 1];
                                    var date = tr.ChildNodes.Where(s => s.Name.Equals("td")).ToList()[datePlace - 1];

                                    if (season.InnerText.Equals("Fall", StringComparison.OrdinalIgnoreCase))
                                    {
                                        notDoneSeasons.Remove(SeasonNames.Fall);

                                        var fallSeason = DataContext.Set<Season>().SingleOrDefault(s => s.ClubId == CurrentSchool.Id && s.Name == SeasonNames.Fall && s.Status != SeasonStatus.Deleted && s.Year == 2018);

                                        DateTime? startDate = null;
                                        DateTime? endDate = null;
                                        if (fallSeason != null)
                                        {

                                            if (fallSeason.Setting != null && fallSeason.Setting.SeasonProgramInfoSetting != null)
                                            {
                                                startDate = fallSeason.Setting.SeasonProgramInfoSetting.StartDate;
                                                endDate = fallSeason.Setting.SeasonProgramInfoSetting.EndDate;
                                            }
                                            else
                                            {
                                                Log($"{CurrentSchool.Domain} season {(fallSeason != null ? fallSeason.Title : string.Empty)} settings has error");
                                                //return false;
                                            }
                                        }
                                        else
                                        {
                                            Log($"{CurrentSchool.Domain} can't find fall season");
                                        }

                                        date.InnerHtml = GetSeasonDateRange(startDate, endDate, SeasonNames.Fall.ToString());
                                    }
                                    else if (season.InnerText.Equals("Winter", StringComparison.OrdinalIgnoreCase))
                                    {
                                        notDoneSeasons.Remove(SeasonNames.Winter);

                                        var winterSeason = DataContext.Set<Season>().SingleOrDefault(s => s.ClubId == CurrentSchool.Id && s.Name == SeasonNames.Winter && s.Status != SeasonStatus.Deleted && s.Year == 2019);

                                        DateTime? startDate = null;
                                        DateTime? endDate = null;

                                        if (winterSeason != null)
                                        {

                                            if (winterSeason.Setting != null && winterSeason.Setting.SeasonProgramInfoSetting != null)
                                            {
                                                startDate = winterSeason.Setting.SeasonProgramInfoSetting.StartDate;
                                                endDate = winterSeason.Setting.SeasonProgramInfoSetting.EndDate;
                                            }
                                            else
                                            {
                                                Log($"{CurrentSchool.Domain} season {(winterSeason != null ? winterSeason.Title : string.Empty)} settings has error");
                                                //return false;
                                            }
                                        }
                                        else
                                        {
                                            Log($"{CurrentSchool.Domain} can't find winter season");
                                        }

                                        date.InnerHtml = GetSeasonDateRange(startDate, endDate, SeasonNames.Winter.ToString());
                                    }
                                    else if (season.InnerText.Equals("Spring", StringComparison.OrdinalIgnoreCase))
                                    {
                                        notDoneSeasons.Remove(SeasonNames.Spring);
                                        var springSeason = DataContext.Set<Season>().SingleOrDefault(s => s.ClubId == CurrentSchool.Id && s.Name == SeasonNames.Spring && s.Status != SeasonStatus.Deleted && s.Year == 2019);

                                        DateTime? startDate = null;
                                        DateTime? endDate = null;
                                        if (springSeason != null)
                                        {
                                            if (springSeason != null && springSeason.Setting != null && springSeason.Setting.SeasonProgramInfoSetting != null)
                                            {
                                                startDate = springSeason.Setting.SeasonProgramInfoSetting.StartDate;
                                                endDate = springSeason.Setting.SeasonProgramInfoSetting.EndDate;
                                            }
                                            else
                                            {
                                                Log($"{CurrentSchool.Domain} season {(springSeason != null ? springSeason.Title : string.Empty)} settings has error");
                                                //return false;
                                            }
                                        }
                                        else
                                        {
                                            Log($"{CurrentSchool.Domain} can't find spring season");
                                        }

                                        date.InnerHtml = GetSeasonDateRange(startDate, endDate, SeasonNames.Spring.ToString());
                                    }
                                    else if (season.InnerText.Equals("Interim", StringComparison.OrdinalIgnoreCase) || season.InnerText.Equals(SeasonNames.FallInterim.ToDescription(), StringComparison.OrdinalIgnoreCase) || season.InnerText.Equals(SeasonNames.WinterInterim.ToDescription(), StringComparison.OrdinalIgnoreCase))
                                    {
                                        date.InnerHtml = GetSeasonDateRange(null, null, "Interim");
                                    }
                                }

                                if (notDoneSeasons.Any())
                                    Log($"{CurrentSchool.Domain} can't find seasons {string.Join(", ", notDoneSeasons.Select(n => n.ToString()).ToList())} in table");

                                titlePack.Paragraph = html.DocumentNode.InnerHtml;
                            }
                            else
                            {
                                Log($"{CurrentSchool.Domain} can't find table");
                                return false;
                            }
                        }
                        else
                        {
                            Log($"{CurrentSchool.Domain} parapraph is null");
                            return false;
                        }
                    }
                    else
                    {
                        Log($"{CurrentSchool.Domain} can't find fisrt title pack");
                        return false;
                    }
                }
                else
                {
                    Log($"{CurrentSchool.Domain} welcome tab is not exist");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Log($"{CurrentSchool.Domain} {(ex.InnerException != null ? ex.InnerException.Message : ex.Message)}");
                return false;
            }
        }

        private string GetSeasonDateRange(DateTime? startDate, DateTime? endDate, string season)
        {

            if (startDate.HasValue && endDate.HasValue)
            {
                var start = $"{startDate.Value.ToString("MMM.")} {NumberHelper.ToOrdinal(startDate.Value.Day)}";
                var end = $"{endDate.Value.ToString("MMM.")} {NumberHelper.ToOrdinal(endDate.Value.Day)}";

                return $"{start} - {end}";
            }

            Log($"{CurrentSchool.Domain} ({season}) season dates are null");
            return "*****";
        }
    }

    public class DataManipulationFixDuplicateProfiles : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Fix duplicate profiles";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 500;
            }
        }

        public override int GetNumberOfElements()
        {
            return AllUsers.Count();
        }

        public List<int> CurrentUsers { get; set; }
        public int CurrentUser { get; set; }

        public IEnumerable<JbUser> AllUsers { get; set; }

        public List<PlayerProfile> CurrentParents { get; set; }

        public List<FamilyContact> CurrentAuthorizes { get; set; }
        public List<FamilyContact> CurrentEmergencys { get; set; }

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            AllUsers = DataContext.Set<JbUser>();


            var numberOfUsers = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentUsers = AllUsers.OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).Select(c => c.Id).ToList();
            }

            Initialize(numberOfUsers);

            FixDuplicates();

            FinalizeM();

            return this.Reslult;
        }

        private void FixDuplicates()
        {
            CurrentParents = DataContext.Set<PlayerProfile>().Where(p => CurrentUsers.Contains(p.UserId) && p.Relationship == RelationshipType.Parent && p.Status != PlayerProfileStatusType.Deleted).Include("Contact").ToList();
            CurrentAuthorizes = DataContext.Set<FamilyContact>().Where(f => CurrentUsers.Contains(f.Family.UserId) && f.Type == FamilyContactType.AuthorizedPickup && f.Status == FamilyContactStatusType.Active).Include("Contact").ToList();
            CurrentEmergencys = DataContext.Set<FamilyContact>().Where(f => CurrentUsers.Contains(f.Family.UserId) && f.Type == FamilyContactType.Emergency && f.Status == FamilyContactStatusType.Active).Include("Contact").ToList();

            foreach (var item in CurrentUsers)
            {
                CurrentUser = item;

                FixDuplicateParent();

                FixDuplicateAuthorize();

                FixDuplicateEmergency();
            }
        }

        private void FixDuplicateParent()
        {
            var userParents = GetParents(CurrentUser);

            if (userParents.Any())
            {
                foreach (var profile in userParents)
                {
                    var sameProfiles = userParents.Where(p => p.Contact.FirstName.EqualsTrim(profile.Contact.FirstName) && p.Contact.LastName.EqualsTrim(profile.Contact.LastName) && profile.Id != p.Id && p.Id > profile.Id).ToList();

                    if (sameProfiles.Any())
                    {
                        foreach (var item in sameProfiles)
                        {
                            item.Status = PlayerProfileStatusType.Deleted;
                            Log(string.Format("Deleted parent:  {0} ", item.Id));
                        }
                    }
                }
            }
            else
            {
                return;
            }
        }

        public List<PlayerProfile> GetParents(int userId)
        {
            var parents = CurrentParents.Where(u => u.UserId == userId).ToList();

            return parents;
        }

        private void FixDuplicateAuthorize()
        {
            var authorizedPickups = GetAuthorizedPickups(CurrentUser);

            if (authorizedPickups.Any())
            {
                foreach (var profile in authorizedPickups)
                {
                    var sameProfiles = authorizedPickups.Where(p => p.Contact.FirstName.EqualsTrim(profile.Contact.FirstName) && p.Contact.LastName.EqualsTrim(profile.Contact.LastName) && profile.Id != p.Id && p.Id > profile.Id).ToList();

                    if (sameProfiles.Any())
                    {
                        foreach (var item in sameProfiles)
                        {
                            item.Status = FamilyContactStatusType.Deleted;
                            Log(string.Format("Deleted authorize id:  {0} ", item.Id));
                        }

                    }
                }

            }
            else
            {
                return;
            }
        }


        public List<FamilyContact> GetAuthorizedPickups(int userId)
        {
            var authorizedPickups = CurrentAuthorizes.Where(u => u.Family.UserId == userId).ToList();

            return authorizedPickups;
        }

        private void FixDuplicateEmergency()
        {
            var emergencyContacts = GetEmergencyContacts(CurrentUser);

            if (emergencyContacts.Any())
            {
                var profilesToRemove = new List<FamilyContact>();

                foreach (var profile in emergencyContacts)
                {
                    var sameProfiles = emergencyContacts.Where(p => p.Contact.FirstName.EqualsTrim(profile.Contact.FirstName) && p.Contact.LastName.EqualsTrim(profile.Contact.LastName) && profile.Id != p.Id && p.Id > profile.Id).ToList();

                    if (sameProfiles.Any())
                    {

                        foreach (var item in sameProfiles)
                        {
                            item.Status = FamilyContactStatusType.Deleted;
                            Log(string.Format("Deleted emergency:  {0} ", item.Id));
                        }

                    }
                }

            }
            else
            {
                return;
            }
        }

        public List<FamilyContact> GetEmergencyContacts(int userId)
        {
            var emergencyContacts = CurrentEmergencys.Where(u => u.Family.UserId == userId).ToList();

            return emergencyContacts;
        }
    }

    public class DataManipulationGetDifferentCreditCards : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Get different credit cards";
            }
        }

        private static string RASClubDomain = "rightatschool";
        private static string RASCanadaClubDomain = "rightatschoolcanada";
        private static bool IsRAS = true;
        private static bool IsWithCategory = true;
        public override int? NumbersInCycle
        {
            get
            {
                return 300;
            }
        }

        public override int GetNumberOfElements()
        {
            return AllCreditCardWithCustomerId.Count();
        }

        public List<UserCreditCard> CurrentCreditCards { get; set; }

        public IEnumerable<UserCreditCard> AllCreditCardWithCustomerId { get; set; }

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            if (IsRAS)
            {
                AllCreditCardWithCustomerId = DataContext.Set<UserCreditCard>().Where(c => !c.IsDeleted && c.TypePaymentGateway == PaymentGateway.Stripe && !string.IsNullOrEmpty(c.CustomerId) &&
                (c.Club.Domain == RASClubDomain || (c.Club.PartnerId.HasValue && c.Club.PartnerClub.Domain == RASClubDomain) || c.Club.Domain == RASCanadaClubDomain || (c.Club.PartnerId.HasValue && c.Club.PartnerClub.Domain == RASCanadaClubDomain)));

            }
            else
            {
                AllCreditCardWithCustomerId = DataContext.Set<UserCreditCard>().Where(c => !c.IsDeleted && c.TypePaymentGateway == PaymentGateway.Stripe && !string.IsNullOrEmpty(c.CustomerId) &&
                (c.Club.Domain != RASClubDomain && (c.Club.PartnerId.HasValue && c.Club.PartnerClub.Domain != RASClubDomain) && c.Club.Domain != RASCanadaClubDomain && (c.Club.PartnerId.HasValue && c.Club.PartnerClub.Domain != RASCanadaClubDomain)));

            }

            var numberOfCreditCards = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentCreditCards = AllCreditCardWithCustomerId.OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfCreditCards);

            GetDifferentCreditCards();

            FinalizeM();

            return this.Reslult;
        }

        private void GetDifferentCreditCards()
        {
            foreach (var firstItem in CurrentCreditCards)
            {
                try
                {
                    if (firstItem.CardId != null)
                    {
                        var card = GetCard(firstItem.CustomerId, firstItem.CardId);

                        if (card?.Last4 != firstItem.LastDigits)
                        {
                            if (IsWithCategory)
                            {
                                string category = string.Empty;
                                var userCreditCards = DataContext.Set<UserCreditCard>().Where(c => !c.IsDeleted && c.CardId != firstItem.CardId && c.TypePaymentGateway == PaymentGateway.Stripe && c.UserId == firstItem.UserId && !string.IsNullOrEmpty(c.CustomerId));

                                if (userCreditCards.Any())
                                {
                                    int i = 1;

                                    foreach (var carditem in userCreditCards)
                                    {

                                        var card2 = GetCard(carditem.CustomerId, carditem.CardId);

                                        if (card2?.Last4 == carditem.LastDigits)
                                        {
                                            if (firstItem.IsDefault)
                                            {
                                                category = "4";
                                            }
                                            else
                                            {
                                                category = "3";
                                            }

                                            var expire = string.Format("{0}/{1}", firstItem.ExpiryMonth, firstItem.ExpiryYear);
                                            Log(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", firstItem.CardId, firstItem.CustomerId, firstItem.UserId, firstItem.User.UserName, firstItem.LastDigits, card?.Last4, firstItem.ClubId.HasValue ? firstItem.Club.Domain : null, firstItem.Name, expire, category));

                                            break;
                                        }
                                        else
                                        {
                                            if (i == userCreditCards.Count())
                                            {
                                                if (firstItem.IsDefault)
                                                {
                                                    category = "2";
                                                }
                                                else
                                                {
                                                    category = "1";
                                                }

                                                var expire = string.Format("{0}/{1}", firstItem.ExpiryMonth, firstItem.ExpiryYear);
                                                Log(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", firstItem.CardId, firstItem.CustomerId, firstItem.UserId, firstItem.User.UserName, firstItem.LastDigits, card?.Last4, firstItem.ClubId.HasValue ? firstItem.Club.Domain : null, firstItem.Name, expire, category));

                                            }
                                            else
                                            {
                                                continue;
                                            }

                                        }

                                        i++;
                                    }

                                }
                                else
                                {
                                    if (firstItem.IsDefault)
                                    {
                                        category = "2";
                                    }
                                    else
                                    {
                                        category = "1";
                                    }

                                    var expire = string.Format("{0}/{1}", firstItem.ExpiryMonth, firstItem.ExpiryYear);
                                    Log(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", firstItem.CardId, firstItem.CustomerId, firstItem.UserId, firstItem.User.UserName, firstItem.LastDigits, card?.Last4, firstItem.ClubId.HasValue ? firstItem.Club.Domain : null, firstItem.Name, expire, category));

                                }

                            }
                            else
                            {
                                var expire = string.Format("{0}/{1}", firstItem.ExpiryMonth, firstItem.ExpiryYear);
                                Log(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}", firstItem.CardId, firstItem.CustomerId, firstItem.UserId, firstItem.User.UserName, firstItem.LastDigits, card?.Last4, firstItem.ClubId.HasValue ? firstItem.Club.Domain : null, firstItem.Name, expire));

                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Log(string.Format("Error message :  {0} ", ex.Message));
                }


            }
        }

        public static StripeCard GetCard(string customerId, string cardId)
        {

            var cardService = new StripeCardService("sk_live_2o4t7SfhBGNx3GgmdQgZWiiE");
            StripeCard stripeCard = cardService.Get(customerId, cardId);  ////Get Card

            return stripeCard;
        }

    }

    public class DataManipulationFixOrderTeacher : DataManipulationBaseModel<DataManipulationFixOrderTeachersItem>
    {
        public override string Title
        {
            get
            {
                return "Fix order teachers";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 50;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationFixOrderTeachersItem> CurrentOrderItems { get; set; }
        public DataManipulationFixOrderTeachersItem CurrentOrderItem { get; set; }
        public JbDropDown TeacherElement { get; set; }

        private Club SourceFormClub { get; set; }
        public const string SourceFormClubDomain = "kiplingpto";
        public const string FormTemplateTitle = "Class Registration Form";
        public const string TeacherElementTitle = "Teacher/Class";

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentOrderItems = SourceItems;

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);


            ManipulateOrderItems();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateOrderItems()
        {

            SourceFormClub = DataContext.Set<Club>().SingleOrDefault(c => c.Domain.Equals(SourceFormClubDomain, StringComparison.OrdinalIgnoreCase));

            var clubFormTemplate = DataContext.Set<ClubFormTemplate>().SingleOrDefault(c => c.Club.Domain.Equals(SourceFormClubDomain, StringComparison.OrdinalIgnoreCase) && !c.IsDeleted && c.Title.Equals(FormTemplateTitle, StringComparison.OrdinalIgnoreCase));

            if (clubFormTemplate == null)
            {
                Log($"{CurrentOrderItem.Index} Program form template is not exit");
                return;
            }

            var clubForm = clubFormTemplate.JbForm;
            var teacherElement = GetFormElementByTitle(clubForm, SectionsName.SchoolSection.ToString(), TeacherElementTitle);

            if (teacherElement == null)
            {
                Log($"{CurrentOrderItem.Index} can't find teacher element in program form template");
                return;
            }

            TeacherElement = teacherElement as JbDropDown;

            foreach (var item in CurrentOrderItems)
            {
                CurrentOrderItem = item;
                ManipulateOrderItem();
            }
        }

        private void ManipulateOrderItem()
        {
            try
            {
                var programScheduleIds = DataContext.Set<Program>().Where(p => p.ClubId == SourceFormClub.Id && p.Name.Equals(CurrentOrderItem.Program, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted).SelectMany(p => p.ProgramSchedules).Select(s => s.Id).ToList();

                var orderItemJbForms = DataContext.Set<OrderItem>().Where(o => o.ProgramScheduleId.HasValue && o.ItemStatus != OrderItemStatusCategories.deleted && programScheduleIds.Contains(o.ProgramScheduleId.Value) && o.JbFormId.HasValue).Select(j => j.JbForm).ToList().Where(f => ChecKIfFirstAndLastNameEquals(f)).ToList();

                if (orderItemJbForms.Any())
                {

                    if (orderItemJbForms.Count() > 1)
                        Log($"{CurrentOrderItem.Index} more than one participant exist, Jbforms: ({string.Join(", ", orderItemJbForms.Select(o => o.Id).ToList())})");

                    foreach (var itemJbForm in orderItemJbForms)
                    {

                        var orderTeacherElement = GetFormElementByTitle(itemJbForm, SectionsName.SchoolSection.ToString(), TeacherElementTitle);
                        TeacherElement.SetValue(CurrentOrderItem.Teacher);

                        if (orderTeacherElement == null)
                        {
                            Log($"{CurrentOrderItem.Index} Can't find teacher element");

                            (itemJbForm.Elements.Single(e => e.Name == SectionsName.SchoolSection.ToString()) as JbSection).Elements.Add(TeacherElement);
                        }
                        else
                            orderTeacherElement = TeacherElement;

                        itemJbForm.JsonElements = JsonConvert.SerializeObject(itemJbForm.Elements);
                    }
                }
                else
                {
                    Log($"{CurrentOrderItem.Index} Can't find participant");
                    return;
                }
            }
            catch (Exception ex)
            {
                Log($"{CurrentOrderItem.Index} has total error: {ex.Message}");
            }
        }

        private IJbBaseElement GetFormElementByTitle(JbForm jbForm, string sectionName, string elementTitle)
        {
            IJbBaseElement result = null;

            try
            {
                if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Title.Equals(elementTitle, StringComparison.OrdinalIgnoreCase)))
                {
                    result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Title.Equals(elementTitle, StringComparison.OrdinalIgnoreCase));
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        private bool ChecKIfFirstAndLastNameEquals(JbForm jbform)
        {
            if (jbform == null)
                return false;

            var parentSection = jbform.Elements.Single(e => e.Name == SectionsName.ParticipantSection.ToString()) as JbSection;

            var firstName = parentSection.Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue()?.ToString();
            var lastName = parentSection.Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue()?.ToString();

            return firstName.Equals(CurrentOrderItem.FirstName, StringComparison.OrdinalIgnoreCase) && lastName.Equals(CurrentOrderItem.LastName, StringComparison.OrdinalIgnoreCase);
        }
    }

    public class DataManipulationFixProgramTeacher : DataManipulationBaseModel<DataManipulationFixProgramTeachersItem>
    {
        public override string Title
        {
            get
            {
                return "Fix program teachers";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationFixProgramTeachersItem> CurrentSchools { get; set; }
        public DataManipulationFixProgramTeachersItem CurrentSchool { get; set; }
        //public JbDropDown TeacherElement { get; set; }

        public const string PartnerDomain = "flexacademies";
        //public const string FormTemplateTitle = "Class Registration Form";
        //public const string TeacherElementTitle = "Teacher/Class";

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentSchools = SourceItems;

            var numberOfPrograms = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentSchools = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfPrograms);


            ManipulateFormTemplates();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateFormTemplates()
        {

            foreach (var item in CurrentSchools)
            {
                CurrentSchool = item;
                ManipulateFormTemplate();
            }
        }

        private void ManipulateFormTemplate()
        {
            try
            {
                var school = DataContext.Set<Club>().SingleOrDefault(c => c.PartnerId.HasValue && c.PartnerClub.Domain.Equals(PartnerDomain, StringComparison.OrdinalIgnoreCase) && c.Domain.Equals(CurrentSchool.SchoolDomain));

                if (school == null)
                {
                    Log($"{CurrentSchool.Index} school is not exist");
                    return;
                }

                var formTemplateName = $"{school.Name} Registration Form 2018-19";

                var clubFormTemplate = DataContext.Set<ClubFormTemplate>().SingleOrDefault(c => c.ClubId == school.Id && c.Title.Equals(formTemplateName, StringComparison.OrdinalIgnoreCase));

                if (clubFormTemplate == null)
                {
                    Log($"{CurrentSchool.Index} Program form template is not exit");
                    return;
                }

                var clubJbForm = clubFormTemplate.JbForm;
                var teacherElement = GetFormElementByName(clubJbForm, SectionsName.SchoolSection.ToString(), ElementsName.TeacherName.ToString());

                if (teacherElement == null)
                {
                    Log($"{CurrentSchool.Index} can't find teacher element in program form template");
                    return;
                }

                var teachers = CurrentSchool.Teachers.Split(',');

                if (teachers == null || !teachers.Any())
                {
                    Log($"{CurrentSchool.Index} can't find any teacher in source elements");
                    return;
                }

                var teacherDropdownItems = new List<JbElementItem>();

                teacherDropdownItems.Add(new JbElementItem { ReadOnly = true, Text = "Teacher not listed", Value = "Teacher not listed" });

                teacherDropdownItems.AddRange(teachers.Select(t => new JbElementItem { Text = t, Value = t }).ToList());

                (teacherElement as JbDropDown).Items = teacherDropdownItems;

                clubJbForm.JsonElements = JsonConvert.SerializeObject(clubJbForm.Elements);
            }
            catch (Exception ex)
            {
                Log($"{CurrentSchool.Index} has total error: {ex.Message}");
            }
        }

        private IJbBaseElement GetFormElementByName(JbForm jbForm, string sectionName, string elementName)
        {
            IJbBaseElement result = null;

            try
            {
                if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                {
                    result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

    }

    public class DataManipulationCampSessions : DataManipulationBaseModel<DataManipulationAddCampSessionsItem>
    {
        public override string Title
        {
            get
            {
                return "Add camp sessions";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationAddCampSessionsItem> CurrentCharges { get; set; }
        public DataManipulationAddCampSessionsItem CurrentCharge { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentCharges = SourceItems;

            var numberOfCharges = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentCharges = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfCharges);


            ManipulateFormTemplates();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateFormTemplates()
        {

            foreach (var item in CurrentCharges)
            {
                CurrentCharge = item;
                ManipulateAddChargeSessions();
            }
        }

        private void ManipulateAddChargeSessions()
        {
            try
            {
                var chargeId = Int64.Parse(CurrentCharge.ChargeId);
                var charge = DataContext.Set<Charge>().FirstOrDefault(c => c.Id == chargeId);
                if (charge != null)
                {
                    AddSessinsForCharge(charge);
                }
            }
            catch (Exception ex)
            {
                Log($"{CurrentCharge.Index} has total error: {ex.Message}");
            }
        }

        private void AddSessinsForCharge(Charge charge)
        {
            var programSessionList = new List<ProgramSession>();
            var schedule = charge.ProgramSchedule;
            var attribute = ((ScheduleAttribute)schedule.Attributes);
            var days = attribute.Days;
            var startDate = schedule.StartDate;
            var endDate = schedule.EndDate;

            if (days != null)
            {
                for (DateTime dateTime = startDate; dateTime <= endDate; dateTime = dateTime.AddDays(1))
                {
                    if (days.Select(d => d.DayOfWeek).Contains(dateTime.DayOfWeek))
                    {
                        var day = days.Single(d => d.DayOfWeek == dateTime.DayOfWeek);

                        var startTime = day.StartTime;
                        var endTime = day.EndTime;

                        var programSession = new ProgramSession()
                        {
                            StartDateTime = dateTime.Date.Add(startTime.Value),
                            EndDateTime = dateTime.Date.Add(endTime.Value),
                            ChargeId = charge.Id,
                        };

                        programSessionList.Add(programSession);
                    }
                }
            }

            DataContext.Set<ProgramSession>().AddRange(programSessionList);
        }
    }

    public class DataManipulationAddCampOrderItemSessions : DataManipulationBaseModel<DataManipulationAddCampSessionsItem>
    {
        public override string Title
        {
            get
            {
                return "Add orderItem sessions";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 200;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationAddCampSessionsItem> CurrentCharges { get; set; }
        public DataManipulationAddCampSessionsItem CurrentCharge { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentCharges = SourceItems;

            var numberOfCharges = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentCharges = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfCharges);


            ManipulateOrderItemSessions();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateOrderItemSessions()
        {

            foreach (var item in CurrentCharges)
            {
                CurrentCharge = item;
                ManipulateAddOrderItemSessions();
            }
        }

        private void ManipulateAddOrderItemSessions()
        {
            try
            {
                var chargeId = Int64.Parse(CurrentCharge.ChargeId);
                var charge = DataContext.Set<Charge>().FirstOrDefault(c => c.Id == chargeId);
                if (charge != null)
                {
                    var orderItems = DataContext.Set<OrderItem>().Where(i => i.ItemStatus != OrderItemStatusCategories.deleted && i.ItemStatus != OrderItemStatusCategories.notInitiated && i.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.ChargeId == charge.Id));

                    if (orderItems.Any())
                    {
                        foreach (var item in orderItems)
                        {
                            AddSessinsForCharge(item, charge);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log($"{CurrentCharge.Index} has total error: {ex.Message}");
            }
        }

        private void AddSessinsForCharge(OrderItem orderItem, Charge charge)
        {
            var ordersessions = new List<OrderSession>();

            var programSessions = charge.ProgramSessions;

            foreach (var programSession in programSessions)
            {
                var orderSession = new OrderSession()
                {
                    ProgramSessionId = programSession.Id,
                    MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                    }
                };

                orderItem.OrderSessions.Add(orderSession);
            }
        }
    }

    public class DataManipulationUpdateStopedPayment : DataManipulationBaseModel<DataManipulationStopedPaymentItem>
    {
        public override string Title
        {
            get
            {
                return "Update stoped payment";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationStopedPaymentItem> CurrentOrderItems { get; set; }
        public DataManipulationStopedPaymentItem CurrentItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentOrderItems = SourceItems;

            var numberOfCharges = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfCharges);


            ManipulateFormTemplates();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateFormTemplates()
        {

            foreach (var item in CurrentOrderItems)
            {
                CurrentItem = item;
                ManipulateUpdateStopedOrderItemPayment();
            }
        }

        private void ManipulateUpdateStopedOrderItemPayment()
        {
            try
            {
                var orderitemId = Int64.Parse(CurrentItem.Id);
                var orderItem = DataContext.Set<OrderItem>().FirstOrDefault(c => c.Id == orderitemId);
                if (orderItem != null)
                {
                    UpdateStopedOrder(orderItem);
                }
                else
                {
                    Log($"{CurrentItem.Index} order item is null");
                    return;
                }
            }
            catch (Exception ex)
            {
                Log($"{CurrentItem.Index} has total error: {ex.Message}");
            }
        }

        private void UpdateStopedOrder(OrderItem orderItem)
        {
            try
            {
                var orderItemIntallments = orderItem.Installments;
                var installmentDate = Convert.ToDateTime(CurrentItem.InstallmentDate);
                decimal newAmount = Convert.ToDecimal(CurrentItem.NewAmount);
                decimal differenceAmounts = Convert.ToDecimal(CurrentItem.Diffrence);
                var newInstallmentDate = Convert.ToDateTime("2018-09-17 00:00:00.000");

                var currentInstallment = orderItemIntallments.FirstOrDefault(i => i.InstallmentDate.Date == installmentDate.Date);

                if (currentInstallment == null)
                {
                    Log($"{CurrentItem.Index} hasn't installment");
                    return;
                }

                currentInstallment.Amount = newAmount;
                currentInstallment.InstallmentDate = newInstallmentDate;

                orderItem.TotalAmount -= differenceAmounts;
                orderItem.EntryFee -= differenceAmounts;

                var entryFee = orderItem.GetOrderChargeDiscounts().FirstOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee);

                if (entryFee == null)
                {
                    Log($"{CurrentItem.Index} hasn't entryFee");
                    return;
                }
                else
                {
                    entryFee.Amount -= differenceAmounts;
                }

                var schedulePart = orderItem.OrderItemScheduleParts.FirstOrDefault(p => p.ProgramPartId == currentInstallment.ProgramSchedulePartId);

                if (schedulePart == null)
                {
                    Log($"{CurrentItem.Index} hasn't schedulePart");
                    return;
                }
                else
                {
                    schedulePart.PartAmount -= differenceAmounts;
                }

                orderItem.Order.OrderAmount -= differenceAmounts;

                orderItem.PaymentPlanStatus = PaymentPlanStatus.Started;
                orderItem.PaymentPlanStatusUpdateDate = DateTime.UtcNow;
            }
            catch (Exception ex)
            {
                Log($"{CurrentItem.Index} has total error: {ex.Message}");
            }
        }
    }

    public class DataManipulationStartOrderPayment : DataManipulationBaseModel<DataManipulationStopedPaymentItem>
    {
        public override string Title
        {
            get
            {
                return "Start payment for orders";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationStopedPaymentItem> CurrentOrderItems { get; set; }
        public DataManipulationStopedPaymentItem CurrentItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentOrderItems = SourceItems;

            var numberOfCharges = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfCharges);


            ManipulateFormTemplates();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateFormTemplates()
        {

            foreach (var item in CurrentOrderItems)
            {
                CurrentItem = item;
                ManipulateStartOrderItemPayment();
            }
        }

        private void ManipulateStartOrderItemPayment()
        {
            try
            {
                var orderitemId = Int64.Parse(CurrentItem.Id);
                var orderItem = DataContext.Set<OrderItem>().FirstOrDefault(c => c.Id == orderitemId);
                if (orderItem != null)
                {
                    StartOrderPayment(orderItem);
                }
            }
            catch (Exception ex)
            {
                Log($"{CurrentItem.Index} has total error: {ex.Message}");
            }
        }

        private void StartOrderPayment(OrderItem orderItem)
        {
            orderItem.PaymentPlanStatus = PaymentPlanStatus.Started;
            orderItem.PaymentPlanStatusUpdateDate = DateTime.UtcNow;
        }
    }

    public class DataManipulationDeleteDuplicateOrderSessions : DataManipulationBaseModel<DataManipulationUpdateDuplicateSessionItem>
    {
        public override string Title
        {
            get
            {
                return "Delete duplicate order sessions";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationUpdateDuplicateSessionItem> CurrentOrderItems { get; set; }
        public DataManipulationUpdateDuplicateSessionItem CurrentItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentOrderItems = SourceItems;

            var numberOfCharges = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfCharges);


            ManipulateFormTemplates();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateFormTemplates()
        {

            foreach (var item in CurrentOrderItems)
            {
                CurrentItem = item;
                ManipulateRemoveDuplicateOrderSession();
            }
        }

        private void ManipulateRemoveDuplicateOrderSession()
        {
            try
            {
                var orderitemId = Int64.Parse(CurrentItem.OrderItemId);
                var orderItem = DataContext.Set<OrderItem>().FirstOrDefault(c => c.Id == orderitemId);
                if (orderItem != null)
                {
                    RemoveDuplicateOrderSession(orderItem);
                }
            }
            catch (Exception ex)
            {
                Log($"{CurrentItem.Index} has total error: {ex.Message}");
            }
        }

        private void RemoveDuplicateOrderSession(OrderItem orderItem)
        {
            var programSessionId = Int64.Parse(CurrentItem.ProgramSessionId);
            var orderSessions = orderItem.OrderSessions.Where(i => i.ProgramSessionId == programSessionId);
            var sessionIds = orderSessions.Select(o => o.Id).ToList();
            var attendancedSessions = DataContext.Set<StudentAttendance>().Where(a => sessionIds.Contains(a.OrderSessionId));

            if (attendancedSessions.Any())
            {
                if (attendancedSessions.Count() != orderSessions.Count())
                {
                    foreach (var sessions in orderSessions)
                    {
                        var sessionAttendance = DataContext.Set<StudentAttendance>().FirstOrDefault(c => c.OrderSessionId == sessions.Id);

                        if (sessionAttendance == null)
                        {
                            DataContext.Set<OrderSession>().Remove(sessions);
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    Log($"{CurrentItem.OrderItemId} with programSessionId {CurrentItem.ProgramSessionId} don't delete sessions.");
                }
            }
            else
            {
                DataContext.Set<OrderSession>().Remove(orderSessions.First());
            }
        }
    }

    public class DataManipulationAddOrders : DataManipulationBaseModel<DataManipulationAddOrderItem>
    {
        public override string Title
        {
            get
            {
                return "Add orders";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationAddOrderItem> CurrentOrders { get; set; }
        public DataManipulationAddOrderItem CurrentOrder { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentOrders = SourceItems;

            var numberOfCharges = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrders = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfCharges);


            ManipulateFormTemplates();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateFormTemplates()
        {
            foreach (var item in CurrentOrders)
            {
                CurrentOrder = item;
                ManipulateAddOrders();
            }
        }

        private void ManipulateAddOrders()
        {
            try
            {
                var club = DataContext.Set<Club>().FirstOrDefault(c => c.Domain == CurrentOrder.ClubDomain);
                var program = DataContext.Set<Program>().FirstOrDefault(p => p.ClubId == club.Id && p.Name == CurrentOrder.ProgramName && p.Status != ProgramStatus.Deleted);

                if (program == null)
                {
                    Log($"{CurrentOrder.Index} program is null");
                    return;
                }

                AddOrder(program);
            }
            catch (Exception ex)
            {
                Log($"{CurrentOrder.Index} has total error: {ex.Message}");
            }
        }

        private void AddOrder(Program program)
        {
            try
            {
                GenerateOrderItem(program);

            }
            catch (Exception ex)
            {
                Log($"{CurrentOrder.Index} has total error: {ex.Message}");
            }
        }

        private void GenerateOrderItem(Program program)
        {
            var order = new Order();
            var orderItem = new OrderItem();
            order.OrderItems = new List<OrderItem>();

            try
            {
                var schedule = program.ProgramSchedules.FirstOrDefault(s => !s.IsDeleted);
                var charge = DataContext.Set<Charge>().FirstOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee && !c.IsDeleted && c.ProgramScheduleId == schedule.Id);

                if (charge == null)
                {
                    Log($"{CurrentOrder.Index} Entry fee is null");
                    return;
                }

                var programParts = DataContext.Set<ProgramSchedulePart>().Where(i => i.ProgramId == program.Id && !i.IsDeleted).ToList();

                if (programParts == null)
                {
                    Log($"{CurrentOrder.Index} program part is null");
                    return;
                }

                var programPartCharges = DataContext.Set<ProgramSchedulePartCharge>().Where(i => i.ChargeId == charge.Id).ToList();

                if (programPartCharges == null)
                {
                    Log($"{CurrentOrder.Index} program part charges is null");
                    return;
                }

                var programDays = JsonConvert.DeserializeObject<ScheduleAfterBeforeCareAttribute>(schedule.AttributesSerialized).Days;
                var days = programDays.Select(d => d.DayOfWeek).ToList();
                var orderItemattributes = new OrderItemAttributes();

                orderItemattributes.WeekDays = days;
                orderItem.Attributes = orderItemattributes;
                orderItem.Installments = new List<OrderInstallment>();
                orderItem.OrderItemScheduleParts = new List<OrderItemSchedulePart>();

                var resultAppliedProgramParts = programParts.OrderBy(o => o.DueDate).ToList();
                var firstProgramPartDate = resultAppliedProgramParts.Min(s => s.StartDate);
                var endProgramPartDate = resultAppliedProgramParts.Max(s => s.EndDate);

                orderItem.DesiredStartDate = firstProgramPartDate;
                orderItem.Start = schedule.StartDate;
                orderItem.End = schedule.EndDate;

                foreach (var programPart in programParts)
                {
                    decimal partAmount = programPartCharges.FirstOrDefault(c => c.SchedulePartId == programPart.Id).ChargeAmount;

                    orderItem.Installments.Add(new OrderInstallment
                    {
                        Amount = partAmount == 0 ? partAmount : 75,
                        EnableReminder = false,
                        InstallmentDate = programPart.DueDate.Value,
                        PaidAmount = partAmount == 0 ? partAmount : 75,
                        Status = OrderStatusCategories.completed,
                        ProgramSchedulePartId = programPart.Id,
                        PaidDate = DateTime.UtcNow,
                        TransactionActivities = new List<TransactionActivity>
                        {
                            new TransactionActivity
                            {
                               Amount = partAmount == 0 ? partAmount : 75,
                               TransactionType = TransactionType.Payment,
                               SeasonId = program.SeasonId,
                               TransactionDate = DateTime.UtcNow,
                               TransactionCategory = TransactionCategory.Sale,
                               ClubId = program.ClubId,
                               OrderItem = orderItem,
                               Order = order,
                               TransactionStatus = TransactionStatus.Success,
                               HandleMode = HandleMode.Offline,
                               MetaData = new MetaData(){DateCreated = DateTime.UtcNow , DateUpdated = DateTime.UtcNow},
                               PaymentDetail = new PaymentDetail
                               {
                                   PaymentMethod = PaymentMethod.Cash,
                                   Status = PaymentDetailStatus.COMPLETED,
                               }
                            }
                        }
                    });

                    orderItem.OrderItemScheduleParts.Add(new OrderItemSchedulePart
                    {
                        ProgramPartId = programPart.Id,
                        PartAmount = partAmount == 0 ? partAmount : 75,
                    });
                }

                orderItem.OrderChargeDiscounts = new List<OrderChargeDiscount>();

                orderItem.OrderChargeDiscounts.Add(new OrderChargeDiscount
                {
                    ChargeId = charge.Id,
                    Amount = orderItem.Installments.Sum(i => i.Amount),
                    Category = ChargeDiscountCategory.EntryFee,
                    Name = CurrentOrder.TuitionLable,
                    Description = "",
                    Subcategory = ChargeDiscountSubcategory.Charge
                });

                ComplateOrderItem(order, orderItem, program);

                CreateSession(orderItem, schedule);

                order.ConfirmationId = Ioc.OrderBusiness.GenerateConfirmationId();

                order.CompleteDate = DateTime.UtcNow;

                order.ClubId = program.ClubId;
                order.PaymentPlanType = PaymentPlanType.Installment;


                order.OrderHistories = new List<OrderHistory>();

                order.OrderHistories.Add(
                new OrderHistory
                {
                    Action = OrderAction.Paid,
                    ActionDate = DateTime.UtcNow,
                    Description = "order " + order.ConfirmationId + " is placed and paid by migration.",
                    UserId = 2,
                });

                order.OrderItems.Add(orderItem);

                DataContext.Set<Order>().Add(order);

            }
            catch (Exception ex)
            {
                Log($"{CurrentOrder.Index} has total error: {ex.Message}");
            }

        }

        public void CreateSession(OrderItem orderItem, ProgramSchedule schedule)
        {
            orderItem.OrderSessions = new List<OrderSession>();
            var orderSessions = new List<OrderSession>();

            var programSessions = schedule.ProgramSessions.Where(s => !s.IsDeleted);

            orderSessions = programSessions.Select(p =>
                new OrderSession
                {
                    ProgramSessionId = p.Id,
                    MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                    }
                })
                .ToList();

            orderItem.OrderSessions = orderSessions;
        }

        public void ComplateOrderItem(Order order, OrderItem orderItem, Program program)
        {
            try
            {
                var schedule = program.ProgramSchedules.FirstOrDefault(s => !s.IsDeleted);
                var orderEntryFee = orderItem.OrderChargeDiscounts.First().Amount;
                var itemName = Ioc.ProgramBusiness.GetProgramName(program.Name, schedule.StartDate, schedule.EndDate, "");
                itemName += " " + orderEntryFee.ToString().Replace(".00", "").Replace(".0", "");
                var orderAmount = orderItem.Installments.Sum(i => i.Amount);

                orderItem.FirstName = CurrentOrder.StudentFirstName;
                orderItem.LastName = CurrentOrder.StudentLastName;
                orderItem.SeasonId = program.SeasonId;
                orderItem.Name = itemName;
                orderItem.DateCreated = DateTime.UtcNow;

                orderItem.ProgramTypeCategory = program.TypeCategory;
                orderItem.ProgramScheduleId = schedule.Id;
                orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schedule.Id && !s.IsDeleted);

                orderItem.ItemStatus = OrderItemStatusCategories.completed;
                orderItem.EntryFeeName = CurrentOrder.TuitionLable;
                orderItem.EntryFee = orderAmount;
                orderItem.TotalAmount = orderAmount;
                orderItem.PaidAmount = orderAmount;
                orderItem.PaymentPlanType = PaymentPlanType.Installment;
                orderItem.Mode = OrderItemMode.Noraml;
                orderItem.RegType = RegistrationType.AllSession;

                order.OrderAmount = orderAmount;
                order.OrderMode = OrderMode.Offline;
                order.OrderStatus = OrderStatusCategories.completed;
                order.IsDraft = false;
                order.IsLive = true;
                order.IsAutoCharge = true;

                var user = DataContext.Set<JbUser>().FirstOrDefault(u => u.UserName == CurrentOrder.AccountEmail);

                if (user == null)
                {
                    Log($"{CurrentOrder.Index} user is invalid");
                    return;
                }

                var player = DataContext.Set<PlayerProfile>().FirstOrDefault(p => p.UserId == user.Id && p.Contact.FirstName == CurrentOrder.StudentFirstName && p.Contact.LastName == CurrentOrder.StudentLastName);

                if (player == null)
                {
                    Log($"{CurrentOrder.Index} player is invalid");
                    return;
                }

                orderItem.Player = player;
                order.UserId = user.Id;

                AddOrderItemJbform(program, orderItem);

            }
            catch (Exception ex)
            {
                Log($"{CurrentOrder.Index} has total error: {ex.Message}");
            }

        }

        private void AddOrderItemJbform(Program program, OrderItem orderItem)
        {
            var programJbForm = program.JbForm;

            var jbForm = new JbForm()
            {
                JsonElements = programJbForm.JsonElements,
                CreatedDate = DateTime.UtcNow,
                LastModifiedDate = DateTime.UtcNow,
                ParentId = programJbForm.Id,
                RefEntityName = "Order"
            };

            var firstName = GetFormElement(jbForm, ElementsName.FirstName.ToString(), SectionsName.ParticipantSection.ToString());
            firstName.SetValue(CurrentOrder.StudentFirstName);

            var lastName = GetFormElement(jbForm, ElementsName.LastName.ToString(), SectionsName.ParticipantSection.ToString());
            lastName.SetValue(CurrentOrder.StudentLastName);

            var grade = GetFormElement(jbForm, ElementsName.Grade.ToString(), SectionsName.SchoolSection.ToString());
            //grade.SetValue(FormatGrade(CurrentOrder.StudentGrade));


            var teacherName = GetFormElement(jbForm, ElementsName.TeacherNotListed.ToString(), SectionsName.SchoolSection.ToString());
            teacherName.SetValue(CurrentOrder.TeacherName);

            var dismissal = GetFormElement(jbForm, ElementsName.DismissalFromEnrichment.ToString(), SectionsName.SchoolSection.ToString());
            dismissal.SetValue(CurrentOrder.DismissalFromKidzArt);

            var parent1firstName = GetFormElement(jbForm, ElementsName.FirstName.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1firstName.SetValue(CurrentOrder.Parent1FirstName);

            var parent1LastName = GetFormElement(jbForm, ElementsName.LastName.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1LastName.SetValue(CurrentOrder.Parent1LastName);

            var parent1Email = GetFormElement(jbForm, ElementsName.Email.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1Email.SetValue(CurrentOrder.Parent1Email);

            var parent1HomePhone = GetFormElement(jbForm, ElementsName.Phone.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1HomePhone.SetValue(CurrentOrder.Parent1HomePhonePrimaryPhone);

            var parent1MobilePhone = GetFormElement(jbForm, ElementsName.AlternatePhone.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1MobilePhone.SetValue(CurrentOrder.Parent1MobilePhoneAlternatePhone);

            var authorizedPickup1FirstName = GetFormElement(jbForm, ElementsName.FirstName.ToString(), SectionsName.AuthorizedPickup1.ToString());
            authorizedPickup1FirstName.SetValue(CurrentOrder.AuthorizedPickUp1FirstName);

            var authorizedPickup1LastName = GetFormElement(jbForm, ElementsName.LastName.ToString(), SectionsName.AuthorizedPickup1.ToString());
            authorizedPickup1LastName.SetValue(CurrentOrder.AuthorizedPickUp1LastName);

            var authorizedPickup2FirstName = GetFormElement(jbForm, ElementsName.FirstName.ToString(), SectionsName.AuthorizedPickup2.ToString());
            authorizedPickup2FirstName.SetValue(CurrentOrder.AuthorizedPickUp2FirstName);

            var authorizedPickup2LastName = GetFormElement(jbForm, ElementsName.LastName.ToString(), SectionsName.AuthorizedPickup2.ToString());
            authorizedPickup2LastName.SetValue(CurrentOrder.AuthorizedPickUp2LastName);


            jbForm.JsonElements = JsonConvert.SerializeObject(jbForm.Elements);

            orderItem.JbForm = jbForm;
        }

        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

    }

    public class DataManipulationUpdateTeacherName : DataManipulationBaseModel<DataManipulationUpdateFormItem>
    {
        public override string Title
        {
            get
            {
                return "Update teacher name in reg form";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }


        public List<DataManipulationUpdateFormItem> CurrentOrderItems { get; set; }
        public DataManipulationUpdateFormItem CurrentItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            CurrentOrderItems = SourceItems;

            var numberOfCharges = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfCharges);


            ManipulateFormTemplates();

            FinalizeM();

            return this.Reslult;
        }

        private void ManipulateFormTemplates()
        {

            foreach (var item in CurrentOrderItems)
            {
                CurrentItem = item;
                ManipulateUpdateTeacherName();

            }
        }

        private void ManipulateUpdateTeacherName()
        {
            try
            {
                var club = DataContext.Set<Club>().SingleOrDefault(c => c.Domain == CurrentItem.ClubDomain);

                var programScheduleIds = DataContext.Set<Program>().Where(p => p.ClubId == club.Id && p.Name.Equals(CurrentItem.Program, StringComparison.OrdinalIgnoreCase) && p.Status != ProgramStatus.Deleted).SelectMany(p => p.ProgramSchedules).Select(s => s.Id).ToList();

                var orderItemJbForms = DataContext.Set<OrderItem>().Where(o => o.ProgramScheduleId.HasValue && o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.initiated && o.ItemStatus != OrderItemStatusCategories.notInitiated && programScheduleIds.Contains(o.ProgramScheduleId.Value) && o.JbFormId.HasValue).Select(j => j.JbForm).ToList().Where(f => ChecKIfFirstAndLastNameEquals(f)).ToList();

                if (orderItemJbForms.Any())
                {

                    if (orderItemJbForms.Count() > 1)
                        Log($"{CurrentItem.Index} more than one participant exist, Jbforms: ({string.Join(", ", orderItemJbForms.Select(o => o.Id).ToList())})");

                    foreach (var itemJbForm in orderItemJbForms)
                    {

                        var teacherName = GetFormElement(itemJbForm, ElementsName.TeacherName.ToString(), SectionsName.SchoolSection.ToString());

                        if (teacherName == null)
                        {
                            Log($"{CurrentItem.Index} element is null");
                            return;
                        }

                        teacherName.SetValue(CurrentItem.TeacherName);

                        itemJbForm.JsonElements = JsonConvert.SerializeObject(itemJbForm.Elements);
                    }
                }
                else
                {
                    Log($"{CurrentItem.Index} Can't find participant");
                    return;
                }

            }
            catch (Exception ex)
            {
                Log($"{CurrentItem.Index} has total error: {ex.Message}");
            }
        }

        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }

        private bool ChecKIfFirstAndLastNameEquals(JbForm jbform)
        {
            if (jbform == null)
                return false;

            var parentSection = jbform.Elements.Single(e => e.Name == SectionsName.ParticipantSection.ToString()) as JbSection;

            var firstName = parentSection.Elements.Single(e => e.Name == ElementsName.FirstName.ToString()).GetValue()?.ToString();
            var lastName = parentSection.Elements.Single(e => e.Name == ElementsName.LastName.ToString()).GetValue()?.ToString();

            return firstName.Trim().Equals(CurrentItem.FirstName.Trim(), StringComparison.OrdinalIgnoreCase) && lastName.Trim().Equals(CurrentItem.LastName.Trim(), StringComparison.OrdinalIgnoreCase);
        }
    }

    public class DataManipulationFixInstallmentsPaidAmount : DataManipulationBaseModel<DataManipultaionTransactionsItem>
    {


        public override string Title
        {
            get
            {
                return "Fix installments paid amount";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 10;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        public List<DataManipultaionTransactionsItem> CurrentTransactions { get; set; }
        public DataManipultaionTransactionsItem CurrentItem { get; set; }


        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            var numberOfTransactions = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentTransactions = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfTransactions);

            FixPaidAmounts();

            FinalizeM();

            return this.Reslult;
        }

        private void FixPaidAmounts()
        {
            foreach (var item in CurrentTransactions)
            {
                CurrentItem = item;

                FixInstallmentsPaidAmount();

            }
        }

        private void FixInstallmentsPaidAmount()
        {
            try
            {
                var transactionId = Int64.Parse(CurrentItem.TransactionId);
                var transaction = DataContext.Set<TransactionActivity>().SingleOrDefault(c => c.Id == transactionId);

                if (transaction != null)
                {
                    var paymentDetail = transaction.PaymentDetail;
                    var installment = transaction.Installment;
                    var orderitem = transaction.OrderItem;
                    var json = JsonConvert.DeserializeObject<TransactionsViewModel>(paymentDetail.PaypalIPN);

                    if (json != null)
                    {
                        var dollarAmount = Math.Round(json.Amount / 100, 2);
                        var difference = transaction.Amount - dollarAmount;

                        installment.PaidAmount = installment.PaidAmount - difference;
                        orderitem.PaidAmount = orderitem.PaidAmount - difference;
                        transaction.Amount = transaction.Amount - difference;

                        Log($"{CurrentItem.Index} Updated");
                    }
                    else
                    {
                        Log($"{CurrentItem.Index} Can't find json or json.amount ");

                    }
                }
                else
                {
                    Log($"{CurrentItem.Index} Can't find transaction");

                }

            }
            catch (Exception ex)
            {
                Log($"{CurrentItem.Index} has total error: {ex.Message}");
            }

        }

    }

    public class DataManipulationFixDifferentCreditCards : DataManipulationBaseModel<DataManipultaionCreditCardsItem>
    {

        public override string Title
        {
            get
            {
                return "Fix different credit card";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 10;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        public List<DataManipultaionCreditCardsItem> CurrentCards { get; set; }
        public DataManipultaionCreditCardsItem CurrentItem { get; set; }


        public override DataManipulationGeneralResult Operate()
        {

            var from = From.Value - 1;

            FillSource();

            var numberOfTransactions = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentCards = SourceItems.OrderBy(o => o.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfTransactions);

            FixCreditCards();

            FinalizeM();

            return this.Reslult;
        }

        private void FixCreditCards()
        {
            foreach (var item in CurrentCards)
            {
                CurrentItem = item;

                FixDifferentCreditCard();

            }
        }

        private void FixDifferentCreditCard()
        {
            try
            {

                var DBCreditCard = DataContext.Set<UserCreditCard>().SingleOrDefault(c => !c.IsDeleted && c.CardId == CurrentItem.CardId);

                if (DBCreditCard != null)
                {

                    var card = GetCard(DBCreditCard.CustomerId, DBCreditCard.CardId);

                    if (card?.Last4 != DBCreditCard.LastDigits)
                    {

                        DBCreditCard.LastDigits = card.Last4;
                        DBCreditCard.Name = card.Name;
                        DBCreditCard.Brand = card.Brand;
                        DBCreditCard.ExpiryMonth = card.ExpirationMonth.ToString();
                        DBCreditCard.ExpiryYear = card.ExpirationYear.ToString();
                        DBCreditCard.Address.City = card.AddressCity;
                        DBCreditCard.Address.Country = card.AddressCountry;
                        DBCreditCard.Address.Zip = card.AddressZip;
                        DBCreditCard.Address.State = card.AddressState;
                        DBCreditCard.Address.Street = card.AddressLine1;
                        DBCreditCard.Address.Street2 = card.AddressLine2;
                        DBCreditCard.Address.Address = card.AddressLine1 + "," + card.AddressCity + "," + card.AddressState + " " + card.AddressZip + "," + card.AddressCountry;

                        Log($"{CurrentItem.Index} Updated");
                    }
                    else
                    {
                        Log($"{CurrentItem.Index} Cards are same ");

                    }
                }
                else
                {
                    Log($"{CurrentItem.Index} Can't find card in DB");

                }

            }
            catch (Exception ex)
            {
                Log($"{CurrentItem.Index} has total error: {ex.Message}");
            }

        }


        public static StripeCard GetCard(string customerId, string cardId)
        {

            var cardService = new StripeCardService("sk_live_2o4t7SfhBGNx3GgmdQgZWiiE");
            StripeCard stripeCard = cardService.Get(customerId, cardId);  ////Get Card

            return stripeCard;
        }
    }

    public class DataManipulationFixPayerType : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Fix payer type";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return DataContext.Set<TransactionActivity>().Where(p => p.TransactionCategory == TransactionCategory.Installment
            || p.TransactionCategory == TransactionCategory.Invoice).Count();
        }

        public List<TransactionActivity> CurrentInvoiceTransactions { get; set; }
        public IEnumerable<TransactionActivity> CurrentInstallmentTransactions { get; set; }
        public DataManipultaionCreditCardsItem CurrentItem { get; set; }


        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var numberOfTransactions = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentInstallmentTransactions = DataContext.Set<TransactionActivity>().OrderBy(o => o.Id).Skip(from)
                    .Where(p => p.TransactionCategory == TransactionCategory.Installment).Include("PaymentDetail")
                   .Take(NumbersInCycle.Value).ToList();

                CurrentInvoiceTransactions = DataContext.Set<TransactionActivity>().OrderBy(o => o.Id).Skip(from)
                    .Where(p => p.TransactionCategory == TransactionCategory.Invoice).Include("PaymentDetail").Take(NumbersInCycle.Value).ToList();
            }


            Initialize(numberOfTransactions);

            FixInvoicePayerType();
            FixInstallmentPayerType();

            FinalizeM();

            return this.Reslult;
        }

        private void FixInvoicePayerType()
        {
            var invoiceIds = CurrentInvoiceTransactions.Select(i => i?.PaymentDetail?.InvoiceId).ToList();
            var orderItemIds = CurrentInvoiceTransactions.Select(i => i?.OrderItemId).ToList();
            var invoiceHistories = DataContext.Set<InvoiceHistory>().Where(p => invoiceIds.Contains(p.InvoiceId) && p.Action == Invoicestatus.Paid).ToList();

            var orderHistories = DataContext.Set<OrderHistory>().Where(p => orderItemIds.Contains(p.OrderItemId)).ToList();

            foreach (var item in CurrentInvoiceTransactions)
            {
                if (item.PaymentDetail != null && item.PaymentDetail.InvoiceId.HasValue)
                {
                    var transactionInvoiceHistory = invoiceHistories?.Where(i => i.InvoiceId == item.PaymentDetail.InvoiceId && i.ActionDate >= item.TransactionDate).ToList();

                    if (transactionInvoiceHistory.Any())
                    {
                        var firstHistory = transactionInvoiceHistory.First();

                        if (firstHistory.Description.Contains("made") && item.PaymentDetail != null)
                        {
                            item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                        }
                        else
                        {
                            if (item.PaymentDetail != null)
                            {
                                item.PaymentDetail.PayerType = RoleCategoryType.Dashboard;
                            }
                        }
                    }
                    else
                    {
                        if (item.PaymentDetail != null)
                        {
                            item.PaymentDetail.PayerType = RoleCategoryType.Dashboard;
                        }
                    }

                }
                else
                {
                    var orderHistory = orderHistories.Where(p => p.OrderItemId == item.OrderItemId).ToList();

                    if (!orderHistory.Any() && item.PaymentDetail != null)
                    {
                        item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                    }
                    else
                    {
                        var history = orderHistory.Where(h => h.ActionDate >= item.TransactionDate).ToList();

                        if (history.Any())
                        {
                            var userId = history.First().UserId;

                            if (userId > 0)
                            {
                                var user = DataContext.Set<JbUser>().FirstOrDefault(u => u.Id == userId);

                                if (user == null && item.PaymentDetail != null)
                                {
                                    item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                                }
                                else
                                {
                                    var staff = Ioc.ClubBusiness.GetStaff(user.UserName, item.ClubId);

                                    if (staff != null && item.PaymentDetail != null)
                                    {
                                        item.PaymentDetail.PayerType = RoleCategoryType.Dashboard;
                                    }
                                    else
                                    {
                                        if (item.PaymentDetail != null)
                                        {
                                            item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                                        }

                                    }
                                }

                            }
                            else // success installment autocharge
                            {
                                if (item.PaymentDetail != null)
                                {
                                    item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                                }
                            }
                        }
                        else // success installment autocharge
                        {
                            if (item.PaymentDetail != null)
                            {
                                item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                            }
                        }

                    }
                }

                Log($"Invoice transaction with Id {item.Id } set payerType {item.PaymentDetail.PayerType} ");
            }
        }

        private void FixInstallmentPayerType()
        {
            var successTransactions = CurrentInstallmentTransactions.Where(p => p.TransactionStatus == TransactionStatus.Success);
            var failureTransactions = CurrentInstallmentTransactions.Where(p => p.TransactionStatus != TransactionStatus.Success);

            var orderItemIds = CurrentInvoiceTransactions.Select(i => i?.OrderItemId).ToList();
            var orderHistories = DataContext.Set<OrderHistory>().Where(p => orderItemIds.Contains(p.OrderItemId)).ToList();

            foreach (var item in successTransactions)
            {
                var orderHistory = orderHistories.Where(p => p.OrderItemId == item.OrderItemId).ToList();

                if (!orderHistory.Any())
                {
                    item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                }
                else
                {
                    var histories = orderHistory.Where(h => h.ActionDate >= item.TransactionDate).ToList();

                    if (histories.Any())
                    {
                        var userId = histories.First().UserId;

                        if (userId > 0)
                        {
                            var user = DataContext.Set<JbUser>().FirstOrDefault(u => u.Id == userId);

                            if (user == null)
                            {
                                item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                            }
                            else
                            {
                                var staff = Ioc.ClubBusiness.GetStaff(user.UserName, item.ClubId);

                                if (staff != null)
                                {
                                    item.PaymentDetail.PayerType = RoleCategoryType.Dashboard;
                                }
                                else
                                {
                                    item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                                }
                            }

                        }
                        else // success installment autocharge
                        {
                            item.PaymentDetail.PayerType = RoleCategoryType.System;
                        }
                    }
                    else // success installment autocharge
                    {
                        item.PaymentDetail.PayerType = RoleCategoryType.System;
                    }


                }
                Log($"Installment transaction with Id {item.Id } set payerType {item.PaymentDetail.PayerType} ");
            }

            foreach (var item in failureTransactions)
            {
                if (item.Installment != null)
                {
                    if (!item.Installment.EnableReminder)
                    {
                        item.PaymentDetail.PayerType = RoleCategoryType.System;
                    }
                    else
                    {
                        item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                    }
                }
                else
                {
                    item.PaymentDetail.PayerType = RoleCategoryType.Parent;
                }

                Log($"Installment transaction with Id {item.Id } set payerType {item.PaymentDetail.PayerType} ");
            }

        }

    }


    public class DataManipulationQueueEmailLoadTesting : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Queue email load testing";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return ElementItems.Count();
        }

        public List<int> ElementItems { get; set; } = new List<int>();

        private List<string> EmailAddresses { get; set; } = new List<string>();

        public int ClubId { get; set; }

        public override DataManipulationGeneralResult Operate()
        {
            for (int i = 0; i < 1000; i++)
            {
                ElementItems.Add(i + 1);
            }


            EmailAddresses = new List<string>
            {
                //"mostafa@jumbula.com",
                //"qa@jumbula.com",
                //"sabikeh@jumbula.com",
                //"hadi@jumbula.com",
            };

            for (int i = 1; i < 20; i++)
            {
                EmailAddresses.Add($"freetrial{i}@jumbula.com");
            }

            var elementItems = new List<int>();

            var allElementNumbers = GetNumberOfElements();


            var from = From.Value - 1;
            if (NumbersInCycle.HasValue)
            {
                elementItems = ElementItems.OrderBy(s => s).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                elementItems = ElementItems;
            }

            Initialize(allElementNumbers);

            ClubId = 12819;

            QueueEmails(elementItems);

            FinalizeM();

            return Reslult;
        }


        private void QueueEmails(List<int> elementItems)
        {
            var i = 0;

            foreach (var item in elementItems)
            {
                QueueEmail(EmailAddresses[i], ClubId);

                if (i >= (EmailAddresses.Count() - 1))
                    i = 0;
                else
                    i++;
            }
        }

        private void QueueEmail(string userName, int clubId)
        {
            var parameter = new ResetPasswordSuccessParameterModel
            {
                UserName = userName,
                ClubId = clubId
            };

            Ioc.EmailBusiness.Send(EmailCategory.ResetPasswordSuccess, parameter);
        }

    }


    public class DataManipulationCancelLastChargeForCancelOrders : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Update category last charge to cancel";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return DataContext.Set<OrderItem>().Where(p => p.PaymentPlanType == PaymentPlanType.Installment
                                                           && p.ItemStatusReason == OrderItemStatusReasons.canceled
            && p.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && p.ProgramTypeCategory != ProgramTypeCategory.Subscription).Count();
        }

        public List<OrderItem> CurrentOrderItem { get; set; }
        public IEnumerable<OrderItem> CurrentOrderItems { get; set; }
        public DataManipultaionCreditCardsItem CurrentItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var numberOfOrderItems = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = DataContext.Set<OrderItem>().Where(p => p.PaymentPlanType == PaymentPlanType.Installment
                                                                            && p.ItemStatusReason == OrderItemStatusReasons.canceled
                && p.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && p.ProgramTypeCategory != ProgramTypeCategory.Subscription)
                .OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfOrderItems);

            FixLastCharges();

            FinalizeM();

            return this.Reslult;
        }

        private void FixLastCharges()
        {
            foreach (var item in CurrentOrderItems)
            {
                if (item.OrderChargeDiscounts.Any(c => c.Name.ToLower() == "Cancel/edit fee".ToLower()) && !item.OrderChargeDiscounts.Any(c => c.Category == ChargeDiscountCategory.CancellationFee))
                {
                    var lastCharge = item.OrderChargeDiscounts.Where(c => c.Name.ToLower() == "Cancel/edit fee".ToLower()).Last();

                    lastCharge.Category = ChargeDiscountCategory.CancellationFee;
                    lastCharge.Name = "Cancellation fee";
                    lastCharge.Description = "Cancellation fee";

                    Log($"Order item with Id {item.Id } set last charge {lastCharge.Id} to cancel category ");
                }

            }
        }

    }

    public class DataManipulationDeleteInstallmentForCancelAndTansferOutOrders : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Delete installments and charges for transfer out and cancel order";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return DataContext.Set<OrderItem>().Where(p =>
            p.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare
            && p.ProgramTypeCategory != ProgramTypeCategory.Subscription &&
            (p.ItemStatusReason == OrderItemStatusReasons.canceled || p.ItemStatusReason == OrderItemStatusReasons.transferOut)).Count();
        }

        public List<OrderItem> CurrentOrderItem { get; set; }
        public IEnumerable<OrderItem> CurrentOrderItems { get; set; }
        public DataManipultaionCreditCardsItem CurrentItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var numberOfOrderItems = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = DataContext.Set<OrderItem>().Where(p => p.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare
            && p.ProgramTypeCategory != ProgramTypeCategory.Subscription &&
            (p.ItemStatusReason == OrderItemStatusReasons.canceled || p.ItemStatusReason == OrderItemStatusReasons.transferOut)).OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfOrderItems);

            FixDeleteInstallments();

            FinalizeM();

            return this.Reslult;
        }

        private void FixDeleteInstallments()
        {
            foreach (var item in CurrentOrderItems)
            {
                if (item.ItemStatusReason == OrderItemStatusReasons.canceled)
                {
                    if (item.Installments.Any())
                    {
                        item.Installments.ForEach(i => i.IsDeleted = true);
                        item.Installments.ForEach(i => i.DeleteReason = DeleteReason.Cancel);
                    }

                    if (item.OrderChargeDiscounts.Any())
                    {
                        item.OrderChargeDiscounts.Where(c => c.Category != ChargeDiscountCategory.CancellationFee).ToList().ForEach(i => i.IsDeleted = true);
                    }

                    Log($"Order item with Id {item.Id} delete all installments and charges");
                }
                else if (item.ItemStatusReason == OrderItemStatusReasons.transferOut)
                {
                    if (item.Installments.Any())
                    {
                        item.Installments.ForEach(i => i.IsDeleted = true);
                        item.Installments.ForEach(i => i.DeleteReason = DeleteReason.Transfer);
                    }

                    if (item.OrderChargeDiscounts.Any())
                    {
                        item.OrderChargeDiscounts.Where(c => c.Category != ChargeDiscountCategory.CancellationFee).ToList().ForEach(i => i.IsDeleted = true);
                    }

                    Log($"Order item with Id {item.Id} delete all installments and charges");
                }

            }
        }

    }

    public class DataManipulationAddInstallmentForCancelChargeAndUpdateInstallmentPaidAmount : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Add installment for cancel charge and update paid amount";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {

            var orderItemIdsNotUse = new List<long>() { 287096, 185526, 156109, 185375, 355600, 304900, 350727, 362567, 388799, 374472, 432892 };

            return DataContext.Set<OrderItem>().Where(p => p.ItemStatusReason == OrderItemStatusReasons.canceled
            && p.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && p.ProgramTypeCategory != ProgramTypeCategory.Subscription && !orderItemIdsNotUse.Contains(p.Id)).Count();
        }

        public List<OrderItem> CurrentOrderItem { get; set; }
        public IEnumerable<OrderItem> CurrentOrderItems { get; set; }
        public DataManipultaionCreditCardsItem CurrentItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var orderItemIdsNotUse = new List<long>() { 287096, 185526, 156109, 185375, 355600, 304900, 350727, 362567, 388799, 374472, 432892 };

            var numberOfOrderItems = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = DataContext.Set<OrderItem>().Where(p => p.ItemStatusReason == OrderItemStatusReasons.canceled
            && p.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && p.ProgramTypeCategory != ProgramTypeCategory.Subscription
            && !orderItemIdsNotUse.Contains(p.Id) && p.OrderChargeDiscounts.Any(c => c.Category == ChargeDiscountCategory.CancellationFee)).OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfOrderItems);

            AddInstallmentAddOnAndUpdateInstallmentPaidAmount();

            FinalizeM();

            return this.Reslult;
        }

        private void AddInstallmentAddOnAndUpdateInstallmentPaidAmount()
        {
            foreach (var item in CurrentOrderItems)
            {
                if (item.OrderChargeDiscounts.Any(p => p.Category == ChargeDiscountCategory.CancellationFee))
                {
                    var charge = item.OrderChargeDiscounts.LastOrDefault(p => p.Category == ChargeDiscountCategory.CancellationFee);
                    var orderItemHistory = DataContext.Set<OrderHistory>().Where(p => p.OrderItemId == item.Id && p.Action == OrderAction.Canceled);

                    if (charge != null)
                    {
                        if (item.Installments.Any())
                        {
                            var installment =
                              new OrderInstallment
                              {
                                  Amount = charge.Amount,
                                  InstallmentDate = orderItemHistory != null ? orderItemHistory.ToList().Last().ActionDate : DateTime.UtcNow,
                                  Token = null,
                                  PaidAmount = 0,
                                  PaidDate = null,
                                  NoticeSent = false,
                                  EnableReminder = false,
                                  Type = InstallmentType.AddOn,
                                  Status = OrderStatusCategories.initiated
                              };

                            item.Installments.Add(installment);
                            charge.OrderInstallment = installment;

                            if (item.Installments.Any(i => i.PaidAmount > 0))
                            {
                                foreach (var inst in item.Installments.Where(i => i.PaidAmount > 0 && i.Type != InstallmentType.AddOn))
                                {
                                    Ioc.OrderItemBusiness.UpdateAddOnInstallment(inst, installment, "support@jumbula.com");
                                }
                            }
                        }
                    }

                    Log($"Order item with Id {item.Id} Add new installment for cancellation charge");
                }
            }
        }

    }

    public class DataManipulationUpdateCancelOrderChargeDiscount : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Update cancel and transfer out item amounts";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return DataContext.Set<OrderItem>().Where(p => (p.ItemStatusReason == OrderItemStatusReasons.canceled || p.ItemStatusReason == OrderItemStatusReasons.transferOut || p.ItemStatusReason == OrderItemStatusReasons.refund)
            && p.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && p.ProgramTypeCategory != ProgramTypeCategory.Subscription).Count();
        }

        public List<OrderItem> CurrentOrderItem { get; set; }
        public IEnumerable<OrderItem> CurrentOrderItems { get; set; }
        public DataManipultaionCreditCardsItem CurrentItem { get; set; }

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();



            var numberOfOrderItems = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentOrderItems = DataContext.Set<OrderItem>().Where(p => (p.ItemStatusReason == OrderItemStatusReasons.canceled || p.ItemStatusReason == OrderItemStatusReasons.transferOut || p.ItemStatusReason == OrderItemStatusReasons.refund)
            && p.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && p.ProgramTypeCategory != ProgramTypeCategory.Subscription).OrderBy(o => o.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfOrderItems);

            AddCancelTransferOutAmounts();

            FinalizeM();

            return this.Reslult;
        }

        private void AddCancelTransferOutAmounts()
        {
            foreach (var item in CurrentOrderItems)
            {
                var charge = item.OrderChargeDiscounts.FirstOrDefault(p => !p.IsDeleted && p.Category == ChargeDiscountCategory.CancellationFee);
                var amount = charge != null ? charge.Amount : 0;

                item.Order.OrderAmount -= item.TotalAmount;
                item.TotalAmount = amount;
                item.Order.OrderAmount += amount;
                item.EntryFee = 0;

                Log($"Order item with Id {item.Id} updated amounts");

            }
        }

    }

    public class DataManipulationGenerateTransactionIdForFamilyCredit : DataManipulationBaseModel<DataManipulationSimpleSourceItemModel>
    {
        public override string Title
        {
            get
            {
                return "Generate transactionId for family credit transaction";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 100;
            }
        }

        public override int GetNumberOfElements()
        {
            return DataContext.Set<TransactionActivity>().Count(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Credit && (t.TransactionType == TransactionType.Refund || (t.TransactionType == TransactionType.Payment && t.PaymentDetail.PayerType == RoleCategoryType.System)));
        }
        public IEnumerable<TransactionActivity> CurrentTransactions { get; set; }
        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var numberOfTransactions = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                CurrentTransactions = DataContext.Set<TransactionActivity>().Where(t => t.PaymentDetail.PaymentMethod == PaymentMethod.Credit && (t.TransactionType == TransactionType.Refund || (t.TransactionType == TransactionType.Payment && t.PaymentDetail.PayerType == RoleCategoryType.System))).Include("PaymentDetail").OrderBy(c => c.Id).Skip(from).Take(NumbersInCycle.Value).ToList();
            }

            Initialize(numberOfTransactions);

            GenerateTransactionId();

            FinalizeM();

            return this.Reslult;
        }

        private void GenerateTransactionId()
        {
            var transactionBusiness = Ioc.TransactionActivityBusiness;

            foreach (var transaction in CurrentTransactions)
            {
                var transactionId = GeneratGuidTransactionId();

                transaction.PaymentDetail.TransactionId = transactionId;

                Log($"Generate transaction id for transaction {transaction.Id}");
            }
        }

        public string GeneratGuidTransactionId()
        {
            var guidTransactionId = "jb_";
            guidTransactionId += Guid.NewGuid().ToString("n");

            var isAnyTransactionId = DataContext.Set<TransactionActivity>().Where(t => t.HandleMode == HandleMode.Offline).Select(t => t.PaymentDetail).Any(o => o.TransactionId.Equals(guidTransactionId));

            var isAnyTransactionIdInLocal = DataContext.Set<TransactionActivity>().Local.Where(t => t.HandleMode == HandleMode.Offline).Select(t => t.PaymentDetail).Any(o => o.TransactionId.Equals(guidTransactionId));

            if (isAnyTransactionId || isAnyTransactionIdInLocal)
            {
                guidTransactionId = GeneratGuidTransactionId();
            }

            return guidTransactionId;
        }
    }


    public class DataManipulationAddEmailTemplate : DataManipulationBaseModel<DataManipulationAddEmailTemplateItem>
    {
        public override string Title
        {
            get
            {
                return "Add email templates";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 10;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        List<DataManipulationAddEmailTemplateItem> sourceItems;

        DataManipulationAddEmailTemplateItem CurrentSeason;

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var allElementNumbers = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                sourceItems = SourceItems.OrderBy(s => s.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                sourceItems = SourceItems;
            }

            Initialize(allElementNumbers);

            AddTemplates();

            FinalizeM();

            return Reslult;
        }


        private void AddTemplates()
        {
            foreach (var item in sourceItems)
            {
                CurrentSeason = item;

                AddTemplate();
            }
        }

        private void AddTemplate()
        {


            var school = DataContext.Set<Club>().SingleOrDefault(c => !c.IsDeleted && c.Domain.Equals(CurrentSeason.SchoolDomain, StringComparison.OrdinalIgnoreCase) && c.PartnerId.HasValue && c.PartnerClub.Domain.Equals(CurrentSeason.PartnerDomain, StringComparison.OrdinalIgnoreCase));

            if (school == null)
            {
                Log($"{CurrentSeason.Index} school doesn't exist");
                return;
            }

            Season season;

            var seasonName = EnumHelper.ParseEnum<SeasonNames>(CurrentSeason.Season);
            var seasonYear = int.Parse(CurrentSeason.Year);
            var seasons = DataContext.Set<Season>().Where(s => s.Status != SeasonStatus.Deleted && s.ClubId == school.Id && s.Title.Equals(CurrentSeason.SchoolSeasonTitle, StringComparison.OrdinalIgnoreCase) && s.Year == seasonYear && s.Name == seasonName).ToList();

            if (seasons.Count() == 0)
            {
                Log($"{CurrentSeason.Index} Season doesn't exist");
                return;
            }
            else if (seasons.Count() > 1)
            {
                Log($"{CurrentSeason.Index} more than one season exist");
                return;
            }
            else
            {
                season = seasons[0];
            }
            Season sourceSeason;
            var sourceSeasonName = EnumHelper.ParseEnum<SeasonNames>(CurrentSeason.SourceSeasonName);
            var sourceSeasonYear = int.Parse(CurrentSeason.SourceSeasonYear);
            var sourceSeasons = DataContext.Set<Season>().Where(s => s.Status != SeasonStatus.Deleted && s.ClubId == school.Id && s.Title.Equals(CurrentSeason.SourceSchoolSeasonTitle, StringComparison.OrdinalIgnoreCase) && s.Year == sourceSeasonYear && s.Name == sourceSeasonName).ToList();

            if (sourceSeasons.Count() == 0)
            {
                Log($"{CurrentSeason.Index} Source season doesn't exist");
                return;
            }
            else if (sourceSeasons.Count() > 1)
            {
                Log($"{CurrentSeason.Index} more than one source season exist");
                return;
            }
            else
            {
                sourceSeason = sourceSeasons[0];
            }

            if (season.Template == null)
            {
                season.Template = new EmailTemplate();
                season.Template.Club_Id = school.Id;
                season.Template.TemplateName = $"{season.Domain}Template";
                season.Template.Type = TemplateType.Registration;
            }
            if (sourceSeason.Template == null)
            {
                Log($"{CurrentSeason.Index} source season template is null");
                return;
            }
            season.Template.Template = sourceSeason.Template.Template;


            //DataContext.Set<EmailTemplate>().Add();
        }
    }


    public class DataManipulationAlabamaTeamOrders : DataManipulationBaseModel<DataManipulationAlabamaTeamOrdersItem>
    {
        public override string Title
        {
            get
            {
                return "Alabama team orders";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 10;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        List<DataManipulationAlabamaTeamOrdersItem> sourceItems;

        DataManipulationAlabamaTeamOrdersItem CurrentOrder;

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var allElementNumbers = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                sourceItems = SourceItems.OrderBy(s => s.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                sourceItems = SourceItems;
            }

            Initialize(allElementNumbers);

            AddChessOrders();

            FinalizeM();

            return Reslult;
        }


        private void AddChessOrders()
        {
            foreach (var item in sourceItems)
            {
                CurrentOrder = item;

                AddChessOrder();
            }
        }

        private void AddChessOrder()
        {
            var clubDomain = CurrentOrder.ClubDomain;

            if (HttpContext.Current.Request.IsLocal)
            {
                clubDomain = "qaclubsand";
            }

            var program = DataContext.Set<Program>().Single(p => p.Status != ProgramStatus.Deleted && p.Domain.Equals(CurrentOrder.Program, StringComparison.OrdinalIgnoreCase) && p.Club.Domain.Equals(clubDomain, StringComparison.OrdinalIgnoreCase) && p.Season.Domain.Equals(CurrentOrder.SeasonDomain, StringComparison.OrdinalIgnoreCase));

            var club = program.Club;

            var programSchedule = program.ProgramSchedules.Single(p => !p.IsDeleted);

            long programScheduleId = programSchedule.Id;

            var tuition = programSchedule.Charges.Single(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee && c.Name.Equals(CurrentOrder.Tuition, StringComparison.OrdinalIgnoreCase));

            var season = program.Season;

            var userName = CurrentOrder.Username.Replace(" ", string.Empty).Trim().ToLower();
            var user = DataContext.Set<JbUser>().SingleOrDefault(p => p.UserName.ToLower() == userName);

            if (user == null)
            {
                UserManager.PasswordValidator = new PasswordValidator
                {
                    RequiredLength = 8,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = false,
                    RequireUppercase = false,
                };

                var password = userName;
                user = new JbUser() { UserName = userName, Email = userName };
                var res = UserManager.Create(user, password);

            }

            if (!base.UserManager.IsEmailConfirmed(user.Id))
            {
                // confirm email
                UserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));

                string token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                UserManager.ConfirmEmail(user.Id, token);
            }


            #region Player
            var player = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.LastName.ToLower());
            if (player == null)
            {
                player = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.LastName.ToLower());
            }
            if (player == null)
            {
                var newPlayer = new PlayerProfile
                {
                    Contact = new ContactPerson
                    {
                        FirstName = CurrentOrder.FirstName,
                        LastName = CurrentOrder.LastName

                    },

                    Relationship = RelationshipType.Registrant,
                    Status = PlayerProfileStatusType.Active,
                    UserId = user.Id,

                };

                DataContext.Set<PlayerProfile>().Add(newPlayer);

                player = newPlayer;
            }

            #endregion

            var uTcDate = DateTime.UtcNow;
            var ists = DateTimeHelper.DateTimetoTimeStamp(uTcDate);

            var confirmationId = Ioc.OrderBusiness.GenerateConfirmationId();

            var result = new Order
            {
                ClubId = club.Id,
                UserId = user.Id,
                ConfirmationId = confirmationId,
                OrderMode = OrderMode.Offline,
                CompleteDate = uTcDate,
                SubmitDate = uTcDate,
                PaymentPlanType = PaymentPlanType.FullPaid,
                OrderStatus = OrderStatusCategories.completed,
                IsDraft = false,
                IsLive = true,

            };

            var itemName = Ioc.ProgramBusiness.GetProgramName(program.Name, programSchedule.StartDate, programSchedule.EndDate, "");

            itemName += " " + tuition.Amount.ToString().Replace(".00", "").Replace(".0", "");


            var orderItem = new OrderItem()
            {
                DateCreated = uTcDate,
                Start = programSchedule.StartDate,
                End = programSchedule.EndDate,
                EntryFee = tuition.Amount,
                EntryFeeName = tuition.Name,
                TotalAmount = tuition.Amount,
                FirstName = CurrentOrder.FirstName,
                LastName = CurrentOrder.LastName,
                ISTS = ists,
                ItemStatus = OrderItemStatusCategories.completed,
                ItemStatusReason = OrderItemStatusReasons.regular,
                Name = itemName,
                OrderChargeDiscounts = new List<OrderChargeDiscount>()
                {
                    new OrderChargeDiscount()
                    {
                        Amount = Convert.ToDecimal(tuition.Amount),
                        Category = ChargeDiscountCategory.EntryFee,
                        ChargeId = tuition.Id,
                        Description = tuition.Description,
                        Name = tuition.Name,
                        Order = result,
                        Subcategory = ChargeDiscountSubcategory.Charge
                    }
                },

                PaidAmount = 0,
                PaymentPlanType = PaymentPlanType.FullPaid,
                Player = player,
                ProgramSchedule = programSchedule,
                ProgramTypeCategory = program.TypeCategory,
                RegType = RegistrationType.AllSession,
                SeasonId = season.Id,
                OrderItemChess = new OrderItemChess
                {
                    Schedule = CurrentOrder.Schedule,
                    Section = CurrentOrder.Section,
                }
            };

            result.OrderItems.Add(orderItem);

            var USCF1Info = GetUSCFInfo(CurrentOrder.USCFID1);
            orderItem.OrderItemChess.Id = Convert.ToInt64(USCF1Info[0]);
            orderItem.OrderItemChess.UscfExpiration = USCF1Info[1];
            orderItem.OrderItemChess.UscfRatingReg = USCF1Info[3];

            var history = new OrderHistory
            {
                Action = OrderAction.Initiated,
                ActionDate = result.CompleteDate,
                Description = "Migrate orders",
                OrderId = result.Id,
                UserId = user.Id,
            };

            result.OrderAmount = Convert.ToDecimal(tuition.Amount);
            result.OrderHistories = new List<OrderHistory>();
            result.OrderHistories.Add(history);

            var form = program.ClubFormTemplates.Single(f => f.FormType == FormType.Registration);
            orderItem.JbForm = getJbForm(form.JbForm);

            if (!club.Users.Any(c => c.UserId == user.Id))
            {
                club.Users.Add(new ClubUser() { ClubId = club.Id, UserId = user.Id });
            }


            DataContext.Set<Order>().Add(result);
        }

        private JbForm getJbForm(JbForm form)
        {

            var result = new JbForm();

            // Team
            var teamNameElement = GetFormElement(form, "Team name_fXfzLf", "Team Information_vodlGD");
            teamNameElement.SetValue(CurrentOrder.TeamName);

            // Registrant 
            var firstNameElement = GetFormElement(form, ElementsName.FirstName.ToString(), SectionsName.ParticipantSection.ToString());
            firstNameElement.SetValue(CurrentOrder.FirstName);

            var lastNameElement = GetFormElement(form, ElementsName.LastName.ToString(), SectionsName.ParticipantSection.ToString());
            lastNameElement.SetValue(CurrentOrder.LastName);

            var emailElement = GetFormElement(form, "Email", SectionsName.ParticipantSection.ToString());
            emailElement.SetValue(CurrentOrder.EmailAddress);

            var organizationElement = GetFormElement(form, "Organization_SQFJGz", SectionsName.ParticipantSection.ToString());
            organizationElement.SetValue(CurrentOrder.Organization);

            var haveMmbershipElement = GetFormElement(form, "I have verified that all team members have Alabama Chess Federation memberships that will be active on the tournament date._QTOYFd", SectionsName.ParticipantSection.ToString());
            if (CurrentOrder.IHaveAlabamaChessFederationMemberships.ToLower() == "yes")
            {
                haveMmbershipElement.SetValue("True");
            }
            else
            {
                haveMmbershipElement.SetValue("False");
            }

            // Participant 1
            var firstName1Element = GetFormElement(form, "FirstName_1", "Participant_1");
            firstName1Element.SetValue(CurrentOrder.FirstName1);

            var lastName1Element = GetFormElement(form, "LastName_1", "Participant_1");
            lastName1Element.SetValue(CurrentOrder.LastName1);

            var grade1Element = GetFormElement(form, ElementsName.Grade.ToString(), "Participant_1");
            var grade = FormatGradeForRegForm(CurrentOrder.Grade1);

            if (grade == "0")
            {
                Log($"{CurrentOrder.Index} Grade1 is invalid.");
            }
            else
            {
                grade1Element.SetValue(grade);
            }

            var USCF1Element = GetFormElement(form, "USCF Membership #_ncZKlj", "Participant_1") as USCFInfo;
            var USCF1Info = GetUSCFInfo(CurrentOrder.USCFID1);
            SetUSCFInfo(USCF1Element, USCF1Info);

            // Participant 2
            var firstName2Element = GetFormElement(form, "FirstName_2", "Participant_2");
            firstName2Element.SetValue(CurrentOrder.FirstName2);

            var lastName2Element = GetFormElement(form, "LastName_2", "Participant_2");
            lastName2Element.SetValue(CurrentOrder.LastName2);

            var grade2Element = GetFormElement(form, ElementsName.Grade.ToString(), "Participant_2");
            grade = FormatGradeForRegForm(CurrentOrder.Grade2);

            if (grade == "0")
            {
                Log($"{CurrentOrder.Index} Grade2 is invalid.");
            }
            else
            {
                grade2Element.SetValue(grade);
            }

            var USCF2Element = GetFormElement(form, "USCF Membership #_IxdnFK", "Participant_2") as USCFInfo;
            var USCF2Info = GetUSCFInfo(CurrentOrder.USCFID2);
            SetUSCFInfo(USCF2Element, USCF2Info);

            // Participant 3
            var firstName3Element = GetFormElement(form, "FirstName_4", "Participant 3_GbYDiW");
            firstName3Element.SetValue(CurrentOrder.FirstName3);

            var lastName3Element = GetFormElement(form, "LastName_4", "Participant 3_GbYDiW");
            lastName3Element.SetValue(CurrentOrder.LastName3);

            var grade3Element = GetFormElement(form, ElementsName.Grade.ToString(), "Participant 3_GbYDiW");
            grade = FormatGradeForRegForm(CurrentOrder.Grade3);

            if (grade == "0")
            {
                Log($"{CurrentOrder.Index} Grade3 is invalid.");
            }
            else
            {
                grade3Element.SetValue(grade);
            }

            var USCF3Element = GetFormElement(form, "USCF Membership #_EXkGKM", "Participant 3_GbYDiW") as USCFInfo;
            var USCF3Info = GetUSCFInfo(CurrentOrder.USCFID3);
            SetUSCFInfo(USCF3Element, USCF3Info);

            // Participant 4
            var firstName4Element = GetFormElement(form, "FirstName_5", "Participant 4_Uksftq");
            firstName4Element.SetValue(CurrentOrder.FirstName4);

            var lastName4Element = GetFormElement(form, "LastName_5", "Participant 4_Uksftq");
            lastName4Element.SetValue(CurrentOrder.LastName4);
            if (!string.IsNullOrEmpty(CurrentOrder.Grade4))
            {
                var grade4Element = GetFormElement(form, ElementsName.Grade.ToString(), "Participant 4_Uksftq");
                grade = FormatGradeForRegForm(CurrentOrder.Grade4);

                if (grade == "0")
                {
                    Log($"{CurrentOrder.Index} Grade4 is invalid.");
                }
                else
                {
                    grade4Element.SetValue(grade);
                }
            }
            var USCF4Element = GetFormElement(form, "USCF Membership #_qjKBwW", "Participant 4_Uksftq") as USCFInfo;
            var USCF4Info = GetUSCFInfo(CurrentOrder.USCFID4);
            SetUSCFInfo(USCF4Element, USCF4Info);

            // Participant 5
            var firstName5Element = GetFormElement(form, "FirstName_6", "Participant 5_xdIDRp");
            firstName5Element.SetValue(CurrentOrder.FirstName5);

            var lastName5Element = GetFormElement(form, "LastName_6", "Participant 5_xdIDRp");
            lastName5Element.SetValue(CurrentOrder.LastName5);

            if (!string.IsNullOrEmpty(CurrentOrder.Grade5))
            {
                var grade5Element = GetFormElement(form, ElementsName.Grade.ToString(), "Participant 5_xdIDRp");
                grade = FormatGradeForRegForm(CurrentOrder.Grade5);

                if (grade == "0")
                {
                    Log($"{CurrentOrder.Index} Grade5 is invalid.");
                }
                else
                {
                    grade5Element.SetValue(grade);
                }
            }
            if (!string.IsNullOrEmpty(CurrentOrder.USCFID5))
            {
                var USCF5Element = GetFormElement(form, "USCF Membership #_gZWdOB", "Participant 5_xdIDRp") as USCFInfo;
                var USCF5Info = GetUSCFInfo(CurrentOrder.USCFID5);
                SetUSCFInfo(USCF5Element, USCF5Info);
            }
            // School
            var schooNameElement = GetFormElement(form, "Name", "SchoolSection");
            schooNameElement.SetValue(CurrentOrder.SchoolName);

            result.JsonElements = JsonConvert.SerializeObject(form.Elements);
            result.CreatedDate = DateTime.UtcNow;
            result.LastModifiedDate = DateTime.UtcNow;
            result.RefEntityName = "Order";
            result.ParentId = form.ParentId;

            return result;
        }
        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Contains(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Contains(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }
        private string FormatGradeForRegForm(string grade)
        {
            string result;
            grade = grade.ToLower();
            if (grade == "not in school")
            {
                result = "1";
            }
            else if (grade == "preschool - 3")
            {
                result = "20";
            }
            else if (grade == "preschool - 4")
            {
                result = "21";
            }
            else if (grade == "preschool")
            {
                result = "2";
            }
            else if (grade == "kindergarten")
            {
                result = "3";
            }
            else if (grade == "1")
            {
                result = "4";
            }
            else if (grade == "2")
            {
                result = "5";
            }
            else if (grade == "3")
            {
                result = "6";
            }
            else if (grade == "4")
            {
                result = "7";
            }
            else if (grade == "5")
            {
                result = "8";
            }
            else if (grade == "6")
            {
                result = "9";
            }
            else if (grade == "7")
            {
                result = "10";
            }
            else if (grade == "8")
            {
                result = "11";
            }
            else if (grade == "9")
            {
                result = "12";
            }
            else if (grade == "10")
            {
                result = "13";
            }
            else if (grade == "11")
            {
                result = "14";
            }
            else if (grade == "12")
            {
                result = "15";
            }
            else if (grade == "college")
            {
                result = "16";
            }
            else
            {
                result = "0";
            }
            return result;
        }
        private void SetUSCFInfo(USCFInfo uscfInfo, string[] data)
        {
            uscfInfo.UscfId = data[0];
            uscfInfo.UscfExpiration = data[1];
            uscfInfo.PlayerName = data[2];
            uscfInfo.UscfRatingReg = data[3];
        }
        private string[] GetUSCFInfo(string uscfId)
        {
            Uscf uscf = new Uscf();
            string[] data = uscf.UscfMemSearch(uscfId);

            Thread.Sleep(1000);

            return data;
        }
    }
    public class DataManipulationAlabamaSingleOrders : DataManipulationBaseModel<DataManipulationAlabamaSingleOrdersItem>
    {
        public override string Title
        {
            get
            {
                return "Alabama single orders";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 10;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        List<DataManipulationAlabamaSingleOrdersItem> sourceItems;

        DataManipulationAlabamaSingleOrdersItem CurrentOrder;

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var allElementNumbers = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                sourceItems = SourceItems.OrderBy(s => s.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                sourceItems = SourceItems;
            }

            Initialize(allElementNumbers);

            AddChessOrders();

            FinalizeM();

            return Reslult;
        }


        private void AddChessOrders()
        {
            foreach (var item in sourceItems)
            {
                CurrentOrder = item;

                AddChessOrder();
            }
        }

        private void AddChessOrder()
        {
            var clubDomain = CurrentOrder.ClubDomain;

            if (HttpContext.Current.Request.IsLocal)
            {
                clubDomain = "qaclubsand";
            }

            var program = DataContext.Set<Program>().Single(p => p.Status != ProgramStatus.Deleted && p.Domain.Equals(CurrentOrder.Program, StringComparison.OrdinalIgnoreCase) && p.Club.Domain.Equals(clubDomain, StringComparison.OrdinalIgnoreCase) && p.Season.Domain.Equals(CurrentOrder.SeasonDomain, StringComparison.OrdinalIgnoreCase));

            var club = program.Club;

            var programSchedule = program.ProgramSchedules.Single(p => !p.IsDeleted);

            long programScheduleId = programSchedule.Id;

            var tuition = programSchedule.Charges.Single(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee && c.Name.Equals(CurrentOrder.Tuition, StringComparison.OrdinalIgnoreCase));

            var season = program.Season;

            var userName = CurrentOrder.Username.Replace(" ", string.Empty).Trim().ToLower();
            var user = DataContext.Set<JbUser>().SingleOrDefault(p => p.UserName.ToLower() == userName);

            if (user == null)
            {
                UserManager.PasswordValidator = new PasswordValidator
                {
                    RequiredLength = 8,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = false,
                    RequireUppercase = false,
                };

                var password = userName;
                user = new JbUser() { UserName = userName, Email = userName };
                var res = UserManager.Create(user, password);

            }

            if (!base.UserManager.IsEmailConfirmed(user.Id))
            {
                // confirm email
                UserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));

                string token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                UserManager.ConfirmEmail(user.Id, token);
            }


            #region Player
            var player = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.LastName.ToLower());
            if (player == null)
            {
                player = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.FirstName.ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.LastName.ToLower());
            }
            if (player == null)
            {
                var newPlayer = new PlayerProfile
                {
                    Contact = new ContactPerson
                    {
                        FirstName = CurrentOrder.FirstName,
                        LastName = CurrentOrder.LastName

                    },

                    Relationship = RelationshipType.Registrant,
                    Status = PlayerProfileStatusType.Active,
                    UserId = user.Id,

                };

                DataContext.Set<PlayerProfile>().Add(newPlayer);

                player = newPlayer;
            }

            #endregion

            var uTcDate = DateTime.UtcNow;
            var ists = DateTimeHelper.DateTimetoTimeStamp(uTcDate);

            var confirmationId = Ioc.OrderBusiness.GenerateConfirmationId();

            var result = new Order
            {
                ClubId = club.Id,
                UserId = user.Id,
                ConfirmationId = confirmationId,
                OrderMode = OrderMode.Offline,
                CompleteDate = uTcDate,
                SubmitDate = uTcDate,
                PaymentPlanType = PaymentPlanType.FullPaid,
                OrderStatus = OrderStatusCategories.completed,
                IsDraft = false,
                IsLive = true,

            };

            var itemName = Ioc.ProgramBusiness.GetProgramName(program.Name, programSchedule.StartDate, programSchedule.EndDate, "");

            itemName += " " + tuition.Amount.ToString().Replace(".00", "").Replace(".0", "");


            var orderItem = new OrderItem()
            {
                DateCreated = uTcDate,
                Start = programSchedule.StartDate,
                End = programSchedule.EndDate,
                EntryFee = tuition.Amount,
                EntryFeeName = tuition.Name,
                TotalAmount = tuition.Amount,
                FirstName = CurrentOrder.FirstName,
                LastName = CurrentOrder.LastName,
                ISTS = ists,
                ItemStatus = OrderItemStatusCategories.completed,
                ItemStatusReason = OrderItemStatusReasons.regular,
                Name = itemName,
                OrderChargeDiscounts = new List<OrderChargeDiscount>()
                {
                    new OrderChargeDiscount()
                    {
                        Amount = Convert.ToDecimal(tuition.Amount),
                        Category = ChargeDiscountCategory.EntryFee,
                        ChargeId = tuition.Id,
                        Description = tuition.Description,
                        Name = tuition.Name,
                        Order = result,
                        Subcategory = ChargeDiscountSubcategory.Charge
                    }
                },

                PaidAmount = 0,
                PaymentPlanType = PaymentPlanType.FullPaid,
                Player = player,
                ProgramSchedule = programSchedule,
                ProgramTypeCategory = program.TypeCategory,
                RegType = RegistrationType.AllSession,
                SeasonId = season.Id,
                OrderItemChess = new OrderItemChess
                {
                    Schedule = CurrentOrder.Schedule,
                    Section = CurrentOrder.Section,
                }
            };

            result.OrderItems.Add(orderItem);

            #region Parent 1
            if (string.IsNullOrEmpty(CurrentOrder.ParentFirstName))
            {
                Log($"{CurrentOrder.Index} parent first name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.ParentLastName))
            {
                Log($"{CurrentOrder.Index} parent last name is required.");
                return;
            }
            
            if (string.IsNullOrEmpty(CurrentOrder.ParentEmail))
            {
                Log($"{CurrentOrder.Index} Parent  email is required.");
                return;
            }
            if (!IsvalidEmail(CurrentOrder.ParentEmail))
            {
                Log($"{CurrentOrder.Index} Parent  email is invalid.");
                return;
            }
            var parent1 = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.ParentFirstName.Trim().ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.ParentLastName.Trim().ToLower() && p.Relationship == RelationshipType.Parent);
            if (parent1 == null)
            {
                parent1 = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.ParentFirstName.Trim().ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.ParentLastName.Trim().ToLower() && p.Relationship == RelationshipType.Parent);
            }
            if (parent1 == null)
            {
                var newParent1 = new PlayerProfile
                {
                    Contact = new ContactPerson
                    {
                        FirstName = CurrentOrder.ParentFirstName.Trim(),
                        LastName = CurrentOrder.ParentLastName.Trim()
                    },
                    Relationship = RelationshipType.Parent,
                    Status = PlayerProfileStatusType.Active,
                    UserId = user.Id,
                };

                parent1 = newParent1;
                DataContext.Set<PlayerProfile>().Add(parent1);
            }
            parent1.Contact.Email = CurrentOrder.ParentEmail;
           
            if (!string.IsNullOrEmpty(CurrentOrder.ParentCellPhone))
            {
                var cellPhone = FormatPhone(CurrentOrder.ParentCellPhone);
                if (!IsValidPhone(cellPhone))
                {
                    Log($"{CurrentOrder.Index} Parent  cell phone is invalid.");
                    return;
                }
                parent1.Contact.Cell = cellPhone;
                parent1.Contact.Phone = cellPhone;
            }
            
            #endregion

            #region School
            var playerInfo = DataContext.Set<PlayerInfo>().SingleOrDefault(f => f.Id == player.InfoId);
            if (playerInfo == null)
            {
                playerInfo = DataContext.Set<PlayerInfo>().Local.SingleOrDefault(f => f.Id == player.InfoId);
            }
            if (playerInfo == null)
            {
                playerInfo = new PlayerInfo();
                player.Info = playerInfo;
            }
            
            if (!string.IsNullOrEmpty(CurrentOrder.Grade))
            {
                var grade = FormatGradeForProfile(CurrentOrder.Grade.Trim());
                player.Info.Grade = grade;
            }
            #endregion
            player.Contact.DoB = DateTime.Parse(CurrentOrder.DOB.Trim());
            player.Gender = FormatProfileGender(CurrentOrder.Gender);
            player.Contact.Email = CurrentOrder.Email.Trim();
            var history = new OrderHistory
            {
                Action = OrderAction.Initiated,
                ActionDate = result.CompleteDate,
                Description = "Migrate orders",
                OrderId = result.Id,
                UserId = user.Id,
            };

            result.OrderAmount = Convert.ToDecimal(tuition.Amount);
            result.OrderHistories = new List<OrderHistory>();
            result.OrderHistories.Add(history);

            var form = program.ClubFormTemplates.Single(f => f.FormType == FormType.Registration);
            orderItem.JbForm = getJbForm(form.JbForm);

            if (!club.Users.Any(c => c.UserId == user.Id))
            {
                club.Users.Add(new ClubUser() { ClubId = club.Id, UserId = user.Id });
            }


            DataContext.Set<Order>().Add(result);
        }

        private JbForm getJbForm(JbForm form)
        {

            var result = new JbForm();

            

            // Participant 
            var firstNameElement = GetFormElement(form, ElementsName.FirstName.ToString(), SectionsName.ParticipantSection.ToString());
            firstNameElement.SetValue(CurrentOrder.FirstName);

            var lastNameElement = GetFormElement(form, ElementsName.LastName.ToString(), SectionsName.ParticipantSection.ToString());
            lastNameElement.SetValue(CurrentOrder.LastName);

            var emailElement = GetFormElement(form, "Email", SectionsName.ParticipantSection.ToString());
            emailElement.SetValue(CurrentOrder.Email);

            var GenderElement = GetFormElement(form, ElementsName.Gender.ToString(), SectionsName.ParticipantSection.ToString());
            GenderElement.SetValue(FormatGenderRegForm(CurrentOrder.Gender));

            var dobElement = GetFormElement(form, ElementsName.DoB.ToString(), SectionsName.ParticipantSection.ToString());
            dobElement.SetValue(DateTime.Parse(CurrentOrder.DOB.Trim()).ToString("MM/dd/yyyy"));

            var USCFElement = GetFormElement(form, "USCF #_zAlBQw", SectionsName.ParticipantSection.ToString()) as USCFInfo;
            var USCFInfo = GetUSCFInfo(CurrentOrder.USCFId);
            SetUSCFInfo(USCFElement, USCFInfo);

            // Contact
            var addressElement = GetFormElement(form, ElementsName.Address.ToString(), SectionsName.ContactSection.ToString()) as JbAddress;
            addressElement.AddressLine1 = CurrentOrder.AddressLine1.Trim();
            addressElement.AddressLine2 = CurrentOrder.AddressLine2.Trim();
            addressElement.City = CurrentOrder.City.Trim();
            addressElement.Zip = CurrentOrder.Zip.Trim();
            addressElement.State = CurrentOrder.State.Trim();
            addressElement.Country = CurrentOrder.Country.Trim();

            var phoneElement = GetFormElement(form, ElementsName.Phone.ToString(), SectionsName.ContactSection.ToString());
            phoneElement.SetValue(CurrentOrder.Phone.Trim());

            // Parent
             
            var parentFirstNameElement = GetFormElement(form, ElementsName.FirstName.ToString(), SectionsName.ParentGuardianSection.ToString());
            parentFirstNameElement.SetValue(CurrentOrder.FirstName);

            var parentlastNameElement = GetFormElement(form, ElementsName.LastName.ToString(), SectionsName.ParentGuardianSection.ToString());
            parentlastNameElement.SetValue(CurrentOrder.LastName);

            var parentemailElement = GetFormElement(form, "Email", SectionsName.ParentGuardianSection.ToString());
            parentemailElement.SetValue(CurrentOrder.ParentEmail);

            var parentPhoneElement = GetFormElement(form, ElementsName.Phone.ToString(), SectionsName.ParentGuardianSection.ToString());
            phoneElement.SetValue(CurrentOrder.Phone.Trim());

            // School
            if (FormatGradeForRegForm(CurrentOrder.Grade)!="0")
            {
                var gradeElement = GetFormElement(form, ElementsName.Grade.ToString(), "SchoolSection");
                gradeElement.SetValue(FormatGradeForRegForm(CurrentOrder.Grade));
            }
            result.JsonElements = JsonConvert.SerializeObject(form.Elements);
            result.CreatedDate = DateTime.UtcNow;
            result.LastModifiedDate = DateTime.UtcNow;
            result.RefEntityName = "Order";
            result.ParentId = form.ParentId;

            return result;
        }
        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Contains(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Contains(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }
        private void SetUSCFInfo(USCFInfo uscfInfo, string[] data)
        {
            uscfInfo.UscfId = data[0];
            uscfInfo.UscfExpiration = data[1];
            uscfInfo.PlayerName = data[2];
            uscfInfo.UscfRatingReg = data[3];
        }
        private string[] GetUSCFInfo(string uscfId)
        {
            Uscf uscf = new Uscf();
            string[] data = uscf.UscfMemSearch(uscfId);

            Thread.Sleep(1000);

            return data;
        }
        private string FormatGradeForRegForm(string grade)
        {
            string result;
            grade = grade.ToLower();
            if (grade == "not in school")
            {
                result = "1";
            }
            else if (grade == "preschool - 3")
            {
                result = "20";
            }
            else if (grade == "preschool - 4")
            {
                result = "21";
            }
            else if (grade == "preschool")
            {
                result = "2";
            }
            else if (grade == "kindergarten")
            {
                result = "3";
            }
            else if (grade == "1")
            {
                result = "4";
            }
            else if (grade == "2")
            {
                result = "5";
            }
            else if (grade == "3")
            {
                result = "6";
            }
            else if (grade == "4")
            {
                result = "7";
            }
            else if (grade == "5")
            {
                result = "8";
            }
            else if (grade == "6")
            {
                result = "9";
            }
            else if (grade == "7")
            {
                result = "10";
            }
            else if (grade == "8")
            {
                result = "11";
            }
            else if (grade == "9")
            {
                result = "12";
            }
            else if (grade == "10")
            {
                result = "13";
            }
            else if (grade == "11")
            {
                result = "14";
            }
            else if (grade == "12")
            {
                result = "15";
            }
            else if (grade == "college")
            {
                result = "16";
            }
            else
            {
                result = "0";
            }
            return result;
        }
        private SchoolGradeType FormatGradeForProfile(string grade)
        {
            SchoolGradeType result;
            grade = grade.ToLower();
            if (grade == "not in school")
            {
                result = SchoolGradeType.NotInSchool;
            }
            else if (grade == "preschool - 3")
            {
                result = SchoolGradeType.PreSchool3;
            }
            else if (grade == "preschool - 4")
            {
                result = SchoolGradeType.PreSchool4;
            }
            else if (grade == "preschool")
            {
                result = SchoolGradeType.PreSchool;
            }
            else if (grade == "kindergarten")
            {
                result = SchoolGradeType.Kindergarten;
            }
            else if (grade == "1")
            {
                result = SchoolGradeType.S1;
            }
            else if (grade == "2")
            {
                result = SchoolGradeType.S2;
            }
            else if (grade == "3")
            {
                result = SchoolGradeType.S3;
            }
            else if (grade == "4")
            {
                result = SchoolGradeType.S4;
            }
            else if (grade == "5")
            {
                result = SchoolGradeType.S5;
            }
            else if (grade == "6")
            {
                result = SchoolGradeType.S6;
            }
            else if (grade == "7")
            {
                result = SchoolGradeType.S7;
            }
            else if (grade == "8")
            {
                result = SchoolGradeType.S8;
            }
            else if (grade == "9")
            {
                result = SchoolGradeType.S9;
            }
            else if (grade == "10")
            {
                result = SchoolGradeType.S10;
            }
            else if (grade == "11")
            {
                result = SchoolGradeType.S11;
            }
            else if (grade == "12")
            {
                result = SchoolGradeType.S12;
            }
            else
            {
                result = SchoolGradeType.College;
            }

            return result;
        }
        private GenderCategories FormatProfileGender(string gender)
        {
            gender = gender.ToLower().Trim();
            if (gender == "male")
            {
                return GenderCategories.Male;
            }
            else if (gender == "female")
            {
                return GenderCategories.Female;
            }
            else if (gender == "gender neutral")
            {
                return GenderCategories.GenderNeutral;
            }
            else
            {
                return GenderCategories.None;
            }
        }
        private DateTime? FormatDOB(string dob)
        {
            try
            {
                return Convert.ToDateTime(dob);
            }
            catch
            {
                return null;
            }
        }
        private bool IsvalidEmail(string email)
        {
            return Regex.IsMatch(email.Trim().ToLower(), Common.Constants.Constants.EmailRegex);
        }
        private bool IsValidPhone(string phone)
        {
            if (phone.Length >= 8 && phone.Length <= 15)
            {
                return true;
            }
            return false;
        }
        private string FormatPhone(string phone)
        {
            return phone.Trim().Replace("-", "").Replace("(", "").Replace(")", "");
        }
        private string FormatGenderRegForm(string gender)
        {
            var result ="";
            gender = gender.ToLower();
            if (gender == "male" || gender == "m")
            {
                result = "Male";
            }
            else if (gender == "female" || gender == "f")
            {
                result = "Female";
            }

            return result;
        }
    }
    public class DataManipulationImapactDojoOrders : DataManipulationBaseModel<DataManipulationImapactDojoOrdersItem>
    {
        public override string Title
        {
            get
            {
                return "Imapact Dojo orders";
            }
        }

        public override int? NumbersInCycle
        {
            get
            {
                return 10;
            }
        }

        public override int GetNumberOfElements()
        {
            return SourceItems.Count();
        }

        List<DataManipulationImapactDojoOrdersItem> sourceItems;

        DataManipulationImapactDojoOrdersItem CurrentOrder;

        public override DataManipulationGeneralResult Operate()
        {
            var from = From.Value - 1;

            FillSource();

            var allElementNumbers = GetNumberOfElements();

            if (NumbersInCycle.HasValue)
            {
                sourceItems = SourceItems.OrderBy(s => s.Index).Skip(from).Take(NumbersInCycle.Value).ToList();
            }
            else
            {
                sourceItems = SourceItems;
            }

            Initialize(allElementNumbers);

            AddOrders();

            FinalizeM();

            return Reslult;
        }


        private void AddOrders()
        {
            foreach (var item in sourceItems)
            {
                CurrentOrder = item;

                AddOrder();
            }
        }

        private void AddOrder()
        {
            var clubDomain = CurrentOrder.ClubDomain;

            if (HttpContext.Current.Request.IsLocal)
            {
                clubDomain = "qaclubsand";
            }

            var programs = DataContext.Set<Program>().Where(p => p.Status != ProgramStatus.Deleted && p.Name.Equals(CurrentOrder.Program, StringComparison.OrdinalIgnoreCase) && p.Club.Domain.Equals(clubDomain, StringComparison.OrdinalIgnoreCase) && p.Season.Domain.Equals(CurrentOrder.SeasonDomain, StringComparison.OrdinalIgnoreCase)).ToList();
            Program program;
            if (programs == null || programs.Count == 0)
            {
                Log($"{CurrentOrder.Index} program doesn't exist.");
                return;
            }
            else if (programs.Count > 1)
            {
                Log($"{CurrentOrder.Index} more than one program exist.");
                return;
            }
            else
            {
                program = programs[0];
            }

            var club = program.Club;

            var programSchedule = program.ProgramSchedules.Single(p => !p.IsDeleted);

            long programScheduleId = programSchedule.Id;

            var tuition = programSchedule.Charges.Single(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee && c.Name.Equals(CurrentOrder.Tuition, StringComparison.OrdinalIgnoreCase));

            var season = program.Season;

            var userName = CurrentOrder.Username.Replace(" ", string.Empty).Trim().ToLower();
            if (!IsvalidEmail(userName))
            {
                Log($"{CurrentOrder.Index} username is invalid.");
                return;
            }
            var user = DataContext.Set<JbUser>().SingleOrDefault(p => p.UserName.ToLower() == userName);

            if (user == null)
            {
                UserManager.PasswordValidator = new PasswordValidator
                {
                    RequiredLength = 8,
                    RequireNonLetterOrDigit = false,
                    RequireDigit = false,
                    RequireLowercase = false,
                    RequireUppercase = false,
                };

                var password = userName;
                user = new JbUser() { UserName = userName, Email = userName };
                var res = UserManager.Create(user, password);

            }

            if (!base.UserManager.IsEmailConfirmed(user.Id))
            {
                // confirm email
                UserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create("EmailConfirmation"));

                string token = UserManager.GenerateEmailConfirmationTokenAsync(user.Id).Result;
                UserManager.ConfirmEmail(user.Id, token);
            }


            #region Player
            if (string.IsNullOrEmpty(CurrentOrder.FirstName))
            {
                Log($"{CurrentOrder.Index} Participant first name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.LastName))
            {
                Log($"{CurrentOrder.Index} Participant last name is required.");
                return;
            }
            var player = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.FirstName.Trim().ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.LastName.Trim().ToLower() && p.Relationship == RelationshipType.Registrant);
            if (player == null)
            {
                player = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.FirstName.Trim().ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.LastName.Trim().ToLower() && p.Relationship == RelationshipType.Registrant);
            }
            if (player == null)
            {
                var newPlayer = new PlayerProfile
                {
                    Contact = new ContactPerson
                    {
                        FirstName = CurrentOrder.FirstName.Trim(),
                        LastName = CurrentOrder.LastName.Trim()
                    },
                    Relationship = RelationshipType.Registrant,
                    Status = PlayerProfileStatusType.Active,
                    UserId = user.Id,
                };

                DataContext.Set<PlayerProfile>().Add(newPlayer);

                player = newPlayer;
            }

            if (!string.IsNullOrEmpty(CurrentOrder.Gender))
            {
                var gender = FormatProfileGender(CurrentOrder.Gender);
                if (gender == GenderCategories.None)
                {
                    Log($"{CurrentOrder.Index} Gender is invalid.");
                    return;
                }
                player.Gender = gender;
            }

            if (!string.IsNullOrEmpty(CurrentOrder.DOB))
            {
                var dob = FormatDOB(CurrentOrder.DOB);
                if (dob == null)
                {
                    Log($"{CurrentOrder.Index} DOB is invalid.");
                    return;
                }
                player.Contact.DoB = dob;
            }
            if (!string.IsNullOrEmpty(CurrentOrder.Email))
            {
                if (!IsvalidEmail(CurrentOrder.Email))
                {
                    Log($"{CurrentOrder.Index} Email is invalid.");
                    return;
                }
                player.Contact.Email = CurrentOrder.Email;
            }
            


            #endregion

            #region Parent 1
            if (string.IsNullOrEmpty(CurrentOrder.Parent1FirstName))
            {
                Log($"{CurrentOrder.Index} parent1 first name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.Parent1LastName))
            {
                Log($"{CurrentOrder.Index} parent1 last name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.Parent1Email))
            {
                Log($"{CurrentOrder.Index} parent1 email is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.Parent1Email))
            {
                Log($"{CurrentOrder.Index} Parent 1 email is required.");
                return;
            }
            if (!IsvalidEmail(CurrentOrder.Parent1Email))
            {
                Log($"{CurrentOrder.Index} Parent 1 email is invalid.");
                return;
            }
            var parent1 = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.Parent1FirstName.Trim().ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.Parent1LastName.Trim().ToLower() && p.Relationship == RelationshipType.Parent);
            if (parent1 == null)
            {
                parent1 = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.Parent1FirstName.Trim().ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.Parent1LastName.Trim().ToLower() && p.Relationship == RelationshipType.Parent);
            }
            if (parent1 == null)
            {
                var newParent1 = new PlayerProfile
                {
                    Contact = new ContactPerson
                    {
                        FirstName = CurrentOrder.Parent1FirstName.Trim(),
                        LastName = CurrentOrder.Parent1LastName.Trim()
                    },
                    Relationship = RelationshipType.Parent,
                    Status = PlayerProfileStatusType.Active,
                    UserId = user.Id,
                };

                parent1 = newParent1;
                DataContext.Set<PlayerProfile>().Add(parent1);
            }
            parent1.Contact.Email = CurrentOrder.Parent1Email;
            if (!string.IsNullOrEmpty(CurrentOrder.Parent1Gender))
            {
                var gender = FormatProfileGender(CurrentOrder.Parent1Gender);
                if (gender == GenderCategories.None)
                {
                    Log($"{CurrentOrder.Index} Parent 1 Gender is invalid.");
                    return;
                }
                parent1.Gender = gender;
            }

            if (!string.IsNullOrEmpty(CurrentOrder.Parent1DOB))
            {
                var dob = FormatDOB(CurrentOrder.Parent1DOB);
                if (dob == null)
                {
                    Log($"{CurrentOrder.Index} Parent 1 DOB is invalid.");
                    return;
                }
                parent1.Contact.DoB = dob;
            }
            if (!string.IsNullOrEmpty(CurrentOrder.Parent1CellPhone))
            {
                var cellPhone = FormatPhone(CurrentOrder.Parent1CellPhone);
                if (!IsValidPhone(cellPhone))
                {
                    Log($"{CurrentOrder.Index} Parent 1 cell phone is invalid.");
                    return;
                }
                parent1.Contact.Cell = cellPhone;
                parent1.Contact.Phone = cellPhone;
            }
            if (!string.IsNullOrEmpty(CurrentOrder.Parent1Occupation))
            {
                parent1.Contact.Occupation = CurrentOrder.Parent1Occupation;
            }
            if (!string.IsNullOrEmpty(CurrentOrder.Parent1Employer))
            {
                parent1.Contact.Employer = CurrentOrder.Parent1Employer;
            }
            #endregion

            #region Parent 2
            if (string.IsNullOrEmpty(CurrentOrder.Parent2FirstName))
            {
                Log($"{CurrentOrder.Index} parent2 first name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.Parent2LastName))
            {
                Log($"{CurrentOrder.Index} parent2 last name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.Parent2Email))
            {
                Log($"{CurrentOrder.Index} parent2 email is required.");
                return;
            }
            if (!IsvalidEmail(CurrentOrder.Parent2Email))
            {
                Log($"{CurrentOrder.Index} parent2 email is invalid.");
                return;
            }
            var parent2 = DataContext.Set<PlayerProfile>().Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.Parent2FirstName.Trim().ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.Parent2LastName.Trim().ToLower() && p.Relationship == RelationshipType.Parent);
            if (parent2 == null)
            {
                parent2 = DataContext.Set<PlayerProfile>().Local.Where(u => u.UserId == user.Id)
                        .FirstOrDefault(p => p.Contact.FirstName.ToLower() == CurrentOrder.Parent2FirstName.Trim().ToLower() && p.Contact.LastName.ToLower() == CurrentOrder.Parent2LastName.Trim().ToLower() && p.Relationship == RelationshipType.Parent);
            }
            if (parent2 == null)
            {
                var newParent2 = new PlayerProfile
                {
                    Contact = new ContactPerson
                    {
                        FirstName = CurrentOrder.Parent2FirstName.Trim(),
                        LastName = CurrentOrder.Parent2LastName.Trim()
                    },
                    Relationship = RelationshipType.Parent,
                    Status = PlayerProfileStatusType.Active,
                    UserId = user.Id,
                };

                parent2 = newParent2;
                DataContext.Set<PlayerProfile>().Add(parent2);
            }
            if (!string.IsNullOrEmpty(CurrentOrder.Parent2Gender))
            {
                var gender = FormatProfileGender(CurrentOrder.Parent2Gender);
                if (gender == GenderCategories.None)
                {
                    Log($"{CurrentOrder.Index} Parent 2 Gender is invalid.");
                    return;
                }
                parent2.Gender = gender;
            }

            if (!string.IsNullOrEmpty(CurrentOrder.Parent2DOB))
            {
                var dob = FormatDOB(CurrentOrder.Parent2DOB);
                if (dob == null)
                {
                    Log($"{CurrentOrder.Index} Parent 2 DOB is invalid.");
                    return;
                }
                parent2.Contact.DoB = dob;
            }
            parent2.Contact.Email = CurrentOrder.Parent2Email;
            if (!string.IsNullOrEmpty(CurrentOrder.Parent2CellPhone))
            {
                var cellPhone = FormatPhone(CurrentOrder.Parent2CellPhone);
                if (!IsValidPhone(cellPhone))
                {
                    Log($"{CurrentOrder.Index} Parent 2 cell phone is invalid.");
                    return;
                }
                parent2.Contact.Cell = cellPhone;
                parent2.Contact.Phone = cellPhone;
            }
            if (!string.IsNullOrEmpty(CurrentOrder.Parent2Occupation))
            {
                parent2.Contact.Occupation = CurrentOrder.Parent2Occupation;
            }
            if (!string.IsNullOrEmpty(CurrentOrder.Parent2Employer))
            {
                parent2.Contact.Employer = CurrentOrder.Parent2Employer;
            }
            #endregion

            var family = DataContext.Set<Family>().SingleOrDefault(f => f.User.Id == user.Id);
            if (family == null)
            {
                family = DataContext.Set<Family>().Local.SingleOrDefault(f => f.User.Id == user.Id);
            }
            if (family == null)
            {
                family = new Family()
                {
                    User = user
                };
            }

            #region Emergency contact
            if (string.IsNullOrEmpty(CurrentOrder.EmergencyFirstName))
            {
                Log($"{CurrentOrder.Index} Emergency first name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.EmergencyLastName))
            {
                Log($"{CurrentOrder.Index} Emergency last name is required.");
                return;
            }
            if (string.IsNullOrEmpty(CurrentOrder.EmergencyEmail))
            {
                Log($"{CurrentOrder.Index} Emergency email is required.");
                return;
            }
            if (!IsvalidEmail(CurrentOrder.EmergencyEmail))
            {
                Log($"{CurrentOrder.Index} Emergency email is invalid.");
                return;
            }
            var emergency = DataContext.Set<FamilyContact>().SingleOrDefault(f => f.FamilyId == family.Id && f.Type == FamilyContactType.Emergency && f.Contact.FirstName.ToLower() == CurrentOrder.EmergencyFirstName.Trim().ToLower() && f.Contact.LastName.ToLower() == CurrentOrder.EmergencyLastName.Trim().ToLower());
            if (emergency == null)
            {
                emergency = DataContext.Set<FamilyContact>().Local.SingleOrDefault(f => f.FamilyId == family.Id && f.Type == FamilyContactType.Emergency && f.Contact.FirstName.ToLower() == CurrentOrder.EmergencyFirstName.Trim().ToLower() && f.Contact.LastName.ToLower() == CurrentOrder.EmergencyLastName.Trim().ToLower());
            }
            if (emergency == null)
            {
                emergency = new FamilyContact();
                emergency.Family = family;
                emergency.Status = FamilyContactStatusType.Active;
                emergency.Type = FamilyContactType.Emergency;
                emergency.Contact = new ContactPerson()
                {
                    FirstName = CurrentOrder.EmergencyFirstName.Trim(),
                    LastName = CurrentOrder.EmergencyLastName.Trim()
                };
                DataContext.Set<FamilyContact>().Add(emergency);
            }
            
            emergency.Contact.Email = CurrentOrder.EmergencyEmail;

            if(!string.IsNullOrEmpty(CurrentOrder.EmergencyHomePhone))
            {
                var phone = FormatPhone(CurrentOrder.EmergencyHomePhone);
                if(!IsValidPhone(phone))
                {
                    Log($"{CurrentOrder.Index} Emergency Home Phone is invalid.");
                    return;
                }
                emergency.Contact.Phone = phone;
            }

            if (!string.IsNullOrEmpty(CurrentOrder.EmergencyWorkPhone))
            {
                var phone = FormatPhone(CurrentOrder.EmergencyWorkPhone);
                if (!IsValidPhone(phone))
                {
                    Log($"{CurrentOrder.Index} Emergency Work Phone is invalid.");
                    return;
                }
                emergency.Contact.Work = phone;
            }
            #endregion

            #region School
            var playerInfo = DataContext.Set<PlayerInfo>().SingleOrDefault(f => f.Id == player.InfoId);
            if (playerInfo == null)
            {
                playerInfo = DataContext.Set<PlayerInfo>().Local.SingleOrDefault(f => f.Id == player.InfoId);
            }
            if (playerInfo == null)
            {
                playerInfo = new PlayerInfo();
                player.Info = playerInfo;
            }
            playerInfo.SchoolName = CurrentOrder.SchoolName.Trim();
            if (!string.IsNullOrEmpty(CurrentOrder.Grade))
            {
                var grade = FormatGradeForProfile(CurrentOrder.Grade.Trim());
                player.Info.Grade = grade;
            }
            #endregion

            #region Insurance
            if (!string.IsNullOrEmpty(CurrentOrder.CompanyName))
            {
                family.InsuranceCompanyName = CurrentOrder.CompanyName.Trim();
            }
            if (!string.IsNullOrEmpty(CurrentOrder.PolicyNumber))
            {
                family.InsuranceCompanyName = CurrentOrder.PolicyNumber.Trim();
            }
            #endregion

            #region Medical
            if (!string.IsNullOrEmpty(CurrentOrder.HaveMedicalConditions))
            {
                if (CurrentOrder.HaveMedicalConditions.Trim().ToLower() == "yes")
                {
                    playerInfo.HasSpecialNeeds = true;
                }
                else
                {
                    playerInfo.HasSpecialNeeds = false;
                }
            }
            
            playerInfo.SpecialNeeds = CurrentOrder.MedicalConditions.Trim();

            if (!string.IsNullOrEmpty(CurrentOrder.HaveAllergies))
            {
                if (CurrentOrder.HaveAllergies.Trim().ToLower() == "yes")
                {
                    playerInfo.HasAllergies = true;
                }
                else
                {
                    playerInfo.HasAllergies = false;
                }
            }

            playerInfo.Allergies = CurrentOrder.Allergies.Trim();
             
            #endregion

            var uTcDate = DateTime.UtcNow;
            var ists = DateTimeHelper.DateTimetoTimeStamp(uTcDate);

            var confirmationId = Ioc.OrderBusiness.GenerateConfirmationId();

            var result = new Order
            {
                ClubId = club.Id,
                UserId = user.Id,
                ConfirmationId = confirmationId,
                OrderMode = OrderMode.Offline,
                CompleteDate = uTcDate,
                SubmitDate = uTcDate,
                PaymentPlanType = PaymentPlanType.FullPaid,
                OrderStatus = OrderStatusCategories.completed,
                IsDraft = false,
                IsLive = true,

            };

            var itemName = Ioc.ProgramBusiness.GetProgramName(program.Name, programSchedule.StartDate, programSchedule.EndDate, "");

            itemName += " " + tuition.Amount.ToString().Replace(".00", "").Replace(".0", "");


            var orderItem = new OrderItem()
            {
                DateCreated = uTcDate,
                Start = programSchedule.StartDate,
                End = programSchedule.EndDate,
                EntryFee = tuition.Amount,
                EntryFeeName = tuition.Name,
                TotalAmount = tuition.Amount,
                FirstName = CurrentOrder.FirstName,
                LastName = CurrentOrder.LastName,
                ISTS = ists,
                ItemStatus = OrderItemStatusCategories.completed,
                ItemStatusReason = OrderItemStatusReasons.regular,
                Name = itemName,
                OrderChargeDiscounts = new List<OrderChargeDiscount>()
                {
                    new OrderChargeDiscount()
                    {
                        Amount = Convert.ToDecimal(tuition.Amount),
                        Category = ChargeDiscountCategory.EntryFee,
                        ChargeId = tuition.Id,
                        Description = tuition.Description,
                        Name = tuition.Name,
                        Order = result,
                        Subcategory = ChargeDiscountSubcategory.Charge
                    }
                },

                PaidAmount = tuition.Amount,
                PaymentPlanType = PaymentPlanType.FullPaid,
                Player = player,
                ProgramSchedule = programSchedule,
                ProgramTypeCategory = program.TypeCategory,
                RegType = RegistrationType.AllSession,
                SeasonId = season.Id,

            };

            result.OrderItems.Add(orderItem);
            #region Transaction
            PaymentDetail paymentdetail = new PaymentDetail()
            {
                PaymentMethod = PaymentMethod.Cash,
                Status = PaymentDetailStatus.COMPLETED,
                PayerEmail = CurrentOrder.Username,
                PayerType = RoleCategoryType.Dashboard
            };

            TransactionActivity transaction = new TransactionActivity()
            {
                Amount = tuition.Amount,
                Club = club,
                HandleMode = HandleMode.Offline,
                IsAutoCharge = false,
                Order = result,
                OrderItem = orderItem,
                PaymentDetail = paymentdetail,
                Season = season,
                TransactionCategory = TransactionCategory.TakePayment,
                TransactionDate = uTcDate,
                TransactionStatus = TransactionStatus.Success,
                TransactionType = TransactionType.Payment
            };
            transaction.MetaData = new MetaData()
            {
                DateCreated = uTcDate,
                DateUpdated = uTcDate
            };
            DataContext.Set<TransactionActivity>().Add(transaction);
            #endregion
            var history = new OrderHistory
            {
                Action = OrderAction.Initiated,
                ActionDate = result.CompleteDate,
                Description = "Migrate orders",
                OrderId = result.Id,
                UserId = user.Id,
            };

            result.OrderAmount = Convert.ToDecimal(tuition.Amount);
            result.OrderHistories = new List<OrderHistory>();
            result.OrderHistories.Add(history);

            var form = program.ClubFormTemplates.Single(f => f.FormType == FormType.Registration);
            orderItem.JbForm = getJbForm(form.JbForm);

            if (!club.Users.Any(c => c.UserId == user.Id))
            {
                club.Users.Add(new ClubUser() { ClubId = club.Id, UserId = user.Id });
            }


            DataContext.Set<Order>().Add(result);
        }

        private JbForm getJbForm(JbForm form)
        {

            var result = new JbForm();

            // Participant 
            var firstNameElement = GetFormElement(form, ElementsName.FirstName.ToString(), SectionsName.ParticipantSection.ToString());
            firstNameElement.SetValue(CurrentOrder.FirstName.Trim());

            var lastNameElement = GetFormElement(form, ElementsName.LastName.ToString(), SectionsName.ParticipantSection.ToString());
            lastNameElement.SetValue(CurrentOrder.LastName.Trim());

            var GenderElement = GetFormElement(form, ElementsName.Gender.ToString(), SectionsName.ParticipantSection.ToString());
            GenderElement.SetValue(CurrentOrder.Gender.Trim());

            var dobElement = GetFormElement(form, ElementsName.DoB.ToString(), SectionsName.ParticipantSection.ToString());
            dobElement.SetValue(CurrentOrder.DOB.Trim());

            var emailElement = GetFormElement(form, "Email", SectionsName.ParticipantSection.ToString());
            emailElement.SetValue(CurrentOrder.Email.Trim());

            var addressElement = GetFormElement(form, ElementsName.Address.ToString(), SectionsName.ContactSection.ToString()) as JbAddress;
            addressElement.AddressLine1 = CurrentOrder.AddressLine1.Trim();
            addressElement.AddressLine2 = CurrentOrder.AddressLine2.Trim();
            addressElement.City = CurrentOrder.City.Trim();
            addressElement.Zip = CurrentOrder.Zip.Trim();
            addressElement.State = CurrentOrder.State.Trim();
            addressElement.Country = CurrentOrder.Country.Trim();

            var phoneElement = GetFormElement(form, ElementsName.Phone.ToString(), SectionsName.ContactSection.ToString());
            phoneElement.SetValue(CurrentOrder.Phone.Trim());
            // Parent 1
            var parent1FirstNameElement = GetFormElement(form, ElementsName.FirstName.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1FirstNameElement.SetValue(CurrentOrder.Parent1FirstName.Trim());

            var parent1LastNameElement = GetFormElement(form, ElementsName.LastName.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1LastNameElement.SetValue(CurrentOrder.Parent1LastName.Trim());

            var parent1EmailElement = GetFormElement(form, ElementsName.Email.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1EmailElement.SetValue(CurrentOrder.Parent1Email.Trim());

            var parent1GenderElement = GetFormElement(form, ElementsName.Gender.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1GenderElement.SetValue(CurrentOrder.Parent1Gender.Trim());

            var parent1dobElement = GetFormElement(form, ElementsName.DateOfBirth.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1dobElement.SetValue(CurrentOrder.Parent2DOB.Trim());

            var parent1PhoneElement = GetFormElement(form, ElementsName.Phone.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1PhoneElement.SetValue(CurrentOrder.Parent1CellPhone.Trim());

            var parent1OccupationElement = GetFormElement(form, ElementsName.Occupation.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1OccupationElement.SetValue(CurrentOrder.Parent1Occupation.Trim());

            var parent1EmployerElement = GetFormElement(form, ElementsName.Employer.ToString(), SectionsName.ParentGuardianSection.ToString());
            parent1EmployerElement.SetValue(CurrentOrder.Parent1Employer.Trim());
            // Parent 2
            var parent2FirstNameElement = GetFormElement(form, ElementsName.FirstName.ToString(), SectionsName.Parent2GuardianSection.ToString());
            parent2FirstNameElement.SetValue(CurrentOrder.Parent2FirstName.Trim());

            var parent2LastNameElement = GetFormElement(form, ElementsName.LastName.ToString(), SectionsName.Parent2GuardianSection.ToString());
            parent2LastNameElement.SetValue(CurrentOrder.Parent2LastName.Trim());

            var parent2EmailElement = GetFormElement(form, ElementsName.Email.ToString(), SectionsName.Parent2GuardianSection.ToString());
            parent2EmailElement.SetValue(CurrentOrder.Parent2Email.Trim());

            var parent2GenderElement = GetFormElement(form, ElementsName.Gender.ToString(), SectionsName.Parent2GuardianSection.ToString());
            parent2GenderElement.SetValue(CurrentOrder.Parent2Gender.Trim());

            var parent2dobElement = GetFormElement(form, ElementsName.DateOfBirth.ToString(), SectionsName.Parent2GuardianSection.ToString());
            parent2dobElement.SetValue(CurrentOrder.Parent2DOB.Trim());

            var parent2PhoneElement = GetFormElement(form, ElementsName.Phone.ToString(), SectionsName.Parent2GuardianSection.ToString());
            parent2PhoneElement.SetValue(CurrentOrder.Parent2CellPhone.Trim());

            var parent2OccupationElement = GetFormElement(form, ElementsName.Occupation.ToString(), SectionsName.Parent2GuardianSection.ToString());
            parent2OccupationElement.SetValue(CurrentOrder.Parent2Occupation.Trim());

            var parent2EmployerElement = GetFormElement(form, ElementsName.Employer.ToString(), SectionsName.Parent2GuardianSection.ToString());
            parent2EmployerElement.SetValue(CurrentOrder.Parent2Employer.Trim());

            // Emergency contact
            var emergencyFirstNameElement = GetFormElement(form, ElementsName.FirstName.ToString(), SectionsName.EmergencyContactSection.ToString());
            emergencyFirstNameElement.SetValue(CurrentOrder.EmergencyFirstName.Trim());

            var emergencyLastNameElement = GetFormElement(form, ElementsName.LastName.ToString(), SectionsName.EmergencyContactSection.ToString());
            emergencyLastNameElement.SetValue(CurrentOrder.EmergencyLastName.Trim());

            var emergencyEmailElement = GetFormElement(form, ElementsName.EmailAddress.ToString(), SectionsName.EmergencyContactSection.ToString());
            emergencyEmailElement.SetValue(CurrentOrder.EmergencyEmail.Trim());

            var emergencyHomePhoneElement = GetFormElement(form, ElementsName.HomePhone.ToString(), SectionsName.EmergencyContactSection.ToString());
            emergencyHomePhoneElement.SetValue(CurrentOrder.EmergencyHomePhone.Trim());

            var emergencyWorkPhoneElement = GetFormElement(form, ElementsName.WorkPhone.ToString(), SectionsName.EmergencyContactSection.ToString());
            emergencyWorkPhoneElement.SetValue(CurrentOrder.EmergencyWorkPhone.Trim());

            // School

            var schoolNameElement = GetFormElement(form, ElementsName.Name.ToString(), SectionsName.SchoolSection.ToString());
            schoolNameElement.SetValue(CurrentOrder.SchoolName.Trim());

            var gradeElement = GetFormElement(form, ElementsName.Grade.ToString(), SectionsName.SchoolSection.ToString());
            gradeElement.SetValue(FormatGradeForRegForm(CurrentOrder.Grade.Trim()));

            var teacherElement = GetFormElement(form, ElementsName.TeacherName.ToString(), SectionsName.SchoolSection.ToString());
            teacherElement.SetValue(CurrentOrder.TeacherName.Trim());

            // Insurance

            var companyElement = GetFormElement(form, ElementsName.CompanyName.ToString(), SectionsName.InsuranceSection.ToString());
            companyElement.SetValue(CurrentOrder.CompanyName.Trim());

            var policyElement = GetFormElement(form, ElementsName.PolicyNumber.ToString(), SectionsName.InsuranceSection.ToString());
            policyElement.SetValue(CurrentOrder.PolicyNumber.Trim());

            // Medical

            var hasMedicalElement = GetFormElement(form, ElementsName.HasMedicalConditions.ToString(), SectionsName.HealthSection.ToString());
            if (CurrentOrder.HaveMedicalConditions.Trim().ToLower() == "yes")
            {
                hasMedicalElement.SetValue("1");
            }
            else if(CurrentOrder.HaveMedicalConditions.Trim().ToLower() == "no")
            {
                hasMedicalElement.SetValue("0");
            }
            var medicalElement = GetFormElement(form, ElementsName.MedicalConditions.ToString(), SectionsName.HealthSection.ToString());
            medicalElement.SetValue(CurrentOrder.MedicalConditions);

            var hasAllergyElement = GetFormElement(form, ElementsName.HasAllergiesInfo.ToString(), SectionsName.HealthSection.ToString());
            if (CurrentOrder.HaveAllergies.Trim().ToLower() == "yes")
            {
                hasAllergyElement.SetValue("1");
            }
            else if (CurrentOrder.HaveMedicalConditions.Trim().ToLower() == "no")
            {
                hasAllergyElement.SetValue("0");
            }
            var allergyElement = GetFormElement(form, ElementsName.AllergiesInfo.ToString(), SectionsName.HealthSection.ToString());
            allergyElement.SetValue(CurrentOrder.Allergies);

            result.JsonElements = JsonConvert.SerializeObject(form.Elements);
            result.CreatedDate = DateTime.UtcNow;
            result.LastModifiedDate = DateTime.UtcNow;
            result.RefEntityName = "Order";
            result.ParentId = form.ParentId;

            return result;
        }
        private IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Contains(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Contains(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }
        private string FormatGradeForRegForm(string grade)
        {
            string result;
            grade = grade.ToLower();
            if (grade == "not in school")
            {
                result = "1";
            }
            else if (grade == "preschool - 3")
            {
                result = "20";
            }
            else if (grade == "preschool - 4")
            {
                result = "21";
            }
            else if (grade == "preschool")
            {
                result = "2";
            }
            else if (grade == "kindergarten")
            {
                result = "3";
            }
            else if (grade == "1")
            {
                result = "4";
            }
            else if (grade == "2")
            {
                result = "5";
            }
            else if (grade == "3")
            {
                result = "6";
            }
            else if (grade == "4")
            {
                result = "7";
            }
            else if (grade == "5")
            {
                result = "8";
            }
            else if (grade == "6")
            {
                result = "9";
            }
            else if (grade == "7")
            {
                result = "10";
            }
            else if (grade == "8")
            {
                result = "11";
            }
            else if (grade == "9")
            {
                result = "12";
            }
            else if (grade == "10")
            {
                result = "13";
            }
            else if (grade == "11")
            {
                result = "14";
            }
            else if (grade == "12")
            {
                result = "15";
            }
            else if (grade == "college")
            {
                result = "16";
            }
            else
            {
                result = "0";
            }
            return result;
        }
        private SchoolGradeType FormatGradeForProfile(string grade)
        {
            SchoolGradeType result ;
            grade = grade.ToLower();
            if (grade == "not in school")
            {
                result = SchoolGradeType.NotInSchool;
            }
            else if (grade == "preschool - 3")
            {
                result = SchoolGradeType.PreSchool3;
            }
            else if (grade == "preschool - 4")
            {
                result = SchoolGradeType.PreSchool4;
            }
            else if (grade == "preschool")
            {
                result = SchoolGradeType.PreSchool;
            }
            else if (grade == "kindergarten")
            {
                result = SchoolGradeType.Kindergarten;
            }
            else if (grade == "1")
            {
                result = SchoolGradeType.S1;
            }
            else if (grade == "2")
            {
                result = SchoolGradeType.S2;
            }
            else if (grade == "3")
            {
                result = SchoolGradeType.S3;
            }
            else if (grade == "4")
            {
                result = SchoolGradeType.S4;
            }
            else if (grade == "5")
            {
                result = SchoolGradeType.S5;
            }
            else if (grade == "6")
            {
                result = SchoolGradeType.S6;
            }
            else if (grade == "7")
            {
                result = SchoolGradeType.S7;
            }
            else if (grade == "8")
            {
                result = SchoolGradeType.S8;
            }
            else if (grade == "9")
            {
                result = SchoolGradeType.S9;
            }
            else if (grade == "10")
            {
                result = SchoolGradeType.S10;
            }
            else if (grade == "11")
            {
                result = SchoolGradeType.S11;
            }
            else if (grade == "12")
            {
                result = SchoolGradeType.S12;
            }
            else
            {
                result = SchoolGradeType.College;
            }
           
            return result;
        }
        private GenderCategories FormatProfileGender(string gender)
        {
            gender = gender.ToLower().Trim();
            if (gender == "male")
            {
                return GenderCategories.Male;
            }
            else if (gender == "female")
            {
                return GenderCategories.Female;
            }
            else if (gender == "gender neutral")
            {
                return GenderCategories.GenderNeutral;
            }
            else
            {
                return GenderCategories.None;
            }
        }
        private DateTime? FormatDOB(string dob)
        {
            try
            {
                return Convert.ToDateTime(dob);
            }
            catch
            {
                return null;
            }
        }
        private bool IsvalidEmail(string email)
        {
            return Regex.IsMatch(email.Trim().ToLower(), Common.Constants.Constants.EmailRegex);
        }
        private bool IsValidPhone(string phone)
        {
            if (phone.Length >= 8 && phone.Length <= 15)
            {
                return true;
            }
            return false;
        }
        private string FormatPhone(string phone)
        {
            return phone.Trim().Replace("-", "").Replace("(", "").Replace(")", "");
        }
    }
}





namespace Jumbula.Web.Infrastructure.DataMigration.DataManipulationItems
{
    public class DataManipulationImapactDojoOrdersItem : DataManipulationSourceItemBaseModel
    {
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string Username { get; set; }
        public string Program { get; set; }
        public string Tuition { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Parent1FirstName { get; set; }
        public string Parent1LastName { get; set; }
        public string Parent1Email { get; set; }
        public string Parent1Gender { get; set; }
        public string Parent1DOB { get; set; }
        public string Parent1CellPhone { get; set; }
        public string Parent1Occupation { get; set; }
        public string Parent1Employer { get; set; }
        public string Parent2FirstName { get; set; }
        public string Parent2LastName { get; set; }
        public string Parent2Email { get; set; }
        public string Parent2Gender { get; set; }
        public string Parent2DOB { get; set; }
        public string Parent2CellPhone { get; set; }
        public string Parent2Occupation { get; set; }
        public string Parent2Employer { get; set; }
        public string EmergencyFirstName { get; set; }
        public string EmergencyLastName { get; set; }
        public string EmergencyEmail { get; set; }
        public string EmergencyHomePhone { get; set; }
        public string EmergencyWorkPhone { get; set; }
        public string Grade { get; set; }
        public string SchoolName { get; set; }
        public string TeacherName { get; set; }
        public string CompanyName { get; set; }
        public string PolicyNumber { get; set; }
        public string HaveMedicalConditions { get; set; }
        public string MedicalConditions { get; set; }
        public string HaveAllergies { get; set; }
        public string Allergies { get; set; }
    }
    public class DataManipulationAlabamaTeamOrdersItem : DataManipulationSourceItemBaseModel
    {
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string Username { get; set; }
        public string Program { get; set; }
        public string Schedule { get; set; }
        public string Section { get; set; }
        public string Tuition { get; set; }
        public string TeamName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Organization { get; set; }
        public string IHaveAlabamaChessFederationMemberships { get; set; }
        public string FirstName1 { get; set; }
        public string LastName1 { get; set; }
        public string Grade1 { get; set; }
        public string USCFID1 { get; set; }
        public string FirstName2 { get; set; }
        public string LastName2 { get; set; }
        public string Grade2 { get; set; }
        public string USCFID2 { get; set; }
        public string FirstName3 { get; set; }
        public string LastName3 { get; set; }
        public string Grade3 { get; set; }
        public string USCFID3 { get; set; }
        public string FirstName4 { get; set; }
        public string LastName4 { get; set; }
        public string Grade4 { get; set; }
        public string USCFID4 { get; set; }
        public string FirstName5 { get; set; }
        public string LastName5 { get; set; }
        public string Grade5 { get; set; }
        public string USCFID5 { get; set; }
        public string USCFName5 { get; set; }
        public string USCFRegularRating5 { get; set; }
        public string USCFExpirationDate5 { get; set; }
        public string SchoolName { get; set; }
    }
    public class DataManipulationAlabamaSingleOrdersItem : DataManipulationSourceItemBaseModel
    {
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string Username { get; set; }
        public string Program { get; set; }
        public string Schedule { get; set; }
        public string Section { get; set; }
        public string Tuition { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        public string USCFId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string ParentFirstName { get; set; }
        public string ParentLastName { get; set; }
        public string ParentEmail { get; set; }
        public string ParentCellPhone { get; set; }
        public string Grade { get; set; }
        
    }
    public class DataManipulationAddEmailTemplateItem : DataManipulationSourceItemBaseModel
    {
        public string PartnerDomain { get; set; }
        public string SchoolDomain { get; set; }
        public string SchoolSeasonTitle { get; set; }
        public string Season { get; set; }
        public string Year { get; set; }
        public string SourceSchoolSeasonTitle { get; set; }
        public string SourceSeasonName { get; set; }
        public string SourceSeasonYear { get; set; }
    }

    public class TransactionsViewModel
    {
        public decimal Amount { get; set; }
    }

    public class DataManipulationAddNewClubsItem : DataManipulationSourceItemBaseModel
    {
        public string PartnerDomain { get; set; }
        public string Domain { get; set; }
        public string Name { get; set; }
        public string TimeZone { get; set; }
        public string Website { get; set; }
        public string Type { get; set; }
        public string TaxIDFEIN { get; set; }
        public string TaxIDSSN { get; set; }
        public string MemberSince { get; set; }
        public string AgreementExpirationDate { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string FirstName1 { get; set; }
        public string LastName1 { get; set; }
        public string Email1 { get; set; }
        public string PhoneNumber1 { get; set; }
        public string Extension1 { get; set; }
        public string Title1 { get; set; }
        public string HasAccess1 { get; set; }
        public string AccessRole1 { get; set; }
        public string FirstName2 { get; set; }
        public string LastName2 { get; set; }
        public string Email2 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string Extension2 { get; set; }
        public string Title2 { get; set; }
        public string HasAccess2 { get; set; }
        public string AccessRole2 { get; set; }
        public string FirstName3 { get; set; }
        public string LastName3 { get; set; }
        public string Email3 { get; set; }
        public string PhoneNumber3 { get; set; }
        public string Extension3 { get; set; }
        public string Title3 { get; set; }
        public string HasAccess3 { get; set; }
        public string AccessRole3 { get; set; }
        public bool HasError { get; set; }
        public string ErrorMessage { get; set; }

    }

    public class DataManipulationCreateProgramFromCatalogItem : DataManipulationSourceItemBaseModel
    {
        public string School { get; set; }
        public string Provider { get; set; }
        public string Class { get; set; }
        public string GradesAges { get; set; }
        public string Min { get; set; }
        public string Max { get; set; }
        public string TuitionLabel { get; set; }
        public string Price { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string RegStartTime { get; set; }
        public string RegEndTime { get; set; }
        public string Day { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PMAM { get; set; }
        public string Season { get; set; }
        public string Year { get; set; }
        public string SchoolSeasonTitle { get; set; }
        public string PartnerDomain { get; set; }
        public string RegStartDate { get; set; }
        public string RegEndDate { get; set; }
        public string OldUniqueKey { get; set; }
        public string NewUniqueKey { get; set; }
        public string AlreadyAdded { get; set; }
        public string Action { get; set; }

    }

    public class DataManipultaionCreateOrderSessioinsItem : DataManipulationSourceItemBaseModel
    {
        public string OrderItemId { get; set; }

        public string Mon { get; set; }
        public string Tue { get; set; }
        public string Wed { get; set; }
        public string Thu { get; set; }
        public string Fri { get; set; }
    }

    public class DataManipultaionUpdateClassPriceItem : DataManipulationSourceItemBaseModel
    {
        public string PartnerDomain { get; set; }

        public string Provider { get; set; }

        public string Class { get; set; }

        public string Price { get; set; }
    }

    public class DataManipultaionUpdateSeasonSettingItem : DataManipulationSourceItemBaseModel
    {

        public string PartnerDomain { get; set; }
        public string SchoolDomain { get; set; }
        public string SchoolSeasonTitle { get; set; }
        public string Season { get; set; }
        public string Year { get; set; }
        public string SeasonStartDate { get; set; }
        public string SeasonEndDate { get; set; }
        public string GeneralRegistrationStartDate { get; set; }
        public string GeneralRegistrationEndDate { get; set; }
        public string GeneralRegistrationStartTime { get; set; }
        public string GeneralRegistrationEndTime { get; set; }

        public string HasPreRegistration { get; set; }
        public string PreRegistrationStartDate { get; set; }
        public string PreRegistrationEndDate { get; set; }
        public string PreRegistrationStartTime { get; set; }
        public string PreRegistrationEndTime { get; set; }
        public string HasLateRegistration { get; set; }
        public string LateRegistrationStartDate { get; set; }
        public string LateRegistrationEndDate { get; set; }
        public string LateRegistrationStartTime { get; set; }
        public string LateRegistrationEndTime { get; set; }
        public string HasLateRegistrationCharge { get; set; }
        public string LateRegistrationCharge { get; set; }
        public string LateRegistrationLabel { get; set; }
        public string SourceFormName { get; set; }
        public string SourceFormClub { get; set; }
        public string FormTemplateName { get; set; }

        public string Action { get; set; }
    }

    public class DataManipultaionFamilyProfileItem : DataManipulationSourceItemBaseModel
    {
        #region Account
        public string ClubDomain { get; set; }
        public string AccountEmail { get; set; }
        #endregion

        #region Student 
        public string StudentFirstName { get; set; }
        public string StudentLastName { get; set; }
        public string StudentGender { get; set; }
        public string StudentDOB { get; set; }
        public string StudentGrade { get; set; }
        public string StudentEmail { get; set; }
        public string PhotographyVideoReleasePermission { get; set; }
        #endregion Student 

        #region Parent1
        public string Parent1Email { get; set; }
        public string Parent1AddressLine1 { get; set; }
        public string Parent1AddressLine2 { get; set; }
        public string Parent1City { get; set; }
        public string Parent1State { get; set; }
        public string Parent1ZipCode { get; set; }
        public string Parent1Country { get; set; }
        public string Parent1FirstName { get; set; }
        public string Parent1LastName { get; set; }
        public string Parent1Gender { get; set; }
        public string Parent1DOB { get; set; }
        public string Parent1Relationship { get; set; }
        public string Parent1HomePhonePrimaryPhone { get; set; }
        public string Parent1WorkPhone { get; set; }
        public string Parent1MobilePhoneAlternatePhone { get; set; }
        public string Parent1Employer { get; set; }
        public string Parent1Occupation { get; set; }
        #endregion Parent1

        #region Parent2
        public string Parent2Email { get; set; }
        public string Parent2AddressLine1 { get; set; }
        public string Parent2AddressLine2 { get; set; }
        public string Parent2City { get; set; }
        public string Parent2State { get; set; }
        public string Parent2ZipCode { get; set; }
        public string Parent2Country { get; set; }
        public string Parent2FirstName { get; set; }
        public string Parent2LastName { get; set; }
        public string Parent2Gender { get; set; }
        public string Parent2DOB { get; set; }
        public string Parent2Relationship { get; set; }
        public string Parent2HomePhonePrimaryPhone { get; set; }
        public string Parent2WorkPhone { get; set; }
        public string Parent2MobilePhoneAlternatePhone { get; set; }
        public string Parent2Employer { get; set; }
        public string Parent2Occupation { get; set; }

        #endregion Parent2

        #region Emergency1
        public string EmergencyContact1FirstName { get; set; }
        public string EmergencyContact1LastName { get; set; }
        public string EmergencyContact1Relationship { get; set; }
        public string EmergencyContact1PrimaryPhone { get; set; }
        public string EmergencyContact1AlternatePhone { get; set; }
        public string EmergencyContact1Email { get; set; }

        #endregion Emergency1

        #region Emergency2
        public string EmergencyContact2FirstName { get; set; }
        public string EmergencyContact2LastName { get; set; }
        public string EmergencyContact2Relationship { get; set; }
        public string EmergencyContact2PrimaryPhone { get; set; }
        public string EmergencyContact2AlternatePhone { get; set; }
        public string EmergencyContact2Email { get; set; }

        #endregion Emergency2

        #region AuthorizedPickUp1
        public string AuthorizedPickUp1FirstName { get; set; }
        public string AuthorizedPickUp1LastName { get; set; }
        public string AuthorizedPickUp1Relationship { get; set; }
        public string AuthorizedPickUp1PrimaryPhone { get; set; }
        public string AuthorizedPickUp1AlternatePhone { get; set; }
        public string AuthorizedPickUp1IsEmergencyContact { get; set; }
        public string AuthorizedPickUp1Email { get; set; }
        #endregion AuthorizedPickUp1

        #region AuthorizedPickUp2
        public string AuthorizedPickUp2FirstName { get; set; }
        public string AuthorizedPickUp2LastName { get; set; }
        public string AuthorizedPickUp2Relationship { get; set; }
        public string AuthorizedPickUp2PrimaryPhone { get; set; }
        public string AuthorizedPickUp2AlternatePhone { get; set; }
        public string AuthorizedPickUp2IsEmergencyContact { get; set; }
        public string AuthorizedPickUp2Email { get; set; }
        #endregion AuthorizedPickUp2

        #region AuthorizedPickUp3
        public string AuthorizedPickUp3FirstName { get; set; }
        public string AuthorizedPickUp3LastName { get; set; }
        public string AuthorizedPickUp3Relationship { get; set; }
        public string AuthorizedPickUp3PrimaryPhone { get; set; }
        public string AuthorizedPickUp3AlternatePhone { get; set; }
        public string AuthorizedPickUp3IsEmergencyContact { get; set; }
        public string AuthorizedPickUp3Email { get; set; }
        #endregion AuthorizedPickUp3

        #region AuthorizedPickUp4
        public string AuthorizedPickUp4FirstName { get; set; }
        public string AuthorizedPickUp4LastName { get; set; }
        public string AuthorizedPickUp4Relationship { get; set; }
        public string AuthorizedPickUp4PrimaryPhone { get; set; }
        public string AuthorizedPickUp4AlternatePhone { get; set; }
        public string AuthorizedPickUp4IsEmergencyContact { get; set; }
        public string AuthorizedPickUp4Email { get; set; }
        #endregion AuthorizedPickUp4

        #region School
        public string SchoolName { get; set; }
        public string Homeroom { get; set; }

        #endregion School

        #region Medical
        public string HasMedicalConditionsAndSpecialNeeds { get; set; }
        public string MedicalConditionsAndSpecialNeeds { get; set; }
        public string HasAllergiesAndDietaryRestrictions { get; set; }
        public string AllergiesAndDietaryRestrictions { get; set; }
        public string SelfAdministeredMedication { get; set; }
        public string SelfAdministeredMedicationDescription { get; set; }
        public string HasNutAllergy { get; set; }
        public string NutAllergyDescription { get; set; }
        public string DoctorName { get; set; }
        public string DoctorLocation { get; set; }
        public string DoctorPhone { get; set; }

        #endregion Medical

        #region Insurance
        public string CompanyName { get; set; }
        public string CompanyPhone { get; set; }
        public string PolicyNumber { get; set; }

        #endregion Insurance
    }

    public class DataManipultaionClubHolidaysItem : DataManipulationSourceItemBaseModel
    {
        public string PartnerDomain { get; set; }
        public string SchoolDomain { get; set; }
        public string AMPM { get; set; }
        public string AM { get; set; }
        public string PM { get; set; }
    }

    public class DataManipulationAddFormAndWaiverItem : DataManipulationSourceItemBaseModel
    {
        public string PartnerDomain { get; set; }
        public string SchoolDomain { get; set; }
        public string SourceFormName { get; set; }
        public string SourceFormClub { get; set; }
        public string FormTemplateName { get; set; }
    }

    public class DataManipulationFixOrderTeachersItem : DataManipulationSourceItemBaseModel
    {
        public string Program { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Teacher { get; set; }
    }

    public class DataManipulationFixProgramTeachersItem : DataManipulationSourceItemBaseModel
    {
        public string SchoolDomain { get; set; }
        public string Teachers { get; set; }
    }

    public class DataManipulationAddCampSessionsItem : DataManipulationSourceItemBaseModel
    {
        public string ChargeId { get; set; }
    }

    public class DataManipulationStopedPaymentItem : DataManipulationSourceItemBaseModel
    {
        public string Id { get; set; }
        public string InstallmentDate { get; set; }
        public string NewAmount { get; set; }
        public string Diffrence { get; set; }

    }

    public class DataManipulationUpdateDuplicateSessionItem : DataManipulationSourceItemBaseModel
    {
        public string OrderItemId { get; set; }
        public string ProgramSessionId { get; set; }
    }

    public class DataManipulationAddOrderItem : DataManipulationSourceItemBaseModel
    {
        public string ClubDomain { get; set; }
        public string ProgramName { get; set; }
        public string ProgramPrice { get; set; }
        public string TuitionLable { get; set; }

        public string AccountEmail { get; set; }

        public string StudentFirstName { get; set; }

        public string StudentLastName { get; set; }

        public string StudentGrade { get; set; }

        public string TeacherName { get; set; }

        public string DismissalFromKidzArt { get; set; }

        public string Parent1FirstName { get; set; }

        public string Parent1LastName { get; set; }

        public string Parent1Email { get; set; }

        public string Parent1HomePhonePrimaryPhone { get; set; }

        public string Parent1MobilePhoneAlternatePhone { get; set; }

        public string AuthorizedPickUp1FirstName { get; set; }

        public string AuthorizedPickUp1LastName { get; set; }

        public string AuthorizedPickUp2FirstName { get; set; }

        public string AuthorizedPickUp2LastName { get; set; }
    }

    public class DataManipulationUpdateFormItem : DataManipulationSourceItemBaseModel
    {
        public string TeacherName { get; set; }
        public string Program { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ClubDomain { get; set; }
    }


    public class DataManipultaionTransactionsItem : DataManipulationSourceItemBaseModel
    {
        public string OrderitemId { get; set; }
        public string TransactionId { get; set; }
        public string PaymentdetailId { get; set; }
        public string JsonAmount { get; set; }
        public string StripeAmount { get; set; }
    }

    public class DataManipultaionCreditCardsItem : DataManipulationSourceItemBaseModel
    {
        public string CardId { get; set; }

    }

}