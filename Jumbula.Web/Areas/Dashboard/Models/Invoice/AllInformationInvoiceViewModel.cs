﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AllInformationInvoiceViewModel
    {
        public List<InvoiceHistoryViewModel> HistoryModel { get; set; }

        public InvoiceHistoryViewModel InfoModel { get; set; }

        public InvoiceItemViewModel InvoiceItemModel { get; set; }
    }
}