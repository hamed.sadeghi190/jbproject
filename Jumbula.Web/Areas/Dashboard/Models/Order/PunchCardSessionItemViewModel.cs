﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class PunchCardSessionItemViewModel
    {
        public long Id { get; set; }

        public bool HasTwoSessionInADay { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DateTime? StartDate2 { get; set; }
        public DateTime? EndDate2 { get; set; }

        public DateTime? DateCreated { get; set; }

        public string StrDateTime
        {
            get
            {
                return StartDate.HasValue ? string.Format("{0} - {1}", StartDate.Value.ToString("MMM dd, yyyy hh:mm tt"), EndDate.Value.ToString("hh:mm tt")) : string.Empty;
            }
        }

        public string StrDateTime2
        {
            get
            {
                return StartDate2.HasValue ? string.Format("{0} - {1}", StartDate2.Value.ToString("MMM dd, yyyy hh:mm tt"), EndDate2.Value.ToString("hh:mm tt")) : string.Empty;
            }
        }

        public string StrDateCreated
        {
            get
            {
                return DateCreated.HasValue ? string.Format("{0}", DateCreated.Value.ToString("MMM dd, yyyy hh:mm tt")) : string.Empty;
            }
        }

        public string Title { get; set; }

        public string BeforeAfter { get; set; }

        public string BeforeAfter2 { get; set; }

        public PunchcardAssignStatus Status { get; set; }
    }

    public enum PunchcardAssignStatus
    {
        UnAssigned,
        Assigned
    }
}