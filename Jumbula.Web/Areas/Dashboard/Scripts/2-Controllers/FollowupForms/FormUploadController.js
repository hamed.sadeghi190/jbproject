﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('FormUploadController', ['$scope', '$http', 'cfpLoadingBar', '$stateParams', '$state', 'jbToast', 'jbRandom', 'programService', 'formService', 'jbConfirmation', function ($scope, $http, cfpLoadingBar, $stateParams, $state, jbToast, jbRandom, programService, formService, jbConfirmation) {
        //window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.Urls = [];

        $stateParams.templateId = 0;
        $stateParams.formType = "FormUpload";

        $scope.JbForm = {};
        $scope.JbForm.EditMode = false;

        if ($scope.MC.IsFormUploadRename) {
            $scope.JbForm.ConfirmMode = true;
        }

        $scope.JbElement = {}

        $scope.JbForm.goBack = function () {

            if ($scope.JbForm.ConfirmMode) {
                $scope.JbForm.ConfirmMode = false;
            }
            else {
                window.history.back();
            }
        }

        $scope.JbForm.Save = function () {

            var formElements = $scope.Model.JbForm.Elements;
            for (var si = 0; si < formElements.length; si++) {
                if (formElements[si].CurrentMode == "Hidden") {
                    formElements.removeByIndex(si--);
                } else {
                    for (var ei = 0; ei < formElements[si].Elements.length; ei++) {
                        if (formElements[si].Elements[ei].CurrentMode == "Hidden") {
                            formElements[si].Elements.removeByIndex(ei--);
                        }
                    }
                }



                if ($stateParams.templateId == 0) {
                    $scope.JbForm.SaveAsMode = true;
                }
                console.log(formElements[si].Elements)
                if (formElements[si].Elements.length == 0) {

                    jbToast.error("Please upload the supplemental document.");
                }
                else {
                if ($scope.JbForm.SaveAsMode === true || $scope.JbForm.SaveAsMode === 'true') {
                    // Save club form template

                    $http.post('/FormBuilder/Create', { formJson: JSON.stringify($scope.Model.JbForm) })
                        .success(function (data, status, headers, config) {
                            if (data.Status == 'Success') {

                                $http.post('/FormBuilder/AddTemplate', { jbFormId: data.FormId, templateName: $scope.Model.FormName, formType: 2 })
                                    .success(function (data2, status, headers, config) {
                                        if (data2.Status == true) {

                                            $scope.Model.FormTemplate = data2.templateId;

                                            $scope.Model.FormTemplates.push({ Text: $scope.Model.FormName, Value: data2.templateId });

                                            formService.setModel($scope.Model);
                                            $scope.MC.IsFormUploadRename = false;
                                            back();
                                        }
                                        else {
                                            $scope.MC.handleErrors($scope, data2);
                                        }

                                    });
                            }
                        });

                }
            }
            }
        };

        $scope.JbForm.load = function () {
            $http.get("Dashboard/Club/GetJbForm", { params: { templateId: $stateParams.templateId, formType: $stateParams.formType } }).success(function (data, status, headers, config) {
                $scope.Model = data;
                if ($stateParams.templateId == 0) {
                    $scope.JbForm.EditMode = true;
                }

                formService.setModel(data);

            });
        }

        $scope.onSelect = function () {
        }

        $scope.onSuccess = function (e) {
            var newElement = {};
            newElement.Type = 'JbUpload';
            newElement.Title = e.response.fileName;
            newElement.Value = e.response.URL;

            $scope.Model.JbForm.Elements[0].Elements.push(newElement);

        }

        $scope.JbForm.load();

        $scope.JbElement.Base = function () {
            return {
                "ElementId": "JbBaseElement" + jbRandom.Generate(9, true, false, false, false, false),
                "Name": null,
                "Title": "",
                "Value": "",
                "Type": null,
                "HelpText": null,
                "IsCustomElement": false,
                "Size": 0,
                "Items": [
                    {
                        Text: "Option 1",
                        Value: "Option1"
                    }
                ],
                "VisibleMode": "Both",
                "CurrentMode": "Design",
                "AccessRole": {},
                "Validations": []
            }
        }

        $scope.JbElement.Operate = {
            Title: function (element) {
                try {
                    element.Title = element.Title.replace(/'/mi, "’").replace(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\/\\\.\=\+\~\n\ ’]/mi, "");
                } catch (e) { }
                if (element.CurrentMode == "Design") {
                    element.Name = element.Title + "_" + jbRandom.Generate(6, false, true, true, true, false);
                }
            },
            Value: function (element) {
                element.Value = element.Value.replace(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\/\\\.\=\+\~\n\ ]/mi, "");
            },
            IsSectionTitleEditing: function (section) {
                return is.existy(section._titleIsEditing) && section._titleIsEditing;
            },
            EditSectionTitle: function (section) {
                section._titleIsEditing = true;
                section._Title = section.Title;
                setTimeout(function () {
                    $("#sectionTitle" + section.Name).focus().unbind("keyup.edit").bind("keyup.edit", function (e) {
                        if (e.which == 13) {
                            $scope.JbElement.Operate.SaveEditSectionTitle(section);
                            $scope.$apply();
                        } else if (e.which == 27) {
                            $scope.JbElement.Operate.CancelEditSectionTitle(section);
                            $scope.$apply();
                        }
                    });
                });
            },
            CancelEditSectionTitle: function (section) {
                section._titleIsEditing = false;
                section.Title = section._Title;
            },
            SaveEditSectionTitle: function (section) {
                if (is.not.empty(section.Title)) {
                    section._Title = '';
                } else {
                    section.Title = section._Title;
                }
                section._titleIsEditing = false;
            },
            ToggleHiddenReadOnlyModes: function (element) {
                if (element.CurrentMode == "ReadOnly") {
                    element.CurrentMode = "Hidden";
                    return true;
                } else if (element.CurrentMode == "Hidden") {
                    element.CurrentMode = "ReadOnly";
                    return true;
                } else {
                    return false;
                }
            },
            CanShowHide: function (section, element) {
                if (is.not.propertyDefined(requiredByJbElementNames, section.Name) || (section && section.CurrentMode == "Design") || (element && element.CurrentMode == "Design")) {
                    return true;
                } else {
                    return ((section && is.propertyDefined(requiredByJbElementNames, section.Name) && !element) ? requiredByJbElementNames[section.Name].requiredSection == false : is.not.inArray(element.Name, requiredByJbElementNames[section.Name].children));
                }
            },
            IsRequiredByJB: function (section, element) {
                if (is.propertyDefined(requiredByJbElementNames, section.Name)) {
                    if (element) {
                        return is.inArray(element.Name, requiredByJbElementNames[section.Name].children);
                    } else {
                        return requiredByJbElementNames[section.Name].requiredSection;
                    }
                } else {
                    return false;
                }
            },
            HasNoElement: function (section) {
                return section.Elements.length == 0;
            },
            GetBase: function (type) {
                var result = {};
                angular.forEach($scope.DefaultElementsTemplate, function (value, key) {
                    if (value.Type == type) {
                        result = value;
                    }
                });
                return result;
            },
            GetTypeTitle: function (type) {
                var typeTitles = {
                    "JbSection": "Section",
                    "JbTextBox": "Text",
                    "JbTextArea": "Paragraph text",
                    "JbDropDown": "Choose from a list",
                    "JbRadioList": "Select from a list",
                    "JbCheckBox": "Check box",
                    "JbEmail": "Email",
                    "JbNumber": "Number",
                    "JbPhone": "Phone number",
                    "JbAddress": "Address",
                    "USCF": "USCF info",
                    "JbParagraph": "Text display",
                    "JbTextEditor": "Text Editor",
                }
                return typeTitles[type];
            },
            Add: {
                Section: function () {
                    $scope.Model.JbForm.Elements.push({
                        "Type": "JbSection",
                        "IsCustomElement": false,
                        "Elements": [],
                        "ElementId": "JbSection" + jbRandom.Generate(9, true, false, false, false, false),
                        "TypeTitle": "Section",
                        "Name": "CustomSection_" + jbRandom.Generate(6, true, true, true, true, false),
                        "Title": "Custom section",
                        "HelpText": null,
                        "Size": 0,
                        "VisibleMode": "Both",
                        "CurrentMode": "Design",
                        "AccessRole": {},
                        "Validations": []
                    });
                },
                Element: {
                    ShowTypes: function (section) {
                        angular.forEach($scope.Model.JbForm.Elements, function (section, skey) {
                            angular.forEach(section.Elements, function (element, ekey) {
                                if (element._IsNewElement) {
                                    section.Elements.removeByIndex(ekey);
                                }
                            });
                        });
                        var newElement = $scope.JbElement.Base();
                        newElement._IsNewElement = true;
                        newElement._IsInAddProcess = true;
                        section.Elements.push(newElement);
                        $scope.JbElement.Operate.Edit.SetAsActiveElement(section, newElement);
                    },
                    SelectType: function (element, type) {
                        element.Type = type;
                        angular.extend(element, $scope.JbElement.GetBaseByType(type));
                        element._IsNewElement = false;
                        element.ElementId = type + jbRandom.Generate(9, true, false, false, false, true);
                        element.Name = element.Type + "_" + jbRandom.Generate(6, false, true, true, true, false);
                    },
                    CanAddNew: function () {
                        return is.empty($scope.JbElement.Active);
                    }
                }
            },
            Edit: {
                SetAsActiveElement: function (section, element) {
                    $scope.JbElement.Active.Element = element;
                    $scope.JbElement.Active._Element = angular.copy(element);
                    $scope.JbElement.Active.Section = section;
                },
                SaveActiveElement: function (element, activeForm) {
                    if (activeForm.$invalid) {
                        if (activeForm._submited) {
                            jbToast.error("Please check your inputs again, something is wrong with the inputs.");
                        }
                        activeForm._submited = true;
                        setTimeout(function () {
                            $("[validation-error-tooltip]").tooltip("show");
                        }, 20);
                    }
                    else {
                        activeForm._submited = false;
                        $scope.JbElement.Active = {};
                        if (element._IsInAddProcess) {
                            element._IsInAddProcess = false;
                        }
                    }
                },
                CancelActiveElement: function (section, elementIndex) {
                    section.Elements[elementIndex] = $scope.JbElement.Active._Element;
                    $scope.JbElement.Active = {};
                    if (section.Elements[elementIndex]._IsInAddProcess) {
                        $scope.JbElement.Operate.Delete.Element(section, elementIndex);
                    }
                },
                IsActiveElement: function (element) {
                    return $scope.JbElement.Active.Element == element;
                },
                CanEdit: function (section, element) {
                    return is.empty($scope.JbElement.Active);
                },
            },
            Delete: {
                Section: function (sectionIndex) {
                    $scope.Model.JbForm.Elements.removeByIndex(sectionIndex);
                },
                Element: function (section, elementIndex) {
                    section.Elements.removeByIndex(elementIndex);
                }
            },
            IsRequired: function (element) {
                var hasRequiredValidation;
                if (element.IsRequired) {
                    hasRequiredValidation = false;
                    angular.forEach(element.Validations, function (validation, key) {
                        if (validation.Type == "Required") {
                            hasRequiredValidation = true;
                        }
                    });
                    if (!hasRequiredValidation) {
                        element.Validations.push({
                            "Type": "Required",
                            "Message": element.Title + " is required",
                            "IsChecked": true
                        });
                    }
                } else {
                    hasRequiredValidation = false;
                    var validationIndex = -1;
                    angular.forEach(element.Validations, function (validation, key) {
                        if (validation.Type == "Required") {
                            hasRequiredValidation = true;
                            validationIndex = key;
                        }
                    });
                    if (hasRequiredValidation) {
                        element.Validations.removeByIndex(validationIndex);
                    }
                }
            }
        }


        $scope.goBack = function () {
            alert("back");
        };

        var back = function () {
            $scope.MC.IsFormUploadRename = false;
            $state.go('FollowUpForms', { programId: $scope.MC.stateParams.programId }, 'ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
        }
    }]);
})();