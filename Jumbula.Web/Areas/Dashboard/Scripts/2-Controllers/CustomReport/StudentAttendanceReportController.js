﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('StudentAttendanceReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = customReportService.getModelFromStrorage();

        var programId = $scope.Model._SelectedPrograms;

        if (programId == 'undefined' || programId == null) {
            programId = 0;
        }

        $http.get(baseUrl + '/GetProgramSessions', { params: { programId: programId } }).success(function (data, status, headers, config) {

            $scope.ProgramSessions = data.Sessions;

            $scope.Model.ProgramName = data.ProgramName;

            $scope.Model.LastSessionDate = data.LastSessionDate;

            $scope.Model.SelectedSession = data.LastSessionId;

            prepareGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        var prepareGrid = function () {
            $scope.SessionAttendanceReportDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                    {
                        read: {
                            url: baseUrl + '/SessionAttendanceReportRun',
                            type: "POST",
                            data: function () {
                                return {
                                    programId: programId,
                                    sessionId: $scope.Model.SelectedSession,
                                };
                            },
                        },

                    },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },

                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
            });

            $scope.StudentAttendanceReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Student_Attendance_Report_" + $scope.Model.ProgramName + ".xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{ value: 'Session: ' + $scope.Model.LastSessionDate }]
                    });
                },
                dataSource: $scope.SessionAttendanceReportDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                scrollable: true,
                sortable: { mode: 'single' },
                columns: [
                        {
                            field: "FullName",
                            title: "Full name",
                            width: "200px"
                        },
                         {
                             field: "IsPresent",
                             title: "Present",
                             width: "200px",
                             template: "<input type='checkbox' disabled value='' #if(IsPresent){#  checked  #}# />"
                         },
                        {
                            field: "IsLate",
                            title: "Late",
                            width: "200px",
                            template: "<input type='checkbox' disabled value='' #if(IsLate){#  checked  #}# />"
                        },
                        {
                            field: "IsAbsent",
                            title: "Absent",
                            width: "200px",
                            template: "<input type='checkbox' disabled value='' #if(IsAbsent){#  checked  #}# />"
                        },
                ]
            }
        }

        $scope.filterData = function () {
            $scope.StudentAttendanceReportGridOptions.dataSource.read();
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#StudentAttendanceReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();