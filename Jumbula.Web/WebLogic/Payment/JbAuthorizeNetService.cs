﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.CreditCard;
using Jumbula.Web.Areas.Dashboard.Services;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using Serilog;
using Constants = Jumbula.Common.Constants.Constants;
using static Jumbula.Common.Constants.Payment;
using Jumbula.Core.Business;
using Jumbula.Core.Model.Order;

namespace Jumbula.Web.WebLogic.Payment
{
    public class JbAuthorizeNetService : JbBasePaymentGateway, IPaymentGatewayService
    {
        public JbAuthorizeNetService(string clubDomain, bool isTestMode, long clientId = 0)
            : base(PaymentGateway.AuthorizeNet)
        {

            if (!string.IsNullOrEmpty(clubDomain))
            {
                Club club = Ioc.ClubBusiness.Get(clubDomain);

                ClientPaymentMethod clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
                IsTestMode = clientPaymentMethod.IsCreditCardTestMode || isTestMode;

                if (IsTestMode)
                {
                    ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
                }
                else
                {
                    ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
                }
                
                LoginId = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.AuthorizeNet_LoginId_Test : clientPaymentMethod.AuthorizeLoginId;
                TransactionKey = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.AuthorizeNet_Transaction_Test : clientPaymentMethod.AuthorizeTransactionKey;
            }
            else if (clientId > 0)
            {
                ClientPaymentMethod clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(clientId);

                IsTestMode = clientPaymentMethod.IsCreditCardTestMode || isTestMode;
                LoginId = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.AuthorizeNet_LoginId_Test : clientPaymentMethod.AuthorizeLoginId;
                TransactionKey = (clientPaymentMethod.IsCreditCardTestMode || IsTestMode) ? WebConfigHelper.AuthorizeNet_Transaction_Test : clientPaymentMethod.AuthorizeTransactionKey;

            }
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey,
            };

        }


        private bool IsTestMode = true;
        private const string REQUESTMETHOD = "POST";
        private const int DEFAULT_TIMEOUT = 3600000;
        private string LoginId { get; set; }
        private string TransactionKey { get; set; }

        public createTransactionResponse Refund(string chargeId, decimal refundAmount, Dictionary<string, string> metaData)
        {

            var paymentDetail = Ioc.TransactionActivityBusiness.GetPaymentDetail(chargeId);
            var credit = paymentDetail.CreditCard;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey
            };

            var creditCard = new creditCardType
            {
                cardNumber = credit.LastDigits,

                expirationDate = (credit.ExpiryMonth.Length == 1 ? ("0" + credit.ExpiryMonth) : credit.ExpiryMonth) + credit.ExpiryYear.Substring(2, 2),
            };

            //standard api call to retrieve response
            var paymentType = new paymentType { Item = creditCard };

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.refundTransaction.ToString(),    // refund type
                payment = paymentType,
                amount = refundAmount,
                refTransId = chargeId,

            };

            var request = new createTransactionRequest { transactionRequest = transactionRequest };

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();
            return response;

        }

        public getTransactionDetailsResponse GetTransactionDetails(string TransactionID)
        {
            if (IsTestMode)
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey,
            };

            var request = new getTransactionDetailsRequest();
            request.transId = TransactionID;

            // instantiate the controller that will call the service
            var controller = new getTransactionDetailsController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            return controller.GetApiResponse();

        }

        public AuthorizeNetChargeResponse Charge(long clientId, CurrencyCodes currency, decimal amount, PaymentModels card, PostalAddress address, string userCustomerId, string cardId, long installmentId = 0)
        {
            if (IsTestMode)
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey,
            };
            createTransactionRequest request = null;
            if (string.IsNullOrWhiteSpace(card.CVV))
            {
                //create a customer payment profile
                customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
                profileToCharge.customerProfileId = userCustomerId;
                profileToCharge.paymentProfile = new paymentProfile { paymentProfileId = cardId };

                var transactionRequest = new transactionRequestType
                {
                    transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // refund type
                    amount = amount,
                    profile = profileToCharge,
                };

                request = new createTransactionRequest { transactionRequest = transactionRequest };
            }
            else
            {
                var creditCard = new creditCardType
                {
                    cardNumber = card.CardNumber,
                    expirationDate = card.Month + card.Year.Substring(2, 2),
                    cardCode = card.CVV
                };

                var paymentType = new paymentType { Item = creditCard };

                var transactionRequest = new transactionRequestType
                {
                    transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),    // charge the card
                    amount = amount,
                    payment = paymentType,
                    billTo = GetCustomerAddressType(address, card),
                };

                request = new createTransactionRequest { transactionRequest = transactionRequest };
            }

            // instantiate the contoller that will call the service
            var controller = new createTransactionController(request);
            controller.Execute();

            var result = controller.GetResultCode();

            return new AuthorizeNetChargeResponse { Response = controller.GetApiResponse(), Status = result == messageTypeEnum.Ok, Errors = controller.GetResults() };

        }

        private customerAddressType GetCustomerAddressType(PostalAddress address, PaymentModels card)
        {
            var result = new customerAddressType();

            try
            {
                if (address == null || string.IsNullOrWhiteSpace(address.Address))
                {
                    if (card != null)
                    {
                        address = card.Address;
                    }
                }

                var names = JbBasePaymentGateway.SplitName(card.CardHolderName);
                var firstName = names[0];
                var lastName = names[1];

                result = new customerAddressType
                {
                    firstName = firstName,
                    lastName = lastName,
                    address = address.Street,
                    city = address.City,
                    zip = address.Zip,
                    state = address.State,
                    country = address.Country
                };


                return result;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }


            return result;

        }

        public PaymentResponseModel DirectPay(ControllerContext controllerContext, PayViewModel model, string clubDomain)
        {
            var creditCards = Ioc.UserCreditCardBusiness;
            resultResponse.cardErrors = new Dictionary<string, string>();
            IQueryable<UserCreditCard> userCreditCardNotLast;
            bool isCardCreated = false;
            createCustomerProfileResponse result = null;
            var selectedCreditCard = creditCards.Get(model.CreditId);
            currency = CurrencyCodes.USD;

            try
            {
                var isCreditExist = false;
                JbPaymentService paymentService = new JbPaymentService();

                SetParameters(model);

                var authorizeNetService = new JbAuthorizeNetService(clubDomain, isOrderTestMode);
                isCreditExist = isCreditCardExist(model, _paymentGateway, userId);

                var userCreditCard = creditCards.GetUserCreditCards(userId, _paymentGateway);

                if (model.PaymentMethod == PaymentMethod.Card)
                {
                    CreditCardExistWithCustomer = Ioc.UserCreditCardBusiness.GetCreditCards(model.CardNumber, userId, _paymentGateway).Where(c => c.CustomerId != "").FirstOrDefault();
                    modelParentBilling = GetModelParentBilling(model);
                }

                if (club != null)
                {
                    var client = club.Client;
                    currency = club.Currency;
                    var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                    if (club.PartnerId.HasValue && club.IsSchool)
                    {
                        client = club.PartnerClub.Client;
                        currency = club.PartnerClub.Currency;
                    }

                    var userCustomerId = string.Empty;
                    var customerPaymentProfileId = string.Empty;
                    var userCustomerResponse = string.Empty;
                    var token = model.Token;
                    var cardId = "";

                    var modelCard = new PaymentModels();

                    if (model.PaymentMethod == PaymentMethod.Card) // No select express payment methods available
                    {

                        var brand = GetBrandCard(model.CardNumber);

                        modelCard = GetPaymentModels(model);
                        modelCard.Brand = brand;
                    }

                    if ((model.ActionCaller == JumbulaSubSystem.Order || model.ActionCaller == JumbulaSubSystem.MakeaPayment) && order.IsAutoCharge)
                    {
                        if (model.PaymentMethod == PaymentMethod.Card) // No select express payment methods available
                        {
                            if (userCreditCard != null && userCreditCard.Any())//There are previus-cards In db for the user with customerId
                            {
                                var firstCard = userCreditCard.ToList().First();
                                var lastCard = userCreditCard.ToList().LastOrDefault();

                                //show express payment methods available if select express payment methods  just create token in card
                                if (firstCard.CustomerId == lastCard.CustomerId)
                                {
                                    userCustomerId = lastCard.CustomerId;
                                }
                                //Token in state create in javascript
                            }
                            else
                            {
                                result = authorizeNetService.CreateAuthorizeNetCustomerId(order); //create just customer in Authorize.net

                                if (result != null && result.messages.resultCode == messageTypeEnum.Ok)
                                {
                                    if (result != null && result.messages.message != null)
                                    {
                                        userCustomerId = result.customerProfileId;
                                        userCustomerResponse = JsonConvert.SerializeObject(result);
                                    }
                                }
                                else if (result.messages.resultCode == messageTypeEnum.Error && result.messages.message[0].code == "E00039")
                                { // when Available customer for user just in authorize dashboard
                                    var error = result.messages.message[0].text;
                                    userCustomerId = error.Substring(27, 10); // get customer from response

                                }
                                else if (result != null)
                                {
                                    if (result.messages.message[0].code == "E00044")
                                    {
                                        resultResponse.cardErrors.Add("error", "Please contact your organization administrator. The Authorize.Net customer payment method is not properly configured.");
                                    }
                                    else
                                    {
                                        resultResponse.cardErrors.Add("error", result.messages.message[0].text);
                                    }

                                    FailurePayment(model);
                                    return resultResponse;
                                }

                            }
                        }
                        else //select express payment methods available
                        {

                            if (userCreditCard != null && userCreditCard.Any())
                            {

                                userCustomerId = selectedCreditCard.CustomerId;

                                cardId = GetCardId(selectedCreditCard);

                                if (!userCreditCard.Any(u => u.IsDefault)) // If no preferred in express payment methods available
                                {
                                    selectedCreditCard.IsDefault = true;
                                    selectedCreditCard.DefaultDate = DateTime.UtcNow;
                                    creditCards.Update(selectedCreditCard); //update payment methods for preferred payment methods
                                }

                            }
                        }
                    }
                    else
                    {
                        if (model.PaymentMethod != PaymentMethod.Card) //select express payment methods available
                        {
                            userCustomerId = selectedCreditCard.CustomerId;

                            cardId = GetCardId(selectedCreditCard);
                            modelCard.CardId = cardId;
                            modelCard = GetPaymentModelsForSelectedCard(selectedCreditCard);
                        }

                    }
                    //charge first installment
                    AuthorizeNetChargeResponse authorizeNetCharge;
                    var confirmationIds = new List<string>();

                    if (model.ActionCaller == JumbulaSubSystem.Invoice)
                    {
                        var transactionsGroup = transactions.GroupBy(t => t.OrderId);
                        foreach (var tran in transactionsGroup)
                        {
                            var confirmation = tran.First().Order.ConfirmationId;
                            confirmationIds.Add(confirmation);
                        }
                        model.ConfirmatiocIds = confirmationIds;

                        authorizeNetCharge = authorizeNetService.Charge(client.Id, currency, payableAmount, modelCard, model.Address, userCustomerId, cardId);

                        if (authorizeNetCharge.Status)
                        {
                            modelCard.Brand = authorizeNetCharge.Response.transactionResponse.accountType;
                        }
                        else
                        {
                            resultResponse.cardErrors.Add("errorex" + rnd.Next(52), authorizeNetCharge.Errors[0]);

                            if (authorizeNetCharge.Errors[0].Contains("E00003"))
                            {
                                resultResponse.cardErrors.Add("error", "The address you have entered is too long and cannot be processed. Please try another address or contact your organization administrator.");
                            }
                            else
                            {
                                resultResponse.cardErrors.Add("error", authorizeNetCharge.Errors[0]);
                            }
                        }
                    }
                    else
                    {
                        authorizeNetCharge = authorizeNetService.Charge(client.Id, currency, payableAmount, modelCard, model.Address, userCustomerId, cardId);

                        if (authorizeNetCharge.Status)
                        {
                            modelCard.Brand = authorizeNetCharge.Response.transactionResponse.accountType;
                        }
                        else if (!authorizeNetCharge.Status && authorizeNetCharge.Response == null)
                        {
                            resultResponse.cardErrors.Add("errorex" + rnd.Next(52), authorizeNetCharge.Errors[0]);

                            if (authorizeNetCharge.Errors[0].Contains("E00003"))
                            {
                                resultResponse.cardErrors.Add("error", "The address you have entered is too long and cannot be processed. Please try another address or contact your organization administrator.");
                            }
                            else
                            {
                                resultResponse.cardErrors.Add("error", authorizeNetCharge.Errors[0]);
                            }
                            FailurePayment(model);
                            return resultResponse;
                        }

                    }

                    if (authorizeNetCharge.Status && authorizeNetCharge.Response.messages.resultCode == messageTypeEnum.Ok)
                    {
                        ipn = Newtonsoft.Json.JsonConvert.SerializeObject(authorizeNetCharge);  //If charge was Error

                        if (authorizeNetCharge.Response.transactionResponse.messages != null)
                        {
                            try
                            {
                                ipn = Newtonsoft.Json.JsonConvert.SerializeObject(authorizeNetCharge);

                                if (model.ActionCaller == JumbulaSubSystem.Order || model.ActionCaller == JumbulaSubSystem.MakeaPayment)
                                {
                                    #region Register Order

                                    var isMakeaPayment = false;
                                    var isOnline = false;

                                    if (model.ActionCaller == JumbulaSubSystem.MakeaPayment)
                                    {
                                        isMakeaPayment = true;
                                    }

                                    if (order.OrderMode == OrderMode.Online)
                                    {
                                        isOnline = true;
                                    }

                                    if (model.PaymentMethod == PaymentMethod.Card)
                                    {
                                        var countActive = 0;
                                        var countInstallment = 0;
                                        var isSubscription = false;
                                        var orderItems = new List<OrderItem>();

                                        if (model.ActionCaller != JumbulaSubSystem.MakeaPayment)
                                        {
                                            orderItems = model.Cart.Order.OrderItems.Where(o => o.Order.IsAutoCharge).ToList();

                                            foreach (var item in orderItems)
                                            {
                                                countInstallment += item.Installments.Where(i => !i.IsDeleted).Count();
                                                if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                                {
                                                    isSubscription = true;
                                                }
                                            }
                                        }

                                        countActive = GetCountActive(userCreditCard);

                                        SpecifyDefault(countActive, model, countInstallment);

                                        if ((countActive < 1 && model.IsDefault == false && (countInstallment > 0 || orderItems.Count > 0)) || isSubscription)//in installment autocharge
                                        {
                                            model.IsDefault = true;
                                        }
                                        if (model.IsDefault)
                                        {
                                            if (CreditCardExistWithCustomer != null)
                                            {
                                                creditCard = CreditCardExistWithCustomer;
                                            }
                                            else
                                            {
                                                creditCard = Ioc.UserCreditCardBusiness.Get(modelParentBilling, userId, model.PaymentMethod, model.CreditId, _paymentGateway, model.Address);
                                            }
                                        }
                                        else
                                        {
                                            creditCard = Ioc.UserCreditCardBusiness.Get(modelParentBilling, userId, model.PaymentMethod, model.CreditId, _paymentGateway, model.Address);
                                        }

                                        if (creditCard != null && isCreditExist) // Exist Lastdigit in all credit cards in db 
                                        {
                                            var creditId = creditCard.Id;
                                            var CardId = creditCard.CardId;
                                            if (model.IsDefault)
                                            {
                                                createCustomerPaymentProfileResponse response = null;
                                                updateCustomerPaymentProfileResponse responseUpdate = null;

                                                if (string.IsNullOrEmpty(creditCard.CustomerId)) // if credit card exist in db not have StripeCustomerId
                                                {
                                                    if (!isCardCreated)
                                                    {
                                                        response = authorizeNetService.CreateAuthorizeNetPaymentProfile(modelCard, model.Address, userCustomerId);

                                                        if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
                                                        {
                                                            CardId = response.customerPaymentProfileId;
                                                            userCustomerResponse = JsonConvert.SerializeObject(response);
                                                            isCardCreated = true;
                                                        }
                                                        else if (response != null)
                                                        {
                                                            if (response.messages.message[0].code == "E00044")
                                                            {
                                                                resultResponse.cardErrors.Add("error", "Please contact your organization administrator. The Authorize.Net customer payment method is not properly configured.");
                                                            }
                                                            else
                                                            {
                                                                resultResponse.cardErrors.Add("error", response.messages.message[0].text);
                                                            }

                                                            FailurePayment(model);
                                                            return resultResponse;
                                                        }
                                                        else if (response == null)
                                                        {

                                                            resultResponse.cardErrors.Add("error", "The address you have entered is too long and cannot be processed. Please try another address or contact your organization administrator.");


                                                            FailurePayment(model);
                                                            return resultResponse;
                                                        }
                                                    }

                                                } //if credit card exist in db have StripeCustomerId we update a creditcard
                                                else
                                                {
                                                    modelParentBilling = GetModelParentBilling(model);


                                                    responseUpdate = authorizeNetService.UpdateAuthorizeNetCustomerForUser(modelParentBilling, model.Address, userCustomerId, CardId);

                                                    if (responseUpdate == null)
                                                    {
                                                        Log.Error("Response update credit card is null {LogCategory} {CardId}", "Payment", creditId);
                                                    }

                                                    if (responseUpdate != null && responseUpdate.messages.resultCode == messageTypeEnum.Ok)
                                                    {
                                                        userCustomerResponse = JsonConvert.SerializeObject(responseUpdate);
                                                    }
                                                    else if (responseUpdate != null)
                                                    {
                                                        if (responseUpdate.messages.message[0].code == "E00044")
                                                        {
                                                            resultResponse.cardErrors.Add("error", "Please contact your organization administrator. The Authorize.Net customer payment method is not properly configured.");
                                                        }
                                                        else
                                                        {
                                                            resultResponse.cardErrors.Add("error", responseUpdate.messages.message[0].text);
                                                        }

                                                        FailurePayment(model);
                                                        return resultResponse;
                                                    }

                                                }

                                                if (responseUpdate != null)
                                                {
                                                    var brand = GetBrandCard(model.CardNumber);
                                                    SetCreditCardForUpdate(model);

                                                    creditCard.CustomerId = userCustomerId;
                                                    creditCard.CardId = CardId;
                                                    creditCard.CustomerResponse = userCustomerResponse;
                                                    creditCard.Brand = brand;
                                                    creditCard.Id = creditId;

                                                    if (creditCard.CardId == "")
                                                    {
                                                        creditCard.CardId = customerPaymentProfileId;
                                                    }

                                                    //update in Authorize & db
                                                    foreach (var CreditCard in userCreditCard)
                                                    {
                                                        if (CreditCard.IsDefault)
                                                        {
                                                            CreditCard.IsDefault = false;
                                                            creditCards.Update(CreditCard);
                                                        }
                                                    }

                                                    creditCard.IsDefault = true;
                                                    creditCard.DefaultDate = DateTime.UtcNow;
                                                    creditCard.UpdatedDate = DateTime.UtcNow;
                                                    var status = creditCards.Update(creditCard);
                                                }
                                            }
                                            else
                                            {
                                                creditCard.Id = creditId;
                                                // just charge 
                                            }

                                        }
                                        else if (creditCard == null && isCreditExist) // one information is changed ,creditCard if =null was not card with all the same information.
                                        {
                                            createCustomerPaymentProfileResponse response = null;

                                            if (model.IsDefault)
                                            {
                                                if (!isCardCreated && CreditCardExistWithCustomer == null)
                                                {
                                                    response = authorizeNetService.CreateAuthorizeNetPaymentProfile(modelCard, model.Address, userCustomerId);

                                                    if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
                                                    {
                                                        if (userCreditCard.Any())
                                                        {
                                                            foreach (var CreditCard in userCreditCard)
                                                            {
                                                                if (CreditCard.IsDefault)
                                                                {
                                                                    CreditCard.IsDefault = false;
                                                                    creditCards.Update(CreditCard);
                                                                }
                                                            }
                                                        }
                                                        isCardCreated = true;
                                                        userCustomerResponse = JsonConvert.SerializeObject(response);

                                                        creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, response.customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                    }
                                                    else if (response != null)
                                                    {
                                                        if (response.messages.message[0].code == "E00044")
                                                        {
                                                            resultResponse.cardErrors.Add("error", "Please contact your organization administrator. The Authorize.Net customer payment method is not properly configured.");
                                                        }
                                                        else
                                                        {
                                                            resultResponse.cardErrors.Add("error", response.messages.message[0].text);
                                                        }

                                                        FailurePayment(model);
                                                        return resultResponse;
                                                    }
                                                }
                                                else if ((isCardCreated && CreditCardExistWithCustomer != null) || (!isCardCreated && CreditCardExistWithCustomer != null))
                                                {
                                                    modelParentBilling = GetModelParentBilling(model);
                                                    modelParentBilling.Id = CreditCardExistWithCustomer.Id;

                                                    updateCustomerPaymentProfileResponse responseUpdate;
                                                    responseUpdate = authorizeNetService.UpdateAuthorizeNetCustomerForUser(modelParentBilling, model.Address, userCustomerId, CreditCardExistWithCustomer.CardId);

                                                    userCustomerResponse = JsonConvert.SerializeObject(responseUpdate);

                                                    if (responseUpdate.messages.resultCode == messageTypeEnum.Ok)
                                                    {
                                                        creditCard = CreditCardExistWithCustomer;
                                                        var brand = GetBrandCard(model.CardNumber);
                                                        SetCreditCardForUpdate(model);

                                                        creditCard.CustomerId = userCustomerId;
                                                        creditCard.CustomerResponse = userCustomerResponse;
                                                        creditCard.CardId = CreditCardExistWithCustomer.CardId;
                                                        creditCard.Id = CreditCardExistWithCustomer.Id;
                                                        creditCard.Brand = brand;
                                                        //update in stripe & db
                                                        foreach (var CreditCard in userCreditCard)
                                                        {
                                                            if (CreditCard.IsDefault)
                                                            {
                                                                CreditCard.IsDefault = false;

                                                                creditCards.Update(CreditCard);
                                                            }
                                                        }
                                                        creditCard.IsDefault = true;
                                                        creditCard.DefaultDate = DateTime.UtcNow;
                                                        creditCard.UpdatedDate = DateTime.UtcNow;
                                                        creditCards.Update(creditCard);
                                                    }
                                                    else if (responseUpdate != null)
                                                    {
                                                        if (response.messages.message[0].code == "E00044")
                                                        {
                                                            resultResponse.cardErrors.Add("error", "Please contact your organization administrator. The Authorize.Net customer payment method is not properly configured.");
                                                        }
                                                        else
                                                        {
                                                            resultResponse.cardErrors.Add("error", response.messages.message[0].text);
                                                        }

                                                        FailurePayment(model);
                                                        return resultResponse;
                                                    }
                                                }
                                                else if (isCardCreated && CreditCardExistWithCustomer == null)
                                                {
                                                    creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                }
                                            }
                                            else
                                            {
                                                userCustomerId = "";
                                                creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                            }
                                        }
                                        else
                                        {
                                            creditCard = GetCreditCard(modelCard, userId, model.PaymentMethod, model.CreditId);

                                            if (userId > 0 && creditCard != null) //full Similar card in db
                                            {
                                                if (!model.IsDefault)
                                                {
                                                    if (string.IsNullOrEmpty(creditCard.CustomerId) && model.ActionCaller == JumbulaSubSystem.Order && order.IsAutoCharge)
                                                    {
                                                        creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                    }
                                                }
                                                if (model.IsDefault)
                                                {
                                                    if (string.IsNullOrEmpty(creditCard.CustomerId))
                                                    {
                                                        if (userCreditCard != null && userCreditCard.Any())
                                                        {
                                                            result = authorizeNetService.CreateAuthorizeNetCustomerId(modelCard, model.Address, order); //Create Customer and card in authorize.net

                                                            if (result != null && result.messages.resultCode == messageTypeEnum.Ok)
                                                            {
                                                                userCustomerResponse = JsonConvert.SerializeObject(result);

                                                                foreach (var CreditCard in userCreditCard)
                                                                {
                                                                    if (CreditCard.IsDefault)
                                                                    {
                                                                        CreditCard.IsDefault = false;
                                                                        creditCards.Update(CreditCard);
                                                                    }
                                                                }

                                                                creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, customerPaymentProfileId, userCustomerResponse, true, _paymentGateway, isMakeaPayment, isOnline);
                                                            }
                                                            else if (result != null)
                                                            {
                                                                if (result.messages.message[0].code == "E00044")
                                                                {
                                                                    resultResponse.cardErrors.Add("error", "Please contact your organization administrator. The Authorize.Net customer payment method is not properly configured.");
                                                                }
                                                                else
                                                                {
                                                                    resultResponse.cardErrors.Add("error", result.messages.message[0].text);
                                                                }
                                                                FailurePayment(model);
                                                                return resultResponse;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!string.IsNullOrEmpty(creditCard.CustomerId) && model.ActionCaller != JumbulaSubSystem.Order && !order.IsAutoCharge)
                                                        {
                                                            foreach (var CreditCard in userCreditCard)
                                                            {
                                                                if (CreditCard.IsDefault)
                                                                {
                                                                    CreditCard.IsDefault = false;
                                                                    creditCards.Update(CreditCard);
                                                                }
                                                            }
                                                            creditCard.IsDefault = true;
                                                            creditCard.DefaultDate = DateTime.UtcNow;
                                                            creditCards.Update(creditCard); //update payment methods for preferred payment methods}

                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (userCustomerId == "") // No select express payment methods available and no installment just save in db
                                                {
                                                    creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                }
                                                else if (userCustomerId != "" && model.PaymentMethod != PaymentMethod.Express)
                                                {
                                                    if (userCreditCard != null && userCreditCard.Any())
                                                    {
                                                        if (newDefault)
                                                        {
                                                            var response = authorizeNetService.CreateAuthorizeNetPaymentProfile(modelCard, model.Address, userCustomerId);
                                                            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
                                                            {
                                                                isCardCreated = true;
                                                                userCustomerResponse = JsonConvert.SerializeObject(response);
                                                                creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, response.customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                            }
                                                            else if (response != null)
                                                            {
                                                                if (response.messages.message[0].code == "E00044")
                                                                {
                                                                    resultResponse.cardErrors.Add("error", "Please contact your organization administrator. The Authorize.Net customer payment method is not properly configured.");
                                                                }
                                                                else
                                                                {
                                                                    resultResponse.cardErrors.Add("error", response.messages.message[0].text);
                                                                }

                                                                FailurePayment(model);
                                                                return resultResponse;
                                                            }
                                                            if (isUpdate)
                                                            {
                                                                if (userCreditCard != null && userCreditCard.Any())
                                                                {
                                                                    userCreditCardNotLast = userCreditCard.Take(userCreditCard.Count() - 1);//select items Except Last
                                                                    foreach (var CreditCard in userCreditCardNotLast)
                                                                    {
                                                                        if (CreditCard.IsDefault)
                                                                        {
                                                                            CreditCard.IsDefault = false;
                                                                            creditCards.Update(CreditCard);
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                        }
                                                        else
                                                        {
                                                            userCustomerId = "";
                                                            creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                        }

                                                    }
                                                    else  //no express payment method available and select pay by card and is installment(create in stripe first action) 
                                                    {
                                                        if (model.IsDefault)
                                                        {
                                                            if (!isCardCreated)
                                                            {
                                                                var response = authorizeNetService.CreateAuthorizeNetPaymentProfile(modelCard, model.Address, userCustomerId);

                                                                userCustomerResponse = JsonConvert.SerializeObject(response);

                                                                if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
                                                                {
                                                                    isCardCreated = true;
                                                                    creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, response.customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                                }
                                                                else if (response != null)
                                                                {
                                                                    RegisterError(userCustomerResponse, controllerContext, modelCard, userId, userCustomerId, model != null ? model.Address : null, true);
                                                                }
                                                                else if (response == null)
                                                                {
                                                                    RegisterError("respose is null", controllerContext, modelCard, userId, userCustomerId, model != null ? model.Address : null, true);
                                                                }

                                                            }
                                                            else
                                                            {
                                                                creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, customerPaymentProfileId, userCustomerResponse, newDefault, _paymentGateway, isMakeaPayment, isOnline);
                                                            }
                                                        }

                                                        if (isUpdate)
                                                        {
                                                            if (userCreditCard != null && userCreditCard.Any())
                                                            {

                                                                userCreditCardNotLast = userCreditCard.Take(userCreditCard.Count() - 1);//select items Except Last
                                                                foreach (var CreditCard in userCreditCardNotLast)
                                                                {
                                                                    if (CreditCard.IsDefault)
                                                                    {
                                                                        CreditCard.IsDefault = false;
                                                                        creditCards.Update(CreditCard);
                                                                    }
                                                                }

                                                            }
                                                        }

                                                    }

                                                }

                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else /*if (model.ActionCaller == JumbulaSubSystem.Invoice || model.ActionCaller == JumbulaSubSystem.Installment) //add for invoice  -Installment*/
                                {
                                    if (model.PaymentMethod == PaymentMethod.Card)
                                    {
                                        creditCard = Ioc.UserCreditCardBusiness.Get(modelParentBilling, userId, model.PaymentMethod, model.CreditId, _paymentGateway, model.Address);

                                        if (userId > 0 && creditCard == null)
                                        {

                                            if (userCustomerId == "")
                                            {
                                                creditCard = paymentService.CreateCreditCard(modelCard, userId, model.Address, userCustomerId, customerPaymentProfileId, userCustomerResponse, false, _paymentGateway, false, false);
                                            }
                                        }
                                    }
                                }
                                //update in db
                                resultResponse = SuccessPayment(model, authorizeNetCharge.Response, payableAmount, controllerContext, selectedCreditCard);
                                return resultResponse;
                            }
                            catch (Exception ex)
                            {
                                if (authorizeNetCharge.Response.messages.resultCode == messageTypeEnum.Ok)
                                {
                                    if (authorizeNetCharge.Response.transactionResponse.messages != null)
                                    {
                                        resultResponse.errorMsg = "You have successfully charged the user, but the balance may not reflect the correct amount. Do not charge the user again. Please contact Jumbula at support@jumbula.com and report this issue.";

                                        if (ex.InnerException == null)
                                        {
                                            resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);
                                            resultResponse.cardErrors.Add("error", resultResponse.errorMsg);
                                        }
                                        else
                                        {
                                            resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.InnerException.Message);
                                            resultResponse.cardErrors.Add("error", resultResponse.errorMsg);
                                        }

                                        if (WebConfigHelper.IsProductionMode)
                                        {
                                            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
                                            string emailSubject = "Error for " + model.ConfirmationId;
                                            string emailBody = ViewHelper.RenderViewToString(controllerContext, @"~/Views/Shared/Email/ErrorInTakePayment.cshtml", null);
                                            Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, emailBody, true);
                                        }

                                    }
                                    else
                                    {
                                        if (authorizeNetCharge.Response.transactionResponse.errors != null)
                                        {
                                            resultResponse.errorMsg = authorizeNetCharge.Response.transactionResponse.errors[0].errorText;
                                            resultResponse.cardErrors.Add("errorex" + rnd.Next(52), authorizeNetCharge.Response.transactionResponse.errors[0].errorText);
                                            resultResponse.cardErrors.Add("error", authorizeNetCharge.Response.transactionResponse.errors[0].errorText);

                                            FailurePayment(model);
                                            return resultResponse;

                                        }
                                    }

                                }
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }
                        else
                        {
                            if (authorizeNetCharge.Response.transactionResponse.errors != null)
                            {

                                resultResponse.errorMsg = authorizeNetCharge.Response.transactionResponse.errors[0].errorText;

                                if (resultResponse.errorMsg == "A duplicate transaction has been submitted.")
                                {
                                    resultResponse.errorMsg = "You have just submitted another transaction. Please wait 2 minutes and try again.";
                                }

                                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), resultResponse.errorMsg);
                                resultResponse.cardErrors.Add("error", resultResponse.errorMsg);

                                FailurePayment(model);
                                return resultResponse;

                            }

                        }
                    }
                    else if (!authorizeNetCharge.Status && authorizeNetCharge.Response != null)
                    {
                        if (authorizeNetCharge.Response.transactionResponse != null && authorizeNetCharge.Response.transactionResponse.errors != null)
                        {
                            if (authorizeNetCharge.Response.transactionResponse.errors[0].errorCode == "17")
                            {
                                resultResponse.errorMsg = authorizeNetCharge.Response.transactionResponse.errors[0].errorText;
                                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), authorizeNetCharge.Response.transactionResponse.errors[0].errorText);
                                resultResponse.cardErrors.Add("error", authorizeNetCharge.Response.transactionResponse.errors[0].errorText);
                            }
                            else
                            {
                                if (authorizeNetCharge.Response.transactionResponse.errors[0].errorText == "A duplicate transaction has been submitted.")
                                {
                                    resultResponse.errorMsg = "You have just submitted another transaction. Please wait 2 minutes and try again.";
                                }
                                else if (authorizeNetCharge.Response.transactionResponse.errors[0].errorText == "Customer Information Manager is not enabled.")
                                {
                                    resultResponse.errorMsg = "Please contact your organization administrator. The Authorize.Net payment method is not properly configured.";
                                }
                                else
                                {
                                    resultResponse.errorMsg = authorizeNetCharge.Response.transactionResponse.errors[0].errorText;

                                }
                                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), resultResponse.errorMsg);
                                resultResponse.cardErrors.Add("error", resultResponse.errorMsg);
                            }

                        }
                        else
                        {
                            resultResponse.errorMsg = authorizeNetCharge.Response.messages.message[0].text;
                            resultResponse.cardErrors.Add("errorex" + rnd.Next(52), authorizeNetCharge.Response.messages.message[0].text);
                            resultResponse.cardErrors.Add("error", authorizeNetCharge.Response.messages.message[0].text);
                        }

                    }
                    else
                    {
                        resultResponse.cardErrors.Add("errorex" + rnd.Next(52), authorizeNetCharge.Errors[0]);

                    }

                }

            }
            catch (Exception ex)
            {
                resultResponse.errorMsg = ex.Message;
                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);
                resultResponse.cardErrors.Add("error", ex.Message);


                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            FailurePayment(model);
            return resultResponse;
        }

        public string UpdateInstallmentBasedOnPayment(List<OrderInstallment> installmentList, createTransactionResponse authorizeNetCharge, string ipn, int? creditcardId, DateTime date, RoleCategoryType currentUserRoleType, decimal? payableAmount = null)
        {
            var result = string.Empty;

            try
            {
                var installmentDB = Ioc.InstallmentBusiness;
                if (installmentList == null && installmentList.SelectMany(c => c.TransactionActivities).Any(x => x.PaymentDetail.PayKey == authorizeNetCharge.transactionResponse.transHash))
                {
                    return "";
                }
                OperationStatus updateStatus = new OperationStatus() { Status = true };
                var currencyCode = CurrencyCodes.USD;

                PaymentDetail paymentDetail = new PaymentDetail(username, PaymentDetailStatus.COMPLETED, authorizeNetCharge.transactionResponse.transHash, PaymentMethod.Card, currencyCode, ipn, string.Empty, authorizeNetCharge.transactionResponse.transId, currentUserRoleType, creditcardId);


                var itemInstallment = installmentList.GroupBy(i => i.OrderItemId);
                var orderDb = Ioc.OrderBusiness;
                foreach (var installments in itemInstallment)
                {
                    var orderItem = Ioc.OrderItemBusiness.GetItem(installments.Key);
                    var currency = orderItem.Order.Club.Currency;
                    decimal totalpaidAmount = 0;
                    foreach (var item in installments)
                    {
                        var paidAmount = payableAmount.HasValue ? payableAmount.Value : item.Balance;
                        totalpaidAmount += paidAmount;
                        item.PaidAmount = paidAmount + (item.PaidAmount.HasValue ? item.PaidAmount.Value : 0);
                        item.PaidDate = date;
                        item.Status = OrderStatusCategories.completed;
                        orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Success, item, date, paidAmount);
                        installmentDB.Update(item);

                        var historyInstallment = Constants.OrderInstallment_Paid;

                        if (currentUserRoleType == RoleCategoryType.Parent)
                        {
                            historyInstallment = Common.Constants.Payment.ParentOrderInstallmentPaid;
                        }

                        Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Paid, string.Format(historyInstallment, CurrencyHelper.FormatCurrencyWithPenny(item.PaidAmount, currency), item.InstallmentDate.ToString(Constants.DefaultDateFormat)), item.OrderItem.Order_Id, userId, item.OrderItemId);

                    }

                    orderItem.PaidAmount += totalpaidAmount;
                    orderDb.Update(orderItem.Order);
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return ex.Message;
            }
        }

        public string UpdateOrderBasedOnPayment(ControllerContext controllerContext, Order order, createTransactionResponse authorizeNetCharge, string ipn, int? creditcardId, PaymentDetailStatus paymentStaus = PaymentDetailStatus.COMPLETED, TransactionStatus transactionStatus = TransactionStatus.Success, PaymentMethod paymentMethod = PaymentMethod.Card, List<TransactionActivity> transactions = null)
        {
            try
            {
                if (transactions == null)
                {

                    PaymentDetail paymentDetail = new PaymentDetail(username, paymentStaus, authorizeNetCharge.transactionResponse.transHash, paymentMethod, order.Club.Currency, ipn, string.Empty, authorizeNetCharge.transactionResponse.transId, JbUserService.GetCurrentUserRoleType(), creditcardId);

                    if (order.IsDraft == false && order.TransactionActivities != null && order.TransactionActivities.Count() > 0)
                    {
                        return "";
                    }
                    order.IsDraft = false;
                    if (order.OrderStatus == OrderStatusCategories.deleted)
                    {
                        order.OrderStatus = OrderStatusCategories.completed;
                    }

                    order.OrderStatus = OrderStatusCategories.completed;
                    var installmentToken = string.Empty;
                    var date = DateTime.UtcNow;
                    foreach (OrderItem item in order.RegularOrderItems)
                    {
                        item.CreditCardId = creditcardId;
                        item.ItemStatus = OrderItemStatusCategories.completed;
                        item.PaidAmount = paymentStaus == PaymentDetailStatus.COMPLETED ? item.TotalAmount : 0;
                        if (string.IsNullOrEmpty(installmentToken))
                        {
                            installmentToken = Guid.NewGuid().ToString("N");
                        }
                        if (item.PaymentPlanType == PaymentPlanType.Installment && item.Installments.Any())
                        {
                            if (item.ProgramTypeCategory != ProgramTypeCategory.Subscription && item.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                            {
                                var firstInst = item.Installments.Where(i => !i.IsDeleted).FirstOrDefault();
                                item.PaidAmount = paymentStaus == PaymentDetailStatus.COMPLETED ? firstInst.Amount : 0;
                                firstInst.PaidDate = date;
                                firstInst.PaidAmount = paymentStaus == PaymentDetailStatus.COMPLETED ? firstInst.Amount : 0;
                                firstInst.Status = OrderStatusCategories.completed;
                                firstInst.NoticeSent = true;
                                firstInst.Token = installmentToken;
                            }
                            else
                            {
                                Ioc.SubscriptionBusiness.SetInstallmentsStatus(item, date);
                            }
                        }
                    }
                    Ioc.OrderBusiness.SetPaymentTransaction(order, paymentDetail, transactionStatus, null, date);
                    Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Paid, string.Format(Constants.Order_Paid, order.ConfirmationId), order.Id, (JbUserService.IsUserAuthenticated()) ? userId : -1);


                    var result = Ioc.OrderBusiness.Update(order);

                }

                //add for update invoice transactions
                else
                {
                    var tranDb = Ioc.TransactionActivityBusiness;

                    PaymentDetail paymentDetail = new PaymentDetail(username, paymentStaus, authorizeNetCharge.transactionResponse.transId, paymentMethod, order.Club.Currency, ipn, string.Empty, authorizeNetCharge.transactionResponse.transId, JbUserService.GetCurrentUserRoleType(), creditcardId);
                    var result = tranDb.UpdateTransactions(transactions, paymentDetail);
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return ex.Message;
            }
        }

        public createCustomerProfileResponse CreateAuthorizeNetCustomerId(Order order)
        {
            if (IsTestMode)
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey,
            };

            customerProfileType customerProfile = new customerProfileType();

            customerProfile.email = order.User.UserName;
            customerProfile.description = string.Format("{0} ({1})", order.User.UserName, order.UserId);

            var request = new createCustomerProfileRequest { profile = customerProfile, validationMode = validationModeEnum.none };

            var controller = new createCustomerProfileController(request);          // instantiate the contoller that will call the service
            controller.Execute();

            return controller.GetApiResponse();   // get the response from the service (errors contained if any)

        }

        public createCustomerProfileResponse CreateAuthorizeNetCustomerId(PaymentModels card, PostalAddress address, Order order)
        {
            //create customer and card in authorize.net
            if (IsTestMode)
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey,
            };


            var names = JbBasePaymentGateway.SplitName(card.CardHolderName);
            var firstName = names[0];
            var lastName = names[1];

            var creditCard = new creditCardType
            {
                cardNumber = card.CardNumber,
                expirationDate = card.Month + card.Year.Substring(2, 2),

            };
            var billTo = new customerAddressType
            {
                firstName = firstName,
                lastName = lastName,
                address = address.Street,
                city = address.City,
                zip = address.Zip,
                state = address.State,
                country = address.Country,
            };
            paymentType cc = new paymentType { Item = creditCard };
            List<customerPaymentProfileType> paymentProfileList = new List<customerPaymentProfileType>();
            customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
            ccPaymentProfile.payment = cc;

            ccPaymentProfile.billTo = billTo;
            paymentProfileList.Add(ccPaymentProfile);

            customerAddressType homeAddress = new customerAddressType();

            customerProfileType customerProfile = new customerProfileType();

            customerProfile.email = order != null ? order.User.UserName : username;
            customerProfile.description = string.Format("{0} ({1})", order != null ? order.User.UserName : username, order != null ? order.UserId : userId);
            customerProfile.paymentProfiles = paymentProfileList.ToArray();


            var request = new createCustomerProfileRequest { profile = customerProfile, validationMode = validationModeEnum.none };

            var controller = new createCustomerProfileController(request);          // instantiate the contoller that will call the service
            controller.Execute();

            return controller.GetApiResponse();   // get the response from the service (errors contained if any)

        }

        public createCustomerPaymentProfileResponse CreateAuthorizeNetPaymentProfile(PaymentModels card, PostalAddress address, string userCustomerId)
        {
            if (IsTestMode)
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey,
            };
            var creditCard = new creditCardType
            {
                cardNumber = card.CardNumber,
                expirationDate = card.Month + card.Year.Substring(2, 2),
            };

            var names = JbBasePaymentGateway.SplitName(card.CardHolderName);
            var firstName = names[0];
            var lastName = names[1];

            paymentType paymentProfile = new paymentType { Item = creditCard };

            var billTo = new customerAddressType
            {
                firstName = firstName,
                lastName = lastName,
                address = address.Street,
                city = address.City,
                zip = address.Zip,
                state = address.State,
                country = address.Country,
            };
            customerPaymentProfileType customerPaymentProfile = new customerPaymentProfileType();
            customerPaymentProfile.payment = paymentProfile;
            customerPaymentProfile.billTo = billTo;

            customerPaymentProfileType customerProfile = new customerPaymentProfileType();


            var request = new createCustomerPaymentProfileRequest
            {
                customerProfileId = userCustomerId,
                validationMode = validationModeEnum.none,
                paymentProfile = customerPaymentProfile

            };
            var controller = new createCustomerPaymentProfileController(request);          // instantiate the contoller that will call the service
            controller.Execute();

            createCustomerPaymentProfileResponse response = controller.GetApiResponse();

            return response;
        }

        public creditCardMaskedType getCustomerPaymentProfile(string customerProfileId, string customerPaymentProfileId)
        {
            if (IsTestMode)
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey,
            };

            var request = new getCustomerPaymentProfileRequest();
            request.customerProfileId = customerProfileId;
            request.customerPaymentProfileId = customerPaymentProfileId;

            var controllerCustomer = new getCustomerPaymentProfileController(request);
            controllerCustomer.Execute();
            var response = controllerCustomer.GetApiResponse();

            return (response.paymentProfile.payment.Item as creditCardMaskedType);


        }

        public updateCustomerPaymentProfileResponse UpdateAuthorizeNetCustomerForUser(ParentBillingMethodsViewModel card, PostalAddress address, string customerId, string cardId)
        {
            try
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
                {
                    name = LoginId,
                    ItemElementName = ItemChoiceType.transactionKey,
                    Item = TransactionKey,
                };

                if (card.CardNumber == null)
                {
                    var result = getCustomerPaymentProfile(customerId, cardId);

                    if (result != null)
                    {
                        card.CardNumber = result.cardNumber;
                    }
                }

                var creditCard = new creditCardType
                {
                    cardNumber = card.CardNumber,
                    expirationDate = card.Month + card.Year2Char,
                    cardCode = card.CVV,
                };


                var paymentType = new paymentType { Item = creditCard };

                var names = JbBasePaymentGateway.SplitName(card.CardHolderName);
                var firstName = names[0];
                var lastName = names[1];

                var paymentProfile = new customerPaymentProfileExType
                {
                    billTo = new customerAddressType
                    {
                        // change information as required for billing
                        firstName = firstName,
                        lastName = lastName,
                        address = address.Street,
                        city = address.City,
                        state = address.State,
                        zip = address.Zip,
                        country = address.Country,

                    },
                    payment = paymentType,
                    customerPaymentProfileId = cardId.ToString()
                };

                var request = new updateCustomerPaymentProfileRequest();
                request.customerProfileId = customerId;
                request.paymentProfile = paymentProfile;
                request.validationMode = validationModeEnum.testMode;

                var controller = new updateCustomerPaymentProfileController(request);
                controller.Execute();

                // get the response from the service (errors contained if any)
                var response = controller.GetApiResponse();

                return response;// getCustomerPaymentProfile(customerId, cardId.ToString());
            }
            catch
            {
                throw;
            }

        }

        internal static UserCreditCard GetCreditCard(PaymentModels source, int userId, PaymentMethod method, int creditId)
        {
            if (userId > 0)
            {
                //PostalAddress postalAddress = new PostalAddress();
                //Utilities.GetGEO(source.StrAddress, ref postalAddress);

                if (method != PaymentMethod.Express)
                {
                    var cards = Ioc.UserCreditCardBusiness.GetAllList(userId, PaymentGateway.AuthorizeNet);
                    if (cards.Any())
                    {
                        var userCard = cards.FirstOrDefault(c => c.Address.Address == source.Address.Address && c.LastDigits == source.CardNumber.Substring((source.CardNumber.Length) - 4, 4) && c.Name == source.CardHolderName && c.ExpiryMonth == source.Month && c.ExpiryYear == source.Year);
                        if (userCard != null)
                        {
                            return userCard;
                        }
                    }
                }
                else
                {
                    var cards = Ioc.UserCreditCardBusiness.Get(creditId);
                    if (cards != null)
                    {
                        return cards;
                    }
                }

            }
            return null;
        }

        public string SucceedInvoicePayment(ControllerContext controllerContext, List<TransactionActivity> transactions, createTransactionResponse authorizeNetCharge, string ipn, int? creditcardId, bool backToDetail = false)
        {
            try
            {
                var club = transactions.First().Club;

                var tranDb = Ioc.TransactionActivityBusiness;
                if (transactions == null || transactions.Count == 0)
                {
                    return "Invalid Token";
                }
                var paidAmount = transactions.Sum(c => c.Amount);
                var orderItem = Ioc.OrderItemBusiness.GetItem(transactions.FirstOrDefault().OrderItemId.Value);
                var invoiceCategory = transactions.Select(t => t.TransactionCategory).First();

                //if (Ioc.TransactionActivityBusiness.IsPayKeyExist(authorizeNetCharge.transactionResponse.transHash))
                //{
                //    return string.Empty;
                //}

                PaymentDetail paymentDetail = new PaymentDetail(username, PaymentDetailStatus.COMPLETED, authorizeNetCharge.transactionResponse.transHash, PaymentMethod.Card, transactions.FirstOrDefault().Club.Currency, ipn, string.Empty, authorizeNetCharge.transactionResponse.transId, JbUserService.GetCurrentUserRoleType(), creditcardId);

                var date = DateTime.UtcNow;

                var invoice = transactions.First().PaymentDetail.invoice;
                var userId = invoice.UserId;
                invoice.PaidAmount = paidAmount;
                invoice.Status = InvoiceStatus.Paid;
                Ioc.InvoiceBusiness.Update(invoice);
                if (invoiceCategory != TransactionCategory.Invoice)
                {
                    foreach (var transaction in transactions)
                    {
                        transaction.PaymentDetail.Currency = paymentDetail.Currency;
                        transaction.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                        transaction.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                        transaction.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                        transaction.PaymentDetail.Status = paymentDetail.Status;
                        transaction.PaymentDetail.PayKey = paymentDetail.PayKey;
                        transaction.PaymentDetail.PayerType = paymentDetail.PayerType;
                        transaction.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                        transaction.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                        transaction.PaymentDetail.CreditCardId = paymentDetail.CreditCardId;
                        transaction.TransactionStatus = TransactionStatus.Success;
                        transaction.TransactionDate = date;
                        tranDb.Update(transaction);

                        if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                        {
                            var installment = Ioc.InstallmentBusiness.Get(transaction.InstallmentId.Value);
                            installment.PaidAmount = (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0) + transaction.Amount;
                            installment.PaidDate = date;
                            installment.Status = OrderStatusCategories.completed;
                            Ioc.InstallmentBusiness.Update(installment);

                        }

                    }
                    var invoiceHistoryDescription = Constants.Order_InvoiceHistory;
                    var invoiceHistoryDescriptionPaided = Constants.Invoice_Paided;

                    if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                    {
                        invoiceHistoryDescription = Common.Constants.Payment.ParentInvoiceDescription;
                        invoiceHistoryDescriptionPaided = Common.Constants.Payment.ParentInvoiceDescriptionPaided;
                    }

                    foreach (var transaction in transactions)
                    {
                        if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                        {
                            var orderItemInInvoice = transaction.OrderItem;
                            var instalment = transaction.Installment;
                            var InstalmentordeItamBalance = transaction.Amount;
                            orderItemInInvoice.PaidAmount += transaction.Amount;
                            Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(invoiceHistoryDescription, InstalmentordeItamBalance), orderItemInInvoice.Order_Id, userId, orderItemInInvoice.Id);

                        }
                        else
                        {
                            var orderItemInInvoice = transaction.OrderItem;
                            var ordeItamBalance = transaction.Amount;
                            orderItemInInvoice.PaidAmount += transaction.Amount;
                            Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(invoiceHistoryDescription, ordeItamBalance), orderItemInInvoice.Order_Id, userId, orderItemInInvoice.Id);
                        }

                    }
                    var invoiceId = invoice.Id;
                    var userName = invoice.User.UserName;
                    var today = DateTime.UtcNow;
                    var clubTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, today);
                    var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(invoiceHistoryDescriptionPaided, userName, CurrencyHelper.FormatCurrencyWithPenny(paidAmount, club.Currency)) };
                    Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);

                    var totalPaidAmount = transactions.Sum(c => c.Amount);

                    EmailService.Get(controllerContext).SendReceivedFamilyInvoicePaymentEmail(transactions, totalPaidAmount, DateTime.UtcNow, paymentDetail, invoice);

                    return string.Empty;

                }
                else
                {
                    //var invoice = transactions.First().PaymentDetail.invoice;
                    //invoice.PaidAmount = paidAmount;
                    //invoice.Status = InvoiceStatus.Paid;
                    //Ioc.InvoiceBusiness.Update(invoice);
                    foreach (var transaction in transactions)
                    {
                        transaction.PaymentDetail.Currency = paymentDetail.Currency;
                        transaction.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                        transaction.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                        transaction.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                        transaction.PaymentDetail.Status = paymentDetail.Status;
                        transaction.PaymentDetail.PayKey = paymentDetail.PayKey;
                        transaction.PaymentDetail.PayerType = paymentDetail.PayerType;
                        transaction.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                        transaction.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                        transaction.PaymentDetail.CreditCardId = paymentDetail.CreditCardId;
                        transaction.TransactionStatus = TransactionStatus.Success;
                        transaction.TransactionDate = date;
                        tranDb.Update(transaction);

                        if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                        {
                            var installment = Ioc.InstallmentBusiness.Get(transaction.InstallmentId.Value);
                            installment.PaidAmount = (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0) + transaction.Amount;
                            installment.PaidDate = date;
                            installment.Status = OrderStatusCategories.completed;
                            Ioc.InstallmentBusiness.Update(installment);

                        }

                    }
                    foreach (var transaction in transactions)
                    {
                        if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                        {
                            var orderItemInInvoice = transaction.OrderItem;
                            var instalment = transaction.Installment;
                            var InstalmentordeItamBalance = transaction.Amount;
                            orderItemInInvoice.PaidAmount += transaction.Amount;
                            Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(Constants.Order_InvoiceHistory, InstalmentordeItamBalance, transaction.Note), orderItemInInvoice.Order_Id, (JbUserService.IsUserAuthenticated()) ? userId : -1, orderItemInInvoice.Id);

                        }
                        else
                        {
                            var orderItemInInvoice = transaction.OrderItem;
                            var ordeItamBalance = transaction.Amount;
                            orderItemInInvoice.PaidAmount += transaction.Amount;
                            Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(Constants.Order_InvoiceHistory, ordeItamBalance, transaction.Note), orderItemInInvoice.Order_Id, (JbUserService.IsUserAuthenticated()) ? userId : -1, orderItemInInvoice.Id);
                        }

                    }
                    var invoiceId = invoice.Id;
                    var userName = invoice.User.UserName;
                    var today = DateTime.UtcNow;
                    var clubTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, today);
                    if (backToDetail)
                    {
                        var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(Constants.Invoice_TakePaymentWithCreditcard, userName, CurrencyHelper.FormatCurrencyWithPenny(paidAmount, club.Currency)) };
                        Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
                    }
                    else
                    {
                        var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(Constants.Invoice_Paided, userName, CurrencyHelper.FormatCurrencyWithPenny(paidAmount, club.Currency)) };
                        Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
                    }


                    var totalPaidAmount = transactions.Sum(c => c.Amount);

                    EmailService.Get(controllerContext).SendReceivedFamilyInvoicePaymentEmail(transactions, totalPaidAmount, DateTime.UtcNow, paymentDetail, invoice);

                    return string.Empty;
                }
            }


            catch (Exception e)
            {
                LogHelper.LogException(e);

                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                return e.Message;

            }

        }

        public string SucceedTakePayment(ControllerContext controllerContext, List<TransactionActivity> transactions, createTransactionResponse charge, string ipn, int? creditCardId, bool isFamilyTakePayment = false)
        {
            if (transactions != null && transactions.Count > 0)
            {
                var errorMsg = string.Empty;

                PaymentDetail paymentDetail = new PaymentDetail(username, PaymentDetailStatus.COMPLETED, charge.transactionResponse.transHash, PaymentMethod.Card, transactions.First().Club.Currency, ipn, string.Empty, charge.transactionResponse.transId, JbUserService.GetCurrentUserRoleType(), creditCardId);


                var orderItems = transactions.Select(t => t.OrderItem).GroupBy(i => i.Id).ToList();

                decimal totalPaidAmount = transactions.Sum(c => c.Amount);

                var selectedInstallment = new List<OrderInstallment>();
                foreach (var tran in transactions)
                {
                    if (tran.InstallmentId.HasValue && tran.InstallmentId.Value > 0)
                    {
                        selectedInstallment.Add(Ioc.InstallmentBusiness.Get(tran.InstallmentId.Value));
                    }
                }
                OperationStatus result = null;

                if (isFamilyTakePayment == false)
                {
                    var orderItem = transactions.First().OrderItem;

                    result = OrderServices.Get().ReceivedPayment(orderItem.Id, totalPaidAmount, paymentDetail, transactions.First().Note, transactions.First().TransactionDate, transactions.First().CheckId, selectedInstallment, transactions);
                    if (result.Status)
                    {
                        EmailService.Get(controllerContext).SendReceivedPaymentEmail(orderItem, totalPaidAmount, transactions.First().TransactionDate, paymentDetail);
                    }

                    return result.Status == false ? result.ExceptionMessage : errorMsg;
                }
                else
                {
                    var listOderItems = new List<OrderItem>();

                    listOderItems.AddRange(orderItems.Select(o => o.FirstOrDefault()));

                    result = OrderServices.Get().ReceivedPaymentFromMultiOrder(listOderItems, null, totalPaidAmount, paymentDetail, transactions.First().Note, transactions.First().TransactionDate, transactions.First().CheckId, selectedInstallment, transactions);

                    if (result.Status)
                    {
                        EmailService.Get(controllerContext).SendReceivedFromCreditCardFamilyTakePaymentEmail(transactions, transactions.First().TransactionDate, paymentDetail, transactions.First().Order.UserId, transactions.First().ClubId);
                    }
                    return result.Status == false ? result.ExceptionMessage : errorMsg;
                }


            }

            return "No Element Found";
        }

        public deleteCustomerPaymentProfileResponse DeleteCustomerPaymentProfile(string customerProfileId, string customerPaymentProfileId)
        {
            if (IsTestMode)
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            }
            else
            {
                ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
            }
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = TransactionKey,
            };

            //please update the subscriptionId according to your sandbox credentials
            var request = new deleteCustomerPaymentProfileRequest
            {
                customerProfileId = customerProfileId,
                customerPaymentProfileId = customerPaymentProfileId
            };

            //Prepare Request
            var controller = new deleteCustomerPaymentProfileController(request);
            controller.Execute();

            //Send Request to EndPoint
            deleteCustomerPaymentProfileResponse response = controller.GetApiResponse();


            return response;

        }

        private PaymentResponseModel SuccessPayment(PayViewModel model, createTransactionResponse authorizeNetCharge, decimal payableAmount, ControllerContext controllerContext, UserCreditCard selectedCreditCard)
        {

            int? creditCardId;
            creditCardId = creditCard != null ? creditCardId = creditCard.Id : selectedCreditCard != null ? creditCardId = selectedCreditCard.Id : null;

            switch (model.ActionCaller)
            {
                case JumbulaSubSystem.Installment:
                    {
                        var date = DateTime.UtcNow;
                        resultResponse.errorMsg = UpdateInstallmentBasedOnPayment(installments, authorizeNetCharge, ipn, creditCardId, date, JbUserService.GetCurrentUserRoleType(), payableAmount);
                        if (string.IsNullOrEmpty(resultResponse.errorMsg))
                        {
                            EmailService.Get(controllerContext).SendInstallmentPaymentConfirmation(installments.Where(i => !i.IsDeleted).First().Token, date, false);
                        }
                        return resultResponse;
                    }
                case JumbulaSubSystem.Order:
                    {
                        resultResponse.errorMsg = UpdateOrderBasedOnPayment(controllerContext, order, authorizeNetCharge, ipn, creditCardId);

                        return resultResponse;// if select express payment method 
                    }
                case JumbulaSubSystem.Invoice:
                    {
                        if (!string.IsNullOrEmpty(model.Token))
                        {
                            resultResponse.errorMsg = SucceedInvoicePayment(controllerContext, transactions, authorizeNetCharge, ipn, creditCardId, model.BackToDetail);
                            var invoiceId = transactions.First().PaymentDetail.InvoiceId;
                        }
                        else
                        {
                            if (model.PaymentMethod == PaymentMethod.Card)
                            {
                                resultResponse.errorMsg = SucceedInvoicePayment(controllerContext, transactions, authorizeNetCharge, ipn, creditCardId);
                            }
                            else
                            {
                                resultResponse.errorMsg = SucceedInvoicePayment(controllerContext, transactions, authorizeNetCharge, ipn, creditCardId);
                                return resultResponse;


                            }
                        }

                        return resultResponse;
                    }
                case JumbulaSubSystem.MakeaPayment:
                    {
                        try
                        {
                            resultResponse.errorMsg = SucceedTakePayment(controllerContext, transactions, authorizeNetCharge, ipn, creditCardId, model.IsFamilyTakePayment);

                            return resultResponse;
                        }
                        catch (Exception ex)
                        {
                            resultResponse.errorMsg = "You have successfully charged the user, but the balance may not reflect the correct amount. Do not charge the user again. Please contact Jumbula at support@jumbula.com and report this issue.";

                            if (ex.InnerException == null)
                            {
                                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.Message);
                            }
                            else if (ex.InnerException != null)
                            {
                                resultResponse.cardErrors.Add("errorex" + rnd.Next(52), ex.InnerException.Message);
                            }

                            if (WebConfigHelper.IsProductionMode)
                            {
                                var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
                                string emailSubject = "Error in TakePayment Success payment " + club.Name;
                                string emailBody = ViewHelper.RenderViewToString(controllerContext, @"~/Views/Shared/Email/ErrorInTakePayment.cshtml", null);
                                Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, emailBody, true);
                            }
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            return resultResponse;
                        }
                    }
            }

            throw new NotSupportedException("This type of payment don't supported.");
        }


        private void RegisterError(string response, ControllerContext controllerContext, PaymentModels model, int userid, string userCustomerId, PostalAddress address, bool sendMail = false)
        {

            if (sendMail)
            {
                try
                {
                    //Send to support
                    EmailService.Get(controllerContext).SendRegisterError(response, model, userid, userCustomerId, address, club);
                }
                catch { }
            }
        }

        public RefundResultModel Refund(TransactionActivity transactionActivity, string clubDomain, decimal refundAmount, bool isTestMode, Dictionary<string, string> metadata)
        {
            var result = new RefundResultModel();

            try
            {
                var authorizeNetService = new JbAuthorizeNetService(clubDomain, isTestMode);

                if (IsTestMode)
                {
                    ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
                }
                else
                {
                    ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
                }

                var response = authorizeNetService.Refund(transactionActivity.PaymentDetail.TransactionId, refundAmount, metadata);
                result.Response = JsonConvert.SerializeObject(response);
               
                // validate response
                ValidateResponse(response, result);
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.ErrorMessage = ex.Message;
                Log.Error(ex, "Error in refund by authorize {TransactionId}", transactionActivity.PaymentDetail.TransactionId);
            }

            return result;
        }

        private void ValidateResponse(createTransactionResponse response, RefundResultModel model)
        {
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.transactionResponse.messages != null)
                    {
                        model.Status = true;
                        model.TransactionId = response.transactionResponse.transId;
                    }
                    else
                    {
                        model.Status = false;
                        model.ErrorMessage = FailProcessRefund;
                        model.TransactionId = response.transactionResponse.transId;

                        if (response.transactionResponse.errors != null)
                        {
                            model.ErrorMessage = response.transactionResponse.errors[0].errorText;
                        }

                        Log.Error("Refund fail {TransactionId}", model.TransactionId);
                    }
                    
                }
                else
                {
                    model.Status = false;
                    model.ErrorMessage = FailProcessRefund;
                    model.TransactionId = response.transactionResponse.transId;
              
                    if (response.transactionResponse != null && response.transactionResponse.errors != null)
                    {
                        model.ErrorMessage = response.transactionResponse.errors[0].errorText;
                    }
                    else
                    {
                        model.ErrorMessage = response.messages.message[0].text;
                    }

                    Log.Error("Refund fail {TransactionId}", model.TransactionId);
                }
            }
            else
            {
                model.Status = false;
                model.TransactionId = response.transactionResponse.transId;
                model.ErrorMessage = FailProcessRefund;
                Log.Error("Refund response is null {TransactionId}", model.TransactionId);
            }
        }

        public PaymentDetailModel GetPaymentDetails(string clubDomain, string payKey, IEnumerable<TransactionActivity> oldRefunded, bool isPaypalTransaction, bool isTestMode)
        {
            var authorizeNetService = new JbAuthorizeNetService(clubDomain, isTestMode);

            var payDetailAuthorize = authorizeNetService.GetTransactionDetails(payKey);

            if (payDetailAuthorize != null)
                return new PaymentDetailModel
                {
                    Amount = payDetailAuthorize.transaction.authAmount,
                    AmountRefunded = oldRefunded.Sum(t => t.Amount),
                    Status = payDetailAuthorize.messages.resultCode == messageTypeEnum.Ok
                };

            Log.Error("Transaction details response is null {payKey}", payKey);

            return new PaymentDetailModel
            {
                Amount = 0,
                AmountRefunded = 0,
                Status = false
            };

        }
    }

}