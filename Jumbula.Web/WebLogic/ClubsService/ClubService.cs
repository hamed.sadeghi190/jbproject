﻿using System;
using System.Web;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.WebLogic.ClubsService
{
    public class ClubService
    {
        private ClubService()
        {
        }

        public static ClubService Get()
        {
            return new ClubService();
        }

        public ClubBaseInfoModel GetClubBaseInfo()
        {
            string clubSubDomain;

            var result = new ClubBaseInfoModel();

            if (!HttpContext.Current.Request.Url.HasSubDaomin())
            {
                throw new Exception("Club subdomain not found.");
            }
            else
            {
                clubSubDomain = HttpContext.Current.Request.Url.GetSubDomain();

                var club = Ioc.ClubBusiness.GetBaseInfo(clubSubDomain);

                result = new ClubBaseInfoModel()
                {
                    Id = club.Id,
                    ClubType = club.ClubType,
                    Domain = club.Domain,
                    TimeZone = club.TimeZone,
                    Site = club.Site,
                    Logo = UrlHelpers.GetClubLogoUrl(clubSubDomain, club.Logo),
                    Name = club.Name,
                    Currency = club.Currency,
                    PartnerId=club.PartnerId
                };
            }

            return result;
        }

        public ClubBaseInfoModel GetClubBaseInfo(Core.Domain.Club club)
        {
            var result = new ClubBaseInfoModel()
            {
                Id = club.Id,
                ClubType = club.ClubType,
                Domain = club.Domain,
                TimeZone = club.TimeZone,
                Logo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo),
                Name = club.Name,
                Currency = club.Currency,
                PartnerId= club.PartnerId
            };

            if(club.PartnerId.HasValue && club.PartnerClub != null)
            {
                result.PartnerLogo = UrlHelpers.GetClubLogoUrl(club.PartnerClub.Domain, club.PartnerClub.Logo);

                var partnerRegistrationPageLogoRedirectUrl = UrlHelpers.GetProperUrl(club.PartnerClub.Setting.RegistrationPageLogoRedirectUrl, club.PartnerClub.Site, HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));

                result.PartnerSite = partnerRegistrationPageLogoRedirectUrl;
            }

            var clubRegistrationPageLogoRedirectUrl = UrlHelpers.GetProperUrl(club.Setting.RegistrationPageLogoRedirectUrl, club.Site, HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority));

            result.Site = clubRegistrationPageLogoRedirectUrl;

            return result;
        }

        public string GetDynamicStyle (ClubAppearanceSetting clubApearance)
        {
            var result = string.Empty;

            if (clubApearance == null)
            {
                return string.Empty;
            }

            var erButtonBackColor = !string.IsNullOrEmpty(clubApearance.ERButtonBackColor) ? clubApearance.ERButtonBackColor : Constants.DefaultERButtonBackColor;
            var erButtonTextColor = !string.IsNullOrEmpty(clubApearance.ERButtonTextColor) ? clubApearance.ERButtonTextColor : Constants.DefaultERButtonTextColor;
            var contactBoxBackgroundColor = !string.IsNullOrEmpty(clubApearance.ContactBoxBackColor) ? clubApearance.ContactBoxBackColor : Constants.DefaultContactBoxBackColor;
            var testimonialsBoxBackgroundColor = !string.IsNullOrEmpty(clubApearance.TestimonialsBoxBackColor) ? clubApearance.TestimonialsBoxBackColor : Constants.DefaultTestimonialsBoxBackColor;
            var testimonialsBoxColor = !string.IsNullOrEmpty(clubApearance.TestimonialsBoxColor) ? clubApearance.TestimonialsBoxColor : Constants.DefaultTestimonialsBoxColor;
            var testimonialsTitle = !string.IsNullOrEmpty(clubApearance.TestimonialsTitle) ? clubApearance.TestimonialsTitle : Constants.DefaultTestimonialsTitle;

            var priceTextColor = !string.IsNullOrEmpty(clubApearance.PriceTextColor) ? clubApearance.PriceTextColor : Constants.DefualtPriceTextColor;
            var tuitionLabelTextColor = !string.IsNullOrEmpty(clubApearance.TuitionLabelTextColor) ? clubApearance.TuitionLabelTextColor : Constants.DefaultTuitionLabeltextColor;
            var priceBackColor = !string.IsNullOrEmpty(clubApearance.PriceBackColor) ? clubApearance.PriceBackColor : Constants.DefaultPriceBackColor;

            result += string.Format(".btn-jb, .btn-jb:hover, .btn-jb[disabled], #registrationOptions .schedule .charges ul li .plus {{background-color: {0};!important}} ", erButtonBackColor);
            result += string.Format(".btn-jb, .btn-jb:hover, .btn-jb:focus {{color: {0};!important}} ", erButtonTextColor);
            result += string.Format("#programInfo #callMap > a, #sideBar #contactBox .program-section-title, #sideBar #contactBox, #sideBar #contactBox .container-bg{{background-color: {0};}} ", contactBoxBackgroundColor);
            result += string.Format("#dates .start span, #dates .end span {{color: {0};}} ", contactBoxBackgroundColor);
            result += string.Format("#sideBar #testimonialsBox .program-section-title, #sideBar #testimonialsBox,.container-testimonialsBox * {{background-color: {0} !important;}}", testimonialsBoxBackgroundColor);
            result += string.Format("#sideBar #testimonialsBox .program-section-title, #sideBar #testimonialsBox,.container-testimonialsBox * {{color: {0};}} ", testimonialsBoxColor);

            result += string.Format(".tuition-prices span {{color: {0}!important;}} ", priceTextColor);
            result += string.Format(".tuition-label, .tuition-select, .amount{{color: {0}!important}} ", tuitionLabelTextColor);
            result += string.Format(".tuition .tuition-prices,.tuition .tuition-prices:before {{ background-color: {0}; }}", priceBackColor);
            result += string.Format(".tuition {{color: {0}; border: 1px solid {0}; border-left-color: transparent; border-top-color: transparent; border-right-color: transparent; }}", priceBackColor);

            return result;
        }
   
    }
}