
GO

/****** Object:  Table [dbo].[JbUserClaims]    Script Date: 23/02/1395 10:26:36 �.� ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JbUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.JbUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[JbUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbUserClaims_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[JbUserClaims] CHECK CONSTRAINT [FK_dbo.JbUserClaims_dbo.JbUsers_UserId]
GO


