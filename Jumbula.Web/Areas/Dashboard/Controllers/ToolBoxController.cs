﻿using Jumbula.Core.Business;
using Jumbula.Core.Model.Toolbox;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using System.Web.Mvc;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class ToolBoxController : DashboardBaseController
    {
        private readonly IClientBusiness _clientBusiness;

        public ToolBoxController(IClientBusiness clientBusiness)
        {
            _clientBusiness = clientBusiness;
        }

        [HttpGet]
        public ActionResult GetGoogleAnalytics()
        {
            return JsonNet(_clientBusiness.GetToolboxGoogleAnalytics(this.GetCurrentClubId()));
        }

        [HttpPost]
        public ActionResult SaveGoogleAnalytics(ToolboxGoogleAnalyticsViewModel model)
        {
            return JsonNet(_clientBusiness.SaveToolboxGoogleAnalytics(model, this.GetCurrentClubId()));
        }
    }
}