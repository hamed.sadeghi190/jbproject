﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Jumbula.Core.Model.People
{
    public class DelinquentFilterViewModel
    {
        public List<SelectListItem> Status { get; set; }
        public List<SelectListItem> Attempt { get; set; }
        public List<SelectListItem> Policy { get; set; }
        public List<SelectListItem> Season { get; set; }
        public List<SelectListItem> Program { get; set; }
        public string Confirmation { get; set; }
        public string Participant { get; set; }
        public string User { get; set; }
        public float Balance { get; set; }
        public float Paid { get; set; }
        public float Amount { get; set; }
    }
}
