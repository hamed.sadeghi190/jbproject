﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CatalogOverviewActivityReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};


        $scope.Model = customReportService.getModelFromStrorage();


        $http.get(baseUrl + '/GetCatalogOverviewActivityReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            dataInfo = data;
            prepareGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        var prepareGrid = function () {
            $scope.CatalogOverviewActivityDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetCatalogOverviewActivityReportInfo",
                        dataType: "json",
                        type: "GET",
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 100,
                resizable: true,
                batch: true,
            });

            $scope.CatalogOverviewActivityGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Catalog overview).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Catalog overview - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.CatalogOverviewActivityDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#CatalogOverviewActivityGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "Name",
                        title: "Provider",
                        width: "150px",
                        sortable: true,
                        template: '<a href="#=Domain#" target="_blank"> #=Name# </a>'
                    }, {
                        field: "CatalogItemCount",
                        title: "Number of activity",
                        width: "150px",
                        sortable: true
                    }, {
                        field: "LastUpdateCatalog",
                        title: "Last updated",
                        width: "120px",
                        sortable: true,
                    }, {
                        field: "Status",
                        title: "Active",
                        width: "70px",
                        sortable: true
                    },
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#CatalogOverviewActivityGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();