﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsAdditionalController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', function ($scope, $http, jbToast, $stateParams, $state) {
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSettingsRegistration", order: 4, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;

        var getAllCapacities = function () {

            var result = [];

            for (var i = 0; i < 21; i++) {
                result.push({ Value: i, Text: i });
            }

            return result;
        }

        $scope.allCapacities = getAllCapacities();

        $http.get('Dashboard/Season/GetSeasonSchoolSettingAdditional?seasonDomain=' + $stateParams.seasonDomain).success(function (data, status, headers, config) {

            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });



        $scope.save = function () {

            $http.post('Dashboard/Season/SaveSeasonSchoolSettingAdditional', { model: $scope.Model, seasonDomain: $stateParams.seasonDomain })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     $scope.errors = null;
                     jbToast.success("Program information updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }
    }])

})();