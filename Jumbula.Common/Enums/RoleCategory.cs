﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Jumbula.Common.Attributes;


namespace Jumbula.Common.Enums
{
    public enum RoleCategory
    {
        [RoleType(RoleCategoryType.System)]
        [Description("System admin")]
        SysAdmin = 1,
        [RoleType(RoleCategoryType.System)]
        [Description("Support")]
        Support = 2,

        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Partner")]
        Partner = 11,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Admin")]
        Admin = 12,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Instructor")]
        Instructor = 13,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Manager")]
        Manager = 14,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Owner")]
        Owner = 15,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Contributor")]
        Contributor = 16,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Accountant")]
        Accountant = 17,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("School contributor")]
        SchoolContributor = 18,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Onsite coordinator")]
        OnsiteCoordinator = 19,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Onsite support manager")]
        OnsiteSupportManager = 20,
        [RoleType(RoleCategoryType.Dashboard)]
        [Description("Restricted manager")]
        RestrictedManager = 21,


        [RoleType(RoleCategoryType.Parent)]
        [Description("Parent")]
        Parent = 31,
    }

}
