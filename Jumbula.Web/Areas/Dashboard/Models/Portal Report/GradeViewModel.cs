﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class GradeViewModel
    {
        public List<SelectKeyValue<string>> Grades { get; set; }
        public SchoolGradeType Grade { get; set; }
    }
}