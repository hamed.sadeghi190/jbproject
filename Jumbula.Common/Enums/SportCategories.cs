﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum SportCategories
    {
        None = 0,
        Chess = 1, // start with 1 b/c the Id of first sport entry is db is 1
        [Display(Name = "Table tennis")]
        [Description("Table tennis")]
        TableTennis = 2,
        [Display(Name = "Wrestling")]
        Wrestling = 3,
        Judo = 4,
        Hockey = 5,
        Shooting = 6,
        [Display(Name = "Water Polo")]
        WaterPolo = 7,
        [Display(Name = "Weight Lifting")]
        WeightLifting = 8,
        [Display(Name = "Track And Field")]
        TrackAndField = 9,
        Karate = 10,
        Archery = 11,
        Fencing = 12,
    }
}
