﻿using Destructurama;
using Jumbula.Web.Mvc.WebConfig;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using SerilogWeb.Classic;
using SerilogWeb.Classic.Enrichers;
using System.Diagnostics;
using System.Linq;

namespace Jumbula.Bootstrapper
{

    public partial class Startup
    {
        public void ConfigureLog(bool enableGlobalHttpRequestLogging = true)
        {
            // Initialize global logging configuration
            var loggingLevelSwitch = new LoggingLevelSwitch(); // Allow log level to be controlled by Seq
            var loggerConfiguration = new LoggerConfiguration()
                .Destructure.UsingAttributes()
                .MinimumLevel.ControlledBy(loggingLevelSwitch)
                .Enrich.FromLogContext()
                .Enrich.With<HttpRequestIdEnricher>()
                .Enrich.With<CallerEnricher>();
            // May not want to use SerilogWeb.Classic logging because we want more flexibility using our HttpRequestLogHandler
            SerilogWebClassic.Configure(cfg => cfg.Enable());

            if (enableGlobalHttpRequestLogging)
            {
                loggerConfiguration = loggerConfiguration
                    .Enrich.With<HttpRequestClientHostIPEnricher>()
                    .Enrich.With<UserNameEnricher>();
            }

            // Seq sink
            var seqServerUrl = WebConfigHelper.SeqServerUrl;
            var seqApiKey = WebConfigHelper.SeqApiKey;

            loggerConfiguration
                .WriteTo.Seq(seqServerUrl, apiKey: seqApiKey, compact: true, controlLevelSwitch: loggingLevelSwitch);

            // Assign globally shared logger
            Log.Logger = loggerConfiguration.CreateLogger();

            Log.Information("Logger configured");
        }

        public static void Close()
        {
            Log.Information("Logger closed");
            Log.CloseAndFlush();
        }
    }

    public class CallerEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var skip = 3;
            while (true)
            {
                var stack = new StackFrame(skip);

                var method = stack.GetMethod();

                if (method == null || method.DeclaringType == null)
                {
                    logEvent.AddPropertyIfAbsent(new LogEventProperty("Caller", new ScalarValue("<unknown method>")));
                    return;
                }
                    
                if (method.DeclaringType.Assembly != typeof(Log).Assembly)
                {
                    var caller = $"{method.DeclaringType.FullName}.{method.Name}({string.Join(", ", method.GetParameters().Select(pi => pi.ParameterType.FullName))})";
                    logEvent.AddPropertyIfAbsent(new LogEventProperty("Caller", new ScalarValue(caller)));
                }

                skip++;
            }
        }
    }
}
