﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('calendarAddEditStep2Point5Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation) {

        $scope.baseUrl = window.location.origin + "/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.MC.stateParams = $stateParams;

        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;

        $scope.calendarSchedules = [];

        $http.get('Dashboard/Program/CalendarCreateEditStep2Point5', { params: { programId: $stateParams.programId } })
             .success(function (data, status, headers, config) {

                 $scope.Model = data;

                 $scope.calendarSchedules = [];

                 for (var i = 0; i < $scope.Model.Schedules.length; i++) {

                     $scope.calendarSchedules.push({ text: $scope.Model.Schedules[i].Text, value: $scope.Model.Schedules[i].Value, color: $scope.Model.Schedules[i].Color });
                 }

                 $scope.calendarDataSource = null;

                 $scope.calendarDataSource = new kendo.data.SchedulerDataSource({
                     batch: true,
                     transport: {
                         read: {
                             url: window.location.origin + '/Dashboard/Program/GetCalendar?programId=' + $stateParams.programId,
                             dataType: "json"
                         },
                         parameterMap: function (options, operation) {
                             if (operation !== "read" && options.models) {
                                 return { models: kendo.stringify(options.models) };
                             }
                         }
                     },

                     schema: {
                         model: {
                             id: "taskId",
                             fields: {
                                 taskId: { from: "TaskId", type: "number" },
                                 title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                                 start: { type: "date", from: "Start" },
                                 end: { type: "date", from: "End" },
                                 startTimezone: { from: "StartTimezone" },
                                 endTimezone: { from: "EndTimezone" },
                                 description: { from: "Description" },
                                 recurrenceId: { from: "RecurrenceId" },
                                 recurrenceRule: { from: "RecurrenceRule" },
                                 recurrenceException: { from: "RecurrenceException" },
                                 ownerId: { type: "number", from: "OwnerId" },
                                 isAllDay: { type: "boolean", from: "IsAllDay" }
                             }
                         }
                     },
                 });

                 $("#programScheduler").html('');

                 $("#programScheduler").kendoScheduler({
                     date: new Date($scope.Model.Date),
                     startTime: new Date($scope.Model.StartDate),
                     height: 800,
                     editable: false,
                     views: [
                       "month",
                       {
                           type: "month", selected: true
                       },
                     ],
                     timezone: "Etc/UTC",
                     dataSource: $scope.calendarDataSource,

                     resources: [
                             {
                                 field: "ownerId",
                                 title: "Owner",
                                 dataSource: $scope.calendarSchedules,
                             }
                     ]
                 });

             });

        $scope.back = function () {
            $state.go('ProgramAddEditStep2', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
        }

        if ($state.returnByBack()) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();

