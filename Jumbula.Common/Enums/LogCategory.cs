﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum LogCategory
    {
        [Display(Name = "General")]
        [Description("General")]
        General = 0,

        [Display(Name = "PerformanceTest")]
        [Description("Performance Test")]
        PerformanceTest = 1,
    }
}
