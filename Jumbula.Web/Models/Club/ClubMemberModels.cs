﻿using System.Collections.Generic;
using System.Linq;

namespace SportsClub.Models
{
    public class ClubMemberItemViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string TotalAmount { get; set; }

        public string RegisterDate { get; set; }

        public int UserId { get; set; }

        public string Email { get; set; }

        public string ClubPlayers { get; set; }
    }

    public class ClubPlayerItemViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Relationship { get; set; }

        public int PlayerId { get; set; }

        public int UserId { get; set; }
    }

    public class AllClubMembersViewModel
    {
        public List<ClubMemberItemViewModel> ClubMemberItems { get; set; }

        private string ClubDomain { get; set; }

        public int Total { get; set; }

        private int CurrentPage { get; set; }

        private int PageSize { get; set; }

        public AllClubMembersViewModel()
        {
            ClubMemberItems = new List<ClubMemberItemViewModel>();

        }

        public AllClubMembersViewModel(string domain, int pageSize, int currentPage)
            : this()
        {
            ClubDomain = domain;
            PageSize = pageSize;
            CurrentPage = currentPage;

        }

       
    }

    public class ClubMemberOrderItemsModel
    {
        public string OrderDate { get; set; }

        public string EventName { get; set; }

        public string Gross { get; set; }

        public string EventType { get; set; }

        public string ConfirmationId { get; set; }

        public string PlayerFullName { get; set; }
    }

    public class ClubMemberOrdersModel
    {
        public List<ClubMemberOrderItemsModel> Orders { get; set; }

        private int? UserId { get; set; }

        private int? PlayerId { get; set; }


        public ClubMemberOrdersModel()
        {
            Orders = new List<ClubMemberOrderItemsModel>();
        }

        public ClubMemberOrdersModel(int? userId, int? playerId)
            : this()
        {
            UserId = userId;
            PlayerId = playerId;
        }

      
    }
}