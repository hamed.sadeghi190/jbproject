﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('seasonSchoolSettingsRegistrationController', ['$scope', '$http', function ($scope, $http) {

        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSchoolSettingsRegistration", order: 3, menu: "SEASONS" });
    }]);
})();