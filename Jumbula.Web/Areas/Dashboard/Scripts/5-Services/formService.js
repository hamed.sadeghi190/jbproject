﻿(function () {
    'use strict';

    angular.module('jbTools').service("formService", ['$http', function ($http) {

        var _model = {};

        function setModel(data) {
            _model = data
        }

        function getModel() {
            return _model
        }

        return {
            setModel: setModel,
            getModel: getModel,
        }
    }]);
})();