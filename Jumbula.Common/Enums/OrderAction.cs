﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum OrderAction : byte
    {
        [Description("Swap")]
        Swap = 0,
        [Description("Edit order")]
        EditChargeDiscount = 1,
        [Description("Edit information")]
        EditInformation = 2,
        [Description("Edit class name")]
        EditClassName = 3,
        [Description("Enrollment")]
        Initiated = 4,
        [Description("Paid")]
        Paid = 5,
        [Description("Cancel order")]
        Canceled = 6,
        [Description("Refund")]
        PartiallyRefunded = 7,
        [Description("Action was failed")]
        Failed = 9,
        [Description("Order status changed")]
        ChangeOrderStatus = 10,
        [Description("Delete")]
        Deleted = 11,
        [Description("Take a payment")]
        MadePayment = 12,
        [Description("Transfer order")]
        Transfer = 13,
        [Description("Edit a payment")]
        EditPayment = 14,
        [Description("Order is canceled and refunded to credit")]
        RefundToCredit = 15,
        [Description("Invoice")]
        Invoice = 16,
        [Description("Change day")]
        ChangeDay = 17,
        [Description("Change desired start date")]
        ChangeDesiredStartDate = 18,
        [Description("Edit registration form")]
        EditRegistrationForm = 19,
        [Description("Punchcard assigned")]
        PunchcardAssigned = 20,
        [Description("Punchcard reassigned")]
        PunchcardReassigned = 21,
        [Description("Punchcard unassigned")]
        PunchcardUnassigned = 22,
        [Description("Registration form reassignment")]
        ReassignRegistrationForm = 23,
        [Description("Undo cancel")]
        UndoCancel = 24,
        [Description("Undo transfer to family credit")]
        UndoFamilyCreditTransfer = 25,

    }
}
