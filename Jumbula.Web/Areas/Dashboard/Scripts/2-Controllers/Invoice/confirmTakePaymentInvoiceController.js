﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('confirmTakePaymentInvoiceController', ['$scope', '$http', '$stateParams', '$state', 'TakePaymentInvoiceService', '$filter', 'jbToast', function ($scope, $http, $stateParams, $state, TakePaymentInvoiceService, $filter,jbToast) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/Invoice";


        $scope.Model = TakePaymentInvoiceService.get();

        $scope.cancelAction = function () {
            TakePaymentInvoiceService.set($scope.Model);
            $state.back();
        }
        $scope.formatDate = function () {
            return new Date($scope.Model.PaymentDate).format('mmm d, yyyy');

        }
        $scope.getPaymentMethodText = function () {
            var paymentMethodValue = $scope.Model.SelectedPaymentMethod;
            var paymentMethod = $filter('getById')($scope.Model.PaymentMethods, paymentMethodValue);
            return paymentMethod.Text;
        }
     

        $scope.TakePayment = function () {
            $('#submitTakePayment').prop("disabled", true);
            $http.post($scope.baseUrl + '/SubmitInvoiceTakePayment', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    
                    if (data.Status == true) {
                        $scope.MC.backQueueItems.pop();
                        if (data.result == "Refresh") {
                            $state.go("InvoiceDetail", {invoiceId:$stateParams.invoiceId });
                        }
                        if (data.result == "Redirect") {
                            window.onbeforeunload = null;
                            window.location.replace(data.url);
                        }
                    }
                    else if (data.Status == false) {
                       
                        if (data.result == "Refresh") {
                            jbToast.error("You have already canceled or paid this invoice.");
                        }
                    }
                });

        };
        
    }])

})();