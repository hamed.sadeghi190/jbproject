﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using SportsClub.Domain;
using SportsClub.Infrastructure;
using System.IO;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SportsClub.Infrastructure.UIHelper;
using SportsClub.Db;
using SportsClub.Infrastructure.Lucene;
using SportsClub.Infrastructure.Helper;

namespace SportsClub.Models
{
    public enum TimeLineFilter
    {
        All,
        Camp,
        Tourney,
        Course
    }

    public enum EventStatusSection
    {
        All,
        Camp,
        Tourney,
        Course
    }

    public class ClubPageViewModel
    {

        public string Domain { get; set; }
        public string MenubarClubLogoUri { get; set; }
        public int UserID { get; set; }

        public bool IsAffiliator { get; set; }

        //public SportCategories Sport { get; set; }
        public int CategoryId { get; set; }
        public bool EditMode { get; set; }

        public List<ClubAffiliationBasicInfo> AffiliationsClub { get; set; }
        public ClubPageMenuViewModel Menu { get; set; }
        public ClubPageInfoViewModel Info { get; set; }
        public List<ClubPageLatestNewsViewModel> LatestNews { get; set; }
        public ImageGalleryViewModel Gallery { get; set; }
        public List<ClubPageCoachViewModel> Coaches { get; set; }
        public ClubPageTimelineViewModel Timeline { get; set; }
        public GoogleMapsViewModel Map { get; set; }
        public PageMetaDataViewModel MetaData { get; set; }
        //public MeetUpTimes MeetUpTimes { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                // Get cart
                var cartDb = Ioc.ICartDbService;

                // Get domain of orders in the cart(if there is no order in cart return empty)
                string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                if (!string.IsNullOrEmpty(currentDomain))
                {
                    return !(currentDomain == this.Domain);
                }

                return false;
            }
        }

        //config view base on container category
        public CategoryConfigurationModels Configuration { get; set; }

        public ClubPageViewModel()
        {
            Menu = new ClubPageMenuViewModel();
            Info = new ClubPageInfoViewModel();
            LatestNews = new List<ClubPageLatestNewsViewModel>();
            Gallery = new ImageGalleryViewModel();
            Coaches = new List<ClubPageCoachViewModel>();
            Timeline = new ClubPageTimelineViewModel();
            Map = new GoogleMapsViewModel();
            MetaData = new PageMetaDataViewModel();
            AffiliationsClub = new List<ClubAffiliationBasicInfo>();
            //MeetUpTimes = new MeetUpTimes();
            Configuration = new CategoryConfigurationModels();
        }

        public ClubPageViewModel(Club club)
            : this()
        {
            Domain = club.Domain;
            //Sport = club.SportCategory;
            CategoryId = club.CategoryId;
            MenubarClubLogoUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, Domain, Constants.Club_Logo_Small_Name + Path.GetExtension(club.Logo)).AbsoluteUri;

            //MeetUpTimes = GetMeetUpTimes(club.MeetUpTimes);

            IsAffiliator = club.IsAffiliator;

            if (IsAffiliator)
            {
                IClubDb clubDb = Infrastructure.Helper.Ioc.IClubDbService;
                AffiliationsClub = clubDb.GetBaseInfoOfAffilicationsClubs(club.Id).ToList();
            }

            UserID = club.UserId;

            //set configuration from club category
            Configuration = CategoryModel.Categories.Single(x => x.Id == club.CategoryId).Configuration;


            Timeline = new ClubPageTimelineViewModel
            {
                ClubDomain = Domain,
                ClubCategoryId = CategoryId,
                CurrentPage = 1
            };

            Menu = new ClubPageMenuViewModel()
            {
                ClubDomain = Domain,
                ClubCategoryId = CategoryId
            };

            string siteUrl = string.Empty;

            if (!string.IsNullOrEmpty(club.Site))
            {
                siteUrl = club.Site.ToLower().StartsWith("http") ? club.Site : "http://" + club.Site;

                // remove http / https from site url
                club.Site = club.Site.Replace("http://", "").Replace("https://", "");

                // remove last backslash from url
                if (club.Site.EndsWith("/"))
                {
                    club.Site = club.Site.Remove(club.Site.Length - 1);
                }

            }

            string logoUri = Utilities.GetClubLogoURL(Domain, club.Logo, CategoryId);



            // check if club has affiliator
            bool hasAffilator = false;
            string affiliatorLogoUri = string.Empty;
            string affiliatorDomain = string.Empty;
            string affiliatorName = string.Empty;

            if (club.AffiliatorID.HasValue)
            {
                hasAffilator = true;
                IClubDb clubDb = Infrastructure.Helper.Ioc.IClubDbService;
                var affiliatorClub = clubDb.GetBasicInfoOfAffiliatorClubByAffiliatorID(club.AffiliatorID.Value);

                if (affiliatorClub != null)
                {
                    affiliatorLogoUri = Utilities.GetClubLogoURL(affiliatorClub.Domain, affiliatorClub.Logo, affiliatorClub.CategoryId);
                    affiliatorDomain = affiliatorClub.Domain;
                    affiliatorName = affiliatorClub.Name;
                }
            }

            Info = new ClubPageInfoViewModel()
            {
                // alphabetic sort
                Address = club.ClubLocations.First().PostalAddress,
                Announce = club.Announce,
                ClubDomain = Domain,
                ClubCategoryId = CategoryId,
                Name = club.Name,
                MeetupTimes = FormatMeetupTimes(club.MeetUpTimes),
                Owner = club.ContactPersons.FirstOrDefault(),
                Site = !string.IsNullOrEmpty(club.Site) ? (club.Site.StartsWith("http") ? club.Site : "http://" + club.Site) : string.Empty,
                SiteLink = siteUrl,
                LogoUri = logoUri,
                Description = club.Description,
                HasAffiliator = hasAffilator,
                AffiliatorLogoUri = affiliatorLogoUri,
                AffiliatorDomain = affiliatorDomain,
                AffiliatorName = affiliatorName,
                ClubCoverPath = GetClubCoverPath(club.Domain, club.CoverImage, club.CategoryId),
            };


            MetaData = new PageMetaDataViewModel()
            {
                PageTitle = club.Name + Constants.S_SpaceDashSpace +
                            CategoryModel.GetCategoryName(club.CategoryId) + Constants.C_Space +
                            "club" + Constants.S_SpaceDashSpace
                            + Utilities.StringNullHandle(club.ClubLocations.First().PostalAddress.Street, Constants.S_SpaceDashSpace) +
                            Utilities.StringNullHandle(club.ClubLocations.First().PostalAddress.City, Constants.S_CommaSpace) +
                            Utilities.GetAbbreviationState(club.ClubLocations.First().PostalAddress.State)
                            ,

                MetaKeywords = string.Join(Constants.S_CommaSpace,
                          new string[] 
                            { 
                            club.Name,
                            CategoryModel.GetCategoryName(club.CategoryId) + Constants.C_Space + "club", 
                            Utilities.StringNullHandle(club.ClubLocations.First().PostalAddress.City,string.Empty), 
                            Utilities.GetAbbreviationState(club.ClubLocations.First().PostalAddress.State) 
                            }),

                MetaDesc = club.Description,
            };

            if (Info.Owner != null)
            {
                if (Info.Owner.Phone != null)
                {
                    Info.Tel = Info.Owner.Phone;
                }

                if (Info.Owner.Email != null)
                {
                    Info.Email = Info.Owner.Email;
                }
            }

            // load gallery images from azure storage

            Gallery = new ImageGalleryViewModel()
            {
                Images = new List<ImageGalleryItemViewModel>(),
                Thumbnails = new List<ImageGalleryItemViewModel>()
            };
            List<Domain.ImageGalleryItem> images = Ioc.IImageGalleryDbService.GetImageGalleryClub(Domain);

            // if we dont have any image show default image to gallery
            if (!images.Any())
            {
                var imageGalleryDirectory = HttpContext.Current.Server.MapPath(Constants.ImageGallery_Local_Directory);

                var imageAddress = Directory.GetFiles(imageGalleryDirectory);


                //for (int i = 0; i < imageAddress.Where(p => p.Contains("_Thumbnail")).Count(); i++)
                //{
                int i = 0;
                foreach (var s in imageAddress.Where(p => p.Contains("_Thumbnail")).ToList())
                {
                    var url = Constants.ImageGallery_Local_Directory + Path.GetFileName(s);

                    Gallery.Thumbnails.Add(new ImageGalleryItemViewModel()
                        {
                            Url = url,
                            Alt = "Jumbula Image" + i.ToString(),
                            Id = i.ToString(),
                            Title = "Jumbula Image" + i.ToString(),
                            Description = "Image Description" + i.ToString()
                        });
                    i++;
                }

                i = 0;
                foreach (var s in imageAddress.Where(p => p.Contains("_Thumbnail") == false).ToList())
                {
                    var url = Constants.ImageGallery_Local_Directory + Path.GetFileName(s);

                    Gallery.Images.Add(new ImageGalleryItemViewModel()
                    {
                        Url = url,
                        Alt = "Jumbula Image" + i.ToString(),
                        Id = i.ToString(),
                        Title = "Jumbula Image" + i.ToString(),
                        Description = "Image Description" + i.ToString()
                    });

                    i++;
                }

            }
            foreach (var item in images)
            {
                Gallery.Images.Add(new ImageGalleryItemViewModel()
                {
                    Url = StorageManager.GetUri(Constants.Path_ClubFilesContainer, Domain, item.Path).AbsoluteUri,
                    Alt = item.Title,
                    Id = item.Id.ToString(),
                    Title = item.Title,
                    Description = item.Description
                });
                Gallery.Thumbnails.Add(new ImageGalleryItemViewModel()
                {
                    Url = StorageManager.GetUri(Constants.Path_ClubFilesContainer, Domain, "ImageGallery/" + Path.GetFileNameWithoutExtension(item.Path) + "_Thumbnail" + Path.GetExtension(item.Path)).AbsoluteUri,
                    Alt = item.Title,
                    Id = item.Id.ToString(),
                    Title = item.Title,
                    Description = item.Description
                });
            }

            Coaches = new List<ClubPageCoachViewModel>();

            // StorageManager smCoach = new SportsClub.Infrastructure.StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_CoachAvatarChildDir);

            var activeCoachs = club.Coaches.Where(p => p.EventStatus == EventStatusCategories.open).ToList();
            foreach (Coach coach in activeCoachs)
            {
                Coaches.Add(new ClubPageCoachViewModel()
                {
                    Id = coach.Id,
                    Domain = coach.Domain,
                    CategoryId = coach.CategoryId,
                    Biography = coach.Biography,
                    ClubDomain = Domain,
                    Contact = coach.Contact,
                    Default = false,
                    ImageUrl = coach.ImageUrl
                });
            }

            bool hasDefault = false;

            foreach (ClubPageCoachViewModel coach in Coaches)
            {
                if (coach.Default)
                {
                    hasDefault = true;
                    break;
                }
            }

            if (!hasDefault && Coaches.Count > 0)
            {
                Coaches[0].Default = true;
            }

            // event such as camps, classes & tournaments should convert to timeline event ( not too hard  ;) )
            Timeline = new ClubPageTimelineViewModel();

            Map = new GoogleMapsViewModel()
            {
                Name = club.Name,
                PostalAddress = club.ClubLocations.First().PostalAddress
            };
        }

        public ClubPageViewModel(Club club, ICollection<News> news)
            : this(club)
        {
            LatestNews = new List<ClubPageLatestNewsViewModel>();
            var newsCount = Ioc.INewsDbService.CountOfClubNews(Domain);
            foreach (var item in news)
            {
                LatestNews.Add(new ClubPageLatestNewsViewModel()
                {
                    NewsId = item.Id,
                    Title = item.Title,
                    Summary = item.Summary,
                    Description = item.Description,
                    NewsCount = newsCount,
                    ClubDomain = Domain,
                    ClubCategoryId = CategoryId,
                    DateTime = item.MetaData.DateUpdated
                });
            }

            // to define the latest item,
            // in view we use it to understand we need a ajax call and reload
            if (LatestNews.Any())
            {
                LatestNews.OrderByDescending(x => x.NewsId).Last().EndPageElement = true;
            }
        }

        public ClubPageViewModel(string domain)
            : this()
        {
            Domain = domain;
            // get club
            int categoryId = Ioc.IClubDbService.GetBasicInfo(domain).CategoryId;

            // load configuration 
            Configuration = CategoryModel.Categories.Single(x => x.Id == categoryId).Configuration;
        }

        //private MeetUpTimes GetMeetUpTimes(string xmlMeetup)
        //{
        //    var meetUpTimes = new MeetUpTimes();

        //    if (!string.IsNullOrEmpty(xmlMeetup))
        //    {
        //        meetUpTimes = SerializeHelper.Deserialize<MeetUpTimes>(XDocument.Parse(xmlMeetup));
        //    }

        //    return meetUpTimes;
        //}

        private string GetClubCoverPath(string clubDomain, string coverImageName, int categoryId)
        {
            // Path of club cover image
            string path = string.Empty;

            try
            {
                // Check if there isn't cover image for current club
                if (string.IsNullOrEmpty(coverImageName))
                {
                    // Get category name 
                    string categoryName = CategoryModel.GetCategoryName(categoryId);

                    // Build cover image path based on category name
                    string categoryRelativeCoverPath = string.Format("/Images/Covers/{0}-cover.jpg", categoryName.Replace(" ", string.Empty).ToLower());

                    // Absolute path for check file existence
                    string categoryAbsoluteCoverPath = HttpContext.Current.Server.MapPath(categoryRelativeCoverPath);

                    // Set an appropriate image for club based on club category if file exist
                    if (File.Exists(categoryAbsoluteCoverPath))
                    {
                        path = categoryRelativeCoverPath;
                    }
                    // if isn't exist appropriate image for this category
                    else
                    {
                        path = System.Web.VirtualPathUtility.ToAbsolute(Constants.Cover_Default_Path);
                    }
                }
                // If cover image exist for this club
                else
                {
                    path = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, coverImageName).AbsoluteUri;
                }
            }
            catch
            {
                // Set default cover image if any error occured
                path = System.Web.VirtualPathUtility.ToAbsolute(Constants.Cover_Default_Path);
            }

            return path;
        }

        private string FormatMeetupTimes(string xmlMeetup)
        {
            string model = "";

            //if (!string.IsNullOrEmpty(xmlMeetup))
            //{
            //    var meetupTimes = SerializeHelper.Deserialize<MeetUpTimes>(XDocument.Parse(xmlMeetup));
            //    foreach (var item in meetupTimes.RecurrenceTimes.Where(x => x.StartTime != null && x.FinishTime != null))
            //    {
            //        model += item.Day + "  " + item.StartTime + " To " + item.FinishTime + "</br>";
            //    }
            //}

            return model;
        }
    }

    public class ClubPageMenuViewModel
    {
        public string ClubDomain { get; set; }
        public int ClubCategoryId { get; set; }
        //public SportCategories ClubSport { get; set; }
    }

    public class ClubPageInfoViewModel
    {
        // for links
        public string ClubDomain { get; set; }
        public int ClubCategoryId { get; set; }
        //public SportCategories ClubSport { get; set; }

        // left side
        public string Name { get; set; }
        public PostalAddress Address { get; set; }
        public ContactPerson Owner { get; set; }
        public string Tel { get; set; }
        public string Site { get; set; }
        public string SiteLink { get; set; }
        public string Email { get; set; }
        public string Description { get; set; } // used in share on social networks ( not for display in club page )
        public string LogoUri { get; set; }
        public string MeetupTimes { get; set; }

        // affiliator 
        public bool HasAffiliator { get; set; }

        public string AffiliatorLogoUri { get; set; }

        public string AffiliatorDomain { get; set; }

        public string AffiliatorName { get; set; }

        public string ClubCoverPath { get; set; }

        // right side
        public string Announce { get; set; }

        public bool IsEmailType { get; set; }
    }

    public class ClubPageLatestNewsViewModel
    {
        public ClubPageLatestNewsViewModel()
        {
            EndPageElement = false;
        }

        public string ClubDomain { get; set; }
        //public SportCategories ClubSport { get; set; }
        public int ClubCategoryId { get; set; }

        public int NewsId { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public DateTime DateTime { get; set; }

        //show sum of News
        public int NewsCount { get; set; }

        //show page number of this news element
        public int PageNo { get; set; }

        // if true this show this news is latest news in this page 
        public bool EndPageElement { get; set; }
    }

    public class ClubPageCoachViewModel
    {
        public string Domain { get; set; }
        public string ClubDomain { get; set; }
        //public SportCategories SportCategory { get; set; }
        public int CategoryId { get; set; }

        public int Id { get; set; }

        public virtual ContactPerson Contact { get; set; }

        public string Biography { get; set; }

        public string ImageUrl { get; set; }

        // is default coach ( active ) in crousel ?
        public bool Default { get; set; }
    }

    public enum ClubPageTimelineEventType
    {
        Camp,
        Tournament,
        Class
    }

    public enum ClubPageTimelineEventStatus
    {
        Open,
        InProgress,
        Expired
    }

    public class ClubPageTimelineViewModel
    {
        public string ClubDomain { get; set; }

        //public SportCategories ClubSport { get; set; }
        public int ClubCategoryId { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }

        public List<BaseEventSearchResult> Events { get; set; }

        public int CurrentPage { get; set; }

        public bool HasTourney
        {
            get;
            set;
        }
        public bool HasCamp
        {
            get;
            set;
        }
        public bool HasCourse
        {
            get;
            set;
        }

        public bool ShowCamp { get; set; }

        public bool ShowCourse { get; set; }

        public bool ShowTourney { get; set; }

        public bool CompactMode { get; set; }

        public int StartIndex
        {
            get
            {
                return 0;
            }
        }

        public int LastIndex
        {
            get
            {
                return int.MaxValue;
                //int last = (CurrentPage + 1) * Constants.Club_Timeline_Event_Page_Size;
                // index is zero based 
                //return last - 1;
            }
        }

        public bool NoUpdate { get; set; }

        public TimeLineFilter Filter { get; set; }

        public EventStatusSection EventSection { get; set; }

        public bool IsOpen { get; set; }

        public bool IsClose { get; set; }

        public bool IsInProgress { get; set; }

        public bool ModelChanged { get; set; }

        public ClubPageTimelineViewModel()
        {
           
        }
    }

    public class ExistCoverViewModel
    {
        public ExistCoverViewModel()
        {
            ImagesUrl = new List<string>();
        }

        public CoverImageMode CoverImageMode { get; set; }
        public string Domain { get; set; }

        public IList<string> ImagesUrl { get; set; }
    }

    public enum CoverImageMode
    {
        None,
        FromClub,
        FromJumbula
    }

    public class ClubPageUploadLogoViewModel
    {
        public string Domain { get; set; }

        public string ImageUrl { get; set; }
        public string UploaderString { get; set; } // store cute uploader generated string
        public bool HasImage { get; set; }
    }

    public class ClubPageEditContactInfoViewModel
    {
        public string ClubDomain { get; set; }

        [Required]
        [Display(Name = "First name")]
        [StringLength(32, ErrorMessage = "{0} is too long")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        [StringLength(32, ErrorMessage = "{0} is too long")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}")]
        public string LastName { get; set; }

        [Phone]
        [Display(Name = "Tel number")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "10 digits with no dash or parentheses.")]
        public string TelNumber { get; set; }


        [DisplayName("Email")]
        [StringLength(128)]
        [EmailAddress(ErrorMessage = "Incorrect email address")]
        public string Email { get; set; }

        public string EmailLink { get; set; }

        public string Site { get; set; }

        public string SiteUrl { get; set; }
    }

    public class ClubPagedEditHourInfoViewModel : IValidatableObject
    {
        public string ClubDomain { get; set; }

        //public MeetUpTimes MeetUpTimes { get; set; }
        //public List<CourseRecurrenceTimeViewModel> MeetUpTimes { get; set; }

        //public ClubPagedEditHourInfoViewModel(MeetUpTimes meetupTimes, string clubDomain)
        //{
        //    SeedMeetUpTimes(meetupTimes);
        //    ClubDomain = clubDomain;
        //}

        public ClubPagedEditHourInfoViewModel()
        {
            //MeetUpTimes = new MeetUpTimes();
        }

        //private void SeedMeetUpTimes(MeetUpTimes meetupTimes)
        //{

        //    MeetUpTimes = new MeetUpTimes();
        //    if (meetupTimes.RecurrenceTimes == null || meetupTimes.RecurrenceTimes.Count <= 0)
        //    {

        //        MeetUpTimes.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Sunday });
        //        MeetUpTimes.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Monday });
        //        MeetUpTimes.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Tuesday });
        //        MeetUpTimes.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Wednesday });
        //        MeetUpTimes.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Thursday });
        //        MeetUpTimes.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Friday });
        //        MeetUpTimes.RecurrenceTimes.Add(new CourseRecurrenceTimeViewModel { Day = ScheduleDayOfWeek.Saturday });



        //    }
        //    else
        //    {
        //        MeetUpTimes.RecurrenceTimes = meetupTimes.RecurrenceTimes;
        //        MeetUpTimes.Description = meetupTimes.Description;
        //    }
        //}

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //if (
            //    MeetUpTimes.RecurrenceTimes.Where(item => item.IsChecked)
            //               .Any(item => !item.FinishTime.HasValue || !item.StartTime.HasValue))
            //{
            //    yield return new ValidationResult("hour is required");
            //}


            yield return null;
        }
    }

    public class ClaimClub
    {
        //string clubName, string fullName, string email, string phoneNumber

        [Required(ErrorMessage = "Club name number is required")]
        [Display(Name = "Club name")]
        public string ClubName { get; set; }


        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}")]
        [Required(ErrorMessage = "Full name number is required")]
        public string FullName { get; set; }

        [DisplayName("Email")]
        [StringLength(128)]
        [EmailAddress(ErrorMessage = "Incorrect email address")]
        [Required(ErrorMessage = "Email number is required")]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Tel number")]
        [StringLength(64, ErrorMessage = "{0} is too long")]
        [RegularExpression("[0-9]{10}", ErrorMessage = "10 digits with no dash or parentheses.")]
        [Required(ErrorMessage = "Phone number is required")]
        public string PhoneNumber { get; set; }
    }

}