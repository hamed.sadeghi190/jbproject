﻿using System.Collections.Generic;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models.Email
{
    public class InstallmentConfirmationEmail
    {
        public InstallmentConfirmationEmail()
        {
            CardNumber = string.Empty;
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
        }


        public List<OrderInstallment> OrderInstallments { get; set; }

        public string CardNumber { get; set; }

        public string CardType { get; set; }

        public string ClubName { get; set; }

        public string ClubImageUrl { get; set; }

        public string ClubUrl { get; set; }
        public string EventName { get; set; }

        public string EventAddress { get; set; }
        public string RootSiteUrl { get; set; }
        public string BodyMailTitle { get; set; }

        public string PlayerFullName { get; set; }
        public string PlayerImageUrl { get; set; }
        public string ConfirmationId { get; set; }

        public string OrderPaidDate { get; set; }

        public string ClubEmail { get; set; }

        public string ClubPhone { get; set; }

        public string AppliedText { get; set; }

        public string PayPalInfo { get; set; }
        public string PaidAmount { get; set; }
    }
}