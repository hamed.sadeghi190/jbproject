﻿using System.Data.Entity.ModelConfiguration;
using Jumbula.Core.Domain;

namespace Jumbula.Data.DbConfigs
{
    public class OrderConfig : EntityTypeConfiguration<Order>
    {
        public OrderConfig()
        {
            HasMany(r => r.OrderItems).WithRequired(orderItem => orderItem.Order).HasForeignKey(orderItem => orderItem.Order_Id);
            HasMany(p => p.TransactionActivities).WithOptional(c => c.Order).HasForeignKey(c => c.OrderId);
            HasMany(p => p.OrderPreapprovals).WithRequired(c => c.Order).HasForeignKey(c => c.OrderId);
            HasMany(p => p.PendingPreapprovalTransactions).WithRequired(c => c.Order).HasForeignKey(c => c.OrderId);
           HasRequired(r => r.Club).WithMany().HasForeignKey(order => order.ClubId);
        }
    }
}
