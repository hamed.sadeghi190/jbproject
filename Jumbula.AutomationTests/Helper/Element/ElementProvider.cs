﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    public class ElementProvider<T> where T : BaseElements
    {
        public T Get(IWebDriver driver)
        {
           var param = new List<IWebDriver>();

            param.Add(driver);

            T resource = (T)Activator.CreateInstance(typeof(T), args: param.ToArray());
            
            return resource;

        }
    }
}
