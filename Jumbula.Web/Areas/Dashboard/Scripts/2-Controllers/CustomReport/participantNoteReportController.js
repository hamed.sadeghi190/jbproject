﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('participantNoteReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = {};
        var detailExportPromises = [];

        $http.get(baseUrl + '/GetParticipantNoteReportHeader', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.SeasonName = data.SeasonName;

        }).error(function (data) {
            jbToast.error("error " + data);
        });

        $http.get(baseUrl + '/GetAllProgramsSignOutSheetReport', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data) {
            $scope.AllPrograms = data;
            $scope.AllPrograms.unshift({ Text: "All", Value: null });
            $scope.Model.ScheduleId = null;
            prepareGrid();

        }).error(function (data) {
            jbToast.error(data.Message);
        });

        
        var prepareGrid = function () {


            $scope.dataSourceFulldata = new kendo.data.DataSource({
                type: "json",
                transport: {
                    read: {
                        url: baseUrl + '/GetAllParticipantNoteReport',
                        type: "GET",
                        data: function () {
                            return {
                                seasonDomain: $scope.stateParams.seasonDomain,
                                scheduleId: $scope.Model.ScheduleId
                            };
                        },
                    },
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                pageSize: 50,
                serverSorting: false,
                serverPaging: true
            });


            $scope.dataSourceFulldata.read();



            $scope.ParticipantNoteReportDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                {
                    read: {
                        url: baseUrl + '/GetParticipantNoteReport',
                        type: "Get",
                        data: function () {
                            return {
                                seasonDomain: $scope.stateParams.seasonDomain,
                                scheduleId: $scope.Model.ScheduleId
                            };
                        }

                    }

                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                pageSize: 50,
                serverSorting: false,
                serverPaging: true
            });

            $scope.participantNoteGridOptions = {
                excel: {
                    allPages: true,
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {
                    e.preventDefault();
                    var workbook = e.workbook;
                    detailExportPromises = [];
                    // kendo new version
                    var masterData = e.data;
                    for (var rowIndex = 0; rowIndex < masterData.length; rowIndex++) {
                        exportChildData(masterData[rowIndex].Id, rowIndex);
                    }
                    
                    $.when.apply(null, detailExportPromises)
                        .then(function () {
                            // get the export results
                            var detailExports = $.makeArray(arguments);

                            // sort by masterRowIndex
                            detailExports.sort(function (a, b) {
                                return a.masterRowIndex - b.masterRowIndex;
                            });

                            // add an empty column
                            workbook.sheets[0].columns.unshift({
                                width: 30
                            });

                            // prepend an empty cell to each row
                            for (var i = 0; i < workbook.sheets[0].rows.length; i++) {
                                workbook.sheets[0].rows[i].cells.unshift({});
                            }

                            // merge the detail export sheet rows with the master sheet rows
                            // loop backwards so the masterRowIndex doesn't need to be updated
                            for (var i = detailExports.length - 1; i >= 0; i--) {
                                var masterRowIndex = detailExports[i].masterRowIndex + 1; // compensate for the header row

                                var sheet = detailExports[i].sheet;

                                // prepend an empty cell to each row
                                for (var ci = 0; ci < sheet.rows.length; ci++) {
                                    if (sheet.rows[ci].cells[0].value) {
                                        sheet.rows[ci].cells.unshift({});
                                    }
                                }

                                // insert the detail sheet rows after the master row
                                [].splice.apply(workbook.sheets[0].rows, [masterRowIndex + 1, 0].concat(sheet.rows));
                            }

                            // save the workbook
                            kendo.saveAs({
                                dataURI: new kendo.ooxml.Workbook(workbook).toDataURL(),
                                fileName: "ParticipantNote.xlsx",
                                proxyURL: "/Dashboard/CustomReport/Save"
                            });


                        });
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.ParticipantNoteReportDataSource,
                resizable: true,
                scrollable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100, 200
                        ],
                    buttonCount: 5
                },
                detailTemplate: kendo.template($("#template").html()),
                detailInit: detailInit,
                dataBound: function (e) {
                    //customReportService.initializeDoubleScroll();
                    this.expandRow(this.tbody.find("tr.k-master-row").first());

                },

                sortable: { mode: 'single' },
                columns: [
                    {
                        field: "FullName",
                        title: "Participant",
                        width: "auto"
                    },

                ],
            }

            function detailInit(e) {
                
                var detailRow = e.detailRow;
                detailRow.find(".orders").kendoGrid({
                    dataSource: {
                        type: "json",
                        transport: {
                            read: {
                                url: baseUrl + '/GetParticipantNoteReportDetail',
                                type: "GET",
                                data: function () {
                                    return {
                                        profileId: e.data.Id,
                                        scheduleId: $scope.Model.ScheduleId
                                    };
                                }

                            }
                        },
                        schema: {
                            total: "TotalCount",
                            data: "DataSource"
                        },
                        pageSize: 50,
                        serverSorting: false,
                        serverPaging: true
                    },

                    excelExport: function (e) {
                        e.preventDefault();
                    },
                    scrollable: true,
                    sortable: true,
                    resizable: true,
                    columns: [
                        {
                            field: "Note",
                            title: "Notes",
                            width: "auto",
                            template: '<div style="white-space: pre-line;"> <span style="font-weight: bold; font-size: 16px;"> #=Title# </span> <br/> <span> #=Note# </span> </div>'
                        },
                        {
                            field: "ProgramName",
                            title: "Program",
                            width: "150px"
                        },
                        {
                            field: "Owner",
                            title: "Owner",
                            width: "150px"
                        },
                        {
                            field: "Date",
                            title: "Date",
                            width: "130px"
                        }
                    ]
                });
            }





            function exportChildData(Id, rowIndex) {

                var deferred = $.Deferred();
                detailExportPromises.push(deferred);

                $scope.dataSourceFulldata.filter({ field: "Id", operator: "eq", value: Id });

                var exporter = new kendo.ExcelExporter({
                    columns: [
                        {
                            field: "ProgramName",
                            title: "Program",
                            width: "200px"
                        },
                        {
                            field: "Note",
                            title: "Note",
                            width: "200px",
                            template: '<div style="white-space: pre-line;"> <span style="font-weight: bold; font-size: 16px;"> #=Title# </span> <br/> <span> #=Note# </span> </div>'
                        }
                    ],

                    dataSource: $scope.dataSourceFulldata
                });

                exporter.workbook().then(function (book, data) {
                    deferred.resolve({
                        masterRowIndex: rowIndex,
                        sheet: book.sheets[0]
                    });
                });
            }




           
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#participantNoteGrid").getKendoGrid().saveAsExcel();
        }

        $scope.filterReport = function () {
            $scope.dataSourceFulldata.read();
            $("#participantNoteGrid").data("kendoGrid").dataSource.read();
        }
      
    }
    ]);

})();