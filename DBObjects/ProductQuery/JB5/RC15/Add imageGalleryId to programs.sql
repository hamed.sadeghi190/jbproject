

alter table Programs Add ImageGalleryId int null
Go

ALTER TABLE [dbo].[Programs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Programs_dbo.ImageGallerys_ImageGalleryId] FOREIGN KEY([ImageGalleryId])
REFERENCES [dbo].[ImageGallerys] ([Id])