﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Common.Enums
{
    public enum ActionReason : byte
    {
        Cancel = 0,
        Transfer = 1,
    }
}
