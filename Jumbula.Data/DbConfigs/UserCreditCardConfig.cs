﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class UserCreditCardConfig : EntityTypeConfiguration<UserCreditCard>
    {
        public UserCreditCardConfig()
        {
            HasKey(u => u.Id);
        }
    }
}
