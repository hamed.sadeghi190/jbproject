﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemCreditController', ['$scope', '$http', '$stateParams', '$state', 'makePaymentClubService', function ($scope, $http, $stateParams, $state, makePaymentClubService) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/System";
        
        $scope.Model = {};
        $scope.Model.clubId = $stateParams.clubId; 

        
      $http.get($scope.baseUrl + '/GetCreditInfo?ClubId=' + $stateParams.clubId).success(function (data, status, headers, config) {
            $scope.Model = data;
        });

        $scope.confirmCancellation = function () {

            $http.post($scope.baseUrl + '/ConfirmCreditInfo', { model: $scope.Model })
              
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        makePaymentClubService.set($scope.Model);

                        $state.go('SystemConfirmCredit', { clubId: $stateParams.clubId }, 'SystemPaymentWithCredit', { clubId: $stateParams.clubId });

                    }
                    else {


                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };
        $scope.back = function () {
            $state.go('SystemClubBillingHistory', { clubId: $stateParams.clubId });
        }

    }])

})();