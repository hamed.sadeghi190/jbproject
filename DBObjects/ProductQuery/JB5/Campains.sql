Alter Table [dbo].[CampaignRecipients] Alter Column [RecipientName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table [dbo].[CampaignRecipients] Alter Column [RecipientLastName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table [dbo].[CampaignRecipients] Alter Column [EmailAddress] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
-----

Alter Table [dbo].[Campaigns] Alter Column [CampaignName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table  [dbo].[Campaigns] Alter Column [Sender] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table  [dbo].[Campaigns] Alter Column [From] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table  [dbo].[Campaigns] Alter Column [Subject] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

-----

Alter Table [dbo].[MailListContacts] Alter Column [EmailAddress] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
Alter Table [dbo].[MailListContacts] Alter Column [RecipientName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table [dbo].[MailListContacts] Alter Column [RecipientLastName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
------
Alter Table [dbo].[MailLists] Alter Column [ListName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
--Alter Table [dbo].[MailLists] Alter Column [Club_Id] [int] NOT NULL CONSTRAINT [DF__MailLists__Club___0371755F] DEFAULT ((0))
-----

Alter Table [dbo].[ProgramSchedules] Alter Column [Attributes] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

-----