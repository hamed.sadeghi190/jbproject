using System;
using System.Collections.Generic;
using System.Linq;
using Jb.Framework.Common.Forms.JsonFormatter;

using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbCheckBox : JbBaseElement, IInputElement
    {
        private string _stringElements;

        public bool Value
        {
            get;
            set;
        }        

        public string GetValue()
        {
            return Value.ToString();
        }


        public bool IsRequired
        {
            get;
            set;
        }
        public string PlaceHolder { get; set; }

    }
}