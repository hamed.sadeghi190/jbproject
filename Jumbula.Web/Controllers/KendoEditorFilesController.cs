﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Jumbula.Common.Helper;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.UserService;
using Kendo.Mvc.UI;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{
    public class KendoEditorFilesController : EditorImageBrowserController
    {
        public KendoEditorFilesController()
            :base()
        {
            _clubSubDoamin = this.GetCurrentClubDomain();
        }

        private readonly string _clubSubDoamin;
        private const string contentFolderRoot = "~/Content/";
        private const string prettyName = "Images/";
        private static readonly string[] foldersToCopy = new[] { "~/Content/shared/" };

        /// <summary>
        /// Gets the base paths from which content will be served.
        /// </summary>
        public override string ContentPath
        {
            get
            {
                return CreateUserFolder();
            }
        }

        private string CreateUserFolder()
        {
            var virtualPath = Path.Combine(contentFolderRoot, "UserFiles", prettyName);

            var path = Server.MapPath(virtualPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                foreach (var sourceFolder in foldersToCopy)
                {
                    CopyFolder(Server.MapPath(sourceFolder), path);
                }
            }
            return virtualPath;
        }
       [HttpPost]
        public override  ActionResult Destroy(string path, ImageBrowserEntry entry)
        {

            var fileName = entry.Name;
            var clubDomain = _clubSubDoamin;
            var sourcePath = StorageManager.PathCombine(clubDomain + "/JbPageBuilder" , fileName);
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                Constants.Path_ClubFilesContainer);

           
            if (
                StorageManager.IsExist(
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain + "/JbPageBuilder" ,
                        fileName).AbsoluteUri))
            {
               
                storageManager.DeleteBlob(clubDomain + "/JbPageBuilder" , fileName);
            }

           
            return null;
            
        }
        private void CopyFolder(string source, string destination)
        {
            if (!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            foreach (var file in Directory.EnumerateFiles(source))
            {
                var dest = Path.Combine(destination, Path.GetFileName(file));
                System.IO.File.Copy(file, dest);
            }

            foreach (var folder in Directory.EnumerateDirectories(source))
            {
                var dest = Path.Combine(destination, Path.GetFileName(folder));
                CopyFolder(folder, dest);
            }
        }

        public virtual ActionResult UploadImageWithThumbnail(HttpPostedFileBase file, string path)
        {
            var clubDomain = _clubSubDoamin;
            this.EnsureUserIsAdminForClub(clubDomain);

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }
            var fileName = file.FileName;

            string[] legalExtention = { ".jpg", ".png", ".jpeg", ".gif" };

            if (!legalExtention.Contains(Path.GetExtension(fileName.ToLower())))
            {
                return Json(new { message = Constants.M_Ilegal_Uploaded_Image_Message });
            }
            var image = new WebImage(fileData);

            if (string.IsNullOrEmpty(path))
            {
                path = "JbPageBuilder";
            }

            var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
            string contentType = string.Format(Constants.F_Image_MimeType, Path.GetExtension(file.FileName).Substring(1));

            Stream ImageStream = new MemoryStream(image.GetBytes());

            sm.UploadBlob(clubDomain + "/" + path, fileName, ImageStream, contentType);
            var thumbnailImage = Image.FromStream(ImageStream).GetThumbnailImage(120, 120, () => false, IntPtr.Zero);
            ImageStream.Close();

            byte[] thumbnailArray;
            using (MemoryStream mStream = new MemoryStream())
            {
                thumbnailImage.Save(mStream, GetImageFormat(Path.GetExtension(fileName.ToLower())));
                thumbnailArray = mStream.ToArray();
            }
            if (thumbnailArray != null)
            {
                Stream thumbnailStream = new MemoryStream(thumbnailArray);

                sm.UploadBlob(clubDomain + "/" + path + "/Thumbnail", fileName, thumbnailStream, contentType);
                thumbnailStream.Close();
            }

            string imageUrl = StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + clubDomain + "/" + path + "/" + fileName).AbsoluteUri;
            return new JsonResult()
            {
                Data = Json(new { name = fileName, type = "f", size = fileData.Length }),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8
            };
        }
        private ImageFormat GetImageFormat(string extension)
        {
           
            switch (extension.ToLower())
            {
               
                case @".png":
                    return ImageFormat.Png;

             case @".jpeg":
             case @".jpg":
                    return ImageFormat.Jpeg;

                case @".gif":
                    return ImageFormat.Gif;

                default:
                    throw new NotImplementedException();
            }
        }
        public override ActionResult Upload(string path, HttpPostedFileBase file)
        {
            var clubDomain =_clubSubDoamin;
            this.EnsureUserIsAdminForClub(clubDomain);

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }
            var fileName = file.FileName;

            string[] legalExtention = { ".jpg", ".png", ".jpeg", ".gif", ".doc", ".pdf", ".docx", ".zip", ".rar" };

            if (!legalExtention.Contains(Path.GetExtension(fileName.ToLower())))
            {
                return Json(new { message = Constants.M_Ilegal_Uploaded_Image_Message });
            }
            var image = new WebImage(fileData);



            var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
            string contentType = string.Format(Constants.F_Image_MimeType, Path.GetExtension(file.FileName).Substring(1));

            Stream ImageStream = new MemoryStream(image.GetBytes());
            // upload file
            sm.UploadBlob(clubDomain + "/JbFormImages", fileName, ImageStream, contentType);
            ImageStream.Close();
            string imageUrl = StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + clubDomain + "/JbFormImages/" + fileName).AbsoluteUri;
            return Json(new { imageUrl, imageName = fileName });
        }

        public override ActionResult Create(string path, ImageBrowserEntry entry)
        {
            return base.Create(path, entry);
        }

        public virtual JsonResult ReadFiles(string path)
        {
            var clubDomain = _clubSubDoamin;
            this.EnsureUserIsAdminForClub(clubDomain);

            var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);


            var blobPath = clubDomain;
            if (path != "" && path != "/")
            {
                blobPath += "/" + path;
            }
            else
            {
                blobPath = "JbPageBuilder";
            }

            var filesList = sm.ListObjectsInDirectory(blobPath);

            return new JsonResult()
            {
                Data = filesList,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8
            };
        }

        public override JsonResult Read(string path)
        {
            var clubDomain = _clubSubDoamin;
            this.EnsureUserIsAdminForClub(clubDomain);

            var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

            var blobPath = clubDomain + "/JbFormImages";
            if (path != "" && path != "/")
            {
                blobPath += "/" + path;
            }

            var filesList = sm.ListObjectsInDirectory(blobPath);

            return new JsonResult()
            {
                Data = filesList,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                ContentType = "application/json",
                ContentEncoding = Encoding.UTF8
            };
        }



        public string FileUrl(string path)
        {
            if(string.IsNullOrEmpty(path))
            {
                path = "JbFormImages";
            }
            var clubDomain = _clubSubDoamin;
            this.EnsureUserIsAdminForClub(clubDomain);
            return StorageManager.GetUriByPath("clubs/" + clubDomain + "/"+path+"/").ToString();
        }

        public string FileUrltest(string filename)
        {
            var clubDomain = _clubSubDoamin;
            this.EnsureUserIsAdminForClub(clubDomain);
            return StorageManager.GetUriByPath("clubs/" + clubDomain + "/JbFormUploadedDocs/" + filename).ToString();
        }

        //Kendo UI Upload
        [HttpPost]
        public virtual ActionResult Submit(IEnumerable<HttpPostedFileBase> files, string formName)
        {
            var clubDomain = _clubSubDoamin;
            this.EnsureUserIsAdminForClub(clubDomain);
            var file = files.FirstOrDefault();

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(file.InputStream))
            {
                fileData = binaryReader.ReadBytes(file.ContentLength);
            }

            var fileName = file.FileName.Replace(" ", "_");

            string[] legalExtention = { ".jpg", ".png", ".jpeg", ".gif", ".doc", ".pdf", ".docx", ".zip", ".rar",".txt" };

            if (!legalExtention.Contains(Path.GetExtension(fileName.ToLower())))
            {
                return Json(new { message = Constants.M_Ilegal_Uploaded_Image_Message });
            }
            // var image = new WebImage(fileData);

            var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
            string contentType = string.Format(Constants.F_Image_MimeType, Path.GetExtension(file.FileName).Substring(1));

            Stream fileStream = new MemoryStream(fileData);
            // upload file
            sm.UploadBlob(clubDomain + "/JbFormUploadedDocs/" + formName, fileName, fileStream, contentType);
            fileStream.Close();

            string imageUrl = StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + clubDomain + "/JbFormUploadedDocs/" + formName + "/" + fileName).AbsoluteUri;

            return Json(new
            {
                URL = imageUrl,
                fileName = fileName
            });
        }

        [HttpPost]
        public virtual ActionResult Remove(string[] fileNames, string formName)
        {

            var fileName = fileNames[0];
            var clubDomain = _clubSubDoamin;
            var sourcePath = StorageManager.PathCombine(clubDomain + "/JbFormUploadedDocs/" + formName, fileName);
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                Constants.Path_ClubFilesContainer);

            var targetPath = StorageManager.PathCombine(clubDomain + "/JbFormUploadedDocs/" + formName, fileName);
            if (
                StorageManager.IsExist(
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain + "/JbFormUploadedDocs/" + formName,
                        fileName).AbsoluteUri))
            {
                storageManager.ReadFile(sourcePath, targetPath);
                storageManager.DeleteBlob(clubDomain + "/JbFormUploadedDocs/" + formName, fileName);
            }

            var file = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain + "/JbFormUploadedDocs/" + formName, fileName).AbsoluteUri;
            return null;
        }
        [HttpPost]
        public virtual ActionResult RemoveUplodedFile(string[] fileNames, string location)
        {
            try
            {
                var fileName = fileNames[0];
                var clubDomain = _clubSubDoamin;
                var sourcePath = StorageManager.PathCombine(clubDomain + location, fileName);
                var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                    Constants.Path_ClubFilesContainer);

                var targetPath = StorageManager.PathCombine(clubDomain + location, fileName);
                if (
                    StorageManager.IsExist(
                        StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain + location,
                            fileName).AbsoluteUri))
                {
                    storageManager.ReadFile(sourcePath, targetPath);
                    storageManager.DeleteBlob(clubDomain + location, fileName);
                }

                var file = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain + location, fileName).AbsoluteUri;
                return Json(new JResult { Status = true, Message = "", RecordsAffected = 0 });
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = Constants.M_E_CreateEdit, RecordsAffected = 0 });
            }
        }
        public virtual ActionResult UploadImageOrPdf(IEnumerable<HttpPostedFileBase> files, string location,int index=-1)
        {

            if (files != null)
            {
                var clubDomain = Request.Url.GetSubDomain();
                var file = files.FirstOrDefault();

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }
                var fileName = file.FileName;
               
                string[] legalExtention = { ".pdf", ".jpg", ".png" };

                if (!legalExtention.Contains(Path.GetExtension(fileName.ToLower())))
                {
                    return Json(new { Status = false, message = Constants.M_Ilegal_Uploaded_Pdf_Image_Doc_Message });
                }
                var fulllocation = clubDomain + location;
                var uplodedFileName = Path.GetFileNameWithoutExtension(fileName).Replace(' ','_') + "_" + DateTime.UtcNow.ToString("MMddyyyyHHmm") + Path.GetExtension(fileName.ToLower());
                FileUploder(fileData, uplodedFileName, fulllocation);
                var fileUrl = StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + fulllocation + "/" + uplodedFileName).AbsoluteUri;
                if (!string.IsNullOrEmpty(uplodedFileName))
                {
                    return Json(new
                    {
                        Status = true,
                        URL = fileUrl,
                        FileName = uplodedFileName,
                        Index= index,
                        uploadDate = DateTime.UtcNow.ToString(Constants.DateTime_Comma)
                    });
                }
            }

            return Json(new { Status = false, message = Constants.M_E_CreateEdit });

        }
        public virtual ActionResult UploadImageOrPdfOrDoc(IEnumerable<HttpPostedFileBase> files, string location, int index = -1)
        {

            if (files != null)
            {
                var clubDomain = Request.Url.GetSubDomain();
                var file = files.FirstOrDefault();

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }
                var fileName = file.FileName;

                string[] legalExtention = { ".pdf", ".jpg", ".png", ".doc" };

                if (!legalExtention.Contains(Path.GetExtension(fileName.ToLower())))
                {
                    return Json(new { Status = false, message = Constants.M_Ilegal_Uploaded_Pdf_Image_Doc_Message });
                }

                var fulllocation = clubDomain + location;
                var uplodedFileName = Path.GetFileNameWithoutExtension(fileName).Replace(' ', '_') + "_" + DateTime.UtcNow.ToString("MMddyyyyHHmm") + Path.GetExtension(fileName.ToLower());
                FileUploder(fileData, uplodedFileName, fulllocation);
                var fileUrl = StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + fulllocation + "/" + uplodedFileName).AbsoluteUri;
                if (!string.IsNullOrEmpty(uplodedFileName))
                {
                    return Json(new
                    {
                        Status = true,
                        URL = fileUrl,
                        FileName = uplodedFileName,
                        Index = index,
                        UploadDate = DateTime.UtcNow.ToString(Constants.DateTime_Comma)
                    });
                }
            }

            return Json(new { Status = false, message = Constants.M_E_CreateEdit });

        }
        private string FileUploder(byte[] fileData, string fileName, string location)
        {
            try
            {
               
                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                string contentType = string.Format(Constants.F_Image_MimeType, Path.GetExtension(fileName).Substring(1));
               
                Stream fileStream = new MemoryStream(fileData);

                sm.UploadBlob(location, fileName, fileStream, contentType);
                fileStream.Close();

                return fileName;


            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return string.Empty;
            }
        }
        public virtual ActionResult SaveFile(IEnumerable<HttpPostedFileBase> supplementalDocument, long orderItemId, int formId)
        {

            if (supplementalDocument != null)
            {
                var clubDomain = Request.Url.GetSubDomain();
                var file = supplementalDocument.FirstOrDefault();

                byte[] fileData = null;
                using (var binaryReader = new BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }
                var fileName = file.FileName;

                string[] legalExtention = { ".jpg", ".png", ".jpeg", ".gif", ".doc", ".pdf", ".docx", ".zip", ".rar" };

                if (!legalExtention.Contains(Path.GetExtension(fileName.ToLower())))
                {
                    return Json(new { message = Constants.M_Ilegal_Uploaded_Image_Message });
                }
                //var image = new WebImage(fileData);

                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                string contentType = string.Format(Constants.F_Image_MimeType, Path.GetExtension(file.FileName).Substring(1));

                Stream fileStream = new MemoryStream(fileData);
                // upload file
                sm.UploadBlob(clubDomain + string.Format(Constants.Path_FollowUpForms, orderItemId, formId), fileName, fileStream, contentType);
                fileStream.Close();

                var imageUrl = StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + clubDomain + string.Format(Constants.Path_FollowUpForms, orderItemId, formId) + "/" + fileName).AbsoluteUri;

                return Json(new
                {
                    URL = imageUrl,
                    fileName = fileName
                });
            }

            return null;
        }

        public virtual ActionResult Remove(string[] fileNames)
        {
            // The parameter of the Remove action must be called "fileNames"

            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                    // TODO: Verify user permissions

                    if (System.IO.File.Exists(physicalPath))
                    {
                        // The files are not actually removed in this demo
                        // System.IO.File.Delete(physicalPath);
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }

    }



}