﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignAddUserListStep1controller', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaigns-add-userlist';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        // for detect user uploaded a list.
        $scope.uploadedList = null;

        $scope.showListName = null;

        $scope.campaignId = $scope.MC.stateParams.campaignId;
        $scope.pageMode = null;

        $scope.Model = {};

        if (is.undefined($scope.campaignId) || $scope.campaignId == '') {
            $scope.pageMode = 'Create';
        } else {
            $scope.pageMode = 'Edit';
            $scope.showListName = true;
        }

        if ($scope.pageMode == 'Create') {
            $http.get(baseUrl + '/CreateCampaign', { params: { campaignType: 'UserList' } }).success(function (data, status, headers, config) {
                $scope.Model = data;
            });
        } else {
            $http.get(baseUrl + '/EditCampaign', { params: { id: $scope.campaignId, campaignType: 'UserList' } }).success(function (data, status, headers, config) {
                $scope.Model = data;
            });
        }

        $scope.clubMailListsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetAllClubMailList',
                        type: "POST",
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.listGridOptions = {
            dataSource: $scope.clubMailListsDataSource,
            pageable: true,
            sortable: true,
            columns: [
                {
                    field: "ListName",
                    title: "List name",
                    width: "200px",
                }, {
                    field: "ListType",
                    title: "List type",
                    width: "120px"
                }, {
                    field: "CreateDate",
                    title: "Created date",
                    width: "120px",
                    template: "#= kendo.toString(kendo.parseDate(CreateDate), 'MMM dd, yyyy H:mm') #",
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    //template: "<div class='box-col k-content'><ul kendo-menu style='display: inline-block;' k-orientation='horizontal' k-rebind='horizontal' k-on-select=''> <li> ... <ul> <li ui-sref='EditEmailList({listId: #=Id# })'> Edit </li> <li ng-click='deleteCampaignList(#=Id#)'> Delete </li> </ul></div>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				    <ul>" +
                        "<li ng-click='SaveStep1(#= Id #)' class='k-item k-state-default k-first' role='menuitem'>\
						    <span class='k-link jbi-angle-right'>Select and continue</span>\
				      </li>" +
                        "#if(ListType!='Dynamic' ){#<li  ngaction='CampaignRecipients_View' ui_sref='EditEmailList({listId: #=Id# })' class='k-item k-state-default k-first' role='menuitem'>\
						    <span class='k-link jbi-pencil'>Edit</span>\
				      </li>#}#\<li disabled='disabled'><hr /></li>\
				                <li ng-click='deleteCampaignList(#=Id#)'>\
					                <span class='k-link jbi-remove'>Delete</span>\
				                </li>\
				            </ul>\
			            </li>\
		            </ul>",
                    attributes: {
                        class: "grid-actions"
                    }
                }
            ]
        };

        $scope.listsSearch = function () {
            $scope.listGridOptions.dataSource.read();
        }

        $scope.deleteCampaignList = function (Id) {
            jbConfirmation
                .yes(function () {
                    $http.post(baseUrl + '/DeleteCampaignMailList', { listId: Id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $scope.listGridOptions.dataSource.read();
                        }
                    });
                }).confirm();
        }

        $scope.SaveStep1 = function (Id) {
            $scope.Model.ListId = Id;
            $http.post(baseUrl + '/CampaignAddEditStep1', { model: $scope.Model }).success(function (data, status, headers, config) {
                if (data.Status == true) {
                    $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                }
                else {
                    $scope.MC.handleErrors($scope, data);
                    if (data.FormErrors[0].Key == "model.NoUser") {
                        jbToast.error($scope.MC.joinStringsWithBreak(data.FormErrors[0].Messages));
                    }
                }
            });
        }

        $scope.saveUploadedList = function () {
            $http.post(baseUrl + '/SaveUploadedList', { listName: $scope.Model.ListName }).success(function (data, status, headers, config) {
                if (data.Status == true) {
                    $scope.removeOldList();
                    $state.go('EmailCampaignAddUserListStep1', { campaignType: "UserList" });
                } else {
                    $scope.MC.handleErrors($scope, data);
                    if (data.FormErrors[0] && data.FormErrors[0].Key == "model.NoFileUploaded") {
                        jbToast.error($scope.MC.joinStringsWithBreak(data.FormErrors[0].Messages));
                    }
                    if (data.FormErrors[0].Key == "model.FileContentError") {
                        $(".k-upload-files.k-reset").find("li").remove();
                        jbToast.error($scope.MC.joinStringsWithBreak(data.FormErrors[0].Messages));
                    }
                    if (data.FormErrors[0].Key == "model.FileNotInFormat") {
                        $(".k-upload-files.k-reset").find("li").remove();
                        jbToast.error($scope.MC.joinStringsWithBreak(data.FormErrors[0].Messages));
                    }
                }
            });
        }

        $scope.OnUploaderSuccess = function (e) {
            if (e.response.Status == true && e.response.Data == 'Add') {
                jbToast.success("File uploaded.");
                $scope.uploadedList = true;
            } else if (e.response.Status == true && e.response.Data == 'Remove') {
                jbToast.success("Remove uploaded file completed.");
            }
        }

        $scope.onUpload = function (e) {
            var files = e.files;
            $.each(files, function (index, value) {
                if (value.extension !== '.csv' && value.extension !== '.txt') {
                    jbToast.success("Extention not allowed!");
                    e.preventDefault();
                }
            });
        }

        $scope.removeOldList = function () {
            $http.post(baseUrl + '/DeleteUploadedList')
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        //$scope.uploadedList = {};
                        $scope.showListName = false;
                        $scope.uploadedList = false;
                        $scope.Model.ListName = "";
                    }
                });
        }

    }]);
})();