﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('reminderInvoicesListFromReportController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', 'reminderInvoicesService', function ($scope, $http, $stateParams, $state, jbToast, reminderInvoicesService) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/PortalReport";

        $scope.Model = reminderInvoicesService.get();
        $scope.InfoModel = {};

        var checkedInvoice = [];
        for (var i in $scope.Model.checkedIds) {
            if ($scope.Model.checkedIds[i]) {
                checkedInvoice.push(i);
            }
        }

        $http.get($scope.baseUrl + '/GetInformationForReminder', { params: { invoiceIds: checkedInvoice } }).success(function (data, status, headers, config) {

            $scope.InfoModel = data;
            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
        });

        $scope.SendReminder = function () {
            $http.post($scope.baseUrl + "/SendReminderInvoices", { model: $scope.InfoModel })
                             .success(function (data, status, headers, config) {

                                 if (data.Status == true) {
                                     $state.go('InvoicesPortalReports');
                                     jbToast.success("You have sent invoices reminder successfully.")
                                 } else {
                                     jbToast.error(data.Message);
                                 }
                             }).error(function (data, status, headers, config) {

                             });
                         };


    }])

})();