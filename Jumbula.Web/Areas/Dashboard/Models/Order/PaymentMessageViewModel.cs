﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SportsClub.Areas.Dashboard.Models
{
    public class PaymentMessageViewModel
    {
      
        public string Username { get; set; }
        public string SeasonDomain { get; set; }
        public long ProgramId { get; set; }
        public long OrderItemId { get; set; }

        public string ConfirmationId { get; set; }
        public string FullName { get; set; }
        public string Message { get; set; }

        public bool IsSuceed { get; set; }
      
    }

    
   

}