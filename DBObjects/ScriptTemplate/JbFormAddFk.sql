--FOREIGNKEYS DEFINITION For JBFORM--@mn

declare @TableName nvarchar(50)='TableNamexxxxxxx' --without dbo.


IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'FK_'+@TableName+'_JbForms')
BEGIN
declare @command nvarchar(500)='ALTER TABLE [dbo].['+@TableName+'] Add JbForm_Id int null';
exec(@command);
print 'JbForm_Id column created successfully ';

set @command='ALTER TABLE [dbo].['+@TableName+']  WITH CHECK ADD  CONSTRAINT [FK_'+@TableName+'_JbForms] FOREIGN KEY([JbForm_Id])
REFERENCES [dbo].[JbForms] ([Id])
ALTER TABLE [dbo].['+@TableName+'] CHECK CONSTRAINT [FK_'+@TableName+'_JbForms]';
exec(@command);
print 'FK created successfully ';
END
ELSE
BEGIN
	print 'Fk has already existed';
END
