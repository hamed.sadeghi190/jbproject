﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CatalogsSearchController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', '$location', '$anchorScroll', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, $location, $anchorScroll) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams;

        $scope.Model = {};
        $scope.SearchModel = {};
        $scope.page= 1,
        $scope.itemsPerPage= 10,
        $scope.totalItems = 0

        $http.get('Dashboard/Catalog/GetAllValueForSearchCatalog').success(function (data, status, headers, config) {
            $scope.Model = data;

        });

        $scope.OneProviderCatalogs = function (providerId) {
            if (providerId != null) {
                $state.go('ViewCatalogs', ({ providerId: providerId }));
            }
        }

        $scope.SearchClassesForAllprovider = function () {
            $http.post('Dashboard/Catalog/SearchClassesForAllProvider', { model: $scope.Model })
              .success(function (data, status, headers, config) {
                  $scope.SearchModel = data;

                  $scope.totalItems = $scope.SearchModel.TotalItems;
                
              });
        };

        $scope.SeeClassesDetails = function (providerId,catalogId) {
            if (providerId!=null && catalogId!=null) {
                var url = $state.href('ViewCatalogs', ({ providerId: providerId, catalogId: catalogId }));
                window.open(url, '_blank');
            }
        }

        $scope.selectPage = function (page) {
            $scope.pagingInfo.page = page;
            loadUsers();
        };

    }]);
})();


