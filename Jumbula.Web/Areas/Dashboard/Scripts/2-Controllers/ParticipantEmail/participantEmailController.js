﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('participantEmailController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'templateEditorService', 'CampaignService', 'imageUploadService', function ($scope, $http, jbToast, $stateParams, $state, templateEditorService, CampaignService, imageUploadService) {
        window.app.globalObjects.pageClass = 'form-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        // init model.
        if (is.undefined($scope.Model)) {
            $scope.Model = {};
            $scope.Model.MailTemplate = {};
        }

        //Image validate and crop start
        $scope.validateImage = function (elem, pos) {
            if (pos == 'header') {
                $scope.positionBanner = 'header';
            }
            else if (pos = 'footer') {
                $scope.positionBanner = 'footer';
            }
            imageUploadService.validateImage(this, elem)
        }
        $scope.typeBannerHeader = function (typeb) {
            $scope.typeBannerHeaderValue = typeb;
        }
        $scope.typeBannerFooter = function (typeb) {
            $scope.typeBannerFooterValue = typeb;
        }
        $scope.showCropper1 = true;
        $scope.showCropper2 = false;
        $scope.hideCropper1 = function () {
            $scope.showCropper1 = false;
            $scope.showCropper2 = true;
            $scope.cropper = imageUploadService.clearCropper(this);
        }

        $scope.hideCropper2 = function () {
            $scope.showCropper1 = true;
            $scope.showCropper2 = false;
            $scope.cropper = imageUploadService.clearCropper(this);
        }
        $scope.clearCropper = function () {
            $scope.cropper = imageUploadService.clearCropper(this);

        }

        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        }

        $scope.existingImages = templateEditorService.existingImages;

        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        }

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        }

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        }

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        }

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        }

        $scope.enterEditing = function () {
            return templateEditorService.enterEditing(this);
        }

        $scope.saveEditing = function (posbanner, typeBannerHV, typeBannerFV) {
            $scope.positionBanner = null;
            $scope.Model.MailTemplate.ChangeTemplate = true;
            return templateEditorService.saveEditing(this, posbanner, typeBannerHV, typeBannerFV);
        }

        $scope.cancelEditing = function () {
            $scope.positionBanner = null;
            return templateEditorService.cancelEditing(this);
        }
        
        console.log($scope.MC.stateParams.programId)
        $http.get(baseUrl + '/SendParticipantEmail', { params: { userId: $scope.MC.stateParams.userId, programId: $scope.MC.stateParams.programId, orderItemId: $scope.MC.stateParams.orderItemId } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
                $scope.ChangeTemplate(0);
            });

        $scope.ChangeTemplate = function (idx) {
            $http.get(baseUrl + '/GetTemplate', { params: { id: idx } }).success(function (data, stats, headers, config) {
                $scope.Model.MailTemplate = data;
                if ($scope.Model.MailTemplate.Name == "") {
                    $scope.Model.MailTemplate.ChangeTemplate = true;
                } else {
                    $scope.Model.MailTemplate.ChangeTemplate = false;
                }
            });
        };

        $scope.SendEmail = function () {
            $http.post(baseUrl + '/SendParticipantEmail', { model: $scope.Model })
              .success(function (data, status, headers, config) {
                  if (data.Status == true, data.Message == 'Program') {
                      jbToast.success("Email is sent successfully.");
                      $state.go('Program', { seasonDomain: $scope.MC.stateParams.seasonDomain, programId: $scope.MC.stateParams.programId });
                  } else if (data.Status == true, data.Message == 'Donation') {
                      jbToast.success("Email is sent successfully.");
                  }
                  else {
                      $scope.MC.handleErrors($scope, data);
                  }
              });
        }

    }]);
})();