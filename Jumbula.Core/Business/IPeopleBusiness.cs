﻿using System.Collections.Generic;
using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using Jumbula.Core.Model.Notification;
using Jumbula.Core.Model.People;

namespace Jumbula.Core.Business
{
    public interface IPeopleBusiness
    {
        PaginatedList<DelinquentListViewModel> GetDelinquentList(int clubId, DelinquentFilterViewModel filters, List<Sort> sorts, int pageIndex, int pageSize);
        object GetDelinquentsTotal(int clubId);

        EmailViewModel GetDelinquentEmail(string relatedEntity, long relatedEntityId, Club club,int userId);
        OperationStatus GetDelinquentEmails(List<DelinquentEmailParameterRelatedEntityItemModel> relatedEntity, Club club,int userId);

        bool IsDelinquent(int userId, int clubId);

        NotificationModel GetDelinquentNotificationItem(int userId, ClubBaseInfoModel clubId);

        ParticipantClassScheduleViewModel GetParticipantClassSchedule(int playerId, int clubId);

        byte[] GetExcelParticipantClassSchedule(int playerId, int clubId);
    }
}
