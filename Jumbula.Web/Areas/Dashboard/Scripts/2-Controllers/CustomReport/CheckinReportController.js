﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CheckinReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = {};
        $scope.Model.checkedIds = {};
        var detailExportPromises = [];

        $scope.Model = customReportService.getModelFromStrorage();
        var model = $scope.Model;


        var dataSourceFulldata = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: baseUrl + '/GetAllCheckinsReport',
                    type: "POST",
                    data: function () {
                        return {
                            model: model
                        };
                    },
                },
            },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            pageSize: 10,
            serverSorting: false,
            serverPaging: true,
            resizable: true,
        });


        dataSourceFulldata.read();


        $scope.CheckinReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetCheckinsReport',
                        type: "POST",
                        data: function () {
                            return {
                                model: model,
                            };
                        },

                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            pageSize: 10,
            serverSorting: false,
            serverPaging: true,
            resizable: true,
        });

        $scope.CheckinReportGridOptions = {
            excel: {
                allPages: true,
                proxyURL: "/Dashboard/CustomReport/Save"
            },
            excelExport: function (e) {
                e.preventDefault();
                var workbook = e.workbook;
                detailExportPromises = [];
                // kendo new version
                var masterData = e.data;
                for (var rowIndex = 0; rowIndex < masterData.length; rowIndex++) {
                    exportChildData(masterData[rowIndex].Id, rowIndex);
                }

                $.when.apply(null, detailExportPromises)
                    .then(function () {
                        // get the export results
                        var detailExports = $.makeArray(arguments);

                        // sort by masterRowIndex
                        detailExports.sort(function (a, b) {
                            return a.masterRowIndex - b.masterRowIndex;
                        });

                        // add an empty column
                        workbook.sheets[0].columns.unshift({
                            width: 30
                        });

                        // prepend an empty cell to each row
                        for (var i = 0; i < workbook.sheets[0].rows.length; i++) {
                            workbook.sheets[0].rows[i].cells.unshift({});
                        }

                        // merge the detail export sheet rows with the master sheet rows
                        // loop backwards so the masterRowIndex doesn't need to be updated
                        for (var i = detailExports.length - 1; i >= 0; i--) {
                            var masterRowIndex = detailExports[i].masterRowIndex + 1; // compensate for the header row

                            var sheet = detailExports[i].sheet;

                            // prepend an empty cell to each row
                            for (var ci = 0; ci < sheet.rows.length; ci++) {
                                if (sheet.rows[ci].cells[0].value) {
                                    sheet.rows[ci].cells.unshift({});
                                }
                            }

                            // insert the detail sheet rows after the master row
                            [].splice.apply(workbook.sheets[0].rows, [masterRowIndex + 1, 0].concat(sheet.rows));
                        }

                        // save the workbook
                        kendo.saveAs({
                            dataURI: new kendo.ooxml.Workbook(workbook).toDataURL(),
                            fileName: "Check-inReport.xlsx",
                            proxyURL: "/Dashboard/CustomReport/Save"
                        });


                    });
                cfpLoadingBar.complete();
            },
            dataSource: $scope.CheckinReportDataSource,
            resizable: true,
            scrollable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5
            },
            detailTemplate: kendo.template($("#template").html()),
            detailInit: detailInit,
            dataBound: function (e) {
                //customReportService.initializeDoubleScroll();
                this.expandRow(this.tbody.find("tr.k-master-row"));

            },

            sortable: { mode: 'single' },
            columns: [
                {
                    field: "ProgramName",
                    title: "Program",
                    width: "auto",
                },

            ],
        }

        function detailInit(e) {
            var detailRow = e.detailRow;
            detailRow.find(".orders").kendoGrid({
                dataSource: {
                    type: "json",
                    transport: {
                        read: {
                            url: baseUrl + '/GetCheckinsReportDetails',
                            type: "POST",
                            data: function () {
                                return {
                                    programId: e.data.Id,
                                };
                            },

                        },
                    },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                },
                
                excelExport: function (e) {
                    e.preventDefault();
                },
                scrollable: true,
                sortable: true,
                resizable: true,
                columns: [
                    {
                        field: "SessionDate",
                        title: "Session date",
                        width: "auto"
                    },
                    {
                        field: "UserName",
                        title: "User",
                        width: "auto",
                    },
                    {
                        field: "CheckinTime",
                        title: "Check-in time",
                        width: "auto",
                    },
                    {
                        field: "Distance",
                        title: "Distance (meter)",
                        width: "auto"
                    },
                    {
                        field: "CheckinDelay",
                        title: "Delay (min)",
                        width: "auto"
                    },
                    {
                        field: "Address",
                        title: "Check-in address",
                        width: "auto",
                        template: '<span style="white-space: pre-line" > #=Address# </span>'
                    },
                    {
                        field: "GPS",
                        title: "GPS",
                        width: "auto",
                        template: '<span style="white-space: pre-line" > #=GPS# </span>'
                    },

                ]
            });
        }





        function exportChildData(Id, rowIndex) {

            var deferred = $.Deferred();
            detailExportPromises.push(deferred);

            dataSourceFulldata.filter({ field: "Id", operator: "eq", value: Id });

            var exporter = new kendo.ExcelExporter({
                columns: [
                    {
                        field: "SessionDate",
                        title: "Session date",
                        width: "auto"
                    },
                    {
                        field: "UserName",
                        title: "User",
                        width: "auto",
                    },
                    {
                        field: "CheckinTime",
                        title: "Check-in time",
                        width: "auto",
                    },
                    {
                        field: "Distance",
                        title: "Distance (meter)",
                        width: "auto"
                    },
                    {
                        field: "CheckinDelay",
                        title: "Delay (min)",
                        width: "auto"
                    },
                    {
                        field: "Address",
                        title: "Checkin address",
                        width: "auto",
                        template: '<span style="white-space: pre-line" > #=Address# </span>'
                    },
                    {
                        field: "GPS",
                        title: "GPS",
                        width: "auto",
                        template: '<span style="white-space: pre-line" > #=GPS# </span>'
                    },
                ],

                dataSource: dataSourceFulldata
            });

            exporter.workbook().then(function (book, data) {
                deferred.resolve({
                    masterRowIndex: rowIndex,
                    sheet: book.sheets[0]
                });
            });
        }



        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#CheckinReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);

})();