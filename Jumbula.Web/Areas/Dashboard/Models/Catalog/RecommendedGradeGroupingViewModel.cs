﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class RecommendedGradeGroupingViewModel
    {
        public int Id { get; set; }

        public string StringId
        {
            get
            {
                return Id == 0 ? Guid.NewGuid().ToString(): Id.ToString();
            }
        }

        public string Title { get; set; }

        public bool IsChecked { get; set; }

        public bool IsDefault { get; set; }
    }
}