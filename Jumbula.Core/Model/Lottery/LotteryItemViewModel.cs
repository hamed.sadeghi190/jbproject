﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Model.Lottery
{
    public class LotteryItemViewModel
    {
        public long Id { get; set; }

        public long OrderId { get; set; }

        public string Attendee { get; set; }
        public string OrderDate { get; set; }
        public string ConfirmationId { get; set; }
        public string EntryFee { get; set; }
        public string EntryFeeName { get; set; }
        public LotteryStatus LotteryStatus { get; set; }
        public OrderItemStatusCategories ItemStatus { get; set; }
        public bool EnableMarkAsCondidate { get; set; }

    }
}
