﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DailyDismissalReportViewModel
    {
        public string Id { get; set; }
        public string StudentName { get; set; }
        public string DismissalFromEnrichment { get; set; }
        public string IsPickedUp { get; set; }
        public string PickupTime { get; set; }
        public string PickupName { get; set; }
    }
}