﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CatalogPriceViewModel
    {
        public CatalogPriceViewModel()
        {
            ListAgendas = new List<AgendaViewModel>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage="{0} is required.")]
        public string Title { get; set; }

        public string DurationTitle { get; set; }

        [Display(Name="Fee")]
        [Required(ErrorMessage = "{0} is required.")]
        public decimal? Amount { get; set; }

        public int Weeks { get; set; }

        public bool IsChecked { get; set; }

        public bool IsDefault { get; set; }
        public List<AgendaViewModel> ListAgendas { get; set; }
    }


    public class AgendaViewModel
    {
        public string Title { get; set; }
        //[Required(ErrorMessage = "{0} is required.")]
        //[MaxLength(140, ErrorMessage = "{0} is required.")]
        public string AgendaWeek { get; set; }
    }
}
