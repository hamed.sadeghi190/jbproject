﻿using System;
using System.Collections.Generic;

namespace Jumbula.Business
{
    public abstract class BaseReportEngine
    {
        public abstract object GetSchema();
        public abstract List<object> GetData(params object[] paremeters);
    }

    public class DailyDismissalReportEngine : BaseReportEngine
    {
        //IOrderItemBusiness _orderItemBusiness;
        //public DailyDismissalReportEngine(IOrderItemBusiness orderItemBusiness)
        //{
        //    _orderItemBusiness = orderItemBusiness;
        //}

        public override List<object> GetData(params object[] paremeters)
        {
            //return _orderItemBusiness.GetList().Take(10).Select(r => new { Name = r.FirstName}).ToList<object>();
            throw new NotImplementedException();
        }

        public override object GetSchema()
        {
            throw new NotImplementedException();
        }
    }

    public class CampRosterReportEngine : BaseReportEngine
    {
        public CampRosterReportEngine()
        {

        }

        public override List<object> GetData(params object[] paremeters)
        {
            throw new NotImplementedException();
        }

        public override object GetSchema()
        {
            throw new NotImplementedException();
        }
    }
}
