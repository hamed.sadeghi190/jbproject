using Jb.Framework.Common.Forms.JsonFormatter;
using Newtonsoft.Json;

namespace Jb.Framework.Common.Forms
{
    [JsonConverter(typeof(JbElementConverter))]
    public class JbTextArea : JbBaseElement, IInputElement
    {
        public JbTextArea()
        {
            Width = ElementWidth.Full;
        }


        public bool IsChecked { get; set; }

        public string Value
        {
            get;
            set;
        }

        public string GetValue()
        {
            return Value;
        }


        public bool IsRequired
        {
            get;
            set;
        }

        public string PlaceHolder { get; set; }

    }
}