﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemClubShowPlanController', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.baseUrl = window.location.origin + "/Dashboard/System";
        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.Model = {};
        $scope.Model.Id = $stateParams.clubId;

       
        $scope.loadModel = function (editable) {
            $http.get(window.location.origin + '/Dashboard/System/GetShowPlan/?clubId=' + $scope.Model.Id ).success(function (data, status, headers, config) {
                $scope.Model = data;
                //console.log(data);
                $scope.Model.Editable = editable;
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/GetCreditCard/?clubId=' + $scope.Model.Id,
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                              
                           }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            pageSize: 50,
            batch: true,
            serverPaging: true,
            serverSorting: true,
            schema: {
                data: "data",
                total: "total"
            },
            sortable: true
        });
        $scope.mainGridOptions = {
            dataSource: $scope.mainGridDataSource,
            sortable: true,
            resizable: false,
            pageable: {
                messages: {
                    empty: "Club hasn't any credit card."
                }
            },
            columns: [
                {
                    field: "CardHolderName",
                    title: "CardHolder Name",
                },
                {
                    field: "Method",
                    title: "Method",
                    width: "200px",
                    template: "#= Method # #if(IsDefault) {# <i class='grid-label label bg-green  fg-white'>Active</i> #}#"
                },
               
                {
                    field: "ExpiryDate",
                    title: "Expiration",
                },
               
                 {
                     field: "Address",
                     title: "Address",
                 },
                 {
                     title: "Actions",
                     width: "70px",
                     template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                      <li  ng-click="deleteClub( #=Id# )">\
                                          <span class="k-link jbi-remove">Delete</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>'
                 }
            ],
        }

        $scope.loadModel(false);

        $scope.goBack = function () {
            if ("goBackToOverView" in window) {
                if (window.goBackToOverView) {
                    window.goBackToOverView = false;
                    window.history.back();
                }
                else {
                    window.goBackToOverView = false;
                    $state.go('Program', ({ seasonDomain: $scope.MC.stateParams.seasonDomain, programId: $scope.Model.ProgramId }));
                }
            }
            if ("goToFamilyOrder" in window) {
                if (window.goToFamilyOrder > 0) {
                    var userId = window.goToFamilyOrder;
                    window.goToFamilyOrder = -1;
                    $state.go('FamilyOrders', { userId: userId });
                }
            }
            else {
                window.goBackToOverView = false;
                $state.go('Program', ({ seasonDomain: $scope.MC.stateParams.seasonDomain, programId: $scope.Model.ProgramId }));
            }
        }

        $scope.editPayment = function (programid, transactionId) {
            $state.go("MakePayment", { seasonDomain: $stateParams.seasonDomain, programId: programid, clubId: $stateParams.clubId, transactionId: transactionId });
        }
        $scope.makePayment = function (seasonDomain, programId, id) {
            window.comesFromOrderItem = true;
            $state.go("MakePayment", { seasonDomain: seasonDomain, programId: programId, clubId: id });
        }
        $scope.refund = function (seasonDomain, programId, id) {
            window.comesFromOrderItem = true;
            $state.go("PartiallyRefundOrderItem", { seasonDomain: seasonDomain, programId: programId, clubId: id });
        }
    }]);
})();