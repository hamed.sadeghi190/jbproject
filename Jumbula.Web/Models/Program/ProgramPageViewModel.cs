﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ProgramPageViewModel : BaseViewModel<Program, long>, IProgramModel
    {
        public string ProviderName { get; set; }
        public string ProviderContact { get; set; }
        public string ProviderPhone { get; set; }
        public string ProviderEmail { get; set; }
        public bool HasProvider { get; set; }
        public string PartnerName { get; set; }
        public bool HasPartner { get; set; }
        public List<string> ImageUrls { get; set; }
        public Dictionary<string,string> ListOfImagesAndSource { get; set; }
        public string PartnerEmail { get; set; }
        public bool IsDraft { get; set; }
        public int ClubId { get; set; }
        public bool DisableCapacityRestriction { get; set; }
        public long SeasonId { get; set; }
        public bool IsViewMode { get; set; }
        public string ClubDomain { get; set; }

        public bool IsWaitListMode { get; set; }
        public bool IsStaffActive { get; set; }

        public string CategoriesName
        {
            get
            {
                if (TypeCategory != ProgramTypeCategory.ChessTournament)
                {
                    if (Categories != null && Categories.Any())
                    {
                        return string.Join(", ", Categories.Select(c => c.Name));
                    }
                }
                return string.Empty;
            }
        }
        public string ClubName { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }

        public Jumbula.Common.Enums.TimeZone  ClubTimeZone { get; set; }

        public ProgramStatus Status { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public ProgramTypeCategory TypeCategory { get; set; }

        public IProgramBodyViewModel Body { get; set; }
        public bool IsTeamRegistration { get; set; }
        public TournamentType TournamentType { get; set; }

        public GoogleMapsViewModel Map { get; set; }

        public string ClubPhoneNumber { get; set; }
        public string PhoneExtension { get; set; }
        public string ClubEmail { get; set; }

        public List<CategoryViewModel> Categories { get; set; }

        public bool HasNoSession { get; set; }
        public string ProgramRegStatus { get; set; }

        public int? CapacityLeft { get; set; }

        public bool HideProgramDates { get; set; }
        public bool HideEnrollmentCapacity { get; set; }
        public bool HidePhoneNumber { get; set; }

        public string ERButtonTextLabel { get; set; }
        public string TestimonialsTitle { get; set; }
        public string DynamicStyle { get; set; }
        public string Testimonials { get; set; }
        public bool HasTestimonials { get; set; }
        public bool ShowViewEntries { get; set; }
        public bool IsFromHomePage { get; set; }
        public bool ShowProviderNameInClassPage { get; set; }
        public bool IsTestMode { get; set; }

    }
}