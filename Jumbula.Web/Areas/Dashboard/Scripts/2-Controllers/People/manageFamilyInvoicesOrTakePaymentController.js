﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('manageFamilyInvoicesOrTakePaymentController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'familyOrderItemsService', 'familyOrdersService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, familyOrderItemsService, familyOrdersService) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.MC.stateParams = $stateParams;


        var userId = $scope.stateParams.userId;
        var clubId = $scope.stateParams.clubId;
        var orderId = $scope.stateParams.orderId;
        var orderItemId = $scope.stateParams.orderItemId;
        var isToBackStep = $scope.stateParams.IsToBack;
        $scope.UserName = "";

        if ($scope.stateParams.type) {
            switch ($scope.stateParams.type) {
                case "Invoice":
                    {
                        $scope.reportName = "Invoice";
                        break;
                    }
                case "TakePayment":
                    {
                        $scope.reportName = "Take a Payment";
                        break;
                    }

            }
        }


        var returnedByBack = $state.returnByBack();
        $scope.Model = {};
        $scope.OrderitemModel = {};

        if (isToBackStep == 'false' && orderItemId > 0) {
            returnedByBack = false;
        }

        if (returnedByBack) {
            $scope.Model = familyOrdersService.get();
            $scope.OrderitemModel = familyOrderItemsService.get();

            for (var i = 0; i < $scope.Model.length; i++) {
                if (i == 0) {
                    $scope.UserName = $scope.Model[i].UserName;

                }
            }
            $scope.getOrderItems = function (order) {
                $http.get(window.location.origin + '/Dashboard/People/GetFamilyOrderItems?orderid=' + order.Id).success(function (data, status, headers, config) {

                    $scope.OrderitemModel[order.Id] = data;

                    $scope.OrderInvoiceAmount(order.Id, order.expanded);
                });

            }
        }

        if (!returnedByBack) {

            $scope.MC.backQueueItems = [];

            $http.get(window.location.origin + '/Dashboard/People/GetFamilyOrders?userid=' + userId + '&clubId=' + clubId).success(function (data, status, headers, config) {
                $scope.Model = data;

                var orders = $scope.Model;
                for (var i = 0; i < orders.length; i++) {

                    if (orders[i].Id == $scope.stateParams.orderId) {

                        orders[i].Checked = true;

                        $http.get(window.location.origin + '/Dashboard/People/GetFamilyOrderItems?orderid=' + $scope.stateParams.orderId).success(function (data, status, headers, config) {

                            $scope.OrderitemModel[$scope.stateParams.orderId] = data;

                            $scope.OrderInvoiceAmount($scope.stateParams.orderId, true);

                        });
                        orders[i].expanded = true;
                    }
                }

            });

            $scope.getOrderItems = function (order) {
                $http.get(window.location.origin + '/Dashboard/People/GetFamilyOrderItems?orderid=' + order.Id).success(function (data, status, headers, config) {

                    $scope.OrderitemModel[order.Id] = data;

                    $scope.OrderInvoiceAmount(order.Id, order.expanded);

                });

            }


        }


        $scope.confirmItems = function () {

            var CheckedModel = [];
            var count = 0
            for (var i = 0; i < $scope.Model.length; i++) {

                if ($scope.Model[i].expanded) {

                    count++;
                }
            }
            if (count > 0) {

                familyOrdersService.set($scope.Model);

                familyOrderItemsService.set($scope.OrderitemModel);

                if ($stateParams.type == 'Invoice') {

                    $state.forward('ConfirmFamilyInvoice', { userId: $stateParams.userId, clubId: $stateParams.clubId, orderItemId: $stateParams.orderItemId }, 'FamilyInvoicesOrTakePayment', { userId: $stateParams.userId, type: $stateParams.type, clubId: $stateParams.clubId, orderItemId: $stateParams.orderItemId }, function () { });

                }
                else {
                    $state.forward('FamilyTakePayment', { userId: $stateParams.userId, clubId: $stateParams.clubId }, 'FamilyInvoicesOrTakePayment', { userId: $stateParams.userId, type: $stateParams.type, clubId: $stateParams.clubId }, function () { });
                }

            } else {
                jbToast.error("Please select an item to continue.", "");
            }
        }


        //expanded for orderitemlevel and InstalmentLevel, when it is false The means open
        $scope.OrderInvoiceAmount = function (orderId, expanded) {

            var orders = $scope.Model;
            var orderItems = $scope.OrderitemModel[orderId];

            for (var i = 0; i < orders.length; i++) {

                if (orders[i].Id == orderId) {
                    orders[i].InvoiceAmount = 0
                    if (orders[i].expanded == expanded) {

                        for (var j = 0; j < orderItems.length; j++) {


                            if (orderItems[j].Balance > 0 && orderItems[j].PaymentplanType == 'Installment') {

                                orderItems[j].OriginalBalance = orderItems[j].Balance;

                                orders[i].InvoiceAmount += orderItems[j].OriginalBalance;

                                orderItems[j].expanded = false;

                                orderItems[j].Checked = true;

                                for (var k = 0; k < orderItems[j].Installments.length; k++) {

                                    if (orderItems[j].Installments[k].Balance > 0) {

                                        orderItems[j].Installments[k].Checked = true;
                                        orderItems[j].Installments[k].expanded = false;
                                        orderItems[j].Installments[k].InvoiceAmount = orderItems[j].Installments[k].Balance
                                    }

                                }
                            }
                            if (orderItems[j].Balance > 0 && orderItems[j].PaymentplanType != 'Installment') {
                                orderItems[j].OriginalBalance = orderItems[j].Balance;

                                orders[i].InvoiceAmount += orderItems[j].OriginalBalance;

                                orderItems[j].expanded = false;

                                orderItems[j].Checked = true;
                            }

                            if (orderItems[j].Balance == 0 && orderItems[j].PaymentplanType == 'Installment') {
                                orderItems[j].expanded = false;
                            }

                        }

                    }

                }

            }


        }

        //when orderLevel is expanded invoiceAmount for it is 0
        $scope.orderInvoceAmountNotExpanded = function (orderId, expanded) {
            var orders = $scope.Model;
            for (var i = 0; i < orders.length; i++) {

                if (orders[i].Id == orderId) {
                    if (orders[i].expanded == expanded) {

                        orders[i].InvoiceAmount = 0
                    }
                }
            }

        }

        $scope.OrderamountNotchecked = function (orderId, orderExpanded, orderitemId, OrderItemExpanded) {
            var orders = $scope.Model;
            var orderItems = $scope.OrderitemModel[orderId];

            for (var i = 0; i < orders.length; i++) {

                if (orders[i].Id == orderId) {

                    if (orders[i].expanded == orderExpanded) {

                        for (var j = 0; j < orderItems.length; j++) {


                            if (orderItems[j].Id == orderitemId) {
                                if (orderItems[j].expanded == OrderItemExpanded && orderItems[j].PaymentplanType != 'Installment' && orderItems[j].Balance > 0) {



                                    orderItems[j].OriginalBalance = orderItems[j].InvoiceAmount;

                                    orders[i].InvoiceAmount -= orderItems[j].OriginalBalance;

                                    orderItems[j].InvoiceAmount = 0;

                                }

                            }

                            if (orderItems[j].expanded == OrderItemExpanded && orderItems[j].PaymentplanType == 'Installment' && orderItems[j].Balance > 0) {

                                if (orderItems[j].Id == orderitemId) {
                                    orderItems[j].OriginalBalance = orderItems[j].InvoiceAmount;

                                    orders[i].InvoiceAmount -= orderItems[j].OriginalBalance;
                                    orderItems[j].InvoiceAmount = 0;
                                    for (var k = 0; k < orderItems[j].Installments.length; k++) {

                                        orderItems[j].Installments[k].expanded = true;
                                        orderItems[j].Installments[k].InvoiceAmount = 0;

                                        orderItems[j].expanded = true;

                                    }

                                }
                            }
                        }

                        var count = 0
                        for (var e = 0; e < orderItems.length; e++) {

                            if (orderItems[e].Checked) {
                                count++;
                            }
                        }
                        if (count > 0) {
                            continue
                        } else {
                            orders[i].expanded = false

                        }

                    }

                }

            }

        }


        $scope.OrderamountWithSumOrderitemAmount = function (orderId, orderExpanded, orderitemId, OrderItemExpanded) {
            var orders = $scope.Model;
            var orderItems = $scope.OrderitemModel[orderId];
            for (var i = 0; i < orders.length; i++) {

                if (orders[i].Id == orderId) {

                    if (orders[i].expanded == orderExpanded) {

                        for (var j = 0; j < orderItems.length; j++) {


                            if (orderItems[j].expanded == OrderItemExpanded && orderItems[j].PaymentplanType != 'Installment' && orderItems[j].Balance > 0) {

                                if (orderItems[j].Id == orderitemId) {

                                    orderItems[j].OriginalBalance = orderItems[j].Balance;

                                    orders[i].InvoiceAmount += orderItems[j].OriginalBalance;
                                    orderItems[j].InvoiceAmount = orderItems[j].Balance;
                                }

                            }

                            if (orderItems[j].expanded == OrderItemExpanded && orderItems[j].PaymentplanType == 'Installment' && orderItems[j].Balance > 0) {

                                if (orderItems[j].Id == orderitemId) {

                                    orderItems[j].OriginalBalance = orderItems[j].Balance;

                                    orders[i].InvoiceAmount += orderItems[j].OriginalBalance;

                                    orderItems[j].InvoiceAmount = orderItems[j].Balance;

                                    for (var k = 0; k < orderItems[j].Installments.length; k++) {

                                        orderItems[j].Installments[k].expanded = false;
                                        orderItems[j].Installments[k].Checked = true;

                                        orderItems[j].Installments[k].InvoiceAmount = orderItems[j].Installments[k].Balance;

                                    }
                                }



                            }

                        }

                    }

                }

            }
        }


        $scope.OrderItemAmounWithInstalmentNotChecked = function (orderId, orderExpanded, orderitemId, OrderItemExpanded, InstalmentId, InstalmentExpanded) {
            var orders = $scope.Model;
            var orderItems = $scope.OrderitemModel[orderId];

            for (var i = 0; i < orders.length; i++) {

                if (orders[i].Id == orderId) {

                    if (orders[i].expanded == orderExpanded) {

                        for (var j = 0; j < orderItems.length; j++) {

                            if (orderItems[j].Id == orderitemId) {

                                if (orderItems[j].expanded == OrderItemExpanded || orderItems[j].expanded == 'undefined') {

                                    for (var k = 0; k < orderItems[j].Installments.length; k++) {

                                        if (orderItems[j].Installments[k].id == InstalmentId) {

                                            if (orderItems[j].Installments[k].expanded == InstalmentExpanded) {

                                                orderItems[j].Installments[k].OriginalBalance = orderItems[j].Installments[k].Balance;

                                                orderItems[j].InvoiceAmount -= orderItems[j].Installments[k].OriginalBalance;

                                                orders[i].InvoiceAmount -= orderItems[j].Installments[k].OriginalBalance;
                                                orderItems[j].Installments[k].InvoiceAmount = 0

                                            }
                                        }

                                    }
                                    var countinst = 0
                                    for (var d = 0; d < orderItems[j].Installments.length; d++) {

                                        if (orderItems[j].Installments[d].Checked == true && orderItems[j].Installments[d].Balance > 0) {
                                            countinst++;
                                        }

                                    }
                                    if (countinst > 0) {

                                        continue
                                    }
                                    else {
                                        orderItems[j].expanded = true
                                        orderItems[j].Checked = false
                                    }
                                }

                            }

                        }
                        var count = 0
                        for (var e = 0; e < orderItems.length; e++) {
                            if (orderItems[e].Checked == true && orderItems[e].Balance > 0) {

                                count++;
                            }

                        }
                        if (count > 0) {
                            continue
                        } else {
                            orders[i].expanded = false

                        }
                    }

                }

            }
        }


        $scope.OrderItemAmounWithInstalmentChecked = function (orderId, orderExpanded, orderitemId, OrderItemExpanded, InstalmentId, InstalmentExpanded) {

            var orders = $scope.Model;
            var orderItems = $scope.OrderitemModel[orderId];

            for (var i = 0; i < orders.length; i++) {

                if (orders[i].Id == orderId) {

                    if (orders[i].expanded == orderExpanded) {

                        for (var j = 0; j < orderItems.length; j++) {

                            if (orderItems[j].Id == orderitemId) {

                                if (orderItems[j].expanded == OrderItemExpanded || orderItems[j].expanded == 'undefined') {

                                    for (var k = 0; k < orderItems[j].Installments.length; k++) {

                                        if (orderItems[j].Installments[k].id == InstalmentId) {

                                            if (orderItems[j].Installments[k].expanded == InstalmentExpanded) {

                                                orderItems[j].Installments[k].OriginalBalance = orderItems[j].Installments[k].Balance;

                                                orderItems[j].InvoiceAmount += orderItems[j].Installments[k].OriginalBalance;

                                                orders[i].InvoiceAmount += orderItems[j].Installments[k].OriginalBalance;
                                                orderItems[j].Installments[k].Checked = true;
                                                orderItems[j].Installments[k].InvoiceAmount = orderItems[j].Installments[k].Balance;
                                                orderItems[j].expanded = false;


                                            }
                                        }

                                    }
                                }

                            }

                        }

                    }

                }

            }

        }


        $scope.OrderItemExpanded = function (orderId, orderExpanded, orderitemId, orderItemChecked) {
            var orders = $scope.Model;
            var orderItems = $scope.OrderitemModel[orderId];

            for (var i = 0; i < orders.length; i++) {
                if (orders[i].Id == orderId) {

                    if (orders[i].expanded == orderExpanded) {

                        for (var j = 0; j < orderItems.length; j++) {

                            if (orderItems[j].Id == orderitemId) {

                                var count = 0
                                for (var e = 0; e < orderItems.length; e++) {

                                    if (orderItems[e].expanded == false) {

                                        count++;
                                    }

                                }
                                if (count > 0) {
                                    continue
                                } else {
                                    orders[i].expanded = false

                                }
                            }

                        }

                    }

                }

            }


        }


        $scope.back = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("FamilyDetail", { userId: $stateParams.userId, clubId: $stateParams.clubId });
                }
            }
            else {
                $state.go("FamilyDetail", { userId: $stateParams.userId, clubId: $stateParams.clubId });
            }
        }

        if (returnedByBack) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();

