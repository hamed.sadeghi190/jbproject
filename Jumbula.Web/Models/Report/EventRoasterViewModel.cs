﻿using System.Reflection;
using System.Data;
using System.EnterpriseServices;
using Jb.Framework.Web.Model;

namespace SportsClub.Models
{
    [JavasciptViewModel]
    public class EventRoasterViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}