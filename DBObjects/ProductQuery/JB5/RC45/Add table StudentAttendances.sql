SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StudentAttendances](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderSessionId] [bigint] NOT NULL,
	[AttendanceStaffId] [int] NULL,
	[AttendanceStatus] [int] NULL,
	[AttendanceDate] [datetime] NULL,
	[DismissalStaffId] [int] NULL,
	[IsDismissed] [bit] NOT NULL,
	[DismissalInfo] [nvarchar](4000) NULL,
	[DismissalDate] [datetime] NULL,
 CONSTRAINT [PK_dbo.StudentAttendances] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[StudentAttendances]  WITH CHECK ADD  CONSTRAINT [FK_dbo.StudentAttendances_dbo.ClubStaffs_AttendanceStaffId] FOREIGN KEY([AttendanceStaffId])
REFERENCES [dbo].[ClubStaffs] ([Id])
GO

ALTER TABLE [dbo].[StudentAttendances] CHECK CONSTRAINT [FK_dbo.StudentAttendances_dbo.ClubStaffs_AttendanceStaffId]
GO

ALTER TABLE [dbo].[StudentAttendances]  WITH CHECK ADD  CONSTRAINT [FK_dbo.StudentAttendances_dbo.ClubStaffs_DismissalStaffId] FOREIGN KEY([DismissalStaffId])
REFERENCES [dbo].[ClubStaffs] ([Id])
GO

ALTER TABLE [dbo].[StudentAttendances] CHECK CONSTRAINT [FK_dbo.StudentAttendances_dbo.ClubStaffs_DismissalStaffId]
GO

ALTER TABLE [dbo].[StudentAttendances]  WITH CHECK ADD  CONSTRAINT [FK_dbo.StudentAttendances_dbo.OrderSessions_OrderSessionId] FOREIGN KEY([OrderSessionId])
REFERENCES [dbo].[OrderSessions] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[StudentAttendances] CHECK CONSTRAINT [FK_dbo.StudentAttendances_dbo.OrderSessions_OrderSessionId]
GO