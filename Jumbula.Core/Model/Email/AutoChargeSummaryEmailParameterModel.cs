﻿using System;
using System.Collections.Generic;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeSummaryEmailParameterModel
    {
        public AutoChargeSummaryEmailParameterModel(int clubId,DateTime date)
        {
            ClubId = clubId;
            Date = date;
        }

        public int ClubId { get; set; }
        public DateTime Date { get; set; }
        public List<Tuple<long, long>> SuccessInstallments { get; set; } = new List<Tuple<long, long>>();
        public List<Tuple<long, long>> FailedInstallments { get; set; } = new List<Tuple<long, long>>();

    }
}
