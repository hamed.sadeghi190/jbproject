﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('supplementalFormViewController', ['$scope', '$http', 'cfpLoadingBar', '$stateParams', '$state', 'jbToast', function ($scope, $http, cfpLoadingBar, $stateParams, $state, jbToast) {
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + '/Dashboard/Club';


        $http.get($scope.baseUrl + '/GetSupplementalFormViewDetail?formId=' + $stateParams.formId).success(function (data) {
            $scope.Model = data;
        }).error(function (data) {
            jbToast.error(data);
        });

    }]);
})();