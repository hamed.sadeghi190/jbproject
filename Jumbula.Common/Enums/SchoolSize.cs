﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum SchoolSize
    {
        [Description("0-100")]
        Size1,
        [Description("101-300")]
        Size2,
        [Description("301-500")]
        Size3,
        [Description("501-700")]
        Size4,
        [Description("701-900")]
        Size5,
        [Description("901 or higher")]
        Size6
    }

}
