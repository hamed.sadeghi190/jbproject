
alter table ProgramSessions drop CONSTRAINT  [FK_dbo.ProgramSessions_dbo.ProgramScheduleParts_ProgramSchedulePartId]

GO

--ALTER TABLE ProgramSessions drop DF__ProgramSe__Progr__288DEB75

ALTER TABLE ProgramSessions ALTER COLUMN ProgramSchedulePartId int NULL

Go

ALTER TABLE [dbo].[ProgramSessions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramSessions_dbo.ProgramScheduleParts_ProgramSchedulePartId] FOREIGN KEY([ProgramSchedulePartId])
REFERENCES [dbo].[ProgramScheduleParts] ([Id])
GO

ALTER TABLE ProgramSessions ADD  ProgramScheduleId bigint NULL
Go
ALTER TABLE [dbo].[ProgramSessions]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramSessions_dbo.ProgramSchedules_ProgramScheduleId] FOREIGN KEY([ProgramScheduleId])
REFERENCES [dbo].[ProgramSchedules] ([Id])
ON DELETE CASCADE