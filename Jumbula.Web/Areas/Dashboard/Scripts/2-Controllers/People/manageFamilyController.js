﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('manageFamilyController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast ) {
        window.app.globalObjects.pageClass = 'family-manage-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/People';

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });

        $scope.MC.stateParams = $stateParams;

        $http.get(baseUrl + '/FamilyDetails/?userId=' + $scope.MC.stateParams.userId + '&clubId=' + $scope.MC.stateParams.clubId)
       .success(function (data, status, headers, config) {
           $scope.Model = data;
       });

        $scope.saveFamily = function () {
            $http.post(baseUrl + '/SubmitManageFamily', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $state.go('FamilyDetail', { userId: $scope.MC.stateParams.userId + '&clubId=' + $scope.MC.stateParams.clubId})
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        }
    
    }]);
})();