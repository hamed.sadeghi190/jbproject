﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AddToWaitListAdminViewModel : BaseViewModel
    {
        public string ProgramName { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone number")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string PhoneNumber { get; set; }

        public List<SelectKeyValue<string>> GradeItems { get; set; }

        [Display(Name = "Grade")]
        [Required(ErrorMessage = "{0} is required.")]
        public SchoolGradeType? SelectedGrade { get; set; }
        public string WaitlistPolicy { get; set; }

        public int UserId { get; set; }

        public List<ScheduleViewModel> Schedules { get; set; }
        public string Message { get; set; }

        public ProgramTypeCategory ProgramType { get; set; }

        [Display(Name = "Schedule")]
        [Required(ErrorMessage = "{0} is required.")]
        public long? scheduleId { get; set; }
    }
}