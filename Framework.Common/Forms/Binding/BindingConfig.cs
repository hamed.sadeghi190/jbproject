﻿using System.Collections.Generic;

namespace Jb.Framework.Common.Forms.Binding
{
    public class BindingConfig
    {
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public List<BindingMapElement> MapElements { get; set; }
    }
}
