﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Jumbula.Core.Model.Contact
{
    public class ContactEditViewModel : ContactCreateViewModel
    {
        public ContactEditViewModel()
        {
        }

        public int Id { get; set; }



        [StringLength(64)]
        public string Nickname { get; set; }

        public bool ShowNickname { get; set; }

        [Display(Name = "Cell phone")]
        [RegularExpression("[0-9]+$",
               ErrorMessage = "Please enter the cell phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string CellPhone { get; set; }

        public bool ShowCellPhone { get; set; }

        [Display(Name = "Work phone")]
        [RegularExpression("[0-9]+$",
              ErrorMessage = "Please enter the work phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete cell phone number including area code.")]
        public string WorkPhone { get; set; }

        public bool ShowWorkPhone { get; set; }


        [Display(Name = "Date of birth")]
        [DataType(DataType.Date, ErrorMessage = "{0} is not a valid date.")]
        public DateTime? DoB { get; set; }

        public bool ShowDoB { get; set; }

        public string Title { get; set; }

        public bool ShowTitle { get; set; }

        public List<SelectListItem> StaffTitles { get; set; }

        public int? OrganizationId { get; set; }

        public List<SelectListItem> Organizations { get; set; }

        public bool IsStaff { get; set; }

        public bool IsAssignable { get; set; }

        public bool IsPrimary { get; set; }

        public bool ShowPhoneNumbersSection
        {
            get
            {
                return ShowCellPhone || ShowWorkPhone || ShowPhone;
            }

        }

        public bool ShowAdditionalInfoSection
        {
            get
            {
                return ShowDoB;
            }

        }
    }
}
