﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class ClubType : BaseEntity
    {
        public string Name { get; set; }
        [ForeignKey("ParentType")]
        public int? ParentId { get; set; }
        public virtual ClubType ParentType { get; set; }
        public ClubTypesEnum EnumType { get; set; }

        [NotMapped]
        public string ParentName
        {
            get
            {
                if (ParentType == null && !ParentId.HasValue)
                {
                    return "";
                }
                return ParentType.Name;
            }
        }
    }
}
