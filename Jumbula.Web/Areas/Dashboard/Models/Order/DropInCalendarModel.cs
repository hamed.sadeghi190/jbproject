﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SportsClub.Models
{
    public class DropInCalendarModel : ISchedulerEvent
    {
        public DropInCalendarModel()
        {
        }

        public int Id { get; set; }

        public long ProgramId { get; set; }

        public int TaskID { get; set; }
        public string Title { get; set; }
        public string Description2 { get; set; }

        //private DateTime start;
        public DateTime Start { get; set; }

        //private DateTime end;
        public DateTime End { get; set; }

        public DateTime DoDate { get; set; }

        public string RecurrenceRule { get; set; }
        public int? RecurrenceID { get; set; }
        public string RecurrenceException { get; set; }
        public bool IsAllDay { get; set; }

        public string EndTimezone { get; set; }
        public string StartTimezone { get; set; }
        public long CurrentSessionId { get; set; }


        //public SelectList CapacityList { get; set; }
        public bool Selected { get; set; }
        public long ScheduleId { get; set; }
        public string ScheduleTitle { get; set; }
        public string ChargeCategory { get; set; }
        public long ChargeId { get; set; }
        public string ChargeName { get; set; }
        public decimal Amount { get; set; }
        public CalendarOrderItemViewModel OrderCalendar { get; set; }
        public DateTime StartProgramDate { get; set; }

        public DateTime EndProgramDate { get; set; }

        public bool IsChecked { get; set; }
        public bool IsOrderSession { get; set; }

        public string Color
        {
            get
            {
                if (!this.IsOrderSession && this.IsChecked)
                {
                    return "#e27e08";
                }
                else
                {
                    return "#004881";
                }
            }
        }
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string ProgramDomain { get; set; }

        bool _isFull;
        
        public bool IsFull
        {
            get
            {
                return _isFull;

            }

            set
            {
                _isFull = value;
            }
        }

       

        public string Description
        {
            get;
            set;
        }
    }

    public class CalendarOrderItemViewModel
    {
        public long Value { get; set; }

        public string Text { get; set; }

        //public string Color { get; set; }
    }
}