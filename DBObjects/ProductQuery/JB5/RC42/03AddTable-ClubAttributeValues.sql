
/****** Object:  Table [dbo].[ClubAttributeValues]    Script Date: 11/1/2017 2:06:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ClubAttributeValues](
	[ClubId] [int] NOT NULL,
	[AttributeId] [int] NOT NULL,
	[Value] [nvarchar](max) NULL,
	[AttributeOptionId] [int] NULL,
 CONSTRAINT [PK_dbo.ClubAttributeValues] PRIMARY KEY CLUSTERED 
(
	[ClubId] ASC,
	[AttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClubAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubAttributeValues_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
GO

ALTER TABLE [dbo].[ClubAttributeValues] CHECK CONSTRAINT [FK_dbo.ClubAttributeValues_dbo.Clubs_ClubId]
GO

ALTER TABLE [dbo].[ClubAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubAttributeValues_dbo.JbAttributeOptions_AttributeOptionId] FOREIGN KEY([AttributeOptionId])
REFERENCES [dbo].[JbAttributeOptions] ([Id])
GO

ALTER TABLE [dbo].[ClubAttributeValues] CHECK CONSTRAINT [FK_dbo.ClubAttributeValues_dbo.JbAttributeOptions_AttributeOptionId]
GO

ALTER TABLE [dbo].[ClubAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubAttributeValues_dbo.JbAttributes_AttributeId] FOREIGN KEY([AttributeId])
REFERENCES [dbo].[JbAttributes] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClubAttributeValues] CHECK CONSTRAINT [FK_dbo.ClubAttributeValues_dbo.JbAttributes_AttributeId]
GO


