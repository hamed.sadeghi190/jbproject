/****** Object:  Table [dbo].[Counties]    Script Date: 3/3/2016 6:08:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Counties](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](64) NULL,
	[StateId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Counties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Counties]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Counties_dbo.States_StateId] FOREIGN KEY([StateId])
REFERENCES [dbo].[States] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Counties] CHECK CONSTRAINT [FK_dbo.Counties_dbo.States_StateId]
GO





/****** Object:  Table [dbo].[CatalogSettingCounties]    Script Date: 3/3/2016 6:10:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CatalogSettingCounties](
	[CatalogSettingId] [int] NOT NULL,
	[CountyId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.CatalogSettingCounties] PRIMARY KEY CLUSTERED 
(
	[CatalogSettingId] ASC,
	[CountyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CatalogSettingCounties]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogSettingCounties_dbo.CatalogSettings_CatalogSettingId] FOREIGN KEY([CatalogSettingId])
REFERENCES [dbo].[CatalogSettings] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CatalogSettingCounties] CHECK CONSTRAINT [FK_dbo.CatalogSettingCounties_dbo.CatalogSettings_CatalogSettingId]
GO

ALTER TABLE [dbo].[CatalogSettingCounties]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CatalogSettingCounties_dbo.Counties_CountyId] FOREIGN KEY([CountyId])
REFERENCES [dbo].[Counties] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CatalogSettingCounties] CHECK CONSTRAINT [FK_dbo.CatalogSettingCounties_dbo.Counties_CountyId]
GO


