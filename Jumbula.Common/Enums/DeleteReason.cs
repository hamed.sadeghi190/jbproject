﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Common.Enums
{
    public enum DeleteReason : byte
    {
        [Description("Normal Delete")]
        NormalDelete = 0,
        [Description("Session delete")]
        SessionDelete = 1,
        [Description("Cancel")]
        Cancel = 2,
        [Description("Transfer")]
        Transfer = 3,
        [Description("Change desired start date")]
        ChangeDesiredStartDate = 4
    }
}
