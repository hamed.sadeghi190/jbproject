﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using OfficeOpenXml;

namespace Jumbula.Web.Infrastructure.DataMigration
{
    public interface IDataManipulationService
    {
        List<Core.Domain.DataMigration> GetList();
        Core.Domain.DataMigration Get(int id);
        List<Core.Domain.DataMigration> GetByName(string name);
        bool Create(Core.Domain.DataMigration dataMigration);
        bool Edit(Core.Domain.DataMigration dataMigration);
        bool SaveSource(Stream stream, int id);
        DataManipulationGeneralResult Manipulate(int id, int? from = null, int? endPoint = null);
        void SetControllerContext(ControllerContext controllerContext);
        List<IDataManipulation> GetDataManipulationClasses();
        IDataManipulation GetDataManipulationClass(string name);
        List<Core.Domain.DataMigration> GetResults(string name);
        int GetIdOfLastItem(string name);
        DataManipulationLogResult RunReport(int id, string token = null);



        string GetWrongMedicalElements(bool rasOnly, int from = 1, int to = int.MaxValue);
        string GetWrongMedicalNutElements(bool rasOnly, int from = 1, int to = int.MaxValue);
        string GetWrongSchoolElements(int from = 1, int to = int.MaxValue);

        string GetWrongParent1Elements(bool rasOnly, int from = 1, int to = int.MaxValue);
        string GetWrongParent2Elements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetWrongEmergencyElements(bool rasOnly, int from = 1, int to = int.MaxValue);
        string GetWrongAuthorizePickup1Elements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetWrongAuthorizePickup2Elements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetWrongAuthorizePickup3Elements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetWrongAuthorizePickup4Elements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetWrongMedical2Elements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetWrongInsuranceElements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetWrongPermissionPhotographyElements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetWrongSchool2Elements(bool rasOnly, int from = 1, int to = int.MaxValue);

        string GetAllEmFormElements();
    }

    public interface IDataManipulation
    {
        string Name { get; }
        string Title { get; }
        bool HasSource { get; set; }
        int? From { get; set; }

        int? EndPoint { get; set; }

        int? ContinueFrom { get; }

        int? AllItems { get; }

        int? NumbersInCycle { get; }

        bool IsRunning { get; set; }

        DataManipulationGeneralResult Operate();

        DataManipulationGeneralResult Reslult { get; }

        IEntitiesContext DataContext { get; set; }

        IApplicationUserManager<JbUser, int> UserManager { get; set; }

        int GetNumberOfElements();

        void FinalizeM(bool save = true);

        void Stop();
    }

    public abstract class DataManipulationSourceItemBaseModel
    {
        public int Index { get; set; }
    }

    public class DataManipulationSimpleSourceItemModel : DataManipulationSourceItemBaseModel
    {

    }


    public abstract class DataManipulationBaseModel<TItem> : IDataManipulation
        where TItem : DataManipulationSourceItemBaseModel
    {
        public DataManipulationBaseModel()
        {
            _result = new DataManipulationGeneralResult();

            SourceItems = new List<TItem>();
        }

        public List<TItem> SourceItems { get; set; }

        private void ConvertSourceExcelFileToSourceObject()
        {
            var plainExcelRows = GetExcelRows(MvcApplication.DataManipulationSourceFile);

            if (plainExcelRows == null) return;

            foreach (var row in plainExcelRows.Skip(1))
            {
                try
                {
                    var item = (TItem)Activator.CreateInstance(typeof(TItem));

                    item.Index = row.Line;

                    foreach (var cell in row.Cells)
                    {
                        string value = cell.Value;

                        PropertyInfo propertyInfo = item.GetType().GetProperty(cell.Key);

                        if (propertyInfo != null)
                        {
                            propertyInfo.SetValue(item, Convert.ChangeType(value, propertyInfo.PropertyType), null);
                        }

                    }
                    SourceItems.Add(item);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error in converting line {row.Line}");
                }
            }
        }

        private List<DataMigrationPlainRowItem> GetExcelRows(byte[] file)
        {
            if (file == null) return null;

            try
            {
                var result = new List<DataMigrationPlainRowItem>();
                var titles = new List<string>();
                var ms = new MemoryStream(file);

                using (var xlPackeage = new ExcelPackage(ms))
                {
                    ExcelWorksheet worksheet = xlPackeage.Workbook.Worksheets[1];
                    int rowCount = worksheet.Dimension.End.Row;
                    int columnCount = worksheet.Dimension.End.Column;

                    for (int i = 1; i <= columnCount; i++)
                    {
                        var cell = worksheet.Cells[1, i].Value != null ? FixSourceString(worksheet.Cells[1, i].Value.ToString()) : string.Empty;
                        titles.Add(cell);
                    }

                    for (int i = 1; i <= rowCount; i++)
                    {
                        Dictionary<string, string> cells = new Dictionary<string, string>();
                        var dataMigrationItem = new DataMigrationPlainRowItem();

                        for (int j = 1; j <= columnCount; j++)
                        {
                            try
                            {
                                var cell = worksheet.Cells[i, j].Value != null ? FixSourceString(worksheet.Cells[i, j].Value.ToString()) : string.Empty;
                                cells.Add(titles[j - 1], cell);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(string.Format("Cell: {0}", j));
                            }
                        }

                        dataMigrationItem.Line = i;
                        dataMigrationItem.Cells = cells;
                        result.Add(dataMigrationItem);

                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in converting excell file to plain rows");
            }
        }

        private string FixSourceString(string str)
        {
            if (str == null)
            {
                str = string.Empty;
            }

            var result = str;

            result = result.Trim();

            result = result.Replace(Environment.NewLine, " ");
            result = Regex.Replace(result, @"\r\n?|\n", " ");
            result = result.Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ");

            return result;
        }

        public int? EndPoint { get; set; }

        public int? ContinueFrom
        {
            get
            {
                int? result = null;

                if (!NumbersInCycle.HasValue)
                {
                    return null;
                }
                else
                {
                    if (From.HasValue)
                    {
                        result = To + 1;
                    }
                }

                if (result > AllItems)
                {
                    result = null;
                }

                return result;
            }
        }

        int? from;
        public virtual int? From
        {
            get
            {
                return from;
            }
            set
            {
                from = value.HasValue ? value : 1;
            }
        }

        public int? To
        {
            get
            {
                if (!NumbersInCycle.HasValue)
                {
                    return null;
                }

                var to = From + (NumbersInCycle - 1);

                if (to > allItems)
                {
                    to = allItems;
                }

                return to;
            }
        }


        public virtual int? NumbersInCycle
        {
            get
            {
                return null;
            }
        }

        public bool HasSource { get; set; }

        public string Name
        {
            get
            {
                return this.ToString();
            }
        }



        public virtual string Title
        {
            get
            {
                return Name;
            }
        }

        DataManipulationGeneralResult _result;
        public DataManipulationGeneralResult Reslult
        {
            get
            {
                var result = _result;

                var endPoint = allItems;

                if (EndPoint.HasValue)
                {
                    endPoint = EndPoint.Value;
                }

                result.ContinueFrom = this.ContinueFrom;

                if (!To.HasValue)
                {
                    result.Status = DataManipulationResultStatus.Success;

                    result.Message = string.Format("All {0} elements done successfully.", endPoint);

                    return result;
                }

                if (To <= endPoint)
                {
                    result.Message = string.Format("Elements from {0} to {1} done.", From, To);
                }

                if (To >= endPoint)
                {
                    result.Status = DataManipulationResultStatus.Success;
                }
                else
                {
                    result.Status = DataManipulationResultStatus.HalFinished;
                }

                return result;
            }

        }

        int? allItems;
        public int? AllItems
        {
            get
            {
                return allItems;
            }
        }

        public IEntitiesContext DataContext { get; set; }

        public IApplicationUserManager<JbUser, int> UserManager { get; set; }

        public bool IsRunning
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public abstract DataManipulationGeneralResult Operate();

        protected void Initialize(int allItems)
        {
            SetAllItems(allItems);

            InitializeLog();
        }

        protected void FillSource()
        {
            ConvertSourceExcelFileToSourceObject();
        }

        private void InitializeLog()
        {
            var log = new DataManipulationLog()
            {
                Items = new List<DataManipulationLogItem>(),
            };

            var token = Guid.NewGuid().ToString();
            log.Id = token;
            //log..Token = token;
            log.Title = string.Format("Elements from {0} to {1}", this.From, this.To);

            this.Reslult.SetLog(log);
            this.Reslult.SetToken(token);
        }

        private void SetAllItems(int allItems)
        {
            this.allItems = allItems;
        }

        protected void Log(string text)
        {
            var item = new DataManipulationLogItem(text);
            this.Reslult.Log.Items.Add(item);
        }

        public void FinalizeM(bool save = true)
        {
            var saveResult = 0;

            //Log("Save chages started.");
            if (save)
            {
                saveResult = Save();
            }

          //  Log("Save chages finished.");

            this.Reslult.Log.RecordAffected = saveResult;
        }

        private int Save()
        {
            var result = 0;

            try
            {
                result = DataContext.SaveChanges();

                Log(string.Format("Successfully saved chages and {0} record affected.", result));

                return result;
            }
            catch (DbEntityValidationException e)
            {
                var validationMsessages = new List<string>();

                foreach (var eve in e.EntityValidationErrors)
                {
                    validationMsessages.Add(getValidationMessageFromEntityValidationErro(eve));
                }

                Log("************** Error in saving changes to db **************");

                foreach (var item in validationMsessages)
                {
                    Log(item);
                }

                Log("************** End errors of save **************");

                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception("Error in saving changes to db"));
            }
            catch(Exception ex)
            {
                Log("Error in saving changes.");
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception("Error in saving changes to db."));
            }


            return result;
        }


        private string getValidationMessageFromEntityValidationErro(DbEntityValidationResult validationError)
        {
            var result = string.Empty;

            var entityId = validationError.Entry != null ? GetIdOfEntity(validationError.Entry.Entity) : string.Empty;


            result = string.Format("EntityId: {0}  ", entityId);

            result += string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        validationError.Entry.Entity.GetType().Name, validationError.Entry.State);

            foreach (var ve in validationError.ValidationErrors)
            {
                result += string.Format("-- Property: \"{0}\", Error: \"{1}\"",
                     ve.PropertyName, ve.ErrorMessage);
            }

            return result;
        }

        private string GetIdOfEntity(object src)
        {
            try
            {
                var propName = "Id";
                return src.GetType().GetProperty(propName).GetValue(src, null).ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        public abstract int GetNumberOfElements();

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }

    // For save as json
    public class DataManipulationLogResult
    {
        public DataManipulationLogResult()
        {
            Items = new List<DataManipulationLog>();
        }

        public List<DataManipulationLog> Items { get; set; }
    }

    public class DataManipulationLog
    {
        public string Id { get; set; }

        public int RecordAffected { get; set; }

        public string Title { get; set; }

        public List<DataManipulationLogItem> Items { get; set; }
    }

    public class DataManipulationLogItem
    {
        public DataManipulationLogItem(string text)
        {
            Text = text;
        }

        public string Text { get; set; }
    }

    public class DataManipulationGeneralResult
    {
        public void SetLog(DataManipulationLog log)
        {
            Log = log;
        }

        public void SetToken(string token)
        {
            Token = token;
        }

        public bool HasError { get; set; }

        public DataManipulationResultStatus Status { get; set; }

        public string Message { get; set; }

        public int? ContinueFrom { get; set; }

        public DataManipulationLog Log { get; set; }

        public string Token { get; set; }


    }

    public static class StaticValueClass
    {
        public static Queue<int> QueuedTasks;
        public static List<string> StaticValues { get; set; }

        public static void Initialize()
        {
            if (StaticValues == null)
            {
                StaticValues = new List<string>();
            }
        }

        public static void Reset()
        {
            StaticValues = new List<string>();
        }

        public static void Add(string value)
        {
            Initialize();
            StaticValues.Add(value);
        }

        public static bool IsExist(string value)
        {
            Initialize();
            return StaticValues.Contains(value);
        }
    }

    public enum DataManipulationResultStatus
    {
        HalFinished,
        Success,
    }

    public class DataMigrationPlainRowItem
    {
        public DataMigrationPlainRowItem()
        {
            Cells = new Dictionary<string, string>();
        }
        public int Line { get; set; }
        public Dictionary<string, string> Cells { get; set; }
    }

}