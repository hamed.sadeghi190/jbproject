
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Emails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Body] [nvarchar](max) NULL,
	[Sent] [bit] NOT NULL,
	[Subject] [nvarchar](max) NULL,
	[DateCreated] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[DateSent] [datetime] NULL,
	[SendToSupport] [bit] NOT NULL DEFAULT ((0)),
	[Delivered] [bit] NOT NULL DEFAULT ((0)),
	[EmailType] [tinyint] NOT NULL DEFAULT ((0)),
	[DeliverDate] [datetime] NULL,
	[From] [nvarchar](max) NULL,
	[FromName] [nvarchar](max) NULL,
	[Recipients] [nvarchar](max) NULL,
	[ReplyTo] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Emails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO