﻿using Autofac;
using Jumbula.Business.ReportBuilders;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Bootstrapper.Modules
{
    public class ReportBusinessModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MemberListReportBuilderBusiness>().As<IReportBuilderBusiness>().Keyed<IReportBuilderBusiness>(ReportName.MemberList).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType<DailyDismissalReportBuilderBusiness>().As<IReportBuilderBusiness>().Keyed<IReportBuilderBusiness>(ReportName.DailyDismissal).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType<InstallmentPortalReportBuilderBusiness>().As<IReportBuilderBusiness>().Keyed<IReportBuilderBusiness>(ReportName.InstallmentPortal).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterType<CampRosterPortalReportBuilderBusiness>().As<IReportBuilderBusiness>().Keyed<IReportBuilderBusiness>(ReportName.CampRosterPortal).InstancePerRequest().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
        }
    }

}
