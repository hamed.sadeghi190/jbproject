﻿(function () {
    'use strict';

    angular.module('jbTools').service("informationInvoiceService", [function () {

        var _model;

        function setInfoModel(Data) {
            _model = Data;
        }

        function getInfoModel() {
            return _model;
        }

        return {
            set: setInfoModel,
            get: getInfoModel
        }
    }]);
})();