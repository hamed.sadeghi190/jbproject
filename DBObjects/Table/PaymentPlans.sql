
IF OBJECT_ID('[dbo].[PaymentPlans]') IS NULL
BEGIN
CREATE TABLE [dbo].[PaymentPlans](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MetaData_DateCreated] [datetime] NOT NULL,
	[MetaData_DateUpdated] [datetime] NOT NULL,
	[PaymentPlanType] [int] NOT NULL,
	[Camp_Id] [int] NULL,
	[Course_Id] [int] NULL,
	[Configuration] [xml] NULL,
	[Tourney_Id] [int] NULL,
	[TTTourney_Id] [int] NULL)
END
GO


--<< ADD PRIMARY KEYS >>--
IF NOT EXISTS(SELECT 1 FROM sys.objects where name='PK_dbo.PaymentPlans') 
BEGIN
	ALTER TABLE [PaymentPlans] 
	ADD CONSTRAINT [PK_dbo.PaymentPlans] PRIMARY KEY CLUSTERED(id desc)
END

--<< ADD COLUMN >>--
IF NOT EXISTS(SELECT 1 FROM sys.columns  where object_id=OBJECT_ID('[dbo].[PaymentPlans]') AND name='Event_Id') 
BEGIN
ALTER TABLE [dbo].[PaymentPlans] ADD [Event_Id] int null;
END


--<< ADD FOREIGN KEYS >>--
IF NOT EXISTS(select 1 from sys.objects where name='FK_dbo.PaymentPlans_dbo.Events_Event_Id')
BEGIN
ALTER TABLE [dbo].[PaymentPlans] 
ADD CONSTRAINT [FK_dbo.PaymentPlans_dbo.Events_Event_Id] FOREIGN KEY ([Event_Id]) 
    REFERENCES [dbo].[Events] ([Id]) 
    ON DELETE CASCADE
    ON UPDATE CASCADE;

END
GO