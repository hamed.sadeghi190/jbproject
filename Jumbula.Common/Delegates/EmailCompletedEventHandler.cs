﻿using System;

namespace Jumbula.Common.Delegates
{
    public class EmailCompletedEventArgs : EventArgs
    {
        public EmailCompletedEventArgs(int emailId, Exception exp, string response = "")
        {
            EmailId = emailId;
            Exception = exp;
            Succeed = false;
            Response = response;
        }

        public EmailCompletedEventArgs(int emailId, bool success, string response = "")
        {
            EmailId = emailId;
            Succeed = success;
            Response = response;
        }

        public bool Succeed { get; set; }

        public int EmailId { get; set; }

        public string Response { get; set; }

        public Exception Exception { get; set; }

    }

    public delegate void EmailCompletedEventHandler(object sender, EmailCompletedEventArgs e);
}
