﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Jumbula.Core.Model.Contact
{
    public class ContactFilterViewModel
    {
        public string Term { get; set; }

        public string Title { get; set; }
        public ContactSearchType? SearchType { get; set; }

        public int? MemberId { get; set; }

        public List<SelectListItem> Titles { get; set; }

        public List<SelectListItem> SearchTypes { get; set; }

        public List<SelectListItem> Members { get; set; }
    }
}
