Alter table [dbo].[WaitLists]  add [IsLive] [bit]  NULL
Go

update [dbo].[WaitLists] set Islive = 1
Go

alter TABLE [dbo].[WaitLists] alter column [IsLive] [bit] Not NULL 
Go

ALTER TABLE [dbo].[WaitLists]  ADD  DEFAULT ((0)) FOR [IsLive]
Go