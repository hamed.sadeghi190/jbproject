﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CheckinReportViewModel

    {
        public long Id { get; set; }

        public string ProgramName { get; set; }

        public string SessionDate { get; set; }
        
        public string CheckinTime { get; set; }

        public string GPS { get; set; }

        public string UserName { get; set; }

        public string Address { get; set; }

        public string Distance { get; set; }

        public string CheckinDelay { get; set; }

        public string SessionTime { get; set; }
    }
}