﻿using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface IDiscountEngineRuleBusiness
    {
        void CalculateDiscount(Order order, DiscountMode discountMode);
    }
}
