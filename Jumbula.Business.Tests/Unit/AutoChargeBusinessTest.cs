﻿using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Jumbula.Common.Enums;
using Jumbula.Core.Infrastructure;
using Jumbula.Core.Model.Email;
using Jumbula.Core.Model.Payment;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class AutoChargeBusinessTest
    {
        
        private Mock<IRepository<OrderInstallment, long>> _mockOrderInstallmentRepository;
        private Mock<IClubRepository> _mockClubRepository;
        private Mock<IPaymentBusiness> _mockPaymentBusiness;
        private Mock<IOrderInstallmentBusiness> _mockInstallmentBusiness;
        private Mock<ITransactionActivityBusiness> _mockTransactionActivityBusiness;
        private Mock<IClubSettingBusiness> _mockClubSettingBusiness;
        private Mock<IEmailBusiness> _mockEmailBusiness;
        private Mock<IJbDateTime> _mockJbDateTime;
        private Mock<IJbLocker> _mockJbLocker;

        private const long InstallmentId = 1;
        private const int ClubId = 45;
        private Club _club;

        [TestMethod]
        public void Run_allDueInstallmentsIsNull_SendEmailWithZeroValue()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(new Dictionary<int, IEnumerable<long>>());

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

            //Act
            service.Run(DateTime.UtcNow);

            //Assert
            _mockEmailBusiness.Verify(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null), Times.Once);

            var totalcount = emailParameterCallBack.SuccessInstallments.Count + emailParameterCallBack.FailedInstallments.Count
                             + emailParameterCallBack.NotAttemptedInstallments.Count + emailParameterCallBack.OtherErrorInstallments.Count;

            totalcount.Should().Be(0);
        }

        [TestMethod]
        public void Run_allDueInstallmentsIsNotNull_SendEmailWithValue()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.InstallmentDate = DateTime.UtcNow;
            });

            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime>()))
                .Returns(new JbPaymentResult() { Succeed = false, Message = "This transaction has been declined" });
            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            var totalcount = emailParameterCallBack.SuccessInstallments.Count
                             + emailParameterCallBack.FailedInstallments.Count
                             + emailParameterCallBack.SystemFailedInstallments.Count
                             + emailParameterCallBack.NotAttemptedInstallments.Count
                             + emailParameterCallBack.OtherErrorInstallments.Count;

            totalcount.Should().BeGreaterThan(0);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyInstallmentBalaceIsZero_SendEmailWithValueInNotAttemptInstallmets()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };
            var dueDate = DateTime.UtcNow;

            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.Amount = 0;
                i.PaidAmount = 0;
                i.AutoChargeAttemps = 0;
                i.OrderItem.Order.ClubId = ClubId;
            });

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);

            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, dueDate))
                .Returns(new JbPaymentResult() { Succeed = false, Message = "This transaction has been declined" });

            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(installment, _club, dueDate), Times.Never);

            emailParameterCallBack.NotAttemptedInstallments.Count.Should().Be(1);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyInstallmentAutoChargeAttemptsIsZero_ChargeInstallmentCalledOnce()
        {
            //Arrange
            var service = CreateService();
            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };

            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.Amount = 100;
                i.AutoChargeAttemps = 0;
                i.OrderItem.Order.ClubId = ClubId;
            });

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);

            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = false, Message = "" });

            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()), Times.Once);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyInstallmentAutoChargeAttemptsIsOneAndAttemptCountIsTwoAndlastAutochargeTransactionIsNull_ChargeInstallmentNeverCalled()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var policy = Defultpolicy();
            policy.Attempts.ElementAt(1).Enabled = true;

            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.Amount = 100;
                i.AutoChargeAttemps = 1;
                i.OrderItem.Order.ClubId = ClubId;
            });

            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };

            _mockClubSettingBusiness.Setup(c => c.ReadAutoChargePolicy(It.IsAny<Club>())).Returns(policy);

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);

            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = false });

            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p =>
                p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(),
                    It.IsAny<Club>(), It.IsAny<DateTime?>()), Times.Never);

            emailParameterCallBack.OtherErrorInstallments.Count.Should().Be(1);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyInstallmentAutoChargeAttemptsIsTwoAndMaxAttemptIsThreeAndlastAutochargeTransactionAttemptNumberIsWrong_ChargeInstallmentNeverCalled()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var policy = Defultpolicy();
            policy.Attempts.ElementAt(1).Enabled = true;
            policy.Attempts.ElementAt(2).Enabled = true;

           

            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.Amount = 100;
                i.AutoChargeAttemps = 2;
                i.OrderItem.Order.ClubId = ClubId;
                i.TransactionActivities = new List<TransactionActivity>()
                {
                    FactoryGirl.Build<TransactionActivity>(t =>
                    {
                        t.TransactionCategory = TransactionCategory.Installment;
                        t.IsAutoCharge = true;
                        t.TransactionStatus = TransactionStatus.Failure;
                        t.InstallmentId = InstallmentId;
                        t.AutoChargePolicyAttempt = 1;
                        t.TransactionDate = new DateTime(2018, 12, 1);
                        t.PaymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
                        {
                            
                        });
                    })
                };
            });

            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };

            _mockClubSettingBusiness.Setup(c => c.ReadAutoChargePolicy(It.IsAny<Club>())).Returns(policy);

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);

            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = false });

            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

           

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(), It.IsAny<Club>(), It.IsAny<DateTime?>()), Times.Never);
            emailParameterCallBack.OtherErrorInstallments.Count.Should().Be(1);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyInstallmentAutoChargeAttemptsIsTwoAndMaxAttemptIsThreeAndlastAutochargeTransactionDateIsToday_ChargeInstallmentNeverCalled()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var policy = Defultpolicy();
            policy.Attempts.ElementAt(1).Enabled = true;
            policy.Attempts.ElementAt(2).Enabled = true;

          

             var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.Amount = 100;
                i.AutoChargeAttemps = 1;
                i.OrderItem.Order.ClubId = ClubId;
                i.TransactionActivities = new List<TransactionActivity>()
                {
                    FactoryGirl.Build<TransactionActivity>(t =>
                    {
                        t.TransactionCategory = TransactionCategory.Installment;
                        t.IsAutoCharge = true;
                        t.TransactionStatus = TransactionStatus.Failure;
                        t.InstallmentId = InstallmentId;
                        t.AutoChargePolicyAttempt = 1;
                        t.TransactionDate = DateTime.UtcNow;
                        t.PaymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
                        {

                        });

                    })
                };
            });

            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };

            _mockClubSettingBusiness.Setup(c => c.ReadAutoChargePolicy(It.IsAny<Club>())).Returns(policy);

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);


            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = false });

            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

           

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()), Times.Never);
            emailParameterCallBack.NotAttemptedInstallments.Count.Should().Be(1);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyInstallmentAutoChargeAttemptsIsTwoAndMaxAttemptIsThreeAndCurrentAttemptPolicyIsNull_ChargeInstallmentNeverCalled()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var policy = Defultpolicy();
            policy.Attempts.ElementAt(1).Enabled = true;
            policy.Attempts.ElementAt(1).AttemptOrder = 3;
            policy.Attempts.ElementAt(2).Enabled = true;
            policy.Attempts.ElementAt(2).AttemptOrder = 4;

           

            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.Amount = 100;
                i.AutoChargeAttemps = 1;
                i.OrderItem.Order.ClubId = ClubId;
                i.TransactionActivities = new List<TransactionActivity>()
                {
                    FactoryGirl.Build<TransactionActivity>(t =>
                    {
                        t.TransactionCategory = TransactionCategory.Installment;
                        t.IsAutoCharge = true;
                        t.TransactionStatus = TransactionStatus.Failure;
                        t.InstallmentId = InstallmentId;
                        t.AutoChargePolicyAttempt = 1;
                        t.TransactionDate = new DateTime(2018,12,1);})
                };
            });

            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };

            _mockClubSettingBusiness.Setup(c => c.ReadAutoChargePolicy(It.IsAny<Club>())).Returns(policy);

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);


            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = false });

            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

           

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(), It.IsAny<Club>(), It.IsAny<DateTime?>()), Times.Never);
            emailParameterCallBack.OtherErrorInstallments.Count.Should().Be(1);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyInstallmentAutoChargeAttemptsIsOneAndMaxAttemptIsThreeAndcurrentAttemptDueDateLessThanOrEqualDueDate_ChargeInstallmentCalledOnce()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var policy = Defultpolicy();
            policy.Attempts.ElementAt(1).Enabled = true;
            policy.Attempts.ElementAt(2).Enabled = true;

            var club = FactoryGirl.Build<Club>(c => c.Id = 1);

            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = 1;
                i.Amount = 100;
                i.AutoChargeAttemps = 1;
                i.OrderItem.Order.ClubId = 1;
                i.TransactionActivities = new List<TransactionActivity>()
                {
                    FactoryGirl.Build<TransactionActivity>(t =>
                    {
                        t.TransactionCategory = TransactionCategory.Installment;
                        t.IsAutoCharge = true;
                        t.TransactionStatus = TransactionStatus.Failure;
                        t.InstallmentId = 1;
                        t.AutoChargePolicyAttempt = 1;
                        t.TransactionDate = new DateTime(2018,11,18);})
                };
            });

            var installmentpair = new Dictionary<int, IEnumerable<long>> { { 1, new List<long>() { 1 } } };

            _mockClubSettingBusiness.Setup(c => c.ReadAutoChargePolicy(It.IsAny<Club>())).Returns(policy);

            _mockClubRepository.Setup(c => c.Get(It.IsAny<int>())).Returns(club);

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(It.IsAny<long>())).Returns(installment);

            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(), It.IsAny<Club>(), It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = false, Message = "unknown error" });

            _mockInstallmentBusiness.Setup(i => i.Update(It.IsAny<OrderInstallment>()));

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(), It.IsAny<Club>(), It.IsAny<DateTime?>()), Times.Once);
            var installmentTriedToCharge = emailParameterCallBack.FailedInstallments.Count +
                                           emailParameterCallBack.SystemFailedInstallments.Count +
                                           emailParameterCallBack.SuccessInstallments.Count;
            installmentTriedToCharge.Should().Be(1);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyInstallmentAutoChargeAttemptsIsOneAndMaxAttemptIsThreeAndcurrentAttemptDueDateGreaterThanDueDate_ChargeInstallmentCalledOnce()
        {
            //Arrange
            var service = CreateService();
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var policy = Defultpolicy();
            policy.Attempts.ElementAt(1).Enabled = true;
            policy.Attempts.ElementAt(2).Enabled = true;

            var club = FactoryGirl.Build<Club>(c => c.Id = 1);

            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = 1;
                i.Amount = 100;
                i.AutoChargeAttemps = 1;
                i.OrderItem.Order.ClubId = 1;
                i.TransactionActivities = new List<TransactionActivity>()
                {
                    FactoryGirl.Build<TransactionActivity>(t =>
                    {
                        t.TransactionCategory = TransactionCategory.Installment;
                        t.IsAutoCharge = true;
                        t.TransactionStatus = TransactionStatus.Failure;
                        t.InstallmentId = 1;
                        t.AutoChargePolicyAttempt = 1;
                        t.TransactionDate = DateTime.UtcNow.Date.AddDays(1);})
                };
            });

            var installmentpair = new Dictionary<int, IEnumerable<long>> { { 1, new List<long>() { 1 } } };

            _mockClubRepository.Setup(c => c.Get(It.IsAny<int>())).Returns(club);

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(It.IsAny<long>())).Returns(installment);


            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(), It.IsAny<Club>(), It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = false, Message = "unknown error" });

            _mockInstallmentBusiness.Setup(i => i.Update(It.IsAny<OrderInstallment>()));

            

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(), It.IsAny<Club>(), It.IsAny<DateTime?>()), Times.Never);
            emailParameterCallBack.NotAttemptedInstallments.Count.Should().Be(1);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyReturnTrueAndPaymentStatusIsTrue_SuccessInstallmentsCountShouldBeOne()
        {
            //Arrange
            var service = CreateService();
           
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var date = DateTime.UtcNow;
            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.Amount = 100;
                i.AutoChargeAttemps = 0;
                i.OrderItem.Order.ClubId = ClubId;
                i.TransactionActivities = new List<TransactionActivity>()
                {
                    FactoryGirl.Build<TransactionActivity>(t =>
                    {
                        t.TransactionCategory = TransactionCategory.Installment;
                        t.IsAutoCharge = true;
                        t.TransactionStatus = TransactionStatus.Failure;
                        t.InstallmentId = InstallmentId;
                        t.AutoChargePolicyAttempt = 1;
                        t.TransactionDate = date;})
                };
            });


            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };

            _mockJbDateTime.Setup(d => d.UtcNow).Returns(date);

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);

            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = true });

            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date1) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSuccess, It.IsAny<AutoChargeSuccessEmailParameterModel>(), null));

           
            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(), It.IsAny<Club>(), It.IsAny<DateTime?>()), Times.Once);
            _mockInstallmentBusiness.Verify(i => i.Update(It.IsAny<OrderInstallment>()), Times.Once);
            _mockTransactionActivityBusiness.Verify(t => t.Update(It.IsAny<TransactionActivity>()), Times.Once);
            _mockEmailBusiness.Verify(e => e.Send(EmailCategory.AutoChargeSuccess, It.IsAny<AutoChargeSuccessEmailParameterModel>(), null), Times.Once);
            emailParameterCallBack.SuccessInstallments.Count.Should().Be(1);
        }

        [TestMethod]
        public void Run_PassAutoChargePolicyReturnTrueAndPaymentStatusIsFalse_FailedInstallmentsCountShouldBeOne()
        {
            //Arrange
            var service = CreateService();
           
            var emailParameterCallBack = new AutoChargeSupportSummaryEmailParameterModel();
            var date = DateTime.UtcNow;
            var installment = FactoryGirl.Build<OrderInstallment>(i =>
            {
                i.Id = InstallmentId;
                i.Amount = 100;
                i.AutoChargeAttemps = 0;
                i.OrderItem.Order.ClubId = ClubId;
                i.TransactionActivities = new List<TransactionActivity>()
                {
                    FactoryGirl.Build<TransactionActivity>(t =>
                    {
                        t.TransactionCategory = TransactionCategory.Installment;
                        t.IsAutoCharge = true;
                        t.TransactionStatus = TransactionStatus.Failure;
                        t.InstallmentId = InstallmentId;
                        t.AutoChargePolicyAttempt = 1;
                        t.TransactionDate = date;})
                };
            });

            var installmentpair = new Dictionary<int, IEnumerable<long>> { { ClubId, new List<long>() { InstallmentId } } };

            _mockJbDateTime.Setup(d => d.UtcNow).Returns(date);

            

            _mockInstallmentBusiness.Setup(i => i.GetAllInstallmentsByDueDate(It.IsAny<DateTime>())).Returns(installmentpair);

            _mockInstallmentBusiness.Setup(o => o.Get(InstallmentId)).Returns(installment);


            _mockPaymentBusiness.Setup(p => p.ChargeInstallmentNew(installment, _club, It.IsAny<DateTime?>()))
                .Returns(new JbPaymentResult() { Succeed = false, Message = "unknown error" });

            _mockInstallmentBusiness.Setup(i => i.Update(installment));

            

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeSupportSummary, It.IsAny<AutoChargeSupportSummaryEmailParameterModel>(), null))
                .Callback<EmailCategory, AutoChargeSupportSummaryEmailParameterModel, DateTime?>(
                    (category, supportSummaryEmailParameter, date1) =>
                    {
                        emailParameterCallBack = supportSummaryEmailParameter;
                    });

            _mockEmailBusiness.Setup(e => e.Send(EmailCategory.AutoChargeFail, It.IsAny<AutoChargeFailEmailParameterModel>(), null));

            //Act
            service.Run(DateTime.UtcNow);

            //Assert

            _mockPaymentBusiness.Verify(p => p.ChargeInstallmentNew(It.IsAny<OrderInstallment>(), It.IsAny<Club>(), It.IsAny<DateTime?>()), Times.Once);
            _mockInstallmentBusiness.Verify(i => i.Update(It.IsAny<OrderInstallment>()), Times.Once);
            _mockTransactionActivityBusiness.Verify(t => t.Update(It.IsAny<TransactionActivity>()), Times.Once);
            //_mockEmailBusiness.Verify(e => e.Send(EmailCategory.AutoChargeFail, It.IsAny<AutoChargeFailEmailParameterModel>(),   null), Times.Once);

            (emailParameterCallBack.FailedInstallments.Count + emailParameterCallBack.SystemFailedInstallments.Count).Should().Be(1);
        }

        public AutoChargeBusiness CreateService()
        {
            _mockOrderInstallmentRepository = new Mock<IRepository<OrderInstallment, long>>();
            _mockClubRepository = new Mock<IClubRepository>();
            _mockPaymentBusiness = new Mock<IPaymentBusiness>();
            _mockInstallmentBusiness = new Mock<IOrderInstallmentBusiness>();
            _mockTransactionActivityBusiness = new Mock<ITransactionActivityBusiness>();
            _mockClubSettingBusiness = new Mock<IClubSettingBusiness>();
            _mockEmailBusiness = new Mock<IEmailBusiness>();
            _mockJbDateTime = new Mock<IJbDateTime>();
            _mockJbLocker = new Mock<IJbLocker>();
   

            var result = new AutoChargeBusiness(_mockOrderInstallmentRepository.Object, _mockClubRepository.Object,
                _mockPaymentBusiness.Object, _mockInstallmentBusiness.Object,
                _mockTransactionActivityBusiness.Object, _mockClubSettingBusiness.Object, _mockEmailBusiness.Object, _mockJbDateTime.Object,_mockJbLocker.Object);

          
            _mockClubSettingBusiness.Setup(c => c.ReadAutoChargePolicy(It.IsAny<Club>())).Returns(Defultpolicy());

            _club = FactoryGirl.Build<Club>(c => c.Id = ClubId);

            _mockClubRepository.Setup(c => c.Get(ClubId)).Returns(_club);

            _mockTransactionActivityBusiness.Setup(t => t.Update(It.IsAny<TransactionActivity>()));

            _mockJbDateTime.Setup(d => d.UtcNow).Returns(DateTime.UtcNow);

            return result;
        }
        private static AutoChargePolicy Defultpolicy()
        {
            return new AutoChargePolicy()
            {

                Attempts = new List<AutoChargeAttempt>()
                {
                    new AutoChargeAttempt()
                    {
                        AttemptOrder = 1,
                        IntervalDays = 0,
                        Enabled = true,
                    },
                    new AutoChargeAttempt()
                    {
                        AttemptOrder = 2,
                        IntervalDays = 2,
                        Enabled = false,
                    },
                    new AutoChargeAttempt()
                    {
                        AttemptOrder = 3,
                        IntervalDays =5,
                        Enabled = false
                    },
                    new AutoChargeAttempt()
                    {
                        AttemptOrder = 4,
                        IntervalDays =1,
                        Enabled = false
                    },
                    new AutoChargeAttempt()
                    {
                        AttemptOrder = 5,
                        IntervalDays =4,
                        Enabled = false
                    }
                },
                SuccessEmailToAdmin = true,
                SuccessParentEmailTemplate = "auto payment of your account was successfully",
                SuccessAdminEmailTemplate = "auto payment of parent account was successfully",
                IsCombinePayments = false

            };
        }
    }
}
