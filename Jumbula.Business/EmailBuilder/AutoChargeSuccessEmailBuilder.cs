﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Jumbula.Business.EmailBuilder
{
    public class AutoChargeSuccessEmailBuilder : BaseEmailBuilder<AutoChargeSuccessEmailParameterModel>
    {

        #region Fields
        private readonly IClubBusiness _clubBusiness;
        private readonly IClubSettingBusiness _clubSettingBusiness;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;

        private OrderInstallment _orderInstallment;
        private List<string> _clubOwnerEmails;
        private AutoChargePolicy _autoChargePolicy;
        private TransactionActivity _transactionActivity;
        private bool _sendToAdmin;
        #endregion

        #region Constants
        private const string AdminSubCategory = "Admin";
        private const string ParentSubCategory = "Parent";
        private const string EmailTemplateFileName = "InstallmentPaymentConfirmation";

        private const string Subject = "Auto-charge payment confirmation";
        private const string AdminSubjectSplicer = "for";

        private const string PreviewProgramName = "Advanced chess class";
        private const string PreviewConfirmationId = "7A53-79-3AF0";
        private const string PreviewPlayerName = "John Doe";
        private const decimal PreviewAmount = 200;
        #endregion

        #region Constructors
        public AutoChargeSuccessEmailBuilder(IClubBusiness clubBusiness, IOrderInstallmentBusiness orderInstallmentBusiness, IClubSettingBusiness clubSettingBusiness, IOrderItemBusiness orderItemBusiness)
        {
            _clubBusiness = clubBusiness;
            _clubSettingBusiness = clubSettingBusiness;
            _orderInstallmentBusiness = orderInstallmentBusiness;
            _orderItemBusiness = orderItemBusiness;
        }
        #endregion

        #region Properties 

        public override string TemplateName => EmailTemplateFileName;

        public override IEnumerable<string> SubCategories => new List<string> { AdminSubCategory, ParentSubCategory };
        public override EmailCategory Category => EmailCategory.AutoChargeSuccess;

        public override bool ShouldSendToSupport => true;

        public override EmailCallerType CallerType => EmailCallerType.AutoCharge;

        public override EmailSendPriority EmailSendPriority => EmailSendPriority.Low;
        #endregion

        #region Public Methods
        public override void Initiate(AutoChargeSuccessEmailParameterModel parameters)
        {
            _orderInstallment = _orderInstallmentBusiness.Get(parameters.InstallmentId);

            var club = _orderInstallment.OrderItem.Order.Club;
            _clubOwnerEmails = _clubBusiness.ClubOwnerEmailList(club);

            _autoChargePolicy = _clubSettingBusiness.ReadAutoChargePolicy(club);
            _sendToAdmin = _autoChargePolicy.SuccessEmailToAdmin;
            _transactionActivity = _orderInstallment.TransactionActivities.SingleOrDefault(t => t.Id == parameters.TransactionActivityId);
        }


        public override void InitiatePreview(AutoChargeSuccessEmailParameterModel parameters)
        {
        }

        
        public override object BuildBodyModel(string subCategory, EmailBodyGenerationMode previewMode = EmailBodyGenerationMode.Normal)
        {
            var order = _orderInstallment.OrderItem.Order;
            var club = order.Club;

            var installmentConfirmationText = _autoChargePolicy.SuccessParentEmailTemplate;

            if (subCategory.Equals(AdminSubCategory))
                installmentConfirmationText = _autoChargePolicy.SuccessAdminEmailTemplate;

            var result = new AutoChargeSuccessEmailBodyModel
            {
                ClubName = club.Name,
                TransactionId = _transactionActivity != null ? _transactionActivity.PaymentDetail.TransactionId : string.Empty,
                ClubCurrency = club.Currency,
                ClubEmail = club.ContactPersons.Any() ? club.ContactPersons.First().Email : string.Empty,
                ClubPhone = club.ContactPersons.Any() ? PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.First().Phone) : string.Empty,
                ClubDomain = club.Domain,
                SeasonDomain = _orderInstallment.OrderItem.Season.Domain,
                PlayerFullName = _orderInstallment.OrderItem.FullName,
                InstallmentDate = _orderInstallment.PaidDate.HasValue
                    ? _orderInstallment.PaidDate.Value.ToString(Constants.DefaultDateFormat)
                    : string.Empty,
                BodyMailTitle = RepairBodyHtmlEditorTags(installmentConfirmationText),
                ClubImageUrl = _clubBusiness.GetClubLogo(club),
                ReservationItems = new List<AutoChargeEmailBodyItemModel>
                {
                    new AutoChargeEmailBodyItemModel(club.Currency)
                    {
                        EventName = (_orderInstallment.OrderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || _orderInstallment.OrderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription)? _orderItemBusiness.GetSubscriptionSchedulePartDateRange(_orderInstallment) : _orderInstallment.OrderItem.Name,
                        ConfirmationId = _orderInstallment.OrderItem.Order.ConfirmationId,
                        Price = _orderInstallment.TransactionActivities.Any()
                            ? _orderInstallment.TransactionActivities.Last().Amount
                            : 0
                    }
                },
                IsAdminMode = subCategory.Equals(AdminSubCategory)
            };

            if (_transactionActivity?.PaymentDetail.CreditCardId != null)
            {
                result.LastDigits = _transactionActivity.PaymentDetail.CreditCard.LastDigits;
            }

            return result;
        }

        public override object BuildPreviewBodyModel(AutoChargeSuccessEmailParameterModel parameters, string subCategory)
        {
            var club = _clubBusiness.Get(parameters.ClubId);

            return new AutoChargeSuccessEmailBodyModel
            {
                ClubName = club.Name,
                BodyMailTitle = RepairBodyHtmlEditorTags(parameters.SuccessMessage),
                ClubCurrency = CurrencyCodes.USD,
                ClubEmail = club.ContactPersons.Any() ? club.ContactPersons.First().Email : string.Empty,
                ClubPhone = club.ContactPersons.Any() ? PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.First().Phone) : string.Empty,
                ClubDomain = club.Domain,
                InstallmentDate = DateTime.UtcNow.ToString(Constants.DefaultDateFormat),
                PlayerFullName = PreviewPlayerName,
                ClubImageUrl = _clubBusiness.GetClubLogo(club),
                ReservationItems = new List<AutoChargeEmailBodyItemModel>
                {
                    new AutoChargeEmailBodyItemModel(CurrencyCodes.USD)
                    {
                        EventName = PreviewProgramName,
                        ConfirmationId = PreviewConfirmationId,
                        Price = PreviewAmount
                    }
                },
                IsPreviewMode = true,
                IsAdminMode = subCategory?.Equals(AdminSubCategory) ?? false
            };
        }

        public override string GetSubject(string subCategory = null)
        {
            var result = Subject;

            if (subCategory == AdminSubCategory)
                result = $"{result} {AdminSubjectSplicer} {_orderInstallment.OrderItem.Order.User.UserName}";

            return result;
        }

        public override MailAddress GetFrom(string subCategory = null)
        {
            return new MailAddress(Constants.W_NoReplyEmailAddress, _orderInstallment.OrderItem.Order.Club.Name);
        }

        public override string GetFromDisplayName(string subCategory = null)
        {
            return _orderInstallment.OrderItem.Order.Club.Name;
        }

        public override IEnumerable<MailAddress> GetTos(string subCategory = null)
        {
            var result = new List<MailAddress>();

            if (subCategory == ParentSubCategory)
                result.Add(new MailAddress(_orderInstallment.OrderItem.Order.User.UserName));

            if (subCategory == AdminSubCategory && _sendToAdmin)
                result = _clubOwnerEmails.Select(e => new MailAddress(e)).ToList();

            return result;
        }

        public override IEnumerable<MailAddress> GetCCs(string subCategory = null)
        {
            return new List<MailAddress>();
        }

        public override IEnumerable<MailAddress> GetBcCs(string subCategory = null)
        {
            return new List<MailAddress>();
        }

        public override string GetReplyTo(string subCategory = null)
        {
            var result = Constants.W_NoReplyEmailAddress;

            if (subCategory == ParentSubCategory && _autoChargePolicy.HasReplyTo)
                result = _autoChargePolicy.ReplyToEmailAddress;

            return result;
        }

        public override List<CommunicationHistory> GetHistories()
        {
            return new List<CommunicationHistory>
            {
                new CommunicationHistory{ RelatedEntity = nameof(OrderInstallment), RelatedEntityId = _orderInstallment.Id}
            };
        }

        #endregion

        #region Private

        private string RepairBodyHtmlEditorTags(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
                return text.Replace("<p>", string.Empty).Replace("</p>", "</br>");

            return text;
        }

        #endregion
    }
}
