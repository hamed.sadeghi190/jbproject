﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignAddController', ['$scope', '$http', '$log', '$stateParams', '$state', 'templateEditorService', 'jbToast', 'jbConfirmation', 'imageUploadService', function ($scope, $http, $log, $stateParams, $state, templateEditorService, jbToast, jbConfirmation,imageUploadService) {
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        window.app.globalObjects.pageClass = 'campaigns-add ' + $scope.MC.stateParams.campaignType;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $scope.validateImage = function (elem, pos) {
            if (pos == 'header')
            {
                $scope.positionBanner = 'header';
            }
            else if (pos = 'footer')
            {
                $scope.positionBanner = 'footer';
            }
            imageUploadService.validateImage(this, elem)
          
        }
        $scope.typeBannerHeader = function (typeb) {
            $scope.typeBannerHeaderValue = typeb;
        }
        $scope.typeBannerFooter = function (typeb) {
            $scope.typeBannerFooterValue = typeb;
        }
        $scope.showCropper1 = true;
        $scope.showCropper2 = false;
        $scope.hideCropper1 = function () {
            $scope.showCropper1 = false;
            $scope.showCropper2 = true;
            $scope.cropper = imageUploadService.clearCropper(this);
        }
        $scope.hideCropper2 = function () {
            $scope.showCropper1 = true;
            $scope.showCropper2 = false;
            $scope.cropper = imageUploadService.clearCropper(this);
        }
        $scope.clearCropper = function () {
            $scope.cropper = imageUploadService.clearCropper(this);

        }
    
        $scope.Model = {};
        $scope.Model.MailTemplate = {};

        // use an array for attachments.
        $scope.Model.Attachments = [];

        $scope.showNewRecipientBox = false;
        $scope.addNewRecipient = [];

        $scope.showNewRecipient = function () {
            $scope.showNewRecipientBox = !$scope.showNewRecipientBox;
        }

        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        }

        $scope.existingImages = templateEditorService.existingImages;

        $scope.setHeaderBackgroundImageToNone = function () {
            if ($scope.Model.MailTemplate)
            $scope.Model.MailTemplate.HeaderBackground = '';
        }
        $scope.setFooterBackgroundImageToNone = function () {
            if ($scope.Model.MailTemplate)
                $scope.Model.MailTemplate.FooterBackground = '';
        }
        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        }

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        }

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        }

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        }

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        }

        $scope.enterEditing = function () {
            return templateEditorService.enterEditing(this);
        }

        $scope.saveEditing = function (posbanner, typeBannerHV, typeBannerFV) {
            $scope.positionBanner = null;
            return templateEditorService.saveEditing(this, posbanner, typeBannerHV, typeBannerFV);
        }

        $scope.cancelEditing = function () {
            $scope.positionBanner = null;
            return templateEditorService.cancelEditing(this);
        }
        //

        $http.get(baseUrl + '/GetNewRecipientModel', { params: { id: $scope.MC.stateParams.templateId, campaignId: $scope.MC.stateParams.campaignId } })
                .success(function (data, stats, headers, config) {
                    $scope.addNewRecipient = data;
                });

        if (($scope.MC.stateParams.templateId || $scope.MC.stateParams.templateId != '' || $scope.MC.stateParams.templateId != 'undefined'))
            $http.get(baseUrl + '/GetTemplate', { params: { id: $scope.MC.stateParams.templateId, campaignId: $scope.MC.stateParams.campaignId } })
                .success(function (data, stats, headers, config) {
                    $scope.Model.MailTemplate = data;
                });

        $scope.gridOptionsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + "/GetAllRecipients?campaignId=" + $scope.MC.stateParams.campaignId,
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchRecipient,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.fillGrid = {
            dataSource: $scope.gridOptionsDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100
                ],
                buttonCount: 5
            },
            columns: [
                {
                    field: "Email",
                    title: "Email",
                    width: "200px",
                }, {
                    field: "Name",
                    title: "Full name",
                    width: "200px",
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template:
                        "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				        <ul>\
				            <li ng-click='deleteCampaignRecipient(#=Id#)'>\
					            <span class='k-link jbi-remove'>Delete</span>\
				            </li>\
				            </ul>\
			            </li>\
		            </ul>",
                    attributes: {
                        class: "grid-actions"
                    }
                }
            ]
        };

        $scope.deleteCampaignRecipient = function (id) {
            jbConfirmation.yes(function () {
                $http.post(baseUrl + '/DeleteCampaignRecipient', { campaignId: $scope.MC.stateParams.campaignId, recipientId: id })
                .success(function (data, stats, headers, config) {
                    $scope.fillGrid.dataSource.read();
                });
            }).confirm();
        }

        $scope.addNewRecipientDirect = function () {
            $http.post(baseUrl + '/AddNewRecipientDirect', { campaignId: $scope.MC.stateParams.campaignId, model: $scope.addNewRecipient })
              .success(function (data, status, headers, config) {
                  if (data.Status == true) {
                      jbToast.success("New recipient added successfully.");
                      $scope.addNewRecipient.Email = "";
                      $scope.addNewRecipient.FirstName = "";
                      $scope.addNewRecipient.LastName = "";
                      $scope.fillGrid.dataSource.read();
                  }
                  else {
                      $scope.MC.handleErrors($scope, data);
                  }
              });
        }

        $scope.searchRecipients = function () {
            $scope.fillGrid.dataSource.read();
        }

        $scope.saveTemplate = function () {

            if ($scope.Model.MailTemplate.Name == null || $scope.Model.MailTemplate.Name == '') {
                jbToast.error("Please enter the template name.")
            } else {
                $http.post(baseUrl + '/SaveUpdateTemplate', { Model: $scope.Model.MailTemplate, templateId: $scope.MC.stateParams.templateId })
                         .success(function (data, status, headers, config) {
                             $scope.Model.Id = parseInt(data.Data, 0);
                             if (data.Status) {
                                 $scope.step = "delivery";
                                 switch (data.Data) {
                                     case "TemplateList":
                                         {
                                             $state.go("EmailCampaignTemplates");
                                             break;
                                         }
                                 }
                             }
                         });
            }
  
        }

        $scope.onUpload = function (e) {
            e.data = { campaignId: $scope.MC.stateParams.campaignId };
        }

        $scope.OnUploaderSuccess = function (data) {
            $scope.fillGrid.dataSource.read();
            if (data.response.Status == true) {
                $scope.fillGrid.dataSource.read();
            } else {
                if (data.response.FormErrors[0] && data.response.FormErrors[0].Key == "model.NoFileUploaded") {
                    jbToast.error($scope.MC.joinStringsWithBreak(data.response.FormErrors[0].Messages));
                }
                if (data.response.FormErrors[0].Key == "model.FileContentError") {
                    $(".k-upload-files.k-reset").find("li").remove();
                    $("strong.k-upload-status.k-upload-status-total").remove();
                    jbToast.error($scope.MC.joinStringsWithBreak(data.response.FormErrors[0].Messages));
                }
                if (data.response.FormErrors[0].Key == "model.FileNotInFormat") {
                    $(".k-upload-files.k-reset").find("li").remove();
                    $("strong.k-upload-status.k-upload-status-total").remove();
                    jbToast.error($scope.MC.joinStringsWithBreak(data.response.FormErrors[0].Messages));
                }
            }
        }
    }]);
})();