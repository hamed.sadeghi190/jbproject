﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class PeopleRegistrantViewModel
    {

        public int Id { get; set; }

        public int UserId { get; set; }

        public int PlayerId { get; set; }

        public string FullName { get; set; }

        public int Age { get; set; }
        public string DOB { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Parent { get; set; }

        public string LastRegister { get; set; }
        public string Gender { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Balance { get; set; }
        public int TotalOrders { get; set; }
        public string Graduated { get; set; }

        public string ClubName { get; set; }
        public int ClubId { get; set; }
        public string Grade { get; set; }
    }
}