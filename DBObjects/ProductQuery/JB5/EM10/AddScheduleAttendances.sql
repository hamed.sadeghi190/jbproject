
EXEC sp_rename 'ProgramSchedules.Sections', 'Sessions', 'COLUMN'
go


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ScheduleAttendances](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ScheduleId] [bigint] NOT NULL,
	[ProfileId] [int] NOT NULL,
	[Status] [int] NULL,
	[Start] [datetime] NOT NULL,
	[End] [datetime] NOT NULL,
	[Note] [nvarchar](4000) NULL,
	[SessionId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ScheduleAttendances] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ScheduleAttendances] ADD  DEFAULT ((0)) FOR [SessionId]
GO

ALTER TABLE [dbo].[ScheduleAttendances]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ScheduleAttendances_dbo.PlayerProfiles_ProfileId] FOREIGN KEY([ProfileId])
REFERENCES [dbo].[PlayerProfiles] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ScheduleAttendances] CHECK CONSTRAINT [FK_dbo.ScheduleAttendances_dbo.PlayerProfiles_ProfileId]
GO

ALTER TABLE [dbo].[ScheduleAttendances]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ScheduleAttendances_dbo.ProgramSchedules_ScheduleId] FOREIGN KEY([ScheduleId])
REFERENCES [dbo].[ProgramSchedules] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ScheduleAttendances] CHECK CONSTRAINT [FK_dbo.ScheduleAttendances_dbo.ProgramSchedules_ScheduleId]
GO


