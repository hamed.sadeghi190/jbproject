﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;
using Jumbula.Core.Domain;
using Jumbula.Core.Domain.Generic;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models
{
    // select
    public class GroupedSelectListItem : SelectListItem
    {
        public string GroupKey { get; set; }
        public string GroupName { get; set; }
    }

    public class ChargeDiscountItem
    {
        public string Name { get; set; }

        public decimal Price { get; set; }
    }
    public class JsonResponse
    {
        public string Type { get; set; }
        public string Message { get; set; }
        public IEnumerable<JsonValidationError> Errors { get; set; }

        public JsonResponse()
        {
            Errors = new List<JsonValidationError>();
        }
    }

    public class JsonValidationError
    {
        public string Key { get; set; }
        public string Message { get; set; }
    }


    public enum DonationReceiveType
    {
        [Description("Received from Club")]
        FromClub = 0,
        [Description("Received from Event")]
        FromEvent = 1
    }


    public enum SearchPaymentStaus
    {
        [Description("All")]
        All = -1,
        [Description("Pending")]
        Pending = 2,
        [Description("Completed")]
        Completed = 3,
        [Description("Canceled")]
        Canceled = 4,
        [Description("Reject")]
        Reject = 13,
        [Description("Partially completed")]
        PartiallyCompleted = 16
    }

    
    public enum ExportMode : byte
    {
        Pdf,
        Excel
    }

    public enum ReportType : byte
    {
        Capacity,
        Registration,
        Installment,
        Custom,
        FollowupForm,
        Balance,
        ChargeDiscount,
        Finance,
        Coupon,
        CustomParent,
        CustomParentPortal
    }

    public enum TableTennisPageStep
    {
        General,
        Events,
        Extras,
        Policies,
        Finish
    }

    public enum ViewMode
    {
        Create = 0,
        Edit = 1,
        View = 2
    }

  
    public enum AssignMode : int
    {
        ALL = 0,
        NTH = 1,
        NTH_PLUS = 2
    }

    public class WizardStep
    {
        public EventPageStep CurrentPage { get; set; }

        public EventPageStep LastCreatedPage { get; set; }

        public PageMode PageMode { get; set; }

        public WizardStep()
        {

        }

        public WizardStep(EventPageStep currentPage, EventPageStep lastCreatedPage, PageMode? pageMode = null)
        {
            CurrentPage = currentPage;
            LastCreatedPage = lastCreatedPage;

            if (!pageMode.HasValue)
            {
                if (IsStepCreated(currentPage, lastCreatedPage))
                {
                    PageMode = PageMode.Edit;
                }
                else
                {
                    PageMode = PageMode.Create;
                }
            }
            else
            {
                PageMode = pageMode.Value;
            }
        }

        private bool IsStepCreated(EventPageStep eventPageStep, EventPageStep? lastStep)
        {
            if (lastStep.HasValue)
            {
                if (eventPageStep == lastStep.Value || eventPageStep < lastStep.Value)
                {
                    return true;
                }
            }

            return false;
        }
    }





    public class ContactPersonViewModel : BaseViewModel<ContactPerson>
    {
        [Required]
        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}.")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string Email { get; set; }

        [Required]
        [Phone]
        [StringLength(10, ErrorMessage = "{0} is too long.")]
        [RegularExpression("[0-9]{10}",
            ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876).")]
        public string Phone { get; set; }

        public ContactPersonViewModel()
        {
        }

      

        public ContactPerson ToContactPerson()
        {
            return new ContactPerson { FirstName = FirstName, LastName = LastName, Email = Email, Phone = Phone };
        }
    }

    public class CampsRegistrationContactPersonViewModel
    {
        [Required]
        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}.")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}.")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email address")]
        [StringLength(128, ErrorMessage = "{0} is too long.")]
        public string Email { get; set; }

        [Phone]
        [Display(Name = "Home phone")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [RegularExpression("[0-9]{10}",
            ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876).")]
        public string Phone { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Cell phone")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [RegularExpression("[0-9]{10}",
            ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876).")]
        public string Cell { get; set; }

        [Phone]
        [DisplayName("Work phone")]
        [StringLength(64)]
        [RegularExpression("[0-9]{10}",
            ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876).")]
        public string Work { get; set; }

        public CampsRegistrationContactPersonViewModel()
        {
        }

        public CampsRegistrationContactPersonViewModel(ContactPerson model)
        {
            FirstName = model.FirstName;
            LastName = model.LastName;
            Email = model.Email;
            Phone = model.Phone;
            Cell = model.Cell;
            Work = model.Work;
        }

        public ContactPerson ToContactPerson()
        {
            return new ContactPerson
            {
                FirstName = FirstName,
                LastName = LastName,
                Email = Email,
                Phone = Phone,
                Cell = Cell,
                Work = Work
            };
        }
    }

    public class DoctorContactViewModel
    {
        [Display(Name = "First name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete {0}.")]
        public string LastName { get; set; }

        [Display(Name = "Phone Number")]
        [StringLength(64)]
        [RegularExpression("[0-9]{10}",
            ErrorMessage = "Please enter exactly 10 digits including area code (e.g. 5551239876).")]
        public string Phone { get; set; }

        public DoctorContactViewModel()
        {

        }

        public DoctorContactViewModel(ContactPerson model)
        {
            if (model != null)
            {
                FirstName = model.FirstName;
                LastName = model.LastName;
                Phone = model.Phone;
            }
        }

        public ContactPerson ToContactPerson()
        {
            return new ContactPerson { FirstName = FirstName, LastName = LastName, Phone = Phone };
        }
    }

    


    [Serializable]
    public class MultipleWeekDiscountMetaData
    {
        public MultipleWeekDiscountMetaData()
        {
            RequiredRegister = 2;
        }

        [DisplayName("Apply before")]
        public DateTime? ApplyBefor { get; set; }

        [Range(2, int.MaxValue, ErrorMessage = "{0} must be a equal or grater than 2.")]
        [DisplayName("Required register")]
        public int RequiredRegister { get; set; }

        [DisplayName("Limited")]
        public decimal? MaximumDiscount { get; set; }

        [DisplayName("Repeated or one time")]
        public bool IsRepeated { get; set; }

    }

    [Serializable]
    [XmlInclude(typeof(OptionalChargesDiscountRadioListItem))]
    public class OptionalChargesDiscountRadioListMetaData
    {
        public List<OptionalChargesDiscountRadioListItem> List { get; set; }

        public OptionalChargesDiscountRadioListMetaData()
        {
            List = new List<OptionalChargesDiscountRadioListItem>();
        }
    }

    [Serializable]
    public class OptionalChargesDiscountRadioListItem
    {
        public string Text { get; set; }

        public bool Selected { get; set; }

        public bool TrueValue { get; set; }
    }

    [Serializable]
    public class ExtendedCareChargeMetaData
    {
        [DisplayName("Start Time")]
        public string StartTime { get; set; }

        [DisplayName("End Time")]
        public string EndTime { get; set; }
    }

    [Serializable]
    public class SiblingDiscountMetaData
    {

        public bool PerEvent { get; set; }

    }
    public enum MultipleDiscountPerType
    {
        Schedule = 1,
        Program = 2
    }


    /// <summary>
    /// I developed this but actually need a view model for tournament names within a club.
    /// Still useful but not tested.
    /// </summary>
    public class SubDomainViewModel
    {
        private string _domain;
        private SubDomainCategories? _subDomainCatg;

        [Required]
        [Display(Name = "Subdomain")]
        [RegularExpression("[0-9]{1,2,3}",
            ErrorMessage = "You must select a {0}.")]
        public String SubDomain { get; set; }

        public SelectList SubDomainList { get; set; }

      


       
    }




    public enum SendMailModel
    {
        sendAll = 0,
        sendCustom = 1,
    }

    public class SendMailToPlayerViewModel
    {

        public SendMailToPlayerViewModel()
        {
            Emails = new List<SendingMailInformation>();
            LegalAttachmentType = Constants.LegalAttachmentType;
        }

        public SendMailModel Mode { get; set; }

        public string From { get; set; }

        [Required]
        public string Subject { get; set; }

        [AllowHtml]
        public string Body { get; set; }

        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }

        [Display(Name = "File attachments")]
        public string FileNames { get; set; }

        public string[] LegalAttachmentType { get; set; }

        public SubDomainCategories EventType { get; set; }

        //public SportCategories Sport { get; set; }

        public int CategoryId { get; set; }

        public List<SendingMailInformation> Emails { get; set; }

        [Display(Name = "Events")]
        public List<SelectListItem> Events { get; set; }

    }

  
    public class MimType
    {
        public static readonly Dictionary<string, string> MIMETypesDictionary = new Dictionary<string, string>
            {
              {".pdf","application/pdf"},
            {".gif","image/gif"},
            {".png","image/png"},
            {".jpeg","image/jpeg"},
            {".jpg","image/jpg"},
            {".msword","application/msword"},
            {".docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
            {".zip","application/zip"},
            {".rar","application/x-rar-compressed"},
            {".pptx","application/vnd.openxmlformats-officedocument.presentationml.presentation"},
            {".xls","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
            {".txt","text/plain"}
            };
    }

    public class RegistrationPeriodViewModel
    {
        [Required(ErrorMessage = "{0} is required.")]
        [DisplayName("Register start date")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? RegisterStartDate { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [DisplayName("Register end date")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? RegisterEndDate { get; set; }

     
       
        [Display(Name = "Register start time")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? RegisterStartTime { get; set; }

       
        [JsonConverter(typeof(CustomTimeConverter))]
        [Display(Name = "Register end time")]
        public DateTime? RegisterEndTime { get; set; }

        public RegistrationPeriodViewModel()
        {

        }

        public RegistrationPeriodViewModel(DateTime startDate, DateTime endDate)
        {
            RegisterStartDate = startDate;
            RegisterEndDate = endDate;
        }
    }

    [Serializable]
    public class MeetUpTimes
    {
        public MeetUpTimes()
        {
            RecurrenceTimes = new List<CourseRecurrenceTimeViewModel>();
        }

        public List<CourseRecurrenceTimeViewModel> RecurrenceTimes { get; set; }

        [MaxLength(ErrorMessage = "Description should be 100 character maximum.")]
        public string Description { get; set; }
    }

    public class CourseRecurrenceTimeViewModel
    {
        [Required]
        public bool IsChecked { get; set; }

        public ScheduleDayOfWeek Day { get; set; }

        //Timespan not saved to xml correctly i ignored
        [XmlIgnore]
        [Display(Name = "From")]
        [DataType(DataType.Time)]
        public TimeSpan? StartTime { get; set; }

        //Timespan not saved to xml correctly i ignored
        [XmlIgnore]
        [Display(Name = "To")]
        [DataType(DataType.Time)]
        public TimeSpan? FinishTime { get; set; }

        //insted Of Saving StartTime Timespan whe save StartTimeTicks long
        public long StartTimeTicks
        {
            get
            {
                if (StartTime != null) return ((TimeSpan)StartTime).Ticks;
                return 0;
            }
            set
            {
                if (value != 0)
                {
                    StartTime = TimeSpan.FromTicks((long)value);
                }
                else
                {

                    StartTime = null;
                }

            }
        }

        //insted Of Saving FinishTime Timespan whe save FinishTimeTicks long
        public long FinishTimeTicks
        {
            get
            {
                if (FinishTime != null) return ((TimeSpan)FinishTime).Ticks;
                return 0;

            }
            set
            {
                if (value != 0)
                {
                    FinishTime = TimeSpan.FromTicks((long)value);
                }
                else
                {
                    FinishTime = null;
                }

            }
        }

        public CourseRecurrenceTimeViewModel()
        {
        }
    }

    public enum ScheduleDayOfWeek
    {
        [Display(Name = "Sunday")]
        Sunday = 6,
        [Display(Name = "Monday")]
        Monday = 0,
        [Display(Name = "Tuesday")]
        Tuesday = 1,
        [Display(Name = "Wednesday")]
        Wednesday = 2,
        [Display(Name = "Thursday")]
        Thursday = 3,
        [Display(Name = "Friday")]
        Friday = 4,
        [Display(Name = "Saturday")]
        Saturday = 5
    }
}