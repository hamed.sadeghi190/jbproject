﻿using Jumbula.Core.Model.Report.Filter;
using System.Collections.Generic;
using Jumbula.Common.Utilities;

namespace Jumbula.Core.Model.Report
{
    public class ReportDataModel
    {
        public ReportDataModel(PaginatedList<object> data, List<ReportGridColumn> columns)
        {
            Data = new DataModel() {DataSource = data ?? new PaginatedList<object>(new List<object>(), 0, 0, 0), TotalCount = data?.TotalCount ?? 0};
            Columns = columns;
        }

        public DataModel Data { get; set; }
        public List<ReportGridColumn> Columns { get; set; }
    }
}
