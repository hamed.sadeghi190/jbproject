﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios
{
    class SchoolTestScenarion : BaseTestScenario
    {
        [Test]
        public void ViewSchools()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();
            ExtraAPI.Wait(20000);

            // Go to schools
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToSchools();
            ExtraAPI.Wait(5000);

            // View schools test
            var schooltest = new SchoolTest(WebDriver);
            schooltest.View();

            WebDriver.Close();
        }

        [Test]
        public void FilterSchools()
        {
            BaseUrl = DataDriver.PartnerURL;
            base.Browser = Browser.Chrome;

            //Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(5000);

            // login
            var hompageTest = new HomePageTest(WebDriver);
            hompageTest.ClickLoginLink();
            var logingTest = new LoginTest(WebDriver);
            logingTest.LoginPartner();
            
            // Go to schools
            var bladeTest = new BladeTest(WebDriver);
            bladeTest.GoToSchools();
            

            // View schools test
            var schooltest = new SchoolTest(WebDriver);
            schooltest.Filter();

            WebDriver.Close();
        }
    }
}
