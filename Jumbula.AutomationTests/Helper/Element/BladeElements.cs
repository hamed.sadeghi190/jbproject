﻿using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    public class BladeElements : BaseElements
    {
        public BladeElements(IWebDriver driver)
            :base(driver)
        {

        }

        public IWebElement SeasonsLink
        {
            get
            {
                return Driver.FindElement(By.Id("Seasons")); ;
            }
        }
        public IWebElement Fall2017SeasonLink
        {
            get
            {
                return Driver.FindElement(By.LinkText("Fall 2017")); ;
            }
        }
        public IWebElement SetupLink
        {
            get
            {
                return Driver.FindElement(By.LinkText("Setup")); ;
            }
        }
        public IWebElement ProgramLink
        {
            get
            {
                return Driver.FindElement(By.LinkText("Programs")); ;
            }
        }
        public IWebElement AddProgramLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Add program")) ;
            }
        }
        public IWebElement AddClassLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Class"));
            }
        }
        public IWebElement ProgramAndOrderLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Programs and orders"));
            }
        }
        #region organization
        public IWebElement OrganizationLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Organizations"));
            }
        }
        public IWebElement SchoolsLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Schools"));
            }
        }
        public IWebElement ProvidersLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Providers"));
            }
        }
        public IWebElement SchoolDistrictsLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("School districts"));
            }
        }
        public IWebElement GovernmentSubsidiesLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Government subsidies"));
                                                          
            }
        }
        public IWebElement AddSchoolLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Add school"));
            }
        }
        public IWebElement AddProviderLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Add provider"));
            }
        }
        public IWebElement AddDistrictLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Add district"));
            }
        }
        public IWebElement AddSubsidyLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Add subsidy"));
            }
        }

        #endregion
        #region Contacts
        public IWebElement ContactsLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Contacts"));
            }
        }
        public IWebElement AllContactsLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("All contacts"));
            }
        }
        public IWebElement ContactSettingsLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[@ui-sref='ContactSettings']"));
            }
        }
        #endregion Contacts
        #region
        public IWebElement SettingLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Settings"));
            }
        }
        public IWebElement NotificationsLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Notifications"));
            }
        }
        public IWebElement JumbulaHomeSiteLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Jumbula home site"));
            }
        }
        public IWebElement EditSiteLink
        {
            get
            {
                return Driver.FindElement(By.PartialLinkText("Edit site"));
            }
        }
        #endregion
    }
}









