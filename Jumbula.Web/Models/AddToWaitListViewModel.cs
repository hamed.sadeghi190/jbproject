﻿using Jumbula.Common.Enums;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;

namespace SportsClub.Models
{
    public class AddToWaitListViewModel : BaseViewModel
    {
        public AddToWaitListViewModel()
        {
            
        }

        public string ProgramName { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone number")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter the phone number including area code (e.g. 5551239876).")]
        [StringLength(15, ErrorMessage = "{0} is too long.")]
        [MinLength(8, ErrorMessage = "Enter a complete phone number including area code.")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Grade")]
        [Required(ErrorMessage = "{0} is required.")]
        public SchoolGradeType? Grade { get; set; }

        public long ScheduleId { get; set; }
        public string WaitlistPolicy { get; set; }
    }
}