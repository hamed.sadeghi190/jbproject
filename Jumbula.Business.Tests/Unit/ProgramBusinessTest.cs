﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class ProgramBusinessTest
    {
        #region Fields
        private Mock<IClubBusiness> _mockClubBusiness;
        private Mock<ISubscriptionBusiness> _mockSubscriptionBusiness;
        private Mock<IAttributeBusiness> _mockAttributeBusiness;
        private Mock<IOrderBusiness> _mockOrderBusiness;
        private Mock<IOrderItemBusiness> _mockOrderItemBusiness;
        private Mock<IOrderSessionBusiness> _mockOrderSessionBusiness;
        private Mock<IProgramSessionBusiness> _mockProgramSessionBusiness;
        private Mock<ISeasonBusiness> _mockSeasonBusiness;
        private Mock<IImageGalleryBusiness> _mockImageGalleryBusiness;
        private Mock<ICategoryBusiness> _mockCategoryBusiness;
        private Mock<IRepository<Program, long>> _mockProgramRepository;
        private Mock<IEntitiesContext> _mockDataContext;
        private Mock<ILocationBusiness> _mockLocationBusiness;
        private Mock<IRepository<OrderChargeDiscount, long>> _mockOrderChargeDiscountBusiness;

        private const int Id = 1;
        private const int ClubId = 1;
        #endregion


        #region Public methods
        [TestMethod]
        public void GetList_ClubId_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();
            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns(CreatePrograms().AsQueryable());

            //Act
            var result = service.GetList(ClubId);

            //Assert
            result.Count().Should().Be(CreatePrograms().Count());
        }

        [TestMethod]
        public void GetList_ClubId_ShouldReturnEmpty()
        {
            //Arrange
            var service = CreateService();
            const int clubId = 1;
            _mockProgramRepository.Setup(x => x.GetList(p => p.Status != ProgramStatus.Deleted && p.Season.ClubId == clubId)).Returns(CreatePrograms().AsQueryable());

            //Act
            var result = service.GetList(clubId);

            //Assert
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GetClubPrograms_ClubId_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();
            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns(CreatePrograms().AsQueryable());

            //Act
            var result = service.GetList(ClubId);

            //Assert
            result.Should().NotBeNull();
            result.Should().NotBeEmpty();
        }

        [TestMethod]
        public void GetClubPrograms_ClubId_ShouldReturnEmpty()
        {
            //Arrange
            var service = CreateService();
            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns<IQueryable<Program>>(null);

            //Act
            var result = service.GetList(ClubId);

            //Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetProgramCalendarItems_SeasonIdsIsNull_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();
            var program = CreateProgramForGetProgramCalendarItems();
            var baseUrl = "https://em-myschool.jumbuladev.local";

            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns(new List<Program> { program }.AsQueryable());
            _mockOrderItemBusiness.Setup(x => x.GetProgramOrderItems(It.IsAny<long>(), OrderItemStatusCategories.completed)).Returns(CreateOrderItems());

            //Act
            var result = service.GetProgramCalendarItems(null, ClubId, baseUrl, DateTime.Now.Date, DateTime.Now.Date).ToList();

            //Assert
            result.Count.Should().Be(1);
            result.ElementAt(0).Title.Should().Be("After school - Chess");
            result.ElementAt(0).SeasonDomain.Should().Be("Fall2018");
        }

        [TestMethod]
        public void GetProgramCalendarItems_SeasonIdsHasValue_ShouldReturnValidResult()
        {
            //Arrange
            var service = CreateService();
            var program = CreateProgramForGetProgramCalendarItems();
            var baseUrl = "https://em-myschool.jumbuladev.local";

            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns(new List<Program> { program }.AsQueryable());
            _mockOrderItemBusiness.Setup(x => x.GetProgramOrderItems(It.IsAny<long>(), OrderItemStatusCategories.completed)).Returns(CreateOrderItems());

            //Act
            var result = service.GetProgramCalendarItems(null, ClubId, baseUrl, DateTime.Now.Date, DateTime.Now.Date).ToList();

            //Assert
            result.Count.Should().Be(1);
            result.ElementAt(0).Title.Should().Be("After school - Chess");
            result.ElementAt(0).SeasonDomain.Should().Be("Fall2018");
        }

        [TestMethod]
        public void GetProgramCalendarItems_ScheduleTitleHasValue_ShouldReturnScheduleTitleWithProgramName()
        {
            //Arrange
            var service = CreateService();
            var program = CreateProgramForGetProgramCalendarItems();
            var baseUrl = "https://em-myschool.jumbuladev.local";

            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns(new List<Program> { program }.AsQueryable());
            _mockOrderItemBusiness.Setup(x => x.GetProgramOrderItems(It.IsAny<long>(), OrderItemStatusCategories.completed)).Returns(CreateOrderItems());

            //Act
            var result = service.GetProgramCalendarItems(null, ClubId, baseUrl, DateTime.Now.Date, DateTime.Now.Date).ToList();

            //Assert
            result.Count.Should().Be(1);
            result.ElementAt(0).Title.Should().Be("After school - Chess");
        }

        [TestMethod]
        public void GetProgramCalendarItems_CapacityMoreThanZero_ShouldReturnTwelve()
        {
            //Arrange
            var service = CreateService();
            var program = CreateProgramForGetProgramCalendarItems();
            var baseUrl = "https://em-myschool.jumbuladev.local";

            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns(new List<Program> { program }.AsQueryable());
            _mockOrderItemBusiness.Setup(x => x.GetProgramOrderItems(It.IsAny<long>(), OrderItemStatusCategories.completed)).Returns(CreateOrderItems());

            //Act
            var result = service.GetProgramCalendarItems(null, ClubId, baseUrl, DateTime.Now.Date, DateTime.Now.Date).ToList();

            //Assert
            result.Count.Should().Be(1);
            result.ElementAt(0).Capacity.Should().Be("12");
        }

        [TestMethod]
        public void GetProgramCalendarItems_ScheduleChargeIsNull_CapacityIsUnlimited()
        {
            //Arrange
            var service = CreateService();
            var program = CreateProgramForGetProgramCalendarItems(0);
            var baseUrl = "https://em-myschool.jumbuladev.local";

            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns(new List<Program> { program }.AsQueryable());
            _mockOrderItemBusiness.Setup(x => x.GetProgramOrderItems(It.IsAny<long>(), OrderItemStatusCategories.completed)).Returns(CreateOrderItems());

            //Act
            var result = service.GetProgramCalendarItems(null, ClubId, baseUrl, DateTime.Now.Date, DateTime.Now.Date).ToList();

            //Assert
            result.Count.Should().Be(1);
            result.ElementAt(0).Capacity.Should().Be("Unlimited");
        }

        [TestMethod]
        public void GetProgramCalendarItems_ScheduleChargeHasValue_CapacityIsSeven()
        {
            //Arrange
            var service = CreateService();
            var program = CreateProgramForGetProgramCalendarItems(0, new List<Charge>() { new Charge { Capacity = 7, ProgramScheduleId = 1 } });
            var baseUrl = "https://em-myschool.jumbuladev.local";

            _mockProgramRepository.Setup(x => x.GetList(It.IsAny<Expression<Func<Program, bool>>>())).Returns(new List<Program> { program }.AsQueryable());
            _mockOrderItemBusiness.Setup(x => x.GetProgramOrderItems(It.IsAny<long>(), OrderItemStatusCategories.completed)).Returns(CreateOrderItems());

            //Act
            var result = service.GetProgramCalendarItems(null, ClubId, baseUrl, DateTime.Now.Date, DateTime.Now.Date).ToList();

            //Assert
            result.Count.Should().Be(1);
            result.ElementAt(0).Capacity.Should().Be("7");
        }

        #endregion


        #region Private methods

        private ProgramBusiness CreateService()
        {
            _mockClubBusiness = new Mock<IClubBusiness>();
            _mockSubscriptionBusiness = new Mock<ISubscriptionBusiness>();
            _mockAttributeBusiness = new Mock<IAttributeBusiness>();
            _mockOrderBusiness = new Mock<IOrderBusiness>();
            _mockOrderItemBusiness = new Mock<IOrderItemBusiness>();
            _mockOrderSessionBusiness = new Mock<IOrderSessionBusiness>();
            _mockProgramSessionBusiness = new Mock<IProgramSessionBusiness>();
            _mockSeasonBusiness = new Mock<ISeasonBusiness>();
            _mockImageGalleryBusiness = new Mock<IImageGalleryBusiness>();
            _mockCategoryBusiness = new Mock<ICategoryBusiness>();
            _mockProgramRepository = new Mock<IRepository<Program, long>>();
            _mockDataContext = new Mock<IEntitiesContext>();
            _mockLocationBusiness = new Mock<ILocationBusiness>();
            _mockOrderChargeDiscountBusiness = new Mock<IRepository<OrderChargeDiscount, long>>();

            var result = new ProgramBusiness(_mockClubBusiness.Object,
                _mockAttributeBusiness.Object,
                _mockImageGalleryBusiness.Object,
                _mockCategoryBusiness.Object,
                _mockProgramRepository.Object,
                _mockDataContext.Object,
                _mockLocationBusiness.Object,
                _mockOrderChargeDiscountBusiness.Object)
            {
                OrderItemBusiness = _mockOrderItemBusiness.Object,
                SubscriptionBusiness = _mockSubscriptionBusiness.Object,
                OrderBusiness = _mockOrderBusiness.Object,
                OrderSessionBusiness = _mockOrderSessionBusiness.Object,
                ProgramSessionBusiness = _mockProgramSessionBusiness.Object,
                SeasonBusiness = _mockSeasonBusiness.Object
            };


            return result;
        }

        private static IEnumerable<Program> CreatePrograms()
        {
            var program1 = FactoryGirl.Build<Program>(p =>
            {
                p.Id = Id + 1;
                p.Status = ProgramStatus.Open;
                p.LastCreatedPage = ProgramPageStep.Step4;
                p.Season = FactoryGirl.Build<Season>(s =>
                {
                    s.Status = SeasonStatus.Live;
                    s.ClubId = 1;
                });
                p.ProgramSchedules = new List<ProgramSchedule>()
                {
                    FactoryGirl.Build<ProgramSchedule>(ps =>
                    {
                        ps.Id = Id;
                        ps.Attributes = new ScheduleAttribute() { Days = new List<ProgramScheduleDay>(){ new ProgramScheduleDay() {DayOfWeek =  DayOfWeek.Monday} }};
                        ps.Program = FactoryGirl.Build<Program>(pr =>
                            {
                                pr.Season = FactoryGirl.Build<Season>(s =>
                                {
                                    s.Setting = new SeasonSchoolSetting();
                                    s.Club = FactoryGirl.Build<Club>(c =>
                                    {
                                        c.Setting = new SchoolSetting();
                                    });
                                    s.Domain = "Fall2018";
                                });
                                pr.Club = FactoryGirl.Build<Club>(c =>
                                {
                                    c.Setting = new SchoolSetting();
                                });
                                pr.Name = "Chess";
                                pr.Charges = new List<Charge>();
                                pr.ProgramSchedules = new List<ProgramSchedule>
                                {
                                    FactoryGirl.Build<ProgramSchedule>(pss =>
                                    {
                                        pss.AttributesSerialized = "{\"Days\":[{\"DayOfWeek\":1,\"StartTime\":\"00:00:00\",\"EndTime\":\"01:20:00\"},{\"DayOfWeek\":2,\"StartTime\":\"00:05:00\",\"EndTime\":\"01:50:00\"},{\"DayOfWeek\":3,\"StartTime\":\"00:10:00\",\"EndTime\":\"01:55:00\"}],\"ContinueType\":0,\"Occurances\":1,\"IsProrate\":false,\"WeekDaysMode\":0,\"EnableDropIn\":false,\"DropInLable\":null,\"MinimumEnrollment\":3,\"Capacity\":12,\"ShowCapacityLeft\":false,\"ShowCapacityLeftNumber\":0}";
                                    })
                                };
                            });
                        ps.Sessions = new List<ScheduleSession> {new ScheduleSession() {Id = 1, ScheduleId = 1, Start = DateTime.UtcNow.Date, End = DateTime.UtcNow.Date } };
                        ps.Charges = new List<Charge>();
                        ps.AttributesSerialized = "{\"Days\":[{\"DayOfWeek\":1,\"StartTime\":\"00:00:00\",\"EndTime\":\"01:20:00\"},{\"DayOfWeek\":2,\"StartTime\":\"00:05:00\",\"EndTime\":\"01:50:00\"},{\"DayOfWeek\":3,\"StartTime\":\"00:10:00\",\"EndTime\":\"01:55:00\"}],\"ContinueType\":0,\"Occurances\":1,\"IsProrate\":false,\"WeekDaysMode\":0,\"EnableDropIn\":false,\"DropInLable\":null,\"MinimumEnrollment\":3,\"Capacity\":12,\"ShowCapacityLeft\":false,\"ShowCapacityLeftNumber\":0}";
                    })

                };
                p.Club = FactoryGirl.Build<Club>();
            });
            var program2 = FactoryGirl.Build<Program>(p =>
            {
                p.Id = Id + 2;
                p.Status = ProgramStatus.Deleted;
                p.LastCreatedPage = ProgramPageStep.Step4;
                p.Season = FactoryGirl.Build<Season>(s => s.Status = SeasonStatus.Live);
                p.ProgramSchedules = new List<ProgramSchedule>()
                {
                    FactoryGirl.Build<ProgramSchedule>(ps =>
                    {
                        ps.Id = Id + 1;
                        ps.Attributes = new ScheduleAttribute() { Days = new List<ProgramScheduleDay>(){ new ProgramScheduleDay() {DayOfWeek =  DayOfWeek.Tuesday} }};
                        ps.Program = FactoryGirl.Build<Program>();
                    })
                };
                p.Club = FactoryGirl.Build<Club>();
            });
            var program3 = FactoryGirl.Build<Program>(p =>
            {
                p.Id = Id + 3;
                p.Status = ProgramStatus.Open;
                p.LastCreatedPage = ProgramPageStep.Step3;
                p.ProgramSchedules = new List<ProgramSchedule>() { FactoryGirl.Build<ProgramSchedule>() };
                p.Club = FactoryGirl.Build<Club>();
            });

            return new List<Program> { program1, program2, program3 };
        }

        private static IEnumerable<OrderItem> CreateOrderItems()
        {
            var orderItem1 = FactoryGirl.Build<OrderItem>(p => p.Id = Id);
            var orderItem2 = FactoryGirl.Build<OrderItem>(p => p.Id = Id + 1);
            var orderItem3 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 2;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.completed;
            });
            var orderItem4 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 3;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.ProgramSchedule = null;
            });
            var orderItem5 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 4;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Test;
                p.ItemStatus = OrderItemStatusCategories.completed;

            });
            var orderItem6 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 5;
                p.Order.IsLive = false;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.changed;
            });
            var orderItem7 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 6;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.changed;
            });

            return new List<OrderItem> { orderItem1, orderItem2, orderItem3, orderItem4, orderItem5, orderItem6, orderItem7 };
        }

        private static Program CreateProgramForGetProgramCalendarItems(int capacity = 12, List<Charge> charges = null)
        {
            var program = FactoryGirl.Build<Program>(p =>
            {
                p.Id = Id + 1;
                p.Status = ProgramStatus.Open;
                p.LastCreatedPage = ProgramPageStep.Step4;
                p.Season = FactoryGirl.Build<Season>(s =>
                {
                    s.Status = SeasonStatus.Live;
                    s.ClubId = 1;
                });
                p.ProgramSchedules = new List<ProgramSchedule>()
                {
                    FactoryGirl.Build<ProgramSchedule>(ps =>
                    {
                        ps.Id = Id;
                        ps.Attributes = new ScheduleAttribute() { Days = new List<ProgramScheduleDay>(){ new ProgramScheduleDay() {DayOfWeek =  DayOfWeek.Monday} }};
                        ps.Program = FactoryGirl.Build<Program>(pr =>
                            {
                                pr.Season = FactoryGirl.Build<Season>(s =>
                                {
                                    s.Setting = new SeasonSchoolSetting();
                                    s.Club = FactoryGirl.Build<Club>(c =>
                                    {
                                        c.Setting = new SchoolSetting();
                                    });
                                    s.Domain = "Fall2018";
                                });
                                pr.Club = FactoryGirl.Build<Club>(c =>
                                {
                                    c.Setting = new SchoolSetting();
                                });
                                pr.Name = "Chess";
                                ps.Sessions = new List<ScheduleSession> {new ScheduleSession() {Id = 1, ScheduleId = 1, Start = DateTime.UtcNow.Date, End = DateTime.UtcNow.Date } };
                                pr.ProgramSchedules = new List<ProgramSchedule>
                                {
                                    FactoryGirl.Build<ProgramSchedule>(pss =>
                                    {
                                        pss.AttributesSerialized = "{\"Days\":[{\"DayOfWeek\":1,\"StartTime\":\"00:00:00\",\"EndTime\":\"01:20:00\"},{\"DayOfWeek\":2,\"StartTime\":\"00:05:00\",\"EndTime\":\"01:50:00\"},{\"DayOfWeek\":3,\"StartTime\":\"00:10:00\",\"EndTime\":\"01:55:00\"}],\"ContinueType\":0,\"Occurances\":1,\"IsProrate\":false,\"WeekDaysMode\":0,\"EnableDropIn\":false,\"DropInLable\":null,\"MinimumEnrollment\":3,\"Capacity\":"+capacity+",\"ShowCapacityLeft\":false,\"ShowCapacityLeftNumber\":0}";
                                    })
                                };
                                ps.Title = "After school";
                            });
                        ps.Sessions = new List<ScheduleSession> {new ScheduleSession() {Id = 1, ScheduleId = 1, Start = DateTime.UtcNow.Date, End = DateTime.UtcNow.Date } };
                        ps.Charges = charges ?? new List<Charge>();
                        ps.AttributesSerialized = "{\"Days\":[{\"DayOfWeek\":1,\"StartTime\":\"00:00:00\",\"EndTime\":\"01:20:00\"},{\"DayOfWeek\":2,\"StartTime\":\"00:05:00\",\"EndTime\":\"01:50:00\"},{\"DayOfWeek\":3,\"StartTime\":\"00:10:00\",\"EndTime\":\"01:55:00\"}],\"ContinueType\":0,\"Occurances\":1,\"IsProrate\":false,\"WeekDaysMode\":0,\"EnableDropIn\":false,\"DropInLable\":null,\"MinimumEnrollment\":3,\"Capacity\":"+capacity+",\"ShowCapacityLeft\":false,\"ShowCapacityLeftNumber\":0}";
                    })

                };
                p.Club = FactoryGirl.Build<Club>();
            });

            return program;
        }

        #endregion


    }
}
