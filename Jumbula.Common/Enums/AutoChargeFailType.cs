﻿using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum AutoChargeFailType
    {
        [Description("latest Failed Transaction is null")]
        LatestFailedTransactionNull = 1,

        [Description("Transaction attempt number is wrong")]
        TransactionAttemptNumberIsWrong = 2,

        [Description("Policy not founded for this attempt")]
        PolicyNotFounded = 3,

        [Description("It's Not User valid failed")]
        IsNotUserFailed = 4,

        [Description("it's User valid failed")]
        IsUserFailed = 5

    }
}
