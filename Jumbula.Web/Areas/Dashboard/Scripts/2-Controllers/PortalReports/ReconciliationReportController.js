﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ReconciliationReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReports", order: 1, menu: "PortalReports" });


        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

       
        $scope.stateParams = $stateParams;
        $scope.MC.stateParams = $stateParams;
        $scope.Model = {};
       

        $scope.reportName = "";
        var nameModel = customReportService.getModelFromStrorage();
        var seasonId = nameModel.seasonId;
        var schoolId = nameModel.schoolId;

       
        if (is.not.undefined(nameModel) && nameModel != null) {
           
           
            $http.get('Dashboard/PortalReport/GetReoprtNameWithSeason', { params: { schoolId: schoolId, seasonId: seasonId } }).success(function (data, status, headers, config) {
                $scope.reportName = data;
              
               
               
                SetTheGrid();
            }).error(function (data, status, headers, config) {
                SetTheGrid();
                jbToast.error(data.Message);
            });
        }
    
        $scope.ProgramsReportGridOptions = {
            
            excel: {
                allPages: true,
                fileName: "Provider_Seasons_Reconciliation.xlsx",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: {

                type: "json",
                transport: {
                    read: {
                        url: '/Dashboard/PortalReport/SelectProgramsList?seasonId='+ $scope.MC.stateParams.seasonId ,
                    }
                   
                },
                schema: {
                    
                    total: "TotalCount",
                    data: "DataSource"
                },
                aggregate: [
               { field: "TotalRegistrations", aggregate: "sum" },
                { field: "TotalGrossAmtRegistrations", aggregate: "sum" },
                { field: "TotalPTAPTO", aggregate: "sum" },
                { field: "CashPaymentsPTAPTO", aggregate: "sum" },
                { field: "TotalAmtProviderFundedScholarships", aggregate: "sum" },
                { field: "TotalClassFeeslessProviderFundedScholarships", aggregate: "sum" },
                { field: "TotalAmtofClassFeesCollected", aggregate: "sum" },
                { field: "AmtofEMFeetoDeduct", aggregate: "sum" },
                { field: "ProviderPenaltiesandFines", aggregate: "sum" },
                { field: "TotalNetPaymenttoProvider", aggregate: "sum" },
                
                ],
              
            
                batch: true,
                
                serverSorting: false,
                resizable: true,
               
            },
            scrollable: true,
            sortable: true,

            editable: false,
    
            columns: [
               
                 {
                     field: "ClassName",
                     title: "Class name",
                     width: "200px",
                     footerTemplate: "Total"
                 },
                  {
                      field: "ProviderName",
                      title: "Provider name",
                      width: "170px"
                  },
                   {
                       field: "ClassFeeString",
                       title: "Class fee",
                       width: "150px"
                   },
                   {
                       field: "EMRatestr",
                       title: "EM rate",
                       width: "100px",
                       
                   },
                    {
                        field: "TotalRegistrations",
                        title: "Total # of registrations",
                        width: "200px",
                        footerTemplate: " #= kendo.toString(sum) #"
                    },
                     {
                         field: "TotalGrossAmtRegistrations",
                         title: "Total gross $ amt for registrations",
                         width: "300px",
                         
                         format: "{0:c2}",
                         footerTemplate: '#= kendo.toString(sum, "c2")# ',

                         Template: ' #=kendo.toString(TotalGrossAmtRegistrations, "c2")# '
                     },
                      {
                          field: "TotalPTAPTO",
                          title: "Total $ amt of PTA/PTO-funded scholarships",
                          width: "330px",
                          format: "{0:c2}",
                          footerTemplate: '#= kendo.toString(sum, "c2")# ',
                          Template: ' #=kendo.toString(TotalPTAPTO, "c2")# '
                      },
                        {
                            field: "CashPaymentsPTAPTO",
                            title: "Cash payments to PTA/PTO",
                            width: "250px",
                            format: "{0:c2}",
                            footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                            Template: ' #=kendo.toString(CashPaymentsPTAPTO, "c2")# '
                        },
                      {
                          field: "TotalAmtProviderFundedScholarships",
                          title: "Total $ amt of provider-funded scholarships",
                          width: "330px",
                          format: "{0:c2}",
                          footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                          Template: ' #=kendo.toString(TotalAmtProviderFundedScholarships, "c2")# '
                      },
                       {
                           field: "TotalClassFeeslessProviderFundedScholarships",
                           title: "Total class fees less provider-funded scholarships",
                           width: "400px",
                           format: "{0:c2}",
                           footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                           Template: ' #=kendo.toString(TotalClassFeeslessProviderFundedScholarships, "c2")# '
                       },
                         {
                             field: "TotalAmtofClassFeesCollected",
                             title: "Total $ amt of class fees collected",
                             width: "300px",
                             format: "{0:c2}",
                             footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                             Template: ' #=kendo.toString(TotalAmtofClassFeesCollected, "c2")# '

                         },
                         {
                             field: "AmtofEMFeetoDeduct",
                             title: "$ Amt of em fee to deduct",
                             width: "250px",
                             format: "{0:c2}",
                             footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                             Template: ' #=kendo.toString(AmtofEMFeetoDeduct, "c2")# '
                         },
                         {
                              field: "ProviderPenaltiesandFines",
                              title: "Provider penalties and fines",
                              width: "230px",
                              format: "{0:c2}",
                              footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                              Template: ' #=kendo.toString(ProviderPenaltiesandFines, "c2")# '
                          },
                          {
                              field: "TotalNetPaymenttoProvider",
                              title: "Total net payment to provider",
                              width: "300px",
                              format: "{0:c2}",
                              footerTemplate: ' #= kendo.toString(sum, "c2")# ',
                              Template: ' #=kendo.toString(TotalNetPaymenttoProvider, "c2")# '
                          },
            ],

            Sort: {
                field: "ClassName",
                dir: "Asc"
            },
        };

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ProgramsReportGrid").getKendoGrid().saveAsExcel();
        }
    }
    ]);
})();