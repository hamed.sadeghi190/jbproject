﻿using Elmah;
using Flurl.Http;
using Insightly;
using Insightly.Models.Api;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Models.Email;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using MailChimp.Errors;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using SportsClub.Models.Email;
using SportsClub.Models.Home;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.WebLogic.Email
{
    public class EmailService
    {
        private ControllerContext ControllerContext { get; set; }
        private EmailService(ControllerContext controllerContext)
        {
            this.ControllerContext = controllerContext;
        }
        private EmailService()
        {

        }
        public static EmailService Get(ControllerContext controllerContext)
        {
            return new EmailService(controllerContext);
        }
        internal void SendEnterpriseEmailToSupport(EnterpriseViewModel model)
        {
            var body = ViewHelper.RenderViewToString(ControllerContext, "~/Views/Shared/Email/EnterprisePlanEmaill.cshtml", model);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress);
            MailAddress to;
            List<MailAddress> cc;
            if (WebConfigHelper.IsProductionMode)
            {
                to = new MailAddress(Constants.W_JumbulaSupportEmail);
                cc = new List<MailAddress>() { new MailAddress("nacho@jumbula.com") };
            }
            else
            {
                to = new MailAddress("azadeh@jumbula.com");
                cc = new List<MailAddress>() { new MailAddress("azadeh@jumbula.com") };

            }
            Ioc.OldEmailBusiness.SendEmail(
                from, to,
                string.Format("Enterprise plan request from: {0}", model.FullName),
                body, true, null, cc, null, null
                );


        }
        public void SendGretingEmail(FreeTrialViewModel model)
        {
            var body = ViewHelper.RenderViewToString(ControllerContext, "~/Views/Shared/Email/FreeTrialEmaill.cshtml", model);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress);
            MailAddress to;
            List<MailAddress> cc;

            if (WebConfigHelper.IsProductionMode)
            {
                to = new MailAddress(Constants.W_JumbulaSupportEmail);
                cc = new List<MailAddress>() { new MailAddress("nacho@jumbula.com") };
            }
            else
            {
                to = new MailAddress("azadeh@jumbula.com");
                cc = new List<MailAddress>() { new MailAddress("azadeh@jumbula.com") };

            }
            Ioc.OldEmailBusiness.SendEmail(
                from, to,
                string.Format("Free trial request from: {0}", model.FullName),
                body, true, null, cc, null, null
                );

            try
            {

                MailChimpHelper.AddSubescriber(model.Email, model,
                    WebConfigHelper.MailChimpTrialListID, WebConfigHelper.MailChimpAPIKey);

                //Add contact to insightly
                CreateContactAsync(model);

            }
            catch (MailChimpAPIException ex)
            {
                LogHelper.LogException(ex);
            }
        }

        public void CreateContactAsync(FreeTrialViewModel model)
        {
            var contact = new InsightlyContact();
            contact.FirstName = model.FullName;

            var tag = new Tag();
            tag.OccasionName = "Free trial";
            contact.Tags.Add(tag);

            var address = new Address();
            address.AddressType = "Other";
            address.PostalCode = model.Address;

            contact.Addresses.Add(address);


            var contactEmail = new ContactInfo();
            contactEmail.ContactType = "EMAIL";
            contactEmail.Label = "Personal";
            contactEmail.Detail = model.Email;
            contact.ContactInfos.Add(contactEmail);

            var contactPhone = new ContactInfo();
            contactPhone.ContactType = "PHONE";
            contactPhone.Label = "Work";
            contactPhone.Detail = model.Phone;
            contact.ContactInfos.Add(contactPhone);

            var res = DoRequest<Insightly.Contact>("/Contacts", "POST", contact).Result;
        }
        private static async Task<T> DoRequest<T>(string url, string method, object body)
        {
            FlurlClient request = Authorise(url);
            T response = default(T);
            try
            {
                switch (method.ToLower())
                {
                    case "post":
                        if (body == null)
                            throw new ArgumentNullException("body");
                        response = await request.PostJsonAsync(body).ReceiveJson<T>().ConfigureAwait(false);
                        break;
                    case "get":
                        response = await request.GetJsonAsync<T>().ConfigureAwait(false);
                        break;
                    case "put":
                        if (body == null)
                            throw new ArgumentNullException("body");
                        response = await request.PutJsonAsync(body).ReceiveJson<T>().ConfigureAwait(false);
                        break;
                }
            }
            catch { }
            return response;
        }
        private static FlurlClient Authorise(string path)
        {
            var _apiUri = new Uri(Constants.Insightl_Url);
            string request = _apiUri + path;
            //6f445a34-7b86-4d07-af96-e118d9eb8266
            return request.WithBasicAuth(WebConfigHelper.InsightlyAPIKey, "");
        }
        internal void SendClientBillingNotification(string billingId, long clientId, Core.Domain.Club club, bool billingStatus, string periodDate, string subject, string text, string description)
        {
            // 

            var client = Ioc.ClientBusiness.GetById(clientId);
            var billingHistory = client.BillingHistories.LastOrDefault(c => c.BillingId == billingId);
            var transaction = billingHistory.TransactionActivities.Last();
            var emailModel = new ClientBillingNotification()
            {
                isFail = !(billingStatus),
                ClubName = club.Name,
                Amount = billingStatus == true ? CurrencyHelper.FormatCurrencyWithPenny(billingHistory.Credit, CurrencyCodes.USD) : CurrencyHelper.FormatCurrencyWithPenny(billingHistory.Debit, CurrencyCodes.USD),
                Brand = transaction.CreditCard == null ? "" : transaction.CreditCard.Brand,
                LastFour = transaction.CreditCard == null ? "" : transaction.CreditCard.LastDigits,
                PeriodDate = periodDate,
                Text = text,
                BillingId = billingHistory.BillingId,
                Description = (client.BillingNotes != null && client.BillingNotes.Any()) ? string.Join(", ", client.BillingNotes.Select(c => c.Description).ToList()) : description + " " + periodDate,
                Status = transaction.TransactionStatus.ToDescription(),
                TransactionDate = transaction.TransactionDate.ToString(Constants.DefaultDateFormat),
                TransactionId = transaction.TransactionId
            };
            var emailSubject = string.Format(subject, billingStatus ? "Succeed" : "Failed");

            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

            string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ClientMonthlyChargePaymentNotification.cshtml", emailModel);
            Ioc.OldEmailBusiness.SendClientBillingNotification(clubOwnerEmail, emailSubject, body);
        }
        //Add Email for sent to client for Take credit in jb/Dashboard
        internal void SendClientBillingByCreditNotification(long billingId, long clientId, Core.Domain.Club club, bool billingStatus, string periodDate, string description, string paymentmethod)
        {
            // 
            var client = Ioc.ClientBusiness.GetById(clientId);
            var billingHistory = client.BillingHistories.Last(c => c.Id == billingId);
            var transaction = billingHistory.TransactionActivities.Last();
            var emailModel = new ClientBillingNotification()
            {
                isFail = !(billingStatus),
                ClubName = club.Name,
                Amount = billingStatus == true ? CurrencyHelper.FormatCurrencyWithPenny(billingHistory.Credit, CurrencyCodes.USD) : CurrencyHelper.FormatCurrencyWithPenny(billingHistory.Debit, CurrencyCodes.USD),
                PeriodDate = periodDate,
                TransactionType = paymentmethod,
                Description = (client.BillingNotes != null && client.BillingNotes.Any()) ? string.Join(", ", client.BillingNotes.Select(c => c.Description).ToList()) : description + " " + "in" + " " + periodDate,
                Status = transaction.TransactionStatus.ToDescription(),
                TransactionDate = transaction.TransactionDate.ToString(Constants.DefaultDateFormat),

            };
            var emailSubject = string.Format(Constants.F_ClientBillingByCreditSubject, billingStatus ? "" : "Failed");

            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

            string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ClientChargePaymentByCreditNotification.cshtml", emailModel);
            Ioc.OldEmailBusiness.SendClientBillingNotification(clubOwnerEmail, emailSubject, body);
        }
        //Add Email to client for Refund Extera Amount
        internal void SendClientRefundBillingNotification(string billingId, long clientId, Core.Domain.Club club, bool refundstatus, string periodDate, string description)
        {
            var client = Ioc.ClientBusiness.GetById(clientId);
            var billingHistory = client.BillingHistories.LastOrDefault(c => c.BillingId == billingId);
            var transaction = billingHistory.TransactionActivities.Last();
            var emailModel = new ClientBillingNotification()
            {
                isFail = !(refundstatus),
                ClubName = club.Name,
                Amount = refundstatus == true ? CurrencyHelper.FormatCurrencyWithPenny(billingHistory.Credit, CurrencyCodes.USD) : CurrencyHelper.FormatCurrencyWithPenny(billingHistory.Debit, CurrencyCodes.USD),
                Brand = transaction.CreditCard == null ? "" : transaction.CreditCard.Brand,
                LastFour = transaction.CreditCard == null ? "" : transaction.CreditCard.LastDigits,
                PeriodDate = periodDate,
                BillingId = billingHistory.BillingId,
                Description = (client.BillingNotes != null && client.BillingNotes.Any()) ? string.Join(", ", client.BillingNotes.Select(c => c.Description).ToList()) : description + " " + periodDate,
                Status = transaction.TransactionStatus.ToDescription(),
                TransactionDate = transaction.TransactionDate.ToString(Constants.DefaultDateFormat),
                TransactionId = transaction.TransactionId
            };
            var emailSubject = string.Format(Constants.F_ClientBillingRefundSubject, refundstatus ? "Succeed" : "Failed");

            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

            string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ClientRefunaNotification.cshtml", emailModel);
            Ioc.OldEmailBusiness.SendClientBillingNotification(clubOwnerEmail, emailSubject, body);
        }
        internal void SendBackGroundCheckEmailToEM(AdvanceBackgroundCheckViewModel model, Core.Domain.Club club)
        {
            var url = System.Web.HttpContext.Current.Request.Url;

            dynamic emailmodel = new ExpandoObject();
            emailmodel.ClubName = club.PartnerClub.Name;
            emailmodel.ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.PartnerClub.Domain, club.PartnerClub.Logo).AbsoluteUri;
            emailmodel.ClubUrl = club.PartnerClub.Site;
            emailmodel.ProvideClubName = club.Name;
            emailmodel.BackgroundCertificatesURLs = model.BackgroundCertificatesURLs;
            emailmodel.BackgroundCertificatesNames = model.BackgroundCertificatesNames;
            emailmodel.RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);

            var body = ViewHelper.RenderViewToString(ControllerContext, "~/Views/Shared/Email/BackgroundCheckCertificateForEM.cshtml", emailmodel);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress);
            MailAddress to = null;
            List<MailAddress> cc = null;

            var mailReciever = string.Empty;

            if (club.PartnerId.HasValue)
            {
                var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(club.PartnerId.Value);

                mailReciever = partnerSetting.BackGroundCheckEmailReceiver;
            }

            if (!string.IsNullOrEmpty(mailReciever))
            {
                to = new MailAddress(mailReciever);
            }

            if (WebConfigHelper.IsProductionMode)
            {
                cc = new List<MailAddress>() { new MailAddress(Constants.W_JumbulaSupportEmail) };
            }

            if (to != null)
            {
                Ioc.OldEmailBusiness.SendEmail(
                    from, to,
                    string.Format("Background check certificates update for {0}", club.Name),
                    body, true, null, cc, null, null
                    );
            }

        }

        public void SendEcheckClearedNotification(bool isfail, long orderId, decimal paidAmount)
        {

            var order = Ioc.OrderBusiness.Get(orderId);


            var emailModel = new EmailReminderModel()
            {
                ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, order.Club.Domain, order.Club.Logo).AbsoluteUri,
                ClubName = order.Club.Name,
                ClubEmail = order.Club.ContactPersons.FirstOrDefault().Email,
                ClubPhone = PhoneNumberHelper.FormatPhoneNumber(order.Club.ContactPersons.FirstOrDefault().Phone),
                ClubDomain = order.Club.Domain,
                ClubCurrency = order.Club.Currency,
                SeasonDomain = string.Empty,
                PlayerFullName = order.User.UserName
            };
            emailModel.ConfirmationId = order.ConfirmationId;
            emailModel.PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(paidAmount, order.Club.Currency);
            emailModel.isFail = isfail;
            var emailSubject = string.Format(Constants.F_EcheckClearedNotification, order.User.UserName);

            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(order.Club);

            string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ECheckClearedPaymentNotification.cshtml", emailModel);
            Ioc.OldEmailBusiness.SendECheckClearedNotification(clubOwnerEmail, emailSubject, body, emailModel.ClubName);


        }

        public void SendPreapprovalConfirmation(long orderId, string preapprovalKey)
        {

            var firstOrder = Ioc.OrderBusiness.Get(orderId);
            var club = Ioc.ClubBusiness.Get(firstOrder.ClubId);

            var emailModel = new EmailReminderModel()
            {
                ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                ClubName = club.Name,
                ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                ClubDomain = club.Domain,
                ClubCurrency = club.Currency,
                SeasonDomain = string.Empty,
                PlayerFullName = firstOrder.User.UserName,
                PreapprovalKey = preapprovalKey
            };

            var emailSubject = Constants.F_preapprovedpaymentsplanSubject;

            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

            var primaryPlayerEmail = firstOrder.User.UserName;
            string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/PreapprovalEmail.cshtml", emailModel);
            Ioc.OldEmailBusiness.SendPreApprovalEmail(primaryPlayerEmail, clubOwnerEmail, emailSubject, body, emailModel.ClubName);


        }
        public void SendPreapprovalPaymentConfirmationEmail(long orderId, DateTime date)
        {
            try
            {
                var strDate = date.ToString("ddMMyyyyHHmmss");
                var transactionActivities = Ioc.TransactionActivityBusiness.GetAllsucceedTransactionByOrderId(orderId).ToList().Where(t => t.strTransactionDate == strDate);
                if (transactionActivities != null && transactionActivities.Count() > 0)
                {
                    var orderDb = Ioc.OrderBusiness;

                    var order = orderDb.Get(orderId);
                    order.User = Ioc.UserProfileBusiness.Get(order.UserId);
                    var cartItemsEmailModel = new ReservationNotificationViewModel();
                    cartItemsEmailModel.BodyMailTitle = Constants.ConfirmPreapprovalPaymentTitle;
                    var emailSubject = Constants.F_CashConfirmEmailSubject;
                    var clubOfOrder = order.Club;

                    cartItemsEmailModel.ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubOfOrder.Domain, clubOfOrder.Logo).AbsoluteUri;
                    cartItemsEmailModel.ClubName = clubOfOrder.Name;
                    cartItemsEmailModel.ClubEmail = clubOfOrder.ContactPersons.FirstOrDefault().Email;
                    cartItemsEmailModel.ClubPhone = PhoneNumberHelper.FormatPhoneNumber(clubOfOrder.ContactPersons.FirstOrDefault().Phone);
                    cartItemsEmailModel.OrderConfirmationId = order.ConfirmationId;
                    cartItemsEmailModel.ClubUrl = cartItemsEmailModel.RootSiteUrl + "/" + clubOfOrder.Domain;

                    cartItemsEmailModel.OrderPaidDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(date, clubOfOrder.TimeZone).ToString(Constants.DefaultDateTimeFormat), clubOfOrder.TimeZone.ToString());

                    var orderPaidAmount = CurrencyHelper.FormatCurrencyWithPenny(transactionActivities.Sum(t => t.Amount), clubOfOrder.Currency);

                    var appliedText = string.Format("Applied: ({0})", orderPaidAmount);

                    cartItemsEmailModel.CardType = "PayPal payment";
                    cartItemsEmailModel.PayPalInfo = string.Format(" - {0} (${1})", cartItemsEmailModel.CardType, orderPaidAmount);
                    cartItemsEmailModel.AppliedText = appliedText;
                    cartItemsEmailModel.PaidAmount = orderPaidAmount;

                    foreach (var transaction in transactionActivities)
                    {
                        var reservatioItem = new ReservationItem(clubOfOrder.Currency);
                        var orderItem = transaction.OrderItem;
                        reservatioItem.PlayerFullName = orderItem.FullName;


                        reservatioItem.EventName = Ioc.ProgramBusiness.GetProgramName(orderItem.ProgramSchedule, orderItem.EntryFeeName);

                        reservatioItem.EventAddress = orderItem.ProgramSchedule.Program.ClubLocation.PostalAddress;

                        reservatioItem.Amount = transaction.Amount;


                        if (cartItemsEmailModel.ReservationItems == null)
                        {
                            cartItemsEmailModel.ReservationItems = new List<ReservationItem>();
                        }

                        cartItemsEmailModel.ReservationItems.Add(reservatioItem);
                    }

                    var primaryPlayerEmail = new List<string>();
                    var secondaryPlayerEmail = new List<string>();

                    List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(clubOfOrder);

                    List<string> outSourcerClubOwnerEmail = null;

                    primaryPlayerEmail.Add(order.User.UserName);

                    string cartItemsBody = ViewHelper.RenderViewToString(ControllerContext, Constants.Path_ConfirmPreapprovalBody, cartItemsEmailModel);

                    var seasonId = order.RegularOrderItems.Any(o => o.SeasonId.HasValue) ? order.RegularOrderItems.Last(o => o.SeasonId.HasValue).SeasonId.Value : order.Club.Seasons.Last().Id;

                    var emailTemplate = Ioc.SeasonBusiness.Get(seasonId).Template;

                    var templateModel = (emailTemplate != null) ? JsonConvert.DeserializeObject<EmailTemplateModel>(emailTemplate.Template) : new EmailTemplateModel();

                    templateModel.Body = cartItemsEmailModel.BodyMailTitle;

                    templateModel.IsRegisterationEmail = true;

                    templateModel.Body = cartItemsBody;

                    string body = ViewHelper.RenderViewToString(ControllerContext, Constants.Path_EmailTemplate, templateModel);


                    Ioc.OldEmailBusiness.SendReservationEmail(primaryPlayerEmail, null, clubOwnerEmail, outSourcerClubOwnerEmail, emailSubject, body, cartItemsEmailModel.ClubName);

                }
            }
            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }
        }

        public void SendPreapprovalRequestEmail(long orderId)
        {

            try
            {

                var orderDb = Ioc.OrderBusiness;
                var order = orderDb.Get(orderId);

                var club = Ioc.ClubBusiness.Get(order.ClubId);
                var playerName = order.RegularOrderItems.FirstOrDefault().FullName;
                var orderPaidAmount = Ioc.TransactionActivityBusiness.GetPaidAmountForOrder(orderId);
                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    PlayerFullName = order.RegularOrderItems.Any(c => !c.FullName.Equals(playerName)) ? order.User.UserName : playerName,
                    ConfirmationId = order.ConfirmationId,
                    InstallmentDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(order.CompleteDate, club.TimeZone).ToString(Constants.DefaultDateTimeFormat), club.TimeZone.ToString()),
                    PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(orderPaidAmount, club.Currency),
                    Balance = (order.OrderAmount - orderPaidAmount) > 0 ? CurrencyHelper.FormatCurrencyWithPenny(order.OrderAmount - orderPaidAmount, club.Currency) : CurrencyHelper.FormatCurrencyWithPenny(0, club.Currency)
                };

                emailModel.PaymentLink = emailModel.RootSiteUrl + "/Cart/PreapprovalRequest?token=" + order.ConfirmationId;
                emailModel.ReservationItems = new List<InstallmentReminderItem>();

                foreach (var item in order.RegularOrderItems)
                {
                    emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                    {
                        PlayerFullName = item.FullName,
                        EventName = item.Name,
                        Price = item.TotalAmount

                    });
                }

                var emailSubject = Constants.F_PreapprovalReminderSubject;
                var primaryPlayerEmail = order.User.UserName;
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                string cartItemsBody = ViewHelper.RenderViewToString(ControllerContext, Constants.Path_RequestPreapprovalBody, emailModel);

                var seasonId = order.RegularOrderItems.Any(o => o.SeasonId.HasValue) ? order.RegularOrderItems.Last(o => o.SeasonId.HasValue).SeasonId.Value : order.Club.Seasons.Last().Id;

                var emailTemplate = Ioc.SeasonBusiness.Get(seasonId).Template;

                var templateModel = (emailTemplate != null) ? JsonConvert.DeserializeObject<EmailTemplateModel>(emailTemplate.Template) : new EmailTemplateModel();

                templateModel.Body = emailModel.BodyMailTitle;

                templateModel.IsRegisterationEmail = true;

                templateModel.Body = cartItemsBody;

                string body = ViewHelper.RenderViewToString(ControllerContext, Constants.Path_EmailTemplate, templateModel);


                Ioc.OldEmailBusiness.SendInstallmentReminderEmail(primaryPlayerEmail, clubOwnerEmail, emailSubject, body, emailModel.ClubName);

            }
            catch
            {
                throw;
            }
        }

        internal void SendInstallmentAutoChargeReminder(List<OrderInstallment> playerInstallment, int clubId)
        {
            try
            {
                var club = Ioc.ClubBusiness.Get(clubId);
                var accountingService = Ioc.AccountingBusiness;

                var orderDb = Ioc.OrderBusiness;
                var firstItem = playerInstallment.FirstOrDefault();
                var orderItem = Ioc.OrderItemBusiness.GetItem(firstItem.OrderItemId);


                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    ClubCurrency = club.Currency,
                    SeasonDomain = orderItem.Season.Domain,
                    PlayerFullName = orderItem.FullName,
                    InstallmentDate = firstItem.InstallmentDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US")),
                    BodyMailTitle = Constants.InstallmentReminderTitle,

                };


                emailModel.ReservationItems = new List<InstallmentReminderItem>();

                foreach (var item in playerInstallment.Where(c => c.Balance > 0))
                {
                    var orderitem = Ioc.OrderItemBusiness.GetItem(item.OrderItemId);
                    emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                    {
                        EventName = orderitem.Name,
                        ConfirmationId = orderitem.Order.ConfirmationId,
                        Price = item.Balance

                    });
                }

                var emailSubject = Constants.F_AutomaticInstallmentReminderSubject;

                var primaryPlayerEmail = orderItem.Order.User.UserName;
                var clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);
                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/AutoChargeInstallmentDueDateReminder.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInstallmentReminderEmail(primaryPlayerEmail, clubOwnerEmail, emailSubject, body, emailModel.ClubName);

            }
            catch (FormatException ex)
            {
                var playerInstallments = playerInstallment != null ? JsonHelper.JsonSerializer(playerInstallment) : " playerInstallment is Null";
                var message = $"{playerInstallments} : {clubId} ";
                LogHelper.LogException(ex, message);
            }
            catch (Exception ex)
            {
                var playerInstallments = playerInstallment != null ? JsonHelper.JsonSerializer(playerInstallment) : " playerInstallment is Null";
                var message = $"{playerInstallments} : {clubId} ";
                LogHelper.LogException(ex, message);
            }
        }
        public void SendInstallmentReminder(List<OrderInstallment> playerInstallment, string token)
        {
            try
            {
                if (playerInstallment == null)
                {
                    return;
                }
                var orderDb = Ioc.OrderBusiness;
                var firstItem = playerInstallment.FirstOrDefault();
                var orderItem = Ioc.OrderItemBusiness.GetItem(firstItem.OrderItemId);
                var accountingService = Ioc.AccountingBusiness;

                if (playerInstallment.Sum(c => c.Balance) <= 0)
                {
                    return;
                }
                var club = Ioc.ClubBusiness.Get(orderItem.Order.ClubId);
                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    ClubCurrency = club.Currency,
                    SeasonDomain = orderItem.Season.Domain,
                    PlayerFullName = orderItem.FullName,
                    InstallmentDate = firstItem.InstallmentDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US")),
                    BodyMailTitle = Constants.InstallmentReminderTitle,

                };
                var baseUrl = UrlHelpers.GetBaseUrl(emailModel.ClubDomain);
                var urlToRedirect = string.Format("{0}/Cart?actionCaller={1}&token={2}&backToDetail={3}", baseUrl, JumbulaSubSystem.Installment, token, false);

                emailModel.PaymentLink = urlToRedirect;

                emailModel.ReservationItems = new List<InstallmentReminderItem>();

                foreach (var item in playerInstallment.Where(c => c.Balance > 0))
                {
                    var orderitem = Ioc.OrderItemBusiness.GetItem(item.OrderItemId);
                    emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                    {
                        EventName = orderitem.Name,
                        ConfirmationId = orderitem.Order.ConfirmationId,
                        Price = item.Balance

                    });
                }

                var emailSubject = Constants.F_ManualInstallmentReminderSubject;

                var primaryPlayerEmail = orderItem.Order.User.UserName;
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);
                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/InstallmentDueDateReminder.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInstallmentReminderEmail(primaryPlayerEmail, clubOwnerEmail, emailSubject, body, emailModel.ClubName);

            }
            catch
            {
                throw;
            }
        }

        public void SendInstallmentReminderByToken(List<OrderInstallment> playerInstallment, string token)
        {
            try
            {
                if (playerInstallment == null)
                {
                    return;
                }
                var orderDb = Ioc.OrderBusiness;
                var firstItem = playerInstallment.FirstOrDefault();
                var orderItem = Ioc.OrderItemBusiness.GetItem(firstItem.OrderItemId);
                var accountingService = Ioc.AccountingBusiness;

                if (playerInstallment.Sum(c => c.Balance) <= 0)
                {
                    return;
                }
                var club = Ioc.ClubBusiness.Get(orderItem.Order.ClubId);
                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    ClubCurrency = club.Currency,
                    SeasonDomain = orderItem.Season.Domain,
                    PlayerFullName = orderItem.FullName,
                    InstallmentDate = firstItem.InstallmentDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US")),
                    BodyMailTitle = Constants.InstallmentReminderTitle,

                };
                var urlToRedirect = string.Format("{0}/Cart?actionCaller={1}&token={2}&backToDetail={3}", emailModel.RootSiteUrl, JumbulaSubSystem.Installment, token, false);

                emailModel.PaymentLink = urlToRedirect;

                emailModel.ReservationItems = new List<InstallmentReminderItem>();

                foreach (var item in playerInstallment.Where(c => c.Balance > 0))
                {
                    var orderitem = Ioc.OrderItemBusiness.GetItem(item.OrderItemId);
                    emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                    {
                        EventName = orderitem.Name,
                        ConfirmationId = orderitem.Order.ConfirmationId,
                        Price = item.Balance

                    });
                }

                var emailSubject = Constants.F_ManualInstallmentReminderSubject;

                var primaryPlayerEmail = orderItem.Order.User.UserName;
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);
                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/InstallmentDueDateReminder.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInstallmentReminderEmail(primaryPlayerEmail, clubOwnerEmail, emailSubject, body, emailModel.ClubName);

            }
            catch
            {
                throw;
            }
        }

        public void SendInstallmentReminderManually(List<OrderInstallment> playerInstallment, string token)
        {
            try
            {
                if (playerInstallment == null)
                {
                    return;
                }
                var orderDb = Ioc.OrderBusiness;
                var firstItem = playerInstallment.FirstOrDefault();
                var orderItem = Ioc.OrderItemBusiness.GetItem(firstItem.OrderItemId);
                var accountingService = Ioc.AccountingBusiness;

                if (playerInstallment.Sum(c => c.Balance) <= 0)
                {
                    return;
                }
                var club = Ioc.ClubBusiness.Get(orderItem.Order.ClubId);
                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    ClubCurrency = club.Currency,
                    SeasonDomain = orderItem.Season.Domain,
                    PlayerFullName = orderItem.FullName,
                    InstallmentDate = firstItem.InstallmentDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US")),
                    BodyMailTitle = Constants.InstallmentReminderTitle,

                };
                var urlToRedirect = string.Format("{0}/Cart?actionCaller={1}&token={2}&backToDetail={3}", emailModel.RootSiteUrl, JumbulaSubSystem.Installment, token, false);

                emailModel.PaymentLink = urlToRedirect;

                emailModel.ReservationItems = new List<InstallmentReminderItem>();

                foreach (var item in playerInstallment.Where(c => c.Balance > 0))
                {
                    var orderitem = Ioc.OrderItemBusiness.GetItem(item.OrderItemId);
                    emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                    {
                        EventName = orderitem.Name,
                        ConfirmationId = orderitem.Order.ConfirmationId,
                        Price = item.Balance

                    });
                }

                var emailSubject = Constants.F_ManualInstallmentReminderSubject;

                var primaryPlayerEmail = orderItem.Order.User.UserName;
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);
                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/InstallmentDueDateReminder.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInstallmentReminderEmail(primaryPlayerEmail, clubOwnerEmail, emailSubject, body, emailModel.ClubName);

            }
            catch
            {
                throw;
            }
        }

        public void SendEditCancelDropInEmail(OrderItem orderItem, string cid, List<DropInSessionsViewModel> sessions, int clubId, int userId, ReservationEmailMode emailMode, OrderMode orderMode = OrderMode.Online)
        {
            try
            {
                var club = Ioc.ClubBusiness.Get(clubId);
                var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);
                var partner = club.PartnerClub;
                var clubOfOrder = orderItem.Order.Club;
                bool hasPartner = partner != null ? true : false;
                var clubLogo = "";

                if (clubInfo.logo)
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo);
                }
                else
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
                }

                var totalAmount = orderItem.GetOrderChargeDiscounts().Sum(o => (decimal?)o.Amount) ?? 0;
                var emailModel = new EmailSubscriptionOrderViewModel()
                {
                    ClubImageUrl =
                        StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = clubInfo.Name,
                    MemberName = club.Name,
                    SeasonDomain = orderItem.Season.Domain,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    ClubLogo = clubLogo,
                    HasPartner = hasPartner,
                    InstallmentDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, club.TimeZone).ToString(Constants.DefaultDateTimeFormat), club.TimeZone.ToString()),
                    BodyMailTitle = emailMode == ReservationEmailMode.Edit ? "The following registration is modified." : "The following registration is cancelled.",
                    ConfirmationId = cid,
                    PlayerFullName = orderItem.FirstName + " " + orderItem.LastName,
                    EntryFee = orderItem.EntryFee,
                    TotalAmount = CurrencyHelper.FormatCurrencyWithPenny(totalAmount, clubOfOrder.Currency),
                    HasSessions = sessions.Count > 0 ? true : false
                };


                decimal chargeAmount = 0;
                decimal discountAmount = 0;

                if (emailModel.OrderDiscounts == null)
                {
                    emailModel.OrderDiscounts = new List<ReservationItemDetail>();
                }
                if (emailModel.OrderCharges == null)
                {
                    emailModel.OrderCharges = new List<ReservationItemDetail>();
                }

                foreach (var charge in orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge))
                {
                    chargeAmount = charge.Amount;
                    var programName = orderItem.ProgramSchedule.Program.Name;
                    if (charge.Category == ChargeDiscountCategory.EntryFee)
                    {
                        emailModel.OrderCharges.Add(new ReservationItemDetail { Category = charge.Category, DetailName = programName + "(" + charge.Name + ")", Price = CurrencyHelper.FormatCurrencyWithPenny(chargeAmount, clubOfOrder.Currency) });
                    }
                    else
                    {
                        emailModel.OrderCharges.Add(new ReservationItemDetail { Category = charge.Category, DetailName = charge.Name, Price = CurrencyHelper.FormatCurrencyWithPenny(chargeAmount, clubOfOrder.Currency) });
                    }

                }
                foreach (var charge in orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount))
                {
                    discountAmount = charge.Amount;
                    emailModel.OrderDiscounts.Add(new ReservationItemDetail { Category = charge.Category, DetailName = charge.Name, Price = CurrencyHelper.FormatCurrencyWithPenny(discountAmount, clubOfOrder.Currency) });
                }

                emailModel.ReservationItems = new List<EmailDropInSessionsViewModel>();
                foreach (var session in sessions)
                {
                    emailModel.ReservationItems.Add(new EmailDropInSessionsViewModel(club.Currency)
                    {
                        Date = session.StrDate,
                        NewDate = session.UpdatedDate.HasValue ? session.UpdatedDate.Value.ToString(Constants.DateTime_Comma) : null,
                        Status = session.Status,
                        Amount = CurrencyHelper.FormatCurrencyWithPenny(session.Amount, club.Currency)
                    });
                }

                var primaryPlayerEmail = Ioc.UserProfileBusiness.Get(userId).UserName;

                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                var emailSubject = emailMode == ReservationEmailMode.Edit ? string.Format(Constants.F_EditDropInSubject, primaryPlayerEmail) : string.Format(Constants.F_CancelDropInSubject, primaryPlayerEmail);

                string outSourcerClubOwnerEmail = string.Empty;

                string bodyEmail = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/EditCancelDropInItem.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendSubscriptionForAdminEmail(primaryPlayerEmail, clubOwnerEmail, emailSubject, bodyEmail, emailModel.ClubName);
            }

            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }


        }
        public void SendReservationEmail(string cid, PaymentMethod? paymentMethod, string hostName, long? orderItemId = null, ReservationEmailMode emailMode = ReservationEmailMode.Normal, OrderMode orderMode = OrderMode.Online)
        {
            Order order;
            try
            {

                var orderDb = Ioc.OrderBusiness;

                order = orderDb.GetOrdersByConfirmationID(cid);
                if (!hostName.ToLower().Contains(order.Club.Domain.ToLower()))
                {
                    hostName = string.Format("{0}.{1}", order.Club.Domain.ToLower(), hostName);
                }
                if (emailMode != ReservationEmailMode.Normal)
                {
                    var OrderItem = order.OrderItems.FirstOrDefault(o => o.Id == orderItemId.Value);

                    order.OrderItems.Clear();

                    order.OrderItems.Add(OrderItem);

                    order.OrderAmount = order.CalculateOrderAmount;
                }

                order.User = Ioc.UserProfileBusiness.Get(order.UserId);

                var cartItemsEmailModel = new ReservationNotificationViewModel();

                if (!order.RegularOrderItems.Any())
                {
                    return;
                }

                var clubOfOrder = order.Club;

                if (clubOfOrder.PartnerId.HasValue && clubOfOrder.IsSchool)
                {
                    cartItemsEmailModel.ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubOfOrder.PartnerClub.Domain, clubOfOrder.PartnerClub.Logo).AbsoluteUri;
                }
                else
                {
                    cartItemsEmailModel.ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubOfOrder.Domain, clubOfOrder.Logo).AbsoluteUri;
                }

                cartItemsEmailModel.ClubName = clubOfOrder.Name;
                cartItemsEmailModel.ClubEmail = clubOfOrder.ContactPersons.FirstOrDefault().Email;
                cartItemsEmailModel.ClubPhone = PhoneNumberHelper.FormatPhoneNumber(clubOfOrder.ContactPersons.FirstOrDefault().Phone);
                cartItemsEmailModel.OrderConfirmationId = order.ConfirmationId;
                cartItemsEmailModel.OrderTestLive = order.IsLive;
                cartItemsEmailModel.OrderUserName = order.User.UserName;
                cartItemsEmailModel.ClubUrl = cartItemsEmailModel.RootSiteUrl + "/" + clubOfOrder.Domain;


                cartItemsEmailModel.OrderPaidDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(order.CompleteDate, clubOfOrder.TimeZone).ToString(Constants.DefaultDateTimeFormat), clubOfOrder.TimeZone.ToString());
                var orderPaidAmount = CurrencyHelper.FormatCurrencyWithPenny(order.PaidAmount, clubOfOrder.Currency, true);
                cartItemsEmailModel.OrderPaidAmount = orderPaidAmount;
                var appliedText = string.Format("Applied: ({0})", orderPaidAmount);
                cartItemsEmailModel.PayPalInfo = string.Empty;
                cartItemsEmailModel.PaymentInstruction = string.Empty;

                ClientPaymentMethod clientPaymentMethod = null;

                if (clubOfOrder.PartnerId.HasValue && clubOfOrder.PartnerClub != null && clubOfOrder.IsSchool && (clubOfOrder.Domain.Equals("bc-alx-georgemason", StringComparison.OrdinalIgnoreCase) || (clubOfOrder.Domain.Equals("bc-alx-barrett", StringComparison.OrdinalIgnoreCase)) || (clubOfOrder.Domain.Equals("bc-arl-claremont", StringComparison.OrdinalIgnoreCase)) || (clubOfOrder.Domain.Equals("bc-alx-macarthur", StringComparison.OrdinalIgnoreCase)) || (clubOfOrder.Domain.Equals("bc-arl-arlingtonsciencefocus", StringComparison.OrdinalIgnoreCase)) || clubOfOrder.Domain.Equals("bc-arl-campbell", StringComparison.OrdinalIgnoreCase)))
                {
                    var client = Ioc.ClientBusiness.Get(clubOfOrder.Id);
                    clientPaymentMethod = client.PaymentMethods;
                }
                else
                {
                    clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(clubOfOrder.ClientId);
                }


                switch (paymentMethod)
                {
                    case PaymentMethod.Cash:
                        cartItemsEmailModel.CardType = "cash/check";
                        appliedText = string.Format("The amount of (${0}) to be paid by cash or check.", Ioc.OrderBusiness.CalculateOrderPayableAmount(order));
                        cartItemsEmailModel.PaymentInstruction = clientPaymentMethod.CashPaymentInstruction;
                        break;
                    case PaymentMethod.Paypal:
                        cartItemsEmailModel.CardType = "PayPal";
                        cartItemsEmailModel.PayPalInfo = string.Format(" - {0} ({1}){2}", cartItemsEmailModel.CardType, orderPaidAmount, cartItemsEmailModel.CardNumber);
                        break;
                    case PaymentMethod.Card:
                        cartItemsEmailModel.CardNumber = "";
                        cartItemsEmailModel.CardType = "credit card";
                        cartItemsEmailModel.PayPalInfo = string.Format(" - {0} ({1}){2}", cartItemsEmailModel.CardType, orderPaidAmount, cartItemsEmailModel.CardNumber);
                        break;
                    case PaymentMethod.Express:
                        cartItemsEmailModel.CardNumber = "";
                        cartItemsEmailModel.CardType = "credit card";
                        cartItemsEmailModel.PayPalInfo = string.Format(" - {0} ({1}){2}", cartItemsEmailModel.CardType, orderPaidAmount, cartItemsEmailModel.CardNumber);
                        break;
                    case PaymentMethod.Echeck:
                        cartItemsEmailModel.CardType = "eCheck";
                        cartItemsEmailModel.PayPalInfo = string.Format(" - {0} ({1})", cartItemsEmailModel.CardType, orderPaidAmount);
                        cartItemsEmailModel.PaymentInstruction = clientPaymentMethod.AchPaymentInstruction;
                        cartItemsEmailModel.PaymentMethodMessage = "This eCheck payment will be cleared in about 3-5 business days.";
                        break;
                    case PaymentMethod.BankTransfer:
                        cartItemsEmailModel.CardType = "bank transfer";
                        appliedText = paymentMethod.ToDescription();
                        cartItemsEmailModel.PaymentInstruction = clientPaymentMethod.BankTransferPaymentInstruction;
                        break;
                    case PaymentMethod.Scholarship:
                    case PaymentMethod.FinancialAid:
                    case PaymentMethod.Other:
                        cartItemsEmailModel.CardType = paymentMethod.ToDescription().ToLower();
                        appliedText = paymentMethod.ToDescription();
                        cartItemsEmailModel.PaymentInstruction = clientPaymentMethod.ManualPaymentInstruction;
                        break;
                    default:
                        appliedText = "";
                        cartItemsEmailModel.PaymentInstruction = "";
                        break;
                }

                if (order.OrderMode == OrderMode.Offline)
                {
                    cartItemsEmailModel.CardType = "";
                }
                cartItemsEmailModel.AppliedText = appliedText;

                if (emailMode == ReservationEmailMode.Cancel)
                {
                    cartItemsEmailModel.PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(order.OrderItems.Sum(s => s.TotalAmount), clubOfOrder.Currency, true);
                }
                else
                {
                    order.RegularOrderItems.Where(r => (r.ProgramTypeCategory == ProgramTypeCategory.Subscription || r.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && r.PaymentPlanType == PaymentPlanType.Installment).ToList().ForEach(t => t.TotalAmount = t.PaidAmount);

                    cartItemsEmailModel.PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(order.CalculateOrderAmount, clubOfOrder.Currency, true);
                }

                var primaryPlayerEmail = new List<string>();
                var secondaryPlayerEmail = new List<string>();

                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(clubOfOrder);

                List<string> outSourcerClubOwnerEmail = null;

                var seasonId = order.RegularOrderItems.Any(o => o.SeasonId.HasValue) ? order.RegularOrderItems.Last(o => o.SeasonId.HasValue).SeasonId.Value : order.Club.Seasons.Last().Id;

                foreach (var item in order.RegularOrderItems)
                {
                    var reservatioItem = new ReservationItem(order.Club.Currency);
                    if (item.ProgramScheduleId.HasValue)
                    {
                        reservatioItem.PlayerFullName = item.FullName;
                        reservatioItem.TotalAmount = CurrencyHelper.FormatCurrencyWithPenny(item.GetOrderChargeDiscounts().Sum(a => a.Amount), clubOfOrder.Currency, true);

                    }
                    else
                    {
                        reservatioItem.PlayerFullName = item.FirstName;
                    }

                    reservatioItem.PaymentPlanType = order.PaymentPlanType;

                    var ShowCombinedPriceOnlyForClub = false;
                    if (clubOfOrder.Setting != null)
                    {
                        ShowCombinedPriceOnlyForClub = clubOfOrder.Setting.ShowCombinedPriceOnly;

                        reservatioItem.EventName = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramName(item.ProgramSchedule, item.EntryFeeName) : "Donation";

                        if (!clubOfOrder.Setting.AppearanceSetting.HideProgramDatesInCart)
                        {
                            reservatioItem.EventDate = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramDate(item.ProgramSchedule) : "Donation";
                        }
                        else
                        {
                            reservatioItem.EventName = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName) : "Donation";

                        }
                    }
                    else
                    {
                        reservatioItem.EventName = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName) : "Donation";

                    }

                    reservatioItem.EventUrl = string.Empty;
                    if (item.ProgramScheduleId.HasValue && item.ProgramSchedule != null)
                    {
                        if (item.Season != null && item.ProgramSchedule != null && item.ProgramSchedule.Program != null)
                        {
                            reservatioItem.EventUrl = GetProgramUrl(item);
                        }

                    }
                    if (item.ProgramTypeCategory == ProgramTypeCategory.Class && item.ProgramScheduleId.HasValue)
                    {
                        foreach (var day in ((ScheduleAttribute)item.ProgramSchedule.Attributes).Days)
                        {
                            reservatioItem.DayAndTime += string.IsNullOrEmpty(reservatioItem.DayAndTime) ? "" : ", ";
                            if (day.StartTime.HasValue && day.EndTime.HasValue)
                            {
                                var startTime = new DateTime(1, 1, 1, day.StartTime.Value.Hours, day.StartTime.Value.Minutes, day.StartTime.Value.Seconds);
                                var endTime = new DateTime(1, 1, 1, day.EndTime.Value.Hours, day.EndTime.Value.Minutes, day.EndTime.Value.Seconds);
                                reservatioItem.DayAndTime += string.Format("{0} ({1} - {2})", day.DayOfWeek, startTime.ToString(Constants.DefaultTimeFormat), endTime.ToString(Constants.DefaultTimeFormat));
                            }
                            else
                            {
                                reservatioItem.DayAndTime += string.Format("{0}", day.DayOfWeek);
                            }
                        }
                    }
                    if (item.ProgramScheduleId.HasValue)
                    {
                        reservatioItem.EventAddress = item.ProgramSchedule.Program.ClubLocation.PostalAddress;
                    }

                    reservatioItem.Amount = item.TotalAmount;

                    if (reservatioItem.AdditionalParameters == null)
                    {
                        reservatioItem.AdditionalParameters = new List<string>();
                    }

                    if (reservatioItem.OrderDiscounts == null)
                    {
                        reservatioItem.OrderDiscounts = new List<ReservationItemDetail>();
                    }
                    if (reservatioItem.OrderCharges == null)
                    {
                        reservatioItem.OrderCharges = new List<ReservationItemDetail>();
                    }

                    if (item.ProgramScheduleId.HasValue)
                    {
                        var installmentItem = item.Installments != null ? item.Installments.Where(i => !i.IsDeleted).ToList().Count : 0;

                        if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                        {
                            if (installmentItem > 0)
                            {
                                reservatioItem.TotalAmount = cartItemsEmailModel.PaidAmount;
                            }
                            else
                            {
                                reservatioItem.TotalAmount = cartItemsEmailModel.PaidAmount;
                            }
                        }

                        foreach (var charge in item.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge))
                        {
                            decimal chargeAmount = charge.Amount;

                            if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                            {
                                if (item.ItemStatusReason == OrderItemStatusReasons.transferIn)
                                {
                                    if (item.Installments != null)
                                    {
                                        chargeAmount = charge.Amount / item.Installments.Where(i => i.Type == InstallmentType.Noraml).ToList().Count;
                                    }
                                }
                                else if (item.ItemStatusReason == OrderItemStatusReasons.canceled)
                                {
                                    if (charge.Category == ChargeDiscountCategory.ApplicationFee || charge.Category == ChargeDiscountCategory.CancellationFee || charge.Category == ChargeDiscountCategory.TransferFee || charge.Category == ChargeDiscountCategory.EditFee || charge.Category == ChargeDiscountCategory.ProrateFee)
                                    {
                                        chargeAmount = charge.Amount;

                                    }
                                    else
                                    {
                                        if (installmentItem > 0)
                                        {
                                            chargeAmount = charge.Amount != 0 ? charge.Amount / installmentItem : 0;

                                        }
                                        else
                                        {
                                            chargeAmount = charge.Amount != 0 ? charge.Amount : 0;
                                        }

                                    }
                                }
                                else
                                {
                                    chargeAmount = Ioc.SubscriptionBusiness.CalculateChargeForAppliedItems(item, charge);

                                }
                            }

                            if (charge.Category == ChargeDiscountCategory.EntryFee)
                            {
                                if (item.ProgramTypeCategory == ProgramTypeCategory.Class)
                                {
                                    chargeAmount = Ioc.CartBusiness.CalculateOrderItemTuitionPrice(item, 0);
                                    reservatioItem.OrderCharges.Add(new ReservationItemDetail { Type = charge.Category == ChargeDiscountCategory.EntryFee ? "Amount" : "Charge", Category = charge.Category, DetailName = charge.Name, Amount = chargeAmount, Price = CurrencyHelper.FormatCurrencyWithPenny(chargeAmount, clubOfOrder.Currency) });
                                }
                                else
                                {
                                    reservatioItem.OrderCharges.Add(new ReservationItemDetail { Type = charge.Category == ChargeDiscountCategory.EntryFee ? "Amount" : "Charge", Category = charge.Category, DetailName = charge.Name, Amount = chargeAmount, Price = CurrencyHelper.FormatCurrencyWithPenny(chargeAmount, clubOfOrder.Currency) });
                                }
                            }
                            else
                            {
                                if (!ShowCombinedPriceOnlyForClub)
                                {
                                    reservatioItem.OrderCharges.Add(new ReservationItemDetail { Type = charge.Category == ChargeDiscountCategory.EntryFee ? "Amount" : "Charge", Category = charge.Category, DetailName = charge.Name, Amount = chargeAmount, Price = CurrencyHelper.FormatCurrencyWithPenny(chargeAmount, clubOfOrder.Currency) });
                                }
                                else
                                {
                                    if (ShowCombinedPriceOnlyForClub && charge.OrderItem.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Class)
                                    {
                                        if (charge.Category != ChargeDiscountCategory.PartnerSurcharge && charge.Category != ChargeDiscountCategory.Surcharge)
                                        {
                                            reservatioItem.OrderCharges.Add(new ReservationItemDetail { Type = charge.Category == ChargeDiscountCategory.EntryFee ? "Amount" : "Charge", Category = charge.Category, DetailName = charge.Name, Amount = chargeAmount, Price = CurrencyHelper.FormatCurrencyWithPenny(chargeAmount, clubOfOrder.Currency) });
                                        }
                                    }
                                    else
                                    {
                                        reservatioItem.OrderCharges.Add(new ReservationItemDetail { Type = charge.Category == ChargeDiscountCategory.EntryFee ? "Amount" : "Charge", Category = charge.Category, DetailName = charge.Name, Amount = chargeAmount, Price = CurrencyHelper.FormatCurrencyWithPenny(chargeAmount, clubOfOrder.Currency) });

                                    }

                                }
                            }
                        }

                        foreach (var charge in item.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount))
                        {
                            decimal discountAmount = charge.Amount;

                            if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                            {
                                var instCount = item.Installments != null ? item.Installments.Where(i => i.Type == InstallmentType.Noraml && !i.IsDeleted).ToList().Count : 0;
                                var itemApplication = item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.ApplicationFee || c.Category == ChargeDiscountCategory.PartnerApplicationFee).ToList();
                                if (item.ItemStatusReason == OrderItemStatusReasons.transferIn || item.ItemStatusReason == OrderItemStatusReasons.canceled)
                                {
                                    if (instCount > 0)
                                    {
                                        decimal amount = 0;

                                        if (charge.CouponId.HasValue)
                                        {
                                            if (charge.Coupon.CouponCalculateType == CalculationType.TotalAmount && itemApplication.Count > 0)
                                            {
                                                amount = charge.Amount / (instCount + 1);
                                            }
                                            else
                                            {
                                                amount = charge.Amount / instCount;
                                            }
                                        }
                                        else
                                        {
                                            amount = charge.Amount / instCount;
                                        }

                                        discountAmount = amount != 0 ? amount : 0;
                                    }
                                    else
                                    {
                                        discountAmount = charge.Amount != 0 ? charge.Amount : 0;
                                    }
                                }
                                else
                                {
                                    if (charge.DiscountId != null)
                                    {
                                        discountAmount = item.PaymentPlanType == PaymentPlanType.FullPaid ? Ioc.SubscriptionBusiness.CalculateDiscountForAppliedItems(item, charge) : Ioc.SubscriptionBusiness.CalculateDiscountForAppliedItems(item, charge);
                                    }
                                    else
                                    {
                                        discountAmount = item.PaymentPlanType == PaymentPlanType.FullPaid ? Ioc.SubscriptionBusiness.CalculateDiscountForAppliedItems(item, charge) : Ioc.SubscriptionBusiness.CalculateDiscountForAppliedItems(item, charge);
                                        if (item.Mode != OrderItemMode.DropIn && item.Mode != OrderItemMode.PunchCard && item.PaymentPlanType != PaymentPlanType.FullPaid)
                                        {
                                            discountAmount = Ioc.SubscriptionBusiness.CalculateDiscountForShowInView(item, charge, discountAmount);
                                        }
                                    }
                                }
                            }

                            reservatioItem.OrderDiscounts.Add(new ReservationItemDetail { Category = charge.Category, DetailName = charge.Name, Amount = discountAmount, Price = CurrencyHelper.FormatCurrencyWithPenny(discountAmount, clubOfOrder.Currency, true) });
                        }
                    }
                    //else
                    //{
                    //    reservatioItem.Price = Utilities.FormatCurrencyWithPenny(item.TotalAmount, clubOfOrder.Currency);
                    //}

                    if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        if (item.PaymentPlanType == PaymentPlanType.Installment)
                        {
                            var beforeAfterCarePart = new ProgramSchedulePart();
                            var subscriptionItem = new ProgramSubscriptionItem();
                            decimal amount = 0;
                            var selectedCharge = Ioc.SubscriptionBusiness.GetSelectedCharge(item);

                            if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                            {
                                amount = selectedCharge.Amount;
                            }
                            else
                            {
                                beforeAfterCarePart = Ioc.SubscriptionBusiness.GetAppliedProgramPart(item.ProgramSchedule, item.DesiredStartDate);
                                var programPartCharges = Ioc.ProgramBusiness.GetSchedulePartCharges(selectedCharge);


                                if (Ioc.SubscriptionBusiness.IsProrateProgramPart(beforeAfterCarePart, item.ProgramSchedule, item.DesiredStartDate))
                                {
                                    var selectedDays = item.Attributes.WeekDays;
                                    amount = Ioc.SubscriptionBusiness.CalculateBeforeAfterCareProrateAmount(beforeAfterCarePart, item.DesiredStartDate.Value, selectedCharge, selectedDays);
                                }
                                else
                                {
                                    amount = programPartCharges.Where(c => c.SchedulePartId == beforeAfterCarePart.Id).FirstOrDefault().ChargeAmount;
                                }
                            }


                            reservatioItem.Amount = Ioc.SubscriptionBusiness.CalculateSubscriptionPayableAmount(item);

                            if (clubOfOrder.Setting != null)
                            {
                                reservatioItem.EventName = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramName(item.ProgramSchedule, item.EntryFeeName) : "Donation";

                                if (!clubOfOrder.Setting.AppearanceSetting.HideProgramDatesInCart)
                                {
                                    reservatioItem.EventDate = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramDate(item.ProgramSchedule) : "Donation";
                                }
                                else
                                {
                                    reservatioItem.EventName = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName) : "Donation";

                                }
                            }
                            else
                            {
                                reservatioItem.EventName = Ioc.ProgramBusiness.GetEmailProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName);
                            }

                            reservatioItem.OrderCharges.Where(o => o.Category == ChargeDiscountCategory.EntryFee).ToList().ForEach(r => r.Price = CurrencyHelper.FormatCurrencyWithPenny(amount, clubOfOrder.Currency, true));
                            reservatioItem.OrderCharges.Where(o => o.Category == ChargeDiscountCategory.EntryFee).ToList().ForEach(r => r.Amount = amount);
                            item.TotalAmount = item.PaidAmount;

                            if (item.ItemStatusReason == OrderItemStatusReasons.transferIn && (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare))
                            {
                                if (order.CalculateOrderAmount == 0)
                                {
                                    cartItemsEmailModel.PaidAmount = reservatioItem.Price;
                                }
                            }

                            if (cartItemsEmailModel.PaidAmount == CurrencyHelper.FormatCurrencyWithPenny(0, clubOfOrder.Currency, true) && order.CalculateOrderAmount != 0)
                            {
                                cartItemsEmailModel.PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(amount, clubOfOrder.Currency, true);
                            }

                            reservatioItem.AdditionalParameters.Add(string.Format("Desired start date: {0}", item.DesiredStartDate.Value.ToShortDateString()));

                            if (item.ItemStatusReason == OrderItemStatusReasons.transferIn || item.ItemStatusReason == OrderItemStatusReasons.canceled)
                            {
                                if (item.ItemStatusReason == OrderItemStatusReasons.transferIn)
                                {
                                    reservatioItem.AdditionalParameters.Add(string.Format("Effective transfer date: {0}", item.Attributes.TransferEffectiveDate.Value.ToShortDateString()));
                                }
                                else
                                {
                                    reservatioItem.AdditionalParameters.Add(string.Format("Effective cancellation date: {0}", item.Attributes.CancelEffectiveDate.Value.ToShortDateString()));
                                }
                            }

                        }
                        else
                        {
                            if (clubOfOrder.Setting != null)
                            {
                                reservatioItem.EventName = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramName(item.ProgramSchedule, item.EntryFeeName) : "Donation";

                                if (!clubOfOrder.Setting.AppearanceSetting.HideProgramDatesInCart)
                                {

                                    reservatioItem.EventDate = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramDate(item.ProgramSchedule) : "Donation";
                                }
                                else
                                {
                                    reservatioItem.EventName = item.ProgramScheduleId.HasValue ? Ioc.ProgramBusiness.GetEmailProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName) : "Donation";

                                }
                            }
                            else
                            {
                                reservatioItem.EventName = Ioc.ProgramBusiness.GetEmailProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName);
                            }

                            if (item.Mode == OrderItemMode.DropIn)
                            {
                                foreach (var session in item.OrderSessions.Select(p => p.ProgramSession).ToList().OrderBy(s => s.StartDateTime.Day))
                                {
                                    reservatioItem.AdditionalParameters.Add(session.StartDateTime.ToString("MMM dd") + " " + session.StartDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", "") + "-" + session.EndDateTime.ToString("h:mm tt", CultureInfo.InvariantCulture).ToLower().Replace(" ", ""));
                                }

                            }
                        }

                        try
                        {
                            //var days = item.OrderSessions.Select(s => s.ProgramSession.StartDateTime.DayOfWeek).Distinct().OrderBy(o => o).ToList();
                            if (item.Attributes != null && item.Attributes.WeekDays.Any())
                            {
                                var days = item.Attributes.WeekDays.OrderBy(o => o).ToList();
                                reservatioItem.AdditionalParameters.Add(string.Format("Days: {0}", string.Join(", ", days)));
                            }
                        }
                        catch
                        {
                            ErrorSignal.FromCurrentContext().Raise(new Exception("Error in add subscription days to email."));
                        }
                    }

                    if (item.ProgramTypeCategory == ProgramTypeCategory.Camp && item.Mode == OrderItemMode.DropIn)
                    {
                        var sessions = Ioc.OrderItemBusiness.ShowItemSessions(item);

                        foreach (var session in sessions)
                        {
                            reservatioItem.AdditionalParameters.Add(session);
                        }
                    }

                    if (cartItemsEmailModel.ReservationItems == null)
                    {
                        cartItemsEmailModel.ReservationItems = new List<ReservationItem>();
                    }

                    if (reservatioItem.EventName == "Donation")
                    {
                        reservatioItem.OrderCharges.Add(new ReservationItemDetail { Type = "Amount", Category = ChargeDiscountCategory.Donation, DetailName = reservatioItem.EventName, Amount = reservatioItem.Amount, Price = CurrencyHelper.FormatCurrencyWithPenny(reservatioItem.Amount, clubOfOrder.Currency, true) });
                    }

                    cartItemsEmailModel.ReservationItems.Add(reservatioItem);
                }

                var mainAddress = string.Empty;

                mainAddress = string.Empty;

                if (!cartItemsEmailModel.ReservationItems.Any(c => c.EventAddress != null && c.EventAddress.Address != mainAddress))
                {
                    if (cartItemsEmailModel.ReservationItems[0].EventAddress != null)
                    {
                        cartItemsEmailModel.Location = mainAddress;
                        cartItemsEmailModel.Lat = cartItemsEmailModel.ReservationItems[0].EventAddress.Lat.ToString();
                        cartItemsEmailModel.Lng = cartItemsEmailModel.ReservationItems[0].EventAddress.Lng.ToString();
                    }
                }

                if (Ioc.FollowupBusiness.HasFollowupForm(order))
                {
                    cartItemsEmailModel.FollowUpForms += ViewHelper.RenderViewToString(ControllerContext, "~/Views/Email/_FollowupFormTable.cshtml", order);
                }

                order.OrderInstallments = Ioc.InstallmentBusiness.MergeOrderItemInstallments(order);

                var installmentModel = new EmailReservationInstallmentModel(order, emailMode);
                if (installmentModel.Items.Any())
                {
                    cartItemsEmailModel.InstallmentsBody = ViewHelper.RenderViewToString(ControllerContext, "~/Views/Shared/Email/_OrderInstallmentsTable.cshtml", installmentModel);
                }

                //Calculate sum subtotal
                foreach (var item in order.RegularOrderItems)
                {
                    cartItemsEmailModel.OrderTotalAmount += item.TotalAmount;
                }

                cartItemsEmailModel.EmailMode = emailMode;
                cartItemsEmailModel.OrderMode = orderMode;
                cartItemsEmailModel.Currency = order.Club.Currency;
                string cartItemsBody = ViewHelper.RenderViewToString(ControllerContext, Constants.Path_ConfirmReservationBody, cartItemsEmailModel);

                var emailTemplate = Ioc.SeasonBusiness.Get(seasonId).Template;

                var templateModel = (emailTemplate != null) ? JsonConvert.DeserializeObject<EmailTemplateModel>(emailTemplate.Template) : new EmailTemplateModel(); // new EmailTemplateModel();

                templateModel.IsRegisterationEmail = true;

                templateModel.Body = cartItemsBody;


                if (!string.IsNullOrEmpty(templateModel.HeaderBackground) && !templateModel.HeaderBackground.Contains("http:") && !templateModel.HeaderBackground.Contains("https:"))
                {
                    templateModel.HeaderBackground = GetImageURL(templateModel.HeaderBackground, templateModel.ClubDomain, "emailTemplate");
                }
                if (!string.IsNullOrEmpty(templateModel.FooterBackground) && !templateModel.FooterBackground.Contains("http:") && !templateModel.FooterBackground.Contains("https:"))
                {
                    templateModel.FooterBackground = GetImageURL(templateModel.FooterBackground, templateModel.ClubDomain, "emailTemplate");
                }

                templateModel.ClubLogo = Ioc.ClubBusiness.GetClubLogo(order.Club);

                #region Addresses
                if (templateModel.UseProgramLocation)
                {

                    List<PostalAddress> programAddresses = order.RegularOrderItems.Where(o => o.ProgramScheduleId.HasValue).Select(o => o.ProgramSchedule.Program.ClubLocation.PostalAddress).ToList();

                    var lng = string.Empty;
                    var lat = string.Empty;
                    if (programAddresses != null && programAddresses.Count > 0)
                    {
                        if (programAddresses.Where(p => p.Id == programAddresses[0].Id).Count() != programAddresses.Count())
                        {
                            templateModel.HasMap = false;
                        }
                        else
                        {
                            lng = programAddresses[0].Lng.ToString();
                            lat = programAddresses[0].Lat.ToString();

                            templateModel.MapLocation = string.Format("{0},{1}", lat, lng);
                        }
                    }
                }
                #endregion

                var emailSubject = string.Empty;

                switch (emailMode)
                {
                    case ReservationEmailMode.Normal:
                        {
                            emailSubject = Constants.F_ConfirmReservationSubject;
                        }
                        break;
                    case ReservationEmailMode.Transfer:
                        {
                            emailSubject = Constants.F_ConfirmTransferSubject;

                            templateModel.CongratulationMessage = "The registration is transferred to the following program.";
                        }
                        break;
                    case ReservationEmailMode.Edit:
                        {
                            emailSubject = "Edit reservation";

                            templateModel.CongratulationMessage = "The following registration is modified.";
                        }
                        break;
                    case ReservationEmailMode.Cancel:
                        {
                            emailSubject = "Cancel reservation";

                            templateModel.CongratulationMessage = "The following registration is canceled.";
                        }
                        break;
                    default:
                        break;
                }
                templateModel.IsTest = !order.IsLive;
                string body = ViewHelper.RenderViewToString(ControllerContext, Constants.Path_EmailTemplate, templateModel);

                primaryPlayerEmail.Add(order.User.UserName);

                Ioc.OldEmailBusiness.SendReservationEmail(primaryPlayerEmail, secondaryPlayerEmail, clubOwnerEmail, outSourcerClubOwnerEmail, emailSubject, body, cartItemsEmailModel.ClubName);

                return;
            }
            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }

            throw new Exception();
        }

        private string GetProgramUrl(OrderItem orderItem)
        {
            var result = string.Empty;
            var clubDomain = string.Empty;
            var seasonDomain = string.Empty;
            var programDomain = string.Empty;

            if (orderItem.ProgramScheduleId.HasValue)
            {
                clubDomain = orderItem.ProgramSchedule.Program.Club.Domain;
                seasonDomain = orderItem.ProgramSchedule.Program.Season.Domain;
                programDomain = orderItem.ProgramSchedule.Program.Domain;
            }

            var baseUrl = UrlHelpers.GetBaseUrl(clubDomain);

            result = string.Format("{0}/{1}/{2}/View", baseUrl, seasonDomain, programDomain);

            return result;
        }

        public void SendInstallmentPaymentConfirmation(long installmentId)
        {
            try
            {
                var firstItem = Ioc.InstallmentBusiness.Get(installmentId);

                var order = firstItem.OrderItem.Order;
                var club = Ioc.ClubBusiness.Get(order.ClubId);
                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    SeasonDomain = firstItem.OrderItem.Season.Domain,
                    PlayerFullName = firstItem.OrderItem.FullName,
                    InstallmentDate = firstItem.PaidDate.Value.ToString(Constants.DefaultDateFormat),
                    BodyMailTitle = Constants.InstallmentConfirmationBodyTitle,

                };
                var emailSubject = Constants.F_CashConfirmEmailSubject;
                emailModel.ReservationItems = new List<InstallmentReminderItem>();


                emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                {
                    EventName = firstItem.OrderItem.Name,
                    ConfirmationId = firstItem.OrderItem.Order.ConfirmationId,
                    Price = firstItem.TransactionActivities.LastOrDefault().Amount

                });


                emailModel.PayPalInfo = string.Format(" - Credit card payment ({0})", emailModel.TotalAmount);
                emailModel.AppliedText = string.Format("Applied: ({0})", emailModel.TotalAmount);


                var primaryPlayerEmail = order.User.UserName;
                var secondaryPlayerEmail = firstItem.OrderItem.Player.Contact != null ? firstItem.OrderItem.Player.Contact.Email : string.Empty;
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                string outSourcerClubOwnerEmail = string.Empty;

                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/OldInstallmentPaymentConfirmation.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInstallmentConfirmationEmail(primaryPlayerEmail, clubOwnerEmail, outSourcerClubOwnerEmail, emailSubject, body, emailModel.ClubName);
            }

            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }


        }
        public void SendInstallmentPaymentConfirmation(string token, DateTime transactionDate, bool isPaypal)
        {
            try
            {

                var installment = Ioc.InstallmentBusiness.GetListByToken(token).Where(i => !i.IsDeleted).ToList();
                if (installment == null)
                {
                    return;
                }

                var firstItem = installment.FirstOrDefault();
                var order = Ioc.OrderItemBusiness.GetItem(firstItem.OrderItemId).Order;
                var club = Ioc.ClubBusiness.Get(order.ClubId);
                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    SeasonDomain = firstItem.OrderItem.Season.Domain,
                    PlayerFullName = firstItem.OrderItem.FullName,
                    InstallmentDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(firstItem.PaidDate.Value, club.TimeZone).ToString(Constants.DefaultDateTimeFormat), club.TimeZone.ToString()),
                    BodyMailTitle = Constants.InstallmentConfirmationBodyTitle,

                };
                var emailSubject = Constants.F_CashConfirmEmailSubject;
                emailModel.ReservationItems = new List<InstallmentReminderItem>();

                foreach (var item in installment.Where(c => c.TransactionActivities.Any(t => t.TransactionDate.Equals(transactionDate))))
                {
                    emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                    {
                        EventName = item.OrderItem.Name,
                        ConfirmationId = item.OrderItem.Order.ConfirmationId,
                        Price = item.TransactionActivities.First(c => c.TransactionDate.Equals(transactionDate)).Amount

                    });
                }

                if (isPaypal)
                {
                    emailModel.PayPalInfo = string.Format(" - PayPal payment ({0})", emailModel.TotalAmount);
                }
                else
                {
                    emailModel.PayPalInfo = string.Format("Credit card payment ({0})", emailModel.TotalAmount);
                }
                emailModel.AppliedText = string.Format("Applied: ({0})", emailModel.TotalAmount);


                var primaryPlayerEmail = order.User.UserName;
                var secondaryPlayerEmail = firstItem.OrderItem.Player.Contact != null ? firstItem.OrderItem.Player.Contact.Email : string.Empty;
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                string outSourcerClubOwnerEmail = string.Empty;

                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/OldInstallmentPaymentConfirmation.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInstallmentConfirmationEmail(primaryPlayerEmail, clubOwnerEmail, outSourcerClubOwnerEmail, emailSubject, body, emailModel.ClubName);
            }

            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }
        }

        public void SendReceivedPaymentEmail(OrderItem orderitem, decimal totalPaidAmount, DateTime transactionDate, PaymentDetail paymentDetail, bool isEdited = false)
        {
            try
            {
                var club = Ioc.ClubBusiness.Get(orderitem.Order.ClubId);
                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl =
                        StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    SeasonDomain = orderitem.Season != null ? orderitem.Season.Domain : null,
                    PlayerFullName = orderitem.FullName,
                    InstallmentDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(transactionDate, club.TimeZone).ToString(Constants.DefaultDateTimeFormat), club.TimeZone.ToString()),
                    BodyMailTitle =
                        (!isEdited) ? Constants.ConfirmPreapprovalPaymentTitle : Constants.ConfirmEditPaymentTitle,

                };
                var emailSubject = (!isEdited) ? Constants.F_CashConfirmEmailSubject : Constants.F_CashEditedEmailSubject;
                emailModel.ReservationItems = new List<InstallmentReminderItem>();

                emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                {
                    EventName = orderitem.Name,
                    ConfirmationId = orderitem.Order.ConfirmationId,
                    Price = totalPaidAmount

                });


                emailModel.PayPalInfo = string.Format(" {0} payment ({1}) ", paymentDetail.PaymentMethod.ToDescription(), emailModel.TotalAmount);

                var primaryPlayerEmail = orderitem.Order.User.UserName;

                if (orderitem.Player != null)
                {
                    var secondaryPlayerEmail = orderitem.Player.Contact != null ? orderitem.Player.Contact.Email : string.Empty;
                }

                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                string outSourcerClubOwnerEmail = string.Empty;

                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/OldInstallmentPaymentConfirmation.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInstallmentConfirmationEmail(primaryPlayerEmail, clubOwnerEmail, outSourcerClubOwnerEmail, emailSubject, body, emailModel.ClubName);
            }

            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }
        }


        public void SendRegisterError(string response, PaymentModels model, int userid, string userCustomerId, PostalAddress address, Core.Domain.Club club)
        {
            try
            {
                var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club != null ? club.Name : string.Empty);

                var emailModel = new EmailRegisterErrorModel()
                {
                    CardNumber = model.CardNumber,
                    CardHolderName = model.CardHolderName,
                    ClubName = club.Name,
                    Month = model.Month,
                    Year = model.Year,
                    Userid = userid,
                    Address = address != null ? address.Address : string.Empty,
                    City = address != null ? address.City : string.Empty,
                    Zip = address != null ? address.Zip : string.Empty,
                    State = address != null ? address.State : string.Empty,
                    Country = address != null ? address.Country : string.Empty,
                    UserCustomerId = userCustomerId,
                    Response = response
                };

                string emailSubject = "Error in create customer payment profile";

                var primaryPlayerEmail = string.Empty;

                List<string> clubOwnerEmail = new List<string>();
                clubOwnerEmail.Add("support@jumbula.com");
                clubOwnerEmail.Add("maryam@jumbula.com");

                string outSourcerClubOwnerEmail = string.Empty;

                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/RegisterErrorMessage.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendRegisterErrorEmail(primaryPlayerEmail, clubOwnerEmail, outSourcerClubOwnerEmail, emailSubject, body, emailModel.ClubName);
            }

            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }
        }

        public void SendReceivedFromCreditCardFamilyTakePaymentEmail(List<TransactionActivity> transactions, DateTime transactionDate, PaymentDetail paymentDetail, int userId, int clubId)
        {
            try
            {
                var club = Ioc.ClubBusiness.Get(clubId);
                var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);
                var partner = club.PartnerClub;
                bool hasPartner = partner != null ? true : false;
                var clubLogo = "";
                if (clubInfo.logo)
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo);
                }
                else
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
                }
                var emailModel = new EmailFamilyTakePaymentModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = clubInfo.Name,
                    MemberName = club.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    ClubLogo = clubLogo,
                    HasPartner = hasPartner,
                    InstallmentDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(transactionDate, club.TimeZone).ToString(Constants.DefaultDateTimeFormat), club.TimeZone.ToString()),
                    BodyMailTitle = Constants.ConfirmFamilyTakePaymentTitle,
                };
                emailModel.ReservationItems = new List<FamilyTakePaymentItem>();

                foreach (var transaction in transactions)
                {
                    emailModel.ReservationItems.Add(new FamilyTakePaymentItem(club.Currency)
                    {
                        EventName = transaction.OrderItem.Name,
                        PlayerFullName = transaction.OrderItem.FullName,
                        ConfirmationId = transaction.OrderItem.Order.ConfirmationId,
                        Price = transaction.Amount
                    });
                }

                emailModel.PayPalInfo = string.Format(" {0} payment ({1}) ", paymentDetail.PaymentMethod.ToDescription(), emailModel.TotalAmount);
                var primaryPlayerEmail = Ioc.UserProfileBusiness.Get(userId).UserName;

                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                var adminEmailSubject = string.Format(Constants.F_AdminTakePaymentSubject, primaryPlayerEmail);
                var parentEmailSubject = string.Format(Constants.F_ParentTakePaymentSubject, clubInfo.Name);

                string outSourcerClubOwnerEmail = string.Empty;

                string bodyEmailForAdmin = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/FamilyTakeaPaymentConfirmation.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendTakePaymentForAdminEmail(primaryPlayerEmail, clubOwnerEmail, adminEmailSubject, bodyEmailForAdmin, emailModel.ClubName);

                string bodyEmailForParent = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/FamilyTakeaPaymentConfirmation.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendTakePaymentForParentEmail(primaryPlayerEmail, parentEmailSubject, bodyEmailForParent, emailModel.ClubName);
            }

            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }
        }

        public void SendReceivedFamilyTakePaymentEmail(List<OrderItem> selectedDBOrderItems, List<FamilyOrderItemsViewModel> selectedOrderItems, DateTime transactionDate, PaymentDetail paymentDetail, int userId, int clubId, bool sendAdminEmail, bool sendParentEmail)
        {
            try
            {
                var club = Ioc.ClubBusiness.Get(clubId);
                var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);
                var partner = club.PartnerClub;
                bool hasPartner = partner != null ? true : false;
                var clubLogo = "";
                if (clubInfo.logo)
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo);
                }
                else
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
                }

                var emailModel = new EmailFamilyTakePaymentModel()
                {
                    ClubImageUrl =
                        StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = clubInfo.Name,
                    MemberName = club.Name,
                    SeasonDomain = selectedOrderItems.FirstOrDefault().SeasonDomain,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    ClubLogo = clubLogo,
                    HasPartner = hasPartner,
                    InstallmentDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, club.TimeZone).ToString(Constants.DefaultDateTimeFormat), club.TimeZone.ToString()),
                    BodyMailTitle = Constants.ConfirmFamilyTakePaymentTitle,

                };
                emailModel.ReservationItems = new List<FamilyTakePaymentItem>();
                foreach (var orderitem in selectedOrderItems)
                {
                    emailModel.ReservationItems.Add(new FamilyTakePaymentItem(club.Currency)
                    {
                        EventName = orderitem.ProgramName,
                        ConfirmationId = orderitem.ConfirmationId,
                        PlayerFullName = orderitem.Attendee,
                        Price = orderitem.Installments.Any() ? orderitem.Installments.Where(i => i.Checked).Sum(i => i.Balance.Value) : orderitem.Balance
                    });
                }

                emailModel.PayPalInfo = string.Format(" {0} payment ({1}) ", paymentDetail.PaymentMethod.ToDescription(), emailModel.TotalAmount);
                var primaryPlayerEmail = Ioc.UserProfileBusiness.Get(userId).UserName;

                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                var adminEmailSubject = string.Format(Constants.F_AdminTakePaymentSubject, primaryPlayerEmail);
                var parentEmailSubject = string.Format(Constants.F_ParentTakePaymentSubject, clubInfo.Name);

                string outSourcerClubOwnerEmail = string.Empty;

                string bodyEmail = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/FamilyTakeaPaymentConfirmation.cshtml", emailModel);
                if (sendAdminEmail)
                {
                    Ioc.OldEmailBusiness.SendTakePaymentForAdminEmail(primaryPlayerEmail, clubOwnerEmail, adminEmailSubject, bodyEmail, emailModel.ClubName);

                }
                if (sendParentEmail)
                {
                    Ioc.OldEmailBusiness.SendTakePaymentForParentEmail(primaryPlayerEmail, parentEmailSubject, bodyEmail, emailModel.ClubName);
                }

            }

            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }
        }
        public void SendErrorMessaged(string errorMessage)
        {
            Ioc.OldEmailBusiness.SendErrorMessages("support@jumbula.com", errorMessage);
            Ioc.OldEmailBusiness.SendErrorMessages("azadeh@jumbula.com", errorMessage);

        }
        public void SendRegisterErrorMessaged(string errorMessage)
        {
            Ioc.OldEmailBusiness.SendErrorMessages("support@jumbula.com", errorMessage);
            Ioc.OldEmailBusiness.SendErrorMessages("maryam@jumbula.com", errorMessage);

        }
        public void SendUpdateRasInstallment(string errorMessage)
        {
            Ioc.OldEmailBusiness.SendUpdateRasInstallment("support@jumbula.com", errorMessage);
        }
      
        public void SendInvoice(OrderItem orderItem, string token, decimal dueAmount, DateTime? nullable)
        {
            try
            {
                if (orderItem == null)
                {
                    return;
                }

                var club = Ioc.ClubBusiness.Get(orderItem.Order.ClubId);

                var emailModel = new EmailReminderModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = club.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    SeasonDomain = orderItem.Season.Domain,
                    PlayerFullName = orderItem.FullName,
                    InstallmentDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, club.TimeZone).ToString(Constants.DefaultDateTimeFormat), club.TimeZone.ToString()),
                    BodyMailTitle = Constants.InvoiceTitle,

                };

                emailModel.PaymentLink = emailModel.RootSiteUrl + "/cart/MakePayment?Token=" + token;


                emailModel.ReservationItems = new List<InstallmentReminderItem>();


                emailModel.ReservationItems.Add(new InstallmentReminderItem(club.Currency)
                {
                    EventName = orderItem.Name,
                    ConfirmationId = orderItem.Order.ConfirmationId,
                    Price = dueAmount

                });


                var emailSubject = Constants.F_InvoiceSubject;

                var primaryPlayerEmail = orderItem.Order.User.UserName;
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);
                string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/InstallmentDueDateReminder.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInstallmentReminderEmail(primaryPlayerEmail, clubOwnerEmail, emailSubject, body, emailModel.ClubName);

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }

        public void SendFamilyInvoice(List<FamilyOrderItemsViewModel> model, string token, decimal dueAmount, int clubId, int userId, Invoice invoice)
        {
            try
            {
                if (model == null)
                {
                    return;
                }

                var club = Ioc.ClubBusiness.Get(clubId);

                var transactions = Ioc.InvoiceBusiness.GetInvoiceTransactions(invoice.Id).GroupBy(o => o.OrderId);
                var confirmationIds = new List<string>();
                foreach (var tran in transactions)
                {
                    var confermation = tran.First().Order.ConfirmationId;
                    confirmationIds.Add(confermation);
                }

                var primaryPlayerEmail = Ioc.UserProfileBusiness.Get(userId).UserName;
                var userParents = Ioc.UserProfileBusiness.GetUserParents(userId);

                var parentName = "";
                if (userParents.Any())
                {
                    parentName = userParents.First().Contact.FullName;
                }
                var clubLogo = "";
                var partner = club.PartnerClub;
                bool hasPartner = partner != null ? true : false;
                var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);
                if (clubInfo.logo)
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo);
                }
                else
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
                }

                var dueDate = invoice.InvoicingDate.AddDays(invoice.DueDate.Value);
                var emailModel = new EmailReminderFamilyInvoiceModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = clubInfo.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = club.ContactPersons.FirstOrDefault().Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    SeasonDomain = model.First().SeasonDomain,
                    InstallmentDate = string.Format("{0} ({1})", DateTimeHelper.ConvertUtcDateTimeToLocal(DateTime.UtcNow, club.TimeZone).ToString(Constants.DefaultDateTimeFormat), club.TimeZone.ToString()),
                    BodyMailTitle = Constants.InvoiceTitle,
                    TotalAmount = invoice.Amount,
                    NotToRecipeint = invoice.NotToRecipient,
                    ParentEmail = primaryPlayerEmail,
                    ParentName = parentName != null ? parentName : primaryPlayerEmail,
                    InvoiceNumber = invoice.InvoiceNumber,
                    InvoiceId = invoice.Id,
                    DueDate = dueDate,
                    ClubLogo = clubLogo,
                    MemberName = club.Name,
                    HasPartner = hasPartner,
                    ConfirmationIds = confirmationIds
                };

                foreach (var orderitem in model)
                {
                    emailModel.ReservationItems.Add(new OrderItemFamilyInvoiceReminderItem(club.Currency)
                    {
                        EventName = orderitem.ProgramName,
                        ConfirmationId = orderitem.ConfirmationId,
                        PlayerFullName = orderitem.Attendee,
                        Price = orderitem.Installments.Any() ? orderitem.Installments.Where(i => i.Checked).Sum(i => i.Balance.Value) : orderitem.Balance
                    });
                }

                emailModel.ViewLinkForAdmin = emailModel.RootSiteUrl + "/Dashboard#/Invoicing/Invoice/Details/" + invoice.Id;
                emailModel.ViewLinkForAdminViewParent = emailModel.RootSiteUrl + "/Dashboard#/Invoicing/ParentInvoice/Details/" + invoice.Id;
                emailModel.ViewLinkForParent = emailModel.RootSiteUrl + "/Registrant/InvoiceDetail?invoiceId=" + invoice.Id;

                var date = DateTime.UtcNow;
                var username = invoice.User.UserName;
                var invoiceHistory = new InvoiceHistory() { InvoiceId = invoice.Id, ActionDate = date, Action = Invoicestatus.Initiated, Description = string.Format(Constants.Invoice_Sent, CurrencyHelper.FormatCurrencyWithPenny(invoice.Amount, club.Currency), username) };
                Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);


                var adminEmailSubject = string.Format(Constants.F_AdminInvoiceSubject, primaryPlayerEmail);
                var parentEmailSubject = string.Format(Constants.F_ParentInvoiceSubject, clubInfo.Name);

                var ccEmailList = new List<MailAddress>();
                if (invoice.EmailToCc != null && invoice.EmailToCc.Any())
                {
                    ccEmailList.Add(new MailAddress(invoice.EmailToCc));
                }


                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                string bodyEmailForAdmin = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/InvoiceAdminEmail.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInvoiceForAdminEmail(primaryPlayerEmail, clubOwnerEmail, adminEmailSubject, bodyEmailForAdmin, emailModel.ClubName);

                string bodyEmailForParent = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/InvoiceParentEmail.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInvoiceForParentEmail(primaryPlayerEmail, parentEmailSubject, bodyEmailForParent, emailModel.ClubName, ccEmailList);


            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }
        public void SendEmailForHandelErrorFromSendgrid(Campaign campaign, string message)
        {
            try
            {
                var club = campaign.Club;
                var emailModel = new EmailHandelErrorSendgridViewModel()
                {
                    ClubDomain = club.Domain,
                    ClubName = club.Name,
                    TextError = message,
                };
                var subject = string.Format(Constants.F_Cancel_ErrorSendgrid, club.Name);
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);
                var body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/SendgridError.cshtml", emailModel);

                var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
                Ioc.OldEmailBusiness.SendEmailForErrorSendGrid(subject, body, club.Name);
            }
            catch (Exception ex)
            {

                LogHelper.LogException(ex);
            }

        }
        public void SendCancelationAndReminderInvoice(Invoice invoice, bool IsCancel, bool SendMailToRecipient, string Note, string ccEmail)
        {
            try
            {
                if (invoice == null)
                {
                    return;
                }

                var club = invoice.Club;
                var firstTransaction = Ioc.InvoiceBusiness.GetInvoiceTransactions(invoice.Id).First();
                var orderItem = firstTransaction.OrderItem;
                var primaryPlayerEmail = Ioc.UserProfileBusiness.Get(invoice.UserId).UserName;
                var userParents = Ioc.UserProfileBusiness.GetUserParents(invoice.UserId);
                var parentName = "";
                if (userParents.Any())
                {
                    parentName = userParents.First().Contact.FullName;
                }
                var clubLogo = "";
                var partner = club.PartnerClub;
                var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);
                if (clubInfo.logo)
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo);
                }
                else
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
                }
                var dueDate = invoice.InvoicingDate.AddDays(invoice.DueDate.Value);
                var emailModel = new EmailReminderFamilyInvoiceModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = clubInfo.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = clubInfo.Email,
                    ClubPhone = PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone),
                    ClubDomain = club.Domain,
                    SeasonDomain = orderItem.Season.Domain,
                    PlayerFullName = orderItem.FullName,
                    ClubLogo = clubLogo,
                    InvoiceNumber = invoice.InvoiceNumber,
                    InvoiceId = invoice.Id,
                    ParentEmail = primaryPlayerEmail,
                    ParentName = parentName != null ? parentName : primaryPlayerEmail,
                    TotalAmount = invoice.Amount,
                    NotToRecipeint = Note,
                    DueDate = dueDate
                };

                var paymantDetailId = Ioc.PaymentDetailBusiness.GetByInvoiceId(invoice.Id).Id;
                var invoiceTransactions = Ioc.TransactionActivityBusiness.GetList().Where(p => p.PaymentDetailId == paymantDetailId);
                var orderItems = invoiceTransactions.GroupBy(t => t.OrderItem);

                foreach (var items in orderItems)
                {
                    var itemTransaction = items.ToList();
                    var amount = itemTransaction.Sum(t => t.Amount);
                    var confirmationId = itemTransaction.First().Order.ConfirmationId;
                    var programName = itemTransaction.First().OrderItem.Name;
                    var participantName = itemTransaction.First().OrderItem.FullName;

                    emailModel.ReservationItems.Add(new OrderItemFamilyInvoiceReminderItem(club.Currency)
                    {
                        PlayerFullName = participantName,
                        ConfirmationId = confirmationId,
                        EventName = programName,
                        Price = amount
                    });
                }

                var url = System.Web.HttpContext.Current.Request.Url;
                var partnerDomain = url.Authority.Split('.')[0];
                var clubDomain = club.Domain;
                var mainUrl = url.Authority.Replace(partnerDomain, clubDomain);
                var rootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, mainUrl);

                emailModel.ViewLinkForAdmin = rootSiteUrl + "/Dashboard#/Invoicing/Invoice/Details/" + invoice.Id;
                emailModel.ViewLinkForAdminViewParent = rootSiteUrl + "/Dashboard#/Invoicing/ParentInvoice/Details/" + invoice.Id;
                emailModel.ViewLinkForParent = rootSiteUrl + "/Registrant/InvoiceDetail?invoiceId=" + invoice.Id;

                var AdminEmailSubject = "";
                var parentEmailSubject = "";
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);
                string adminBody = "";
                string parentBody = "";

                var ccEmailList = new List<MailAddress>();
                if (!string.IsNullOrEmpty(ccEmail))
                {
                    ccEmailList.Add(new MailAddress(ccEmail));
                }

                if (IsCancel)
                {
                    AdminEmailSubject = string.Format(Constants.Invoice_AdminCancelationSubject, primaryPlayerEmail);
                    adminBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/AdminCancelationInvoice.cshtml", emailModel);
                    if (SendMailToRecipient)
                    {
                        parentEmailSubject = string.Format(Constants.Invoice_ParentCancelationSubject, clubInfo.Name);
                        parentBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ParentCancelationInvoice.cshtml", emailModel);
                    }

                }
                else
                {
                    AdminEmailSubject = string.Format(Constants.Invoice_AdminReminderSubject, primaryPlayerEmail);
                    adminBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/AdminReminderInvoice.cshtml", emailModel);

                    parentEmailSubject = string.Format(Constants.Invoice_ParentReminderSubject, clubInfo.Name);
                    parentBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ParentReminderInvoice.cshtml", emailModel);
                }

                Ioc.OldEmailBusiness.SendCancellationOrReminderEmailToAdmin(primaryPlayerEmail, clubOwnerEmail, AdminEmailSubject, adminBody, emailModel.ClubName);
                if (SendMailToRecipient)
                {
                    Ioc.OldEmailBusiness.SendCancellationOrReminderEmailToParent(primaryPlayerEmail, parentEmailSubject, parentBody, emailModel.ClubName, ccEmailList);
                }


            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
        }
        public void SendReceivedFamilyInvoicePaymentEmail(List<TransactionActivity> transactions, decimal totalPaidAmount, DateTime transactionDate, PaymentDetail paymentDetail, Invoice invoice, bool isEdited = false)
        {

            try
            {
                var club = invoice.Club;
                var transactionId = Ioc.PaymentDetailBusiness.GetByInvoiceId(invoice.Id).TransactionId;

                var transactionsgroup = transactions.GroupBy(t => t.OrderId);
                var confirmationIds = new List<string>();
                foreach (var tran in transactionsgroup)
                {
                    var confermation = tran.First().Order.ConfirmationId;
                    confirmationIds.Add(confermation);
                }
                var userId = invoice.UserId;
                var userParents = Ioc.UserProfileBusiness.GetUserParents(userId);
                //this.GetCurrentUserName();
                var primaryPlayerEmail = Ioc.UserProfileBusiness.Get(userId).UserName;
                var parentName = "";
                if (userParents.Any())
                {
                    parentName = userParents.First().Contact.FullName;
                }
                var dueDate = invoice.InvoicingDate.AddDays(invoice.DueDate.Value);
                var clubLogo = "";
                var partner = club.PartnerClub;
                var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);
                if (clubInfo.logo)
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo);
                }
                else
                {
                    clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
                }
                var playerfullName = transactions.First().OrderItem.FullName;

                var emailModel = new EmailReminderFamilyInvoiceModel()
                {
                    ClubImageUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, club.Logo).AbsoluteUri,
                    ClubName = clubInfo.Name,
                    ClubCurrency = club.Currency,
                    ClubEmail = clubInfo.Email,
                    ClubDomain = club.Domain,
                    ClubAddress = clubInfo.Address,
                    PayPalInfo = paymentDetail.PaymentMethod.ToDescription(),
                    ClubLogo = clubLogo,
                    TotalAmount = invoice.Amount,
                    NotToRecipeint = invoice.NotToRecipient,
                    InvoiceNumber = invoice.InvoiceNumber,
                    InvoiceId = invoice.Id,
                    TransactionID = transactionId,
                    ParentEmail = primaryPlayerEmail,
                    ParentName = parentName != null ? parentName : primaryPlayerEmail,
                    AmountDue = invoice.Amount - invoice.PaidAmount,
                    MemberName = club.Name,
                    ConfirmationIds = confirmationIds

                };

                var paymantDetailId = Ioc.PaymentDetailBusiness.GetByInvoiceId(invoice.Id).Id;
                var invoiceTransactions = Ioc.TransactionActivityBusiness.GetList().Where(p => p.PaymentDetailId == paymantDetailId);
                var orderItems = invoiceTransactions.GroupBy(t => t.OrderItem);

                foreach (var items in orderItems)
                {
                    var itemTransaction = items.ToList();
                    var amount = itemTransaction.Sum(t => t.Amount);
                    var confirmationId = itemTransaction.First().Order.ConfirmationId;
                    var programName = string.Empty;
                    var participantName = itemTransaction.First().OrderItem.FullName;

                    if (itemTransaction.First().OrderItem.ProgramScheduleId.HasValue && itemTransaction.First().OrderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && itemTransaction.First().OrderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription)
                    {
                        programName = Ioc.ProgramBusiness.GetProgramName(itemTransaction.First().OrderItem.ProgramSchedule, itemTransaction.First().OrderItem.EntryFeeName);
                    }
                    else
                    {
                        programName = itemTransaction.First().OrderItem.Name;
                    }

                    emailModel.ReservationItems.Add(new OrderItemFamilyInvoiceReminderItem(club.Currency)
                    {
                        PlayerFullName = participantName,
                        ConfirmationId = confirmationId,
                        EventName = programName,
                        Price = amount
                    });
                }

                emailModel.ViewLinkForParent = emailModel.RootSiteUrl + "/Registrant/InvoiceDetail?invoiceId=" + invoice.Id;
                emailModel.ViewLinkForAdmin = emailModel.RootSiteUrl + "/Dashboard#/Invoicing/Invoice/Details/" + invoice.Id;

                var ccEmailList = new List<MailAddress>();
                if (invoice.EmailToCc != null && invoice.EmailToCc.Any())
                {
                    ccEmailList.Add(new MailAddress(invoice.EmailToCc));
                }

                var userName = invoice.User.UserName;
                //create subject
                var AdminEmailSubject = string.Format(Constants.F_AdminInvoiceEmail, userName, invoice.InvoiceNumber);
                var ParentEmailSubject = string.Format(Constants.F_ParentInvoiceEmail, clubInfo.Name, invoice.InvoiceNumber);

                var firstOrderitem = transactions.First().OrderItem;
                emailModel.PayPalInfo = string.Format("{0} payment", paymentDetail.PaymentMethod.ToDescription());

                var secondaryPlayerEmail = (firstOrderitem.Player != null && firstOrderitem.Player.Contact != null) ? firstOrderitem.Player.Contact.Email : string.Empty;
                List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

                string outSourcerClubOwnerEmail = string.Empty;
                //for admin
                string Adminbody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/AdminInvoicePaymentConfirmation.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInvoicePaymentForAdminEmail(primaryPlayerEmail, clubOwnerEmail, outSourcerClubOwnerEmail, AdminEmailSubject, Adminbody, emailModel.ClubName);
                //for parent
                string Parentbody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ParentInvoicePaymentConfirmation.cshtml", emailModel);
                Ioc.OldEmailBusiness.SendInvoicePaymentForParentEmail(primaryPlayerEmail, clubOwnerEmail, outSourcerClubOwnerEmail, ParentEmailSubject, Parentbody, emailModel.ClubName, ccEmailList);
            }
            catch (System.Net.Mail.SmtpException e)
            {
                LogHelper.LogException(e);
                throw e;
            }
        }

        public void SendProviderRespondToInvitation(ProgramRespondToInvitation respond)
        {
            var program = Ioc.ProgramBusiness.Get(respond.ProgramId);
            var model = new ProviderRespondToInvationEmailModel()
            {
                ProgramName = program.Name,
                RespondInt = (int)respond.RespondType,
                RespondStr = respond.RespondType.ToString(),
                Note = respond.Respond.Note
            };

            var partner = Ioc.ClubBusiness.Get(program.Season.Club.PartnerId.Value);
            model.PartnerName = partner.Name;
            model.ProviderName = program.Season.Club.Name;

            if (respond.RespondType == ProgramRespondType.Accept)
            {
                var respondSetting = ((AcceptRespond)respond.Respond);
                if (respondSetting.AvailableAnyDay)
                {
                    model.PreferedDays = "Available any day";
                }
                else
                {
                    model.PreferedDays = respondSetting.FirstDayPreference.ToString();
                    if ((int)respondSetting.SecondDayPreference > 0)
                    {
                        model.PreferedDays += ", " + respondSetting.SecondDayPreference.ToString();
                    }
                }
            }

            else if (respond.RespondType == ProgramRespondType.Decline)
            {
                var respondSetting = ((DeclineRespond)respond.Respond);
                model.Reason = respondSetting.DeclineResaon.ToDescription();
                model.ReasonInt = (int)respondSetting.DeclineResaon;

            }

            var emailSubject = string.Format(Constants.F_ProviderRespondToInvitationSubject, program.Club.Name, program.Season.Title);


            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(partner);
            string body = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/EMProviderRespondToInvitation.cshtml", model);
            Ioc.OldEmailBusiness.SendProviderRespondToInvitation(clubOwnerEmail, emailSubject, body, partner.Name);
        }

        public void SendAddedToWaitlist(string userEmail, string programName, string clubName, string homePageUrl, string logoUrl, string contactEmail, string clubDomain, string seasonTitle)
        {
            var model = new SendAddedToWaitlistEmailModel();
            var club = Ioc.ClubBusiness.Get(clubDomain);

            model.UserEmail = userEmail;
            model.ProgramName = programName;
            model.HomeTitle = clubName;
            model.ClubDomain = clubDomain;
            model.SeasonTitle = seasonTitle;
            model.HomePageUrl = homePageUrl;
            model.LogoUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, logoUrl).AbsoluteUri;
            model.ContactEmail = contactEmail;

            string emailSubject = Constants.AddedToWaitlistEmailSubject;

            string emailBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/AddedToWaitlistEmail.cshtml", model);
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);

            Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(userEmail), emailSubject, emailBody, true);

            if (WebConfigHelper.IsProductionMode)
            {
                //Send to support
                Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, emailBody, true);
            }
        }

        public void SendAddedToWaitlistForAdmin(string date, string participant, string grade, string userEmail, string programName, string clubName, string homePageUrl, string logoUrl, string contactEmail, string clubDomain)
        {
            var model = new SendAddedToWaitlistEmailModel();
            var club = Ioc.ClubBusiness.Get(clubDomain);


            model.UserEmail = userEmail;
            model.Participant = participant;
            model.ProgramName = programName;
            model.HomeTitle = clubName;
            model.HomePageUrl = homePageUrl;
            model.Date = date;
            model.Grade = grade;
            model.LogoUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, logoUrl).AbsoluteUri;
            model.ContactEmail = contactEmail;

            string emailSubject = Constants.AddedToWaitlistEmailSubject;
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);

            string emailBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/AddedAdminToWaitlistEmailForAdmin.cshtml", model);
            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

            Ioc.OldEmailBusiness.SendWaitListForAdmin(fromClubOwner, clubOwnerEmail, emailSubject, emailBody);

        }

        public void ParentNotificationChangeEmail(string userEmail, string newEmail, string logoUrl, string clubDomain)
        {
            var model = new ParentNotificationChangeEmailViewModel();
            var club = Ioc.ClubBusiness.Get(clubDomain);


            model.UserEmail = userEmail;
            model.NewUserEmail = newEmail;
            model.LogoUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, logoUrl).AbsoluteUri;
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
            string emailSubject = Constants.ChangeEmailLogin;

            string emailBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ParentNotificationChangeEmail.cshtml", model);

            Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(model.UserEmail), emailSubject, emailBody, true);
        }
        public void ParentNotificationChangePassword(string userEmail, string logoUrl, string clubDomain)
        {
            var model = new ParentNotificationChangePasswordViewModel();
            var club = Ioc.ClubBusiness.Get(clubDomain);
            model.UserEmail = userEmail;
            model.LogoUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, logoUrl).AbsoluteUri;

            string emailSubject = Constants.ChangePassword;
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
            string emailBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/ParentNotificationChangePassword.cshtml", model);

            Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(model.UserEmail), emailSubject, emailBody, true);
        }

        internal void SendWaitlistForAdmin(string date, string participant, string grade, string userEmail, string programName, Core.Domain.Club club, string homePageUrl, string logoUrl, string contactEmail, string clubDomain)
        {
            var Model = new SendAddedToWaitlistEmailModel();

            Model.UserEmail = userEmail;
            Model.ProgramName = programName;
            Model.HomeTitle = club.Name;
            Model.HomePageUrl = homePageUrl;
            Model.LogoUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, logoUrl).AbsoluteUri;
            Model.ContactEmail = contactEmail;
            Model.Participant = participant;
            Model.Date = date;
            Model.Grade = grade;
            var emailSubject = string.Format("Waitlist request from " + userEmail);

            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

            string emailBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/AddedToWaitlistEmailForAdmin.cshtml", Model);
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
            Ioc.OldEmailBusiness.SendWaitListForAdmin(fromClubOwner, clubOwnerEmail, emailSubject, emailBody);
        }
        internal void DeleteWaitlistForAdmin(string date, string participant, string grade, string userEmail, string programName, Core.Domain.Club club, string homePageUrl, string contactEmail, string logoUrl)
        {
            var Model = new SendAddedToWaitlistEmailModel();

            Model.UserEmail = userEmail;
            Model.ProgramName = programName;
            Model.HomeTitle = club.Name;
            Model.HomePageUrl = homePageUrl;
            Model.ContactEmail = contactEmail;
            Model.Participant = participant;
            Model.Date = date;
            Model.Grade = grade;
            Model.LogoUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, logoUrl).AbsoluteUri;

            var emailSubject = string.Format("Waitlist request canceled by " + userEmail);

            List<string> clubOwnerEmail = Ioc.ClubBusiness.ClubOwnerEmailList(club);

            string emailBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/DeletedWaitlistEmailForAdmin.cshtml", Model);
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);
            Ioc.OldEmailBusiness.SendWaitListForAdmin(fromClubOwner, clubOwnerEmail, emailSubject, emailBody);
        }
        internal void DeleteWaitlistForParent(string date, string participant, string grade, string userEmail, string programName, Core.Domain.Club club, string homePageUrl, string contactEmail, string logoUrl)
        {
            var Model = new SendAddedToWaitlistEmailModel();

            Model.UserEmail = userEmail;
            Model.ProgramName = programName;
            Model.HomeTitle = club.Name;
            Model.HomePageUrl = homePageUrl;
            Model.ContactEmail = contactEmail;
            Model.Participant = participant;
            Model.Date = date;
            Model.Grade = grade;
            Model.LogoUrl = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, logoUrl).AbsoluteUri;

            var emailSubject = string.Format("Waitlist request canceled by " + userEmail);


            string emailBody = ViewHelper.RenderViewToString(ControllerContext, @"~/Views/Shared/Email/DeletedWaitlistEmailForParent.cshtml", Model);
            var fromClubOwner = new MailAddress(Constants.W_NoReplyEmailAddress, club.Name);

            Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(userEmail), emailSubject, emailBody, true, null);

            ////send email to support
            if (WebConfigHelper.IsProductionMode)
            {
                Ioc.OldEmailBusiness.SendEmail(fromClubOwner, new MailAddress(Constants.W_BCC_EmailNotification), emailSubject, emailBody, true);
            }
        }


        private bool IsTimeForSend(OrderSession orderSession, AgendaSettings agendaSettings)
        {
            var result = false;

            var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(orderSession.ProgramSession.ProgramSchedule.Program.Club, DateTime.UtcNow);

            var clubDate = clubDateTime.Date;

            if (agendaSettings != null && agendaSettings.IsEnabled)
            {
                var sendDateTime = Ioc.ProgramSessionBusiness.GetAgendaDeliverDate(orderSession.ProgramSession);

                if (sendDateTime <= clubDateTime && sendDateTime >= clubDate)
                {
                    return true;
                }
            }

            return result;
        }

        public string GetImageURL(string fileName, string clubDomain, string folderName)
        {
            string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, $@"{clubDomain}\{folderName}", fileName).AbsoluteUri;

            imageUri = string.Concat(imageUri, "?", "rnd=", Guid.NewGuid());

            if (string.IsNullOrEmpty(fileName))
            {
                imageUri = string.Empty;
            }

            return imageUri;
        }

        public void SendMessagingEmail(string reciver, string sender, string message, string clubDomain, string messageId, string clubLogo, string programName, string clubName, string reciverClubDomain, bool senderIsPartner)
        {
            var model = new MessagingEmailViewModel()
            {
                Message = message,
                Reciver = reciver,
                Sender = sender,
                MessageUrl =
                      $"{UrlHelpers.GetBaseUrl(reciverClubDomain)}/Dashboard#/Messaging/{messageId}",
                LogoUrl = UrlHelpers.GetClubLogoUrl(clubDomain, clubLogo, true),
                ProgramName = programName,
                ClubName = clubName
            };

            var body = ViewHelper.RenderViewToString(ControllerContext, "~/Views/Shared/Email/Messaging.cshtml", model);
            var from = new MailAddress(Constants.W_NoReplyEmailAddress);
            MailAddress to;

            to = new MailAddress(reciver);

            var joinString = senderIsPartner ? "from" : "by";

            Ioc.OldEmailBusiness.SendEmail(
                from, to, null,
                $"Session request: {programName} {joinString} {clubName}",
                body, true
            );
        }

    }
}