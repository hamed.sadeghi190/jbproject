﻿(function (angular) {
    'use strict';

    angular
        .module('jbTools')
        .directive('jbPopover', function () {
            return {
                restrict: 'A',
                link: function ($scope, element) {
                    element.unbind("click").unbind("mouseout").click(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).popover("show");
                    }).mouseout(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        $(this).popover("hide");
                    });
                }
            };
        });
})(angular);
