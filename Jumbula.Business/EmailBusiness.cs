﻿using Autofac.Features.Indexed;
using Jumbula.Common.Delegates;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Infrastructure;
using Jumbula.Core.Infrastructure.Email;
using Jumbula.Core.Model.Email;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Jumbula.Business
{
    public class EmailBusiness : IEmailBusiness
    {
        #region Fields
        private readonly IEmailRepository _emailRepository;
        private readonly IIndex<EmailCategory, IEmailBuilder> _builder;
        private readonly bool _isProductionMode;
        private readonly IJbDateTime _jbDateTime;
        private readonly IJbEmailService _jbEmailService;
        private readonly IJbLocker _jbLocker;

        #endregion

        #region Constructors
        public EmailBusiness(IEmailRepository emailRepository, IIndex<EmailCategory, IEmailBuilder> builder, DeploymentEnvironment deploymentEnvironment, IJbDateTime jbDateTime, IJbEmailService jbEmailService, IJbLocker jbLocker)
        {
            _emailRepository = emailRepository;
            _builder = builder;
            _isProductionMode = deploymentEnvironment == DeploymentEnvironment.Production || deploymentEnvironment == DeploymentEnvironment.QA;
            _jbDateTime = jbDateTime;
            _jbEmailService = jbEmailService;
            _jbLocker = jbLocker;
        }
        #endregion

        #region Public Methods

        public void Send<TEmailParameter>(EmailCategory emailCategory, TEmailParameter parameters, DateTime? scheduleDateTimeUtc = null) where TEmailParameter : class
        {
            var emailBuilder = GetEmailBuilder<TEmailParameter>(emailCategory);

            var emails = Queue(emailBuilder, parameters, scheduleDateTimeUtc);

            if (emailBuilder.EmailSendPriority == EmailSendPriority.High)
                Send(emails);
        }

        public void Send(EmailViewModel emailViewModel)
        {
            var emailBuilder = GetEmailBuilder(emailViewModel.Category);

            var email = emailBuilder.GetEmail(emailViewModel);

            _emailRepository.Create(email);

            if (emailBuilder.EmailSendPriority == EmailSendPriority.High)
                Send(email);
        }

        public void SendAllQueued(EmailCategory? emailCategory = null)
        {
            bool lockWasTaken = false;

            try
            {
                if (_jbLocker.TryLock(JbWorker.Email))
                {
                    Log.Information("Sending queued emails started");

                    lockWasTaken = true;

                    var allQueued = GetAllQueued(emailCategory).ToList();

                    foreach (var emailId in allQueued)
                    {
                        Send(emailId);
                    }

                    _emailRepository.Save();
                }
            }
            finally
            {
                if (lockWasTaken)
                {
                    _jbLocker.UnLock(JbWorker.Email);

                    Log.Information("Sending queued emails finished");
                }
            }
        }

        public string GetPreview(int id)
        {
            var email = _emailRepository.Get(id);

            return GetEmailBuilder(email.Category).GenerateBody(email, EmailBodyGenerationMode.Preview);
        }

        public string GetPreview(EmailViewModel emailViewModel)
        {
            var emailBuilder = GetEmailBuilder(emailViewModel.Category);

            return emailBuilder.GeneratePreviewBody(emailViewModel);
        }

        public string GetPreview<TEmailParameter>(EmailCategory emailCategory, TEmailParameter parameters, string subCategory = null)
            where TEmailParameter : class
        {
            var emailBuilder = GetEmailBuilder<TEmailParameter>(emailCategory);

            return emailBuilder.GeneratePreviewBody(parameters, subCategory);
        }

        public EmailViewModel GetViewModel<TEmailParameter>(EmailCategory emailCategory, TEmailParameter parameters) where TEmailParameter : class
        {
            var emailBuilder = GetEmailBuilder<TEmailParameter>(emailCategory);

            return emailBuilder.GetEmailViewModel(parameters);
        }


        #endregion

        #region Private Methods

        private List<Email> Queue<TEmailParameter>(IEmailBuilder<TEmailParameter> emailBuilder, TEmailParameter parameters, DateTime? scheduleDateTimeUtc = null) where TEmailParameter : class
        {
            var result = new List<Email>();

            try
            {
                result = emailBuilder.GetEmails(parameters, _isProductionMode).ToList();

                result.ForEach(e =>
                {
                    e.ScheduleDate = scheduleDateTimeUtc;
                });

                _emailRepository.Create(result);

                Log.Information("Emails with category {EmailCategory} successfully queued.", emailBuilder.Category);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Emails with category {EmailCategory} failed to queue", emailBuilder.Category);
            }

            return result;
        }

        private IEnumerable<int> GetAllQueued(EmailCategory? emailCategory = null)
        {
            var utcNow = _jbDateTime.UtcNow;

            var emails = _emailRepository.GetPendingEmails(emailCategory).Where(e => !e.ScheduleDate.HasValue || (e.ScheduleDate.HasValue && e.ScheduleDate.Value <= utcNow)).OrderBy(o => o.Priority).ThenBy(o => o.DateCreated).Select(e => e.Id);

            return emails;
        }

        private void Send(IEnumerable<Email> emails)
        {
            foreach (var email in emails)
            {
                Send(email);
            }
        }

        private void Send(int emailId)
        {
            var email = _emailRepository.Get(emailId, e => e.Recipients);

            Send(email);
        }


        private void Send(Email email)
        {
            try
            {
                email.Status = EmailSendStatus.Pending;
                email.LastAttemptTime = _jbDateTime.UtcNow;
                email.AttemptNumber++;

                _emailRepository.Save();

                var emailBuilder = GetEmailBuilder(email.Category);

                var body = emailBuilder.GenerateBody(email);

                var tos = email.Recipients.Where(r => r.Type == EmailRecipientType.To).Select(MapMailAddress);
                var ccs = email.Recipients.Where(r => r.Type == EmailRecipientType.Cc).Select(MapMailAddress);
                var bCCs = email.Recipients.Where(r => r.Type == EmailRecipientType.Bcc).Select(MapMailAddress);

                var replyTo = !string.IsNullOrWhiteSpace(email.ReplyTo) ? new MailAddress(email.ReplyTo) : null;

                Send(new MailAddress(email.FromEmail, email.FromDisplayName), tos, email.Subject, body, replyTo, true, email.Id, ccs, bCCs, email.Priority, email.IsCampaignMode, email.Attachments);
            }
            catch (Exception ex)
            {
                SetEmailSendStatus(email.Id, false, null, ex);
                Log.Error(ex, "Email {EmailId} with category {EmailCategory} failed to send", email.Id, email.Category);
            }
        }

        private void Send(MailAddress from, IEnumerable<MailAddress> tos, string subject, string body, MailAddress replyTo, bool isBodyHtml, int emailId, IEnumerable<MailAddress> ccs, IEnumerable<MailAddress> bCCs, EmailSendPriority sendPriority = EmailSendPriority.Normal, bool isCampaignMode = false, IEnumerable<EmailAttachment> attachments = null)
        {
            var mail = new MailMessage
            {
                From = from,
                Subject = subject,
                Body = body,
                IsBodyHtml = isBodyHtml
            };

            if (replyTo != null)
                mail.ReplyToList.Add(replyTo);

            foreach (var to in tos)
            {
                mail.To.Add(to);
            }

            foreach (var cc in ccs)
            {
                mail.CC.Add(cc);
            }

            foreach (var bcc in bCCs)
            {
                mail.Bcc.Add(bcc);
            }

            mail.Priority = MapMailPriority(sendPriority);

            ManipulateAttachments(mail.Attachments, attachments);

            _jbEmailService.Send(mail, emailId, SendCompletedCallback, isCampaignMode);
        }

        private void Send(MailMessage mailMessage, int emailId)
        {
            _jbEmailService.Send(mailMessage, emailId, SendCompletedCallback);
        }

        private MailAddress MapMailAddress(EmailRecipient emailRecipient)
        {
            return new MailAddress(emailRecipient.EmailAddress, emailRecipient.DisplayName);
        }

        private void ManipulateAttachments(AttachmentCollection attachmentCollection, IEnumerable<EmailAttachment> attachments)
        {
            if (attachments != null)
            {
                foreach (var item in attachments)
                {
                    attachmentCollection.Add(MapAttachment(item));
                }
            }
        }

        private static Attachment MapAttachment(EmailAttachment emailAttachment)
        {
            return new Attachment(RequestHelper.GetStreamFromUrl(emailAttachment.FileUrl),emailAttachment.FileName);
        }

        private MailPriority MapMailPriority(EmailSendPriority emailSendPriority)
        {
            return EnumHelper.ParseEnum<MailPriority>(emailSendPriority.ToString());
        }

        private void SendCompletedCallback(object sender, EmailCompletedEventArgs e)
        {
            SetEmailSendStatus(e.EmailId, e.Succeed, e.Response, e.Exception);
        }

        private void SetEmailSendStatus(int emailId, bool success, string response, Exception exception)
        {
            var email = _emailRepository.Get(emailId);

            email.Response = response;

            if (success)
            {
                email.Status = EmailSendStatus.Processed;
                email.SentDate = _jbDateTime.UtcNow;
                Log.Information("Email {EmailId}  with category {EmailCategory} sent", email.Id, email.Category);
            }
            else
            {
                email.Status = EmailSendStatus.Failed;
                Log.Error(exception, "Email {EmailId} with category {EmailCategory} failed to send", email.Id, email.Category);
            }

            _emailRepository.Update(email);
        }

        private IEmailBuilder<TEmailParameter> GetEmailBuilder<TEmailParameter>(EmailCategory emailCategory) where TEmailParameter : class
        {
            return GetEmailBuilder(emailCategory) as IEmailBuilder<TEmailParameter>;
        }

        private IEmailBuilder GetEmailBuilder(EmailCategory emailCategory)
        {
            return _builder[emailCategory];
        }


        #endregion
    }
}
