﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class Donation : BaseEntity<long>
    {
        [Key, ForeignKey("OrderChargeDiscount")]
        public override long Id
        {
            get
            {
                return base.Id;
            }
            set
            {
                base.Id = value;
            }
        }
       
        public virtual OrderChargeDiscount OrderChargeDiscount { get; set; }
    }
}
