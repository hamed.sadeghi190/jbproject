﻿using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models.Email
{
    public class EmailReminderModel
    {
        public EmailReminderModel()
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
            ClubUrl = RootSiteUrl + "/" + ClubDomain;
        }

        public string PreapprovalKey { get; set; }
        public string AppliedText { get; set; }
        public string PayPalInfo { get; set; }
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string ClubImageUrl { get; set; }
        public string ClubName { get; set; }
        public string ClubUrl { get; set; }
        public string ClubEmail { get; set; }
        public string ClubPhone { get; set; }
        public CurrencyCodes ClubCurrency { get; set; }
        public string InstallmentDate { get; set; }
        public string RootSiteUrl { get; set; }
        public string BodyMailTitle { get; set; }

        public string PlayerFullName { get; set; }


        public string PaymentLink { get; set; }

        public string TotalAmount
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(ReservationItems == null ? 0 : ReservationItems.Sum(c => c.Price), ClubCurrency);
            }
        }

        public string ConfirmationId { get; set; }
        public string Balance { get; set; }
        public string PaidAmount { get; set; }
        public List<InstallmentReminderItem> ReservationItems { get; set; }
        public bool isFail { get; set; }

    }

    public class InstallmentReminderItem
    {
        public InstallmentReminderItem()
        {

        }
        public InstallmentReminderItem(CurrencyCodes currency)
        {
            Currency = currency;
        }
        public string ConfirmationId { get; set; }
        public string PlayerFullName { get; set; }
        public CurrencyCodes Currency { get; set; }
        public decimal Price { get; set; }
        public string Amount
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(Price,Currency);
            }
        }
        public string EventName { get; set; }


    }
}