﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class UserEventBusiness :  IUserEventBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IRepository<UserEvent> _userEventRepository;

        public UserEventBusiness(IClubBusiness clubBusiness, IRepository<UserEvent> userEventRepository)
        {
            _clubBusiness = clubBusiness;
            _userEventRepository = userEventRepository;
        }

        public OperationStatus Create(UserEvent userEvents)
        {
            return _userEventRepository.Create(userEvents);
        }

        public OperationStatus CreateEdit(UserEvent userEvents)
        {
            var oldUserCheckin = new UserEvent();
            var club = _clubBusiness.Get(userEvents.ClubId.Value);
            
            var role = EnumHelper.ParseEnum<RoleCategory>(club.ClubStaffs.SingleOrDefault(s => s.JbUserRole.UserId == userEvents.UserId && !s.IsDeleted).JbUserRole.Role.Name);

            if (role == RoleCategory.Instructor)
            {
                oldUserCheckin = Get(userEvents.ProgramId, userEvents.ClubDateTime, userEvents.UserId, userEvents.Type);
            }
            else if (role == RoleCategory.OnsiteCoordinator || role == RoleCategory.OnsiteSupportManager)
            {
                oldUserCheckin = Get(userEvents.ProgramId, userEvents.ClubId, userEvents.ClubDateTime, userEvents.UserId, userEvents.Type);
            }

            if (oldUserCheckin == null)
            {
                return Create(userEvents);
            }
            else
            {

                oldUserCheckin.ClubDateTime = userEvents.ClubDateTime;
                oldUserCheckin.LocalDateTime = userEvents.LocalDateTime;

                oldUserCheckin.Address.Address = userEvents.Address.Address;
                oldUserCheckin.Address.AutoCompletedAddress = userEvents.Address.AutoCompletedAddress;
                oldUserCheckin.Address.City = userEvents.Address.City;
                oldUserCheckin.Address.Country = userEvents.Address.Country;
                oldUserCheckin.Address.County = userEvents.Address.County;
                oldUserCheckin.Address.Lat = userEvents.Address.Lat;
                oldUserCheckin.Address.Lng = userEvents.Address.Lng;
                oldUserCheckin.Address.Route = userEvents.Address.Route;
                oldUserCheckin.Address.Street = userEvents.Address.Street;
                oldUserCheckin.Address.StreetNumber = userEvents.Address.StreetNumber;
                oldUserCheckin.Address.TimeZone = userEvents.Address.TimeZone;
                oldUserCheckin.Address.Zip = userEvents.Address.Zip;

                return Update(oldUserCheckin);
            }
        }

        public UserEvent Get(int id)
        {
            return _userEventRepository.Get(id);
        }

        public UserEvent Get(long? programId, int sessionId, int userId)
        {
            return _userEventRepository.GetList().SingleOrDefault(c => c.ProgramId == programId.Value && c.SessionId == sessionId && c.UserId == userId);
        }

        public UserEvent Get(long? programId, DateTime dateTime, int userId, UserEventType type)
        {
            var today = dateTime.Date;
            var tomorrow = dateTime.AddDays(1).Date;
            return _userEventRepository.GetList().SingleOrDefault(c => c.ProgramId == programId.Value && (c.ClubDateTime >= today && c.ClubDateTime < tomorrow) && c.UserId == userId && c.Type == type);
        }

        public UserEvent Get(long? programId, int? clubId, DateTime dateTime, int userId, UserEventType type)
        {
            var today = dateTime.Date;
            var tomorrow = dateTime.AddDays(1).Date;
            return _userEventRepository.GetList().SingleOrDefault(c => c.ProgramId == programId.Value && c.ClubId == clubId.Value && (c.ClubDateTime >= today && c.ClubDateTime < tomorrow) && c.UserId == userId && c.Type == type);
        }

        public IQueryable<UserEvent> GetList(Expression<Func<UserEvent, bool>> predicate)
        {
            return _userEventRepository.GetList(predicate);
        }

        public IQueryable<UserEvent> GetList()
        {
            return _userEventRepository.GetList();
        }

        public IQueryable<UserEvent> GetList(List<long> programIds)
        {
            return _userEventRepository.GetList(c => programIds.Contains(c.ProgramId.Value));
        }

        public OperationStatus Update(UserEvent userEvents)
        {
            return _userEventRepository.Update(userEvents);
        }
    }
}
