﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AssignRoomViewModel
    {
        public string Room { get; set; }
    }
}