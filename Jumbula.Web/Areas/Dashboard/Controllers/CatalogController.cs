﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Areas.Dashboard.Models.Catalog;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class CatalogController : DashboardBaseController
    {
        [JbAuthorize(JbAction.Catalogs_View)]
        public virtual ActionResult List()
        {
            return View();
        }

        [JbAuthorize(JbAction.CatalogItem_Add)]
        public virtual ActionResult CreateEdit()
        {
            return View();
        }
        [JbAuthorize(JbAction.CatalogItem_Invite)]
        public virtual ActionResult Invite()
        {
            return View();
        }
        [JbAuthorize(JbAction.CatalogSettings_View)]
        public virtual ActionResult Settings()
        {
            return View();
        }

        [JbAuthorize(JbAction.CatalogItem_View)]
        public virtual ActionResult Display()
        {
            return View();
        }

        [JbAuthorize(JbAction.Catalog_ViewAll)]
        public virtual ActionResult All()
        {
            return View();
        }

        [JbAuthorize(JbAction.CatalogItem_Add)]
        public virtual ActionResult Create()
        {
            return RedirectToAction("GetCreateEdit");
        }

        public virtual ActionResult CatalogsSearch()
        {
            return View();
        }

        [JbAuthorize(JbAction.CatalogSettings_View)]
        public virtual JsonNetResult GetSettings()
        {
            var catalogSettings = Ioc.CatalogSettingBusiness.Get(ActiveClub.Id);
            var club = base.ActiveClub;
            var model = new CatalogSettingsViewModel();

            List<string> emCounties = new List<string>()
            {
                 "Alexandria City","Fairfax County", "Arlington County", "Loudoun County","Prince William County", "Montgomery County", "District of Columbia County","Washington, DC","Westchester County",
                 "Fairfield County","Passaic County","Morris County","Middlesex County","Hudson County","Essex County","Bergen County"
            };

            List<string> emStates = new List<string>()
            {
                "Virginia", "Maryland", "District of Columbia","Connecticut","New Jersey","New York"
            };

            model.AllCounties = Ioc.CountryBusiness.GetCounties()
                  .Where(w => emCounties.Contains(w.Name) && emStates.Contains(w.State.Name))
                 .ToList().Select(x => new SelectKeyValue<int>() { Group = x.State.Name, Text = x.Name, Value = x.Id }).OrderBy(c => c.Text).ToList();

            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Montgomery County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Virginia", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Middlesex County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Virginia", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Essex County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Virginia", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Essex County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("New York", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Montgomery County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("New York", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Middlesex County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Connecticut", StringComparison.OrdinalIgnoreCase)));

            if (catalogSettings != null)
            {
                model.Counties = catalogSettings.CatalogSettingCounties.Where(c => !c.Maybe).Any() ? catalogSettings.CatalogSettingCounties.Where(c => !c.Maybe).Select(c => c.CountyId).ToList() : new List<int>();

                model.CountiesMayBe = catalogSettings.CatalogSettingCounties.Where(c => c.Maybe).Any() ? catalogSettings.CatalogSettingCounties.Where(c => c.Maybe).Select(c => c.CountyId).ToList() : new List<int>();
            }

            model.CatalogSearchTags = catalogSettings.CatalogSearchTags;
            model.FairFaxCounty = catalogSettings.FairFaxCounty;
            model.JumbulaMobileApp = catalogSettings.JumbulaMobileApp;
            model.VirtusCertify = catalogSettings.VirtusCertified;
            model.WeeklyEmail = catalogSettings.WeeklyEmail;
            model.HasTitle1SchoolDiscounts = catalogSettings.HasTitle1SchoolDiscounts;
            model.Title1SchoolDiscountType = catalogSettings.Title1SchoolDiscountType;

            model.Image = GetCatalogBannerImageURL(club.Domain);

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.CatalogSettings_Save)]
        public virtual ActionResult SaveSettings(CatalogSettingsViewModel model)
        {
            if (model.Counties == null || (model.Counties != null && !model.Counties.Any()))
            {
                ModelState.AddModelError("Counties", "You should select at least one county.");
            }

            if (model.HasTitle1SchoolDiscounts && string.IsNullOrEmpty(model.Title1SchoolDiscountType))
            {
                ModelState.AddModelError("Title1SchoolDiscountType", "Discount type is required.");
            }

            if (model.Counties != null && model.CountiesMayBe != null)
            {
                var duplicateCounties = new List<int>();

                foreach (var item in model.Counties)
                {
                    if (model.CountiesMayBe.Contains(item))
                    {
                        duplicateCounties.Add(item);
                    }
                }
                if (duplicateCounties.Count > 0)
                {
                    ModelState.AddModelError("Counties", "You cannot repeat a county/disctrict in potential counties/districts list.");
                }
            }

            if (ModelState.IsValid)
            {
                var catalogSetting = Ioc.CatalogSettingBusiness.Get(ActiveClub.Id);


                var counties = Ioc.CountryBusiness.GetCounties(model.Counties).ToList();


                if (catalogSetting.CatalogSettingCounties != null && catalogSetting.CatalogSettingCounties.Any())
                {
                    catalogSetting.CatalogSettingCounties.Clear();
                }

                foreach (var item in counties)
                {
                    catalogSetting.CatalogSettingCounties.Add(new CatalogSettingCounty { County = item });
                }


                if (model.CountiesMayBe != null || (model.CountiesMayBe != null && model.CountiesMayBe.Any()))
                {
                    var maybeCounties = Ioc.CountryBusiness.GetCounties(model.CountiesMayBe).ToList();

                    foreach (var item in maybeCounties)
                    {
                        catalogSetting.CatalogSettingCounties.Add(new CatalogSettingCounty { County = item, Maybe = true });
                    }
                }

                catalogSetting.CatalogSearchTags = model.CatalogSearchTags;
                catalogSetting.FairFaxCounty = model.FairFaxCounty;
                catalogSetting.VirtusCertified = model.VirtusCertify;
                catalogSetting.JumbulaMobileApp = model.JumbulaMobileApp;
                catalogSetting.WeeklyEmail = model.WeeklyEmail;
                catalogSetting.HasTitle1SchoolDiscounts = model.HasTitle1SchoolDiscounts;
                catalogSetting.Title1SchoolDiscountType = model.HasTitle1SchoolDiscounts ? model.Title1SchoolDiscountType : null;

                UploadCatalogBannerImage(model.Image);

                var res = Ioc.CatalogSettingBusiness.Update(catalogSetting);

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }

        public virtual JsonNetResult GetCreateEdit(int? catalogId)
        {
            var club = ActiveClub;

            var catalogSettings = Ioc.CatalogSettingBusiness.Get(ActiveClub.Id);

            var pageMode = catalogId.HasValue ? PageMode.Edit : PageMode.Create;

            var model = new CatalogCreateEditViewModel();

            var partnerId = club.PartnerId;
            if (partnerId.HasValue)
            {
                //get setting catalog
                var clubInfo = Ioc.ClubBusiness.GetCatalogSetting(partnerId.Value);

                model.PriceOptionLable = clubInfo.PriceOptionLable;
                model.priceOptionNote = clubInfo.PriceOptionNote;

                model.ShowGenderBox = clubInfo.ShowGender == true ? true : false;

            }
            else
            {
                model.ShowGenderBox = true;
                model.PriceOptionLable = Constants.PriceOption_Lable;
            }

            model.PageMode = pageMode;
            model.RestrictionTypes = DropdownHelpers.ToSelectList<RestrictionType>();

            model.Ages = SeedAges(120);
            model.Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);
            model.Genders = DropdownHelpers.ToSelectList<Genders>();

            model.AllCategories = Ioc.CategoryBuisness.GetList()
                  .Where(e => e.ParentId != null)
                  .ToList().OrderBy(o => o.Order).Select(x => new SelectKeyValue<int>() { Group = x.ParentCategory.Name, Text = x.Name, Value = x.Id }).ToList();

            model.AllRatios = SeedRatios(100);

            model.AllCapacities = SeedCapacity(Constants.MaximumCapacity);

            switch (pageMode)
            {
                case PageMode.Create:
                    {
                        model.AttendeeRestriction.RestrictionType = RestrictionType.Grade;

                        model.AttendeeRestriction.MinAge = 0;
                        model.AttendeeRestriction.MaxAge = 0;

                        model.MinimumEnrollment = 0;
                        model.MaximumEnrollment = 0;
                        model.MinGrade = "";
                        model.MaxGrade = "";

                        model.StudentRatio = 0;

                        model.PriceGroups = GetDefaultPriceOptions(ActiveClub.Id);

                        model.RecomendedGradeGroups = GetDefaultRecomendedGradeGroups(ActiveClub.Id);

                        model.SpaceRequirements = Ioc.CatalogSettingBusiness.GetDefaultSpaceRequirements(ActiveClub.Id);
                    }
                    break;
                case PageMode.Edit:
                    {

                        var catalog = Ioc.CatalogBusiness.Get(catalogId.Value);
                        CheckResourceForAccess(catalog);

                        model.Id = catalog.Id;
                        model.AttendeeRestriction = catalog.AttendeeRestriction;
                        model.Description = catalog.Description;

                        model.Name = catalog.Name;
                        model.Detail = catalog.Detail;
                        model.Rating = catalog.Rating;

                        model.MinimumEnrollment = catalog.MinimumEnrollment.HasValue ? catalog.MinimumEnrollment : 0;
                        model.MaximumEnrollment = catalog.MaximumEnrollment.HasValue ? catalog.MaximumEnrollment : 0;

                        model.AttendeeRestriction.MinAge = catalog.AttendeeRestriction.MinAge.HasValue ? catalog.AttendeeRestriction.MinAge : 0;
                        model.AttendeeRestriction.MaxAge = catalog.AttendeeRestriction.MaxAge.HasValue ? catalog.AttendeeRestriction.MaxAge : 0;

                        model.Categories = catalog.Categories.Select(c => c.Id).ToList();
                        model.CategoriesNames = catalog.Categories.Select(c => c.Name).ToList();
                        model.StrCategoriesNames = string.Join(", ", model.CategoriesNames);
                        model.StudentRatio = catalog.ISRatio.HasValue ? catalog.ISRatio.Value : 0;

                        model.Detail = catalog.Detail;
                        model.MaterialsNeeded = catalog.MaterialsNeeded;

                        model.PriceGroups = GetDefaultPriceOptions(ActiveClub.Id);
                        model.Gender = catalog.AttendeeRestriction.Gender == Genders.NoRestriction ? "Boys and girls" : (catalog.AttendeeRestriction.Gender == Genders.Female ? "Girls only" : "Boys only");

                        model.MinGrade = catalog.AttendeeRestriction.MinGrade.ToDescription();
                        model.MaxGrade = catalog.AttendeeRestriction.MaxGrade.ToDescription();

                        //Get images
                        if (catalog.ImageGallery_Id.HasValue)
                        {
                            var clubDomain = catalog.ImageGallery.ClubDomain;
                            var fileNames = catalog.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();
                            if (fileNames.Any())
                            {
                                model.Images = GetCatalogImagesURL(fileNames, clubDomain);
                            }
                        }

                        //image for catalog
                        model.Image = GetCatalogImageURL(catalog.ImageFileName, club.Domain);

                        foreach (var group in model.PriceGroups)
                        {
                            foreach (var price in group.Prices)
                            {
                                foreach (var item in catalog.PriceOptions)
                                {
                                    if (price.Title.Equals(item.Title) && group.Title.Equals(item.DurationTitle))
                                    {
                                        price.IsChecked = true;
                                        price.IsDefault = true;
                                        price.Title = item.Title;
                                        price.Amount = item.Amount;
                                        if (item.Attribute != null)
                                        {
                                            //item.Attribute.AgendaList = new List<CatalogPriceOptionAgendaItem>();
                                            price.ListAgendas = item.Attribute.AgendaList.Select(s =>
                                            new AgendaViewModel
                                            {
                                                AgendaWeek = s.AgendaWeek,
                                                Title = s.Title
                                            })
                                            .ToList();
                                        }

                                    }

                                }

                            }
                        }

                        var PriceGroup = catalog.PriceOptions.GroupBy(g => g.DurationTitle).Select(b =>
                               new
                               {
                                   Title = b.Key,
                                   Prices = b.Select(p =>
                                   new
                                   {
                                       Amount = p.Amount,
                                       SessionLength = p.Title,
                                   })
                                   .ToList()
                               })
                               .ToList();

                        model.PriceOptionForView = PriceGroup.ToList<dynamic>();

                        model.RecomendedGradeGroups = GetDefaultRecomendedGradeGroups(ActiveClub.Id);

                        model.RecomendedGradeGroups.Where(r => catalog.CatalogItemOptions.Where(o => o.Type == CatalogOptionType.GradeGrouping).Select(p => p.Title).Contains(r.Title)).ToList().ForEach(u => u.IsChecked = true);
                        model.StrRecomendedGradeGroups = string.Join(", ", model.RecomendedGradeGroups.Where(r => r.IsChecked).Select(r => r.Title));

                        model.SpaceRequirements = Ioc.CatalogSettingBusiness.GetDefaultSpaceRequirements(ActiveClub.Id);

                        model.SpaceRequirements.Where(r => catalog.CatalogItemOptions.Where(o => o.Type == CatalogOptionType.SpaceRequrement).Select(p => p.Title).Contains(r.Title)).ToList().ForEach(u => u.IsChecked = true);
                        model.StrSpaceRequirementsIndoor = string.Join(", ", model.SpaceRequirements.Where(i => i.IsChecked && i.Type == SpaceRequirementType.Indoor).Select(i => i.Title));
                        model.StrSpaceRequirementsOutdoor = string.Join(", ", model.SpaceRequirements.Where(i => i.IsChecked && i.Type == SpaceRequirementType.Outdoor).Select(i => i.Title));
                    }
                    break;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.CatalogItem_Add)]
        public virtual ActionResult CreateEdit(CatalogCreateEditViewModel model)
        {

            CatalogValidationCheck(ModelState, model);

            if (ModelState.IsValid)
            {
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
                var today = DateTime.UtcNow;
                if (model.PriceGroups.SelectMany(g => g.Prices).Any(p => p.Id == 0 && p.IsChecked))
                {
                    foreach (var group in model.PriceGroups)
                    {
                        group.Prices.ToList().ForEach(p => p.DurationTitle = group.Title);
                    }

                    var newCatalogItemPriceOptions = model.PriceGroups.SelectMany(g => g.Prices).Where(p => p.Id == 0 && p.IsChecked).Select(s =>
                        new CatalogItemPriceOption
                        {
                            Amount = s.Amount.Value,
                            Title = s.Title,
                            DurationTitle = s.DurationTitle,
                            AttributeSerialized = JsonConvert.SerializeObject(new CatalogPriceOptionAttribute
                            {
                                AgendaList = s.ListAgendas.Select(t => new CatalogPriceOptionAgendaItem { AgendaWeek = t.AgendaWeek, Title = t.Title }).ToList()
                            }),

                        })
                        .ToList();

                    if (!Ioc.CatalogSettingBusiness.InsertCatalogPriceOptions(ActiveClub.Id, newCatalogItemPriceOptions).Status)
                    {
                        throw new Exception();
                    }
                }

                var newCatalogItemOptions = new List<CatalogItemOption>();

                if (model.RecomendedGradeGroups.Any(p => p.Id == 0 && p.IsChecked))
                {
                    newCatalogItemOptions.AddRange(model.RecomendedGradeGroups.Where(p => p.Id == 0 && p.IsChecked).Select(s =>
                           new CatalogItemOption
                           {
                               Title = s.Title,
                               Type = CatalogOptionType.GradeGrouping,
                           })
                           .ToList());
                }

                if (model.SpaceRequirements.Any(p => p.Id == 0 && p.IsChecked))
                {
                    newCatalogItemOptions.AddRange(model.SpaceRequirements.Where(p => p.Id == 0 && p.IsChecked).Select(s =>
                        new CatalogItemOption
                        {
                            Title = s.Title,
                            Type = CatalogOptionType.SpaceRequrement,
                            Group = s.Type.ToString()
                        })
                        .ToList());
                }

                if (newCatalogItemOptions.Any() && !Ioc.CatalogSettingBusiness.InsertCatalogOptions(ActiveClub.Id, newCatalogItemOptions).Status)
                {
                    throw new Exception();
                }

                var res = new OperationStatus();

                switch (model.PageMode)
                {
                    case PageMode.Create:
                        {
                            var catalogItem = new CatalogItem()
                            {
                                ClubId = ActiveClub.Id,
                                AttendeeRestriction = model.AttendeeRestriction,
                                Name = model.Name,
                                Description = model.Description,
                                Detail = model.Detail,
                                MaterialsNeeded = model.MaterialsNeeded,

                                MinimumEnrollment = model.MinimumEnrollment != 0 ? model.MinimumEnrollment : null,
                                MaximumEnrollment = model.MaximumEnrollment != 0 ? model.MaximumEnrollment : null,

                                ISRatio = model.StudentRatio != 0 ? model.StudentRatio : null,
                                Rating = model.Rating,
                                MetaData = new MetaData()
                                {
                                    DateCreated = DateTime.UtcNow,
                                    DateUpdated = DateTime.UtcNow
                                },

                                CatalogItemOptions = new List<CatalogItemOption>(),
                                PriceOptions = new List<CatalogItemPriceOption>(),
                            };
                            var listCropSrcImage = new Dictionary<string, string>();

                            if (model.Images.Count > 0)
                            {
                                catalogItem.ImageGallery = new ImageGallery();
                                //src
                                listCropSrcImage = model.Images.Zip(model.SourceImages, (k, v) => new { Key = k, Value = v })
                                                .ToDictionary(x => x.Key, x => x.Value);
                            }

                            //Add one image
                            catalogItem.ImageFileName = UploadImage(model.Image, model.SourceImage, club.Domain);
                            //Add images 
                            var pathImages = UploadCatalogDicImages(listCropSrcImage, club.Domain);
                            if (pathImages.Any())
                            {
                                catalogItem.ImageGallery.MetaData = new MetaData()
                                {
                                    DateCreated = DateTime.UtcNow,
                                    DateUpdated = DateTime.UtcNow
                                };
                                catalogItem.ImageGallery.Name = model.Name;
                                catalogItem.ImageGallery.Status = EventStatusCategories.open;
                                catalogItem.ImageGallery.Type = ImageGalleryType.Catalog;
                                catalogItem.ImageGallery.ClubDomain = ActiveClub.Domain;
                                catalogItem.ImageGallery.Images = new List<ImageGalleryItem>();
                                foreach (var path in pathImages)
                                {
                                    catalogItem.ImageGallery.Images.Add(new ImageGalleryItem()
                                    {
                                        Title = model.Name,
                                        Path = path,
                                        Status = EventStatusCategories.open,
                                        Order = 0,
                                        MetaData = new MetaData { DateCreated = today, DateUpdated = today }
                                    });
                                }

                            }
                            //end images
                            foreach (var priceGroup in model.PriceGroups.Where(g => g.IsChecked))
                            {
                                foreach (var price in priceGroup.Prices.Where(p => p.IsChecked))
                                {
                                    catalogItem.PriceOptions.Add(
                                        new CatalogItemPriceOption
                                        {
                                            Title = price.Title,
                                            Amount = price.Amount.Value,
                                            DurationTitle = priceGroup.Title,
                                            AttributeSerialized = JsonConvert.SerializeObject(new CatalogPriceOptionAttribute
                                            {
                                                AgendaList = price.ListAgendas.Select(t => new CatalogPriceOptionAgendaItem { AgendaWeek = t.AgendaWeek, Title = t.Title }).ToList()
                                            }),
                                        });
                                }
                            }

                            foreach (var item in model.RecomendedGradeGroups.Where(r => r.IsChecked))
                            {
                                catalogItem.CatalogItemOptions.Add(
                                    new CatalogItemOption
                                    {
                                        Title = item.Title,
                                        Type = CatalogOptionType.GradeGrouping,
                                    });
                            }

                            foreach (var item in model.SpaceRequirements.Where(r => r.IsChecked))
                            {
                                catalogItem.CatalogItemOptions.Add(
                                    new CatalogItemOption
                                    {
                                        Title = item.Title,
                                        Type = CatalogOptionType.SpaceRequrement,
                                        Group = item.Type.ToString()
                                    });
                            }

                            if (model.Categories != null && model.Categories.Any())
                            {
                                var categories = Ioc.CategoryBuisness.GetList(model.Categories).ToList();

                                catalogItem.Categories.AddEntities<Category>(categories);
                            }

                            if (model.AttendeeRestriction.RestrictionType != RestrictionType.Age)
                            {
                                catalogItem.AttendeeRestriction.MinAge = null;
                                catalogItem.AttendeeRestriction.MaxAge = null;
                            }

                            res = Ioc.CatalogBusiness.Create(catalogItem);
                        }
                        break;
                    case PageMode.Edit:
                        {
                            var catalogItem = Ioc.CatalogBusiness.Get(model.Id);

                            catalogItem.AttendeeRestriction = model.AttendeeRestriction;
                            catalogItem.Description = model.Description;

                            catalogItem.MinimumEnrollment = model.MinimumEnrollment != 0 ? model.MinimumEnrollment : null;
                            catalogItem.MaximumEnrollment = model.MaximumEnrollment != 0 ? model.MaximumEnrollment : null;

                            catalogItem.ISRatio = model.StudentRatio != 0 ? model.StudentRatio : null;
                            catalogItem.Rating = model.Rating;

                            catalogItem.MetaData = new MetaData()
                            {
                                DateCreated = catalogItem.MetaData.DateCreated,
                                DateUpdated = DateTime.UtcNow
                            };

                            catalogItem.Name = model.Name;
                            catalogItem.Detail = model.Detail;
                            catalogItem.MaterialsNeeded = model.MaterialsNeeded;

                            //for one image
                            if (!string.IsNullOrEmpty(model.Image) && !model.Image.Contains("http"))
                            {
                                catalogItem.ImageFileName = UploadImage(model.Image, model.SourceImage, club.Domain);
                            }
                            if (model.Image == null)
                            {
                                catalogItem.ImageFileName = null;
                            }

                            //Edit images

                            var upadateImageslist = new List<ImageGalleryItem>();
                            if (model.Images.Any())
                            {
                                var clubDomain = catalogItem.ImageGallery != null ? catalogItem.ImageGallery.ClubDomain : club.Domain;
                                List<string> newImages = new List<string>();
                                List<string> hasImages = new List<string>();
                                foreach (var item in model.Images)
                                {

                                    if (!item.Contains("http"))
                                    {
                                        newImages.Add(item);
                                    }
                                    else
                                    {
                                        hasImages.Add(item);
                                    }

                                }
                                var removeList = new List<string>();
                                if (hasImages.Any())
                                {
                                    var fileNames = catalogItem.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();
                                    var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                                    var ImagesInDb = GetCatalogImagesURL(fileNames, clubDomain);

                                    removeList.AddRange(ImagesInDb);
                                    foreach (var item in ImagesInDb)
                                    {
                                        if (hasImages.Contains(item))
                                        {
                                            removeList.Remove(item);
                                        }
                                    }

                                    foreach (var fileName in removeList)
                                    {
                                        var subFilName = fileName.Substring(fileName.LastIndexOf("/") + 1);
                                        //var sourcePath = StorageManager.PathCombine(string.Format(@"{0}/{1}", ActiveClub.Domain, Constants.Path_CatalogItems_Folder), fileName);
                                        //var targetPath = StorageManager.PathCombine(string.Format(@"{0}/{1}", ActiveClub.Domain, Constants.Path_CatalogItems_Folder), fileName);
                                        var url = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), subFilName).AbsoluteUri;
                                        if (StorageManager.IsExist(url))
                                        {
                                            //storageManager.ReadFile(sourcePath, targetPath);
                                            storageManager.DeleteBlob(string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), subFilName);
                                        }
                                    }

                                }
                                if (removeList != null && removeList.Any())
                                {
                                    OperationStatus result = new OperationStatus() { Status = false };

                                    var path = "";
                                    foreach (var item in removeList)
                                    {
                                        path = item.Substring(item.LastIndexOf("/") + 1);
                                        var removeImage = catalogItem.ImageGallery.Images.Where(i => i.Path == path).FirstOrDefault();
                                        upadateImageslist.Add(removeImage);
                                    }
                                    result = Ioc.ImageGalleryItemBusiness.DeleteImages(upadateImageslist);
                                }

                                if (catalogItem.ImageGallery == null)
                                {
                                    catalogItem.ImageGallery = new ImageGallery()
                                    {
                                        Name = catalogItem.Name,
                                        ClubDomain = ActiveClub.Domain,
                                        MetaData = new MetaData() { DateCreated = today, DateUpdated = today, },
                                        Type = ImageGalleryType.Catalog,
                                        Status = EventStatusCategories.open,
                                    };

                                }
                                var listSrcImg = new Dictionary<string, string>();
                                if (newImages != null && newImages.Any())
                                {
                                    listSrcImg = newImages.Zip(model.SourceImages, (k, v) => new { Key = k, Value = v })
                                               .ToDictionary(x => x.Key, x => x.Value);

                                    var pathImages = UploadCatalogDicImages(listSrcImg, clubDomain);
                                    foreach (var path in pathImages)
                                    {
                                        catalogItem.ImageGallery.Images.Add(new ImageGalleryItem()
                                        {
                                            Title = model.Name,
                                            Path = path,
                                            Status = EventStatusCategories.open,
                                            Order = 0,
                                            MetaData = new MetaData { DateCreated = today, DateUpdated = today }
                                        });
                                    }
                                }


                            }
                            if (model.Images.Count == 0)
                            {
                                if (catalogItem.ImageGallery_Id.HasValue)
                                {
                                    var fileNames = catalogItem.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();

                                    OperationStatus result = new OperationStatus() { Status = false };

                                    foreach (var item in fileNames)
                                    {
                                        var removeImage = catalogItem.ImageGallery.Images.Where(i => i.Path == item).FirstOrDefault();
                                        upadateImageslist.Add(removeImage);
                                    }
                                    result = Ioc.ImageGalleryItemBusiness.DeleteImages(upadateImageslist);
                                }
                            }
                            // End Edit images

                            if (model.AttendeeRestriction.RestrictionType != RestrictionType.Age)
                            {
                                catalogItem.AttendeeRestriction.MinAge = null;
                                catalogItem.AttendeeRestriction.MaxAge = null;
                            }

                            catalogItem.PriceOptions.Clear();
                            foreach (var priceGroup in model.PriceGroups.Where(g => g.IsChecked))
                            {
                                foreach (var price in priceGroup.Prices.Where(p => p.IsChecked))
                                {
                                    catalogItem.PriceOptions.Add(
                                        new CatalogItemPriceOption
                                        {
                                            Title = price.Title,
                                            Amount = price.Amount.Value,
                                            DurationTitle = priceGroup.Title,
                                            AttributeSerialized = JsonConvert.SerializeObject(new CatalogPriceOptionAttribute
                                            {
                                                AgendaList = price.ListAgendas.Select(t => new CatalogPriceOptionAgendaItem { AgendaWeek = t.AgendaWeek, Title = t.Title }).ToList()
                                            }),
                                        });
                                }
                            }

                            catalogItem.CatalogItemOptions.Clear();
                            foreach (var item in model.RecomendedGradeGroups.Where(r => r.IsChecked))
                            {
                                catalogItem.CatalogItemOptions.Add(
                                    new CatalogItemOption
                                    {
                                        Title = item.Title,
                                        Type = CatalogOptionType.GradeGrouping,
                                    });
                            }

                            foreach (var item in model.SpaceRequirements.Where(r => r.IsChecked))
                            {
                                catalogItem.CatalogItemOptions.Add(
                                    new CatalogItemOption
                                    {
                                        Title = item.Title,
                                        Type = CatalogOptionType.SpaceRequrement,
                                        Group = item.Type.ToString()
                                    });
                            }

                            if (model.Categories != null && model.Categories.Any())
                            {
                                var categories = Ioc.CategoryBuisness.GetList(model.Categories.Select(c => c)).ToList();

                                catalogItem.Categories.Clear();

                                catalogItem.Categories.AddEntities<Category>(categories);
                            }

                            res = Ioc.CatalogBusiness.Update(catalogItem);
                        }
                        break;
                }

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.CatalogItem_Delete)]
        public virtual ActionResult Delete(int id)
        {
            var res = Ioc.CatalogBusiness.Delete(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [HttpPost]
        [JbAuthorize(JbAction.CatalogItem_Archive)]
        public virtual ActionResult Archive(int id)
        {
            var res = Ioc.CatalogBusiness.Archive(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [HttpPost]
        public virtual ActionResult Active(int id)
        {
            var res = Ioc.CatalogBusiness.Active(id);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [HttpPost]
        [JbAuthorize(JbAction.Catalogs_View)]
        public virtual JsonNetResult GetList(PaginationModel paginationModel)
        {
            var catalogBusiness = Ioc.CatalogBusiness;

            var model = catalogBusiness.GetList(ActiveClub.Id).ToList().Select(c =>
                new CatalogItemViewModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    ItemStatus = c.Status,
                    DateCreated = Jumbula.Common.Helper.DateTimeHelper.ConvertUtcDateTimeToLocal(c.MetaData.DateCreated, ActiveClub.TimeZone),
                    DateUpdated = Jumbula.Common.Helper.DateTimeHelper.ConvertUtcDateTimeToLocal(c.MetaData.DateUpdated, ActiveClub.TimeZone),
                }
                )
                .ToList();

            var dataSource = model.Page(paginationModel).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = model.Count() });
        }
        [HttpGet]
        [JbAuthorize(JbAction.CatalogItem_Invite)]
        public virtual ActionResult InviteCatalog(int catalogId)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var partnerId = club.PartnerId;
            var catalog = Ioc.CatalogBusiness.Get(catalogId);
            CheckResourceForAccess(catalog);

            var catalogName = catalog.Name;

            var model = new CatalogInvitationViewModel()
            {
                ProgramName = catalogName,
                ProviderName = ActiveClub.Name,
                ProviderId = ActiveClub.Id,
                CatalogId = catalogId,
                CatalogName = catalogName,

                Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null),
                SelectedMinGrade = Ioc.CatalogBusiness.Get(catalogId).AttendeeRestriction.MinGrade,
                SelectedMaxGrade = Ioc.CatalogBusiness.Get(catalogId).AttendeeRestriction.MaxGrade,
                SendMessageToProvider = Ioc.ClubBusiness.SendMessageToProviderOnScheduleSession(partnerId.Value),
            };

            model.WeekDays = DropdownHelpers.ToSelectList<DayOfWeek>();

            model.ProviderSeasons = Ioc.SeasonBusiness.GetList(ActiveClub.Id).OrderByDescending(c => c.MetaData.DateCreated).ToList().Select(c => new SelectKeyValue<long>()
            {
                Text = c.Title,
                Value = c.Id
            });
            if (partnerId.HasValue && partnerId.Value > 0)
            {

                model.Schools = Ioc.ClubBusiness.GetAllPartnerSchools(partnerId.Value).OrderBy(c => c.Name).ToList().Select(c => new SelectKeyValue<int>()
                {
                    Text = c.Name,
                    Value = c.Id
                });
            }
            model.SchoolSeasons = new List<SelectKeyValue<long>>();
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.CatalogItem_Invite)]
        public virtual ActionResult InviteCatalog(CatalogInvitationViewModel model)
        {
            try
            {
                InviteCatalogValidation(ModelState, model);

                if (ModelState.IsValid)
                {
                    var schoolSeason = Ioc.SeasonBusiness.Get(model.SchoolSeasonId);
                    var outSourceSeason = Ioc.SeasonBusiness.Get(model.ProviderSeasonId);

                    var catalog = Ioc.CatalogBusiness.Get(model.CatalogId);
                    SeasonSchoolSetting outsourceSeasonSetting = null;
                    try
                    {
                        outsourceSeasonSetting = (SeasonSchoolSetting)schoolSeason.Setting;
                    }
                    catch
                    {
                        outsourceSeasonSetting = new SeasonSchoolSetting();
                    }

                    var program = ConvertCatalogToProgram(model, catalog, outsourceSeasonSetting);

                    var res = Ioc.ProgramBusiness.Create(program);

                    if (res.Status)
                    {
                        schoolSeason.HasOutSourcePrograms = true;
                        Ioc.SeasonBusiness.Update(schoolSeason);

                        if (model.SendMessageToProvider)
                            SendInviteMessage(model, program, outSourceSeason);
                    }

                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }

                return JsonNet(false);
            }
            catch (Exception ex)
            {
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        [HttpGet]
        public ActionResult GetSeasonSettings(int seasonId)
        {
            var model = GetCatalogSeasonSettings(seasonId);

            return JsonNet(model);
        }

        private CatalogSeasonSettingsViewModel GetCatalogSeasonSettings(long seasonId)
        {
            var schoolSeason = Ioc.SeasonBusiness.Get(seasonId);

            SeasonSchoolSetting schoolSeasonsSetting = null;
            try
            {
                schoolSeasonsSetting = (SeasonSchoolSetting)schoolSeason.Setting;
            }
            catch
            {
                schoolSeasonsSetting = new SeasonSchoolSetting();
            }

            DateTime? amclasses = null;

            if (schoolSeasonsSetting.SchoolSeasonGeneralSetting != null)
                amclasses = schoolSeasonsSetting.SchoolSeasonGeneralSetting.AMClassesPossible ? schoolSeasonsSetting.SchoolSeasonGeneralSetting.AMClassesStartTime : schoolSeasonsSetting.SchoolSeasonGeneralSetting.PMClassesStartTime;

            var startTime = amclasses;
            var endTime = amclasses?.AddHours(1);

            var model = new CatalogSeasonSettingsViewModel()
            {
                StartTime = startTime,
                EndTime = endTime
            };

            return model;
        }

        private void SendInviteMessage(CatalogInvitationViewModel model, Program program, Season outsourceSeason)
        {
            var messageBusiness = Ioc.MessageBusiness;

            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

            var inviteMessageModel = Ioc.CatalogBusiness.BuildInviteMessageBody(program, outsourceSeason, model.StartTime, model.EndTime);
            var text = ViewHelper.RenderViewToString(ControllerContext, "~/Areas/Dashboard/Views/Catalog/InviteMessage.cshtml", inviteMessageModel);

            var subject = string.Format("Session request: {0}", model.ProgramName);

            int realReceiverId = 0;

            var partner = Ioc.ClubBusiness.GetPartner(club.Domain);
            var school = Ioc.ClubBusiness.Get(model.SchoolId);

            var sender = Ioc.MessageBusiness.GetPartnerMessageReciever(partner, school, model.ProviderSeasonId);

            var receiver = Ioc.MessageBusiness.GetClubMessageReciever(club);

            var attribute = new MessageScheduleDraftSessionAttribute()
            {
                Status = ScheduleDraftSessionStatus.Draft,
                ProgramId = program.Id,
            };

            var attributeSerialized = JsonConvert.SerializeObject(attribute);

            var result = messageBusiness.Send(null, sender.Id, this.GetCurrentClubId(), sender.Id, partner.Id, receiver.Id, club.Id, ref realReceiverId, subject, text, MessageType.ScheduleDraftSession, attributeSerialized, program.Id);

            var context = GlobalHost.ConnectionManager.GetHubContext<SignalRHub>();

            var receiverUser = Ioc.UserProfileBusiness.Get(realReceiverId);

            var chatAttriute = ((MessageScheduleDraftSessionAttribute)(result.Attribute));

            var messageResponse = new
            {
                Unread = messageBusiness.GetUnreadCount(realReceiverId, club.Id),
                RefreshCurrentMessage = false,
                TargetClubDomain = club.Domain,
                Chats = new List<object>()
                    {
                        new
                        {
                            Id = result.Id,
                            ChatId = result.ChatId,
                            Subject = result.Subject,
                            MessageType = result.Type,
                            ShowActions = result.Type == MessageType.ScheduleDraftSession && chatAttriute.Status == ScheduleDraftSessionStatus.Draft && this.GetCurrentUserRole() != RoleCategory.Partner,
                            ProgramId = chatAttriute.ProgramId,
                            MessageStatus = chatAttriute.Status.ToDescription(),
                            Audience = messageBusiness.GetAudience(result, club.Id).Name,
                            DateTime = result.Messages.Max(m => m.CreatedDate),
                            IsUnread = true,
                            DateTimeTicks = result.Messages.Max(m => m.CreatedDate).Ticks,
                            Unread = 1,
                            Logo = UrlHelpers.GetClubLogoUrl(messageBusiness.GetAudience(result, club.Id).Domain, messageBusiness.GetAudience(result, club.Id).Logo),
                            Messages = new List<object>()
                            {
                                new
                                {
                                    Text = text,
                                    DateTime = result.Messages.Last().CreatedDate,
                                    DateTimeTicks = result.Messages.Last().CreatedDate.Ticks
                                }
                            }
                        }
                    },

            };

            context.Clients.User(receiverUser.UserName).refreshSignal(messageResponse);

            var messageResponse2 = new
            {
                Unread = messageBusiness.GetUnreadCount(sender.Id, partner.Id),
                RefreshCurrentMessage = false,
                TargetClubDomain = partner.Domain,
                Chats = new List<object>()
                    {
                        new
                        {
                            Id = result.Id,
                            ChatId = result.ChatId,
                            Subject = result.Subject,
                            MessageType = result.Type,
                            ShowActions = result.Type == MessageType.ScheduleDraftSession && chatAttriute.Status == ScheduleDraftSessionStatus.Draft && this.GetCurrentUserRole() != RoleCategory.Partner,
                            ProgramId = chatAttriute.ProgramId,
                            MessageStatus = chatAttriute.Status.ToDescription(),
                            Audience = messageBusiness.GetAudience(result, partner.Id).Name,
                            DateTime = result.Messages.Max(m => m.CreatedDate),
                            DateTimeTicks = result.Messages.Max(m => m.CreatedDate).Ticks,
                            Unread = 1,
                            Logo = UrlHelpers.GetClubLogoUrl(messageBusiness.GetAudience(result, partner.Id).Domain, messageBusiness.GetAudience(result, partner.Id).Logo),
                            Messages = new List<object>()
                            {
                                new
                                {
                                    Text = text,
                                    DateTime = result.Messages.Last().CreatedDate,
                                    DateTimeTicks = result.Messages.Last().CreatedDate.Ticks
                                }
                            }
                        }
                    },

            };

            EmailService.Get(this.ControllerContext).SendMessagingEmail(receiverUser.UserName, this.GetCurrentUserName(), text, partner.Domain, result.ChatId, partner.Logo, program.Name, partner.Name, club.Domain, true);

            context.Clients.User(sender.UserName).refreshSignal(messageResponse2);
        }

        private Program ConvertCatalogToProgram(CatalogInvitationViewModel model, CatalogItem catalog, SeasonSchoolSetting schoolSeasonSetting)
        {

            var season = Ioc.SeasonBusiness.Get(model.SchoolSeasonId);
            var club = Ioc.ClubBusiness.Get(model.ProviderId);
            var school = Ioc.ClubBusiness.Get(model.SchoolId);
            if (school.ClubLocations == null || !school.ClubLocations.Any() || school.ClubLocations.FirstOrDefault().Id < 1)
            {
                throw new Exception("The school's location is required. Please set the location for school first, than continue the invite process.");
            }
            var location = school.ClubLocations.FirstOrDefault();
            if (!club.ClubLocations.Any(c => c.PostalAddress.Id == location.PostalAddress.Id))
            {
                club.ClubLocations.Add(new ClubLocation()
                {
                    PostalAddress = location.PostalAddress

                });
                Ioc.ClubBusiness.Update(club);
            }

            if (schoolSeasonSetting == null)
            {
                schoolSeasonSetting = new SeasonSchoolSetting();
            }

            var program = new Program()
            {
                CatalogId = catalog.Id,
                Categories = catalog.Categories.ToList(),
                ClubId = model.SchoolId,
                SeasonId = model.SchoolSeasonId,
                Name = model.ProgramName,
                Description = catalog.Description,
                OutSourceSeasonId = model.ProviderSeasonId,
                OutSourceProgramStatus = OutSourceProgramStatus.Invitation,
                Domain = Ioc.ProgramBusiness.GenerateSubDomainName(school.Domain, season.Domain, model.ProgramName),
                RegistrationMode = RegistrationMode.Open,
                Status = ProgramStatus.Open,
                TypeCategory = ProgramTypeCategory.Class,
                ClubLocation = location,
                ClubLocationId = location.Id,
                LastCreatedPage = ProgramPageStep.Step2,

            };

            ProgramSchedule programSchedule = new ProgramSchedule();
            var date = DateTime.UtcNow;
            programSchedule.Charges = new List<Charge>();

            if (catalog.PriceOptions != null && catalog.PriceOptions.Any())
            {
                foreach (var priceOption in catalog.PriceOptions)
                {
                    programSchedule.Charges.Add(new Charge()
                    {
                        Amount = priceOption.Amount,
                        Capacity = 0,
                        Category = ChargeDiscountCategory.EntryFee,
                        MetaData = new MetaData { DateCreated = date, DateUpdated = date },
                        Name = string.Format("{0} ({1})", priceOption.Title, priceOption.DurationTitle),
                        HasEarlyBird = false,
                        Attributes = new ChargeAttribute
                        {
                            EarlyBirds = new List<EarlyBird>(),
                            IsProrate = false
                        },
                    });
                }
            }
            else
            {

                programSchedule.Charges.Add(new Charge()
                {
                    Amount = 1,
                    Capacity = 0,
                    Category = ChargeDiscountCategory.EntryFee,
                    MetaData = new MetaData { DateCreated = date, DateUpdated = date },
                    Name = "TBD",
                    HasEarlyBird = false,
                    Attributes = new ChargeAttribute
                    {
                        EarlyBirds = new List<EarlyBird>(),
                        IsProrate = false
                    },
                });
            }

            programSchedule.AttendeeRestriction = new AttendeeRestriction();
            programSchedule.AttendeeRestriction.Gender = catalog.AttendeeRestriction.Gender;

            programSchedule.AttendeeRestriction.MinGrade = model.SelectedMinGrade != null ? model.SelectedMinGrade : catalog.AttendeeRestriction.MinGrade;
            programSchedule.AttendeeRestriction.MaxGrade = model.SelectedMaxGrade != null ? model.SelectedMaxGrade : catalog.AttendeeRestriction.MaxGrade;


            programSchedule.AttendeeRestriction.RestrictionType = catalog.AttendeeRestriction.RestrictionType;
            programSchedule.AttendeeRestriction.MinAge = null;
            programSchedule.AttendeeRestriction.MaxAge = null;

            if (model.SpecialRequests != null)
            {
                program.CustomFields.SpecialRequests = model.SpecialRequests;
                program.CustomFieldsSerialized = JsonConvert.SerializeObject(program.CustomFields);
            }

            var scheduleAttribute = new ScheduleAttribute()
            {
                Days = new List<ProgramScheduleDay>(),
                ContinueType = ContinueType.Until,
                Occurances = 0,
                IsProrate = false,
                Capacity = catalog.MaximumEnrollment.HasValue ? catalog.MaximumEnrollment.Value : 0,
                MinimumEnrollment = catalog.MinimumEnrollment.HasValue ? catalog.MinimumEnrollment.Value : 0,
                ShowCapacityLeft = true,
                ShowCapacityLeftNumber = 3,
                WeekDaysMode = model.WeekDaysMode
            };

            programSchedule.StartDate = date;
            programSchedule.EndDate = date.AddMonths(1);
            if (schoolSeasonSetting.SchoolSeasonGeneralSetting != null)
            {
                if (schoolSeasonSetting.SeasonProgramInfoSetting.StartDate.HasValue)
                {
                    programSchedule.StartDate = schoolSeasonSetting.SeasonProgramInfoSetting.StartDate.Value;

                    if (schoolSeasonSetting.SeasonProgramInfoSetting.EndDate.HasValue)
                    {
                        programSchedule.EndDate = schoolSeasonSetting.SeasonProgramInfoSetting.EndDate.Value;
                    }
                    else
                    {
                        programSchedule.EndDate = programSchedule.StartDate.AddMinutes(1);
                    }
                }

            }

            var startTime = date.Date;
            var endTime = date.Date.AddDays(1).AddMinutes(-1);


            if (model.StartTime.HasValue)
            {
                startTime = model.StartTime.Value;
            }

            if (model.EndTime.HasValue)
            {
                endTime = model.EndTime.Value;
            }

            if (model.SelectedWeekDays.Any() && model.WeekDaysMode == WeekDaysMode.SpecialDay)
            {
                for (int i = 0; i < model.SelectedWeekDays.Count; i++)
                {
                    scheduleAttribute.Days.Add(new ProgramScheduleDay
                    {
                        DayOfWeek = model.SelectedWeekDays[i],
                        StartTime = startTime.TimeOfDay,
                        EndTime = endTime.TimeOfDay
                    });
                }
            }
            else
            {
                scheduleAttribute.Days.Add(new ProgramScheduleDay
                {
                    DayOfWeek = programSchedule.StartDate.DayOfWeek,
                    StartTime = startTime.TimeOfDay,
                    EndTime = endTime.TimeOfDay
                });
            }

            programSchedule.Attributes = scheduleAttribute;
            programSchedule.RegistrationPeriod = new RegistrationPeriod()
            {
                RegisterStartDate = date,
                RegisterEndDate = date.AddMonths(1)
            };

            if (schoolSeasonSetting.SeasonProgramInfoSetting != null && schoolSeasonSetting.SeasonProgramInfoSetting.GeneralRegistrationOpens.HasValue)
            {
                programSchedule.RegistrationPeriod.RegisterStartDate = schoolSeasonSetting.SeasonProgramInfoSetting.GeneralRegistrationOpens.Value;

            }

            if (schoolSeasonSetting.SeasonProgramInfoSetting != null && schoolSeasonSetting.SeasonProgramInfoSetting.GeneralRegistrationCloses.HasValue)
            {
                programSchedule.RegistrationPeriod.RegisterEndDate = schoolSeasonSetting.SeasonProgramInfoSetting.GeneralRegistrationCloses.Value;

            }

            if (school.Setting != null && ((SchoolSetting)school.Setting).RegistrationInfo != null)
            {
                programSchedule.RegistrationPeriod.RegisterStartTime = ((SchoolSetting)school.Setting).RegistrationInfo.RegistrationStartTime.HasValue ? ((SchoolSetting)school.Setting).RegistrationInfo.RegistrationStartTime.Value.TimeOfDay : new TimeSpan(0, 0, 0);
                programSchedule.RegistrationPeriod.RegisterEndTime = ((SchoolSetting)school.Setting).RegistrationInfo.RegistrationEndTime.HasValue ? ((SchoolSetting)school.Setting).RegistrationInfo.RegistrationEndTime.Value.TimeOfDay : new TimeSpan(23, 59, 0);
            }

            programSchedule.Sessions = Ioc.ProgramBusiness.GenerateSessions(programSchedule.StartDate, programSchedule.EndDate, ((ScheduleAttribute)programSchedule.Attributes).Days, ((ScheduleAttribute)programSchedule.Attributes).ContinueType, ((ScheduleAttribute)programSchedule.Attributes).Occurances);
            program.ProgramSchedules = new List<ProgramSchedule>();
            program.ProgramSchedules.Add(programSchedule);

            program.Flyer = new Flyer()
            {
                FileName = "testFlyer",
                FlyerType = FlyerType.Generate
            };

            return program;
        }

        private void InviteCatalogValidation(ModelStateDictionary modelState, CatalogInvitationViewModel model)
        {
            if (model.ProviderSeasonId < 1)
            {
                ModelState.AddModelError("ProviderSeasonId", "Provider season is required");
            }
            if (model.SchoolId < 1)
            {
                ModelState.AddModelError("SchoolId", "School is required");
            }
            if (model.SchoolSeasonId < 1)
            {
                ModelState.AddModelError("SchoolSeasonId", "School season is required");
            }
            if (model.SelectedWeekDays.Count == 0 && model.WeekDaysMode == WeekDaysMode.SpecialDay)
            {
                ModelState.AddModelError("SelectedWeekDays", "Week Days is required");
            }

            if (model.StartTime.HasValue && model.EndTime.HasValue && model.StartTime >= model.EndTime)
            {
                ModelState.AddModelError("model.StartTime", "Start time should be before of end time.");
            }

            if (model.SendMessageToProvider)
            {
                try
                {
                    var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
                    var partner = Ioc.ClubBusiness.GetPartner(club.Domain);
                    var school = Ioc.ClubBusiness.Get(model.SchoolId);

                    var sender = Ioc.MessageBusiness.GetPartnerMessageReciever(partner, school, model.ProviderSeasonId);
                    var receiver = Ioc.MessageBusiness.GetClubMessageReciever(club);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        private List<CatalogPriceGroupViewModel> GetDefaultPriceOptions(int clubId)
        {
            var catalogPriceOptionGroups = Ioc.CatalogSettingBusiness.GetDefaultCatalogPriceOptions(clubId);

            var priceOptionGropus = catalogPriceOptionGroups.GroupBy(p => p.DurationTitle).Select(b =>
                new CatalogPriceGroupViewModel
                {
                    Title = b.Key,
                    IsDefault = true,
                    Prices = b.Select(p =>
                    new CatalogPriceViewModel
                    {
                        Amount = null,
                        Id = p.Id,
                        IsDefault = true,
                        Title = p.Title,
                        ListAgendas = GetAgendaList(p.Title),
                    })
                    .ToList()
                })
                .ToList();

            return priceOptionGropus;
        }

        private List<AgendaViewModel> GetAgendaList(string priceTitle)
        {
            var result = new List<AgendaViewModel>();

            var weeks = 0;
            try
            {
                int weekLocation = priceTitle.IndexOf("-", StringComparison.Ordinal);
                weeks = int.Parse(priceTitle.Substring(0, weekLocation).Trim());
            }
            catch
            {
                return result;
            }

            for (int i = 1; i <= weeks; i++)
            {
                result.Add(new AgendaViewModel()
                {
                    Title = string.Format("Week {0} parent talking point", i),
                    AgendaWeek = " ",

                });
            }

            return result;

        }
        private List<RecommendedGradeGroupingViewModel> GetDefaultRecomendedGradeGroups(int clubId)
        {
            return Ioc.CatalogSettingBusiness.GetDefaultCatalogOptions(clubId).Where(o => o.Type == CatalogOptionType.GradeGrouping).Select(c =>
                new RecommendedGradeGroupingViewModel
                {
                    Id = c.Id,
                    IsDefault = true,
                    Title = c.Title
                })
                    .ToList();
        }

        private void CatalogValidationCheck(ModelStateDictionary modelState, CatalogCreateEditViewModel model)
        {

            RestrictionsValidationCheck(modelState, model);

            for (int i = 0; i < model.PriceGroups.Count(); i++)
            {
                for (int j = 0; j < model.PriceGroups[i].Prices.Count(); j++)
                {
                    if (!model.PriceGroups[i].Prices[j].IsChecked)
                    {
                        modelState.Remove(string.Format("model.PriceGroups[{0}].Prices[{1}].{2}", i, j, "Amount"));
                        if (model.HasPartner)
                        {
                            if (model.PriceGroups[i].Prices[j].ListAgendas.Count > 0)
                            {
                                for (int k = 0; k < model.PriceGroups[i].Prices[j].ListAgendas.Count(); k++)
                                {
                                    modelState.Remove(string.Format("model.PriceGroups[{0}].Prices[{1}].ListAgendas[{2}].{3}", i, j, k, "WeekAgenda"));
                                }
                            }
                        }
                    }
                }
            }
        }

        private void RestrictionsValidationCheck(ModelStateDictionary modelState, CatalogCreateEditViewModel model)
        {
            if (model.MinimumEnrollment > model.MaximumEnrollment)
            {
                ModelState.AddModelError(string.Format("model.{0}", "MinimumEnrollment"), "Minimum enrollment must be smaller than maximum enrollment.");
            }

            if (model.Categories == null || (model.Categories != null && !model.Categories.Any()))
            {
                ModelState.AddModelError(string.Format("model.{0}", "Categories"), "You should select at least one category.");
            }

            if (model.AttendeeRestriction.RestrictionType == RestrictionType.None)
            {
                ModelState.Remove(string.Format("model.AttendeeRestriction.{0}", "MinAge"));
                ModelState.Remove(string.Format("model.AttendeeRestriction.{0}", "MaxAge"));

                ModelState.Remove(string.Format("model.AttendeeRestriction.{0}", "MinGrade"));
                ModelState.Remove(string.Format("model.AttendeeRestriction.{0}", "MaxGrade"));
            }
            else if (model.AttendeeRestriction.RestrictionType == RestrictionType.Age)
            {
                ModelState.Remove(string.Format("model.AttendeeRestriction.{0}", "MinGrade"));
                ModelState.Remove(string.Format("model.AttendeeRestriction.{0}", "MaxGrade"));

                if (model.AttendeeRestriction.MinAge == 0 && model.AttendeeRestriction.MaxAge == 0)
                {
                    ModelState.AddModelError(string.Format("model.AttendeeRestriction.{0}", "MinAge"), "You must select min or max age.");
                }

                if (model.AttendeeRestriction.MinAge != 0 && model.AttendeeRestriction.MaxAge != 0 && model.AttendeeRestriction.MinAge > model.AttendeeRestriction.MaxAge)
                {
                    ModelState.AddModelError(string.Format("model.AttendeeRestriction.{0}", "MinAge"), "Min age must be smaller than max age.");
                }
            }
            else if (model.AttendeeRestriction.RestrictionType == RestrictionType.Grade)
            {
                ModelState.Remove(string.Format("model.AttendeeRestriction.{0}", "MinAge"));
                ModelState.Remove(string.Format("model.AttendeeRestriction.{0}", "MaxAge"));

                if (model.AttendeeRestriction.MinGrade == null && model.AttendeeRestriction.MaxGrade == null)
                {
                    ModelState.AddModelError(string.Format("model.AttendeeRestriction.{0}", "MinGrade"), "You must select min or max grade.");
                }
                else
                {
                    if (model.AttendeeRestriction.MinGrade.GetOrder() > model.AttendeeRestriction.MaxGrade.GetOrder())
                    {
                        ModelState.AddModelError(string.Format("model.AttendeeRestriction.{0}", "MinGrade"), "Min grade must be smaller than max grade.");
                    }
                }
            }
        }

        private string UploadImage(string image, string sourseImage, string clubDomain)
        {
            var club = base.ActiveClub;

            var result = string.Empty;

            if (!string.IsNullOrEmpty(image) && !image.Contains("http:"))
            {
                string fileName = string.Format("{0}.jpg", Guid.NewGuid());
                //add sourse
                string srcImg = "large-" + fileName;
                sourseImage = sourseImage.Replace("data:image/jpeg;base64,", string.Empty);

                sourseImage = sourseImage.Replace("data:image/png;base64,", string.Empty);
                var srcFiledata = Convert.FromBase64String(sourseImage);
                Stream srcFileStream = new MemoryStream(srcFiledata);


                image = image.Replace("data:image/jpeg;base64,", string.Empty);

                image = image.Replace("data:image/png;base64,", string.Empty);

                var fileData = Convert.FromBase64String(image);

                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

                var fileSize = fileData.Count() / 1024;

                var contentType = "image/jpeg";

                Stream fileStream = new MemoryStream(fileData);

                sm.UploadBlob(string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), fileName, fileStream, contentType);
                sm.UploadBlob(string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), srcImg, srcFileStream, contentType);
                fileStream.Close();
                srcFileStream.Close();
                result = fileName;
            }

            return result;
        }
        //Add dictionry images
        private List<string> UploadCatalogDicImages(Dictionary<string, string> listsrcimg, string clubdomain)
        {
            var club = base.ActiveClub;

            var result = new List<string>();

            foreach (var image in listsrcimg)
            {
                var newImage = image.Key;
                var srcImage = image.Value;
                if (!string.IsNullOrEmpty(newImage) && !newImage.Contains("http:"))
                {
                    string fileName = string.Format("{0}.jpg", Guid.NewGuid());
                    //Add src
                    string srcFileName = "large-" + fileName;
                    srcImage = srcImage.Replace("data:image/jpeg;base64,", string.Empty);

                    srcImage = srcImage.Replace("data:image/png;base64,", string.Empty);
                    var srcFileData = Convert.FromBase64String(srcImage);
                    Stream srcFileStream = new MemoryStream(srcFileData);


                    newImage = newImage.Replace("data:image/jpeg;base64,", string.Empty);

                    newImage = newImage.Replace("data:image/png;base64,", string.Empty);

                    var fileData = Convert.FromBase64String(newImage);

                    var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

                    var contentType = "image/jpeg";

                    Stream fileStream = new MemoryStream(fileData);

                    sm.UploadBlob(string.Format(@"{0}\{1}", clubdomain, Constants.Path_CatalogItems_Folder), fileName, fileStream, contentType);
                    sm.UploadBlob(string.Format(@"{0}\{1}", clubdomain, Constants.Path_CatalogItems_Folder), srcFileName, srcFileStream, contentType);

                    fileStream.Close();
                    srcFileStream.Close();
                    result.Add(fileName);
                }
            }


            return result;
        }

        public string GetCatalogImageURL(string fileName, string clubDomain)
        {
            var club = base.ActiveClub;

            string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), fileName).AbsoluteUri;

            imageUri = string.Concat(imageUri, "?", "rnd=", Guid.NewGuid());

            if (string.IsNullOrEmpty(fileName))
            {
                imageUri = string.Empty;
            }

            return imageUri;
        }

        public List<string> GetCatalogImagesURL(List<string> fileNames, string clubDomain)
        {
            var club = base.ActiveClub;
            var ImageUrls = new List<string>();
            foreach (var name in fileNames)
            {
                string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), name).AbsoluteUri;

                if (string.IsNullOrEmpty(name))
                {
                    imageUri = string.Empty;
                }

                ImageUrls.Add(imageUri);
            }
            return ImageUrls;
        }
        private void UploadCatalogBannerImage(string bannerImage)
        {
            var club = base.ActiveClub;

            string fileName = string.Format("{0}.jpg", "Catalog_PageBanner_Image");

            if (!string.IsNullOrEmpty(bannerImage) && !bannerImage.Contains("http"))
            {
                bannerImage = bannerImage.Replace("data:image/jpeg;base64,", string.Empty);

                bannerImage = bannerImage.Replace("data:image/png;base64,", string.Empty);

                var fileData = Convert.FromBase64String(bannerImage);

                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

                var fileSize = fileData.Count() / 1024;

                var contentType = "image/jpeg";

                Stream fileStream = new MemoryStream(fileData);

                sm.UploadBlob(club.Domain, fileName, fileStream, contentType);

                fileStream.Close();
            }

        }

        private string GetCatalogBannerImageURL(string clubDomain)
        {

            var club = base.ActiveClub;

            string fileName = string.Format("{0}.jpg", "Catalog_PageBanner_Image");

            string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, fileName).AbsoluteUri;

            imageUri = string.Concat(imageUri, "?", "rnd=", Guid.NewGuid());

            if (string.IsNullOrEmpty(fileName) || !StorageManager.IsExist(imageUri))
            {
                string basixUrl = string.Format("{0}://{1}", Request.Url.Scheme, Request.Url.Host);
                var defaultImageFile = "provider-catalog-default-banner.jpg";

                imageUri = string.Concat(basixUrl, "/Images/", defaultImageFile);
            }

            return imageUri;
        }

        private IEnumerable<string> GetFileInfo(IEnumerable<HttpPostedFileBase> files)
        {
            return
                from a in files
                where a != null
                select string.Format("{0} ({1} bytes)", Path.GetFileName(a.FileName), a.ContentLength);
        }
        [HttpGet]
        public virtual JsonNetResult GetAllCatalogs(int? clubId)
        {
            var catalogItems = new List<CatalogItem>();
            var clubBusiness = Ioc.ClubBusiness;

            var activeClub = clubBusiness.Get(ActiveClub.Id);

            var club = new Club();
            if (clubId.HasValue)
            {
                club = clubBusiness.Get(clubId.Value);
            }
            else
            {
                club = clubBusiness.Get(ActiveClub.Id);
            }

            var descriptionClub = Ioc.ClubBusiness.Get(club.Id).Description;
            var memberSince = Ioc.ClubBusiness.Get(club.Id).Setting.MemberSince;
            var catalogSettings = Ioc.CatalogSettingBusiness.Get(club.Id);
            if (catalogSettings == null)
            {
                Ioc.CatalogSettingBusiness.Create(club.Id);

                catalogSettings = Ioc.CatalogSettingBusiness.Get(club.Id);
            }

            catalogItems = Ioc.CatalogBusiness.GetList(club.Id).Where(c => c.Status == CatalogStatus.Active).ToList();

            var clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);



            object result = null;
            result = new
            {
                BannerImage = GetCatalogBannerImageURL(club.Domain),
                Logo = clubLogo,
                Name = club.Name,
                Description = descriptionClub,
                Locations = string.Join(", ", catalogSettings.CatalogSettingCounties.Where(c => !c.Maybe).ToList().Select(n => n.County.Name)),
                MaybeLocations = string.Join(", ", catalogSettings.CatalogSettingCounties.Where(c => c.Maybe).ToList().Select(n => n.County.Name)),
                About = new { FairFaxCounty = catalogSettings.FairFaxCounty, VirtusCertified = catalogSettings.VirtusCertified, JumbulaMobileApp = catalogSettings.JumbulaMobileApp, WeeklyEmail = catalogSettings.WeeklyEmail },
                ProgramTypes = (catalogSettings.CatalogSearchTags.AfterSchool && catalogSettings.CatalogSearchTags.BeforeSchool) ? " Before & After School" : (catalogSettings.CatalogSearchTags.AfterSchool ? "After School" : (catalogSettings.CatalogSearchTags.BeforeSchool ? "Before School" : string.Empty)),
                MemberSince = memberSince.HasValue ? memberSince.Value.ToString("MMM dd, yyyy") : string.Empty,
                Catalogs = catalogItems.ToList().Select(c => new
                {
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description,
                    Rating = c.Rating,
                    ImageUrl = GetCatalogImageURL(c.ImageFileName, club.Domain),
                    Categories = string.Join(", ", c.Categories.ToList().Select(n => n.Name)),
                    PriceGroups = c.PriceOptions.GroupBy(g => g.DurationTitle).Select(b =>
                    new
                    {
                        Title = b.Key,
                        Prices = b.Select(p =>
                        new
                        {
                            Amount = p.Amount,
                            SessionLength = p.Title,
                        })
                        .ToList()
                    })
                    .ToList(),
                    MinGrade = c.AttendeeRestriction.MinGrade.ToDescription(),
                    MaxGrade = c.AttendeeRestriction.MaxGrade.ToDescription(),
                    RecommendedGradeGroupings = string.Join(", ", c.CatalogItemOptions.Where(i => i.Type == CatalogOptionType.GradeGrouping).Select(a => a.Title)),
                    MinimumMaximum = (c.MaximumEnrollment.HasValue && c.MinimumEnrollment.HasValue ? string.Format("Minimum: {0}/Maximum: {1}", c.MinimumEnrollment, c.MaximumEnrollment) : (c.MinimumEnrollment.HasValue ? string.Concat("Minimum enrollment:", c.MinimumEnrollment.Value) : c.MinimumEnrollment.HasValue ? string.Concat("Maximum enrollment:", c.MaximumEnrollment.Value) : string.Empty)),
                    InstructorStudentRatio = c.ISRatio,
                    Genders = c.AttendeeRestriction.Gender == Genders.NoRestriction ? "Boys and girls" : (c.AttendeeRestriction.Gender == Genders.Female ? "Girls only" : "Boys only"),
                    Indoor = SpaceRequirments(c, "indoor"),
                    Outdoor = SpaceRequirments(c, "outdoor"),
                    Detail = c.Detail,
                    MaterialsNeeded = c.MaterialsNeeded
                }
                 )
                 .ToList(),
            };

            return JsonNet(new { Result = result, IsPartner = activeClub.IsPartner });
        }
        private string SpaceRequirments(CatalogItem catalogItem, string type)
        {
            var result = string.Join(", ", catalogItem.CatalogItemOptions.Where(i => i.Type == CatalogOptionType.SpaceRequrement && (!string.IsNullOrEmpty(i.Group) && i.Group.Equals(type, StringComparison.OrdinalIgnoreCase))).Select(s => s.Title));

            return result;
        }
        [HttpGet]
        public virtual JsonNetResult GetAllValueForSearchCatalog()
        {
            var model = new AllValueForSearchCatalogViewModel();
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var partnerDomain = club.IsPartner ? club.Domain : club.PartnerClub.Domain;
            var providers = Ioc.ClubBusiness.GetRelatedClubs(partnerDomain).Where(r => (r.ClubType.ParentType != null && r.ClubType.ParentType.EnumType == ClubTypesEnum.Provider) && !(r.CatalogSetting != null && r.CatalogSetting.CatalogSearchTags.NoGlobalSearch)).OrderBy(c => c.Name);

            model.PartnerProviders = providers.Select(l => new SelectKeyValue<int> { Text = l.Name, Value = l.Id }).ToList();

            model.AllCategories = Ioc.CategoryBuisness.GetList()
                 .Where(e => e.ParentId != null)
                 .ToList().OrderBy(o => o.Order).Select(x => new SelectKeyValue<int>() { Group = x.ParentCategory.Name, Text = x.Name, Value = x.Id }).ToList();

            model.Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);

            model.AllCapacitiesForMin = GetMinEnrollments();
            model.AllCapacitiesForMax = GetMaxEnrollments();
            List<string> emCounties = new List<string>()
            {
                 "Alexandria City","Fairfax County", "Arlington County", "Loudoun County","Prince William County", "Montgomery County", "District of Columbia County","Washington, DC","Westchester County",
                 "Fairfield County","Passaic County","Morris County","Middlesex County","Hudson County","Essex County","Bergen County"
            };

            List<string> emStates = new List<string>()
            {
                "Virginia", "Maryland", "District of Columbia","Connecticut","New Jersey","New York"
            };

            model.AllCounties = Ioc.CountryBusiness.GetCounties()
                  .Where(w => emCounties.Contains(w.Name) && emStates.Contains(w.State.Name))
                 .ToList().Select(x => new SelectKeyValue<int>() { Group = x.State.Name, Text = x.Name, Value = x.Id }).OrderBy(c => c.Text).ToList();

            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Montgomery County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Virginia", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Middlesex County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Virginia", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Essex County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Virginia", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Essex County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("New York", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Montgomery County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("New York", StringComparison.OrdinalIgnoreCase)));
            model.AllCounties.Remove(model.AllCounties.Single(s => s.Text.Equals("Middlesex County", StringComparison.OrdinalIgnoreCase) && s.Group.Equals("Connecticut", StringComparison.OrdinalIgnoreCase)));


            return JsonNet(model, new JsonSerializerSettings());
        }
        [HttpPost]
        public virtual ActionResult SearchClassesForAllProvider(AllValueForSearchCatalogViewModel model)
        {
            List<CatalogItem> allCatalogs = new List<CatalogItem>();

            int page = model.Page != 0 ? model.Page : 1;
            int pageSize = model.EndPage != 0 ? model.EndPage : 10;

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var partnerDomain = club.IsPartner ? club.Domain : club.PartnerClub.Domain;
            var allProviders = Ioc.ClubBusiness.GetRelatedClubs(partnerDomain).Where(r => r.ClubType.ParentType != null && r.ClubType.ParentType.EnumType == ClubTypesEnum.Provider && !(r.CatalogSetting != null && r.CatalogSetting.CatalogSearchTags.NoGlobalSearch));

            var providerIds = allProviders.Select(i => i.Id).ToList();


            allCatalogs = Ioc.CatalogBusiness.GetList().Where(c => providerIds.Contains(c.ClubId) && c.Status == CatalogStatus.Active && !(c.Club.CatalogSetting != null && c.Club.CatalogSetting.CatalogSearchTags.NoGlobalSearch)).ToList();


            if (model.Categories.HasValue && model.Categories != 0)
            {
                allCatalogs = allCatalogs.Where(c => c.Categories.Select(f => f.Id).Contains(model.Categories.Value)).ToList();
            }
            if (model.County.HasValue && model.County != 0)
            {
                allCatalogs = allCatalogs.Where(c => c.Club.CatalogSetting != null && c.Club.CatalogSetting.CatalogSettingCounties.Select(co => co.CountyId).Contains(model.County.Value)).ToList();
            }
            if (model.MinGrade.HasValue)
            {
                allCatalogs = allCatalogs.Where(c => c.AttendeeRestriction.MinGrade.HasValue && c.AttendeeRestriction.MinGrade.GetOrder() >= model.MinGrade.GetOrder()).ToList();
            }

            if (model.MaxGrade.HasValue)
            {
                allCatalogs = allCatalogs.Where(c => c.AttendeeRestriction.MaxGrade.HasValue && c.AttendeeRestriction.MaxGrade.GetOrder() <= model.MaxGrade.GetOrder()).ToList();
            }
            if (model.MinimumEnrollment.HasValue)
            {
                allCatalogs = allCatalogs.Where(c => c.MinimumEnrollment >= model.MinimumEnrollment.Value).ToList();
            }

            if (model.MaximumEnrollment.HasValue)
            {
                allCatalogs = allCatalogs.Where(c => c.MaximumEnrollment <= model.MaximumEnrollment.Value).ToList();
            }
            if (model.BeforeSchool)
            {
                allCatalogs = allCatalogs.Where(c => c.Club.CatalogSetting == null || (c.Club.CatalogSetting != null && c.Club.CatalogSetting.CatalogSearchTags.BeforeSchool)).ToList();
            }

            if (model.Virtus)
            {
                allCatalogs = allCatalogs.Where(c => c.Club.CatalogSetting == null || (c.Club.CatalogSetting != null && c.Club.CatalogSetting.VirtusCertified)).ToList();
            }

            if (model.SendsSessionUpdates)
            {
                allCatalogs = allCatalogs.Where(c => c.Club.CatalogSetting == null || (c.Club.CatalogSetting != null && c.Club.CatalogSetting.WeeklyEmail)).ToList();
            }
            var totalItems = allCatalogs.Count();
            var catalogs = allCatalogs.OrderBy(c => c.Name).ToList();//.Skip(pageSize * (page - 1)).Take(pageSize).ToList();

            var searchResult = new
            {
                Model = model,
                TotalItems = totalItems,
                PageSize = pageSize,
                Catalogs = catalogs.Select(c =>
                new
                {
                    Id = c.Id,
                    Description = c.Description,
                    MinGrade = c.AttendeeRestriction.MinGrade.HasValue ? c.AttendeeRestriction.MinGrade.ToDescription() : string.Empty,
                    MaxGrade = c.AttendeeRestriction.MaxGrade.HasValue ? c.AttendeeRestriction.MaxGrade.ToDescription() : string.Empty,
                    MinPrice = c.PriceOptions != null && c.PriceOptions.Any() ? c.PriceOptions.Min(p => p.Amount) : 0,
                    MaxPrice = c.PriceOptions != null && c.PriceOptions.Any() ? c.PriceOptions.Max(p => p.Amount) : 0,
                    Name = c.Name,
                    ProviderName = c.Club.Name,
                    Category = c.Categories != null && c.Categories.Any() ? c.Categories.First().Name : string.Empty,
                    WeeklyEmails = c.Club.CatalogSetting == null || (c.Club.CatalogSetting != null && c.Club.CatalogSetting.WeeklyEmail),
                    ProviderId = c.Club.Id
                })
               .ToList(),
            };

            return JsonNet(searchResult);

        }

        private List<SelectKeyValue<int>> SeedAges(int max)
        {
            var result = new List<SelectKeyValue<int>>();

            result.Add(new SelectKeyValue<int> { Text = "Select age", Value = 0 });

            for (int i = 1; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<int> { Text = i.ToString(), Value = i });
            }

            return result;
        }
        private List<SelectKeyValue<int>> SeedRatios(int max)
        {
            var result = new List<SelectKeyValue<int>>();

            result.Add(new SelectKeyValue<int> { Text = "Select ratio", Value = 0 });

            for (int i = 1; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<int> { Text = i.ToString(), Value = i });
            }

            return result;
        }
        private List<SelectKeyValue<int>> SeedCapacity(int max)
        {
            var result = new List<SelectKeyValue<int>>();

            result.Add(new SelectKeyValue<int> { Text = string.Empty, Value = 0 });

            for (int i = 1; i < max + 1; i++)
            {
                result.Add(new SelectKeyValue<int> { Text = i.ToString(), Value = i });
            }

            return result;
        }
        private List<SelectKeyValue<int>> GetMinEnrollments()
        {
            var result = new List<SelectKeyValue<int>>();
            for (int i = 1; i < 9; i++)
            {
                result.Add(new SelectKeyValue<int> { Text = i.ToString(), Value = i });

            }

            return result;
        }
        private List<SelectKeyValue<int>> GetMaxEnrollments()
        {
            var result = new List<SelectKeyValue<int>>();
            for (int i = 9; i < 21; i++)
            {
                result.Add(new SelectKeyValue<int> { Text = i.ToString(), Value = i });

            }

            result.Add(new SelectKeyValue<int> { Text = "20+", Value = 500 });

            return result;
        }
    }
}