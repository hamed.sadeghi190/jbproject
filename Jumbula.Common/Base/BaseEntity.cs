﻿using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Base
{
    public abstract class BaseEntity : IBaseEntity
    {

        [Key]
        public virtual int Id { get; set; }
        public TViewModel ToViewModel<TViewModel>() where TViewModel : class, new()
        {
            var vm = new TViewModel();
            vm = Jumbula.Common.Utilities.AutoMapper.Map<TViewModel>(this);
            return vm;
        }
    }

    public abstract class BaseEntity<TPrimaryKey> : IBaseEntity<TPrimaryKey>
        where TPrimaryKey : struct
    {

        [Key]
        public virtual TPrimaryKey Id { get; set; }
        public TViewModel ToViewModel<TViewModel>() where TViewModel : class, new()
        {
            var vm = new TViewModel();
            vm = Utilities.AutoMapper.Map<TViewModel>(this);
            return vm;
        }
    }
}

