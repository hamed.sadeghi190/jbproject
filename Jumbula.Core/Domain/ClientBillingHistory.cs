﻿using Jumbula.Common.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class ClientBillingHistory : BaseEntity<long>
    {
        public ClientBillingHistory()
        {
        }

        public string BillingId { get; set; }
        public string Description { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public DateTime BillingDate { get; set; }
        public long ClientId { get; set; }
        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }

        public virtual List<BillingTransactionActivity> TransactionActivities { get; set; }
    }
}
