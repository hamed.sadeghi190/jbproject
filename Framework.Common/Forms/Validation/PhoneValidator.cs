﻿using System;
using System.Linq;
using FluentValidation;
using System.Text.RegularExpressions;

namespace Jb.Framework.Common.Forms.Validation
{
    public class PhoneValidator : AbstractValidator<JbPhone>
    {
        public PhoneValidator()
        {
            RuleFor(x => x.Value)
             .Must(LenValidation)
              .When(w => w.PhoneType==PhoneType.US && w.CurrentMode != AccessMode.Design && w.VisibleMode == VisibilityMode.Both && w.Validations.Any(x => x.Type == ElementValidationType.Required))
             .WithMessage("{0}", "Invalid phone number");

        }

        private bool LenValidation(JbPhone textBox, String value)
        {
           
            if (value == null)
            {
                return true;
            }
            if (textBox.PhoneType==PhoneType.US)
            {
                var reg = new Regex(@"^[0-9]{3}-[0-9]{3}-[0-9]{4}$|^[0-9]{10}$");
                return reg.IsMatch(value);
            }
            return true; 

        }
     

    }
}