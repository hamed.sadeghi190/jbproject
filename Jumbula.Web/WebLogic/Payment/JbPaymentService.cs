﻿using AuthorizeNet.Api.Contracts.V1;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Payment;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Serilog;
using Jumbula.Core.Model.Order;

namespace Jumbula.Web.WebLogic.Payment
{
    public class JbPaymentService : IPaymentBusiness
    {

        private Dictionary<string, string> _metaData;

        public JbPaymentService()
        {
         
        }

        public Dictionary<string, string> GenerateMetaData(JumbulaSubSystem actionCaller, Order order, List<OrderInstallment> installments = null, List<TransactionActivity> transactions = null, Client client = null, string billingId = "", bool isRefund = false, decimal refundAmount = 0)
        {
            _metaData = new Dictionary<string, string>();

            if (order != null)
            {
                AddMetaData(Common.Constants.Payment.ConfirmationId, order.ConfirmationId);

                AddMetaData(Common.Constants.Payment.Email, order.User.UserName);

                AddMetaData(Common.Constants.Payment.Source, Common.Constants.Payment.JumbulaWithDash + order.Club.Domain);

                for (int i = 0; i < order.RegularOrderItems.Count(); i++)
                {
                    var item = order.RegularOrderItems.ElementAt(i);

                    AddMetaData(i + 1 + Common.Constants.Payment.UnderlineParticipant, string.Format(Constants.F_FullName, item.FirstName, item.LastName));
                    AddMetaData(i + 1 + Common.Constants.Payment.UnderlineProgram, item.ProgramScheduleId.HasValue ? item.ProgramTypeCategory != ProgramTypeCategory.ChessTournament ? Ioc.ProgramBusiness.GetProgramNameWithScheduleTitleAndDate(item, item.EntryFeeName) : Ioc.ProgramBusiness.GetProgramNameWithScheduleTitleAndDate(item, item.EntryFeeName, item.OrderItemChess.Schedule) : Common.Constants.Payment.Donation);
                    AddMetaData(i + 1 + Common.Constants.Payment.UnderlineSeason, item.ProgramScheduleId.HasValue ? item.Season.Title : Common.Constants.Payment.Donation);
                }

            }

            if (installments != null)
            {
                var instFirst = !isRefund ? installments.First(i => !i.IsDeleted) : installments.First();

                AddMetaData(Common.Constants.Payment.InstallmentToken, instFirst.Token);

                AddMetaData(Common.Constants.Payment.Participant, string.Format(Constants.F_FullName, instFirst.OrderItem.FirstName, instFirst.OrderItem.LastName));

                for (int i = 0; i < installments.Count; i++)
                {
                    AddMetaData(i + 1 + Common.Constants.Payment.InstallmentDueDate, installments[i].InstallmentDate.ToString(Constants.DefaultDateFormat));

                    AddMetaData(i + 1 + Common.Constants.Payment.InstallmentAmount,
                        !isRefund ? installments[i].Balance.ToString() : refundAmount.ToString());
                }

            }

            if (transactions != null)
            {
                var transactionsGroupBy = transactions.GroupBy(t => t.OrderItemId).ToList();

                var transactioncategory = transactions.First().TransactionCategory;

                if (transactioncategory != TransactionCategory.Invoice && (transactioncategory == TransactionCategory.TakePayment && transactionsGroupBy.Count == 1))
                {
                    var tranFirst = transactions.First();

                    AddMetaData(Common.Constants.Payment.Email, tranFirst.Order.User.UserName);
                    AddMetaData(Common.Constants.Payment.Source, Common.Constants.Payment.JumbulaWithDash + tranFirst.Club.Domain);

                    //metaData.Add("TransactionToken", tranFirst.Token);
                    AddMetaData(Common.Constants.Payment.Participant, string.Format(Constants.F_FullName, tranFirst.OrderItem.FirstName, tranFirst.OrderItem.LastName));
                    AddMetaData(Common.Constants.Payment.TakePaymentAmount, transactions.Sum(c => c.Amount).ToString());

                    if (tranFirst.Season != null)
                    {
                        AddMetaData(Common.Constants.Payment.Season, tranFirst.Season.Title);
                    }

                }
                else
                {
                    var orderItemTransaction = transactions.Where(o => o.InstallmentId == null).ToList();
                    var instalmenttransaction = transactions.Where(i => i.InstallmentId != null).ToList();
                    var seasonDomains = new List<string>();

                    if (orderItemTransaction != null || instalmenttransaction != null)
                    {
                        var tranFirst = transactions.First();
                        var userName = tranFirst.Order.User.UserName;

                        AddMetaData(Common.Constants.Payment.Email, tranFirst.Order.User.UserName);
                        AddMetaData(Common.Constants.Payment.Source, Common.Constants.Payment.JumbulaWithDash + tranFirst.Club.Domain);

                        var orderTransactionGroup = orderItemTransaction.GroupBy(t => t.SeasonId);
                        var instTransactionGroup = instalmenttransaction.GroupBy(t => t.SeasonId);

                        if (orderItemTransaction != null)
                        {
                            foreach (var item in orderTransactionGroup)
                            {
                                var first = item.First().Season.Title;
                                seasonDomains.Add(first);
                            }
                        }

                        if (instalmenttransaction != null)
                        {
                            foreach (var item in instTransactionGroup)
                            {
                                var first = item.First().Season.Title;
                                seasonDomains.Add(first);
                            }
                        }

                        var strSeasonDomain = "";

                        if (seasonDomains.Any())
                        {
                            strSeasonDomain = string.Join(", ", seasonDomains);
                        }

                        AddMetaData(Common.Constants.Payment.UserName, string.Format(Constants.F_UserName, userName));
                        AddMetaData(Common.Constants.Payment.Season, strSeasonDomain);

                        for (int i = 0; i < instalmenttransaction.Count; i++)
                        {
                            AddMetaData(i + 1 + Common.Constants.Payment.InstallmentDueDate, instalmenttransaction[i].Installment.InstallmentDate.ToString(Constants.DefaultDateFormat));
                            AddMetaData(i + 1 + Common.Constants.Payment.InstallmentAmount, instalmenttransaction[i].Amount.ToString());
                        }
                    }

                    AddMetaData(Common.Constants.Payment.InvoiceAmount, transactions.Sum(c => c.Amount).ToString());
                }
            }

            if (client != null)
            {
                AddMetaData(Common.Constants.Payment.ClientId, client.Id.ToString());
                AddMetaData(Common.Constants.Payment.BillingId, billingId);
            }

            return _metaData;
        }

        private void AddMetaData(string key, string value)
        {
            if (_metaData.Count() < Common.Constants.Payment.StripeMaximumMetaData)
            {
                _metaData.Add(key, value);
            }

        }
        public PaymentResponseModel Pay(PayViewModel payModel, ControllerContext controllerContext, int clubId)
        {
            var _paymentGateway = Ioc.ClientBusiness.GetPaymentGatewayClub(clubId);

            var paymentResponseModel = new PaymentResponseModel();

            switch (payModel.PaymentMethod) //check select Payment Method
            {
                case PaymentMethod.None:
                    {
                        throw new NotSupportedException(string.Format(Constants.F_CardTypeCategoryNotSupported, payModel.PaymentMethod));
                    }
                case PaymentMethod.Paypal:
                    {
                        paymentResponseModel = PayCartByPaypal();
                    }
                    break;
                case PaymentMethod.Card:
                    {
                        switch (_paymentGateway) //select Payment Gateway
                        {
                            case PaymentGateway.Stripe:
                                {
                                    paymentResponseModel = PayCartByStripe(controllerContext, payModel);
                                }
                                break;
                            case PaymentGateway.AuthorizeNet:
                                {
                                    paymentResponseModel = PayCartByAutorizeNet(controllerContext, payModel);
                                }
                                break;
                            default:
                                {
                                    throw new NotSupportedException(string.Format(Constants.F_PaymentGetwayNotSupported, _paymentGateway));
                                }
                        }
                    }
                    break;
                case PaymentMethod.Cash:
                case PaymentMethod.Scholarship:
                case PaymentMethod.FinancialAid:
                case PaymentMethod.BankTransfer:
                case PaymentMethod.Other:
                    {
                        paymentResponseModel = PayCartByManually(payModel);
                    }
                    break;
                case PaymentMethod.Express:

                    switch (_paymentGateway)
                    {
                        case PaymentGateway.Stripe:
                            {
                                paymentResponseModel = PayCartExpressByStripe(controllerContext, payModel);
                            }
                            break;
                        case PaymentGateway.AuthorizeNet:
                            {
                                paymentResponseModel = PayCartExpressByAutorizeNet(controllerContext, payModel);
                            }
                            break;
                        default:
                            {
                                throw new NotSupportedException(string.Format(Constants.F_PaymentGetwayNotSupported, _paymentGateway));
                            }
                    }
                    break;
                default:
                    {
                        throw new NotSupportedException(string.Format(Constants.F_CardTypeCategoryNotSupported, payModel.PaymentMethod));
                    }

            }
            return paymentResponseModel;
        }

        private PaymentResponseModel PayCartByPaypal()
        {
            PaymentResponseModel response = new PaymentResponseModel();
            response.PaymentMethod = PaymentMethod.Paypal;
            return response;
        }
        private PaymentResponseModel PayCartByManually(PayViewModel model)
        {
            var orderDb = Ioc.OrderBusiness;

            string orderHistoryDescription = string.Empty;

            model.IsPrepaid = model.IsPrepaid && model.Cart.Order.OrderMode == OrderMode.Offline;

            if (!model.IsPrepaid)
            {
                orderHistoryDescription = Constants.Order_Initiated;
            }
            else
            {
                orderHistoryDescription = Constants.Order_Prepaid;
            }
            var date = model.Cart.Order.CompleteDate;
            model.Cart.Order.OrderStatus = OrderStatusCategories.completed;
            model.Cart.Order.IsDraft = false;
            decimal convfee = 0;

            // check if convenience fee hase value show real amount on button
            foreach (var item in model.Cart.Order.RegularOrderItems)
            {
                if (item.GetOrderChargeDiscounts().Any(c => c.Category == ChargeDiscountCategory.Convenience))
                {
                    convfee += item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).Sum(c => c.Amount);
                }
            }
            if (convfee != 0)
            {
                model.Cart.Order.OrderAmount = model.Cart.PayableAmount - convfee;

            }

            string installmentToken = string.Empty;
            foreach (var orderItem in model.Cart.Order.RegularOrderItems)
            {
                decimal convenienceAmount = 0;
                orderItem.ItemStatus = OrderItemStatusCategories.completed;
                orderItem.ItemStatusReason = OrderItemStatusReasons.regular;

                if (convfee != 0 && orderItem.ProgramScheduleId.HasValue)
                {
                    convenienceAmount = orderItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).Sum(c => c.Amount);
                    orderItem.TotalAmount = orderItem.TotalAmount - convenienceAmount;
                }

                if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
                {
                    if (string.IsNullOrEmpty(installmentToken))
                    {
                        installmentToken = Guid.NewGuid().ToString("N");
                    }

                    var firstInst = orderItem.Installments.FirstOrDefault(i => !i.IsDeleted);
                    if (orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.Convenience))
                    {
                        if (firstInst != null) firstInst.Amount = firstInst.Amount - convenienceAmount;
                    }

                    if (firstInst == null) continue;
                    firstInst.Status = OrderStatusCategories.initiated;
                    firstInst.Token = installmentToken;
                    firstInst.NoticeSent = true;
                }
            }

            foreach (var item in model.Cart.Order.RegularOrderItems)
            {
                if (item.OrderChargeDiscounts.All(c => !c.IsDeleted && c.Category != ChargeDiscountCategory.Convenience)) continue;
                {
                    var listDeletedConvenienceFee = item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Convenience).ToList();

                    if (listDeletedConvenienceFee.Any())
                    {
                        Ioc.OrderItemBusiness.RemoveOrderItemCharges(listDeletedConvenienceFee);
                    }
                }
            }

            var result = orderDb.Update(model.Cart.Order);
            orderDb.OrderCompleted(model.Cart.Order, orderHistoryDescription, model.UserId, JbUserService.GetCurrentUserId());

            PaymentResponseModel response = new PaymentResponseModel();
            response.PaymentMethod = PaymentMethod.Cash;

            if (result.Status)
                response.PaymentResult = PaymentResult.Fail;
            else
                response.PaymentResult = PaymentResult.Success;
            return response;
        }
        private PaymentResponseModel PayCartByStripe(ControllerContext controllerContext, PayViewModel payModel)
        {
            var stripeService = new JbStripeService(string.Empty, payModel.IsTestMode);

            var response = stripeService.DirectPay(controllerContext, payModel); //Pay with stripe
            response.PaymentMethod = PaymentMethod.Card;

            return response;
        }
        private PaymentResponseModel PayCartExpressByStripe(ControllerContext controllerContext, PayViewModel payModel)
        {
            var stripeService = new JbStripeService(string.Empty, payModel.IsTestMode);

            var response = stripeService.DirectPay(controllerContext, payModel);
            response.PaymentMethod = PaymentMethod.Express;

            return response;
        }
        private PaymentResponseModel PayCartExpressByAutorizeNet(ControllerContext controllerContext, PayViewModel payModel)
        {
            var autorizeNetService = new JbAuthorizeNetService(string.Empty, payModel.IsTestMode);

            var response = autorizeNetService.DirectPay(controllerContext, payModel, JbUserService.GetCurrentClubDomain());
            response.PaymentMethod = PaymentMethod.Express;

            return response;
        }
        private PaymentResponseModel PayCartByAutorizeNet(ControllerContext controllerContext, PayViewModel payModel)
        {
            var autorizeNetService = new JbAuthorizeNetService(string.Empty, payModel.IsTestMode);

            var response = autorizeNetService.DirectPay(controllerContext, payModel, JbUserService.GetCurrentClubDomain());
            response.PaymentMethod = PaymentMethod.Card;

            return response;
        }

        public UserCreditCard CreateCreditCard(PaymentModels source, int userId, PostalAddress Address, string customerId, string CardId, string customerResponse, bool isDefault, PaymentGateway typeGateway, bool isMakeaPayment, bool isOnline)
        {
            var club = Ioc.ClubBusiness.Get(JbUserService.GetCurrentClubDomain());

            string cardId;
            UserCreditCardType typeCard;
            DateTime? defaultDate = null;

            if (isDefault)
            {
                defaultDate = DateTime.UtcNow;
            }

            if (customerId == "")
            {
                cardId = "";
                isDefault = false;

                if (isMakeaPayment && !isOnline)
                {
                    typeCard = UserCreditCardType.OfflineHistory;
                }
                else
                {
                    typeCard = UserCreditCardType.OnlineHistory;
                }
            }
            else
            {

                cardId = CardId;

                if (isMakeaPayment && !isOnline)
                {
                    typeCard = UserCreditCardType.InstallmentOfflineRegistration;
                }
                else
                {
                    typeCard = UserCreditCardType.InstallmentOnlineRegistration;
                }
            }

            var creditcard = new UserCreditCard()
            {
                Brand = source.Brand,
                ClubId = club.Id,
                ExpiryMonth = source.Month.Length == 1 ? ("0" + source.Month) : source.Month,
                ExpiryYear = source.Year,
                UserId = userId,
                LastDigits = source.CardNumber.Substring((source.CardNumber.Length) - 4, 4),
                Name = source.CardHolderName,
                CustomerId = customerId,
                CustomerResponse = customerResponse,
                CardId = cardId,
                IsDefault = isDefault,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = null,
                DefaultDate = defaultDate,
                DeletedDate = null,
                TypePaymentGateway = typeGateway,
                TypeCreditCard = typeCard

            };

            try
            {
                creditcard.Address = new PostalAddress
                {
                    Country = Address.Country,
                    State = Address.State,
                    City = Address.City,
                    Zip = Address.Zip,
                    Street = Address.Street,
                    Address = Address.Address,
                    Lng = Address.Lng,
                    Lat = Address.Lat,
                    Route = Address.Route,
                    TimeZone = Address.TimeZone,
                    StreetNumber = Address.StreetNumber,
                    County = Address.County,
                    AutoCompletedAddress = Address.AutoCompletedAddress
                };
            }
            catch
            {

            }

            Ioc.UserCreditCardBusiness.Create(creditcard);

            return creditcard;
        }
        public OperationStatus TakeAPaymentNowWithStripe(TakePaymentViewModel model, CurrencyCodes currency, List<FamilyOrderItemsViewModel> orderItems)
        {
            try
            {
                var token = Guid.NewGuid().ToString("N");
                List<OrderInstallment> selectedInstallment = null;
                foreach (var orderItem in orderItems)
                {
                    if (orderItem.Installments != null && orderItem.Installments.Any())
                    {
                        selectedInstallment = new List<OrderInstallment>();
                        foreach (var inst in orderItem.Installments.Where(c => c.Checked))
                        {
                            selectedInstallment.Add(Ioc.InstallmentBusiness.Get(inst.id));
                        }
                    }

                    var orderDb = Ioc.OrderBusiness;
                    var orderItemdb = Ioc.OrderItemBusiness.GetItem(orderItem.Id);

                    var paymentDetail = new PaymentDetail(model.UserName, PaymentDetailStatus.PROCESSING, "", PaymentMethod.Card, currency, string.Empty, string.Empty, string.Empty, JbUserService.GetCurrentUserRoleType());
                    if (selectedInstallment != null && selectedInstallment.Count > 0)
                    {
                        var totalPaidAmount = orderItem.Balance;
                        foreach (var inst in selectedInstallment)
                        {
                            if (totalPaidAmount <= 0)
                            {
                                break;
                            }
                            var instBalance = inst.Amount - (inst.PaidAmount.HasValue ? inst.PaidAmount.Value : 0);

                            if (instBalance > totalPaidAmount)
                            {
                                instBalance = totalPaidAmount;
                            }

                            totalPaidAmount -= instBalance;

                            orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Draft, null, model.PaymentDate, instBalance, orderItemdb, inst.Id, model.Note, token, model.CheckId, TransactionCategory.TakePayment);
                        }
                    }
                    else
                    {
                        orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Draft, null, null, orderItem.Balance, orderItemdb, null, model.Note, token, model.CheckId, TransactionCategory.TakePayment);
                    }
                    Ioc.OrderItemBusiness.Update(orderItemdb);
                }

                var urlToRedirect = string.Format("{0}/Cart?actionCaller={1}&token={2}&backToDetail={3}&isFamilyTakePayment={4}", HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority), JumbulaSubSystem.MakeaPayment, token, false, true);

                return new OperationStatus() { Status = true, Message = urlToRedirect };
            }
            catch
            {
                throw;
            }

        }

        public dynamic ChargeInstallment(OrderInstallment installment)
        {
            Club club = null;
            var orderItem = Ioc.OrderItemBusiness.GetItem(installment.OrderItemId);
            var dueAmount = Ioc.AccountingBusiness.OrderItemDueAmount(orderItem);
            JbPaymentService paymentService = new JbPaymentService();
            dynamic result = new ExpandoObject();

            var errorMsg = string.Empty;
            var token = string.Empty;
            var ipn = string.Empty;
            PaymentGateway _paymentGateway;
            var activeUserCreditCard = new UserCreditCard();

            try
            {
                if (dueAmount > 0)
                {
                    var payableAmount = installment.Balance;
                    if (payableAmount > dueAmount)
                    {
                        payableAmount = dueAmount;
                    }

                    club = Ioc.ClubBusiness.Get(orderItem.Order.ClubId);
                    _paymentGateway = Ioc.ClientBusiness.GetPaymentGatewayClub(club.Id);

                    activeUserCreditCard = Ioc.UserCreditCardBusiness.GetActiveUserCreditCards(installment.OrderItem.Order.UserId, _paymentGateway);

                    var userCreditCards = Ioc.UserCreditCardBusiness.GetUserCreditCards(installment.OrderItem.Order.UserId, _paymentGateway).ToList();

                    if (!userCreditCards.Any())
                    {
                        errorMsg = "No credit card on file.";
                    }
                    else if (activeUserCreditCard == null)
                    {
                        errorMsg = "No default credit card on file.";
                    }

                    if (activeUserCreditCard != null)
                    {

                        var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

                        var clientId = clinetPaymentMethod.Client.Id;
                        var stripeAccessToken = clinetPaymentMethod.StripeAccessToken;
                        var currency = club.Currency;
                        var customerId = clinetPaymentMethod.StripeCustomerId;

                        if (club.PartnerId.HasValue && club.PartnerClub != null && club.IsSchool)
                        {
                            var client = Ioc.ClientBusiness.Get(club.PartnerId.Value);
                            currency = club.PartnerClub.Currency;
                            stripeAccessToken = clinetPaymentMethod.StripeAccessToken;
                            clientId = client.Id;
                            customerId = clinetPaymentMethod.StripeCustomerId;
                        }

                        var cardId = "";


                        var tmpInstallmentList = new List<OrderInstallment>();
                        tmpInstallmentList.Add(installment);
                        var metadata = GenerateMetaData(JumbulaSubSystem.Installment, null, tmpInstallmentList);
                        var stripeService = new JbStripeService(club.Domain, false);
                        var authorizeNetService = new JbAuthorizeNetService(club.Domain, false);


                        if (_paymentGateway == PaymentGateway.Stripe)
                        {
                            if (activeUserCreditCard.CardId == null)
                            {
                                var json = activeUserCreditCard.CustomerResponse;
                                if (json != null)
                                {
                                    dynamic data = JObject.Parse(json);
                                    cardId = data.DefaultSourceId;
                                }
                            }
                            else { cardId = activeUserCreditCard.CardId; }

                            token = stripeService.GetSharedTokenId(activeUserCreditCard.CustomerId, stripeAccessToken, cardId);
                            StripeCharge stripeCharge = stripeService.Charge(clientId, payableAmount, currency, token, metadata, orderItem.Order.ConfirmationId, customerId, stripeAccessToken, installment.Id, tmpInstallmentList);
                            ipn = JsonConvert.SerializeObject(stripeCharge);

                            if (stripeCharge.Status.ToLower().Equals(StripeChargeStatus.succeeded.ToString()) && stripeCharge.Paid)
                            {
                                stripeService.UpdateInstallmentBasedOnPayment(tmpInstallmentList, stripeCharge, ipn, activeUserCreditCard.Id, DateTime.UtcNow, RoleCategoryType.System, payableAmount);

                                result.Status = true;
                                result.message = null;
                                result.ordermode = orderItem.Order.OrderMode.DisplayName();
                                result.ClubDomain = orderItem.Order.Club.Domain;
                                result.UserName = orderItem.Order.User.UserName;
                                return result;
                            }
                            else
                            {
                                errorMsg = stripeCharge.FailureMessage;
                            }
                        }
                        else
                        {
                            if (activeUserCreditCard.CardId == null)
                            {
                                var json = activeUserCreditCard.CustomerResponse;
                                if (json != null)
                                {
                                    dynamic data = JObject.Parse(json);
                                    cardId = data.customerProfileId;
                                }
                            }
                            else { cardId = activeUserCreditCard.CardId; }


                            var modelCard = new PaymentModels()
                            {
                                CardNumber = activeUserCreditCard.LastDigits,
                                Month = activeUserCreditCard.ExpiryMonth,
                                Year = activeUserCreditCard.ExpiryYear,
                                CardHolderName = activeUserCreditCard.Name,
                                Address = activeUserCreditCard.Address,
                                UserId = activeUserCreditCard.UserId,
                            };

                            AuthorizeNetChargeResponse authorizeNetCharge;

                            authorizeNetCharge = authorizeNetService.Charge(clientId, currency, payableAmount, modelCard, activeUserCreditCard.Address, activeUserCreditCard.CustomerId, cardId);

                            ipn = Newtonsoft.Json.JsonConvert.SerializeObject(authorizeNetCharge);

                            if (authorizeNetCharge.Response.messages.resultCode == messageTypeEnum.Ok)
                            {
                                if (authorizeNetCharge.Response.transactionResponse.messages != null)
                                {
                                    authorizeNetService.UpdateInstallmentBasedOnPayment(tmpInstallmentList, authorizeNetCharge.Response, ipn, activeUserCreditCard.Id, DateTime.UtcNow, RoleCategoryType.System, payableAmount);

                                    result.Status = true;
                                    result.message = null;
                                    result.ordermode = orderItem.Order.OrderMode.DisplayName();
                                    result.ClubDomain = orderItem.Order.Club.Domain;
                                    result.UserName = orderItem.Order.User.UserName;
                                    return result;
                                }
                                else
                                {
                                    if (authorizeNetCharge.Response.transactionResponse.errors != null)
                                    {
                                        foreach (var item in authorizeNetCharge.Response.transactionResponse.errors)
                                        {

                                            errorMsg = item.errorText;

                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (authorizeNetCharge.Response.transactionResponse != null && authorizeNetCharge.Response.transactionResponse.errors != null)
                                {
                                    foreach (var item in authorizeNetCharge.Response.transactionResponse.errors)
                                    {
                                        if (item.errorCode == "17")
                                        {
                                            errorMsg = item.errorText;
                                        }
                                        else
                                        {
                                            errorMsg = item.errorText;
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var item in authorizeNetCharge.Response.messages.message)
                                    {
                                        errorMsg = item.text;


                                    }
                                }

                            }
                        }

                    }
                }
            }
            catch (StripeException stripeException)
            {
                errorMsg = stripeException.Message;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }

            var orderdb = Ioc.OrderBusiness;
            var transactionId = Ioc.PaymentDetailBusiness.GetTransactionIdFromResponse(ipn);
            var paymentDetail = new PaymentDetail(orderItem.Order.User.UserName, PaymentDetailStatus.ERROR, token, PaymentMethod.Card, orderItem.Order.Club.Currency, ipn, errorMsg, transactionId, RoleCategoryType.System);


            orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, installment, DateTime.UtcNow);

            if (activeUserCreditCard != null && installment.InstallmentDate.Date.AddDays(4) <= DateTime.UtcNow.Date && !string.IsNullOrEmpty(errorMsg))
            {
                installment.Status = OrderStatusCategories.error;
            }

            Ioc.InstallmentBusiness.Update(installment);
            result.Status = false;
            result.message = " ----  Error Msg: " + errorMsg;
            result.ordermode = orderItem.Order.OrderMode.DisplayName();
            result.ClubDomain = orderItem.Order.Club.Domain;
            result.UserName = orderItem.Order.User.UserName;

            return result;

        }



        public JbPaymentResult ChargeInstallmentNew(OrderInstallment installment, Club club, DateTime? dueDate)
        {
            var result = new JbPaymentResult();
            var orderItem = Ioc.OrderItemBusiness.GetItem(installment.OrderItemId);
            if (orderItem == null)
            {
                result.Succeed = false;
                result.OrderMode = "null";
                result.Message = "order item is null";
                return result;
            }

            var errorMsg = string.Empty;
            var token = string.Empty;
            var ipn = string.Empty;
            var cardId = "";
            int? activeUserCreditCardId = null;

            var dueAmount = installment.Balance;

            try
            {
                if (dueAmount > 0)
                {
                    var payableAmount = installment.Balance;
                    payableAmount = payableAmount > dueAmount ? dueAmount : payableAmount;

                    var paymentGateway = Ioc.ClientBusiness.GetPaymentGatewayClub(club.Id);
                    var activeUserCreditCard = Ioc.UserCreditCardBusiness.
                        GetActiveUserCreditCards(installment.OrderItem.Order.UserId, paymentGateway);

                    var userCreditCards = Ioc.UserCreditCardBusiness.GetUserCreditCards(installment.OrderItem.Order.UserId, paymentGateway).ToList();

                    if (!userCreditCards.Any())
                    {
                        errorMsg = "No credit card on file.";
                    }
                    else if (activeUserCreditCard == null)
                    {
                        errorMsg = "No default credit card on file.";
                    }
                    else
                    {

                        var clinetPaymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);
                        var clientId = clinetPaymentMethod.Client.Id;
                        var stripeAccessToken = clinetPaymentMethod.StripeAccessToken;
                        var currency = club.Currency;
                        var customerId = clinetPaymentMethod.StripeCustomerId;

                        if (club.PartnerId.HasValue && club.IsSchool)
                        {
                            var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(club.PartnerId.Value);
                            var stripeSettingsFromMember = partnerSetting.PortalParameters.ReadStripeSettingsFromMember;
                            if (!stripeSettingsFromMember)
                            {
                                var client = Ioc.ClientBusiness.Get(club.PartnerId.Value);
                                currency = club.PartnerClub.Currency;
                                stripeAccessToken = clinetPaymentMethod.StripeAccessToken;
                                clientId = client.Id;
                                customerId = clinetPaymentMethod.StripeCustomerId;
                            }
                        }


                        var tmpInstallmentList = new List<OrderInstallment>
                        {
                            installment
                        };

                        var metadata = GenerateMetaData(JumbulaSubSystem.Installment, null, tmpInstallmentList);


                        if (paymentGateway == PaymentGateway.Stripe)
                        {
                            if (activeUserCreditCard.CardId == null)
                            {
                                var json = activeUserCreditCard.CustomerResponse;
                                if (json != null)
                                {
                                    dynamic data = JObject.Parse(json);
                                    cardId = data.DefaultSourceId;
                                    // activeUserCreditCardId = activeUserCreditCard.Id;
                                }
                            }
                            else
                            {
                                cardId = activeUserCreditCard.CardId;
                                activeUserCreditCardId = activeUserCreditCard.Id;
                            }

                            var stripeService = new JbStripeService(club.Domain, false);

                            var stopWatch = new Stopwatch();
                            stopWatch.Start();

                            var paymentSucceed = true;

                            token = stripeService.GetSharedTokenId(activeUserCreditCard.CustomerId, stripeAccessToken, cardId);

                            StripeCharge stripeCharge = null;
                            try
                            {
                                stripeCharge = stripeService.Charge(clientId, payableAmount, currency,
                                    token, metadata, orderItem.Order.ConfirmationId, customerId, stripeAccessToken,
                                    installment.Id, tmpInstallmentList);
                                ipn = JsonConvert.SerializeObject(stripeCharge);
                            }
                            catch (StripeException ex)
                            {
                                paymentSucceed = false;
                                ipn = JsonConvert.SerializeObject(ex.StripeError);
                                errorMsg = ex.Message;
                            }


                            stopWatch.Stop();


                            Log.Information("{LogCategory} {AutoChargeMessageType} {DueDate} {GatewaysCallTime} {InstallmentID}",
                                "Auto Charge", "Stripe Call", dueDate, stopWatch.Elapsed.TotalMilliseconds, installment.Id);



                            if (paymentSucceed && stripeCharge.Status.ToLower().Equals(StripeChargeStatus.succeeded.ToString()) && stripeCharge.Paid)
                            {
                                stripeService.UpdateInstallmentBasedOnPayment(tmpInstallmentList, stripeCharge, ipn, activeUserCreditCard.Id, DateTime.UtcNow, RoleCategoryType.System, payableAmount);

                                result.Succeed = true;
                                result.Message = null;
                                result.OrderMode = orderItem.Order.OrderMode.DisplayName();
                                result.ClubDomain = orderItem.Order.Club.Domain;
                                result.UserName = orderItem.Order.User.UserName;
                                return result;
                            }

                        }
                        else
                        {
                            if (activeUserCreditCard.CardId == null)
                            {
                                var json = activeUserCreditCard.CustomerResponse;
                                if (json != null)
                                {
                                    dynamic data = JObject.Parse(json);
                                    cardId = data.customerProfileId;
                                    // activeUserCreditCardId = activeUserCreditCard.Id;
                                }
                            }
                            else
                            {
                                activeUserCreditCardId = activeUserCreditCard.Id;
                                cardId = activeUserCreditCard.CardId;
                            }


                            var modelCard = new PaymentModels()
                            {
                                CardNumber = activeUserCreditCard.LastDigits,
                                Month = activeUserCreditCard.ExpiryMonth,
                                Year = activeUserCreditCard.ExpiryYear,
                                CardHolderName = activeUserCreditCard.Name,
                                Address = activeUserCreditCard.Address,
                                UserId = activeUserCreditCard.UserId,
                            };

                            var stopWatch = new Stopwatch();
                            stopWatch.Start();

                            var authorizeNetService = new JbAuthorizeNetService(club.Domain, false);
                            var authorizeNetCharge = authorizeNetService.Charge(clientId, currency, payableAmount,
                                modelCard, activeUserCreditCard.Address, activeUserCreditCard.CustomerId, cardId);

                            stopWatch.Stop();


                            Log.Information("{LogCategory} {AutoChargeMessageType} {DueDate} {GatewaysCallTime} {InstallmentID}",
                                "Auto Charge", "AuthorizedNet Call", dueDate, stopWatch.Elapsed.TotalMilliseconds, installment.Id);

                            ipn = JsonConvert.SerializeObject(authorizeNetCharge);

                            if (authorizeNetCharge.Response.messages.resultCode == messageTypeEnum.Ok)
                            {
                                if (authorizeNetCharge.Response.transactionResponse.messages != null)
                                {
                                    authorizeNetService.UpdateInstallmentBasedOnPayment(tmpInstallmentList,
                                        authorizeNetCharge.Response, ipn, activeUserCreditCard.Id, DateTime.UtcNow,
                                        RoleCategoryType.System, payableAmount);

                                    result.Succeed = true;
                                    result.Message = null;
                                    result.OrderMode = orderItem.Order.OrderMode.DisplayName();
                                    result.ClubDomain = orderItem.Order.Club.Domain;
                                    result.UserName = orderItem.Order.User.UserName;
                                    return result;
                                }
                                else
                                {
                                    if (authorizeNetCharge.Response.transactionResponse.errors != null)
                                    {
                                        foreach (var item in authorizeNetCharge.Response.transactionResponse.errors)
                                        {
                                            errorMsg = item.errorText;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (authorizeNetCharge.Response.transactionResponse != null &&
                                    authorizeNetCharge.Response.transactionResponse.errors != null)
                                {
                                    foreach (var item in authorizeNetCharge.Response.transactionResponse.errors)
                                    {
                                        errorMsg = item.errorText;
                                    }
                                }
                                else
                                {
                                    foreach (var item in authorizeNetCharge.Response.messages.message)
                                    {
                                        errorMsg = item.text;
                                    }
                                }

                            }
                        }

                    }
                }
            }
            catch (StripeException stripeException)
            {
                errorMsg = stripeException.Message;
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }

            var orderdb = Ioc.OrderBusiness;
            var transactionId = Ioc.PaymentDetailBusiness.GetTransactionIdFromResponse(ipn);
            //            var paymentDetail = new PaymentDetail(orderItem.Order.User.UserName, PaymentDetailStatus.ERROR, token,
            //              PaymentMethod.Card, orderItem.Order.Club.Currency, ipn, errorMsg, transactionId, activeUserCreditCardId);

            var paymentDetail = new PaymentDetail(installment.OrderItem.Order.User.UserName, PaymentDetailStatus.ERROR, token,
                PaymentMethod.Card, installment.OrderItem.Order.Club.Currency, ipn, errorMsg, transactionId, RoleCategoryType.Dashboard, activeUserCreditCardId);

            orderdb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, installment, DateTime.UtcNow);

            var odbresult = Ioc.InstallmentBusiness.Update(installment);

            result.Succeed = false;
            result.Message = errorMsg;
            result.OrderMode = orderItem.Order.OrderMode.DisplayName();
            result.ClubDomain = orderItem.Order.Club.Domain;
            result.UserName = orderItem.Order.User.UserName;

            return result;
        }

        public RefundResultModel Refund(TransactionActivity transactionActivity, List<OrderInstallment> orderInstallments, OrderItem orderItem, string clubDomain, decimal refundAmount, bool isTestMode)
        {
            var paymentGatewayService = BuildPaymentGatewayService(transactionActivity.Club, transactionActivity.PaymentDetail.PaymentMethod == PaymentMethod.Paypal, isTestMode);

            var metadata = orderItem == null ? GenerateMetaData(JumbulaSubSystem.Installment, null, orderInstallments, null, null, null, true, refundAmount) :
                GenerateMetaData(JumbulaSubSystem.Order, orderItem.Order, null, null, null, null, true, refundAmount);

            var result = paymentGatewayService.Refund(transactionActivity, clubDomain, refundAmount, isTestMode, metadata);
            result.RefundAmount = refundAmount;

            return result;
        }

        private IPaymentGatewayService BuildPaymentGatewayService(Club club, bool isPaypalTransaction, bool isTestMode)
        {
            var paymentGateway = Ioc.ClientBusiness.GetPaymentGatewayClub(club.Id);

            if (isPaypalTransaction)
                return new PaypalService(club.Domain, !isTestMode);

            switch (paymentGateway)
            {
                case PaymentGateway.Stripe:
                    return new JbStripeService(club.Domain, isTestMode);
                case PaymentGateway.AuthorizeNet:
                    return new JbAuthorizeNetService(club.Domain, isTestMode);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public PaymentDetailModel GetPaymentDetails(string clubDomain, string payKey, IEnumerable<TransactionActivity> refunded, bool isPaypalTransaction, bool isTestMode)
        {
            var club = Ioc.ClubBusiness.Get(clubDomain);
            var paymentGatewayService = BuildPaymentGatewayService(club, isPaypalTransaction, isTestMode);

            return paymentGatewayService.GetPaymentDetails(clubDomain, payKey, refunded, isPaypalTransaction, isTestMode);
        }
    }

    public class PaymentResponseModel
    {
        public PaymentMethod PaymentMethod { get; set; }
        public Dictionary<string, string> cardErrors { get; set; }
        public string errorMsg { get; set; }
        public PaymentResult PaymentResult { get; set; }
    }

    public enum PaymentResult
    {
        Success,
        Fail
    }

}


