﻿using System;
using System.Text.RegularExpressions;


namespace Jumbula.Common.Helper
{
    public static class JumbulaDomainHelper
    {
        public static string GenerateRandomDomain(string baseName, int iteration)
        {

            var random = new RandomExtension();

            baseName = baseName.Replace(".", "");
            baseName = Regex.Replace(baseName, "[^a-zA-Z0-9_.]+", string.Empty); // remove special chars
            baseName = Regex.Replace(baseName, "\t|\n|\r", string.Empty); // remove spaces|

            string name;

            if (iteration == 0)
            {
                if (string.IsNullOrEmpty(baseName))
                {
                    name = Constants.Constants.S_UnderScore + random.Generate();
                }
                else
                {
                    name = baseName;
                }
            }
            else
            {
                name = baseName + Constants.Constants.S_UnderScore + random.Generate();
            }

            return name;
        }

        public static string RandomSubDomainName(string baseName)
        {

            string result = baseName;
            result = Regex.Replace(result, "[^a-zA-Z0-9_.]+", string.Empty); // remove special chars
            result = Regex.Replace(result, "\t|\n|\r", string.Empty); // remove spaces
            return result;
        }
    }

    public class RandomExtension
    {
        const int Size = 1;
        private readonly Random _random;

        public RandomExtension()
        {
            _random = new Random();
        }

        public string Generate()
        {
            byte[] randByte = new byte[Size];
            _random.NextBytes(randByte);

            return randByte[0].ToString();
        }


    }


}
