﻿using System;

namespace Jumbula.Common.Attributes
{
    public class JbCustomFieldAttribute : Attribute
    {
        public JbCustomFieldAttribute()
        {

        }

        public JbCustomFieldAttribute(bool isenabled, bool isReadOnly, bool showInGrid)
        {
            IsEnabled = isenabled;
            IsReadOnly = isReadOnly;
            ShowInGrid = showInGrid;
        }

        public bool IsEnabled { get; set; }

        public bool IsReadOnly { get; set; }

        public bool ShowInGrid { get; set; }
    }
}
