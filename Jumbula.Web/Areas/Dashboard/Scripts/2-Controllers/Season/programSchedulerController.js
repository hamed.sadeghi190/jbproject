﻿
(function () {
    "use strict";

    angular.module("dashboardApp").controller("programSchedulerController", ["$scope", "$http", "jbToast", "$stateParams", "$state", "jbConfirmation", "customReportService", "$timeout", function ($scope, $http, jbToast, $stateParams, $state, jbConfirmation, customReportService, $timeout) {
        window.app.globalObjects.initializeView();
        $scope.MC.collapseAllBlades();

        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/Season";
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.MC.stateParams = $stateParams;
        var timer = false;
        $scope.currentView = "month";
        $scope.readCount = 0;

        $scope.Model = {};

        $scope.seasonIds = [];
        $scope.startDate = null;
        $scope.endDate = null;

        $scope.isInSeasonMode = function () {
            return Object.keys($scope.MC.stateParams).length !== 0;
        };

        function getClubSeason() {

            $http.get($scope.baseUrl + "/GetLiveSeasonsClub").success(
                function (data) {

                    $scope.AllSeasons = data;

                    $scope.$watch($scope.AllSeasons, function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $("select[chosen]").trigger("chosen:updated");
                        }, 1500);
                    });

                }).error(function (data) {
                    jbToast.error(data.Message);
                });
        }

        var prepareCalendar = function () {
            var date = new Date();

            $scope.schedulerOptions = {
                date: new Date(),
                workDayStart: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 6, 0, 0),
                workDayEnd: new Date(date.getFullYear(), date.getMonth(), date.getDate(), 20, 0, 0),
                showWorkHours: true,
                editable: false,
                views: [
                    "day",
                    "week",
                    { type: "month", selected: true },
                    "agenda",
                    { type: "timeline", eventHeight: 100 }
                ],
                navigate: function (e) {

                    $scope.resizeScheduler(e.date);



                    $scope.currentView = e.view;

                    //reload current view
                    var scheduler = $("#scheduler").data("kendoScheduler");
                    scheduler.view(scheduler.view().name);
                    scheduler.resize();

                },
                dataBound: function (e) {

                    var view = e.sender.view();

                    if (!$scope.isEqualDates(view._startDate, $scope.startDate)) {
                        
                        $scope.startDate = view._startDate; 
                        $scope.endDate = view._endDate; 

                        $("#scheduler").data("kendoScheduler").dataSource.read();
                    }
                    

                    if (e.sender.view().name === "day" || e.sender.view().name === "week") {

                        $(e.sender.element).find(".k-event, .k-more-events").css("border-style", "solid");
                        $(e.sender.element).find(".k-event, .k-more-events").css("border-color", "black");
                        $(e.sender.element).find(".greenStatus").css("margin-bottom", "16px");
                        $(e.sender.element).find(".redStatus").css("margin-bottom", "16px");

                    } else if (e.sender.view().name === "agenda") {

                        $(e.sender.element).find(".greenStatus").css("margin-bottom", "16px");
                        $(e.sender.element).find(".redStatus").css("margin-bottom", "16px");

                    } else if (e.sender.view().name === "timeline") {

                        $(e.sender.element).find(".k-event, .k-more-events").css("border-style", "solid");
                        $(e.sender.element).find(".k-event, .k-more-events").css("border-color", "black");
                    }

                    $(e.sender.element).find(".k-scheduler-content div.k-event").css("background-color", "white").css("color", "black");

                    if ($scope.readCount === 1) {
                        $scope.readCount++;
                        $scope.firstInitializeHeight();
                    } else {
                        $scope.readCount++;
                    }

                },
                dataSource: {
                    batch: true,
                    dataType: "json",
                    transport: {
                        read: {
                            url: $scope.baseUrl + "/GetSeasonProgramCalendarItems",
                            type: "POST"
                        },
                        parameterMap: function () {
                            return {
                                seasonIds: $scope.seasonIds,
                                startDate: $scope.startDate !== null ? $scope.startDate.getMonth() + 1 + "/" + $scope.startDate.getDate() + "/" + $scope.startDate.getFullYear() : null,
                                endDate: $scope.endDate !== null ? $scope.endDate.getMonth() + 1 + "/" + $scope.endDate.getDate() + "/" + $scope.endDate.getFullYear() : null
                            };
                        }
                    },
                    schema: {
                        model: {
                            id: "taskId",
                            fields: {
                                title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                                start: { type: "date", from: "Start" },
                                end: { type: "date", from: "End" },
                                isAllDay: { from: "IsAllDay" },
                                programSessionId: { from: "ProgramSessionId" },
                                date: { from: "Date" },
                                isFull: { from: "IsFull" },
                                strStartTime: { from: "StrStartTime" },
                                strEndTime: { from: "StrEndTime" },
                                programId: { from: "ProgramId" },
                                seasonDomain: { from: "SeasonDomain" },
                                capacity: { from: "Capacity" },
                                enrollment: { from: "Enrollment" },
                                colors: { from: "Colors" }
                            }
                        },
                        data: function (response) {
                            return response;
                        }
                    }
                }
            };
        };

        if ($scope.isInSeasonMode()) {

            $http.get($scope.baseUrl + "/GetClubSeasonsDomain").success(
                function (data) {

                    for (var i = 0; i < data.length; i++) {
                        var season = data[i];
                        if (season.Value === $scope.MC.stateParams.seasonDomain) {
                            $scope.seasonIds.push(season.Key);
                            prepareCalendar();
                            break;
                        }
                    }

                }).error(function (data) {
                    jbToast.error(data.Message);
                });

        } else {
            getClubSeason();
            prepareCalendar();
        }

        $scope.showPopup = function (data) {

            $scope.showProgramActions = true;
            $scope.Model = data;
        };

        $scope.dismissPopup = function () {

            $scope.showProgramActions = false;
        };

        $scope.offlineRegister = function () {

            $http.post(window.location.origin + "/Dashboard/Order/DoesOfflineOrderExist/")
                .success(function (data) {
                    if (data.Status === true) {
                        var url;
                        if (data.RecordsAffected === 0) {
                            url = $state.href("Register", ({ seasonDomain: $scope.Model.seasonDomain, programId: $scope.Model.programId, Type: "register" }));
                            window.open(url, "_blank");
                        }
                        else {
                            url = $state.href("ConfirmNewRegisteration", ({ seasonDomain: $scope.Model.seasonDomain, programId: $scope.Model.programId }));
                            window.open(url, "_blank");
                        }
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

        $scope.rosterReport = function () {

            var url = $state.href("AdvanceClassRosterReportProgramSchedulerRun", { seasonDomain: $scope.Model.seasonDomain, programId: $scope.Model.programId });
            window.open(url, "_blank");
        };

        $scope.viewOrders = function () {

            var url = $state.href("Program", { seasonDomain: $scope.Model.seasonDomain, programId: $scope.Model.programId });
            window.open(url, "_blank");
        };

        $scope.waitList = function () {

            var url = $state.href("ProgramWaitlist", { seasonDomain: $scope.Model.seasonDomain, programId: $scope.Model.programId });
            window.open(url, "_blank");
        };

        $scope.viewProgram = function () {
            window.open($scope.Model.ViewPageUrl, "_blank");
        };

        $scope.firstInitializeHeight = function () {
            var scheduler = $("#scheduler").data("kendoScheduler");
            var date = new Date();
            $scope.resizeScheduler(date);
            scheduler.view(scheduler.view().name);
            scheduler.resize();
        };

        $scope.resizeScheduler = function (date) {

            var defaultGridHeight = "7em";
            var days = [];

            var scheduler = $("#scheduler").data("kendoScheduler");

            for (var i = 0; i < scheduler.dataSource._data.length; i++) {
                var element = scheduler.dataSource._data[i];

                if (element.start.getFullYear() === date.getFullYear() && element.start.getMonth() === date.getMonth()) {

                    var year = element.start.getFullYear();
                    var month = element.start.getMonth() + 1;
                    var day = element.start.getDate();

                    days.push(new Date(year + "/" + month + "/" + day));
                }

            }

            var counts = days.reduce(function (counts, val) {
                if (counts[val]) {
                    counts[val]++;
                } else {
                    counts[val] = 1;
                }
                return counts;
            }, {});

            var duplicateDays = [];
            for (var i = 0; i < Object.keys(counts).length; i++) {
                duplicateDays.push(counts[Object.keys(counts)[i]]);
            }

            var maxDay = 1;

            if (duplicateDays.length !== 0) {
                maxDay = arrayMax(duplicateDays);
            }

            $scope.Size = maxDay === 1 || duplicateDays.length === 0 ? defaultGridHeight : maxDay * 38 + "px";
        };

        function arrayMax(arr) {
            return arr.reduce(function (p, v) {
                return (p > v ? p : v);
            });
        }

        $scope.runCalendar = function () {
            
            $scope.readCount = 1;
            $("#scheduler").data("kendoScheduler").dataSource.read();

        };

        $scope.showTooltip = function (data) {
            var result = "";

            if (data.strStartTime !== null) {
                result += data.strStartTime + " - " + data.strEndTime + " ";
            }

            result += data.title + "\n Sold " + data.enrollment;

            if (data.capacity !== "" && data.capacity !== "Unlimited") {
                result += "/" + data.capacity;
            }

            return result;
        };

        $scope.isEqualDates = function (currentDate, prevDate) {

            if (prevDate === null) return false;

            var currentYear = currentDate.getFullYear();
            var currentMonth = currentDate.getMonth() + 1;
            var currentDay = currentDate.getDate();

            var prevYear = prevDate.getFullYear();
            var prevMonth = prevDate.getMonth() + 1;
            var prevDay = prevDate.getDate();

            if (currentYear === prevYear && currentMonth === prevMonth && currentDay === prevDay) {
                return true;
            }

            return false;
        };


    }]);
})();