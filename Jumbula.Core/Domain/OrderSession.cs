﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class OrderSession : BaseEntity<long>
    {
        public long OrderItemId { get; set; }
        public virtual OrderItem OrderItem { get; set; }

        public virtual ProgramSession ProgramSession { get; set; }

        public int? ProgramSessionId { get; set; }

        public MetaData MetaData { get; set; }

        public decimal? Amount { get; set; }

        public bool IsSameDayDropIn { get; set; }
    }
}
