﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programAddEditStep3Controller', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'formService', 'waiverService', 'jbToast', function ($scope, $http, $log, $stateParams, $state, programService, formService, waiverService, jbToast) {
        $scope.baseUrl = window.location.origin + "/Dashboard/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

     

        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;

        if ($state.returnByBack()) {

            $scope.Model = programService.getModel();
        }

        if (!$state.returnByBack()) {

            $http.get($scope.baseUrl + '/ProgramCreateEditStep3', { params: { programType: $stateParams.programType, seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId } }).success(function (data2, status, headers, config) {
                $scope.Model = data2;

                if ($scope.Model.LastCreatedPage.split("Step")[1] < 2) {
                    jbToast.warning("Please setup your program step by step.");
                    $state.go('ProgramAddEditStep' + (Number($scope.Model.LastCreatedPage.split("Step")[1]) + 1), { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                }
            }).error(function() {
                jbToast.error("Something went wrong!");
            });

        }

        $scope.waiverOptions = {
            placeholder: "Select waivers...",
            dataTextField: "Title",
            dataValueField: "Value",
            autoBind: true,
            dataSource: $scope.waiverDataSource,
        };

        $scope.saveStep3 = function () {

            programService.setModel($scope.Model);

            programService.saveStep3(function (data, status, headers, config) {
                if (data.Status == true) {
                    $state.go('ProgramAddEditStep4', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: data.Data })
                }
                else {
                    $scope.MC.handleErrors($scope, data);
                }
            });
        }

        $scope.goCustomizeForm = function () {

            programService.setModel($scope.Model);

            $state.forward('Form', {  seasonDomain: $scope.MC.stateParams.seasonDomain ,formType: "Registration", templateId: $scope.Model.FormTemplate}, 'ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId }
                , function () {

                    var formModel = formService.getModel();

                    $scope.Model.FormTemplate = formModel.FormTemplate;

                    $scope.Model.FormTemplates = formModel.FormTemplates;
                });
        }

        $scope.goWaivers = function () {

            programService.setModel($scope.Model);

            $state.forward('Waivers', { seasonDomain: $scope.MC.stateParams.seasonDomain }, 'ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId },
                function () {

                    var waiverModel = waiverService.getModel();

                    $scope.Model.AllWaivers = waiverModel.AllWaivers;
                });
        }

        $scope.goFollowUpForms = function () {

            programService.setModel($scope.Model);

            $state.forward('FollowUpForms', { programId: $scope.MC.stateParams.programId, seasonDomain: $scope.MC.stateParams.seasonDomain }, 'ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId },
                function () {

                    var formModel = formService.getModel();

                    $scope.Model.AllFollowUpForms = formModel.AllFollowUpForms;

                });
        }

        if ($state.returnByBack()) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();