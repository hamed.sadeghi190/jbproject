﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SelectCountyController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'select-county-view';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/CatalogReports", order: 2, menu: "PortalReports" });

            $scope.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.AllCounties = [];
            $scope.Model = {};
            var timer = false;

            $scope.$watch('AllCounties', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            $http.get('Dashboard/PortalReport/GetCounties').success(function (data, status, headers, config) {
                $scope.AllCounties = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data.Message);
            });

            var checkValidation = function (countylId) {
                if (is.undefined(countylId)) {
                    jbToast.error("Please select a county.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {
                if (checkValidation($scope.Model.County)) {
                    $state.go('CountyProviders', { county: $scope.Model.County });
                }
            }
        }
    ]);

})();