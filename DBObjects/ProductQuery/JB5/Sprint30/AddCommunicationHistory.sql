SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Contacts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[Category] [int] NOT NULL,
	[Note] [nvarchar](1024) NULL,
	[SenderId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Contacts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Contacts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Contacts_dbo.JbUsers_SenderId] FOREIGN KEY([SenderId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Contacts] CHECK CONSTRAINT [FK_dbo.Contacts_dbo.JbUsers_SenderId]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CommunicationHistories](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailId] [int] NULL,
	[ContactId] [int] NULL,
	[RelatedEntity] [nvarchar](128) NULL,
	[RelatedEntityId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.CommunicationHistories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CommunicationHistories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CommunicationHistories_dbo.Contacts_ContactId] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contacts] ([Id])
GO

ALTER TABLE [dbo].[CommunicationHistories] CHECK CONSTRAINT [FK_dbo.CommunicationHistories_dbo.Contacts_ContactId]
GO

ALTER TABLE [dbo].[CommunicationHistories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.CommunicationHistories_dbo.Emails_EmailId] FOREIGN KEY([EmailId])
REFERENCES [dbo].[Emails] ([Id])
GO

ALTER TABLE [dbo].[CommunicationHistories] CHECK CONSTRAINT [FK_dbo.CommunicationHistories_dbo.Emails_EmailId]
GO


