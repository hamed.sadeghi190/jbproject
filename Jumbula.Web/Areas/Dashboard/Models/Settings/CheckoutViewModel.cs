﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Helper;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using System.Web.Mvc;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CheckoutViewModel
    {
        public CheckoutViewModel()
        {
            RegistrationLogoUrlTypes = SeedRedirectUrlTypes();
            RegistrationLogoUrlSelectedType = "0";
            BrowseMoreUrlTypes = SeedRedirectUrlTypes();
            BrowseMoreUrlSelectedType = "0";
            LogOutUrlTypes = SeedRedirectUrlTypes();
            RegistrationLogoUrlSelectedType = "0";
            PlayerCanEditOrder = false;
            HasChessTourney = false;
            IsNewFamily = false;
            IsDiscountApplyOnOriginPrice = true;
            ConfirmationUrlTypes = SeedRedirectUrlTypes();
            RegisterationFeeLabel = "Application fee";
        }
        public bool IsDiscountApplyOnOriginPrice { get; set; }

        public bool HideProgramDates { get; set; }
        public bool HideProgramDatesInCart { get; set; }
        public bool HideCouponInCart { get; set; }
        public bool HideEnrollmentCapacityInClassPage { get; set; }
        public bool EnablePriceHidden { get; set; }
        public List<SelectKeyValue<long>> ProgramsList { get; set; }
        public bool HideAllProgramsPrice { get; set; }
        public List<long> HiddenPricePrograms { get; set; }
        public bool HasNotPartner { get; set; }
        public string SiteUrl { get; set; }
        public bool IsClubPartner { get; set; }
        public bool PlayerCanEditOrder { get; set; }
        public bool ShowCombinedPriceOnly { get; set; }
        public bool HasChessTourney { get; set; }
        public string RegistrationLogoUrlSelectedType { get; set; }
        public List<SelectKeyValue<string>> RegistrationLogoUrlTypes { get; set; }
        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Registration logo redirect url")]
        [Url]
        [DataType(DataType.Url)]
        public string RegistrationLogoUrl { get; set; }

        public string BrowseMoreUrlSelectedType { get; set; }
        public List<SelectKeyValue<string>> BrowseMoreUrlTypes { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Select classes for a new participant redirect url")]
        [Url]
        [DataType(DataType.Url)]
        public string BrowseMoreUrl { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Button text")]
        public string BrowseMoreText { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Button text")]
        public string AnotherRegisterButtonText { get; set; }
        public string LogOutUrlSelectedType { get; set; }
        public List<SelectKeyValue<string>> LogOutUrlTypes { get; set; }
        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Redirect url")]
        [Url]
        [DataType(DataType.Url)]
        public string ConfirmationUrl { get; set; }

        public string ConfirmationUrlSelectedType { get; set; }
        public List<SelectKeyValue<string>> ConfirmationUrlTypes { get; set; }
        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Log out redirect url")]
        [Url]
        [DataType(DataType.Url)]
        public string LogOutUrl { get; set; }
        public bool EnableDonation { get; set; }
        public bool EnableCartDonation { get; set; }
        public bool EnableConvenienceFee { get; set; }
        public bool EnableSurCharge { get; set; }

        [MaxLength(256, ErrorMessage = "Name cannot be longer than 256 characters.")]
        public string SurchargeLabel { get; set; }
        public string SurchargeMessage { get; set; }

        public decimal SurchargeAmount { get; set; }
        public int SurcahrgeAmountType { get; set; }
        public bool IsNewFamily { get; set; }
        public string NewFamilyNotification { get; set; }
        public ChargeApplyType ApplyType { get; set; }
        public bool DisableForZeroAmount { get; set; }
        public decimal ConvenienceFee { get; set; }
        public int ConvenienceFeeType { get; set; }
        public string ConvenienceFeeMessage { get; set; }

        [MaxLength(256, ErrorMessage = "Name cannot be longer than 256 characters.")]
        public string ConvenienceFeeLabel { get; set; }

        public int DonationForm { get; set; }

        [Required(ErrorMessage = "{0} is required")]
        [Display(Name = "Header")]
        public string DonationMessage { get; set; }

        [Display(Name = "Description")]
        [StringLength(2000, ErrorMessage = "{0} must be at last {1} characters.")]
        public string DonationDescription { get; set; }

        public bool EnableRegisterationFee { get; set; }

        public decimal RegisterationFee { get; set; }

        [MaxLength(256, ErrorMessage = "{0} cannot be longer than 256 characters.")]
        public string RegisterationFeeLabel { get; set; }

        public ChargeApplyType RegistrationFeeApplyType { get; set; }

        List<SelectKeyValue<string>> SeedRedirectUrlTypes()
        {
            var result = new List<SelectKeyValue<string>>();

            foreach (RedirectUrlTypes type in Enum.GetValues(typeof(RedirectUrlTypes)))
            {
                result.Add(new SelectKeyValue<string>() { Text = type.ToDescription(), Value = ((int)type).ToString() });
            }

            return result;
        }
    }

    public enum CheckOutSettingsSection
    {
        Redirect = 0,
        Additional = 1,
        OptionFamily = 2,
        Donation = 3,
        Charges = 4,
        DiscountCoupon = 5,
        ClassPage = 6,
        CartPage = 7,
        LoginPage = 8,
        Miscellaneous = 9,

    }


}