﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramCreateEditModelStep1 : BaseViewModel<Program, long>, IProgramWizardModel
    {
        public ProgramCreateEditModelStep1()
        {
            RegisterPINMessage = "Please enter your access password to register for this class. If the password does not work or you do not have one, contact the club directly.";
            ProgramImages = new List<string>();
        }

        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }
    
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Program name")]
        [StringLength(128, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Name { get; set; }

        [Display(Name = "Room assignment")]
        [StringLength(128, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Room { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Program description")]
        [StringLength(100000, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Description { get; set; }

      
        [Display(Name = "Testimonials")]
        [StringLength(4000, ErrorMessage = "{0} must be at last {1} characters.")]
        public string Testimonials { get; set; }

        public bool HasTestimonials { get; set; }
        
        public bool HasRegisterPIN { get; set; }
        [MaxLength(64)]
        public string RegisterPIN { get; set; }

        [MaxLength(1024)]
        public string RegisterPINMessage { get; set; }

        public bool DisableCapacityRestriction { get; set; }

        public ProgramStatus Status { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public bool IsWaitListAvailable { get; set; }

        public string WaitlistPolicy { get; set; }

        public ProgramTypeCategory TypeCategory { get; set; }

        public List<string> ProgramImages { get; set; }

        public List<string> SourceImages { get; set; }
        public bool HasCatalogId { get; set; }
        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }

        public ProgramPageStep PageStep { get; set; }

        public ProgramPageStep LastCreatedPage { get; set; }

        public List<SelectKeyValue<string>> AllCategories { get; set; }

        public List<string> SelectedCategories { get; set; }

        public List<SelectKeyValue<string>> AllLocations { get; set; }

        public int? InstructorId { get; set; }

        public List<int> SelectedInstructors { get; set; }

        public List<SelectKeyValue<int>> AllInstructors { get; set; }

        public string SelectedLocation { get; set; }

        public SaveType SaveType { get; set; }

        public bool FromTemplate { get; set; }
    }
}