﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('weeklyAgendaController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'catalog-view';//seasons-view programs
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/WeeklyAgenda", order: 0, menu: "WeeklyAgenda" });
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });
    }]);
})();

