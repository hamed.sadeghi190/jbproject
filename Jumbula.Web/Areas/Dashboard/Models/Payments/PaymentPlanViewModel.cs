﻿using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class PaymentPlanViewModel : BaseViewModel<PaymentPlan, long>
    {
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Deposit")]
        public decimal Deposit { get; set; }
        public ChargeDiscountType DepositType { get; set; }
        public bool IsAutoChargeMandatory { get; set; }

        public bool IsUseCustomPrompt { get; set; }
        public decimal? InstallmentFee { get; set; }
        public CalculationType CalculationType { get; set; } // = Domain.CalculationType.OnlyTuition;
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? ExpiryDate { get; set; }
        public Int16 NumOfInstallments { get; set; }

        public bool IsNameVisibleToUsers { get; set; }
        public List<SelectKeyValue<string>> ProgramsList { get; set; }

        public string StrNumOfInstallments
        {
            get
            {
                if (NumOfInstallments <= 1)
                {
                    return string.Format("{0} Installment", NumOfInstallments);
                }
                else
                {
                    return string.Format("{0} Installments", NumOfInstallments);
                }
            }
        }
        public string InstallmentDueDates { get; set; }
        public IEnumerable<string> ListOfInstallmentDates { get; set; }
        public List<DateTime> InstallmentDates { get; set; }
        public bool IsAllProgram { get; set; }
        public bool IsAllUser { get; set; }
        public long SeasonId { get; set; }
        public MetaData MetaData { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsSelected { get; set; }
        public string SeasonDomain { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Label")]
        public string CustomInstallmentLabel { get; set; }

        [Required(ErrorMessage = "Select at least one program.")]
        public List<string> SelectedPrograms { get; set; }

        [Required(ErrorMessage = "Select at least one user.")]
        public List<int> SelectedUser { get; set; }
        public SelectKeyValue<string> SelectedInstallments { get; set; }
        public List<SelectKeyValue<string>> InstallmentItems { get; set; }


        public PaymentPlanViewModel()
        {
            DepositType = ChargeDiscountType.Fixed;
            NumOfInstallments = 1;
            IsAllProgram = true;
            IsAllUser = true;
            IsSelected = false;
            SelectedUser = new List<int>();
           
        }

        private void SeedInstallmentCounts()
        {
            var installments = new List<SelectListItem>();
            for (int i = 2; i <= 10; i++)
            {
                installments.Add(new SelectListItem
                    {
                        Value = (i - 1).ToString(),
                        Text = i.ToString() + " Installments"
                    });
            }


        }

    }

}