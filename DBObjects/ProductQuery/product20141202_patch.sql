--@mn--2014/09/15--

If Object_ID('[dbo].[JbForms]') Is Null
BEGIN
CREATE TABLE [dbo].[JbForms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JsonElements] [nvarchar](max) NULL,
	[Title] [nvarchar](max) NULL,	
	[HelpText] [nvarchar](max) NULL,
	[RefEntityName] [nvarchar] (100) NULL,
	[RefEntityId] int NOT NULL,
	[CreatedDate] datetime NOT NULL default(getdate()),
	[LastModifiedDate] datetime NOT NULL)
END
GO

--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.JbForms')
BEGIN
	ALTER TABLE [dbo].[JbForms] ADD CONSTRAINT [PK_dbo.JbForms] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

--<< ADD COLUMNS >>--
IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='RefEntityName')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD RefEntityName nvarchar(100) NULL;

END
GO

IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='RefEntityId')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD RefEntityId int NOT NULL;
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='CreatedDate')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD CreatedDate datetime NULL default(getdate());
	EXEC('update [dbo].[JbForms] set CreatedDate=getdate()')
	ALTER TABLE [dbo].[JbForms]
	Alter Column CreatedDate datetime NOT NULL;
END
GO

IF NOT EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='LastModifiedDate')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	ADD LastModifiedDate datetime NULL;
	EXEC('update [dbo].[JbForms] set LastModifiedDate=getdate()')
	ALTER TABLE [dbo].[JbForms]
	Alter Column LastModifiedDate datetime NOT NULL;
END
GO

--<< Edit COLUMNS >>--
IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='RefEntityName')
BEGIN
	ALTER TABLE [dbo].[JbForms]
	Alter Column RefEntityName nvarchar(100) NULL;

END
GO


--<< Drop COLUMNS >>--
IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='Size')
BEGIN	
	EXEC [dbo].[JbDropColumn] @tablename='dbo.JbForms' ,@fieldname='Size';
END
GO

IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id=OBJECT_ID('dbo.JbForms') AND name='Name')
BEGIN
	EXEC [dbo].[JbDropColumn] @tablename='dbo.JbForms' ,@fieldname='Name';
END
GO






--Create time 2014/09/03 --- By MN----


If Object_ID('[dbo].[EventRoasters]') Is Null
BEGIN
CREATE TABLE [dbo].[EventRoasters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](1000) NULL,	
	[Events] [nvarchar](max) NOT NULL,
	[Club_Id] [int] NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[ClubDomain] [nvarchar](128) NOT NULL,
	[JbForm_Id] [int] NULL)

END
GO

--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.EventRoasters')
BEGIN
	ALTER TABLE [dbo].[EventRoasters] ADD CONSTRAINT [PK_dbo.EventRoasters] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

--<< FOREIGNKEYS DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'FK_dbo.EventRoasters_dbo.Clubs_Club_Id')
BEGIN
	ALTER TABLE [dbo].[EventRoasters]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EventRoasters_dbo.Clubs_Club_Id] FOREIGN KEY([Club_Id])
	REFERENCES [dbo].[Clubs] ([Id])
END
GO


--<< DROP COLUMNS >>--
--Delete Fields Column by MN----
IF  EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE OBJECT_ID=OBJECT_ID('dbo.EventRoasters') AND
				[name] = 'Fields')
BEGIN
	ALTER TABLE dbo.EventRoasters DROP COLUMN [Fields]
END
GO
IF  EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE OBJECT_ID=OBJECT_ID('dbo.EventRoasters') AND
				[name] = 'ReportTemplate')
BEGIN
	ALTER TABLE dbo.EventRoasters DROP COLUMN [ReportTemplate]
END
GO


---<< FOREIGN KEYS JBFROM>>--
declare @TableName nvarchar(50)='EventRoasters' --without dbo.
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'FK_'+@TableName+'_JbForms')
BEGIN
	declare @command nvarchar(500)='ALTER TABLE [dbo].['+@TableName+'] Add JbForm_Id int null';
	exec(@command);
	print 'JbForm_Id column created successfully ';

	set @command='ALTER TABLE [dbo].['+@TableName+']  WITH CHECK ADD  CONSTRAINT [FK_'+@TableName+'_JbForms] FOREIGN KEY([JbForm_Id])
	REFERENCES [dbo].[JbForms] ([Id])
	ALTER TABLE [dbo].['+@TableName+'] CHECK CONSTRAINT [FK_'+@TableName+'_JbForms]';
	exec(@command);
	print 'FK created successfully ';
	END
	ELSE
	BEGIN
		print 'Fk has already existed';
END


GO