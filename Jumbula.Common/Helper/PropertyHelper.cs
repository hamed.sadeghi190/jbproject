﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Jumbula.Common.Helper
{
    public static class PropertyHelper
    {
        public static string GetPropertyName<T, TProperty>(Expression<Func<T, TProperty>> property)
        {
            PropertyInfo propertyInfo = null;
            var body = property.Body;

            switch (body)
            {
                case MemberExpression _:
                    propertyInfo = (body as MemberExpression)?.Member as PropertyInfo;
                    break;
                case UnaryExpression _:
                    propertyInfo = ((MemberExpression)((UnaryExpression)body).Operand).Member as PropertyInfo;
                    break;
            }

            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");
            }

            var propertyName = propertyInfo.Name;

            return propertyName;
        }

        public static string GetPropertyName<T>(Expression<Func<T, object>> property)
        {
            PropertyInfo propertyInfo = null;
            var body = property.Body;

            if (body is MemberExpression memberExpression)
            {
                propertyInfo = memberExpression.Member as PropertyInfo;
            }
            else if (body is UnaryExpression expression)
            {
                propertyInfo = ((MemberExpression)expression.Operand).Member as PropertyInfo;
            }

            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");
            }

            var propertyName = propertyInfo.Name;

            return propertyName;
        }

        public static object GetPropertyValue(object obj, string propertyName)
        {
            return obj.GetType().GetProperty(propertyName).GetValue(obj, null);
        }
    }
}
