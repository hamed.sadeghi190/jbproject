﻿namespace Jumbula.Common.Constants
{
    public static class CartConstants
    {
        public const string DuplicateRegistrationInCart = "Duplicate registration in the cart";
        public const string AlreadyRegisteredInSeason = "Already registered in the season";
        public const string ConflictClassScheduleWithPreviousRegistration = "Conflicting class schedule with a previous registration";
        public const string ConflictClassSchedule = "Conflicting class schedule";
        public const string ConflictDropInInCart = "Conflicting drop-in in the cart";
        public const string ConflictDropInWithPreviousDropIn = "Conflicting drop-in with a previously registered drop-in";
        public const string ConflictProgramsInCart = "Conflicting programs in the cart";
        public const string ProgramConflictWithPreviousRegistration = "Program conflicts with a previous registration";
        public const string ConflictingClassSchedule = "Conflicting class schedule";
    }
}
