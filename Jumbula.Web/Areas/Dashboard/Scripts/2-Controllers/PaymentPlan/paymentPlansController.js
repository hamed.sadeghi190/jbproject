﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('paymentPlansController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'payment-plans-view';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/PaymentPlan";

        $scope.mainDiscountGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + '/GetAllPaymentPlans?seasonDomain=' + $scope.MC.stateParams.seasonDomain,
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainDiscountGridDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [{
                field: "Name",
                title: "Installment",
            }, {
                field: "InstallmentDueDates",
                title: "Due dates",
               width: "400px"

    }, {
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                    <li ui-sref='PaymentPlanManage({seasonDomain:MC.stateParams.seasonDomain,paymentPlanId: #=Id#})'>\
                                          <span class='k-link jbi-pencil'>Edit</span>\
                                    </li>\
                                    <li ng-click='DeletePaymentPlan( #=Id# )'>\
                                          <span class='k-link jbi-remove'>Delete</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>"
            }],
        };

        $scope.DeletePaymentPlan = function (Id) {
            jbConfirmation
                .yes(function () {
                    $http.post($scope.baseUrl + '/DeletePaymentPlan', { paymentPlanId: Id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $scope.mainGridOptions.dataSource.read();

                        }
                    });
                })
                .confirm();
        };



        $scope.paymentPlanSearch = function () {
            $scope.mainGridOptions.dataSource.read();
        }
    }]);
})();