﻿
using Jb.Framework.Common;
using System;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class PaypalIPNBusiness : IPaypalIPNBusiness
    {
        private readonly IRepository<PaypalIPN, long> _paypalPinRepository;

        public PaypalIPNBusiness(IRepository<PaypalIPN, long> paypalPinRepository)
        {
            _paypalPinRepository = paypalPinRepository;
        }

        public OperationStatus Create(PaypalIPN paypalIPN)
        {
            return _paypalPinRepository.Create(paypalIPN);
        }

        public IQueryable<PaypalIPN> GetValidUnProceedIPNs()
        {
            var date = DateTime.UtcNow/*.AddMinutes(-5)*/;
            return _paypalPinRepository.GetList().Where(p => !p.IsProceed && p.IsValid && p.CreateDate.CompareTo(date) <= 0);
        }
        public bool CheckTransactionExist(string transactionId)
        {
            return _paypalPinRepository.GetList().Any(p => p.TransactionId == transactionId);
        }

        public OperationStatus Update(PaypalIPN paypalIpn)
        {
            return _paypalPinRepository.Update(paypalIpn);
        }
    }
}
