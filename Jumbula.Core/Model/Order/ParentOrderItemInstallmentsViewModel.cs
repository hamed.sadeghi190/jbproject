﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using System;
using PaymentConstants = Jumbula.Common.Constants.Payment;

namespace Jumbula.Core.Model.Order
{
    public class ParentOrderItemInstallmentsViewModel : BaseViewModel<OrderInstallment, long>
    {
        public DateTime DueDate { get; set; }
        public bool IsPastDue { get; set; }
        public string SchedulePart { get; set; }
        public ProgramTypeCategory ProgramType { get; set; }
        public string StrPaidDate
        {
            get
            {

                if (PayerType == RoleCategoryType.Parent)
                {
                    return PaidDate != null ? DateTimeHelper.FormatShortDateSeperetedByCamma(PaidDate.Value) : "-";
                }

                return PaidDate != null ? DateTimeHelper.FormatDate(PaidDate.Value) : "";

            }
        }

        public DateTime? PaidDate { get; set; }
        public bool IsDeleted { get; set; }
        public RoleCategoryType PayerType { get; set; }
        public InstallmentType Type { get; set; }
        public string Category { get; set; }
        public string InstallmentStatus
        {
            get
            {
                if (IsDeleted)
                    return (PaidAmount ?? 0) > 0 ? PaymentConstants.OverPaidInstallmentStatus : string.Empty;

                if (DueDate > DateTime.Now)
                {
                    if (Status == OrderStatusCategories.error)
                    {
                        return Balance != 0 ? PaymentConstants.FailInstallmentStatus : string.Empty;
                    }


                    if (Balance == Amount)
                    {
                        return string.Empty;
                    }

                    if (Balance == 0)
                    {
                        return PaymentConstants.PaidInstallmentStatus;
                    }

                    if (Balance > 0 && Balance < Amount)
                    {
                        return PaymentConstants.PartiallyPaidInstallmentStatus;
                    }

                    if (PaidAmount > Amount)
                    {
                        return PaymentConstants.OverPaidInstallmentStatus;
                    }
                }

                switch (Status)
                {
                    case OrderStatusCategories.error when Balance > 0:
                        return PaymentConstants.FailInstallmentStatus;

                    case OrderStatusCategories.error when Balance == 0:
                        return PaymentConstants.PaidInstallmentStatus;

                    case OrderStatusCategories.completed when (Balance > 0 && DueDate.Date >= DateTime.UtcNow.Date):
                        return PaymentConstants.PartiallyPaidInstallmentStatus;

                    case OrderStatusCategories.completed when (Balance > 0 && DueDate.Date < DateTime.UtcNow.Date):
                        return PaymentConstants.PastDueInstallmentStatus;

                    case OrderStatusCategories.completed when Balance == 0:
                        return PaymentConstants.PaidInstallmentStatus;

                    default:
                        {
                            if (Balance < 0)
                                return PaymentConstants.OverPaidInstallmentStatus;

                            if (Status != OrderStatusCategories.error && Status != OrderStatusCategories.canceled &&
                                DueDate.Date < DateTime.UtcNow.Date && Balance > 0)
                            {
                                return PaymentConstants.PastDueInstallmentStatus;
                            }

                            if (Status == OrderStatusCategories.canceled)
                            {
                                if (Amount == Balance)
                                    return PaymentConstants.PaidInstallmentStatus;

                                return Amount > Balance
                                    ? PaymentConstants.PartiallyPaidInstallmentStatus
                                    : PaymentConstants.UnpaidInstallmentStatus;
                            }

                            if ((Status == OrderStatusCategories.initiated || Status == OrderStatusCategories.completed) && Amount == 0 && PaidAmount == 0)
                            {
                                return "";
                            }

                            return PaymentConstants.UnpaidInstallmentStatus;
                        }
                }
            }
        }


        public string Schedule
        {
            get
            {
                if (ProgramType == ProgramTypeCategory.Subscription)
                {
                    //return Schedule.StartDate.ToString("MM/dd/yyyy") + " - " + Schedule.EndDate.ToString("MM/dd/yyyy");
                    return SchedulePart;
                }
                else if (ProgramType == ProgramTypeCategory.BeforeAfterCare)
                {
                    return SchedulePart;
                }
                else
                {
                    return null;
                }

            }
        }

        //public string InstallmentDueDate { get; set; }
        public string InstallmentDueDate
        {
            get
            {

                if (PayerType == RoleCategoryType.Parent)
                {
                    return DateTimeHelper.FormatShortDateSeperetedByCamma(DueDate);
                }

                if (IsDeposit && Type == InstallmentType.Noraml)
                {
                    return
                        $"{DateTimeHelper.FormatDate(DueDate)} {(ProgramType != ProgramTypeCategory.Subscription && ProgramType != ProgramTypeCategory.BeforeAfterCare ? "(Deposit)" : "")}";
                }

                return DateTimeHelper.FormatDate(DueDate);

            }
        }
        public bool IsDeposit { get; set; }

        public decimal Amount { get; set; }
        public decimal? PaidAmount { get; set; }

        public string StrPaidAmount => PaidAmount != null ? CurrencyHelper.FormatCurrencyWithPenny(PaidAmount, Currency) : CurrencyHelper.FormatCurrencyWithPenny(0, Currency);

        public decimal Balance => !IsDeleted ? Amount - (PaidAmount ?? 0) : PaidAmount * (-1) ?? 0;

        public string StrBalance => CurrencyHelper.FormatCurrencyWithPenny(Balance, Currency, true);
        public CurrencyCodes Currency { get; set; }
        public string StrAmount => CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency);

        public long? OrderItemId { get; set; }

        public JumbulaSubSystem ActionColler { get; set; }
        public OrderStatusCategories Status { get; set; }

        public decimal AmountAutomaticallyTransactions { get; set; }
        public decimal RefundAmount { get; set; }
        public decimal TakePaymentAmount { get; set; }
        public bool IsSelected { get; set; }

    }
}
