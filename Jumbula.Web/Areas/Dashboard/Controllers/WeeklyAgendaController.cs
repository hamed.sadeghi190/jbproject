﻿using System;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class WeeklyAgendaController : DashboardBaseController
    {
        // GET: Dashboard/WeeklyAgenda
        [JbAuthorize(JbAction.WeeklyAgenda_Settings_View)]
        public virtual ActionResult Settings()
        {
            return View();
        }

        [JbAuthorize(JbAction.WeeklyAgenda_View)]
        public virtual ActionResult Manage()
        {
            return View();
        }

        [JbAuthorize(JbAction.WeeklyAgenda_Settings_View)]
        [HttpGet]
        public virtual ActionResult GetSttings()
        {
            var model = new WeeklyAgendaSettingsViewModel();

            var agendaSettings = Ioc.ClubBusiness.GetAgendaSettings(ActiveClub.Id);

            if (agendaSettings == null)
            {
                model.IsExactDay = true;
                model.ExactDateTime = DateTime.Now.Date.AddHours(18);
                model.Subject = "Engaging enrichment talking points";
                model.Wrapper = "Here is some helpful information about your child's activity today at school to make for fun dinner discussion.";
            }

            model.ExactDateTime = agendaSettings.ExactTime.HasValue ? agendaSettings.ExactTime : DateTime.Now.Date.AddHours(18);
            model.HourAfterClassEnds = agendaSettings.HourAfterClassEnds == 0 ? 1 : agendaSettings.HourAfterClassEnds;
            model.IsExactDay = agendaSettings.IsExactDay;
            model.Subject = agendaSettings.Subject != null ? agendaSettings.Subject : "Engaging enrichment talking points";
            model.Wrapper = agendaSettings.Wrapper != null ? agendaSettings.Wrapper : "Here is some helpful information about your child's activity today at school to make for fun dinner discussion."; ;
            model.IsEnabled = agendaSettings.IsEnabled;

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.WeeklyAgenda_Settings_Save)]
        [HttpPost]
        public virtual ActionResult SaveSttings(WeeklyAgendaSettingsViewModel model)
        {
            var result = new OperationStatus();

            CheckSettingsValidation(model);

            if (ModelState.IsValid)
            {
                var agendaSettings = Ioc.ClubBusiness.GetAgendaSettings(ActiveClub.Id);

                agendaSettings.ExactTime = model.ExactDateTime;
                agendaSettings.HourAfterClassEnds = model.HourAfterClassEnds;
                agendaSettings.IsExactDay = model.IsExactDay;
                agendaSettings.Subject = model.Subject != null ? model.Subject : string.Empty;
                agendaSettings.Wrapper = model.Wrapper != null ? model.Wrapper : string.Empty;
                agendaSettings.IsEnabled = model.IsEnabled;

                result = Ioc.ClubBusiness.SaveAgendaSettings(ActiveClub.Id, agendaSettings);

                return Json(new JResult { Status = result.Status, RecordsAffected = result.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        private void CheckSettingsValidation(WeeklyAgendaSettingsViewModel model)
        {
            if(!model.IsEnabled)
            {
                ModelState.Remove("model.Subject");
                ModelState.Remove("model.ExactDateTime");
            }
        }

        [JbAuthorize(JbAction.WeeklyAgenda_View)]
        public virtual ActionResult GetManage()
        {
            var club = this.ActiveClub;

            var model = new ManageWeeklyAgendaViewModel
            {
                Date = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow).Date
            };

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.WeeklyAgenda_View)]
        public virtual ActionResult GetDatePrograms(PaginationModel paginationModel, DateTime? date)
        {
            var club = this.ActiveClub;

            var programSessionBusiness = Ioc.ProgramSessionBusiness;

            if (!date.HasValue)
            {
                date = Ioc.ClubBusiness.GetClubDateTime(club.Id, DateTime.UtcNow);
            }

            var sessionsOfDay = Ioc.ProgramSessionBusiness.GetList(date.Value).Where(p => p.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Class && p.ProgramSchedule.Program.ClubId == club.Id);

            var sessionsCount = sessionsOfDay.Count();

            var result = sessionsOfDay.Page(paginationModel)
                                        .Select(s =>
                                        new
                                        {
                                            Id = s.Id,
                                            ProgramTime = DateTimeHelper.FormatTimeRange(s.StartDateTime.TimeOfDay, s.EndDateTime.TimeOfDay),
                                            ProgramName = s.ProgramSchedulePart.ProgramSchedule.Program.Name,
                                            Agenda = s.WeeklyAgenda,
                                            Status = GetAgendaStatus(s),
                                            ShowActions = programSessionBusiness.IsStatusChangeAllowed(s)
                                        })
                                        .ToList();

            return JsonNet(new { DataSource = result, TotalCount = sessionsCount });
        }



        private string GetAgendaStatus(ProgramSession programSession)
        {
            var result = string.Empty;

            if (programSession.Status == ProgramSessionAgendaSendStatus.Procecced)
            {
                return ProgramSessionAgendaSendStatus.Procecced.ToString();
            }
            else if (programSession.Status == ProgramSessionAgendaSendStatus.Stopped)
            {
                return ProgramSessionAgendaSendStatus.Stopped.ToString();
            }
            else if (string.IsNullOrEmpty(programSession.WeeklyAgenda))
            {
                return "Null";
            }

            return result;
        }

        public ActionResult GetAgenda(int sessionId)
        {
            var session = Ioc.ProgramSessionBusiness.Get(sessionId);

            return JsonNet(new { Id = session.Id, Text = session.WeeklyAgenda });
        }

        [HttpPost]
        public ActionResult EditAgenda(int sessionId, string text)
        {
            var result = Ioc.ProgramSessionBusiness.EditAgenda(sessionId, text);

            return Json(new JResult(result));
        }

        [HttpPost]
        public ActionResult StopSendAgenda(int sessionId)
        {
            var session = Ioc.ProgramSessionBusiness.Get(sessionId);

            var result = Ioc.ProgramSessionBusiness.StopSendAgenda(sessionId);

            return Json(new JResult { Status = result.Status, RecordsAffected = result.RecordsAffected });
        }

        [HttpPost]
        public ActionResult ScheduleSendAgenda(int sessionId)
        {
            var session = Ioc.ProgramSessionBusiness.Get(sessionId);

            var result = Ioc.ProgramSessionBusiness.ScheduleSendAgenda(sessionId);

            return Json(new JResult { Status = result.Status, RecordsAffected = result.RecordsAffected });
        }

        [HttpPost]
        public ActionResult StopAll(DateTime date)
        {
            var clubId = this.ActiveClub.Id;
            var result = Ioc.ProgramSessionBusiness.StopSendAgenda(date, clubId);

            return Json(new JResult(result));
        }

        [HttpPost]
        public ActionResult SchedsuleAll(DateTime date)
        {
            var clubId = this.ActiveClub.Id;
            var result = Ioc.ProgramSessionBusiness.ScheduleSendAgenda(date, clubId);

            return Json(new JResult(result));
        }
    }
}