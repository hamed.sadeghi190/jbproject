﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class PaypalTrackingConfig : EntityTypeConfiguration<PaypalTracking>
    {
        public PaypalTrackingConfig()
        {
            HasKey(c => c.Id);

        }
    }
}
