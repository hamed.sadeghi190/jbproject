﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Form;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Business
{
    public class FollowupFormBusiness : IFollowupFormBusiness
    {
        private readonly IOrderBusiness _orderBusiness;
        private readonly IJbFormBusiness _jbFormBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IRepository<OrderItemFollowupForm> _orderItemFollowupFormRepository;
        private readonly IRepository<ClubFormTemplate> _clubFormTemplateRepository;
        private readonly IEntitiesContext _dataContext;
        private readonly IProgramBusiness _programBusiness;

        public FollowupFormBusiness(IOrderBusiness orderBusiness, IJbFormBusiness jbFormBusiness, IRepository<OrderItemFollowupForm> orderItemFollowupFormRepository, IEntitiesContext dataContext, IRepository<ClubFormTemplate> clubFormTemplateRepository, IClubBusiness clubBusiness, IProgramBusiness programBusiness)
        {
            _orderBusiness = orderBusiness;
            _jbFormBusiness = jbFormBusiness;
            _orderItemFollowupFormRepository = orderItemFollowupFormRepository;
            _dataContext = dataContext;
            _clubFormTemplateRepository = clubFormTemplateRepository;
            _clubBusiness = clubBusiness;
            _programBusiness = programBusiness;
        }

        public OrderItemFollowupForm Get(int formId, string guid)
        {
            return _orderItemFollowupFormRepository.Get(f => f.Id == formId && f.Guid == guid);
        }

        public OrderItemFollowupForm Get(int formId)
        {
            return _orderItemFollowupFormRepository.Get(form => form.Id == formId);
        }

        public SupplementalFormDetailViewModel GetSupplementalForm(int formId)
        {
            var form = _clubFormTemplateRepository.Get(formId);

            var result = MapSupplementalFormViewModel(form);

            return result;
        }
        public bool HasFollowupForm(Order order)
        {
            var hasNotFillFollowUpForm = order.RegularOrderItems.SelectMany(i => i.FollowupForms).Any(f => f.Status != FollowupStatus.Completed);

            if (order.RegularOrderItems.Any(m => hasNotFillFollowUpForm && m.FollowupForms != null && m.FollowupForms.Count > 0))
            {
                return true;
            }
            return false;
        }

        public OperationStatus OrderFollowUpForms(Order order)
        {
            foreach (var orderItem in order.RegularOrderItems.Where(i => i.ProgramScheduleId.HasValue))
            {
                if (orderItem.ProgramSchedule.Program.ClubFormTemplates.Any(form => !form.IsDeleted && form.FormType != FormType.Registration) && (!order.RegularOrderItems.Any(m => m.FollowupForms.Any(z => z.ISTS == orderItem.ISTS)) || orderItem.IsMultiRegister))
                {
                    foreach (var form in orderItem.ProgramSchedule.Program.ClubFormTemplates.Where(form => !form.IsDeleted && form.FormType != FormType.Registration).Select(f => f).ToList())
                    {
                        if (!orderItem.FollowupForms.Any(o => o.FormName.Equals(form.Title)))
                            orderItem.FollowupForms.Add(CreateOrderItemFollowUpForm(form, orderItem));
                    }
                }
            }

            return _orderBusiness.Update(order);
        }

        public OrderItemFollowupForm CreateOrderItemFollowUpForm(ClubFormTemplate form, OrderItem orderItem)
        {
            var formEmptyJbForm = new JbForm();
            formEmptyJbForm.CreatedDate = DateTime.UtcNow;
            formEmptyJbForm.JsonElements = form.JbForm.JsonElements;
            formEmptyJbForm.LastModifiedDate = DateTime.UtcNow;
            formEmptyJbForm.Id = 0;
            formEmptyJbForm.RefEntityName = typeof(OrderItemFollowupForm).Name;

            _jbFormBusiness.CreateEdit(formEmptyJbForm);

            var newfollowupForm = new OrderItemFollowupForm
            {
                FormType = form.FormType,
                Status = FollowupStatus.NotFill,
                Guid = Guid.NewGuid().ToString(),
                JbForm = formEmptyJbForm,
                JbFormId = formEmptyJbForm.Id,
                ISTS = orderItem.ISTS,
                FormName = form.Title
                //Link = string.Format("/FollowupForm/CompleteForm")
            };

            return newfollowupForm;
        }

        public OrderItemFollowupForm CreateOrderItemMultiRegisterFollowUpForm(JbForm form, ClubFormTemplate clubFormTemplate, OrderItem orderItem, int formId)
        {
            var formEmptyJbForm = new JbForm();
            formEmptyJbForm.CreatedDate = DateTime.UtcNow;
            formEmptyJbForm.JsonElements = form.JsonElements;
            formEmptyJbForm.LastModifiedDate = DateTime.UtcNow;
            formEmptyJbForm.ParentId = formId;
            formEmptyJbForm.RefEntityName = typeof(OrderItemFollowupForm).Name;

            var newfollowupForm = new OrderItemFollowupForm
            {
                FormType = clubFormTemplate.FormType,
                Status = FollowupStatus.Completed,
                Guid = Guid.NewGuid().ToString(),
                JbForm = formEmptyJbForm,
                ISTS = orderItem.ISTS,
                FormName = clubFormTemplate.Title,
                FollowUpFormMode = clubFormTemplate.FollowUpFormMode
            };

            return newfollowupForm;
        }
        public IQueryable<OrderItemFollowupForm> GetOrderItemFollowupForms(long orderIsts, OrderItemStatusReasons itemStatusReasons, long orderItemId = 0)
        {
            if (orderItemId != 0)
            {
                if (itemStatusReasons == OrderItemStatusReasons.transferIn || itemStatusReasons == OrderItemStatusReasons.transferOut)
                {
                    return _dataContext.Set<OrderItemFollowupForm>().Where(form => form.ISTS == orderIsts && form.OrderItem.Any(oi => oi.Id == orderItemId));
                }
                else
                {
                    return _dataContext.Set<OrderItemFollowupForm>().Where(form => form.ISTS == orderIsts && form.OrderItem.Any(oi => oi.Id == orderItemId));
                }
            }
            else
            {
                return _dataContext.Set<OrderItemFollowupForm>().Where(form => form.ISTS == orderIsts);
            }
        }

        public OperationStatus Update(OrderItemFollowupForm form)
        {
            return _orderItemFollowupFormRepository.Update(form);
        }

        private SupplementalFormDetailViewModel MapSupplementalFormViewModel(ClubFormTemplate formTemplate)
        {
            var urlList = new List<string>();

            foreach (var item in formTemplate.JbForm.Elements)
            {
                var itemElements = ((JbSection)item).Elements.Where(e => e.TypeTitle == Constants.Path_SupplementalType);

                foreach (var element in itemElements)
                {
                    var value = element.GetValue().ToString();
                    urlList.Add(value);
                }

            }

            return new SupplementalFormDetailViewModel()
            {
                Name = formTemplate.Title,
                DateCreated = _clubBusiness.GetClubDateTime(formTemplate.ClubId, formTemplate.DateCreated).ToString(Constants.DateTime_Comma),
                FormUrls = urlList
            };
        }

        public OperationStatus ChangeFollowUpFormMode(int formId)
        {
            var fromTemplate = _dataContext.Set<ClubFormTemplate>().SingleOrDefault(c => c.Id == formId);

            if (fromTemplate != null && fromTemplate.FollowUpFormMode == FollowUpFormMode.Offline)
            {
                fromTemplate.FollowUpFormMode = FollowUpFormMode.Online;
            }
            else
            {
                if (fromTemplate != null) fromTemplate.FollowUpFormMode = FollowUpFormMode.Offline;
            }

            return _clubFormTemplateRepository.Save();
        }

        public bool AssignedTemplateToPrograms(int id, int clubId)
        {
            var result = false;
            var clubTemplats = _programBusiness.GetList().Where(p => p.ClubId == clubId).SelectMany(c => c.ClubFormTemplates).Where(f => !f.IsDeleted).AsQueryable();

            if (clubTemplats.Select(t => t.Id).Contains(id))
            {
                result = true;
            }

            return result;

        }

        public IQueryable<ClubFormTemplate> GetProgramClubFormTemplateFromOrderItem(List<OrderItem> orderItems)
        {
            IQueryable<ClubFormTemplate> followUpFormsPrograms;

            var selectedPrograms = orderItems.Select(o => o.ProgramSchedule.Program).AsQueryable();

            followUpFormsPrograms = selectedPrograms.SelectMany(p => p.ClubFormTemplates).Where(f => f.FormType == FormType.FollowUp && !f.IsDeleted);

            return followUpFormsPrograms;
        }

        public string GetFormProgramsName(long clubFormTemplateId, List<long> programIds)
        {
            var programsName = new List<string>();
            string result = string.Empty;

            foreach (var id in programIds)
            {
                var program = _programBusiness.Get(id);

                var form = program.ClubFormTemplates.FirstOrDefault(f => f.Id == clubFormTemplateId && !f.IsDeleted);

                if (form != null)
                {
                    programsName.Add(program.Name);
                }
            }

            if (programsName.Any())
            {
                return string.Join(", ", programsName);
            }

            return result;
        }

        public string GetNamesOfProgramsThatAssignedTemplate(int clubFormTemplateId, int clubId)
        {
            var programsName = new List<string>();
            var result = string.Empty;

            var clubPrograms = _programBusiness.GetList().Where(p => p.ClubId == clubId && p.Status != ProgramStatus.Deleted);

            var programs = clubPrograms.Where(p => p.ClubFormTemplates.Select(c => c.Id).Contains(clubFormTemplateId));

            foreach (var program in programs)
            {
                programsName.Add($"{"<br/>"}{program.Name} ({program.Season.Title})");
            }

            if (programsName.Any())
            {
                return string.Join(", ", programsName);
            }

            return result;
        }
    }
}
