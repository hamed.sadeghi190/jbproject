﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MailChimp.Ecomm;


namespace SportsClub.Models
{
    public class ParentInvoiceViewModel
    {
        public ParentInvoiceInfoViewModel InfoModel { get; set; }
        public ParentInvoiceItemViewModel InvoiceItemModel { get; set; }
        public bool? Status { get; set; }
    }
}