﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;

namespace SportsClub.Models.Email
{
    public class ConfirmationEmail
    {
        public string HomeTitle { get; set; }

        public string HomePageUrl { get; set; }

        public string ClubName { get; set; }
        public string ClubNamePartner { get; set; }
        
        public RoleCategory Role { get; set; }

        public string UserFullName { get; set; }

        public string LogoUrl { get; set; }

        public string VerificationLink { get; set; }

        public RequestType RequestType { get; set; }

        public string Message { get; set; }

        public string EmailStaff { get; set; }

        public bool InvaiteFromPartner { get; set; }

        public bool IsEmailAlreadyActived { get; set; }

        public DateTime ExpirationDate { get; set; }

        public bool ShowPlayStoreIconForStaff { get; set; }
    }
}