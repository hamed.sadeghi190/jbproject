﻿using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FlyerClassCatalogViewModel : BaseFlyerTemplateViewModel
    {
        public FlyerClassCatalogViewModel()
            : base()
        {
            Body = new FlyerClassCatalogBodyViewModel();
        }
        public new FlyerClassCatalogBodyViewModel Body { get; set; }
    }

    public class FlyerClassCatalogBodyViewModel : BaseFlyerBodyViewModel
    {
        public FlyerClassCatalogBodyViewModel()
        {
            Programs = new List<FlyerProgramItemDetailViewModel>();
        }
        public List<FlyerProgramItemDetailViewModel> Programs { get; set; }

        public string MinMaxTimeAM { get; set; }
        public string MinMaxTimePM { get; set; }
        public int CountPMAndAM { get; set; }
        public string RootSiteUrl { get; set; }
        public string ClubUrl { get; set; }
    }
}