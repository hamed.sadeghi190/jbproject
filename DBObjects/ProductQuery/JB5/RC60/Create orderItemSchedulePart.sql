
CREATE TABLE [dbo].[OrderItemScheduleParts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderItemId] [bigint] NOT NULL,
	[ProgramPartId] [int] NULL,
	[PartAmount] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_dbo.OrderItemScheduleParts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OrderItemScheduleParts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderItemScheduleParts_dbo.OrderItems_OrderItemId] FOREIGN KEY([OrderItemId])
REFERENCES [dbo].[OrderItems] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OrderItemScheduleParts] CHECK CONSTRAINT [FK_dbo.OrderItemScheduleParts_dbo.OrderItems_OrderItemId]
GO

ALTER TABLE [dbo].[OrderItemScheduleParts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.OrderItemScheduleParts_dbo.ProgramScheduleParts_ProgramPartId] FOREIGN KEY([ProgramPartId])
REFERENCES [dbo].[ProgramScheduleParts] ([Id])
GO

ALTER TABLE [dbo].[OrderItemScheduleParts] CHECK CONSTRAINT [FK_dbo.OrderItemScheduleParts_dbo.ProgramScheduleParts_ProgramPartId]
GO


