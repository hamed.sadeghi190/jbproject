﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Areas.Dashboard.Models.Invoice;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class InvoiceController : DashboardBaseController
    {
        private readonly IProgramBusiness _programBusiness;
        private readonly IInvoiceBusiness _invoiceBusiness;
        public InvoiceController(IProgramBusiness programBusiness, IInvoiceBusiness invoiceBusiness)
        {
            _programBusiness = programBusiness;
            _invoiceBusiness = invoiceBusiness;
        }

        public virtual ActionResult ManageInvoices()
        {
            return View();
        }
        public virtual ActionResult InvoiceDetail()
        {
            return View();
        }
        public virtual ActionResult InvoiceTakePayment()
        {
            return View();
        }
        public virtual ActionResult ConfirmTakePaymentInvoice()
        {
            return View();
        }
        public virtual ActionResult CancelAndRemainderInvoice()
        {
            return View();
        }

        [HttpGet]
        public virtual JsonNetResult InvoicePage()
        {

            var model = new FilterClubInvoicesViewModel { InvoiceTypes = DropdownHelpers.ToSelectList<InvoiceType>() };
            return JsonNet(model);
        }
        [HttpPost]
        public virtual JsonNetResult GetAllInvoices(PaginationModel paginationModel, int? term, string typeId,
          string emailTerm, InvoiceType invoiceType = InvoiceType.All)
        {
            try
            {
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                var invoices = _invoiceBusiness.GetInvoiceByClubId(ActiveClub.Id).ToList();

                switch (invoiceType)
                {
                    case InvoiceType.All:
                        break;
                    case InvoiceType.Paid:
                        {
                            invoices = invoices.Where(i => i.Status == InvoiceStatus.Paid).ToList();
                        }
                        break;
                    case InvoiceType.Unpaid:
                        {
                            invoices = invoices.Where(i => i.Status == InvoiceStatus.Unpaid).ToList();
                        }
                        break;
                    case InvoiceType.Canceled:
                        {
                            invoices = invoices.Where(i => i.Status == InvoiceStatus.Canceled).ToList();
                        }
                        break;
                    case InvoiceType.PastDue:
                        {
                            var pastDueInvoices = new List<Invoice>();

                            var today = DateTime.UtcNow;
                            var clubTime = Ioc.ClubBusiness.GetClubDateTime(ActiveClub.Id, today);

                            foreach (var invoice in invoices.Where(i => i.PaidAmount == 0))
                            {

                                var date = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), invoice.InvoicingDate);
                                var dueValue = invoice.DueDate;
                                if (dueValue != null)
                                {
                                    var dueDate = date.AddDays(dueValue.Value);

                                    if (clubTime > dueDate)
                                    {
                                        pastDueInvoices.Add(invoice);
                                    }
                                }
                            }

                            invoices = pastDueInvoices;
                        }
                        break;
                    default:
                        break;
                }
                if (term > 0)
                {
                    invoices = invoices.Where(invoice => invoice.InvoiceNumber == term).ToList();
                }
                if (!string.IsNullOrEmpty(emailTerm))
                {
                    invoices = invoices.Where(invoice => invoice.User.UserName == emailTerm.ToLower()).ToList();
                }

                var model = invoices.OrderByDescending(c => c.Id).Skip((paginationModel.Page - 1) * paginationModel.PageSize).Take(paginationModel.PageSize).ToList().Select(c => new AllInvoicesClubViewModel()
                {
                    Date = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), c.InvoicingDate),
                    InvoiceId = c.InvoiceNumber,
                    Amount = c.Amount,
                    Status = c.Status.ToString(),
                    Recipient = c.User.UserName.ToLower(),
                    DueDateValue = c.DueDate,
                    Id = c.Id,
                    ClubId = c.ClubId,
                    StrAmount = CurrencyHelper.FormatCurrencyWithPenny(c.Amount, club.Currency),
                });


                var datasource = model.ToList();
                return JsonNet(new { data = datasource, total = invoices.Count() },
                    new Newtonsoft.Json.JsonSerializerSettings(), "");

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return JsonNet(null, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }
        [HttpGet]
        public virtual JsonNetResult GetInvoiceDetail(int invoiceId)
        {
            var invoiceHistories = Ioc.InvoiceHistoryBusiness.GetList().Where(h => h.InvoiceId == invoiceId);


            var historyModel = new List<InvoiceHistoryViewModel>();
            foreach (var history in invoiceHistories)
            {
                historyModel.Add(new InvoiceHistoryViewModel()
                {
                    Date = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), history.ActionDate),
                    Description = history.Description

                });

            }
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var clubId = club.Id;
            var clubLogo = "";
            var partner = club.PartnerClub;
            bool hasPartner = partner != null ? true : false;
            var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);
            if (clubInfo.logo)
            {
                clubLogo = UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo);
            }
            else
            {
                clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
            }

            var invoice = _invoiceBusiness.Get(invoiceId);
            CheckResourceForAccess(invoice);

            var paymantDetailId = Ioc.PaymentDetailBusiness.GetByInvoiceId(invoiceId).Id;
            var transactions = Ioc.TransactionActivityBusiness.GetList().Where(p => p.PaymentDetailId == paymantDetailId);
            var totalAmount = transactions.Sum(o => (decimal?)o.Amount) ?? 0;

            var userId = invoice.UserId;
            var userName = Ioc.UserProfileBusiness.Get(userId).UserName;
            var dueDateValue = invoice.DueDate;
            var invoiceDate = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), invoice.InvoicingDate);
            DateTime duedate;
            if (dueDateValue > 0)
            {
                duedate = invoiceDate.AddDays(dueDateValue.Value);
            }
            else
            {
                duedate = invoiceDate;
            }
            //fill invoice details
            var infoModel = new InvoiceHistoryViewModel()
            {
                Currency = club.Currency,
                InvoiceNumber = invoice.InvoiceNumber,
                Refrense = invoice.Reference,
                EmailCc = invoice.EmailToCc,
                DueDate = duedate,
                UserEmail = userName,
                ClubSite = clubInfo.Site,
                ContactClubEmail = clubInfo.Email,
                ClubAddress = clubInfo.Address,
                ClubUrlLogo = clubLogo,
                ClubName = clubInfo.Name,
                NoteToRecipient = invoice.NotToRecipient,
                Memo = invoice.Memo,
                StatusInvoice = invoice.Status.ToString(),
                TotalAmount = totalAmount,
                AmountPaid = invoice.PaidAmount > 0 ? -(invoice.PaidAmount) : invoice.PaidAmount,
                AmountDue = invoice.Amount - invoice.PaidAmount,
                MemberName = club.Name,
                HasPartner = hasPartner
            };
            decimal orderbalance;
            decimal orderItemInstallmentBalanse;
            decimal orderInvoiceAmount = 0;

            var invoiceItemModel = new InvoiceItemViewModel();
            invoiceItemModel.Orders = new List<InvoiceOrderViewModel>();
            invoiceItemModel.OrderItems = new List<InvoiceOrderItemViewModel>();
            invoiceItemModel.Instalments = new List<InvoiceInstallmentViewModel>();

            var ordersGroupBy = transactions.Select(o => o.Order).GroupBy(i => i.Id).ToList();


            var participantName = Ioc.PlayerProfileBusiness.GetList().Where(p => p.UserId == userId).First().Contact.FullName;
            var orderItems = Ioc.OrderItemBusiness;
            var allInstallments = Ioc.InstallmentBusiness;
            //First get any order in invoice 
            foreach (var orderGroup in ordersGroupBy)
            {
                var orderFirst = orderGroup.First();
                var orderItemsInGourp = orderItems.GetCompletetCancelOrderItems(orderFirst.Id);

                var orderItemsGroupBy = transactions.Select(o => o.OrderItem).Where(or => or.Order_Id == orderFirst.Id).GroupBy(oi => oi.Id).ToList();
                //second get orderItem 
                foreach (var orderItemByGroup in orderItemsGroupBy)
                {

                    var orderItemGroupFirst = orderItemByGroup.First();
                    var allOrderItemInstallments = allInstallments.GetListByOrderItemId(orderItemGroupFirst.Id).Where(i => !i.IsDeleted);
                    var orderItemInstallments = transactions.Select(inst => inst.Installment).Where(i => i.OrderItemId == orderItemGroupFirst.Id && !i.IsDeleted);

                    //If orderitem Isn't Donation
                    if (orderItemGroupFirst.ProgramScheduleId.HasValue)
                    {
                        //If orderitem Isn't Donation && ordeitem is Installment
                        if (orderItemInstallments != null && orderItemInstallments.Any())
                        {
                            var orderItemInstallmentpaidAmounts = allOrderItemInstallments.Sum(p => (decimal?)p.PaidAmount) ?? 0;

                            var orderItemInstallmentTotalAmounts = allOrderItemInstallments.Sum(a => (decimal?)a.Amount) ?? 0;

                            orderItemInstallmentBalanse = (orderItemInstallmentTotalAmounts - orderItemInstallmentpaidAmounts);

                            decimal orderItemInvoiceAmount = 0;

                            orderItemInvoiceAmount = orderItemInstallments.Sum(t => t.Amount);
                            //Get installments
                            foreach (var installment in orderItemInstallments)
                            {

                                var paidAmount = installment.PaidAmount != null ? installment.PaidAmount : 0;
                                invoiceItemModel.Instalments.Add(new InvoiceInstallmentViewModel
                                {

                                    DueDate = installment.InstallmentDate,
                                    Amount = installment.Amount,
                                    PaidAmount = paidAmount,
                                    id = installment.Id,
                                    LastDueDate = installment.PaidDate,
                                    InvoiceAmount = (installment.Amount - paidAmount).Value,
                                    OrderItemId = installment.OrderItemId,

                                });
                                //orderItemInvoiceAmount += installment.Balance;
                            }

                            invoiceItemModel.OrderItems.Add(new InvoiceOrderItemViewModel
                            {
                                Attendee = orderItemGroupFirst.FirstName + " " + orderItemGroupFirst.LastName,
                                EntryFee = orderItemGroupFirst.TotalAmount,
                                PaidAmount = orderItemGroupFirst.PaidAmount,
                                Balance = orderItemInstallmentBalanse > 0 ? orderItemInstallmentBalanse : orderItemInstallmentBalanse * (-1),
                                Id = orderItemGroupFirst.Id,
                                EntryFeeName = orderItemGroupFirst.EntryFeeName,
                                PaymentplanType = orderItemGroupFirst.PaymentPlanType,
                                InvoiceAmount = orderItemInvoiceAmount,
                                ProgramName = orderItemGroupFirst.ProgramTypeCategory != ProgramTypeCategory.Camp ? _programBusiness.GetProgramNameOnlyTuition(orderItemGroupFirst.ProgramSchedule, orderItemGroupFirst.EntryFeeName)
                                : _programBusiness.GetProgramName(orderItemGroupFirst.ProgramSchedule, orderItemGroupFirst.EntryFeeName),
                                ProgramScheduleId = orderItemGroupFirst.ProgramScheduleId.Value,
                                OrderId = orderItemGroupFirst.Order_Id,
                            });

                            orderInvoiceAmount += orderItemInvoiceAmount;

                        }
                        else
                        {
                            var orderItemBalance = orderItemGroupFirst.TotalAmount - orderItemGroupFirst.PaidAmount;
                            invoiceItemModel.OrderItems.Add(new InvoiceOrderItemViewModel
                            {
                                Attendee = orderItemGroupFirst.FirstName + " " + orderItemGroupFirst.LastName,
                                EntryFee = orderItemGroupFirst.TotalAmount,
                                PaidAmount = orderItemGroupFirst.PaidAmount,
                                Balance = orderItemBalance > 0 ? orderItemBalance : orderItemBalance <= 0 ? orderItemBalance * (-1) : 0,
                                Id = orderItemGroupFirst.Id,
                                EntryFeeName = orderItemGroupFirst.EntryFeeName,
                                PaymentplanType = orderItemGroupFirst.PaymentPlanType,
                                InvoiceAmount = orderItemBalance > 0 ? orderItemBalance : orderItemBalance <= 0 ? orderItemBalance * (-1) : 0,
                                ProgramName = orderItemGroupFirst.ProgramTypeCategory != ProgramTypeCategory.Camp ? _programBusiness.GetProgramNameOnlyTuition(orderItemGroupFirst.ProgramSchedule, orderItemGroupFirst.EntryFeeName)
                                : _programBusiness.GetProgramName(orderItemGroupFirst.ProgramSchedule, orderItemGroupFirst.EntryFeeName),
                                ProgramScheduleId = orderItemGroupFirst.ProgramScheduleId.Value,
                                OrderId = orderItemGroupFirst.Order_Id,

                            });

                            orderInvoiceAmount += orderItemGroupFirst.TotalAmount - orderItemGroupFirst.PaidAmount;
                        }
                    }

                    //orderItem that is donation
                    else
                    {
                        var orderItemBalance = orderItemGroupFirst.TotalAmount - orderItemGroupFirst.PaidAmount;
                        invoiceItemModel.OrderItems.Add(new InvoiceOrderItemViewModel
                        {

                            Attendee = participantName,
                            EntryFee = orderItemGroupFirst.TotalAmount,
                            PaidAmount = orderItemGroupFirst.PaidAmount,
                            Balance = orderItemBalance > 0 ? orderItemBalance : orderItemBalance <= 0 ? orderItemBalance * (-1) : 0,
                            Id = orderItemGroupFirst.Id,
                            EntryFeeName = orderItemGroupFirst.EntryFeeName,
                            PaymentplanType = orderItemGroupFirst.PaymentPlanType,
                            InvoiceAmount = orderItemBalance > 0 ? orderItemBalance : orderItemBalance <= 0 ? orderItemBalance * (-1) : 0,
                            ProgramName = orderItemGroupFirst.Name,
                            OrderId = orderItemGroupFirst.Order_Id,

                        });
                        orderInvoiceAmount += orderItemGroupFirst.TotalAmount;
                    }

                }

                var orderPaidAmounts = orderItemsInGourp.Where(o => o.ItemStatusReason != OrderItemStatusReasons.canceled || ((o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || o.ProgramTypeCategory == ProgramTypeCategory.Subscription) && o.ItemStatusReason == OrderItemStatusReasons.canceled)).Sum(p => (decimal?)p.PaidAmount) ?? 0;
                var orderTotalAmounts = orderItemsInGourp.Where(o => o.ItemStatusReason != OrderItemStatusReasons.canceled || ((o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || o.ProgramTypeCategory == ProgramTypeCategory.Subscription) && o.ItemStatusReason == OrderItemStatusReasons.canceled)).Sum(a => (decimal?)a.TotalAmount) ?? 0;
                //-----------Get amount for cancelation orderItem-------------
                var cancelOrderItems = orderItemsInGourp.Where(o => o.ItemStatusReason == OrderItemStatusReasons.canceled && o.ProgramTypeCategory != ProgramTypeCategory.Subscription);
                decimal cancelOrderItemTotalAmount = 0;
                decimal cancelOrderItemPaidAmount = 0;

                if (cancelOrderItems != null && cancelOrderItems.Any())
                {
                    cancelOrderItemTotalAmount = 0;
                    cancelOrderItemPaidAmount = cancelOrderItems.Where(o => o.ItemStatusReason == OrderItemStatusReasons.canceled).Sum(p => (decimal?)p.PaidAmount) ?? 0;
                    foreach (var cancelItem in cancelOrderItems)
                    {
                        var cancelOredrItem = cancelItem.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CancellationFee);
                        decimal cncelOredrItemAmount = 0;

                        if (cancelOredrItem.Any())
                        {
                            cncelOredrItemAmount = cancelOredrItem.OrderBy(c => c.Id).ToList().LastOrDefault().Amount;
                        }

                        cancelOrderItemTotalAmount = cancelOrderItemTotalAmount + cncelOredrItemAmount;
                    }
                }
                var cancelItemBalance = cancelOrderItemTotalAmount - cancelOrderItemPaidAmount;

                //----------------end------------------------------------------


                orderbalance = (orderTotalAmounts - orderPaidAmounts) + cancelItemBalance;
                //fill orders
                invoiceItemModel.Orders.Add(new InvoiceOrderViewModel()
                {
                    OrderDate = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), orderFirst.CompleteDate),
                    ConfirmationId = orderFirst.ConfirmationId,
                    OrderAmount = orderTotalAmounts,
                    Balance = orderbalance > 0 ? orderbalance : orderbalance <= 0 ? orderbalance * (-1) : 0,
                    PaidAmount = orderPaidAmounts,
                    Id = orderFirst.Id,
                    InvoiceAmount = orderInvoiceAmount > 0 ? orderInvoiceAmount : orderInvoiceAmount < 0 ? orderInvoiceAmount * (-1) : 0,

                });
                orderInvoiceAmount = 0;
            }

            //model is complex
            var model = new AllInformationInvoiceViewModel()
            {
                HistoryModel = historyModel,
                InfoModel = infoModel,
                InvoiceItemModel = invoiceItemModel
            };

            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult InvoiceExportPdf(int invoiceId, string pageMode)
        {
            var ClubName = ActiveClub.Name;
            dynamic modelPdf = new System.Dynamic.ExpandoObject();
            modelPdf = GetInvoiceDetail(invoiceId);
            ViewBag.pageMode = pageMode;
            return Pdf(ClubName + " (InvoiceDetailReport)", "_InvoiceDetailReportPdfExportView", modelPdf.Data);
        }

        public virtual JsonNetResult GetInvoiceStatus(int invoiceId)
        {
            var status = false;
            var invoiceStatus = _invoiceBusiness.Get(invoiceId).Status;
            if (invoiceStatus == InvoiceStatus.Unpaid && invoiceStatus != InvoiceStatus.Canceled)
            {
                return JsonNet(true);
            }
            else
            {
                return JsonNet(false);
            }
        }

        public virtual JsonNetResult GetTakePaymentModel(int invoiceId)
        {
            var clubBusiness = Ioc.ClubBusiness;
            var invoice = _invoiceBusiness.Get(invoiceId);
            CheckResourceForAccess(invoice);

            var club = invoice.Club;
            var paymentMethods = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
            var model = new TakePaymentInvoiceViewModel()
            {
                InvoiceNumber = invoice.InvoiceNumber,
                AmountDue = invoice.Amount,
                UserName = invoice.User.UserName,
                AvailableCredit = clubBusiness.GetUserCredit(invoice.Club, invoice.UserId, false),
                IsStripeEnabled = (paymentMethods.EnableStripePayment || paymentMethods.EnableAuthorizePayment) ? true : false,
                IsPaypalEnabled = paymentMethods.EnablePaypalPayment,
                PaymentDate = clubBusiness.GetClubDateTime(invoice.ClubId, DateTime.UtcNow),
                InvoiceId = invoiceId
            };

            return JsonNet(model);
        }
        public virtual ActionResult ValidateInvoiceTakePayment(TakePaymentInvoiceViewModel model)
        {

            if (model.SelectedChargeMethod == ((int)ChargeMethodForInvoice.ReceivedPayment).ToString())
            {
                if (model.SelectedPaymentMethod == "0")
                {
                    ModelState.AddModelError("model.SelectedPaymentMethod", "Please select the payment method");

                }
                if (!model.PaymentDate.HasValue)
                {
                    ModelState.AddModelError("model.PaymentDate", "Payment date is required.");
                }
            }
            if (!ModelState.IsValid)
            {
                return JsonFormResponse();
            }

            return Json(new JResult { Status = true, Message = string.Empty, RecordsAffected = 0 });

        }
        public virtual ActionResult SubmitInvoiceTakePayment(TakePaymentInvoiceViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = false };
            try
            {
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                var invoice = _invoiceBusiness.Get(model.InvoiceId);
                CheckResourceForAccess(invoice);

                if (invoice.Status == InvoiceStatus.Paid || invoice.Status == InvoiceStatus.Canceled)
                {
                    return Json(new { Status = result.Status, result = "Refresh", Message = Constants.E_Balance_Zero, RecordsAffected = 0 });
                }
                var currency = invoice.Club.Currency;
                if (invoice.Club.PartnerId.HasValue && invoice.Club.IsSchool)
                {
                    if (ActiveClub.PartnerId != null)
                        currency = Ioc.ClubBusiness.Get(ActiveClub.PartnerId.Value).Currency;
                }
                var amountPayable = invoice.Amount;
                if (amountPayable <= 0)
                {
                    return Json(new { Status = result.Status, result = "Refresh", Message = Constants.E_Balance_Zero, RecordsAffected = 0 });
                }

                var timezone = club.TimeZone > 0 ? club.TimeZone : Jumbula.Common.Enums.TimeZone.UTC;

                if (!model.PaymentDate.HasValue)
                {
                    model.PaymentDate = DateTime.UtcNow;
                }
                if (model.SelectedChargeMethod == ((int)ChargeMethodForInvoice.ReceivedPayment).ToString())
                {

                    PaymentDetail paymentDetail = new PaymentDetail(model.UserName, PaymentDetailStatus.COMPLETED, "", (PaymentMethod)(Convert.ToInt32(model.SelectedPaymentMethod)), currency, string.Empty, string.Empty, string.Empty, JbUserService.GetCurrentUserRoleType());
                    result = UpdateInvoiceAndTransactions(invoice, paymentDetail, DateTimeHelper.ConvertLocalDateTimeToUtc(model.PaymentDate.Value, timezone), false, model.CheckId, model.Note);

                }
                else if (model.SelectedChargeMethod == ((int)ChargeMethodForInvoice.PayNowWithCreditCard).ToString())
                {
                    var token = _invoiceBusiness.GetTokenForInvoice(invoice.Id);
                    var res = _invoiceBusiness.AddMemoforInvoiceTransactions(invoice.Id, model.Note);
                    if (Request.Url != null)
                    {
                        var urlToRedirect =
                            $"{Request.Url.GetLeftPart(UriPartial.Authority)}/Cart?actionCaller={JumbulaSubSystem.Invoice}&token={token}&backToDetail={true}";

                        return Json(new { Status = true, result = "Redirect", url = urlToRedirect, RecordsAffected = 0 });
                    }
                }
                else if (model.SelectedChargeMethod == ((int)ChargeMethodForInvoice.PayWithCredit).ToString())
                {
                    invoice.Club.Users.SingleOrDefault(u => u.UserId == invoice.UserId).Credit -= amountPayable;
                    PaymentDetail paymentDetail = new PaymentDetail(model.UserName, PaymentDetailStatus.COMPLETED, "", PaymentMethod.Credit, currency, string.Empty, string.Empty, string.Empty, JbUserService.GetCurrentUserRoleType());
                    result = UpdateInvoiceAndTransactions(invoice, paymentDetail, model.PaymentDate.Value, true, model.CheckId, model.Note);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(new { Status = result.Status, result = "Refresh", Message = result.ExceptionMessage, RecordsAffected = 0 });

        }
        public OperationStatus UpdateInvoiceAndTransactions(Invoice invoice, PaymentDetail paymentDetail, DateTime paymentDate, bool credit, string checkId = null, string note = null)
        {
            var currency = invoice.Club.Currency;
            if (invoice.Club.PartnerId.HasValue && invoice.Club.IsSchool)
            {
                if (ActiveClub.PartnerId != null) currency = Ioc.ClubBusiness.Get(ActiveClub.PartnerId.Value).Currency;
            }
            var tranDb = Ioc.TransactionActivityBusiness;
            invoice.PaidAmount = invoice.Amount;
            invoice.Status = InvoiceStatus.Paid;
            _invoiceBusiness.Update(invoice);
            var paymentDetailId = Ioc.PaymentDetailBusiness.GetByInvoiceId(invoice.Id).Id;
            var transactions = Ioc.TransactionActivityBusiness.GetList().Where(p => p.PaymentDetailId == paymentDetailId).ToList();
            foreach (var transaction in transactions)
            {
                transaction.PaymentDetail.Currency = paymentDetail.Currency;
                transaction.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                transaction.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                transaction.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                transaction.PaymentDetail.Status = paymentDetail.Status;
                transaction.PaymentDetail.PayKey = paymentDetail.PayKey;
                transaction.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                transaction.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                transaction.PaymentDetail.CreditCardId = paymentDetail.CreditCardId;
                transaction.TransactionStatus = TransactionStatus.Success;
                transaction.HandleMode = HandleMode.Offline;
                transaction.CheckId = checkId;
                transaction.Note = note;
                transaction.TransactionDate = paymentDate;
                tranDb.Update(transaction);

                if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                {
                    var installment = Ioc.InstallmentBusiness.Get(transaction.InstallmentId.Value);
                    installment.PaidAmount = (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0) + transaction.Amount;
                    installment.PaidDate = paymentDate;
                    installment.Status = OrderStatusCategories.completed;
                    Ioc.InstallmentBusiness.Update(installment);

                }

            }
            foreach (var transaction in transactions)
            {
                if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                {
                    var orderItemInInvoice = transaction.OrderItem;
                    var instalment = transaction.Installment;
                    var instalmentordeItamBalance = transaction.Amount;
                    orderItemInInvoice.PaidAmount += transaction.Amount;
                    Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                    Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(Constants.Order_InvoiceHistory, instalmentordeItamBalance, transaction.Note), orderItemInInvoice.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, orderItemInInvoice.Id);

                }
                else
                {
                    var orderItemInInvoice = transaction.OrderItem;
                    var ordeItamBalance = transaction.Amount;
                    orderItemInInvoice.PaidAmount += transaction.Amount;
                    Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                    Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(Constants.Order_InvoiceHistory, ordeItamBalance, transaction.Note), orderItemInInvoice.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, orderItemInInvoice.Id);
                }

            }
            var invoiceId = invoice.Id;
            var today = DateTime.UtcNow;
            var clubTime = paymentDate;
            var userName = this.GetCurrentUserName();
            if (credit)
            {
                var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(Constants.Invoice_TakePaymentWithCredit, userName, CurrencyHelper.FormatCurrencyWithPenny(invoice.Amount, currency)) };
                Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
            }
            else
            {
                var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(Constants.Invoice_TakePayment, userName, CurrencyHelper.FormatCurrencyWithPenny(invoice.Amount, currency)) };
                Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
            }


            EmailService.Get(this.ControllerContext).SendReceivedFamilyInvoicePaymentEmail(transactions, invoice.Amount, DateTime.UtcNow, paymentDetail, invoice);
            return new OperationStatus() { Status = true };
        }
        [HttpGet]
        public virtual JsonNetResult CancelAndReminderInvoice(int invoiceId)
        {
            var invoice = _invoiceBusiness.Get(invoiceId);
            CheckResourceForAccess(invoice);

            var model = new CancelAndReminderInvoiceViewModel()
            {
                InvoiceNumber = invoice.InvoiceNumber,
                UserName = invoice.User.UserName,
                InvoiceId = invoiceId,

            };
            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SubmitCancelationInvoice(CancelAndReminderInvoiceViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = false };
            try
            {
                var invoice = _invoiceBusiness.Get(model.InvoiceId);
                CheckResourceForAccess(invoice);

                invoice.Status = InvoiceStatus.Canceled;
                _invoiceBusiness.Update(invoice);
                var paymentDetail = Ioc.PaymentDetailBusiness.GetByInvoiceId(model.InvoiceId);
                var noteToRecipent = model.NoteToRecipent;
                result = _invoiceBusiness.UpdateInvoiceTransactions(paymentDetail);

                var club = invoice.Club;
                var today = DateTime.UtcNow;
                var invoiceHistory = new InvoiceHistory() { InvoiceId = model.InvoiceId, ActionDate = today, Action = Invoicestatus.Canceled, Description = "You canceled this invoice." };
                Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
                var isCancel = true;

                EmailService.Get(this.ControllerContext).SendCancelationAndReminderInvoice(invoice, isCancel, model.SendEmail, noteToRecipent, model.EmailCc);

            }
            catch (Exception)
            {

                throw;
            }
            return Json(new { Status = result.Status, Message = result.ExceptionMessage });
        }
        public virtual ActionResult SubmitReminderInvoice(CancelAndReminderInvoiceViewModel model)
        {
            OperationStatus result = new OperationStatus() { Status = false };

            if (ModelState.IsValid)
            {
                var invoice = _invoiceBusiness.Get(model.InvoiceId);
                CheckResourceForAccess(invoice);

                var club = invoice.Club;
                var user = invoice.User;
                var token = _invoiceBusiness.GetTokenForInvoice(model.InvoiceId);
                var today = DateTime.UtcNow;
                var username = this.GetCurrentUserName();
                var noteToRecipent = model.NoteToRecipent;
                if (invoice != null)
                {
                    var descriptionHistory = string.IsNullOrEmpty(model.EmailCc) ? string.Format(Constants.Invoice_Reminder, username) : string.Format(Constants.Invoice_ReminderWithCcEmail, username, model.EmailCc);
                    var invoiceHistory = new InvoiceHistory() { InvoiceId = model.InvoiceId, ActionDate = today, Action = Invoicestatus.Initiated, Description = descriptionHistory };
                    Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
                    EmailService.Get(this.ControllerContext).SendCancelationAndReminderInvoice(invoice, false, true, noteToRecipent, model.EmailCc);
                    return Json(new { Status = true, Message = result.ExceptionMessage });
                }
            }

            return JsonFormResponse();
        }
    }
}