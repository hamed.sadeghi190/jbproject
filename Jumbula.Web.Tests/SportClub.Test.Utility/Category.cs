﻿namespace Jumbula.Test.Utility
{
    public static class Category
    {
        // For use with TestCategoryAttribute
        public const string Unit = "Unit";
        public const string Integration = "Integration";
        public const string Database = "Database";
    }
}
