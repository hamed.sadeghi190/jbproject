﻿(function () {
    'use strict';

    angular.module('jbTools').service("customReportService", ['$state', function ($state) {

        var _model;

        function setModel(Data) {
            _model = Data;
        }

        function getModel() {
            return _model;
        }

        function setModelInCookie(name, value, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 1 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = name + "=" + value + "; " + expires;
        }

        function getModelFromCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ')
                    c = c.substring(1);
                if (c.indexOf(name) == 0)
                    return c.substring(name.length, c.length);
            }
            return "";
        }

        function setModelInStorage(data) {
            if (typeof (Storage) !== "undefined") {
                if (sessionStorage.getItem("reportModel")) {
                    sessionStorage.removeItem('reportModel');
                }
                sessionStorage.setItem("reportModel", JSON.stringify(data));
            } else {
                setModelInCookie("reportModel", JSON.stringify(data), 1);
            }
        }

        function saveSchoolSeasonId(schoolId, seasonId) {
            var model = {};
            model.seasonId = seasonId;
            model.schoolId = schoolId;
            setModelInStorage(model);
        }

        function getModelFromStrorage() {
            var model = {};
            if (typeof (Storage) !== "undefined") {
                if (sessionStorage.getItem("reportModel")) {
                    var retrievedModel = sessionStorage.getItem("reportModel");
                    model = JSON.parse(retrievedModel);
                }
            } else {
                var modelString = getModelFromCookie("reportModel");
                model = JSON.parse(modelString);
            }
            return model;
        }

        function initializeDoubleScroll() {
            var scrollInit = setInterval(function () {
                if ($(".double-scroll").size() == 0) {
                    clearInterval(scrollInit);
                    $(".double-scroll .wrapper1,.double-scroll .k-grid-content,#bodyPanel").unbind("scroll.doubleScroll");
                } else {
                    $(".double-scroll .k-grid-header").width($(".double-scroll").width());
                    $(".double-scroll .k-grid-header-wrap").width($(".double-scroll").width() - $(".double-scroll .k-grid-header-locked").width());
                    $(".double-scroll .wrapper1c").css("padding-left", $(".double-scroll .k-grid-header-locked").width());
                    $(".double-scroll .wrapper1 .div1").width($(".double-scroll .k-grid-content table").width());
                    $(".double-scroll .k-grid-content").width($(".double-scroll").width() - $(".double-scroll .k-grid-content-locked").width());
                }
            }, 200);

            $(".double-scroll .wrapper1").unbind("scroll.doubleScroll").bind("scroll.doubleScroll", function () {
                $(".double-scroll .k-grid-content").scrollLeft($(".double-scroll .wrapper1").scrollLeft());
            });

            $(".double-scroll .k-grid-content").unbind("scroll.doubleScroll").bind("scroll.doubleScroll", function () {
                $(".double-scroll .wrapper1").scrollLeft($(".double-scroll .k-grid-content").scrollLeft());
            });
            $("#bodyPanel").unbind("scroll.doubleScroll").bind("scroll.doubleScroll", function () {
                if ($(".double-scroll").offset().top < 0) {
                    $(".double-scroll").css("padding-top", $(".double-scroll .k-grid-header").outerHeight() + 17);
                    $(".double-scroll .k-grid-header,.double-scroll .wrapper1c").css({ "position": "fixed", top: 62, "z-index": 10, "width": $(".double-scroll").width() });
                    $(".double-scroll .wrapper1c").css({ "top": 45 });
                } else {
                    $(".double-scroll").css("padding-top", 0);
                    $(".double-scroll .k-grid-header,.double-scroll .wrapper1c").css({ "position": "relative", top: 0, "width": "auto" });
                }
            });

            var headerCols = $(".double-scroll .k-grid-header colgroup col"),
                contentCols = $(".double-scroll .k-grid-content-locked colgroup col,.double-scroll .k-grid-content colgroup col"),
                headers = $(".double-scroll .k-grid-header th"),
                rows = $(".double-scroll .k-grid-content tr"),
                columnsCount = headerCols.size(),
                rowsCount = rows.size(),
                unit = 9;

            for (var i = 0; i < columnsCount ; i++) {
                if ((is.undefined(headerCols.eq(i).attr("style")) || headerCols.eq(i).attr("style").indexOf("width") == -1) || headerCols.eq(i).data("autoSized")) {
                    var len = headers.eq(i).text().length;
                    for (var j = 0; j < rowsCount ; j++) {
                        var rowCells = $.merge($(".double-scroll .k-grid-content-locked tr").eq(j).children(), $(".double-scroll .k-grid-content tr").eq(j).children());
                        var _len = rowCells.eq(i).text().length;
                        if (_len > len) {
                            len = _len;
                        }
                    }
                    var width = unit * len;
                    headerCols.eq(i).width(width >= 110 ? width : 110).data("autoSized", true);
                    contentCols.eq(i).width(width >= 110 ? width : 110);
                }
            }

            //if ($(".double-scroll .k-grid-content-locked").width() == 0) {
            //    $(".double-scroll .k-grid-content-locked").width(function () {
            //        return $(this).find("tr").width();
            //    }).css("height", "auto");
            //    $(".double-scroll .k-grid-header-locked").width(function () {
            //        return $(this).find("tr").width();
            //    });
            //}

            if ($(".wrapper1c").size() == 0) {
                $(".double-scroll .wrapper1").wrap("<div class='wrapper1c'>");
            }
        }

        function goOrderDetail(seasonDomain, orderId) {
            window.goBackToOverView = true;
            $state.go('Order', { seasonDomain: seasonDomain, orderId: orderId });
        }

        function goOrderItemDetail(seasonDomain, orderItemId) {
            window.goBackToOverView = true;
            $state.go('OrderItem', { seasonDomain: seasonDomain, orderItemId: orderItemId });
        }



        return {
            set: setModel,
            get: getModel,
            initializeDoubleScroll: initializeDoubleScroll,
            goOrderDetail: goOrderDetail,
            goOrderItemDetail: goOrderItemDetail,
            setModelInStorage: setModelInStorage,
            getModelFromStrorage: getModelFromStrorage,
            saveSchoolSeasonId: saveSchoolSeasonId
        }
    }]);
})();