﻿using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;


namespace SportsClub.Areas.Dashboard.Models
{
    public class InvoiceOrderViewModel: BaseViewModel<OrderItem, long>
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Attendee { get; set; }
        public DateTime OrderDate { get; set; }
        public string StrOrderDate
        {
            get
            {
                return OrderDate.ToString("MMM dd, yyyy");
            }

        }
        public long OrderId { get; set; }
        public long ProgramId { get; set; }
        public string ConfirmationId { get; set; }
        public string EntryFee { get; set; }
        public string EntryFeeName { get; set; }
        public decimal Balance { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal OrderAmount { get; set; }
        public string Schedule { get; set; }
        public string ProgramName { get; set; }
    }
}