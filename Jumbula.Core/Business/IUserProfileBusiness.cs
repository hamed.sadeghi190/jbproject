﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IUserProfileBusiness
    {
        JbUser Get(int userId);
        JbUser Get(string userName);
        IQueryable<JbUser> GetList();
        IQueryable<JbUser> GetList(IEnumerable<string> JbUserUsernames);
        IQueryable<JbUser> GetList(IEnumerable<int> users);
        //IQueryable<JbUser> Include(string entityName);
        bool IsUserOwnerOfProfile(int userId, int profileId);
        void EnsureUserIsProfileOwner(int userId, string userName, int profileId);
        OperationStatus Update(JbUser JbUser);
        //bool SetFirstLogin(JbUser profile);
        OperationStatus AddUsersToRole(string userId, RoleCategory role, int ClubId);

        bool IsUserInRole(string userName, RoleCategory role);
        JbRole GetRole(RoleCategory RoleCategory);

        IQueryable<PlayerProfile> GetPlayerProfiles(int userId);

        IQueryable<PlayerProfile> GetPlayerProfiles(List<int> userIds);

        IQueryable<PlayerProfile> GetUserParents(int userId);

        OperationStatus UpdateNotBrandedToken(string userName, string token);

        JbUser GetUserByToken(string token);

        string GetLastLoggedinClubDomain(string userName);

        void UpdateLastLoggedInClub(string userName, string clubDomain);

        string GenerateTokenForSwitchLogin(string userName);

        string GetUrlForLogin(string clubDomain, string userName);
        string GetUrlForLogin(string token);

        bool IsTokenValid(string token);

        string GetStudentImageFolder(GenderCategories gender = GenderCategories.None);
        bool HasPaidApplicationFeeInItems(int clubId, int userId, long seasonId, bool isTestMode);

        bool IsParentDashboardAccesibleByAdmin(int clubId, int userId, bool isPartner);
        bool HasPaidApplicationFeeParticipant(int playerId, long seasonId, bool isTestMode);
    }
}
