﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AccountBillingViewModel:BaseViewModel<ClientCreditCard, long>
    {
        public AccountBillingViewModel()
        { }
        public AccountBillingViewModel(string planType)
        {
            PlanType = planType;
        }
      
        [Display(Name = "Card number")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$",ErrorMessage = "Please enter a valid card number.")]
        [StringLength(17, ErrorMessage = "{0} is too long.")]
        public string CardNumber { get; set; }

        public string Last4Digit { get; set; }

        [Display(Name = "Expiration year")]
        public int Year2Char { get; set; }
       
        public string Method
        {
            get
            {
                return string.Format("{0} ***{1}", Brand, Last4Digit);
            }
        }
        public string Brand { get; set; }
        public string ExpiryDate
        {
            get { return string.Format("{0}/{1}", Month, Year); }
        }
        [Display(Name = "CVC")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter a valid CVC number.")]
        [StringLength(5, ErrorMessage = "{0} is too long.")]
        public string CVV { get; set; }

        [Display(Name = "Expiration year")]
       
        public string Year { get; set; }

        [Display(Name = "Expiration month")]
        [Required(ErrorMessage = "{0} is required")]
        public string Month { get; set; }

        [DisplayName("Cardholder Name")]
        [Required(ErrorMessage = "{0} is required")]
        public string CardHolderName { get; set; }


        [Required(ErrorMessage = "Address is required.")]
        public string Address { get; set; }

        public bool IsDefault { get; set; }

        public string PlanType { get; set; }

        public long ClientId { get; set; }
    }
}

