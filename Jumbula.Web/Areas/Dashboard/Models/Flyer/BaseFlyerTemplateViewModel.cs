﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public abstract class BaseFlyerTemplateViewModel
    {
        public BaseFlyerTemplateViewModel()
        {
            Header = new BaseFlyerHeaderViewModel();
            Body = new BaseFlyerBodyViewModel();
            Footer = new BaseFlyerFooterViewModel();
           
        }

        public BaseFlyerHeaderViewModel Header { get; set; }

        public virtual BaseFlyerBodyViewModel Body { get; set; }

        public BaseFlyerFooterViewModel Footer { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }

        public string Comments { get; set; }
        public bool IsGreenColor { get; set; }

        public bool IsBlueColor { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        public string Phone { get; set; }
    }

    public class BaseFlyerHeaderViewModel
    {
        public string SchoolLogo { get; set; }
        public string PartnerLogo { get; set; }
    }

    public class BaseFlyerBodyViewModel
    {
        public string SchoolName { get; set; }
        public string SchoolDomain { get; set; }
        public string SeasonName { get; set; }
        public string RegistrationOpenDate { get; set; }
        
        public string SeasonDate { get; set; }
        public int ClubId { get; set; }
        public int SeasonId { get; set; }
        public string GeneralRegistrationOpens { get; set; }
        public string GeneralRegistrationCloses { get; set; }

    }


    public class BaseFlyerFooterViewModel
    {
        public string lateRegistrationOpenDateTime { get; set; }

        public decimal  LateFee { get; set; }
        public string Phone { get; set; }

        public string Email { get; set; }
    }


    public class FlyerProgramItemViewModel
    {

        public string Day { get; set; }
        public List<string> NoClass { get; set; }
        public List<int> Session { get; set; }

        public string StrSession {
            get {

                if (Session != null && Session.Any())
                {
                    return string.Join("-", Session);
                }

                return string.Empty;
            }

        }

        public string ProgramsNoClass
        {
            get
            {
                if (NoClass != null && NoClass.Any())
                {
                    return string.Join(", ", NoClass.Distinct());
                }

                return string.Empty;
            }

        }

        public List<FlyerProgramItemDetailViewModel> Details { get; set; }
    }

    public class FlyerProgramItemDetailViewModel
    {
        public int CountSession { get; set; }
        public string ProgramName { get; set; }
        public List<string> NoClass { get; set; }
        public string ProgramDescription { get; set; }
        public List<ClassSessionsDateTime> ClassDates { get; set; }
        public string ProviderName { get; set; }
        public string GradeOrAges { get; set; }
        public decimal? Amount { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StrEndTime { get; set; }
        public string StrStartTime { get; set; }
        public List<DayOfWeek> Days { get; set; }

        public string StrDays {
            get
            {
                var programDays=new List<string>();
                if (Days != null && Days.Any())
                {
                    foreach (var item in Days)
                    {
                      
                        var day = item == DayOfWeek.Thursday ? item.ToString().Substring(0, 2) : item.ToString().Substring(0, 1);
                        programDays.Add(day);
                    }
                    return string.Join(" ", programDays);

                }
                return string.Empty;
            }
        }
    }

}