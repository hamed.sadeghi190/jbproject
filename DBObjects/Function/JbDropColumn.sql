--<< DROP COLUMN Function>>--@mn--2014/01/27	
	
If Object_ID('dbo.JbDropColumn') Is not NULL
DROP function  [dbo].[JbDropColumn]
GO
Create function [dbo].[JbDropColumn]
(
		@tablename nvarchar(100)='[dbo].[JbForms]',--example: dbo.JbForms
		@fieldname nvarchar(100)='Size'--example: Size
)
RETURNS int
AS
BEGIN
	declare @constraint_name sysname, @sql nvarchar(max);

	select @constraint_name = name 
	from sys.default_constraints 
	where parent_object_id = object_id(@tablename)
	AND type = 'D'
	AND parent_column_id = (
		select column_id 
		from sys.columns 
		where object_id = object_id(@tablename)
		and name = @fieldname
		)

	IF EXISTS(SELECT 1 FROM sys.objects WHERE OBJECT_ID=Object_ID(@constraint_name))
	BEGIN
		set @sql = N'alter table '+@tablename+' drop constraint ' + @constraint_name
		exec sp_executesql @sql	
	END

	IF EXISTS(SELECT 1 FROM sys.columns WHERE OBJECT_ID=Object_ID(@tablename) AND [name]=@fieldname)
	BEGIN
		set @sql = N'ALTER TABLE '+@tablename+' Drop Column '+@fieldname
		exec sp_executesql @sql
	END
	return 1;
END		
