﻿namespace Jumbula.Core.Model.Order
{
    public class OrderItemSchedulePartModel
    {
        public decimal Amount { get; set; }
        public int PartId { get; set; }
    }
}
