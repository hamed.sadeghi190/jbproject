﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('accountSubscriptionController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', function ($scope, $http, jbToast, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AccountSettings", order: 2, menu: "SETTINGS" });
        $scope.baseUrl = window.location.origin + "/Dashboard/AccountSetting";
     
        $scope.nonprofit = false;
       
        $scope.selectedPlan = '$0.00 ';
        $scope.isNonProfitPlan = '';
       
        $scope.mainDiscountGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + '/GetAllSubscriptions',
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 5,
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainDiscountGridDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10
                    ],
                buttonCount: 5
            },
            selectable: true,
            change: onChange,
            columns: [
                {
                    field: "Id",
                    title: "Id",
                    hidden: true
                }
                ,{
                field: "Description",
                title: "Annual revenue",
                template: "<strong class='grid-row-title'>#= Description #</strong>"
            }, {
                field: "StrPrice",
                title: "Monthly price",
             
            }],
        };
        $scope.selectedRevenueId = 0;
        function onChange(arg) {
        
            var selectedItem = this.select();
            var price = this.dataItem (selectedItem).StrPrice;
           
            $scope.selectedRevenueId = this.dataItem(selectedItem).Id;
            $scope.selectedPlan = price; //+ ' per month'
            $scope.isNonProfitPlan = '';
            if($scope. nonprofit)
            {
               
                $scope.isNonProfitPlan = '-$10.00 discount (nonprofit organization)';
            }
           
            $scope.$apply();
        }
        $scope.subscriptionSearch = function () {
            $scope.mainGridOptions.dataSource.read();
        }
        $scope.isNonProfit=function()
        {
            $scope.isNonProfitPlan = '';
            if ($scope.nonprofit) {

                $scope.isNonProfitPlan = '-$10.00 discount (nonprofit organization)';
            }
          
      
        }
        $scope.saveSubscription = function () {
            
            $http.post($scope.baseUrl + '/SaveClubSubscription', { revenueId: $scope.selectedRevenueId, isNonProfit: $scope.nonprofit })
                .success(function (data, status, headers, config) {
                    if (data.Status) {
                        if (data.Message == "NeedCredit") {
                            $state.go('AccountSettingManageBilling', { type: 'Subscription' });
                        }
                        else {
                            jbToast.success("Your plan changed successfully.", "");
                            $state.go('AccountSettings');
                        }
                       
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

    }])

})();