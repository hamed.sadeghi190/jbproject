﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;
using SportsClub.Domain;
using SportsClub.Infrastructure;
using SportsClub.Infrastructure.Helper;
using System.IO;
using SportsClub.Models;
using SportsClub.Models.Api;
using System.Web.Helpers;
using SportsClub.Infrastructure.Lucene;

namespace SportsClub.Controllers.Api
{
    public partial class ClubController : ApiController
    {
        // GET api/api
        public IEnumerable<string> Get()
        {
            return new string[] { "Club1", "Club2", "Club3" };
        }
        // GET api/api/5
        public ClubServiceModel Get(string id)
        {
            var clubdomain = id;
            var clubdb = Ioc.IClubDbService.Get(clubdomain, false);
            var events = Ioc.ILuceneHelperService.GetAllActiveEvents(clubdomain);
            var images = Ioc.IImageGalleryDbService.GetImageGalleryItem(clubdomain, null);
            return new ClubServiceModel()
                {
                    Address = clubdb.Address.Address,
                    Coaches =
                        clubdb.Coaches.Select(
                            c =>
                            new ClubServiceModel.ClubCoach()
                                {
                                    Name = c.FullName,
                                    Email = c.Contact.Email,
                                    ImageUri = StorageManager.GetUri(Constants.Path_CoachAvatarChildDir, clubdomain, c.ImageUrl).AbsoluteUri,
                                }).ToList(),
                    Description = clubdb.Description,
                    Domain = clubdb.Domain,
                    Email = clubdb.ContactPersons.FirstOrDefault().Email,
                    Events = events.Select(e => new ClubServiceModel.ClubEvent() { Title = e.Name }).ToList(),
                    Gallery = images.Select(g => new ClubServiceModel.ClubGalleryImage()
                    {
                        Title = g.Title,
                        Description = g.Description,
                        ImageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubdomain, g.Path).AbsoluteUri,
                        ThumbnamilUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubdomain, "ImageGallery/" + Path.GetFileNameWithoutExtension(g.Path) + "_Thumbnail" + Path.GetExtension(g.Path)).AbsoluteUri

                    }).ToList(),
                    Location = new ClubServiceModel.ClubLocation() { Lat = clubdb.Address.Lat, Lng = clubdb.Address.Lng },
                    LogoUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubdomain, clubdb.Logo).AbsoluteUri,
                    Name = clubdb.Name,
                    Site = clubdb.Site,
                    SiteLink = clubdb.Site,
                    Tel = clubdb.ContactPersons.FirstOrDefault().Phone
                };
        }

        // POST api/api
        public void Post([FromBody]string value)
        {
        }

        // PUT api/api/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/api/5
        public void Delete(int id)
        {
        }

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public List<AllClubEvents> Get(string clubDomain, TimeLineFilter eventType, int pageSize, int page)
        {
            // grid paging
            //int pageSize = request.PageSize;
            //int page = request.Page;
            int total = 0;

            //request.Page = 1;

            ClubGridSearchMode searchMode = ClubGridSearchMode.PresentAndFuture;

            #region Sort

            // Define default sort member to startdate and sort direction to descending(sooner events will show upper)
            SortCategory sortMember = SortCategory.StartDate;
            SortDirection sortDirection = SortDirection.Descending;


            #endregion

            ILuceneHelper luceneHelper = Ioc.ILuceneHelperService;
            List<BaseEventSearchResult> lClubEvents = new List<BaseEventSearchResult>();

            // Define search filter model
            AllClubEventsSearch searchModel = new AllClubEventsSearch(); // (JsonConvert.DeserializeObject<AllClubEventsSearch>(Request["Search"].Replace("Search.", "")));

            // Assing search filter to required field for lucene search method

            lClubEvents = luceneHelper.SearchClubsEvent(clubDomain, searchModel.StartDate, searchModel.EndDate, eventType, searchModel.IsOpen, false, searchModel.IsExpired, pageSize, page, ref total, sortMember, sortDirection, searchMode);

            List<AllClubEvents> clubEvents = new List<AllClubEvents>();

            clubEvents = lClubEvents.Select(p => new AllClubEvents
            {
                ClubDomain = p.ClubDomain,
                Domain = p.Domain,
                Name = p.Name,
                StartDate = p.StartDate,
                EndDate = p.EndDate,
                EventType = p.SubDomainCategory.Value,
                EventStatus = p.EventStatus,
                EventStatusType = p.Status,
                CategoryId = p.CategoryId,
                Fee = p.Fee,
                ExpireDate = p.ExpireDate,
                Address = Utilities.JoinStringList(new string[] { p.City, Utilities.GetAbbreviationState(p.State) }, ", "),
                Times = p.Times,
                AdditionalInfo = p.AdditionalInfo,
                MinAge = p.MinAge,
                MaxAge = p.MaxAge,
                MinGrade = p.MinGrade,
                MaxGrade = p.MaxGrade,
                LocationName = p.LocationName,
                Room = p.Room,
                TimeZone = p.TimeZone,
                OutSourcerClubDomain = p.OutSourcerClubDomain,
                VendorClubName = p.VendorClubName,
                FullAddress = p.Address,
                StartRegistreationDeadLine = Utilities.ConvertDateToLocal(p.StartRegisterationDeadLine.HasValue ? p.StartRegisterationDeadLine.Value : new DateTime(), p.TimeZone).ToShortDateString(),
                RegisterStartDate = p.RegisterStartDate.HasValue ? p.RegisterStartDate.Value.ToShortDateString() : string.Empty,
                EventRegStatus = p.EventRegStatus,
                RegisterEndDate = p.RegisterEndDate
            })
            .ToList();

            var result = clubEvents; //.ToDataSourceResult(request);

            // send total order count for show all page in kendo grid
            //result.Total = total;

            return result;


        }
    }
}
