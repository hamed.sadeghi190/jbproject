﻿namespace Jumbula.Common.Enums
{
    public enum EmailRecipientType : byte
    {
        To,
        Cc,
        Bcc
    }
}
