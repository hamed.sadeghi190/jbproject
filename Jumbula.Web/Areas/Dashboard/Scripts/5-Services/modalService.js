﻿(function () {
    'use strict';

    angular.module('dashboardApp').service("modalService", ['$sce', function ($sce) {
        var _model;
        function setModel(Data) {
            _model = Data;
        }
        function getModel() {
            return _model;
        }

        this.showModal = function (elementID) {
            var modal = angular.element('#' + elementID),
                close = angular.element('#fullModalClose');
            modal.find(".modal__content").html('');
  
            close.on('click', function () {
                modal.find('iframe').attr('src', '');
                modal.slideUp(500);
            })
            var videoURL = $sce.trustAsResourceUrl(arguments[1].URL);
            if (arguments[1] && arguments[1].Type == 'Video') {
                if (arguments[1].Title) {
                    
                    modal.find(".modal__content").append('<h2>' + arguments[1].Title + '</h2>');
                }
                modal.find(".modal__content").append('<iframe class="modal-video" src="' + videoURL + '" allowfullscreen></iframe>');
                modal.slideDown(500);
            }


        }
       

    }]);
})();

