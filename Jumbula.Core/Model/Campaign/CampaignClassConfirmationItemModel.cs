﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jumbula.Core.Model.Campaign
{
    public class CampaignClassConfirmationItemModel
    {
        public long ProgramId { get; set; }

        public string ProgramName { get; set; }

        public string SchoolName { get; set; }

        public int RegistrationCount { get; set; }
    }
}
