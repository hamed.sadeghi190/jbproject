﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Report;
using Jumbula.Core.Model.Report.Filter;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Business.ReportBuilders
{
    public class MemberListReportBuilderBusiness : BaseReportBuilder
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly INewReportBusiness _reportBusiness;

        private const string SeasonDomainParam = "seasonDomain";

        private string _seasonDomain;

        public MemberListReportBuilderBusiness(IClubBusiness clubBusiness, INewReportBusiness reportBusiness)
        {
            _reportBusiness = reportBusiness;
            _clubBusiness = clubBusiness;
        }
        public override ReportName ReportName => ReportName.MemberList;
        public override PdfTemplateName? PdfTemplate => PdfTemplateName.Default;
        public override ExcelTemplateName? ExcelTemplate => ExcelTemplateName.Default;

        public override ReportDataModel BindData(Dictionary<string, object> parameters, ClubBaseInfoModel clubBaseInfo, PaginationModel paginationModel, List<ReportGridSort> sort = null)
        {
            var organizationId = GetParameter(parameters, "organizationId").ToString();
            var activeStatus = GetParameter(parameters, "activeStatus").ToString();
            var startDate = GetParameter(parameters, "startDate").ToString() != "-1" ? (DateTime?)GetParameter(parameters, "startDate") : null;
            var endDate = GetParameter(parameters, "startDate").ToString() != "-1" ? (DateTime?)GetParameter(parameters, "endDate") : null;

            var relatedClub = _clubBusiness.GetRelatedClubs(clubBaseInfo.Id);

            if (activeStatus == "1")
            {
                relatedClub = relatedClub.Where(c => c.IsInActive == false);
            }
            else if (activeStatus == "2")
            {
                relatedClub = relatedClub.Where(c => c.IsInActive);
            }

            if (!string.IsNullOrEmpty(organizationId) && organizationId != "-1")
            {
                int typeId = (int.Parse(organizationId));
                relatedClub = relatedClub.Where(c => c.ClubType.ParentId.Value == typeId);
            }

            var data = relatedClub.ToList().Select(p => new MemberListReportBuilderViewModel
            {
                Name = !string.IsNullOrEmpty(p.Name) ? p.Name : "-",
                ContactName = p.ContactPersons.Any() && p.ContactPersons != null ? p.ContactPersons.SingleOrDefault()?.FullName : "-",
                EmailAddress = p.ContactPersons.Any() && p.ContactPersons != null ? p.ContactPersons.SingleOrDefault()?.Email : "-",
                PhoneNumber = p.ContactPersons.Any() && p.ContactPersons != null ? p.ContactPersons.SingleOrDefault()?.Phone : "-",
                AgreementExpirationDate = p.ExpirationDate
            }).AsQueryable();


            if (startDate != null)
            {
                data =
                    data.Where(
                        item =>
                            item.AgreementExpirationDate.HasValue && item.AgreementExpirationDate.Value >= startDate);
            }
            if (endDate != null)
            {
                var date = endDate;
                date = date.Value.AddHours(24 - date.Value.Hour);
                data =
                    data.Where(
                        item =>
                            item.AgreementExpirationDate.HasValue && item.AgreementExpirationDate.Value < date);
            }

            var paginatedData = data.Paginate(paginationModel.Page, paginationModel.PageSize);

            var result = paginatedData.ToPaginatedList<object>(paginationModel.Page, paginationModel.PageSize, data.Count());

            return new ReportDataModel(result, ReportColumn.MemberListReport());
        }

        public override List<IJbReportFilterElement> GetFilters()
        {
            var organizationFilter = new JbDropDownReportFilter()
            {
                Id = 0,
                Name = ReportFilterElementName.Organization,
                Title = ReportFilterElementName.Organization.ToDescription(),
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.PrepareData,
                TargetFilter = null,
                IsRequired = true,
                ParameterName = ReportConstants.OrganizationIdParam,
                Data = _reportBusiness.GetOrganizationList()
            };

            var activeStatusFilter = new JbDropDownReportFilter()
            {
                Id = 1,
                Name = ReportFilterElementName.ActiveStatus,
                Title = ReportFilterElementName.ActiveStatus.ToDescription(),
                Type = ReportFilterType.DropDown,
                ActionFilter = ReportFilterAction.PrepareData,
                TargetFilter = null,
                IsRequired = false,
                ParameterName = ReportConstants.ActiveStatusParam,
                Data = _reportBusiness.GetActiveStatus(),
                IsBr = true,
                IsHr = false
            };

            var startDateFilter = new JbDatePickerReportFilter()
            {
                Id = 2,
                Name = ReportFilterElementName.StartDate,
                Title = ReportFilterElementName.StartDate.ToDescription(),
                Type = ReportFilterType.DatePicker,
                ActionFilter = null,
                TargetFilter = null,
                IsRequired = false,
                ParameterName = ReportConstants.StartDateParam,
                ParameterValue = null
            };

            var endDateFilter = new JbDatePickerReportFilter()
            {
                Id = 3,
                Name = ReportFilterElementName.EndDate,
                Title = ReportFilterElementName.EndDate.ToDescription(),
                Type = ReportFilterType.DatePicker,
                ActionFilter = null,
                TargetFilter = null,
                IsRequired = false,
                ParameterName = ReportConstants.EndDateParam
            };

            return new List<IJbReportFilterElement>() {organizationFilter, activeStatusFilter, startDateFilter, endDateFilter};
        }

        public override List<ReportHeader> GetHeaders()
        {
            return null;
        }

        public override List<ReportGridColumn> GetColumns()
        {
            return null;
        }

        public override PaginatedList<object> GetData()
        {
            return null;
        }


        public override void Initiate()
        {
            _seasonDomain = GetParameter(Parameters, SeasonDomainParam).ToString();
        }

    }

}
