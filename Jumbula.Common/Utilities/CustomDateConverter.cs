﻿using Newtonsoft.Json.Converters;

namespace Jumbula.Common.Utilities
{
    public class CustomDateConverter : IsoDateTimeConverter
    {
        public CustomDateConverter()
        {
            base.DateTimeFormat = "dd MMM yyyy";
        }
    }

    public class CustomTimeConverter : IsoDateTimeConverter
    {
        public CustomTimeConverter()
        {
        
            base.DateTimeFormat = "h:mm tt";
        }
    }

    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {

            base.DateTimeFormat = "dd MMM yyyy h:mm tt";
        }
    }
}