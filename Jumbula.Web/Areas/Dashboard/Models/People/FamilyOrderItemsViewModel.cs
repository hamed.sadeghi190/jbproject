﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FamilyOrderItemsViewModel :BaseViewModel<OrderItem, long>
    {
        public int UserId { get; set; }
        public string Attendee { get; set; }
        public string UserName { get; set; }
        public decimal EntryFee { get; set; }
        public string EntryFeeName { get; set; }
        public decimal Balance { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PaidAmount { get; set; }
       
        public int ItemStatusReason { get; set; }
        public int ItemStatus { get; set; }
        public string ProgramName { get; set; }
        public string Season { get; set; }
        public long? SeasonId { get; set; }
        public string SeasonDomain { get; set; }
        public PaymentPlanType PaymentplanType { get; set; }

        public List<FamilyOrderItemInstalmentsViewModel> Installments { get; set; }
        public bool Checked { get; set; }

        public long PaymentDetailId { get; set; }

        public string CheckId { get; set; }
        public long OrderId { get; set; }

        public string ConfirmationId { get; set; }
        public long ProgramScheduleId { get; set; }
        public bool IsInvoiced { get; set; }
        public bool IsCancel { get; set; }

    }



}