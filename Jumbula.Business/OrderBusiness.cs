﻿
using Jb.Framework.Common;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Enums;
using Jumbula.Common.Exceptions;
using Jumbula.Common.Helper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
using Jumbula.Core.Model.Order;
using Jumbula.Core.Model.Email;

namespace Jumbula.Business
{
    public class OrderBusiness : IOrderBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderHistoryBusiness _orderHistoryBusiness;
        private IProgramBusiness _programBusiness;
        private ISubscriptionBusiness _subscriptionBusiness;
        private readonly IFormBusiness _formBusiness;
        private readonly IRepository<Order, long> _orderRepository;
        private readonly IRepository<Club> _clubRepository;
        private readonly IProgramSessionBusiness _programSessionBusiness;
        private readonly IOrderSessionBusiness _orderSessionBusiness;
        private readonly IEmailBusiness _emailBusiness;

        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();


        private readonly IEntitiesContext _dataContext;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;

        public OrderBusiness(IClubBusiness clubBusiness, IOrderHistoryBusiness orderHistoryBusiness, IFormBusiness formBusiness,
            IRepository<Order, long> orderRepository, IProgramSessionBusiness programSesssionBusiness,
            IOrderSessionBusiness orderSessioinBusiness, IRepository<Club> clubRepository, IEntitiesContext dataContext,
            IOrderInstallmentBusiness orderInstallmentBusiness, IOrderItemBusiness orderItemBusiness, IEmailBusiness emailBusiness)
        {
            _clubBusiness = clubBusiness;
            _orderHistoryBusiness = orderHistoryBusiness;
            _formBusiness = formBusiness;
            _programSessionBusiness = programSesssionBusiness;
            _orderSessionBusiness = orderSessioinBusiness;
            _dataContext = dataContext;
            _orderRepository = orderRepository;
            _clubRepository = clubRepository;
            _orderInstallmentBusiness = orderInstallmentBusiness;
            _orderItemBusiness = orderItemBusiness;
            _emailBusiness = emailBusiness;
        }

        public IProgramBusiness ProgramBusiness
        {
            get => _programBusiness;
            set => _programBusiness = value;
        }

        public ISubscriptionBusiness SubscriptionBusiness
        {
            get => _subscriptionBusiness;
            set => _subscriptionBusiness = value;
        }

        public Order Get(long orderId)
        {
            return GetAllOrders().SingleOrDefault(o => o.Id == orderId);
        }
        public Order Get(string confirmationId)
        {
            return GetList().SingleOrDefault(order => order.ConfirmationId == confirmationId);
        }
        public Order Get(long orderId, params Expression<Func<Order, object>>[] includes)
        {
            return _orderRepository.GetList(includes).SingleOrDefault(order => order.Id == orderId);
        }
        public Order GetOrderInCart(int userId, int clubId)
        {
            var utcNow = DateTime.Parse(Constants.LastDateForGetCartOrders);

            if (GetAllOrders().Any(r => r.UserId == userId && r.ClubId == clubId && r.IsDraft && r.CompleteDate > utcNow))
            {
                return GetAllOrders().Where(r => r.UserId == userId && r.ClubId == clubId && r.IsDraft).OrderByDescending(r => r.Id).First();
            }
            return null;
        }

        public int CartItems(int userId, int clubId)
        {
            var item = GetOrderInCart(userId, clubId);
            return (item != null && item.OrderItems != null && item.OrderItems.Any(c => c.ItemStatus != OrderItemStatusCategories.deleted)) ? item.RegularOrderItems.Count() + item.PreregisterItems.Count() : 0;
        }
        public IQueryable<Order> GetAllOrders()
        {
            return _orderRepository.GetList().Where(c => c.OrderStatus != OrderStatusCategories.deleted);
        }
        public IQueryable<Order> GetAllOrdersNeedCreditCard()
        {
            return _orderRepository.GetList().Where(o => o.OrderStatus != OrderStatusCategories.deleted && o.OrderAmount > 0 && o.IsAutoCharge && o.IsLive && o.OrderStatus == OrderStatusCategories.completed).OrderByDescending(o => o.CompleteDate);
        }
        public IQueryable<Order> GetList()
        {
            return _orderRepository.GetList().Where(order => !order.IsDraft && (order.OrderStatus == OrderStatusCategories.completed || order.OrderStatus == OrderStatusCategories.pending || order.OrderStatus == OrderStatusCategories.canceled
                                                                                                                         || order.OrderStatus == OrderStatusCategories.refunded || order.OrderStatus == OrderStatusCategories.transferred));
        }
        public OperationStatus Create(Order order)
        {
            try
            {
                order.OrderAmount = order.CalculateOrderAmount;
                return _orderRepository.Create(order);
            }
            catch (DbEntityValidationException edx)
            {
                throw new JumbulaDbException(edx);
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public OperationStatus BaseCreate(Order order)
        {
            try
            {
                return _orderRepository.Create(order);
            }
            catch (DbEntityValidationException edx)
            {
                throw new JumbulaDbException(edx);
            }
            catch (Exception e)
            {
                throw new JumbulaDbException(e);
            }
        }

        public OperationStatus Update(Order order)
        {

            try
            {
                var clubId = GetOrderClubIdFromItems(order.OrderItems.Where(o => o.ItemStatus == OrderItemStatusCategories.completed).ToList());
                order.ClubId = clubId.HasValue ? clubId.Value : order.ClubId;
                order.OrderAmount = order.CalculateOrderAmount;

                var result = _orderRepository.Update(order);

                TransferPreregisteredAndLotteryItemsToNewOrder(order);

                return result;
            }
            catch (Exception ex)
            {

                return new OperationStatus() { Status = false, Exception = ex, Message = ex.Message };
            }
        }

        public Order GetOrdersByConfirmationID(string confirmationID)
        {
            return _orderRepository.GetList().Include("Club").SingleOrDefault(r => r.ConfirmationId == confirmationID);
        }


        public IQueryable<OrderChargeDiscount> GetChargeDiscounts(long orderItemId)
        {
            return _dataContext.Set<OrderChargeDiscount>().Where(c => c.OrderItemId == orderItemId);
        }

        public OperationStatus DeleteSessions(List<OrderSession> orderSessions)
        {
            OperationStatus opStatus = new OperationStatus { Status = true };
            foreach (var session in orderSessions)
                _dataContext.Set<OrderSession>().Remove(session);

            opStatus.Status = _dataContext.SaveChanges() > 0;
            return opStatus;
        }

        public JbForm GetLastOrderRegForm(int userId, int? playerId, long programId)
        {

            try
            {
                OrderItem resultOrderItem = null;

                bool differentPlayerMode = !playerId.HasValue;

                var program = _programBusiness.Get(programId);
                var regFormTemplate = program.ClubFormTemplates.SingleOrDefault(s => s.JbForm != null && s.FormType == FormType.Registration);

                var orderItems = GetAllOrders()
                  .Where(r => r.UserId == userId)
                  .SelectMany(order => order.OrderItems)
                  .Where(item => item.ItemStatus != OrderItemStatusCategories.deleted && item.JbFormId.HasValue && (!playerId.HasValue || item.PlayerId == playerId))
                  .OrderBy(r => r.Id);

                if (orderItems == null || !orderItems.Any())
                {
                    orderItems = GetAllOrders()
                     .Where(r => r.UserId == userId)
                     .SelectMany(order => order.OrderItems)
                     .Where(item => item.ItemStatus != OrderItemStatusCategories.deleted && item.JbFormId.HasValue)
                     .OrderBy(r => r.Id);

                    if (orderItems != null && orderItems.Any())
                    {
                        differentPlayerMode = true;
                    }
                }

                var lastSimilarForms = orderItems.Where(o => o.JbForm.ParentId.HasValue && o.JbForm.ParentId == regFormTemplate.JbForm_Id).ToList();

                if (lastSimilarForms != null && lastSimilarForms.Any())
                {
                    resultOrderItem = lastSimilarForms.Last();
                }
                else
                {
                    resultOrderItem = orderItems.ToList().LastOrDefault();
                }

                if (resultOrderItem != null)
                {
                    if (differentPlayerMode)
                    {
                        var participantSection = resultOrderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString());

                        var formBusiness = _formBusiness;

                        foreach (var element in participantSection.Elements)
                        {
                            formBusiness.SetElementEmpty(element);
                        }
                    }

                    return resultOrderItem.JbForm;
                }
            }
            catch
            {

            }

            return null;
        }

        public JbForm GetLastOrderRegForm(int userId, int? playerId)
        {

            try
            {
                var firstOrDefault = GetAllOrders()
                    .Where(r => r.UserId == userId)
                    .SelectMany(order => order.OrderItems)
                    .Where(item => item.ItemStatus != OrderItemStatusCategories.deleted && item.JbFormId.HasValue && (!playerId.HasValue || item.PlayerId == playerId))
                    .OrderByDescending(r => r.Id).FirstOrDefault();


                if (firstOrDefault != null)
                {
                    if (!playerId.HasValue)
                    {
                        firstOrDefault.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString())
                            .Elements.Where(elm => elm is JbTextBox).ToList().ForEach(elm => elm.SetValue(null));
                    }

                    return firstOrDefault.JbForm;
                }
            }
            catch
            {

            }

            return null;
        }

        public IQueryable<Order> GetClubUserAllOrders(int userId, int clubId)
        {
            return GetList().Where(o => o.Club.Id.Equals(clubId) && o.UserId.Equals(userId));
        }

        public IQueryable<Order> GetUserOrders(int userId)
        {
            return GetList().Where(o => o.UserId.Equals(userId) && o.IsLive);
        }

        public IQueryable<OrderItem> GetClubUserOrderItems(int userId, int playerId, int clubId)
        {

            return
                GetList()
                    .Where(
                        o =>
                            o.Club.Id.Equals(clubId) &&
                            o.UserId.Equals(userId) &&
                            o.IsLive)
                    .SelectMany(
                        o =>
                            o.OrderItems.Where(
                                oi =>
                                    oi.ItemStatus != OrderItemStatusCategories.deleted &&
                                    oi.ItemStatus != OrderItemStatusCategories.notInitiated &&
                                    oi.PlayerId.HasValue &&
                                    oi.PlayerId.Value == playerId));
        }

        public Order GetLastUserOrder(int userId, int clubId)
        {
            var lastOrder = GetList().Where(o => o.UserId.Equals(userId) && o.ClubId.Equals(clubId)).OrderByDescending(o => o.Id).First();

            return lastOrder;
        }
        public Order GetLastUserOrder(int userId)
        {
            var lastOrder = GetList().Where(o => o.UserId.Equals(userId)).OrderByDescending(o => o.Id).First();

            return lastOrder;
        }
        public Order GetLastUserOrder(string userName, List<int> clubIds)
        {
            var lastOrder = _orderRepository.GetList().Where(o => o.User.UserName == userName && clubIds.Contains(o.ClubId)).ToList().LastOrDefault();

            return lastOrder;
        }

        public Order GetLastOfflineOrderForClub(int clubId, string clubDomain = "")
        {

            if (DoesOfflineOrderExist(clubId, clubDomain))
            {
                var utcNow = DateTime.Parse(Constants.LastDateForGetCartOrders);

                if (clubId > 0)
                {
                    return GetAllOrders().Where(o => o.ClubId == clubId && o.IsDraft == true && o.OrderMode == OrderMode.Offline && o.CompleteDate.CompareTo(utcNow) > 0).OrderByDescending(o => o.Id).First();
                }
                else
                {
                    return GetAllOrders().Where(o => o.Club.Domain == clubDomain && o.IsDraft == true && o.OrderMode == OrderMode.Offline && o.CompleteDate.CompareTo(utcNow) > 0).OrderByDescending(o => o.Id).First();
                }
            }
            return null;
        }
        public bool DoesOfflineOrderExist(int clubId, string clubDomain = "")
        {
            var utcNow = DateTime.Parse(Constants.LastDateForGetCartOrders);

            if (clubId > 0)
            {
                return GetAllOrders().Any(o => o.ClubId == clubId && o.IsDraft == true && o.OrderMode == OrderMode.Offline && o.CompleteDate.CompareTo(utcNow) > 0);
            }
            else
            {
                return GetAllOrders().Any(o => o.Club.Domain == clubDomain && o.IsDraft == true && o.OrderMode == OrderMode.Offline && o.CompleteDate.CompareTo(utcNow) > 0);
            }
        }

        public OperationStatus Delete(long orderId)
        {

            var order = Get(orderId);
            if (order != null && order.OrderItems.All(c => c.ItemStatus == OrderItemStatusCategories.deleted))
            {
                order.OrderStatus = OrderStatusCategories.deleted;
                return Update(order);
            }
            return new OperationStatus() { Status = true, RecordsAffected = 0 };
        }

        public decimal CalculateOrderPayableAmount(Order order)
        {
            if (order.PaymentPlanType == PaymentPlanType.FullPaid)
            {
                return order.CalculateOrderAmount;
            }
            else
            {
                decimal payableAmount = 0;
                foreach (var item in order.OrderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.notInitiated && o.ItemStatus != OrderItemStatusCategories.changed && o.PreRegistrationStatus != PreRegistrationStatus.Initiated))
                {
                    if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                    {
                        payableAmount += _subscriptionBusiness.CalculateSubscriptionPayableAmount(item);
                    }
                    else
                    {
                        if (item.PaymentPlanType == PaymentPlanType.Installment && item.Installments.Any(i => !i.IsDeleted))
                        {
                            payableAmount += item.Installments.First().Amount;
                        }
                        else
                        {
                            payableAmount += item.TotalAmount;
                        }
                    }
                }

                return payableAmount;
            }
        }

        public void SetPaymentTransaction(Order order, PaymentDetail paymentDetail, TransactionStatus transactionStatus, OrderInstallment installment = null, DateTime? date = null, decimal amount = -1, OrderItem orderItem = null, long? instId = null, string note = "", string token = "", string checkId = "", TransactionCategory tranCategory = TransactionCategory.Sale, HandleMode handleMode = HandleMode.Online)
        {
            TransactionCategory category = tranCategory;
            if (date == null)
            {
                date = DateTime.UtcNow;
            }
            if (order != null && order.OrderItems.Any(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.notInitiated))
            {
                foreach (var item in order.OrderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.notInitiated))
                {
                    var transactionAmount = amount;
                    long? installmentId = null;

                    if (item.TransactionActivities == null)
                    {
                        item.TransactionActivities = new List<TransactionActivity>();
                    }

                    if (transactionAmount < 0)
                    {
                        transactionAmount = item.TotalAmount;

                        if (item.PaymentPlanType == PaymentPlanType.Installment && item.Installments.Any())
                        {
                            if (item.ProgramTypeCategory != ProgramTypeCategory.Subscription && item.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                            {
                                transactionAmount = item.Installments.Where(i => !i.IsDeleted).FirstOrDefault().Amount;
                                installmentId = item.Installments.Where(i => !i.IsDeleted).FirstOrDefault().Id;
                            }
                            else
                            {
                                var subscriptionBusiness = _subscriptionBusiness;

                                foreach (var installmentItem in item.Installments.Where(i => !i.IsDeleted))
                                {
                                    if (subscriptionBusiness.IsInstallmentItemIsForPay(item, installmentItem, transactionStatus))
                                    {
                                        item.TransactionActivities.Add(new TransactionActivity()
                                        {
                                            Amount = installmentItem.Amount,
                                            TransactionCategory = category,
                                            ClubId = order.ClubId,
                                            OrderId = order.Id,
                                            OrderItemId = item.Id,
                                            InstallmentId = transactionStatus == TransactionStatus.Success ? installmentItem.Id : (long?)null,
                                            PaymentDetail = paymentDetail,
                                            TransactionDate = date.Value,
                                            TransactionStatus = transactionStatus,
                                            TransactionType = TransactionType.Payment,
                                            SeasonId = item.SeasonId,
                                            Note = note,
                                            Token = token,
                                            CheckId = checkId,
                                            HandleMode = handleMode,
                                            MetaData = new MetaData
                                            {
                                                DateCreated = DateTime.UtcNow,
                                                DateUpdated = DateTime.UtcNow
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }

                    if (!((item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && item.PaymentPlanType == PaymentPlanType.Installment))
                    {
                        item.TransactionActivities.Add(new TransactionActivity()
                        {
                            Amount = transactionAmount,
                            TransactionCategory = category,
                            ClubId = order.ClubId,
                            OrderId = order.Id,
                            OrderItemId = item.Id,
                            InstallmentId = installmentId,
                            PaymentDetail = paymentDetail,
                            TransactionDate = date.Value,
                            TransactionStatus = transactionStatus,
                            TransactionType = TransactionType.Payment,
                            SeasonId = item.SeasonId,
                            Note = note,
                            Token = token,
                            CheckId = checkId,
                            HandleMode = handleMode,
                            MetaData = new MetaData
                            {
                                DateCreated = DateTime.UtcNow,
                                DateUpdated = DateTime.UtcNow
                            }
                        });
                    }

                }
            }

            if (orderItem != null)
            {
                if (orderItem.TransactionActivities == null)
                {
                    orderItem.TransactionActivities = new List<TransactionActivity>();
                }
                orderItem.TransactionActivities.Add(new TransactionActivity()
                {
                    Amount = amount,
                    TransactionCategory = category,
                    ClubId = orderItem.Order.ClubId,
                    OrderId = orderItem.Order_Id,
                    OrderItemId = orderItem.Id,
                    InstallmentId = instId,
                    PaymentDetail = paymentDetail,
                    TransactionDate = date.Value,
                    TransactionStatus = transactionStatus,
                    TransactionType = TransactionType.Payment,
                    SeasonId = orderItem.SeasonId,
                    Note = note,
                    Token = token,
                    CheckId = checkId,
                    HandleMode = handleMode,
                    MetaData = new MetaData
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow
                    }
                });
            }

            if (installment != null)
            {
                if (installment.TransactionActivities == null)
                {
                    installment.TransactionActivities = new List<TransactionActivity>();
                }
                var transactionAmount = amount;

                if (transactionAmount < 0)
                {
                    transactionAmount = installment.Amount;
                }
                installment.TransactionActivities.Add(new TransactionActivity()
                {
                    Amount = transactionAmount,
                    TransactionCategory = category == TransactionCategory.Sale ? TransactionCategory.Installment : category,
                    ClubId = installment.OrderItem.Order.ClubId,
                    OrderId = installment.OrderItem.Order_Id,
                    OrderItemId = installment.OrderItemId,
                    InstallmentId = installment.Id,
                    PaymentDetail = paymentDetail,
                    TransactionDate = date.Value,
                    TransactionStatus = transactionStatus,
                    TransactionType = TransactionType.Payment,
                    SeasonId = installment.OrderItem.SeasonId,
                    Note = note,
                    Token = token,
                    CheckId = checkId,
                    HandleMode = handleMode,
                    MetaData = new MetaData
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow
                    }
                });
            }
        }

        public OperationStatus Transfer(long sourceOrderItemId, OrderItem targetOrderItem, int userId, string transferMemo)
        {
            var order = GetAllOrders().SingleOrDefault(o => o.OrderItems.Any(i => i.Id == sourceOrderItemId));


            var sourceOrderItem = order.OrderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.notInitiated).SingleOrDefault(o => o.Id == sourceOrderItemId);

            sourceOrderItem.ItemStatus = OrderItemStatusCategories.changed;
            sourceOrderItem.ItemStatusReason = OrderItemStatusReasons.transferOut;

            sourceOrderItem.Installments.ForEach(i => i.PendingPreapprovalTransactions.ToList().ForEach(p => p.Status = PendingPreapprovalTransactionStatus.Deleted));

            sourceOrderItem.TransactionActivities.ToList().ForEach(c => c.TransactionStatus = TransactionStatus.Deleted);


            var sourceTuition = sourceOrderItem.OrderChargeDiscounts.SingleOrDefault(s => !s.IsDeleted && s.Category == ChargeDiscountCategory.EntryFee);
            var targetTuition = targetOrderItem.OrderChargeDiscounts.SingleOrDefault(s => !s.IsDeleted && s.Category == ChargeDiscountCategory.EntryFee);


            sourceOrderItem.PaidAmount = 0;

            if (sourceOrderItem.ProgramTypeCategory == ProgramTypeCategory.Camp)
            {
                var orderSessions = _orderSessionBusiness.GetOrderItemSessions(sourceOrderItem.Id);
                if (orderSessions.Any())
                {
                    var RemainingOrdersessions = orderSessions.Where(s => s.ProgramSession.StartDateTime > DateTime.UtcNow);
                    _dataContext.Set<OrderSession>().RemoveRange(RemainingOrdersessions);
                }
            }

            if (targetOrderItem.ProgramTypeCategory == ProgramTypeCategory.Camp)
            {
                var charge = targetOrderItem.ProgramSchedule.Charges.FirstOrDefault(c => c.Id == targetTuition.ChargeId);
                var programSessions = _programSessionBusiness.GetList(charge);

                foreach (var programSession in programSessions)
                {
                    var orderSession = new OrderSession()
                    {
                        ProgramSessionId = programSession.Id,
                        MetaData = new MetaData()
                        {
                            DateCreated = DateTime.UtcNow,
                            DateUpdated = DateTime.UtcNow,
                        }
                    };

                    targetOrderItem.OrderSessions.Add(orderSession);
                }
            }

            order.OrderItems.Add(targetOrderItem);

            var result = Update(order);

            var name = sourceTuition == null ? string.Empty : sourceTuition.Name;
            var amount = sourceTuition != null ? sourceTuition.Amount : 0;

            var historyDescription = string.Format("Order {0} - {1}(${2}) transferred to {3} - {4}(${5})",
                   sourceOrderItem.ProgramSchedule.Program.Name, name, amount,
                   targetOrderItem.ProgramSchedule.Program.Name, targetTuition.Name, targetTuition.Amount);

            historyDescription += string.Format(" Memo: {0}", transferMemo);

            if (result.Status)
            {
                _orderHistoryBusiness.AddOrderHistory(OrderAction.Transfer, historyDescription, order.Id, userId, targetOrderItem.Id);
                _orderHistoryBusiness.AddOrderHistory(OrderAction.Transfer, historyDescription, order.Id, userId, sourceOrderItem.Id);
            }

            return result;
        }

        public void PayOrderByCredit(int? userId, Cart cart, string orderHistoryDescription, DateTime date, decimal amount)
        {
            var clubBusiness = _clubBusiness;

            var isTestMode = !cart.Order.IsLive;

            var userClub = cart.Order.Club.Users.SingleOrDefault(u => u.UserId == cart.Order.UserId);

            if (userClub == null)
            {
                var status = clubBusiness.AddUserInClubUsers(cart.Order.UserId, cart.Order.ClubId);

                if (status)
                {
                    userClub = cart.Order.Club.Users.SingleOrDefault(u => u.UserId == cart.Order.UserId);
                    userClub.Credit = clubBusiness.GetUserCredit(userClub.Club, userClub.UserId, false);
                    userClub.TestCredit = clubBusiness.GetUserCredit(userClub.Club, userClub.UserId, true);
                }
                else
                {
                    userClub = new ClubUser();
                    userClub.Credit = 0;
                    userClub.TestCredit = 0;
                }
            }


            var userCredit = isTestMode ? userClub.TestCredit : userClub.Credit;

            if (amount > 0 && userCredit >= amount)
            {
                PaymentDetail paymentdetail = new PaymentDetail(cart.Order.User.UserName, PaymentDetailStatus.COMPLETED, "", PaymentMethod.Credit, cart.Order.Club.Currency, "", "", string.Empty, RoleCategoryType.Parent);



                if (isTestMode)
                {
                    userClub.TestCredit -= amount;
                }
                else
                {
                    userClub.Credit -= amount;
                }

                var availablecredtit = amount;

                foreach (var orderitem in cart.Order.RegularOrderItems)
                {
                    var transactionactivity = new TransactionActivity()
                    {
                        Club = cart.Order.Club,
                        HandleMode = HandleMode.Online,
                        Order = cart.Order,
                        PaymentDetail = paymentdetail,
                        TransactionDate = date,
                        TransactionType = TransactionType.Payment,
                        TransactionStatus = TransactionStatus.Success,
                    };

                    orderitem.ItemStatus = OrderItemStatusCategories.completed;
                    orderitem.ItemStatusReason = OrderItemStatusReasons.regular;

                    if (orderitem.PaymentPlanType == PaymentPlanType.Installment)
                    {
                        if (orderitem.ProgramTypeCategory != ProgramTypeCategory.Subscription && orderitem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare)
                        {
                            var firstInst = orderitem.Installments.Where(i => !i.IsDeleted).FirstOrDefault();

                            firstInst.Status = OrderStatusCategories.initiated;
                            firstInst.Token = Guid.NewGuid().ToString("N");
                            firstInst.NoticeSent = true;

                            if (availablecredtit - firstInst.Amount >= 0 & firstInst.Amount != 0)
                            {
                                firstInst.PaidAmount = firstInst.Amount;
                                orderitem.PaidAmount = firstInst.Amount;

                                transactionactivity.Amount = firstInst.Amount;
                                transactionactivity.Installment = firstInst;
                                transactionactivity.OrderItem = orderitem;
                                transactionactivity.TransactionCategory = TransactionCategory.Installment;

                                orderitem.TransactionActivities.Add(transactionactivity);
                                availablecredtit -= firstInst.Amount;
                            }
                        }
                        else
                        {
                            var itemPayableAmount = _subscriptionBusiness.CalculateSubscriptionPayableAmount(orderitem);

                            decimal appliedPayableAmount = 0;
                            int i = 0;

                            while (appliedPayableAmount <= itemPayableAmount)
                            {
                                var installmentItem = orderitem.Installments[i];

                                installmentItem.Status = OrderStatusCategories.completed;
                                installmentItem.PaidAmount = installmentItem.Amount;
                                installmentItem.NoticeSent = true;
                                installmentItem.Token = Guid.NewGuid().ToString("N");

                                appliedPayableAmount += installmentItem.Amount;
                                i++;
                            }
                        }

                    }
                    else
                    {
                        if (availablecredtit - orderitem.EntryFee >= 0 & orderitem.EntryFee != 0)
                        {
                            orderitem.PaidAmount = orderitem.EntryFee;

                            transactionactivity.Amount = orderitem.EntryFee;
                            transactionactivity.Installment = null;
                            transactionactivity.OrderItem = orderitem;
                            transactionactivity.TransactionCategory = TransactionCategory.TakePayment;

                            orderitem.TransactionActivities.Add(transactionactivity);
                            availablecredtit -= orderitem.EntryFee;
                        }
                        //else if (availablecredtit - orderitem.EntryFee < 0 && availablecredtit != 0)
                        //{
                        //    orderitem.PaidAmount = availablecredtit;

                        //    transactionactivity.Amount = availablecredtit;
                        //    transactionactivity.Installment = null;
                        //    transactionactivity.OrderItem = orderitem;
                        //    transactionactivity.TransactionCategory = TransactionCategory.TakePayment;

                        //    orderitem.TransactionActivities.Add(transactionactivity);
                        //    availablecredtit = 0;
                        //}
                    }
                }
            }
        }

        public OperationStatus Update(Order order, List<OrderItem> newOrderItems)
        {
            var ists = newOrderItems.First().ISTS;

            List<OrderItem> orderItemsForDelete = new List<OrderItem>();

            foreach (var item in order.OrderItems.Where(i => i.ISTS == ists))
            {
                orderItemsForDelete.Add(item);
            }

            foreach (var item in orderItemsForDelete)
            {
                _dataContext.Entry(item).State = EntityState.Deleted;
            }

            foreach (var item in newOrderItems)
            {
                order.OrderItems.Add(item);
            }

            var result = Update(order);

            return result;

        }

        public void UpdateLiveTestMode(Order order)
        {
            bool currentIsLive = order.IsLive;
            bool newIsLive = false;

            if (!order.RegularOrderItems.Any(o => o.SeasonId.HasValue))
            {
                newIsLive = true;
            }
            else
            {
                if (!order.RegularOrderItems.Any(c => c.SeasonId.HasValue && c.Season.Status == SeasonStatus.Test))
                {
                    newIsLive = true;
                }
            }

            if (currentIsLive != newIsLive)
            {
                order.IsLive = newIsLive;
                Update(order);
            }
        }

        public void OrderCompleted(Order order, string orderHistoryDescription, int? userId, int CurrentUserId)
        {
            order.CompleteDate = DateTime.UtcNow;

            var validUserId = order.OrderMode == OrderMode.Online ? userId.HasValue ? userId.Value : CurrentUserId : CurrentUserId;

            _orderHistoryBusiness.AddOrderHistory(OrderAction.Initiated, String.Format(orderHistoryDescription, order.ConfirmationId), order.Id, validUserId);
        }

        public OperationStatus SeparateOrderItemFromCartOrder(Order order, long separatedOrderItemId)
        {
            var result = new OperationStatus();

            var ordersWhichRemainInCart = order.OrderItems.Where(i => i.Id != separatedOrderItemId).ToList();

            if (ordersWhichRemainInCart.Any() && !order.IsDraft)
            {
                ordersWhichRemainInCart.ForEach(i => i.Order_Id = 0);

                var newOrder = new Order();

                var clubid = GetOrderClubIdFromItems(ordersWhichRemainInCart);
                newOrder.ClubId = clubid.HasValue ? clubid.Value : order.ClubId;
                newOrder.IsAutoCharge = order.IsAutoCharge;
                newOrder.IsDraft = true;
                newOrder.IsEditedOrder = order.IsEditedOrder;
                newOrder.IsLive = order.IsLive;
                newOrder.OrderAmount = order.OrderAmount;
                newOrder.CompleteDate = order.CompleteDate;
                newOrder.OrderMode = order.OrderMode;
                newOrder.OrderStatus = OrderStatusCategories.notInitiated;
                newOrder.PaymentPlanType = order.PaymentPlanType;
                newOrder.UserId = order.UserId;
                newOrder.OrderItems = ordersWhichRemainInCart;

                result = Create(newOrder);

                return result;
            }

            return new OperationStatus() { Status = true };
        }


        public OperationStatus TransferPreregisteredAndLotteryItemsToNewOrder(Order order)
        {
            var result = new OperationStatus();

            var preregisterOrderItems = order.PreregisterItems;
            var lotteryOrderItems = order.LotteryItems;

            if (preregisterOrderItems != null && (preregisterOrderItems.Any() || lotteryOrderItems.Any()) && !order.IsDraft)
            {
                preregisterOrderItems.ToList().ForEach(i => i.Order_Id = 0);
                lotteryOrderItems.ToList().ForEach(i => i.Order_Id = 0);

                var allPreregisterAndLotteryItems = preregisterOrderItems.ToList();
                allPreregisterAndLotteryItems.AddRange(lotteryOrderItems);

                var newOrder = new Order();

                var clubid = GetOrderClubIdFromItems(allPreregisterAndLotteryItems);
                newOrder.ClubId = clubid.HasValue ? clubid.Value : order.ClubId;
                newOrder.IsAutoCharge = order.IsAutoCharge;
                newOrder.IsDraft = true;
                newOrder.IsEditedOrder = order.IsEditedOrder;
                newOrder.IsLive = order.IsLive;
                newOrder.OrderAmount = order.OrderAmount;
                newOrder.CompleteDate = order.CompleteDate;
                newOrder.OrderMode = order.OrderMode;
                newOrder.OrderStatus = OrderStatusCategories.notInitiated;
                newOrder.PaymentPlanType = order.PaymentPlanType;
                newOrder.UserId = order.UserId;
                newOrder.OrderItems = allPreregisterAndLotteryItems;

                result = Create(newOrder);
            }

            return result;
        }

        public int? GetOrderClubIdFromItems(List<OrderItem> orderItems)
        {
            var orderItemsNotDonation = orderItems.Where(i => i.ProgramScheduleId.HasValue);
            var clubId = orderItemsNotDonation.Any() ? orderItemsNotDonation.Last().ProgramSchedule.Program.ClubId : (int?)null;
            return clubId;
        }

        public bool IsTakePaymentOrderIsAutoCharge(Order order)
        {
            var result = false;

            if (order.IsAutoCharge)
            {
                if (order.RegularOrderItems.Any(o => o.Installments.Any(i => !i.EnableReminder)))
                {
                    result = true;
                }
            }

            return result;
        }

        public bool IsUserOwnsTheOrder(int currentUserId, int orderUserId, string clubDomain, string outSourceClubDomain)
        {
            return true;
            // We should fix this issue later, the PBI 2045 is about it.

            //var userId = JbUserService.GetCurrentUserId();

            if (currentUserId == orderUserId)
            {
                return true;
            }
            //if ((!string.IsNullOrEmpty(clubDomain) && JbUserService.IsUserAdminForClub(clubDomain, userId)) || (!string.IsNullOrEmpty(outSourceClubDomain) && JbUserService.IsUserAdminForClub(outSourceClubDomain, userId)))
            //{
            //    return true;
            //}
            //return false;

            return false;
        }

        public bool UserHasAnyOrderOnClub(int userId, int clubId)
        {
            return _orderRepository.GetList().Any(o => o.UserId == userId && o.ClubId == clubId);
        }

        public bool UserHasAnyOrderOnPartnerClub(int userId, int partnerId)
        {
            var memberIds = _clubRepository.GetList(c => !c.IsDeleted && c.PartnerId.HasValue && c.PartnerId == partnerId).Select(c => c.Id).ToList();
            return _orderRepository.GetList().Any(o => o.UserId == userId && memberIds.Contains(o.ClubId));
        }

        public string GenerateConfirmationId()
        {
            string s;
            string ss;
            string sss;

            lock (syncLock)
            {
                byte[] buffer = new byte[5];

                random.NextBytes(buffer);

                s = BitConverter.ToString(buffer, 0, 1) + BitConverter.ToString(buffer, 1, 1);
                ss = BitConverter.ToString(buffer, 2, 1);
                sss = BitConverter.ToString(buffer, 3, 1) + BitConverter.ToString(buffer, 4, 1);
            }

            var result = $"{s}-{ss}-{sss}";

            if (GetList().Any(o => o.ConfirmationId.Equals(result)))
            {
                result = GenerateConfirmationId();
            }

            return result;
        }

        public RefundResultModel Refund(OrderItemsViewModel model)
        {
            var refundedAmount = 0m;
            var result = model.IsFullPaid ? _orderItemBusiness.Refund(model, ref refundedAmount) : _orderInstallmentBusiness.Refund(model, ref refundedAmount);

            if (refundedAmount == 0) return result;

            var emailParameter = new RefundEmailParameterModel
            {
                OrderItemId = model.Id == 0 ? (long?)null : model.Id,
                OrderInstallmentId = model.IntallmentId == 0 ? (long?)null : model.IntallmentId,
                RefundedAmount = refundedAmount,
                RefundNote = model.CustomRefund.Name
            };

            _emailBusiness.Send(EmailCategory.RefundOrder, emailParameter);

            return result;
        }

    }
}
