
CREATE TABLE [dbo].[Families](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[InsuranceCompanyName] [nvarchar](128) NULL,
	[InsurancePolicyNumber] [nvarchar](128) NULL,
	[InsurancePhone] [nvarchar](64) NULL,
 CONSTRAINT [PK_dbo.Families] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Families]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Families_dbo.JbUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[JbUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Families] CHECK CONSTRAINT [FK_dbo.Families_dbo.JbUsers_UserId]
GO


