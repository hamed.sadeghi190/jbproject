﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Areas.Dashboard.Models;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Services
{
    public class OrderServices
    {
        private OrderServices()
        {

        }

        public static OrderServices Get()
        {
            return new OrderServices();
        }
        public OperationStatus ReceivedPaymentFromMultiOrder(List<OrderItem> orderItems, List<FamilyOrderItemsViewModel> selectedorderitems, decimal totalPaidAmount, PaymentDetail paymentDetail, string note, DateTime transactionDate, string checkId, List<OrderInstallment> selectedInstallments, List<TransactionActivity> transactions = null, HandleMode handleMode = HandleMode.Online, bool isEdit = false)
        {
            try
            {
                var orderDb = Ioc.OrderBusiness;
                List<TransactionActivity> orderItemtransactions = null;

                foreach (var orderitem in orderItems)
                {
                    if (transactions != null)
                    {
                        orderItemtransactions = transactions.Where(t => t.OrderItemId == orderitem.Id).ToList();
                        totalPaidAmount = orderItemtransactions.Sum(t => t.Amount);
                    }
                    else
                    {
                        if (selectedorderitems != null)
                        {
                            totalPaidAmount = selectedorderitems.Where(t => t.Id == orderitem.Id).Sum(t => t.InvoiceAmount);
                        }
                    }

                    var installmentToken = string.Empty;
                    var date = transactionDate;
                    var description = Constants.Order_FamilyMakePayment;

                    if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                    {
                        description = Common.Constants.Payment.OrderParentMakePayment;
                    }

                    var orderHistory = new OrderHistory
                    {
                        Action = (!isEdit) ? OrderAction.MadePayment : OrderAction.EditPayment,
                        ActionDate = DateTime.UtcNow,
                        Description =
                            (!isEdit)
                                ? string.Format(description,
                                    CurrencyHelper.FormatCurrencyWithPenny(totalPaidAmount, orderitem.Order.Club.Currency), note)
                                : null,
                        OrderId = orderitem.Order_Id,
                        OrderItemId = orderitem.Id, //orderItemId,
                        UserId = JbUserService.GetCurrentUserId()
                    };
                    orderitem.Order.OrderHistories.Add(orderHistory);
                    // new code for edit payments.

                    if (transactions != null)
                    {
                        orderitem.PaidAmount += totalPaidAmount;
                    }

                    if (orderItemtransactions != null && orderItemtransactions.Any())
                    {
                        orderItemtransactions.ForEach(t => t.TransactionStatus = TransactionStatus.Deleted);
                    }
                    if (string.IsNullOrEmpty(installmentToken))
                    {
                        installmentToken = Guid.NewGuid().ToString("N");
                    }
                    if (orderitem.PaymentPlanType == PaymentPlanType.Installment && orderitem.Installments.Any() && (orderitem.ItemStatusReason != OrderItemStatusReasons.canceled || JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent))
                    {
                        if (selectedInstallments == null || selectedInstallments.Count == 0)
                        {
                            selectedInstallments = orderitem.Installments.Where(o => o.PaidAmount.HasValue == false || o.PaidAmount.Value < o.Amount).ToList();
                        }
                        foreach (var inst in selectedInstallments.Where(t => t.OrderItemId == orderitem.Id).ToList())
                        {

                            var transactionAmount = inst.PaidAmount.HasValue ? inst.Balance : inst.Amount;

                            if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                            {
                                transactionAmount = totalPaidAmount;
                            }

                            if (transactions == null)
                            {
                                orderitem.PaidAmount += transactionAmount;
                            }
                            inst.PaidDate = date;

                            if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                            {
                                inst.PaidAmount = inst.PaidAmount.HasValue ? inst.PaidAmount.Value + totalPaidAmount : totalPaidAmount;
                            }
                            else
                            {
                                inst.PaidAmount = inst.PaidAmount.HasValue ? inst.PaidAmount.Value + inst.Balance : inst.Balance;
                            }

                            inst.Status = OrderStatusCategories.completed;
                            inst.Token = (string.IsNullOrEmpty(inst.Token)) ? installmentToken : inst.Token;
                            var token = transactions != null ? orderItemtransactions.FirstOrDefault().Token : string.Empty;
                            orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Success, null, date,
                                transactionAmount, orderitem, inst.Id, note, token, checkId,
                                TransactionCategory.TakePayment, handleMode);
                        }
                    }
                    else
                    {
                        if (totalPaidAmount > 0)
                        {
                            if (transactions == null)
                            {
                                orderitem.PaidAmount += totalPaidAmount;
                            }
                            var token = transactions != null ? orderItemtransactions.FirstOrDefault().Token : string.Empty;
                            orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Success, null, date,
                                totalPaidAmount, orderitem, null, note, token, checkId, TransactionCategory.TakePayment,
                                handleMode);
                        }
                    }
                }

                var result = Ioc.OrderItemBusiness.UpdateOrderItems(orderItems);

                return result;

            }
            catch (Exception e)
            {
                LogHelper.LogException(e);
                return new OperationStatus() { Status = false, Exception = e, ExceptionMessage = e.Message };
            }

        }


        public OperationStatus ReceivedPayment(long orderItemId, decimal totalPaidAmount, PaymentDetail paymentDetail, string note, DateTime transactionDate, string checkId, List<OrderInstallment> selectedInstallments, List<TransactionActivity> transactions = null, HandleMode handleMode = HandleMode.Online, bool isEdit = false)
        {
            try
            {
                var orderDb = Ioc.OrderBusiness;
                var orderitem = Ioc.OrderItemBusiness.GetItem(orderItemId);
                var installmentToken = string.Empty;
                var date = transactionDate;
                string description = Constants.Order_MakePayment;
                var parentToken = string.Empty;

                if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                {
                    description = Common.Constants.Payment.OrderParentMakePayment;
                    parentToken = transactions != null ? transactions.FirstOrDefault()?.Token : string.Empty;
                }

                var orderHistory = new OrderHistory
                {
                    Action = (!isEdit) ? OrderAction.MadePayment : OrderAction.EditPayment,
                    ActionDate = DateTime.UtcNow,
                    Description =
                        (!isEdit)
                            ? string.Format(description,
                                CurrencyHelper.FormatCurrencyWithPenny(totalPaidAmount, orderitem.Order.Club.Currency), note)
                            : null,
                    OrderId = orderitem.Order_Id,
                    OrderItemId = orderItemId,
                    UserId = JbUserService.GetCurrentUserId()
                };

                Ioc.OrderHistoryBusiness.Create(orderHistory);

                // new code for edit payments.
                if (isEdit && transactions != null)
                {


                    if (orderitem.PaymentPlanType == PaymentPlanType.Installment && orderitem.Installments.Any())
                    {
                        foreach (var transaction in transactions)
                        {
                            if (transaction.InstallmentId.HasValue)
                            {
                                var inst = orderitem.Installments.Single(i => i.Id == transaction.InstallmentId.Value);
                                inst.PaidAmount -= transaction.Amount;
                            }
                        }
                    }


                    orderitem.PaidAmount -= transactions.Sum(t => t.Amount);
                }


                orderitem.PaidAmount += totalPaidAmount;

                if (isEdit && transactions != null)
                {
                    orderHistory.Description = GenerateEditTransactionHistory(transactions, paymentDetail, selectedInstallments, date, totalPaidAmount, note, checkId, orderitem.Order.Club.Currency);
                }
                if (transactions != null && transactions.Any())
                {
                    transactions.ForEach(t => t.TransactionStatus = TransactionStatus.Deleted);
                }
                if (string.IsNullOrEmpty(installmentToken))
                {
                    installmentToken = Guid.NewGuid().ToString("N");
                }
                if (orderitem.PaymentPlanType == PaymentPlanType.Installment && orderitem.Installments.Any())
                {
                    if (selectedInstallments == null || selectedInstallments.Count == 0)
                    {
                        selectedInstallments = orderitem.Installments.Where(o => o.PaidAmount.HasValue == false || o.PaidAmount.Value < o.Amount).ToList();
                    }

                    foreach (var inst in selectedInstallments)
                    {
                        if (totalPaidAmount <= 0)
                        {
                            break;
                        }

                        var instBalance = inst.Amount - (inst.PaidAmount.HasValue ? inst.PaidAmount.Value : 0);

                        if (instBalance > totalPaidAmount)
                        {
                            instBalance = totalPaidAmount;
                        }

                        totalPaidAmount -= instBalance;

                        inst.PaidDate = date;

                        inst.PaidAmount = inst.PaidAmount.HasValue ? inst.PaidAmount.Value + instBalance : instBalance;

                        inst.Status = OrderStatusCategories.completed;
                        inst.Token = (string.IsNullOrEmpty(inst.Token)) ? installmentToken : inst.Token;

                        var token = transactions != null ? transactions.FirstOrDefault().Token : string.Empty;
                        orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Success, null, date,
                            instBalance, orderitem, inst.Id, note, token, checkId,
                            TransactionCategory.TakePayment, handleMode);

                    }

                }

                if (totalPaidAmount > 0)
                {
                    orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Success, null, date,
                        totalPaidAmount, orderitem, null, note, parentToken, checkId, TransactionCategory.TakePayment,
                        handleMode);

                }

                var result = Ioc.OrderItemBusiness.Update(orderitem);

                return result;

            }
            catch (Exception e)
            {
                LogHelper.LogException(e);
                return new OperationStatus() { Status = false, Exception = e, ExceptionMessage = e.Message };
            }

        }
        private string GenerateEditTransactionHistory(List<TransactionActivity> transactions, PaymentDetail paymentDetail, List<OrderInstallment> selectedInstallments, DateTime date, decimal totalPaidAmount, string note, string checkId, CurrencyCodes currency)
        {
            var fisrtTransactions = transactions.First();
            var descriptionDetail = string.Format("The transaction on {0} is changed as follows:",
                   fisrtTransactions.TransactionDate.ToString(Constants.DefaultDateFormatWith3CharMonth));
            var transactionInstallment = "";
            if (transactions.Any(c => c.InstallmentId.HasValue) && selectedInstallments != null && selectedInstallments.Any())
            {
                var newInstallments = selectedInstallments.Select(c => c.Id);
                var oldInstallment = transactions.Where(t => t.InstallmentId.HasValue).Select(t => t.InstallmentId.Value);
                var removedInstallments = oldInstallment.Except(newInstallments);
                var addedInstallments = newInstallments.Except(oldInstallment);

                if (removedInstallments != null && removedInstallments.Any())
                {
                    transactionInstallment = "Installments:";
                    foreach (var instid in removedInstallments)
                    {
                        transactionInstallment += string.Format(" {0} (removed)", transactions.Single(t => t.InstallmentId == instid).Installment.InstallmentDate.ToString(Constants.DefaultDateFormat));
                    }

                }
                if (addedInstallments != null && addedInstallments.Any())
                {
                    transactionInstallment = string.IsNullOrEmpty(transactionInstallment) ? "Installments:" : transactionInstallment;
                    foreach (var instid in addedInstallments)
                    {
                        transactionInstallment += string.Format(" {0} (added)", selectedInstallments.Single(t => t.Id == instid).InstallmentDate.ToString(Constants.DefaultDateFormat));
                    }

                }
            }
            var transactionAmount = (totalPaidAmount != transactions.Sum(c => c.Amount))
                ? string.Format("Amount: ${0} --> {1}", transactions.Sum(c => c.Amount),
                    CurrencyHelper.FormatCurrencyWithPenny(totalPaidAmount, currency))
                : null;

            var transactionNewDate = (fisrtTransactions.TransactionDate.Date != date.Date)
                ? string.Format("Date: {0} --> {1}",
                    fisrtTransactions.TransactionDate.ToString(Constants.DateTime_Comma),
                    date.ToString(Constants.DateTime_Comma))
                : null;

            var transactionPayementMethod = (fisrtTransactions.PaymentDetail.PaymentMethod !=
                                             paymentDetail.PaymentMethod)
                ? string.Format("Payment method: {0} --> {1}",
                    fisrtTransactions.PaymentDetail.PaymentMethod.ToDescription(),
                    paymentDetail.PaymentMethod.ToDescription())
                : null;

            var transactionId = (fisrtTransactions.CheckId != checkId)
                ? string.Format("Transaction/check ID: {0} --> {1}", fisrtTransactions.CheckId, checkId)
                : null;

            var transavtionMemo = (fisrtTransactions.Note != note)
                ? string.Format("Memo: {0} --> {1}", fisrtTransactions.Note, note)
                : null;

            if (!String.IsNullOrEmpty(transactionInstallment))
            {
                descriptionDetail += string.Format(" {0} ", transactionInstallment);
            }
            if (!String.IsNullOrEmpty(transactionAmount))
            {
                descriptionDetail += string.Format(" {0} ", transactionAmount);
            }
            if (!string.IsNullOrEmpty(transactionNewDate))
            {
                descriptionDetail += string.Format(" {0} ", transactionNewDate);
            }
            if (!string.IsNullOrEmpty(transactionPayementMethod))
            {
                descriptionDetail += string.Format(" {0} ", transactionPayementMethod);
            }
            if (!string.IsNullOrEmpty(transactionId))
            {
                descriptionDetail += string.Format(" {0} ", transactionId);
            }
            if (!string.IsNullOrEmpty(transavtionMemo))
            {
                descriptionDetail += string.Format(" {0} ", transavtionMemo);
            }

            return descriptionDetail;
        }

        public OperationStatus DeleteTransaction(long orderItemId, long transactionId)
        {
            var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId);
            var result = new OperationStatus
            {
                Status = false
            };
            var orderItemTransactions = Ioc.TransactionActivityBusiness.GetAllTransactionByOrderItemId(orderItemId);

            if (orderItemTransactions.Any(t => t.Id == transactionId))
            {
                var oldTransaction = orderItemTransactions.Single(t => t.Id == transactionId);
                orderItem.PaidAmount -= oldTransaction.Amount;
                if (orderItem.PaymentPlanType == PaymentPlanType.Installment && orderItem.Installments.Any())
                {
                    if (oldTransaction.InstallmentId.HasValue && orderItem.Installments.Any(i => i.Id == oldTransaction.InstallmentId.Value))
                    {
                        var installment =
                            orderItem.Installments.Single(i => i.Id == oldTransaction.InstallmentId.Value);

                        var paidAmount = oldTransaction.Amount;
                        installment.PaidAmount -= paidAmount;
                        if (installment.PaidAmount == 0)
                        {
                            installment.Status = OrderStatusCategories.initiated;
                            installment.PaidDate = (DateTime?)null;
                            installment.PaidAmount = (decimal?)null;
                        }
                    }
                }
                result = Ioc.OrderItemBusiness.Update(orderItem);
                var orderHistory = new OrderHistory
                {
                    Action = OrderAction.MadePayment,
                    ActionDate = DateTime.UtcNow,
                    Description = string.Format(Constants.Order_Delete_MakePayment, CurrencyHelper.FormatCurrencyWithPenny(oldTransaction.Amount, orderItem.Order.Club.Currency), oldTransaction.Note),
                    OrderId = orderItem.Order_Id,
                    OrderItemId = orderItemId,
                    UserId = JbUserService.GetCurrentUserId()
                };
                Ioc.OrderHistoryBusiness.Create(orderHistory);
                var resultTransaction = Ioc.TransactionActivityBusiness.DeleteTransaction(transactionId);
            }
            return result;
        }

    }
}