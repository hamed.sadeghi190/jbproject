﻿using Jb.Framework.Common.Forms;
using Microsoft.Ajax.Utilities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;


namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramReportViewModel : BaseViewModel<EventRoaster>
    {

        public ProgramReportViewModel()
        {
            JbForm = new JbForm();
            FollowupForms = new List<ProgramReportFollowupItemViewModel>();
        }

        public ProgramReportViewModel(int clubId, long seasonId, CustomReportType customType)
        {
            var reportBusiness = Ioc.ReportBusiness;

            JbForm = new JbForm();

            SeasonId = seasonId;
            CustomReportType = customType;
            
            switch (customType)
            {
                case CustomReportType.Admin:
                    
                    GenerateJbForm(clubId, seasonId);

                    var followupForms = reportBusiness.GetFolloupFormReports(clubId);

                    FollowupForms = new List<ProgramReportFollowupItemViewModel>();

                    foreach (var item in followupForms)
                    {
                        FollowupForms.Add(new ProgramReportFollowupItemViewModel()
                        {
                            Id = item.Id,
                            Title = item.Title,
                            JbForm = reportBusiness.GenerateReportFollowupForm(item)
                        });
                    }
                    break;

                case CustomReportType.Parent:
                    JbForm = Ioc.PlayerProfileBusiness.ParentReportSectionGenerator(JbForm, seasonId, clubId);
                    break;
            }



        }

        [Required(AllowEmptyStrings = false)]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool HasHeader { get; set; }
        public EventRoasterType ReportType { get; set; }
        public CustomReportType CustomReportType { get; set; }
        public long? SeasonId { get; set; }

        public JbForm JbForm { get; set; }

        public List<ProgramReportFollowupItemViewModel> FollowupForms { get; set; }

        private void GenerateJbForm(int clubId, long seasonId)
        {
            var newform = new JbForm();

            var jbForms =
                Ioc.ClubBusiness.GetFormTemplates(clubId, FormType.Registration).Select(m => m.JbForm).ToList();

            foreach (var form in jbForms)
            {
                try
                {
                    form.CopyByTitle(newform);
                }
                catch
                {

                }
            }

            var lastSection = new JbSection();

            var moreSection = Ioc.CustomReportBusiness.CreateMoreInfoSection();

            Ioc.CustomReportBusiness.SetAccessControllJbSection(moreSection, clubId);

            var allChargeInSeason = GetAllSeasonCharges(seasonId, clubId).ToList();
            if (allChargeInSeason.Any())
            {
                var charges = allChargeInSeason.Select(c => new JbCheckBox
                {
                    Name = "Charges",
                    Title = c.Name.Trim()
                })
                .ToList();

                moreSection.Elements.AddRange(charges);
            }

            var hasChessTourney = Ioc.ClubBusiness.Get(clubId).Client.HasChessTourney;

            foreach (var element in newform.Elements)
            {
                if (element is JbSection)
                {
                    lastSection = new JbSection { Name = element.Name, Title = element.Title };
                    JbForm.Elements.Add(lastSection);
                    foreach (var element2 in (element as JbSection).Elements.Where(e => !(e is JbHidden) && !(e is JbParagraph) && !(e is JbTextEditor)))
                    {
                        var checkbox = new JbCheckBox { ElementId = element2.ElementId, Name = element2.Name, Title = element2.Title };
                        lastSection.Elements.Add(checkbox);
                    }
                }
                else
                {
                    var checkbox = new JbCheckBox();
                    Jumbula.Common.Utilities.AutoMapper.Map(element, ref checkbox);
                    (lastSection as JbSection).Elements.Add(checkbox);
                }
            }

            JbForm.Elements.Add(Ioc.CustomReportBusiness.CreateDiscountCouponSection(seasonId, clubId));

            JbForm.Elements.Add(Ioc.CustomReportBusiness.CreateChessTournamentSection());

            JbForm.Elements.Add(moreSection);

            if (hasChessTourney)
            {
                JbForm.Elements.Add(Ioc.CustomReportBusiness.CreateChessTourneySection());
            }
        }

        private static IEnumerable<OrderChargeDiscount> GetAllSeasonCharges(long seasonId, int clubId)
        {
            var season = Ioc.SeasonBusiness.Get(seasonId);

            var allOrdersCharges =
                Ioc.OrderItemBusiness.GetOrderItems(clubId: clubId, seasonId: season.Id, isTestMode: season.Status == SeasonStatus.Test)
                    .SelectMany(oi => oi.OrderChargeDiscounts)
                    .Where(c => !c.IsDeleted && c.Category > 0 && c.Category != ChargeDiscountCategory.EntryFee)
                    .DistinctBy(c => new { c.Name, c.Category });
            return allOrdersCharges;
        }

    }

    public class ProgramReportFollowupItemViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool IsSelected { get; set; }

        public JbForm JbForm { get; set; }
    }
}