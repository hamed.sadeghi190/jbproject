﻿using Jumbula.Common.Enums;
using System;
using System.Collections.Generic;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ChessTourneyBodyViewModel : BaseViewModel<Program, long>, IProgramBodyViewModel
    {
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }

        public bool EnablePriceHidden { get; set; }
        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public PostalAddress Address { get; set; }

        public string LocationName { get; set; }

        public string ClubLogo { get; set; }

        public bool DisableCapacityRestriction { get; set; }

        public Jumbula.Common.Enums.TimeZone ClubTimeZone { get; set; }

        public TournamentType TournamentType { get; set; }

        public decimal? PrizeFund { get; set; }

        public int? FullEntries { get; set; }

        public string PrizeFundGuarantee { get; set; }

        public string Trophies { get; set; }

        public List<TournamentSection> Sections { get; set; }

        public List<TournamentSchedule> Schedules { get; set; }

        public int? MaxOfByes { get; set; }

        public int? LastRoundBye { get; set; }

        public int? NumberOfRounds { get; set; }
        public CurrencyCodes Currency { get; set; }
        public string ByeNote { get; set; }

        public ProgramBodySchedule Schedule { get; set; }

        public bool HideGoogleMapInClassPage { get; set; }
    }
}