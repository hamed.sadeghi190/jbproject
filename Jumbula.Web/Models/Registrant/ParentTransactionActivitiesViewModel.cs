﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System;
using System.Globalization;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
namespace SportsClub.Models
{
    public class ParentTransactionActivitiesViewModel : BaseViewModel<TransactionActivity, long>
    {
      
            public string Participant { get; set; }
            public string ConfirmationId { get; set; }
            public string SeasonDomain { get; set; }
            public DateTime TransactionDate { get; set; }

            public string StrTransactionDate
            {
                get
                {
                    return TransactionDate.ToString(Constants.DefaultDateFormat, new CultureInfo("en-US"));
                }
            }
            public decimal Amount { get; set; }
            public CurrencyCodes Currency { get; set; }
            public string StrAmount
            {
                get
                {

                    return (TransactionType == TransactionType.Refund) ? CurrencyHelper.FormatCurrencyWithPenny(-1 * Amount, Currency) : CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency);
                }
            }
            public string Payment
            {
                get
                {
                    return (TransactionType == TransactionType.Payment) ? CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency) : string.Empty;
                }

            }
            public string Refund
            {
                get
                {
                    return (TransactionType == TransactionType.Refund) ? CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency) : string.Empty;
                }

            }
            public TransactionType TransactionType { get; set; }
            public string Note { get; set; }
            public string CheckId { get; set; }
            public long? OrderId { get; set; }
            public long? OrderItemId { get; set; }
            public long PaymentDetailId { get; set; }

            public HandleMode HandleMode { get; set; }

            public string Token { get; set; }

            public long ProgramId { get; set; }

            public PaymentMethod PaymentMethod
            {
                get
                {
                    if (PaymentDetail != null)
                    {
                        return PaymentDetail.PaymentMethod;
                    }
                    return PaymentMethod.Cash;
                }
            }
            public string StrPaymentMethod
            {
                get
                {
                    if (this.TransactionStatus == TransactionStatus.Invoice)
                    {
                        return "-";
                    }
                    return PaymentMethod.ToDescription();
                }
            }
            public string TransactionId
            {
                get
                {
                    if (PaymentDetail != null && !string.IsNullOrEmpty(PaymentDetail.TransactionId))
                    {
                        return PaymentDetail.TransactionId;
                    }
                    return CheckId;
                }
            }
            public TransactionStatus TransactionStatus { get; set; }
            public string StrTransactionStatus
            {
                get
                {
                    return TransactionStatus.ToDescription();
                }

            }
            public PaymentDetailViewModel PaymentDetail { get; set; }

        }
    public class PaymentDetailViewModel : BaseViewModel<PaymentDetail, long>
    {

        public PaymentMethod PaymentMethod { get; set; }
        public string Currency { get; set; }

        public string TransactionId { get; set; }
    }
}
