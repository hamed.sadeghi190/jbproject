﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class PaymentPlanBusiness : IPaymentPlanBusiness
    {
        private readonly IProgramBusiness _programBusiness;
        private readonly IUserProfileBusiness _userProfileBusiness;
        private readonly IRepository<PaymentPlan, long> _paymentPlanRepository;

        public PaymentPlanBusiness(IProgramBusiness programBusiness, IUserProfileBusiness userProfileBusiness, IRepository<PaymentPlan, long> paymentPlanRepository)
        {
            _programBusiness = programBusiness;
            _userProfileBusiness = userProfileBusiness;
            _paymentPlanRepository = paymentPlanRepository;
        }

        public IQueryable<PaymentPlan> GetList(long seasonId)
        {
            return _paymentPlanRepository.GetList().Where(p => p.SeasonId == seasonId && p.IsDeleted != true);
        }
        public PaymentPlan Get(long PaymentPlanId)
        {
            return _paymentPlanRepository.Get(p => p.Id == PaymentPlanId && p.IsDeleted != true);
        }
        public OperationStatus Delete(long paymentPlanId)
        {
            var paymentPlan = _paymentPlanRepository.Get(d => d.Id == paymentPlanId);
            paymentPlan.IsDeleted = true;
            return Update(paymentPlan);
        }

        public List<PaymentPlan> GetPaymentPlansByProgramId(long programId, long seasonId)
        {
            List<PaymentPlan> paymentplans = new List<PaymentPlan>();
            var allProgramPaymentplan = _paymentPlanRepository.GetList().Where(p => p.SeasonId == seasonId && p.IsAllProgram == true && p.IsDeleted == false);
            if (allProgramPaymentplan != null)
            {
                paymentplans.AddRange(allProgramPaymentplan.ToList());
            }

            var program = _paymentPlanRepository.GetList().SelectMany(p => p.Programs).FirstOrDefault(p => p.Id == programId);
            if (program != null)
            {
                paymentplans.AddRange(program.PaymentPlans.Where(p => p.IsDeleted == false && p.SeasonId == program.SeasonId).ToList());
            }

            return paymentplans.Where(p => p.ExpiryDate == null || p.ExpiryDate.Value.Date.CompareTo(DateTime.UtcNow.Date) >= 0).ToList();

        }

        public IEnumerable<PaymentPlan> GetPaymentPlansByProgramIdForUser(long programId, long seasonId, int userId)
        {
            List<PaymentPlan> paymentplans = new List<PaymentPlan>();
            var allProgramPaymentplan = _paymentPlanRepository.GetList().Where(p => p.SeasonId == seasonId && p.IsAllProgram == true && p.IsDeleted == false);
            if (allProgramPaymentplan != null)
            {
                paymentplans.AddRange(allProgramPaymentplan.ToList());
            }

            var program = _paymentPlanRepository.GetList().SelectMany(p => p.Programs).FirstOrDefault(p => p.Id == programId);
            if (program != null)
            {
                paymentplans.AddRange(program.PaymentPlans.Where(p => p.IsDeleted == false && p.SeasonId == program.SeasonId).ToList());
            }

            return paymentplans.Where(p => (p.IsAllUser || p.Users.Any(c => c.UserId == userId)) && (p.ExpiryDate == null || p.ExpiryDate.Value.Date.CompareTo(DateTime.UtcNow.Date) >= 0));

        }

        public OperationStatus CopyInSeason(long paymentPlanId, long seasonId, int clubId, string newSeasonDomain, bool forwardDates, int? monthNum)
        {
            var dbPaymentPlan =
                _paymentPlanRepository.GetList()
                    .AsNoTracking()
                    .SingleOrDefault(p => p.Id == paymentPlanId && !p.IsDeleted);

            dbPaymentPlan.SeasonId = seasonId;
            dbPaymentPlan.Season = null;

            if (forwardDates && monthNum.HasValue)
            {
                var installmentsDueDate = string.Empty;

                foreach (var dueDate in dbPaymentPlan.ListOfInstallmentDates)
                {
                    var date = DateTime.Parse(dueDate);
                    date = date.AddMonths(monthNum.Value);
                    installmentsDueDate += string.Format("{0},", date.ToString("dd MMM yyyy"));
                }
                installmentsDueDate = installmentsDueDate.Remove(installmentsDueDate.Length - 1, 1);
                dbPaymentPlan.InstallmentDueDates = installmentsDueDate;
            }

            if (!dbPaymentPlan.IsAllProgram)
            {
                var programs = dbPaymentPlan.Programs.Select(p => p.Domain).ToList();
                dbPaymentPlan.Programs.Clear();

                foreach (var newProgram in programs.Select(program => _programBusiness.Get(clubId, newSeasonDomain, program)).Where(newProgram => newProgram != null))
                {
                    dbPaymentPlan.Programs.Add(newProgram);
                }
            }

            if (!dbPaymentPlan.IsAllUser)
            {
                var users = dbPaymentPlan.Users.Select(u => u.UserId).ToList();
                dbPaymentPlan.Users.Clear();

                foreach (var user in users)
                {
                    var dbUser = _userProfileBusiness.Get(user);
                    dbPaymentPlan.Users.Add(new UserPaymentPlan() { UserId = user, PaymentPlanId = dbPaymentPlan.Id });
                }
            }

            return _paymentPlanRepository.Create(dbPaymentPlan);
        }

        public OperationStatus Create(PaymentPlan paymentPlan)
        {
            return _paymentPlanRepository.Create(paymentPlan);
        }

        public OperationStatus Update(PaymentPlan paymentPlan)
        {
            return _paymentPlanRepository.Update(paymentPlan);
        }
    }
}
