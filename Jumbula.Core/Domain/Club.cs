﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Jumbula.Core.Domain
{
    /// <summary>
    /// <remarks>
    /// Must create a unique index for Domain
    /// </remarks>
    /// </summary>
    public class Club : BaseEntity, IProtectedResource
    {
        private string _domain;

        private ClubSetting _setting;

        [Required]
        [StringLength(128)]
        public string Domain
        {
            get
            {
                return !string.IsNullOrEmpty(_domain) ? _domain.ToLower() : string.Empty;
            }
            set
            {
                _domain = value;
            }
        }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        public int TypeId { get; set; }
        [ForeignKey("TypeId")]
        public virtual ClubType ClubType { get; set; }

        public string Description { get; set; }

        [StringLength(128)]
        public string Logo { get; set; }

        [StringLength(256)]
        public string Site { get; set; }

        public bool IsDeleted { get; set; }

        public int? CategoryId { get; set; }
        public int PartnerCommisionRate { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        [Required]
        public MetaData MetaData { get; set; }
        public Jumbula.Common.Enums.TimeZone TimeZone { get; set; }

        public bool IsNonProfit { get; set; }
        public CurrencyCodes Currency { get; set; }

        [Column("Setting")]
        public string SettingSerialized { get; set; }

        [NotMapped]
        public DateTime? ExpirationDate
        {
            get
            {
                if (Setting == null)
                    return null;
                return Setting.AgreementExpirationDate;
            }
        }

        [NotMapped]
        public bool IsSettingChanged { get; set; }
        [NotMapped]
        public ClubSetting Setting
        {
            get
            {
                if (_setting == null)
                {

                    if (!string.IsNullOrEmpty(SettingSerialized))
                    {
                        if (IsSchool)
                        {
                            _setting = JsonConvert.DeserializeObject<SchoolSetting>(SettingSerialized);
                        }
                        else if (IsPartner)
                        {
                            _setting = JsonConvert.DeserializeObject<PartnerSetting>(SettingSerialized);
                        }
                        else
                        {
                            _setting = JsonConvert.DeserializeObject<ClubSetting>(SettingSerialized);
                        }

                    }
                    else
                    {
                        if (IsSchool)
                        {
                            _setting = new SchoolSetting();
                        }
                        else if (IsPartner)
                        {
                            _setting = new PartnerSetting();
                        }
                        {
                            _setting = new ClubSetting();
                        }
                    }
                }

                return _setting;
            }

            set
            {
                _setting = value;
            }
        }
        public virtual PostalAddress Address { get; set; } // address is a class
        public long? ClientId { get; set; }
        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }

        [ForeignKey("District")]
        public int? DistrictId { get; set; }
        public virtual Club District { get; set; }
        public bool IsInActive { get; set; }

        public int? ClubRevenueId { get; set; }

        public ContactPerson PrimaryContact
        {
            get => ContactPersons.First(c => c.IsPrimary);
        }

        [ForeignKey("ClubRevenueId")]
        public virtual ClubRevenue ClubRevenue { get; set; }
        public virtual ICollection<ContactPerson> ContactPersons { get; set; } // contact person is a class
        public virtual ICollection<EventRoaster> EventRoasters { get; set; }
        public virtual ICollection<ClubLocation> ClubLocations { get; set; }

        public virtual ICollection<ClubWaiver> ClubWaivers { get; set; }
        public virtual ICollection<Subsidy> Subsidies { get; set; }

        public virtual ICollection<JbESignature> PartnerSignatures { get; set; }

        public virtual ICollection<JbESignature> MemberSignatures { get; set; }
        public virtual ICollection<Charge> Charges { get; set; }

        public virtual ICollection<ClubUser> Users { get; set; }
        public virtual ICollection<Campaign> EmailCampaigns { get; set; }

        public virtual ICollection<EmailTemplate> Templates { get; set; }

        public virtual ICollection<JbPage> Pages { get; set; }

        public virtual ICollection<MailList> EmailLists { get; set; }
        [ForeignKey("PartnerClub")]
        public int? PartnerId { get; set; }
        public virtual Club PartnerClub { get; set; }
        public virtual ICollection<Club> RelatedClubs { get; set; }

        public virtual ICollection<Season> Seasons { get; set; } // news is a class

        //public virtual ICollection<> Seasons { get; set; } // news is a class
        public virtual ICollection<TransactionActivity> TransactionActivities { get; set; }

        public virtual ICollection<ClubStaff> ClubStaffs { get; set; }
        public virtual ICollection<ClubSubsidy> DistrictSubsidies { get; set; }
        public virtual ICollection<ClubFormTemplate> ClubFormTemplates { get; set; }

        public virtual CatalogSetting CatalogSetting { get; set; }

        public virtual ICollection<CatalogItem> CatalogItems { get; set; }

        public virtual ICollection<JbAttribute> Attributes { get; set; }

        public virtual ICollection<ClubAttributeValue> ClubAttributeValues { get; set; }

        [NotMapped]
        public bool IsSchool
        {
            get
            {
                if (ClubType != null && ((ClubType.EnumType == ClubTypesEnum.School) || (ClubType.ParentType != null && ClubType.ParentType.EnumType == ClubTypesEnum.School)))
                {
                    return true;
                }
                return false;
            }
        }
        [NotMapped]
        public bool IsProvider
        {
            get
            {

                if (ClubType != null && ((ClubType.EnumType == ClubTypesEnum.Provider) || (ClubType.ParentType != null && ClubType.ParentType.EnumType == ClubTypesEnum.Provider)))
                {
                    return true;
                }
                return false;
            }
        }

        [NotMapped]
        public bool IsPartner
        {
            get
            {

                if (ClubType != null && ((ClubType.EnumType == ClubTypesEnum.Partner)))
                {
                    return true;
                }
                return false;
            }
        }
        public Club()
        {
            IsNonProfit = false;

            ContactPersons = new HashSet<ContactPerson>();

            ClubLocations = new HashSet<ClubLocation>();
            Charges = new HashSet<Charge>();

            TimeZone =Jumbula.Common.Enums.TimeZone.PST;
            Currency = CurrencyCodes.USD;
            Site = string.Empty;
        }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if(PartnerId.HasValue && PartnerClub.Id == clubId)
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

    public class ReservedDomain
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }
    }
}
