﻿using FluentAssertions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Enums;
using System;
using Jumbula.Core.Domain.Generic;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class ProgramSessionBusinessTest
    {
        private Mock<IClubBusiness> _mockClubBusiness;
        private Mock<IProgramBusiness> _mockProgramBusiness;
        private Mock<IOrderSessionBusiness> _mockOrderSessionBusiness;
        private Mock<ISubscriptionBusiness> _mockSubscriptionBusiness;
        private Mock<IOrderItemBusiness> _mockOrderItemBusiness;
        private Mock<IRepository<ProgramSession>> _mockProgramSessionRepository;
        private Mock<IEntitiesContext> _mockDataContext;
        private Mock<IOrderHistoryBusiness> _mockOrderHistoryBusiness;
        private Mock<ITransactionActivityBusiness> _mockTransactionActivityBusiness;

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsClassAndSessionNotEmpty_ShouldReturnFirstJsonSession()
        {
            //Arrange
            var service = CreateService();
            var programSchedules = CreateProgramSchedule();
            var programScheduleMode = TimeOfClassFormation.Both;
            var classSession = CreateClassSessionsDateTime();
            var classSessionWithOutHoliday = classSession;
            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 1;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.Class;
                p.Order.CompleteDate = DateTime.UtcNow;
            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);
            _mockProgramBusiness.Setup(x => x.GetClassDates(programSchedules.First(), programScheduleMode)).Returns(classSession);
            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);

            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(classSessionWithOutHoliday.OrderBy(p => p.StartTime).First().StartTime);
        }

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsSeminarTourOrChessTournamentAndSessionNotEmptyAndNotOrderCompleteDateMoreThanAnySession_ShouldReturnFirstProgramSchedules()
        {
            //Arrange
            var service = CreateService();
            var programSchedules = CreateProgramSchedule();
            var programScheduleMode = TimeOfClassFormation.Both;
            var classSession = CreateClassSessionsDateTime();
            var classSessionWithOutHoliday = classSession;
            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 1;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.SeminarTour;
                p.Order.CompleteDate = DateTime.UtcNow.AddDays(-1);
            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);
            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);

            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(classSessionWithOutHoliday.OrderBy(p => p.StartTime).First().StartTime);
        }

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsSeminarTourOrChessTournamentAndSessionNotEmptyAndIsOrderCompleteDateMoreThanManySession_ShouldReturnOrderCompleteDate()
        {
            //Arrange
            var service = CreateService();
            var programSchedules = CreateProgramSchedule();
            var programScheduleMode = TimeOfClassFormation.Both;
            var classSessionSameOrderComplete = new ClassSessionsDateTime
            {
                StartTime = DateTime.UtcNow.AddDays(1),
                EndTime = DateTime.UtcNow.AddDays(1),
                SessionDate = DateTime.UtcNow.AddDays(1),
            };
            var classSessionWithOutHoliday = new List<ClassSessionsDateTime>() { classSessionSameOrderComplete };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 1;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.SeminarTour;
                p.Order.CompleteDate = DateTime.UtcNow.AddDays(1);
            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);
            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);

            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(orderItem.Order.CompleteDate);
        }

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsSubscriptionAndSessionNotEmpty_ShouldReturnFirstOrderItemSession()
        {
            //Arrange
            var service = CreateService();
            var programSchedules = CreateProgramSchedule();
            var programScheduleMode = TimeOfClassFormation.Both;
            var classSession = CreateClassSessionsDateTime();
            var orderSession = CreateOrderSession();
            var classSessionWithOutHoliday = classSession;

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 1;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.Subscription;
                p.Order.CompleteDate = DateTime.UtcNow.AddDays(1);

            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);
            _mockOrderSessionBusiness.Setup(x => x.GetOrderItemSessions(orderItem.Id)).Returns(orderSession.AsQueryable());

            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);

            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(DateTime.UtcNow);
        }

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsSubscriptionAndSessionIsEmptyAndOrderItemPaidAmountIsMoreThanZeroAndTransactionSuccessIsAnyAndLastTransactionSuccessIsNotNull_ShouldReturnLastSuccessTransactionDate()
        {
            //Arrange
            var service = CreateService();

            var classSessionSameLastSuccessTransaction = new ClassSessionsDateTime
            {
                StartTime = DateTime.UtcNow.AddDays(2),
                EndTime = DateTime.UtcNow.AddDays(2),
                SessionDate = DateTime.UtcNow.AddDays(2),
            };
            var classSessionWithOutHoliday = new List<ClassSessionsDateTime>() { classSessionSameLastSuccessTransaction };

            var programSchedules = CreateProgramSchedule();
            var programScheduleMode = TimeOfClassFormation.Both;
            var listTransactions = CreateTransactions();
            var orderSession = CreateOrderSession();


            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 20;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.Subscription;
                p.Order.CompleteDate = DateTime.UtcNow.AddDays(1);
                p.PaidAmount = 200;
            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);
            _mockOrderSessionBusiness.Setup(x => x.GetList()).Returns(orderSession.AsQueryable());
            _mockTransactionActivityBusiness.Setup(x => x.GetAllsucceedTransactionByOrderItemId(orderItem.Id)).Returns(listTransactions.AsQueryable());
            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);

            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(classSessionWithOutHoliday.OrderBy(p => p.StartTime).First().StartTime);
        }

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsSubscriptionAndSessionIsEmptyAndOrderItemPaidAmountIsMoreThanZeroAndTransactionSuccessIsNotAnyAndLastTransactionSuccessIsNull_ShouldReturnFirstProgramSchedule()
        {
            //Arrange
            var service = CreateService();
            var programSchedules = CreateProgramSchedule();

            var classSessionSameProgramSchedule = new ClassSessionsDateTime
            {
                StartTime = programSchedules.OrderBy(p => p.StartDate).First().StartDate,
                EndTime = programSchedules.OrderBy(p => p.StartDate).First().EndDate,
                SessionDate = programSchedules.OrderBy(p => p.StartDate).First().StartDate,
            };
            var classSessionWithOutHoliday = new List<ClassSessionsDateTime> { classSessionSameProgramSchedule };


            var programScheduleMode = TimeOfClassFormation.Both;
            var orderSession =new List<OrderSession>();
            var programSession = CreateProgramSession();

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 20;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.Subscription;
                p.Order.CompleteDate = DateTime.UtcNow.AddDays(1);
                p.PaidAmount = 200;
            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);
            _mockOrderSessionBusiness.Setup(x => x.GetOrderItemSessions(orderItem.Id)).Returns(orderSession.AsQueryable());
            _mockTransactionActivityBusiness.Setup(x => x.GetAllsucceedTransactionByOrderItemId(orderItem.Id)).Returns(new List<TransactionActivity>().AsQueryable());
            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);

            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(programSession.OrderBy(p => p.StartDateTime).First().StartDateTime);
        }

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsSubscriptionAndSessionIsEmptyAndOrderItemPaidAmountIsEqualZeroAndCancelHistoryIsNotEmpty_ShouldReturnLastCancelHistory()
        {
            //Arrange
            var service = CreateService();
            var programSchedules = CreateProgramSchedule();
            var programScheduleMode = TimeOfClassFormation.Both;
            var orderHistories = CreateOrderHistory();
            var orderSession = CreateOrderSession();

            var classSessionSameCancelHistory = new ClassSessionsDateTime
            {
                StartTime = orderHistories.OrderBy(p => p.ActionDate).Last().ActionDate,
                EndTime = orderHistories.OrderBy(p => p.ActionDate).Last().ActionDate,
                SessionDate = orderHistories.OrderBy(p => p.ActionDate).Last().ActionDate,
            };

            var classSessionWithOutHoliday = new List<ClassSessionsDateTime>() { classSessionSameCancelHistory };


            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 20;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.Subscription;
                p.Order.CompleteDate = DateTime.UtcNow.AddDays(1);
                p.PaidAmount = 0;
            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);
            _mockOrderSessionBusiness.Setup(x => x.GetList()).Returns(orderSession.AsQueryable());
            _mockOrderHistoryBusiness.Setup(x => x.GetCancelHistory(orderItem.Order_Id, orderItem.Id)).Returns(orderHistories.AsQueryable());
            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);


            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(classSessionWithOutHoliday.OrderBy(s => s.StartTime).First().StartTime);
        }

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsSubscriptionAndSessionIsEmptyAndOrderItemPaidAmountIsEqualZeroAndCancelHistoryIsEmpty_ShouldReturnFirstProgramSchedule()
        {
            //Arrange
            var service = CreateService();
            var programSchedules = CreateProgramSchedule();
            var programScheduleMode = TimeOfClassFormation.Both;
            var orderSession = CreateOrderSession();
            var programSession = CreateProgramSession();

            var classSessionSameProgramSchedule = new ClassSessionsDateTime
            {
                StartTime = programSchedules.OrderBy(p => p.StartDate).First().StartDate,
                EndTime = programSchedules.OrderBy(p => p.StartDate).First().EndDate,
                SessionDate = programSchedules.OrderBy(p => p.StartDate).First().StartDate,
            };
            var classSessionWithOutHoliday = new List<ClassSessionsDateTime>() { classSessionSameProgramSchedule };


            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 20;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.Subscription;
                p.Order.CompleteDate = DateTime.UtcNow.AddDays(1);
                p.PaidAmount = 0;
            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);
            _mockOrderHistoryBusiness.Setup(x => x.GetCancelHistory(orderItem.Order_Id, orderItem.Id)).Returns(new List<OrderHistory>().AsQueryable());
            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);
            _mockOrderSessionBusiness.Setup(x => x.GetList()).Returns(orderSession.AsQueryable());

            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(programSession.OrderBy(p => p.StartDateTime).First().StartDateTime);
        }

        [TestMethod]
        public void GetFirstSession_ProgramTypeCategoryIsBeforeAfterAndOrderItemIsPunchCardAndSessionNotEmpty_ShouldReturnFirstProgramSession()
        {
            //Arrange
            var service = CreateService();
            var programSchedules = CreateProgramSchedule();
            var programScheduleMode = TimeOfClassFormation.Both;
            var orderSession = CreateOrderSession();
            var programSession = CreateProgramSession();

            var classSessionSameProgramSchedule = new ClassSessionsDateTime
            {
                StartTime = programSchedules.OrderBy(p => p.StartDate).First().StartDate,
                EndTime = programSchedules.OrderBy(p => p.StartDate).First().EndDate,
                SessionDate = programSchedules.OrderBy(p => p.StartDate).First().StartDate,
            };
            var classSessionWithOutHoliday = new List<ClassSessionsDateTime>() { classSessionSameProgramSchedule };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 1;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.Id = 1;
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramSchedule.Program.Club = FactoryGirl.Build<Club>();
                p.ProgramSchedule.Program.ProgramSchedules = programSchedules;
                p.ProgramSchedule.Program.TypeCategory = ProgramTypeCategory.BeforeAfterCare;
                p.Mode = OrderItemMode.PunchCard;
                p.Order.CompleteDate = DateTime.UtcNow.AddDays(1);

            });

            _mockProgramBusiness.Setup(x => x.GetClassScheduleTime(orderItem.ProgramSchedule.Program)).Returns(programScheduleMode);

            _mockProgramBusiness.Setup(x => x.ApplyHolidaysToClassDates(orderItem.ProgramSchedule.Program.Club, It.IsAny<List<ClassSessionsDateTime>>(), TimeOfClassFormation.None)).Returns(classSessionWithOutHoliday);
            _mockOrderSessionBusiness.Setup(x => x.GetList()).Returns(orderSession.AsQueryable());

            //Act
            var result = service.GetFirstSession(orderItem);

            //Assert
            result.StartTime.Should().BeSameDateAs(programSchedules.OrderBy(p => p.StartDate).First().StartDate);
        }

        #region Private Methods

        private ProgramSessionBusiness CreateService()
        {
            _mockClubBusiness = new Mock<IClubBusiness>();
            _mockProgramBusiness = new Mock<IProgramBusiness>();
            _mockOrderSessionBusiness = new Mock<IOrderSessionBusiness>();
            _mockSubscriptionBusiness = new Mock<ISubscriptionBusiness>();
            _mockOrderItemBusiness = new Mock<IOrderItemBusiness>();
            _mockProgramSessionRepository = new Mock<IRepository<ProgramSession>>();
            _mockDataContext = new Mock<IEntitiesContext>();
            _mockOrderHistoryBusiness = new Mock<IOrderHistoryBusiness>();
            _mockTransactionActivityBusiness = new Mock<ITransactionActivityBusiness>();

            return new ProgramSessionBusiness(_mockClubBusiness.Object,
                _mockProgramBusiness.Object,
                _mockOrderSessionBusiness.Object,
                _mockSubscriptionBusiness.Object,
                _mockOrderItemBusiness.Object,
                _mockProgramSessionRepository.Object,
                _mockDataContext.Object,
                _mockOrderHistoryBusiness.Object,
                _mockTransactionActivityBusiness.Object);
        }

        private List<OrderHistory> CreateOrderHistory()
        {
            var orderHistory1 = FactoryGirl.Build<OrderHistory>(p =>
            {
                p.Id = 1;
                p.ActionDate = DateTime.UtcNow;
                p.Action = OrderAction.Canceled;
            });

            return new List<OrderHistory> { orderHistory1 };

        }

        private List<ClassSessionsDateTime> CreateClassSessionsDateTime()
        {
            var classSessionsDateTime1 = FactoryGirl.Build<ClassSessionsDateTime>(p =>
            {
                p.Id = 1;
                p.SessionDate = DateTime.UtcNow;
                p.StartTime = DateTime.UtcNow;
                p.EndTime = DateTime.UtcNow.AddDays(30);
            });

            var classSessionsDateTime2 = FactoryGirl.Build<ClassSessionsDateTime>(p =>
            {
                p.Id = 2;
                p.SessionDate = DateTime.UtcNow.AddDays(1);
                p.StartTime = DateTime.UtcNow.AddDays(1);
                p.EndTime = DateTime.UtcNow.AddDays(30).AddHours(1);
            });

            return new List<ClassSessionsDateTime> { classSessionsDateTime1, classSessionsDateTime2 };
        }

        private List<ProgramSchedule> CreateProgramSchedule()
        {
            var programSchedule1 = FactoryGirl.Build<ProgramSchedule>(p =>
            {
                p.Id = 1;
                p.IsDeleted = false;
                p.StartDate = DateTime.UtcNow;
                p.EndDate = DateTime.UtcNow.AddDays(30);
                p.ProgramSessions = CreateProgramSession();
            });

            var programSchedule2 = FactoryGirl.Build<ProgramSchedule>(p =>
            {
                p.Id = 2;
                p.IsDeleted = false;
                p.StartDate = DateTime.UtcNow.AddDays(1);
                p.EndDate = DateTime.UtcNow.AddDays(30);
                p.ProgramSessions = CreateProgramSession();
            });

            var programSchedule3 = FactoryGirl.Build<ProgramSchedule>(p =>
            {
                p.Id = 3;
                p.IsDeleted = false;
                p.StartDate = DateTime.UtcNow.AddDays(2);
                p.EndDate = DateTime.UtcNow.AddDays(30);
                p.ProgramSessions = CreateProgramSession();
            });

            var programSchedule4 = FactoryGirl.Build<ProgramSchedule>(p =>
            {
                p.Id = 4;
                p.IsDeleted = false;
                p.StartDate = DateTime.UtcNow.AddDays(3);
                p.EndDate = DateTime.UtcNow.AddDays(30);
                p.ProgramSessions = CreateProgramSession();
            });

            var programSchedule5 = FactoryGirl.Build<ProgramSchedule>(p =>
            {
                p.Id = 5;
                p.IsDeleted = true;
                p.StartDate = DateTime.UtcNow;
                p.EndDate = DateTime.UtcNow.AddDays(30);
                p.ProgramSessions = CreateProgramSession();
            });

            return new List<ProgramSchedule> { programSchedule1, programSchedule2, programSchedule3, programSchedule4, programSchedule5 };
        }

        private List<TransactionActivity> CreateTransactions()
        {
            var transaction1 = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.Id = 1;
                p.TransactionDate = DateTime.UtcNow.AddDays(1);
                p.TransactionStatus = TransactionStatus.Success;
                p.Amount = 200;
            });

            var transaction2 = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.Id = 1;
                p.TransactionDate = DateTime.UtcNow.AddDays(2);
                p.TransactionStatus = TransactionStatus.Success;
                p.Amount = 100;
            });

            return new List<TransactionActivity> { transaction1, transaction2 };
        }

        private List<OrderSession> CreateOrderSession()
        {
            var programSession1 = FactoryGirl.Build<ProgramSession>(p =>
            {
                p.Id = 1;
                p.StartDateTime = DateTime.UtcNow;
                p.EndDateTime = DateTime.UtcNow;
            });

            var orderSession1 = FactoryGirl.Build<OrderSession>(p =>
            {
                p.Id = 1;
                p.OrderItemId = 1;
                p.OrderItem = FactoryGirl.Build<OrderItem>();
                p.OrderItem.Id = 1;
                p.ProgramSession = FactoryGirl.Build<ProgramSession>();
                p.ProgramSession = programSession1;
            });

            var orderSession2 = FactoryGirl.Build<OrderSession>(p =>
            {
                p.Id = 1;
                p.OrderItemId = 11;
                p.OrderItem = FactoryGirl.Build<OrderItem>();
                p.OrderItem.Id = 11;
                p.ProgramSession = FactoryGirl.Build<ProgramSession>();
                p.ProgramSession = programSession1;
            });

            return new List<OrderSession> { orderSession1, orderSession2 };
        }

        private List<ProgramSession> CreateProgramSession()
        {
            var programSession1 = FactoryGirl.Build<ProgramSession>(p =>
            {
                p.Id = 1;
                p.StartDateTime = DateTime.UtcNow;
                p.EndDateTime = DateTime.UtcNow;
            });

            var programSession2 = FactoryGirl.Build<ProgramSession>(p =>
            {
                p.Id = 1;
                p.StartDateTime = DateTime.UtcNow;
                p.EndDateTime = DateTime.UtcNow;
            });

            return new List<ProgramSession> { programSession1, programSession2 };
        }

        private List<ScheduleSession> CreateScheduleSession()
        {
            var scheduleSession1 = FactoryGirl.Build<ScheduleSession>(p =>
            {
                p.Id = 1;
                p.Start = DateTime.UtcNow.AddDays(12);
                p.End = DateTime.UtcNow.AddDays(12);
            });

            var scheduleSession2 = FactoryGirl.Build<ScheduleSession>(p =>
            {
                p.Id = 1;
                p.Start = DateTime.UtcNow.AddDays(13);
                p.End = DateTime.UtcNow.AddDays(13);
            });

            return new List<ScheduleSession> { scheduleSession1, scheduleSession2 };
        }

        #endregion
    }
}
