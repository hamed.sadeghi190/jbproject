﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('subscriptionInternalReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.Model = customReportService.getModelFromStrorage();

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.InfoModel = {};


        $http.get(baseUrl + '/GetSubscriptionInternalReportDetail', { params: { programIds: $scope.Model._SelectedPrograms, startDate: $scope.Model.DateProgram } }).success(function (data, status, headers, config) {

                $scope.InfoModel = data;
               
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
     
        $scope.saveToPDF = function () {
            
                $http({
                    url: baseUrl + '/GenerateInernalReportPdf',
                    method: "POST",
                    data: 
                       {
                            programIds: $scope.Model._SelectedPrograms,
                            startDate: $scope.Model.DateProgram,
                        
                    }, 
                    headers: {
                        'Content-type': 'application/json'
                    },
                    responseType: 'arraybuffer'
                }).success(function (data, status, headers, config) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/pdf" });

                    var fileURL = URL.createObjectURL(blob);
                    a.href = fileURL;
                    a.target = '_self';

                    a.download = "Internal roster.pdf";
                    a.click();
                });
        }
    }
    ]);

})();