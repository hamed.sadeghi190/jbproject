﻿using Jumbula.Common.Enums;
using System;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class SeminarProgramBodyViewModel : BaseViewModel<Program, long>, IProgramBodyViewModel
    {
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }
        public bool EnablePriceHidden { get; set; }
        public string Domain { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public PostalAddress Address { get; set; }

        public string LocationName { get; set; }

        public bool DisableCapacityRestriction { get; set; }

        public string ClubLogo { get; set; }

        public Jumbula.Common.Enums.TimeZone  ClubTimeZone { get; set; }
        public CurrencyCodes Currency { get; set; }
        public ProgramBodySchedule Schedule { get; set; }

        public bool HideGoogleMapInClassPage { get; set; }
    }
}