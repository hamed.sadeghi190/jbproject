﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IPricePlanBusiness
    {
        PricePlan GetbyType(PricePlanType planType);
        IQueryable<PricePlan> GetList();
        PricePlan Get(int pricePlanId);
        OperationStatus Create(PricePlan client);
        OperationStatus Update(PricePlan client);
        OperationStatus Delete(int pricePlanId);
        IQueryable<ClubRevenue> GetAllRevenues();
        PricePlan GetByRevenueIdAndType(int revenueId, PricePlanType planType, decimal revenuePrice);
        PricePlan GetFreeTrialPlan();
        PricePlan GetPayAsYouGoPlan();
        PricePlan GetSubscriptionPlan(int revenueId);
        SelectList GetClubRevenueList();
    }
}
