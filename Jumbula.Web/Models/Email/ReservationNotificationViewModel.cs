﻿using System.Collections.Generic;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Models.Email
{
    public class ReservationNotificationViewModel
    {
        public ReservationNotificationViewModel()
        {
            CardNumber = string.Empty;
            Location = string.Empty;
            ReservationItems = new List<ReservationItem>();
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
        }
        public string PaymentInstruction { get; set; }

        public string FollowUpForms { get; set; }

        public string CardNumber { get; set; }

        public string CardType { get; set; }

        public string ClubName { get; set; }
        public CurrencyCodes Currency { get; set; }
        public string ClubImageUrl { get; set; }
        public string OrderPaidAmount { get; set; }

        public decimal OrderTotalAmount { get; set; }

        public string ClubUrl { get; set; }
        
        public string RootSiteUrl { get; set; }

        public string BodyMailTitle { get; set; }

        public string InstallmentsBody { get; set; }
        
        public string OrderConfirmationId { get; set; }
        public bool OrderTestLive { get; set; }
        public string OrderPaidDate { get; set; }

        public List<ReservationItem> ReservationItems { get; set; }
        public string ClubEmail { get; set; }

        public string ClubPhone { get; set; }

        public string AppliedText { get; set; }

        public string PayPalInfo { get; set; }

        public string PaidAmount { get; set; }
        public string Location { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
        public ReservationEmailMode EmailMode{ get; set; }
        public OrderMode OrderMode { get; set; }
        public string OrderUserName { get; set; }
        public string PaymentMethodMessage { get;  set; }
    }

    public class ReservationItem
    {
        public ReservationItem()
        { }
        public ReservationItem(CurrencyCodes currency)
        {
            Currency = currency;
        }
        public string PlayerFullName { get; set; }
        public string TotalAmount { get; set; }
  
        public string Price
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(Amount, Currency);
            }
        }
  
        public decimal Amount { get; set; }
        public CurrencyCodes Currency { get; set; }
        public string EventName { get; set; }
        public string EventDate { get; set; }
        public string DayAndTime { get; set; }
        public string EventUrl{get;set;}
        public List<OrderInstallment> OrderInstallments { get; set; }
        public List<ReservationItemDetail> OrderCharges { get; set; }
        public List<ReservationItemDetail> OrderDiscounts { get; set; }
        public PaymentPlanType PaymentPlanType { get; set; }

        public List<string> AdditionalParameters { get; set; }
        public PostalAddress EventAddress { get; set; }
       
    }
    public class ReservationItemDetail
    {
        public string Price { get; set; }

        public string DetailName { get; set; }
        public ChargeDiscountCategory Category { get; set; }
        public string Type { get; set; }

        public decimal Amount { get; set; }
    }

    public enum ReservationEmailMode
    {
        Normal = 0,
        Transfer = 1,
        Edit = 2,
        Cancel = 3
    }
}