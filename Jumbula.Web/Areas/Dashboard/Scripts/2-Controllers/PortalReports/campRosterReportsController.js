﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('campRosterReportsController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            $scope.IsRunReport = false;
            var timer = false;
            var isFirstRunReport = false;

            $scope.Model = {};
            $scope.Model.schoolId = [];
            $scope.Model.seasonId = [];


            $http.get(baseUrl + '/GetAllSchool')
                .success(function (data, status, headers, config) {

                    $scope.AllSchools = data;
                });

            $scope.fillSeasonData = function (schoolId) {
                $http.get(baseUrl + '/GetClubSeasons', { params: { clubId: schoolId } }).success(function (data, status, headers, config) {  ///GetClubSeasonsProvider delete

                    $scope.AllSeasons = data;
                    $scope.Model.schoolId = schoolId;

                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }

            $scope.fillProgramsData = function (seasonId) {
                if (seasonId == null || seasonId == 0) {
                    $scope.AllPrograms = [];
                    return;
                }
                $http.get(baseUrl + '/GetAllCampClubs', { params: { seasonId: seasonId } }).success(function (data, status, headers, config) {

                    $scope.AllPrograms = data;

                }).error(function (data, status, headers, config) {
                    jbToast.error(data.Message);
                });
            }

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };
            $scope.$watch('AllSchools', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });
            $scope.$watch('AllPrograms', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });
            $scope.$watch('AllSeasons', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    $scope.updateChosenScope();
                }, 1500);
            });

            var checkValidation = function (schoolId, seasonId, programId) {
                if (is.undefined(schoolId) || schoolId <= 0) {
                    jbToast.error("Please select a school.");
                    return false;
                }
                else if (is.undefined(seasonId) || seasonId <= 0) {
                    jbToast.error("Please select a season.");
                    return false;
                }
                else if (is.undefined(programId) || programId <= 0) {
                    jbToast.error("Please select a program.");
                    return false;
                }
                else {
                    return true;
                }
            }

            $scope.runReport = function () {
                if (checkValidation($scope.Model.schoolId, $scope.Model.SeasonId, $scope.Model.ProgramId)) {
                    customReportService.saveSchoolSeasonId($scope.Model.schoolId, $scope.Model.SeasonId);

                    $scope.IsRunReport = true;

                    fillScheduleData($scope.Model.ProgramId);

                    $scope.filter.entryFeeId = "0";
                    $scope.filter.scheduleId = "0";

                    prepareGrid();
                }
            }

            $scope.saveToExcel = function () {

                var dataSource = $("#campRosterGrid").data("kendoGrid");

                for (var i = 0; i < dataSource.columns.length; i++) {
                    dataSource.columns[i].title = $scope.Titles[i];
                }

                cfpLoadingBar.start();
                $("#campRosterGrid").getKendoGrid().saveAsExcel();
            }

            var fillScheduleData = function (programId) {
                $http.get(window.location.origin + '/dashboard/program/GetDetails/?programid=' + programId).success(function (data, status, headers, config) {

                    $scope.Model.Schedules = data.Schedules;
                    $scope.Model.Charges = data.Charges;
                    $scope.$watch('Schedules', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });
                    $scope.$watch('Charges', function () {
                        if (timer) {
                            $timeout.cancel(timer);
                        }
                        timer = $timeout(function () {
                            $scope.updateChosenScope();
                        }, 1500);
                    });

                }).error(function (data, status, headers, config) {
                    jbToast.error(data);
                });
            }

            $scope.scheduleChange = function () {
                $scope.filter.entryFeeId = "0";
                $("#campRosterGrid").data("kendoGrid").setOptions({
                    columns: []
                });
                $scope.campRosterGridOptions.dataSource.page(1);
                $("#campRosterGrid").data("kendoGrid").dataSource.read();
            }

            $scope.tuitionChange = function () {
                $("#campRosterGrid").data("kendoGrid").setOptions({
                    columns: []
                });
                $scope.campRosterGridOptions.dataSource.page(1);
                $("#campRosterGrid").data("kendoGrid").dataSource.read();
            }

            $scope.clearFilters = function () {
                $scope.filter.entryFeeId = "0";
                $scope.filter.scheduleId = "0";

                $scope.campRosterGridOptions.dataSource.page(1);
                $("#campRosterGrid").data("kendoGrid").dataSource.read();
            }

            var prepareGrid = function () {

                if (isFirstRunReport) {
                    
                    $("#campRosterGrid").data("kendoGrid").setOptions({
                        columns: []
                    });

                    $scope.campRosterGridOptions.dataSource.page(1);
                    $("#campRosterGrid").data("kendoGrid").dataSource.read();
                    
                    return;
                }

                isFirstRunReport = true;

                $scope.campRosterDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport:
                        {
                            read: {
                                url: baseUrl + "/GetCampRosterReport",
                                type: "POST",
                                data: function () {
                                    return {
                                        programId: $scope.Model.ProgramId,
                                        scheduleId: $scope.filter.scheduleId,
                                        entryFeeId: $scope.filter.entryFeeId,
                                    };
                                },
                            }
                        },
                    schema: {
                        total: "TotalCount",
                        data: function (response) {

                            $scope.Titles = response.Titles;
                            return response.DataSource;
                        },
                    },
                    serverSorting: false,
                    serverPaging: true,
                    pageSize: 50,
                    resizable: true,
                });


                var columns = [];
                $scope.oldColumns = [];

                $scope.campRosterGridOptions = {

                    dataSource: $scope.campRosterDataSource,

                    sortable: true,
                    scrollable: true,
                    resizable: true,
                    columnMenu: true,

                    pageable: {
                        refresh: true,
                        pageSizes:
                            [
                                5, 10, 20, 30, 50, 100
                            ],
                        buttonCount: 5
                    },
                    dataBound: function (e) {
                        customReportService.initializeDoubleScroll(e);

                        $scope.oldColumns = $("#campRosterGrid").data("kendoGrid").columns;

                        //Rename columns when select another program with different columns
                        for (var j = 0; j < e.sender.columns.length; j++) {

                            var column = e.sender.columns[j];  

                            var newTitle = $scope.Titles[j]

                            $("#campRosterGrid thead [data-field=" + column.field + "] .k-link").html(newTitle);

                            resizeColumn(newTitle, 260);
                        }


                    },
                    excel: {
                        allPages: true,
                        fileName: "Camp roster.xlsx",
                        proxyURL: "/Dashboard/PortalReport/Save"
                    },
                    excelExport: function (e) {

                        var rows = e.workbook.sheets[0].rows;

                        rows.unshift({
                            cells: [{
                                value: ""
                            }]
                        });

                        rows.unshift({
                            cells: [{
                                value: "Camp roster report"
                            }]
                        });

                        cfpLoadingBar.complete();
                    },
                }



                $(document).bind("ajaxComplete.changeColumns", function () {


                    if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                        for (var i = 0; i < $scope.oldColumns.length; i++) {
                            var column = [];

                            column = {
                                field: $scope.oldColumns[i].field,
                                title: $scope.Titles[i],
                                width: '250px',
                                locked: i == 0 ? true : false,
                            }

                            columns.push(column);
                        }
                        $("#campRosterGrid").data("kendoGrid").setOptions({
                            columns: columns
                        });
                    }
                    $(document).unbind("ajaxComplete.changeColumns");
                });


                function resizeColumn(title, width) {
                    var index = $("#campRosterGrid .k-grid-header-wrap").find("th:contains(" + title + ")").index();

                    $("#campRosterGrid .k-grid-header-wrap") //header
                        .find("colgroup col")
                        .eq(index)
                        .css({ width: width });

                    $("#campRosterGrid .k-grid-content") //content
                        .find("colgroup col")
                        .eq(index)
                        .css({ width: width });
                }

            }

        }
    ]);
})();