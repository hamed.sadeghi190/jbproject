﻿using System;

namespace Jumbula.Core.Domain.Generic
{
    
    public class SendingMailInformation
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string CCEmail { get; set; }
        public string Events { get; set; }
    }

    
    
    public class ClassSessionsDateTime
    {
        public int Id { get; set; }
        public bool TimeChanges { get; set; }
        public DateTime SessionDate { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}
