﻿using System;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class OrderHistory : BaseEntity
    {
       
        public int UserId { get; set; }

        public long OrderId{ get; set; }

        public long? OrderItemId { get; set; }

        public DateTime ActionDate { get; set; }

        public OrderAction Action { get; set; }

        public string Description { get; set; }
    }
}
