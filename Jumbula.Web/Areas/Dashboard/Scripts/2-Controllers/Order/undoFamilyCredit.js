﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('undoFamilyCredit', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }
        $scope.baseUrl = window.location.origin + "/Dashboard/Order/";
        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $stateParams.seasonDomain;
        $scope.orderItemId = $stateParams.orderId;

        $scope.Model = {};

        $scope.PageMode = "Step1";

        $http.get($scope.baseUrl + '/GetUndoRefundToOrder/?orderItemId=' + $stateParams.orderItemId + '&transacTionId=' + $stateParams.transacTionId).success(function (data, status, headers, config) {
            $scope.Model = data;
            $scope.Model.TransactionActivity.IsDeleted = false;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
            });

        

        $scope.confirmFamilyCredit = function () {

            $http.post($scope.baseUrl + '/ValidateUndoFamilyCredit', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status) {

                        $scope.Model.OrderInfo.PaidAmount += $scope.Model.ReturnedAmount;
                        $scope.Model.OrderInfo.Balance = $scope.Model.OrderInfo.TotalAmount - $scope.Model.OrderInfo.PaidAmount;

                        $scope.Model.FamilyCredit -= $scope.Model.ReturnedAmount;

                        $scope.PageMode = "Step2";
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });

        }

        $scope.cancelAction = function () {
            if ($scope.PageMode == "Step1") {
                $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId })
            } else {

                $scope.Model.OrderInfo.PaidAmount -= $scope.Model.ReturnedAmount;
                $scope.Model.OrderInfo.Balance = $scope.Model.OrderInfo.TotalAmount - $scope.Model.OrderInfo.PaidAmount;

                $scope.Model.FamilyCredit += $scope.Model.ReturnedAmount;

                $scope.PageMode = "Step1"
            }
        }


        $scope.SubmitUndoRefundToOrder = function () {

            $http.post($scope.baseUrl + '/SubmitUndoRefundToOrder', { model: $scope.Model })
             .success(function (data, status, headers, config) {
                 if (data.Status) {
                     $scope.errors = null;
                     jbToast.success("Undo credit transfer done successfully.");
                     $state.go("OrderItem", { seasonDomain: $stateParams.seasonDomain, orderItemId: $stateParams.orderItemId })
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

    }]);
})();