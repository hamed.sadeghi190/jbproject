﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ModelState;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SportsClub.Models.Register;
using JsonHelper = Jumbula.Common.Helper.JsonHelper;

namespace Jumbula.Web.WebLogic.Register
{
    public class RegisterService
    {
        private RegisterService()
        {

        }
        public static RegisterService Get()
        {
            return new RegisterService();
        }

        public void ManipulatePaymentPlans(RegisterProgramModel model)
        {
            if (model.ProgramTypeCategory == ProgramTypeCategory.Subscription || model.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
            {
                return;
            }

            if (model.PaymentPlanLists != null && model.PaymentPlanLists.Count > 0)
            {
                model.PaymentPlanLists.Clear();
            }

            List<PaymentPlan> paymentPlanList = null;
            if (model.OrderMode == OrderMode.Online)
            {

                paymentPlanList = Ioc.PaymentPlanBusiness.GetPaymentPlansByProgramIdForUser(model.ProgramId, model.SeasonId, JbUserService.GetCurrentUserId()).OrderBy(c => c.NumOfInstallments).ToList();
            }
            else
            {
                paymentPlanList = Ioc.PaymentPlanBusiness.GetPaymentPlansByProgramId(model.ProgramId, model.SeasonId).OrderBy(c => c.NumOfInstallments).ToList();
            }
            if (paymentPlanList != null)
            {
                foreach (var paymentPlan in paymentPlanList)
                {
                    var numOfInstallment = paymentPlan.ListOfInstallmentDates.Where(d => Convert.ToDateTime(d).CompareTo(DateTime.UtcNow.Date) > 0).Count();
                    if (numOfInstallment > 0)
                    {
                        if (model.PaymentPlanLists == null)
                        {
                            model.PaymentPlanLists = new List<PaymentPlan>();
                        }
                        model.PaymentPlanLists.Add(paymentPlan);
                    }
                }
            }


        }
        public void ManipulatePlayer(RegisterProgramModel model)
        {
            //model.UserId = TheMembership.GetCurrentUserId();
            var playerProfiles = Ioc.PlayerProfileBusiness.GetList(model.UserId)
                .Select(p => new
                {
                    p.Id,
                    p.Contact.FirstName,
                    p.Contact.LastName,
                    p.Contact.Nickname,
                    p.Relationship
                })
                .ToList();

            model.PlayerLists = playerProfiles.Where(p => p.Relationship == RelationshipType.Registrant).Select(p => new SelectListItem
            {
                Value = p.Id.ToString(),
                Text = p.FirstName + " " + p.LastName + (string.IsNullOrEmpty(p.Nickname) ? "" : "(" + p.Nickname + ")")
            })
            .ToList();

            if (!model.ProfileId.HasValue || model.ProfileId.Value == 0)
            {
                return;
            }

            var playerProfile = model.ProfileId.HasValue ? Ioc.PlayerProfileBusiness.Get(model.ProfileId.Value) : null;

            JbUser userProfile = null;
            try
            {
                userProfile = Ioc.UserProfileBusiness.Get(model.UserId);
            }
            catch { }
            string playerAvatar = string.Empty;


            model.Player = playerProfile;
            model.PlayerImageUrl = playerAvatar;
            model.SelectedProfileId = model.ProfileId;
        }

        public string ManipulateOrderItem(RegisterProgramModel model, Program program, long ists)
        {
            var order = Ioc.CartBusiness.Get(model.UserId, JbUserService.GetCurrentClubId()).Order;
            var items = order.RegularOrderItems.Where(item => item.ISTS == ists);

            model.Player = items.First().Player;
            model.ProfileId = items.First().Player.Id;
            model.PaymentPlanId = items.First().PaymentPlanId.HasValue ? items.First().PaymentPlanId.Value : 0;


            var charges = new[]
            {
                new { OrderItemId =(long)(0), Amount = (decimal)(0), Category = string.Empty, ChargeId = (long?)null, Name = string.Empty, IsMandatory = false, ParentChargeId = (long?)null, Subcategory = string.Empty, ScheduleId = (long?)null, ScheduleTitle = string.Empty}
            }
            .ToList();


            if (program.TypeCategory != ProgramTypeCategory.Subscription)
            {
                charges = items.SelectMany(item => item.GetOrderChargeDiscounts().Where(orc => (int)orc.Category > 0 && orc.Category != ChargeDiscountCategory.InstallmentFee && orc.Category != ChargeDiscountCategory.Convenience && orc.Category != ChargeDiscountCategory.Surcharge && orc.Category != ChargeDiscountCategory.PartnerSurcharge && orc.Category != ChargeDiscountCategory.ApplicationFee && orc.Category != ChargeDiscountCategory.PartnerApplicationFee))
                    .Select(charge => new
                    {
                        OrderItemId = charge.OrderItemId.Value,
                        Amount = charge.Amount,
                        Category = charge.Category.ToString(),
                        ChargeId = charge.ChargeId,
                        Name = charge.Name,
                        IsMandatory = charge.Charge.Attributes.IsMandatory,
                        ParentChargeId = charge.OrderItem.GetOrderChargeDiscounts().First(oc => oc.Category == ChargeDiscountCategory.EntryFee).ChargeId,
                        Subcategory = charge.Subcategory.ToString(),
                        ScheduleId = charge.OrderItem.ProgramScheduleId,
                        ScheduleTitle = program.TypeCategory == ProgramTypeCategory.Calendar ? charge.OrderItem.ProgramSchedule.Title : charge.OrderItem.Start.Value.ToShortDateString() + " " + charge.OrderItem.End.Value.ToShortDateString(),
                    })
                    .ToList();
            }
            else
            {
                var schedule = program.ProgramSchedules.Last(s => !s.IsDeleted);
                var season = program.Season;
                var subscriptionAttribute = schedule.Attributes as ScheduleSubscriptionAttribute;

                var subscriptionBusiness = Ioc.SubscriptionBusiness;

                var totalSubscritptionAmount = subscriptionBusiness.GetTotalSubscriptionAmount(schedule, model.DesiredStartDate);

                var firstSubscritptionAmount = subscriptionBusiness.GetFirstSubscriptionAmount(schedule, model.DesiredStartDate);

                var firstSubscritptionTitle = string.Empty;// subscriptionAttribute.Subscription.PriceLabel;

                var entryFee = items.SelectMany(item => item.GetOrderChargeDiscounts()).SingleOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee);
                var entryFeeId = entryFee.Id;

                var subscriptionOrderCharges = new List<OrderChargeDiscountModel>()
                    {
                        new OrderChargeDiscountModel
                        {
                            Id = entryFeeId,
                            Amount = totalSubscritptionAmount,
                            Category = ChargeDiscountCategory.EntryFee,
                            Name = firstSubscritptionTitle,
                            Description = "",
                            Subcategory = ChargeDiscountSubcategory.Charge,
                            ChargeId = entryFeeId,
                            ScheduleId = (int)entryFeeId,
                        }
                    };

                if (schedule.Charges.Any(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted))
                {
                    var applicationFeeCharge = schedule.Charges.Single(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted);

                    subscriptionOrderCharges.Add(new OrderChargeDiscountModel
                    {
                        Amount = applicationFeeCharge.Amount,
                        Category = applicationFeeCharge.Category,
                        ChargeId = applicationFeeCharge.Id,
                        Description = applicationFeeCharge.Description,
                        Name = applicationFeeCharge.Name,
                        Subcategory = ChargeDiscountSubcategory.Charge
                    }
                    );
                }

                charges = subscriptionOrderCharges
                    .Select(charge => new
                    {
                        OrderItemId = items.First().Id,
                        Amount = charge.Amount,
                        Category = charge.Category.ToString(),
                        ChargeId = (long?)charge.Id,
                        Name = charge.Name,
                        IsMandatory = true,
                        ParentChargeId = (long?)entryFeeId,// charge.OrderItem.OrderChargeDiscounts.First(oc => oc.Category == ChargeDiscountCategory.EntryFee).ChargeId,
                        Subcategory = charge.Subcategory.ToString(),
                        ScheduleId = (long?)schedule.Id,
                        ScheduleTitle = "",// program.TypeCategory == ProgramTypeCategory.Calendar ? charge.OrderItem.ProgramSchedule.Title : charge.OrderItem.Start.Value.ToShortDateString() + " " + charge.OrderItem.End.Value.ToShortDateString(),
                    })
                    .ToList();
            }

            if (model.OrderMode == OrderMode.Offline && (model.ProgramTypeCategory == ProgramTypeCategory.Camp || model.ProgramTypeCategory == ProgramTypeCategory.Class))
            {
                var entryfees = charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee.ToString()).ToList();
                foreach (var ef in entryfees)
                {
                    model.ProgramSchedules.SelectMany(c => c.Charges).First(c => c.Id == ef.ChargeId).Amount = ef.Amount;
                }
            }
            if (program.TypeCategory == ProgramTypeCategory.ChessTournament)
            {
                model.ChessTournament.Section = items.First().OrderItemChess.Section;
                model.ChessTournament.Schedule = items.First().OrderItemChess.Schedule;
                model.ChessTournament.ScheduleAttribute = program.ProgramSchedules.FirstOrDefault().Attributes as TournamentScheduleAttribute;
                if (!string.IsNullOrEmpty(items.First().OrderItemChess.Byes))
                {
                    var nums = items.First().OrderItemChess.Byes.Split(new char[] { ',' }).Select(b => int.Parse(b.Trim()));
                    var bytes = Enumerable.Repeat(false, model.ChessTournament.ScheduleAttribute.NumberOfRounds.Value).ToArray();
                    foreach (var num in nums)
                    {
                        bytes[num - 1] = true;
                    }
                    model.ChessTournament.ByeRequests = bytes;
                }
            }

            if (model.ProgramTypeCategory == ProgramTypeCategory.Calendar)
            {
                throw new NotImplementedException();
            }

            model.JbForm = items.First().JbForm;
            return JsonHelper.JsonSerializer(charges);
        }

        public void ManipulateJbForm(RegisterProgramModel model, JbForm form, long programId, bool isNewParticipantMode = false)
        {
            Ioc.FormBusiness.RemoveHiddenItems(form);

            model.JbForm = form;

            if (model.JbForm != null)
            {
                model.JbForm.Id = 0;
                model.JbForm.CurrentMode = AccessMode.Edit;
                model.JbForm.RefEntityName = typeof(Order).Name;
                model.JbForm.RefEntityId = 0;
                model.JbForm.VisibleMode = model.OrderMode == OrderMode.Offline ? VisibilityMode.Offline : VisibilityMode.Online;
                model.JbForm.SetElementsCurrentMode(AccessMode.Edit);

                if (!isNewParticipantMode)
                    Ioc.FormBusiness.SetParticipantFieldsReadonly(model.JbForm);

                var profileBusiness = Ioc.PlayerProfileBusiness;

                profileBusiness.AutoFillProfileForm(model.JbForm, model.UserId, programId, model.SelectedProfileId);
            }
        }

        public void ManipulateJbForm(JbForm form, int userId, int? profileId, long programId, bool isEditMode = false)
        {
            Ioc.FormBusiness.RemoveHiddenItems(form);

            if (form != null)
            {
                form.Id = 0;
                form.CurrentMode = AccessMode.Edit;
                form.RefEntityName = typeof(Order).Name;
                form.RefEntityId = 0;
                //form.VisibleMode = model.OrderMode == OrderMode.Offline ? VisibilityMode.Offline : VisibilityMode.Online;
                form.SetElementsCurrentMode(AccessMode.Edit);

                if(profileId.HasValue && profileId != 0)
                    Ioc.FormBusiness.SetParticipantFieldsReadonly(form);

                Ioc.PlayerProfileBusiness.AutoFillProfileForm(form, userId, programId, profileId);
            }
        }

        public bool ManipulatePlayerProfile(RegisterProgramModel model, SectionsName sectionName, ModelStateDictionary ModelState)
        {
            var isCreateMode = false;
            var player = Ioc.PlayerProfileBusiness.GetFromForm(model.JbForm, sectionName);
            var players = Ioc.PlayerProfileBusiness.GetList(model.UserId);

            if (player != null && !string.IsNullOrEmpty(player.Contact.FirstName))
            {
                if (player.Contact.DoB.HasValue && player.Contact.DoB.Value.Date.CompareTo(DateTime.UtcNow.Date) > 0)
                {
                    ModelState.AddModelError(model.JbForm, SectionsName.ParticipantSection, ElementsName.DoB, "Date of birth is invalid");
                    return false;
                }
                if (player.Relationship == RelationshipType.Registrant && ((player.Contact.DoB.HasValue && player.Contact.DoB.Value.Year < 1910)))
                {
                    ModelState.AddModelError(model.JbForm, SectionsName.ParticipantSection, ElementsName.DoB, "Date of birth is invalid");
                    return false;
                }

                PlayerProfile profile = null;

                if (model.ProfileId.HasValue && model.ProfileId.Value > 0 && player.Relationship == RelationshipType.Registrant)
                {
                    profile = Ioc.PlayerProfileBusiness.Get(model.ProfileId.Value);
                    isCreateMode = false;
                }
                else if (player.Relationship == RelationshipType.Parent)
                {
                    profile = players.FirstOrDefault(p => p.Relationship == RelationshipType.Parent && p.Contact.FirstName.Equals(player.Contact.FirstName, StringComparison.OrdinalIgnoreCase) && p.Contact.LastName.Equals(player.Contact.LastName, StringComparison.OrdinalIgnoreCase));
                }

                if (profile == null)
                {
                    profile = new PlayerProfile() { UserId = model.UserId };
                    isCreateMode = true;
                }

                profile.Status = PlayerProfileStatusType.Active;
                profile.Gender = player.Gender;
                if (sectionName == SectionsName.ParentGuardianSection || sectionName == SectionsName.Parent2GuardianSection)
                {
                    profile.Relationship = RelationshipType.Parent;
                }
                else
                {
                    profile.Relationship = RelationshipType.Registrant;
                }

                profile.Contact = player.Contact;
                if (profile.Relationship == RelationshipType.Registrant)
                {
                    if (!model.IsTeamRegistration)
                    {
                        if (players != null && players.Any())
                        {
                            if (players.Any(c => c.Id != model.ProfileId && c.Contact.FirstName == player.Contact.FirstName && c.Contact.LastName == player.Contact.LastName && c.Relationship == RelationshipType.Registrant))
                            {
                                ModelState.AddModelError(model.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName, "The participant already exists");
                                return false;
                            }
                        }
                    }
                }
                var res = new OperationStatus() { Status = true };
                if (model.IsTeamRegistration)
                {

                    if (players != null && players.Any())
                    {
                        if (!(players.Any(c => c.Id != model.ProfileId && c.Contact.FirstName == player.Contact.FirstName && c.Contact.LastName == player.Contact.LastName)))
                        {
                            res = Ioc.PlayerProfileBusiness.Create(profile);

                            if (res.Status && profile.Relationship == RelationshipType.Registrant)
                            {
                                model.Player = profile;
                                model.ProfileId = profile.Id;
                                return true;
                            }
                            else if (res.Status)
                            {
                                return true;
                            }

                        }
                        else
                        {
                            profile = players.Where(c => c.Contact.FirstName == player.Contact.FirstName && c.Contact.LastName == player.Contact.LastName).First();
                        }
                    }
                    else
                    {
                        res = Ioc.PlayerProfileBusiness.Create(profile);
                        if (res.Status && profile.Relationship == RelationshipType.Registrant)
                        {
                            model.Player = profile;
                            model.ProfileId = profile.Id;
                            return true;
                        }
                        else if (res.Status)
                        {
                            return true;
                        }

                    }
                }

                if (isCreateMode)
                {
                    res = Ioc.PlayerProfileBusiness.Create(profile);
                }
                else
                {
                    res = Ioc.PlayerProfileBusiness.Update(profile);
                }

                if (res.Status && profile.Relationship == RelationshipType.Registrant)
                {
                    model.Player = profile;
                    model.ProfileId = profile.Id;
                    return true;
                }
                else if (res.Status)
                {
                    return true;
                }

                ModelState.AddModelError("JbForm.Elements[0].Elements[0].Value",
                         "Error occurred during a create profile operation,please enter correct information");
            }

            return false;
        }

        public PlayerProfile CreatePlayerProfile(JbForm jbForm, int userId, SectionsName sectionName, ModelStateDictionary ModelState)
        {
            PlayerProfile result = null;

            var player = Ioc.PlayerProfileBusiness.GetFromForm(jbForm, sectionName);
            if (player != null && !string.IsNullOrEmpty(player.Contact.FirstName))
            {
                if (player.Contact.DoB.HasValue && player.Contact.DoB.Value.Date.CompareTo(DateTime.UtcNow.Date) > 0)
                {
                    ModelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.DoB, "Date of birth is invalid");
                    return result;
                }
                if (player.Relationship == RelationshipType.Registrant && ((player.Contact.DoB.HasValue && player.Contact.DoB.Value.Year < 1910)))
                {
                    ModelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.DoB, "Date of birth is invalid");
                    return result;
                }

                var profile = new PlayerProfile() { UserId = userId };
                profile.Status = PlayerProfileStatusType.Active;
                profile.Gender = player.Gender;
                if (sectionName == SectionsName.ParentGuardianSection || sectionName == SectionsName.Parent2GuardianSection)
                {
                    profile.Relationship = RelationshipType.Parent;
                }
                else
                {
                    profile.Relationship = RelationshipType.Registrant;
                }

                profile.Contact = player.Contact;
                if (profile.Relationship == RelationshipType.Registrant)
                {


                }
                var res = new OperationStatus() { Status = true };

                res = Ioc.PlayerProfileBusiness.Create(profile);

                if (res.Status && profile.Relationship == RelationshipType.Registrant)
                {
                    return profile;
                }
                else if (res.Status)
                {
                    return profile;
                }

                ModelState.AddModelError("JbForm.Elements[0].Elements[0].Value",
                         "Error occurred during a create profile operation,please enter correct information");
            }

            return result;
        }

        public OperationStatus SetFormToParentDashboard(JbForm jbForm, int? playerId, int userId, ref PlayerProfile playerProfile)
        {

            var family = Ioc.FamilyBusiness.GetFamily(userId);
            var playerProfileBusiness = Ioc.PlayerProfileBusiness;
            if (family == null)
            {
                family = Ioc.FamilyBusiness.AddUserFamily(userId);
            }

            PlayerProfile player = null;
            var user = Ioc.UserProfileBusiness.Get(userId);
            var newPlayerList = new PlayerProfile();

            if (playerId.HasValue)
            {
                player = Ioc.PlayerProfileBusiness.Get(playerId.Value);
            }
            else
            {
                player = new PlayerProfile() { UserId = userId, User = user };
            }
            var formBusiness = Ioc.FormBusiness;

            try
            {
                var playerList = new List<PlayerProfile>();
                var familyContactList = new List<FamilyContact>();

                #region ParticipantInfo

                var firstName = formBusiness.GetPlayerFirstName(jbForm);
                var LastName = formBusiness.GetPlayerLastName(jbForm);
                var userParticipants = playerProfileBusiness.GetParticipants(player.UserId);


                if (userParticipants != null)
                {
                    if (!userParticipants.Any(p => p.Contact.FirstName.ToLower() == firstName.ToLower() && p.Contact.LastName.ToLower() == LastName.ToLower() && p.Id != playerId))
                    {
                        playerList.Add(playerProfileBusiness.GetPlayerParticipantjbForm(jbForm, player, player.User));

                        Ioc.FormBusiness.SetParticipantFieldsReadonly(jbForm);
                    }
                    else
                    {
                        return new OperationStatus { Status = false, Message = "Participant" };
                    }
                }
                else
                {
                    var newPlayer = new PlayerProfile();
                    playerList.Add(playerProfileBusiness.GetPlayerParticipantjbForm(jbForm, newPlayer, player.User));
                }
                #endregion

                #region ParentInfo

                var parent1FirstName = formBusiness.GetParentFirstName(jbForm);
                var parent1LastName = formBusiness.GetParentLastName(jbForm);
                var parent2FirstName = formBusiness.GetParent2FirstName(jbForm);
                var parent2LastName = formBusiness.GetParent2LastName(jbForm);

                var parent1FullName = string.Format("{0} {1}", parent1FirstName.Trim(), parent1LastName.Trim());
                var parent2FullName = string.Format("{0} {1}", parent2FirstName.Trim(), parent2LastName.Trim());

                if (!string.IsNullOrEmpty(parent1FullName.Trim()) && !string.IsNullOrEmpty(parent2FullName.Trim()))
                {
                    if (parent1FullName.ToLower() == parent2FullName.ToLower())
                    {
                        return new OperationStatus { Status = false, Message = "Parent1" };
                    }
                }


                var userParents = playerProfileBusiness.GetParents(player.UserId).ToList();


                playerList.AddRange(playerProfileBusiness.CompareParent1(player, jbForm, null));

                playerList.AddRange(playerProfileBusiness.CompareParent2(player, jbForm, null));

                #endregion

                #region AuthorizePickupsInfo
                var authorizedPickups = Ioc.FamilyContactBusiness.GetAuthorizedPickups(userId);

                var authorizePickup1FirstName = formBusiness.GetAuthorizePickup1FirstName(jbForm);
                var authorizePickup1LastName = formBusiness.GetAuthorizePickup1LastName(jbForm);
                var authorizePickup2FirstName = formBusiness.GetAuthorizePickup2FirstName(jbForm);
                var authorizePickup2LastName = formBusiness.GetAuthorizePickup2LastName(jbForm);
                var authorizePickup3FirstName = formBusiness.GetAuthorizePickup3FirstName(jbForm);
                var authorizePickup3LastName = formBusiness.GetAuthorizePickup3LastName(jbForm);
                var authorizePickup4FirstName = formBusiness.GetAuthorizePickup4FirstName(jbForm);
                var authorizePickup4LastName = formBusiness.GetAuthorizePickup4LastName(jbForm);

                var authorizePickup1FullName = string.Format("{0} {1}", authorizePickup1FirstName.Trim(), authorizePickup1LastName.Trim());
                var authorizePickup2FullName = string.Format("{0} {1}", authorizePickup2FirstName.Trim(), authorizePickup2LastName.Trim());
                var authorizePickup3FullName = string.Format("{0} {1}", authorizePickup3FirstName.Trim(), authorizePickup3LastName.Trim());
                var authorizePickup4FullName = string.Format("{0} {1}", authorizePickup4FirstName.Trim(), authorizePickup4LastName.Trim());

                if (!string.IsNullOrEmpty(authorizePickup1FullName.Trim()) && (!string.IsNullOrEmpty(authorizePickup2FullName.Trim())))
                {
                    if (authorizePickup1FullName.ToLower() == authorizePickup2FullName.ToLower())
                    {
                        return new OperationStatus { Status = false, Message = "authorizedPickup1" };
                    }
                }

                if (!string.IsNullOrEmpty(authorizePickup1FullName.Trim()) && !string.IsNullOrEmpty(authorizePickup3FullName.Trim()))
                {
                    if (authorizePickup1FullName.ToLower() == authorizePickup3FullName.ToLower())
                    {
                        return new OperationStatus { Status = false, Message = "authorizedPickup1" };
                    }
                }

                if (!string.IsNullOrEmpty(authorizePickup1FullName.Trim()) && !string.IsNullOrEmpty(authorizePickup4FullName.Trim()))
                {
                    if (authorizePickup1FullName.ToLower() == authorizePickup4FullName.ToLower())
                    {
                        return new OperationStatus { Status = false, Message = "authorizedPickup1" };
                    }
                }

                if (!string.IsNullOrEmpty(authorizePickup2FullName.Trim()) && !string.IsNullOrEmpty(authorizePickup3FullName.Trim()))
                {
                    if (authorizePickup2FullName.ToLower() == authorizePickup3FullName.ToLower())
                    {
                        return new OperationStatus { Status = false, Message = "authorizedPickup2" };
                    }
                }

                if (!string.IsNullOrEmpty(authorizePickup2FullName.Trim()) && !string.IsNullOrEmpty(authorizePickup4FullName.Trim()))
                {
                    if (authorizePickup2FullName.ToLower() == authorizePickup4FullName.ToLower())
                    {
                        return new OperationStatus { Status = false, Message = "authorizedPickup2" };
                    }
                }

                if (!string.IsNullOrEmpty(authorizePickup3FullName.Trim()) && !string.IsNullOrEmpty(authorizePickup4FullName.Trim()))
                {
                    if (authorizePickup3FullName.ToLower() == authorizePickup4FullName.ToLower())
                    {
                        return new OperationStatus { Status = false, Message = "authorizedPickup3" };
                    }
                }


                familyContactList.AddRange(playerProfileBusiness.CompareAuthorizePickup1(player, jbForm, null));

                familyContactList.AddRange(playerProfileBusiness.CompareAuthorizePickup2(player, jbForm, null));

                familyContactList.AddRange(playerProfileBusiness.CompareAuthorizePickup3(player, jbForm, null));

                familyContactList.AddRange(playerProfileBusiness.CompareAuthorizePickup4(player, jbForm, null));


                #endregion

                #region EmergencyContact

                familyContactList.AddRange(playerProfileBusiness.CompareEmergencyContact(player, jbForm, null));

                #endregion

                playerProfileBusiness.GetInsurancejbForm(jbForm, family);

                if (playerList.Any(p => p.Relationship == RelationshipType.Registrant))
                {
                    var profile = playerList.FirstOrDefault(p => p.Relationship == RelationshipType.Registrant);
                    playerProfile = profile;
                }

                var resultdb = playerProfileBusiness.SaveProfiles(playerList, familyContactList, family);

                if (resultdb.Status)
                {
                    return new OperationStatus { Status = true };
                }


                return new OperationStatus { Status = true };

            }
            catch (Exception ex)
            {
                return new OperationStatus { Status = false };
                //ModelState.AddModelError("JbForm.Elements[0].Elements[0].Value",
                //       "Error occurred during a create profile operation,please enter correct information");
            }

        }

        public void CheckRestrictions(RegisterProgramModel model, ModelStateDictionary modelState, bool isOrderOnline, Program program = null)
        {
            var programBusiness = Ioc.ProgramBusiness;
            var jbForm = model.JbForm;

            if (program == null)
            {
                program = programBusiness.Get(model.ProgramId);
            }

            var programSchedule = GetScheduleForCheckRestrictions(program, model);

            bool isTeamChessTourney = Ioc.ProgramBusiness.IsTeamChessTourney(program);

            if (!isTeamChessTourney)
            {
                CheckAgeRestriction(jbForm, programSchedule, modelState, isOrderOnline);
            }

            CheckGenderRestriction(jbForm, programSchedule, modelState, isOrderOnline);

            if (!isTeamChessTourney)
            {
                CheckGradeRestriction(jbForm, programSchedule, modelState, isOrderOnline);
            }

            CheckPhoneNumberValidation(jbForm, modelState);
            CheckDobValidation(jbForm, modelState);
            CheckSignitureValidation(jbForm, modelState);

            foreach (var state in modelState.Keys.Where(k => k.StartsWith("day", StringComparison.OrdinalIgnoreCase)).ToList())
            {
                modelState.Remove(state);
            }

            modelState.Remove("ChessTournament.Schedule");
            modelState.Remove("ChessTournament.Section");
        }

        private ProgramSchedule GetScheduleForCheckRestrictions(Program program, RegisterProgramModel model)
        {
            try
            {
                var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(model.ChargeDiscountsText);

                var scheduleIds = charges.Select(c => long.Parse(c.ScheduleId.ToString())).Distinct().ToList();

                if (scheduleIds.Count() == 1)
                {
                    return program.ProgramSchedules.First(s => s.Id == scheduleIds.First());
                }
                else
                {
                    return program.ProgramSchedules.First(s => scheduleIds.Contains(s.Id));
                }
            }
            catch
            {

            }

            return program.ProgramSchedules.First(s => !s.IsDeleted);
        }

        public void CheckAgeRestriction(JbForm jbForm, ProgramSchedule schedule, ModelStateDictionary modelState,bool isOrderOnline)
        {
            var ageElements = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Where(e => e.Name == ElementsName.DoB.ToString());
            bool formHasDob = ageElements != null && ageElements.Any();

            if (formHasDob)
            {
                var elementsMode = ageElements.Select(e => e.VisibleMode);
                var elementMode = elementsMode.First();

                var checkValidation = true;

                if (elementMode != VisibilityMode.Both)
                {
                    checkValidation = (isOrderOnline && elementMode == VisibilityMode.Online) || (!isOrderOnline && elementMode == VisibilityMode.Offline);
                }

                if (!checkValidation) return;

                var dobElement = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.DoB.ToString());
                var strDob = dobElement.GetValue();
                DateTime dob;

                if (strDob.ToString() == string.Empty)
                {
                    if (dobElement.Validations.Any(v => v.Type == Jb.Framework.Common.Forms.Validation.ElementValidationType.Required) || schedule.AttendeeRestriction.RestrictionType == RestrictionType.Age)
                    {
                        modelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.DoB, "Date of birth is required");
                    }

                    return;
                }

                if (DateTime.TryParse(strDob.ToString(), out dob))
                {
                    var ageStartDate = DateTime.UtcNow;
                    if (schedule.AttendeeRestriction.ApplyAtProgramStart)
                        ageStartDate = schedule.StartDate;

                    var playerAge = DateTimeHelper.CalculateAge(dob, ageStartDate);

                    if (dob.Year < 1920 || dob > DateTime.UtcNow)
                    {
                        modelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.DoB, "Date of birth is invalid");
                    }

                    var isAgeRestricted = Ioc.ProgramBusiness.CheckAgeRestriction(schedule, playerAge);

                    if (isAgeRestricted)
                    {
                        var message = Ioc.ProgramBusiness.AgeRestrictionMessage(schedule.AttendeeRestriction.MinAge, schedule.AttendeeRestriction.MaxAge, schedule.AttendeeRestriction.ApplyAtProgramStart);

                        if (!string.IsNullOrEmpty(message))
                        {
                            modelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.DoB, message);
                        }
                    }
                }
                else
                {
                    modelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.DoB, "Format should be MM/DD/YYYY.");
                }
            }
        }

        public void CheckGenderRestriction(JbForm jbForm, ProgramSchedule schedule, ModelStateDictionary modelState, bool isOrderOnline)
        {

            var sectionElements = jbForm.Elements.Where(s => s.Name == SectionsName.ParticipantSection.ToString());
            var genderElements = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Where(e => e.Name == ElementsName.Gender.ToString());

            bool formHasGender = (sectionElements != null && sectionElements.Any()) && (genderElements != null && genderElements.Any());

            if (formHasGender)
            {
                var elementsMode = genderElements.Select(e => e.VisibleMode);
                var elementMode = elementsMode.First();

                var checkValidation = true;

                if (elementMode != VisibilityMode.Both)
                {
                    checkValidation = (isOrderOnline && elementMode == VisibilityMode.Online) || (!isOrderOnline && elementMode == VisibilityMode.Offline);
                }

                if (!checkValidation) return;

                var gender = Genders.NoRestriction;

                var objGender = ((JbSection)jbForm.Elements.Single(s => s.Name == SectionsName.ParticipantSection.ToString())).Elements.Single(e => e.Name == ElementsName.Gender.ToString()).GetValue();
                var strGender = objGender != null ? objGender.ToString() : string.Empty;

                if (string.IsNullOrEmpty(strGender) && schedule.AttendeeRestriction.Gender != Genders.NoRestriction)
                {
                    modelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.Gender, "Gender is required");
                }

                if (strGender != Genders.Male.ToString() && strGender != Genders.Female.ToString())
                {
                    return;
                }

                if (!string.IsNullOrEmpty(strGender))
                {
                    gender = EnumHelper.ParseEnum<Genders>(strGender);
                    var restrictioinMessage = Ioc.ProgramBusiness.CheckGenderRestriction(schedule, gender);

                    if (!string.IsNullOrEmpty(restrictioinMessage))
                    {
                        modelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.Gender, restrictioinMessage);
                    }
                }

            }
        }

        public void CheckGradeRestriction(JbForm jbForm, ProgramSchedule schedule, ModelStateDictionary modelState , bool isOrderOnline)
        {
            var formBusiness = Ioc.FormBusiness;

            var gradeElements = formBusiness.GetFormElements(jbForm, ElementsName.Grade.ToString());
            bool formHasGrade = gradeElements != null && gradeElements.Any();


            if (formHasGrade)
            {
                var elementsMode = gradeElements.Select(e => e.VisibleMode);
                var elementMode = elementsMode.First();

                var checkValidation = true;

                if (elementMode != VisibilityMode.Both)
                {
                    checkValidation = (isOrderOnline && elementMode == VisibilityMode.Online) || (!isOrderOnline && elementMode == VisibilityMode.Offline);
                }

                if (!checkValidation) return;

                var grade = formBusiness.GetPlayerGrade(jbForm);

                var gradeObject = !string.IsNullOrEmpty(grade.ToDescription()) ? grade.ToDescription() : null;

                var gradeRestrictionMessage = Ioc.ProgramBusiness.CheckGradeRestriction(schedule, gradeObject);

                if (!string.IsNullOrEmpty(gradeRestrictionMessage))
                {
                    modelState.AddModelError(jbForm, SectionsName.SchoolSection, ElementsName.Grade, gradeRestrictionMessage);

                    modelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.Grade, gradeRestrictionMessage);
                }

            }
        }

        public bool IsGenderRestricted(string playerGender, Program program)
        {

            if (string.IsNullOrEmpty(playerGender))
                return false;

            var schedule = program.ProgramSchedules.First(p => !p.IsDeleted);

            var gender = EnumHelper.ParseEnum<Genders>(playerGender);

            if (schedule.AttendeeRestriction.Gender.Value != Genders.NoRestriction)
            {
                if (schedule.AttendeeRestriction.Gender.Value != gender)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsGradeRestricted(string playerGrade, Program program)
        {
            var schedule = program.ProgramSchedules.First(p => !p.IsDeleted);

            if (string.IsNullOrEmpty(playerGrade))
                return false;

            var grade = EnumHelper.GetValueFromDescription<SchoolGradeType?>(playerGrade);

            var gradeObject = !string.IsNullOrEmpty(grade.ToDescription()) ? grade.ToDescription() : null;

            var gradeRestrictionMessage = Ioc.ProgramBusiness.CheckGradeRestriction(schedule, gradeObject);

            if (!string.IsNullOrEmpty(gradeRestrictionMessage))
                return true;

            return false;
        }

        public bool IsAgeRestricted(int? playerAge, Program program)
        {
            if (!playerAge.HasValue)
                return false;

            return Ioc.ProgramBusiness.CheckAgeRestriction(program, playerAge.Value);
        }

        public string GetAgeRestrictionMessage(Program program)
        {
            var schedule = program.ProgramSchedules.First(p => !p.IsDeleted);

            return Ioc.ProgramBusiness.AgeRestrictionMessage(schedule.AttendeeRestriction.MinAge, schedule.AttendeeRestriction.MaxAge, false);

        }

        public string GetGradeRestrictionMessage(JbForm jbForm, ProgramSchedule schedule)
        {

            var result = string.Empty;
            var formBusiness = Ioc.FormBusiness;

            var gradeElements = formBusiness.GetFormElements(jbForm, ElementsName.Grade.ToString());
            var formHasGrade = gradeElements != null && gradeElements.Any();

            if (!formHasGrade) return result;

            var grade = formBusiness.GetPlayerGrade(jbForm);

            var gradeObject = !string.IsNullOrEmpty(grade.ToDescription()) ? grade.ToDescription() : null;

            result = Ioc.ProgramBusiness.CheckGradeRestriction(schedule, gradeObject);

            return result;
        }

        public void CheckPhoneNumberValidation(JbForm jbForm, ModelStateDictionary modelState)
        {
            var phones = new List<IJbBaseElement>();

            phones = jbForm.Elements.SelectMany(e => ((JbSection)e).Elements).Where(el => el is JbPhone).ToList();

            foreach (var phone in phones)
            {

                var phoneValue = phone.GetValue();

                if (!string.IsNullOrEmpty(phoneValue.ToString()))
                {
                    var strPhoneValue = phoneValue.ToString();

                    if (!Regex.Match(strPhoneValue, @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}").Success || strPhoneValue.Length > 15)
                    {
                        var sectionIndex = jbForm.Elements.FindIndex(s => ((JbSection)s).Elements.Any(e => e.ElementId == phone.ElementId));

                        var section = jbForm.Elements.Single(s => ((JbSection)s).Elements.Any(e => e.ElementId == phone.ElementId)) as JbSection;

                        var elementIndex = Ioc.FormBusiness.GetElementIndexInSection(section, phone as JbPhone);//jbForm.Elements.SelectMany(e => ((JbSection)e).Elements).ToList().FindIndex(f => f.ElementId == phone.ElementId);

                        modelState.AddModelError(string.Format("JbForm.Elements[{0}].Elements[{1}].Value", sectionIndex, elementIndex), "The phone number is invalid");
                    }
                }
            }

        }

        public void CheckDobValidation(JbForm jbForm, ModelStateDictionary modelState)
        {
            var dobs = new List<IJbBaseElement>();

            dobs = jbForm.Elements.SelectMany(e => ((JbSection)e).Elements).Where(el => el is JbTextBox && (el.Name == ElementsName.DoB.ToString() || el.Name == ElementsName.DateOfBirth.ToString())).ToList();

            foreach (var dob in dobs)
            {
                var dobValue = dob.GetValue();

                if (!string.IsNullOrEmpty(dobValue.ToString()))
                {
                    DateTime dateTime = DateTime.Now;
                    var isValid = DateTime.TryParse(dobValue.ToString(), out dateTime);

                    if (!isValid)
                    {
                        var sectionIndex = jbForm.Elements.FindIndex(s => ((JbSection)s).Elements.Any(e => e.ElementId == dob.ElementId));

                        var section = jbForm.Elements.Single(s => ((JbSection)s).Elements.Any(e => e.ElementId == dob.ElementId)) as JbSection;

                        var elementIndex = Ioc.FormBusiness.GetElementIndexInSection(section, dob as JbTextBox);

                        modelState.AddModelError(string.Format("JbForm.Elements[{0}].Elements[{1}].Value", sectionIndex, elementIndex), "Date format should be MM/DD/YYYY.");
                    }

                }
            }

        }

        public void CheckSignitureValidation(JbForm jbForm, ModelStateDictionary modelState)
        {
            var signitures = new List<IJbBaseElement>();

            signitures = jbForm.Elements.SelectMany(e => ((JbSection)e).Elements).Where(el => el is JbSignature).ToList();

            foreach (var signiture in signitures.Where(s => s.Validations.Any()))
            {
                var signitureValue = signiture.GetValue();

                var strSigniture = signitureValue != null ? signitureValue.ToString().Replace("image/jsignature;base30,", string.Empty) : string.Empty;

                if (string.IsNullOrEmpty(strSigniture))
                {
                    var sectionIndex = jbForm.Elements.FindIndex(s => ((JbSection)s).Elements.Any(e => e.ElementId == signiture.ElementId));

                    var section = jbForm.Elements.Single(s => ((JbSection)s).Elements.Any(e => e.ElementId == signiture.ElementId)) as JbSection;

                    var elementIndex = Ioc.FormBusiness.GetElementIndexInSection(section, signiture as JbSignature);

                    modelState.AddModelError(string.Format("JbForm.Elements[{0}].Elements[{1}].Value", sectionIndex, elementIndex), "Signiture is required");
                }
            }

        }


        public void ManipulateEntryFeePrice(RegisterProgramModel model, IEnumerable<RegisterChargeDiscountModel> changedCharges)
        {
            if (model.OrderMode == OrderMode.Offline && (model.ProgramTypeCategory == ProgramTypeCategory.Class || model.ProgramTypeCategory == ProgramTypeCategory.Camp))
            {

                var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(model.ChargeDiscountsText);
                foreach (var charge in changedCharges)
                {
                    if (charges.Any(c => c.ChargeId == charge.Id))
                    {
                        var ef = model.ProgramSchedules.SelectMany(c => c.Charges).First(c => c.Id == charge.Id);
                        ef.Amount = charge.ChangedPrice;
                        ef.ChangedPrice = charge.ChangedPrice;
                        ef.IsPriceChanged = charge.IsPriceChanged;
                        charges.First(c => c.ChargeId == charge.Id).Amount = charge.ChangedPrice;

                    }
                }
            }

        }

        public void ManipulateSubscriptionOrder(Order order, Program program, RegisterProgramModel model, long ists, DateTime uTCDate, bool isInstallmentOrder)
        {
            OrderItem orderItem;
            string itemName;
            var subscriptionBusiness = Ioc.SubscriptionBusiness;
            var schedule = program.ProgramSchedules.Last(s => !s.IsDeleted);
            var season = program.Season;
            var subscriptionAttribute = schedule.Attributes as ScheduleSubscriptionAttribute;

            var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(model.ChargeDiscountsText);

            var orderItemWaivers = new List<OrderItemWaiver>();
            if ((!model.ISTS.HasValue || model.ISTS == 0) && model.Waivers != null)
            {
                var programWaivers = program.ClubWaivers.ToList();
                foreach (var item in model.Waivers)
                {
                    var programWaiver = programWaivers.Single(p => p.Id == item.WaiverId);

                    var orderItemWaiver = new OrderItemWaiver();

                    orderItemWaiver.Signature = item.Signiture;
                    orderItemWaiver.Agreed = item.Agreed;
                    orderItemWaiver.Name = programWaiver.Name;
                    orderItemWaiver.Text = programWaiver.Text;
                    orderItemWaiver.IsRequired = programWaiver.IsRequired;
                    orderItemWaiver.WaiverConfirmationType = programWaiver.WaiverConfirmationType;

                    orderItemWaivers.Add(orderItemWaiver);
                }
            }

            var parentFormId = program.JbForm.Id;
            model.JbForm.ParentId = parentFormId;

            if (model.SubscriptionMode == SubscriptionMode.DropIn)
            {
                var selectedSessionIds = charges.Select(c => c.Id).ToList();
                var dropInsessions = subscriptionBusiness.GetDropInProgramSessions(program, selectedSessionIds);

                var gorupSessionsWithDay = dropInsessions.GroupBy(s => s.StartDateTime.Date);

                var sessionIds = new List<long>();

                foreach (var item in gorupSessionsWithDay)
                {
                    var firstsessionId = item.First().Id;
                    sessionIds.Add(firstsessionId);
                }


                var startDate = dropInsessions.Min(s => s.StartDateTime);
                var endDate = dropInsessions.Max(s => s.EndDateTime);

                decimal entryFee = Ioc.ProgramBusiness.GetDropInTotalFee(program, sessionIds);

                var title = subscriptionAttribute.DropInSessionLabel;


                var entryFeeItem = new RegisterChargeDiscountModel()
                {
                    Amount = entryFee,
                    Category = ChargeDiscountCategory.EntryFee,
                    Name = title,
                    Description = "",
                    IsPriceChanged = false
                };

                var orderCharges = new List<OrderChargeDiscount>()
                {
                    new OrderChargeDiscount()
                    {
                        Amount = entryFeeItem.Amount,
                        Category = ChargeDiscountCategory.EntryFee,
                        Name = entryFeeItem.Name,
                        Subcategory = ChargeDiscountSubcategory.Charge
                    }
                };


                itemName = " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? string.Format("{0} ({1} - {2}), {3}", program.Name, startDate.ToShortDateString(), endDate.ToShortDateString(), entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "")) : string.Format("{0} ({1} - {2}), {3}", program.Name, startDate.ToShortDateString(), endDate.ToShortDateString(), entryFeeItem.Amount.ToString().Replace(".00", "").Replace(".0", "")));

                orderItem = new OrderItem();
                orderItem.SeasonId = season.Id;
                orderItem.Name = itemName;
                orderItem.DateCreated = uTCDate;
                orderItem.ISTS = ists;
                orderItem.PlayerId = model.Player.Id;
                orderItem.FirstName = model.Player.Contact.FirstName;
                orderItem.LastName = model.Player.Contact.LastName;
                orderItem.DesiredStartDate = model.DesiredStartDate;

                orderItem.JbFormId = 0;
                orderItem.JbForm = model.JbForm;

                orderItem.ProgramTypeCategory = program.TypeCategory;
                orderItem.ProgramScheduleId = schedule.Id;
                orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schedule.Id);

                orderItem.Start = startDate;
                orderItem.End = endDate;
                orderItem.PassedPin = model.PassedPin;

                orderItem.ItemStatus = OrderItemStatusCategories.initiated;
                orderItem.EntryFeeName = entryFeeItem.Name;
                orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : entryFee;

                orderItem.OrderChargeDiscounts = orderCharges;

                orderItem.TotalAmount = entryFee;

                orderItem.OrderItemWaivers = orderItemWaivers;

                orderItem.PaymentPlanType = PaymentPlanType.FullPaid;
                orderItem.Mode = OrderItemMode.DropIn;

                order.IsAutoCharge = false;

                order.OrderItems.Add(orderItem);
            }
            else
            {
                itemName = Ioc.ProgramBusiness.GetProgramName(program.Name, schedule.StartDate, schedule.EndDate, "");


                var clubDateTime = DateTimeHelper.GetCurrentLocalDateTime(program.Club.TimeZone);
                var prorateTime = subscriptionAttribute.ProrateTime;

                var programParts = Ioc.ProgramBusiness.GetScheduleParts(program.Id);

                var chargeId = charges.First(s => s.Category == ChargeDiscountCategory.EntryFee).ChargeId;
                var totalSubscritptionAmount = subscriptionBusiness.GetTotalBeforeAfterCareAmount(schedule, model.DesiredStartDate,model.Days, chargeId);

                var selectedSubscriptionItem = subscriptionBusiness.GetSelectedCharge(schedule, chargeId);

                var firstSubscritptionTitle = selectedSubscriptionItem.Name;

                var appliedPeogramPart = subscriptionBusiness.GetAppliedProgramPart(schedule, model.DesiredStartDate);

                var acceptableItems = new List<ProgramSchedulePart>();

                if (appliedPeogramPart != null)
                {
                    acceptableItems = programParts.Where(a => a.DueDate >= appliedPeogramPart.DueDate).ToList();
                }

                var resultAppliedPeogramParts = acceptableItems.OrderBy(o => o.DueDate).ToList();

                var startDate = resultAppliedPeogramParts.Min(s => s.StartDate);
                var endDate = resultAppliedPeogramParts.Max(s => s.EndDate);

                var days = new List<DayOfWeek>();
                var orderItemattributes = new OrderItemAttributes();
                if (model.Days != null && model.Days.Any())
                {
                    days = model.Days;
                    orderItemattributes.WeekDays = days;
                }


                var charge = new OrderChargeDiscountModel()
                {
                    ChargeId = chargeId,
                    Amount = totalSubscritptionAmount,
                    Category = ChargeDiscountCategory.EntryFee,
                    Name = firstSubscritptionTitle,
                    Description = "",
                    Subcategory = ChargeDiscountSubcategory.Charge
                };

                var orderCharges = new List<OrderChargeDiscount>()
                {
                    new OrderChargeDiscount
                    {
                        ChargeId = chargeId,
                        Amount = totalSubscritptionAmount,
                        Category = ChargeDiscountCategory.EntryFee,
                        Name = firstSubscritptionTitle,
                        Description = "",
                        Subcategory = ChargeDiscountSubcategory.Charge

                    }
                };

                var entryFeeItem = new RegisterChargeDiscountModel()
                {
                    Amount = totalSubscritptionAmount,
                    Category = ChargeDiscountCategory.EntryFee,
                    Name = firstSubscritptionTitle,
                    Description = "",
                    IsPriceChanged = false
                };


                if (model.SubscriptionHasFullPayOption && !isInstallmentOrder)
                {
                    var fullPayDiscount = new OrderChargeDiscount
                    {
                        ChargeId = chargeId,
                        Amount = -subscriptionBusiness.GetFullPayDiscount(schedule, totalSubscritptionAmount),
                        Category = ChargeDiscountCategory.FullPaySubscription,
                        Name = ChargeDiscountCategory.FullPaySubscription.ToDescription(),
                        Description = "",
                        Subcategory = ChargeDiscountSubcategory.Discount
                    };

                    if (fullPayDiscount.Amount != 0)
                    {
                        orderCharges.Add(fullPayDiscount);
                    }
                }

                decimal applicationFeeAmount = 0;
                if (schedule.Charges.Any(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted))
                {
                    var applicationFeeCharge = schedule.Charges.Single(c => c.Category == ChargeDiscountCategory.ApplicationFee && !c.IsDeleted);
                    applicationFeeAmount = applicationFeeCharge.Amount;

                    if (applicationFeeCharge.Attributes.EarlyBirds != null && applicationFeeCharge.Attributes.EarlyBirds.Any())
                    {
                        foreach (var earlyBrid in applicationFeeCharge.Attributes.EarlyBirds.OrderBy(e => e.ExpireDate))
                        {
                            if (DateTime.UtcNow.Date <= earlyBrid.ExpireDate.Value.Date)
                            {
                                applicationFeeAmount = earlyBrid.Price;
                                break;
                            }
                        }
                    }

                    orderCharges.Add(new OrderChargeDiscount
                    {
                        Amount = applicationFeeAmount,
                        Category = applicationFeeCharge.Category,
                        ChargeId = applicationFeeCharge.Id,
                        Description = applicationFeeCharge.Description,
                        Name = applicationFeeCharge.Name,
                        Subcategory = ChargeDiscountSubcategory.Charge
                    }
                    );
                }

                if (model.ISTS.HasValue && order.RegularOrderItems.Any(i => i.ISTS == model.ISTS.Value && i.Name.StartsWith(itemName)))
                {

                    orderItem = order.RegularOrderItems.SingleOrDefault(item => item.ISTS == ists && item.Name.StartsWith(itemName));
                    orderItem.OrderChargeDiscounts.Clear();
                    orderItem.OrderChargeDiscounts = orderCharges;

                    orderItem.TotalAmount = orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category > 0).Sum(c => c.Amount) -
                                            orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category < 0).Sum(c => c.Amount);

                    orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : charge.Amount;

                    itemName += " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "") : charge.Amount.ToString().Replace(".00", "").Replace(".0", ""));

                    orderItem.Name = itemName;
                    orderItem.EntryFeeName = charge.Name;
                    orderItem.OrderItemWaivers = orderItemWaivers;

                    order.OrderItems.Add(orderItem);
                }
                else
                {
                    itemName += " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "") : charge.Amount.ToString().Replace(".00", "").Replace(".0", ""));
                    orderItem = new OrderItem();

                    orderItem.SeasonId = season.Id;
                    orderItem.Name = itemName;
                    orderItem.DateCreated = uTCDate;
                    orderItem.ISTS = ists;
                    orderItem.PlayerId = model.Player.Id;
                    orderItem.FirstName = model.Player.Contact.FirstName;
                    orderItem.LastName = model.Player.Contact.LastName;
                    orderItem.DesiredStartDate = model.DesiredStartDate;
                    orderItem.PassedPin = model.PassedPin;

                    orderItem.JbFormId = 0;
                    orderItem.JbForm = model.JbForm;

                    orderItem.ProgramTypeCategory = program.TypeCategory;
                    orderItem.ProgramScheduleId = schedule.Id;
                    orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schedule.Id);

                    orderItem.Start = startDate;
                    orderItem.End = endDate;

                    orderItem.ItemStatus = OrderItemStatusCategories.initiated;
                    orderItem.EntryFeeName = charge.Name;
                    orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : charge.Amount;

                    orderItem.OrderChargeDiscounts = orderCharges;

                    orderItem.TotalAmount = totalSubscritptionAmount;

                    orderItem.PaymentPlanType = isInstallmentOrder ? PaymentPlanType.Installment : PaymentPlanType.FullPaid;
                    orderItem.Attributes = orderItemattributes;
                    orderItem.OrderItemWaivers = orderItemWaivers;

                    order.IsAutoCharge = true;

                    order.OrderItems.Add(orderItem);
                }
            }
        }

        public string ManipulateSubscriptionSchedule(string chargeDiscountText, Program program, DateTime desiredStartDate)
        {
            var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(chargeDiscountText);

            var charge = charges.First(s => s.Category == ChargeDiscountCategory.EntryFee);
            var subscriptionItem = Ioc.SubscriptionBusiness.GetSubscriptionItemFromDesiredStartDate(program, desiredStartDate);

            charge.ScheduleTitle = string.Format("{0} - {1}", subscriptionItem.StartDate.ToShortDateString(), subscriptionItem.EndDate.ToShortDateString());

            var jsonSerializerSettings = new JsonSerializerSettings();
            jsonSerializerSettings.Converters.Add(new StringEnumConverter { CamelCaseText = true });

            return JsonHelper.JsonSerializer(charges, jsonSerializerSettings);
        }

        public void ManipulateBeforAfterCareOrder(Order order, Program program, RegisterProgramModel model, long ists, DateTime uTCDate, bool isInstallmentOrder, long scheduleId)
        {
            OrderItem orderItem;
            string itemName;
            var subscriptionBusiness = Ioc.SubscriptionBusiness;
            var schedule = program.ProgramSchedules.FirstOrDefault(s => s.Id == scheduleId);
            var season = program.Season;
            var subscriptionAttribute = schedule.Attributes as ScheduleSubscriptionAttribute;
            var BeforeAfterCareAttribute = schedule.Attributes as ScheduleAfterBeforeCareAttribute;

            var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(model.ChargeDiscountsText);

            var orderItemWaivers = new List<OrderItemWaiver>();
            if ((!model.ISTS.HasValue || model.ISTS == 0) && model.Waivers != null)
            {
                var programWaivers = program.ClubWaivers.ToList();
                foreach (var item in model.Waivers)
                {
                    var programWaiver = programWaivers.Single(p => p.Id == item.WaiverId);

                    var orderItemWaiver = new OrderItemWaiver();

                    orderItemWaiver.Signature = item.Signiture;
                    orderItemWaiver.Agreed = item.Agreed;
                    orderItemWaiver.Name = programWaiver.Name;
                    orderItemWaiver.Text = programWaiver.Text;
                    orderItemWaiver.IsRequired = programWaiver.IsRequired;
                    orderItemWaiver.WaiverConfirmationType = programWaiver.WaiverConfirmationType;

                    orderItemWaivers.Add(orderItemWaiver);
                }
            }

            var parentFormId = program.JbForm.Id;
            model.JbForm.ParentId = parentFormId;

            switch (model.SubscriptionMode)
            {
                case SubscriptionMode.Normal:
                    {
                        itemName = Ioc.ProgramBusiness.GetProgramName(program.Name, schedule.StartDate, schedule.EndDate, "");

                        var clubDateTime = DateTimeHelper.GetCurrentLocalDateTime(program.Club.TimeZone);
                        var prorateTime = program.TypeCategory == ProgramTypeCategory.Subscription ? subscriptionAttribute.ProrateTime : BeforeAfterCareAttribute.ProrateTime;

                        var chargeId = charges.First(s => s.Category == ChargeDiscountCategory.EntryFee).ChargeId;

                        var totalSubscritptionAmount = subscriptionBusiness.GetTotalBeforeAfterCareAmount(schedule, model.DesiredStartDate,model.Days, chargeId);

                        var selectedChargeItem = subscriptionBusiness.GetSelectedCharge(schedule, chargeId);

                        var firstSubscritptionTitle = selectedChargeItem.Name;

                        DateTime startDate;
                        DateTime endDate;

                        var programParts = Ioc.ProgramBusiness.GetScheduleParts(program.Id);
                        var appliedPeogramPart = subscriptionBusiness.GetAppliedProgramPart(schedule, model.DesiredStartDate);

                        var acceptableItems = new List<ProgramSchedulePart>();

                        if (appliedPeogramPart != null)
                        {
                            acceptableItems = programParts.Where(a => a.DueDate >= appliedPeogramPart.DueDate).ToList();
                        }

                        var resultAppliedPeogramParts = acceptableItems.OrderBy(o => o.DueDate).ToList();

                        startDate = resultAppliedPeogramParts.Min(s => s.StartDate);
                        endDate = resultAppliedPeogramParts.Max(s => s.EndDate);

                        var days = new List<DayOfWeek>();
                        var orderItemattributes = new OrderItemAttributes();
                        if (model.Days != null && model.Days.Any())
                        {
                            days = model.Days;
                            orderItemattributes.WeekDays = days;
                        }

                        var charge = new OrderChargeDiscountModel()
                        {
                            ChargeId = chargeId,
                            Amount = totalSubscritptionAmount,
                            Category = ChargeDiscountCategory.EntryFee,
                            Name = firstSubscritptionTitle,
                            Description = "",
                            Subcategory = ChargeDiscountSubcategory.Charge
                        };

                        var orderCharges = new List<OrderChargeDiscount>()
                        {
                            new OrderChargeDiscount
                            {
                                ChargeId = chargeId,
                                Amount = totalSubscritptionAmount,
                                Category = ChargeDiscountCategory.EntryFee,
                                Name = firstSubscritptionTitle,
                                Description = "",
                                Subcategory = ChargeDiscountSubcategory.Charge

                            }
                        };

                        var entryFeeItem = new RegisterChargeDiscountModel()
                        {
                            Amount = totalSubscritptionAmount,
                            Category = ChargeDiscountCategory.EntryFee,
                            Name = firstSubscritptionTitle,
                            Description = "",
                            IsPriceChanged = false
                        };


                        if (model.SubscriptionHasFullPayOption && !isInstallmentOrder)
                        {
                            var fullPayDiscount = new OrderChargeDiscount
                            {
                                ChargeId = chargeId,
                                Amount = -subscriptionBusiness.GetFullPayDiscount(schedule, totalSubscritptionAmount),
                                Category = ChargeDiscountCategory.FullPaySubscription,
                                Name = ChargeDiscountCategory.FullPaySubscription.ToDescription(),
                                Description = "",
                                Subcategory = ChargeDiscountSubcategory.Discount
                            };

                            if (fullPayDiscount.Amount != 0)
                            {
                                orderCharges.Add(fullPayDiscount);
                            }
                        }

                        if (model.ISTS.HasValue && order.RegularOrderItems.Any(i => i.ISTS == model.ISTS.Value && i.Name.StartsWith(itemName)))
                        {
                            orderItem = order.RegularOrderItems.SingleOrDefault(item => item.ISTS == ists && item.Name.StartsWith(itemName));
                            orderItem.OrderChargeDiscounts.Clear();
                            orderItem.OrderChargeDiscounts = orderCharges;

                            orderItem.OrderItemScheduleParts.Clear();
                            orderItem.OrderItemScheduleParts = Ioc.OrderItemBusiness.SetOrderItemsScheduleParts(orderItem, model.DesiredStartDate, chargeId);

                            orderItem.TotalAmount = orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category > 0).Sum(c => c.Amount) -
                                                    orderItem.GetOrderChargeDiscounts().Where(c => (short)c.Category < 0).Sum(c => c.Amount);

                            orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : charge.Amount;

                            itemName += " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "") : charge.Amount.ToString().Replace(".00", "").Replace(".0", ""));

                            orderItem.Name = itemName;
                            orderItem.EntryFeeName = charge.Name;
                            orderItem.OrderItemWaivers = orderItemWaivers;
                            order.OrderItems.Add(orderItem);
                        }
                        else
                        {
                            itemName += " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "") : charge.Amount.ToString().Replace(".00", "").Replace(".0", ""));
                            orderItem = new OrderItem();

                            orderItem.SeasonId = season.Id;
                            orderItem.Name = itemName;
                            orderItem.DateCreated = uTCDate;
                            orderItem.ISTS = ists;
                            orderItem.PlayerId = model.Player.Id;
                            orderItem.FirstName = model.Player.Contact.FirstName;
                            orderItem.LastName = model.Player.Contact.LastName;
                            orderItem.DesiredStartDate = model.DesiredStartDate;
                            orderItem.PassedPin = model.PassedPin;

                            orderItem.JbFormId = 0;
                            orderItem.JbForm = model.JbForm;

                            orderItem.ProgramTypeCategory = program.TypeCategory;
                            orderItem.ProgramScheduleId = schedule.Id;
                            orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schedule.Id);

                            orderItem.Start = startDate;
                            orderItem.End = endDate;

                            orderItem.ItemStatus = OrderItemStatusCategories.initiated;
                            orderItem.EntryFeeName = charge.Name;
                            orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : charge.Amount;

                            orderItem.OrderChargeDiscounts = orderCharges;

                            orderItem.TotalAmount = totalSubscritptionAmount;

                            orderItem.PaymentPlanType = isInstallmentOrder ? PaymentPlanType.Installment : PaymentPlanType.FullPaid;
                            orderItem.Attributes = orderItemattributes;
                            orderItem.OrderItemWaivers = orderItemWaivers;
                            orderItem.OrderItemScheduleParts = Ioc.OrderItemBusiness.SetOrderItemsScheduleParts(orderItem, model.DesiredStartDate, chargeId);

                            order.IsAutoCharge = true;

                            order.OrderItems.Add(orderItem);
                        }
                    }
                    break;
                case SubscriptionMode.DropIn:
                    {
                        var selectedSessionIds = charges.Select(c => c.Id).ToList();
                        var dropInsessions = subscriptionBusiness.GetDropInProgramSessions(program, selectedSessionIds);

                        var startDate = dropInsessions.Min(s => s.StartDateTime);
                        var endDate = dropInsessions.Max(s => s.EndDateTime);

                        var gorupSessionsWithDay = dropInsessions.GroupBy(s => s.StartDateTime.Date);
                        var sessionIds = new List<long>();

                        foreach (var item in gorupSessionsWithDay)
                        {
                            var firstsessionId = item.First().Id;
                            sessionIds.Add(firstsessionId);
                        }

                        var title = string.Empty;
                        decimal entryFee = Ioc.ProgramBusiness.GetDropInTotalFee(program, sessionIds, schedule);

                        if (program.TypeCategory == ProgramTypeCategory.Subscription)
                        {
                            title = subscriptionAttribute.DropInSessionLabel;
                        }
                        else
                        {
                            title = BeforeAfterCareAttribute.DropInSessionLabel;
                        }

                        var entryFeeItem = new RegisterChargeDiscountModel()
                        {
                            Amount = entryFee,
                            Category = ChargeDiscountCategory.EntryFee,
                            Name = title,
                            Description = "",
                            IsPriceChanged = false
                        };

                        var orderCharges = new List<OrderChargeDiscount>()
                        {
                            new OrderChargeDiscount()
                            {
                                Amount = entryFeeItem.Amount,
                                Category = ChargeDiscountCategory.EntryFee,
                                Name = entryFeeItem.Name,
                                Subcategory = ChargeDiscountSubcategory.Charge
                            }
                        };

                        itemName = " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? string.Format("{0} ({1} - {2}), {3}", program.Name, startDate.ToShortDateString(), endDate.ToShortDateString(), entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "")) : string.Format("{0} ({1} - {2}), {3}", program.Name, startDate.ToShortDateString(), endDate.ToShortDateString(), entryFeeItem.Amount.ToString().Replace(".00", "").Replace(".0", "")));

                        orderItem = new OrderItem();
                        orderItem.SeasonId = season.Id;
                        orderItem.Name = itemName;
                        orderItem.DateCreated = uTCDate;
                        orderItem.ISTS = ists;
                        orderItem.PlayerId = model.Player.Id;
                        orderItem.FirstName = model.Player.Contact.FirstName;
                        orderItem.LastName = model.Player.Contact.LastName;
                        orderItem.DesiredStartDate = model.DesiredStartDate;
                        orderItem.PassedPin = model.PassedPin;

                        orderItem.JbFormId = 0;
                        orderItem.JbForm = model.JbForm;

                        orderItem.ProgramTypeCategory = program.TypeCategory;
                        orderItem.ProgramScheduleId = schedule.Id;
                        orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schedule.Id);

                        orderItem.Start = startDate;
                        orderItem.End = endDate;

                        orderItem.ItemStatus = OrderItemStatusCategories.initiated;
                        orderItem.EntryFeeName = entryFeeItem.Name;
                        orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : entryFee;

                        orderItem.OrderChargeDiscounts = orderCharges;

                        orderItem.TotalAmount = entryFee;

                        orderItem.PaymentPlanType = PaymentPlanType.FullPaid;
                        orderItem.Mode = OrderItemMode.DropIn;
                        orderItem.OrderItemWaivers = orderItemWaivers;
                        order.IsAutoCharge = false;

                        order.OrderItems.Add(orderItem);
                    }
                    break;
                case SubscriptionMode.PunchCard:
                    {

                        var startDate = schedule.StartDate;
                        var endDate = schedule.EndDate;

                        var title = string.Empty;
                        decimal entryFee = BeforeAfterCareAttribute.PunchCard.Amount.Value;

                        title = BeforeAfterCareAttribute.PunchCard.Title;

                        var entryFeeItem = new RegisterChargeDiscountModel()
                        {
                            Amount = entryFee,
                            Category = ChargeDiscountCategory.EntryFee,
                            Name = title,
                            Description = "",
                            IsPriceChanged = false
                        };

                        var orderCharges = new List<OrderChargeDiscount>()
                        {
                            new OrderChargeDiscount()
                            {
                                Amount = entryFeeItem.Amount,
                                Category = ChargeDiscountCategory.EntryFee,
                                Name = entryFeeItem.Name,
                                Subcategory = ChargeDiscountSubcategory.Charge
                            }
                        };

                        itemName = string.Format("{0} ({1} - {2}), {3}", program.Name, startDate.ToShortDateString(), endDate.ToShortDateString(), entryFeeItem.Amount.ToString().Replace(".00", "").Replace(".0", ""));

                        orderItem = new OrderItem();
                        orderItem.SeasonId = season.Id;
                        orderItem.Name = itemName;
                        orderItem.DateCreated = uTCDate;
                        orderItem.ISTS = ists;
                        orderItem.PlayerId = model.Player.Id;
                        orderItem.FirstName = model.Player.Contact.FirstName;
                        orderItem.LastName = model.Player.Contact.LastName;
                        orderItem.DesiredStartDate = model.DesiredStartDate;

                        orderItem.JbFormId = 0;
                        orderItem.JbForm = model.JbForm;

                        orderItem.ProgramTypeCategory = program.TypeCategory;
                        orderItem.ProgramScheduleId = schedule.Id;
                        orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == schedule.Id);

                        orderItem.Start = startDate;
                        orderItem.End = endDate;
                        orderItem.PassedPin = model.PassedPin;

                        orderItem.ItemStatus = OrderItemStatusCategories.initiated;
                        orderItem.EntryFeeName = entryFeeItem.Name;
                        orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : entryFee;

                        orderItem.OrderChargeDiscounts = orderCharges;

                        orderItem.TotalAmount = entryFee;

                        orderItem.PaymentPlanType = PaymentPlanType.FullPaid;
                        orderItem.Mode = OrderItemMode.PunchCard;
                        orderItem.OrderItemWaivers = orderItemWaivers;
                        orderItem.AttributesSerialized = JsonConvert.SerializeObject(new OrderItemAttributes { DayNumbers = BeforeAfterCareAttribute.PunchCard.SessionNumber });

                        order.IsAutoCharge = false;

                        order.OrderItems.Add(orderItem);
                    }
                    break;
                default:
                    break;
            }
        }

        public void ManipulateProgramDropIn(Order order, Program program, RegisterProgramModel model, long ists, DateTime uTCDate, bool isInstallmentOrder, long scheduleId)
        {
            OrderItem orderItem;
            string itemName;
            var season = program.Season;
            var charges = JsonHelper.JsonDeserialize<List<OrderChargeDiscountModel>>(model.ChargeDiscountsText).Where(c => c.ScheduleId == scheduleId);
            var selectedSessionIds = charges.Select(c => c.Id).ToList();

            var dropInsessions = Ioc.ProgramSessionBusiness.GetList(p => selectedSessionIds.Contains(p.Id));
            var startDate = dropInsessions.Min(s => s.StartDateTime);
            var endDate = dropInsessions.Max(s => s.EndDateTime);

            var title = string.Empty;
            decimal entryFee = Ioc.ProgramBusiness.GetDropInTotalFee(program, selectedSessionIds);

            var charge = program.ProgramSchedules.SelectMany(s => s.Charges).FirstOrDefault(c => c.Id == dropInsessions.First().ChargeId);
            title = ((ScheduleAttribute)charge.ProgramSchedule.Attributes).DropInLable;

            var entryFeeItem = new RegisterChargeDiscountModel()
            {
                Amount = entryFee,
                Category = ChargeDiscountCategory.EntryFee,
                Name = title,
                Description = "",
                IsPriceChanged = false
            };

            var orderCharges = new List<OrderChargeDiscount>()
                        {
                            new OrderChargeDiscount()
                            {
                                Amount = entryFeeItem.Amount,
                                Category = ChargeDiscountCategory.EntryFee,
                                Name = entryFeeItem.Name,
                                Subcategory = ChargeDiscountSubcategory.Charge
                            }
                        };

            itemName = " " + ((model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? string.Format("{0} ({1} - {2}), {3}", program.Name, startDate.ToShortDateString(), endDate.ToShortDateString(), entryFeeItem.ChangedPrice.ToString().Replace(".00", "").Replace(".0", "")) : string.Format("{0} ({1} - {2}), {3}", program.Name, startDate.ToShortDateString(), endDate.ToShortDateString(), entryFeeItem.Amount.ToString().Replace(".00", "").Replace(".0", "")));

            orderItem = new OrderItem();
            orderItem.SeasonId = season.Id;
            orderItem.Name = itemName;
            orderItem.DateCreated = uTCDate;
            orderItem.ISTS = ists;
            orderItem.PlayerId = model.Player.Id;
            orderItem.FirstName = model.Player.Contact.FirstName;
            orderItem.LastName = model.Player.Contact.LastName;
            orderItem.DesiredStartDate = model.DesiredStartDate;
            orderItem.PassedPin = model.PassedPin;

            var parentFormId = program.JbForm.Id;
            model.JbForm.ParentId = parentFormId;
            orderItem.JbFormId = 0;
            orderItem.JbForm = model.JbForm;

            orderItem.ProgramTypeCategory = program.TypeCategory;
            orderItem.ProgramScheduleId = scheduleId;
            orderItem.ProgramSchedule = program.ProgramSchedules.First(s => s.Id == scheduleId);

            orderItem.Start = startDate;
            orderItem.End = endDate;

            orderItem.ItemStatus = OrderItemStatusCategories.initiated;
            orderItem.EntryFeeName = entryFeeItem.Name;
            orderItem.EntryFee = (model.OrderMode == OrderMode.Offline && entryFeeItem.IsPriceChanged) ? entryFeeItem.ChangedPrice : entryFee;

            orderItem.OrderChargeDiscounts = orderCharges;

            orderItem.TotalAmount = entryFee;

            orderItem.PaymentPlanType = PaymentPlanType.FullPaid;
            orderItem.Mode = OrderItemMode.DropIn;

            order.IsAutoCharge = false;

            foreach (var programSession in dropInsessions)
            {
                var orderSession = new OrderSession()
                {
                    ProgramSessionId = programSession.Id,
                    MetaData = new MetaData()
                    {
                        DateCreated = DateTime.UtcNow,
                        DateUpdated = DateTime.UtcNow,
                    }
                };

                orderItem.OrderSessions.Add(orderSession);
            }

            order.OrderItems.Add(orderItem);

        }
    }
}