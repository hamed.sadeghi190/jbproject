﻿

namespace Jumbula.Common.Enums
{
    public enum TourneyCoverageType
    {
        Pairing = 0,
        Standing = 1,
        Result = 2,
        Image = 3
    }
}
