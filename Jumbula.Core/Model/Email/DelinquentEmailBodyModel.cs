﻿using Jumbula.Common.Constants;

namespace Jumbula.Core.Model.Email
{
    public class DelinquentEmailBodyModel
    {

        public DelinquentEmailBodyModel()
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
            ClubUrl = $"{RootSiteUrl}/{ClubDomain}";
        }

        public string ClubDomain { get; set; }
        public string ClubImageUrl { get; set; }
        public string ClubName { get; set; }
        public string ClubUrl { get; set; }
        public string ClubEmail { get; set; }
        public string ClubPhone { get; set; }
        public string RootSiteUrl { get; set; }
        public string BodyMailTitle { get; set; }
    }
}
