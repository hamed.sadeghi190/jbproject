﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AllESignatureViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Company { get; set; }
        public string Status { get; set; }
        public string DocumentUrl { get; set; }
        public DateTime CreateDocument { get; set; }
        public bool IsMember { get; set; }
        public string StrCreateDocument
        {
            get
            {

                return CreateDocument.ToString("MMM dd, yyyy");
            }
        }
        public DateTime? DeadLine { get; set; }

        public string StrDeadLine
        {
            get
            {

                return DeadLine.HasValue? DeadLine.Value.ToString("MMM dd, yyyy"):"-";
            }
        }
    }
}