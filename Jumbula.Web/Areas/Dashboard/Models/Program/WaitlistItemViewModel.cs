﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace SportsClub.Areas.Dashboard.Models
{
    public class WaitlistItemViewModel : BaseViewModel
    {
        public string Email { get; set; }

        public string ParticipantName { get; set; }

        public string Grade { get; set; }

        public string Schedule { get; set; }
        public string PhoneNumber { get; set; }

        public ProgramTypeCategory ProgramType { get; set; }
        public int UserId { get; set; }
        public string Date { get; set; }
        public long ScheduleId { get; set; }
    }
}