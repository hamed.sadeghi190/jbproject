﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MailChimp.Ecomm;
using System.Globalization;

namespace SportsClub.Models
{
    public class ParentInvoiceInstallmentViewModel
    {
        public DateTime DueDate { get; set; }

        public string StrDueDate
        {
            get
            {

                return DueDate.ToString("MMM dd, yyyy");
            }
        }
        public DateTime? LastDueDate { get; set; }
        public long OrderItemId { get; set; }
        public string StrPaidDate
        {
            get
            {
                return LastDueDate.HasValue ? LastDueDate.Value.ToString("MMM dd, yyyy") : "-";
            }
        }
        public decimal? PaidAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal? Balance
        {
            get
            {

                return Amount - PaidAmount;
            }
        }


        public long id { get; set; }
        public decimal InvoiceAmount { get; set; }
    }
}