﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class FSAReportItemViewModel
    {
        public int Id { get; set; }

        public string StudentName { get; set; }

        public string ClassName { get; set; }

        public string ClassMeetingDates { get; set; }

        public string MethodOfPayment { get; set; }

        public decimal AmountPaid { get; set; }

        public string ProviderName { get; set; }

        public string ProviderTaxId { get; set; }

        public string Date { get; set; }
        public string ScheduleDate { get; set; }

    }
}