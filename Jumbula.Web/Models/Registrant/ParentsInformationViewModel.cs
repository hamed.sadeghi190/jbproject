﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ParentsInformationViewModel
    {
        public int Age { get; set; }
        public int Id { get; set; }

        public PageStep PageStep { get; set; }
   
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }

        public string Zip { get; set; }
        
        public string State { get; set; }
        public string TxtState { get; set; }
        public string City { get; set; }

        public string Country { get; set; }

        public int PlayerId { get; set; }

        [Display(Name = "First name")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete first name.")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        [Required(ErrorMessage = "{0} is required.")]
        [StringLength(64, ErrorMessage = "{0} is too long.")]
        [MinLength(2, ErrorMessage = "Enter a complete last name.")]
        public string LastName { get; set; }

        [Display(Name = "Date of birth")]
        public string DOB { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "The phone number is invalid.")]
        [Display(Name = "Primary phone")]
        public string PrimaryPhone { get; set; }
        public string Employer { get; set; }
        public string Occupation { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "The phone number is invalid.")]
        [Display(Name = "Alternate phone")]
        public string CellPhone { get; set; }
        public string FullName { get; set; }

        [EmailAddress(ErrorMessage = "Email is not a valid email address.")]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public int UserId { get; set; }
        public int ContactId { get; set; }
        public PageMode PageMode { get; set; }

        [Display(Name = "Gender")]
        public GenderCategories GenderList { get; set; }
        public RelationshipType PlayerType { get; set; }

        [Display(Name = "Relationship")]
        public ParentRelationship? ParentRelationship { get; set; }
        
    }
    
}