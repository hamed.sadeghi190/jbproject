﻿
using System.ComponentModel.DataAnnotations;


namespace SportsClub.Areas.Dashboard.Models
{
    public class FlyersSettingsViewModel
    {

        public FlyersSettingsViewModel()
        {
         
        }


        [Required(ErrorMessage = "Phone is required.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Email is required.")]
        public string Email { get; set; }
       

 
    }

     
}