﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('lotteryController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation) {
        window.app.globalObjects.pageClass = 'seasons-view program';
        window.app.globalObjects.initializeView();

        if (!$scope.MC.account.isPartner) {
            $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });
        }

        $scope.MC.stateParams = $stateParams;

        $scope.Model = {};

        $scope.IsDisabled = true;

        var programId = $stateParams.programId;

        $scope.CheckedIds = [];

        var getModel = function () {
            $http.get(window.location.origin + '/dashboard/Order/GetLotteryDetails/?programId=' + programId).success(function (data, status, headers, config) {
                $scope.Model = data;

            }).error(function (data, status, headers, config) {
                jbToast.error(data);
            });
        }

        getModel();

        var manipulateGrid = function () {
            $scope.orderDataSource = new kendo.data.DataSource({
                type: "json",
                transport: {
                    read: {
                        url: window.location.origin + '/Dashboard/Order/GetLotteryItems?programId=' + programId,
                        contentType: "application/json; charset=utf-8",
                        type: "POST"
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            data = $.extend(
                                {
                                    sort: null,
                                    filter: null,
                                }, data);
                            return JSON.stringify(data);
                        }
                    }
                },
                pageSize: 50,
                batch: true,
                serverPaging: true,
                serverSorting: false,
                schema:
                    {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                sortable: true
            });

            $scope.orderGridOptions = {
                dataSource: $scope.orderDataSource,
                serverPaging: true,
                columns: [
                    {
                        template: "<input type='checkbox' class='checkbox' />",
                        width: "30px",
                        headerTemplate: '<input type="checkbox" class="checkAll" />'
                    },
                    {
                        field: "Attendee",
                        title: "Participant",
                        width: "150px",
                        template: "<a ngaction='OrderDetail_View' ui_sref='OrderItem({seasonDomain:MC.stateParams.seasonDomain ,orderItemId:  #=Id#})'  class='block'> #= Attendee # </a>" 
                    },
                    {
                        field: "OrderDate",
                        title: "Order date",
                        width: "140px",
                    },
                    {
                        field: "EntryFeeName",
                        title: 'Tuition option',
                        width: '140px',
                        template: '<span> #=EntryFeeName# (#=EntryFee#)</span>'
                    },
                    {
                        field: "LotteryStatus",
                        title: "Lottery status ",
                        width: "120px",
                        hidden: $scope.hideConfirmation,
                        template: "<span>#if(ItemStatus == 'completed'){# <i class='grid-label label bg-green  fg-white'>Completed</i> #}else if(LotteryStatus == 'Condidate') {# <i class='grid-label label bg-yellow  fg-white'>Condidate</i> #}else if(LotteryStatus == 'Won') {#<i class='grid-label label bg-blue fg-white'>Won</i> #}#"
                    },
                    {
                        field: "",
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul ng-show='isActionsEnabled()'  kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                                <li> ... \
                                    <ul>\
                                        <li ng-show='#= EnableMarkAsCondidate #' ng-click='addLotteryItem(#= Id #)'>\
                                                <span class='k-link jbi-plus'>Mark as candidate</span>\
                                        </li>\
                                        <li ng-show='#= LotteryStatus == 'Condidate' #' ng-click='removeLotteryItem(#= Id #)'>\
                                                <span class='k-link jbi-remove'>Remove candidate</span>\
                                        </li>\
                                        <li ng-show='#= LotteryStatus == 'Won' && ItemStatus != 'completed' #' ng-click='completeRegistration(#= Id #)'>\
                                                <span class='k-link jbi-register'>Complete registration</span>\
                                        </li>\
                                    </ul>\
                                </li>\
                              </ul>"
                    }

                ],
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if ($scope.CheckedIds[view[i].Id]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .find(".checkbox")
                                .attr("checked", "checked");
                        }
                    }
                }
            };
        }

        manipulateGrid();

        $(document.body).on('click', '#orderGrid table .checkbox', selectRow);
        $(document.body).on('click', '#orderGrid table .checkAll', checkAll);

        function selectRow() {
            var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#orderGrid").data("kendoGrid"),
                dataItem = grid.dataItem(row);

            $scope.CheckedIds[dataItem.Id] = checked;
        }

        function checkAll() {
            var grid = $("#orderGrid").data("kendoGrid");
            var state = $("#orderGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
            var ckbox;
            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                ckbox = $this.find("td .checkbox");
                ckbox.prop("checked", state);
                var dataItem = grid.dataItem($this);
                $scope.CheckedIds[dataItem.Id] = state;
            });
        }


        $scope.isActionsEnabled = function () {
            return $scope.Model.EnableDraw || $scope.Model.EnableFinalize || $scope.Model.IsLotteryOpenForAdmin;
        }

        $scope.removeLotteryItem = function (id) {

            $http.post(window.location.origin + '/Dashboard/Order/RemoveLotteryItem/', { orderItemId: id })
                .success(function (data, status, headers, config) {
                    $("#orderGrid").data("kendoGrid").dataSource.read();
                    getModel();
                    jbToast.success('Successfully removed the candidate');
                });
        }

        $scope.addLotteryItem = function (id) {

            $http.post(window.location.origin + '/Dashboard/Order/AddLotteryItem/', { orderItemId: id })
                .success(function (data, status, headers, config) {
                    $("#orderGrid").data("kendoGrid").dataSource.read();
                    getModel();
                    jbToast.success(data.Message);
                });
        }

        $scope.draw = function () {

            if (!$scope.Model.EnableDraw)
                return;

            jbConfirmation
                .yes(function () {
                    $http.post(window.location.origin + '/Dashboard/Order/Draw/', { programId: programId })
                        .success(function (data, status, headers, config) {
                            if (data.Status) {
                                $("#orderGrid").data("kendoGrid").dataSource.read();

                                for (var i = 0; i < data.SelectedIds.length; i++) {
                                    $scope.CheckedIds[data.SelectedIds[i]] = true;
                                }

                                jbToast.success('Draw done successfully');
                            }
                            else {
                                jbToast.error(data.Message);
                            }

                            getModel();
                        });
                })
                .title('Draw lottery')
                .message('Are you sure you want to draw the lottery?')
                .confirm();
        }

        $scope.finalize = function () {

            if (!$scope.Model.EnableFinalize) 
                return;

            jbConfirmation
                .yes(function () {
                    $http.post(window.location.origin + '/Dashboard/Order/FinalizeLottery/', { programId: programId })
                        .success(function (data, status, headers, config) {
                            $("#orderGrid").data("kendoGrid").dataSource.read();
                            getModel();
                            jbToast.success('Lottery was finalized successfully');
                        });
                })
                .title('Finalize lottery')
                .message('After you finalize, lottery winners can register for the class. This action is final and you cannot undo it.')
                .confirm();
        }

        $scope.completeRegistration = function (orderItemId) {
            jbConfirmation
                .yes(function () {
                    $http.post(window.location.origin + '/Dashboard/Order/CompleteLotteryItem/', { orderItemId: orderItemId })
                        .success(function (data, status, headers, config) {
                            $("#orderGrid").data("kendoGrid").dataSource.read();
                            getModel();
                            jbToast.success('Order completed');
                        });
                })
                .title('Complete order')
                .message('Are you sure you want to complete the registration on behalf of the participant?')
                .confirm();
        }

        $scope.MakeCampaign = function () {
            var checked = [];

            for (var i in $scope.CheckedIds) {
                if ($scope.CheckedIds[i]) {
                    checked.push(i);
                }
            }

            if (checked.length > 0) {
                $http.post(window.location.origin + "/dashboard/Order/SendEmailToLotteries", { orderItemIds: checked })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                        } else {
                            jbToast.error(data.Message);
                        }
                    }).error(function (data, status, headers, config) {
                    });
            } else {
                jbToast.error("You must select at least one order");
            }
        };

    }])
})();
