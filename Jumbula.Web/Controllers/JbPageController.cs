﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using Constants = Jumbula.Common.Constants.Constants;
using StaticRandom = Jumbula.Common.Utilities.StaticRandom;

namespace Jumbula.Web.Controllers
{
    [AllowAnonymous]
    public class JbPageController : JbBaseController
    {
        #region Fields
        private readonly object _thisLock = new object();
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IProgramBusiness _programBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IPageBusiness _pageBusiness;
        private readonly IOrderBusiness _orderBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IUserProfileBusiness _userProfileBusiness;
        private readonly IPlayerProfileBusiness _playerProfileBusiness;
        private readonly IClientBusiness _clientBusiness;
        #endregion

        #region Constractors
        public JbPageController(IProgramBusiness programBusiness, IOrderItemBusiness orderItemBusiness, IClubBusiness clubBusiness, IPageBusiness pageBusiness, IOrderBusiness orderBusiness, ISeasonBusiness seasonBusiness, IUserProfileBusiness userProfileBusiness, IPlayerProfileBusiness playerProfileBusiness, IClientBusiness clientBusiness)
        {
            _programBusiness = programBusiness;
            _orderItemBusiness = orderItemBusiness;
            _clubBusiness = clubBusiness;
            _pageBusiness = pageBusiness;
            _orderBusiness = orderBusiness;
            _seasonBusiness = seasonBusiness;
            _userProfileBusiness = userProfileBusiness;
            _playerProfileBusiness = playerProfileBusiness;
            _clientBusiness = clientBusiness;
        }
        #endregion

        public virtual ActionResult Index(string clubDomain, string mode = "", bool iframeMode = false)
        {
            if (!_clubBusiness.IsClubDomainExist(ClubSubDomain))
            {
                PageNotFound();
            }

            if (!string.IsNullOrEmpty(mode) && mode.Equals("edit", StringComparison.OrdinalIgnoreCase))
            {
                if (!this.IsUserAuthenticated() ||
                    (this.IsUserAuthenticated() && !(this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)))
                {
                    //PageNotFound();
                    return RedirectToAction("Login", "Account", new { returnUrl = "/edit" });
                }
            }
            else
            {
                //preview mode for club admin
                if (!string.IsNullOrEmpty(mode) && mode.Equals("view", StringComparison.OrdinalIgnoreCase))
                {
                    if (!this.IsUserAuthenticated() ||
                        (this.IsUserAuthenticated() && !(this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard)))
                    {
                        //return RedirectToAction("Login", "Account", new { returnUrl = "/view" });
                        return Redirect("/");
                    }
                }
                else
                {
                    if (!iframeMode && this.IsUserAuthenticated() &&
                        (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard) && !JbUserService.IsSwitchedAsParentMode())
                    {
                        return Redirect(Constants.Jumbula_Dashbaord_Url);
                    }
                }
            }

            ViewBag.ClubDomain = clubDomain;

            #region GA
            var gaModel = _clientBusiness.GetToolboxGoogleAnalytics(this.GetCurrentClubId());

            if (!string.IsNullOrEmpty(gaModel.TrackingId))
                ViewBag.GATrackingId = gaModel.TrackingId;
            #endregion

            return View();
        }

        [HttpGet]
        public virtual JsonNetResult Manage(string clubDomain, string seasonDomain, SaveType? saveType,
            bool isIframe = false, bool preview = false, string mode = "")
        {
            var club = _clubBusiness.Get(clubDomain);

            //we may pass saveType in design mode to load specific mode (Draft/Publish)
            if (!isIframe && this.IsUserAuthenticated() &&
                (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard))
            {
                saveType = saveType.HasValue ? saveType : SaveType.Draft;
            }
            else
            {
                saveType = SaveType.Publish;
            }
            var page = _pageBusiness.Get(club.Id, saveType);

            //I lock this block to prevent insert duplicate row during multi referesh page
            lock (_thisLock)
            {
                if (page.Id == 0 && string.IsNullOrEmpty(page.JsonBody))
                {
                    page = _pageBusiness.GetDefaultPage(club);

                    page.Header.LogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);

                    if (string.IsNullOrEmpty(page.Header.LogoUrl))
                    {
                        page.Header.LogoUrl = "~/Images/club-nosport.png";
                    }

                    if (saveType == SaveType.Publish)
                    {
                        page.SaveType = SaveType.Publish;
                    }

                    _pageBusiness.CreateEdit(page);
                }
            }

            page.Header.LogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
            if (club.Setting != null)
            {
                page.Header.LogoRedirecturl = UrlHelpers.GetProperUrl(club.Setting.RegistrationPageLogoRedirectUrl,
                    club.Site, Request.Url.GetLeftPart(UriPartial.Authority));
            }
            else
            {
                page.Header.LogoRedirecturl = UrlHelpers.GetProperUrl(new ClubRedirectUrl(), club.Site,
                    Request.Url.GetLeftPart(UriPartial.Authority));
            }
            if (string.IsNullOrEmpty(page.Header.LogoUrl))
            {
                page.Header.LogoUrl = "~/Images/club-nosport.png";
            }

            //By MGH: Schedules object is null in page json, we fill it! 
            var grids =
                page.Elements.Where(e => e is JbPTab)
                    .SelectMany(e => ((JbPTab)e).Elements)
                    .Where(e => e is JbPGrid)
                    .Select(e => e as JbPGrid)
                    .ToList();

            foreach (var grid in grids)
            {
                var links = grid.RegLinks.ToList();
                UpdateRegLinks(links);
            }
            var sections =
                page.Elements.Where(e => e is JbPTab)
                    .SelectMany(e => ((JbPTab)e).Elements)
                    .Where(e => e is JbPSection)
                    .Select(e => e as JbPSection)
                    .ToList();
            foreach (var section in sections)
            {
                var links = section.Columns.SelectMany(c => c.RegLinks).ToList();
                UpdateRegLinks(links);
            }

            // if in the tab we have before after
            var beforeAfterRegLink = page.Elements.Where(e => e is JbPTab)
                   .SelectMany(e => ((JbPTab)e).Elements)
                   .Where(e => e is JbPBeforeAfterCareRegLinks)
                   .Select(e => e as JbPBeforeAfterCareRegLinks)
                   .ToList();

            if (beforeAfterRegLink.Count > 0)
            {
                UpdateBeforeAfterLinks(beforeAfterRegLink);
            }

            if (isIframe)
            {
                page.PageMode = JbPMode.Iframe;
            }
            else if (preview && this.IsUserAuthenticated() && (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard))
            {
                page.PageMode = JbPMode.Preview;
            }
            else if (this.IsUserAuthenticated() && (this.GetCurrentUserRoleType() == RoleCategoryType.Dashboard) && (!JbUserService.IsSwitchedAsParentMode() || mode == "edit"))
            {
                page.PageMode = JbPMode.Design;
            }
            else
            {
                page.PageMode = JbPMode.View;
            }

            if (club.ClubLocations != null && club.ClubLocations.Any())
            {
                page.Address.Lat = club.ClubLocations.First().PostalAddress.Lat;
                page.Address.Lng = club.ClubLocations.First().PostalAddress.Lng;
            }

            return JsonNet(page);
        }

        [Authorize(Roles = "Admin, Manager")]
        public virtual JsonNetResult GetElementTemplates()
        {
            var club = _clubBusiness.Get(ClubSubDomain);

            dynamic elementTemplates = new
            {
                JbPTitlePack = new JbPTitlePack
                {
                    Title = "Title",
                    Subtitle = "Sub title",
                    Paragraph = ""
                },
                JbPDivider = new JbPDivider
                {
                    DividerHeight = 50,
                    LineThickness = 1,
                    LineColor = "rgba(1,1,1,1)"
                },
                JbPSection = new JbPSection
                {
                    Title = "Title",
                    ColumnCount = JbPColumnCount.Two,
                    Columns = new List<JbPSectionColumn>
                    {
                        new JbPSectionColumn
                        {
                            Title = "Title"
                        },
                        new JbPSectionColumn
                        {
                            Title = "Title"
                        }
                    }
                },
                JbPGrid = new JbPGrid
                {
                    Title = "Title"
                },
                JbPRegLink = new JbPRegLink
                {
                    Title = "Title",
                    LinkUrl = "#",
                    LinkTitle = "Express register"
                },
                JbPRegBtn = new JbPRegBtn
                {
                    Title = "Express Register",
                    Alignment = "center",
                    Size = "large",
                    RegBtnLabelColor = "#fff",
                    RegBtnBackgroundColor = "#60a917",
                },
                JbPContact = new JbPContact
                {
                    Title = "Title",
                    Contact = "<h3>Contact:</h3>" + "Website: " + club.Site + "<br/>" + (club.ClubLocations.First().PostalAddress.Address != null ? "Address: " + club.ClubLocations.First().PostalAddress.Address + "<br/>" : string.Empty) + (club.ContactPersons.Any() ? ((club.ContactPersons.First().Phone != null ? "Phone: " + club.ContactPersons.First().Phone + "<br/>" : string.Empty) + (club.ContactPersons.First().Email != null ? "Email: " + club.ContactPersons.First().Email : string.Empty)) : string.Empty),
                    //Website = club.Site,
                    //Address = club.ClubLocations.Any() ? club.ClubLocations.First().PostalAddress.Address : string.Empty,
                    //Phone = club.ContactPersons.Any() ? club.ContactPersons.First().Phone : string.Empty,
                    //Email = club.ContactPersons.Any() ? club.ContactPersons.First().Email : string.Empty

                },
                JbPSeasonCalendar = new JbPSeasonCalendar
                {
                    Title = "Title"
                },
                JbPSeasonGrid = new JbPSeasonGrid()
                {
                    Title = "Title",
                    Filter = new JbPFilter(true)
                },
                JbPMap = new JbPMap
                {
                    Title = "Title"
                },
                JbPVideo = new JbPVideo
                {
                    Title = "Title"
                },
                JbPBeforeAfterCareRegLinks = new JbPBeforeAfterCareRegLinks()
                {
                    Title = "Title",
                    ColorSetting = new JbPBeforeAfterCareSetting(true)
                },
                JbPImageGallery = new JbPImageGallery()
                {
                    Title = "Title",
                    ElementId = new JbPImageGallery().GetType().Name + Jumbula.Common.Utilities.StaticRandom.Instance.Next(10000, 99999),
                },
                JbPImageSlider = new JbPImageSlider()
                {
                    Title = "Title",
                    ElementId = new JbPImageSlider().GetType().Name + Jumbula.Common.Utilities.StaticRandom.Instance.Next(10000, 99999),

                },
                JbPSectionColumn = new JbPSectionColumn
                {
                    Title = "Title"
                },
                JbPMenuItem = new JbPMenuItem
                {
                    Title = "Link"
                },
                CartItemCount = 12
            };

            return JsonNet(elementTemplates);
        }

        public virtual ActionResult GetCartItemCount()
        {

            dynamic cartModel = new ExpandoObject();

            var order = _orderBusiness.GetOrderInCart(this.GetCurrentUserId(), this.GetCurrentClubId());
            if (order != null)
            {
                cartModel.OrderCount = order.RegularOrderItems.Count() + order.PreregisterItems.Count() + order.LotteryItems.Count();


                cartModel.CartTitle = "Cart";

                if (order.PreregisterItems.Any())
                {
                    cartModel.HasPreregisteredOrder = true;
                    cartModel.CartTitle = "You have pre-registration items.";
                }

                if (order.RegularOrderItems.Any(o => o.PreRegistrationStatus == PreRegistrationStatus.Added))
                {
                    cartModel.HasAddedPreregisteredOrder = true;
                    cartModel.CartTitle = "You can pay for your pre-registration items now.";
                }

                if (order.LotteryItems.Any())
                {
                    cartModel.HasLotteryOrder = true;
                    cartModel.CartTitle = "You have lottery items.";
                }

                if (order.LotteryItems.Any(o => o.LotteryStatus == LotteryStatus.Won))
                {
                    cartModel.HasWonLotteryOrder = true;
                    cartModel.CartTitle = "You can pay for your won lottery items now.";
                }
            }
            else
            {
                cartModel.OrderCount = 0;
                cartModel.HasPreregisteredOrder = false;
                cartModel.HasAddedPreregisteredOrder = false;
                cartModel.HasLotteryOrder = false;
                cartModel.HasWonLotteryOrder = false;
            }

            return JsonNet(cartModel);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Manager")]
        public virtual JsonResult Manage(JbPage page)
        {
            var res = _pageBusiness.CreateEdit(page);

            //also update draft record after publish record updated
            if (page.SaveType == SaveType.Publish)
            {
                var listDeleted = new List<JbPImage>();
                if (page.deletedImages.Count > 0)
                {
                    var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);
                    var club = this.GetCurrentClubBaseInfo();
                    var clubDomain = club.Domain;


                    foreach (var image in page.deletedImages)
                    {
                        var paths = new List<string>();
                        paths.Add(image.largeImage);
                        paths.Add(image.smallImage);

                        foreach (var path in paths)
                        {
                            if (path != null)
                            {
                                var locationImg = image.location;
                                if (path != null)
                                {
                                    var fileName = path.Split('/').Last();
                                    var sourcePath = StorageManager.PathCombine(clubDomain + locationImg, fileName);
                                    var targetPath = StorageManager.PathCombine(clubDomain + locationImg, fileName);

                                    if (StorageManager.IsExist(StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain + locationImg, fileName).AbsoluteUri))
                                    {
                                        storageManager.ReadFile(sourcePath, targetPath);
                                        storageManager.DeleteBlob(clubDomain + locationImg, fileName);
                                    }
                                }
                            }
                        }

                        listDeleted.Add(image);

                    }
                    foreach (var item in listDeleted)
                    {
                        page.deletedImages.Remove(item);
                    }
                }

                var drafPage = page;
                drafPage.SaveType = SaveType.Draft;

                _pageBusiness.CreateEdit(drafPage);
            }

            var s = Json(new JResult { Status = true }, JsonRequestBehavior.AllowGet);
            return s;
        }

        [HttpPost]
        [Authorize(Roles = "Admin, Manager")]
        public virtual JsonResult Delete(int id)
        {
            var res = _pageBusiness.Delete(id);
            return Json(new JResult { Status = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonNetResult GetTemplate(string seasonDomain)
        {
            var club = _clubBusiness.Get(ClubSubDomain);
            //var season = _seasonBusiness.Get(seasonDomain, club.Domain);

            var page = _pageBusiness.GetDefaultPage(club);

            page.Header.LogoUrl = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
            if (club.Setting == null)
            {
                page.Header.LogoRedirecturl = UrlHelpers.GetProperUrl(club.Setting.RegistrationPageLogoRedirectUrl,
                    club.Site, Request.Url.GetLeftPart(UriPartial.Authority));
            }
            else
            {
                page.Header.LogoRedirecturl = UrlHelpers.GetProperUrl(new ClubRedirectUrl(), club.Site,
                    Request.Url.GetLeftPart(UriPartial.Authority));
            }
            return JsonNet(page);
        }

        [HttpPost]
        public virtual JsonNetResult UpdateRegLinks(List<JbPRegLink> links)
        {
            if (links != null)
            {
                foreach (var link in links.Where(link => link.IsExternal == false))
                {
                    try
                    {
                        var program = _programBusiness.Get(link.ProgramId.Value);

                        link.LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded
                            ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain
                            : "/" + program.Season.Domain + "/" + program.Domain;
                        link.RegistrationPeriod = new RegistrationPeriod
                        {
                            RegisterStartDate = _programBusiness.RegisterStartDate(program),
                            RegisterEndDate = _programBusiness.RegisterEndDate(program)
                        };
                        link.LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain : "/" + program.Season.Domain + "/" + program.Domain;
                        link.RegistrationPeriod = new RegistrationPeriod() { RegisterStartDate = _programBusiness.RegisterStartDate(program), RegisterEndDate = _programBusiness.RegisterEndDate(program) };
                        //link.RegistrationPeriod.RegisterStartDate.ToString("MMMddyyyy");// ("");
                        //link.RegistrationPeriod.RegisterStartDate = String.Format("MMMddyyyy", link.RegistrationPeriod.RegisterStartDate.ToString());
                        link.StartDate = _programBusiness.StartDate(program);
                        link.EndDate = _programBusiness.EndDate(program);
                        link.Location = !string.IsNullOrEmpty(program.ClubLocation.Name)
                            ? string.Format("{0}, {1}", program.ClubLocation.Name,
                                program.ClubLocation.PostalAddress.Address)
                            : program.ClubLocation.PostalAddress.Address;
                        link.MinGrade = _programBusiness.GetMinGrade(program).ToDescription();
                        link.MaxGrade = _programBusiness.GetMaxGrade(program).ToDescription();
                        link.MinAge = _programBusiness.GetMinAge(program);
                        link.MaxAge = _programBusiness.GetMaxAge(program);

                        link.GenderRestrictions = _programBusiness.GetGender(program);

                        link.Tuitions = program.ProgramSchedules.Where(s => !s.IsDeleted).SelectMany(p => p.Charges)
                            .Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee)
                            .Select(c =>
                                new JbPTuition
                                {
                                    Label =
                                        _programBusiness.FormatTuitionOption(c.Name, c.Amount, c.ProgramSchedule.StartDate,
                                            c.ProgramSchedule.EndDate),
                                    CapacityLeft = _programBusiness.CapacityLeftFormated(c, c.ProgramSchedule.Program.Season.Status == SeasonStatus.Test),
                                    Price = c.Amount
                                })
                            .ToList();

                        link.RegStatus = _programBusiness.GetRegStatus(program);
                        link.ProgramType = program.TypeCategory;

                        link.IsFreezed = program.Status == ProgramStatus.Frozen;
                        link.IsDeleted = program.Status == ProgramStatus.Deleted;

                        link.IsFull = _programBusiness.IsFull(program, true);

                        link.Schedules = program.ProgramSchedules.Where(s => !s.IsFreezed && !s.IsDeleted).Select(s =>
                            new JbPScheduleViewModel
                            {
                                Title =
                                    string.Format("{0} - {1}", s.StartDate.ToShortDateString(),
                                        s.EndDate.ToShortDateString()),
                                StartDate = s.StartDate,
                                EndDate = s.EndDate,
                                Days = GetLinkDays(s).ToArray()
                            })
                            .ToList();
                    }
                    catch
                    {
                    }
                }
            }

            return JsonNet(links);
        }

        [HttpPost]
        public virtual JsonNetResult UpdateBeforeAfterLinks(List<JbPBeforeAfterCareRegLinks> links)
        {
            if (links != null)
            {
                foreach (var link in links.Where(link => link.IsExternal == false))
                {
                    try
                    {
                        var program = _programBusiness.Get(link.BeforeAfterProgramId.Value);
                        var programSchedule = program.ProgramSchedules.Where(p => !p.IsFreezed && !p.IsDeleted);
                        var defaultScheduleAttribute = (ScheduleAfterBeforeCareAttribute)programSchedule.FirstOrDefault().Attributes;
                        var paymentPriod = _programBusiness.ProgramPaymentPriod(programSchedule.FirstOrDefault());
                        link.PaymentPriod = paymentPriod == "BiWeekly" ? "Bi-weekly Fees" : paymentPriod == "Weekly" ? "Weekly Fees" : "Monthly Fees";
                        link.Description = program.Description;
                        link.MinGrade = _programBusiness.GetMinGrade(program).ToDescription();
                        link.MaxGrade = _programBusiness.GetMaxGrade(program).ToDescription();
                        link.LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain : "/" + program.Season.Domain + "/" + program.Domain + "/Register?mode=NoDropInPunchcard";
                        link.LinkClassPageUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain : "/" + program.Season.Domain + "/" + program.Domain;
                        link.IsEnabeldDropIn = defaultScheduleAttribute.EnableDropIn;
                        link.IsEnabeldPunchcard = defaultScheduleAttribute.PunchCard.Enabled;

                        var strTimes = programSchedule.Any(s => s.ScheduleMode == TimeOfClassFormation.Both) ? _programBusiness.GetComboTime(programSchedule.Where(s => s.ScheduleMode != TimeOfClassFormation.Both).ToList()) : null;

                        var programTimes = new List<ScheduleTimesViewModel>();

                        foreach (var schedule in programSchedule.Where(s => s.ScheduleMode != TimeOfClassFormation.Both))
                        {
                            programTimes.AddRange(_programBusiness.GetTimesProgram(schedule));
                        }

                        if (defaultScheduleAttribute.EnableDropIn)
                        {
                            link.DropInInformation = new DropInPunchCardInformation
                            {
                                Title = defaultScheduleAttribute.DropInSessionLabel,
                                Amount = defaultScheduleAttribute.DropInSessionPrice.ToString(),
                                Times = programTimes,
                                StrTimes = strTimes,
                                FeeLable = "/day",
                                LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain : "/" + program.Season.Domain + "/" + program.Domain + "/Register?mode=DropInPunchcard",
                                LinkClassPageUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain : "/" + program.Season.Domain + "/" + program.Domain
                            };
                        }

                        if (defaultScheduleAttribute.PunchCard.Enabled)
                        {
                            link.PunchcardInformation = new DropInPunchCardInformation
                            {
                                Title = defaultScheduleAttribute.PunchCard.Title,
                                Amount = defaultScheduleAttribute.PunchCard.Amount.ToString(),
                                Times = programTimes,
                                StrTimes = strTimes,
                                FeeLable = "/" + defaultScheduleAttribute.PunchCard.SessionNumber + "-day card",
                                LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain : "/" + program.Season.Domain + "/" + program.Domain + "/Register?mode=DropInPunchcard",
                                LinkClassPageUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain : "/" + program.Season.Domain + "/" + program.Domain
                            };
                        }

                        link.Schedules = programSchedule.OrderByDescending(o => o.ScheduleMode).Select(s => new JbPScheduleViewModel
                        {
                            Description = s.Description,
                            Title = s.Title,
                            Tuitions = s.Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Select(c => new JbPTuition
                            {
                                Label = c.Name,
                                Price = c.Amount,
                                ChargeId = c.Id,
                                PaymentPriod = paymentPriod == "BiWeekly" ? "bi-weekly" : paymentPriod == "Weekly" ? "week" : "month",

                    }).ToList(),

                            Mode = s.ScheduleMode,
                            ScheduleDaysTime = s.ScheduleMode != TimeOfClassFormation.Both ? _programBusiness.GetTimesProgram(s) : programTimes,

                        }).ToList();


                        foreach (var schedule in link.Schedules)
                        {
                            schedule.StrScheduleDaysTime = new List<string>();
                            if (schedule.ScheduleDaysTime.Count > 1)
                            {
                                if (schedule.Mode != TimeOfClassFormation.Both)
                                {
                                    foreach (var time in schedule.ScheduleDaysTime)
                                    {
                                        schedule.StrScheduleDaysTime.Add(string.Format("{0}: {1}", time.Day, time.MainTime));
                                    }
                                }
                                else
                                {
                                    schedule.StrScheduleDaysTime.AddRange(strTimes);
                                }
                            }
                            else
                            {
                                schedule.StrScheduleDaysTime.Add(schedule.ScheduleDaysTime.FirstOrDefault().MainTime);
                            }
                        }

                        TuitionBeforeAfterProgramSorting(program, link.Schedules);
                    }
                    catch
                    {
                    }
                }

            }

            return JsonNet(links);
        }


        /// <summary>
        /// used in season controller(season registration page)
        /// </summary>
        /// <param name="seasonDomain"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual JsonNetResult GetListLinks()
        {
            var clubId = this.GetCurrentClubId();

            var club = _clubBusiness.Get(clubId);

            var programs = _programBusiness.GetList(club.Id).Where(p => p.LastCreatedPage == ProgramPageStep.Step4).ToList();

            var model = programs.Select(p => new JbPRegLink()
            {
                SeasonId = p.Season.Id,
                ProgramId = p.Id,
                Title = p.Name,
                LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + club.Domain + "/" + p.Season.Domain + "/" + p.Domain : "/" + p.Season.Domain + "/" + p.Domain,
                RegistrationPeriod = new RegistrationPeriod() { RegisterStartDate = _programBusiness.RegisterStartDate(p), RegisterEndDate = _programBusiness.RegisterEndDate(p) },
                StartDate = _programBusiness.StartDate(p),
                EndDate = _programBusiness.EndDate(p),
                Location = p.ClubLocation.PostalAddress.Address,
                MinGrade = _programBusiness.GetMinGrade(p).ToDescription(),
                MaxGrade = _programBusiness.GetMaxGrade(p).ToDescription(),
                MinAge = _programBusiness.GetMinAge(p),
                MaxAge = _programBusiness.GetMaxAge(p),
                GenderRestrictions = _programBusiness.GetGender(p),

                Tuitions = p.ProgramSchedules.Where(s => !s.IsFreezed && !s.IsDeleted).SelectMany(s => s.Charges)
                    .Where(c => c.Category == ChargeDiscountCategory.EntryFee)
                    .Select(c =>
                        new JbPTuition
                        {
                            Label = _programBusiness.FormatTuitionOption(c.Name, c.Amount, c.ProgramSchedule.StartDate, c.ProgramSchedule.EndDate),
                            CapacityLeft = _programBusiness.CapacityLeftFormated(c, c.ProgramSchedule.Program.Season.Status == SeasonStatus.Test),
                            Price = c.Amount
                        })
                        .ToList(),

                RegStatus = _programBusiness.GetRegStatus(p),
                ProgramType = p.TypeCategory,

                IsFreezed = p.Status == ProgramStatus.Frozen,
                IsDeleted = p.Status == ProgramStatus.Deleted,

                IsFull = _programBusiness.IsFull(p, true),

                Schedules = p.ProgramSchedules.Where(s => !s.IsDeleted).Select(s =>
                    new JbPScheduleViewModel
                    {
                        Title = string.Format("{0} - {1}", s.StartDate.ToShortDateString(), s.EndDate.ToShortDateString()),
                        StartDate = s.StartDate,
                        EndDate = s.EndDate,

                        Days = _programBusiness.GetProgramScheduleDays(s).Select(d =>
                        new JbPScheduleDayViewModel
                        {
                            DayOfWeek = d.DayOfWeek,
                            StartTime = d.StartTime.HasValue ? DateTimeHelper.FormatTimeSpan(d.StartTime.Value) : string.Empty,
                            EndTime = d.EndTime.HasValue ? DateTimeHelper.FormatTimeSpan(d.EndTime.Value) : string.Empty,
                        })
                        .ToArray()
                    })
                    .ToList()
            });

            var t = JsonNet(model);

            return t;
        }

        [HttpGet]
        public virtual JsonNetResult GetListBeforeAfterCareLinks()
        {

            var clubId = this.GetCurrentClubId();

            var club = _clubBusiness.Get(clubId);

            var programs = _programBusiness.GetList(club.Id).Where(p => p.LastCreatedPage == ProgramPageStep.Step4 && p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).ToList();

            var model = new List<JbPBeforeAfterCareRegLinks>();

            foreach (var p in programs)
            {
                var beforeAfterInfo = new JbPBeforeAfterCareRegLinks();
                var dropInInformation = new DropInPunchCardInformation();
                var punchcardInformation = new DropInPunchCardInformation();

                var programSchedules = p.ProgramSchedules.Where(s => !s.IsFreezed && !s.IsDeleted);
                var defaultScheduleAttribute = (ScheduleAfterBeforeCareAttribute)programSchedules.FirstOrDefault().Attributes;

                var programTimes = new List<ScheduleTimesViewModel>();

                var paymentPriod = _programBusiness.ProgramPaymentPriod(programSchedules.FirstOrDefault());

                foreach (var schedule in programSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both))
                {
                    programTimes.AddRange(_programBusiness.GetTimesProgram(schedule));
                }

                var strTimes = programSchedules.Any(s => s.ScheduleMode == TimeOfClassFormation.Both) ? _programBusiness.GetComboTime(programSchedules.Where(s => s.ScheduleMode != TimeOfClassFormation.Both).ToList()) : null;

                if (defaultScheduleAttribute.EnableDropIn)
                {
                    dropInInformation = new DropInPunchCardInformation
                    {
                        Title = defaultScheduleAttribute.DropInSessionLabel,
                        Amount = defaultScheduleAttribute.DropInSessionPrice.ToString(),
                        Times = programTimes,
                        StrTimes = strTimes,
                        FeeLable = "/day",
                        LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + club.Domain + "/" + p.Season.Domain + "/" + p.Domain : "/" + p.Season.Domain + "/" + p.Domain + "/Register?mode=DropInPunchcard",
                        LinkClassPageUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + p.Season.Club.Domain + "/" + p.Season.Domain + "/" + p.Domain : "/" + p.Season.Domain + "/" + p.Domain
                    };
                }

                if (defaultScheduleAttribute.PunchCard.Enabled)
                {
                    punchcardInformation = new DropInPunchCardInformation
                    {
                        Title = defaultScheduleAttribute.PunchCard.Title,
                        Amount = defaultScheduleAttribute.PunchCard.Amount.ToString(),
                        Times = programTimes,
                        StrTimes = strTimes,
                        FeeLable = "/" + defaultScheduleAttribute.PunchCard.SessionNumber + "-day card",
                        LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + club.Domain + "/" + p.Season.Domain + "/" + p.Domain : "/" + p.Season.Domain + "/" + p.Domain + "/Register?mode=DropInPunchcard",
                        LinkClassPageUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + p.Season.Club.Domain + "/" + p.Season.Domain + "/" + p.Domain : "/" + p.Season.Domain + "/" + p.Domain
                    };
                }

                beforeAfterInfo = new JbPBeforeAfterCareRegLinks
                {
                    BeforeAfterProgramId = p.Id,
                    Description = p.Description,
                    MinGrade = _programBusiness.GetMinGrade(p).ToDescription(),
                    MaxGrade = _programBusiness.GetMaxGrade(p).ToDescription(),

                    LinkUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + club.Domain + "/" + p.Season.Domain + "/" + p.Domain : "/" + p.Season.Domain + "/" + p.Domain + "/Register?mode=NoDropInPunchcard",
                    LinkClassPageUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + p.Season.Club.Domain + "/" + p.Season.Domain + "/" + p.Domain : "/" + p.Season.Domain + "/" + p.Domain,

                    PaymentPriod = paymentPriod == "BiWeekly" ? "Bi-weekly Fees" : paymentPriod == "Weekly" ? "Weekly Fees" : "Monthly Fees",
                    DropInInformation = dropInInformation,
                    PunchcardInformation = punchcardInformation,
                    IsEnabeldDropIn = defaultScheduleAttribute.EnableDropIn,
                    IsEnabeldPunchcard = defaultScheduleAttribute.PunchCard.Enabled,

                    Schedules = p.ProgramSchedules.Where(s => !s.IsFreezed && !s.IsDeleted).OrderByDescending(o => o.ScheduleMode).Select(s => new JbPScheduleViewModel
                    {
                        Description = s.Description,
                        Title = s.Title,
                        Tuitions = s.Charges.Where(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee).Select(c => new JbPTuition
                        {
                            Label = c.Name,
                            Price = c.Amount,
                            ChargeId = c.Id,
                            PaymentPriod = paymentPriod == "BiWeekly" ? "bi-weekly" : paymentPriod == "Weekly" ? "week" : "month",
                        }).ToList(),

                        Mode = s.ScheduleMode,
                        ScheduleDaysTime = s.ScheduleMode != TimeOfClassFormation.Both ? _programBusiness.GetTimesProgram(s) : programTimes,

                    }).ToList(),

                };

                foreach (var schedule in beforeAfterInfo.Schedules)
                {
                    schedule.StrScheduleDaysTime = new List<string>();
                    if (schedule.ScheduleDaysTime.Count > 1)
                    {
                        if (schedule.Mode != TimeOfClassFormation.Both)
                        {
                            foreach (var time in schedule.ScheduleDaysTime)
                            {
                                schedule.StrScheduleDaysTime.Add(string.Format("{0}: {1}", time.Day, time.MainTime));
                            }
                        }
                        else
                        {
                            schedule.StrScheduleDaysTime.AddRange(strTimes);
                        }
                    }
                    else
                    {
                        schedule.StrScheduleDaysTime.Add(schedule.ScheduleDaysTime.FirstOrDefault().MainTime);
                    }
                }

                TuitionBeforeAfterProgramSorting(p, beforeAfterInfo.Schedules);

                model.Add(beforeAfterInfo);
            }


            return JsonNet(model);
        }
        private List<JbPScheduleDayViewModel> GetLinkDays(ProgramSchedule programSchdeule)
        {
            var result = new List<JbPScheduleDayViewModel>();

            if (programSchdeule.Program.TypeCategory != ProgramTypeCategory.Subscription)
            {
                result = ((ScheduleAttribute)programSchdeule.Attributes).Days.Select(d =>
                                    new JbPScheduleDayViewModel
                                    {
                                        DayOfWeek = d.DayOfWeek,
                                        StartTime = DateTimeHelper.FormatTimeSpan(d.StartTime.Value),
                                        EndTime = DateTimeHelper.FormatTimeSpan(d.EndTime.Value)
                                    })
                                    .ToList();
            }
            else
            {
                if (programSchdeule.Program.TypeCategory == ProgramTypeCategory.Subscription)
                {
                    result = ((ScheduleSubscriptionAttribute)programSchdeule.Attributes).Days.Select(d =>
                                                       new JbPScheduleDayViewModel
                                                       {
                                                           DayOfWeek = d.DayOfWeek,
                                                           StartTime = DateTimeHelper.FormatTimeSpan(d.StartTime.Value),
                                                           EndTime = DateTimeHelper.FormatTimeSpan(d.EndTime.Value)
                                                       })
                                                       .ToList();
                }
                else
                {
                    result = ((ScheduleAfterBeforeCareAttribute)programSchdeule.Attributes).Days.Select(d =>
                                   new JbPScheduleDayViewModel
                                   {
                                       DayOfWeek = d.DayOfWeek,
                                       StartTime = DateTimeHelper.FormatTimeSpan(d.StartTime.Value),
                                       EndTime = DateTimeHelper.FormatTimeSpan(d.EndTime.Value)
                                   })
                                   .ToList();
                }

            }

            return result;
        }

        [Authorize(Roles = "Admin, Manager")]
        [HttpPost]
        public string UploadCover(string coverImage, int pageId)
        {
            //var coverImage = Request["coverImage"];

            var club = _clubBusiness.Get(ClubSubDomain);
            this.EnsureUserIsAdminForClub(ClubSubDomain);

            var fileName = "";

            if (!string.IsNullOrEmpty(coverImage))
            {
                coverImage = coverImage.Replace("data:image/jpeg;base64,", string.Empty);

                coverImage = coverImage.Replace("data:image/png;base64,", string.Empty);

                //byte[] fileData = null;

                var fileData = Convert.FromBase64String(coverImage);

                var fileSize = fileData.Count() / 1024;

                // Max image size is 6 MB
                if (fileSize > 6144)
                {
                    return "Exceed";
                }

                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

                var contentType = "image/jpeg";

                Stream fileStream = new MemoryStream(fileData);

                fileName = string.Format("{0}_{1}_{2}.{3}", "TabCover", pageId, Guid.NewGuid(), "jpg");

                sm.UploadBlob(club.Domain.ToLower(), fileName, fileStream, contentType);

                fileStream.Close();
            }

            var uri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain.ToLower(), fileName).AbsoluteUri;

            return uri;
        }


        [Authorize(Roles = "Admin, Manager")]
        public virtual ActionResult RemoveUploadedCover(int pageId)
        {
            var fileName = string.Format("{0}/{1}", pageId, "PageCover.jpg");
            var clubDomain = ClubSubDomain;
            var sourcePath = StorageManager.PathCombine(clubDomain, fileName);
            var storageManager = new StorageManager(WebConfigHelper.AzureStorageConnection,
                Constants.Path_ClubFilesContainer);
            var targetPath = StorageManager.PathCombine(clubDomain, fileName);
            if (
                StorageManager.IsExist(
                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain,
                        fileName).AbsoluteUri))
            {
                storageManager.ReadFile(sourcePath, targetPath);
                storageManager.DeleteBlob(clubDomain, fileName);
            }

            return JsonNet(new { message = "Done." });
        }

        [Authorize(Roles = "Admin, Manager")]
        [HttpPost]
        public virtual ActionResult UpdatePageSettings(JbPSetting model)
        {
            var club = _clubBusiness.Get(ClubSubDomain);

            var page = _pageBusiness.Get(club.Id, SaveType.Draft);

            page.Setting = model;

            var res = _pageBusiness.CreateEdit(page);

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        [HttpPost]
        public virtual JsonNetResult SeasonProgramsCalendar(JbPSeasonCalendar seasonCalendarSetting)
        {
            //var programBussiness = _programBusiness;

            var programs = _programBusiness.GetList(seasonCalendarSetting).ToList();

            var model = new List<ProgramCalendarItemViewModel>();

            foreach (var program in programs)
            {
                if (program.ProgramSchedules != null)
                {
                    var isAllDays = false;

                    switch (program.TypeCategory)
                    {
                        case ProgramTypeCategory.Class:
                            break;

                        case ProgramTypeCategory.Camp:
                            break;

                        case ProgramTypeCategory.ChessTournament:
                            isAllDays = true;
                            break;

                        case ProgramTypeCategory.Calendar:
                            break;

                        case ProgramTypeCategory.SeminarTour:
                            isAllDays = true;

                            break;
                        default:
                            continue;
                    }

                    var programSessions = _programBusiness.GenerateSessionItems(program, seasonCalendarSetting.SchoolGradeType);

                    model.AddRange(programSessions.Select(s =>
                        new ProgramCalendarItemViewModel
                        {
                            TaskId = s.Id,
                            Start = s.Start.ToString(),
                            End = s.End.ToString(),
                            Title = s.Title,
                            IsAllDay = isAllDays,
                            //Description = "description of this task",
                            ProgramType = program.TypeCategory.ToString(),
                            ScheduleMinGrade = s.ScheduleMinGrade.ToDescription(),
                            ScheduleMaxGrade = s.ScheduleMaxGrade.ToDescription()
                            //ProgramGradeList = s.ProgramGradeList
                        }));
                }
            }
            return JsonNet(model);
        }

        [HttpPost]
        public virtual JsonNetResult SeasonProgramsGrid(JbPSeasonGrid seasonGridSetting)
        {
            var programs = _programBusiness.GetList(seasonGridSetting).ToList();

            if (seasonGridSetting.HideFrozen)
            {
                programs = programs.Where(p => p.Status != ProgramStatus.Frozen).ToList();
            }

            if (seasonGridSetting.HideExpired)
            {
                programs = programs.Where(p => _programBusiness.GetScheduleRegStatus(p.ProgramSchedules.FirstOrDefault()) != ProgramRegStatus.Closed).ToList();
            }

            if (seasonGridSetting.HideFuture)
            {
                programs = programs.Where(p => _programBusiness.GetScheduleRegStatus(p.ProgramSchedules.FirstOrDefault()) != ProgramRegStatus.NotStarted).ToList();
            }


            var timePm = new TimeSpan(12, 0, 0); //pm
            var timeAm = new TimeSpan(0, 0, 0); //AM

            if (!string.IsNullOrEmpty(seasonGridSetting.SelectedAddress) && seasonGridSetting.SelectedAddress != "All")
            {
                programs = programs.Where(p => !string.IsNullOrEmpty(p.ClubLocation.PostalAddress.Address) && p.ClubLocation.PostalAddress.Address.ToLower().Trim() == seasonGridSetting.SelectedAddress.ToLower().Trim()).ToList();
            }
            if (!string.IsNullOrEmpty(seasonGridSetting.CityName) && seasonGridSetting.CityName != "All")
            {
                programs = programs.Where(p => p.ClubLocation.PostalAddress.City == seasonGridSetting.CityName).ToList();
            }
            if (seasonGridSetting.SelectedCategory != null && seasonGridSetting.SelectedCategory.Any())
            {
                var programsFilterByCategory = new List<Program>();
                if (seasonGridSetting.SelectedCategory.FirstOrDefault() != 0)
                {
                    foreach (var item in programs)
                    {
                        var programList = seasonGridSetting.SelectedCategory.Where(s => item.Categories.Select(p => p.Id).ToList().Contains(s)).ToList();

                        if (programList.Count > 0)
                        {
                            programsFilterByCategory.Add(item);
                        }
                    }
                    programs = programsFilterByCategory;
                }

            }
            if (!string.IsNullOrEmpty(seasonGridSetting.SelectedLocationName) && seasonGridSetting.SelectedLocationName != "All")
            {

                programs = programs.Where(p => !string.IsNullOrEmpty(p.ClubLocation.Name) && p.ClubLocation.Name.ToLower().Trim() == seasonGridSetting.SelectedLocationName.ToLower().Trim()).ToList();
            }

            if (seasonGridSetting.SelectedGrade != "0" && seasonGridSetting.SelectedGrade != null)
            {
                var programsFilterByGrade = new List<Program>();

                foreach (var item in programs)
                {
                    var min = _programBusiness.GetMinGrade(item);
                    var max = _programBusiness.GetMaxGrade(item);
                    var selectedGrade = _playerProfileBusiness.GetGradeValueFromDescription(seasonGridSetting.SelectedGrade);

                    if (min != null && max != null)
                    {
                        if (selectedGrade != null && (min.Value <= selectedGrade.Value && max.Value >= selectedGrade.Value))
                        {
                            programsFilterByGrade.Add(item);
                        }
                    }
                    else if (selectedGrade != null && ((min != null && (min.Value <= selectedGrade.Value)) || (max != null && (max.Value >= selectedGrade.Value))))
                    {
                        programsFilterByGrade.Add(item);
                    }
                    else if (min == null && max == null)
                    {
                        programsFilterByGrade.Add(item);
                    }
                }

                programs = programsFilterByGrade;
            }

            if (seasonGridSetting.SelectedWeekDay != null && seasonGridSetting.SelectedWeekDay.Any())
            {
                var programsFilterByDay = new List<Program>();

                foreach (var item in programs)
                {
                    var day = _programBusiness.GetClassDays(item).ToList();

                    var programList = seasonGridSetting.SelectedWeekDay.Where(s => day.Select(p => p.DayOfWeek).ToList().Contains(s)).ToList();

                    if (programList.Count > 0)
                    {
                        programsFilterByDay.Add(item);
                    }
                }

                programs = programsFilterByDay;
            }

            if (seasonGridSetting.SelectedAMPM != null && seasonGridSetting.SelectedAMPM.Any())
            {
                var programsFilterByAmpm = new List<Program>();

                if (seasonGridSetting.SelectedAMPM.Count > 1)
                {
                    foreach (var item in programs)
                    {
                        var programsPmAndAm = _programBusiness.GetClassDays(item).Where(pr => (pr.StartTime.HasValue && pr.StartTime >= timePm) || (pr.StartTime >= timeAm && pr.StartTime < timePm)).ToList();
                        if (programsPmAndAm.Count > 0)
                        {
                            programsFilterByAmpm.Add(item);
                        }
                    }
                    programs = programsFilterByAmpm;
                }
                else
                {
                    if (seasonGridSetting.SelectedAMPM.FirstOrDefault() == "PM")
                    {
                        foreach (var item in programs)
                        {
                            var programsPm = _programBusiness.GetClassDays(item).Where(pr => pr.StartTime >= timePm).ToList();
                            if (programsPm.Count > 0)
                            {
                                programsFilterByAmpm.Add(item);
                            }
                        }
                        programs = programsFilterByAmpm;
                    }
                    else
                    {

                        foreach (var item in programs)
                        {
                            var programsAm = _programBusiness.GetClassDays(item).Where(pr => pr.StartTime >= timeAm && pr.StartTime < timePm).ToList();
                            if (programsAm.Count > 0)
                            {
                                programsFilterByAmpm.Add(item);
                            }
                        }
                        programs = programsFilterByAmpm;
                    }
                }

            }

            if (seasonGridSetting.SelectedProgramStatus != null && seasonGridSetting.SelectedProgramStatus.Any())
            {
                if (seasonGridSetting.SelectedProgramStatus.FirstOrDefault() != "All")
                {
                    var programsFilterStatus = new List<Program>();
                    foreach (var program in programs)
                    {
                        var programStatus = _programBusiness.GetEmProgramStatus(program, program.Season.Status == SeasonStatus.Test);

                        if (seasonGridSetting.SelectedProgramStatus.Contains(programStatus))
                        {
                            programsFilterStatus.Add(program);
                        }
                    }
                    programs = programsFilterStatus;
                }

            }

            var model = new List<ProgramGridItemViewModel>();

            var programsList = new List<Program>();
            var pmPrograms = new List<Program>();
            var amPrograms = new List<Program>();


            foreach (var item in programs)
            {
                if (_programBusiness.GetClassDays(item).Any(pr => pr.StartTime >= timePm))
                {
                    pmPrograms.Add(item);
                }
                else
                {
                    amPrograms.Add(item);
                }

            }

            programsList.AddRange(amPrograms.OrderBy(p => p.Name));
            programsList.AddRange(pmPrograms.OrderBy(p => p.Name));

            foreach (var program in programs)
            {
                if (program.ProgramSchedules != null)
                {
                    var programSessions = _programBusiness.GenerateSessionItems(program, seasonGridSetting.SchoolGradeType).Where(p => seasonGridSetting.SelectedWeekDay != null ? seasonGridSetting.SelectedWeekDay.Contains(p.Day) : true);

                    if (seasonGridSetting.SelectedAMPM != null) //Filter PM and AM in session
                    {
                        if (seasonGridSetting.SelectedAMPM.Count() > 1)
                        {
                            programSessions = programSessions.Where(pr => (pr.Start.TimeOfDay >= timePm) || (pr.Start.TimeOfDay >= timeAm && pr.Start.TimeOfDay < timePm));
                        }
                        else
                        {
                            if (seasonGridSetting.SelectedAMPM.FirstOrDefault() == "PM")
                            {
                                programSessions = programSessions.Where(pr => pr.Start.TimeOfDay >= timePm).ToList();
                            }
                            else
                            {
                                programSessions = programSessions.Where(pr => pr.Start.TimeOfDay >= timeAm && pr.Start.TimeOfDay < timePm).ToList();
                            }
                        }
                    }

                    var isAllDays = false;

                    switch (program.TypeCategory)
                    {
                        case ProgramTypeCategory.Class:
                        case ProgramTypeCategory.Camp:
                        case ProgramTypeCategory.Calendar:
                        case ProgramTypeCategory.BeforeAfterCare:
                            break;
                        case ProgramTypeCategory.ChessTournament:
                            isAllDays = true;
                            break;
                        case ProgramTypeCategory.SeminarTour:
                            isAllDays = true;
                            break;
                        default:
                            continue;
                    }

                    model.AddRange(programSessions.Select(s =>
                      new ProgramGridItemViewModel
                      {
                          ProgramId = program.Id,
                          CalendarTaskId = s.Id,
                          CalendarScheduleMinGrade = s.ScheduleMinGrade.ToDescription(),
                          CalendarScheduleMaxGrade = s.ScheduleMaxGrade.ToDescription(),
                          CalendarIsAllDay = isAllDays,
                          Start = !(seasonGridSetting.GridLayoutType == GridLayoutType.Calendar || seasonGridSetting.GridLayoutType == GridLayoutType.Agenda) ? s.Start.ToString("hh:mm tt") : s.Start.ToString(),
                          End = !(seasonGridSetting.GridLayoutType == GridLayoutType.Calendar || seasonGridSetting.GridLayoutType == GridLayoutType.Agenda) ? s.End.ToString("hh:mm tt") : s.End.ToString(),
                          SecondStart = s.SecondStart != null ? !(seasonGridSetting.GridLayoutType == GridLayoutType.Calendar || seasonGridSetting.GridLayoutType == GridLayoutType.Agenda) ? s.SecondStart.Value.ToString("hh:mm tt") : s.SecondStart.Value.ToString() : null,
                          SecondEnd = s.SecondEnd != null ? !(seasonGridSetting.GridLayoutType == GridLayoutType.Calendar || seasonGridSetting.GridLayoutType == GridLayoutType.Agenda) ? s.SecondEnd.Value.ToString("hh:mm tt") : s.SecondEnd.Value.ToString() : null,
                          StartOrder = int.Parse((s.Start.Hour.ToString() + s.Start.Minute.ToString("00"))),
                          EndOrder = int.Parse((s.End.Hour.ToString() + s.End.Minute.ToString("00"))),
                          Order = int.Parse((s.Start.Hour.ToString() + s.Start.Minute.ToString("00")) + int.Parse((s.End.Hour.ToString() + s.End.Minute.ToString("00")))),
                          Title = seasonGridSetting.ShowProgramColumn ? s.Title : string.Empty,
                          DayOfWeek = s.Day,
                          ProgramType = program.TypeCategory.ToString(),
                          ProgramRegStatus = program.TypeCategory != ProgramTypeCategory.Camp ? seasonGridSetting.ShowStatusColumn ? s.ProgramStatus : string.Empty : string.Empty,
                          Classfee = _programBusiness.GetClassFees(program, program.Club.Currency),
                          CapacityLeft = seasonGridSetting.ShowCapacityLeftColumn ? _programBusiness.GetProgramChargesSpotsLeft(program, program.Season.Status == SeasonStatus.Test) : null,
                          Domain = s.Domain,
                          SeasonDomain = s.SeasonDomain,
                          StartEndTime = DateTimeHelper.FormatTimeRangeAmPm(s.Start.TimeOfDay, s.End.TimeOfDay),
                          SecondStartEndTime = s.SecondStart.HasValue && s.SecondEnd.HasValue ? DateTimeHelper.FormatTimeRangeAmPm(s.SecondStart.Value.TimeOfDay, s.SecondEnd.Value.TimeOfDay) : null,
                          DisplayUrl = $"{s.SeasonDomain}/{s.Domain}/view",
                          SingleRegisterUrl = $"{s.SeasonDomain}/{s.Domain}",
                          RegisterUrl = this.GetCurrentRequestType() == RequestType.NotBranded ? "/" + program.Season.Club.Domain + "/" + program.Season.Domain + "/" + program.Domain : "/" + program.Season.Domain + "/" + program.Domain + "/Register",
                          Room = seasonGridSetting.ShowRoomColumn ? s.Room != null ? s.Room : string.Empty : string.Empty,
                          Instructors = seasonGridSetting.ShowInstructorsColumn ? _programBusiness.GetInstractorsInHomeSite(program) : null,
                          Provider = seasonGridSetting.ShowProviderColumn ? (program.OutSourceSeasonId.HasValue) ? program.OutSourceSeason.Club.Name : string.Empty : string.Empty,
                          Location = seasonGridSetting.ShowLocationColumn ? program.ClubLocation.PostalAddress.Address : string.Empty,
                          Capacity = seasonGridSetting.ShowCapacityColumn ? _programBusiness.GetProgramChargesCapacities(program) : null,
                          PMAM = DateTimeHelper.IsTimeAm(s.Start.TimeOfDay) ? "AM" : "PM",
                          EnablePopUp = seasonGridSetting.EnabledPopup,
                          ProgramRegBtnBackgroundColor = seasonGridSetting.ProgramRegBtnBackgroundColor,
                          ProgramRegBtnLabelColor = seasonGridSetting.ProgramRegBtnLabelColor,
                          ProgramRegBtnTitle = seasonGridSetting.ProgramRegBtnTitle,
                          ProgramRegBtnLinkTo = seasonGridSetting.ProgramRegBtnLinkTo,
                          EnabledRegistrationPopup = seasonGridSetting.EnabledRegistrationPopup,
                          GenderRestrictions = seasonGridSetting.ShowRestrictionsColumn ? _programBusiness.GetStringGender(program) : string.Empty,
                          CalenderProgramTextColor = seasonGridSetting.CalenderProgramTextColor,
                          CalenderProgramBackgroundColor = seasonGridSetting.CalenderProgramBackgroundColor,

                          GradeAge = seasonGridSetting.ShowGradeAgeColumn ? !string.IsNullOrEmpty(_programBusiness.GenerateGradeInfoLabelForFlyer(program)) ? _programBusiness.GenerateGradeInfoLabelForFlyer(program) : !string.IsNullOrEmpty(_programBusiness.GenerateAgeInfoLabel(program)) ? _programBusiness.GenerateAgeInfoLabel(program) : string.Empty : string.Empty,
                          Dates = seasonGridSetting.ShowDatesColumn ?
                              $"{(_programBusiness.StartDate(program) != null ? _programBusiness.StartDate(program)?.ToString(Constants.DateTime_Comma) : string.Empty)} - {(_programBusiness.EndDate(program) != null ? _programBusiness.EndDate(program)?.ToString(Constants.DateTime_Comma) : string.Empty)}" : string.Empty,
                          RegistrationPeriod = seasonGridSetting.ShowRegistrationDatesColumn ?
                              $"{_programBusiness.RegisterStartDate(program).ToString(Constants.DateTime_Comma)} - {_programBusiness.RegisterEndDate(program).ToString(Constants.DateTime_Comma)}" : string.Empty,
                          Schedules = seasonGridSetting.ShowDaysTimesColumn ? program.ProgramSchedules.Where(ps => !ps.IsFreezed && !ps.IsDeleted).Select(ps =>
                              new JbPScheduleViewModel
                              {
                                  Days = _programBusiness.GetProgramScheduleDays(ps).Select(d =>
                                  new JbPScheduleDayViewModel
                                  {
                                      DayOfWeek = d.DayOfWeek,
                                      StartTime = d.StartTime.HasValue ? DateTimeHelper.FormatTimeSpan(d.StartTime.Value) : string.Empty,
                                      EndTime = d.EndTime.HasValue ? DateTimeHelper.FormatTimeSpan(d.EndTime.Value) : string.Empty,
                                  })
                                 .ToArray()
                              }).ToList() : null,



                      }));
                }
            }

            if (!(seasonGridSetting.GridLayoutType == GridLayoutType.Calendar || seasonGridSetting.GridLayoutType == GridLayoutType.Agenda))
            {
                var result = model.DistinctBy(s => new { s.ProgramType, s.Title, s.StartEndTime, s.DayOfWeek, intDayOfWeek = s.IntDayOfWeek, s.ProgramId }).OrderBy(c => c.Order).ToList();

                if (seasonGridSetting.GridLayoutType == GridLayoutType.List)
                {
                    result = result.DistinctBy(s => new { s.Title, s.ProgramId }).ToList();
                }

                return JsonNet(new
                {
                    DataSource = result,
                    TotalCount = programs.Count()
                });
            }
            else
            {
                return JsonNet(model);
            }

        }
        [HttpPost]
        public virtual ActionResult GetProgramInformation(long programId)
        {
            var program = _programBusiness.Get(programId);
            var programScheduleMode = _programBusiness.GetClassScheduleTime(program);
            var club = program.Club;
            var clubSetting = club.Setting;
            var clubInfo = _clubBusiness.GetHelpInformationSetting(club);
            var showProviderNameInClassPage = false;

            if (club.PartnerId.HasValue)
            {

                var partner = Ioc.ClubBusiness.Get(club.PartnerId.Value);
                if (partner.Setting?.AppearanceSetting != null)
                {
                    showProviderNameInClassPage = partner.Setting.AppearanceSetting.ShowProviderNameInClassPage;
                }
            }

            var programBody = new ProgramInformationModel()
            {
                Schedule = new ProgramBodySchedule()
                {
                    Prices = program.ProgramSchedules.First().Charges.Where(c => c.Category == ChargeDiscountCategory.EntryFee && c.IsDeleted == false)
                     .Select(c =>
                     new ChargeViewModel
                     {
                         Name = c.Name,
                         Amount = _programBusiness.CalculateProgramChargeAmount(club, c).Value,
                         StrAmount = CurrencyHelper.FormatCurrencyWithPenny(_programBusiness.CalculateProgramChargeAmount(club, c), club.Currency),
                         Category = c.Category,
                         Capacity = c.Capacity,
                         Description = c.Description,
                         EarlyBirds = c.Attributes.EarlyBirds,
                         HasEarlyBird = c.HasEarlyBird,
                         IsProrate = _programBusiness.IsProgramScheduleProrate(program.ProgramSchedules.ElementAt(0), c),
                         ProratePrice = !c.Attributes.IsProrate ? 0 : _programBusiness.CalculateProratePrice(program.ProgramSchedules.ElementAt(0), c),
                         LeftSessions = _programBusiness.CalculateLeftSessions(program.ProgramSchedules.ElementAt(0)),
                         TotalSessions = _programBusiness.CalculateTotalSessions(program.ProgramSchedules.ElementAt(0)),
                         IsMandatory = c.Attributes.IsMandatory,
                         NumberOfRegistration = _orderItemBusiness.RegisteredCount(c.Id, program.Season.Status == SeasonStatus.Test)
                     })
                     .ToList(),

                    ProgramServices = program.ProgramSchedules.First()
                        .Charges.Where(c => (c.Category == ChargeDiscountCategory.Lunch ||
                         c.Category == ChargeDiscountCategory.DayCare ||
                         c.Category == ChargeDiscountCategory.CustomCharge) && c.IsDeleted == false).Select(s => new ChargeViewModel
                         {
                             Name = s.Name,
                             Amount = _programBusiness.CalculateProgramChargeAmount(club, s).Value,
                             StrAmount = CurrencyHelper.FormatCurrencyWithPenny(_programBusiness.CalculateProgramChargeAmount(club, s), club.Currency),
                             Category = s.Category,
                             Capacity = s.Capacity,
                             Description = s.Description,
                         }
                         ).
                         ToList(),
                    Restrict = program.ProgramSchedules.First().AttendeeRestriction,

                    RegistrationPeriod = new RegistrationPeriod(program.ProgramSchedules.First().RegistrationPeriod, _seasonBusiness.GetGeneralRegOpenDate(program.Season), _seasonBusiness.GetGeneralRegCloseDate(program.Season)),

                    RecurrenceTime = (ScheduleAttribute)program.ProgramSchedules.First(o => o.IsDeleted == false).Attributes

                },

                Currency = program.Club.Currency,
                Address = program.ClubLocation.PostalAddress,
                ClubDomain = program.Club.Domain,
                ClubName = clubInfo.Name,
                ClubLogo = program.Club.Logo,
                ClubTimeZone = program.Club.TimeZone,
                Description = program.Description,
                Domain = program.Domain,
                EndDate = _programBusiness.EndDate(program).Value,
                StartDate = _programBusiness.StartDate(program).Value,
                StrEndDate = _programBusiness.EndDate(program).Value.ToString(Constants.DefaultDateFormat),
                StrStartDate = _programBusiness.StartDate(program).Value.ToString(Constants.DefaultDateFormat),
                Id = program.Id,
                LocationName = program.ClubLocation.Name,
                RoomAssignment = program.Room,
                Name = program.Name,
                SeasonDomain = program.Season.Domain,
                MaterialsNeeded = program.Catalog != null ? program.Catalog.MaterialsNeeded : string.Empty,
                ClassDates = _programBusiness.GetClassDates(program.ProgramSchedules.First(), programScheduleMode),
                HasTestimonials = program.HasTestimonial,
                Testimonials = program.Testimonials,
                ClubEmail = clubInfo.Email,
                ClubPhone = PhoneNumberHelper.FormatPhoneNumber(clubInfo.Phone),
                ClubPhoneExtension = clubInfo.PhoneExtension != null ? PhoneNumberHelper.FormatPhoneNumber(clubInfo.PhoneExtension) : string.Empty,
                AppearanceSetting = new ClubAppearanceSetting
                {
                    HidePhoneNumber = clubSetting != null ? (clubSetting.AppearanceSetting != null ? clubSetting.AppearanceSetting.HidePhoneNumber : false) : false,
                    TestimonialsTitle = clubSetting != null ? (clubSetting.AppearanceSetting != null ? (!string.IsNullOrEmpty(clubSetting.AppearanceSetting.TestimonialsTitle) ? clubSetting.AppearanceSetting.TestimonialsTitle : Constants.DefaultTestimonialsTitle) : Constants.DefaultTestimonialsTitle) : Constants.DefaultTestimonialsTitle,
                    TestimonialsBoxBackColor = club.Setting != null && club.Setting.AppearanceSetting != null && !string.IsNullOrEmpty(club.Setting.AppearanceSetting.TestimonialsBoxBackColor) ? club.Setting.AppearanceSetting.TestimonialsBoxBackColor : Constants.DefaultTestimonialsBoxBackColor,
                    HideProgramDates = clubSetting != null ? (clubSetting.AppearanceSetting != null ? clubSetting.AppearanceSetting.HideProgramDates : false) : false,
                    TuitionLabelTextColor = club.Setting != null && club.Setting.AppearanceSetting != null && !string.IsNullOrEmpty(club.Setting.AppearanceSetting.TuitionLabelTextColor) ? club.Setting.AppearanceSetting.TuitionLabelTextColor : Constants.DefaultTuitionLabeltextColor,
                    PriceTextColor = club.Setting != null && club.Setting.AppearanceSetting != null && !string.IsNullOrEmpty(club.Setting.AppearanceSetting.PriceTextColor) ? club.Setting.AppearanceSetting.PriceTextColor : Constants.DefualtPriceTextColor,
                    PriceBackColor = club.Setting != null && club.Setting.AppearanceSetting != null && !string.IsNullOrEmpty(club.Setting.AppearanceSetting.PriceBackColor) ? club.Setting.AppearanceSetting.PriceBackColor : Constants.DefaultPriceBackColor,
                    ContactBoxBackColor = club.Setting != null && club.Setting.AppearanceSetting != null && !string.IsNullOrEmpty(club.Setting.AppearanceSetting.ContactBoxBackColor) ? club.Setting.AppearanceSetting.ContactBoxBackColor : Constants.DefaultContactBoxBackColor,
                    TestimonialsBoxColor = club.Setting != null && club.Setting.AppearanceSetting != null && !string.IsNullOrEmpty(club.Setting.AppearanceSetting.TestimonialsBoxColor) ? club.Setting.AppearanceSetting.TestimonialsBoxColor : Constants.DefaultTestimonialsBoxColor,
                    ShowProviderNameInClassPage = showProviderNameInClassPage,
                }
            };

            var firstSchedule = program.ProgramSchedules.First(o => !o.IsFreezed && !o.IsDeleted);

            if (firstSchedule.Attributes.Capacity > 0)
            {
                ((ProgramInformationModel)programBody).MaxEnrollment = firstSchedule.Attributes.Capacity.ToString();
            }
            if (firstSchedule.Attributes.MinimumEnrollment > 0)
            {
                ((ProgramInformationModel)programBody).MinEnrollment = firstSchedule.Attributes.MinimumEnrollment.ToString();
            }

            programBody.Categories = program.Categories.Select(s =>
                        new SportsClub.Models.CategoryViewModel
                        {
                            Id = s.Id,
                            Name = s.Name,
                        })
                        .ToList();

            programBody.HasProvider = program.OutSourceSeasonId.HasValue;

            if (club.PartnerId.HasValue)
            {
                var partner = _clubBusiness.Get(club.PartnerId.Value);
                if (partner.Domain == "flexacademies")
                {
                    programBody.ClubDomain = "flexacademies";
                }
            }

            if (programBody.HasProvider && program.OutSourceSeason != null)
            {
                var providerClub = _clubBusiness.Get(program.OutSourceSeason.ClubId);
                programBody.ProviderName = providerClub.Name;
                var providerFirstContact = providerClub.ContactPersons.First();
                programBody.ProviderContact = string.Format(Constants.F_FullName, providerFirstContact.FirstName, providerFirstContact.LastName);
                programBody.ProviderPhone = PhoneNumberHelper.FormatPhoneNumber(providerFirstContact.Phone);
                programBody.ProviderEmail = providerFirstContact.Email;
            }

            SetProgramAddtionalInforMation(programBody, program);

            var result = programBody;
            return JsonNet(new { DataSource = result });
        }
        public virtual JsonNetResult ProgramGrades()
        {
            var list = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);

            return JsonNet(list);
        }

        public virtual ActionResult DeleteClubPage(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var result = _pageBusiness.DeleteClubPage(clubId);

            return null;
        }

        public virtual ActionResult GetUrlForLogin(string clubDomain)
        {
            var url = string.Empty;
            var userName = this.GetCurrentUserName();

            var host = Request.Url.Host;
            var scheme = Request.Url.Scheme;
            var loginByTokenUrl = _userProfileBusiness.GetUrlForLogin(clubDomain, userName);

            host = string.Concat(host.Split('.')[1], ".", host.Split('.')[2]);

            url = string.Concat(scheme, "://", clubDomain, ".", host, "/", loginByTokenUrl);

            var model = new { Url = url };

            return JsonNet(model);
        }

        public virtual ActionResult GetGallery()
        {
            return JsonNet(new JbPImageGallery()
            {
                Title = "Gallery 1",
                ElementId = new JbPImageGallery().GetType().Name + Jumbula.Common.Utilities.StaticRandom.Instance.Next(10000, 99999),
                Images = new List<JbPImage>()
                {
                  new JbPImage {ElementId= new JbPImage().GetType().Name + Jumbula.Common.Utilities.StaticRandom.Instance.Next(10000, 99999), Title = "Image 1", smallImage="Images/Markets/Vendors/business_meeting.png" },
                  new JbPImage {ElementId= new JbPImage().GetType().Name + Jumbula.Common.Utilities.StaticRandom.Instance.Next(10000, 99999), Title = "Image 1", smallImage="Images/Markets/Vendors/business_meeting.png" },
                  new JbPImage {ElementId= new JbPImage().GetType().Name + StaticRandom.Instance.Next(10000, 99999), Title = "Image 2", smallImage="Images/Markets/Vendors/Vendors-header.jpg" },

                }
                .ToList()
            });

        }

        private List<Program> GetAllPrograms(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var club = _clubBusiness.Get(ClubSubDomain);
            var seasons = _seasonBusiness.GetList(club.Id).Where(s => seasonIds.Contains(s.Id));

            var programs = _programBusiness.GetList(club.Id).Where(p => p.LastCreatedPage == ProgramPageStep.Step4 && p.TypeCategory != ProgramTypeCategory.Subscription && seasons.Select(s => s.Id).ToList().Contains(p.SeasonId));
            var Allporograms = new List<Program>();
            if (!IncludeCamp)
            {
                programs = programs.Where(p => p.TypeCategory != ProgramTypeCategory.Camp);
            }
            if (!IncludeChessTournament)
            {
                programs = programs.Where(p => p.TypeCategory != ProgramTypeCategory.ChessTournament);
            }
            if (!IncludeClasses)
            {
                programs = programs.Where(p => p.TypeCategory != ProgramTypeCategory.Class);
            }
            if (!IncludeSeminarTour)
            {
                programs = programs.Where(p => p.TypeCategory != ProgramTypeCategory.SeminarTour);
            }

            foreach (var program in programs)
            {
                if (program.TypeCategory == ProgramTypeCategory.Class || program.TypeCategory == ProgramTypeCategory.Camp)
                {
                    foreach (var schedule in program.ProgramSchedules)
                    {
                        DateTime currentDate = schedule.StartDate;
                        while (currentDate <= schedule.EndDate)
                        {
                            DayOfWeek dayOfWeek = currentDate.DayOfWeek;
                            foreach (var item in ((ScheduleAttribute)schedule.Attributes).Days)
                            {
                                if (dayOfWeek == item.DayOfWeek)
                                {
                                    Allporograms.Add(program);
                                }
                            }
                            currentDate = currentDate.AddDays(1);
                        }
                    }
                }
                else
                {
                    Allporograms.Add(program);
                }

            }

            return Allporograms;
        }


        [HttpPost]
        public virtual ActionResult GetProgramLocations(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var club = _clubBusiness.Get(ClubSubDomain);
            var clubLocations = GetAllLocationsInSeason(seasonIds, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);
            var address = clubLocations != null && clubLocations.Any() ? clubLocations.Where(c => c.Club.Id == club.Id).Select(a => a.PostalAddress).Distinct().ToList() : null;
            var model = new List<ProgramLocationsViewModel>();
            model = address != null ? address.Select(a => new ProgramLocationsViewModel
            {
                AddressId = a.Id,
                Address = a.Address,

            }).OrderBy(c => c.Address).ToList() : model;

            var listLocations = new { Locations = model.DistinctBy(a => a.Address.ToLower()) };

            return JsonNet(listLocations);
        }

        [HttpPost]
        public virtual ActionResult GetProgramLocationsName(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var club = _clubBusiness.Get(ClubSubDomain);
            var clubLocations = GetAllLocationsInSeason(seasonIds, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour).ToList();

            var model = clubLocations.Where(l => l.Name != null).Select(a => new ProgramLocationsViewModel
            {
                ClubLocationId = a.Id,
                LocationName = a.Name,

            }).OrderBy(c => c.LocationName);
            return JsonNet(model.DistinctBy(l => l.LocationName.ToLower()));
        }

        [HttpPost]
        public virtual ActionResult GetProgramCities(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var club = _clubBusiness.Get(ClubSubDomain);
            var clubLocations = GetAllLocationsInSeason(seasonIds, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);
            var address = clubLocations != null && clubLocations.Any() ? clubLocations.Where(c => c.Club.Id == club.Id).Select(a => a.PostalAddress).Distinct().ToList() : null;
            var model = new List<ProgramLocationsViewModel>();
            model = address != null ? address.Where(a => !string.IsNullOrEmpty(a.City)).Select(a => new ProgramLocationsViewModel
            {
                AddressId = a.Id,
                City = a.City,
            }).OrderBy(c => c.City).ToList() : model;

            var listCities = new { Cities = model.DistinctBy(c => c.City.ToLower()) };

            return JsonNet(listCities);
        }

        [HttpPost]
        public virtual ActionResult GetCamps(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var programs = GetAllPrograms(seasonIds, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);

            var model = programs.Select(a => new FillterCampViewModel
            {
                Name = a.Name,
                SeasonId = a.SeasonId,
                CampId = a.Id,
            });

            var listCities = new { Camps = model };

            return JsonNet(listCities);
        }


        private List<ClubLocation> GetAllLocationsInSeason(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {

            var programs = GetAllPrograms(seasonIds, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);

            var clubLocations = programs.Select(p => p.ClubLocation).Distinct().ToList();

            return clubLocations;
        }

        [HttpPost]
        public virtual ActionResult GetProgramsCategories(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var programs = GetAllPrograms(seasonIds, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);

            var programsCategories = programs.SelectMany(p => p.Categories);

            var listProgramsCategories = programsCategories.ToList().Distinct().Select(x => new JbTitleValue(x.Name, x.Id));
            return JsonNet(listProgramsCategories.ToList().OrderBy(c => c.Title));
        }

        [HttpPost]
        public virtual ActionResult GetProgramsWeekDays(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var programs = GetAllPrograms(seasonIds, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);

            List<DayOfWeek> programsDay = new List<DayOfWeek>();
            foreach (var item in programs)
            {
                programsDay.AddRange(_programBusiness.GetClassDays(item).Select(pr => pr.DayOfWeek).ToList());
            }
            return JsonNet(programsDay.Distinct());
        }

        [HttpPost]
        public virtual JsonNetResult GetprogramGrades(List<long> seasonIds, bool IncludeCalendar, bool IncludeCamp, bool IncludeChessTournament, bool IncludeClasses, bool IncludeSeminarTour)
        {
            var programs = GetAllPrograms(seasonIds, IncludeCalendar, IncludeCamp, IncludeChessTournament, IncludeClasses, IncludeSeminarTour);

            List<SchoolGradeType?> programsGrade = new List<SchoolGradeType?>();
            foreach (var item in programs)
            {
                programsGrade.Add(_programBusiness.GetMinGrade(item));
                programsGrade.Add(_programBusiness.GetMaxGrade(item));
            }
            return JsonNet(programsGrade);
        }

        [HttpPost]
        public virtual ActionResult UploadGallery(JbPImageGallery imageGallery)
        {
            throw new NotImplementedException();
        }

        public void TuitionBeforeAfterProgramSorting(Program program, List<JbPScheduleViewModel> beforeAfterSchedules)
        {
            foreach (var schedule in beforeAfterSchedules)
            {
                foreach (var tuition in schedule.Tuitions)
                {
                    var charge = program.ProgramSchedules.SelectMany(s => s.Charges).Where(c => c.Id == tuition.ChargeId).FirstOrDefault();

                    if (charge != null)
                    {
                        var chargeWeekDay = ((BeforeAfterCareChargeAttribute)charge.Attributes).NumberOfClassDay;
                        tuition.NumberOfClassDays = int.Parse(chargeWeekDay);
                    }
                }

                schedule.Tuitions = schedule.Tuitions.OrderByDescending(t => t.NumberOfClassDays).ToList();
            }
        }

        public void SetProgramAddtionalInforMation(ProgramInformationModel model, Program program)
        {
            model.ProgramDays = model.Schedule.RecurrenceTime.Days.Select(d => new ProgramDays
            {
                EndTime = d.EndTime,
                StartTime = d.StartTime,
                DayOfWeek = d.DayOfWeek,
                StrTimes = DateTimeHelper.FormatTimeRange(d.StartTime.Value, d.EndTime.Value)

            }).ToList();

            model.RegistrationPriodStartDate = model.Schedule.RegistrationPeriod.RegisterStartDate.Value.ToString(Constants.DefaultDateFormat);
            model.RegistrationPriodEndDate = model.Schedule.RegistrationPeriod.RegisterEndDate.Value.ToString(Constants.DefaultDateFormat);

            model.RestrictionMessages = new ProgramRestrictionMessag();
            switch (model.Schedule.Restrict.RestrictionType)
            {
                case RestrictionType.Age:
                    {
                        model.RestrictionMessages.AgeMessage = Ioc.ProgramBusiness.AgeRestrictionMessage(model.Schedule.Restrict.MinAge, model.Schedule.Restrict.MaxAge, model.Schedule.Restrict.ApplyAtProgramStart);
                    }
                    break;
                case RestrictionType.Grade:
                    {
                        model.RestrictionMessages.GradeMessage = Ioc.ProgramBusiness.GradeRestrictionMessage(model.Schedule.Restrict.MinGrade, model.Schedule.Restrict.MaxGrade, model.Schedule.Restrict.ApplyAtProgramStart);
                    }
                    break;
            }

            if (model.Schedule.Restrict.Gender.HasValue)
            {
                if (model.Schedule.Restrict.Gender != Genders.NoRestriction)
                {
                    model.RestrictionMessages.GenderMessage = "This class is open to " + model.Schedule.Restrict.Gender.ToDescription().ToLower() + "s" + " only.";
                }

            }

            var allImageUrls = new List<string>();

            if (program.CatalogId.HasValue && program.Catalog != null && !string.IsNullOrEmpty(program.Catalog.ImageFileName))
            {
                allImageUrls.Add(program.Catalog.ImageFileName);

                if (program.Catalog.ImageGallery_Id.HasValue)
                {
                    var fileNames = program.Catalog.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();

                    if (fileNames != null)
                    {
                        allImageUrls.AddRange(fileNames);
                        model.ImageUrls = GetCatalogDicImagesURL(allImageUrls, program.Catalog.ImageGallery.ClubDomain);
                    }
                }
                else
                {
                    model.ImageUrls = GetCatalogDicImagesURL(allImageUrls, program.Catalog.Club.Domain);
                }
            }
            else if (program.ImageGalleryId.HasValue)
            {
                var fileNames = program.ImageGallery.Images.Where(i => i.Status == EventStatusCategories.open).Select(i => i.Path).ToList();

                if (fileNames != null)
                {
                    allImageUrls.AddRange(fileNames);
                    model.ImageUrls = GetCatalogDicImagesURL(allImageUrls, program.ImageGallery.ClubDomain);

                }
            }
        }

        public ActionResult GetPageData()
        {

            var clubDomain = this.GetCurrentClubDomain();

            RoleCategory? roleCategory = null;
            if (this.IsUserAuthenticated())
            {
                roleCategory = this.GetCurrentUserRole();
            }

            string switchedParentUserName = null;

            if (JbUserService.IsSwitchedAsParentMode())
            {
                var switchedParentUserId = JbUserService.GetSwitchedParentUserId(this.GetCurrentUserId());

                var parent = _userProfileBusiness.Get(switchedParentUserId);

                switchedParentUserName = parent.UserName;
            }

            var clubs = _clubBusiness.GetMemeberClubs(clubDomain, this.GetCurrentUserName(), roleCategory);

            var result = new
            {
                Clubs = clubs,
                SwitchedParentUserName = switchedParentUserName
            };

            return JsonNet(result);
        }

        public List<ImageUrls> GetCatalogDicImagesURL(List<string> fileNames, string clubDomain)
        {
            var ImageUrls = new List<ImageUrls>();
            foreach (var name in fileNames)
            {
                string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), name).AbsoluteUri;
                string largeImageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), "large-" + name).AbsoluteUri;
                var isExists = StorageManager.IsExist(largeImageUri);
                var existLargUri = isExists ? largeImageUri : null;
                if (string.IsNullOrEmpty(name))
                {
                    imageUri = string.Empty;
                }
                if (!string.IsNullOrEmpty(imageUri) && !string.IsNullOrEmpty(existLargUri))
                {
                    ImageUrls.Add(new ImageUrls
                    {
                        SmallUrl = imageUri,
                        LargeUrl = existLargUri

                    });
                }
                else
                {
                    ImageUrls.Add(new ImageUrls
                    {
                        SmallUrl = imageUri,
                        LargeUrl = null

                    });
                }
            }
            return ImageUrls;
        }
    }
}