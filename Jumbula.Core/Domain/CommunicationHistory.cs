﻿using Jumbula.Common.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class CommunicationHistory : BaseEntity
    {
        [ForeignKey("EmailId")]
        public virtual Email Email { get; set; }

        public int? EmailId { get; set; }

        [ForeignKey("ContactId")]
        public virtual Contact Contact{ get; set; }

        public int? ContactId { get; set; }

        [StringLength(128)]
        public string RelatedEntity { get; set; }

        public long RelatedEntityId { get; set; }

    }
}
