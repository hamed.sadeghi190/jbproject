﻿using System;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class PendingPreapprovalTransaction : BaseEntity<long>
    {
        public long OrderId { get; set; }
        public long? InstallmentId { get; set; }
        public DateTime DueDate { get; set; }
        public decimal DueAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public PendingPreapprovalTransactionStatus Status { get; set; }
        public virtual Order Order { get; set; }
        public virtual OrderInstallment Installment { get; set; }
    }

    public enum PendingPreapprovalTransactionStatus : byte
    {
        Pending = 0,
        Completed = 1,
        PartiallyCompleted = 2,
        Error = 8,
        Stop = 9,
        Deleted = 10
    }
}
