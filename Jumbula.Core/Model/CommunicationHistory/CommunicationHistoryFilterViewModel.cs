﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Model.CommunicationHistory
{
    public class CommunicationHistoryFilterViewModel
    {
        public DateTime Date { get; set; }

        public List<SelectListItem> Categories { get; set; }
        public ContactCategory Category { get; set; }

        public List<SelectListItem> Sender { get; set; }

        public DateTime DeliverDateTime { get; set; }

        public DateTime OpenDateTime { get; set; }

        public string Note { get; set; }

        public string RelatedEntity { get; set; }

        public int RelatedEntityId { get; set; }
    }
}
