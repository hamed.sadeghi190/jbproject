﻿namespace Jumbula.Core.Model.Email
{
    public class RequestDemoEmailParameterModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string OrganizationName { get; set; }

        public string Message { get; set; }
    }
}
