﻿using SportsClub.Domain;
using SportsClub.Domain.Generic;
using SportsClub.Infrastructure;
using SportsClub.Infrastructure.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class TourneyOrderDetailsViewModel
    {
        public string TourneyName { get; set; }

        public string PlayerName { get; set; }

        public string TotalPrice { get; set; }

        public string Charges { get; set; }

        public string Donation { get; set; }

        public string Discounts { get; set; }

        public string TotalAllPrice { get; set; }

        public string ConfirmationID { get; set; }

        public string Date { get; set; }

        public List<TourneyOrderDetailsItemViewModel> OrderDetails { get; set; }

        public dynamic OrderCharges { get; set; }

        public dynamic OrderDiscounts { get; set; }

        public TourneyOrderDetailsViewModel(int orderId, string eventName, string playerName)
        {
            TourneyName = eventName;

            PlayerName = playerName;

            //Get order 
            Order order = Ioc.IOrderDbService.GetOrder(orderId, true);

            // order and orderitems charges
            OrderCharges = new List<ChargeDiscount>();
            foreach (var charge in order.OrderChargeDiscounts.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge))
            {
                // OrderCharges.Add(charge.ToChargeDiscount());
            }

            foreach (var charge in order.OrderItems.First().OrderChargeDiscounts.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge))
            {
                //OrderCharges.Add(charge.ToChargeDiscount());
            }
            //OrderCharges.AddRange(order.Charges);
            //OrderCharges.AddRange(order.OrderItems.First().OrderCharges.Where(m=>m.Subcategory== ChargeDiscountSubcategory.Charge));

            // order and orderitems discounts

            OrderDiscounts = new List<ChargeDiscount>();
            //OrderDiscounts.AddRange(order.Discounts);
            //OrderDiscounts.AddRange(order.OrderItems.First().Discounts);
            foreach (var dis in order.OrderChargeDiscounts.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount))
            {
               // OrderDiscounts.Add(dis.ToChargeDiscount());
            }

            foreach (var dis in order.OrderItems.First().OrderChargeDiscounts.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount))
            {
                OrderCharges.Add(dis.ToChargeDiscount());
            }

            Date = order.OrderDate.ToShortDateString();
            ConfirmationID = order.ConfirmationId;

            var charges = order.OrderChargeDiscounts.Where(m => m.Subcategory == ChargeDiscountSubcategory.Charge).Sum(c => c.Amount)
                            +
                         order.OrderItems.SelectMany(orderItem => orderItem.OrderChargeDiscounts.Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge)).Sum(item => item.Amount);

            Charges = Utilities.FormatCurrencyWithPenny(charges);

            Donation = Utilities.FormatCurrencyWithoutPenny(order.OrderChargeDiscounts.Where(c => c.Category == ChargeDiscountCategory.Donation).Sum(c => c.Amount));

            var discount = order.OrderChargeDiscounts.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount).Sum(d => d.Amount)
                               + order.OrderItems.SelectMany(orderitem => orderitem.OrderChargeDiscounts.Where(m => m.Subcategory == ChargeDiscountSubcategory.Discount)).Sum(item => item.Amount);
            Discounts = Utilities.FormatCurrencyWithoutPenny(discount);


            //For security reasons
            int userId = TheMembership.GetCurrentUserId();

            List<TourneyOrderDetailItem> items = Ioc.IOrderDbService.TourneyOrderDetails(orderId, userId).ToList();

            if (items != null)
            {
                OrderDetails = items.Select(i => new TourneyOrderDetailsItemViewModel
                {
                    Section = i.Section,
                    Schedule = i.Schedule,
                    Options = i.Options,
                    Byes = i.Byes,
                    Fees = i.Fees
                })
                .ToList();

                TotalPrice = Utilities.FormatCurrencyWithPenny(OrderDetails.Sum(i => i.Fees));

                TotalAllPrice = Utilities.FormatCurrencyWithPenny(order.OrderAmount);
            }
        }
    }
}