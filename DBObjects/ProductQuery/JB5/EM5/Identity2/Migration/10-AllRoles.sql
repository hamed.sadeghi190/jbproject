USE [SportsClubProd12Aug]
GO
SET IDENTITY_INSERT [dbo].[JbRoles] ON 

INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (1, N'Accountant')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (2, N'Admin')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (3, N'Contributor')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (4, N'Instructor')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (5, N'Manager')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (6, N'Owner')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (7, N'Parent')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (8, N'Partner')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (9, N'Support')
INSERT [dbo].[JbRoles] ([Id], [Name]) VALUES (10, N'SysAdmin')
SET IDENTITY_INSERT [dbo].[JbRoles] OFF
