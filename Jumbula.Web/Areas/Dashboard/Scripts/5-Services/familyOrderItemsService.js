﻿(function () {
    'use strict';

    angular.module('jbTools').service("familyOrderItemsService", [function () {

        var _model;

        function setOrderitemModel(Data) {
            _model = Data;
        }

        function getOrderitemModel() {
            return _model;
        }

        return {
            set: setOrderitemModel,
            get: getOrderitemModel
        }
    }]);
})();