﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('catalogSettingsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'catalog-settings-view';
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        //image cropper start
        $scope.cropper = {};
        $scope.cropper.sourceImage = null;
        $scope.cropper.croppedImage = null;
        $scope.bounds = {};
        $scope.bounds.left = 0;
        $scope.bounds.right = 0;
        $scope.bounds.top = 0;
        $scope.bounds.bottom = 0;

        $http.get('Dashboard/Catalog/GetSettings').success(function (data, status, headers, config) {
            $scope.Model = data;

            $scope.AllCounties = $scope.Model.AllCounties;

            $scope.Model.AllCounties = null;

            $scope.cropper.croppedImage = $scope.Model.Image;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.save = function () {
            $scope.Model.Image = $scope.cropper.croppedImage;

            $http.post('Dashboard/Catalog/SaveSettings', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {

                     $scope.errors = null;

                     jbToast.success("Saved successfully.", "");

                     $state.go("Catalog");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }
 
        $scope.init = function () {
            $('[data-role=dropdown]').dropdown();
        }
    }]);
})();

