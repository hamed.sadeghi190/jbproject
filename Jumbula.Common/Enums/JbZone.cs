﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum JbZone
    {
        [Description("Parent dashboard")]
        ParentDashboard,
        [Description("Admin dashboard")]
        AdminDashboard
    }
}
