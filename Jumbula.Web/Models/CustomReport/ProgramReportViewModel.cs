﻿using Jb.Framework.Common.Forms;
using Jb.Framework.Web.Model;
using SportsClub.Domain;
using SportsClub.Infrastructure.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ProgramReportViewModel : BaseViewModel<EventRoaster>
    {

        public ProgramReportViewModel()
        {
            JbForm = new JbForm();
        }

        public ProgramReportViewModel(long seasonId, int clubId)
        {
            JbForm = new JbForm();
            GenerateJbForm(seasonId, clubId);
        }

        [Required(AllowEmptyStrings = false)]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public long? SeasonId { get; set; }

        public JbForm JbForm { get; set; }

        private void GenerateJbForm(long seasonId, int clubId)
        {
            var newform = new JbForm();

            //var jbForms =
            //    Ioc.SeasonBusiness.Get(seasonId).Programs.Where(m => m.JbForm != null).Select(m => m.JbForm).ToList();

            var jbForms =
                Ioc.ClubBusiness.GetFormTemplates(clubId, FormType.Registration).Select(m => m.JbForm).ToList();

            //jbForms.CopyByTitle(newform);

            foreach (var form in jbForms)
            {
                try
                {
                    form.CopyByTitle(newform);
                }
                catch 
                {

                }
            }

            var lastSection = new JbSection();
            var moreSection = new JbSection()
            {
                Name = "MoreInfoSection",
                Title = "More Info",
                Elements = new List<IJbBaseElement> 
                {
                    new JbCheckBox { Name="Tuition",Title="Tuition Price" },
                    //new JbCheckBox { Name="PaidAmount",Title="Paid" },
                    new JbCheckBox { Name="DiscountTotal",Title="Discount Total" },
                    new JbCheckBox { Name="OrderDate", Title="Registrant Date" },
                    new JbCheckBox { Name="ProgramStart",Title="Program Start Date" },
                    new JbCheckBox { Name="ProgramEnd",Title="Program End Date" },
                    new JbCheckBox { Name="ProgramLocation",Title="Program Location" },
                    new JbCheckBox { Name="ProgramType",Title="Program Type" },
                }
            };
            foreach (var element in newform.Elements)
            {
                if (element is JbSection)
                {
                    lastSection = new JbSection() { Name = element.Name, Title = element.Title };
                    JbForm.Elements.Add(lastSection);
                    foreach (var element2 in (element as JbSection).Elements.Where(e => !(e is JbHidden)))
                    {
                        var checkbox = new JbCheckBox() { ElementId = element2.ElementId, Name = element2.Name, Title = element2.Title };
                        lastSection.Elements.Add(checkbox);
                    }
                }
                else
                {
                    var checkbox = new JbCheckBox();
                    Jb.Framework.Helper.AutoMapper.Map(element, ref checkbox);
                    (lastSection as JbSection).Elements.Add(checkbox);
                }
            }
            JbForm.Elements.Add(moreSection);
        }

    }
}