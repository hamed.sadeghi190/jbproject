﻿using Jumbula.Common.Enums;
using System.ComponentModel;
using Jumbula.Common.Attributes;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ProgramCreateEditModelStep4 : BaseViewModel<Program, long>, IProgramWizardModel
    {
        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }
        public int? PartnerCommisionRate { get; set; }

        [DisplayName("no-show penalty")]
        public decimal? PartnerNoshowpenalty { get; set; }

        [DisplayName("late check-in penalty")]
        public decimal? PartnerLateCheckInPenalty { get; set; }

        public bool IsPartnerProvider { get; set; }

        public ProgramStatus Status { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public ProgramTypeCategory TypeCategory { get; set; }

        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }

        public ProgramPageStep PageStep { get; set; }

        public ProgramPageStep LastCreatedPage { get; set; }

        [MustBeTrue(ErrorMessage="You should confirm agreement.")]
        public bool IsConfiremed { get; set; }

        public bool HasPTAFee { get; set; }
        public bool HasGenerationFee { get; set; }
        public string PTAFee { get; set; }
        public string LeadGenerationFee { get; set; }
        public SaveType SaveType { get; set; }
        public bool IsFlex { get; set; }
    }
}