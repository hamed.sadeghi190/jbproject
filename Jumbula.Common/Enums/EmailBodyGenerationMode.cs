﻿using System;

namespace Jumbula.Common.Enums
{
    [Flags]
    public enum EmailBodyGenerationMode
    {
        Normal = 0,
        Preview = 1,
        SamplePreview = 2
    }
}
