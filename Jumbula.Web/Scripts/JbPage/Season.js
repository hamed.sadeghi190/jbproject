﻿;
(function () {
    'use strict';

    angular.module('pageBuilder',
        [
            'angular-loading-bar',
            'dndLists',
            'ngAnimate',
            'ngSanitize',
            'ui.bootstrap',
            'kendo.directives',
            'angularFileUpload',
            'wu.masonry',
            'dm.stickyNav'
        ])
        .config([
            '$httpProvider', function ($httpProvider) {
                $httpProvider.defaults.headers.common["FROM-ANGULAR"] = "true";
            }
        ])
        .service('kendoImageUrl',
        function ($http) {
            var property = '';
            $http.get("KendoEditorFiles/FileUrl?path=JbPageBuilder").success(
                function (data, status, headers, config) {
                    property = data;

                });
            return {
                getProperty: function () {
                    return property;
                }
            };
        })
        //.factory('youtubeId', function () {
        //    return {
        //        id: function (url) {
        //            var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        //            var match = url.match(regExp);
        //            if (match && match[2].length == 11) {
        //                return match[2];
        //            } else {
        //                return url;
        //                //error
        //            }
        //        }
        //    }
        //})
        .controller('mainController', ['$scope', '$http', '$modal', '$timeout', '$sce',
            function ($scope, $http, $modal, $timeout, $sce) {
         
                // It use for fixed tabs to top
                $scope.tabFixedTop = function (index) {
                    if (index == 0) {
                        if (!$scope.if.isDesignMode()) {
                            $("#mainHeader>nav.navbar.navbar-default").addClass('navbar-fixed-top');
                            $(".jb-home-tabset>ul.nav.nav-pills", document).addClass('tab-fixed-top');
                        }
                        else {
                            $(".jb-home-tabset .tab-content").css('paddingTop', '0px');
                        }

                    }
                }



                if (is.undefined(window.location.origin)) {
                    window.location.origin = window.location.protocol +
                        "//" +
                        window.location.host +
                        (window.location.port != "" ? ":" + window.location.port : "");
                }

                function isIframe() {
                    try {
                        return window.self !== window.top;
                    } catch (e) {
                        return true;
                    }
                }


                var baseUrl = 'JbPage';

                $http.get(baseUrl + '/GetCartItemCount')
                    .then(function (response) {

                        $scope.cartModel = response.data;
                    });

                $http.get('/JbPage/GetPageData')
                    .then(function (response) {

                        console.log(response);

                        $scope.Clubs = response.data.Clubs;
                        $scope.SwitchedParentUserName = response.data.SwitchedParentUserName;
                    });

                var _designModeFunctionsRegistered = false;
                $scope.registerDesignModeFunctions = function () {
                    if ($scope.if.isDesignMode() && !_designModeFunctionsRegistered) {
                        _designModeFunctionsRegistered = true;
                        window.onbeforeunload = function () {
                            return "Changes you made may not be saved."
                        };
                        $scope.loadScript = function (url, type, charset) {
                            if (type === undefined) type = 'text/javascript';
                            if (url) {
                                var script = document.querySelector("script[src*='" + url + "']");
                                if (!script) {
                                    var heads = document.getElementsByTagName("head");
                                    if (heads && heads.length) {
                                        var head = heads[0];
                                        if (head) {
                                            script = document.createElement('script');
                                            script.setAttribute('src', url);
                                            script.setAttribute('type', type);
                                            if (charset) script.setAttribute('charset', charset);
                                            head.appendChild(script);
                                        }
                                    }
                                }
                                return script;
                            }
                        };
                        //load cropper resource in design mode
                        $scope.loadScript('/Content/ngCropper/cropper.js', 'text/javascript', 'utf-8');
                        angular.element('head')
                            .append('<link href="/Content/ngCropper/cropper.min.css" rel="stylesheet">');

                        // Auto saving [disabled by MGH - requested from Jalal on 14 May 2016]
                        /*setTimeout(function () {
                            $scope._lastSavedModel = JSON.stringify($scope.Model);
                            setInterval(function () {
                                var newModelStringed = JSON.stringify($scope.Model);
                                if (newModelStringed != $scope._lastSavedModel) {
                                    $scope.save(false);
                                    $scope._lastSavedModel = newModelStringed;
                                }
                            }, 15000);
                        }, 15000);*/

                        $http.get(baseUrl + '/GetListLinks')
                            .then(function (response) {
                                $scope.Programs = response.data;

                                //We need to update reglink before they use. @Mo
                                $http.post(baseUrl + '/UpdateRegLinks', { links: $scope.Programs }).then(
                                    function (responseUpdate) {
                                        $scope.Programs = responseUpdate.data;

                                    },
                                    function (responseUpdate) {
                                        //$scope.status = "An error occurred...";
                                        //$("#statusBar").addClass("error");
                                        console.log("ERROR:");
                                        console.log(responseUpdate);
                                    });


                            },
                            function (response) {
                                $scope.status = "An error occurred...";
                                $("#statusBar").addClass("error");
                                console.log("ERROR:");
                                console.log(response);
                            });

                        $http.get(baseUrl + '/GetListBeforeAfterCareLinks')
                            .then(function (response) {
                                $scope.BeforeAfterCarePrograms = response.data;

                                //We need to update before-afters-programs before they use.
                                $http.post(baseUrl + '/UpdateBeforeAfterLinks',
                                    { links: $scope.BeforeAfterCarePrograms }).then(function (responseUpdate) {
                                        $scope.BeforeAfterCarePrograms = responseUpdate.data;

                                    },
                                    function (responseUpdate) {
                                        console.log("ERROR:");
                                        console.log(responseUpdate);
                                    });


                            },
                            function (response) {
                                $scope.status = "An error occurred...";
                                $("#statusBar").addClass("error");
                                console.log("ERROR:");
                                console.log(response);
                            });


                        $http.get('/Dashboard/Season/GetClubSeasons')
                            .then(function (response) {
                                $scope.Seasons = response.data;
                            },
                            function (response) {
                                $scope.status = "An error occurred...";
                                $("#statusBar").addClass("error");
                                console.log("ERROR:");
                                console.log(response);
                            });

                        $http.get('/Dashboard/Season/GetClubSeasonsDomain')
                            .then(function (response) {
                                $scope.SeasonsDomain = response.data;
                            },
                            function (response) {
                                $scope.status = "An error occurred...";
                                $("#statusBar").addClass("error");
                                console.log("ERROR:");
                                console.log(response);
                            });

                        $http.get(baseUrl + '/GetElementTemplates')
                            .then(function (response) {
                                $scope.ElementTemplates = response.data;
                                $scope.AddableElements = [
                                    $scope.ElementTemplates.JbPTitlePack,
                                    $scope.ElementTemplates.JbPDivider,
                                    $scope.ElementTemplates.JbPSection,
                                    $scope.ElementTemplates.JbPGrid,
                                    $scope.ElementTemplates.JbPSeasonGrid,
                                    $scope.ElementTemplates.JbPRegLink,
                                    $scope.ElementTemplates.JbPBeforeAfterCareRegLinks,
                                    $scope.ElementTemplates.JbPRegBtn,
                                    $scope.ElementTemplates.JbPSeasonCalendar,  
                                    $scope.ElementTemplates.JbPContact,
                                    $scope.ElementTemplates.JbPMap,
                                    $scope.ElementTemplates.JbPVideo,
                                    $scope.ElementTemplates.JbPImageGallery,
                                    $scope.ElementTemplates.JbPImageSlider,
                                ];
                                for (var i = 0; i < $scope.AddableElements.length; i++) {
                                    $scope.AddableElements[i]._IsInAddProcess = true;
                                }
                            },
                            function (response) {
                                $scope.status = "An error occurred...";
                                $("#statusBar").addClass("error");
                                console.log("ERROR:");
                                console.log(response);
                            });


                        $scope.cartItem = 0;

                        $scope.save = function (saveType) {

                            if (saveType != 'Publish' && $("#save-as-draft").hasClass("disabled")) {
                                return;
                            }
                            if (saveType == 'Publish' && $("#save-and-publish").hasClass("disabled")) {
                                return;
                            }

                            $scope.Model.SaveType = saveType ? saveType : 'Draft';
                            $("#statusBar").addClass("loading");
                            $scope.status = "Saving...";

                            $http.post(baseUrl + '/Manage', { page: $scope.Model }).then(function (response) {

                                $scope.status = "Saved";

                                $("#statusBar").removeClass();
                                $("#statusBar").addClass("saved");

                                if (saveType == 'Publish') {

                                    $scope.load('Publish');
                                }

                                //store last model
                                var newModelStringed = angular.copy($scope.Model);
                                $scope._lastSavedModel = newModelStringed;

                                $scope.ModelPublished = false;

                                setTimeout(function () {
                                    $scope.$apply(function () {
                                        $("#statusBar").removeClass("saved");
                                        $scope.status = "";

                                        //update Tabs list after publish
                                        if (saveType == 'Publish') {

                                            $scope.Tabs = new Array();
                                            $scope.Tabs = $scope.tabsData($scope.PublishedTabs);

                                            $scope.ModelPublished = true;
                                        }

                                    });
                                },
                                    3000);

                                //detachEvent for page change reload
                                $scope.ModelChanged = false;

                                if (window.removeEventListener) { // For all major browsers, except IE 8 and earlier
                                    window.removeEventListener("beforeunload", function (event) { }, false);
                                } else if (window.detachEvent) { // For IE 8 and earlier versions
                                    window.detachEvent("beforeunload", function (event) { });
                                }

                                window.beforeunload = null;

                            },
                                function (response) {
                                    $scope.status = "An error occurred...";
                                    $("#statusBar").addClass("error");
                                    console.log("ERROR:");
                                    console.log(response);
                                });
                        }
                
                        $scope.reset = function (tab, exit) {
                            if (confirm("Clear the tab and discard all the changes?")) {

                                var index = $scope.Model.Elements.indexOf(tab);
                                $scope.Model.Elements[index].Elements = [];
                                $scope.Model.Elements[index].Cover.Visible = false;

                            }
                        }

                        $scope.returnToDashboard = function () {

                            //I used angular.toJson instead of JSON.strinigfy casue of prevent to insert $$hash key
                            var newModelStringed = angular.toJson($scope.Model);
                            var _lastSavedModel = angular.toJson($scope._lastSavedModel);

                            //if (newModelStringed != _lastSavedModel) {
                            //if ($scope.ModelChanged) {
                            if (!$("#save-as-draft").hasClass("disabled")) {
                                if (confirm("You have unsaved work. \nDiscard changes and return to the dashboard?")) {
                                    window.location = "/Dashboard";
                                } else {
                                    return false;
                                }
                            } else {
                                window.location = "/Dashboard";
                                //$scope.save(false);
                            }
                        }

                        $scope.ModelChanged = false;


                        $scope.$watch("Model",
                            function (newValue, oldValue) {
                                $scope.disableSave = false;
                            },
                            true);

                        /*$scope.$watch("Model", function (newValue, oldValue) {
                            if (!$scope.ModelChanged) {
            
                                $($scope.Model.Elements).each(function (index, value) {
                                    if ($scope._lastSavedModel.Elements[index] != undefined)
                                        $scope._lastSavedModel.Elements[index].active = $scope.Model.Elements[index].active
                                })
            
                                var newModelStringed = angular.toJson($scope.Model);
                                var _lastSavedModel = angular.toJson($scope._lastSavedModel);
                            }
            
                            if ($scope.ModelChanged || (newModelStringed != _lastSavedModel)) {
                                $scope.ModelChanged = true;
            
                                window.addEventListener("beforeunload", function (event) {
                                    var confirmationText = "You have unsaved work.";
            
                                    event.returnValue = confirmationText; // Gecko, Trident, Chrome 34+
                                    return confirmationText;              // Gecko, WebKit, Chrome <34
            
                                   
                                }, false);
            
                                return null;
                               
                            } else {
                                if (window.removeEventListener) {                   // For all major browsers, except IE 8 and earlier
                                    window.removeEventListener("beforeunload", function (event) { });
                                } else if (window.detachEvent) {                    // For IE 8 and earlier versions
                                    window.detachEvent("beforeunload", function (event) { });
                                }
            
                                return null;
                            }
                        }, true)*/

                        $scope.isModelPublished = function () {

                            if ($scope.ModelPublished) {
                                return true;
                            }
                            return false;
                        }

                        $scope.isModelChanged = function () {
                            var newModelStringed = angular.toJson($scope.Model);
                            var _lastSavedModel = angular.toJson($scope._lastSavedModel);

                            if (newModelStringed != _lastSavedModel) {
                                return true;
                            }
                            return false;
                            //return $scope.ModelChanged ? true : false;
                        }

                        $scope.WidgetDropedInTab = function (index, item, list) {

                            if (item.Type == "JbPMap") {

                                setTimeout(function () {
                                    $scope.map(item.ElementId);
                                },
                                    3000);

                                return item;

                            }

                            if (item.Type != 'JbPSeasonCalendar' &&
                                item.Type != 'JbPSeasonGrid' &&
                                item.Type != 'JbPRegBtn' &&
                                item.Type != 'JbPContact' &&
                                item.Type != 'JbPVideo' &&
                                item.Type != 'JbPImageGallery' &&
                                item.Type != 'JbPImageSlider' &&
                                item.Type != 'JbPBeforeAfterCareRegLinks') {
                                return item;
                            }

                            //start modal for season selector
                            if (item._IsInAddProcess) {

                                var _Model = angular.copy($scope.Model);

                                $scope.Model._Active = item;


                                //  console.log($scope.Model._Active);
                                var modalInstance = $modal.open({
                                    animation: true,
                                    templateUrl: item.Type + 'Edit.html',
                                    controller: 'activeElementCtrl',
                                    size: "lg",
                                    backdrop: 'static',
                                    resolve: {
                                        Model: function () {
                                            return $scope.Model;
                                        },
                                        Programs: function () {
                                            return $scope.Programs;
                                        },
                                        Seasons: function () {
                                            return $scope.Seasons;
                                        },
                                        BeforeAfterCarePrograms: function () {
                                            return $scope.BeforeAfterCarePrograms;
                                        },
                                        SeasonsDomain: function () {
                                            return $scope.SeasonsDomain;
                                        },
                                        Parent: function () {
                                            return parent;
                                        }
                                    }
                                });

                                modalInstance.result.then(function (Model) {
                                    var seasons = $scope.Seasons;

                                    if (item.Type == 'JbPContact') {
                                        list.splice(
                                            index,
                                            0,
                                            angular.copy($scope.Model._Active));
                                    }

                                    if (item.Type == 'JbPVideo') {
                                        list.splice(
                                            index,
                                            0,
                                            angular.copy($scope.Model._Active));

                                        //$scope.$apply();
                                    }

                                    if (item.Type == 'JbPBeforeAfterCareRegLinks') {
                                        list.splice(
                                            index,
                                            0,
                                            angular.copy($scope.Model._Active));

                                        //$scope.$apply();
                                    }
                                    if (item.Type == 'JbPImageSlider') {
                                        list.splice(
                                            index,
                                            0,
                                            angular.copy($scope.Model._Active));

                                        //$scope.$apply();
                                    }
                                    if (item.Type == 'JbPImageGallery') {
                                        list.splice(
                                            index,
                                            0,
                                            angular.copy($scope.Model._Active));


                                        //$scope.$apply();
                                    }

                                    if (item.Type == 'JbPRegBtn') {
                                        if (typeof $scope.Model._Active._season === 'object') {
                                            var seasonId = $scope.Model._Active._season.Key;
                                            seasonId = seasonId.toString();
                                        } else {
                                            var seasonId = $scope.Model._Active._season;
                                        }

                                        for (var i = 0; i < $scope.SeasonsDomain.length; i++) {
                                            if ($scope.SeasonsDomain[i].Key == seasonId) {
                                                item.SeasonDomain = $scope.SeasonsDomain[i].Value;

                                                break;
                                            }
                                        }
                                        var alignment = $scope.Model._Active.Alignment;
                                    } else {
                                        if ($scope.Model._Active.Type == "JbPSeasonGrid") {
                                            var seasonId = $scope.Model._Active.SeasonId;
                                        } else {
                                            var seasonId = $scope.Model._Active._season;
                                        }

                                    }

                                    delete $scope.Model._Active._season;
                                    //delete $scope.Model._Active._alignment;
                                    delete item._IsInAddProcess;
                                    //if (!item.IsExternal) {
                                    seasonId = String(seasonId)

                                    if (is.not.undefined(seasonId) && seasonId.length > 0) {
                                        for (var i = 0; i < seasons.length; i++) {
                                            //for (var j = 0; j < seasonId.length; j++) {
                                            //if (seasons[i].Key == seasonId[j]) {
                                            if (seasons[i].Key == seasonId) {
                                                /*for (var propertyName in seasons[i]) {
                                                    if (is.inArray(propertyName,
                                                    [
                                                        "$$hashKey"
                                                        //"LinkTitle",
                                                        //"ShowAgeRestrictions",
                                                        //"ShowGradeRestrictions",
                                                        //"ShowLocation",
                                                        //"ShowRegistrationPeriod",
                                                        //"ShowStartEndDates",
                                                        //"ShowDaysTimes",
                                                        //"DaysTimesFormat",
                                                        //"ShowCapacityLeft",
                                                        //"ShowGenderRestrictions",
                                                        //"AdditionalNote"
                                                    ])) {
                                                        continue;
                                                    }
                                                    $scope.Model._Active[propertyName] = seasons[i][propertyName];
                                                }*/
                                                //$scope.Model._Active[propertyName] = seasons[i][propertyName];

                                                $scope.Model._Active.SeasonId = seasons[i].Key;
                                                $scope.Model._Active.ElementId =
                                                    Math.round(Math.random() * 1000000);
                                                list.splice(
                                                    index,
                                                    0,
                                                    angular.copy($scope.Model._Active));

                                                //
                                            }
                                            //}
                                        }
                                    }
                                    //else {
                                 
                                    //console.log(seasonId)
                                    //$scope.Model._Active._season = {};
                                    //$scope.Model._Active._season.Key = $scope.Model._Active.SeasonId;
                                    //}
                                    /*} else {
                                        $scope.Model._Active.ElementId = Math.round(Math.random() * 1000000);
                                        $scope.Model._Active.SeasonId = null;
                                        list.splice(
                                            index,
                                            0,
                                            angular.copy($scope.Model._Active));
                                    }*/
                                },
                                    function (reason) {
                                        //Array.prototype.push.apply(_Model, $scope.Model.newImages);
                                        $scope.Model = _Model;

                                    });
                            } else {
                                return item;
                            }
                        }

                        //start tab
                        /* $scope.Model.jbConfirmationModal = { show: false };
                         var newModelStringed = angular.copy($scope.Model);
                         $scope._lastSavedModel = newModelStringed;
             
                         //disabled by MGH [2016-5-17]
                         $scope.jbConfirmation = function (tab) {
             
                             if ($scope.Model.Elements.length > 1) {
                                 $scope.Model.jbConfirmationModal = {
                                     show: true,
                                     title: "Warning!",
                                     message: "Are you sure you want to proceed with this action?",
                                     yesText: "Yes, I'm sure",
                                     noText: "Cancel",
                                     clear: function () {
                                         this.title = this.message = this.yes = this.no = this.yesText = this.noText = null;
                                         this.show = false;
                                     },
             
                                     keyup: function (e) {
                                         if (e.keyCode == 27) {
                                             this.clear();
                                         }
                                     },
                                     focusOnYes: function (e) {
                                         $(e.currentTarget).find(".yes").focus();
                                     },
                                     //title: title || "Warning!",
                                     //message: message || "Are you sure you want to proceed with this action?",
                                     yes: function () {
                                         $scope.deleteTab(tab);
                                         this.clear();
                                     },
                                     no: function () {
                                         this.clear();
                                     },
                                     //yesText: yesText || "Yes, I'm sure",
                                     //noText: noText || "Cancel"
                                 };
                             }
                         }
                         */

                        $scope.enableEditor = function (tab) {
                            $scope.currentTabTitle = tab.Title;

                            if (index) {
                                $scope.Model.Elements[index].view.editorEnabled = false;
                            }
                            var index = $scope.Model.Elements.indexOf(tab);

                            $scope.Model.Elements[index].view = {
                                editorEnabled: false,
                                Title: tab.Title
                            }
                            $scope.Model.Elements[index].view.editorEnabled = true;
                            $scope.Model.Elements[index].view.Title = tab.Title;
                            setTimeout(function () {
                                angular.element.find('input')[index].focus();
                            });
                        };


                        $scope.disableEditor = function (tab, type) {
                            var index = $scope.Model.Elements.indexOf(tab);
                            if (type == 'cancel') {
                                tab.Title = $scope.currentTabTitle;
                            }
                            $scope.Model.Elements[index].view.editorEnabled = false;

                        };
                        $scope.saveTabName = function (tab, $event, type) {
                            if (type == undefined || (event.sourceCapabilities == null && event.type == "blur")) {
                                return;
                            }
                            var index = $scope.Model.Elements.indexOf(tab);
                            $scope.Model.Elements[index].view.Title = tab.Title;
                            var newTabLink = $scope.parseTab(tab.Title);
                            $scope.Model.Elements[index].TabLink = newTabLink;
                            $scope.disableEditor(tab);
                        };
                       

                        $scope.deleteTabConfirm = function (tab, $scope) {
                            $scope.jbConfirmation(tab);
                        };

                        $scope.deleteTab = function (tab) {
                            //$scope.Model.confirmDeleteEnabled = false;
                            if (confirm("Delete the tab and discard all the changes?")) {
                                var index = $scope.Model.Elements.indexOf(tab);
                                //we must keep minimum 1 tab
                                if ($scope.Model.Elements.length > 1) {
                                    $scope.Model.Elements.splice(index, 1);
                                }
                            }
                        };
                        $scope.copyTab = function (tab) {
                            var title = "Copy of " + tab.Title;
                            var index = $scope.Model.Elements.indexOf(tab);
                            var _TabCoverSettings = angular.copy(tab);
                            _TabCoverSettings.active = false;
                            $scope.Model.Elements[index] = {};
                            $scope.Model.Elements[index] = _TabCoverSettings
                            var next = index + 1;
                            var newTabLink = $scope.parseTab(title);

                            $scope.Model.Elements.splice(next,
                                0,
                                {
                                    Type: "JbPTab",
                                    Elements: tab.Elements,
                                    Appearance: tab.Appearance,
                                    TypeTitle: "Tab",
                                    Title: title,
                                    active: true,
                                    TabLink: newTabLink,
                                    Cover: tab.Cover,
                                    HideForUsers: tab.HideForUsers,
                                    FilterTypes: tab.FilterTypes,
                                    FilterToggle: tab.FilterToggle,
                                    FilterSelected: tab.FilterSelected,
                                    FilterData: tab.FilterData,
                                    EnableFilter: tab.EnableFilter,
                                });
                        };
                        $scope.addNewTab = function () {
                            var Title = "new-tab";

                            var newTabLink = $scope.parseTab(Title);

                            $scope.Model.Elements.push({
                                Type: "JbPTab",
                                Elements: [],
                                Appearance: {},
                                TypeTitle: "Tab",
                                Title: Title,
                                active: true,
                                TabLink: newTabLink
                            });
                        };

                        //type == 1(right)
                        //type == 2(left)
                        $scope.moveTab = function (tab, type) {
                            var index = $scope.Model.Elements.indexOf(tab);

                            //if (index || type == 2) {
                            var prev = index - 1;
                            var next = index + 1;

                            if ($scope.Model.Elements[index]) {
                                var tmp = {
                                };
                                tmp = $scope.Model.Elements[index];
                                switch (type) {
                                    case 1:

                                        if (next != $scope.Model.Elements.length) {
                                            $scope.Model.Elements[index] = $scope.Model.Elements[next];
                                            $scope.Model.Elements[next] = tmp;
                                        }

                                        break;
                                    case 2:

                                        if (prev >= 0 && $scope.Model.Elements.length > prev) {

                                            $scope.Model.Elements[index] = $scope.Model.Elements[prev];
                                            $scope.Model.Elements[prev] = tmp;
                                        }
                                        break;
                                }

                                /*window.addEventListener("beforeunload", function (event) {
                                    var confirmationText = "You have unsaved work.";
                                        event.returnValue = confirmationText; // Gecko, Trident, Chrome 34+
                                        return confirmationText;              // Gecko, WebKit, Chrome <34
                                   
                                });*/
                            }
                            //}
                        };
                        //end tab

                        $scope.openTabSettings = function (tab, type) {
                            var index = $scope.Model.Elements.indexOf(tab);
                            var _Model = $scope.Model;
                            var templateUrl = "tabSettingsModal.html";
                            var controller = "tabSettingsCtrl";

                            if (type && type == "bannerText") {
                                var templateUrl = "bannerTextModal.html";
                                var controller = "bannerTextCtrl";
                            } else if (type && type == "tabBaseSettings") {
                                var templateUrl = "tabBaseSettingsModal.html";
                                var controller = "tabBaseSettingsCtrl";
                            }
                            //make a copy
                            var _TabCoverSettings = angular.copy(tab.Cover);

                            //var _Model = $scope.Model.Elements[index];
                            var modalInstance = $modal.open({
                                animation: true,
                                templateUrl: templateUrl,
                                controller: controller,
                                size: "lg",
                                backdrop: 'static',
                                resolve: {
                                    Model: function () {
                                        return $scope.Model;
                                    },
                                    Tab: function () {
                                        return tab;
                                    }
                                }
                            });

                            modalInstance.result.then(function (Model) {
                            },
                                function (reason) {
                                    $scope.Model = _Model;

                                    //come back them
                                    var index = $scope.Model.Elements.indexOf(tab);

                                    if (is.not.undefined($scope.Model.Elements[index])) {
                                        $scope.Model.Elements[index].Cover = _TabCoverSettings;
                                    }

                                    console.log(reason);
                                });
                        }

                        $scope.openPageSettings = function () {
                            var _Model = $scope.Model;

                            //by MGH: make a copy from page settings
                            var PageSettings_Setting = angular.copy($scope.Model.Setting);
                            var PageSettings_Header = angular.copy($scope.Model.Header);
                            var PageSettings_Footer = angular.copy($scope.Model.Footer);

                            var modalInstance = $modal.open({
                                animation: true,
                                templateUrl: 'pageSettingsModal.html',
                                controller: 'pageSettingsCtrl',
                                size: "lg",
                                backdrop: 'static',
                                resolve: {
                                    Model: function () {
                                        return $scope.Model;
                                    }
                                }
                            });

                            modalInstance.result.then(function (Model) {
                            },
                                function (reason) {
                                    $scope.Model = _Model;

                                    //come back page settings
                                    $scope.Model.Setting = PageSettings_Setting;
                                    $scope.Model.Header = PageSettings_Header;
                                    $scope.Model.Footer = PageSettings_Footer;

                                    console.log(reason);
                                });
                        }

                        $scope.openExport = function () {
                            var _Model = angular.copy($scope.Model);
                            //var tabs = $scope.Tabs;

                            var modalInstance = $modal.open({
                                animation: true,
                                templateUrl: 'exportModal.html',
                                controller: 'exportCtrl',
                                size: "lg",
                                backdrop: 'static',
                                resolve: {
                                    Model: function () {
                                        return $scope.Model;
                                    },
                                    Tabs: function () {
                                        return $scope.Tabs;
                                    },
                                }
                            });

                            modalInstance.result.then(function (Model) {

                            },
                                function (reason) {
                                });
                        }

                        $scope.openAvtiveElement = function (item, parent) {
                            var _Model = angular.copy($scope.Model);
                            $scope.Model._Active = item;
                       
                            var modalInstance = $modal.open({
                                animation: true,
                                templateUrl: item.Type + 'Edit.html',
                                controller: 'activeElementCtrl',
                                size: "lg",
                                backdrop: 'static',
                                resolve: {
                                    Model: function () {
                                        return $scope.Model;
                                    },
                                    Seasons: function () {
                                        if (undefined != item) {
                                            if ($scope.Seasons != undefined) {
                                                $scope.Seasons.SelectedItem = {};
                                                $scope.Seasons.SelectedItem = item;
                                            }
                                           
                                        }
                                        return $scope.Seasons;
                                    },
                                    SeasonsDomain: function () {

                                        return $scope.SeasonsDomain;
                                    },
                                    BeforeAfterCarePrograms: function () {
                                        return $scope.BeforeAfterCarePrograms;
                                    },
                                    Programs: function () {

                                        var seasons = [];
                                        var i = 0;

                                        if ($scope.Programs == null || $scope.Programs == '' || $scope.Programs == 'undefined' || $scope.Programs.length < 1)
                                            return [];

                                        for (var i = 0; i < $scope.Programs.length; i++) {

                                            var season = getSeason(seasons, $scope.Programs[i].SeasonId)

                                            if (season == null) {
                                                seasons.push({
                                                    Id: $scope.Programs[i].SeasonId,
                                                    Programs: []
                                                })

                                                season = getSeason(seasons, $scope.Programs[i].SeasonId)
                                            }

                                            season.Programs.push($scope.Programs[i]);
                                        }

                                        $scope.seasonPrograms = seasons;

                                        if (item != undefined) {
                                            
                                            $scope.Programs.SelectedItem = {};
                                            $scope.Programs.SelectedItem = item;
                                        }

                                        $scope.Programs.seasonPrograms = $scope.seasonPrograms;

                                        return $scope.Programs;
                                    },
                                    Parent: function () {
                                        return parent;
                                    },
                                    Tabs: function () {
                                        return $scope.Tabs;
                                    }

                                }
                            });

                            if (item.Type != "JbPSeasonCalendar" &&
                                item.Type != "JbPSeasonGrid" &&
                                item.Type != "JbPTitlePack") {

                                if (item.Type == "JbPRegBtn") {

                                    //MGH: does not need modalInstance.result.then... anymore, will remove in future
                                    modalInstance.result.then(function (Model) {
                                        var seasons = $scope.Seasons;
                                        var seasonId = $scope.Model._Active._season.Key;
                                        item.SeasonId = seasonId;
                                        delete item._IsInAddProcess;

                                        for (var i = 0; i < $scope.SeasonsDomain.length; i++) {
                                            if ($scope.SeasonsDomain[i].Key == seasonId) {
                                                item.SeasonDomain = $scope.SeasonsDomain[i].Value;

                                                break;
                                            }
                                        }

                                        if (is.not.undefined(seasonId) && seasonId) {
                                            for (var i = 0; i < seasons.length; i++) {
                                                if (seasons[i].Key == seasonId) {

                                                    $scope.Model._Active.SeasonId = seasons[i].Key;
                                                    $scope.Model._Active.ElementId =
                                                        Math.round(Math.random() * 1000000);
                                                    //list.splice(
                                                    //    index,
                                                    //    0,
                                                    //    angular.copy($scope.Model._Active));
                                                }
                                            }
                                        }
                                    },
                                        function (reason) {
                                            $scope.Model = _Model;
                                            console.log(reason);
                                        });

                                } else {

                                    modalInstance.result.then(function (Model) {

                                        //By MGH
                                        var programs = $scope.Programs;
                                        var programIds = new Array();
                                        programIds[0] = $scope.Model._Active._programs;
                                        delete $scope.Model._Active._programs;
                                        delete item._IsInAddProcess;
                                        if (!item.IsExternal) {
                                            if (is.not.undefined(programIds) && programIds.length > 0) {
                                                for (var i = 0; i < programs.length; i++) {
                                                    for (var j = 0; j < programIds.length; j++) {
                                                        if (programs[i].ProgramId == programIds[j]) {
                                                            for (var propertyName in programs[i]) {
                                                                if (is.inArray(propertyName,
                                                                    [
                                                                        "$$hashKey",
                                                                        "LinkTitle",
                                                                        "ShowAgeRestrictions",
                                                                        "ShowGradeRestrictions",
                                                                        "ShowLocation",
                                                                        "ShowRegistrationPeriod",
                                                                        "ShowStartEndDates",
                                                                        "ShowDaysTimes",
                                                                        "DaysTimesFormat",
                                                                        "HideDayTimesDate",
                                                                        "ShowCapacityLeft",
                                                                        "ShowTuitionsPrices",
                                                                        "ShowGenderRestrictions",
                                                                        "AdditionalNote"
                                                                    ])) {
                                                                    continue;
                                                                }
                                                                //I got Title from textbox (MGH)
                                                                if (propertyName != 'Title') {
                                                                    $scope.Model._Active[propertyName] =
                                                                        programs[i][propertyName];
                                                                }
                                                            }
                                                            $scope.Model._Active.ElementId =
                                                                Math.round(Math.random() * 1000000);
                                                            /*list.splice(
                                                                index + j,
                                                                0,
                                                                angular.copy($scope.Model._Active));*/
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $scope.Model._Active.ElementId = Math.round(Math.random() * 1000000);
                                            $scope.Model._Active.ProgramId = null;
                                            /*list.splice(
                                                index,
                                                0,
                                                angular.copy($scope.Model._Active));*/
                                        }
                                    },
                                        function (reason) {
                                            $scope.Model = _Model;
                                            console.log(reason);
                                        });
                                }
                            }
                        }

                        $scope.regLinkDropedInSection = function (index, item, list, parent) {

                            if (item._IsInAddProcess) {
                  
                                var _Model = angular.copy($scope.Model);
                                $scope.Model._Active = item;
                                var modalInstance = $modal.open({
                                    animation: true,
                                    templateUrl: item.Type + 'Edit.html',
                                    controller: 'activeElementCtrl',
                                    size: "lg",
                                    backdrop: 'static',
                                    resolve: {
                                        Model: function () {
                                            return $scope.Model;
                                        },
                                        Seasons: function () {
                                            return $scope.Seasons;
                                        },
                                        BeforeAfterCarePrograms: function () {
                                            return $scope.BeforeAfterCarePrograms;
                                        },
                                        SeasonsDomain: function () {
                                            return $scope.SeasonsDomain;
                                        },
                                        Programs: function () {
                                            var seasons = [];
                                            var i = 0;
                                            //group by programs by season @Mo

                                            for (var i = 0; i < $scope.Programs.length; i++) {

                                                var season = getSeason(seasons, $scope.Programs[i].SeasonId)

                                                if (season == null) {
                                                    seasons.push({
                                                        Id: $scope.Programs[i].SeasonId,
                                                        Programs: []
                                                    })

                                                    season = getSeason(seasons, $scope.Programs[i].SeasonId)
                                                }

                                                season.Programs.push($scope.Programs[i]);

                                            }
                                            $scope.seasonPrograms = seasons;

                                            return $scope.seasonPrograms;
                                        },
                                        Parent: function () {
                                            return parent;
                                        }
                                    }
                                });

                                modalInstance.result.then(function (Model) {

                                    var programs = $scope.Programs;
                                    var programIds = $scope.Model._Active._programs;
                                    delete $scope.Model._Active._programs;
                                    delete item._IsInAddProcess;
                                    if (!item.IsExternal) {
                                        if (is.not.undefined(programIds) && programIds.length > 0) {
                                            for (var i = 0; i < programs.length; i++) {
                                                for (var j = 0; j < programIds.length; j++) {
                                                    if (programs[i].ProgramId == programIds[j]) {
                                                        for (var propertyName in programs[i]) {
                                                            if (is.inArray(propertyName,
                                                                [
                                                                    "$$hashKey",
                                                                    "LinkTitle",
                                                                    "ShowAgeRestrictions",
                                                                    "ShowGradeRestrictions",
                                                                    "ShowLocation",
                                                                    "ShowRegistrationPeriod",
                                                                    "ShowStartEndDates",
                                                                    "ShowDaysTimes",
                                                                    "DaysTimesFormat",
                                                                    "HideDayTimesDate",
                                                                    "ShowCapacityLeft",
                                                                    "ShowTuitionsPrices",
                                                                    "ShowGenderRestrictions",
                                                                    "AdditionalNote"
                                                                ])) {
                                                                continue;
                                                            }
                                                            $scope.Model._Active[propertyName] =
                                                                programs[i][propertyName];
                                                        }
                                                        $scope.Model._Active.ElementId =
                                                            Math.round(Math.random() * 1000000);
                                                        list.splice(
                                                            index + j,
                                                            0,
                                                            angular.copy($scope.Model._Active));
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $scope.Model._Active.ElementId = Math.round(Math.random() * 1000000);
                                        $scope.Model._Active.ProgramId = null;
                                        list.splice(
                                            index,
                                            0,
                                            angular.copy($scope.Model._Active));
                                    }

                                },
                                    function (reason) {
                                        $scope.Model = _Model;
                                        console.log(reason);
                                    });
                            } else {
                                return item;
                            }
                        }

                        var getSeason = function (seasons, id) {

                            for (var i = 0; i < seasons.length; i++) {
                                if (seasons[i].Id == id) {
                                    return seasons[i];
                                }
                            }

                            return null;
                        }

                        $scope.regLinkDropedInGrid = $scope.regLinkDropedInSection;

                        $scope.addSectionColumn = function (section) {
                            var newSection = angular.copy($scope.ElementTemplates["JbPSectionColumn"]);
                            newSection.ElementId = Math.round(Math.random() * 1000000);
                            section.Columns.push(newSection);
                        }

                        $scope.addMenuItem = function (menu) {
                            var _Model = angular.copy($scope.Model);

                            var item = angular.copy($scope.ElementTemplates["JbPMenuItem"]);
                            item.ElementId = Math.round(Math.random() * 1000000);
                            menu.push(item);

                            $scope.Model._Active = item;
                            var modalInstance = $modal.open({
                                animation: true,
                                templateUrl: item.Type + 'Edit.html',
                                controller: 'activeElementCtrl',
                                backdrop: 'static',
                                resolve: {
                                    Model: function () {
                                        return $scope.Model;
                                    },
                                    Programs: function () {
                                        return $scope.Programs;
                                    },
                                    BeforeAfterCarePrograms: function () {
                                        return $scope.BeforeAfterCarePrograms;
                                    },
                                    Parent: function () {
                                        return menu;
                                    }
                                }
                            });

                            modalInstance.result.then(function (Model) {
                                delete item._IsInAddProcess;
                            },
                                function (reason) {
                                    $scope.Model = _Model;
                                    console.log(reason);
                                });
                        }

                        $scope.dragStart = function (element) {
                        }

                        $scope.dragEnd = function (element) {
                        }

                        $scope.resetToLastPublish = function () {
                            //by MGH to fix reset to last publish
                            //$scope.load('Publish', 'both');
                            $scope.Model = $scope.PublishedTabs;
                            $scope.Model.Elements[0].active = true;

                            //$scope.disableSave = true;

                            $scope.$watch('status',
                                function () {

                                    switch ($scope.status) {
                                        case "Saved":
                                            console.log("saved");
                                            //$scope.save();
                                            $("#statusBar").removeClass("error");
                                            $("#statusBar").addClass("saved");
                                            break;

                                        /*case "Loading...":
                                            $("#statusBar").removeClass("error");
                                            $("#statusBar").addClass("loaded");
                                            break;*/

                                        case "Loaded":
                                            console.log("loaded");
                                            $scope.save();
                                            $("#statusBar").removeClass("error");
                                            $("#statusBar").addClass("loaded");
                                            break;

                                        case "An error occurred...":
                                            console.log("error");
                                            $("#statusBar").removeClass("saved");
                                            $("#statusBar").addClass("error");
                                            break;

                                        default:
                                            console.log("default");
                                            $("#statusBar").removeClass();

                                    }

                                });
                        }

                        $scope.prepareToAddNew = function (element) {
                            element.ElementId = Math.round(Math.random() * 1000000);
                            element._IsInAddProcess = true;
                        }

                        $scope.$watch("Model.Elements",
                            function (Elements, oldValue) { //by MGH
                                if (Elements) {
                                    $(Elements).each(function (index, value) {
                                        var newValue = value.Elements;

                                        //$scope._lastSavedModel.Elements[index].active = Elements[index].active;

                                        //console.log("[" + Elements[index].active + "]");


                                        for (var i = 0; i < newValue.length; i++) {

                                            if (newValue[i].Type == "JbPSection") {
                                                if (is.undefined(oldValue) ||
                                                    is.undefined(oldValue[i]) ||
                                                    is.undefined(oldValue[i].Columns) ||
                                                    (newValue[i].Columns.length != oldValue[i].Columns.length)) {
                                                    if (newValue[i].Columns.length <= 2) {
                                                        newValue[i].ColumnCount = "Two";
                                                    } else if (newValue[i].Columns.length == 3) {
                                                        newValue[i].ColumnCount = "Three";
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            },
                            true);

                        $scope.$watch('Model._Active.ShowCapacityLeft',
                            function (newVal, oldVal) {

                                if (newVal == true) {
                                    $scope.Model._Active.ShowTuitionsPrices = true;
                                }
                            },
                            true);

                        $scope.$watch('Model._Active.ShowTuitionsPrices',
                            function (newVal, oldVal) {

                                if (newVal == false) {
                                    $scope.Model._Active.ShowCapacityLeft = false;
                                }
                            },
                            true);


                    }
                }

                //Start map
                $scope.map = function (ElementId) {
                    $scope.AvailableAddress = false;
                    if ($scope.Model.Address.Lat && $scope.Model.Address.Lat) {
                        $scope.AvailableAddress = true;

                        setTimeout(function () {
                            $scope.initializeMap(ElementId);
                        },
                            3000);
                    }
                }

                var geocoder = new google.maps.Geocoder();

                $scope.geocodePosition = function (pos) {
                    geocoder.geocode({
                        latLng: pos
                    },
                        function (responses) {
                            if (responses && responses.length > 0) {
                                $scope.updateMarkerAddress(responses[0].formatted_address);
                            } else {
                                $scope.updateMarkerAddress('Address not found!');
                            }
                        });
                }

                $scope.updateMarkerStatus = function (str) {
                    document.getElementById('markerStatus').innerHTML = str;
                }

                $scope.updateMarkerPosition = function (latLng) {

                    $scope.Model.Address.Lat = latLng.lat();
                    $scope.Model.Address.Lng = latLng.lng();

                }

                $scope.updateMarkerAddress = function (str) {
                    $scope.Model.Address.Address = str;
                    $scope.$apply();
                }

                $scope.initializeMap = function (ElementId) {

                    var latLng = new google.maps.LatLng($scope.Model.Address.Lat, $scope.Model.Address.Lng);

                    setTimeout(function () {
                        var map = new google.maps.Map(document.getElementById('e_' + ElementId),
                            {
                                zoom: 8,
                                scrollwheel: false,
                                center: latLng,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            });

                        var marker = new google.maps.Marker({
                            position: latLng,
                            title: 'Point A',
                            map: map,
                            draggable: false
                        });

                        // Update current position info.
                        $scope.updateMarkerPosition(latLng);
                        $scope.geocodePosition(latLng);

                        google.maps.event.addListener(marker,
                            'drag',
                            function () {
                                $scope.updateMarkerStatus('Dragging...');
                                $scope.updateMarkerPosition(marker.getPosition());
                            });

                        google.maps.event.addListener(marker,
                            'dragend',
                            function () {
                                $scope.updateMarkerStatus('Drag ended');
                                $scope.geocodePosition(marker.getPosition());
                            });

                    },
                        3000);

                    // Onload handler to fire off the app.
                    google.maps.event.addDomListener(window, 'load', $scope.initializeMap);
                }


                //Slider tasks start
                $scope.initializeSlider = function (slider) {
                    if (slider.Images.length != 0) {
                        var slideInterval;

                        slider.ImageCount = function () {
                            return this.Images.length;
                        };

                        slider.CurrentImageIndex = 0;

                        slider.Start = function () {
                            slideInterval = setInterval(function () {
                                if (slider.CurrentImageIndex < slider.ImageCount() - 1) {
                                    slider.CurrentImageIndex++;
                                } else {
                                    slider.CurrentImageIndex = 0;
                                }

                                for (var i = 0; i < slider.Images.length; i++) {
                                    if (i == slider.CurrentImageIndex) {
                                        slider.Images[i].Show = true;
                                    } else {
                                        slider.Images[i].Show = false;
                                    }
                                }

                                $scope.$apply();

                            },
                                5000);
                        }

                        slider.Slide = function () {
                            for (var i = 0; i < slider.Images.length; i++) {
                                slider.Images[i].Show = false;
                            }
                            slider.Images[0].Show = true;
                            slideInterval = setInterval(function () {

                                if (slider.CurrentImageIndex < slider.ImageCount() - 1) {
                                    slider.CurrentImageIndex++;
                                } else {
                                    slider.CurrentImageIndex = 0;
                                }

                                for (var i = 0; i < slider.Images.length; i++) {
                                    if (i == slider.CurrentImageIndex) {
                                        slider.Images[i].Show = true;
                                    } else {
                                        slider.Images[i].Show = false;
                                    }
                                }

                                $scope.$apply();

                            },
                                5000);
                        }

                        slider.Next = function () {
                            slider.Images[slider.CurrentImageIndex].Show = false;
                            if (slider.CurrentImageIndex < slider.ImageCount() - 1) {
                                slider.CurrentImageIndex++;
                            } else {
                                slider.CurrentImageIndex = 0;
                            }

                            slider.Images[slider.CurrentImageIndex].Show = true;
                        }

                        slider.Previuos = function () {
                            slider.Images[slider.CurrentImageIndex].Show = false;
                            if (slider.CurrentImageIndex > 0) {
                                slider.CurrentImageIndex--;
                            } else {
                                slider.CurrentImageIndex = slider.ImageCount() - 1;
                            }
                            slider.Images[slider.CurrentImageIndex].Show = true;
                        }

                        slider.Stop = function () {
                            clearInterval(slideInterval);

                        }

                        slider.Slide();
                    }

                }
                //Slider tasks end

                $scope.resetJBPage = function () {

                    var confirmActions =
                        confirm("Are you sure you want to reset the page? All data will be permanently lost.");
                    if (confirmActions == true) {
                        $scope.resetToLastPublish();
                    } else {

                    }

                }
                //Gallery delete start
                $scope.deleteAllImages = function (gitem, glist, gindex) {

                    var confirmActions = confirm("Are you sure you want to delete the images?");
                    if (confirmActions == true) {

                        Array.prototype.push.apply($scope.Model.deletedImages, gitem.Images);

                        glist.splice(gindex, 1)
                    } else {

                    }
                    //document.getElementById("demo").innerHTML = txt;
                }

                //Gallery delete end

                //Gallery modal start
                $scope.showModalGallery = function (index, images) {
                    function fullscreenone() {
                        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
                            (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
                            (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
                            (document.msFullscreenElement && document.msFullscreenElement !== null);

                        var docElm = document.documentElement;
                        if (!isInFullScreen) {
                            if (docElm.requestFullscreen) {
                                docElm.requestFullscreen();
                            } else if (docElm.mozRequestFullScreen) {
                                docElm.mozRequestFullScreen();
                            } else if (docElm.webkitRequestFullScreen) {
                                docElm.webkitRequestFullScreen();
                            } else if (docElm.msRequestFullscreen) {
                                docElm.msRequestFullscreen();
                            }
                        }
                    }

                    function fullscreenCancel() {
                        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
                            (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
                            (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
                            (document.msFullscreenElement && document.msFullscreenElement !== null);

                        var docElm = document.documentElement;

                        if (document.exitFullscreen) {
                            document.exitFullscreen();
                        } else if (document.webkitExitFullscreen) {
                            document.webkitExitFullscreen();
                        } else if (document.mozCancelFullScreen) {
                            document.mozCancelFullScreen();
                        } else if (document.msExitFullscreen) {
                            document.msExitFullscreen();
                        }

                    }

                    fullscreenCancel();

                    function fullscreentoggle() {
                        var isInFullScreen = (document.fullscreenElement && document.fullscreenElement !== null) ||
                            (document.webkitFullscreenElement && document.webkitFullscreenElement !== null) ||
                            (document.mozFullScreenElement && document.mozFullScreenElement !== null) ||
                            (document.msFullscreenElement && document.msFullscreenElement !== null);

                        var docElm = document.documentElement;
                        if (!isInFullScreen) {
                            if (docElm.requestFullscreen) {
                                docElm.requestFullscreen();
                            } else if (docElm.mozRequestFullScreen) {
                                docElm.mozRequestFullScreen();
                            } else if (docElm.webkitRequestFullScreen) {
                                docElm.webkitRequestFullScreen();
                            } else if (docElm.msRequestFullscreen) {
                                docElm.msRequestFullscreen();
                            }
                        } else {
                            if (document.exitFullscreen) {
                                document.exitFullscreen();
                            } else if (document.webkitExitFullscreen) {
                                document.webkitExitFullscreen();
                            } else if (document.mozCancelFullScreen) {
                                document.mozCancelFullScreen();
                            } else if (document.msExitFullscreen) {
                                document.msExitFullscreen();
                            }
                        }
                    }

                    var slideGalleryInterval;
                    $scope.galleryImages = images;

                    images.ImageCount = function () {
                        return this.length;

                    };
                    images.currentImageIndex = index;
                    $scope.galleryModalImageActive = images[images.currentImageIndex].largeImage;
                    $scope.galleryModalTitleActive = images[images.currentImageIndex].Title;
                    $scope.showGalleryModal = true;
                    images.Next = function () {
                        if (images.currentImageIndex < images.ImageCount() - 1) {
                            images.currentImageIndex++;
                        } else {
                            images.currentImageIndex = 0;
                        }
                        $scope.galleryModalImageActive = images[images.currentImageIndex].largeImage;
                        $scope.galleryModalTitleActive = images[images.currentImageIndex].Title;
                    }

                    images.Previuos = function () {
                        if (images.currentImageIndex > 0) {
                            images.currentImageIndex--;
                        } else {
                            images.currentImageIndex = images.ImageCount() - 1;
                        }
                        $scope.galleryModalImageActive = images[images.currentImageIndex].largeImage;
                        $scope.galleryModalTitleActive = images[images.currentImageIndex].Title;
                    }
                    images.play = function () {
                        $scope.playGet = false;
                        $scope.galleryPlay = !($scope.galleryPlay);
                        if ($scope.galleryPlay) {
                            fullscreenone();
                            slideGalleryInterval = setInterval(function () {
                                if (images.currentImageIndex < images.ImageCount() - 1) {
                                    images.currentImageIndex++;
                                } else {
                                    images.currentImageIndex = 0;
                                }

                                $scope.galleryModalImageActive = images[images.currentImageIndex].largeImage;
                                $scope.galleryModalTitleActive = images[images.currentImageIndex].Title;


                                $scope.$apply();

                            },
                                3000);
                        } else {

                            clearInterval(slideGalleryInterval);
                        }

                    }
                    images.pause = function () {
                        fullscreenCancel();
                        $scope.galleryPlay = false;
                        clearInterval(slideGalleryInterval);
                    }
                    images.simplePause = function (gPlay) {
                        $scope.playGet = gPlay;
                        $scope.galleryPlay = false;
                        clearInterval(slideGalleryInterval);
                    }
                }
                //Gallery modal end
                //collect tabs title and ids
                $scope.tabsData = function (data) {
                    var Tabs = [];

                    $scope.PublishedTabs = {
                    };
                    $scope.PublishedTabs = data;

                    var tabId = 0;
                    angular.forEach($scope.PublishedTabs.Elements,
                        function (value, key) {
                            if (value.ElementId) {
                                tabId = parseInt(value.ElementId.replace("JbPTab", ""));
                                Tabs[key] = {
                                    Id: tabId,
                                    Title: value.Title
                                }
                            }
                        });
                    return Tabs;
                }

                $scope.iframeOptions = {
                    showheader: false,
                    showcover: false,
                    showfooter: false,
                    background: "#EAEDF1",
                    availableTabs: ""
                };

                $scope.grades = function () {
                    $http.get('/Season/ProgramGrades')
                        .then(function (response) {

                            $scope.ProgramGrades = response.data;

                            setTimeout(function () {
                                $scope.$apply(function () {
                                    //$scope.status = "";
                                });
                            },
                                3000);
                        });
                };

                $scope.grades();

                //$scope.toTrustedHTML = function (html) {
                //    return $sce.trustAsHtml(html);
                //}

                $scope.load = function (saveType, loadType) {
                    var saveType = saveType ? saveType : 'Draft';

                    //preview mode for club admin
                    var preview = false;
                    var url = window.location;
                    if (url.pathname.toLowerCase() == "/view") {
                        preview = true;
                    }

                    var mode = "view";
              
                    if (url.pathname.toLowerCase() == "/edit") {
                        mode = "edit"
                    }

                    $("#statusBar").addClass("loading");
                    $scope.status = "Loading...";

                    var url = baseUrl + '/Manage/' + isIframe() + '/' + saveType + '/' + preview + '/' + mode;

                    $http.get(url)
                        .then(function (response) {

                            if (saveType != 'Publish' &&
                                (loadType == 'both' ||
                                    loadType == 'page' ||
                                    angular.isUndefined(loadType) ||
                                    loadType === null)) {

                                if (isIframe()) {
                                    var allTabElements = response.data.Elements;
                                    $scope.Model = response.data;
                                    //I remove all tabs in Model and fill it with available tabs again
                                    $scope.Model.Elements = [];
                                } else {
                                    $scope.Model = response.data;
                                    //set first tab active

                                    if ($scope.if.isDesignMode()) {
                                        //$scope._lastSavedModel.Elements[0].active = true;
                                    }
                                    $scope.Model.Elements[0].active = true;

                                }
                                $scope.status = "Loaded";

                                $("#statusBar").removeClass();
                                $("#statusBar").addClass("loaded");

                                setTimeout(function () {
                                    $scope.$apply(function () {
                                        $("#statusBar").removeClass("loaded");
                                        $scope.status = "";

                                        var elementKey = 0;
                                        angular.forEach($scope.Model.Elements,
                                            function (value, tabKey) {

                                                if (value.Elements[0] != undefined &&
                                                    value.Elements[0].Type == 'JbPMap') {
                                                    $scope.map(value.Elements[0].ElementId);
                                                }
                                                var pIds = [];

                                                angular.forEach(value.Elements,
                                                    function (element, elementKey) {

                                                        if (!angular.isUndefined(element) &&
                                                            is.not.undefined(element.Type) &&
                                                            element.Type == 'JbPGrid') {

                                                            if (element.RegLinks.length > 0) {
                                                                angular.forEach(element.RegLinks,
                                                                    function (regValue, regKey) {

                                                                        angular.forEach($scope.Programs,
                                                                            function (programValue, programKey) {
                                                                                if (programValue.ProgramId ==
                                                                                    regValue.ProgramId) {
                                                                                    $scope.Model.Elements[tabKey
                                                                                    ]
                                                                                        .Elements[elementKey]
                                                                                        .RegLinks[regKey]
                                                                                        .Schedules =
                                                                                        programValue.Schedules;
                                                                                    $scope.Model.Elements[tabKey
                                                                                    ]
                                                                                        .Elements[elementKey]
                                                                                        .RegLinks[regKey]
                                                                                        .RegistrationPeriod =
                                                                                        programValue
                                                                                            .RegistrationPeriod;
                                                                                    $scope.Model.Elements[tabKey
                                                                                    ]
                                                                                        .Elements[elementKey]
                                                                                        .RegLinks[regKey]
                                                                                        .Tuitions =
                                                                                        programValue.Tuitions;
                                                                                }
                                                                            })
                                                                    })
                                                            }

                                                        }
                                                    })
                                            })

                                    });
                                },
                                    3000);

                                //set default values for color settings
                                if (!$scope.Model.Setting.TabBackgroundColor) {
                                    $scope.Model.Setting.TabBackgroundColor = "#fff";
                                }

                                if (!$scope.Model.Setting.TabTextColor) {
                                    $scope.Model.Setting.TabTextColor = "#01a3f1";
                                }

                                if (!$scope.Model.Setting.TabHoverTextColor) {
                                    $scope.Model.Setting.TabHoverTextColor = "#333";
                                }

                                if (!$scope.Model.Setting.TabBorderColor) {
                                    $scope.Model.Setting.TabBorderColor = "#01a3f1";
                                }

                                if (!$scope.Model.Setting.TabSelectedBackgroundColor) {
                                    $scope.Model.Setting.TabSelectedBackgroundColor = "#01a3f1";
                                }

                                if (!$scope.Model.Setting.TabSelectedTextColor) {
                                    $scope.Model.Setting.TabSelectedTextColor = "#fff";
                                }

                                if (!$scope.Model.Setting.TabSelectedBorderColor) {
                                    $scope.Model.Setting.TabSelectedBorderColor = "#01a3f1";
                                }

                                if (!$scope.Model.Setting.TabSelectedHoverTextColor) {
                                    $scope.Model.Setting.TabSelectedHoverTextColor = "#eee";
                                }

                                if (!$scope.Model.Setting.TabFontSize) {
                                    $scope.Model.Setting.TabFontSize = "14";
                                }
                                if (!$scope.Model.Setting.TabFontWeight) {
                                    $scope.Model.Setting.TabFontWeight = "400";
                                }

                                if (!$scope.Model.Footer.BackgroundColor) {
                                    $scope.Model.Footer.BackgroundColor = "#01a3f1";
                                }

                                if ($scope.if.isIframeMode()) {

                                    window.addEventListener('message',
                                        function (e) {
                                            $scope.$apply(function () {

                                                $scope.iframeOptions = e.data;
                                                //prepare available tabs to display on view
                                                var empty_string = '[]';
                                                var empty_string2 = '';
                                                if ($scope.iframeOptions.availableTabs &&
                                                    (empty_string.localeCompare($scope.iframeOptions.availableTabs) !=
                                                        0 &&
                                                        empty_string2.localeCompare($scope.iframeOptions
                                                            .availableTabs) !=
                                                        0)) {
                                                    var tabIds = JSON.parse($scope.iframeOptions.availableTabs);
                                                    var availableTabIds = [];
                                                    //preparing available tabs
                                                    angular.forEach(tabIds,
                                                        function (v, k) {
                                                            availableTabIds.push('JbPTab' + v);
                                                        });

                                                    var availableTabElements = [];
                                                    var i = 0;

                                                    angular.forEach(allTabElements,
                                                        function (value, key) {
                                                            if (availableTabIds.indexOf(value.ElementId) > -1) {
                                                                //we keep it
                                                                availableTabElements[i] = value;
                                                                i++;
                                                            }
                                                        });
                                                    $scope.Model.Elements = availableTabElements;
                                                } else {
                                                    $scope.Model.Elements = allTabElements;
                                                }

                                                if ($scope.Model.Elements[0]) {
                                                    if ($scope.if.isDesignMode()) {
                                                        $scope._lastSavedModel.Elements[0].active = true;
                                                    }
                                                    $scope.Model.Elements[0].active = true;
                                                }
                                            });
                                            setTimeout(function () {
                                                window.top.postMessage({
                                                    'height': document.getElementById('editorBody')
                                                        .scrollHeight
                                                },
                                                    '*');
                                            },
                                                0);
                                        });
                                    window.top.postMessage({ 'ready': true }, '*');
                                }

                                var newModelStringed = angular.copy($scope.Model);
                                $scope._lastSavedModel = newModelStringed;

                                if ($scope.if.isDesignMode() && !isIframe()) {
                                    $scope._lastSavedModel.Elements[0].active = true;
                                }

                                $scope.registerDesignModeFunctions();

                            }

                            var hash = window.location.hash;

                            if (hash) {

                                var tabId = hash.split('#/').join('');

                                for (var i = 0; i < $scope.Model.Elements.length; i++) {
                                    if ($scope.Model.Elements[i].TabLink.toLowerCase() == tabId.toLowerCase()) {
                                        //$scope._lastSavedModel.Elements[i].active = true;
                                        $scope.Model.Elements[i].active = true;

                                        if ($scope.if.isDesignMode()) {
                                            $scope._lastSavedModel.Elements[i].active = true;
                                        }

                                        continue;
                                    } // else {
                                    //    $scope._lastSavedModel.Elements[i].active = false;
                                    //    $scope.Model.Elements[i].active = false;
                                    //}
                                }
                            }

                            //if (loadType == 'tmp' || loadType == 'both') {
                            /*if (saveType == 'Publish') {
                                $scope.Tabs = new Array();
                                $scope.Tabs = $scope.tabsData(response.data);
            
                                return;
                            }*/

                            if (saveType != 'Publish') {
                                $scope.load('Publish'); //I separatelly load published tabs

                            } else {
                                if (saveType == 'Publish') {
                                    $scope.Tabs = new Array();
                                    $scope.Tabs = $scope.tabsData(response.data);

                                    //return;
                                }
                            }


                        },
                        function (response) {
                            $scope.status = "An error occurred...";
                            $("#statusBar").addClass("error");
                            console.log("ERROR:");
                            console.log(response);
                        });

                }

                $scope.load('', 'both');

                //load published tabs for first one in design mode
                /*$scope.$watch('status', function () {
                    if ($scope.status == 'Loaded') {
                        if ($scope.if.isDesignMode()) {
                            $scope.load('Publish');//I separatelly load published tabs
                        }
                    }
            
                });*/
                $scope.RedirectTo = function (SeasonDomain) {

                    if (!$scope.if.isDesignMode()) {

                        if (arguments[1]) {
                            var filterTabID = arguments[1].ElementId
                            window.open(
                                '/' + SeasonDomain + "/Register?tab=" + filterTabID,
                                '_blank'
                            );
                        } else {
                            window.open(
                                '/' + SeasonDomain + "/Register",
                                '_blank'
                            );
                        }


                    }

                }

                $scope.parseTab = function (text) {
                    //var pattern = "^[a-zA-Z0-9\-\_]*$";
                    //console.log(text);
                    var newText = text.replace(/[\W_]+/g, "-"); //accept all numbers, alphabetic and underscore chars
                    var isDuplicated = false;
                    var value = "";
                    var i = $scope.PublishedTabs.Elements.length;

                    var index = 0;

                    //disabled by MGH: is does not need check duplication for published tabs
                    //for published tabs
                    /*for (i; i > 0; i--) {
                        index = i - 1;
                        value = $scope.PublishedTabs.Elements[index];
            
                        if (newText.toLowerCase() == value.TabLink.toLowerCase()) {
                            console.log("duplicated");
                            var isDuplicated = true;
            
                            newText += Math.ceil(Math.random() * 9);
            
                            //loop again
                            i = $scope.PublishedTabs.Elements.length;
                            //break;
                        }
                    };*/

                    //for draft tabs
                    i = $scope.Model.Elements.length;

                    for (i; i > 0; i--) {
                        index = i - 1;
                        value = $scope.Model.Elements[index];

                        if (newText.toLowerCase() == value.TabLink) {
                            var isDuplicated = true;

                            newText += Math.ceil(Math.random() * 9);

                            //loop again
                            i = $scope.Model.Elements.length;
                            //break;
                        }
                    };

                    return newText;
                }

                $scope.updateHtmlEditor = function (color) {
                    console.log(color)
                    $(".k-editable-area").find('body').css('background-color', color);
                }


                $scope.refreshTab = function (tabTitle, tabIndex) {

                    if (tabIndex >= 0)
                        if ($scope.Model.Elements[tabIndex] != undefined) {
                            window.location.hash = '#/' + $scope.Model.Elements[tabIndex].TabLink.toLowerCase();
                        } else
                            window.location.hash = '';

                    setTimeout(function () {
                        $(window).trigger('resize');

                        if ($scope.if.isIframeMode()) {
                            window.top.postMessage({ 'height': document.getElementById('editorBody').scrollHeight },
                                '*');
                        }
                    },
                        0);

                }

                $scope.programFilter = function (elementId, type) {
                    if (type == "grid") {
                        $("#Grid_" + elementId).data("kendoGrid").dataSource.read();
                    } else
                        $("#Sched_" + elementId).data("kendoScheduler").dataSource.read();
                };

                $scope.schedulerOptions = {
                };

                function scheduler_change(e) {
                    if (e.events.length) {
                        var data = e.events[0];
                        //console.log(e.events[0]);
                        window.location.href = data.SeasonDomain + "/" + data.Domain;
                    }
                }

                $scope.calendarDataSource = function (tabElement) {
                    var elementId = tabElement.ElementId;

                    var screenWidth = $(window).outerWidth();

                    var viewMode = 'week';

                    if (screenWidth < 768) {
                        viewMode = 'day';
                    }

                    $scope.schedulerOptions[elementId] = {
                        date: new Date(),

                        //startTime: new Date("2016/01/01 07:00 AM"),
                        filterable: true,
                        editable: false,
                        showWorkHours: true,
                        //timezone: "Etc/UTC",
                        height: 600,
                        views: [
                            "day",
                            "workWeek",
                            {
                                type: viewMode,
                                selected: true,
                                eventTemplate: $("#SchedTemplate").html(),
                                allDayEventTemplate: $("#SchedTemplate").html(),
                                //dayTemplate: kendo.template($("#SchedTemplate").html())
                            },
                            "month",
                        ],
                        template: $("#SchedTemplate").html(),
                        //dateHeaderTemplate: kendo.template("<strong>#=kendo.toString(date, 'd')#</strong>"),
                        dataBound: function (e) {
                            //this.expandRow(this.tbody.find("tr.k-master-row").first());

                        },
                        dataSource: {
                            batch: true,
                            serverFiltering: true,
                            transport: {
                                read: {
                                    url: "/Season/SeasonProgramsCalendar",
                                    //data: parameterMap,
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8", // tells the web method to serialize JSON
                                    //accepts: "application/json",
                                    dataType: "json"
                                },
                                parameterMap: function (options, operation) {
                                    if (operation !== "read" && options.models) {
                                        return {
                                            models: kendo.stringify(options.models)
                                        };
                                    }

                                    if (operation == "read") {
                                        var SchoolGradeType = 0;
                                        if (tabElement.selectedGrade != null && tabElement.selectedGrade != "undefined" && tabElement.selectedGrade.length > 0) {
                                            var SchoolGradeType = tabElement.selectedGrade[0];
                                        }
                                        var data = $.extend(
                                            {
                                                IncludeClasses: tabElement.IncludeClasses,
                                                IncludeCamp: tabElement.IncludeCamp,
                                                IncludeCalendar: tabElement.IncludeCalendar,
                                                IncludeChessTournament: tabElement.IncludeChessTournament,
                                                IncludeSeminarTour: tabElement.IncludeSeminarTour,
                                                ShowGrade: tabElement.ShowGrade,
                                                ShowGradeInformation: tabElement.ShowGradeInformation,
                                                SeasonId: tabElement.SeasonId,
                                                SchoolGradeType: SchoolGradeType
                                            }, data);

                                        return JSON.stringify(data);
                                    }

                                }
                            },
                            schema: {
                                model: {
                                    id: "taskId",
                                    fields: {
                                        taskId: {
                                            from: "TaskID",
                                            type: "number"
                                        },
                                        title: {
                                            from: "Title",
                                            defaultValue: "No title",
                                            validation: {
                                                required: true
                                            }
                                        },
                                        start: {
                                            type: "date",
                                            from: "Start"
                                        },
                                        end: {
                                            type: "date",
                                            from: "End"
                                        },
                                        //startTimezone: { from: "StartTimezone" },
                                        //endTimezone: { from: "EndTimezone" },
                                        description: {
                                            from: "Description"
                                        },
                                        recurrenceId: {
                                            from: "RecurrenceID"
                                        },
                                        recurrenceRule: {
                                            from: "RecurrenceRule"
                                        },
                                        recurrenceException: {
                                            from: "RecurrenceException"
                                        },
                                        ownerId: {
                                            from: "OwnerID",
                                            defaultValue: 1
                                        },
                                        isAllDay: {
                                            type: "boolean",
                                            from: "IsAllDay"
                                        },
                                        programType: {
                                            from: "ProgramType"
                                        },
                                        scheduleMinGrade: {
                                            type: "integer",
                                            from: "ScheduleMinGrade"
                                        },
                                        scheduleMaxGrade: {
                                            type: "integer",
                                            from: "ScheduleMaxGrade"
                                        },
                                        programGradeList: {
                                            from: "ProgramGradeList"
                                        }
                                    }
                                }
                            },
                        },
                        resources: {
                            field: "ProgramType",
                            //name: "Program",
                            dataColorField: "color",
                            dataSource: [
                                {
                                    value: "Class",
                                    text: "C1",
                                    color: "red"
                                },
                                { value: "Camp", text: "C2", color: "green" },
                            ],
                            multiple: true
                        }
                    };
                };


                $scope.seasonGridCalendarDataSource = function (gridElement) {             
                    var elementId = gridElement.ElementId;

                    var screenWidth = $(window).outerWidth();

                    var viewMode = 'week';

                    if (screenWidth < 768) {
                        viewMode = 'day';
                    }

                    $scope.schedulerOptions[elementId] = {
                        date: gridElement.ScheduleStartDate ? new Date(gridElement.ScheduleStartDate) : new Date(),                     
                        filterable: true,
                        editable: false,
                        showWorkHours: true,
                        height: 600,
                        views: function (gridElement) {
                            return gridElement.GridLayoutType === 'Agenda' ? [
                                {
                                    type: "agenda",
                                    eventTemplate: $("#gridAgendaProgramTemplate").html(),
                                    selected: true
                                }
                            ] : [
                                  
                                {
                                    type: "day",
                                    eventTemplate: $("#gridCalendarProgramTemplate").html()
                                },
                      
                                {
                                    type: "workWeek",
                                    eventTemplate: $("#gridCalendarProgramTemplate").html()
                                },
                                    {
                                        type: viewMode,
                                        selected: true,
                                        eventTemplate: $("#gridCalendarProgramTemplate").html(),
                                        allDayEventTemplate: $("#gridCalendarProgramTemplate").html(),
                                        //dayTemplate: kendo.template($("#SchedTemplate").html())
                                    },
                         
                            {
                                type: "month",
                                    eventTemplate: $("#gridCalendarProgramTemplate").html()
                            }
                                ];
                        }(gridElement),
                        messages: {
                            time: "Time of the day",
                            event: "Program",
                            date: "Date"
                        },
                        template: $("#SchedTemplate").html(),
                        dataBound: function (e) {
                        },
                        dataSource: {
                            batch: true,
                            serverFiltering: true,
                            transport: {
                                read: {
                                    url: "/Season/SeasonProgramsGrid",
                                    //data: parameterMap,
                                    type: "POST",
                                    contentType:
                                        "application/json; charset=utf-8", // tells the web method to serialize JSON
                                    //accepts: "application/json",
                                    dataType: "json"
                                },
                                parameterMap: function (options, operation) {
                                    if (operation !== "read" && options.models) {
                                  
                                        console.log(options.models);
                                        return {
                                            models: kendo.stringify(options.models)
                                        };
                                    }
                            
                                    if (operation == "read") {
                                        var SchoolGradeType = 0;
                                        if (gridElement.selectedGrade != null &&
                                            gridElement.selectedGrade != "undefined" &&
                                            gridElement.selectedGrade.length > 0) {
                                            var SchoolGradeType = gridElement.selectedGrade[0];
                                        }
                                        var data = $.extend(
                                            {
                                                IncludeClasses: gridElement.IncludeClasses,
                                                IncludeCamp: gridElement.IncludeCamp,
                                                IncludeBeforeAfter: gridElement.IncludeBeforeAfter,
                                                IncludeSeminarTour: gridElement.IncludeSeminarTour,
                                                HideFuture: gridElement.HideFuture,
                                                HideExpired: gridElement.HideExpired,
                                                HideFrozen: gridElement.HideFrozen,
                                                SeasonId: gridElement.SeasonId,
                                                CampId: gridElement.campId,
                                                enabledPopup: gridElement.EnabledPopup,
                                                GridLayoutType: gridElement.GridLayoutType,
                                                ProgramRegBtnLinkTo: gridElement.ProgramRegBtnLinkTo,
                                                ProgramRegBtnBackgroundColor: gridElement.ProgramRegBtnBackgroundColor,
                                                ProgramRegBtnLabelColor: gridElement.ProgramRegBtnLabelColor,
                                                ProgramRegBtnTitle: gridElement.ProgramRegBtnTitle,
                                                CalenderProgramTextColor: gridElement.CalenderProgramTextColor,
                                                CalenderProgramBackgroundColor: gridElement.CalenderProgramBackgroundColor,
                                                ShowProgramColumn: gridElement.ShowProgramColumn,
                                                ShowPriceColumn: gridElement.ShowPriceColumn,
                                                ShowCapacityColumn: gridElement.ShowCapacityColumn,
                                                ShowCapacityLeftColumn: gridElement.ShowCapacityLeftColumn,
                                                ShowStatusColumn: gridElement.ShowStatusColumn,
                                                ShowGradeAgeColumn: gridElement.ShowGradeAgeColumn,
                                                ShowRestrictionsColumn: gridElement.ShowRestrictionsColumn,
                                                ShowRoomColumn: gridElement.ShowRoomColumn,
                                                ShowLocationColumn: gridElement.ShowLocationColumn,
                                                ShowDaysTimesColumn: gridElement.ShowDaysTimesColumn,
                                                ShowDatesColumn: gridElement.ShowDatesColumn,
                                                ShowRegistrationDatesColumn: gridElement.ShowRegistrationDatesColumn,
                                                ShowProviderColumn: gridElement.ShowProviderColumn,
                                                ShowInstructorsColumn: gridElement.ShowInstructorsColumn,
                                                ShowRegistrationColumn: gridElement.ShowRegistrationColumn
                                            },
                                            data);
                                 
                              
                                        return JSON.stringify(data);
                                    }

                                }
                            },
                            schema: {
                                model: {
                                    id: "CalendarTaskId",
                                    fields: {
                                        taskId: {
                                            from: "CalendarTaskId",
                                            type: "number"
                                        },
                                        title: {
                                            from: "Title",
                                            defaultValue: "No title",
                                            validation: {
                                                required: true
                                            }
                                        },
                                        start: {
                                            type: "date",
                                            from: "Start"
                                        },
                                        end: {
                                            type: "date",
                                            from: "End"
                                        },
                                        description: {
                                            from: "Description"
                                        },
                                        recurrenceId: {
                                            from: "RecurrenceID"
                                        },
                                        recurrenceRule: {
                                            from: "RecurrenceRule"
                                        },
                                        recurrenceException: {
                                            from: "RecurrenceException"
                                        },
                                        ownerId: {
                                            from: "OwnerID",
                                            defaultValue: 1
                                        },
                                        isAllDay: {
                                            type: "boolean",
                                            from: "CalendarIsAllDay"
                                        },
                                        programType: {
                                            from: "ProgramType"
                                        },
                                        scheduleMinGrade: {
                                            type: "integer",
                                            from: "CalendarScheduleMinGrade"
                                        },
                                        scheduleMaxGrade: {
                                            type: "integer",
                                            from: "CalendarScheduleMaxGrade"
                                        },
                                        programGradeList: {
                                            from: "ProgramGradeList"
                                        }
                                    }
                                }
                            },
                        },
                        resources: {
                            field: "ProgramType",
                            //name: "Program",
                            dataColorField: "color",
                            dataSource: [
                                {
                                    value: "Class",
                                    text: "C1",
                                    color: "red"
                                },
                                { value: "Camp", text: "C2", color: "green" },
                            ],
                            multiple: true
                        }
                    };
                };
                $scope.gridOptions = {
                };

                $scope.calculateDayOfWeek = function (intDayofWeek, dayofWeek) {
                    return dayofWeek;
                }
                $scope.viewClass = function (url) {
                    if (arguments.length == 1) {
                        var tabName = window.location.href.split('#')[1];
                        window.open("Https://" + window.location.host + "/" + url, '_blank');
                    } else if (arguments.length == 4) {
                        var statePopUp = (arguments[3] == 'true');
                        //this is for popup footer register btn
                        //var stateRegistrationPopUp = (arguments[4] == 'true');
                        //var registerURL = "Https://" + window.location.host + "/" + arguments[5];
                        if (arguments[1] == 'Class' && statePopUp) {

                            $scope.Model.SeasonGridPopupData = {};
                            var sliderInterval, slideIndex = 0;

                            function showSlide(showIndex) {

                                $scope.$apply(function () {
                                    for (var slide in $scope.Model.SeasonGridPopupData.ImageUrls) {
                                        $scope.Model.SeasonGridPopupData.ImageUrls[slide].slideActive = false;
                                    }
                                    $scope.Model.SeasonGridPopupData.ImageUrls[showIndex].slideActive = true;
                                });

                            }

                            $http.post('/JbPage/GetProgramInformation', { 'programId': arguments[2] }).success(
                                function (data, status, headers, config) {

                                    $scope.Model.SeasonGridPopupData = data.DataSource;
                                    console.log($scope.Model.SeasonGridPopupData);
                                    //this is for popup footer register btn
                                    //add register url
                                    //$scope.Model.SeasonGridPopupData.RegisterUrl = registerURL;
                                    //active footer
                                    //if (stateRegistrationPopUp) {
                                    //    $scope.Model.SeasonGridPopupData.EnabledRegistrationPopup = true;
                                    //}
                                    //else {
                                    //    $scope.Model.SeasonGridPopupData.EnabledRegistrationPopup = false;
                                    //}
                                    //Logic codes
                                    //Tuitions start

                                    $.each($scope.Model.SeasonGridPopupData.Schedule.Prices,
                                        function (index, charge) {
                                            //Spots
                                            charge.spotNote = "";
                                            if (charge.CapacityLeft > 0 &&
                                                $scope.Model.SeasonGridPopupData.Schedule.RecurrenceTime
                                                    .ShowCapacityLeft &&
                                                charge.CapacityLeft <=
                                                $scope.Model.SeasonGridPopupData.Schedule.RecurrenceTime
                                                    .ShowCapacityLeftNumber) {
                                                charge.spotNote = charge.CapacityLeft + " spots left";
                                            } else if (
                                                $scope.Model.SeasonGridPopupData.Schedule.TotalCapacityLeft > 0 &&
                                                $scope.Model.SeasonGridPopupData.Schedule.RecurrenceTime
                                                    .ShowCapacityLeft &&
                                                $scope.Model.SeasonGridPopupData.Schedule.TotalCapacityLeft <=
                                                $scope.Model.SeasonGridPopupData.Schedule.RecurrenceTime
                                                    .ShowCapacityLeftNumber) {
                                                charge.spotNote =
                                                    $scope.Model.SeasonGridPopupData.Schedule.TotalCapacityLeft +
                                                    " spots left";
                                            }
                                            if (charge.CapacityLeft == 0) {
                                                charge.spotNote = "Full";
                                            }


                                        });


                                    //Tuitions end
                                    //dayOfweek
                                    $scope.Model.SeasonGridPopupData.Schedule.RecurrenceTime.FullInfoDays = {
                                        Sunday: false,
                                        Monday: false,
                                        Tuesday: false,
                                        Wednesday: false,
                                        Thursday: false,
                                        Friday: false,
                                        Saturday: false
                                    }


                                    $.each($scope.Model.SeasonGridPopupData.ProgramDays,
                                        function (index, day) {
                                            $scope.Model.SeasonGridPopupData.Schedule.RecurrenceTime.FullInfoDays[day
                                                .DayOfWeek] = day
                                        })

                                    //Restrictions 
                                    if (!(
                                        $scope.Model.SeasonGridPopupData.Schedule.Restrict.RestrictionType == 'None' &&
                                        (!$scope.Model.SeasonGridPopupData.Schedule.Restrict.Gender ||
                                            $scope.Model.SeasonGridPopupData.Schedule.Restrict.Gender &&
                                            ($scope.Model.SeasonGridPopupData.Schedule.Restrict.Gender ==
                                                'NoRestriction')))) {

                                        $scope.Model.SeasonGridPopupData.Schedule.Restrict.Active = true;
                                        $scope.Model.SeasonGridPopupData.RestrictionMessages.Active = "";
                                        $scope.Model.SeasonGridPopupData.RestrictionMessages.ActiveGender = "";

                                        switch ($scope.Model.SeasonGridPopupData.Schedule.Restrict.RestrictionType) {
                                            case 'Age':
                                                $scope.Model.SeasonGridPopupData.RestrictionMessages.Active =
                                                    $scope.Model.SeasonGridPopupData.RestrictionMessages.AgeMessage;
                                                break;
                                            case 'Grade':
                                                $scope.Model.SeasonGridPopupData.RestrictionMessages.Active =
                                                    $scope.Model.SeasonGridPopupData.RestrictionMessages.GradeMessage;
                                                break;
                                        }
                                        if ($scope.Model.SeasonGridPopupData.Schedule.Restrict.Gender) {
                                            if ($scope.Model.SeasonGridPopupData.Schedule.Restrict.Gender !=
                                                'NoRestriction') {
                                                $scope.Model.SeasonGridPopupData.RestrictionMessages.ActiveGender =
                                                    $scope.Model.SeasonGridPopupData.RestrictionMessages.GenderMessage;
                                            }

                                        } else {
                                            $scope.Model.SeasonGridPopupData.Schedule.Restrict.Active = false;
                                        }
                                    }
                                    //Categories
                                    var categoryNames = [];
                                    $.each($scope.Model.SeasonGridPopupData.Categories,
                                        function (index, category) {
                                            categoryNames.push(category.Name);
                                            $scope.Model.SeasonGridPopupData.Categories.Names =
                                                categoryNames.join(', ');
                                        });


                                    //slider logical codes start
                                    if ($scope.Model.SeasonGridPopupData.ImageUrls.length != 0) {
                                        sliderInterval = setInterval(function () {
                                            if (slideIndex <= $scope.Model.SeasonGridPopupData.ImageUrls.length - 1
                                            ) {
                                                showSlide(slideIndex);
                                                slideIndex += 1;
                                            } else {
                                                showSlide(0);
                                                slideIndex = 0;
                                                slideIndex += 1;
                                            }

                                        },
                                            4000)
                                    }

                                    $scope.stopSlider = function () {
                                        clearInterval(sliderInterval);
                                    }
                                    $scope.startSlider = function () {
                                        if ($scope.Model.SeasonGridPopupData.ImageUrls.length != 0) {
                                            sliderInterval = setInterval(function () {
                                                if (slideIndex <=
                                                    $scope.Model.SeasonGridPopupData.ImageUrls.length - 1
                                                ) {
                                                    showSlide(slideIndex);
                                                    slideIndex += 1;
                                                } else {
                                                    showSlide(0);
                                                    slideIndex = 0;
                                                    slideIndex += 1;
                                                }

                                            },
                                                4000)
                                        }
                                    }
                                    //slider logical codes end

                                });


                            //Show popup start

                            //get base elements
                            var element = angular.element('[data-program-info]'),
                                tabContent = element.find('.tab-modal__contents'),
                                tabItems = element.find('.tab-modal__items'),
                                slider = element.find('.popup-slider'),
                                slides = slider.find(">div"),
                                activeElement = element.find('.tab-modal__item-active'),
                                nextSlide = slider.find('i.jbi-angle-right,.next-slide-area'),
                                prevSlide = slider.find('i.jbi-angle-left,.prev-slide-area'),
                                closeBtn = element.find('.modal-fluid__btn-close');

                            //slider start
                            //Next slide
                            nextSlide.on('click',
                                function () {
                                    slideIndex++;
                                    if (slideIndex <= $scope.Model.SeasonGridPopupData.ImageUrls.length - 1) {
                                        showSlide(slideIndex);
                                    } else {
                                        showSlide(0);
                                        slideIndex = 0;
                                    }

                                });
                            //Previous slide
                            prevSlide.on('click',
                                function () {
                                    slideIndex--;
                                    if (slideIndex >= 0) {

                                        showSlide(slideIndex);
                                    } else {
                                        showSlide($scope.Model.SeasonGridPopupData.ImageUrls.length - 1);
                                        slideIndex = $scope.Model.SeasonGridPopupData.ImageUrls.length - 1;
                                    }
                                });
                            //Slider open
                            $scope.sliderOpen = function () {
                                slider.addClass('popup-slider-open');
                                slider.find('.small-image-slide').hide();
                                slider.find('.large-image-slide').show();
                                slider.find('.close-slider').show();
                            }
                            //Slider close
                            $scope.sliderClose = function () {
                                slider.find('.large-image-slide').hide();
                                slider.removeClass('popup-slider-open');
                                slider.find('.small-image-slide').show();
                                slider.find('.large-image-slide').hide();
                                slider.find('.close-slider').hide();

                            }
                            //Slider end
                            element.stop().slideDown(500);
                            tabContent.find('[data-modal-tab]').css('display', 'none');
                            tabContent.find('[data-modal-tab=' + activeElement.attr('data-modal-tab') + ']')
                                .css('display', 'block');

                            //tab click
                            tabItems.find('[data-modal-tab]').on('click',
                                function () {

                                    activeElement = element.find('.tab-modal__item-active');
                                    tabContent.find('[data-modal-tab]').css('display', 'none');
                                    activeElement.removeClass('tab-modal__item-active');
                                    angular.element(this).addClass('tab-modal__item-active');
                                    activeElement = element.find('.tab-modal__item-active');
                                    tabContent.find('[data-modal-tab=' + activeElement.attr('data-modal-tab') + ']')
                                        .css('display', 'block');
                                });
                            //close
                            closeBtn.on('click',
                                function () {
                                    //element.find('.tab-modal__footer').hide();
                                    activeElement = element.find('.tab-modal__item-active');
                                    tabContent.find('[data-modal-tab]').css('display', 'none');
                                    activeElement.removeClass('tab-modal__item-active');
                                    tabItems.find("[data-modal-tab=first]").addClass('tab-modal__item-active');
                                    element.stop().slideUp(500);
                                    $scope.Model.SeasonGridPopupData = {};
                                    clearInterval(sliderInterval);
                                })
                        } else {
                            var tabName = window.location.href.split('#')[1];
                            window.open("Https://" + window.location.host + "/" + url, '_blank');
                        }

                        //Show popup end

                    }

                    //window.location.replace("Https://" + window.location.host + "/" + url);
                }
      
                $scope.gridDataSource = function (gridElement) {
                    var elementId = gridElement.ElementId;
                    //var screenWidth = $(window).outerWidth();

                    $scope.gridOptions[elementId] = {
                        excel: {
                            allPages: true,
                            //fileName: "balance (" + $scope.Model.SeasonDomain + ").xlsx",
                        },
                        excelExport: function (e) {
                            //cfpLoadingBar.complete();
                        },
                        dataSource:
                            {
                                batch: true,
                                serverFiltering: false,
                                transport: {
                                    read: {
                                        url: "/Season/SeasonProgramsGrid",
                                        //data: parameterMap,
                                        type: "POST",
                                        contentType:
                                            "application/json; charset=utf-8", // tells the web method to serialize JSON
                                        //accepts: "application/json",
                                        dataType: "json"
                                    },
                                    parameterMap: function (options, operation) {
                                        if (operation == "read") {
                                            var SchoolGradeType = 0;
                                            if (gridElement.selectedGrade != null &&
                                                gridElement.selectedGrade != "undefined" &&
                                                gridElement.selectedGrade.length > 0) {
                                                var SchoolGradeType = gridElement.selectedGrade[0];
                                            }

                                            var data = $.extend(
                                                {
                                                    IncludeClasses: gridElement.IncludeClasses,
                                                    IncludeBeforeAfter: gridElement.IncludeBeforeAfter,
                                                    IncludeCamp: gridElement.IncludeCamp,
                                                    IncludeCalendar: gridElement.IncludeCalendar,
                                                    IncludeChessTournament: gridElement.IncludeChessTournament,
                                                    IncludeSeminarTour: gridElement.IncludeSeminarTour,
                                                    ShowGrade: gridElement.ShowGrade,
                                                    ShowGradeInformation: gridElement.ShowGradeInformation,
                                                    SeasonId: gridElement.SeasonId,
                                                    SchoolGradeType: SchoolGradeType,
                                                    SelectedGrade: gridElement.selectedGradeOpt,
                                                    SelectedCategory: gridElement.selectedCategoryOpt,
                                                    SelectedWeekDay: gridElement.selectedWeekDayOpt,
                                                    SelectedLocationName: gridElement.selectedLocationNameOpt,
                                                    selectedAddress: gridElement.selectedAddressOpt,
                                                    enabledPopup: gridElement.EnabledPopup,
                                                    //EnabledRegistrationPopup: gridElement.EnabledRegistrationPopup,
                                                    CityName: gridElement.cityName,
                                                    CampId: gridElement.campId,
                                                    selectedAMPM: gridElement.selectedAMPM,
                                                    SelectedProgramStatus: gridElement.SelectedProgramStatus,
                                                    HideFuture: gridElement.HideFuture,
                                                    HideExpired: gridElement.HideExpired,
                                                    HideFrozen: gridElement.HideFrozen,
                                                    ProgramRegBtnLinkTo: gridElement.ProgramRegBtnLinkTo,
                                                    GridLayoutType: gridElement.GridLayoutType,
                                                    ProgramRegBtnBackgroundColor: gridElement.ProgramRegBtnBackgroundColor,
                                                    ProgramRegBtnLabelColor: gridElement.ProgramRegBtnLabelColor,
                                                    ProgramRegBtnTitle: gridElement.ProgramRegBtnTitle,

                                                    ShowProgramColumn: gridElement.ShowProgramColumn,
                                                    ShowPriceColumn: gridElement.ShowPriceColumn,
                                                    ShowCapacityColumn: gridElement.ShowCapacityColumn,
                                                    ShowCapacityLeftColumn: gridElement.ShowCapacityLeftColumn,
                                                    ShowStatusColumn: gridElement.ShowStatusColumn,
                                                    ShowGradeAgeColumn: gridElement.ShowGradeAgeColumn,
                                                    ShowRestrictionsColumn: gridElement.ShowRestrictionsColumn,
                                                    ShowRoomColumn: gridElement.ShowRoomColumn,
                                                    ShowLocationColumn: gridElement.ShowLocationColumn,
                                                    ShowDaysTimesColumn: gridElement.ShowDaysTimesColumn,
                                                    ShowDatesColumn: gridElement.ShowDatesColumn,
                                                    ShowRegistrationDatesColumn: gridElement.ShowRegistrationDatesColumn,
                                                    ShowProviderColumn: gridElement.ShowProviderColumn,
                                                    ShowInstructorsColumn: gridElement.ShowInstructorsColumn,
                                                    ShowRegistrationColumn: gridElement.ShowRegistrationColumn
                                                },
                                                data);
                                            //console.log("data send season grid");
                                            //console.log(data)
                                            return JSON.stringify(data);
                                        }
                                    }
                                },

                                groupable: true,
                                sortable: true,
                                group: function () {
                                    if (gridElement.GridLayoutType === 'Weekly') {
                                        return [
                                            {
                                                field: "IntDayOfWeek",
                                            }
                                        ];
                                    }

                                    return [];
                                }(),
                                sort: [
                                    { field: "StartOrder", dir: "asc" }, { field: "EndOrder", dir: "asc" },
                                    { field: "Title", dir: "asc" }
                                ],
                                schema: {
                                    total: "TotalCount",
                                    data: "DataSource"
                                },
                                serverSorting: false,
                                serverGrouping: false,
                            },

                        resizable: true,
                        dataBound: function (e) {
                            if (gridElement.ShowRoomColumn == false) {
                                this.hideColumn('Room');
                            } else {
                                this.showColumn('Room');
                            }
                            if (gridElement.ShowStatusColumn == false) {
                                this.hideColumn('ProgramRegStatus');
                            } else {
                                this.showColumn('ProgramRegStatus');
                            }
                            if (gridElement.ShowRegistrationColumn == false) {
                                this.hideColumn('Registration');
                            } else {
                                this.showColumn('Registration');
                            }


                            if (gridElement.ShowCapacityColumn == false) {
                                this.hideColumn('Capacity');
                            } else {
                                this.showColumn('Capacity');
                            }

                            if (gridElement.ShowCapacityLeftColumn == false) {
                                this.hideColumn('CapacityLeft');
                            } else {
                                this.showColumn('CapacityLeft');
                            }
                            if (gridElement.ShowProviderColumn == false) {
                                this.hideColumn('Provider');
                            } else {
                                this.showColumn('Provider');
                            }

                            if (gridElement.ShowInstructorsColumn == false) {
                                this.hideColumn('Instructors');
                            } else {
                                this.showColumn('Instructors');
                            }
                            if (gridElement.ShowLocationColumn == false) {
                                this.hideColumn('Location');
                            } else {
                                this.showColumn('Location');
                            }

                            if (gridElement.ShowPriceColumn == false) {
                                this.hideColumn('Classfee');
                            } else {
                                this.showColumn('Classfee');
                            }

                            if (gridElement.GridLayoutType === 'List') {
                                this.hideColumn('StartEndTime');
                            } else if (gridElement.GridLayoutType === 'Weekly') {
                                this.showColumn('StartEndTime');
                            }


                            if (gridElement.ShowRegistrationDatesColumn == false) {
                                this.hideColumn('RegistrationPeriod');
                            } else {
                                this.showColumn('RegistrationPeriod');
                            }

                            if (gridElement.ShowDatesColumn == false) {
                                this.hideColumn('Dates');
                            } else {
                                this.showColumn('Dates');
                            }


                            if (gridElement.ShowRestrictionsColumn == false) {
                                this.hideColumn('GenderRestrictions');
                            } else {
                                this.showColumn('GenderRestrictions');
                            }

                            if (gridElement.ShowDaysTimesColumn == false) {
                                this.hideColumn('DaysTimes');
                            } else {
                                this.showColumn('DaysTimes');
                            }

                            if (gridElement.ShowProgramColumn == false) {
                                this.hideColumn('Title');
                            } else {
                                this.showColumn('Title');
                            }

                            if (gridElement.ShowGradeAgeColumn == false) {
                                this.hideColumn('GradeAge');
                            } else {
                                this.showColumn('GradeAge');
                            }
                        },
                        columns: [
                            {
                                field: "IntDayOfWeek",
                                title: "Day",
                                hidden: true,
                                template: "#=DayOfWeek #",
                                groupHeaderTemplate: "#=value.substring(1)#",
                                width: "180px"
                            },
                            {
                                field: "StartEndTime",
                                title: "Time",
                                template: "#=StartEndTime # <br /> #if(SecondStartEndTime != null){# #=SecondStartEndTime# #}#",
                                groupHeaderTemplate: "#= value #",
                                width: "150px"
                            },              
                            {
                                field: "Title",
                                title: "Program",
                                width: "320px",
                                template:
                                    "<a class='program-title' ng-click='viewClass(\"#=DisplayUrl#\",\"#=ProgramType#\",\"#=ProgramId#\",\"#=EnablePopUp#\")' href='' target='_blank'> #= Title # </a>"
                            },
                            {
                                field: "Classfee",
                                title: "Price",
                                width: "110px",
                                template: "<div ng-bind-html='arrayValuesFormat(#=JSON.stringify(Classfee)#,\"div-block\",\"Classfee\",\"#=ProgramType#\")'></div>"
                            },
                            {
                                field: "Capacity",
                                title: "Capacity",
                                width: "75px",
                                template: "<div ng-bind-html='arrayValuesFormat(#=JSON.stringify(Capacity)#,\"span-block\",\"Capacity\",\"#=ProgramType#\")'></div>"
                            },
                            {
                                field: "CapacityLeft",
                                title: "Spots left",
                                width: "80px",
                                //template: " #=CapacityLeft# ",
                                template: "<div ng-bind-html='arrayValuesFormat(#=JSON.stringify(CapacityLeft)#,\"span-block\",\"CapacityLeft\",\"#=ProgramType#\")'></div>"

                            },
                            {
                                field: "ProgramRegStatus",
                                title: "Status",
                                width: "90px"
                                //template: "#=ProgramRegStatus#  # if (CapacityLeft > 0 && CapacityLeft<= 3 && ProgramRegStatus != 'Canceled') { # - spots left: #=CapacityLeft# # } #"
                            },
                            {
                                field: "GradeAge",
                                title: "Restriction",
                                width: "160px"
                            },
                            {
                                field: "GenderRestrictions",
                                title: "Gender restriction",
                                width: "130px"

                            },
                            {
                                field: "Room",
                                title: "Room #",
                                width: "70px"
                            },
                            {
                                field: "Location",
                                title: "Location",
                                width: "240px"
                            },
                            {
                                field: "DaysTimes",
                                title: "Day & time",
                                template: "#=DaysTimes#",
                                width: "200px"
                            },
                            {
                                field: "Dates",
                                title: "Program dates",
                                template: "#=Dates#",
                                width: "200px"
                            },
                            {
                                field: "RegistrationPeriod",
                                title: "Registration dates",
                                width: "200px"
                            },
                            {
                                field: "Provider",
                                title: "Provider",
                                width: "120px"
                            },
                            {
                                field: "Instructors",
                                title: "Instructor",
                                template: "<div ng-bind-html='arrayValuesFormat(#=JSON.stringify(Instructors)#,\"span-block\",\"Instructors\",\"#=ProgramType#\")'></div>",
                                width: "120px"
                            },
                            {
                                field: "Registration",
                                title: "Registration",
                                template:
                                    "<a href='# if(ProgramRegBtnLinkTo==\'RegisterationPage\' ) { # #=SingleRegisterUrl#/Register # } else{# #=SingleRegisterUrl# #} #' target='_blank' class='btn btn-success btn-xs' style='text-transform: initial;color:#=ProgramRegBtnLabelColor#;background-color:#=ProgramRegBtnBackgroundColor#'>#=ProgramRegBtnTitle#</a>",
                                width: "150px"
                            }
                        ],
                    }

                };
   
                $scope.priceCapacitySpotsFormat = function (arr, enableCapacity, enableSpots, enablePrice, programType) {
                    
                    var resultStr = "";
                    if (arr && arr.length != 0) {
                        $.each(arr, function (index, value) {

                         
                            var priceAndEarlyBird = "";
                            if (enablePrice) {
                                 priceAndEarlyBird = (value.HasEarlyBird === true)
                                    ? "<del style='color:red;font-weight:500'>" + value.StrAmount + "</del> " + "<span style='color:green;font-weight:500'>" + value.StrEarlyBirdAmount + "</span></span><span style='color: #fff; font-weight: 500; padding: 0 5px; margin-left: 5px; background-color: #5eab1e;'>Early bird expires on " + value.EarlyBirdExpiryDate + "</span>"
                                    : value.StrAmount;
                            }
                                  
                          
                            resultStr += "<div style='display: block;color: #fff; white-space: nowrap; width: 100%; text-overflow: ellipsis; overflow: hidden;'>";
                            if ((enablePrice || (enableCapacity || enableSpots))) {
                                resultStr += "<sapn style='color:#787878'>" + value.Name + ": </span>";
                            }

                            resultStr += (enablePrice) ? priceAndEarlyBird : "";
                            if (value.Capacity || value.CapacityLeft) {

                                resultStr += ((enableCapacity || enableSpots)) ? " (" : "";
                                (value.Capacity && enableCapacity) ? resultStr += "capacity: " + value.Capacity : resultStr += "";
                                (value.Capacity && value.CapacityLeft && enableCapacity && enableSpots) ? resultStr += ", " : resultStr += "";
                                (value.CapacityLeft && enableSpots) ? resultStr += "spots: " + value.CapacityLeft : resultStr += "";
                                resultStr += ((enableCapacity || enableSpots)) ? ")" : "";
                            }
                            resultStr += "</div>";


                        });
                        return $sce.trustAsHtml(resultStr);
                    }
                    else if (programType === "Camp" || programType === "BeforeAfterCare") {
                        return $sce.trustAsHtml("Follow the registration to see prices");

                    }
                    return "";
                };
                $scope.arrayValuesFormat = function (arr, format, type, programType) {
                   
                    var resultStr = "", resultCapacity = "", resultCapacityLeft = "";
        
                    if (arr && arr.length!=0) {
                        $.each(arr,
                            function(index, value) {
                                if (format === "span-block") {
                                    if (type === "Instructors" || type === "Capacity") {
                                        resultCapacity = value ? value : "&nbsp;";
                                        resultStr += "<span style='display:block'>" + resultCapacity + "</span>";
                                    } else if (type === "CapacityLeft") {
                                        resultCapacityLeft = value.Value ? value.Value : "&nbsp;";
                                        if (value.Value === "Full")
                                            resultStr +=
                                                "<span style='display: inline-block; background-color: #d01c1c; color: #fff; padding: 0 5px; line-height: 1.3;    min-width: 20px; text-align: center; font-weight: 500; margin-bottom: 2px;'>" +
                                                value.Value +
                                                "</span><br />";
                                        else
                                            resultStr +=
                                                "<span style='display:inline-block;padding: 0 5px;line-height: 1.3;min-width: 20px;text-align:center;margin-bottom:2px;'>" +
                                                resultCapacityLeft +
                                                "</span><br />";
                                    }
                                } else if (format === "div-block") {
                                 
                                    if (type === 'Classfee') {

                                        var priceAndEarlyBird = (value.HasEarlyBird === true)
                                            ? "<del style='color:red;font-weight:500'>" +
                                            value.StrAmount +
                                            "</del> " +
                                            "<span style='color:green;font-weight:500'>" +
                                            value.StrEarlyBirdAmount +
                                            "</span></span><span style='color: #fff; font-weight: 500; padding: 0 5px; margin-left: 5px; background-color: #5eab1e;'>Early bird expires on " +
                                            value.EarlyBirdExpiryDate +
                                            "</span>"
                                            : value.StrAmount;
                                        
                                        var titlePrice = (arr.length > 1) ? value.Name : "";
                                        resultStr +=
                                            "<div style='display: block; white-space: nowrap; width: 100%; text-overflow: ellipsis; overflow: hidden;color:#787878'>"+"<sapn style='color:#787878'>" +priceAndEarlyBird + " </span>"+
                                            "<sapn style='color:#787878'> " + titlePrice + "</span>" +
                                            "</div>";

                                    }
                                }

                            });
                        return $sce.trustAsHtml(resultStr);
                    } else if (programType === "Camp" || programType === "BeforeAfterCare") {
                        if (format === "div-block") {

                            if (type === 'Classfee') {
                                return $sce.trustAsHtml("Follow the registration to see prices");

                            }
                        }
                      
                    }
                    return "";
                };
                $scope.if = {
                    isDesignMode: function () {
                        return is.existy($scope.Model) && $scope.Model.PageMode == "Design";
                    },
                    isIframeMode: function () {
                        return is.existy($scope.Model) && $scope.Model.PageMode == "Iframe";
                    },
                    isViewMode: function () {
                        return is.existy($scope.Model) && $scope.Model.PageMode == "View";
                    },
                    isPreviewMode: function () {
                        return is.existy($scope.Model) && $scope.Model.PageMode == "Preview";
                    },
                    showHeader: function () {
                        return is.existy($scope.Model) && ($scope.if.isDesignMode() || ($scope.if.isIframeMode() ? $scope.iframeOptions.showheader : true));
                    },
                    showFooter: function () {
                        return is.existy($scope.Model) && ($scope.Model.Footer.Visible && ($scope.if.isIframeMode() ? $scope.iframeOptions.showfooter : true));
                    },
                    showCover: function () {
                        return is.existy($scope.Model) && ($scope.Model.Cover.Visible && ($scope.if.isIframeMode() ? $scope.iframeOptions.showcover : true));
                    },
                    showFooterContact: function () {
                        return is.existy($scope.Model) && ($scope.Model.Footer.Email || $scope.Model.Footer.Phone || $scope.Model.Footer.Website || $scope.Model.Footer.Address);
                    }
                }
                           
                $scope.format = function (type, value) {
                    switch (type) {
                        case 'date':
                            if (is.not.existy(value)) {
                                return value;
                            }
                            var date;

                            var month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                            //by MGH to ignore local timezone
                            var spliteDateTmp = value.split('T');
                            var spliteDate = spliteDateTmp[0].split('-');

                            var index = parseInt(spliteDate[1]) - 1;

                            //for kidding Safari browser
                            if (spliteDate[1] == "08") {
                                index = 7;
                            }
                            if (spliteDate[1] == "09") {
                                index = 8;
                            }

                            var e = month_names_short[index];
                            var date = e + ' ' + spliteDate[2] + ', ' + spliteDate[0];

                            //date = new Date(value);
                            //date = date.toUTCString();

                            if (date == "Invalid Date") {
                                var a = value.split(/[^0-9]/);
                                date = new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);
                            }

                            return date;
                            //return date.format("mmm dd, yyyy");
                            break;
                        case 'phone':
                            if (is.not.existy(value)) {
                                return value;
                            }
                            return value.trim().replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2 $3");
                            break;
                        case 'address':
                            if (is.not.existy(value)) {
                                return value;
                            }
                            return value.replace(/(.*),(.*),(.*)$/, "$1, <br> $2, <br> $3")
                            break;
                        case 'age':
                            if (is.not.existy(value)) {
                                return value;
                            } else if (is.existy(value.minAge) && is.existy(value.maxAge) && value.minAge == value.maxAge) {
                                return 'Only ' + value.minAge;
                            } else if (is.existy(value.minAge) && is.existy(value.maxAge) && value.minAge != value.maxAge) {
                                return value.minAge + ' - ' + value.maxAge;
                            } else if (is.existy(value.minAge)) {
                                return 'Minimum age: ' + value.minAge
                            } else if (is.existy(value.maxAge)) {
                                return 'Maximum age: ' + value.maxAge
                            } else {
                                return null;
                            }
                            break;
                        case 'grade':
                            if (is.not.existy(value)) {
                                return value;
                            } else if (value.minGrade && value.maxGrade && value.minGrade == value.maxGrade) {
                                return 'Only ' + value.minGrade;
                            } else if (value.minGrade && value.maxGrade && value.minGrade != value.maxGrade) {
                                return value.minGrade + ' - ' + value.maxGrade;
                            } else if (value.minGrade) {
                                return 'Minimum grade: ' + value.minGrade
                            } else if (value.maxGrade) {
                                return 'Maximum grade: ' + value.maxGrade
                            } else {
                                return null;
                            }
                            break;
                        case 'price':
                            if (is.not.existy(value)) {
                                return value;
                            }
                            var v = value,
                                n = 2,
                                x = null,
                                notValidErrorMessage = 'Input is not valid. expected types: string(e.g. \'$-21.15\') and number(e.g. 58)';
                            if (typeof v === 'string') {
                                v = v.replace(/[^0-9.-]/g, '');
                                if (v === '') {
                                    console.error(notValidErrorMessage)
                                }
                                v = Number(v);
                                if (v === NaN) {
                                    console.error(notValidErrorMessage)
                                }
                            } else if (typeof v !== 'number') {
                                console.error(notValidErrorMessage)
                            }
                            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
                            if (v >= 0) {
                                return '$' + v.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                            } else {
                                v = v * -1;
                                return '-$' + v.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
                            }
                            break;
                        default:
                            throw "Type not found. Available types: 'date', 'phone', 'age', 'grade' and 'address' ";
                    }
                }

            }
        ])
        .controller('filterController', ['$scope', '$http', '$modal', '$timeout', 'FileUploader', function ($scope, $http, $modal, $timeout, FileUploader) {

            $scope.toggleItems = function (tab, itemMenu) {
                if (tab.FilterToggle[itemMenu])
                    tab.FilterToggle[itemMenu] = false;
                else
                    tab.FilterToggle[itemMenu] = true;
            }


            $scope.showAllowedGrades = function (obj) {
                return obj.IsEnabled;
            }
            $scope.showActiveFilterAddress = function (tab) {
                return function (sAddress) {
                    return sAddress.AddressId == tab.FilterSelected.Address;
                }
            }
            $scope.showActiveFilterCategories = function (tab) {

                return function (sCategory) {
                    return (tab.FilterSelected.Category.indexOf(sCategory.Value) != -1);
                }
            }
            $scope.showActiveFilterWeekDay = function (tab) {

                return function (sWeekDay) {
                    return (tab.FilterSelected.WeekDay.indexOf(sWeekDay) != -1);
                }
            }
            $scope.showActiveFilterLocationName = function (tab) {

                return function (sLocationName) {
                    return (tab.FilterSelected.LocationName == sLocationName.ClubLocationId);
                }
            }
            $scope.showActiveFilterGrade = function (tab) {

                return function (sGrade) {
                    return (tab.FilterSelected.Grade == sGrade.Grade);
                }
            }
            //filter end
            $scope.toggleFilter = function (dataHandel) {
                if (dataHandel)
                    dataHandel = false;
                else
                    dataHandel = true;
                return dataHandel;
            };
            $scope.getProgramsForFilter = function (tab) {

                tab.EnableFilter = false;
                //filter result 


                var dataFilter = tab.Elements;
                var results = [];
                var searchField = "Type";
                var searchVal = "JbPSeasonGrid";
                for (var i = 0; i < dataFilter.length; i++) {
                    if (dataFilter[i][searchField] == searchVal) {
                        results.push(dataFilter[i]);
                        break;
                    }
                }

                var seasonIDs = [];
                tab.FilterTypes = [];

                tab.FilterToggle = [];
                tab.FilterToggle.Address = false;
                tab.FilterToggle.Category = false;
                tab.FilterToggle.Status = false;
                tab.FilterToggle.AMPM = false;
                tab.FilterToggle.Category = false;
                tab.FilterToggle.City = false;
                tab.FilterToggle.LocationName = false;
                tab.FilterToggle.WeekDay = false;
                tab.FilterToggle.Grade = false;




                tab.FilterSelected = [];
                tab.FilterSelected.City = "All";
                tab.FilterSelected.Address = "All";
                tab.FilterSelected.Category = [0];
                tab.FilterSelected.LocationName = "All";
                tab.FilterSelected.WeekDay = [];
                tab.FilterSelected.Grade = null;
                tab.FilterSelected.AMPM = [];
                tab.FilterSelected.Status = ["All"];

                tab.FilterData = [];
                tab.FilterData.HasAddress = false;
                tab.FilterData.HasCategory = false;
                tab.FilterData.HasStatus = false;
                tab.FilterData.HasCategory = false;
                tab.FilterData.HasCity = false;
                tab.FilterData.HasLocationName = false;
                tab.FilterData.HasWeekDay = false;
                tab.FilterData.HasGrade = false;
                tab.FilterData.EnabledFilterOptions = true;



                tab.FilterData.ProgramsAddress = [];
                tab.FilterData.ProgramsCategory = [];
                tab.FilterData.ProgramsCitys = [];
                tab.FilterData.ProgramsGrades = [];
                tab.FilterData.ProgramsLocationName = [];
                tab.FilterData.ProgramsWeekDay = [];
                tab.FilterData.Setting = [];






                if (results[0] && results[0]['Filter'] != null) {
                    seasonIDs.push(results[0]["SeasonId"]);
                    var checkIncludeCalendar = results[0]["IncludeCalendar"],
                        checkIncludeCamp = results[0]["IncludeCamp"],
                        checkIncludeChessTournament = results[0]["IncludeChessTournament"],
                        checkIncludeClasses = results[0]["IncludeClasses"],
                        checkIncludeSeminarTour = results[0]["IncludeSeminarTour"];


                    //console.log("Program types: IncludeCalendar-" + checkIncludeCalendar + " IncludeCamp-" + checkIncludeCamp + " IncludeChessTournament-" + checkIncludeChessTournament + " IncludeClasses-" + checkIncludeClasses + " IncludeSeminarTour-" + checkIncludeSeminarTour);

                    tab.FilterData.Setting = results[0]["Filter"]["Setting"];
                    if (results[0]["Filter"]["HasAddress"]) {
                        tab.FilterTypes.push("HasAddress");
                        tab.FilterData.HasAddress = true;
                        $http.post('/JbPage/GetProgramLocations', { 'seasonIds': seasonIDs, 'IncludeCalendar': checkIncludeCalendar, 'IncludeCamp': checkIncludeCamp, 'IncludeChessTournament': checkIncludeChessTournament, 'IncludeClasses': checkIncludeClasses, 'IncludeSeminarTour': checkIncludeSeminarTour }).success(function (data, status, headers, config) {
                            tab.FilterData.ProgramsAddress = data.Locations;
                            tab.FilterData.ProgramsAddress.unshift({ "AddressId": "0", "Address": "All", "SeasonId": 0 });
                        });
                    }

                    if (results[0]["Filter"]["HasStatus"]) {
                        tab.FilterTypes.push("HasStatus");
                        tab.FilterData.HasStatus = true;

                    }
                    if (results[0].EnabledFilterOptions != undefined && results[0].EnabledFilterOptions != null) {
                        tab.FilterData.EnabledFilterOptions = results[0].EnabledFilterOptions;
                    }
                    if (results[0].GridLayoutType != undefined && results[0].GridLayoutType != null) {
                        tab.FilterData.GridLayoutType = results[0].GridLayoutType;
                    }
                    if (results[0]["Filter"]["HasCategory"]) {
                        tab.FilterTypes.push("HasCategory");
                        tab.FilterData.HasCategory = true;
                        $http.post('/JbPage/GetProgramsCategories', { 'seasonIds': seasonIDs, 'IncludeCalendar': checkIncludeCalendar, 'IncludeCamp': checkIncludeCamp, 'IncludeChessTournament': checkIncludeChessTournament, 'IncludeClasses': checkIncludeClasses, 'IncludeSeminarTour': checkIncludeSeminarTour }).success(function (data, status, headers, config) {
                            tab.FilterData.ProgramsCategory = data;
                            tab.FilterData.ProgramsCategory.unshift({ "Value": 0, "Title": "All" });
                        });

                    }
                    if (results[0]["Filter"]["HasCity"]) {
                        tab.FilterTypes.push("HasCity");
                        tab.FilterData.HasCity = true;

                        $http.post('/JbPage/GetProgramCities', { 'seasonIds': seasonIDs, 'IncludeCalendar': checkIncludeCalendar, 'IncludeCamp': checkIncludeCamp, 'IncludeChessTournament': checkIncludeChessTournament, 'IncludeClasses': checkIncludeClasses, 'IncludeSeminarTour': checkIncludeSeminarTour }).success(function (data, status, headers, config) {
                            tab.FilterData.ProgramsCitys = data.Cities;
                            tab.FilterData.ProgramsCitys.unshift({ "LocationId": "0", "Location": null, "SeasonId": 0, "City": "All" });
                        });
                    }
                    if (results[0]["Filter"]["HasGrade"]) {
                        tab.FilterTypes.push("HasGrade");
                        tab.FilterData.HasGrade = true;
                        tab.FilterData.ProgramsGrades = results[0]["Filter"]["Grades"];
                        //tab.FilterData.ProgramsGrades.unshift({ "Grade": "0", "Title": "All grades","IsEnabled":true });
                    }
                    if (results[0]["Filter"]["HasLocationName"]) {
                        tab.FilterTypes.push("HasLocationName");
                        tab.FilterData.HasLocationName = true;
                        $http.post('/JbPage/GetProgramLocationsName', { 'seasonIds': seasonIDs, 'IncludeCalendar': checkIncludeCalendar, 'IncludeCamp': checkIncludeCamp, 'IncludeChessTournament': checkIncludeChessTournament, 'IncludeClasses': checkIncludeClasses, 'IncludeSeminarTour': checkIncludeSeminarTour }).success(function (data, status, headers, config) {
                            tab.FilterData.ProgramsLocationName = data;
                            tab.FilterData.ProgramsLocationName.unshift({ "ClubLocationId": "0", "LocationName": "All", "SeasonId": 0 });
                        });

                    }
                    if (results[0]["Filter"]["HasWeekDay"]) {
                        tab.FilterTypes.push("HasWeekDay");
                        tab.FilterData.HasWeekDay = true;
                        $http.post('/JbPage/GetProgramsWeekDays', { 'seasonIds': seasonIDs, 'IncludeCalendar': checkIncludeCalendar, 'IncludeCamp': checkIncludeCamp, 'IncludeChessTournament': checkIncludeChessTournament, 'IncludeClasses': checkIncludeClasses, 'IncludeSeminarTour': checkIncludeSeminarTour }).success(function (data, status, headers, config) {
                            tab.FilterData.ProgramsWeekDay = data;

                        });
                    }

                    if (tab.FilterTypes.length > 0) {
                        tab.EnableFilter = true;
                        if (tab.FilterData.EnabledFilterOptions == false) {
                            tab.EnableFilter = false;
                        }
                    }
                    else {
                        tab.EnableFilter = false;
                    }
                }
                var seasonComplate = results[0];
            };
            $scope.refreshAllGrids = function () {

                var tab, type, dataFilterParam, doIt;
                if (arguments.length == 1) {
                    tab = arguments[0];
                }
                else if (arguments.length == 3) {
                    tab = arguments[0];
                    type = arguments[1];
                    dataFilterParam = arguments[2];
                }
                else if (arguments.length == 4) {
                    tab = arguments[0];
                    type = arguments[1];
                    dataFilterParam = arguments[2];
                    doIt = arguments[3];
                }

                var dataFilter = tab.Elements;


                var results = [];
                var searchField = "Type";
                var searchVal = "JbPSeasonGrid";
                if (doIt === undefined || doIt == true) {
                    for (var i = 0; i < dataFilter.length; i++) {
                        if (dataFilter[i][searchField] == searchVal) {

                            if (type && type == 'address') {
                                tab.FilterSelected.Address = dataFilterParam;
                                dataFilter[i]["selectedAddressOpt"] = tab.FilterSelected.Address;
                            }
                            if (type && type == 'grade') {
                                tab.FilterSelected.Grade = dataFilterParam;
                                dataFilter[i]["selectedGradeOpt"] = tab.FilterSelected.Grade;
                            }
                            if (type && type == 'locationName') {
                                tab.FilterSelected.LocationName = dataFilterParam;
                                dataFilter[i]["selectedLocationNameOpt"] = tab.FilterSelected.LocationName;
                            }
                            if (type && type == 'city') {
                                tab.FilterSelected.City = dataFilterParam;
                                dataFilter[i]["cityName"] = tab.FilterSelected.City;
                            }
                            if (type && type == 'AMPM') {

                                if (tab.FilterSelected.AMPM.length == 0) {
                                    tab.FilterSelected.AMPM.push(dataFilterParam)
                                    dataFilter[i]["selectedAMPM"] = tab.FilterSelected.AMPM;

                                }
                                else {
                                    var findRepeat = false;
                                    for (var cl = 0; cl <= tab.FilterSelected.AMPM.length; cl++) {
                                        if (tab.FilterSelected.AMPM[cl] == dataFilterParam) {
                                            tab.FilterSelected.AMPM.splice(cl, 1);
                                            findRepeat = true;
                                        }

                                    }
                                    if (!findRepeat) {
                                        tab.FilterSelected.AMPM.push(dataFilterParam);
                                    }
                                    dataFilter[i]["selectedAMPM"] = tab.FilterSelected.AMPM;
                                    //$(".tab-pane.active [data-gridtracker]").data("kendoGrid").dataSource.sort({ field: "Title", dir: "asc" });
                                    //$(".tab-pane.active [data-gridtracker]").data("kendoGrid").dataSource.sort({ field: "PMAM", dir: "asc" });

                                }


                            }
                            if (type && type == 'status') {

                                //Status start
                                if (tab.FilterSelected.Status.length == 1) {
                                    if (dataFilterParam == "All") {
                                        tab.FilterSelected.Status = [];
                                        tab.FilterSelected.Status.push(dataFilterParam)
                                        dataFilter[i]["SelectedProgramStatus"] = tab.FilterSelected.Status;
                                    }
                                    else {
                                        if (tab.FilterSelected.Status[0] == "All")
                                            tab.FilterSelected.Status = [];
                                        if (tab.FilterSelected.Status[0] == dataFilterParam) {
                                            tab.FilterSelected.Status.splice(0, 1);
                                            tab.FilterSelected.Status.push("All");
                                        }
                                        else {
                                            tab.FilterSelected.Status.push(dataFilterParam)
                                            dataFilter[i]["SelectedProgramStatus"] = tab.FilterSelected.Status;
                                        }

                                    }
                                }
                                else {
                                    var findRepeat = false;
                                    var findAllStatus = false;

                                    for (var cl = 0; cl <= tab.FilterSelected.Status.length; cl++) {
                                        if (dataFilterParam == "All" || tab.FilterSelected.Status[cl] == "All") {

                                            if (tab.FilterSelected.Status[cl] == "All") {
                                                tab.FilterSelected.Status.splice(cl, 1);
                                            }
                                            if (dataFilterParam == "All") {
                                                findAllStatus = true;
                                            }
                                        }
                                        else {

                                            if (tab.FilterSelected.Status[cl] == dataFilterParam) {
                                                tab.FilterSelected.Status.splice(cl, 1);
                                                findRepeat = true;
                                            }
                                        }
                                    }
                                    if (!findRepeat) {
                                        if (!findAllStatus) {
                                            tab.FilterSelected.Status.push(dataFilterParam);
                                            dataFilter[i]["SelectedProgramStatus"] = tab.FilterSelected.Status;
                                        }
                                        else {
                                            tab.FilterSelected.Status = [];
                                            tab.FilterSelected.Status.push("All");
                                            dataFilter[i]["SelectedProgramStatus"] = tab.FilterSelected.Status;
                                        }
                                    }
                                }
                                //status end


                            }
                            if (type && type == 'category') {
                                if (tab.FilterSelected.Category.length == 1) {
                                    if (dataFilterParam == 0) {
                                        tab.FilterSelected.Category = [];
                                        tab.FilterSelected.Category.push(dataFilterParam)
                                        dataFilter[i]["selectedCategoryOpt"] = tab.FilterSelected.Category;
                                    }
                                    else {
                                        if (tab.FilterSelected.Category[0] == 0)
                                            tab.FilterSelected.Category = [];
                                        if (tab.FilterSelected.Category[0] == dataFilterParam) {
                                            tab.FilterSelected.Category.splice(0, 1);
                                            tab.FilterSelected.Category.push(0);
                                        }
                                        else {
                                            tab.FilterSelected.Category.push(dataFilterParam)
                                            dataFilter[i]["selectedCategoryOpt"] = tab.FilterSelected.Category;
                                        }

                                    }
                                }
                                else {
                                    var findRepeat = false;
                                    var findAllCat = false;

                                    for (var cl = 0; cl <= tab.FilterSelected.Category.length; cl++) {
                                        if (dataFilterParam == 0 || tab.FilterSelected.Category[cl] == 0) {

                                            if (tab.FilterSelected.Category[cl] == 0) {
                                                tab.FilterSelected.Category.splice(cl, 1);
                                            }
                                            if (dataFilterParam == 0) {
                                                findAllCat = true;
                                            }
                                        }
                                        else {

                                            if (tab.FilterSelected.Category[cl] == dataFilterParam) {
                                                tab.FilterSelected.Category.splice(cl, 1);
                                                findRepeat = true;
                                            }
                                        }
                                    }
                                    if (!findRepeat) {
                                        if (!findAllCat) {
                                            tab.FilterSelected.Category.push(dataFilterParam);
                                            dataFilter[i]["selectedCategoryOpt"] = tab.FilterSelected.Category;
                                        }
                                        else {
                                            tab.FilterSelected.Category = [];
                                            tab.FilterSelected.Category.push(0);
                                            dataFilter[i]["selectedCategoryOpt"] = tab.FilterSelected.Category;
                                        }
                                    }
                                }
                            }
                            if (type && type == 'weekDay') {
                                if (tab.FilterSelected.WeekDay.length == 0) {
                                    tab.FilterSelected.WeekDay.push(dataFilterParam)
                                    dataFilter[i]["selectedWeekDayOpt"] = tab.FilterSelected.WeekDay
                                }
                                else {
                                    var findRepeat = false;
                                    for (var wl = 0; wl <= tab.FilterSelected.WeekDay.length; wl++) {
                                        if (tab.FilterSelected.WeekDay[wl] == dataFilterParam) {
                                            tab.FilterSelected.WeekDay.splice(wl, 1);
                                            findRepeat = true;
                                        }
                                    }
                                    if (!findRepeat) {
                                        tab.FilterSelected.WeekDay.push(dataFilterParam);
                                    }
                                    dataFilter[i]["selectedWeekDayOpt"] = tab.FilterSelected.WeekDay;
                                }
                            }

                            break;
                        }
                    }
                    //if (tab.FilterSelected.AMPM.length == 2 || tab.FilterSelected.AMPM.length == 0) {
                    //    $(".tab-pane.active [data-gridtracker]").data("kendoGrid").dataSource.sort({ field: "Title", dir: "asc" });
                    //    $(".tab-pane.active [data-gridtracker]").data("kendoGrid").dataSource.sort({ field: "PMAM", dir: "asc" });

                    //    //$(".tab-pane.active [data-gridtracker]").data("kendoGrid").dataSource.group({ field: "PMAM" }).sort({ field: "title", dir: "asc" });

                    //}
                    //else if (tab.FilterSelected.AMPM.length == 1) {
                    //    $(".tab-pane.active [data-gridtracker]").data("kendoGrid").dataSource.sort({ field: "Title", dir: "asc" });
                    //}

                    $(".tab-pane.active [data-gridtracker]").data("kendoGrid").dataSource.read($scope.gridDataSource);

                    //group: [{ field: "intDayOfWeek", sort: "none" }],
                    ////sort: { field: "Title", dir: "asc" },
                    //sort: { field: "PMAM", dir: "asc" },
                }


            }
        }])
        //.controller('beforeAfterProgramController', ['$scope', '$http', '$modal', '$timeout', function ($scope, $http, $modal, $timeout) {
        //    //var getSeason = function (seasons, id) {

        //    //    for (var i = 0; i < seasons.length; i++) {
        //    //        if (seasons[i].Id == id) {
        //    //            return seasons[i];
        //    //        }
        //    //    }

        //    //    return null;
        //    //}
        //    $scope.changeSeason = function (selectedSeason) {

        //        //$scope.Model._Active.SeasonId = selectedSeason.Key;
        //        //$scope.Model._Active.seasonId

        //    }



        //}])
        .controller('galleryImageController', ['$scope', '$http', '$modal', '$timeout', 'FileUploader', function ($scope, $http, $modal, $timeout, FileUploader) {
            $scope.Model.newImages = []

            $scope.galleryCancelSmart = function () {
                var newImages = $scope.Model.newImages;

                if (newImages.length > 0) {

                    $http.post('/File/RemoveImages', { images: newImages, location: "/homeSiteImageGallery" })
                        .success(function (data, status, headers, config) {

                            if (data.Status == true) {
                                $scope.Cancel();
                            } else {
                                $scope.Cancel();
                                //$scope.MC.handleErrors($scope, data);
                            }
                        })
                } else {
                    $scope.Cancel();
                }
                //Array.prototype.push.apply($scope.Model.deletedImages, newImages);
                //console.log(newImages);
            }
            $scope.gallerySaveSmart = function (removedItems) {
                Array.prototype.push.apply($scope.Model.deletedImages, removedItems);
                $scope.Save();
            }
            var uploader = $scope.uploader = new FileUploader({
                url: '/File/UploadGallery?name=Gallery'

            });
            $scope.saveTitle = function (keyEvent, item) {
                if (keyEvent.which === 13)
                    item.hideItem = false;

            }
            $scope.addHide = function (item) {
                item.hideItem = true;
            }
            $scope.manageQueue = function (count) {

                if (!count) {
                    $scope.uploader.clearQueue();
                }
            }
            $scope.removeImageItem = function (item, index, removedItems) {

                removedItems.push({ ElementId: item.ElementId, smallImage: item.smallImage, largeImage: item.largeImage, location: item.location });
                $scope.Model._Active.Images.splice(index, 1);

            }
            // FILTERS

            uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            });

            // CALLBACKS

            uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                //console.info('onWhenAddingFileFailed', item, filter, options);
            };
            uploader.onAfterAddingFile = function (fileItem) {

                //console.info('onAfterAddingFile', fileItem);
            };
            uploader.onAfterAddingAll = function (addedFileItems) {
                //console.info('onAfterAddingAll', addedFileItems);
            };
            uploader.onBeforeUploadItem = function (item) {
                //console.info('onBeforeUploadItem', item);
            };
            uploader.onProgressItem = function (fileItem, progress) {
                //console.info('onProgressItem', fileItem, progress);
            };
            uploader.onProgressAll = function (progress) {
                //console.info('onProgressAll', progress);
            };
            uploader.onSuccessItem = function (fileItem, response, status, headers) {
                //console.info('onSuccessItem', fileItem, response, status, headers);
                //console.log($scope.Model._Active);
                var imgID = "JbPImage" + Math.round(Math.random() * 1000000);
                fileItem.remove();
                $scope.Model.newImages.push({ ElementId: imgID, smallImage: response.SmallImage, largeImage: response.LargeImage, location: "/homeSiteImageGallery" });
                $scope.Model._Active.Images.push({ ElementId: imgID, smallImage: response.SmallImage, largeImage: response.LargeImage, Title: null, location: "/homeSiteImageGallery" });

            };
            uploader.onErrorItem = function (fileItem, response, status, headers) {
                //console.info('onErrorItem', fileItem, response, status, headers);
                $scope.error_upload_file = true;
            };
            uploader.onCancelItem = function (fileItem, response, status, headers) {
                //console.info('onCancelItem', fileItem, response, status, headers);
            };
            uploader.onCompleteItem = function (fileItem, response, status, headers) {
                //console.info('onCompleteItem', fileItem, response, status, headers);
            };
            uploader.onCompleteAll = function () {
                //console.info('onCompleteAll');
            };

            //console.info('uploader', uploader);
        }])
        .controller('sliderImageController', ['$scope', '$http', '$modal', '$timeout', 'FileUploader', function ($scope, $http, $modal, $timeout, FileUploader) {
            $scope.Model.newImages = [];

            $scope.sliderCancelSmart = function () {
                var newImages = $scope.Model.newImages;

                if (newImages.length > 0) {

                    $http.post('/File/RemoveImages', { images: newImages, location: "/homeSiteSlider" })
                        .success(function (data, status, headers, config) {

                            if (data.Status == true) {
                                $scope.Cancel();
                            } else {
                                $scope.Cancel();
                                //$scope.MC.handleErrors($scope, data);
                            }
                        })
                } else {
                    $scope.Cancel();
                }
            }
            $scope.sliderSaveSmart = function (removedItems) {
                Array.prototype.push.apply($scope.Model.deletedImages, removedItems);
                $scope.Save();
            }
            var uploader = $scope.uploader = new FileUploader({
                url: '/File/UploadGallery?name=Slider'
            });
            $scope.saveTitle = function (keyEvent, item) {
                if (keyEvent.which === 13)
                    item.hideItem = false;

            }
            $scope.addHide = function (item) {
                item.hideItem = true;
            }
            $scope.manageQueue = function (count) {

                if (!count) {
                    $scope.uploader.clearQueue();
                }
            }
            $scope.removeImageItem = function (item, index, removedItems) {

                //var parameters = {
                //    SmallImage: smallImage,
                //    LargeImage: largeImage
                //};
                //var config = {
                //    params: parameters
                //};

                //$http.get('/ServerRequest/GetData', config)
                //.success(function (data, status, headers, config) {
                //   alert(data)
                //})
                //.error(function (data, status, header, config) {

                //});

                removedItems.push({ ElementId: item.ElementId, smallImage: item.smallImage, largeImage: item.largeImage, location: item.location });
                $scope.Model._Active.Images.splice(index, 1);


                //Start base remove from storage
                //$http.post('/File/RemoveImage', { firstPath: item.smallImage, secondpath: item.largeImage, location: "/homeSiteImageGallery" })
                //.success(function (data, status, headers, config) {

                //    if (data.Status == true) {
                //        console.log($scope.Model._Active.Images);
                //        $scope.Model._Active.Images.splice(index, 1);
                //        console.log($scope.Model._Active.Images);
                //        //$modalInstance.close($scope.Model);
                //    } else {
                //        //$scope.MC.handleErrors($scope, data);
                //    }
                //})
                //End base remove from storage

            }
            // FILTERS

            uploader.filters.push({
                name: 'imageFilter',
                fn: function (item /*{File|FileLikeObject}*/, options) {
                    var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            });

            // CALLBACKS

            uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
                //console.info('onWhenAddingFileFailed', item, filter, options);
            };
            uploader.onAfterAddingFile = function (fileItem) {

                //console.info('onAfterAddingFile', fileItem);
            };
            uploader.onAfterAddingAll = function (addedFileItems) {
                //console.info('onAfterAddingAll', addedFileItems);
            };
            uploader.onBeforeUploadItem = function (item) {
                //console.info('onBeforeUploadItem', item);
            };
            uploader.onProgressItem = function (fileItem, progress) {
                //console.info('onProgressItem', fileItem, progress);
            };
            uploader.onProgressAll = function (progress) {
                //console.info('onProgressAll', progress);
            };
            uploader.onSuccessItem = function (fileItem, response, status, headers) {
                //console.info('onSuccessItem', fileItem, response, status, headers);
                //console.log($scope.Model._Active);
                var imgID = "JbPImage" + Math.round(Math.random() * 1000000);
                fileItem.remove();
                $scope.Model.newImages.push({ ElementId: imgID, smallImage: response.SmallImage, largeImage: response.LargeImage, location: "/homeSiteSlider" });
                $scope.Model._Active.Images.push({ ElementId: imgID, smallImage: response.SmallImage, largeImage: response.LargeImage, Title: null, location: "/homeSiteSlider" });
            };
            uploader.onErrorItem = function (fileItem, response, status, headers) {
                //console.info('onErrorItem', fileItem, response, status, headers);
            };
            uploader.onCancelItem = function (fileItem, response, status, headers) {
                //console.info('onCancelItem', fileItem, response, status, headers);
            };
            uploader.onCompleteItem = function (fileItem, response, status, headers) {
                //console.info('onCompleteItem', fileItem, response, status, headers);
            };
            uploader.onCompleteAll = function () {
                //console.info('onCompleteAll');
            };

          

        }])
        .controller('pageSettingsCtrl', function ($scope, $http, $modalInstance, Model, kendoImageUrl) {

            $scope.Model = Model;
            $scope.pageSettingEditorOptions = {


                tools: ['fontName', 'bold', 'italic', 'underline', 'strikethrough', 'fontSize', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'foreColor', 'insertUnorderedList', 'insertOrderedList', 'indent', 'outdent', 'createLink', 'unlink', 'insertImage', 'cleanFormatting', 'backColor'],

                imageBrowser: {
                    messages: {
                        dropFilesHere: "Drop files here"
                    },
                    transport: {
                        type: "imagebrowser-aspnetmvc",
                        read: {
                            url: "/KendoEditorFiles/ReadFiles?path=JbPageBuilder",
                            type: 'GET'
                        },
                        destroy: {
                            url: "/KendoEditorFiles/DestroyImage",
                            type: "POST"
                        },
                        uploadUrl: "/KendoEditorFiles/UploadImageWithThumbnail?path=JbPageBuilder",
                        thumbnailUrl: function (path, name) {
                            return kendoImageUrl.getProperty() + "Thumbnail/" + name;
                        },
                        imageUrl: function (name) {
                            return kendoImageUrl.getProperty() + name;
                        }
                    }
                }
            }


            //make a backup from Model
            var PageSettings_Setting = angular.copy($scope.Model.Setting);
            var PageSettings_Header = angular.copy($scope.Model.Header);
            var PageSettings_Footer = angular.copy($scope.Model.Footer);

            $scope.Save = function () {

                $http.post('/Season/UpdatePageSettings', { model: $scope.Model.Setting })
                    .success(function (data, status, headers, config) {

                        if (data.Status == true) {
                            $modalInstance.close($scope.Model);
                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    })
                //}

            };

            $scope.Cancel = function () {

                $scope.Model.Setting = PageSettings_Setting;
                $scope.Model.Header = PageSettings_Header;
                $scope.Model.Footer = PageSettings_Footer;
                $modalInstance.dismiss("Canceled");

            };

        })
        .controller('tabSettingsCtrl', function ($scope, $http, $modalInstance, Model, Tab) {

            $scope.defaultCovers = [
                {
                    full: "/Images/Covers/gray-banner.png",
                    thumb: "/Images/Covers/Thumbnail/gray-banner.png",
                },
                {
                    full: "/Images/Covers/all-sport-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/all-sport-cover.jpg",
                },
                {
                    full: "/Images/Covers/chess-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/chess-cover.jpg",
                },
                {
                    full: "/Images/Covers/class-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/class-cover.jpg",
                },
                {
                    full: "/Images/Covers/fencing-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/fencing-cover.jpg",
                },
                {
                    full: "/Images/Covers/kids-camp-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/kids-camp-cover.jpg",
                },
                {
                    full: "/Images/Covers/kids-ok-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/kids-ok-cover.jpg",
                },
                {
                    full: "/Images/Covers/kids-run-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/kids-run-cover.jpg",
                },
                {
                    full: "/Images/Covers/mixedmaterialarts-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/mixedmaterialarts-cover.jpg",
                },
                {
                    full: "/Images/Covers/school-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/school-cover.jpg",
                },
                {
                    full: "/Images/Covers/swimming-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/swimming-cover.jpg",
                },
                {
                    full: "/Images/Covers/tabletennis-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/tabletennis-cover.jpg",
                },
                {
                    full: "/Images/Covers/wrestling-cover.jpg",
                    thumb: "/Images/Covers/Thumbnail/wrestling-cover.jpg",
                }
            ];

            $scope.IsValidImageUrl = function (url) {
                $("#preview-external-image").attr("src", "Content/Frameworks/KendoUI/Uniform/loading-image.gif");

                if (url.trim()) {
                    $("<img>", {
                        src: url,
                        error: function () {
                            Tab.IsValidImageUrl = false;
                            $("#preview-external-image").attr("src", "");
                        },
                        load: function () {
                            Tab.IsValidImageUrl = true;
                            $("#preview-external-image").attr("src", url);
                        }
                    });
                } else {
                    $("#preview-external-image").attr("src", "");
                    Tab.IsValidImageUrl = true;
                }
            }

            $scope.Model = Model;
            $scope.Tab = Tab;

            if (Tab.Cover != undefined) {
                if (Tab.Cover.BackgroundMode == 'Url') {
                    Tab.Cover.BackgroundUrl = Tab.Cover.Background;
                    $scope.IsValidImageUrl(Tab.Cover.BackgroundUrl);
                } else {
                    Tab.Cover.BackgroundUrl = "";
                }
            }

            //make a copy from current cover settings
            var _TabCoverSettings = angular.copy($scope.Tab.Cover);

            $scope.isColorCode = function (colorCode) {
                if (colorCode && colorCode != undefined && colorCode.indexOf("rgba") >= 0) {
                    return true;
                }

                return false;
            }
      
            $scope.Save = function (tab) {

                if (tab.Cover.BackgroundMode == "Upload") {
                    var console = window.console || {
                        log: function () {
                        }
                    };
                    var $image = $('#image');
                    var $download = $('#download');
                    var $dataX = $('#dataX');
                    var $dataY = $('#dataY');
                    var $dataHeight = $('#dataHeight');
                    var $dataWidth = $('#dataWidth');
                    var $dataRotate = $('#dataRotate');
                    var $dataScaleX = $('#dataScaleX');
                    var $dataScaleY = $('#dataScaleY');
                    var options = {
                        aspectRatio: 3.72 / 1,
                        preview: '.img-preview',
                        crop: function (e) {
                            $dataX.val(Math.round(e.x));
                            $dataY.val(Math.round(e.y));
                            $dataHeight.val(Math.round(e.height));
                            $dataWidth.val(Math.round(e.width));
                            $dataRotate.val(e.rotate);
                            $dataScaleX.val(e.scaleX);
                            $dataScaleY.val(e.scaleY);
                        }
                    };

                    // Tooltip
                    $('[data-toggle="tooltip"]').tooltip();

                    // Methods
                    var $this = $(this);
                    var data = $this.data();
                    var $target;
                    var result;

                    data.method = "getCroppedCanvas";

                    if ($image.data('cropper') && data.method) {
                        data = $.extend({}, data); // Clone a new one

                        if (typeof data.target !== 'undefined') {
                            $target = $(data.target);

                            if (typeof data.option === 'undefined') {
                                try {
                                    data.option = JSON.parse($target.val());
                                } catch (e) {
                                    console.log(e.message);
                                }
                            }
                        }
                        data.option = {
                            "width": 1600,
                            "height": 430
                        };

                        result = $image.cropper(data.method, data.option, data.secondOption);

                        var croppedBlob = null;

                        if (result.id == "canvasCroppedImg") {
                            switch (data.method) {
                                case 'getCroppedCanvas':
                                    if (result) {

                                        // Bootstrap's Modal
                                        $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                                        croppedBlob = result.toDataURL('image/jpeg');

                                        $(".modal-backdrop").remove();
                                    }

                                    break;
                            }

                            if ($.isPlainObject(result) && $target) {
                                try {
                                    $target.val(JSON.stringify(result));
                                } catch (e) {
                                    console.log(e.message);
                                }
                            }

                        }
                    }
                }

                var index = $scope.Model.Elements.indexOf(Tab);
                $scope.Model.Elements[index].Cover.Visible = false;
             
                switch (tab.Cover.BackgroundMode) {
                    case "Url":
                        $scope.Model.Elements[index].Cover.Background = tab.Cover.BackgroundUrl;
                        $scope.Model.Elements[index].Cover.Visible = tab.Cover.BackgroundUrl ? true : false;
                        break;

                    case "Solid":
                        $scope.Model.Elements[index].Cover.Visible = tab.Cover.Background ? true : false;
                        break;

                    case "None":
                        $scope.Model.Elements[index].Cover.Visible = false;
                        $scope.Model.Elements[index].Cover.Background = "";
                        $scope.Model.Elements[index].Cover.BackgroundUrl = "";
                        break;

                    default:
                        $scope.Model.Elements[index].Cover.BackgroundUrl = "";
                        $scope.Model.Elements[index].Cover.Background = tab.Cover.Background;
                }

                var index = $scope.Model.Elements.indexOf(tab);

                //if ($scope.Model.Elements[index].Cover.BackgroundMode != "None" || ($scope.Model.Elements[index].Cover.Paragraph != undefined && $scope.Model.Elements[index].Cover.Paragraph.trim())) {
                if ($scope.Model.Elements[index].Cover.BackgroundMode != "None" || ($scope.Model.Elements[index].Cover.Title != undefined && $scope.Model.Elements[index].Cover.Title.trim()) || ($scope.Model.Elements[index].Cover.Subtitle != undefined && $scope.Model.Elements[index].Cover.Subtitle.trim()) || ($scope.Model.Elements[index].Cover.Paragraph != undefined && $scope.Model.Elements[index].Cover.Paragraph.trim())) {
                    $scope.Model.Elements[index].Cover.Visible = true;
                }

                if (croppedBlob) {
                    $http.post(SeasonUrl + '/UploadCover', { coverImage: croppedBlob, pageId: Model.Id })
                        .success(function (data, status, headers, config) {

                            if (data == "Exceed") {
                                $scope.status = "Maximum file size exceeded";
                                $("#statusBar").addClass("error");
                                console.log("ERROR: " + data);
                            } else {
                                $scope.Model.Elements[index].Cover.Background = data;
                                $scope.Model.Elements[index].Cover.Visible = true;

                                if (data.Status == true) {

                                    $scope.errors = null;
                                } else {
                                }
                            }

                        })

                } else {
                    //$scope.Model.Elements[index].Cover.Background = "";
                    //$scope.status = "upload failed...";
                    //console.log("upload failed:" + data);
                }

                //$scope.Model.Elements[index].Cover.Visible = tab.Cover.visible;
                $modalInstance.close($scope.Model);

            };

            $scope.Cancel = function (tab) {

                var index = $scope.Model.Elements.indexOf(tab);

                if (is.not.undefined($scope.Model.Elements[index])) {
                    $scope.Model.Elements[index].Cover = _TabCoverSettings;
                }

                $modalInstance.dismiss("Canceled");
            };

        })
        .controller('tabBaseSettingsCtrl', function ($scope, $http, $modalInstance, Model, Tab, kendoImageUrl) {

            $scope.Model = Model;
            $scope.Tab = Tab;


            //set default values for color settings
            if (!Tab.Appearance.TabBackgroundColor) {
                Tab.Appearance.TabBackgroundColor = "#fff";
            }
            if (!Tab.Appearance.TabTextColor) {
                Tab.Appearance.TabTextColor = "#01a3f1";
            }
            if (!Tab.Appearance.TabHoverTextColor) {
                Tab.Appearance.TabHoverTextColor = "#333";
            }

            if (!Tab.Appearance.TabBorderColor) {
                Tab.Appearance.TabBorderColor = "#01a3f1";
            }


            if (!Tab.Appearance.TabSelectedBackgroundColor) {
                Tab.Appearance.TabSelectedBackgroundColor = "#01a3f1";
            }
            if (!Tab.Appearance.TabSelectedHoverTextColor) {
                Tab.Appearance.TabSelectedHoverTextColor = "#eee";
            }
            if (!Tab.Appearance.TabSelectedTextColor) {
                Tab.Appearance.TabSelectedTextColor = "#fff";
            }
            if (!Tab.Appearance.TabSelectedBorderColor) {
                Tab.Appearance.TabSelectedBorderColor = "#01a3f1";
            }

            $scope.resetTabColors = function () {
                Tab.Appearance.TabBackgroundColor = "#fff";
                Tab.Appearance.TabTextColor = "#01a3f1";
                Tab.Appearance.TabHoverTextColor = "#333";
                Tab.Appearance.TabBorderColor = "#01a3f1";
                Tab.Appearance.TabSelectedBackgroundColor = "#01a3f1";
                Tab.Appearance.TabSelectedHoverTextColor = "#eee";
                Tab.Appearance.TabSelectedTextColor = "#fff";
                Tab.Appearance.TabSelectedBorderColor = "#01a3f1";
            }

            //make a copy from current Appearance settings
            var _TabAppearanceSettings = angular.copy($scope.Tab.Appearance);

            $scope.Save = function (tab) {
                var index = $scope.Model.Elements.indexOf(tab);

                $modalInstance.close($scope.Model);

            };

            $scope.Cancel = function (tab) {
                var index = $scope.Model.Elements.indexOf(tab);

                if (is.not.undefined($scope.Model.Elements[index])) {
                    $scope.Model.Elements[index].Appearance = _TabAppearanceSettings;
                }

                $modalInstance.dismiss("Canceled");
            };

        })
        .controller('bannerTextCtrl', function ($scope, $http, $modalInstance, Model, Tab, kendoImageUrl) {

            $scope.Model = Model;
            $scope.Tab = Tab;

            $scope.pageSettingBannerOptions = {


                tools: ['fontName', 'bold', 'italic', 'underline', 'strikethrough', 'fontSize', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'foreColor', 'insertUnorderedList', 'insertOrderedList', 'indent', 'outdent', 'createLink', 'unlink', 'insertImage', 'cleanFormatting', 'backColor'],

                imageBrowser: {
                    messages: {
                        dropFilesHere: "Drop files here"
                    },
                    transport: {
                        type: "imagebrowser-aspnetmvc",
                        read: {
                            url: "/KendoEditorFiles/ReadFiles?path=JbPageBuilder",
                            type: 'GET'
                        },
                        destroy: {
                            url: "/KendoEditorFiles/DestroyImage",
                            type: "POST"
                        },
                        uploadUrl: "/KendoEditorFiles/UploadImageWithThumbnail?path=JbPageBuilder",
                        thumbnailUrl: function (path, name) {
                            return kendoImageUrl.getProperty() + "Thumbnail/" + name;
                        },
                        imageUrl: function (name) {
                            return kendoImageUrl.getProperty() + name;
                        }
                    }
                }
            }
            //make a copy from current cover settings
            var _TabCoverSettings = angular.copy($scope.Tab.Cover);

            $scope.Save = function (tab) {

                var index = $scope.Model.Elements.indexOf(tab);
                //var visible = tab.Cover.Visible;
                //if ($scope.Model.Elements[index].Cover.BackgroundMode != "None" || ($scope.Model.Elements[index].Cover.Paragraph != undefined && $scope.Model.Elements[index].Cover.Paragraph.trim())) {
                if ($scope.Model.Elements[index].Cover.BackgroundMode != "None" || ($scope.Model.Elements[index].Cover.Title != undefined && $scope.Model.Elements[index].Cover.Title.trim()) || ($scope.Model.Elements[index].Cover.Subtitle != undefined && $scope.Model.Elements[index].Cover.Subtitle.trim()) || ($scope.Model.Elements[index].Cover.Paragraph != undefined && $scope.Model.Elements[index].Cover.Paragraph.trim())) {
                    $scope.Model.Elements[index].Cover.Visible = true;
                } else {
                    $scope.Model.Elements[index].Cover.Visible = false;
                }

                $modalInstance.close($scope.Model);

            };

            $scope.Cancel = function (tab) {

                var index = $scope.Model.Elements.indexOf(tab);

                if (is.not.undefined($scope.Model.Elements[index])) {
                    $scope.Model.Elements[index].Cover = _TabCoverSettings;
                }

                $modalInstance.dismiss("Canceled");
            };

        })
        .controller('exportCtrl', function ($scope, $modalInstance, Model, Tabs) {
            $scope.Model = Model;
            $scope.Tabs = Tabs;

            $scope.iframeUrl = window.location.origin;
            $scope.selectIframe = function () {
                $("#exportIframeContents").select();
            }

            $scope.Save = function () {
                $modalInstance.close($scope.Model);
            };

            $scope.Cancel = function () {
                $modalInstance.dismiss("Canceled");
            };

            $scope.convertToInt = function (data) {
                $scope.$watch('_export.iframe.availableTabs', function (value, key) {

                    for (var i = 0; i < $scope._export.iframe.availableTabs.length; i++) {
                        $scope._export.iframe.availableTabs[i] = parseInt($scope._export.iframe.availableTabs[i]);
                    }
                })

            }

        })
        .controller('activeElementCtrl', function ($scope, $modalInstance, Model, Programs, Seasons, Parent, $filter, $http, SeasonsDomain, BeforeAfterCarePrograms, kendoImageUrl) {

            $scope.Model = Model;
            var copyModel = angular.copy($scope.Model._Active);
            $scope.Programs = Programs;
            $scope.Seasons = Seasons;
            if (undefined != SeasonsDomain) {
                $scope.SeasonsDomain = SeasonsDomain;
            }
            if (undefined != BeforeAfterCarePrograms) {
                $scope.BeforeAfterCarePrograms = BeforeAfterCarePrograms;
            }
            //show actve json
            //console.log($scope.Model);
            $scope.Parent = Parent;

            $scope.onE = function (item) {
                $scope.Model._Active.Paragraph = $scope.Model._Active.Paragraph.replace(/\"panel-collapse collapse"/g, '"panel-collapse collapse in"');
            }
      
            $scope.contactSettingEditorOptions = {


                tools: ['fontName', 'bold', 'italic', 'underline', 'strikethrough', 'fontSize', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'foreColor', 'createTable', 'addRowAbove', 'addRowBelow', 'addColumnLeft', 'addColumnRight', 'deleteRow', 'insertImage', 'deleteColumn', 'insertUnorderedList', 'insertOrderedList', 'indent', 'outdent', 'createLink', 'unlink', 'cleanFormatting', 'backColor', 'viewHtml', 'formatting'],
                imageBrowser: {
                    messages: {
                        dropFilesHere: "Drop files here"
                    },
                    transport: {
                        type: "imagebrowser-aspnetmvc",
                        read: {
                            url: "/KendoEditorFiles/ReadFiles?path=JbPageBuilder",
                            type: 'GET'
                        },
                        destroy: {
                            url: "/KendoEditorFiles/DestroyImage",
                            type: "POST"
                        },
                        uploadUrl: "/KendoEditorFiles/UploadImageWithThumbnail?path=JbPageBuilder",
                        thumbnailUrl: function (path, name) {
                            return kendoImageUrl.getProperty() + "Thumbnail/" + name;
                        },
                        imageUrl: function (name) {
                            return kendoImageUrl.getProperty() + name;
                        }
                    }
                }
            }
            var imageUrl = "";
            $scope.editorOptions = {
                stylesheets: [
                    "/Content/Frameworks/Bootstrap/bootstrap.css",
                    "/Content/Frameworks/MetroUi/metro-bootstrap.min.css",
                    "/Content/Frameworks/KendoUI/kendo.common.min.css",
                    "/Content/Frameworks/KendoUI/kendo.default.min.css",
                    "/Content/Frameworks/KendoUI/kendo.metro.min.css",
                    "/Content/Frameworks/Bootstrap/bootstrap-custom.css",
                    "/Content/Student/Season/Style.css"
                ],

                //resizable: true,
                execute: function (e) {
                    if (e.name == "_createtable") {
                        var win = $("<div id='editorTableWindow' style='display:none'>" +
                            "<label for='editorTableWidth'>Set table border</label> " +
                            "<input id='editorTableWidth' /> " +
                            "<button class='k-button' id='editorTableButton'>Apply</button> " +
                            "</div>'")
                            .appendTo(document.body)
                            .kendoWindow({
                                title: "Set table properties",
                                width: 600,
                                visible: false,
                                modal: true,
                                close: function (e) {
                                    var tableWidth = $("#editorTableWidth").data("kendoNumericTextBox").value();
                                    //var table = $($("#editor").data("kendoEditor").body).find("table:not(.manipulated)");
                                    var editor = $("#htmleditor").data("kendoEditor");
                                    var table = $(editor.body).find("table:not(.manipulated)");
                                    table.width(tableWidth).addClass("manipulated");
                                },
                                deactivate: function (e) { e.sender.destroy(); }
                            }).data("kendoWindow");
                        $("#editorTableWidth").kendoNumericTextBox({ min: 1, decimals: 0 });
                        win.center().open();
                        $("#editorTableButton").click(function (e) {
                            win.close();
                        });
                    }
                },

                keydown: function (e) {
                    //it must run for remove chars from editor (DELETE/BACKSPACE/REPLACE) not for add keys

                    var editor = $("#htmleditor").data("kendoEditor");
                    var initEditorRange = editor.getRange().endContainer;

                    //editor.update();
                    var acc = $("acc", editor.body);

                    var accElements = $(acc);

                    var parentElement = $(initEditorRange.parentNode.parentNode.parentNode).attr('id');

                    if (parentElement == undefined) {
                        parentElement = $(initEditorRange.parentNode.parentNode).attr('id');
                    }

                    var res = [];
                    if (parentElement) {
                        res = parentElement.split('_');
                    }

                    var accId = res[1];
                    var accElement = $(editor.body).find("#acc_" + accId);

                    var titleElement = $(accElement).find('.panel-title strong').text();
                    var bodyElement = $(accElement).find('.panel-body').text();

                    titleElement = titleElement.replace(/[^\x20-\x7E]/gmi, "");
                    bodyElement = bodyElement.replace(/[^\x20-\x7E]/gmi, "");

                    titleElement = titleElement.replace(/(\r\n|\n|\r|&nbsp;|<br>|<br \/>)/gm, "");
                    bodyElement = bodyElement.replace(/(\r\n|\n|\r|&nbsp;|<br>|<br \/>)/gm, "");

                    titleElement = titleElement.replace("&nbsp;", "");
                    bodyElement = bodyElement.replace("&nbsp;", "");

                    titleElement = titleElement.replace(/\u00a0/g, "");
                    bodyElement = bodyElement.replace(/\u00a0/g, "");

                    //remove acc element if has no text
                    if (titleElement != undefined && !titleElement.trim() && bodyElement != undefined && !bodyElement.trim()) {
                        //console.log('remove element');
                        $(accElement).remove();
                    }

                },
                tools: [
                    'fontName', 'bold', 'italic', 'underline', 'strikethrough', 'fontSize', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', 'foreColor', 'createTable', 'addRowAbove', 'addRowBelow', 'addColumnLeft', 'addColumnRight', 'deleteRow', 'insertImage', 'deleteColumn', 'insertUnorderedList', 'insertOrderedList', 'indent', 'outdent', 'createLink', 'unlink', 'cleanFormatting', 'backColor', 'viewHtml', 'formatting',
                    {
                        name: "Accordion",
                        tooltip: "Insert collapsible item",
                        template: "<a id=\"kendo-editor-custom-tool\" href=\"\" unselectable=\"on\" role=\"button\" class=\"k-tool k-group-start collapsable gray-color\" title=\"Insert collapsible item\"><span unselectable=\"on\" class=\"jbi-accordion-menu\"></span></a>",
                        exec: function (e) {
                            var editor = $(this).data("kendoEditor");
                                                   
                            var popupHtml =
                                '<div class="k-editor-dialog k-popup-edit-form k-edit-form-container k-window-content k-content" data-role="window" tabindex="0">' +
                                '<div class="k-edit-label">' +
                                '<label for="accordion-title">Title</label>' +
                                '</div>' +
                                '<div class="k-edit-field">' +
                                '<input id="accordion-title" type="text" class="k-input k-textbox"></input>' +
                                '</div>' +
                                '<div class="k-edit-label"><label for="accordion-text">Text</label></div>' +
                                '<div class="k-edit-field">' +
                                '<textarea id="accordion-text" type="text" cols="38" rows="5"></textarea>' +
                                '</div>' +
                                '<div class="k-edit-buttons k-state-default">' +
                                '<button class="k-dialog-insert k-button k-primary">Insert</button>' +
                                '<button class="k-dialog-close k-button">Cancel</button>' +
                                '</div>' +
                                '</div>';

                            var popupWindow = $(popupHtml)
                                .appendTo(document.body)
                                .kendoWindow({
                                    // modality is recommended in this scenario
                                    modal: true,
                                    //width: 450,
                                    resizable: false,
                                    title: "Insert collapsible item",
                                    // ensure opening animation
                                    visible: false,
                                    // remove the Window from the DOM after closing animation is finished
                                    deactivate: function (e) {
                                        e.sender.destroy();
                                    }
                                }).data("kendoWindow")
                                .center().open();

                            // insert the new content in the Editor when the Insert button is clicked
                            popupWindow.element.find(".k-dialog-insert").click(function () {
                                var Title = popupWindow.element.find("#accordion-title").val();
                                var Text = popupWindow.element.find("#accordion-text").val();
                                var time = new Date().valueOf();
                                var customHtml = '<acc  id="acc_' + time + '"><div  class="accordionFAQ panel-group" id="accordion_' + time + '"><div id="panel_' + time + '" class="panel panel-default"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion_' + time + '" href="#collapse_' + time + '" id="href_' + time + '"><strong>' + Title + '</strong></a></h4></div><div id="collapse_' + time + '" class="panel-collapse collapse in"><div class="panel-body" id="body_' + time + '">' + Text + '</div></div></div></div></acc><p>&nbsp;</p>';
                                editor.exec("inserthtml", {
                                    value: customHtml
                                });
                            });
                            // close the Window when any button is clicked
                            popupWindow.element.find(".k-edit-buttons button").click(function () {
                                // detach custom event handlers to prevent memory leaks
                                popupWindow.element.find(".k-edit-buttons button").off();
                                popupWindow.close();
                            });
                        },
                    },
                    {
                        name: "Manage collapsible items",
                        tooltip: "Manage collapsible items",
                        template: "<a id=\"kendo-editor-custom-tool\" href=\"\" unselectable=\"on\" role=\"button\" class=\"k-tool k-group-start collapsable gray-color\" title=\"Manage collapsible items\"><span unselectable=\"on\" class=\"jbi-manage-collapsible\" style='font-size: 22px; vertical-align: middle;'></span></a>",
                        exec: function (e) {
                            var editor = $(this).data("kendoEditor");
                     
                            var popupHtml =
                                '<div id="sortCollapsible" class="k-editor-dialog k-popup-edit-form k-edit-form-container k-window-content k-content" data-role="window" tabindex="0">' +
                                '<div class="editor-acc-list-container">' + editor.value() + '</div>' +
                                '<div id="editAccordionBox"><div  class="popup-form-group"><label>Title</label><input type="text" name="title" class="popup-form-control" /></div><div class="text-right"><button class="k-button k-primary" id="saveEditAcc" onclick="editorFaqSettings.saveEditAcc(this)">Save</button><button class="k-button" id="cancelEditAcc" style="margin-right: 0;" onclick="editorFaqSettings.cancelEditAcc(this)">Cancel</button></div></div>'+
                                '<p class="box-text-warning" style="margin:15px;display:none">You do not have any collapsible items to sort.</p >' +
                                '<div class="k-edit-buttons k-state-default">' +
                                '<button class="k-dialog-insert k-button k-primary" id="sortFaq">Save</button>' +
                                '<button class="k-dialog-close k-button">Cancel</button>' +
                                '</div>' +
                                '</div>';

                            var popupWindow = $(popupHtml)
                                .appendTo(document.body)
                                .kendoWindow({
                                    // modality is recommended in this scenario
                                    modal: true,
                                    //width: 450,
                                    resizable: false,
                                    title: "Manage collapsible items",
                                    // ensure opening animation
                                    visible: false,
                                    // remove the Window from the DOM after closing animation is finished
                                    deactivate: function (e) {
                                        e.sender.destroy();
                                    }
                                }).data("kendoWindow")
                                .center().open();                       
                            var popupBox = $("#sortCollapsible");
                            var accordionBox = $(".editor-acc-list-container", document);
                            var simpleAccordions = accordionBox.find('.accordionFAQ');
                            var accordions;
                            if (!(simpleAccordions.parent().is('acc'))) {
                                simpleAccordions.wrap('<acc></acc>');
                                var timeForId = "";
                                var accElement;
                                accordions = accordionBox.find("acc");
                                $.each(accordions, function (index, accItem) {
                                    timeForId = new Date().valueOf();
                                    accElement = $(accItem);
                                    accElement.attr('id', 'accordion_' + timeForId + index);
                                    accElement.find('a.accordion-toggle').attr('href', '#collapse_' + timeForId + index);
                                    accElement.find('a.accordion-toggle').attr('data-parent', 'accordion_' + timeForId + index);
                                    accElement.find('a.accordion-toggle').attr('id', 'href_' + timeForId + index);
                                    accElement.find('div.panel').attr('id', 'panel_' + timeForId + index);
                                    accElement.find('div.panel-collapse').attr('id', 'collapse_' + timeForId + index);
                                })
                            }

                            accordionBox.find("acc>br").remove();
                            accordionBox.find("acc:empty").remove();
                            accordions = accordionBox.find("acc");
                          
                            if (accordions.length == 0) {
                                popupBox.find(".box-text-warning").show();
                                accordionBox.hide();
                                popupBox.find('.k-dialog-insert').attr('disabled', 'disabled');
                            }
                            else {
                                accordionBox.show();
                                popupBox.find(".box-text-warning").hide();
                                popupBox.find('.k-dialog-insert').removeAttr('disabled');
                            }
                            accordions.append("<span class='btn-icon move-up-btn' onclick='editorFaqSettings.moveUp(this)'><i class='jbi-arrow-top'></i></span><span class='btn-icon move-down-btn' onclick='editorFaqSettings.moveDown(this)'><i class='jbi-arrow-bottom'></i></span><span class='btn-icon delete-acc-btn' onclick='editorFaqSettings.deleteAccItem(this)'><i class='jbi-delete-trash-1'></i></span><span class='btn-icon edit-acc-btn' onclick='editorFaqSettings.editAccItem(this)'><i class='jbi-edit-document'></i></span>")
                            accordionBox.find("acc").remove();
                            var otherTags = accordionBox.html();
                            accordionBox.html(accordions);
                            accordionBox.find("acc").addClass("accordion-item-sortable");

                            // insert the new content in the Editor when the Insert button is clicked
                            popupWindow.element.find(".k-dialog-insert").click(function () {
                                accordionBox.find(".btn-icon").remove();
                                accordionBox.find("acc").removeClass("accordion-item-sortable");
                                var getAccordions = accordionBox.html();
                                $scope.Model._Active.Paragraph = otherTags + getAccordions +"<p>&nbsp;</p>";
                                editor.value(otherTags + getAccordions+"<p>&nbsp;</p>")
                                editor.refresh();
                            });
                            // close the Window when any button is clicked
                            popupWindow.element.find(".k-edit-buttons button").click(function () {
                                // detach custom event handlers to prevent memory leaks
                                popupWindow.element.find(".k-edit-buttons button").off();
                                popupWindow.close();
                            });
                        },
                    },
                    {
                        name: "insertButton",
                        tooltip: "Insert button",
                        template: "<a id=\"kendo-editor-custom-tool\" href=\"\" unselectable=\"on\" role=\"button\" class=\"k-tool k-group-start collapsable gray-color\" title=\"Insert button\"><span unselectable=\"on\" class=\"jbi-plus\"></span></a>",
                        exec: function (e) {
                            var editor = $(this).data("kendoEditor");
                            // parser test
                            var text = "";
                            var href = "";
                            var btn = editor.selectionRestorePoint.range.startContainer.parentElement;
                            if (btn != null && btn.href != undefined) {
                                text = btn.innerText;
                                href = btn.href;
                            } else {
                                btn = null;
                            }
                            //
                            var popupHtml =
                                '<div class="k-editor-dialog k-popup-edit-form k-edit-form-container" style="width:auto;">' +
                                '<div style="padding: 0 1em;">' +
                                '<div class="k-edit-label"><label for="k-editor-link-url">Enter url</label></div>' +
                                '<div class="k-edit-field"><input type="text" class="k-input k-textbox" id="btnUrl" value="http://"></div>' +
                                '<div class="k-edit-label"><label for="k-editor-link-url">Enter button label</label></div>' +
                                '<div class="k-edit-field"><input type="text" class="k-input k-textbox" id="btnLabel"></div>' +
                                '</div>' +
                                '<div class="k-edit-buttons k-state-default">' +
                                '<button class="k-dialog-insert k-button k-primary">Insert</button>' +
                                '<button class="k-dialog-close k-button">Cancel</button>' +
                                '</div>' +
                                '</div>';

                            var popupWindow = $(popupHtml)
                                .appendTo(document.body)
                                .kendoWindow({
                                    // modality is recommended in this scenario
                                    modal: true,
                                    width: 600,
                                    resizable: false,
                                    title: "Insert button",
                                    // ensure opening animation
                                    visible: false,
                                    // remove the Window from the DOM after closing animation is finished
                                    deactivate: function (e) {
                                        e.sender.destroy();
                                    }
                                }).data("kendoWindow")
                                .center().open();

                            if (btn != null && btn != undefined) {
                                popupWindow.element.find("#btnUrl").val(href);
                                popupWindow.element.find("#btnLabel").val(text);
                            }

                            // insert the new content in the Editor when the Insert button is clicked
                            popupWindow.element.find(".k-dialog-insert").click(function () {
                                var label = popupWindow.element.find("#btnLabel").val();
                                var url = popupWindow.element.find("#btnUrl").val();

                                if (btn != null) {
                                    btn.innerText = label;
                                    btn.href = url;
                                } else {
                                    var customHtml = '<a href="' + url + '" class="btn btn-default" style="background-color: #008287;text-transform: none!Important;">' + label + ' </a>';
                                    editor.exec("inserthtml", { value: customHtml });
                                }
                            });
                            // close the Window when any button is clicked
                            popupWindow.element.find(".k-edit-buttons button").click(function () {
                                // detach custom event handlers to prevent memory leaks
                                popupWindow.element.find(".k-edit-buttons button").off();
                                popupWindow.close();
                            });
                        },
                    },

                    {
                        //name: "insertLineBreak",
                        //enter: false
                    },
                    {
                        //name: "insertLineBreak",
                        //shift: false
                    },
                    {
                        //name: "insertParagraph",
                        //shift: true
                    },
                    {
                        //name: "insertParagraph", enter: false 
                    },
                ],

                imageBrowser: {
                    messages: {
                        dropFilesHere: "Drop files here"
                    },
                    transport: {
                        type: "imagebrowser-aspnetmvc",
                        read: {
                            url: "/KendoEditorFiles/ReadFiles?path=JbPageBuilder",
                            type: 'GET'
                        },
                        destroy: {
                            url: "/KendoEditorFiles/DestroyImage",
                            type: "POST"
                        },
                        uploadUrl: "/KendoEditorFiles/UploadImageWithThumbnail?path=JbPageBuilder",
                        thumbnailUrl: function (path, name) {
                            return kendoImageUrl.getProperty() + "Thumbnail/" + name;
                        },
                        imageUrl: function (name) {
                            return kendoImageUrl.getProperty() + name;
                        }
                    }
                }
            }
            $scope.saveWithType = function (elemType) {
                if (elemType == 'video') {
                    if (document.querySelector('video')) {
                        document.querySelector('video').load();
                    }
                }

                $scope.removeCollapseIn();
                $scope.Model._Active._IsInAddProcess = false;
                $modalInstance.close($scope.Model);
            };
            $scope.refreshSeasonGrid = function (isAddProcess, gridActive) {
                var elmId = gridActive.ElementId;
                var gridElement = gridActive;
                //get season domain for use on register btn season
                if ($scope.SeasonsDomain) {
                    for (var i = 0; i < $scope.SeasonsDomain.length; i++) {
                        if ($scope.SeasonsDomain[i].Key == gridActive.SeasonId) {
                            gridActive.SeasonDomain = $scope.SeasonsDomain[i].Value;
                            break;
                        }
                    }
                }

                if (!isAddProcess) {

                    if (gridActive.GridLayoutType === 'Weekly') {
                        $(".tab-pane.active #Grid_" + elmId + "[data-gridtracker]").data("kendoGrid").dataSource.group([{
                            field: "IntDayOfWeek", 
                        }]);
                        $(".tab-pane.active #Grid_" + elmId + "[data-gridtracker]").data("kendoGrid").dataSource.read();
                    }
                    else if (gridActive.GridLayoutType === 'List') {
                        $(".tab-pane.active #Grid_" + elmId + "[data-gridtracker]").data("kendoGrid").dataSource.group([]);
                        $(".tab-pane.active #Grid_" + elmId + "[data-gridtracker]").data("kendoGrid").dataSource.read();
                    }

                    if (gridActive.GridLayoutType === 'Agenda') {
                        if (gridActive.ScheduleStartDate) {
                            $(".tab-pane.active #Sched_" + elmId).data("kendoScheduler").date(new Date(gridActive.ScheduleStartDate));
                        }

                        $(".tab-pane.active #Sched_" + elmId).data("kendoScheduler").dataSource.read();
                        $(".tab-pane.active #Sched_" + elmId).data("kendoScheduler").refresh();
                    }
                    if (gridActive.GridLayoutType === 'Calendar') {
                        if (gridActive.ScheduleStartDate) {
                            $(".tab-pane.active #Sched_" + elmId).data("kendoScheduler").date(new Date(gridActive.ScheduleStartDate));
                        }
                        $(".tab-pane.active #Sched_" + elmId).data("kendoScheduler").dataSource.read();
                        $(".tab-pane.active #Sched_" + elmId).data("kendoScheduler").refresh();
                    }

                }


            }

            $scope.Save = function () {
                if (arguments.length == 0) {
                    $scope.removeCollapseIn();
                    $scope.Model._Active._IsInAddProcess = false;
                    $modalInstance.close($scope.Model);
                }
                else if (arguments.length == 1 && arguments[0]) {
                    if (arguments[0] == 'BeforeAfterCareSave') {


                        $.each($scope.BeforeAfterCarePrograms, function (index, value) {
                            if (value.BeforeAfterProgramId == $scope.Model._Active.BeforeAfterProgramId) {
                                $scope.Model._Active.Schedules = value.Schedules;
                                $scope.Model._Active.DropInInformation = value.DropInInformation;
                                $scope.Model._Active.PunchcardInformation = value.PunchcardInformation;
                                $scope.Model._Active.PaymentPriod = value.PaymentPriod;
                                $scope.Model._Active.Description = value.Description;
                                $scope.Model._Active.MinGrade = value.MinGrade;
                                $scope.Model._Active.MaxGrade = value.MaxGrade;
                                $scope.Model._Active.LinkUrl = value.LinkUrl;
                                $scope.Model._Active.TypeTitle = value.TypeTitle;
                                $scope.Model._Active.Title = value.Title;
                                $scope.Model._Active.Type = value.Type;
                                $scope.Model._Active.IsEnabeldDropIn = value.IsEnabeldDropIn;
                                $scope.Model._Active.IsEnabeldPunchcard = value.IsEnabeldPunchcard;
                            }
                        })
                        $scope.removeCollapseIn();
                        $scope.Model._Active._IsInAddProcess = false;
                        $modalInstance.close($scope.Model);
                    }
                }
            };

            $scope.Cancel = function () {
              
                $scope.removeCollapseIn();

                $scope.Model._Active._IsInAddProcess = false;
                $modalInstance.dismiss("Canceled");
            };
            $scope.CancelSeasonGrid = function () {

                $.each($scope.Model._Active,
                    function(index, value) {
                        $scope.Model._Active[index] = copyModel[index];
                    });
                $scope.removeCollapseIn();
                $scope.Model._Active._IsInAddProcess = false;
                $modalInstance.dismiss("Canceled");
            };
            $scope.removeCollapseIn = function () {
                if (undefined != $scope.Model._Active.Paragraph) {
                    $scope.Model._Active.Paragraph = $scope.Model._Active.Paragraph.replace(/\"panel-collapse collapse in"/g, '"panel-collapse collapse"');
                }
            }
            //$scope.selectOptions = {
            //    placeholder: "Select products...",
            //    dataTextField: "ProductName",
            //    dataValueField: "ProductID",
            //    ddataSource: $scope.SelectedPrograms,
            //    filter: "contains"
            //    }
            //$scope.Model._Active._season = { 'Key': 10010,'Value': '635' };

            if (undefined != $scope.Seasons.SelectedItem) {

                var item = $scope.Seasons.SelectedItem;
                delete $scope.Seasons.SelectedItem;

                for (var i = 0; i < $scope.Seasons.length; i++) {
                    if ($scope.Seasons[i].Key == item.SeasonId) {
                        var selectedIndex = i;
                        break;
                    }
                }
                if (undefined != $scope.Seasons[selectedIndex]) {

                    $scope.Model._Active._season = $scope.Seasons[selectedIndex];
                }
            }

            $scope.selectedIds = [
                {
                    ProductName: "Chai",
                    ProductID: 1
                }
            ];

            $scope.setChanges = function (season) {
                if (season != undefined) {
                    var sId = season.Key;
                    if (!is.undefined(sId) && sId) {

                        $.each($scope.Seasons, function (index, value) {
                            if (value.SeasonId == sId) {
                                $scope.Seasons.SelectedItem = sId;
                            }
                        })
                    }
                }
            }

            //@Mo return programs for selected season
            $scope.up = function (seasonId) {
                if (seasonId != undefined) {
                    var sId = seasonId.Key;
                    if (!is.undefined(sId) && sId) {

                        $.each($scope.Programs, function (index, value) {
                            if (value.Id == sId) {
                                $scope.SelectedPrograms = value.Programs;
                                //if (!$scope.Model._Active._programs) {
                                //  $scope.Model._Active.Title = "";
                                //}
                            }
                        })
                    }
                }
            }


            $scope.resetFilterColors = function () {
                if (arguments.length == 0) {
                    //reset colors for filter box season grid setting
                    Model._Active.Filter.Setting.TextColor = "#fff";
                    Model._Active.Filter.Setting.BackgroundColor = "#16a085"
                    Model._Active.Filter.Setting.SubBackgroundColor = "#09836b"
                    Model._Active.Filter.Setting.ActiveBackgroundColor = "#105245"
                }
                else if (arguments.length == 1) {
                    if (arguments[0] === "BeforeAfterCareProgram") {
                        Model._Active.ColorSetting.ButtonBackgroundColor = "#1F84A8";
                        Model._Active.ColorSetting.TitleColor = "#1F84A8";
                        Model._Active.ColorSetting.TimeColor = "#535087";
                        Model._Active.ColorSetting.HeaderBackgroundColor = "#D5DF26";
                        Model._Active.ColorSetting.ButtonTextColor = "#fff";
                    }
                }

            }

            if (undefined != $scope.Programs.SelectedItem) {
                var item = $scope.Programs.SelectedItem;
                delete $scope.Programs.SelectedItem;

                for (var i = 0; i < $scope.Programs.length; i++) {
                    if ($scope.Programs[i].ProgramId == item.ProgramId) {
                        var selectedIndex = i;

                        break;
                    }
                }
                if (undefined != $scope.Programs[selectedIndex]) {
                    $scope.Model._Active._programs = $scope.Programs[selectedIndex].ProgramId;
                }
            }
            //@Mo return programs for selected season in edit mode
            $scope.upEdit = function (seasonId, first) {
                if (seasonId != undefined) {
                    var sId = seasonId.Key;
                    if (!is.undefined(sId) && sId) {

                        $.each($scope.Programs.seasonPrograms, function (index, value) {
                            if (value.Id == sId) {
                                $scope.SelectedPrograms = value.Programs;
                                console.log($scope.SelectedPrograms)
                                //$scope.Model._Active.Title = "";
                                if (!first) {
                                    $scope.Model._Active.Title = "";
                                }
                            }
                        })
                    }
                } else {
                    $scope.SelectedPrograms = {

                    };
                }

            }

            $scope.setTitle = function (item) {
                $.each($scope.SelectedPrograms, function (index, value) {
                    if (value.ProgramId == item) {
                        $scope.Model._Active.Title = value.Title;
                    }
                })
            }

            $scope.changeSeason = function (sId) {
                $scope.SelectedBeforeAfterPrograms = []
                //console.log(sId);
                //beforeAfterSeasonId.Key
                if (sId != undefined) {

                    if (!is.undefined(sId) && sId) {
                        $.each($scope.Programs, function (index, value) {

                            if (value.SeasonId == sId) {
                                $scope.SelectedBeforeAfterPrograms.push(value);
                            }
                        })

                    }
                }
            }

        }).filter('unsafe', [
            '$sce', function ($sce) {
                return function (val) {
                    return $sce.trustAsHtml(val);
                };
            }
        ])
        .filter("trustUrl", function ($sce) {
            return function (Url) {
                return $sce.trustAsResourceUrl(Url);
            };
        })
        .filter('GetYouTubeIDJB', ['$sce', function ($sce) {
            return function (url) {


                var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                var match = url.match(regExp);

                if (match && match[2].length == 11) {
                    return $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + match[2]);
                }
                else if (match && match[2].length != 11) {
                    return $sce.trustAsResourceUrl("https://www.youtube.com/embed/");
                }
                else if (url.length == 11) {
                    return $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + url);
                }
                else {
                    return $sce.trustAsResourceUrl("https://www.youtube.com/embed/");
                }
            };
        }])
        .directive('html5vfix', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    attr.$set('src', attr.vsrc);
                }
            }
        })
        .directive('youtubeHelp', function () {
            return {
                restrict: 'AE',
                scope: {
                    header: '@',
                    video: '@'
                },
                transclude: true,
                replace: true,
                template: '<div class="well"><iframe class="home-site-video" ng-src="{{video | GetYouTubeIDJB }}"></iframe></div>',
                link: function (scope, element, attrs) {
                    scope.header = attrs.header;
                }
            };
        })
        .directive('videohtml', function () {
            return {
                restrict: 'AE',
                scope: {
                    header: '@',
                    video: '@'
                },
                transclude: true,
                replace: true,
                template: '<video width="100%"  controls="controls" class="home-site-video" preload="auto"><source preload="auto" ng-src="{{video | trustUrl}}"  type="video/mp4">Your browser does not support HTML5 video.</video>',
                link: function (scope, element, attrs) {
                    scope.header = attrs.header;
                }
            };

        })

        .directive('dynamicUrl', function () {
            return {
                restrict: 'A',
                link: function postLink(scope, element, attr) {

                    element.attr('src', attr.dynamicUrlSrc);


                }
            };

        })
        .directive('ngEnter', function () {
            return function (scope, element, attrs) {
                var bound = element.bind("keydown keypress blur", function (event) {
                    if (event.which === 13 || event.type === "blur") {
                        scope.$apply(function () {
                            scope.$eval(attrs.ngEnter, {
                                'event': event
                            });

                        });

                        //element.unbind('keydown keypress blur');
                        bound;

                        event.preventDefault();
                    }
                });
            };
        })
        .directive('escKey', function () {
            return function (scope, element, attrs) {
                element.bind('keydown keypress', function (event) {
                    if (event.which === 27) { // 27 = esc key
                        scope.$apply(function () {
                            scope.$eval(attrs.escKey);
                        });

                        event.preventDefault();
                    }
                });
            };
        })
        .directive('setHeight', function () {
            return {
                restrict: 'A',
                scope: {
                  
                },
                link: function (scope, element, attrs) {  
                    var elementHeight = 0,
                        baseImage = new Image(),
                        chosenWidth = 0,
                        originalWidth = 0,
                        originalHeight=0;
                    baseImage.onload = function () {
                        originalWidth = this.width;
                        originalHeight = this.height;
                        elementHeight = ($(window).width() * originalHeight / originalWidth);
                        element.css('height', elementHeight);
                    }
                    baseImage.src = attrs.ngSrc;
  
                    angular.element(window).bind('resize', function () {
                        chosenWidth = $(window).width();
                        elementHeight = (chosenWidth * originalHeight / originalWidth);
                        element.css('height', elementHeight);
                    });
                
                }
            }
        })

        .directive('jquerycropper', [
            '$rootScope', function ($rootScope) {

                return {
                    restrict: 'EA',
                    scope: true,

                    link: function (scope, iElement, attrs) {

                        var console = window.console || {
                            log: function () {
                            }
                        };
                        var $image = $('#image');
                        var $download = $('#download');
                        var $dataX = $('#dataX');
                        var $dataY = $('#dataY');
                        var $dataHeight = $('#dataHeight');
                        var $dataWidth = $('#dataWidth');
                        var $dataRotate = $('#dataRotate');
                        var $dataScaleX = $('#dataScaleX');
                        var $dataScaleY = $('#dataScaleY');
                        var options = {
                            aspectRatio: 3.72 / 1,
                            preview: '.img-preview',
                            crop: function (e) {
                                $dataX.val(Math.round(e.x));
                                $dataY.val(Math.round(e.y));
                                $dataHeight.val(Math.round(e.height));
                                $dataWidth.val(Math.round(e.width));
                                $dataRotate.val(e.rotate);
                                $dataScaleX.val(e.scaleX);
                                $dataScaleY.val(e.scaleY);
                            }
                        };


                        // Tooltip
                        $('[data-toggle="tooltip"]').tooltip();

                        // Cropper
                        $image.on({
                            'build.cropper': function (e) {
                                //console.log(e.type);
                            },
                            'built.cropper': function (e) {
                                //console.log(e.type);
                            },
                            'cropstart.cropper': function (e) {
                                //console.log(e.type, e.action);
                            },
                            'cropmove.cropper': function (e) {
                                //console.log(e.type, e.action);

                            },
                            'cropend.cropper': function (e) {
                                //console.log(e.type, e.action);
                            },
                            'crop.cropper': function (e) {
                                var jsonStr = JSON.stringify($(this).cropper('getData'));
                                $('#putData').val(jsonStr);
                                //$(this).cropper('getCroppedCanvas');
                                //console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);
                            },
                            'zoom.cropper': function (e) {
                                //console.log(e.type, e.ratio);
                                //console.log($(this).cropper("getData"));
                            }
                        }).cropper(options);


                        // Buttons
                        if (!$.isFunction(document.createElement('canvas').getContext)) {
                            $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
                        }

                        if (typeof document.createElement('cropper').style.transition === 'undefined') {
                            $('button[data-method="rotate"]').prop('disabled', true);
                            $('button[data-method="scale"]').prop('disabled', true);
                        }

                        // Options
                        $('.docs-toggles').on('change', 'input', function () {
                            var $this = $(this);
                            var name = $this.attr('name');
                            var type = $this.prop('type');
                            var cropBoxData;
                            var canvasData;

                            if (!$image.data('cropper')) {
                                return;
                            }

                            if (type === 'checkbox') {
                                options[name] = $this.prop('checked');
                                cropBoxData = $image.cropper('getCropBoxData');
                                canvasData = $image.cropper('getCanvasData');

                                options.built = function () {
                                    $image.cropper('setCropBoxData', cropBoxData);
                                    $image.cropper('setCanvasData', canvasData);
                                };
                            } else if (type === 'radio') {
                                options[name] = $this.val();
                            }

                            $image.cropper('destroy').cropper(options);
                        });


                        // Methods
                        $('.docs-buttons').on('click', '[data-method]', function () {
                            var $this = $(this);
                            var data = $this.data();
                            var $target;
                            var result;

                            if ($this.prop('disabled') || $this.hasClass('disabled')) {
                                return;
                            }

                            if ($image.data('cropper') && data.method) {
                                data = $.extend({}, data); // Clone a new one

                                if (typeof data.target !== 'undefined') {
                                    $target = $(data.target);

                                    if (typeof data.option === 'undefined') {
                                        try {
                                            data.option = JSON.parse($target.val());
                                        } catch (e) {
                                            console.log(e.message);
                                        }
                                    }
                                }

                                result = $image.cropper(data.method, data.option, data.secondOption);

                                switch (data.method) {
                                    case 'scaleX':
                                    case 'scaleY':
                                        $(this).data('option', -data.option);
                                        break;

                                    case 'getCroppedCanvas':
                                        if (result) {

                                            // Bootstrap's Modal
                                            $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                                            if (!$download.hasClass('disabled')) {
                                                $download.attr('href', result.toDataURL('image/jpeg'));
                                            }
                                        }

                                        break;
                                }

                                if ($.isPlainObject(result) && $target) {
                                    try {
                                        $target.val(JSON.stringify(result));
                                    } catch (e) {
                                        console.log(e.message);
                                    }
                                }

                            }
                        });

                        // Keyboard
                        $(document.body).on('keydown', function (e) {

                            if (!$image.data('cropper') || this.scrollTop > 300) {
                                return;
                            }

                            switch (e.which) {
                                case 37:
                                    e.preventDefault();
                                    $image.cropper('move', -1, 0);
                                    break;

                                case 38:
                                    e.preventDefault();
                                    $image.cropper('move', 0, -1);
                                    break;

                                case 39:
                                    e.preventDefault();
                                    $image.cropper('move', 1, 0);
                                    break;

                                case 40:
                                    e.preventDefault();
                                    $image.cropper('move', 0, 1);
                                    break;
                            }

                        });


                        // Import image
                        var $inputImage = $('#inputImage');
                        var URL = window.URL || window.webkitURL;
                        var blobURL;

                        if (false && scope.Tab.Cover.tmp == true) {
                            $(".choose-banner-area").css("display", "none");
                            $(".banner-area").css("display", "block");
                        } else if (false && scope.Tab.Cover.Background) {
                            $(".choose-banner-area").css("display", "none");
                            $(".banner-area").css("display", "block");
                            //$image.attr("src", scope.Tab.Cover.Background);

                            var file = scope.Tab.Cover.Background;

                            //var blob = new Blob([file], { type: 'image/plain' });
                            //var blobUrl = URL.createObjectURL(blob);

                            //file = scopetrustAsResourcUrl(file);
                            //$image.attr('src', file);
                            $image.cropper('reset').cropper('replace', file);

                            //scope.Tab.Cover.tmp = true;
                            // Revoke when load complete
                            //URL.revokeObjectURL(blobURL);
                        } else {
                            $(".choose-banner-area").css("display", "block");
                            $(".banner-area").css("display", "none");
                        }

                        if (URL) {
                            $inputImage.change(function () {
                                var files = this.files;
                                var file;

                                if (!$image.data('cropper')) {
                                    return;
                                }

                                if (files && files.length) {
                                    file = files[0];

                                    if (/^image\/\w+$/.test(file.type)) {
                                        blobURL = URL.createObjectURL(file);
                                        $image.one('built.cropper', function () {

                                            $(".choose-banner-area").css("display", "none");
                                            $(".banner-area").css("display", "block");
                                            //scope.Tab.Cover.tmp = true;
                                            // Revoke when load complete
                                            URL.revokeObjectURL(blobURL);
                                        }).cropper('reset').cropper('replace', blobURL);
                                        $inputImage.val('');
                                    } else {
                                        //scope.Tab.Cover.tmp = false;
                                        window.alert('Please choose an image file.');
                                    }
                                }
                            });
                        } else {
                            $inputImage.prop('disabled', true).parent().addClass('disabled');
                        }

                        //if (false && scope.Tab.Cover.Background != undefined && scope.Tab.Cover.Background != null) {
                        //$image.attr("src", scope.Tab.Cover.Background);
                        //} else {
                        //$(".img-container").html('');
                        //}
                    }

                }
            }
        ])
        .directive("dr", [
            '$rootScope', function ($rootScope) {

                "use strict";
                return function (scope, element) {
                    var startX = 0,
                        startY = 0,
                        x = 0,
                        y = 0;
                    element = angular.element(document.getElementsByClassName("modal-dialog"));
                    element.css({
                        position: 'fixed',
                        cursor: 'move'
                    });

                    element.on('mousedown', function (event) {
                        // Prevent default dragging of selected content
                        event.preventDefault();
                        startX = event.screenX - x;
                        startY = event.screenY - y;
                        //$rootScope.on('mousemove', mousemove);
                        // $rootScope.on('mouseup', mouseup);
                    });

                    function mousemove(event) {
                        y = event.screenY - startY;
                        x = event.screenX - startX;
                        element.css({
                            top: y + 'px',
                            left: x + 'px'
                        });
                    }

                    function mouseup() {
                        $rootScope.unbind('mousemove', mousemove);
                        $rootScope.unbind('mouseup', mouseup);
                    }
                }
            }
        ])
        .directive('htmleditor', [
            '$rootScope', function ($rootScope) {

                return function (scope, element, attrs) {

                    var img = angular.element('<img class="loading-icon" src="/assets/images/loading-icon.gif"/>');
                    element.append(img);
                    scope.$watch(attrs.ngLoading, function (isLoading) {
                        if (isLoading) {
                            //img.removeClass('ng-hide');
                            //element.addClass('loading');
                            // element.attr('disabled', '');
                        } else {
                            //img.addClass('ng-hide');
                            //element.removeClass('loading');
                            //element.removeAttr('disabled');
                            $('div', element).attr('kendo-panel-bar', '');

                        }
                    });
                }

            }
        ])
        .directive('ngThumb', ['$window', function ($window) {
            var helper = {
                support: !!($window.FileReader && $window.CanvasRenderingContext2D),
                isFile: function (item) {
                    return angular.isObject(item) && item instanceof $window.File;
                },
                isImage: function (file) {
                    var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                    return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
                }
            };

            return {
                restrict: 'A',
                template: '<canvas/>',
                link: function (scope, element, attributes) {
                    if (!helper.support) return;

                    var params = scope.$eval(attributes.ngThumb);

                    if (!helper.isFile(params.file)) return;
                    if (!helper.isImage(params.file)) return;

                    var canvas = element.find('canvas');
                    var reader = new FileReader();

                    reader.onload = onLoadFile;
                    reader.readAsDataURL(params.file);

                    function onLoadFile(event) {
                        var img = new Image();
                        img.onload = onLoadImage;
                        img.src = event.target.result;
                    }

                    function onLoadImage() {
                        var width = params.width || this.width / this.height * params.height;
                        var height = params.height || this.height / this.width * params.width;
                        canvas.attr({ width: width, height: height });
                        canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                    }
                }
            };
        }])
        .directive('compile', function ($compile) {

            return function (scope, element, attrs) {
                scope.$watch(
                    function (scope) {
                        // watch the 'compile' expression for changes
                        return scope.$eval(attrs.compile);
                    },
                    function (value) {
                        // when the 'compile' expression changes
                        // assign it into the current DOM

                        //value = value.replace(/\}/g, '');
                        //value = value.replace(/\{/g, '');
                        element.html(value);

                        // compile the new DOM and link it to the current
                        // scope.
                        // NOTE: we only compile .childNodes so that
                        // we don't get into infinite loop compiling ourselves
                        $compile(element.contents())(scope);
                    }
                );
            };

        });

})();

