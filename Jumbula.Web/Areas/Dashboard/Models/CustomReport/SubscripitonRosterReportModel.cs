﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Helper;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SubscripitonRosterReportModel
    {
        public SubscripitonRosterReportModel()
        {
            Sessons = new List<SubscriptionRosterItem>();
        }

        public string StudentName { get; set; }
        public List<SubscriptionRosterItem> Sessons { get; set; }

        public string Grade { get; set; }

        public string TeacherName { get; set; }

        public string ParentPhoneNumber { get; set; }
    }

    public class SubscriptionRosterItem
    {
        public DateTime SessionDateTime { get; set; }

        public bool IsPresent { get; set; }

        public string SessionTitle
        {
            get
            {
                return string.Format("{0}/{1}", DateTimeHelper.GetDayOfWeekAbbreviation(SessionDateTime.DayOfWeek), SessionDateTime.Date.ToString("MMM dd"));
            }
        }

    }
}