﻿using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface ICountryBusiness
    {
        IQueryable<State> GetStates();

        IQueryable<County> GetCounties();

        IQueryable<County> GetCounties(List<int> ids);

        IQueryable<County> GetCounties(int stateId);

        IQueryable<County> GetStatesCounties(List<int> states);

        County GetCounty(int countId);
        State GetState(int stateId);
        IQueryable<Country> GetUsAndCsCountries();
        IQueryable<State> GetAllStates();
    }
}
