﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class DetailsPlayerViewModel
    {

        public ParentsAddressViewModel ParentsAddress { get; set; }
        public int PlayerId { get; set; }
        public PlayerInfo  Info { get; set; }

        [Required(ErrorMessage = "You must enter first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "You must enter last name.")]
        public string LastName { get; set; }
        public string DOB { get; set; }

        [Required(ErrorMessage = "You must enter primary phone.")]
        public string PrimaryPhone { get; set; }
        public string CellPhone { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int UserId { get; set; }
        public int ContactId { get; set; }
        public int RelationshipId { get; set; }
        
        public RelationshipType PlayerType { get; set; }
    }
    
}