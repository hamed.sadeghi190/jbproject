﻿

namespace Jumbula.Common.Enums
{
    public enum EmailStatus : int
    {
        processed = 0,
        dropped = 1,
        delivered = 2,
        open = 3,
        click = 4,
        bounce = 5,
        deferred = 6,
        spamreport = 7,
        unsubscribe = 8
    }
}
