﻿

namespace Jumbula.Common.Enums
{
    public enum CampaignReciepientsMode : int
    {
        AllUsers = 0,
        SomePeople = 1,
        UserList = 2,
        ClassConfirmation = 3
    }

}
