
-- Add ClubId to JbUserRoles 
ALTER TABLE JbUserRoles ADD ClubId int


-- Remove ClubId to JbUserRoles 
ALTER TABLE JbUserRoles DROP COLUMN ClubId