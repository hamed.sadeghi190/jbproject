﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum TourneyRewardType
    {
        [Description("Select type")]
        None = 0,
        Cash = 1,
        Scholastic = 2,
        Quad = 3
    }
}
