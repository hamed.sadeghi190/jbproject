﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SchoolReconciliationInfoReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/FinanceReports", order: 1, menu: "PortalReports" });


            $scope.stateParams = $stateParams;

            $scope.MC.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            var model = {};
            $scope.filter = {};
            var dataInfo = {};
            $scope.filter.printMode = false;

            $scope.Model = customReportService.getModelFromStrorage();


            $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

                $scope.ClubName = data.ClubName;
                $scope.Date = data.Date;
                dataInfo = data;
                prepareGrid();

            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });


            var prepareGrid = function () {

                $scope.SchoolReconciliationInfoReportDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport: {
                        read: {
                            url: baseUrl + "/GetSchoolReconciliationInfoReportInfo",
                            dataType: "json",
                            //type: "POST",
                            data: function () {
                                return {
                                    startDate: $scope.filter.startDate,
                                    endDate: $scope.filter.endDate,
                                    printMode: $scope.filter.printMode
                                };
                            },
                        }
                    },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },

                    serverSorting: false,
                    serverPaging: true,
                    pageSize: 50,
                    resizable: true,
                });

                $scope.SchoolReconciliationInfoGridOptions = {
                    excel: {
                        allPages: true,
                        fileName: $scope.ClubName + " (School Reconciliation Info).xlsx",
                        proxyURL: "/Dashboard/PortalReport/Save"
                    },
                    excelExport: function (e) {

                        var rows = e.workbook.sheets[0].rows;

                        rows.unshift({
                            cells: [{
                                value: ""
                            }]
                        });
                        rows.unshift({
                            cells: [{
                                value: dataInfo.Date
                            }]
                        });
                        rows.unshift({
                            cells: [{
                                value: "School Reconciliation Info - " + dataInfo.ClubName
                            }]
                        });

                        cfpLoadingBar.complete();
                    },
                    dataSource: $scope.SchoolReconciliationInfoReportDataSource,
                    resizable: true,
                    pageable: {
                        refresh: true,
                        pageSizes:
                        [
                            5, 10, 20, 30, 50, 100, 200
                        ],
                        buttonCount: 5,
                    },
                    scrollable: true,
                    sortable: { mode: 'single' },
                    columns: [
                            {
                                field: "SchoolName",
                                title: "School name",
                                width: "300px"
                            },
                             {
                                 field: "IsCashPayments",
                                 title: "Cash payments",
                                 width: "150px",
                                 template: "<input type='checkbox' disabled value='' #if(IsCashPayments == 'Yes'){#  checked  #}# />"
                             },
                             {
                                 field: "IsDonations",
                                 title: "Donations",
                                 width: "150px",
                                 template: "<input type='checkbox' disabled value='' #if(IsDonations == 'Yes'){#  checked  #}# />"
                             },
                             {
                                 field: "IsPTAFees",
                                 title: "PTA fees",
                                 width: "150px",
                                 template: "<input type='checkbox' disabled value='' #if(IsPTAFees == 'Yes'){#  checked  #}# />"
                             },
                             {
                                 field: "IsPTAScholarships",
                                 title: "PTA scholarships",
                                 width: "150px",
                                 template: "<input type='checkbox' disabled value='' #if(IsPTAScholarships == 'Yes'){#  checked  #}# />"
                             },
                    ]
                }
            }



            $scope.search = function () {
                $scope.filter.printMode = false;
                $("#SchoolReconciliationInfoGrid").data("kendoGrid").dataSource.read();
            };

            $scope.clearFilters = function () {
                $scope.filter.endDate = "";
                $scope.filter.startDate = "";
                $scope.filter.printMode = false;
                $("#SchoolReconciliationInfoGrid").data("kendoGrid").dataSource.read();
            }
            

            $scope.saveToExcel = function () {
                $scope.filter.printMode = false;
                cfpLoadingBar.start();
                $("#SchoolReconciliationInfoGrid").getKendoGrid().saveAsExcel();
                cfpLoadingBar.complete();
            }
            $scope.saveToPDF = function () {
                cfpLoadingBar.start();
                $scope.filter.printMode = true;
                $("#SchoolReconciliationInfoGrid").data("kendoGrid").dataSource.read();
                cfpLoadingBar.complete();

            }
            $scope.print = function () {
                cfpLoadingBar.start();
                $scope.filter.printMode = true;
                $("#SchoolReconciliationInfoGrid").data("kendoGrid").dataSource.read();
                cfpLoadingBar.complete();
            }


        }
    ]);


})();