﻿using System.Linq;
using System.Web.Mvc;
using Jb.Framework.Common.Forms;
using Jumbula.Core.Data;

namespace Jumbula.Core.Business
{
    public interface IJbFormBusiness
    {
        JbForm Get(int id = 0);
        IQueryable<JbForm> Get(string refEntityName);
        void Update(JbForm jbForm);
        void Delete(int jbFormId);
        OperationStatus CreateEdit(JbForm jbForm);
        OperationStatus UpdateJson(int formId, string jsonElements);
        dynamic GetAsPdf(int id);
        IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null);
    }
}