﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ProgramLocationsViewModel
    {
        public long ClubLocationId { get; set; }
        public long AddressId { get; set; }
        public string Address { get; set; }
        public string LocationName { get; set; }
        public long SeasonId { get; set; }
        public string City { get; set; }
    }
}