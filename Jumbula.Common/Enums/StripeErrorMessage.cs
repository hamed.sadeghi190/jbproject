﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum StripeErrorMessage
    {
        [Description("Your card was declined.")]
        [Display(Name = "Your card was declined.")]
        DeclineCard = 0,

        [Description("Your card has insufficient funds.")]
        [Display(Name = "Your card has insufficient funds.")]
        InsufficientFunds = 1,

        [Description("Your card number is incorrect.")]
        [Display(Name = "Your card number is incorrect.")]
        IncorrectCardNumber = 2,

        [Description("Your card has expired.")]
        [Display(Name = "Your card has expired.")]
        ExpiredCard = 3,

        [Description("No credit card on file.")]
        [Display(Name = "No credit card on file.")]
        NoCreditCard = 4,

        [Description("The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder.")]
        [Display(Name = "The address provided does not match billing address")]
        AddressIssue = 6,

        [Description("Your card does not support this type of purchase.")]
        [Display(Name = "Your card does not support this type of purchase.")]
        CardNotSupport = 7,

        [Description("Customer Profile ID or Customer Payment Profile ID not found.")]
        [Display(Name = "Customer Profile ID or Customer Payment Profile ID not found")]
        CustomerNotFound = 9,


        [Description("This transaction has been declined.")]
        [Display(Name = "This transaction has been declined")]
        TransactionCard = 10,

        [Description("Your card was declined.  You can call your bank for details.")]
        [Display(Name = "Your card was declined.  You can call your bank for details")]
        DeclinedCallToBank = 13,


        [Description("The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder.")]
        [Display(Name = "The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder")]
        AvSmismatch = 14,


        [Description("The merchant does not accept this type of credit card.")]
        [Display(Name = "The merchant does not accept this type of credit card")]
        MerchantNotAccept = 15,


        [Description("Your card has expired.")]
        [Display(Name = "Your card has expired")]
        CardExpired = 16,


        [Description("Your card does not support this type of purchase.")]
        [Display(Name = "Your card does not support this type of purchase")]
        NotSupportPurchaseType = 17,


        [Description("The zip code you supplied failed validation.")]
        [Display(Name = "The zip code you supplied failed validation")]
        WrongZipCode = 18,


        [Description("Your card's security code is incorrect.")]
        [Display(Name = "Your card's security code is incorrect")]
        SecurityCodeIncorrect = 19,

        [Description("The credit card has expired.")]
        [Display(Name = "The credit card has expired.")]
        CreditCardExpired = 20



    }
}
