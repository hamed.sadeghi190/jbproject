﻿using System.Collections.Generic;

namespace Jb.Framework.Common.Forms
{
    public interface ICustomElementManager
    {
        List<ICustomElement> GetAllCustomeElement();
        void AddCustomeElement(ICustomElement customeElement);
    }
}