﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class PaypalTrackingBusiness :  IPaypalTrackingBusiness
    {
        private readonly IRepository<PaypalTracking, long> _paypalTrackingRepository;

        public PaypalTrackingBusiness(IRepository<PaypalTracking, long> paypalTrackingRepository)
        {
            _paypalTrackingRepository = paypalTrackingRepository;
        }

        public OperationStatus Create(PaypalTracking paypalTracking)
        {
            return _paypalTrackingRepository.Create(paypalTracking);
        }
        public string GenerateTrackingId(JumbulaSubSystem caller, long callerId, string token)
        {

  
           var  result =string.Format("{0}__{1}__{2}",caller.ToString(),callerId.ToString(), DateTime.UtcNow.ToString("yyyyMMdd_HHmmssffff"));

                var paypalTracking = new PaypalTracking() {
                TrackingId=result,
                IsProceed=false,
                CallerId=callerId,
                PaypalCaller = caller,
                Token=token
                };
                Create(paypalTracking);

               return result;
        }
     
        public IQueryable<PaypalTracking> GetUnProcessedItems()
        {
            return _paypalTrackingRepository.GetList().Where(c => c.IsProceed == false);
        }

        public OperationStatus Update(PaypalTracking paypalTracking)
        {
            return _paypalTrackingRepository.Update(paypalTracking);
        }
    }
}
