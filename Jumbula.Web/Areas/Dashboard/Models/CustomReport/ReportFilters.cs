﻿using System;
using System.Collections.Generic;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ReportFilters
    {
        public DateTime? RegistrationStart { get; set; }

        public DateTime? RegistrationEnd { get; set; }

        public OrderItemStatusReasons? Status { get; set; }

        public long? Tuition { get; set; }

        public bool HasBalance { get; set; }

        public FollowupFormFilter FollowupFilter { get; set; }

        public BalanceReportFilters BalanceFilter { get; set; }

        public int CouponFilter { get; set; }

        public TransactionFilters TransactionFilters { get; set; }

        public List<long> _SelectedPrograms { get; set; }

    }

    public class TransactionFilters
    {
        public TransactionCategory? TransactionCategory { get; set; }

        public PaymentMethod? PaymentMethod { get; set; }
    }
}