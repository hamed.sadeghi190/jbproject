﻿; (function () {
    'use strict';

    window.app = angular.module('dashboardApp', [
            'ngSanitize',
            'ui.router',
            'ui.sortable',
            'angular-loading-bar',
            'ngAnimate',
            'ng-autofocus',
            'kendo.directives',
            'ngval',
            'jbTools',
            'ui.mask',
            'ui.validate',
            'angular.chosen',
            'ngAutocomplete',
            'angular-img-cropper'
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $httpProvider) {


            //initialize get if not there
            if (!$httpProvider.defaults.headers.get) {
                $httpProvider.defaults.headers.get = {};
            }

            $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
            // extra
            $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache, no-store';
            $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

            $httpProvider.defaults.headers.common["FROM-ANGULAR"] = "true";

            $urlRouterProvider.otherwise("/");
            $stateProvider
                .state('Overview', {
                    url: '/',
                    templateUrl: 'Dashboard/Overview',
                    controller: 'overviewController'
                })
                .state('Blade', {
                    url: '/:bladeUrl/',
                    //templateUrl:'',
                    controller: ''
                })
                 /*Invoice*/
                .state('Invoices', {
                    url: '/Invoicing/ManageInvoices',
                    templateUrl: 'Dashboard/Invoice/ManageInvoices',
                    controller: 'invoiceController'
                })
                .state('InvoiceDetail', {
                    url: '/Invoicing/Invoice/Details/:invoiceId',
                    templateUrl: 'Dashboard/Invoice/InvoiceDetail',
                    controller: 'invoiceDetailController'
                })
                .state('ParentInvoiceDetail', {
                    url: '/Invoicing/ParentInvoice/Details/:invoiceId',
                    templateUrl: 'Dashboard/Invoice/InvoiceDetail',
                    controller: 'invoiceDetailController'
                })
                 .state('TakePaymentInvoice', {
                     url: '/Invoicing/TakePayment/:invoiceId',
                     templateUrl: 'Dashboard/Invoice/InvoiceTakePayment',
                     controller: 'invoiceTakePaymentController'
                 })
                   .state('FamilyTakePayment', {
                       url: '/Family/FamilyTakePayment/:userId',
                       templateUrl: 'Dashboard/People/FamilyTakePayment',
                       controller: 'familyTakePaymentController'
                   })
                .state('CancelationInvoice', {
                    url: '/Invoicing/Cancel/:invoiceId',
                    templateUrl: 'Dashboard/Invoice/CancelAndRemainderInvoice',
                    controller: 'cancelAndRemainderInvoiceController'
                })
                .state('ReminderInvoice', {
                    url: '/Invoicing/Reminder/:invoiceId',
                    templateUrl: 'Dashboard/Invoice/CancelAndRemainderInvoice',
                    controller: 'cancelAndRemainderInvoiceController'
                })
                .state('ConfirmTakePaymentInvoice', {
                    url: '/Invoicing/TakePayment/:invoiceId/ReviewPayment',
                    templateUrl: 'Dashboard/Invoice/ConfirmTakePaymentInvoice',
                    controller: 'confirmTakePaymentInvoiceController'
                })
                .state('ConfirmFamilyTakePayment', {
                    url: '/TakePayment/ReviewPayment/:userId',
                    templateUrl: 'Dashboard/People/ConfirmFamilyTakePayment',
                    controller: 'confirmFamilyTakePaymentController'
                })
                .state('Seasons', {
                    url: '/Seasons',
                    template: '',
                    controller: 'seasonsController'
                })
                .state('SeasonAdd', {
                    url: '/Seasons/Add',
                    templateUrl: 'Dashboard/Season/SeasonAddEdit',
                    controller: 'seasonAddController'
                })
                .state('CopySeasonStep2', {
                    url: '/Seasons/Add/CopySeason/Step2',
                    templateUrl: 'Dashboard/Season/CopySeasonStep2',
                    controller: 'CopySeasonController'
                })
                .state('CopySeasonStep3', {
                    url: '/Seasons/Add/CopySeason/Step3',
                    templateUrl: 'Dashboard/Season/CopySeasonStep3',
                    controller: 'CopySeasonController'
                })
                .state('SeasonEdit', {
                    url: '/Season/:seasonDomain/Edit',
                    templateUrl: 'Dashboard/Season/SeasonAddEdit',
                    controller: 'seasonEditController'
                })
                 .state('SeasonFreeze', {
                     url: '/Season/:seasonDomain/Freeze',
                     templateUrl: 'Dashboard/Season/SeasonManage',
                     controller: 'seasonFreezeController'
                 })
                .state('SeasonPublish', {
                    url: '/Season/:seasonDomain/Publish',
                    templateUrl: 'Dashboard/Season/SeasonManage',
                    controller: 'seasonPublishController'
                })
                 .state('SeasonAssignRoomForPrograms', {
                     url: '/Season/:seasonDomain/AssignRoom',
                     templateUrl: 'Dashboard/Season/SeasonAssignRoomForPrograms',
                     controller: 'seasonAssignRoomForProgramsController'
                })
                .state('InstructorAssignForPrograms', {
                    url: '/Season/:seasonDomain/AssignInstructor',
                    templateUrl: 'Dashboard/Season/InstructorAssignForPrograms',
                    controller: 'instructorAssignForProgramsController'
                })
                 .state('ClubsBlade', {
                     url: '/Members',
                     //templateUrl: 'Dashboard/Club/Clubs',
                     template: '',
                     controller: 'clubsBladeController'
                 })
                .state('Clubs', {
                    url: '/Members/:type',
                    templateUrl: 'Dashboard/Club/Clubs',
                    controller: 'clubsController'
                })
                 .state('ClubManage', {
                     url: '/Member/:clubId/:type/Manage',
                     templateUrl: 'Dashboard/Club/ClubManage',
                     controller: 'clubManageController'
                 })
                 .state('ClubResetPassword', {
                     url: '/Member/:clubId/:type/ResetPassword',
                     templateUrl: 'Dashboard/Club/ResetPassword',
                     controller: 'clubResetPasswordController'
                 })
                 .state('ClubUsersEdit', {
                     url: '/Member/:clubId/:type/EditUsers',
                     templateUrl: 'Dashboard/Club/EditClubUser',
                     controller: 'clubEditUserRoleController'
                 })
                .state('Season', {
                    url: '/Season/:seasonDomain',
                    templateUrl: 'Dashboard/SeasonOverview',
                    controller: 'seasonController'
                })
                .state('SeasonSetup', {
                    url: '/Season/:seasonDomain/Setup',
                    template: '',
                    controller: 'seasonSetupController'
                })
                .state('SeasonSchoolSetting', {
                    url: '/Season/:seasonDomain/Settings',
                    template: '',
                    controller: 'seasonSchoolSettingsController'

                }).state('SeasonSchoolSettingGeneralInfo', {
                    url: '/Season/:seasonDomain/Settings/General',
                    templateUrl: 'Dashboard/Season/SchoolSettingGeneral',
                    controller: 'seasonSchoolSettingsGeneralController'

                }).state('SeasonSchoolSettingDetailInfo', {
                    url: '/Season/:seasonDomain/Settings/Detail',
                    templateUrl: 'Dashboard/Season/SchoolSettingDetail',
                    controller: 'seasonSchoolSettingsDetailController'

                }).state('SeasonSchoolSettingScheduleInfo', {
                    url: '/Season/:seasonDomain/Settings/Schedule',
                    templateUrl: 'Dashboard/Season/SchoolSettingSchedule',
                    controller: 'seasonSchoolSettingsScheduleController'

                })
                .state('SeasonSchoolSettingRegistrationInfo', {
                    url: '/Season/:seasonDomain/RegistrationInformation',
                    template: '',
                    controller: 'seasonSchoolSettingsRegistrationController'

                })
                 .state('SeasonSchoolSettingProgramInfo', {
                    url: '/Season/:seasonDomain/RegistrationInformation/ProgramInformation',
                    templateUrl: 'Dashboard/Season/SchoolSettingProgramInfo',
                    controller: 'seasonSchoolSettingsProgramInfoController'

                }).state('SeasonSchoolSettingForms', {
                    url: '/Season/:seasonDomain/RegistrationInformation/Forms',
                    templateUrl: 'Dashboard/Season/SchoolSettingSeasonForms',
                    controller: 'seasonSchoolSettingsFormsController'

                })
                .state('SeasonSchoolPaymentPlan', {
                    url: '/Season/:seasonDomain/RegistrationInformation/PaymentPlan',
                    templateUrl: 'Dashboard/Season/SchoolSettingPaymentPlan',
                    controller: 'seasonSchoolSettingsPaymentPlanController'
                })
                .state('SeasonSchoolSettingAdditionalOptions', {
                    url: '/Season/:seasonDomain/RegistrationInformation/AddtionalOptions',
                    templateUrl: 'Dashboard/Season/SchoolSettingAdditional',
                    controller: 'seasonSchoolSettingsAdditionalController'

                }).state('Programs', {
                    url: '/Season/:seasonDomain/Programs',
                    templateUrl: 'Dashboard/Programs',
                    controller: 'programsController'
                })
                .state('ProgramsReview', {
                    url: '/Season/:seasonDomain/ProgramsReview',
                    templateUrl: 'Dashboard/ProgramsReview',
                    controller: 'programsReviewController'
                })
                 .state('AddToWaitlists', {
                     url: '/Season/:seasonDomain/AddToWaitlist/:programId/:userId',
                     templateUrl: 'Dashboard/Program/AddToWaitlist',
                     controller: 'addToWaitlistController'
                 })
                .state('ProgramSession', {
                    url: '/Season/:seasonDomain/ProgramSessions/:programId',
                    templateUrl: 'Dashboard/Program/Sessions',
                    controller: 'programSessionsController'
                })
                .state('ProgramWaitlist', {
                    url: '/Season/:seasonDomain/ProgramWaitlist/:programId',
                    templateUrl: 'Dashboard/Program/Waitlist',
                    controller: 'programWaitlistController'
                })
                .state('AsignInstructors', {
                    url: '/Season/:seasonDomain/AsignInstructors/:programId',
                    templateUrl: 'Dashboard/Program/AssignInstructors',
                    controller: 'assignInstructorsController'
                })
                .state('AsignRoom', {
                    url: '/Season/:seasonDomain/AsignRoom/:programId',
                    templateUrl: 'Dashboard/Program/AssignRoom',
                    controller: 'assignRoomController'
                })
                 .state('SessionAttendance', {
                     url: '/Season/:seasonDomain/program/:programId/session/:sessionId',
                     templateUrl: 'Dashboard/Program/SessionAttendance',
                     controller: 'sessionAttendanceController'
                 })
                .state('ProgramAddBlade', {
                    url: '/Season/:seasonDomain/Programs/Add',
                    template: '',
                    controller: 'programAddBladeController'
                })
                .state('ProgramAddEditStep1', {
                    url: '/Season/:seasonDomain/Programs/Manage/:programType/Step1/:programId/:fromTemplate',
                    templateUrl: 'Dashboard/Program/ProgramAddEditStep1',
                    controller: 'programAddEditStep1Controller'
                })
                .state('ProgramAddEditStep2', {
                    url: '/Season/:seasonDomain/Programs/Manage/:programType/Step2/:programId',
                    templateUrl: 'Dashboard/Program/ProgramAddEditStep2',
                    controller: 'programAddEditStep2Controller'
                })
                .state('CalendarAddEditStep2Point5', {
                    url: '/Season/:seasonDomain/Programs/Manage/:programType/Step2.5/:programId',
                    templateUrl: 'Dashboard/Program/CalendarAddEditStep2Point5',
                    controller: 'calendarAddEditStep2Point5Controller'
                })
                .state('SubscriptionAddEditStep2Point5', {
                    url: '/Season/:seasonDomain/Programs/Manage/:programType/Step2.5/:programId',
                    templateUrl: 'Dashboard/Program/SubscriptionAddEditStep2Point5',
                    controller: 'subscriptionAddEditStep2Point5Controller'
                })
                .state('ProgramAddEditStep3', {
                    url: '/Season/:seasonDomain/Programs/Manage/:programType/Step3/:programId',
                    templateUrl: 'Dashboard/Program/ProgramAddEditStep3',
                    controller: 'programAddEditStep3Controller'
                })
                .state('ProgramAddEditStep4', {
                    url: '/Season/:seasonDomain/Programs/Manage/:programType/Step4/:programId',
                    templateUrl: 'Dashboard/Program/ProgramAddEditStep4',
                    controller: 'programAddEditStep4Controller'
                })

                .state('ProgramAddStep5', {
                    url: '/Season/:seasonDomain/Programs/Add/:programType/Step5',
                    templateUrl: 'Dashboard/ProgramAddStep5',
                    controller: 'programAddController'
                })
                 .state('BeforeAfterCareAddEditStep2Fees', {
                     url: '/Season/:seasonDomain/Programs/Manage/:programType/Step2/Fees/:programId',
                     templateUrl: 'Dashboard/Program/BeforeAfterCareAddEditStep2Fees',
                     controller: 'beforeAfterCareAddEditStep2FeesController'
                 })
                .state('BeforeAfterCareAddEditStep2ScheduleList', {
                    url: '/Season/:seasonDomain/Programs/Manage/:programType/Step2/ScheduleList/:programId',
                    templateUrl: 'Dashboard/Program/BeforeAfterCareAddEditStep2ScheduleList',
                    controller: 'beforeAfterCareAddEditStep2ScheduleListController'
                })
                  .state('ProgramRespondToInvitation', {
                      url: '/Season/:seasonDomain/Programs/Respond/:programId',
                      templateUrl: 'Dashboard/Program/ProgramRespondToInvitation',
                      controller: 'programRespondToInvitationController'
                  })
                .state('Program', {
                    url: '/Season/:seasonDomain/Program/Summary/:programId',
                    templateUrl: 'Dashboard/Program/Summary',
                    controller: 'summaryController'
                })
                .state('Lottery', {
                    url: '/Season/:seasonDomain/Program/Lottery/:programId',
                    templateUrl: 'Dashboard/Program/GetView/Lottery',
                    controller: 'lotteryController'
                })
                .state('Register', {
                    url: '/Season/:seasonDomain/Program/Register/:programId/:Type',
                    templateUrl: 'Dashboard/Program/Register/',
                    controller: 'registerController'
                })
                .state('ConfirmNewRegisteration', {
                    url: '/Season/:seasonDomain/Program/ConfirmNewRegisteration/:programId',
                    templateUrl: 'Dashboard/Program/ConfirmRegisterForNewUser/',
                    controller: 'confirmNewRegisterController'
                }).state('Transfer', {
                    url: '/Season/:seasonDomain/Order/Transfer/:orderId',
                    templateUrl: 'Dashboard/Order/Transfer',
                    controller: 'transferController'
                })
                .state('OrderChandgeDays', {
                    url: '/Season/:seasonDomain/Order/ChangeDays/:orderId',
                    templateUrl: 'Dashboard/Order/ChangeDays',
                    controller: 'chandgeDaysController'
                })
               .state('OrderChandgeDesiredStartDate', {
                   url: '/Season/:seasonDomain/Order/ChangeDesiredStartDate/:orderId',
                   templateUrl: 'Dashboard/Order/ChangeDesiredStartDate',
                   controller: 'changeDesiredStartDateController'
               })
                .state('EditOrder', {
                    url: '/Season/:seasonDomain/Order/Edit/:orderId',
                    templateUrl: 'Dashboard/Order/EditOrder',
                    controller: 'editOrderController'
                })
                 .state('EditCancelDropInOrder', {
                     url: '/Season/:seasonDomain/Item/Edit/:orderId',
                     templateUrl: 'Dashboard/Order/EditCancelDropInOrder',
                     controller: 'editCancelDropInOrderController'
                 })
                 .state('CancelSubscriptionOrder', {
                     url: '/Season/:seasonDomain/Order/Cancel/:orderId',
                     templateUrl: 'Dashboard/Order/CancelSubscriptionOrder',
                     controller: 'cancelSubscriptionOrderController'
                 })
                 .state('ConfirmCancelSubscriptionItem', {
                     url: '/Season/:seasonDomain/Order/Cancel/Review/:orderId',
                     templateUrl: 'Dashboard/Order/ConfirmCancelSubscriptionItem',
                     controller: 'confirmCancelSubscriptionItemController'
                 })
                .state('ProgramDelete', {
                    url: '/Season/:seasonDomain/Program/:programDomain/Delete',
                    templateUrl: 'Dashboard/ProgramDelete',
                    controller: 'programDeleteController'
                })
                .state('Coupons', {
                    url: '/Season/:seasonDomain/Coupons',
                    templateUrl: 'Dashboard/Coupon/Coupons',
                    controller: 'couponsController'
                }).state('CouponAdd', {
                    url: '/Season/:seasonDomain/Coupons/Add',
                    templateUrl: 'Dashboard/Coupon/CouponManage',
                    controller: 'couponEditController'
                }).state('CouponsUpload', {
                    url: '/Season/:seasonDomain/Coupons/CouponsUpload',
                    templateUrl: 'Dashboard/Coupon/CouponsUpload',
                    controller: 'couponsUploadController'
                })
                .state('CouponEdit', {
                    url: '/Season/:seasonDomain/Coupon/:couponId/Edit',
                    templateUrl: 'Dashboard/Coupon/CouponManage',
                    controller: 'couponEditController'
                }).state('ConfirmMakePayment', {
                    url: '/Season/:seasonDomain/Program/:programId/OrderItem/:orderItemId/ReviewPayment',
                    templateUrl: 'Dashboard/Order/ConfirmMakePayment',
                    controller: 'confirmMakePaymentController'
                }).state('MakePayment', {
                    url: '/Season/:seasonDomain/Program/:programId/OrderItem/:orderItemId/MakePayment/:transactionId',
                    templateUrl: 'Dashboard/Order/MakePayment',
                    controller: 'makePaymentController'
                })
                .state('DisplayPaymentMessage', {
                    url: '/Season/:seasonDomain/OrderItem/:orderItemId/Status/:status/Message/:message',
                    templateUrl: 'Dashboard/Order/DisplayPaymentMessage',
                    controller: 'displayPaymentMessageController'
                }).state('DisplayFamilyTakePaymentPaymentMessage', {
                    url: '/FamilyTakePayment/Status/:status/Message/:message',
                    templateUrl: 'Dashboard/People/DisplayFamilyTakePaymentPaymentMessage',
                    controller: 'displayFamilyTakePaymentPaymentMessageController'
                })
                .state('Order', {
                    url: '/Season/:seasonDomain/Order/:orderId',
                    templateUrl: 'Dashboard/Order/Order',
                    controller: 'orderController'
                }).state('OrderHistory', {
                    url: '/Season/:seasonDomain/OrderHistory/:orderId/:orderItemId',
                    templateUrl: 'Dashboard/Order/OrderHistory',
                    controller: 'orderHistoryController'
                }).state('SchoolSettingRegistrationInfo', {
                    url: '/Settings/School/RegistrationInfo',
                    templateUrl: 'Dashboard/Setting/SchoolRegistrationInfo',
                    controller: 'schoolSettingsRegistrationInfoController'
                }).state('PaymentHistory', {
                    url: '/Season/:seasonDomain/PaymentHistory/:orderItemId/:installmentId',
                    templateUrl: 'Dashboard/Order/PaymentHistory',
                    controller: 'paymentHistoryController'
                }).state('OrderItem', {
                    url: '/Season/:seasonDomain/OrderItem/:orderItemId/:changeFormId',
                    templateUrl: 'Dashboard/Order/OrderItem',
                    controller: 'orderItemController'
                })
                .state('WaiverView', {
                    url: '/Season/:seasonDomain/OrderItem/:orderItemId/WaiverView/:waiverId',
                    templateUrl: 'Dashboard/Order/WaiverView',
                    controller: 'waiverViewController'
                })
                .state('PartiallyRefundOrderItem', {
                    url: '/Season/:seasonDomain/Program/:programId/Refund/:orderItemId/:installmentId',
                    templateUrl: 'Dashboard/Order/PartiallyRefundOrderItem',
                    controller: 'partiallyRefundController'
                }).state('ConfirmPartiallyRefundOrderItem', {
                    url: '/Season/:seasonDomain/Program/:programId/ConfirmRefund/:orderItemId',
                    templateUrl: 'Dashboard/Order/ConfirmPartiallyRefundOrderItem',
                    controller: 'confirmPartiallyRefundController'
                }).state('CampaignsOverview', {
                    url: '/Campaigns',
                    templateUrl: 'Dashboard/CampaignsOverview',
                    controller: 'campaignsOverviewController'
                }).state('EmailCampaigns', {
                    url: '/Campaigns/Email',
                    templateUrl: 'Dashboard/EmailCampaigns',
                    controller: 'campaignsController'
                }).state('Settings', {
                    url: '/Settings',
                    template: '',
                    controller: 'settingsController'
                }).state('PortalReports', {
                    url: '/Reports',
                    template: '',
                    controller: 'PortalReportsBladeController'
                })
                  .state('FinanceReportsBlade', {
                      url: '/Reports/FinanceReports',
                      template: '',
                      controller: 'FinanceReportsBladeController'
                  })
                 .state('FinanceReports', {
                     url: '/Reports/Finance',
                     template: '',
                     controller: 'financeReportsController'
                 })
                   .state('RostersReportsBlade', {
                       url: '/Reports/RostersReports',
                       template: '',
                       controller: 'RostersReportsBladeController'
                   })
                   .state('SchoolSeasonsFinanceReports', {

                       url: '/Reports/FinanceReports/:reportType/SchoolSeasonsFinanceReports',
                       templateUrl: 'Dashboard/PortalReport/SchoolSeasonsFinanceReport',
                       controller: 'SelectSchoolSeasonsFinanceController'
                   })
                 .state('SchoolSeasonReconciliationReports', {

                     url: '/Reports/FinanceReports/:reportType/SchoolSeasonReconciliationReports',
                     templateUrl: 'Dashboard/PortalReport/SchoolSeasonsReport',
                     controller: 'SelectSchoolSeasonsReconciliationController'
                 })


                  //.state('SelectDiscountDate', {
                  //    url: '/Reports/DiscountReports/:reportType/Selectdate',
                  //    templateUrl: '/Dashboard/PortalReport/SelectDisocuntDate',
                  //    controller: 'SelectDiscountDateController'
                  //})
                    .state('EMSeasonReconciliationReports', {

                        url: '/Reports/FinanceReports/:reportType/Report/:reportName',
                        templateUrl: 'Dashboard/PortalReport/EMSeasonReconciliationReport',
                        controller: 'SelectEMSeasonReconciliationController'
                    })
                   .state('AllSeasonReconciliationReport', {
                       url: '/Reports/ReconciliationReports/Run/:seasonId',
                       templateUrl: '/Dashboard/PortalReport/SelectPrograms',
                       controller: 'ReconciliationReportController'
                   })
                 .state('AllRegistrantReconciliationReport', {
                     url: '/Reports/RegistrantReports/Run/:seasonId',
                     templateUrl: '/Dashboard/PortalReport/SelectRegistrantsSeason',
                     controller: 'RegistrantReportController'
                 })
                .state('AllEMSeasonReconciliationReport', {
                    url: '/Reports/EMSeasonReports/Run/:seasonId',
                    templateUrl: '/Dashboard/PortalReport/SelectEMSeason',
                    controller: 'EMSeasonReportController'
                })
                .state('SchoolReconciliationInfoReports', {
                    url: '/Reports/SchoolReconciliationInfoReports/Run',
                    templateUrl: 'Dashboard/PortalReport/SchoolReconciliationInfoReport',
                    controller: 'SchoolReconciliationInfoReportController'
                })
                .state('ReconciliationPrepReports', {
                    url: '/Reports/ReconciliationPrepReports/Run/:seasonId',
                    templateUrl: 'Dashboard/PortalReport/ReconciliationPrepReport',
                    controller: 'ReconciliationPrepReportController'
                })
                .state('MemberTaxIdStatusReports', {
                    url: '/Reports/FinanceReports/MemberTaxIdStatusReport',
                    templateUrl: 'Dashboard/PortalReport/MemberTaxIdStatusReport',
                    controller: 'MemberTaxIdStatusReportController'
                })
                .state('SessionStatementDataReports', {
                    url: '/Reports/FinanceReports/SessionStatementDataReport',
                    templateUrl: 'Dashboard/PortalReport/SessionStatementDataReport',
                    controller: 'SessionStatementDataReportController'
                })
                 .state('SelectPrograms', {
                     url: '/Reports/SelectPrograms',
                     templateUrl: 'Dashboard/PortalReport/SelectPrograms',
                     controller: 'SelectProgramsController'
                 })

                .state('CatalogReports', {
                    url: '/Reports/catalog',
                    template: '',
                    controller: 'CatalogReportsBladeController'
                })
                .state('SchoolReports', {
                    url: '/Reports/school',
                    template: '',
                    controller: 'SchoolReportsBladeController'
                })
                .state('PlanningReports', {
                    url: '/Reports/planning',
                    template: '',
                    controller: 'PlanningReportsBladeController'
                })
                .state('FormReports', {
                    url: '/Reports/Form',
                    template: '',
                    controller: 'FormReportsBladeController'
                })
                .state('ProviderAddressListReports', {
                    url: '/Reports/Planning/ProviderAddressList',
                    templateUrl: '/Dashboard/PortalReport/ProviderAddressListReport',
                    controller: 'ProviderAddressListReportController'
                })
                .state('SchoolAddressListReports', {
                    url: '/Reports/Planning/SchoolAddressList',
                    templateUrl: '/Dashboard/PortalReport/SchoolAddressListReport',
                    controller: 'SchoolAddressListReportController'
                })
                .state('MemberListReports', {
                    url: '/Reports/Planning/MemberList',
                    templateUrl: '/Dashboard/PortalReport/MemberListReport',
                    controller: 'MemberListReportController'
                })

                .state('InsuranceCertificateCheckReports', {
                    url: '/Reports/Planning/InsuranceCertificateCheck',
                    templateUrl: '/Dashboard/PortalReport/InsuranceCertificateCheckReport',
                    controller: 'InsuranceCertificateCheckReportController'
                })
                .state('SeasonSurveyDatesReports', {
                    url: '/Reports/Planning/SeasonSurveyDates',
                    templateUrl: '/Dashboard/PortalReport/SeasonSurveyDatesReport',
                    controller: 'SeasonSurveyDatesReportController'
                })
                .state('InstructorAssignmentsPortalReports', {
                    url: '/Reports/Planning/InstructorAssignmentsPortalReport/:seasonId',
                    templateUrl: '/Dashboard/PortalReport/InstructorAssignmentsPortalReport',
                    controller: 'InstructorAssignmentsPortalReportController'
                })
                .state('FullSeasonClassListReports', {
                    url: '/Reports/Planning/FullSeasonClassListReport',
                    templateUrl: '/Dashboard/PortalReport/FullSeasonClassListReport',
                    controller: 'FullSeasonClassListReportController'
                })
                .state('FollowupReportPortal', {
                    url: '/Reports/Planning/FollowupReport',
                    templateUrl: '/Dashboard/PortalReport/FollowupReportPortal',
                    controller: 'FollowupReportPortalController'
                })
                .state('InstructorCheckinPortalReports', {
                    url: '/Reports/Planning/InstructorCheckinReport',
                    templateUrl: '/Dashboard/PortalReport/InstructorCheckinPortalReport',
                    controller: 'InstructorCheckinPortalReportController'
                })
                .state('DiscountsReportsBlade', {
                    url: '/Reports/DiscountReports',
                    template: '',
                    controller: 'DiscountsReportsBladeController'
                })
                .state('GraduateSelectGradeReports', {
                    url: '/Reports/SelectGrade',
                    templateUrl: 'Dashboard/PortalReport/GraduatesReports',
                    controller: 'graduateSelectGradeReportsController'
                })
                .state('OnsitePersonCheckinReports', {
                    url: '/Reports/OnsitePersonCheckinReport',
                    templateUrl: 'Dashboard/PortalReport/OnsitePersonCheckinReport',
                    controller: 'OnsitePersonCheckinReportController'
                })

                 .state('EnrollmentReports', {
                     url: '/Reports/Enrollment',
                     templateUrl: 'Dashboard/PortalReport/EnrollmentReports',
                     controller: 'enrollmentReportsController'
                 })
                .state('SchoolEnrollmentsReports', {
                    url: '/Reports/SchoolEnrollment',
                    templateUrl: 'Dashboard/PortalReport/SchoolEnrollmentsReports',
                    controller: 'schoolEnrollmentsReportsController'
                })
                 .state('RenewalPTAReports', {
                     url: '/Reports/RenewalPTA',
                     templateUrl: 'Dashboard/PortalReport/RenewalPTAReports',
                     controller: 'renewalPTAReportsController'
                 })
                  .state('ClassProfitsReports', {
                      url: '/Reports/ClassProfits',
                      templateUrl: 'Dashboard/PortalReport/ClassProfitsReports',
                      controller: 'classProfitsReportsController'
                  })
                  .state('TeachersReport', {
                      url: '/Reports/TeachersReport',
                      templateUrl: 'Dashboard/PortalReport/TeachersReport',
                      controller: 'teachersReportController'
                  })
                   .state('SchoolProgramsListCheckInOut', {
                       url: '/Reports/SchoolProgramsListCheckInOut',
                       templateUrl: 'Dashboard/PortalReport/SchoolProgramsListCheckInOut',
                       controller: 'schoolProgramsListCheckInOutController'
                   })
                 .state('GeneralTransactionsReport', {
                     url: '/Reports/TransactionFinance',
                     templateUrl: 'Dashboard/PortalReport/GeneralTransactionsReport',
                     controller: 'generalTransactionsReportController'
                })
                .state('GeneralTransactionsSubscriptionReport', {
                    url: '/Reports/TransactionSubscriptionFinance',
                    templateUrl: 'Dashboard/PortalReport/GetView/GeneralTransactionsSubscriptionReport',
                    controller: 'generalTransactionsSubscriptionReportController'
                })
                .state('InstallmentsReport', {
                    url: '/Reports/Installments',
                    templateUrl: 'Dashboard/PortalReport/InstallmentsReport',
                    controller: 'partnerInstallmentsReportController'
                }).state('ClientCreditCardsReport', {
                    url: '/Reports/ClientCreditCards',
                    templateUrl: 'Dashboard/PortalReport/ClientCreditCardsReport',
                    controller: 'ClientCreditCardsReportController'
                })
                .state('PricingReport', {
                    url: '/Reports/Pricing',
                    templateUrl: 'Dashboard/PortalReport/PricingReport',
                    controller: 'PricingReportController'
                })
                .state('FullClassListReport', {
                    url: '/Reports/FullClassList',
                    templateUrl: 'Dashboard/PortalReport/FullClassListReport',
                    controller: 'FullClassListReportController'
                })
                 .state('InternalRosterReports', {
                     url: '/Reports/InternalRosterReports',
                     templateUrl: 'Dashboard/PortalReport/InternalRosterReports',
                     controller: 'internalRosterReportsController'
                })
                .state('CampRosterReportsPortal', {
                    url: '/Reports/CampRosterReports',
                    templateUrl: 'Dashboard/PortalReport/CampRosterReports',
                    controller: 'campRosterReportsController'
                })
                 .state('InternalRosterSchoolReport', {
                     url: '/Season/:seasonDomain/Report/Default/InternalRosterSchoolReport',
                     templateUrl: '/Dashboard/CustomReport/InternalRosterSchoolReport',
                     controller: 'internalRosterSchoolReportController'
                })
                .state('AllergyMedicalConditionReport', {
                    url: '/Season/:seasonDomain/Report/Default/AllergyMedicalConditionReport',
                    templateUrl: '/Dashboard/CustomReport/AllergyMedicalConditionReport',
                    controller: 'allergyMedicalConditionReportController'
                })
                .state('InternalRosterSchoolReport_v2', {
                    url: '/Season/:seasonDomain/Report/Default/InternalRosterSchoolReport_v2',
                    templateUrl: '/Dashboard/CustomReport/InternalRosterSchoolReport_v2',
                    controller: 'internalRosterSchoolReportController_v2'
                })
                .state('InternalRosterSchoolReport_v3', {
                    url: '/Season/:seasonDomain/Report/Default/InternalRosterSchoolReport_v3',
                    templateUrl: '/Dashboard/CustomReport/GetView/InternalRosterSchoolReport_V3',
                    controller: 'internalRosterSchoolReportController_v3'
                })
                  .state('TeacherParticipantsClassReport', {
                      url: '/Reports/teacherParticipantsClassReport/Run/:seasonId/:teacherName',
                      templateUrl: '/Dashboard/PortalReport/TeacherParticipantsClass',
                      controller: 'teacherParticipantsClassReportController'
                  })
                 .state('GraduateReports', {
                     url: '/Reports/ParticipantByGradeReports/Grade/:grade',
                     templateUrl: 'Dashboard/PortalReport/ParticipantByGradeReports',
                     controller: 'participantByGradeReportController'
                 })
                .state('OrganizationSettings', {
                    url: '/Settings/Organization',
                    templateUrl: 'Dashboard/Setting/Organization',
                    controller: 'organizationSettingsController'
                }).state('AccountSettings', {
                    url: '/Settings/Account',
                    templateUrl: 'Dashboard/AccountSetting/AccountOverview',
                    controller: 'accountSettingsController'

                }).state('AccountSettingChangePlan', {
                    url: '/Settings/Account/Plan',
                    templateUrl: 'Dashboard/AccountSetting/AccountPlan',
                    controller: 'accountSettingsPlanController'
                }).state('SubscriptionSetting', {
                    url: '/Settings/Account/Plan/Subscription',
                    templateUrl: 'Dashboard/AccountSetting/Subscription',
                    controller: 'accountSubscriptionController'
                }).state('AccountSettingBilling', {
                    url: '/Settings/Account/Billing',
                    templateUrl: 'Dashboard/AccountSetting/AccountBillingList',
                    controller: 'accountSettingsBillingListController'
                }).state('BillingHistoryReceipt', {
                    url: '/Settings/Account/Billing/Receipt/:billingId/',
                    templateUrl: 'Dashboard/AccountSetting/BillingReceipt',
                    controller: 'accountSettingsBillingReceiptController'
                }).state('AccountSettingManageBilling', {
                    url: '/Settings/Account/Billing/Manage/:type/:cardId',
                    templateUrl: 'Dashboard/AccountSetting/AccountBilling',
                    controller: 'accountSettingsBillingController'
                }).state('AccountSettingBillingHistory', {
                    url: '/Settings/Account/BillingHistory',
                    templateUrl: 'Dashboard/AccountSetting/AccountBillingHistory',
                    controller: 'accountSettingsBillingHistoryController'
                }).state('PartnerSettings', {
                    url: '/Settings/Partner',
                    templateUrl: 'Dashboard/Setting/PartnerSetting',
                    controller: 'partnerSettingsController'

                })
                .state('ToursJbHomeSite', {
                    url: '/Settings/ToursJbHomeSite',
                    templateUrl: 'Dashboard/Setting/ToursJbHomeSite',
                    controller: 'toursJbHomeSiteController'
                }).state('TourJbHomeSite', {
                    url: '/Settings/TourJbHomeSite/:tourId',
                    templateUrl: 'Dashboard/Setting/TourJbHomeSite',
                    controller: 'tourJbHomeSiteController'
                }).state('JbHomeSiteSettings', {
                    url: '/Settings/JbHomeSite',
                    template: '',
                    controller: 'jbHomeSiteSettingsController'
                }).state('AdvanceSetting', {
                    url: '/Settings/Advance',
                    template: '',
                    controller: 'advanceSettingsController'
                }).state('MobileAppSetting', {
                    url: '/Settings/MobileApp',
                    template: '',
                    controller: 'mobileAppSettingsController'
                }).state('PolicySetting', {
                    url: '/Settings/Policies',
                    template: '',
                    controller: 'policySettingsController'
                }).state('AccountSettingInsurance', {
                    url: '/Settings/Advance/Insurance',
                    templateUrl: 'Dashboard/AccountSetting/AccountInsurance',
                    controller: 'accountSettingsInsuranceController'
                }).state('AdvanceSettingBackgroundCheck', {
                    url: '/Settings/Advance/BackgroundCheck',
                    templateUrl: 'Dashboard/AdvanceSetting/BackgroundCheck',
                    controller: 'advanceSettingsBackgroundCheckController'
                }).state('LogoSetting', {
                    url: '/Settings/Advance/LogoDisplaySetting',
                    templateUrl: 'Dashboard/AdvanceSetting/LogoSetting',
                    controller: 'logoSettingController'
                }).state('AutoChargePolicy', {
                    url: '/Settings/Policies/AutoCharge',
                    templateUrl: 'Dashboard/Setting/GetView/AutoChargePolicy',
                    controller: 'autoChargePolicyController'
                }).state('DelinquentPolicy', {
                    url: '/Settings/Policies/Delinquent',
                    templateUrl: 'Dashboard/Setting/GetView/DelinquentPolicy',
                    controller: 'delinquentPolicyController'
                }).state('ProgramsPolicy', {
                    url: '/Settings/Policies/Programs',
                    templateUrl: 'Dashboard/Setting/GetView/ProgramsPolicy',
                    controller: 'programsPolicyController'
                }).state('DuplicateEnrollmentPolicy', {
                    url: '/Settings/Policies/DuplicateEnrollment',
                    templateUrl: 'Dashboard/Setting/GetView/DuplicateEnrollmentPolicy',
                    controller: 'duplicateEnrollmentPolicyController'
                }).state('JBContentsSettings', {
                    url: '/Settings/JBContents',
                    template: '',
                    controller: 'jbContentsSettingsController'
                }).state('EmailsContent', {
                    url: '/Settings/JBContents/Emails',
                    templateUrl: 'Dashboard/Setting/GetView/EmailsContent',
                    controller: 'emailsContentController'
                }).state('ReportsContent', {
                    url: '/Settings/JBContents/ReportsContent',
                    templateUrl: 'Dashboard/Setting/GetView/ReportsContent',
                    controller: 'reportsContentController'
                }).state('CatalogsContent', {
                    url: '/Settings/JBContents/CatalogsContent',
                    templateUrl: 'Dashboard/Setting/GetView/CatalogsContent',
                    controller: 'catalogsContentController'
                }).state('HolidaysSetting', {
                    url: '/Settings/Holidays',
                    templateUrl: 'Dashboard/Setting/Holidays',
                    controller: 'holidaysSettingController'
                })
                .state('SchoolSetting', {
                    url: '/Settings/School',
                    template: '',
                    controller: 'schoolSettingsController'
                }).state('SchoolSettingInfo', {
                    url: '/Settings/School/Info',
                    templateUrl: 'Dashboard/Setting/SchoolInfo',
                    controller: 'schoolSettingsInfoController'
                }).state('SchoolSettingProgramInfo', {
                    url: '/Settings/School/ProgramInfo',
                    templateUrl: 'Dashboard/Setting/SchoolProgramInfo',
                    controller: 'schoolSettingsProgramInfoController'
                }).state('SchoolSettingOnSiteInfo', {
                    url: '/Settings/School/OnSiteInfo',
                    templateUrl: 'Dashboard/Setting/SchoolOnSiteInfo',
                    controller: 'schoolSettingsOnSiteInfoController'
                }).state('MobileAppSettingOnSiteInfo', {
                    url: '/Settings/School/MobileAppOnSiteInfo',
                    templateUrl: 'Dashboard/Setting/GetView/MobileAppOnSiteInfo',
                    controller: 'mobileAppSettingsOnSiteInfoController'
                }).state('PaymentSettings', {
                    url: '/Settings/Payment/:callbackMessage',
                    templateUrl: 'Dashboard/Setting/Payment',
                    controller: 'paymentSettingsController'
                }).state('CheckOutSetting', {
                    url: '/Settings/CheckOut',
                    templateUrl: 'Dashboard/Setting/CheckOut',
                    controller: 'checkOutSettingsController'
                }).state('AppearanceSetting', {
                    url: '/Settings/Appearance',
                    templateUrl: 'Dashboard/Setting/Appearance',
                    controller: 'appearanceSettingsController'
                }).state('ManageStaffs', {
                    url: '/Staff/ManageStaffs',
                    templateUrl: 'Dashboard/Staff/ManageStaffs',
                    controller: 'manageStaffsController'
                }).state('InviteStaff', {
                    url: '/Staff/Add',
                    templateUrl: 'Dashboard/Staff/AddEditStaff',
                    controller: 'manageStaffController'
                }).state('AddStaff', {
                    url: '/Staff/Add/:staffRole',
                    templateUrl: 'Dashboard/Staff/AddEditStaff',
                    controller: 'manageStaffController'
                }).state('AddStaffFromPartner', {
                    url: '/Staff/AddFromPartner',
                    templateUrl: 'Dashboard/Staff/AddStaffFromPartner',
                    controller: 'addStaffFromPartnerController'
                }).state('Staff', {
                    url: '/Staff',
                    template: '',
                    controller: 'staffController'
                }).state('EditStaff', {
                    url: '/Staff/Edit/:staffId',
                    templateUrl: 'Dashboard/Staff/AddEditStaff',
                    controller: 'manageStaffController'
                })

                /* admin follow up form */
                .state('OrderItemFollowForm', {
                    url: '/FollowupForms/:fId',
                    templateUrl: '/Dashboard/AdminFollowupForm',
                    controller: 'adminFollowupFormsController'
                })
                /* for people */

                .state('People', {
                    url: '/People',
                    template: '',
                    controller: 'registerantPeopleController'
                })
                .state('Catalog', {
                    url: '/Catalog',
                    template: '',
                    controller: 'catalogsController'
                })
                .state('CatalogList', {
                    url: '/Catalog',
                    templateUrl: '/Dashboard/Catalog/List',
                    controller: 'catalogsController'
                })
                  .state('ViewCatalogs', {
                      url: '/Catalogs/View/:providerId/:catalogId',
                      templateUrl: '/Dashboard/Catalog/All', //this is view
                      controller: 'viewCatalogsController' // this is controller
                  })
                 .state('SearchCatalogs', {
                     url: '/Catalog/Search',
                     templateUrl: '/Dashboard/Catalog/CatalogsSearch',
                     controller: 'CatalogsSearchController'
                 })
                .state('AddCatalog', {
                    url: '/Catalog/Add',
                    templateUrl: 'Dashboard/Catalog/CreateEdit',
                    controller: 'createEditCatalogController'
                })
                .state('EditCatalog', {
                    url: '/Catalog/Edit/:catalogId',
                    templateUrl: 'Dashboard/Catalog/CreateEdit',
                    controller: 'createEditCatalogController'
                })
				  .state('ViewCatalog', {
				      url: '/Catalog/View/:catalogId',
				      templateUrl: 'Dashboard/Catalog/Display',
				      controller: 'viewCatalogController'
				  })
                   .state('inviteCatalog', {
                       url: '/Catalog/Invite/:catalogId',
                       templateUrl: 'Dashboard/Catalog/Invite',
                       controller: 'inviteCatalogController'
                   })
                .state('CatalogUpdateDate', {
                    url: '/Reports/Catalog/CatalogUpdate',
                    templateUrl: '/Dashboard/PortalReport/UpdateDate',
                    controller: 'UpdateController'
                })
                .state('SchoolMemberList', {
                    url: '/Reports/school/schoolmemberlist',
                    templateUrl: '/Dashboard/PortalReport/SchoolMemberList',
                    controller: 'SchoolMemberListController'
                })
                .state('CustomReportPortal', {
                    url: '/Reports/School/CustomReport',
                    templateUrl: '/Dashboard/PortalReport/CustomReport',
                    controller: 'CustomReportController'
                })
                .state('CustomReportPortalSelectPrograms', {
                    url: '/Reports/School/PortalReport/:reportType/:reportName/:reportId/Manage',
                    templateUrl: '/Dashboard/PortalReport/CustomReportSelectPrograms',
                    controller: 'CustomReportSelectProgramsController'
                })
                .state('CustomReportPortalAdd', {
                    url: '/Reports/School/CustomReport/Type/:customType/Add',
                    templateUrl: '/Dashboard/PortalReport/CustomReportAdd',
                    controller: 'CustomReportAddController'
                })
                .state('CustomReportPortalEdit', {
                    url: '/Reports/School/CustomReport/Type/:customType/:reportId/Edit',
                    templateUrl: '/Dashboard/PortalReport/CustomReportEdit',
                    controller: 'CustomReportEditController'
                })
                .state('CustomReportPortalRun', {
                    url: '/Reports/School/CustomReport/Type/:reportType/:reportName/:reportId/Run',
                    templateUrl: '/Dashboard/PortalReport/CustomReportRun',
                    controller: 'CustomReportRunController'
                })
                .state('Title1Discount', {
                    url: '/Reports/Catalog/Title1Discount',
                    templateUrl: '/Dashboard/PortalReport/Title1Discount',
                    controller: 'Title1DiscountController'
                })
                .state('SelectCounty', {
                    url: '/Reports/Catalog/SelectCounty',
                    templateUrl: '/Dashboard/PortalReport/SelectCounty',
                    controller: 'SelectCountyController'
                })
                .state('CatalogOverviewActivity', {
                    url: '/Reports/Catalog/CatalogOverviewActivity',
                    templateUrl: '/Dashboard/PortalReport/CatalogOverviewActivityReport',
                    controller: 'CatalogOverviewActivityReportController'
                })
                .state('CatalogDetailedActivities', {
                    url: '/Reports/Catalog/CatalogDetailedActivities',
                    templateUrl: '/Dashboard/PortalReport/CatalogDetailedActivitiesReport',
                    controller: 'CatalogDetailedActivitiesReportController'
                })
                .state('SelectSeason', {
                    url: '/Reports/Planning/:reportType/SelectSeason',
                    templateUrl: '/Dashboard/PortalReport/SelectSeason',
                    controller: 'SelectSeasonController'
                })
                .state('SelectSchoolSeason', {
                    url: '/Reports/Planning/:reportType/SelectSchool',
                    templateUrl: '/Dashboard/PortalReport/SelectSchoolSeason',
                    controller: 'SelectSchoolSeasonController'
                })
                .state('SelectProviderSeason', {
                    url: '/Reports/Planning/:reportType/SelectProvider',
                    templateUrl: '/Dashboard/PortalReport/SelectProviderSeason',
                    controller: 'SelectProviderSeasonController'
                })
                 .state('SharePortalReportAsAttchment', {
                     url: '/Report/:reportName/:seasonId/Share',
                     templateUrl: 'Dashboard/CustomReport/ShareAsAttachment',
                     controller: 'ShareReportController'
                 })
                .state('ProviderContactInfo', {
                    url: '/Reports/Planning/ProviderContactInfo/:seasonId',
                    templateUrl: '/Dashboard/PortalReport/ProviderContactInfo',
                    controller: 'ProviderContactInfoController'
                })
                .state('RoomAssignment', {
                    url: '/Reports/Planning/RoomAssignment/:seasonId',
                    templateUrl: '/Dashboard/PortalReport/RoomAssignments',
                    controller: 'RoomAssignmentController'
                })
                .state('SeasonProcessingDates', {
                    url: '/Reports/Planning/SeasonProcessingDates/:seasonTitle',
                    templateUrl: '/Dashboard/PortalReport/SeasonProcessingDates',
                    controller: 'SeasonProcessingController'
                })
                .state('ADM24Data', {
                    url: '/Reports/Planning/ADM24Data/:seasonId',
                    templateUrl: '/Dashboard/PortalReport/ADM24Data',
                    controller: 'ADM24DataController'
                })
                .state('InsuranceReport', {
                    url: '/Reports/Planning/Insurance',
                    templateUrl: '/Dashboard/PortalReport/InsuranceReport',
                    controller: 'InsuranceReportController'
                })
                .state('CountyProviders', {
                    url: '/Reports/Catalog/CountyProviders/:county',
                    templateUrl: '/Dashboard/PortalReport/CountyProviders',
                    controller: 'CountyProvidersController'
                })
                .state('CatalogSettings', {
                    url: '/Catalog/Settings',
                    templateUrl: 'Dashboard/Catalog/Settings',
                    controller: 'catalogSettingsController'
                })
                 .state('Families', {
                     url: '/People/Families',
                     templateUrl: 'Dashboard/People/Families',
                     controller: 'familiesController'
                 })
                 .state('FamilyDetail', {
                     url: '/People/FamilyDetail/:userId/:clubId',
                     templateUrl: 'Dashboard/People/FamilyDetail',
                     controller: 'familyDetailController'
                 })
                 .state('ManageFamily', {
                     url: '/People/Family/:userId/:clubId/Manage',
                     templateUrl: 'Dashboard/People/ManageFamily',
                     controller: 'manageFamilyController'
                 })
                .state('FamilyOrders', {
                    url: '/People/FamilyOrders/:userId/:clubId',
                    templateUrl: 'Dashboard/People/FamilyOrders',
                    controller: 'familyOrdersController'
                })
                .state('FamilyInvoicesOrTakePayment', {
                    url: '/People/Family/:userId/:type/:orderId/:clubId/:orderItemId/:IsToBack',
                    templateUrl: 'Dashboard/People/ManageInvoicesOrTakePayment',
                    controller: 'manageFamilyInvoicesOrTakePaymentController'
                })
                 //.state('FamilyTakePayment', {
                 //    url: '/People/Family/:userId/TakePayment/:orderId',
                 //    templateUrl: 'Dashboard/People/familyTakePayment',
                 //    controller: 'familyTakePaymentController'
                 //})
                .state('DependentCareReceiptReport', {
                    url: '/People/Family/:userId/:clubId/DependentCareReceipt',
                    templateUrl: 'Dashboard/People/DependentCareReceiptReport',
                    controller: 'DependentCareReceiptController'
                })
                .state('ConfirmFamilyInvoice', {
                    url: '/People/Family/Invoice/Create/:userId/:clubId/:orderItemId',
                    templateUrl: 'Dashboard/People/ConfirmFamilyInvoice',
                    controller: 'confirmFamilyInvoiceController'
                })
                .state('DisplayInvoiceMessage', {
                    url: '/People/Family/Invoice/Review&Send/:userId/:orderItemId',
                    templateUrl: 'Dashboard/People/DisplayInvoiceMessage',
                    controller: 'displayInvoiceMessageController'
                })
                 .state('FamilyTransactions', {
                     url: '/People/FamilyTransactions/:userId/:clubId',
                     templateUrl: 'Dashboard/People/FamilyTransactions',
                     controller: 'familyTransactionsController'
                 })
                .state('RegistrantPeople', {
                    url: '/People/Registrant',
                    templateUrl: 'Dashboard/People/Registrant',
                    controller: 'registerantPeopleController'
                })

                .state('ParentRegistration', {
                    url: '/People/Parent',
                    templateUrl: 'Dashboard/People/ParentRegistration',
                    controller: 'parentRegisteredController'
                })

                .state('RegistrationDetail', {
                    url: '/People/Registrant/:userId/:playerId/:clubId',
                    templateUrl: 'Dashboard/People/RegistrationDetail',
                    controller: 'registerantPeopleController'
                })

                .state('ParticipantClassSchedule', {
                    url: '/People/ParticipantClassSchedule/:playerId',
                    templateUrl: 'Dashboard/People/GetView/ParticipantClassSchedule',
                    controller: 'participantClassSchedulePeopleController'
                })

                .state('ParentDetail', {
                    url: '/People/Parent/:userId/:playerId',
                    templateUrl: 'Dashboard/People/ParentDetail',
                    controller: 'parentDetailController'
                })

                .state('Delinquents', {
                    url: '/People/Delinquents',
                    templateUrl: 'Dashboard/People/GetView/Delinquents',
                    controller: 'delinquentsController'
                })

                .state('ViewHistory', {
                    url: '/People/ViewHistory/:RelatedEntity/:RelatedEntityId',
                    templateUrl: 'Dashboard/People/GetView/DelinquentViewHistory',
                    controller: 'delinquentViewHistoryController'
                })

                .state('SendEmail', {
                    url: '/Email/SendEmail',
                    templateUrl: 'Dashboard/Email/GetView/SendEmail',
                    controller: 'sendEmailController'
                })

                .state('AddHistory', {
                    url: '/People/AddHistory/:orderItemId',
                    templateUrl: 'Dashboard/People/GetView/DelinquentAddHistory',
                    controller: 'delinquentAddHistoryController'
                })

                 .state('AddClubUsers', {
                     url: '/People/AddUser/:type',
                     templateUrl: 'Dashboard/People/AddUser',
                     controller: 'addclubUsersController'
                 })
                .state('EditEmailList', {
                    url: '/Campaigns/EditEmailList/:listId',
                    templateUrl: 'Dashboard/EditEmailListRecipients',
                    controller: 'campaignsController'
                })

                .state('EmailCampaign', {
                    url: '/Campaigns/Email/:campaignId',
                    templateUrl: 'Dashboard/EmailCampaign',
                    controller: 'campaignController'
                })

                .state('EmailCampaignAddBlade', {
                    url: '/Campaigns/Emails/Add',
                    template: '',
                    controller: 'campaignAddBladeController'
                })

                .state('EmailCampaignAddSomePeopleStep1', {
                    url: '/Campaigns/Emails/Manage/SomePeople/Step1/:campaignId',
                    templateUrl: '/Dashboard/Campaign/EmailCampaignSomePeopleStep1',
                    controller: 'campaignAddSomePeopleStep1controller'
                })

                .state('EmailCampaignAddUserListStep1', {
                    url: '/Campaigns/Emails/Manage/UserList/Step1/:campaignId',
                    templateUrl: '/Dashboard/Campaign/EmailCampaignUserListStep1',
                    controller: 'campaignAddUserListStep1controller'
                })
                .state('EmailCampaignAddStep2', {
                    url: '/Campaigns/Emails/Manage/:campaignType/Step2/:campaignId',
                    templateUrl: '/Dashboard/Campaign/EmailCampaignStep2',
                    controller: 'campaignAddStep2controller'
                })
                .state('EmailCampaignAddStep3', {
                    url: '/Campaigns/Emails/Manage/:campaignType/Step3/:campaignId',
                    templateUrl: '/Dashboard/Campaign/EmailCampaignStep3',
                    controller: 'campaignAddStep3controller'
                })
                 .state('EmailCampaignPreviewStep4', {
                     url: '/Campaigns/Emails/Manage/:campaignType/Step4/:campaignId',
                     templateUrl: '/Dashboard/Campaign/EmailCampaignStep4',
                     controller: 'campaignAddStep4controller'
                 })
                .state('EmailListRecipients', {
                    url: '/Campaigns/EmailList/Manage/:listId/Recipients',
                    templateUrl: 'Dashboard/EmailListRecipients',
                    controller: 'campaignsController'
                })
                .state('EmailList', {
                    url: '/Campaigns/EmailList/Manage/NewList',
                    templateUrl: '/Dashboard/Campaign/CreateNewEmailList',
                    controller: 'campaignAddUserListStep1controller'
                })
                .state('ReviewCampaign', {
                    url: '/Campaigns/Emails/Manage/Review/:campaignId',
                    templateUrl: '/Dashboard/Campaign/ReviewSentCampaign',
                    controller: 'reviewSentCampaignController'
                })

                /* For Registration Email template */

                .state('RegistrationEmailTemplateDesigner', {
                    url: '/Season/:seasonDomain/EmialTemplate/Manage/:templateId',
                    templateUrl: 'Email/_RegistrationEmailTemplateDesigner',
                    controller: 'registrationEmailTemplateController'
                })

                /*.state('EmailCampaignAdd', {
                    url: '/Campaigns/Emails/Manage/:campaignType/:campaignId',
                    templateUrl: 'Dashboard/EmailCampaignAdd',
                    controller: 'campaignAddController'
                })*/
                .state('RecipientList', {
                    url: '/Program/Emails/Manage/Recipients/:programId',
                    templateUrl: 'Dashboard/ClassRecipientList',
                    controller: 'recipientListController'

                })
                .state('EmailCampaignDisplayAllRecipients', {
                    url: '/Campaigns/Emails/Manage/:campaignType/Recipients/:campaignId',
                    templateUrl: 'Dashboard/RecipientList',
                    controller: 'campaignAddController'

                }).state('EmailCampaignTemplates', {
                    url: '/Campaigns/Emails/Templates',
                    templateUrl: 'Dashboard/EmailCampaignTemplates',
                    controller: 'campaignTemplatesController'
                }).state('EmailCampaignTemplateAddEdit', {
                    url: '/Campaigns/Emails/Template/Manage/:templateId',
                    templateUrl: 'Dashboard/EmailCampaignTemplateAddEdit',
                    controller: 'campaignAddController'
                }).state('Reports', {
                    url: '/Season/:seasonDomain/Reports',
                    template: '',
                    controller: 'reportsController'
                }).state('RegistrationDataReports', {
                    url: '/Season/:seasonDomain/Reports/RegistrationData',
                    template: '',
                    controller: 'registrationDataReportsController'
                }).state('FinanceReport', {
                    url: '/Season/:seasonDomain/Reports/Finance',
                    template: '',
                    controller: 'financeReportController'
                }).state('RosterReports', {
                    url: '/Season/:seasonDomain/Reports/Roster',
                    template: '',
                    controller: 'rosterReportsController'
                }).state('CapacityReports', {
                    url: '/Season/:seasonDomain/Reports/Capacity',
                    template: '',
                    controller: 'capacityReportsController'
                }).state('CustomReports', {
                    url: '/Season/:seasonDomain/Reports/Custom',
                    templateUrl: 'Dashboard/Reports',
                    controller: 'reportsController'
                }).state('ParticipantEmail', {
                    url: '/Season/:seasonDomain/Program/Summary/:programId/ParticipantEmail/:userId/:orderItemId',
                    templateUrl: '/Dashboard/Campaign/ParticipantEmail',
                    controller: 'participantEmailController'
                })
                .state('ParentsNotification', {
                    url: '/Season/:seasonDomain/Program/Summary/:programId/ParentsNotification/:typeNotification',
                    templateUrl: '/Dashboard/Program/ProgramParentsNotification',
                    controller: 'programParentsNotificationController'
                })
                .state('SubscriptionsReports', {
                    url: '/Season/:seasonDomain/Reports/Subscriptions',
                    template: '',
                    controller: 'subscriptionsReportsController'
                })
                .state('MedicalReports', {
                    url: '/Season/:seasonDomain/Reports/Medical',
                    template: '',
                    controller: 'medicalReportsController'
                })
                .state('OperationsReports', {
                    url: '/Season/:seasonDomain/Reports/OperationsReports',
                    template: '',
                    controller: 'operationsReportsController'
                })
                .state('MobileAppReports', {
                    url: '/Season/:seasonDomain/Reports/MobileApp',
                    template: '',
                    controller: 'mobileAppReportsController'
                })
                /* Report Path. */
                .state('ReportAdd', {
                    url: '/Season/:seasonDomain/Reports/:customType/Add',
                    templateUrl: 'Dashboard/CustomReport/Report',
                    controller: 'reportAddController'
                })

                .state('ReportEdit', {
                    url: '/Season/:seasonDomain/Report/:reportId/Edit',
                    templateUrl: 'Dashboard/CustomReport/Report',
                    controller: 'reportEditController'
                })

                .state('CapacityReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/Capacity/Run',
                    templateUrl: 'Dashboard/CustomReport/CapacityReport',
                    controller: 'CapacityReportController'
                })

                .state('InstallmentReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/Installment/Run',
                    templateUrl: 'Dashboard/CustomReport/InstallmentReport',
                    controller: 'InstallmentReportController'
                })
                  .state('ProviderFinancialRun', {
                      url: '/Season/:seasonDomain/Report/Default/ProviderFinancial/Run',
                      templateUrl: 'Dashboard/CustomReport/ProviderFinancialReport',
                      controller: 'ProviderFinancialReportController'
                  })
                .state('CoordinatorFinancialRun', {
                    url: '/Season/:seasonDomain/Report/Default/CoordinatorFinancial/Run',
                    templateUrl: 'Dashboard/CustomReport/CoordinatorFinancialReport',
                    controller: 'CoordinatorFinancialReportController'
                })
                .state('FollowupReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/Follow-up/Run',
                    templateUrl: 'Dashboard/CustomReport/FollowupFormReport',
                    controller: 'FollowupReportController'
                })

                .state('BalanceReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/Balance/Run',
                    templateUrl: 'Dashboard/CustomReport/BalanceReport',
                    controller: 'BalanceReportController'
                })

                .state('ChargesReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/ChargeDiscount/Run',
                    templateUrl: 'Dashboard/CustomReport/ChargesReport',
                    controller: 'ChargesReportController'
                })

                .state('FinanceReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/Finance/Run',
                    templateUrl: 'Dashboard/CustomReport/Finance',
                    controller: 'FinanceController'
                })
                .state('GeneralTransactionFinanceReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/TransactionFinance/Run',
                    templateUrl: 'Dashboard/CustomReport/FinanceTransaction',
                    controller: 'generalTransactionFinanceController'
                })
                .state('CouponReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/Coupon/Run',
                    templateUrl: 'Dashboard/CustomReport/CouponReport',
                    controller: 'CouponReportController'
                })
                .state('CheckinReportRun', {
                    url: 'Season/:seasonDomain/Report/Default/InstructorCheckin',
                    templateUrl: '/Dashboard/CustomReport/CheckinReport',
                    controller: 'CheckinReportController'
                })
                 .state('StudentAttendanceReportRun', {
                     url: '/Season/:seasonDomain/Report/Default/StudentAttendance/Run',
                     templateUrl: '/Dashboard/CustomReport/StudentAttendanceReport',
                     controller: 'StudentAttendanceReportController'
                })
                .state('StudentSummaryAttendanceReport', {
                    url: '/Season/:seasonDomain/Report/Default/StudentSummaryAttendance/Run',
                    templateUrl: '/Dashboard/CustomReport/StudentSummaryAttendanceReport',
                    controller: 'StudentSummaryAttendanceReportController'
                })
                .state('CustomAttendanceReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/AttendanceSheet/Run',
                    templateUrl: '/Dashboard/CustomReport/CustomAttendanceReport',
                    controller: 'CustomAttendanceReportController'
                })
                .state('SignOutSheetReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/SignOutSheet/Run',
                    templateUrl: '/Dashboard/CustomReport/SignOutSheetReport',
                    controller: 'SignOutSheetReportController'
                })
                .state('DismissalInformationReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/DismissalInformation/Run',
                    templateUrl: '/Dashboard/CustomReport/DismissalInformationReport',
                    controller: 'DismissalInformationReportController'
                })
                .state('DailyDismissalReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/DailyDismissal/Run',
                    templateUrl: '/Dashboard/CustomReport/DailyDismissalReport',
                    controller: 'DailyDismissalReportController'
                })
                .state('RunReport', {
                    url: '/Report/:seasonDomain/:reportName/Run',
                    templateUrl: '/Dashboard/Report/GetView/RunReport',
                    controller: 'runReportController'
                })
                .state('ProviderContactInformationReport', {
                    url: '/Season/:seasonDomain/Report/Default/ProviderContactInformation/Run',
                    templateUrl: '/Dashboard/CustomReport/ProviderContactInformationReport',
                    controller: 'ProviderContactInformationReportController'
                })
                .state('RosterReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/Roster/Run',
                    templateUrl: '/Dashboard/CustomReport/RosterReport',
                    controller: 'RosterReportController'
                })
                .state('AdvanceClassRosterReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/AdvanceClassRoster/Run',
                    templateUrl: '/Dashboard/CustomReport/AdvanceRosterReport',
                    controller: 'AdvanceRosterReportController'
                })
                .state('AdvanceClassRosterReportProgramSchedulerRun', {
                    url: '/Season/:seasonDomain/:programId/Report/Default/AdvanceClassRosterCalendar/Run',
                    templateUrl: '/Dashboard/CustomReport/GetView/AdvanceRosterReportProgramScheduler',
                    controller: 'AdvanceRosterReportProgramSchedulerController'
                })
                .state('CampRosterReport', {
                    url: '/Season/:seasonDomain/Report/Default/CampRoster',
                    templateUrl: '/Dashboard/CustomReport/CampRosterReport',
                    controller: 'CampRosterReportController'
                })
                   .state('RosterReportEdit', {
                       url: '/Season/:seasonDomain/Report/Roster/:reportId/Edit',
                       templateUrl: '/Dashboard/CustomReport/RosterReportEdit',
                       controller: 'RosterReportEditController'
                })
                .state('RosterReportEditProgramScheduler', {
                    url: '/Season/:seasonDomain/Report/Roster/:reportId/Calendar/:programId/Edit',
                    templateUrl: '/Dashboard/CustomReport/GetView/RosterReportEditProgramScheduler',
                    controller: 'RosterReportEditProgramSchedulerController'
                })
                .state('SubscriptionRosterReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/SubscriptionRoster/Run',
                    templateUrl: '/Dashboard/CustomReport/SubscriptionRosterReport',
                    controller: 'SubscriptionRosterReportController'
                })
                .state('InstructorRosterReportRun', {
                    url: '/Season/:seasonDomain/Report/Default/InstructorRoster/Run',
                    templateUrl: '/Dashboard/CustomReport/InstructorRosterReport',
                    controller: 'instructorRosterReportController'
                })
                .state('ReportRun', {
                    url: '/Season/:seasonDomain/Report/:reportType/:reportName/:reportId/Run',
                    templateUrl: 'Dashboard/CustomReport/ReportRun',
                    controller: 'reportRunController'
                })
                .state('ParticipantNoteReport', {
                    url: '/Season/:seasonDomain/Report/ParticipantNote',
                    templateUrl: 'Dashboard/CustomReport/GetView/ParticipantNoteReport',
                    controller: 'participantNoteReportController'
                })
                 .state('SubscriptionSingOutReportRun', {
                     url: '/Season/:seasonDomain/Report/SingOut/Run',
                     templateUrl: '/Dashboard/CustomReport/SubscriptionSingOutReport',
                     controller: 'subscriptionSingOutReportController'
                 })
                 .state('SubscriptionInternalReportRun', {
                     url: '/Season/:seasonDomain/Report/Internal/Run',
                     templateUrl: '/Dashboard/CustomReport/SubscriptionInternalReport',
                     controller: 'subscriptionInternalReportController'
                 })
                 .state('SubscriptionAttendanceReportRun', {
                     url: '/Season/:seasonDomain/Report/Attendance/Run',
                     templateUrl: '/Dashboard/CustomReport/SubscriptionAttendanceReport',
                     controller: 'subscriptionAttendanceReportController'
                 })
                 .state('PreRegistrationRun', {
                     url: '/Season/:seasonDomain/Report/PreRegistration',
                     templateUrl: 'Dashboard/CustomReport/PerRegistrationReport',
                     controller: 'preRegistrationReportController'
                 })
                .state('DeafultReportSelectPrograms', {
                    url: '/Season/:seasonDomain/Report/Default/:reportName/Manage',
                    templateUrl: 'Dashboard/CustomReport/ReportSelectPrograms',
                    controller: 'DefaultReportRunController'
                })
                .state('DeafultReportSelectProviders', {
                    url: '/Reports/Planning/:reportType/providerReport',
                    templateUrl: 'Dashboard/PortalReport/ReportSelectProviders',
                    controller: 'providerReportRunController'
                })
                .state('InstructorListPortalReports', {
                    url: '/Reports/Planning/InstructorListPortal',
                    templateUrl: '/Dashboard/PortalReport/InstructorListPortalReport',
                    controller: 'InstructorListPortalReportController'
                })
                 .state('InstructorListReport', {
                     url: '/Season/:seasonDomain/Report/Default/InstructorList',
                     templateUrl: '/Dashboard/CustomReport/InstructorListReport',
                     controller: 'InstructorListReportController'
                 })
                .state('InstructorAssignmentsReport', {
                    url: '/Season/:seasonDomain/Report/Default/InstructorAssignments',
                    templateUrl: '/Dashboard/CustomReport/InstructorAssignmentsReport',
                    controller: 'InstructorAssignmentsReportController'
                })
                .state('ParentEmailsListReport', {
                    url: '/Season/:seasonDomain/Report/Default/ParentEmailsList',
                    templateUrl: '/Dashboard/CustomReport/ParentEmailsListReport',
                    controller: 'ParentEmailsListReportController'
                })
               .state('DeafultReportSelectSchools', {
                   url: '/Reports/SelectedSchoolReport',
                   templateUrl: 'Dashboard/PortalReport/ReportSelectSchools',
                   controller: 'selectedSchoolReportRunController'
               })
               .state('InvoicesPortalReports', {
                   url: '/Reports/Finance/InvoicesReport',
                   templateUrl: '/Dashboard/PortalReport/InvoicesListPortalReport',
                   controller: 'InvoicesListPortalReportController'
               })
               .state('ReminderInvoicesFromReports', {
                   url: '/Reports/Finance/InvoicesReport/Reminder',
                   templateUrl: '/Dashboard/PortalReport/ReminderInvoicesListFromReport',
                   controller: 'reminderInvoicesListFromReportController'
               })
                .state('RoomAssignmentsAdminReport', {
                    url: '/Season/:seasonDomain/Report/Default/RoomAssignments',
                    templateUrl: '/Dashboard/CustomReport/RoomAssignmentsAdminReport',
                    controller: 'RoomAssignmentsAdminReportController'
                })
                .state('SignOutSheetAdvanced', {
                    url: '/Season/:seasonDomain/Report/Default/SignOutSheetAdvanced',
                    templateUrl: '/Dashboard/CustomReport/SignOutSheetAdvancedReport',
                    controller: 'SignOutSheetAdvancedReportController'
                })
                .state('SignOutSheetAdvancedV2', {
                    url: '/Season/:seasonDomain/Report/Default/SignOutSheetAdvancedV2',
                    templateUrl: '/Dashboard/CustomReport/SignOutSheetAdvancedV2Report',
                    controller: 'SignOutSheetAdvancedV2ReportController'
                })
                .state('SignOutSheetAdvancedV3', {
                    url: '/Season/:seasonDomain/Report/Default/SignOutSheetAdvancedV3',
                    templateUrl: '/Dashboard/CustomReport/GetView/SignOutSheetAdvancedV3Report',
                    controller: 'SignOutSheetAdvancedV3ReportController'
                })
                .state('SignOutSheetAdvancedV4', {
                    url: '/Season/:seasonDomain/Report/Default/SignOutSheetAdvancedV4',
                    templateUrl: '/Dashboard/CustomReport/GetView/SignOutSheetAdvancedV4Report',
                    controller: 'SignOutSheetAdvancedV4ReportController'
                })
                .state('AttendanceSheetAdvanced', {
                    url: '/Season/:seasonDomain/Report/Default/AttendanceSheetAdvanced',
                    templateUrl: '/Dashboard/CustomReport/AttendanceSheetAdvancedReport',
                    controller: 'AttendanceSheetAdvancedReportController'
                })
                .state('CustomReportSelectPrograms', {
                    url: '/Season/:seasonDomain/Report/:reportType/:reportName/:reportId/Manage',
                    templateUrl: 'Dashboard/CustomReport/ReportSelectPrograms',
                    controller: 'DefaultReportRunController'
                })

                .state('ShareAsAttchment', {
                    url: '/Season/:seasonDomain/Report/:reportName/:seasonId/Share',
                    templateUrl: 'Dashboard/CustomReport/ShareAsAttachment',
                    controller: 'ShareReportController'
                })

                .state('FormUpload', {
                    url: '/FormUpload/:programId',
                    templateUrl: 'Dashboard/Form/GetView/FormUpload',
                    controller: 'FormUploadController'
                })
                .state('SupplementalFormView', {
                    url: '/Form/:seasonDomain/SupplementalForm/:formId',
                    templateUrl: '/Dashboard/Form/GetView/SupplementalFormViewDetail',
                    controller: 'supplementalFormViewController'
                })
                .state('Form', {
                    url: '/Form/:seasonDomain/:formType/:templateId',
                    templateUrl: 'Dashboard/Form',
                    controller: 'formController'

                })
                .state('FollowUpForms', {
                    url: '/FollowUpForms/:programId/:seasonDomain',
                    templateUrl: 'Dashboard/FollowUpForms',
                    controller: 'followupFormsController'
                })
                .state('Toolbox', {
                    url: '/Toolbox',
                    template: '',
                    controller: 'toolboxController'
                })
                .state('ClubForms', {
                    url: '/Toolbox/Forms',
                    template: '',
                    controller: 'clubFormsController'
                })
                .state('GoogleAnalytics', {
                    url: '/Toolbox/GoogleAnalytics',
                    templateUrl: 'Dashboard//Toolbox/GetView/GoogleAnalytics',
                    controller: 'googleAnalyticsController'
                })
                .state('ClubFollowUpForms', {
                    url: '/Toolbox/Forms/FollowUpForms',
                    templateUrl: 'Dashboard/FollowUpForms',
                    controller: 'followupFormsController'
                })
                .state('ClubLocations', {
                    url: '/Location/Manage/:action/:locationId',
                    templateUrl: 'Dashboard/Club/ManageLocation',
                    controller: 'clubLocationController'
                }).state('Waivers', {
                    url: '/Waivers/:seasonDomain',
                    templateUrl: 'Dashboard/Club/Waivers',
                    controller: 'waiversController'
                })
                .state('SelectDiscountDate', {
                    url: '/Reports/DiscountReports/:reportType/Selectdate',
                    templateUrl: '/Dashboard/PortalReport/SelectDisocuntDate',
                    controller: 'SelectDiscountDateController'
                })
                .state('AllDiscountReports', {
                    url: '/Reports/DiscountReports/ScholarshipDiscount/Run',
                    templateUrl: '/Dashboard/PortalReport/AllDiscountReports',
                    controller: 'AllDiscountReportsController'
                })
                 .state('DiscountReport', {
                     url: '/Reports/DiscountReports/Discount/Run',
                     templateUrl: '/Dashboard/PortalReport/DiscountReport',
                     controller: 'DiscountReportController'
                 })
                .state('ScholarshipReport', {
                    url: '/Reports/DiscountReports/Scholarship/Run',
                    templateUrl: '/Dashboard/PortalReport/ScholarshipReport',
                    controller: 'ScholarshipReportController'
                })
                .state('Discounts', {
                    url: '/Season/:seasonDomain/Discounts',
                    templateUrl: 'Dashboard/Discount/Discounts',
                    controller: 'discountsController'
                }).state('DiscountsAdd', {
                    url: '/Season/:seasonDomain/Discounts/Add',
                    template: '',
                    controller: 'discountsAddController'
                })
                .state('Charges', {
                    url: '/Season/:seasonDomain/Setup/Charges',
                    templateUrl: 'Dashboard/Charge/Charges',
                    controller: 'chargesController'
                })
                 .state('ChargesAdd', {
                     url: '/Season/:seasonDomain/Charges/Add',
                     template: '',
                     controller: 'chargesAddController'
                 })
                .state('ApplicationFee', {
                    url: '/Season/:seasonDomain/Charges/:type/ApplicationFee',
                    templateUrl: 'Dashboard/Charge/ApplicationFee',
                    controller: 'applicationFeeController'
                })
                .state('MultiScheduleDiscountManage', {
                    url: '/Season/:seasonDomain/Discounts/Manage/MultiSchedule/:discountId',
                    templateUrl: 'Dashboard/Discount/MultiScheduleDiscountManage',
                    controller: 'multiScheduleDiscountController'
                })
                .state('DiscountManage', {
                    url: '/Season/:seasonDomain/Discounts/Manage/:discountCategory/:discountId',
                    templateUrl: 'Dashboard/Discount/DiscountManage',
                    controller: 'discountManageController'
                })
                //add for general discount
                 .state('GeneralDiscountManage', {
                     url: '/Season/:seasonDomain/Discounts/GeneralDiscount/:discountId',
                     templateUrl: 'Dashboard/Discount/GeneralDiscountManage',
                     controller: 'generalDiscountController'
                 })
                .state('PaymentPlans', {
                    url: '/Season/:seasonDomain/PaymentPlans',
                    templateUrl: 'Dashboard/PaymentPlan/PaymentPlans',
                    controller: 'paymentPlansController'
                }).state('PaymentPlanManage', {
                    url: '/Season/:seasonDomain/PaymentPlans/Manage/:paymentPlanId',
                    templateUrl: 'Dashboard/PaymentPlan/PaymentPlanManage',
                    controller: 'paymentPlanManageController'
                }).state('Tours', {
                    url: '/Tours',
                    templateUrl: 'Dashboard/Tours',
                    controller: 'toursController'
                }).state('Tour', {
                    url: '/Tour/:tourId',
                    templateUrl: 'Dashboard/Tour',
                    controller: 'tourController'
                })
                .state('FullcapacityChart', {
                    url: '/Season/:seasonDomain/Capacity-Chart',
                    templateUrl: 'Dashboard/Season/CapacityFullChart',
                    controller: 'CapacityChartController'
                })
                .state('FullSaleChart', {
                    url: '/Season/:seasonDomain/SaleOverTime-Chart',
                    templateUrl: 'Dashboard/Season/SaleOverTimeFullChart',
                    controller: 'SaleOverTimeController'
                })
                .state('FullTotalSaleChart', {
                    url: '/Season/:seasonDomain/TotalSale-Chart',
                    templateUrl: 'Dashboard/Season/TotalSaleFullChart',
                    controller: 'TotalSalesController'
                })
                .state('FullTotalSeasonPaidAmountChart', {
                    url: '/Season/:seasonDomain/TotalSaleDetail-Chart',
                    templateUrl: 'Dashboard/Season/TotalPaidAmountSeasonChart',
                    controller: 'TotalPaidAmountSeasonChartController'
                })
                .state('SeasonProgramScheduler', {
                    url: '/Season/:seasonDomain/Calendar',
                    templateUrl: 'Dashboard/Season/GetView/ProgramScheduler',
                    controller: 'programSchedulerController'
                })
                .state('ClubProgramScheduler', {
                    url: '/Calendar',
                    templateUrl: 'Dashboard/Season/GetView/ProgramScheduler',
                    controller: 'programSchedulerController'
                })
                .state('Donation', {
                    url: '/Donation',
                    templateUrl: 'Dashboard/Donation/Index',
                    controller: 'DonationController'
                }).state('DonationDiplay', {
                    url: '/Donation/Display/:orderItemId',
                    templateUrl: 'Dashboard/Donation/Display',
                    controller: 'DonationDisplayController'
                }).state('DonationsReport', {
                    url: '/Reports/Donation',
                    templateUrl: '/Dashboard/PortalReport/DonationsReport',
                    controller: 'DonationsReportController'
                }).state('ParentMailingListReport', {
                    url: '/Reports/ParentMailingList',
                    templateUrl: '/Dashboard/PortalReport/ParentMailingListReport',
                    controller: 'parentMailingListReportController'
                })
                .state('CustomReportClubReport', {
                    url: '/Reports/CustomReport',
                    templateUrl: '/Dashboard/PortalReport/CustomReport',
                    controller: 'CustomReportController'
                })
                .state('FamilyCreditCardReport', {
                    url: '/Reports/FamilyCreditCardList',
                    templateUrl: '/Dashboard/PortalReport/FamilyCreditCardListReport',
                    controller: 'familyCreditCardListReportController'
                })
                .state('BulkDependentCareReceiptReport', {
                    url: '/Reports/BulkDependentCareReceipt',
                    templateUrl: '/Dashboard/PortalReport/BulkDependentCareReceiptReport',
                    controller: 'BulkDependentCareReceiptReportController'
                })
                .state('SystemClubs', {
                    url: '/System/Clubs',
                    templateUrl: 'Dashboard/System/Clubs',
                    controller: 'systemClubsController'
                })
                .state('SystemClubManage', {
                    url: '/Club/:clubId/Manage',
                    templateUrl: 'Dashboard/System/ClubManage',
                    controller: 'systemClubManageController'
                })
                .state('SystemClubShowPlan', {
                    url: '/System/Club/:clubId/ShowPlan',
                    templateUrl: 'Dashboard/System/ShowPlan',
                    controller: 'systemClubShowPlanController'
                })
               .state('SystemFinanceReport', {
                   url: '/System/Reports/Finance',
                   template: '',
                   controller: 'systemFinanceReportsController'
                })
                .state('SystemAutoChargeReports', {
                    url: '/System/Reports/AutoCharge',
                    template: '',
                    controller: 'systemAutoChargeReportsController'
                })
                 .state('ReportsSystemFinanceInstallments', {
                     url: '/System/Reports/Installments',
                     templateUrl: '/Dashboard/System/InstallmentReport',
                     controller: 'systemFinanceInstallmentsController'
                 })
              .state('SystemTransaction', {
                  url: '/System/Reports/Finance/:reportType/AllTransaction',
                  templateUrl: 'Dashboard/System/SelectTransaction',
                  controller: 'selectTransactionController'
              })
                .state('SystemRevenue', {
                    url: '/System/Reports/Finance/:reportType/ShowRevenue',
                    templateUrl: 'Dashboard/System/SystemRevenue',
                    controller: 'systemRevenueReportController'
                })
                .state('SystemRevenueViewDetails', {
                    url: '/System/Reports/Finance/:reportType/Details/:date',
                    templateUrl: 'Dashboard/System/RevenueDetailes',
                    controller: 'systemRevenueReportDetailesController'
                })
                .state('SystemRevenueMatrix', {
                    url: '/System/Reports/Finance/:reportType/ShowRevenueMatrix',
                    templateUrl: 'Dashboard/System/RevenueMatrix',
                    controller: 'systemRevenueMatrixReportController'
                })
                .state('SystemClientCreditCards', {
                    url: '/System/Reports/ShowClientCreditCards',
                    templateUrl: 'Dashboard/System/SystemClientCreditCards',
                    controller: 'systemClientCreditCardsReportController'
                })
                .state('AllInstallments', {
                    url: '/System/Reports/Showinstallments',
                    templateUrl: 'Dashboard/System/installments',
                    controller: 'installmentsReportController'
                })
            .state('SystemClubBillingHistory', {
                url: '/System/Club/:clubId/BillingHistory',
                templateUrl: 'Dashboard/System/BillingHistory',
                controller: 'systemClubBillingHistoryController'
            })
            .state('SystemBillingHistoryReceipt', {
                url: '/System/Billing/Club/:clubId/Receipt/:billingId',
                templateUrl: 'Dashboard/System/BillingReceipt',
                controller: 'clubBillingReceiptController'
            })
            .state('SystemClubRefund', {
                url: '/System/Club/:clubId/Refund/:billingId',
                templateUrl: 'Dashboard/System/RefundTransaction',
                controller: 'systemRefundTransactionController'
            })
            .state('SystemConfirmRefund', {
                url: '/System/Club/:clubId/ConfirmRefund/:billingId',
                templateUrl: 'Dashboard/System/ConfirmRefundTransaction',
                controller: 'systemConfirmRefundController'
            })
            .state('AddDebitForClub', {
                url: '/System/Club/:clubId/BillingHistory/AddDebit',
                templateUrl: 'Dashboard/System/AddDebit',
                controller: 'systemAddDebitForClubController'
            })
             .state('ConfirmAddDebitForClub', {
                 url: '/System/Club/:clubId/BillingHistory/ConfirmDebit',
                 templateUrl: 'Dashboard/System/ConfirmDebit',
                 controller: 'systemConfirmDebitForClubController'
             })
            .state('SystemChargeClub', {
                url: '/System/Club/:clubId/BillingHistory/ChargeClub',
                templateUrl: 'Dashboard/System/ChargeClub',
                controller: 'systemChargeForClubController'
            })
                .state('SystemConfirmChargeClub', {
                    url: '/System/Club/:clubId/BillingHistory/ChargeClub/ConfirmChargeClub',
                    templateUrl: 'Dashboard/System/ConfirmChargeClub',
                    controller: 'systemConfirmChargeClub'
                })
            .state('SystemPaymentWithCredit', {
                url: '/System/Club/:clubId/BillingHistory/Credit',
                templateUrl: 'Dashboard/System/Credit',
                controller: 'systemCreditController'
            })
            .state('SystemConfirmCredit', {
                url: '/System/Club/:clubId/BillingHistory/Credit/ConfirmCredit',
                templateUrl: 'Dashboard/System/ConfirmCredit',
                controller: 'systemConfirmCreditController'
            })
            .state('MessagingInbox', {
                url: '/Messaging/Inbox',
                template: '',
                controller: 'inboxController'
            })
            .state('Messaging', {
                url: '/Messaging/:chatId',
                templateUrl: '/Dashboard/Messaging/Index',
                controller: 'messagingController'
            })
            .state('ESignature', {
                url: '/ESignature',
                templateUrl: '/Dashboard/ESignature/ManageESignature',
                controller: 'eSignatureController'
            })
            .state('AddESignature', {
                url: '/ESignature/Add',
                templateUrl: '/Dashboard/ESignature/CreateEditESignature',
                controller: 'addEditeESignatureController'
            })
            .state('Flyers', {
                url: '/Flyer/',
                template: '',
                controller: 'flyersController'
            })
           .state('FlyerTemplates', {
               url: '/Flyer/Templates',
               templateUrl: 'Dashboard/Flyer/FlyerTemplates',
               controller: 'flyerTemplatesController'
           }).state('FlyersSetting', {
               url: '/Flyer/FlyersSetting',
                    templateUrl: 'Dashboard/Flyer/FlyersSetting',
                    controller: 'flyersSettingController'
                }).state('FlyerEdit', {
               url: '/Flyer/Manage/:flyerId',
               templateUrl: 'Dashboard/Flyer/EditFlyer',
               controller: 'flyerEditController'
           }).state('ClubFlyers', {
               url: '/Flyer/ClubFlyers',
               templateUrl: 'Dashboard/Flyer/ClubFlyers',
               controller: 'clubFlyersController'
           })
                .state('TemplateAfterSchoolClassesPreview1', {
                url: '/Flyer/Manage',
                    templateUrl: 'Dashboard/Flyer/GetView/TemplateAfterSchoolClassesPreview1',
                    controller: 'templateAfterSchoolClassesPreview1Controller'
                })
                .state('TemplateAfterSchoolClassesPreview2', {
                    url: '/Flyer/AfterSchoolClassesManage',
                    templateUrl: 'Dashboard/Flyer/GetView/TemplateAfterSchoolClassesPreview2',
                    controller: 'templateAfterSchoolClassesPreview2Controller'
                })
                .state('TemplateAfterSchoolClassesPreview3', {
                    url: '/Flyer/AfterSchoolClassesManage',
                    templateUrl: 'Dashboard/Flyer/GetView/TemplateAfterSchoolClassesPreview3',
                    controller: 'templateAfterSchoolClassesPreview3Controller'
                })
                .state('TemplateClassCatalogPreview1', {
                url: '/Flyer/ClassCatalogManage',
                    templateUrl: 'Dashboard/Flyer/GetView/TemplateClassCatalogPreview1',
                    controller: 'templateClassCatalogPreview1Controller'
                }).state('TemplateClassCatalogPreview2', {
                    url: '/Flyer/TemplateClassCatalogManage',
                    templateUrl: 'Dashboard/Flyer/GetView/TemplateClassCatalogPreview2',
                    controller: 'templateClassCatalogPreview2Controller'
                })
            .state('FillFlyerData', {
                url: '/Flyer/FillData',
                templateUrl: 'Dashboard/Flyer/FillFlyerData',
                controller: 'fillFlyerDataController'
            })
            //.state('EditESignature', {
            //    url: '/ESignature/:documentId/Edit',
            //    templateUrl: '/Dashboard/ESignature/CreateEditESignature',
            //    controller: 'addEditeESignatureController'
            //})
            .state('SignDocumentESignature', {
                url: '/ESignature/Sign/:documentId',
                templateUrl: '/Dashboard/ESignature/ViewDocumentESignature',
                controller: 'signESignatureController'
            })
            .state('SettingDocumentSign', {
                url: '/ESignature/Setting',
                templateUrl: '/Dashboard/ESignature/DocumentSignSetting',
                controller: 'settingDocumentSignController'
            })
            .state('WeeklyAgenda', {
                url: '/WeeklyAgenda',
                template: '',
                controller: 'weeklyAgendaController'
            })
            .state('ManageWeeklyAgenda', {
                url: '/WeeklyAgenda/Manage',
                templateUrl: '/Dashboard/WeeklyAgenda/Manage',
                controller: 'manageWeeklyAgendaController'
            })
             .state('WeeklyAgendaSettings', {
                 url: '/WeeklyAgenda/Settings',
                 templateUrl: '/Dashboard/WeeklyAgenda/Settings',
                 controller: 'weeklyAgendaSettingsController'
             })
             .state('Subsidies', {
                 url: '/Member/Subsidies',
                 templateUrl: 'Dashboard/Club/ClubSubsidiesManage',
                 controller: 'subsidiesController'
             })
             .state('AddSubsidy', {
                 url: '/Member/AddSubsidy',
                 templateUrl: 'Dashboard/Club/AddSubsidy',
                 controller: 'addSubsidyController'
             })
             .state('AddSubsidyStep2', {
                 url: '/Member/AddSubsidy/AdditionalInformation/:subsidyId',
                 templateUrl: 'Dashboard/Club/AdditionalInformation',
                 controller: 'additionalInformationSubsidyController'
             })
            .state('SchoolDistrict', {
                url: '/Member/Districts',
                templateUrl: 'Dashboard/Club/SchoolDistrictManage',
                controller: 'schoolDistrictController'
            })
            .state('AddClubDistrict', {
                url: '/Member/AddDistrict',
                templateUrl: 'Dashboard/Club/AddDistrict',
                controller: 'addClubDistrictController'
            })
             .state('AddClubDistrictStep2', {
                 url: '/Member/AddDistrict/Step2/:clubId',
                 templateUrl: 'Dashboard/Club/AddDistrictStep2',
                 controller: 'addClubDistrictStep2Controller'
             })
             .state('Contacts', {
                 url: '/Contacts',
                 templateUrl: '/Dashboard/Contact/GetView/List',
                 controller: 'contactsController'
             })
             .state('AddContact', {
                 url: '/Contact/Add',
                 templateUrl: '/Dashboard/Contact/GetView/Create',
                 controller: 'addContactController'
             })
            .state('EditContact', {
                url: '/Contact/Edit/:contactId',
                templateUrl: '/Dashboard/Contact/GetView/Edit',
                controller: 'editContactController'
            })
            .state('ContactSettings', {
                url: '/Contact/Settings',
                templateUrl: '/Dashboard/Contact/GetView/Settings',
                controller: 'contactSettingsController'
            })
            .state('SeasonReplicate', {
                url: '/Member/AddDistrict/Step2/:clubId/SeasonReplicate/:memberId',
                templateUrl: 'Dashboard/Club/SeasonReplicate',
                controller: 'seasonReplicateController'
            })
            .state('OrderItemPunchCardAssignment', {
                url: '/Season/:seasonDomain/OrderItem/:orderItemId/PunchCard/:sessionId',
                templateUrl: 'Dashboard/Order/GetPunchCardAssignment',
                controller: 'punchCardAssignmentController'
                })
            .state('ActivityLog', {
                url: '/ActivityLog',
                templateUrl: 'Dashboard/ActivityLog/GetView/List',
                controller: 'ToolboxactivityLogsController'
            })
            .state('ActivityLogDetail', {
                url: '/ActivityLog/:id',
                templateUrl: 'Dashboard/ActivityLog/GetView/Detail',
                controller: 'activityLogDetailController'
            }).state('SystemCampaigns', {
                url: '/Campaigns/SystemCampaigns',
                template: '',
                controller: 'systemCampaignsController'
              })
            .state('ClassConfirmation', {
                url: '/Campaigns/ClassConfirmation/Step1',
                templateUrl: 'Dashboard/Campaign/GetView/ClassConfirmationStep1',
                controller: 'classConfirmationController'
                })
            .state('SaveClassConfirmationStep2', {
                url: '/Campaigns/ClassConfirmation/Step2',
                templateUrl: 'Dashboard/Campaign/GetView/ClassConfirmationStep2',
                 controller: 'classConfirmationStep2Controller'
                })
            .state('UndoFamilyCredit', {
                url: '/Season/:seasonDomain/Order/UndoFamilyCredit/:orderItemId/:transacTionId',
                templateUrl: 'Dashboard/Order/GetView/UndoRefund',
                controller: 'undoFamilyCredit'
                });
        }]);
})();