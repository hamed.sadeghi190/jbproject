﻿using System;

namespace Jumbula.Common.Exceptions
{
    public class JumbulaAccountCannotConfirmException : JumbulaAccountException
    {
        public JumbulaAccountCannotConfirmException(Exception e)
            : base(e)
        {
        }
    }
}
