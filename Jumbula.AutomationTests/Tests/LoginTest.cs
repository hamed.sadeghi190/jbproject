﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
    public class LoginTest : BaseTest<LoginElements>
    {
        HomePageElements homepageElementProvider;
        public LoginTest(IWebDriver webDriver)
            :base(webDriver)
        {
            homepageElementProvider = new ElementProvider<HomePageElements>().Get(WebDriver);
        }


   

        public void LoginAdmin()
        {
            Elements.UsernameTextbox.SendKeys(DataDriver.AdminUsername);
            Elements.PasswordTextbox.SendKeys(DataDriver.AdminPassword);
            Elements.LoginButton.Click();
        }
        public void LoginPartner()
        {
            Elements.UsernameTextbox.SendKeys(DataDriver.PartnerUsername);
            Elements.PasswordTextbox.SendKeys(DataDriver.PartnerPassword);
            Elements.LoginButton.Click();
            ExtraAPI.Wait(30000);
        }
        public void LoginQa()
        {
            Elements.UsernameTextbox.SendKeys("qa@jumbula.com");
            Elements.PasswordTextbox.SendKeys("12345678");
            Elements.LoginButton.Click();
            ExtraAPI.Wait(30000);
        }
        public void Login(string username, string password, bool wait=true)
        {
            Elements.UsernameTextbox.Clear();
            Elements.UsernameTextbox.SendKeys(username);
            Elements.PasswordTextbox.Clear();
            Elements.PasswordTextbox.SendKeys(password);
            Elements.LoginButton.Click();
            if (wait)
            {
                ExtraAPI.Wait(30000);
            }
            else
            {
                ExtraAPI.Wait(5000);
            }
            
        }
        
    }
}