﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('teacherParticipantsClassReportController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'county-providers-view';
            window.app.globalObjects.initializeView();

            $scope.MC.openBlade({ url: "Dashboard/Blade/RostersReports", order: 1, menu: "PortalReports" });

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            $scope.stateParams = $stateParams;
            $scope.MC.stateParams = $stateParams;
            $scope.Model = {};


            var nameModel = customReportService.getModelFromStrorage();
            var seasonId = nameModel.seasonId;
            var schoolId = nameModel.schoolId;
            var teacherName = nameModel.teacherName;
          

            $scope.TeachersReportGridOptions = {
                dataSource: {
                    type: "json",
                    transport: {
                        read: {
                            url: '/Dashboard/PortalReport/SelectTeacherParticipantsClass?seasonId=' + seasonId + '&schoolId=' + schoolId + '&teacherName=' + $scope.MC.stateParams.teacherName,
                        }
                    },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                    serverSorting: false,
                    serverPaging: true,
                    pageSize: 50,
                    resizable: true,
                    batch: true,
                },
                scrollable: true,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                           5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                editable: false,
                excel: {
                    allPages: true,
                    fileName: "Export.xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },

                columns: [
                       {
                           field: "TeacherName",
                           title: "Teacher",
                           width: "100px",
                       },
                       {
                           field: "ParticipantName",
                           title: "Participant",
                           width: "200px",
                       },
                       {
                           field: "ClassName",
                           title: "Class",
                           width: "250px",
                       },
                        {
                            field: "ClassDays",
                            title: "Days",
                            width: "100px",
                        },
                        {
                            field: "Room",
                            title: "Room",
                            width: "100px",
                        }],
            };

            $scope.saveToPdf = function () {
              
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = '/Dashboard/PortalReport/GenerateTeacherPdf?seasonId=' + seasonId + '&schoolId=' + schoolId + '&teacherName=' + $scope.MC.stateParams.teacherName;
                a.download = "ReportTeachers.Pdf";
                a.click();
            };

            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                $("#TeachersReportGrid").getKendoGrid().saveAsExcel();
                cfpLoadingBar.complete();
            }

        }
    ]);


})();