﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('accountSettingsBillingReceiptController', ['$scope', '$http', '$stateParams', '$state', function ($scope, $http, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/AccountSettings", order: 2, menu: "SETTINGS" });
        $scope.baseUrl = window.location.origin + "/Dashboard/AccountSetting";

       
        $http.get($scope.baseUrl + '/GetBillingHistoryDetail?billingId=' + $stateParams.billingId).success(function (data, status, headers, config) {
            $scope.Model = data;

        });

    }])

})();