﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('flyersController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbConfirmation, jbToast) {
        window.app.globalObjects.pageClass = 'Flyers_View';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/Flyers", order: 0, menu: "ESIGNATURE" });
        $scope.baseUrl = window.location.origin + '/Dashboard/Flyers';

    }])

})();
