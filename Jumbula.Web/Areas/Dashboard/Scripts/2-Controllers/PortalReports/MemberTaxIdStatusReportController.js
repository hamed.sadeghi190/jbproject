﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('MemberTaxIdStatusReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};
        var isBegin = false;

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.activeStatus = [{ Value: -1, Title: "All" }, { Value: 1, Title: "Active" }, { Value: 2, Title: "Inactive" }];
        $scope.filter.activeStatus = 1;

        $scope.filter.printMode = false;

        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.ProviderNames = data.ProviderNames;
            dataInfo = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $http.get('/Dashboard/Club/ClubPage').success(function (data, status, headers, config) {
            $scope.Model.ClubType = data;
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });
        $scope.filter.typeId = -1;

        var model = $scope.Model;

        var manipulateMemberTaxIdStatusGrid = function () {

            isBegin = true;

            $scope.MemberTaxIdStatusDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetMemberTaxIdStatusReportInfo",
                        dataType: "json",
                        type: "POST",
                        data: function () {
                            return {
                                startDate: $scope.filter.startDate,
                                endDate: $scope.filter.endDate,
                                typeId: $scope.filter.typeId,
                                activeStatus: $scope.filter.activeStatus,
                                printMode: $scope.filter.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.MemberTaxIdStatusGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Member tax id status).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Member tax ID status - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.MemberTaxIdStatusDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#MemberTaxIdStatusGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "Name",
                        title: "Name",
                        width: "210px"
                    }, {
                        field: "TaxIdFEIN",
                        title: "Tax ID FEIN",
                        width: "130px"
                    }, {
                        field: "TaxIdSSN",
                        title: "Tax ID SSN",
                        width: "130px"
                    }, {
                        field: "Provider1099",
                        title: "Provider 1099",
                        width: "80px"
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $scope.filter.printMode = false;
            $("#MemberTaxIdStatusGrid").getKendoGrid().saveAsExcel();

        }

        $scope.saveToPDF = function () {
            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetMemberTaxIdStatusReportInfo', {
                startDate: $scope.filter.startDate,
                endDate: $scope.filter.endDate,
                typeId: $scope.filter.typeId,
                activeStatus: $scope.filter.activeStatus,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (MemberTaxIDStatus).pdf";
                a.click();
            });
            $scope.filter.printMode = false;

        }

        $scope.print = function () {
            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetMemberTaxIdStatusReportInfo', {
                startDate: $scope.filter.startDate,
                endDate: $scope.filter.endDate,
                typeId: $scope.filter.typeId,
                activeStatus: $scope.filter.activeStatus,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (MemberTaxIDStatus).pdf";
                a.click();
            });
            $scope.filter.printMode = false;
        }

        $scope.search = function () {
            if (!isBegin) {
                manipulateMemberTaxIdStatusGrid();
            } else {
                $scope.filter.printMode = false;
                $("#MemberTaxIdStatusGrid").data("kendoGrid").dataSource.read();
            }

        };

        $scope.clearFilters = function () {
            $scope.filter.endDate = "";
            $scope.filter.startDate = "";
            $scope.filter.typeId = -1;
            $scope.filter.activeStatus = -1;
            $scope.filter.printMode = false;
            $("#MemberTaxIdStatusGrid").data("kendoGrid").dataSource.read();
        }


    }
    ]);

})();