﻿using Jumbula.Common.Enums;

namespace Jumbula.Core.Model.Email
{
    public class ResetPasswordSuccessParameterModel
    {
        public string UserName { get; set; }

        public RequestType RequestType { get; set; }

        public int? ClubId { get; set; }
    }
}
