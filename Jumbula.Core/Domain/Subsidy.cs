﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class Subsidy : BaseEntity
    {
        public int PartnerId { get; set; }
        [ForeignKey("PartnerId")]
        public virtual Club Club { get; set; }

        public string Name { get; set; }
        public string Website { get; set; }
        public int State { get; set; }
        public MetaData MetaData { get; set; }
        public int? ContactId { get; set; }
        [ForeignKey("ContactId")]
        public virtual ContactPerson Contact { get; set; }
        public string Instructions { get; set; }

    }
}
