﻿(function () {
    "use strict";

    angular.module("dashboardApp").controller("classProfitsReportsController", ["$scope", "$http", "$stateParams", "jbToast", "cfpLoadingBar", "customReportService", function ($scope, $http, $stateParams, jbToast, cfpLoadingBar, customReportService) {
        window.app.globalObjects.pageClass = "county-providers-view";
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value) {
            if (value.Domain === $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });
        var checkValidation = function () {
            if ($scope.Model.Name === "SelectSeason") {
                jbToast.error("Please select a season.");
                return false;
            }
            if ($scope.Model.Year === "") {
                jbToast.error("Please select a year.");
                return false;
            }
            else {
                return true;
            }
        }
        $scope.baseUrl = window.location.origin + "/Dashboard/PortalReport";

        $scope.Model = {};
        $scope.IsReportRun = false;

        $http.get($scope.baseUrl + "/GetSchoolsEnrollmentsInfo").success(function (data) {
            $scope.Model = data;

        }).error(function (data) {
            jbToast.error("error " + data);
        });

        $scope.runReport = function () {

            if (checkValidation()) {

                $scope.IsReportRun = true;
                $scope.classProfitsDataSource = new kendo.data.DataSource({
                    dataType: "json",
                    transport:
                        {
                            read: {
                                url: $scope.baseUrl + "/GetClassProfits",
                                type: "POST",
                                data: function () {
                                    return {
                                        name: $scope.Model.Name,
                                        year: $scope.Model.Year
                                    };
                                }

                            }
                        },
                    schema: {
                        total: "TotalCount",
                        data: function (response) {

                            $scope.TotalParentFee = response.TotalParentFee;
                            $scope.TotalGrossIncome = response.TotalGrossIncome;
                            $scope.NumberPTAFees = response.NumberPTAFees;
                            $scope.TotalPTAFee = response.TotalPTAFee;
                            $scope.TotalGrossPTAFee = response.TotalGrossPTAFee;
                            $scope.TotalVendorFee = response.TotalVendorFee;
                            $scope.TotalGrossVendorFee = response.TotalGrossVendorFee;
                            $scope.TotalPayToVendor = response.TotalPayToVendor;
                            $scope.TotalFXAProfit = response.TotalFXAProfit;
                            $scope.TotalLeadGen = response.TotalLeadGen;
                            $scope.TotalMgmtFee = response.TotalMgmtFee;
                            return response.DataSource;
                        },
                        model: {
                            fields: {

                                ParentFee: { type: "number" }
                            }
                        }
                    },
                    serverSorting: false,
                    serverPaging: true,
                    pageSize: 50,
                    resizable: true
                });

                $scope.ClassProfitsReportGridOptions = {
                    excel: {
                        allPages: true,
                        fileName: "ClassProfits_Report.xlsx",
                        proxyURL: "/Dashboard/PortalReport/Save"
                    },
                    excelExport: function (e) {

                        var rows = e.workbook.sheets[0].rows;

                        rows.unshift({
                            cells: [{
                                value: ""
                            }]
                        });

                        rows.unshift({
                            cells: [{
                                value: "Total FXA profit: " + $scope.TotalFXAProfit
                            }, {
                                value: "Total lead gen: " + $scope.TotalLeadGen
                            }, {
                                value: "Total mgmt fee: " + $scope.TotalMgmtFee
                            }]
                        });

                        rows.unshift({
                            cells: [{
                                value: "Total vendor fee: " + $scope.TotalVendorFee
                            }, {
                                value: "Total gross vendor fee: " + $scope.TotalGrossVendorFee
                            }, {
                                value: "Total pay to vendor: " + $scope.TotalPayToVendor
                            }]
                        });

                        rows.unshift({
                            cells: [{
                                value: "PTA fee: " + $scope.NumberPTAFees
                            }, {
                                value: "Total PTA fee: " + $scope.TotalPTAFee
                            }, {
                                value: "Total gross PTA fee: " + $scope.TotalGrossPTAFee
                            }]
                        });

                        rows.unshift({
                            cells: [{
                                value: "Total parent fee: " + $scope.TotalParentFee
                            }, {
                                value: "Total gross income: " + $scope.TotalGrossIncome
                            }]
                        });

                        rows.unshift({
                            cells: [{
                                value: ""
                            }]
                        });

                        rows.unshift({
                            cells: [{
                                value: "Class profits report"
                            }]
                        });

                        cfpLoadingBar.complete();
                    },
                    serverPaging: true,
                    dataSource: $scope.classProfitsDataSource,
                    resizable: true,
                    pageable: {
                        refresh: true,
                        pageSizes:
                            [
                                5, 10, 20, 30, 50, 100, 200
                            ],
                        buttonCount: 50
                    },
                    dataBound: function () {
                        var grid = $("#ClassProfitsReportGrid").data("kendoGrid");
                        customReportService.initializeDoubleScroll(grid);

                    },
                    scrollable: true,
                    sortable: { mode: "single", allowUnsort: false },
                    columns: [
                        {
                            field: "ProviderName",
                            title: "Vendor",
                            width: "160px"
                        },
                        {
                            field: "SchoolName",
                            title: "School name",
                            width: "200px"
                        },
                        {
                            field: "ClassName",
                            title: "Class name",
                            width: "160px"
                        },
                        {
                            field: "NumberOfWeeks",
                            title: "# Weeks",
                            width: "160px"
                        },
                        {
                            field: "Days",
                            title: "Days",
                            width: "160px"
                        },
                        {
                            field: "StrStudents",
                            title: "Students",
                            width: "160px"
                        },
                        {
                            field: "StrParentFee",
                            title: "Parent Fee",
                            width: "160px"

                        },
                        {
                            field: "StrGrossIncome",
                            title: "Gross Income",
                            width: "160px"
                        },
                        {
                            field: "StrPTAFee",
                            title: "PTA Fee",
                            width: "160px"
                        },
                        {
                            field: "StrTotalPTAFee",
                            title: "Total PTA Fee",
                            width: "160px"
                        },
                        {
                            field: "StrGrossPTAFee",
                            title: "Gross - PTA Fee",
                            width: "160px"
                        },
                        {
                            field: "StrVendorFee",
                            title: "Vendor Fee",
                            width: "160px"
                        },
                        {
                            field: "StrGrossVendorFee",
                            title: "Gross Vendor Fee",
                            width: "160px"
                        },
                        {
                            field: "StrPaytoVendor",
                            title: "Pay to Vendor",
                            width: "160px"
                        },
                        {
                            field: "StrFXAProfit",
                            title: "FXA Profit",
                            width: "160px"
                        },
                        {
                            field: "StrLeadGen",
                            title: "Lead Gen $",
                            width: "160px"
                        },
                        {
                            field: "StrMgmtFee",
                            title: "Mgmt Fee $",
                            width: "160px"
                        },
                        {
                            field: "StrPercentLeadGen",
                            title: "Lead Gen %",
                            width: "160px"
                        },
                        {
                            field: "StrPercentMgmtFee",
                            title: "Mgmt Fee %",
                            width: "160px"
                        }
                    ]
                }
                $("#ClassProfitsReportGrid").data("kendoGrid").dataSource.read();

            }
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ClassProfitsReportGrid").getKendoGrid().saveAsExcel();

        }

    }]);
})();