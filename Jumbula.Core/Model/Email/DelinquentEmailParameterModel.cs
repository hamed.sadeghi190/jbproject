﻿using System.Collections.Generic;

namespace Jumbula.Core.Model.Email
{
    public class DelinquentEmailParameterModel : CustomizableEmailParameterModel
    {
        public List<DelinquentEmailParameterRelatedEntityItemModel> RelatedEntity { get; set; }
    }
}
