﻿namespace Jumbula.Common.Constants
{
    public static partial class Constants
    {
        public static class SolutionConstants
        {
            public const string CoreProjectAssemblyName = "Jumbula.Core";
            public const string DomainClassesNamespace = "Jumbula.Core.Domain";
            public const string WebProjectAssemblyName = "Jumbula.Web";
            public const string ReportBuildersNamespace = "Jumbula.Business.ReportBuilders";
        }
    }
}
