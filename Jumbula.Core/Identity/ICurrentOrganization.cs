﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Identity
{
    public interface ICurrentOrganization
    {
        int Id { get; set; }

        string Domain { get; set; }

        string Name { get; set; }

        Jumbula.Common.Enums.TimeZone? TimeZone { get; set; }

        CurrencyCodes Currency { get; set; }

        ClubType ClubType { get; set; }

        string Site { get; set; }

        string Logo { get; set; }

        bool HasPartner { get; }

        string PartnerLogo { get; set; }

        string PartnerSite { get; set; }
    }
}
