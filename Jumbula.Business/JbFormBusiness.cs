﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Jumbula.Business
{
    public class JbFormBusiness :  IJbFormBusiness
    {
        private readonly IRepository<JbForm> _jbFormRepository;
        public JbFormBusiness(IRepository<JbForm> jbFormRepository)
        {
            _jbFormRepository = jbFormRepository;
        }

        public JbForm Get(int id = 0)
        {
            return _jbFormRepository.Get(f => f.Id == id);
        }
        public IQueryable<JbForm> Get(string refEntityName)
        {
            return _jbFormRepository.GetList(x => x.RefEntityName == refEntityName);
        }
        public void Update(JbForm jbForm)
        {
            if (jbForm.RefEntityName == null)
                throw new Exception("RefEntityName and RefEntityId can not be null");
            jbForm.LastModifiedDate = DateTime.Now;
            _jbFormRepository.Update(jbForm);
        }

        public void Delete(int jbFormId)
        {
            _jbFormRepository.Delete(jbFormId);
        }

        public OperationStatus CreateEdit(JbForm jbForm)
        {
            if (jbForm.Id == 0)
            {
                jbForm.CreatedDate = DateTime.UtcNow;
                return _jbFormRepository.Create(jbForm);
            }
            else
            {
                jbForm.LastModifiedDate = DateTime.UtcNow;
                jbForm.JsonElements = jbForm.JsonElements.TrimEnd();
                jbForm.JsonElements = jbForm.JsonElements + " ";
                return _jbFormRepository.Update(jbForm);
            }
        }
        public OperationStatus UpdateJson(int formId, string jsonElements)
        {
            var form = Get(formId);
            form.JsonElements = jsonElements;
            return CreateEdit(form);
        }

        public dynamic GetAsPdf(int id)
        {
            var jbForm = Get(id);

            var sectionName = new List<string>();
            var elementName = new List<List<string>>();
            var elementValue = new List<List<string>>();

            foreach (var section in jbForm.Elements)
            {
                sectionName.Add(section.Name);

                var name = new List<string>();
                var value = new List<string>();

                foreach (var element in ((JbSection)section).Elements)
                {
                    name.Add(element.Title);
                    value.Add(element.GetValue() != null ? element.GetValue().ToString() : string.Empty);
                }

                elementName.Add(name);
                elementValue.Add(value);
            }

            dynamic pdfModel = new System.Dynamic.ExpandoObject();
            pdfModel.SectionName = sectionName;
            pdfModel.ElementName = elementName;
            pdfModel.ElementValue = elementValue;
            
            return pdfModel;
        }

        public IJbBaseElement GetFormElement(JbForm jbForm, string elementName, string sectionName = null)
        {
            IJbBaseElement result = null;

            try
            {
                if (sectionName == null)
                {
                    if (jbForm.Elements.Select(e => (JbSection)e)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower())) != null)
                    {
                        result = jbForm.Elements
                            .Select(e => e as JbSection)
                            .SelectMany(e => e.Elements)
                            .SingleOrDefault(e => e.Name.ToLower()
                                .Equals(elementName.ToLower()));
                    }
                }
                else
                {
                    if (jbForm.Elements.Any(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase)) && ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.Any(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase)))
                    {
                        result = ((JbSection)jbForm.Elements.First(s => s.Name.Equals(sectionName, StringComparison.OrdinalIgnoreCase))).Elements.First(e => e.Name.Equals(elementName, StringComparison.OrdinalIgnoreCase));
                    }
                }
            }
            catch
            {
                result = null;
            }

            return result;
        }
    }
}
