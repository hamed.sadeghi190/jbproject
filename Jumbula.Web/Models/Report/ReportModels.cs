﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;
using Jumbula.Core.Domain.Generic;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Models
{
    public class ReportViewModel
    {
        [DataType(DataType.Date)]
        [Display(Name="Start Date:")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [Required(ErrorMessage = "Enter start date")]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "End Date:")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        [Required(ErrorMessage = "Enter end date")]
        public DateTime EndDate { get; set; }

        // get sum of all orderAmounts that is register between startdate and endDate
         [Display(Name = "Total Gross:")]
        public decimal TotalOrderAmount { get; set; }

        // get count of all orders that is register between startdate and endDate
        [Display(Name = "Total attendance:")]
        public int OrdersCount { get; set; }

        // get count of uniq users that register their Orders in date between startdate and endDate
        [Display(Name = "Uniq users count:")]
        public int UniqUsersCount { get; set; }

        //list of keyvalue class that key is date and value is sum of OrderAmount  in this date
        public List<KeyValue> AmountInDay { get; set; }
        public List<KeyValue> OrderCountChartInfo { get; set; }//list of 
        public List<KeyValue> UniqUsersChartInfo { get; set; }

        
        public List<KeyValue> PercentOfEachEvent { get; set; }

        public ReportViewModel()
        {
            AmountInDay = new List<KeyValue>();
            UniqUsersChartInfo = new List<KeyValue>();
            PercentOfEachEvent = new List<KeyValue>();
        }

        public ReportViewModel(List<Order> orders,DateTime startDate,DateTime endDate)
        {
            StartDate = startDate;

            EndDate = endDate;

            TotalOrderAmount = orders.Sum(c => c.OrderAmount);

            OrdersCount = orders.Count();

            //UniqUsersCount = orders.GroupBy(c => c.PlayerId).Count();

            AmountInDay = orders.GroupBy(c => c.CompleteDate.ToShortDateString()).Select(c => new KeyValue
            {
                Key = c.Key,
                Value = c.Sum(m => m.OrderAmount)
            }).ToList();
           
            
            //PercentOfEachEvent = orders.GroupBy(c => c.SubDomainCategory).Select(c => new KeyValue
            //    {
            //        Key = c.Key.ToString(),
            //        Value = (decimal)(((float)c.Count())/((float)OrdersCount))*100,

            //    }).ToList();
        


        }
         
    }

}