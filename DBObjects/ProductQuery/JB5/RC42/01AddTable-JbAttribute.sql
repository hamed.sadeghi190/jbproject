
/****** Object:  Table [dbo].[JbAttributes]    Script Date: 10/31/2017 3:45:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JbAttributes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [tinyint] NOT NULL,
	[Label] [nvarchar](max) NULL,
	[Type] [tinyint] NOT NULL,
	[ReferenceType] [tinyint] NOT NULL,
	[ShowInGrid] [bit] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsRequired] [bit] NOT NULL,
	[ClubId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.JbAttributes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[JbAttributes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.JbAttributes_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[JbAttributes] CHECK CONSTRAINT [FK_dbo.JbAttributes_dbo.Clubs_ClubId]
GO


