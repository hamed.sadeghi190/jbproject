﻿namespace Jumbula.Common.Enums
{
    public enum EmailCategory : byte
    {
        ResetPasswordSuccess = 0,
        AutoChargeSuccess = 1,
        AutoChargeFail = 2,
        AutoChargeSupportSummary = 3,
        RequestDemo = 4,
        AutoChargeSummary = 5,
        ResetPassword = 6,
        Delinquent = 7,
        RefundOrder = 8,
    }
}
