﻿
using Jb.Framework.Common;
using Jumbula.Core.Business;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;


namespace Jumbula.Business
{
    public class CategoryBusiness : ICategoryBusiness
    {
        private readonly IRepository<Category> _categoryRepository;
        private readonly IEntitiesContext _dataContext;
        public CategoryBusiness(IRepository<Category> categoryRepository, IEntitiesContext dataContext)
        {
            _categoryRepository = categoryRepository;
            _dataContext = dataContext;
        }

        public IQueryable<Category> GetList()
        {
            return _categoryRepository.GetList()
                     .OrderBy(x => x.ParentCategory.Name)
                     .ThenBy(x => x.Name);
        }

        public IQueryable<Category> GetList(IEnumerable<int> ids)
        {
            var result = _categoryRepository.GetList(e => ids.Contains(e.Id));

            return result;
        }

        public OperationStatus Update(Category category)
        {
            return _categoryRepository.Update(category);
        }

        public int Update()
        {
            return _dataContext.SaveChanges();
        }

        public List<SelectKeyValue<int>> GetCategories()
        {
            return GetList()
                .Where(e => e.ParentId.HasValue)
                .ToList().Select(x => new SelectKeyValue<int>()
                {
                    Group = x.ParentCategory.Name,
                    Text = x.Name,
                    Value = x.Id
                })
                .ToList();
        }
    }
}
