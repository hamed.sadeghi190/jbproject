﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('invoiceDetailController', ['$scope', '$http', '$stateParams', '$state', function ($scope, $http, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/Invoice";


        $scope.Model = {};

        $http.get($scope.baseUrl + '/GetInvoiceDetail?invoiceId=' + $stateParams.invoiceId).success(function (data, status, headers, config) {

            $scope.InfoModel = data.InfoModel;
            $scope.HistoryModel = data.HistoryModel;
            $scope.InvoiceItemModel = data.InvoiceItemModel;
        });


        $scope.pageMode;

        if ($state.current.name == 'InvoiceDetail') {

            $scope.pageMode = 'AdminInvoiceView'
        }
        else {
            $scope.pageMode = 'ParentInvoiceView'
        }


        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("Invoices", null);
                }
            }
            else {
                $state.go("Invoices", null);
            }
        }


        $scope.saveToPDF = function () {
            
            $http.post($scope.baseUrl + '/InvoiceExportPdf', {
                invoiceId: $stateParams.invoiceId,
                pageMode: $scope.pageMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = "InvoiceDetailReport.pdf";
                a.click();
            });
            
        }
    }])

})();