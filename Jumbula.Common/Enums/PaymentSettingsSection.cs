﻿

namespace Jumbula.Common.Enums
{
    public enum PaymentSettingsSection
    {
        Stripe = 0,
        Paypal = 1,
        Cash = 2,
        BankTransfer = 3,
        Manual = 4,
        AuthorizeNet = 5,
        Currency = 6
    }
}
