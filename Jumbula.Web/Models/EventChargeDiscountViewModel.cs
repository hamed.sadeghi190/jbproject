﻿using SportsClub.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class EventChargeDiscountViewModel : EventBaseCreateEditModel
    {

        public EventChargeDiscountViewModel()
            : base()
        {
            PaymentPlan = new PaymentPlanViewModel();
            Donation=new DonationViewModel();
        }

        public EventChargeDiscountViewModel(string domain)
            : base(domain)
        {
            PaymentPlan = new PaymentPlanViewModel(this.ClubDomain);
            Donation=new DonationViewModel();
            Restricted=new RestrictedViewModel(this.ClubDomain);
        }

        [UIHint("PaymentPlanViewModel")]
        public PaymentPlanViewModel PaymentPlan { get; set; }

        public DonationViewModel Donation { get; set; }

        public RestrictedViewModel Restricted { get; set; }

        //public ICollection<ChargeDiscount> ChargeDiscounts { get; set; }

        //public List<CustomDiscountViewModel> CustomDiscounts { get; set; }

    }

}