﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace SportsClub.Areas.Dashboard.Models
{
    public class AllInvoicesClubViewModel
    {
       
        public DateTime Date { get; set; }
        public string StrDate { get {

                return Date.ToString("MMM dd, yyyy");
            }
        }
        public long Id { get; set; }
        public long InvoiceId { get; set; }
        public int ClubId { get; set; }
        public string Status { get; set; }
               
        public decimal Amount { get; set; }
        public string StrAmount { get; set; }
        public string Recipient { get; set;}
        public int? DueDateValue { get; set; } 
        public int UserId { get; set; }
        public DateTime DueDate
        {
            get
            {
                var dateTime =Date;

                if (DueDateValue > 0)
                {

                    return dateTime.AddDays(DueDateValue.Value);
                    
                }
                else
                {

                    return dateTime;

                }

            }
        }

        public string StrDueDate
        {
            get
            {
                return DueDate.ToString("MMM dd, yyyy");
            }
        }
    }

}