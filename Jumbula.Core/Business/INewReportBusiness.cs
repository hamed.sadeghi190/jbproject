﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using Jumbula.Core.Model.Report;

namespace Jumbula.Core.Business
{
    public interface INewReportBusiness
    {
        ReportDataModel BindData(ReportName reportName, ClubBaseInfoModel activeClub, Dictionary<string, object> parameters, PaginationModel paginationModel, List<ReportGridSort> sort = null);
        ReportModel GetSchema(ReportName reportName, ClubBaseInfoModel activeClub, Dictionary<string, object> parameters);
        Dictionary<string, object> GetFilter(ClubBaseInfoModel activeClub, Dictionary<string, object> filters);

        ReportExportModel GetPdfModel(ReportName reportName, ClubBaseInfoModel activeClub, Dictionary<string, object> parameters,
            PaginationModel paginationModel);

        byte[] GetExcel(ReportName reportName, ClubBaseInfoModel activeClub, Dictionary<string, object> parameters,
            PaginationModel paginationModel, ControllerContext controllerContext);

        Dictionary<string, object> GetReportHeaders(Dictionary<string, object> parameters,
            List<ReportHeader> reportHeaders);

        List<SelectListItem> GetSeasonPrograms(long seasonId, int clubId,
            ProgramTypeCategory? programTypeCategory);

        List<SelectListItem> GetProgramSessions(long programId);

        List<SelectListItem> GetPartnerSchools(int partnerId);

        List<SelectListItem> GetSchoolSeasons(int clubId);

        List<SelectListItem> GetProgramSchedules(long programId);

        List<SelectListItem> GetOrganizationList();

        object GetParameter(Dictionary<string, object> parameters, string parameterName);

        List<SelectListItem> GetActiveStatus();

        List<SelectListItem> GetIdentifierItems();

        List<SelectListItem> GetInstallmentPaymentModeItems();

        byte[] CreateExcelExport(ReportExportModel model);

        string GetDayOfWeekWithStartTime(List<ProgramScheduleDay> days, AbbreviatedDayNameMode abbreviatedDayName = AbbreviatedDayNameMode.TwoLetter);

        string GetDayOfWeekWithEndTime(List<ProgramScheduleDay> days, AbbreviatedDayNameMode abbreviatedDayName = AbbreviatedDayNameMode.TwoLetter);

        List<OrderItem> GetReportOrderItemsWithEffectiveDate(long programScheduleId, DateTime dateTime);

        List<OrderItem> GetReportOrderItemsWithEffectiveDate(long programScheduleId);

        List<SelectKeyValue<long>> GetSeasonProgramsTextValue(int clubId, string seasonDomain = "", List<ProgramTypeCategory> typeCategories = null);

        List<SelectListItem> GetSchoolsAndProviders(int partnerId);
    }
}
