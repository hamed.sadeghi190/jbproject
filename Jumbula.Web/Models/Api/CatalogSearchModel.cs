﻿using System.Collections.Generic;
using Jumbula.Common.Enums;

public class CatalogSearchModel
{
    public int? County { get; set; }

    public int? Category { get; set; }

    public SchoolGradeType? MinGrade { get; set; }

    public SchoolGradeType? MaxGrade { get; set; }

    public int? MinPrice { get; set; }

    public int? MaxPrice { get; set; }

    public int? MinEnrollment { get; set; }

    public int? MaxEnrollment { get; set; }

    public bool BeforeSchool { get; set; }

    public bool Virtus { get; set; }

    public bool WeeklyEmails { get; set; }

    public int Page { get; set; }

    public int Offset { get; set; }
}