﻿namespace Jumbula.Common.Enums
{
    public enum EmailCallerType: byte
    {
        AutoCharge,
        User,
        Delinquent
    }
}
