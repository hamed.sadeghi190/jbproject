﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.WebLogic.UserService;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Jumbula.Web.Mvc.Attributes
{
    public class JbAuthorize : AuthorizeAttribute
    {
        private readonly JbAction _jbAction;

        public JbAuthorize(JbAction jbAction)
        {
            _jbAction = jbAction;
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var roles = new List<RoleCategory>();
            var currentUserRole = JbUserService.GetCurrentUserRole();
            var allowedRoles = Ioc.SecurityBusiness.GetRolesOfAction(_jbAction);

            if (allowedRoles.Contains(currentUserRole))
                roles.Add(currentUserRole);

            if (!roles.Contains(RoleCategory.SysAdmin))
                roles.Add(RoleCategory.SysAdmin);

            if (!roles.Contains(RoleCategory.Support))
                roles.Add(RoleCategory.Support);

            Roles = string.Join(",", roles);

            base.OnAuthorization(filterContext);
        }
    }
}