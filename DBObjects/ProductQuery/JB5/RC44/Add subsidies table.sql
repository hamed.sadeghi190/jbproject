
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Subsidies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PartnerId] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[State] [int] NOT NULL,
	[MetaData_DateCreated] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[MetaData_DateUpdated] [datetime] NOT NULL DEFAULT ('1900-01-01T00:00:00.000'),
	[ContactId] [int] NULL,
	[Website] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Subsidies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Subsidies]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Subsidies_dbo.Clubs_PartnerId] FOREIGN KEY([PartnerId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Subsidies] CHECK CONSTRAINT [FK_dbo.Subsidies_dbo.Clubs_PartnerId]
GO

ALTER TABLE [dbo].[Subsidies]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Subsidies_dbo.ContactPersons_ContactId] FOREIGN KEY([ContactId])
REFERENCES [dbo].[ContactPersons] ([Id])
GO

ALTER TABLE [dbo].[Subsidies] CHECK CONSTRAINT [FK_dbo.Subsidies_dbo.ContactPersons_ContactId]
GO


