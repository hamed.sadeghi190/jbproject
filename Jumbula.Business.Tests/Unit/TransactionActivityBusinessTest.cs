﻿using FluentAssertions;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class TransactionActivityBusinessTest
    {

        private Mock<IRepository<TransactionActivity, long>> _mockTransactionActivityRepository;
        private Mock<IEntitiesContext> _mockDataContext;

        [TestMethod]
        public void GetPaymentTransactions_PaymentDetailIsNull_ShouldReturnZeroTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = null;
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Success;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetPaymentTransactions(installment1);

            //Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetPaymentTransactions_PayKeyIsNull_ShouldReturnZeroTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = null;
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Success;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetPaymentTransactions(installment1);

            //Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetPaymentTransactions_TransactionTypeIsNotPayment_ShouldReturnZeroTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test pay key";
                t.TransactionType = TransactionType.Refund;
                t.TransactionStatus = TransactionStatus.Success;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetPaymentTransactions(installment1);

            //Assert
            result.Count().Should().Be(0);

        }

        [TestMethod]
        public void GetPaymentTransactions_TransactionStatusIsNotSuccess_ShouldReturnZeroTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test PayKey";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Failure;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetPaymentTransactions(installment1);

            //Assert
            result.Count().Should().Be(0);

        }

        [TestMethod]
        public void GetPaymentTransactions_TransactionIsPaymentAndSuccess_ShouldReturnOneTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test PayKey";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Success;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetPaymentTransactions(installment1);

            //Assert
            result.Count().Should().Be(1);

        }

        [TestMethod]
        public void GetRefundAutomaticallyTransactions_PaymentDetailIsNull_ShouldReturnZeroTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = null;
                t.TransactionType = TransactionType.Refund;
                t.TransactionStatus = TransactionStatus.Success;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetRefundAutomaticallyTransactions(installment1);

            //Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetRefundAutomaticallyTransactions_PayKeyIsNull_ShouldReturnZeroTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = null;
                t.TransactionType = TransactionType.Refund;
                t.TransactionStatus = TransactionStatus.Success;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetRefundAutomaticallyTransactions(installment1);

            //Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetRefundAutomaticallyTransactions_TransactionTypeIsNotRefund_ShouldReturnZeroTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test pay key";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Success;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetRefundAutomaticallyTransactions(installment1);

            //Assert
            result.Count().Should().Be(0);

        }

        [TestMethod]
        public void GetRefundAutomaticallyTransactions_TransactionStatusIsNotSuccess_ShouldReturnZeroTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test PayKey";
                t.TransactionType = TransactionType.Refund;
                t.TransactionStatus = TransactionStatus.Failure;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetRefundAutomaticallyTransactions(installment1);

            //Assert
            result.Count().Should().Be(0);

        }

        [TestMethod]
        public void GetRefundAutomaticallyTransactions_TransactionIsRefundAndSuccess_ShouldReturnOneTransactions()
        {
            //Arrange
            var service = CreateService();
            var installment1 = FactoryGirl.Build<OrderInstallment>(p => p.Id = 1);
            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 10;
                t.Installment = installment1;
                t.InstallmentId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test PayKey";
                t.TransactionType = TransactionType.Refund;
                t.TransactionStatus = TransactionStatus.Success;
            });
            installment1.TransactionActivities = new List<TransactionActivity> { transaction1 };

            //Act
            var result = service.GetRefundAutomaticallyTransactions(installment1);

            //Assert
            result.Count().Should().Be(1);
        }

        #region Private methods
        private TransactionActivityBusiness CreateService()
        {
            _mockTransactionActivityRepository = new Mock<IRepository<TransactionActivity, long>>();
            _mockDataContext = new Mock<IEntitiesContext>();

            var result = new TransactionActivityBusiness(

               _mockTransactionActivityRepository.Object,
               _mockDataContext.Object
               );

            return result;
        }

        #endregion
    }
}
