﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using System.Collections.Generic;

namespace Jumbula.Core.Model.Email
{
    public class RefundOrderEmailBodyItemModel
    {
        public RefundOrderEmailBodyItemModel()
        {

        }
        public RefundOrderEmailBodyItemModel(CurrencyCodes currency)
        {
            Currency = currency;
        }
        public string PlayerFullName { get; set; }

        public string Price { get; set; }

        public CurrencyCodes Currency { get; set; }
        public string EventName { get; set; }

        public List<ReservationItemDetail> OrderCharges { get; set; }
        public List<ReservationItemDetail> OrderDiscounts { get; set; }
        public PaymentPlanType PaymentPlanType { get; set; }
        public List<OrderInstallment> OrderInstallments { get; set; }
        public decimal Amount { get; set; }
        public string EventDate { get; set; }
        public string DayAndTime { get; set; }
        public List<string> AdditionalParameters { get; set; }
        public PostalAddress EventAddress { get; set; }

        public class ReservationItemDetail
        {

        }

    }
}
