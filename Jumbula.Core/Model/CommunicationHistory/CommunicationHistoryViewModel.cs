﻿using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Model.CommunicationHistory
{
    public class CommunicationHistoryViewModel
    {
        public int Id { get; set; }

        public string Date { get; set; }

        public ContactCategory Category { get; set; }

        public string Sender { get; set; }

        public int? SenderId { get; set; }

        public string DeliverDateTime { get; set; }

        public string OpenDateTime { get; set; }

        [StringLength(1024,ErrorMessage = Common.Constants.Validation.MaxStringLength)]
        [Display(Name = "Memo")]
        public string Note { get; set; }

        public int? EmailId { get; set; }

        public int? ContactId { get; set; }

        public string RelatedEntity { get; set; }

        public long RelatedEntityId { get; set; }
    }
}
