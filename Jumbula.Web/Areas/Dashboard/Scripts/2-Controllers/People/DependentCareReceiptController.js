﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('DependentCareReceiptController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/People';
        $scope.Model = customReportService.getModelFromStrorage();
        var model = {};
        var dataInfo = {};
        $scope.filter = {};


        $scope.Model.year = [];
        var year = new Date().getFullYear();
        for (var i = year; i >= year - 2; i--) {

            $scope.Model.year.push({ Value: i, Title: i.toString() });
        }

        $scope.filter.year = year;

        $scope.filter.filterType = "Year";
        $scope.filter.printMode = false;

        var isBegin = true;
        var isbeginLoadGrid = false;


        $scope.viewReport = function () {

            getHeaderReport();

            if (isBegin) {

                isBegin = false;
                $scope.DependentCareReceiptDataSource = new kendo.data.DataSource({

                    transport: {
                        read: {
                            url: baseUrl + "/getdependentcarereceiptreport",
                            dataType: "json",
                            type: "Post",
                            data: function () {
                                return {
                                    userId: $scope.stateParams.userId,
                                    filterType: $scope.filter.filterType,
                                    year: $scope.filter.year,
                                    startDate: $scope.filter.startDate,
                                    endDate: $scope.filter.endDate,
                                    printMode: $scope.filter.printMode,
                                    clubId: $scope.stateParams.clubId
                                };
                            },
                        }
                    },
                    requestEnd: function () {

                        if (!isbeginLoadGrid) {
                            addDescription($scope.Model.HeaderInfo.Description);
                            isbeginLoadGrid = true;
                        }
                    },
                    schema: {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                    aggregate: [
                        { field: "AmountPaid", aggregate: "sum" }
                    ],
                    serverSorting: false,
                    serverPaging: false,
                    resizable: true,
                    batch: true,
                });

                $scope.DependentCareReceiptGridOptions = {
                    excel: {
                        allPages: true,
                        fileName: "DependentCareReceiptReport.xlsx",
                        proxyURL: "/Dashboard/PortalReport/Save"
                    },
                    excelExport: function (e) {

                        var rows = e.workbook.sheets[0].rows;

                        if ($scope.Model.HeaderInfo.PartnerEmail != "") {
                            rows.unshift({
                                cells: [{ value: $scope.Model.HeaderInfo.PartnerEmail }]
                            });
                        }

                        if ($scope.Model.HeaderInfo.PartnerSite != "") {
                            rows.unshift({
                                cells: [{ value: $scope.Model.HeaderInfo.PartnerSite }]
                            });
                        }

                        if ($scope.Model.HeaderInfo.PartnerPhone != "") {
                            rows.unshift({
                                cells: [{ value: $scope.Model.HeaderInfo.PartnerPhone }]
                            });
                        }

                        if ($scope.Model.HeaderInfo.PartnerAddress != "") {
                            rows.unshift({
                                cells: [{ value: $scope.Model.HeaderInfo.PartnerAddress }]
                            });
                        }

                        if ($scope.Model.HeaderInfo.PartnerName != "") {
                            rows.unshift({
                                cells: [{ value: $scope.Model.HeaderInfo.PartnerName }]
                            });
                        }


                        rows.unshift({
                            cells: [{ value: "Class meeting dates (" + $scope.Model.HeaderInfo.Date + ")" }]
                        });

                        rows.push({
                            cells: [{
                                value: ""
                            }]
                        });

                        for (var i = 0; i < rows.length; i++) {
                            for (var ci = 0; ci < rows[i].cells.length; ci++) {
                                rows[i].cells[ci].hAlign = "left";
                            }
                        }

                        cfpLoadingBar.complete();
                    },
                    dataSource: $scope.DependentCareReceiptDataSource,
                    resizable: true,
                    dataBound: function () {
                        var grid = $("#DependentCareReceiptGrid").data("kendoGrid");
                        customReportService.initializeDoubleScroll(grid);
                    },

                    sortable: true,
                    toolbar: [
                        { template: kendo.template($("#toolbarTemplate").html()) }
                    ],
                    scrollable: true,
                    columns: [{
                        field: "Date",
                        title: "Payment date",
                        width: "230px",
                    },
                    {
                        field: "StudentName",
                        title: "Student name",
                        width: "230px"
                    },
                    {
                        field: "ClassName",
                        title: "Class name",
                        width: "230px",
                    },
                    {
                        field: "AmountPaid",
                        title: "Amount paid ($)",
                        width: "230px",
                        footerTemplate: 'Total: #= kendo.toString(sum,"c2")#'
                    },
                    {
                        field: "ScheduleDate",
                        title: "Class date",
                        width: "230px",

                    },
                    {
                        field: "ClassMeetingDates",
                        title: "Class meeting dates",
                        width: "230px"
                    },
                    {
                        field: "MethodOfPayment",
                        title: "Method of payment",
                        width: "230px",
                        attributes: { style: "text-align:left" }
                    },
                    {
                        field: "ProviderName",
                        title: "Provider name",
                        width: "230px"

                    },
                    {
                        field: "ProviderTaxId",
                        title: "Provider tax ID",
                        width: "230px",

                    }
                    ]

                }


            } else {
                $("#DependentCareReceiptGrid").data("kendoGrid").dataSource.read();
            }
        }

        var addDescription = function (description) {
      
            $("#DependentCareReceiptGrid").append('<div style="padding:15px 20px">' + description.replace(/\n/g, "<br/>") + '</div>');

        }


        $scope.saveToPDF = function () {
            $scope.filter.printMode = true;

            $http.post(baseUrl + '/getdependentcarereceiptreport', {
                userId: $scope.stateParams.userId,
                filterType: $scope.filter.filterType,
                year: $scope.filter.year,
                startDate: $scope.filter.startDate,
                endDate: $scope.filter.endDate,
                printMode: $scope.filter.printMode,
                clubId: $scope.stateParams.clubId
            }, {
                    headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
                }).success(function (data) {
                    var a = document.createElement("a");
                    document.body.appendChild(a);
                    var blob = new Blob([data], { type: "application/pdf" });

                    var fileURL = URL.createObjectURL(blob);
                    a.href = fileURL;
                    a.download = "DependentCareReceiptReport.pdf";
                    a.click();
                });

            $scope.filter.printMode = false;


        }

        $scope.saveToExcel = function () {
            $("#DependentCareReceiptGrid").getKendoGrid().saveAsExcel();
        }

        $scope.print = function () {
            printGrid();
        }



        function printGrid() {
            var gridElement = $('#DependentCareReceiptGrid'),
                printableContent = '',
                win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
                doc = win.document.open();

            var htmlStart =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title></title>' +
                '<div><ul style="list-style:none"> <li> Class meeting dates (' + $scope.Model.HeaderInfo.Date + ') </li>' +
                '<li>' + $scope.Model.HeaderInfo.PartnerName + '</li>' +
                '<li>' + $scope.Model.HeaderInfo.PartnerAddress + '</li>' +
                '<li>' + $scope.Model.HeaderInfo.PartnerPhone + '</li>' +
                '<li>' + $scope.Model.HeaderInfo.PartnerSite + '</li>' +
                '<li>' + $scope.Model.HeaderInfo.PartnerEmail + '</li>' +
                '</ul> </div>' +
                '<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
                '<style>' +
                'html { font: 11pt sans-serif; text-align: left }' +
                '.k-grid { border-top-width: 0; }' +
                '.k-grid, .k-grid-content { height: auto !important; padding: 20px; text-align: left}' +
                '.k-grid-content { overflow: visible !important; padding: 20px;}' +
                '.k-grid .k-grid-header  th { border-top: 1px solid; padding-bottom: 20px }' +
                '.k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
                '</style>' +
                '</head>' +
                '<body>';

            var htmlEnd =
                '</body>' +
                '</html>';

            var gridHeader = gridElement.children('.k-grid-header');
            if (gridHeader[0]) {
                var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
                printableContent = gridElement
                    .clone()
                    .children('.k-grid-header').remove()
                    .end()
                    .children('.k-grid-content')
                    .find('table')
                    .first()
                    .children('tbody').before(thead)
                    .end()
                    .end()
                    .end()
                    .end()[0].outerHTML;
            } else {
                printableContent = gridElement.clone()[0].outerHTML;
            }

            doc.write(htmlStart + printableContent + htmlEnd);
            doc.close();
            win.print();
        }


        var getHeaderReport = function () {

            $http.get(baseUrl + '/GetDependentCareReceiptReportDetail', { params: { filterType: $scope.filter.filterType, year: $scope.filter.year, startDate: $scope.filter.startDate, endDate: $scope.filter.endDate } }).success(function (data, status, headers, config) {

                $scope.Model.HeaderInfo = data.Data;

            }).error(function (data, status, headers, config) {
                jbtoast.error(data);
            });

        };


        $('#year').attr("disabled", false);
        $("#startDate").attr('disabled', true);
        $("#endDate").attr('disabled', true);


        $scope.YearClicked = function () {
            $('#startDate').attr("disabled", true);
            $('#endDate').attr("disabled", true);
            $('#year').attr("disabled", false);
        }

        $scope.dateRangeClicked = function () {
            $('#year').attr("disabled", true);
            $('#startDate').attr("disabled", false);
            $('#endDate').attr("disabled", false);
        }
    }
    ])

})();


