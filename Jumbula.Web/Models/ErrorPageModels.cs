﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ErrorPageViewModel
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
}