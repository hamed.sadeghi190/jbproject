﻿using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IOrderSessionBusiness
    {
        IQueryable<OrderSession> GetList();

        IQueryable<OrderSession> GetList(Expression<Func<OrderSession, bool>> predicate);

        IQueryable<OrderSession> GetList(long seasonId);

        IQueryable<OrderSession> GetOrderItemSessions(long orderItemId);

        OperationStatus Update(OrderSession session);

        OperationStatus Update(List<OrderSession> sessions);

        OperationStatus Create(OrderItem orderItem);

        OperationStatus Create(OrderItem orderItem, List<DayOfWeek> days, DateTime? desiredStartDate = null, DateTime? cancelEffectiveDate = null, bool IsCombo = false);

        OperationStatus Create(OrderItem orderItem, List<long> selectedSessionIds, bool IsCombo = false);
        OperationStatus CreateForTransfer(OrderItem orderItem, List<DayOfWeek> days, OrderItem oldOrderItemIntransfer, DateTime? desiredStartDate = null);
        bool HasOrder(Program program);
        bool HasActiveOrder(Program program);
        bool HasOrderSession(Program program);
        bool HasOrderSession(ProgramSchedule programSchedule);
        bool HasActiveOrderSession(Program program);
        bool HasActiveOrderSession(ProgramSchedule programSchedule);

        OperationStatus AssignPunchCardSession(long orderItemId, List<int> programSessionIds, int userId);

        OperationStatus ReassignPunchCardSession(long orderItemId, int orderSessionId, List<int> programSessionIds, int userId);

        OperationStatus UnassignPunchCardSession(int orderSessionId, int userId);
        OperationStatus CreateSession(OrderItem orderItem, List<DayOfWeek> days, DateTime? effctiveDate = null, bool IsCombo = false);

        bool HasActiveOrder(ProgramSchedule programSchedule);
        void CreateSession(OrderItem orderItem, long chargeId);

        IQueryable<OrderSession> GetListByDate(long programScheduleId, DateTime dateTime);
        IQueryable<OrderSession> GetListBySchedule(long programScheduleId);

        OperationStatus CreateSessionByChangeDesiredStartDate(OrderItem orderItem, List<DayOfWeek> days, DateTime desiredStartDate, bool IsCombo = false);

        List<OrderSession> GetReportOrderSessionsWithEffectiveDate(long programScheduleId, DateTime dateTime);

        IEnumerable<ProgramSession> CheckSessionsAreInFreezedSchedule(Program program, List<long> programSessionIds);
    }
}
