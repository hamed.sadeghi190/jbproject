﻿
namespace Jumbula.Core.Model.AutoCharge
{
    public class ClubInstallmentPair
    {
        public long InstallmentId { get; set; }
        public int ClubId { get; set; }
    }
}
