﻿(function () {
    'use strict';

    angular.module('jbTools').service("emailNotificationService", [function () {
        var _model;

        function set(Data) {
            _model = Data;
        }

        function get() {
            return _model;
        }

        return {
            set: set,
            get: get
        }
    }]);
})();