﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class DefaultTourneySearchViewModel : SearchViewModelBase
    {
        public DefaultTourneySearchViewModel()
        {
        }

        public DefaultTourneySearchViewModel(string domain, string clubDomain)
        {
            ClubDomain = clubDomain;
            Domain = domain;
        }
    }
}