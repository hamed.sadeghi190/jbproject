﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('catalogsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'catalog-view';//seasons-view programs
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Catalog", order: 0, menu: "CATALOGS" });
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.catalogDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport: {
                read:
                {
                    url: window.location.origin + '/Dashboard/Catalog/GetList/',
                    contentType: "application/json; charset=utf-8", // tells the web method to serialize JSON
                    type: "POST" 
                },
                parameterMap: function (data, operation) {
                    if (operation == "read") {
                        data = $.extend(
                        {
                            sort: null,
                            filter: null
                        },
                        data);

                        return JSON.stringify(data);
                    }
                }
            },
            schema:
                {
                    total: "TotalCount",
                    data: "DataSource",
                },
            sort:
                [{
                    field: "Name",
                    dir: "asc"
                }],
            pageSize: 10,
            batch: true,
            serverFiltering: true,
            serverPaging: true,
            serverSorting: false,
            requestStart: function (e) {
                kendo.ui.progress($('[kendo-grid]'), true);
            },
            requestEnd: function (e) {
                kendo.ui.progress($('[kendo-grid]'), false);
            }
        });

        $scope.catalogGridOptions = {
            dataSource: $scope.catalogDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [{
                field: "Name",
                title: "Class name"
            },
            {
                field: "DateCreated",
                title: "Created date",
                width: "160px",
                template: "#= kendo.toString(kendo.parseDate(DateCreated), 'MMM/dd/yyyy H:mm') #",
            },
            {
                field: "DateUpdated",
                title: "Updated date",
                width: "160px",
                template: "#= DateUpdated == DateCreated ? '<sup class=\"grid-row-empty\">____________________</sup>' : kendo.toString(kendo.parseDate(DateUpdated), 'MMM/dd/yyyy H:mm') #",
                sortable: false,
            },
             {
                 field: "ItemStatus",
                 title: "Status",
                 width: "160px",
             },
            {
                field: "",
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                                <li> ... \
                                    <ul>\
									   <li ngaction='CatalogItem_View' ui_sref='ViewCatalog({catalogId: #=Id#})'>\
                                                <span class='k-link jbi-show'>View</span>\
                                        </li>\
                                       #if(ItemStatus != 'Archived'){#\
                                        <li ngaction='CatalogItem_Archive' ng-click='archiveCatalog(#= Id #)'>\
                                                <span class='k-link jbi-archive' >Archive</span>\
                                        </li>\
                                       #}#\
                                       #if(ItemStatus == 'Archived'){#\
                                        <li ngaction='CatalogItem_Archive' ng-click='unarchiveCatalog(#= Id #)'>\
                                                <span class='k-link jbi-unarchive'>Unarchive</span>\
                                        </li>\
                                       #}#\
                                        <li ngaction='CatalogItem_Edit' ui_sref='EditCatalog({catalogId: #=Id#})'>\
                                                <span class='k-link jbi-pencil'>Edit</span>\
                                        </li>\
                                        <li ngaction='CatalogItem_Delete' ng-click='deleteCatalog(#= Id #)'>\
                                                <span class='k-link jbi-remove'>Delete</span>\
                                        </li>\
                                        <li ngaction='CatalogItem_Invite' ui_sref='inviteCatalog({catalogId: #=Id#})'>\
                                                <span class='k-link'>Schedule draft session</span>\
                                        </li>\
                                    </ul>\
                                </li>\
                          </ul>",
            }],
        };
	
        
    
        $scope.deleteCatalog = function (catalogId) {

            jbConfirmation
               .yes(function () {
                   $http.post('Dashboard/Catalog/Delete', { id: catalogId })
                       .success(function (data, status, headers, config) {

                           if (data.Status == true) {

                               jbToast.success("Catalog deleted successfully.", "");

                               $("#catalogGrid").data("kendoGrid").dataSource.read();
                           }
                           else {
                               $scope.MC.handleErrors($scope, data);
                           }

                       });
               })
               .confirm();
        }

        $scope.archiveCatalog = function (catalogId) {

            jbConfirmation
               .yes(function () {
                   $http.post('Dashboard/Catalog/Archive', { id: catalogId })
                       .success(function (data, status, headers, config) {

                           if (data.Status == true) {

                               jbToast.success("Catalog archived successfully.", "");

                               $("#catalogGrid").data("kendoGrid").dataSource.read();
                           }
                           else {
                               $scope.MC.handleErrors($scope, data);
                           }

                       });
               })
               .confirm();
        }

        $scope.unarchiveCatalog = function (catalogId) {

            jbConfirmation
               .yes(function () {
                   $http.post('Dashboard/Catalog/Active', { id: catalogId })
                       .success(function (data, status, headers, config) {

                           if (data.Status == true) {

                               jbToast.success("Catalog unarchived successfully.", "");

                               $("#catalogGrid").data("kendoGrid").dataSource.read();
                           }
                           else {
                               $scope.MC.handleErrors($scope, data);
                           }

                       });
               })
               .confirm();
        }

        $scope.init = function () {
            $('[data-role=dropdown]').dropdown();
        }
    }]);
})();

