﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('familiesController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'cfpLoadingBar', 'addNewUserService', function ($scope, $http, $log, $stateParams, $state, jbToast, cfpLoadingBar, addNewUserService) {

        window.app.globalObjects.pageClass = 'families-view';
        window.app.globalObjects.initializeView();

        $scope.MC.backQueueItems = [];
        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/People';

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });
        $scope.stateParams = $stateParams;

        $scope.Model = {};
        $scope.Model.EnableViewParentDashboard = false;

        $http.get(window.location.origin + "/Dashboard/OverView/GetAccountInfo")
            .success(function (data, status, headers, config) {
                $scope.Model.HasPartner = data.HasPartner;

                $scope.Model.EnableViewParentDashboard = data.EnableViewParentDashboard;

                if (data.HasPartner) {
                    $scope.SeasonNames = [{ Value: "Fall", Title: "Fall" }, { Value: "Winter", Title: "Winter" }, { Value: "Spring", Title: "Spring" }, { Value: "Summer", Title: "Summer" }];
                    $scope.SeasonName = "Fall";
                    $scope.Year = 0;
                    getYears();

                    $scope.SearchTypes.push({ Text: 'Season', Value: 'Season' });
                    getAllPartnerSchools();
                }

                prepareGrid();
            });

        $scope.MC.stateParams = $stateParams;

        $scope.searchTerm = "";
        $scope.SearchTypes = [{ Text: 'Name', Value: 'Name' }, { Text: 'Family email', Value: 'Email' }, { Text: 'Confirmation ID', Value: 'ConfirmationId' }, { Text: 'Credit', Value: 'Credit' }, { Text: 'Phone', Value: 'Phone' }];
        $scope.SearchType = 'Name';

        $scope.ActiveStatus = [{ Value: "More than zero", Title: "More than zero" }, { Value: "Equal to zero", Title: "Equal to zero" }, { Value: "Less than zero", Title: "Less than zero" }];
        $scope.Status = "More than zero";

        var prepareGrid = function () {
            $scope.userDataSource = new kendo.data.DataSource({
                type: "json",
                transport: {
                    read: {
                        url: window.location.origin + '/Dashboard/People/GetFamilies/',
                        type: "POST",
                        cache: false
                    }, parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                searchType: $scope.SearchType,
                                firstName: $scope.firstName,
                                lastName: $scope.lastName,
                                creditStatus: $scope.Status,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort,
                                year: $scope.Year,
                                seasonName: $scope.SeasonName,
                                selectedSchools: $scope.Model._SelectedSchools
                            }
                        }
                    }
                },
                pageSize: 50,
                batch: true,
                serverPaging: true,
                serverSorting: false,
                schema: {
                    data: "Data",
                    total: "Total"
                },
                requestStart: function (e) {
                    kendo.ui.progress($('[kendo-grid]'), true);
                },
                requestEnd: function (e) {
                    kendo.ui.progress($('[kendo-grid]'), false);
                }
            });

            $scope.userGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Families.xlsx"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Families"
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.userDataSource,
                sortable: true,
                serverPaging: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    var grid = $("#userGrid").data("kendoGrid");

                    grid.hideColumn("ClubName");
                    
                    if ($scope.Model.HasPartner) {
                        grid.showColumn("ClubName");

                        $(".Invoice").each(function (e) {
                            $(this).hide();
                        });
                    }

                    if (!$scope.Model.EnableViewParentDashboard) {
                        $('.switch-parent').remove();
                    }
                },
                columns: [{
                    field: "UserName",
                    title: "Email",
                    width: 250,
                    template: "<span class='block'> #= UserName # </span>"
                }, {
                    field: "PlayersName",
                    title: "Participants",
                    template: "<span> #= PlayersName # </span>"
                },
                {
                    field: "ClubName",
                    title: "School",
                    width: 130
                },
                {
                    field: "TotalOrders",
                    title: "Total orders"
                },
                {
                    field: "Balance",
                    title: 'Balance',
                    template: "<span>#if(Balance >0) {# <p class='fg-red'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance <0) {# <p class='fg-green'>#=(Balance).formatCurrencyWithPenny(2)#</p> #} else if(Balance ==0) {# <p>#=Balance#</p> #}#"
                },
                {
                    field: "Credit",
                    title: "Credit",
                    template: "<span>#if(Credit >0) {# <p class='fg-green'>#=(Credit).formatCurrencyWithPenny(2)#</p> #} else if(Credit <0) {# <p class='fg-red'>#=(Credit).formatCurrencyWithPenny(2)#</p> #} else if(Credit ==0) {# <p>#=Credit#</p> #}#"
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: '<ul kendo-menu style="display: inline-block;" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                     <li > ... \
                              <ul>\
                                    <li ui_sref="FamilyDetail({userId:#=Id#, clubId:#=ClubId#})">\
                                          <span class="k-link jbi-angle-right">View details</span>\
                                    </li>\
                                    <li ui_sref="ManageFamily({userId:#=Id#, clubId:#=ClubId#})">\
                                          <span class="k-link jbi-angle-right">Edit</span>\
                                    </li>\
                                    <li ui_sref="FamilyOrders({userId:#=Id#, clubId:#=ClubId#})">\
                                          <span class="k-link jbi-angle-right">Orders</span>\
                                    </li>\
                                    <li ui_sref="FamilyTransactions({userId:#=Id#, clubId:#=ClubId#})">\
                                          <span class="k-link jbi-angle-right">Transactions</span>\
                                    </li><hr/>\
                                          <li class="Invoice" ui_sref="FamilyInvoicesOrTakePayment({userId:#=Id#,type:' + "'Invoice'" + ', clubId:#=ClubId#})">\
                                          <span class="k-link jbi-angle-right">Invoice</span>\
                                    </li>\
                                    </li>\
                                          <li ui_sref="FamilyInvoicesOrTakePayment({userId:#=Id#,type:' + "'TakePayment'" + ', clubId:#=ClubId#})">\
                                          <span class="k-link jbi-angle-right">Take a payment</span>\
                                    </li><hr/>\
                                    </li>\
                                          <li ui_sref="DependentCareReceiptReport({userId:#=Id#, clubId:#=ClubId#})">\
                                          <span class="k-link jbi-angle-right">Dependent care receipt</span>\
                                    </li>\
                                  <li ngaction="ParentDashboard_View" class="k-item k-state-default switch-parent" role="menuitem"><hr/>\
                                    <a target="_blank" href="/Account/LoginByAdmin?userId=#=Id#">\
                                       <span class="k-link jbi-angle-right">&nbsp; View family dashboard</span>\
                                     </a>\
                                  </li>\
                            </ul>\
                        </li>\
                    </ul>'
                }],
            };
        }

        $scope.addUser = function () {
            var addModel = {
                page: "Families",
                stateParams: $scope.MC.stateParams
            };
            addNewUserService.set(addModel);
            $state.go('AddClubUsers');
        }
        $scope.search = function () {
            $scope.userGridOptions.dataSource.page(1);
            $("#userGrid").data("kendoGrid").dataSource.read();
        };

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#userGrid").getKendoGrid().saveAsExcel();
        }

        $scope.clearFilters = function () {
            $scope.SearchType = 'Name';
            $scope.searchTerm = "";
            $scope.firstName = "";
            $scope.lastName = "";
            $scope.Status = "More than zero";
            $scope.SeasonName = "Fall";
            $scope.Year = new Date().getFullYear().toString();
            $scope.Model._SelectedSchools = [];
            $scope.userGridOptions.dataSource.page(1);
            $("#userGrid").data("kendoGrid").dataSource.read();
        };

        function getYears() {
            $http.get(baseUrl + '/GetCurrentYearsFrom2015').success(function (data, status, headers, config) {

                $scope.Years = data.Years;
                $scope.Year = new Date().getFullYear().toString();
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        function getAllPartnerSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data, status, headers, config) {
                    $scope.AllSchools = data.AllSchools;
                    $scope.Model._SelectedSchools = [''];
                });
        }

    }]);
})();