﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemConfirmChargeClub', ['$scope', '$http', 'jbToast', '$log', '$stateParams', '$state', 'cancelOrderService', 'chargeClubService', function ($scope, $http, jbToast,$log, $stateParams, $state, cancelOrderService, chargeClubService) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.Model = cancelOrderService.get();

        $scope.Model = chargeClubService.get();//get info from step back



        $scope.SubmitChargeClub = function () {

            $http.post($scope.baseUrl + '/AutoChargeClubBilling', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        jbToast.success("The charge operation was successful.", "");
                        $state.go('SystemClubBillingHistory', { clubId: $stateParams.clubId });
                       
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                        jbToast.error("The charge operation failed.");

                    }
                });

        };
        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    //window.comesFromOrderItem = false;
                    $state.go("SystemChargeClub", { clubId: $stateParams.clubId });
                }
            }
            else {
                $state.go("SystemClubBillingHistory", { clubId: $stateParams.clubId });
            }
        }

    }]);
})();