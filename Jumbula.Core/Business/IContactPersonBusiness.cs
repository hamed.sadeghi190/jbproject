﻿using Jb.Framework.Common;
using Jumbula.Common.Utilities;
using Jumbula.Core.Model.Contact;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IContactPersonBusiness
    {
        OperationStatus Create(ContactPerson contactPerson);
        IQueryable<ContactPerson> GetList();
        IQueryable<ContactPerson> GetList(int clubId);

        ContactCreateViewModel GetCreate(int clubId);

        OperationStatus Create(ContactCreateViewModel contact, int clubId);

        ContactEditViewModel GetEdit(int id, ClubBaseInfoModel currentClub);

        OperationStatus Update(ContactEditViewModel contact, ClubBaseInfoModel currentClub);

        ContactSettingsViewModel GetSettings(int clubId);

        ContactListSettingViewModel GetListSettings(int currentClubId);

        ContactFilterViewModel GetFilters(int clubId);

        OperationStatus UpdateSettings(ContactSettingsViewModel contactSettings, int clubId);

        PaginatedList<ContactItemViewModel> GetPartnerContacts(int partnerId, ContactFilterViewModel filters,
            List<Sort> sorts, int pageIndex, int pageSize);

        OperationStatus Update(ContactPerson entity);
        ContactPerson Get(int id);
        OperationStatus Delete(int id);
        
    }
}
