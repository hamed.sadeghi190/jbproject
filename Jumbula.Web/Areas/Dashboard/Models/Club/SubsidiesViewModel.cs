﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SubsidiesViewModel : BaseViewModel<Subsidy>
    {
        public List<SubsidyViewModel> AllSubsidies { get; set; }
       
    }
    public class SubsidyViewModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "{0} is required.")]
        public string Name { get; set; }
        public int ClubId { get; set; }
        public List<SelectKeyValue<int>> States { get; set; }
        [Display(Name = "State")]
        [Required(ErrorMessage = "{0} is required.")]
        public int SelectedState { get; set; }
        public string StateName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public int Id { get; set; }

    }
}