﻿using System;
using Jb.Framework.Common.Forms;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Constants = Jumbula.Common.Constants.Constants;

namespace SportsClub.Areas.Dashboard.Models
{
    public class RunProgramReportViewModel : BaseViewModel<EventRoaster>
    {
        //public string EventName { get; set; }

        public string EventTitle { get; set; }

        public List<JbForm> DataTable { get; set; }

        public JbForm JbForm { get; set; }

        public RunProgramReportViewModel()
        {
            DataTable = new List<JbForm>();
        }

        public RunProgramReportViewModel(string eventName, string eventTitle)
            : this()
        {
            //EventName = eventName;
            //EventTitle = eventTitle;
        }

        public RunProgramReportViewModel(string eventName, string eventTitle, List<JbForm> dataTable)
            : this(eventName, eventTitle)
        {
            DataTable = dataTable;
        }



        public RunProgramReportViewModel(string programName, EventRoaster eventRoaster, List<List<OrderItem>> orderItems, string instructorName = "", string roomNumber = "", string dayOfWeek = "", string schoolName = "")
            : this()
        {
            JbForm = eventRoaster.JbForm;
            EventTitle = programName;

            foreach (var section in JbForm.Elements)
                if (section is JbSection)
                    (section as JbSection).Elements.RemoveAll(e => e is JbCheckBox && !(e as JbCheckBox).Value);

            JbForm.Elements.RemoveAll(e => e is JbSection && !(e as JbSection).Elements.Any());
            var AccountService = Ioc.AccountingBusiness;

            foreach (var item in orderItems.SelectMany(m => m).ToList())
            {
                if (item.JbForm != null)
                {
                    #region FollowupForm
                    var followupForms = new List<JbForm>();

                    foreach (var followupForm in eventRoaster.FollowupForms)
                    {
                        var formTemplate = item.FollowupForms.SingleOrDefault(f => f.FormName.Equals(followupForm.Title));

                        if (formTemplate != null)
                        {
                            foreach (var section in followupForm.Elements)
                                if (section is JbSection)
                                    (section as JbSection).Elements.RemoveAll(e => e is JbCheckBox && !(e as JbCheckBox).Value);

                            followupForm.Elements.RemoveAll(e => e is JbSection && !(e as JbSection).Elements.Any());

                            var newFollowupForm = RightCopyNewByAny(formTemplate.JbForm, followupForm, true);

                            followupForms.Add(newFollowupForm);
                        }
                    }
                    #endregion

                    var newForm = RightCopyNewByAny(item.JbForm, JbForm, true);

                    foreach (var followupForm in followupForms)
                    {
                        foreach (var section in (followupForm.Elements))
                        {
                            newForm.Elements.Add(section);
                        }

                    }

                    #region DiscountCoupon codes

                    if (JbForm.Elements.Any(e => e.Name == SectionsName.DiscountCouponSection.ToString()))
                    {
                        var newDiscountCouponSection = new JbSection
                        {
                            Title =
                                (JbForm.Elements.Single(s => s.Name == SectionsName.DiscountCouponSection.ToString()) as JbSection).Title,
                            Name = (JbForm.Elements.Single(s => s.Name == SectionsName.DiscountCouponSection.ToString()) as JbSection).Name
                        };

                        var discountsCouponsSection = (JbForm.Elements.Single(s => s.Name == SectionsName.DiscountCouponSection.ToString()) as JbSection)
                            .Elements.ToList();

                        foreach (var element in discountsCouponsSection)
                        {
                            var couponsDiscounts = item.GetOrderChargeDiscounts()
                                .Where(c =>
                                    c.Subcategory == ChargeDiscountSubcategory.Discount &&
                                    c.Name.ToLower().Trim() == element.Title.ToLower().Trim());

                            var orderChargeDiscounts = couponsDiscounts as OrderChargeDiscount[] ?? couponsDiscounts.ToArray();

                            var couponDiscount = new JbTextBox
                            {
                                Title = element.Title,
                                Name = element.Name,
                                Value = orderChargeDiscounts.Any() ? string.Join(", ", orderChargeDiscounts.Select(c => c.Amount)) : ""
                            };
                            newDiscountCouponSection.Elements.Add(couponDiscount);
                        }
                        (newForm.Elements.Single(s => s.Name == SectionsName.DiscountCouponSection.ToString()) as JbSection).Elements.Clear();
                        (newForm.Elements.Single(s => s.Name == SectionsName.DiscountCouponSection.ToString()) as JbSection).Elements.AddRange(newDiscountCouponSection.Elements);
                    }

                    #endregion

                    #region MoreInfo Codes

                    if (JbForm.Elements.Any(e => e.Name == "MoreInfoSection"))
                    {
                        var newMoreSection = new JbSection
                        {
                            Title =
                                (JbForm.Elements.Single(s => s.Name == "MoreInfoSection") as JbSection).Title,
                            Name = (JbForm.Elements.Single(s => s.Name == "MoreInfoSection") as JbSection).Name
                        };

                        var moreSection = (JbForm.Elements.Single(s => s.Name == "MoreInfoSection") as JbSection).Elements.ToList();

                        foreach (var element in moreSection)
                        {
                            switch (element.Name)
                            {
                                case "ProgramInstructor":
                                    {
                                        var instructor = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = instructorName,
                                        };
                                        newMoreSection.Elements.Add(instructor);

                                        break;
                                    }
                                case "SchoolId":
                                    {
                                        var schoolId = new JbTextBox
                                        {
                                            Title = "School ID",
                                            Name = element.Name,
                                            Value = item.Order.Club.Id.ToString(),
                                        };
                                        newMoreSection.Elements.Add(schoolId);

                                        break;
                                    }
                                case "SchoolName":
                                    {
                                        var schoolNameItem = new JbTextBox
                                        {
                                            Title = "School",
                                            Name = element.Name,
                                            Value = item.Order.Club.Name,
                                        };
                                        newMoreSection.Elements.Add(schoolNameItem);

                                        break;
                                    }
                                case "ProgramRoomNumber":
                                    {
                                        var roomNumberItem = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = roomNumber,
                                        };
                                        newMoreSection.Elements.Add(roomNumberItem);

                                        break;
                                    }
                                case "ProgramDayOfWeek":
                                    {
                                        var dayOfWeekItem = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = dayOfWeek,
                                        };
                                        newMoreSection.Elements.Add(dayOfWeekItem);

                                        break;
                                    }
                                case "UserEmail":
                                    {
                                        var userEmail = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.Order.User.UserName,
                                        };
                                        newMoreSection.Elements.Add(userEmail);

                                        break;
                                    }
                                case "Tuition":
                                    {
                                        var tuition = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = CurrencyHelper.FormatCurrencyWithPenny(item.EntryFee, item.Order.Club.Currency),
                                        };
                                        newMoreSection.Elements.Add(tuition);

                                        break;
                                    }
                                case "TuitionName":
                                    {
                                        var entryFee = item.OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.EntryFee);
                                        var tuitionName = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = entryFee != null ? entryFee.Name : string.Empty
                                        };
                                        newMoreSection.Elements.Add(tuitionName);

                                        break;
                                    }
                                case "ScheduleStartEnd":
                                    {
                                        var scheduleStartEnd = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                        };

                                        if (item.ProgramTypeCategory != ProgramTypeCategory.ChessTournament)
                                        {
                                            scheduleStartEnd.SetValue(string.Format("{0} - {1}",
                                                item.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma),
                                                item.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma)));
                                        }
                                        else
                                        {
                                            scheduleStartEnd.SetValue(string.Format("{0} - {1}",
                                                item.Start.Value.ToString(Constants.DateTime_Comma),
                                                item.End.Value.ToString(Constants.DateTime_Comma)));
                                        }
                                        newMoreSection.Elements.Add(scheduleStartEnd);
                                        break;
                                    }
                                case "ScheduleStartDate":
                                    {
                                        var scheduleStart = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                        };

                                        if (item.ProgramTypeCategory != ProgramTypeCategory.ChessTournament)
                                        {
                                            scheduleStart.SetValue(item.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma));
                                        }
                                        else
                                        {
                                            scheduleStart.SetValue(item.Start.Value.ToString(Constants.DateTime_Comma));
                                        }
                                        newMoreSection.Elements.Add(scheduleStart);
                                        break;
                                    }
                                case "ScheduleEndDate":
                                    {
                                        var scheduleEnd = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                        };

                                        if (item.ProgramTypeCategory != ProgramTypeCategory.ChessTournament)
                                        {
                                            scheduleEnd.SetValue(item.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma));
                                        }
                                        else
                                        {
                                            scheduleEnd.SetValue(item.End.Value.ToString(Constants.DateTime_Comma));
                                        }
                                        newMoreSection.Elements.Add(scheduleEnd);
                                        break;
                                    }
                                case "DiscountTotal":
                                    {
                                        var discountTotal = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = CurrencyHelper.FormatCurrencyWithPenny(
                                            item.OrderChargeDiscounts.Where(m => !m.IsDeleted && m.OrderItemId == item.Id
                                            && m.Subcategory == ChargeDiscountSubcategory.Discount).Sum(m => m.Amount), item.Order.Club.Currency)
                                        };
                                        newMoreSection.Elements.Add(discountTotal);
                                        break;
                                    }
                                case "OrderDate":
                                    {
                                        var orderDate = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value =
                                                DateTimeHelper.ConvertUtcDateTimeToLocal(item.Order.CompleteDate, item.Order.Club.TimeZone)
                                                    .ToString(Constants.DateTime_Comma)
                                        };
                                        newMoreSection.Elements.Add(orderDate);
                                        break;
                                    }
                                case "OrderAmount":
                                    {
                                        var orderAmount = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = CurrencyHelper.FormatCurrencyWithPenny(item.TotalAmount, item.Order.Club.Currency)
                                        };
                                        newMoreSection.Elements.Add(orderAmount);
                                        break;
                                    }
                                case "OrderPaidAmount":
                                    {
                                        var orderAmount = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = CurrencyHelper.FormatCurrencyWithPenny(item.PaidAmount, item.Order.Club.Currency)
                                        };
                                        newMoreSection.Elements.Add(orderAmount);
                                        break;
                                    }
                                case "Balance":
                                    {
                                        var itemBalance = AccountService.OrderItemBalance(item);
                                        var balance = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = CurrencyHelper.FormatCurrencyWithPenny(itemBalance, item.Order.Club.Currency)
                                        };

                                        if (item.ItemStatusReason == OrderItemStatusReasons.transferOut)
                                        {
                                            balance.Value = "-";
                                        }
                                        else if (item.ItemStatusReason == OrderItemStatusReasons.canceled && itemBalance >= 0)
                                        {
                                            balance.Value = "-";
                                        }
                                        newMoreSection.Elements.Add(balance);
                                        break;
                                    }
                                case "BalanceStatus":
                                    {
                                        var value = string.Empty;
                                        if (item.PaymentPlanType == PaymentPlanType.FullPaid)
                                        {
                                            if (item.PaidAmount >= item.TotalAmount)
                                            {
                                                value = BalanceStatus.Paid.ToDescription();
                                            }
                                            else
                                            {
                                                value = BalanceStatus.PastDue.ToDescription();
                                            }
                                        }
                                        else if (item.PaymentPlanType == PaymentPlanType.Installment)
                                        {
                                            var date = Ioc.ClubBusiness.GetClubDateTime(item.Order.ClubId, DateTime.UtcNow);

                                            if (item.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && item.ProgramTypeCategory != ProgramTypeCategory.Subscription && (item.ItemStatusReason == OrderItemStatusReasons.canceled || item.ItemStatusReason == OrderItemStatusReasons.transferOut))
                                            {
                                                value = string.Empty;
                                            }
                                            else if (item.PaidAmount >= item.TotalAmount)
                                            {
                                                value = BalanceStatus.Paid.ToDescription();
                                            }
                                            else if (item.Installments.Any(i => !i.IsDeleted && i.Status != OrderStatusCategories.canceled && i.InstallmentDate < date && ((i.PaidAmount.HasValue && i.Amount > i.PaidAmount) || i.PaidAmount == null)))
                                            {
                                                value = BalanceStatus.PastDue.ToDescription();
                                            }
                                            else
                                            {
                                                value = BalanceStatus.Current.ToDescription();
                                            }
                                        }

                                        var balanceStatus = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = value
                                        };
                                        newMoreSection.Elements.Add(balanceStatus);

                                        break;
                                    }
                                case "ConfirmationId":
                                    {
                                        var confirmationId = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.Order.ConfirmationId
                                        };
                                        newMoreSection.Elements.Add(confirmationId);
                                        break;
                                    }
                                case "ProgramStart":
                                    {
                                        var programStart = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma)
                                        };
                                        newMoreSection.Elements.Add(programStart);
                                        break;
                                    }
                                case "ProgramEnd":
                                    {
                                        var programEnd = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma)
                                        };
                                        newMoreSection.Elements.Add(programEnd);
                                        break;
                                    }
                                case "ProgramLocation":
                                    {
                                        var programLocation = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value =
                                                (!string.IsNullOrEmpty(item.ProgramSchedule.Program.ClubLocation.Name))
                                                    ? string.Format("{0},{1}",
                                                        item.ProgramSchedule.Program.ClubLocation.Name,
                                                        item.ProgramSchedule.Program.ClubLocation.PostalAddress.Address)
                                                    : item.ProgramSchedule.Program.ClubLocation.PostalAddress.Address
                                        };
                                        newMoreSection.Elements.Add(programLocation);
                                        break;
                                    }
                                case "ProgramLocationName":
                                    {
                                        var locationName = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value =
                                                (!string.IsNullOrEmpty(item.ProgramSchedule.Program.ClubLocation.Name))
                                                    ? item.ProgramSchedule.Program.ClubLocation.Name
                                                    : "N/A"
                                        };
                                        newMoreSection.Elements.Add(locationName);
                                        break;
                                    }
                                case "ProgramType":
                                    {
                                        var programType = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.ProgramTypeCategory.ToDescription()
                                        };
                                        newMoreSection.Elements.Add(programType);
                                        break;
                                    }
                                case "OrderType":
                                    {
                                        var orderType = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.Order.OrderMode.ToString()
                                        };
                                        newMoreSection.Elements.Add(orderType);
                                        break;
                                    }
                                case "Charges":
                                    {
                                        JbTextBox charges;
                                        var charge = item.GetOrderChargeDiscounts().Where(
                                            c =>
                                                c.Category != ChargeDiscountCategory.EntryFee &&
                                                c.Name.ToLower().Trim() == element.Title.ToLower().Trim());
                                        if (charge.Any())
                                        {
                                            var chargesAmount = item.GetOrderChargeDiscounts().Where(
                                                                c =>
                                                                c.Category != ChargeDiscountCategory.EntryFee &&
                                                                c.Name.ToLower().Trim() == element.Title.ToLower().Trim())
                                                                .Sum(c => c.Amount);
                                            charges = new JbTextBox
                                            {
                                                Title = element.Title,
                                                Name = element.Name,
                                                Value = CurrencyHelper.FormatCurrencyWithPenny(chargesAmount, item.Order.Club.Currency)
                                            };
                                        }
                                        else
                                        {
                                            charges = new JbTextBox
                                            {
                                                Title = element.Title,
                                                Name = element.Name,
                                                Value = "-"
                                            };
                                        }

                                        newMoreSection.Elements.Add(charges);
                                        break;
                                    }
                                case "PaymentMethod":
                                    {
                                        var paymentMethod = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                        };
                                        var value = (item.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success)
                                            ? string.Join(", ", item.TransactionActivities.Where(t => t.TransactionStatus == TransactionStatus.Success).Select(p => p.PaymentDetail.PaymentMethod.ToDescription()))
                                            : Constants.S_Dash);

                                        ((JbTextBox)paymentMethod).Value = value;
                                        newMoreSection.Elements.Add(paymentMethod);
                                        break;
                                    }
                                case "PaymentDate":
                                    {
                                        var paymentDate = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                        };
                                        var value = (item.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success)
                                            ? string.Join(", ", item.TransactionActivities.Where(t => t.TransactionStatus == TransactionStatus.Success).Select(p => p.TransactionDate.ToString("MM/dd/yyyy")))
                                            : Constants.S_Dash);

                                        ((JbTextBox)paymentDate).Value = value;
                                        newMoreSection.Elements.Add(paymentDate);
                                        break;
                                    }
                                case "ExtraServices":
                                    {
                                        var exteraService = new JbTextBox
                                        {
                                            Title = "Extra services",
                                            Name = element.Name,
                                        };

                                        var strServices = new List<string>();
                                        var exteraServices = (item.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.Lunch || c.Category == ChargeDiscountCategory.DayCare || c.Category == ChargeDiscountCategory.CustomCharge));

                                        if (exteraServices != null)
                                        {
                                            foreach (var charge in exteraServices)
                                            {
                                                var chargeTitle =
                                                    $"{charge.Name} ({CurrencyHelper.FormatCurrencyWithPenny(charge.Amount, item.Order.Club.Currency)})";
                                                strServices.Add(chargeTitle);
                                            }
                                        }

                                        var value = strServices != null ? string.Join(", ", strServices) : Constants.S_Dash;
                                        ((JbTextBox)exteraService).Value = value;
                                        newMoreSection.Elements.Add(exteraService);

                                        break;
                                    }
                                case "PaymentStatus":
                                    {
                                        var paidOrder = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                        };
                                        if (item.PaidAmount == 0)
                                        {
                                            ((JbTextBox)paidOrder).Value = "Unpaid";
                                        }

                                        else switch (item.ItemStatus)
                                            {
                                                case OrderItemStatusCategories.completed when item.PaidAmount >= item.TotalAmount:
                                                    ((JbTextBox)paidOrder).Value = "Paid";
                                                    break;
                                                case OrderItemStatusCategories.completed:
                                                    if (item.PaidAmount < item.TotalAmount)
                                                    {
                                                        ((JbTextBox)paidOrder).Value = "Partially Paid";
                                                    }

                                                    break;
                                            }
                                        newMoreSection.Elements.Add(paidOrder);
                                        break;
                                    }
                                case "PaymentMethodOrder":
                                    {
                                        var paymentMethod = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                        };
                                        var value = item.Order.PaymentMethod.HasValue ? item.Order.PaymentMethod.ToDescription() : Constants.S_Dash;

                                        ((JbTextBox)paymentMethod).Value = value;
                                        newMoreSection.Elements.Add(paymentMethod);
                                        break;
                                    }
                                case "TransactionId":
                                    {
                                        var transactionId = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                        };
                                        var value = (item.TransactionActivities.Any(t => t.TransactionStatus == TransactionStatus.Success)
                                            ? string.Join(", ", item.TransactionActivities.Where(t => t.TransactionStatus == TransactionStatus.Success).Select(p => !string.IsNullOrEmpty(p.PaymentDetail.TransactionId) ? p.PaymentDetail.TransactionId : !string.IsNullOrEmpty(p.CheckId) ? p.CheckId : "-"))
                                            : Constants.S_Dash);

                                        ((JbTextBox)transactionId).Value = value;
                                        newMoreSection.Elements.Add(transactionId);
                                        break;
                                    }
                                case "SchoolDistrict":
                                    {
                                        var schoolDistrict = new JbTextBox
                                        {
                                            Title = "District",
                                            Name = element.Name,
                                            Value = item.Order.Club.District != null ? item.Order.Club.District.Name : string.Empty
                                        };
                                        newMoreSection.Elements.Add(schoolDistrict);
                                        break;
                                    }
                                case "SeasonName":
                                    {
                                        var seasonName = new JbTextBox
                                        {
                                            Title = "Season",
                                            Name = element.Name,
                                            Value = item.Season.Title
                                        };
                                        newMoreSection.Elements.Add(seasonName);
                                        break;
                                    }
                                case "ScheduleId":
                                    {
                                        var scheduleId = new JbTextBox
                                        {
                                            Title = "Schedule ID",
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.Id.ToString()
                                        };
                                        newMoreSection.Elements.Add(scheduleId);
                                        break;
                                    }
                                case "ScheduleTitle":
                                    {
                                        var scheduleTitle = new JbTextBox
                                        {
                                            Title = "Schedule title",
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.Title
                                        };
                                        newMoreSection.Elements.Add(scheduleTitle);
                                        break;
                                    }
                                case "DesiredStartDate":
                                    {
                                        var desiredStartDate = new JbTextBox
                                        {
                                            Title = "Desired start date",
                                            Name = element.Name,
                                            Value = item.DesiredStartDate?.ToString(Constants.DateTime_Comma)
                                        };
                                        newMoreSection.Elements.Add(desiredStartDate);
                                        break;
                                    }
                                case "ClassDays":
                                    {
                                        var classDays = new JbTextBox
                                        {
                                            Title = "Class days",
                                            Name = element.Name,
                                            Value = item.Attributes != null ? string.Join(", ", DateTimeHelper.GetDayOfWeekAbbreviation(item.Attributes.WeekDays, AbbreviatedDayNameMode.TwoLetter)) : string.Empty
                                        };
                                        newMoreSection.Elements.Add(classDays);
                                        break;
                                    }
                                case "OrganizationActiveType":
                                    {
                                        var organizationActiveType = new JbTextBox
                                        {
                                            Title = "Member is active",
                                            Name = element.Name,
                                            Value = item.Order.Club.IsInActive ? "No" : "Yes"
                                        };
                                        newMoreSection.Elements.Add(organizationActiveType);
                                        break;
                                    }
                                case "RegistrationId":
                                    {
                                        var registrationId = new JbTextBox
                                        {
                                            Title = "Registration ID",
                                            Name = element.Name,
                                            Value = item.Id.ToString()
                                        };
                                        newMoreSection.Elements.Add(registrationId);
                                        break;
                                    }
                                case "Discounts_Coupons":
                                    {
                                        var discountsCoupons = new JbTextBox
                                        {
                                            Title = "Discounts/Coupons",
                                            Name = element.Name,
                                            Value = string.Join(", ", item.GetOrderChargeDiscounts()
                                                .Where(o =>
                                                    o.Subcategory == ChargeDiscountSubcategory.Discount)
                                                .Select(o => o.Name))
                                        };
                                        newMoreSection.Elements.Add(discountsCoupons);
                                        break;
                                    }
                                case "CancellationEffectiveDate":
                                    {
                                        var cancellationEffectiveDate = new JbTextBox
                                        {
                                            Title = "Cancellation effective date",
                                            Name = element.Name,
                                            Value = item.Attributes.CancelEffectiveDate.HasValue ? item.Attributes.CancelEffectiveDate.Value.ToString(Constants.DateTime_Comma) : string.Empty
                                        };
                                        newMoreSection.Elements.Add(cancellationEffectiveDate);
                                        break;
                                    }
                                case "TransferEffectiveDate":
                                    {
                                        var transferEffectiveDate = new JbTextBox
                                        {
                                            Title = "Transfer effective date",
                                            Name = element.Name,
                                            Value = item.Attributes.TransferEffectiveDate.HasValue ? item.Attributes.TransferEffectiveDate.Value.ToString(Constants.DateTime_Comma) : string.Empty
                                        };
                                        newMoreSection.Elements.Add(transferEffectiveDate);
                                        break;
                                    }
                                case "OrderStatus":
                                    {
                                        var status = new JbTextBox
                                        {
                                            Title = "Order status",
                                            Name = element.Name,
                                            Value = item.ItemStatusReason.ToDescription().ToString()
                                        };
                                        newMoreSection.Elements.Add(status);
                                        break;
                                    }
                                case "DropinDates":
                                    {
                                        var dropinDates = new JbTextBox
                                        {
                                            Title = "Drop-in dates",
                                            Name = element.Name,
                                            Value = item.Mode == OrderItemMode.DropIn ? string.Join(", ", item.OrderSessions.Where(o => o.ProgramSession != null).Select(o => o.ProgramSession.StartDateTime.ToString("MM/dd/yy"))) : string.Empty
                                        };
                                        newMoreSection.Elements.Add(dropinDates);
                                        break;
                                    }
                                case "ProgramStartDate":
                                    {
                                        var programStartDate = new JbTextBox
                                        {
                                            Title = "Program start date",
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.Program.ProgramSchedules.First().StartDate.ToString(Constants.DateTime_Comma)
                                        };
                                        newMoreSection.Elements.Add(programStartDate);
                                        break;
                                    }
                                case "ProgramEndDate":
                                    {
                                        var programEndDate = new JbTextBox
                                        {
                                            Title = "Program end date",
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.Program.ProgramSchedules.Last().EndDate.ToString(Constants.DateTime_Comma)
                                        };
                                        newMoreSection.Elements.Add(programEndDate);
                                        break;
                                    }
                                case "ScheduleStartTime":
                                    {
                                        var value = string.Empty;
                                        var daysFormated = new List<string>();
                                        var _reportBusiness = Ioc.NewReportBusiness;

                                        if (item.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both)
                                        {
                                            var beforeScheduleTitle = item.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.AM).Title;
                                            var afterScheduleTitle = item.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.PM).Title;

                                            value = $"{beforeScheduleTitle} ({_reportBusiness.GetDayOfWeekWithStartTime(((ScheduleAfterBeforeCareAttribute)item.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.AM).Attributes).Days)}) - " +
                                                $"{afterScheduleTitle} ({_reportBusiness.GetDayOfWeekWithStartTime(((ScheduleAfterBeforeCareAttribute)item.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.PM).Attributes).Days)})";
                                        }
                                        else
                                        {
                                            if (item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                            {
                                                value = _reportBusiness.GetDayOfWeekWithStartTime(((ScheduleAfterBeforeCareAttribute)item.ProgramSchedule.Attributes).Days);
                                            }
                                            else if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                            {
                                                value = _reportBusiness.GetDayOfWeekWithStartTime(((ScheduleSubscriptionAttribute)item.ProgramSchedule.Attributes).Days);
                                            }
                                            else
                                            {
                                                value = _reportBusiness.GetDayOfWeekWithStartTime(((ScheduleAttribute)item.ProgramSchedule.Attributes).Days);
                                            }
                                        }

                                        var scheduleStartTime = new JbTextBox
                                        {
                                            Title = "Schedule start time",
                                            Name = element.Name,
                                            Value = value
                                        };
                                        newMoreSection.Elements.Add(scheduleStartTime);
                                        break;
                                    }
                                case "ScheduleEndTime":
                                    {
                                        var value = string.Empty;
                                        var daysFormated = new List<string>();
                                        var _reportBusiness = Ioc.NewReportBusiness;

                                        if (item.ProgramSchedule.ScheduleMode == TimeOfClassFormation.Both)
                                        {
                                            var beforeScheduleTitle = item.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.AM).Title;
                                            var afterScheduleTitle = item.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.PM).Title;

                                            value = $"{beforeScheduleTitle} ({_reportBusiness.GetDayOfWeekWithEndTime(((ScheduleAfterBeforeCareAttribute)item.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.AM).Attributes).Days)}) - " +
                                                $"{afterScheduleTitle} ({_reportBusiness.GetDayOfWeekWithEndTime(((ScheduleAfterBeforeCareAttribute)item.ProgramSchedule.Program.ProgramSchedules.Single(p => p.ScheduleMode == TimeOfClassFormation.PM).Attributes).Days)})";
                                        }
                                        else
                                        {
                                            if (item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare)
                                            {
                                                value = _reportBusiness.GetDayOfWeekWithEndTime(((ScheduleAfterBeforeCareAttribute)item.ProgramSchedule.Attributes).Days);
                                            }
                                            else if (item.ProgramTypeCategory == ProgramTypeCategory.Subscription)
                                            {
                                                value = _reportBusiness.GetDayOfWeekWithEndTime(((ScheduleSubscriptionAttribute)item.ProgramSchedule.Attributes).Days);
                                            }
                                            else
                                            {
                                                value = _reportBusiness.GetDayOfWeekWithEndTime(((ScheduleAttribute)item.ProgramSchedule.Attributes).Days);
                                            }
                                        }

                                        var scheduleEndTime = new JbTextBox
                                        {
                                            Title = "Schedule end time",
                                            Name = element.Name,
                                            Value = value
                                        };
                                        newMoreSection.Elements.Add(scheduleEndTime);
                                        break;
                                    }
                            }

                        }
        (newForm.Elements.Single(s => s.Name == "MoreInfoSection") as JbSection).Elements.Clear();
                        (newForm.Elements.Single(s => s.Name == "MoreInfoSection") as JbSection).Elements.AddRange(newMoreSection.Elements);
                    }

                    #endregion

                    #region Chess Tourney codes

                    if (JbForm.Elements.Any(e => e.Name == "chessSection") && item.ProgramTypeCategory == ProgramTypeCategory.ChessTournament)
                    {
                        var newMoreSection = new JbSection
                        {
                            Title =
                                (JbForm.Elements.Single(s => s.Name == "chessSection") as JbSection).Title,
                            Name = (JbForm.Elements.Single(s => s.Name == "chessSection") as JbSection).Name
                        };

                        var moreSection =
                            (JbForm.Elements.Single(s => s.Name == "chessSection") as JbSection).Elements.ToList();

                        foreach (var element in moreSection)
                        {
                            switch (element.Name)
                            {
                                case "SectionName":
                                    {
                                        var sectionName = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.OrderItemChess.Section,
                                        };
                                        newMoreSection.Elements.Add(sectionName);
                                        break;
                                    }
                                case "Bye":
                                    {
                                        var bye = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.OrderItemChess.Byes,
                                        };
                                        newMoreSection.Elements.Add(bye);
                                        break;
                                    }
                            }
                        }
                        (newForm.Elements.Single(s => s.Name == "chessSection") as JbSection).Elements.Clear();
                        (newForm.Elements.Single(s => s.Name == "chessSection") as JbSection).Elements.AddRange(newMoreSection.Elements);
                    }

                    #endregion

                    #region Chess tournament codes

                    if (JbForm.Elements.Any(e => e.Name == "ChessTournamentSection") && item.ProgramTypeCategory == ProgramTypeCategory.ChessTournament)
                    {
                        var newMoreSection = new JbSection
                        {
                            Title =
                                (JbForm.Elements.Single(s => s.Name == "ChessTournamentSection") as JbSection).Title,
                            Name = (JbForm.Elements.Single(s => s.Name == "ChessTournamentSection") as JbSection).Name
                        };

                        var moreSection =
                            (JbForm.Elements.Single(s => s.Name == "ChessTournamentSection") as JbSection).Elements.ToList();

                        foreach (var element in moreSection)
                        {
                            switch (element.Name)
                            {
                                case "SectionName":
                                    {
                                        var sectionName = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.OrderItemChessId.HasValue ? item.OrderItemChess.Section : string.Empty,
                                        };
                                        newMoreSection.Elements.Add(sectionName);
                                        break;
                                    }
                                case "ScheduleName":
                                    {
                                        var bye = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.OrderItemChessId.HasValue ? item.OrderItemChess.Schedule : string.Empty,
                                        };
                                        newMoreSection.Elements.Add(bye);
                                        break;
                                    }
                            }
                        }
                        (newForm.Elements.Single(s => s.Name == "ChessTournamentSection") as JbSection).Elements.Clear();
                        (newForm.Elements.Single(s => s.Name == "ChessTournamentSection") as JbSection).Elements.AddRange(newMoreSection.Elements);
                    }

                    #endregion

                    newForm.CurrentMode = AccessMode.ReadOnly;
                    DataTable.Add(newForm);

                    //DataTable.AddRange(followupForms);
                }
            }
        }

        private JbForm RightCopyNewByAny(JbForm sourceForm, JbForm destForm, bool replaceElements = false)
        {
            if (sourceForm == null)
            {
                return null;
            }

            var newForm = new JbForm();
            foreach (var rootElement in sourceForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    JbSection section;
                    if (TryGet<JbSection>(destForm.Elements, rootElement, out section))
                    {
                        if (!newForm.Elements.Any(s => s.ElementId == section.ElementId))
                            newForm.Elements.Add(new JbSection()
                            {
                                ElementId = section.ElementId,
                                Name = section.Name,
                                Title = section.Title
                            });
                        foreach (var sourceElement in (rootElement as JbSection).Elements)
                        {
                            JbBaseElement element;
                            if (TryGet((section as JbSection).Elements, sourceElement, out element))
                            {
                                if (replaceElements)
                                {
                                    (newForm.Elements.FirstOrDefault(e => e.Title.Equals(section.Title) && e.ElementId == section.ElementId) as JbSection).Elements.Add(sourceElement);
                                }
                                else
                                {
                                    (newForm.Elements.FirstOrDefault(e => e.Title.Equals(section.Title) && e.Name.Equals(section.Name) && e.ElementId == section.ElementId) as JbSection).Elements.Add(element);
                                }
                            }
                        }
                    }
                }
            }

            foreach (var element in destForm.Elements)
            {
                if (element is JbSection && newForm.Elements.All(s => s.ElementId != element.ElementId) || ((JbSection)newForm.Elements.Single(s => s.Name == element.Name)).Elements.Count != ((JbSection)element).Elements.Count)
                {
                    var missSection = new JbSection
                    {
                        Title = element.Title,
                        Name = element.Name,
                        ElementId = element.ElementId
                    };
                    foreach (var item in ((JbSection)element).Elements)
                    {
                        if (newForm.Elements.All(s => s.Name != element.Name) || newForm.Elements.Any(s => s.Name == element.Name) && ((JbSection)newForm.Elements.Single(s => s.Name == element.Name)).Elements.All(e => e.Name != item.Name))
                        {
                            missSection.Elements.Add(new JbTextBox
                            {
                                Value = (sourceForm.Elements.Any(s => s.Name == element.Name) && ((JbSection)sourceForm.Elements.Single(s => s.Name == element.Name)).Elements.Any(s => s.Name == item.Name) && ((JbSection)sourceForm.Elements.Single(s => s.Name == element.Name)).Elements.Single(s => s.Name == item.Name).GetValue() != null) ?
                                ((JbSection)sourceForm.Elements.Single(s => s.Name == element.Name)).Elements.Single(s => s.Name == item.Name).GetValue().ToString() : "",
                                Name = item.Name,
                                Title = item.Title
                            });
                        }
                    }
                    if (newForm.Elements.Any(e => e.Name == element.Name))
                    {
                        ((JbSection)newForm.Elements.Single(e => e.Name == element.Name)).Elements.AddRange(missSection.Elements);
                    }
                    else
                    {
                        newForm.Elements.Add(missSection);
                    }
                }
            }

            return newForm;
        }

        private bool TryGet<T>(IEnumerable<IJbBaseElement> sourceElements, IJbBaseElement searchElement, out T newElement) where T : JbBaseElement, new()
        {
            newElement = new T();
            try
            {

                //Func<JbBaseElement, bool> filter = e => e.Equals(searchElement);
                if (sourceElements.Any(e => e.Equals(searchElement)) ||
                    sourceElements.Any(e => !string.IsNullOrEmpty(searchElement.Title) && !string.IsNullOrEmpty(e.Title) && e.Title.Replace(" ", "").ToLower().Equals(searchElement.Title.Replace(" ", "").ToLower())))
                {
                    var tempElement = sourceElements.FirstOrDefault(e => !string.IsNullOrEmpty(searchElement.Title) && !string.IsNullOrEmpty(e.Title)
                        && e.Title.Replace(" ", "").ToLower().Equals(searchElement.Title.Replace(" ", "").ToLower()));
                    if (tempElement == null)
                        tempElement = sourceElements.FirstOrDefault(e => e.Name.Equals(searchElement.Name));
                    if (tempElement == null)
                        tempElement = sourceElements.FirstOrDefault(e => e.ElementId.Equals(searchElement.ElementId));
                    if (tempElement != null)
                    {
                        newElement = tempElement as T;
                        return true;
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}