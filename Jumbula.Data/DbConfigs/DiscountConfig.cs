﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class DiscountConfig : EntityTypeConfiguration<Discount>
    {
        public DiscountConfig()
        {
            Property(d => d.Name).HasMaxLength(256).IsRequired();
            HasMany(d => d.Programs).WithMany(p => p.Discounts).Map(pd =>
            {
                pd.MapLeftKey("DiscountId");
                pd.MapRightKey("ProgramId");
                pd.ToTable("ProgramDiscounts");
            });


        }
    }
}
