﻿angular.module('dashboardApp').directive('textTooltip', function () {
    return {
        restrict: 'E',
        scope: {
            title: '@title',
            desc: '@desc',
            align: '@align',
            vAlign: '@vAlign'
        },
        templateUrl: "/Areas/Dashboard/Scripts/3-Directives/Tooltips/TextTooltip/TextTooltip.html",

        link: function (scope, element, attributes) {

            scope.toggleTextTooltip = function () {
                element.find('.container-text-tooltip').stop().slideToggle(200);
            }
            if (scope.align && (scope.align == 'left' || scope.align == 'center' || scope.align == 'right')) {
                element.addClass('text-tooltip-align-' + scope.align);
            }

            if (scope.vAlign && (scope.vAlign == 'top')) {
                element.addClass('text-tooltip-valign-' + scope.vAlign);
            }
        }
    }
});


