﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;

namespace Jumbula.Core.Domain
{
    public class Coupon : BaseEntity, IProtectedResource
    {
        public Coupon()
        {
            Programs = new List<Program>();
            MetaData = new MetaData();
            AmounType = ChargeDiscountType.Fixed;            
            Deleted = false;
        }

        [MaxLength(256)]
        public string Name { get; set; }
        public CouponType CouponType { get; set; }
        public string Code { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Amount { get; set; }
        public ChargeDiscountType AmounType { get; set; }
        public CalculationType CouponCalculateType { get; set; }
        public MetaData MetaData { get; set; }
        public string ClubDomain { get; set; }
        public CouponAddType AddType { get; set; }
        public int? NumberOfUse { get; set; }
        public bool Deleted { get; set; }
        public bool IsAllProgram { get; set; }
        public bool IsForAllUsers { get; set; }
        public bool IsOneTimeUse { get; set; }
        public long? SeasonId { get; set; }
        public virtual Season Season { get; set; }
        public virtual ICollection<Program> Programs { get; set; }
        public virtual ICollection<OrderChargeDiscount> OrderChargeDiscounts { get; set; }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Season.ClubId == clubId)
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            return false;
        }
    }

    public enum CouponAddType : byte
    {
        Manually,
        Upload
    }
   
}
