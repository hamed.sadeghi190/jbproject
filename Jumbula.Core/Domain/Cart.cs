﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public enum CartMode
    {
        None,
        CancelOrder
    }

    public class Cart
    {
        public CartMode Mode { get; set; }

        public Order Order { get; set; }

      
        public Cart()
        {
            Order = new Order();        
            Order.OrderItems = new HashSet<OrderItem>();
        }

        public int Count
        {
            get
            {
                //return Items.Count;
                return Order.RegularOrderItems.Count() + Order.PreregisterItems.Count();
            }
        }

        public decimal PayableAmount { get; set; }
        public bool IsEmpty { get; set; }
        public decimal SubTotal
        {
            get
            {
                return CalcSubTotal();
            }
        }

        public decimal Shipping
        {
            get
            {
                return CalcShipping();
            }
        }

        public decimal Total
        {
            get
            {
                return SubTotal + Shipping;
            }
        }

        /// <summary>
        /// We should get this from the globalization data once we go international
        /// </summary>
        public CurrencyCodes Currency
        {
            get
            {
                if (Order != null && Order.Club != null)
                {
                    return Order.Club.Currency;
                }
                else
                    return CurrencyCodes.USD;
            }
        }

        private decimal CalcSubTotal()
        {
            decimal price = 0;
          
            foreach (var item in Order.RegularOrderItems)
            {
                foreach (var charge in item.GetOrderChargeDiscounts())
                {
                    if (charge.Category > 0)
                        price += charge.Amount;
                    else
                        price -= Math.Abs(charge.Amount);
                }
            }

            return Math.Round(price, 2);
        }

        private decimal CalcShipping()
        {
            return 0;
        }

    }

    public class CartCoupon
    {
        public int CouponId { get; set; }
        public string CouponCode { get; set; }
        public decimal Amount { get; set; }
    }
}
