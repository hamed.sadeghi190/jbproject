﻿

namespace Jumbula.Common.Enums
{
    public enum ChargeDiscountType
    {
        Fixed,
        Percent,
    }
}
