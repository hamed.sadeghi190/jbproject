﻿using Jumbula.Common.Delegates;
using Jumbula.Core.Infrastructure.Email;
using System;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Jumbula.Infrastructure.Email
{
    public class CommonEmailProvider : IEmailProvider
    {
        #region Fields
        private readonly SmtpClient _smtpClient;
        private readonly SmtpSection _emailConfig;
        #endregion

        #region Constructors
        public CommonEmailProvider(SmtpSection config)
        {
            _smtpClient = new SmtpClient(config.Network.Host, config.Network.Port);
            _emailConfig = config;

            _smtpClient.EnableSsl = config.Network.EnableSsl;
            _smtpClient.Credentials = new NetworkCredential(config.Network.UserName, config.Network.Password);
        }
        #endregion

        #region Public Methods

        public void Send(MailMessage message, int emailId, EmailCompletedEventHandler emailCompletedEventHandler, bool isCampaignMode = false)
        {
            if (message.Priority == MailPriority.High)
                Task.Run(() => SendAsync(message, emailId, emailCompletedEventHandler));
            else
                SendSync(message, emailId, emailCompletedEventHandler);
        }

        #endregion

        #region Private Methods

        private void SendSync(MailMessage message, int emailId, EmailCompletedEventHandler emailCompletedEventHandler)
        {
            try
            {
                _smtpClient.Send(message);

                emailCompletedEventHandler(_smtpClient, new EmailCompletedEventArgs(emailId, true));
            }
            catch (Exception ex)
            {
                emailCompletedEventHandler(_smtpClient, new EmailCompletedEventArgs(emailId, ex));
            }
        }

        private void SendAsync(MailMessage message, int emailId, EmailCompletedEventHandler emailCompletedEventHandler)
        {
            Func<MailMessage, int, Task> sendMail = async (mail, mailId) =>
            {
                using (var smtp = new SmtpClient(_emailConfig.Network.Host, _emailConfig.Network.Port))
                {
                    smtp.Credentials = new NetworkCredential(_emailConfig.Network.UserName, _emailConfig.Network.Password);
                    smtp.EnableSsl = _emailConfig.Network.EnableSsl;

                    await smtp.SendMailAsync(mail).ContinueWith(response =>
                    {
                        try
                        {
                            emailCompletedEventHandler(this, new EmailCompletedEventArgs(emailId, response.Status == TaskStatus.RanToCompletion));
                        }
                        catch (Exception e)
                        {
                            emailCompletedEventHandler(this, new EmailCompletedEventArgs(emailId, e));
                        }
                    });
                }
            };

            sendMail(message, emailId);
        }


        #endregion
    }
}
