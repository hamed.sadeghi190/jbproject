﻿using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Identity
{
    public class UserAdditionalData : IAdditionalData
    {
        public UserAdditionalData()
        {
            CurrentUser = new CurrentUser();
            CurrentOrganization = new ClubBaseInfoModel();
        }

        public ICurrentUser CurrentUser { get; set; }

        public ICurrentOrganization CurrentOrganization { get; set; }
    }

    public class CurrentUser : ICurrentUser
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public RoleCategory Role { get; set; }

    }
}
