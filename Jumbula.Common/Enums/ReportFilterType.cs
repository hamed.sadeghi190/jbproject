﻿namespace Jumbula.Common.Enums
{
    public enum ReportFilterType
    {
        DropDown,
        TextBox,
        DatePicker
    }
}
