﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('sessionAttendanceController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();
        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var programId = $scope.stateParams.programId;
        var sessionId = $scope.stateParams.sessionId;

        $http.get(window.location.origin + '/Dashboard/Program/GetSessionAttendanceList?programid=' + programId + '&sessionId=' + sessionId).success(function (data, status, headers, config) {
            $scope.Model = data;
        });

        $scope.submitAttendance = function () {
            $http.post(window.location.origin + '/Dashboard/Program/UpdateSessionAttendanceList', { model: $scope.Model })
                .success(function (data, status, headers, config) {

                    jbToast.success("List updated successfully.");

                    $state.go('ProgramSession', { seasonDomain: $scope.MC.stateParams.seasonDomain, programId: programId })
                })
        }
    }]);
})();

