﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Model.CreditCard
{
    public class ParentBillingMethodsViewModel : BaseViewModel<UserCreditCard>
    {
        public ParentBillingMethodsViewModel()
        {

        }

        public ParentBillingMethodsViewModel(string planType)
        {
            PlanType = planType;
        }

        [Display(Name = "Card number")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter a valid card number.")]
        [StringLength(17, ErrorMessage = "{0} cannot be longer than 17 characters.")]

        public string CardNumber { get; set; }

        public string Last4Digit { get; set; }

        [Display(Name = "Expiration year")]
        public int Year2Char { get; set; }

        public string Method => $"{Brand} ***{Last4Digit}";
        public string Brand { get; set; }
        public string ExpiryDate => $"{Month}/{Year}";
        public string ExpiryDateAuthorize => $"{Month}{Year}";

        [Display(Name = "CVC")]
        [Required(ErrorMessage = "{0} is required")]
        [RegularExpression("[0-9]+$", ErrorMessage = "Please enter a valid CVC number.")]
        [StringLength(5, ErrorMessage = "{0} is too long.")]
        public string CVV { get; set; }

        [Display(Name = "Expiration year")]

        public string Year { get; set; }

        public string CustomerResponse { get; set; }

        [Display(Name = "Expiration month")]
        [Required(ErrorMessage = "Expiration date is required")]
        public string Month { get; set; }

        [DisplayName("Cardholder name")]
        [Required(ErrorMessage = "{0} is required")]
        public string CardHolderName { get; set; }


        [Required(ErrorMessage = "Address is required.")]
        public string Address { get; set; }


        public PostalAddress PostalAddress { get; set; }
        [DisplayName(" ")]
        public bool IsDefault { get; set; }

        public string PlanType { get; set; }

        public PageMode PageMode { get; set; }


    }
}
