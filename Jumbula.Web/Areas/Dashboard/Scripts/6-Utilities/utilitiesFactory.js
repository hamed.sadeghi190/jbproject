﻿(function () {
    'use strict';

    angular.module('dashboardApp').factory("jbUtils", [function () {
        return {
            removeSpecialChars: function (string) {
                if (string)
                    return string.replace(/[^a-zA-Z0-9]/g, "");
            },
            removeDamagingChars: function (string) {
                if (string)
                    return string.replace('"', "").replace("'", "’");
            }
        };
    }]);
})();