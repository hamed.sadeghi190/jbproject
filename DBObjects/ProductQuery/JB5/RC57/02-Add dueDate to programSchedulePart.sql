
alter table programscheduleparts drop CONSTRAINT  [FK_dbo.ProgramScheduleParts_dbo.ProgramSchedules_ProgramScheduleId]

GO

ALTER TABLE ProgramScheduleParts ALTER COLUMN ProgramScheduleId bigint NULL

Go

ALTER TABLE [dbo].[ProgramScheduleParts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramScheduleParts_dbo.ProgramSchedules_ProgramScheduleId] FOREIGN KEY([ProgramScheduleId])
REFERENCES [dbo].[ProgramSchedules] ([Id])
GO
 
ALTER TABLE ProgramScheduleParts ADD  ProgramId bigint NULL
Go
ALTER TABLE [dbo].[ProgramScheduleParts]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ProgramScheduleParts_dbo.Programs_ProgramId] FOREIGN KEY([ProgramId])
REFERENCES [dbo].[Programs] ([Id])
ON DELETE CASCADE

ALTER TABLE ProgramScheduleParts ADD  DueDate DATETIME NULL

