﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('TotalSalesController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, jbToast, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'campaigns-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/Season';
        $scope.Model = {};
        var colors = ["#c2e4ff", "#8fcdff", "#5cb7ff", "#29a0ff", "#0088f5", "#006cc2", "#00508f", "#00335c"];

        $http.get(baseUrl + "/GetTotalSalesPieChart", { params: { seasonDomain: $scope.MC.stateParams.seasonDomain } })
            .success(function (data, status, headers, config) {
                $scope.Model.Totals = data;
            });

        $scope.dataSourcePieChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetSalesPieChartReport",
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            seasonDomain: $scope.MC.stateParams.seasonDomain,
                        }
                    }
                }
            },
            sort: {
                field: "Price",
                dir: "asc"
            }
        });

        $scope.dataSourcePieCountChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetSalesPieChartReport",
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            seasonDomain: $scope.MC.stateParams.seasonDomain,
                            isPercentage: false
                        }
                    }
                }
            },
            sort: {
                field: "Price",
                dir: "asc"
            }
        });

        $scope.pieChartOption = {
            dataSource: $scope.dataSourcePieChart,
            legend: {
                visible: false,
            },
            chartArea: {
                background: ""
            },
            seriesDefaults: {
                type: "pie",
                overlay: {
                    gradient: "none"
                },
            },
            seriesColors: colors,
            series: [{
                field: "Price",
                categoryField: "ProgramName",
                padding: 0
            }],
            tooltip: {
                visible: true,
                format: "N0",
                template: "#= category # (#= kendo.format('{0:P}',percentage)#) - (#= kendo.format('{0:C}', value)#)"
            }
        };

        $scope.pieCountChartOption = {
            dataSource: $scope.dataSourcePieCountChart,
            legend: {
                visible: false,
            },
            chartArea: {
                background: "",
            },
            seriesDefaults: {
                type: "pie",
                overlay: {
                    gradient: "none"
                },
            },
            seriesColors: colors,
            series: [{
                field: "Price",
                categoryField: "ProgramName",
                padding: 0
            }],
            tooltip: {
                visible: true,
                format: "N0",
                //template: "#= category # - #= (value)#"
                template: "#= category # - #= kendo.format('{0:P}', percentage)# - (#= value#)"
            }
        };

    }]);
})();