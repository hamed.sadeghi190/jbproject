﻿

namespace Jumbula.Common.Enums
{
    public enum ImageGalleryType
    {
        Tourney,
        Camp,
        Course, /* used instead of class, which is a researved word :) */
        League,
        Coach,
        Club,
        Catalog,
        Program
    }

}
