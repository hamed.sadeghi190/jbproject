﻿using Jumbula.Common.Enums;
using System;
using Jumbula.Common.Helper;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.ClubSetting;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Business
{
    public class ClubSettingBusiness : IClubSettingBusiness
    {
        #region Constants
        #endregion

        #region Fields
        private readonly IClubBusiness _clubBusiness;
        #endregion Fields

        #region Constructors
        public ClubSettingBusiness(IClubBusiness clubBusiness)
        {
            _clubBusiness = clubBusiness;
        }
        #endregion Constructors

        #region Public methods
        public AutoChargePolicySettingViewModel GetAutoChargePolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var settings = JsonHelper.JsonDeserialize<ClubSetting>(club.SettingSerialized);

            var autoChargePolicy = settings?.AutoChargePolicy;

            var model = MapAutoChargePolicySettingViewModel(autoChargePolicy);

            return model;
        }
        public AutoChargePolicy ReadAutoChargePolicy(Club club)
        {
            ClubSetting settings;

            // Club has partner id
            if (club.PartnerId.HasValue)
            {
                var partner = _clubBusiness.Get(club.PartnerId.Value);
                var partnerSettings = JsonHelper.JsonDeserialize<PartnerSetting>(partner.SettingSerialized);

                //In the partner setting sets on read from member
                settings = JsonHelper.JsonDeserialize<ClubSetting>(partnerSettings.PortalParameters.ReadAutoChargePolicyFromMember ? club.SettingSerialized : partner.SettingSerialized);
            }
            else
            {
                settings = JsonHelper.JsonDeserialize<ClubSetting>(club.SettingSerialized);
            }

            var result = settings?.AutoChargePolicy;

            return result;
        }

        public OperationStatus SaveAutoChargePolicy(AutoChargePolicySettingViewModel model, int clubId)
        {
            var autoChargePolicy = MapAutoChargePolicy(model);

            var club = _clubBusiness.Get(clubId);

            var settings = club.Setting ?? new ClubSetting();

            settings.AutoChargePolicy = autoChargePolicy;

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            var result = _clubBusiness.Update(club);

            return result;
        }

        public ProgramsPolicyViewModel GetProgramsPolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var settings = JsonHelper.JsonDeserialize<ClubSetting>(club.SettingSerialized);

            var programsPolicy = settings?.ProgramsPolicy ?? new ProgramsPolicy();

            var model = MapProgramsPolicyToViewModel(programsPolicy);

            return model;
        }

        public OperationStatus SaveSubscriptionProgramPolicy(ProgramsPolicyViewModel model, int clubId)
        {
            var subscriptionPolicy = MapSubcriptionProgramsPolicy(model.SubcriptionPolicy);

            var club = _clubBusiness.Get(clubId);

            var settings = club.Setting ?? new ClubSetting();

            if (settings.ProgramsPolicy == null) settings.ProgramsPolicy = new ProgramsPolicy();

            if (settings.ProgramsPolicy.GeneralPolicy == null) settings.ProgramsPolicy.GeneralPolicy = new ProgramGeneralPolicy();

            settings.ProgramsPolicy.SubscriptionProgramPolicy = subscriptionPolicy;

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            var result = _clubBusiness.Update(club);

            return result;
        }

        public DelinquentPolicySettingViewModel GetDelinquentPolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var settings = JsonHelper.JsonDeserialize<ClubSetting>(club.SettingSerialized);

            var delinquentPolicy = settings?.DelinquentPolicy;

            return MapDelinquentPolicySettingViewModel(delinquentPolicy);
        }

        public OperationStatus SaveDelinquentPolicy(DelinquentPolicySettingViewModel model, int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var settings = club.Setting ?? new ClubSetting();

            settings.DelinquentPolicy = MapDelinquentPolicy(model);

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            return _clubBusiness.Update(club);
        }

        public DelinquentPolicy ReadDelinquentPolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);
            ClubSetting settings;

            if (club.PartnerId.HasValue)
            {
                var partner = _clubBusiness.Get(club.PartnerId.Value);

                if (partner.SettingSerialized == null) return null;

                var partnerSettings = JsonHelper.JsonDeserialize<PartnerSetting>(partner.SettingSerialized);

                settings = JsonHelper.JsonDeserialize<ClubSetting>(partnerSettings.PortalParameters.ReadDelinquentPolicyFromMember ? club.SettingSerialized : partner.SettingSerialized);
            }
            else
            {
                settings = JsonHelper.JsonDeserialize<ClubSetting>(club.SettingSerialized);
            }

            return settings?.DelinquentPolicy;
        }

        public OperationStatus SaveGeneralProgramsPolicy(ProgramsPolicyViewModel model, int clubId)
        {
            var generalPolicy = MapGeneralProgramPolicy(model.GeneralPolicy);

            var club = _clubBusiness.Get(clubId);

            var settings = club.Setting ?? new ClubSetting();

            if (settings.ProgramsPolicy == null) settings.ProgramsPolicy = new ProgramsPolicy();

            if (settings.ProgramsPolicy.SubscriptionProgramPolicy == null) settings.ProgramsPolicy.SubscriptionProgramPolicy = new SubscriptionProgramPolicy();

            settings.ProgramsPolicy.GeneralPolicy = generalPolicy;

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            var result = _clubBusiness.Update(club);

            return result;

        }

        public bool GetCancellationFeePolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);
            var programsPolicy = ReadProgramsPolicy(club);

            if (programsPolicy == null) return false;

            return programsPolicy.GeneralPolicy.EnabledCancellationFee;
        }


        public bool GetTransferFeePolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var programsPolicy = ReadProgramsPolicy(club);

            if (programsPolicy == null) return false;

            return programsPolicy.GeneralPolicy.EnabledTransferFee;
        }

        public bool GetSubscriptionCancellationPolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var programsPolicy = ReadProgramsPolicy(club);

            if (programsPolicy == null || programsPolicy.SubscriptionProgramPolicy == null) return false;

            return programsPolicy.SubscriptionProgramPolicy.EnabledCancellationProrate;
        }

        public bool GetSubscriptionChangeDesiredPolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var programsPolicy = ReadProgramsPolicy(club);

            if (programsPolicy == null || programsPolicy.SubscriptionProgramPolicy == null) return false;

            return programsPolicy.SubscriptionProgramPolicy.EnabledDesiredStartDateProrate;
        }

        public TransferOrderPolicy GetSubscriptionTransferPolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var programsPolicy = ReadProgramsPolicy(club);

            if (programsPolicy == null || programsPolicy.SubscriptionProgramPolicy == null || programsPolicy.SubscriptionProgramPolicy.TransferOrderPolicy == null) return new TransferOrderPolicy();

            return programsPolicy.SubscriptionProgramPolicy.TransferOrderPolicy;
        }
        public ProrationMethod GetProrationMethod(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var programsPolicy = ReadProgramsPolicy(club);

            if (programsPolicy == null || programsPolicy.GeneralPolicy == null) return ProrationMethod.CalculateBySessions;

            return programsPolicy.GeneralPolicy.PerorationMethod;
        }

        public DuplicateEnrollmentPolicySettingViewModel GetDuplicateEnrollmentPolicy(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var settings = club.PartnerId.HasValue ? ((PartnerSetting)club.PartnerClub.Setting)?.PortalParameters != null && ((PartnerSetting)club.PartnerClub.Setting).PortalParameters.ReadDuplicateEnrollmentsFromMember ? JsonHelper.JsonDeserialize<ClubSetting>(club.SettingSerialized) : JsonHelper.JsonDeserialize<PartnerSetting>(club.PartnerClub.SettingSerialized) : JsonHelper.JsonDeserialize<ClubSetting>(club.SettingSerialized);

            var duplicateEnrollmentPolicy = settings?.DuplicateEnrollmentPolicy;

            var model = MapDuplicateEnrollmentPolicySettingViewModel(duplicateEnrollmentPolicy);

            return model;
        }

        public OperationStatus SaveDuplicateEnrollmentPolicy(DuplicateEnrollmentPolicySettingViewModel model, int clubId)
        {
            var duplicateEnrollmentModel = MapDuplicateEnrollmentPolicy(model);

            var club = _clubBusiness.Get(clubId);

            var settings = club.Setting ?? new ClubSetting();

            settings.DuplicateEnrollmentPolicy = duplicateEnrollmentModel;

            club.SettingSerialized = JsonHelper.JsonSerializer(settings);

            var result = _clubBusiness.Update(club);

            return result;
        }

        public bool GetRefundAutomatically(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var programsPolicy = ReadProgramsPolicy(club);

            if (programsPolicy == null || programsPolicy.GeneralPolicy == null) return true;

            return programsPolicy.GeneralPolicy.EnabledRefundAutomaticallyInCancellationOrder;
        }

        public EmailsSettingViewModel GetEmailsContents(int clubId)
        {
            var result = new EmailsSettingViewModel();

            var club = _clubBusiness.Get(clubId);
            result.IsPartner = club.IsPartner;

            if (club.Setting?.Notifications == null) return result;

            result.ContentConfirmationEmail = club.Setting.Notifications.Email.ContentConfirmationEmail;
            result.ContentCancellationEmail = club.Setting.Notifications.Email.ContentCancellationEmail;
            result.ContentAbsentee = club.Setting.Notifications.Email.ContentAbsentee;
            result.DefaultContentConfirmationEmail = Constants.Notification_Confirmation_Content;
            result.DefaultContentCancellationEmail = Constants.Notification_Cancellation_Content;

            return result;
        }

        public OperationStatus SaveEmailsContents(EmailsSettingViewModel model, int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var settings = club.Setting ?? new ClubSetting();

            if (settings.Notifications == null) settings.Notifications = new ClubNotificationSetting();

            club.Setting.Notifications.Email.ContentConfirmationEmail = model.ContentConfirmationEmail;
            club.Setting.Notifications.Email.ContentCancellationEmail = model.ContentCancellationEmail;
            club.Setting.Notifications.Email.ContentAbsentee = model.ContentAbsentee;
            club.IsSettingChanged = true;

            var result = _clubBusiness.Update(club);

            return result;
        }

        public ReportsSettingViewModel GetReportsContents(int clubId)
        {
            var result = new ReportsSettingViewModel();

            var club = _clubBusiness.Get(clubId);
            result.IsPartner = club.IsPartner;

            if (club.Setting?.Notifications == null) return result;

            result.DependentCareDescription = club.Setting.Notifications.Report.DependentCareDescription;
            result.DefaultDependentCareDescription = Constants.Notification_DependentCareDescription;

            return result;
        }

        public OperationStatus SaveReportsContents(ReportsSettingViewModel model, int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var settings = club.Setting ?? new ClubSetting();

            if (settings.Notifications == null) settings.Notifications = new ClubNotificationSetting();

            club.Setting.Notifications.Report.DependentCareDescription = model.DependentCareDescription;
            club.IsSettingChanged = true;

            var result = _clubBusiness.Update(club);

            return result;
        }

        public CatalogsSettingViewModel GetCatalogContents(int clubId)
        {
            var result = new CatalogsSettingViewModel();

            var club = _clubBusiness.Get(clubId);
            result.IsPartner = club.IsPartner;

            if (club.Setting?.Notifications == null) return result;

            result.SendMessageToProviderOnClassSchedule = club.Setting.Notifications != null ? club.Setting.Notifications.Catalog.SendMessageToProviderOnClassSchedule : false;

            return result;
        }

        public OperationStatus SaveCatalogContents(CatalogsSettingViewModel model, int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            var settings = club.Setting ?? new ClubSetting();

            if (settings.Notifications == null) settings.Notifications = new ClubNotificationSetting();

            club.Setting.Notifications.Catalog.SendMessageToProviderOnClassSchedule = model.SendMessageToProviderOnClassSchedule;
            club.IsSettingChanged = true;

            var result = _clubBusiness.Update(club);

            return result;
        }

        #endregion Public methods

        #region Private methods

        private ProgramsPolicy ReadProgramsPolicy(Club club)
        {
            ClubSetting settings;

            // Club has partner id
            if (club.PartnerId.HasValue)
            {
                var partner = _clubBusiness.Get(club.PartnerId.Value);
                var partnerSettings = JsonHelper.JsonDeserialize<PartnerSetting>(partner.SettingSerialized);

                //In the partner setting sets on read from member
                settings = JsonHelper.JsonDeserialize<ClubSetting>(partnerSettings.PortalParameters.ReadProgramsPolicyFromMember ? club.SettingSerialized : partner.SettingSerialized);
            }
            else
            {
                settings = JsonHelper.JsonDeserialize<ClubSetting>(club.SettingSerialized);
            }

            var result = settings?.ProgramsPolicy;

            return result;
        }
        private static AutoChargePolicy MapAutoChargePolicy(AutoChargePolicySettingViewModel autoChargePolicySettingViewModel)
        {
            var result = new AutoChargePolicy
            {
                SuccessEmailToAdmin = autoChargePolicySettingViewModel.SuccessEmailToAdmin,
                SuccessAdminEmailTemplate = autoChargePolicySettingViewModel.SuccessAdminEmailTemplate,
                SuccessParentEmailTemplate = autoChargePolicySettingViewModel.SuccessParentEmailTemplate,
                SendSummeryEmail = autoChargePolicySettingViewModel.SendSummeryEmail,
                IsCombinePayments = autoChargePolicySettingViewModel.CombinePayments,
                HasReplyTo = autoChargePolicySettingViewModel.HasReplyTo,
                ReplyToEmailAddress = autoChargePolicySettingViewModel.ReplyToEmailAddress,

                Attempts = autoChargePolicySettingViewModel.Attempts.Select(viewmodel => new AutoChargeAttempt
                {
                    FailedEmailToAdmin = viewmodel.FailedEmailToAdmin,
                    FailedEmailToParent = viewmodel.FailedEmailToParent,
                    IntervalDays = int.Parse(viewmodel.IntervalDays),
                    AttemptOrder = viewmodel.AttemptOrder,
                    AdminEmailTemplate = viewmodel.FailedAdminEmailTemplate,
                    ParentEmailTemplate = viewmodel.FailedParentEmailTemplate,
                    Enabled = viewmodel.Enabled
                }).ToList()
            };

            return result;
        }

        private static AutoChargePolicySettingViewModel MapAutoChargePolicySettingViewModel(AutoChargePolicy autoChargePolicy)
        {
            var result = new AutoChargePolicySettingViewModel();

            if (autoChargePolicy == null)
            {
                result.Attempts = GetDefaultAutoChargePolicySettingViewModel(result.Attempts).ToList();

                return result;
            }

            result.SuccessEmailToAdmin = autoChargePolicy.SuccessEmailToAdmin;
            result.SuccessAdminEmailTemplate = autoChargePolicy.SuccessAdminEmailTemplate;
            result.SuccessParentEmailTemplate = autoChargePolicy.SuccessParentEmailTemplate;
            result.SendSummeryEmail = autoChargePolicy.SendSummeryEmail;
            result.CombinePayments = autoChargePolicy.IsCombinePayments;
            result.HasReplyTo = autoChargePolicy.HasReplyTo;
            result.ReplyToEmailAddress = autoChargePolicy.ReplyToEmailAddress;

            if (autoChargePolicy.Attempts != null && autoChargePolicy.Attempts.Count == Common.Constants.ClubSetting.CountOfAttempts)
            {
                result.Attempts = autoChargePolicy.Attempts.Select(model => new AutoChargeAttemptViewModel
                {
                    FailedEmailToAdmin = model.FailedEmailToAdmin,
                    FailedEmailToParent = model.FailedEmailToParent,
                    IntervalDays = model.IntervalDays.ToString(),
                    AttemptOrder = model.AttemptOrder,
                    FailedAdminEmailTemplate = model.AdminEmailTemplate,
                    FailedParentEmailTemplate = model.ParentEmailTemplate,
                    Enabled = model.Enabled
                }).ToList();
            }
            else
            {
                result.Attempts = GetDefaultAutoChargePolicySettingViewModel(result.Attempts).ToList();
            }

            result.EnableAttemptsCount = result.Attempts.Count(x => x.Enabled);

            return result;
        }

        private static IEnumerable<AutoChargeAttemptViewModel> GetDefaultAutoChargePolicySettingViewModel(ICollection<AutoChargeAttemptViewModel> autoChargeAttemptViewModel)
        {
            for (var i = 1; i <= Common.Constants.ClubSetting.CountOfAttempts; i++)
            {
                autoChargeAttemptViewModel.Add(new AutoChargeAttemptViewModel { AttemptOrder = i, Enabled = i <= 4 });
            }

            return autoChargeAttemptViewModel;
        }

        private static DelinquentPolicy MapDelinquentPolicy(DelinquentPolicySettingViewModel delinquentPolicySettingViewModel)
        {
            var result = new DelinquentPolicy
            {
                AutoChargeFailAttemptNumber = !delinquentPolicySettingViewModel.AutoChargeFailAttempted ? 0 : int.Parse(delinquentPolicySettingViewModel.AutoChargeFailAttemptNumber),
                ManualInstallmentDelayDays = !delinquentPolicySettingViewModel.ManualInstallmentDelayed ? 0 : int.Parse(delinquentPolicySettingViewModel.ManualInstallmentDelayDays),
                FamilyBalanceNoLimitDate = !delinquentPolicySettingViewModel.CalculateFamilyBalance ? null : delinquentPolicySettingViewModel.FamilyBalanceNoLimitDate,
                FamilyBalanceMinPrice = delinquentPolicySettingViewModel.FamilyBalanceMinPrice,
                AllowEnrollOnDelinquent = delinquentPolicySettingViewModel.AllowEnrollOnDelinquent,
                AllowEnrollTemplate = delinquentPolicySettingViewModel.AllowEnrollTemplate,
                NotAllowEnrollTemplate = delinquentPolicySettingViewModel.NotAllowEnrollTemplate,
                ExpelChildOnDelinquent = delinquentPolicySettingViewModel.ExpelChildOnDelinquent,
                ExpelChildTemplate = delinquentPolicySettingViewModel.ExpelChildTemplate
            };

            return result;
        }

        private static DelinquentPolicySettingViewModel MapDelinquentPolicySettingViewModel(DelinquentPolicy delinquentPolicy)
        {
            var result = new DelinquentPolicySettingViewModel();

            if (delinquentPolicy == null)
            {
                return result;
            }

            result.AutoChargeFailAttempted = delinquentPolicy.AutoChargeFailAttemptNumber != 0;
            result.AutoChargeFailAttemptNumber = delinquentPolicy.AutoChargeFailAttemptNumber != 0 ? delinquentPolicy.AutoChargeFailAttemptNumber.ToString() : 1.ToString();
            result.ManualInstallmentDelayed = delinquentPolicy.ManualInstallmentDelayDays != 0;
            result.ManualInstallmentDelayDays = delinquentPolicy.ManualInstallmentDelayDays != 0 ? delinquentPolicy.ManualInstallmentDelayDays.ToString() : 1.ToString();
            result.CalculateFamilyBalance = delinquentPolicy.FamilyBalanceNoLimitDate != null;
            result.FamilyBalanceNoLimitDate = delinquentPolicy.FamilyBalanceNoLimitDate ?? DateTime.Now.Date;
            result.FamilyBalanceMinPrice = delinquentPolicy.FamilyBalanceMinPrice;
            result.AllowEnrollOnDelinquent = delinquentPolicy.AllowEnrollOnDelinquent;
            result.AllowEnrollTemplate = delinquentPolicy.AllowEnrollTemplate;
            result.NotAllowEnrollTemplate = delinquentPolicy.NotAllowEnrollTemplate;
            result.ExpelChildOnDelinquent = delinquentPolicy.ExpelChildOnDelinquent;
            result.ExpelChildTemplate = delinquentPolicy.ExpelChildTemplate;

            return result;
        }

        private static ProgramsPolicyViewModel MapProgramsPolicyToViewModel(ProgramsPolicy programsPolicy)
        {
            if (programsPolicy.SubscriptionProgramPolicy == null)
            {
                programsPolicy.GeneralPolicy = new ProgramGeneralPolicy();
                programsPolicy.SubscriptionProgramPolicy = new SubscriptionProgramPolicy();
                programsPolicy.SubscriptionProgramPolicy.TransferOrderPolicy = new TransferOrderPolicy();
            }

            if (programsPolicy.SubscriptionProgramPolicy.TransferOrderPolicy == null)
            {
                programsPolicy.SubscriptionProgramPolicy.TransferOrderPolicy = new TransferOrderPolicy();
            }

            var result = new ProgramsPolicyViewModel
            {
                GeneralPolicy = new GeneralPolicyModel
                {
                    EnabledCancellationFee = programsPolicy.GeneralPolicy.EnabledCancellationFee,
                    EnabledTransferFee = programsPolicy.GeneralPolicy.EnabledTransferFee,
                    SelectedProrationMethod = programsPolicy.GeneralPolicy.PerorationMethod,
                    ProrationMethods = GetProrationMethods(),
                    EnabledRefundAutomaticallyInCancellationOrder = programsPolicy.GeneralPolicy.EnabledRefundAutomaticallyInCancellationOrder
                },
                SubcriptionPolicy = new SubcriptionPolicyModel
                {
                    EnabledCancellationProrate = programsPolicy.SubscriptionProgramPolicy.EnabledCancellationProrate,
                    EnabledDesiredStartDateProrate = programsPolicy.SubscriptionProgramPolicy.EnabledDesiredStartDateProrate,
                    EnabledDowngradeProrate = programsPolicy.SubscriptionProgramPolicy.TransferOrderPolicy.EnabledDowngradeProrate,
                    EnabledUpgradeProrate = programsPolicy.SubscriptionProgramPolicy.TransferOrderPolicy.EnabledUpgradeProrate
                }
            };

            return result;
        }

        private static List<SelectListItem> GetProrationMethods()
        {
            var result = DropdownHelper.GetEnumList<ProrationMethod>().OrderBy(o => o.Text).ToList();

            return result;
        }

        private static SubscriptionProgramPolicy MapSubcriptionProgramsPolicy(SubcriptionPolicyModel subcriptionPolicyModel)
        {
            var result = new SubscriptionProgramPolicy
            {
                EnabledCancellationProrate = subcriptionPolicyModel.EnabledCancellationProrate,
                EnabledDesiredStartDateProrate = subcriptionPolicyModel.EnabledDesiredStartDateProrate,
                TransferOrderPolicy = new TransferOrderPolicy
                {
                    EnabledDowngradeProrate = subcriptionPolicyModel.EnabledDowngradeProrate,
                    EnabledDowngradeExpensivePart = !subcriptionPolicyModel.EnabledDowngradeProrate,
                }
            };

            return result;
        }

        private static ProgramGeneralPolicy MapGeneralProgramPolicy(GeneralPolicyModel model)
        {
            var result = new ProgramGeneralPolicy
            {
                EnabledCancellationFee = model.EnabledCancellationFee,
                EnabledTransferFee = model.EnabledTransferFee,
                PerorationMethod = model.SelectedProrationMethod,
                EnabledRefundAutomaticallyInCancellationOrder = model.EnabledRefundAutomaticallyInCancellationOrder
            };

            return result;
        }

        private static DuplicateEnrollmentPolicySettingViewModel MapDuplicateEnrollmentPolicySettingViewModel(DuplicateEnrollmentPolicy duplicateEnrollmentPolicy)
        {
            if (duplicateEnrollmentPolicy == null) return new DuplicateEnrollmentPolicySettingViewModel();

            var result = new DuplicateEnrollmentPolicySettingViewModel
            {
                PreventSameClass = duplicateEnrollmentPolicy.PreventSameClass,
                PreventOtherClasses = duplicateEnrollmentPolicy.PreventOtherClasses,
                PreventSameCampSchedule = duplicateEnrollmentPolicy.PreventSameCampSchedule,
                PreventMultipleCampDropIn = duplicateEnrollmentPolicy.PreventMultipleCampDropIn,
                PreventSameProgramBeforeAfterWeekDay = duplicateEnrollmentPolicy.PreventSameProgramBeforeAfterWeekDay
            };

            return result;
        }

        private static DuplicateEnrollmentPolicy MapDuplicateEnrollmentPolicy(DuplicateEnrollmentPolicySettingViewModel duplicateEnrollmentPolicy)
        {
            var result = new DuplicateEnrollmentPolicy
            {
                PreventSameClass = duplicateEnrollmentPolicy.PreventSameClass,
                PreventOtherClasses = duplicateEnrollmentPolicy.PreventOtherClasses,
                PreventSameCampSchedule = duplicateEnrollmentPolicy.PreventSameCampSchedule,
                PreventMultipleCampDropIn = duplicateEnrollmentPolicy.PreventMultipleCampDropIn,
                PreventSameProgramBeforeAfterWeekDay = duplicateEnrollmentPolicy.PreventSameProgramBeforeAfterWeekDay
            };

            return result;
        }

        #endregion Private methods
    }
}
