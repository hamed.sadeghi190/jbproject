﻿

namespace Jumbula.Common.Enums
{
    public enum RequestType : byte
    {
        NotBranded = 0,
        Branded = 1
    }
}
