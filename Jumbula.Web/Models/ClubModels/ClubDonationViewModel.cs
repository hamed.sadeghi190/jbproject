﻿using Jb.Framework.Common.Forms;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;

namespace SportsClub.Models.ClubModels
{
    public class ClubDonationViewModel : BaseViewModel<Club>
    {
        public ClubDonationViewModel()
        {
            this.JbForm = new JbForm();
        }

        public ClubDonationViewModel(JbForm jbForm)
        {
            this.JbForm = jbForm;
        }

        [Display(Name = "amount")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Amount must be a positive number.")]
        public decimal? Amount { get; set; }

        public string ClubDomain { get; set; }

        public bool isEnable { get; set; }
        public JbForm JbForm { get; set; }

        public bool IsNotCurrentCartDomain
        {
            get
            {
                // Get cart
                //var cartDb = Ioc.ICartDbService;

                //// Get domain of orders in the cart(if there is no order in cart return empty)
                //string currentDomain = cartDb.CurrentDomain(HttpContext.Current);

                //if (!string.IsNullOrEmpty(currentDomain))
                //{
                //    return !(currentDomain == this.ClubDomain);
                //}

                return false;
            }
        }

    }
}