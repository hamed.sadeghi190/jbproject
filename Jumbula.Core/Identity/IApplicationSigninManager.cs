﻿using System.Threading.Tasks;
using Jumbula.Core.Domain;
using Microsoft.AspNet.Identity.Owin;

namespace Jumbula.Core.Identity
{
    public interface IApplicationSignInManager
    {
        Task<SignInStatus> PasswordSignInAsync(string userName, string password, ICurrentOrganization currentOrganization, ICurrentUser currentUser, bool isPersistent, bool shouldLockout);
        Task SignInAsync(JbUser user, ICurrentOrganization currentOrganization, ICurrentUser currentUser, bool isPersistent, bool rememberBrowser);
    }
}
