﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class RecentRegistrationModel
    {

    
        public Int16 ItemStatusReason {get;set;}
        public long OrderItemId { get; set; }

        public string AttendeeName { get; set; }

        public string ProgramName { get; set; }

        public string DateTimeLable { get; set; }

        public string PaidAmount { get; set; }

        public string SeasonDomain { get; set; }

        public string ConfirmationId { get; set; }
        public string TuitionName { get; set; }
        public string Schedule { get; set; }
        public string ClubName { get; set; }
        public int ClubId { get; set; }

    }
}