﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SeasonProcessingController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = {};
        $scope.Model.checkedIds = {};

        $scope.SeasonProcessingReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetSeasonProcessingDates',
                        type: "POST",
                        data: function () {
                            return {
                                seasonTitle: $scope.MC.stateParams.seasonTitle
                            };
                        },
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
        });

        $scope.SeasonProcessingReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "Season processing dates",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.SeasonProcessingReportDataSource,
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5
            },
            sortable: { mode: 'single' },
            columns: [
                {
                    field: "Name",
                    title: "Session Schedule Name",
                    width: "180px",
                },
                {
                    field: "Start",
                    title: "Registration Opens",
                    width: "200px",
                },
                {
                    field: "End",
                    title: "Registration Closes",
                    width: "200px",
                },
                {
                    field: "Roster",
                    title: "Roster Due Date",
                    width: "200px",
                },
                {
                    field: "Payment",
                    title: "Payments Issued",
                    width: "200px",
                }
            ],
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#SeasonProcessingReportGrid").getKendoGrid().saveAsExcel();
        }

    }
    ]);

})();