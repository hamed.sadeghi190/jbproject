﻿using System;

namespace Jumbula.Core.Business
{
    public interface IAutoChargeBusiness
    {
        void Run(DateTime installmentDate);
    }
}
