﻿using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Business
{
    public interface IContactBusiness
    {
        OperationStatus Delete(Contact contact);
    }
}
