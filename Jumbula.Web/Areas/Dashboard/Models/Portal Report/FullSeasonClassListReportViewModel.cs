﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FullSeasonClassListReportViewModel
    {
        public string SchoolName { get; set; }

        public string ClassName { get; set; }

        public string ProviderName { get; set; }

        public string Day { get; set; }

        public string ClassStatus { get; set; }
    }
}