﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Newtonsoft.Json;

namespace Jumbula.Core.Domain
{
    public class OrderItem : BaseEntity<long>, IProtectedResource
    {
        public OrderItem()
        {
            OrderChargeDiscounts = new HashSet<OrderChargeDiscount>();
            OrderItemWaivers = new HashSet<OrderItemWaiver>();
            FollowupForms = new HashSet<OrderItemFollowupForm>();
            OrderSessions = new HashSet<OrderSession>();
            ItemStatus = OrderItemStatusCategories.notInitiated;
            ItemStatusReason = OrderItemStatusReasons.regular;
            ValidationMessages = new List<string>();
            Start = DateTime.UtcNow;
            End = DateTime.UtcNow;
            ISTS = 0;
        }


        [Required]
        public decimal EntryFee { get; set; }

        [Required]
        public string EntryFeeName { get; set; }

        [Required]
        public decimal TotalAmount { get; set; }
        public decimal PaidAmount { get; set; }
        [StringLength(256)]
        public string Name { get; set; }

        public long? SeasonId { get; set; }
        public virtual Season Season { get; set; }
        public PaymentPlanType PaymentPlanType { get; set; }

        public PaymentPlanStatus PaymentPlanStatus { get; set; }

        public DateTime? PaymentPlanStatusUpdateDate { get; set; }

        [Required]
        public long ISTS { get; set; }//First Item Setup TimeStamp

        //[Required]
        [StringLength(64)]
        public string FirstName { get; set; }

        //[Required]
        [StringLength(64)]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);
            }
        }

        [Required]
        public DateTime DateCreated { get; set; }
        public virtual ICollection<OrderChargeDiscount> OrderChargeDiscounts { get; set; }

        public virtual ICollection<OrderItemSchedulePart> OrderItemScheduleParts { get; set; }

        public virtual ICollection<OrderItemWaiver> OrderItemWaivers { get; set; }

        public long Order_Id { get; set; }
        [ForeignKey("Order_Id")]
        public virtual Order Order { get; set; }

        public virtual ICollection<OrderSession> OrderSessions { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public RegistrationType RegType { get; set; }

        public long? ProgramScheduleId { get; set; }
        [ForeignKey("ProgramScheduleId")]
        public virtual ProgramSchedule ProgramSchedule { get; set; }

        #region Tourney
        [StringLength(128)]
        public string Section { get; set; }

        public ProgramTypeCategory ProgramTypeCategory { get; set; }

        [StringLength(128)]
        public string Byes { get; set; }

        [StringLength(128)]
        public string Options { get; set; }
        #endregion

        public int? PlayerId { get; set; }

        public long? PaymentPlanId { get; set; }
        //public virtual ICollection<JbForm> PlayerForms { get; set; }
        public virtual PlayerProfile Player { get; set; }

        public long? OrderItemChessId { get; set; }
        [ForeignKey("OrderItemChessId")]
        public virtual OrderItemChess OrderItemChess { get; set; }
        public int? CreditCardId { get; set; }
        [ForeignKey("CreditCardId")]
        public virtual UserCreditCard CreditCard { get; set; }
        public virtual PaymentPlan PaymentPlan { get; set; }
        public int? JbFormId { get; set; }
        [ForeignKey("JbFormId")]
        public virtual JbForm JbForm { get; set; }

        public DateTime? DesiredStartDate { get; set; }

        [MaxLength(64)]
        public string PassedPin { get; set; }

        //public DateTime? TransferEffectiveDate { get; set; }

        public virtual List<OrderInstallment> Installments { get; set; }
        public virtual ICollection<OrderItemFollowupForm> FollowupForms { get; set; }
        public decimal CalculateTotalAmount(OrderItemStatusReasons orderStatus = OrderItemStatusReasons.regular)
        {
            decimal chargeAmount = OrderChargeDiscounts.Where(cd => !cd.IsDeleted && (int)cd.Category > 5).Sum(cd => cd.Amount);
            decimal discountAmount = 0;

            var installmentCount = Installments != null ? Installments.Where(i => i.Type != InstallmentType.AddOn).Count() : 0;

            if (ProgramSchedule != null && (ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Subscription || ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.BeforeAfterCare) && installmentCount > 0)
            {
                if (orderStatus == OrderItemStatusReasons.canceled || orderStatus == OrderItemStatusReasons.transferIn || orderStatus == OrderItemStatusReasons.transferOut)
                {
                    discountAmount = CalculateDiscounts();
                }
                else
                {
                    discountAmount = CalculateDiscounts(installmentCount);
                }

            }
            else
            {
                discountAmount = CalculateDiscounts();
            }

            decimal couponTotalDiscount = 0;
            if (OrderChargeDiscounts.Any(r => !r.IsDeleted && r.Category == ChargeDiscountCategory.Coupon))
            {
                foreach (var coupon in OrderChargeDiscounts.Where(cd => !cd.IsDeleted && cd.Category == ChargeDiscountCategory.Coupon))
                {
                    if (coupon.Coupon.CouponCalculateType == CalculationType.OnlyTuition)
                    {
                        discountAmount += Math.Abs(coupon.Amount);
                    }
                    else
                    {
                        couponTotalDiscount += Math.Abs(coupon.Amount);
                    }
                }
            }

            if (ProgramSchedule != null && ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Calendar)
                TotalAmount = GetOrderChargeDiscounts().Where(c => (short)c.Category > 0).Sum(c => c.Amount) - Math.Abs(GetOrderChargeDiscounts().Where(c => (short)c.Category < 0).Sum(c => c.Amount));
            else
                TotalAmount = (((EntryFee - discountAmount) > 0 ? (EntryFee - discountAmount) : 0) + chargeAmount) - couponTotalDiscount;

            if (TotalAmount < 0)
                TotalAmount = 0;

            return TotalAmount;
        }
        public decimal CalculateDiscounts()
        {
            return OrderChargeDiscounts.
                       Where(orcd => !orcd.IsDeleted && orcd.Subcategory == ChargeDiscountSubcategory.Discount && orcd.Category != ChargeDiscountCategory.Coupon).
                       Sum(r => Math.Abs(r.Amount));
        }

        public decimal CalculateDiscounts(int installmentCount)
        {
            decimal result = 0;

            foreach (var item in OrderChargeDiscounts.Where(orcd => !orcd.IsDeleted && orcd.Subcategory == ChargeDiscountSubcategory.Discount && orcd.Category != ChargeDiscountCategory.Coupon))
            {
                if (item.Discount != null && item.Discount.AmountType == ChargeDiscountType.Fixed)
                {
                    result += Math.Abs(item.Amount) * installmentCount;
                }
                else
                {
                    result += Math.Abs(item.Amount);
                }
            }

            return result;
        }

        public bool DoesTheClubOwnIt(int clubId)
        {
            if (Order.ClubId == clubId || (Order.Club.PartnerId.HasValue && Order.Club.PartnerId == clubId))
            {
                return true;
            }

            return false;
        }

        public bool DoesTheParentOwnIt(int userId)
        {
            if (Order.UserId == userId)
                return true;

            return false;
        }

        public IEnumerable<OrderChargeDiscount> GetOrderChargeDiscounts()
        {
            return OrderChargeDiscounts.Where(c => !c.IsDeleted);
        }
        public virtual ICollection<TransactionActivity> TransactionActivities { get; set; }

        public OrderItemStatusCategories ItemStatus { get; set; }
        public OrderItemStatusReasons ItemStatusReason { get; set; }
        [NotMapped]
        public bool IsProgramFull { get; set; }

        [NotMapped]
        public bool IsDuplicateEnrollment { get; set; }

        [NotMapped]
        public List<string> ValidationMessages { get; set; }

        public bool IsMultiRegister { get; set; }

        public RegistrationStep LastStep { get; set; }

        string _attributesSerialized;
        [StringLength(4000)]
        [Column("Attributes")]
        public string AttributesSerialized
        {
            get
            {
                return _attributesSerialized;
            }
            set
            {
                _attributesSerialized = value;
            }
        }


        [NotMapped]
        public OrderItemAttributes Attributes
        {
            get
            {
                if (!string.IsNullOrEmpty(AttributesSerialized))
                {
                    return JsonConvert.DeserializeObject<OrderItemAttributes>(_attributesSerialized);
                }

                return new OrderItemAttributes();
            }
            set
            {
                _attributesSerialized = JsonConvert.SerializeObject(value);
            }
        }

        public PreRegistrationStatus PreRegistrationStatus { get; set; }

        public LotteryStatus LotteryStatus { get; set; }

        public OrderItemMode Mode { get; set; }

        public bool IsCompleted => ItemStatus == OrderItemStatusCategories.completed || (ProgramTypeCategory == ProgramTypeCategory.Subscription && (ItemStatusReason == OrderItemStatusReasons.transferOut || ItemStatusReason == OrderItemStatusReasons.canceled));

        [NotMapped]
        public bool IsExpelled => Order.Club.Setting.DelinquentPolicy.ExpelChildOnDelinquent &&
                       ItemStatus != OrderItemStatusCategories.initiated &&
                       ItemStatus != OrderItemStatusCategories.notInitiated &&
                       ItemStatus != OrderItemStatusCategories.deleted &&
                       PaymentPlanType == PaymentPlanType.Installment &&
                       Installments.Any(i => i.Status == OrderStatusCategories.error);
    }

    public enum OrderItemMode : byte
    {
        [Description("Normal")]
        Noraml,
        [Description("DropIn")]
        DropIn,
        [Description("Punchcard")]
        PunchCard
    }

    public enum PreRegistrationStatus : byte
    {
        NotInitiated,
        Initiated,
        Added
    }

    public enum LotteryStatus : byte
    {
        NotInitiated,
        Initiated,
        Condidate,
        Won,
        Lose
    }

    public class OrderItemAttributes
    {
        public OrderItemAttributes()
        {
            WeekDays = new List<DayOfWeek>();
        }

        public List<DayOfWeek> WeekDays { get; set; }
        public DateTime? TransferEffectiveDate { get; set; }
        public DateTime? CancelEffectiveDate { get; set; }
        public DateTime? ChangeDaysEffectiveDate { get; set; }
        public int DayNumbers { get; set; }

    }
}
