﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Areas.Dashboard.Services;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Models;
using SportsClub.Models.Payment;
using Constants = Jumbula.Common.Constants.Constants;


namespace Jumbula.Web.Controllers
{
    [AllowAnonymous]
    public class PaypalController : JbBaseController
    {

        #region Make a payment methods
        public virtual ActionResult MakePayment(string token, string parentReturnUrl, bool isFamilyTakePayment = false)
        {
            string errorMsg = string.Empty;
            try
            {
                var transactionactivities = Ioc.TransactionActivityBusiness.GetDraftByToken(token).ToList();
                if (transactionactivities != null && transactionactivities.Count > 0)
                {
                    var orderItem = transactionactivities.First().OrderItem;
                    var amount = transactionactivities.Sum(c => c.Amount);
                    PaypalService paypal = new PaypalService(orderItem.Order.Club.Domain, orderItem.Order.IsLive);

                    string returnUrl = Url.Action("MakeaPaymentReturnUrl", "Paypal", new { token = token, returnUrl = parentReturnUrl, isFamilyTakePayment = isFamilyTakePayment }, "https") + "&payKey=${payKey}";
                    string cancelUrl = Url.Action("MakeaPaymentCancelUrl", "Order", new { token = token }, "https");
                    string ipnNotificationUrl = Url.Action("PayPalNotificationUrl", "Paypal", new { token = token, actionCaller = (int)JumbulaSubSystem.MakeaPayment }, WebConfigHelper.SecureHttpProtocol);


                    PayResponse response = new PayResponse();
                    string memo = string.Format(Constants.F_Paypal_Pay_Memo_TakePayment, orderItem.Order.ConfirmationId);
                    memo += string.Format(Constants.F_Paypal_Pay_Memo_Detail, orderItem.FullName, orderItem.Name, amount);

                    var res = paypal.Pay(orderItem.Order.ClubId, amount, orderItem.Order.ConfirmationId, returnUrl, cancelUrl, ipnNotificationUrl, JumbulaSubSystem.MakeaPayment, orderItem.Id, memo);
                    response = JsonConvert.DeserializeObject<PayResponse>(res);
                    if (response.responseEnvelope.ack.HasValue && (response.responseEnvelope.ack.Value == AckCode.SUCCESS || response.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING))
                    {
                        var items = new List<OrderItem>();
                        items.Add(orderItem);
                        paypal.SetPaymentOptions(items, response.payKey, orderItem.Order.ClubId);
                        string url = paypal.AuthorizePayment(response.payKey);
                        return Redirect(url);
                    }
                    else
                    {
                        var errormessage = response.error[0] != null ? response.error[0].message : string.Empty;
                        var paymentDetail = new PaymentDetail(orderItem.Order.User.UserName, PaymentDetailStatus.ERROR, response.payKey, PaymentMethod.Paypal, orderItem.Order.Club.Currency, res, errormessage, string.Empty, JbUserService.GetCurrentUserRoleType());

                        if (response.error != null)
                        {
                            foreach (var error in response.error)
                            {
                                errorMsg += error.message;
                            }
                        }

                        foreach (var item in transactionactivities)
                        {
                            if (item.Installment == null)
                            {
                                Ioc.OrderBusiness.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, null, DateTime.UtcNow, item.Amount, item.OrderItem, null, "", token, "", TransactionCategory.TakePayment);
                            }
                            else
                            {
                                Ioc.OrderBusiness.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, item.Installment, DateTime.UtcNow, item.Amount, null, null, "", token, "", TransactionCategory.TakePayment);
                            }
                        }

                        Ioc.OrderBusiness.Update(orderItem.Order);

                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                errorMsg = ex.Message;
            }
            if (isFamilyTakePayment)
            {
                return RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = errorMsg, status = false });
            }
            else
            {
                return RedirectToAction("MakePaymentReturnStatus", "Order", new { token = token, message = errorMsg, status = false });
            }
        }

        public virtual ActionResult MakeaPaymentReturnUrl(string token, string payKey, string returnUrl, bool isFamilyTakePayment = false)
        {
            var errorMsg = string.Empty;
            try
            {
                errorMsg = MakePaymentSucceed(token, payKey, this.GetCurrentUserRoleType(), "", isFamilyTakePayment);
                if (errorMsg == UpdateOrderBasedOnPaymentStatus.Duplicate.ToString())
                {
                    errorMsg = "";
                }
                if (errorMsg == UpdateOrderBasedOnPaymentStatus.NoElementFound.ToString())
                {
                    errorMsg = "Token is invalid";
                }
                if (isFamilyTakePayment)
                {
                    if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = errorMsg, status = true });
                    }
                }
                else
                {
                    if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("MakePaymentReturnStatus", "Order", new { token = token, message = errorMsg, status = string.IsNullOrEmpty(errorMsg) });
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                errorMsg = ex.Message;
            }

            if (isFamilyTakePayment)
            {
                return RedirectToAction("FamilyMakePaymentReturnStatus", "Order", new { message = errorMsg, status = false });
            }
            else
            {
                if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return RedirectToAction("MakePaymentReturnStatus", "Order", new { token = token, message = errorMsg, status = false });
                }

            }
        }

        #endregion

        #region Paypal Payment Cart Checkout Methods
        public virtual ActionResult PaypalCheckOut(string confirmationID, decimal payableAmount = 0)
        {
            try
            {
                var orderDb = Ioc.OrderBusiness;
                var order = orderDb.GetOrdersByConfirmationID(confirmationID);

                PaypalService paypal = new PaypalService(order.Club.Domain, order.IsLive);
                payableAmount = Ioc.OrderBusiness.CalculateOrderPayableAmount(order);

                string returnUrl = Url.Action("PaymentReturnUrl", "Paypal", new { cid = confirmationID }, "https") + "&payKey=${payKey}";
                string cancelUrl = Url.Action("CancelReturnUrl", "Paypal", new { cid = confirmationID }, "https");
                string ipnNotificationUrl = Url.Action("PayPalNotificationUrl", "Paypal", new { token = confirmationID, actionCaller = (int)JumbulaSubSystem.Order }, WebConfigHelper.SecureHttpProtocol);

                PayResponse response = new PayResponse();

                string memo = string.Format(Constants.F_Paypal_Pay_Memo_Checkout, confirmationID);
                foreach (var item in order.RegularOrderItems)
                {
                    memo += string.Format(Constants.F_Paypal_Pay_Memo_Detail, item.FullName, item.Name, item.TotalAmount);
                }

                if (memo.Length > 900)
                {
                    memo = memo.Substring(0, 900);
                }

                var res = paypal.Pay(order.ClubId, payableAmount, confirmationID, returnUrl, cancelUrl, ipnNotificationUrl, JumbulaSubSystem.Order, order.Id, memo);

                response = JsonConvert.DeserializeObject<PayResponse>(res);
                if (response.responseEnvelope.ack.HasValue && (response.responseEnvelope.ack.Value == AckCode.SUCCESS || response.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING))
                {
                    paypal.SetPaymentOptions(order.RegularOrderItems.ToList(), response.payKey, order.ClubId);

                    string url = paypal.AuthorizePayment(response.payKey);
                    return Redirect(url);
                }
                else
                {
                    var errormessage = response.error[0] != null ? response.error[0].message : string.Empty;
                    var paymentDetail = new PaymentDetail(order.User.UserName, PaymentDetailStatus.ERROR, response.payKey, PaymentMethod.Paypal, order.Club.Currency, res, errormessage, string.Empty, JbUserService.GetCurrentUserRoleType());

                    orderDb.SetPaymentTransaction(order, paymentDetail, TransactionStatus.Failure);
                    Ioc.OrderBusiness.Update(order);

                    string errorMsg = string.Empty;
                    if (response.error != null)
                    {
                        foreach (var error in response.error)
                        {
                            errorMsg += error.message + "\n";
                        }
                    }

                    var errorsModel = new PaymentErrorViewModel(Constants.M_Paypal_Payment_Failed, errorMsg, confirmationID, false);

                    return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Paypal_Payment_Failed, msg = errorMsg, status = false, cid = confirmationID });
                }

            }
            catch
            {
                return RedirectToAction("Index", "Cart");
            }
        }
        public virtual ActionResult PaymentReturnUrl(string cid, string payKey)
        {

            var result = UpdateOrderBasedOnPayment(cid, payKey, this.GetCurrentUserRoleType());

            if (result.Equals(UpdateOrderBasedOnPaymentStatus.Duplicate.ToString()))
            {
                return RedirectToAction("FinalizeConfirmation", "Cart", new { cid = cid, paymentMethod = PaymentMethod.Paypal });
            }

            if (result.StartsWith(UpdateOrderBasedOnPaymentStatus.MisMatched.ToString()))
            {
                string errorDesc = result.Remove(0, UpdateOrderBasedOnPaymentStatus.MisMatched.ToString().Length);
                return RedirectToAction("PaymentGeneralError", "Cart", new { subject = "", msg = errorDesc, status = true, cid = cid });
            }
            if (!string.IsNullOrEmpty(result))
            {
                return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Paypal_Payment_Failed, msg = result, status = false, cid = cid });
            }
            return RedirectToAction("FinalizeConfirmation", "Cart", new { cid = cid, paymentMethod = PaymentMethod.Paypal });

        }
        public virtual ActionResult CancelReturnUrl(string cid)
        {
            return RedirectToAction("Index", "Cart");
        }

        #endregion

        #region Paypal Preapproval
        public virtual ActionResult SetPreapproval(string token, JumbulaSubSystem actionCaller)
        {
            try
            {

                if (!string.IsNullOrEmpty(token))
                {
                    Order order = Ioc.OrderBusiness.GetOrdersByConfirmationID(token);


                    if (order != null)
                    {
                        var club = Ioc.ClubBusiness.Get(order.ClubId);

                        PaypalService paypal = new PaypalService(club.Domain, order.IsLive);
                        string returnUrl = Url.Action("PreapprovalReturnUrl", "Paypal", new { token = token, actionCaller = (int)actionCaller }, WebConfigHelper.SecureHttpProtocol) + "&preapprovalKey=${preapprovalKey}";
                        string cancelUrl = Url.Action("PreapprovalCancelUrl", "Paypal", new { token = token, actionCaller = (int)actionCaller }, WebConfigHelper.SecureHttpProtocol);
                        string ipnNotificationUrl = Url.Action("PayPalPreapprovalNotification", "Paypal", new { token = token, actionCaller = (int)actionCaller }, WebConfigHelper.SecureHttpProtocol);
                        PreapprovalResponse preResponse = new PreapprovalResponse();
                        string res = paypal.Preapproval(returnUrl, cancelUrl, ipnNotificationUrl, club.Currency.ToString());

                        preResponse = JsonConvert.DeserializeObject<PreapprovalResponse>(res);

                        if (preResponse.responseEnvelope.ack.HasValue && (preResponse.responseEnvelope.ack.Value == AckCode.SUCCESS || preResponse.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING))
                        {

                            string url = paypal.AuthorizePreapproval(preResponse.preapprovalKey);
                            return Redirect(url);
                        }
                        else
                        {
                            PaypalIPN paypalIPN = new PaypalIPN()
                            {
                                CreateDate = DateTime.UtcNow,
                                IPN = res,
                                IsValid = true,
                                TransactionId = preResponse.preapprovalKey,
                                PaypalIPNCategory = PaypalIPNCategories.customIPN,
                                Token = token,
                                PaypalCaller = JumbulaSubSystem.RequestNewPreapproval,
                                IsProceed = false
                            };

                            Ioc.PaypalIPNBusiness.Create(paypalIPN);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.LogException(e);
            }
            return RedirectToAction("PreapprovalError", "Paypal");
        }
        public virtual ActionResult PreapprovalReturnUrl(string token, int actionCaller, string preapprovalKey)
        {
            try
            {
                if (!string.IsNullOrEmpty(preapprovalKey))
                {

                    var result = SavePreapprovalKey(preapprovalKey, token, (JumbulaSubSystem)actionCaller);


                    if (result.Equals(UpdateOrderBasedOnPaymentStatus.Duplicate.ToString()))
                    {
                        return RedirectToAction("DisplaySucceedOperation", "Cart");
                    }
                    if (string.IsNullOrEmpty(result))
                    {
                        return RedirectToAction("DisplaySucceedOperation", "Cart");
                    }
                }
            }
            catch (Exception e)
            {
                LogHelper.LogException(e);
            }
            return RedirectToAction("PreapprovalError", "Paypal");
        }
        public virtual ActionResult PreapprovalError()
        {
            var errorModel = new PaymentErrorViewModel()
            {
                Subject = "Paypal has encountered an error when processing your preapproval",
                IsMoneyTaken = false,
                ErrorMessage = "Please try again using a different card or Paypal account, you also can contact Paypal to report the issue.",
                OrderConfirmationID = string.Empty
            };

            return View("PaymentGeneralError", errorModel);
        }
        public virtual ActionResult PreapprovalCancelUrl(string token, int actionCaller)
        {
            if (actionCaller == (int)JumbulaSubSystem.Order)
            {
                return RedirectToAction("Index", "Cart");
            }
            else
            {
                return RedirectToAction("PreapprovalRequest", "Cart", new { Token = token });
            }
        }

        #endregion

        #region Installment Payment
        public virtual ActionResult InstallmentPayment(string token)
        {
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    var orderDb = Ioc.OrderBusiness;
                    var installment = Ioc.InstallmentBusiness.GetListByToken(token).Where(i => !i.IsDeleted).ToList();
                    if (installment != null && installment.Count > 0)
                    {
                        var orderItem = Ioc.OrderItemBusiness.GetItem(installment.FirstOrDefault().OrderItemId);
                        var club = Ioc.ClubBusiness.Get(orderItem.Order.ClubId);
                        PaypalService paypal = new PaypalService(club.Domain, orderItem.Order.IsLive);
                        var returnUrl = Url.Action("InstallmentPaymentReturnUrl", "Paypal", new { token = token }, WebConfigHelper.SecureHttpProtocol) + "&payKey=${payKey}";
                        var cancelUrl = Url.Action("InstallmentCancelReturnUrl", "Paypal", new { token = token }, WebConfigHelper.SecureHttpProtocol);
                        string ipnNotificationUrl = Url.Action("PayPalNotificationUrl", "Paypal", new { token = token, actionCaller = (int)JumbulaSubSystem.Installment }, WebConfigHelper.SecureHttpProtocol);

                        PayResponse payResponse = new PayResponse();


                        string memo = string.Format(Constants.F_Paypal_Pay_Memo_Installment, string.Join(",", installment.Select(c => c.OrderItem.Order.ConfirmationId)));
                        foreach (var inst in installment)
                        {
                            memo += string.Format(Constants.F_Paypal_Pay_Memo_Detail, inst.OrderItem.FullName, inst.OrderItem.Name, inst.Balance);
                        }

                        var res = paypal.Pay(club.Id, installment.Sum(c => c.Balance), token, returnUrl, cancelUrl, ipnNotificationUrl, JumbulaSubSystem.Installment, installment.FirstOrDefault().Id, memo);
                        payResponse = JsonConvert.DeserializeObject<PayResponse>(res);
                        if (payResponse.responseEnvelope.ack.HasValue && (payResponse.responseEnvelope.ack.Value == AckCode.SUCCESS || payResponse.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING))
                        {
                            var items = new List<OrderItem>();
                            items.Add(orderItem);
                            paypal.SetPaymentOptions(items, payResponse.payKey, club.Id);
                            string url = paypal.AuthorizePayment(payResponse.payKey);
                            return Redirect(url);

                        }

                        else
                        {


                            var date = DateTime.UtcNow;
                            var errormessage = payResponse.error[0] != null ? payResponse.error[0].message : string.Empty;
                            PaymentDetail paymentdetail = new PaymentDetail(orderItem.Order.User.UserName, PaymentDetailStatus.ERROR, payResponse.payKey, PaymentMethod.Paypal, orderItem.Order.Club.Currency, res, errormessage, string.Empty, JbUserService.GetCurrentUserRoleType());

                            foreach (var item in installment)
                            {

                                orderDb.SetPaymentTransaction(null, paymentdetail, TransactionStatus.Failure, item, date);
                                item.Status = OrderStatusCategories.error;
                                Ioc.InstallmentBusiness.Update(item);
                            }


                            string errorMsg = string.Empty;
                            if (payResponse.error != null)
                            {
                                foreach (var error in payResponse.error)
                                {
                                    errorMsg += error.message + "\n";
                                }
                            }

                            return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Paypal_Payment_Failed, msg = errorMsg, status = false, cid = token });
                        }
                    }
                }
                return HttpNotFound();
            }
            catch (Exception e)
            {
                LogHelper.LogException(e);
                return HttpNotFound();
            }
        }
        public virtual ActionResult InstallmentPaymentReturnUrl(string token, string payKey)
        {

            var result = UpdateInstallmentBasedOnPayment(token, payKey, this.GetCurrentUserRoleType());
            if (result == UpdateOrderBasedOnPaymentStatus.Duplicate.ToString())
            {
                return RedirectToAction("DisplaySucceedOperation", "Cart");
            }

            if (result.StartsWith(UpdateOrderBasedOnPaymentStatus.MisMatched.ToString()))
            {
                string errorDesc = result.Remove(0, UpdateOrderBasedOnPaymentStatus.MisMatched.ToString().Length);
                return RedirectToAction("PaymentGeneralError", "Cart", new { subject = "", msg = errorDesc, status = true, cid = token });
            }
            if (!string.IsNullOrEmpty(result))
            {
                return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Paypal_Payment_Failed, msg = result, status = false, cid = token });
            }

            return RedirectToAction("DisplaySucceedOperation", "Cart");
        }
        public virtual ActionResult InstallmentCancelReturnUrl(string token)
        {

            return RedirectToAction("InstallmentPayment", "Cart", new { token = token });
        }
        #endregion

        #region Invoice
        public virtual ActionResult InvoicePayment(string token, bool IsTakePaymentInvoice)
        {
            try
            {
                if (!string.IsNullOrEmpty(token))
                {
                    var transactions = Ioc.TransactionActivityBusiness.GetInvoicedByToken(token).ToList();
                    var invoiceId = transactions.First().PaymentDetail.InvoiceId;
                    if (transactions != null && transactions.Count > 0)
                    {
                        var orderItem = Ioc.OrderItemBusiness.GetItem(transactions.FirstOrDefault().OrderItemId.Value);
                        var club = Ioc.ClubBusiness.Get(transactions.FirstOrDefault().ClubId);
                        PaypalService paypal = new PaypalService(club.Domain, orderItem.Order.IsLive);
                        var returnUrl = Url.Action("InvoicePaymentReturnUrl", "Paypal", new { token = token, invoiceId = invoiceId, IsTakePaymentInvoice = IsTakePaymentInvoice }, WebConfigHelper.SecureHttpProtocol) + "&payKey=${payKey}";
                        var cancelUrl = Url.Action("InvoiceCancelReturnUrl", "Paypal", new { token = token }, WebConfigHelper.SecureHttpProtocol);
                        string ipnNotificationUrl = Url.Action("PayPalNotificationUrl", "Paypal", new { token = token, actionCaller = (int)JumbulaSubSystem.Invoice }, WebConfigHelper.SecureHttpProtocol);

                        PayResponse payResponse = new PayResponse();
                        var firstTransactioncategoury = transactions.First().TransactionCategory;
                        var userName = transactions.First().Order.User.UserName;
                        string memo = "";
                        if (firstTransactioncategoury == TransactionCategory.Invoice)
                        {
                            memo = string.Format(Constants.F_Paypal_Pay_Memo_FamilyInvoice, userName);

                        }
                        else
                        {
                            memo = string.Format(Constants.F_Paypal_Pay_Memo_Invoice, transactions.First().Order.ConfirmationId);

                            memo += string.Format(Constants.F_Paypal_Pay_Memo_Detail, transactions.First().OrderItem.FullName, transactions.First().OrderItem.Name, transactions.Sum(c => c.Amount));
                        }
                        var res = paypal.Pay(club.Id, transactions.Sum(c => c.Amount), token, returnUrl, cancelUrl, ipnNotificationUrl, JumbulaSubSystem.Invoice, orderItem.Id, memo);
                        payResponse = JsonConvert.DeserializeObject<PayResponse>(res);
                        if (payResponse.responseEnvelope.ack.HasValue && (payResponse.responseEnvelope.ack.Value == AckCode.SUCCESS || payResponse.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING))
                        {
                            var items = new List<OrderItem>();
                            items.Add(orderItem);
                            paypal.SetPaymentOptions(items, payResponse.payKey, club.Id);
                            string url = paypal.AuthorizePayment(payResponse.payKey);
                            return Redirect(url);

                        }

                        else
                        {


                            var date = DateTime.UtcNow;
                            var errormessage = payResponse.error[0] != null ? payResponse.error[0].message : string.Empty;
                            PaymentDetail paymentdetail = new PaymentDetail(orderItem.Order.User.UserName, PaymentDetailStatus.ERROR, payResponse.payKey, PaymentMethod.Paypal, orderItem.Order.Club.Currency, res, errormessage, string.Empty, JbUserService.GetCurrentUserRoleType(), null, invoiceId);

                            foreach (var item in transactions)
                            {
                                if (item.Installment == null)
                                {
                                    Ioc.OrderBusiness.SetPaymentTransaction(null, paymentdetail, TransactionStatus.Failure, null, date, item.Amount, item.OrderItem, null, "", token, "", TransactionCategory.Invoice);
                                }
                                else
                                {
                                    Ioc.OrderBusiness.SetPaymentTransaction(null, paymentdetail, TransactionStatus.Failure, item.Installment, date, item.Amount, null, null, "", token, "", TransactionCategory.Invoice);
                                }
                            }

                            Ioc.OrderItemBusiness.Update(orderItem);


                            string errorMsg = string.Empty;
                            if (payResponse.error != null)
                            {
                                foreach (var error in payResponse.error)
                                {
                                    errorMsg += error.message + "\n";
                                }
                            }

                            return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Paypal_Payment_Failed, msg = errorMsg, status = false, cid = token });
                        }
                    }
                }
                return RedirectToAction("Index", "Cart");
            }
            catch (Exception e)
            {
                LogHelper.LogException(e);
                return RedirectToAction("Index", "Cart");
            }
        }

        public virtual ActionResult InvoicePaymentReturnUrl(string token, string payKey, int? invoiceId, bool IsTakePaymentInvoice)
        {

            var result = SucceedInvoicePayment(token, payKey, this.GetCurrentUserRoleType(), IsTakePaymentInvoice);
            if (IsTakePaymentInvoice)
            {
                var urlToRedirect = string.Format("{0}/Dashboard#/Invoicing/Invoice/Details/{1}", Request.Url.GetLeftPart(UriPartial.Authority), invoiceId);
                return Redirect(urlToRedirect);
            }
            if (!invoiceId.HasValue)
            {
                return RedirectToAction("DisplaySucceedOperation", "Cart");
            }
            else
            if (result == UpdateOrderBasedOnPaymentStatus.Duplicate.ToString())
            {

                return RedirectToAction("InvoiceDetail", "Registrant", new { invoiceId = invoiceId, status = true });
            }
            if (!string.IsNullOrEmpty(result))
            {
                return RedirectToAction("PaymentGeneralError", "Cart", new { subject = Constants.M_Paypal_Payment_Failed, msg = result, status = false, cid = token });
            }


            return RedirectToAction("InvoiceDetail", "Registrant", new { invoiceId = invoiceId, status = true });

        }
        public virtual ActionResult InvoiceCancelReturnUrl(string token)
        {

            return RedirectToAction("MakePayment", "Cart", new { token = token });
        }
        #endregion

        #region Charge Pending Transaction
        public virtual ActionResult ChargePreapprovedOrder(DateTime? date)
        {
            if (!date.HasValue)
            {
                date = DateTime.UtcNow;
            }
            var numberOfCharge = 0;
            decimal chargedAmount = 0;
            var preapprovedList = Ioc.PendingPreapprovalTransactionBusiness.GetListByDueDate(date.Value).ToList();
            var paypal = new PaypalService(ClubSubDomain, true);
            var isNeedForAnotherApprove = false;
            foreach (var preapprovadItem in preapprovedList)
            {
                var preapproveId = Ioc.OrderPreapprovalBusiness.GetPreapprovalKeyByOrderId(preapprovadItem.OrderId);
                var order = Ioc.OrderBusiness.Get(preapprovadItem.OrderId);

                var payableAmount = preapprovadItem.DueAmount - preapprovadItem.PaidAmount;
                if (payableAmount == 0)
                {
                    continue;
                }
                var orderPayableAmount = Ioc.AccountingBusiness.OrderDueAmount(order);
                if (orderPayableAmount == 0)
                {
                    continue;
                }
                if (orderPayableAmount < payableAmount)
                {
                    payableAmount = orderPayableAmount;
                }
                if (!string.IsNullOrEmpty(preapproveId))
                {
                    var isSucced = false;
                    var res = paypal.PreapprovalDetails(preapproveId);
                    PreapprovalDetailsResponse pdResponse = new PreapprovalDetailsResponse();
                    pdResponse = JsonConvert.DeserializeObject<PreapprovalDetailsResponse>(res);
                    if (pdResponse.responseEnvelope.ack.HasValue && (pdResponse.responseEnvelope.ack.Value == AckCode.SUCCESS || pdResponse.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING) && pdResponse.approved.HasValue && pdResponse.approved.Value)
                    {
                        var remindAmount = (pdResponse.maxTotalAmountOfAllPayments.HasValue && pdResponse.curPaymentsAmount.HasValue) ? pdResponse.maxTotalAmountOfAllPayments.Value - pdResponse.curPaymentsAmount.Value : Constants.Default_Paypal_MaxPreapprovalAmount;

                        if (remindAmount < preapprovadItem.DueAmount)
                        {
                            payableAmount = remindAmount;
                            isNeedForAnotherApprove = true;
                        }
                        var returnUrl = Url.Action("PreapprovalPaymentReturnUrl", "Paypal", new { token = preapprovadItem.Id }, WebConfigHelper.SecureHttpProtocol) + "&payKey=${payKey}";
                        var cancelUrl = Url.Action("PreapprovalCancelReturnUrl", "Paypal", new { token = preapprovadItem.Id }, WebConfigHelper.SecureHttpProtocol);
                        string ipnNotificationUrl = Url.Action("PayPalNotificationUrl", "Paypal", new { token = preapprovadItem.Id, actionCaller = (int)JumbulaSubSystem.PreapprovalPayment }, WebConfigHelper.SecureHttpProtocol);

                        PayResponse payResponse = new PayResponse();
                        string memo = string.Format(Constants.F_Paypal_Pay_Memo_Preapproval, order.ConfirmationId, payableAmount);
                        if (preapprovadItem.InstallmentId.HasValue && preapprovadItem.Installment != null)
                        {
                            memo += string.Format(Constants.F_Paypal_Pay_Memo_Preapproval_Detail, preapprovadItem.Installment.OrderItem.FullName, preapprovadItem.Installment.OrderItem.Name);
                        }
                        else
                        {
                            foreach (var item in order.RegularOrderItems)
                            {
                                memo += string.Format(Constants.F_Paypal_Pay_Memo_Preapproval_Detail, item.FullName, item.Name);
                            }
                        }

                        var PayRes = paypal.Pay(order.ClubId, payableAmount, order.ConfirmationId, returnUrl, cancelUrl, ipnNotificationUrl, JumbulaSubSystem.PreapprovalPayment, preapprovadItem.Id, memo, preapproveId);
                        payResponse = JsonConvert.DeserializeObject<PayResponse>(PayRes);
                        if (payResponse.responseEnvelope.ack.HasValue && (payResponse.responseEnvelope.ack.Value == AckCode.SUCCESS || payResponse.responseEnvelope.ack.Value == AckCode.SUCCESSWITHWARNING))
                        {
                            numberOfCharge++;
                            chargedAmount += payableAmount;
                            isSucced = true;
                            SuccedPreapprovalPayment(preapprovadItem.Id, payResponse.payKey, this.GetCurrentUserRoleType(), payableAmount);

                        }
                        if (!isSucced)
                        {
                            FailedPreapprovalPayment(preapprovadItem.Id, payResponse.payKey, PayRes, pdResponse.senderEmail, payableAmount);

                        }
                    }


                }
                if (string.IsNullOrEmpty(preapproveId) || isNeedForAnotherApprove)
                {
                    EmailService.Get(this.ControllerContext).SendPreapprovalRequestEmail(order.Id);
                }
            }

            return Json(new JResult { Status = true, Message = string.Format("Charged amount: {0}", chargedAmount), RecordsAffected = numberOfCharge }, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult PreapprovalPaymentReturnUrl(long preapprovadItemId)
        {
            return Json(new JResult { Status = true, Message = "", RecordsAffected = 0 }, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult PreapprovalCancelReturnUrl(long preapprovadItemId)
        {
            return Json(new JResult { Status = false, Message = "", RecordsAffected = 0 }, JsonRequestBehavior.AllowGet);
        }
        private void FailedPreapprovalPayment(long pendingPreapprovalId, string paykey, string res, string senderEmail, decimal payableAmount)
        {

            var pendingApproval = Ioc.PendingPreapprovalTransactionBusiness.Get(pendingPreapprovalId);

            PaymentDetail paymentDetail = new PaymentDetail(senderEmail, PaymentDetailStatus.ERROR, paykey, PaymentMethod.Paypal, pendingApproval.Order.Club.Currency, res, string.Empty, string.Empty, JbUserService.GetCurrentUserRoleType());


            if (pendingApproval.InstallmentId.HasValue)
            {
                var orderInstallment = Ioc.InstallmentBusiness.Get(pendingApproval.InstallmentId.Value);
                SetPreapprovalPaymentTransaction(null, paymentDetail, TransactionStatus.Failure, payableAmount, orderInstallment, DateTime.UtcNow);

                Ioc.InstallmentBusiness.Update(orderInstallment);

            }
            else
            {
                var order = Ioc.OrderBusiness.Get(pendingApproval.OrderId);
                SetPreapprovalPaymentTransaction(order, paymentDetail, TransactionStatus.Failure, payableAmount, null, DateTime.UtcNow);

                Ioc.OrderBusiness.Update(order);

            }
            pendingApproval.Status = PendingPreapprovalTransactionStatus.Error;

            Ioc.PendingPreapprovalTransactionBusiness.Update(pendingApproval);

            ///ToDo
            ///Check the error code and send email to user
        }


        #endregion
        public virtual JsonResult GetPaymentDetailOption(string paykey)
        {
            var paypal = new PaypalService("", true);
            var res = paypal.GetPaymentDetailOption(paykey);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GetPayDetail(string paykey)
        {
            var paypal = new PaypalService("", true);
            var res = paypal.paymentDetails(paykey);
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public virtual JsonResult GetPayDetailWithTrackingId(string trackingId)
        {
            var paypal = new PaypalService("", true);
            var res = paypal.paymentDetailsWithTrackingId(trackingId);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetPreapprovalDetail(string preapprovalkey)
        {
            var paypal = new PaypalService("", true);
            var res = paypal.PreapprovalDetails(preapprovalkey);
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        private string PaypalPaymentFailed(PaymentDetail detailObj, PaymentDetailsResponse responseObj, string confirmationId, string token = "", long orderItemId = 0, decimal paidAmount = 0, long installmentId = 0)
        {

            string errorMsg = string.Empty;
            if (responseObj.error != null)
            {
                foreach (var error in responseObj.error)
                {
                    errorMsg += error.message + "\n";
                }
            }

            var orderDb = Ioc.OrderBusiness;
            if (!string.IsNullOrEmpty(confirmationId))
            {

                var order = orderDb.GetOrdersByConfirmationID(confirmationId);
                orderDb.SetPaymentTransaction(order, detailObj, TransactionStatus.Failure);
                //  order.OrderStatus = OrderStatusCategories.error;
                orderDb.Update(order);

            }
            if (orderItemId > 0)
            {
                var orderItemDb = Ioc.OrderItemBusiness;
                var orderItem = orderItemDb.GetItem(orderItemId);
                orderDb.SetPaymentTransaction(null, detailObj, TransactionStatus.Failure, null, DateTime.UtcNow, paidAmount, orderItem);
                orderItemDb.Update(orderItem);
            }
            if (!string.IsNullOrEmpty(token) || installmentId > 0)
            {
                List<OrderInstallment> installments = null;
                if (!string.IsNullOrEmpty(token))
                {
                    installments = Ioc.InstallmentBusiness.GetListByToken(token).ToList();
                }
                else
                {
                    installments = new List<OrderInstallment>();
                    installments.Add(Ioc.InstallmentBusiness.Get(installmentId));
                }
                var date = DateTime.UtcNow;
                foreach (var installment in installments)
                {
                    //  installment.Status = OrderStatusCategories.error;
                    orderDb.SetPaymentTransaction(null, detailObj, TransactionStatus.Failure, installment, date);
                    Ioc.InstallmentBusiness.Update(installment);
                }
            }
            return errorMsg;

        }
        public virtual void PayPalPreapprovalNotification(string token = "", int actionCaller = 0)
        {
            try
            {
                byte[] parameters = Request.BinaryRead(Request.ContentLength);

                if (parameters.Length > 0)
                {
                    IPNMessage ipnMessage = new IPNMessage(parameters);
                    var paypal = new PaypalService(ClubSubDomain, true);
                    bool isIpnValidated = paypal.ValidateIPNMessage(ipnMessage.IpnRequest, ipnMessage.IpnEncoding);

                    PaypalIPN paypalIpn = new PaypalIPN()
                    {
                        CreateDate = DateTime.UtcNow,
                        IPN = ipnMessage.IpnRequest,
                        IsValid = isIpnValidated,
                        PaypalIPNCategory = PaypalIPNCategories.IpnNotification,
                        Token = token,
                        TransactionId = ipnMessage.TransactionId,
                        TransactionType = ipnMessage.TransactionType,
                        PaypalCaller = (JumbulaSubSystem)actionCaller,
                        IsProceed = true
                    };
                    Ioc.PaypalIPNBusiness.Create(paypalIpn);



                    if (isIpnValidated)
                    {
                        if (!string.IsNullOrEmpty(ipnMessage.Preapprovalkey))
                        {
                            var isExist = Ioc.OrderPreapprovalBusiness.IsPreapprovalKeyExist(ipnMessage.Preapprovalkey);
                            if (!isExist)
                            {
                                SavePreapprovalKey(ipnMessage.Preapprovalkey, token, (JumbulaSubSystem)actionCaller);
                                paypalIpn.IsActionTaken = true;
                                Ioc.PaypalIPNBusiness.Update(paypalIpn);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }

        }

        public virtual void PayPalNotificationUrl(string token = "", int actionCaller = 0)
        {
            try
            {
                byte[] parameters = Request.BinaryRead(Request.ContentLength);



                if (parameters.Length > 0)
                {

                    IPNMessage ipnMessage = new IPNMessage(parameters);
                    var paypal = new PaypalService(ClubSubDomain, true);
                    bool isIpnValidated = paypal.ValidateIPNMessage(ipnMessage.IpnRequest, ipnMessage.IpnEncoding);
                    JumbulaSubSystem paypalCaller = (JumbulaSubSystem)actionCaller;
                    var date = DateTime.UtcNow;
                    PaypalIPN paypalIpn = new PaypalIPN()
                    {
                        CreateDate = date,
                        IPN = ipnMessage.IpnRequest,
                        IsValid = isIpnValidated,
                        PaypalIPNCategory = PaypalIPNCategories.IpnNotification,
                        Token = token,
                        TransactionId = ipnMessage.TransactionId,
                        TransactionType = ipnMessage.TransactionType,
                        PaypalCaller = paypalCaller,
                        IsProceed = false

                    };
                    Ioc.PaypalIPNBusiness.Create(paypalIpn);


                }
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }

        }

        public virtual void SchedulerToProceedPaypalIPNs()
        {
            var errorMessage = string.Empty;
            var paypalIpnList = Ioc.PaypalIPNBusiness.GetValidUnProceedIPNs().ToList();

            foreach (var paypalIpn in paypalIpnList)
            {
                if (string.IsNullOrEmpty(paypalIpn.Token))
                {
                    errorMessage += "PaypalIpn Id:" + paypalIpn.Id + ", Token is null.\n\r";
                    continue;
                }

                IPNMessage ipnMessage = new IPNMessage(paypalIpn.IPN);
                errorMessage += ProceedPaypalIPNs(ipnMessage, paypalIpn, paypalIpn.PaypalCaller, RoleCategoryType.Parent);
                paypalIpn.IsProceed = true;
                Ioc.PaypalIPNBusiness.Update(paypalIpn);
            }

            if (!string.IsNullOrEmpty(errorMessage))
            { EmailService.Get(this.ControllerContext).SendErrorMessaged(errorMessage); }
        }

        private string ProceedPaypalIPNs(IPNMessage ipnMessage, PaypalIPN paypalIpn, JumbulaSubSystem paypalCaller, RoleCategoryType roleCategoryType)
        {
            var result = string.Empty;

            if (!string.IsNullOrEmpty(ipnMessage.PayKey) && Ioc.TransactionActivityBusiness.IsPayKeyExist(ipnMessage.PayKey))
            {
                return string.Empty;
            }

            switch (paypalCaller)
            {
                case JumbulaSubSystem.Order:
                    {
                        var order = Ioc.OrderBusiness.GetOrdersByConfirmationID(paypalIpn.Token);

                        if (order != null && !string.IsNullOrEmpty(ipnMessage.PayKey))
                        {
                            result = UpdateOrderBasedOnPayment(paypalIpn.Token, ipnMessage.PayKey, roleCategoryType);
                            paypalIpn.IsActionTaken = true;
                            Ioc.PaypalIPNBusiness.Update(paypalIpn);
                        }
                        break;
                    }
                case JumbulaSubSystem.Installment:
                    {
                        var installments = Ioc.InstallmentBusiness.GetListByToken(paypalIpn.Token).Where(i => !i.IsDeleted).ToList();
                        if (installments != null && installments.Count > 0 && !string.IsNullOrEmpty(ipnMessage.PayKey) && installments.TrueForAll(c => c.Status != OrderStatusCategories.completed))
                        {

                            result = UpdateInstallmentBasedOnPayment(paypalIpn.Token, ipnMessage.PayKey, roleCategoryType);
                            paypalIpn.IsActionTaken = true;
                            Ioc.PaypalIPNBusiness.Update(paypalIpn);
                        }
                        break;
                    }
                case JumbulaSubSystem.PreapprovalPayment:
                    {
                        long pendingId = 0;
                        if (long.TryParse(paypalIpn.Token, out pendingId))
                        {
                            var pendingTransaction = Ioc.PendingPreapprovalTransactionBusiness.Get(pendingId);
                            if (pendingTransaction != null && pendingTransaction.PaidAmount < pendingTransaction.DueAmount)
                            {
                                var order = Ioc.OrderBusiness.Get(pendingTransaction.OrderId);
                                var paymentdetail = order.TransactionActivities.Where(t => t.PaymentDetail.PayKey == ipnMessage.PayKey);
                                if (paymentdetail == null || paymentdetail.Count() == 0)
                                {
                                    result = SuccedPreapprovalPayment(pendingId, ipnMessage.PayKey, 0);
                                    paypalIpn.IsActionTaken = true;
                                    Ioc.PaypalIPNBusiness.Update(paypalIpn);
                                }
                            }

                        }
                        break;
                    }

                case JumbulaSubSystem.MakeaPayment:
                    {

                        var transactions = Ioc.TransactionActivityBusiness.GetList().Where(c => c.Token == paypalIpn.Token);


                        if (transactions.Any() && transactions.All(c => c.TransactionStatus == TransactionStatus.Draft))
                        {

                            result = MakePaymentSucceed(paypalIpn.Token, ipnMessage.PayKey, roleCategoryType);
                            paypalIpn.IsActionTaken = true;
                            Ioc.PaypalIPNBusiness.Update(paypalIpn);

                        }
                        break;
                    }

                case JumbulaSubSystem.Invoice:
                    {
                        var transaction = Ioc.TransactionActivityBusiness.GetInvoicedByToken(paypalIpn.Token);
                        if (transaction != null && transaction.Count() > 0)
                        {
                            result = SucceedInvoicePayment(paypalIpn.Token, ipnMessage.PayKey, roleCategoryType);
                            paypalIpn.IsActionTaken = true;
                            Ioc.PaypalIPNBusiness.Update(paypalIpn);
                        }
                        break;
                    }


            }

            if (!string.IsNullOrEmpty(result))
            {

                result = string.Format("Error: Token= {0}, PaypalIpnId= {1}\n\r Message{2} \n\r", paypalIpn.Token, paypalIpn.Id, result);
            }
            return result;
        }

        private string SucceedInvoicePayment(string token, string payKey, RoleCategoryType roleCategoryType, bool isTakePaymentInvoice = false, string trackingId = "")
        {

            try
            {

                var tranDb = Ioc.TransactionActivityBusiness;
                var transactions = tranDb.GetList().Where(c => c.TransactionStatus != TransactionStatus.Failure && c.Token == token).ToList();// .GetPendingByToken(token).ToList();
                var club = transactions.First().Club;
                if (transactions == null || !transactions.Any())
                {
                    return "Invalid Token";
                }
                if (transactions.TrueForAll(c => c.TransactionStatus == TransactionStatus.Success))
                {
                    return UpdateOrderBasedOnPaymentStatus.Duplicate.ToString();
                }
                if (!transactions.TrueForAll(c => c.TransactionStatus == TransactionStatus.Invoice))
                {
                    return "Invalid Token";
                }
                var paidAmount = transactions.Sum(c => c.Amount);
                var orderItem = Ioc.OrderItemBusiness.GetItem(transactions.FirstOrDefault().OrderItemId.Value);
                var invoiceCategory = transactions.Select(t => t.TransactionCategory).First();

                var paypal = new PaypalService(orderItem.Order.Club.Domain, orderItem.Order.IsLive);
                string res = string.Empty;
                if (!string.IsNullOrEmpty(payKey))
                {
                    res = paypal.paymentDetails(payKey);
                }
                else
                {
                    res = paypal.paymentDetailsWithTrackingId(trackingId);
                }

                PaymentDetailsResponse pdResponse = JsonConvert.DeserializeObject<PaymentDetailsResponse>(res);
                if (string.IsNullOrEmpty(payKey))
                {
                    payKey = pdResponse.payKey;
                }
                if (Ioc.TransactionActivityBusiness.IsPayKeyExist(payKey))
                {
                    return UpdateOrderBasedOnPaymentStatus.Duplicate.ToString();
                }

                var paymentdetailStatus = (PaymentDetailStatus)Enum.Parse(typeof(PaymentDetailStatus), pdResponse.status.ToUpper());
                PaymentDetail paymentDetail = new PaymentDetail(string.IsNullOrEmpty(pdResponse.senderEmail) ? orderItem.Order.User.UserName : pdResponse.senderEmail, paymentdetailStatus, string.IsNullOrEmpty(payKey) ? trackingId : payKey, PaymentMethod.Paypal, orderItem.Order.Club.Currency, res, string.Empty, pdResponse.paymentInfoList != null ? pdResponse.paymentInfoList.paymentInfo[0].transactionId : string.Empty, roleCategoryType);


                if (pdResponse.responseEnvelope == null || (pdResponse.responseEnvelope.ack != AckCode.SUCCESS && pdResponse.responseEnvelope.ack != AckCode.SUCCESSWITHWARNING))
                {
                    return PaypalPaymentFailed(paymentDetail, pdResponse, string.Empty, string.Empty, orderItem.Id, paidAmount);
                }

                if (pdResponse.status.ToUpper() == PaymentDetailStatus.ERROR.ToString() || pdResponse.status.ToUpper() == PaymentDetailStatus.INCOMPLETE.ToString())
                {
                    return PaypalPaymentFailed(paymentDetail, pdResponse, string.Empty, string.Empty, orderItem.Id, paidAmount);
                }




                decimal totalPaidAmount = 0;

                foreach (var paymentInfo in pdResponse.paymentInfoList.paymentInfo)
                {
                    if (paymentInfo.receiver.primary)
                    {
                        totalPaidAmount += paymentInfo.receiver.amount.Value;
                    }

                }


                var date = DateTime.UtcNow;

                if (invoiceCategory != TransactionCategory.Invoice)
                {
                    foreach (var tran in transactions)
                    {
                        tran.PaymentDetail.Currency = paymentDetail.Currency;
                        tran.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                        tran.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                        tran.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                        tran.PaymentDetail.Status = paymentDetail.Status;
                        tran.PaymentDetail.PayKey = paymentDetail.PayKey;
                        tran.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                        tran.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                        tran.TransactionStatus = TransactionStatus.Success;
                        tran.PaymentDetail.PayerType = this.GetCurrentUserRoleType();
                        tran.TransactionDate = date;
                        tranDb.Update(tran);
                        if (tran.InstallmentId.HasValue && tran.InstallmentId.Value > 0)
                        {
                            var installment = Ioc.InstallmentBusiness.Get(tran.InstallmentId.Value);
                            installment.PaidAmount = (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0) + tran.Amount;
                            installment.PaidDate = date;
                            installment.Status = OrderStatusCategories.completed;
                            Ioc.InstallmentBusiness.Update(installment);
                        }
                        orderItem.PaidAmount += tran.Amount;
                    }
                    orderItem.PaidAmount += totalPaidAmount;
                    Ioc.OrderItemBusiness.Update(orderItem);
                    Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.MadePayment, string.Format(Constants.Order_MakePayment, totalPaidAmount, transactions.FirstOrDefault().Note), orderItem.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, orderItem.Id);
                    EmailService.Get(this.ControllerContext).SendReceivedPaymentEmail(Ioc.OrderItemBusiness.GetItem(transactions.FirstOrDefault().OrderItemId.Value), totalPaidAmount, DateTime.UtcNow, paymentDetail);

                    return string.Empty;
                }
                else
                {
                    var invoice = transactions.First().PaymentDetail.invoice;
                    invoice.PaidAmount = paidAmount;
                    invoice.Status = InvoiceStatus.Paid;
                    Ioc.InvoiceBusiness.Update(invoice);
                    foreach (var transaction in transactions)
                    {
                        transaction.PaymentDetail.Currency = paymentDetail.Currency;
                        transaction.PaymentDetail.PayerEmail = paymentDetail.PayerEmail;
                        transaction.PaymentDetail.PaymentMethod = paymentDetail.PaymentMethod;
                        transaction.PaymentDetail.PaypalIPN = paymentDetail.PaypalIPN;
                        transaction.PaymentDetail.Status = paymentDetail.Status;
                        transaction.PaymentDetail.PayKey = paymentDetail.PayKey;
                        transaction.PaymentDetail.TransactionId = paymentDetail.TransactionId;
                        transaction.PaymentDetail.TransactionMessage = paymentDetail.TransactionMessage;
                        transaction.PaymentDetail.CreditCardId = paymentDetail.CreditCardId;
                        transaction.PaymentDetail.PayerType = this.GetCurrentUserRoleType();
                        transaction.TransactionStatus = TransactionStatus.Success;
                        transaction.TransactionDate = date;
                        tranDb.Update(transaction);

                        if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                        {
                            var installment = Ioc.InstallmentBusiness.Get(transaction.InstallmentId.Value);
                            installment.PaidAmount = (installment.PaidAmount.HasValue ? installment.PaidAmount.Value : 0) + transaction.Amount;
                            installment.PaidDate = date;
                            installment.Status = OrderStatusCategories.completed;
                            Ioc.InstallmentBusiness.Update(installment);

                        }

                    }

                    var invoiceHistoryDescription = Constants.Order_InvoiceHistory;
                    var invoiceHistoryDescriptionPaided = Constants.Invoice_Paided;

                    if (JbUserService.GetCurrentUserRoleType() == RoleCategoryType.Parent)
                    {
                        invoiceHistoryDescription = Common.Constants.Payment.ParentInvoiceDescription;
                        invoiceHistoryDescriptionPaided = Common.Constants.Payment.ParentInvoiceDescriptionPaided;
                    }

                    foreach (var transaction in transactions)
                    {
                        if (transaction.InstallmentId.HasValue && transaction.InstallmentId.Value > 0)
                        {
                            var orderItemInInvoice = transaction.OrderItem;
                            var instalment = transaction.Installment;
                            var InstalmentordeItamBalance = transaction.Amount;
                            orderItemInInvoice.PaidAmount += transaction.Amount;
                            Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(invoiceHistoryDescription, InstalmentordeItamBalance), orderItemInInvoice.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, orderItemInInvoice.Id);

                        }
                        else
                        {
                            var orderItemInInvoice = transaction.OrderItem;
                            var ordeItamBalance = transaction.Amount;
                            orderItemInInvoice.PaidAmount += transaction.Amount;
                            Ioc.OrderItemBusiness.Update(orderItemInInvoice);
                            Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Invoice, string.Format(invoiceHistoryDescription, ordeItamBalance), orderItemInInvoice.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, orderItemInInvoice.Id);
                        }

                    }
                    var invoiceId = invoice.Id;
                    var userName = invoice.User.UserName;
                    var today = DateTime.UtcNow;
                    var clubTime = Ioc.ClubBusiness.GetClubDateTime(club.Id, today);
                    if (isTakePaymentInvoice)
                    {
                        var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(Constants.Invoice_TakePaymentWithPayPal, userName, CurrencyHelper.FormatCurrencyWithPenny(paidAmount, club.Currency)) };
                        Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
                    }
                    else
                    {
                        var invoiceHistory = new InvoiceHistory() { InvoiceId = invoiceId, ActionDate = clubTime, Action = Invoicestatus.Paid, Description = string.Format(invoiceHistoryDescriptionPaided, userName, CurrencyHelper.FormatCurrencyWithPenny(paidAmount, club.Currency)) };
                        Ioc.InvoiceHistoryBusiness.Create(invoiceHistory);
                    }


                    totalPaidAmount = transactions.Sum(c => c.Amount);

                    EmailService.Get(this.ControllerContext).SendReceivedFamilyInvoicePaymentEmail(transactions, totalPaidAmount, DateTime.UtcNow, paymentDetail, invoice);

                    return string.Empty;
                }
            }

            catch (Exception e)
            {
                LogHelper.LogException(e);
                return e.Message;
            }
        }
        private string SuccedPreapprovalPayment(long pendingPreapprovalId, string paykey, RoleCategoryType roleCategoryType, decimal totalPaid = 0, string trackingId = "")
        {
            try
            {
                var paymentCaller = JumbulaSubSystem.Order;
                var orderDb = Ioc.OrderBusiness;
                var pendingApproval = Ioc.PendingPreapprovalTransactionBusiness.Get(pendingPreapprovalId);
                if (pendingApproval.InstallmentId.HasValue)
                {
                    paymentCaller = JumbulaSubSystem.Installment;
                }
                var order = Ioc.OrderBusiness.Get(pendingApproval.OrderId);
                var paypal = new PaypalService(order.Club.Domain, order.IsLive);
                string res = string.Empty;
                if (!string.IsNullOrEmpty(paykey))
                {
                    res = paypal.paymentDetails(paykey);
                }
                else
                {
                    res = paypal.paymentDetailsWithTrackingId(trackingId);
                }

                PaymentDetailsResponse pdResponse = JsonConvert.DeserializeObject<PaymentDetailsResponse>(res);
                if (string.IsNullOrEmpty(paykey))
                {
                    if (string.IsNullOrEmpty(pdResponse.payKey))
                    {
                        return UpdateOrderBasedOnPaymentStatus.NoPayKey.ToString();
                    }
                    if (order.TransactionActivities != null)
                    {
                        var paymentdetail = order.TransactionActivities.Where(t => t.PaymentDetail.PayKey == pdResponse.payKey);
                        if (paymentdetail != null && paymentdetail.Count() > 0)
                        {
                            return UpdateOrderBasedOnPaymentStatus.Duplicate.ToString();
                        }
                    }
                }
                if (totalPaid == 0)
                {
                    foreach (var paymentInfo in pdResponse.paymentInfoList.paymentInfo)
                    {
                        totalPaid += paymentInfo.receiver.amount.Value;
                    }
                }

                var currencyCode = order.Club.Currency;
                Enum.TryParse<CurrencyCodes>(pdResponse.currencyCode, out currencyCode);
                PaymentDetail paymentDetail = new PaymentDetail(string.IsNullOrEmpty(pdResponse.senderEmail) ? order.User.UserName : pdResponse.senderEmail, PaymentDetailStatus.COMPLETED, paykey, PaymentMethod.Paypal, currencyCode, res, string.Empty, pdResponse.paymentInfoList != null ? pdResponse.paymentInfoList.paymentInfo[0].transactionId : string.Empty, roleCategoryType);
                if (pdResponse.status.ToUpper() == PaymentDetailStatus.ERROR.ToString() || pdResponse.status.ToUpper() == PaymentDetailStatus.INCOMPLETE.ToString())
                {
                    paymentDetail.Status = PaymentDetailStatus.ERROR;
                    if (paymentCaller == JumbulaSubSystem.Order)
                    {
                        return PaypalPaymentFailed(paymentDetail, pdResponse, order.ConfirmationId);
                    }
                    else
                    {

                        return PaypalPaymentFailed(paymentDetail, pdResponse, "", "", 0, totalPaid, pendingApproval.InstallmentId.Value);
                    }
                }

                var remaindPaidAmount = totalPaid;


                var installmentToken = string.Empty;
                var date = DateTime.UtcNow;
                var accountingService = Ioc.AccountingBusiness;
                if (paymentCaller == JumbulaSubSystem.Order)
                {
                    foreach (OrderItem item in order.RegularOrderItems)
                    {
                        if (remaindPaidAmount <= 0)
                        {
                            break;
                        }
                        decimal itemBalance = accountingService.OrderItemDueAmount(item);
                        if (itemBalance == 0)
                        {
                            continue;
                        }
                        var paidamount = itemBalance;
                        if (paidamount > remaindPaidAmount)
                        {
                            paidamount = remaindPaidAmount;
                        }
                        item.PaidAmount += paidamount;

                        if (item.PaymentPlanType == PaymentPlanType.Installment && item.Installments.Any())
                        {
                            var installment = item.Installments.Where(i => !i.IsDeleted).FirstOrDefault();

                            installment.PaidDate = date;
                            if (installment.PaidAmount.HasValue && installment.PaidAmount.Value > 0)
                            {
                                installment.PaidAmount = installment.PaidAmount.Value + paidamount;
                            }
                            else
                            {
                                installment.PaidAmount = paidamount;
                            }
                            installment.Status = OrderStatusCategories.completed;
                            installment.NoticeSent = true;

                            SetPreapprovalPaymentTransaction(null, paymentDetail, TransactionStatus.Success, paidamount, installment, date);


                        }
                        else
                        {

                            SetPreapprovalPaymentTransaction(null, paymentDetail, TransactionStatus.Success, paidamount, null, date, item);
                        }
                        Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Paid, string.Format(Constants.Order_Preapprovad_Paid, CurrencyHelper.FormatCurrencyWithPenny(paidamount, order.Club.Currency), order.ConfirmationId), order.Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, item.Id);

                        remaindPaidAmount -= paidamount;
                    }

                    orderDb.Update(order);
                }
                else
                {

                    var installment = Ioc.InstallmentBusiness.Get(pendingApproval.InstallmentId.Value);

                    installment.PaidAmount = (installment.PaidAmount.HasValue) ? (installment.PaidAmount.Value + totalPaid) : totalPaid;
                    installment.PaidDate = date;
                    installment.Status = OrderStatusCategories.completed;
                    orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Success, installment, date);
                    Ioc.InstallmentBusiness.Update(installment);
                    Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Paid, string.Format(Constants.OrderInstallment_Paid, CurrencyHelper.FormatCurrencyWithPenny(totalPaid, order.Club.Currency), installment.InstallmentDate.ToString(Constants.DefaultDateFormat)), installment.OrderItem.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, installment.OrderItemId);


                    var orderItem = Ioc.OrderItemBusiness.GetItem(installment.OrderItemId);
                    orderItem.PaidAmount += installment.PaidAmount.Value;
                    orderDb.Update(orderItem.Order);
                }
                pendingApproval.PaidAmount = totalPaid;
                pendingApproval.Status = (pendingApproval.DueAmount > pendingApproval.PaidAmount) ? PendingPreapprovalTransactionStatus.PartiallyCompleted : PendingPreapprovalTransactionStatus.Completed;
                Ioc.PendingPreapprovalTransactionBusiness.Update(pendingApproval);
                EmailService.Get(this.ControllerContext).SendPreapprovalPaymentConfirmationEmail(pendingApproval.OrderId, date);
                return string.Empty;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        private string UpdateOrderBasedOnPayment(string cid, string payKey, RoleCategoryType roleCategoryType, string trackingId = "")
        {
            try
            {
                var orderDb = Ioc.OrderBusiness;
                var order = orderDb.GetOrdersByConfirmationID(cid);
                OperationStatus updateStatus = new OperationStatus() { Status = true };

                var paypal = new PaypalService(order.Club.Domain, order.IsLive);
                string res = string.Empty;
                if (!string.IsNullOrEmpty(payKey))
                {
                    res = paypal.paymentDetails(payKey);
                }
                else
                {
                    res = paypal.paymentDetailsWithTrackingId(trackingId);
                }


                PaymentDetailsResponse pdResponse = JsonConvert.DeserializeObject<PaymentDetailsResponse>(res);

                var paymentdetailStatus = (PaymentDetailStatus)Enum.Parse(typeof(PaymentDetailStatus), pdResponse.status.ToUpper(), false);
                var currencyCode = order.Club.Currency;
                Enum.TryParse<CurrencyCodes>(pdResponse.currencyCode, out currencyCode);
                PaymentDetail paymentDetail = new PaymentDetail(pdResponse.senderEmail, paymentdetailStatus, pdResponse.payKey, PaymentMethod.Paypal, currencyCode, res, string.Empty, pdResponse.paymentInfoList != null ? pdResponse.paymentInfoList.paymentInfo[0].transactionId : string.Empty, roleCategoryType);


                if (pdResponse.responseEnvelope == null || (pdResponse.responseEnvelope.ack != AckCode.SUCCESS && pdResponse.responseEnvelope.ack != AckCode.SUCCESSWITHWARNING))
                {
                    return PaypalPaymentFailed(paymentDetail, pdResponse, cid);
                }

                if (pdResponse.status.ToUpper() == PaymentDetailStatus.ERROR.ToString() || pdResponse.status.ToUpper() == PaymentDetailStatus.INCOMPLETE.ToString())
                {
                    return PaypalPaymentFailed(paymentDetail, pdResponse, cid);
                }

                OrderStatusCategories paymentStaus = OrderStatusCategories.completed;

                if (pdResponse.status.ToUpper() == PaymentDetailStatus.CREATED.ToString() || pdResponse.status.ToUpper() == PaymentDetailStatus.PENDING.ToString() || pdResponse.status.ToUpper() == PaymentDetailStatus.PROCESSING.ToString())
                {
                    paymentStaus = OrderStatusCategories.pending;
                }

                var paymentOption = paypal.GetPaymentDetailOption(pdResponse.payKey);

                if (paymentOption.responseEnvelope.ack == AckCode.FAILURE || paymentOption.responseEnvelope.ack == AckCode.FAILUREWITHWARNING)
                {
                    var errorMsg = string.Empty;
                    if (paymentOption.error != null)
                    {
                        foreach (var error in paymentOption.error)
                        {
                            errorMsg += error.message + "\n";
                        }
                    }
                    return errorMsg;
                }

                if (!order.IsDraft && order.TransactionActivities != null && order.TransactionActivities.Any())
                {
                    var paymentdetail = order.TransactionActivities.Where(t => t.TransactionStatus == TransactionStatus.Success && t.PaymentDetail != null && t.PaymentDetail.PayKey == payKey);
                    if (paymentdetail != null && paymentdetail.Any())
                    {
                        return UpdateOrderBasedOnPaymentStatus.Duplicate.ToString();
                    }
                }

                order.IsDraft = false;
                if (order.OrderStatus == OrderStatusCategories.deleted)
                {
                    order.OrderStatus = OrderStatusCategories.completed;
                }

                var paidItemIds = new List<long>();
                List<long> deletedItems = null;
                if (paymentOption.receiverOptions != null && paymentOption.receiverOptions[0].invoiceData != null && paymentOption.receiverOptions[0].invoiceData.item.Count > 0)
                {
                    deletedItems = order.OrderItems.Select(o => o.Id).Except(paymentOption.receiverOptions[0].invoiceData.item.Select(c => long.Parse(c.identifier))).ToList();
                    foreach (var invoiceItem in paymentOption.receiverOptions[0].invoiceData.item)
                    {
                        var paidItem = order.OrderItems.Single(c => c.Id == long.Parse(invoiceItem.identifier));
                        paidItem.ItemStatus = OrderItemStatusCategories.completed;
                        paidItemIds.Add(paidItem.Id);
                    }
                }

                if (deletedItems != null && deletedItems.Any())
                {
                    foreach (var itemId in deletedItems)
                    {
                        var orderItem = order.OrderItems.Single(c => c.Id == itemId);

                        if (orderItem.ItemStatus == OrderItemStatusCategories.initiated)
                        {
                            orderItem.ItemStatus = OrderItemStatusCategories.deleted;

                            Serilog.Log.Warning("OrderItem {0} was deleted with PaypalIPN scheduler", orderItem.Id);
                        }
                    }
                }

                decimal payableAmount = Ioc.OrderBusiness.CalculateOrderPayableAmount(order);

                order.OrderStatus = paymentStaus;
                var installmentToken = string.Empty;
                var date = DateTime.UtcNow;

                foreach (OrderItem item in order.OrderItems.Where(o => paidItemIds.Contains(o.Id)).ToList())
                {

                    item.ItemStatus = OrderItemStatusCategories.completed;
                    item.PaidAmount += item.TotalAmount;

                    if (string.IsNullOrEmpty(installmentToken))
                    {
                        installmentToken = Guid.NewGuid().ToString("N");
                    }

                    if (item.PaymentPlanType == PaymentPlanType.Installment && item.Installments.Any())
                    {
                        var firstInst = item.Installments.Where(i => !i.IsDeleted).FirstOrDefault();
                        item.PaidAmount += firstInst.Amount;
                        firstInst.PaidDate = date;
                        firstInst.PaidAmount = firstInst.Amount;
                        firstInst.Status = OrderStatusCategories.completed;
                        firstInst.NoticeSent = true;
                        firstInst.Token = installmentToken;
                    }

                    Serilog.Log.Information("OrderItem {0} was completed with PaypalIPN scheduler", item.Id);
                }

                orderDb.SetPaymentTransaction(order, paymentDetail, TransactionStatus.Success, null, date);
                Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Paid, string.Format(Constants.Order_Paid, order.ConfirmationId), order.Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1);

                updateStatus = orderDb.Update(order);

                try
                {
                    Ioc.ClubBusiness.AddUserInClubUsers(order);
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }

                EmailService.Get(this.ControllerContext).SendReservationEmail(cid, paymentDetail.PaymentMethod, Request.Url.Host);

                return string.Empty;
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return ex.Message;
            }
        }

        private string UpdateInstallmentBasedOnPayment(string token, string payKey, RoleCategoryType roleCategoryType, string trackingId = "")
        {

            var result = string.Empty;

            try
            {
                var installmentDB = Ioc.InstallmentBusiness;
                var installmentList = installmentDB.GetListByToken(token).Where(i => !i.IsDeleted).ToList();
                if (installmentList == null || installmentList.Count == 0)
                {
                    return UpdateOrderBasedOnPaymentStatus.NoElementFound.ToString();
                }

                var orderDb = Ioc.OrderBusiness;
                var order = installmentList.FirstOrDefault().OrderItem.Order;
                var paypal = new PaypalService(order.Club.Domain, order.IsLive);
                string res = string.Empty;
                if (!string.IsNullOrEmpty(payKey))
                {
                    res = paypal.paymentDetails(payKey);
                }
                else
                {
                    res = paypal.paymentDetailsWithTrackingId(trackingId);

                }
                PaymentDetailsResponse pdResponse = JsonConvert.DeserializeObject<PaymentDetailsResponse>(res);
                if (string.IsNullOrEmpty(payKey))
                {
                    payKey = pdResponse.payKey;
                }
                var currencyCode = CurrencyCodes.USD;
                Enum.TryParse<CurrencyCodes>(pdResponse.currencyCode, out currencyCode);
                var paymentdetailStatus = (PaymentDetailStatus)Enum.Parse(typeof(PaymentDetailStatus), pdResponse.status.ToUpper());
                PaymentDetail paymentDetail = new PaymentDetail(pdResponse.senderEmail, paymentdetailStatus, string.IsNullOrEmpty(payKey) ? trackingId : payKey, PaymentMethod.Paypal, currencyCode, res, string.Empty, pdResponse.paymentInfoList != null ? pdResponse.paymentInfoList.paymentInfo[0].transactionId : string.Empty, roleCategoryType);



                if (pdResponse.responseEnvelope == null || (pdResponse.responseEnvelope.ack != AckCode.SUCCESS && pdResponse.responseEnvelope.ack != AckCode.SUCCESSWITHWARNING))
                {
                    return PaypalPaymentFailed(paymentDetail, pdResponse, string.Empty, token);
                }
                if (pdResponse.status.ToUpper() == PaymentDetailStatus.ERROR.ToString() || pdResponse.status.ToUpper() == PaymentDetailStatus.INCOMPLETE.ToString())
                {
                    return PaypalPaymentFailed(paymentDetail, pdResponse, string.Empty, token);
                }




                var Tmppaymentdetail = installmentList.Where(t => t.TransactionActivities != null).SelectMany(c => c.TransactionActivities).Where(t => t.TransactionStatus == TransactionStatus.Success && t.PaymentDetail != null && t.PaymentDetail.PayKey == payKey);
                if (Tmppaymentdetail != null && Tmppaymentdetail.Count() > 0)
                {
                    return UpdateOrderBasedOnPaymentStatus.Duplicate.ToString();
                }


                //decimal totalPaidAmount = 0;
                //foreach (var paymentInfo in pdResponse.paymentInfoList.paymentInfo)
                //{
                //    totalPaidAmount += paymentInfo.receiver.amount.Value;
                //}
                //if (installmentList.Sum(c => c.Balance) != totalPaidAmount)
                //{
                //    paymentDetail.Status = PaymentDetailStatus.MISMATCHEDAMOUNT;

                //    return UpdateOrderBasedOnPaymentStatus.MisMatched.ToString() + string.Format(Constants.F_Payment_MismatchedAmount, token, installmentList.Sum(c => c.Balance), totalPaidAmount);

                //}


                var date = DateTime.UtcNow;
                var itemInstallment = installmentList.GroupBy(i => i.OrderItemId);

                foreach (var installments in itemInstallment)
                {
                    var orderItem = Ioc.OrderItemBusiness.GetItem(installments.Key);
                    var currency = orderItem.Order.Club.Currency;
                    decimal totalpaidAmount = 0;
                    foreach (var item in installments)
                    {
                        var paidAmount = item.Balance; ;
                        totalpaidAmount += paidAmount;
                        item.PaidAmount = paidAmount + (item.PaidAmount.HasValue ? item.PaidAmount.Value : 0);
                        item.PaidDate = date;
                        item.Status = OrderStatusCategories.completed;
                        orderDb.SetPaymentTransaction(null, paymentDetail, TransactionStatus.Success, item, date, paidAmount);
                        installmentDB.Update(item);

                        Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.Paid, string.Format(Constants.OrderInstallment_Paid, CurrencyHelper.FormatCurrencyWithPenny(item.PaidAmount, currency), item.InstallmentDate.ToString(Constants.DefaultDateFormat)), item.OrderItem.Order_Id, (this.IsUserAuthenticated()) ? this.GetCurrentUserId() : -1, item.OrderItemId);


                    }


                    orderItem.PaidAmount += totalpaidAmount;
                    orderDb.Update(orderItem.Order);
                }

                EmailService.Get(this.ControllerContext).SendInstallmentPaymentConfirmation(token, date, true);

                return string.Empty;
            }


            catch (Exception e)
            {
                LogHelper.LogException(e);

                return e.Message;
            }


        }

        private string MakePaymentSucceed(string token, string payKey, RoleCategoryType roleCategoryType, string trackingId = "", bool isFamilyTakePayment = false)
        {


            var errorMsg = string.Empty;
            var transactions = Ioc.TransactionActivityBusiness.GetList().Where(c => c.Token == token).ToList();
            if (transactions != null && transactions.Count > 0)
            {
                if (transactions.TrueForAll(c => c.TransactionStatus != TransactionStatus.Draft))
                {
                    return UpdateOrderBasedOnPaymentStatus.Duplicate.ToString();
                }
                var orderItem = transactions.First().OrderItem;
                var orderItems = transactions.Select(t => t.OrderItem).GroupBy(i => i.Id).ToList();
                var paypal = new PaypalService(orderItem.Order.Club.Domain, orderItem.Order.IsLive);
                string res = string.Empty;
                if (!string.IsNullOrEmpty(payKey))
                {
                    res = paypal.paymentDetails(payKey);
                }
                else
                {
                    res = paypal.paymentDetailsWithTrackingId(trackingId);
                }
                PaymentDetailsResponse pdResponse = JsonConvert.DeserializeObject<PaymentDetailsResponse>(res);
                if (string.IsNullOrEmpty(payKey))
                {
                    if (string.IsNullOrEmpty(pdResponse.payKey))
                    {
                        return UpdateOrderBasedOnPaymentStatus.NoPayKey.ToString();
                    }
                    payKey = pdResponse.payKey;
                }

                if (orderItem.TransactionActivities.Any(t => t.PaymentDetail.PayKey == pdResponse.payKey))
                {
                    return UpdateOrderBasedOnPaymentStatus.Duplicate.ToString();
                }
                var paymentdetailStatus = (PaymentDetailStatus)Enum.Parse(typeof(PaymentDetailStatus), pdResponse.status.ToUpper(), false);
                var currencyCode = CurrencyCodes.USD;
                Enum.TryParse<CurrencyCodes>(pdResponse.currencyCode, out currencyCode);
                PaymentDetail paymentDetail = new PaymentDetail(pdResponse.senderEmail, paymentdetailStatus, pdResponse.payKey, PaymentMethod.Paypal, currencyCode, res, string.Empty, pdResponse.paymentInfoList != null ? pdResponse.paymentInfoList.paymentInfo[0].transactionId : string.Empty, roleCategoryType);

                decimal totalPaidAmount = 0;

                foreach (var paymentInfo in pdResponse.paymentInfoList.paymentInfo)
                {
                    if (pdResponse.paymentInfoList.paymentInfo.Count > 1)
                    {
                        if (paymentInfo.receiver.primary)
                        {
                            totalPaidAmount += paymentInfo.receiver.amount.Value;
                        }
                    }
                    else
                    {
                        totalPaidAmount += paymentInfo.receiver.amount.Value;
                    }

                }

                if (pdResponse.responseEnvelope == null || (pdResponse.responseEnvelope.ack != AckCode.SUCCESS && pdResponse.responseEnvelope.ack != AckCode.SUCCESSWITHWARNING))
                {
                    errorMsg = PaypalPaymentFailed(paymentDetail, pdResponse, string.Empty, string.Empty, orderItem.Id, totalPaidAmount);

                }

                if (pdResponse.status.ToUpper() == PaymentDetailStatus.ERROR.ToString() || pdResponse.status.ToUpper() == PaymentDetailStatus.INCOMPLETE.ToString())
                {
                    errorMsg = PaypalPaymentFailed(paymentDetail, pdResponse, string.Empty, string.Empty, orderItem.Id, totalPaidAmount);

                }




                var selectedInstallment = new List<OrderInstallment>();
                foreach (var tran in transactions)
                {
                    if (tran.InstallmentId.HasValue && tran.InstallmentId.Value > 0)
                    {
                        selectedInstallment.Add(Ioc.InstallmentBusiness.Get(tran.InstallmentId.Value));
                    }
                }
                OperationStatus result = null;
                if (isFamilyTakePayment)
                {
                    var listOderItems = new List<OrderItem>();

                    listOderItems.AddRange(orderItems.Select(o => o.FirstOrDefault()));

                    result = OrderServices.Get().ReceivedPaymentFromMultiOrder(listOderItems, null, totalPaidAmount, paymentDetail, transactions.First().Note, transactions.First().TransactionDate, transactions.First().CheckId, selectedInstallment, transactions);

                    if (result.Status)
                    {
                        EmailService.Get(this.ControllerContext).SendReceivedFromCreditCardFamilyTakePaymentEmail(transactions, transactions.First().TransactionDate, paymentDetail, transactions.First().Order.UserId, transactions.First().ClubId);
                    }
                    return result.Status == false ? result.ExceptionMessage : errorMsg;
                }
                else
                {
                    result = OrderServices.Get().ReceivedPayment(orderItem.Id, totalPaidAmount, paymentDetail, transactions.First().Note, transactions.First().TransactionDate, transactions.First().CheckId, selectedInstallment, transactions);
                    if (result.Status)
                    {
                        EmailService.Get(this.ControllerContext).SendReceivedPaymentEmail(orderItem, totalPaidAmount, transactions.First().TransactionDate, paymentDetail);
                    }

                    return result.Status == false ? result.ExceptionMessage : errorMsg;
                }


            }

            return UpdateOrderBasedOnPaymentStatus.NoElementFound.ToString();

        }
        private string SavePreapprovalKey(string preapprovalKey, string token, JumbulaSubSystem actionCaller)
        {
            Order order = Ioc.OrderBusiness.GetOrdersByConfirmationID(token);
            if (order == null)
            {
                return "Token is invalid";
            }
            if (!order.IsDraft && Ioc.OrderPreapprovalBusiness.IsPreapprovalKeyExist(preapprovalKey))
            {
                return UpdateOrderBasedOnPaymentStatus.Duplicate.ToString();
            }
            var paypal = new PaypalService(order.Club.Domain, order.IsLive);
            var res = paypal.PreapprovalDetails(preapprovalKey);
            var pdResponse = new PreapprovalDetailsResponse();
            pdResponse = JsonConvert.DeserializeObject<PreapprovalDetailsResponse>(res);
            if ((pdResponse.approved.HasValue && pdResponse.approved.Value))
            {
                if (actionCaller == JumbulaSubSystem.Order)
                {

                    if (order.IsDraft)
                    {
                        List<PendingPreapprovalTransaction> pendingTransaction = new List<PendingPreapprovalTransaction>();
                        order.IsDraft = false;
                        pendingTransaction.Add(
                         new PendingPreapprovalTransaction()
                         {
                             DueAmount = Ioc.OrderBusiness.CalculateOrderPayableAmount(order),
                             OrderId = order.Id,
                             DueDate = DateTime.UtcNow.AddDays(1),
                             Status = PendingPreapprovalTransactionStatus.Pending
                         });
                        order.OrderStatus = OrderStatusCategories.completed;
                        foreach (var orderItem in order.RegularOrderItems)
                        {
                            orderItem.ItemStatus = OrderItemStatusCategories.completed;
                            if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
                            {
                                orderItem.Installments.FirstOrDefault().Status = OrderStatusCategories.completed;
                                foreach (var installment in orderItem.Installments.Skip(1))
                                {
                                    installment.EnableReminder = false;
                                    pendingTransaction.Add(new PendingPreapprovalTransaction()
                                    {
                                        DueAmount = installment.Amount,
                                        OrderId = order.Id,
                                        InstallmentId = installment.Id,
                                        DueDate = installment.InstallmentDate,
                                        Status = PendingPreapprovalTransactionStatus.Pending
                                    });
                                }
                            }
                        }
                        Ioc.OrderBusiness.Update(order);
                        Ioc.PendingPreapprovalTransactionBusiness.CreateList(pendingTransaction);
                    }

                }
                else if (order.PendingPreapprovalTransactions == null || order.PendingPreapprovalTransactions.Count == 0)
                {
                    List<PendingPreapprovalTransaction> pendingTransaction = new List<PendingPreapprovalTransaction>();
                    order.IsAutoCharge = true;
                    foreach (var orderItem in order.RegularOrderItems)
                    {

                        if (orderItem.PaymentPlanType == PaymentPlanType.Installment)
                        {

                            foreach (var installment in orderItem.Installments.Where(c => c.Status == OrderStatusCategories.initiated))
                            {
                                installment.EnableReminder = false;
                                pendingTransaction.Add(new PendingPreapprovalTransaction()
                                {
                                    DueAmount = installment.Amount,
                                    OrderId = order.Id,
                                    InstallmentId = installment.Id,
                                    DueDate = installment.InstallmentDate,
                                    Status = PendingPreapprovalTransactionStatus.Pending
                                });
                            }
                        }
                    }
                    Ioc.OrderBusiness.Update(order);
                    Ioc.PendingPreapprovalTransactionBusiness.CreateList(pendingTransaction);
                }

                var orderPreapprove = new OrderPreapproval()
                {
                    Order = order,
                    OrderId = order.Id,
                    PaypalPreapprovalID = preapprovalKey,
                    PaypalPreapprovalStartDate = Convert.ToDateTime(pdResponse.startingDate),
                    PaypalPreapprovalEndDate = Convert.ToDateTime(pdResponse.endingDate)
                };
                Ioc.OrderPreapprovalBusiness.Create(orderPreapprove);

                EmailService.Get(this.ControllerContext).SendPreapprovalConfirmation(order.Id, preapprovalKey);
                if (actionCaller == JumbulaSubSystem.Order)
                {
                    EmailService.Get(this.ControllerContext).SendReservationEmail(token, PaymentMethod.None, Request.Url.Host);
                }
                return string.Empty;

            }
            else
            {
                PaypalIPN paypalIPN = new PaypalIPN()
                {
                    CreateDate = DateTime.UtcNow,
                    IPN = res,
                    IsValid = true,
                    TransactionId = preapprovalKey,
                    PaypalIPNCategory = PaypalIPNCategories.customIPN,
                    Token = token,
                    PaypalCaller = (JumbulaSubSystem)actionCaller,
                    IsProceed = true
                };

                Ioc.PaypalIPNBusiness.Create(paypalIPN);
                return "Preapproval key is invalid";
            }
        }
        private void SetPreapprovalPaymentTransaction(Order order, PaymentDetail paymentDetail, TransactionStatus transactionStatus, decimal transactionAmount, OrderInstallment installment = null, DateTime? date = null, OrderItem orderItem = null)
        {
            if (date == null)
            {
                date = DateTime.UtcNow;
            }
            if (order != null && order.RegularOrderItems.Any())
            {
                var accountServer = Ioc.AccountingBusiness;
                foreach (var item in order.RegularOrderItems)
                {

                    if (item.TransactionActivities == null)
                    {
                        item.TransactionActivities = new List<TransactionActivity>();
                    }
                    item.TransactionActivities.Add(new TransactionActivity()
                    {
                        Amount = accountServer.OrderItemDueAmount(item, date),
                        TransactionCategory = TransactionCategory.PreapprovalSale,
                        ClubId = order.ClubId,
                        OrderId = order.Id,
                        OrderItemId = item.Id,
                        PaymentDetail = paymentDetail,
                        TransactionDate = date.Value,
                        TransactionStatus = transactionStatus,
                        TransactionType = TransactionType.Payment,
                        SeasonId = item.SeasonId,
                        HandleMode = HandleMode.Online,
                        MetaData = new MetaData
                        {
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        }
                    });

                }
            }
            if (orderItem != null)
            {


                if (orderItem.TransactionActivities == null)
                {
                    orderItem.TransactionActivities = new List<TransactionActivity>();
                }
                orderItem.TransactionActivities.Add(new TransactionActivity()
                {
                    Amount = transactionAmount,
                    TransactionCategory = TransactionCategory.PreapprovalSale,
                    ClubId = orderItem.Order.ClubId,
                    OrderId = orderItem.Order_Id,
                    OrderItemId = orderItem.Id,
                    PaymentDetail = paymentDetail,
                    TransactionDate = date.Value,
                    TransactionStatus = transactionStatus,
                    TransactionType = TransactionType.Payment,
                    SeasonId = orderItem.SeasonId,
                    HandleMode = HandleMode.Online,
                    MetaData = new MetaData
                    {
                        DateCreated = DateTime.Now,
                        DateUpdated = DateTime.Now
                    }
                });


            }
            if (installment != null)
            {

                if (installment.TransactionActivities == null)
                {
                    installment.TransactionActivities = new List<TransactionActivity>();
                }

                installment.TransactionActivities.Add(new TransactionActivity()
                {
                    Amount = transactionAmount,
                    TransactionCategory = TransactionCategory.PreapprovalInstallment,
                    ClubId = installment.OrderItem.Order.ClubId,
                    OrderId = installment.OrderItem.Order_Id,
                    OrderItemId = installment.OrderItemId,
                    InstallmentId = installment.Id,
                    PaymentDetail = paymentDetail,
                    TransactionDate = date.Value,
                    TransactionStatus = transactionStatus,
                    TransactionType = TransactionType.Payment,
                    SeasonId = installment.OrderItem.SeasonId
                });

            }
        }
        private enum UpdateOrderBasedOnPaymentStatus
        {
            Duplicate,
            MisMatched,
            NoPayKey,
            None,
            NoElementFound
        }

    }


}