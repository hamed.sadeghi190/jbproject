﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('parentDetailController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'parent-detail-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/People';

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });

        $http.get(baseUrl + '/ParentDetails', { params: { userId: $scope.MC.stateParams.userId, playerId: $scope.MC.stateParams.playerId } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
            });

        $scope.parentDetailDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetParentChild?userId=' + $scope.MC.stateParams.userId,
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.registrantSearchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.parentDetailGridOptions = {
            dataSource: $scope.parentDetailDataSource,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            sortable: true,
            columns: [
                {
                    field: "FirstName",
                    title: "First name",
                },
                {
                    field: "LastName",
                    title: "Last name",
                },
                {
                    field: "Age",
                    title: "Age",
                },
                {
                    field: "EmailAddress",
                    title: "Email",
                }
            ]
        };

    }]);
})();