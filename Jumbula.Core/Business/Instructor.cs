﻿using System.Collections.Generic;

namespace Jumbula.Core.Business
{
    public class Instructor
    {
        public string HeadInstructorLabel { get; set; }

        public string HeadInstructor { get; set; }

        public string AssistantInstructorLabel { get; set; }

        public List<string> AssistantInstructors { get; set; }
    }
}
