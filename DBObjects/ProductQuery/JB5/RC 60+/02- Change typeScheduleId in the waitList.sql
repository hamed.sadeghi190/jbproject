alter table WaitLists drop CONSTRAINT  [FK_dbo.WaitLists_dbo.ProgramSchedules_ScheduleId]

Go

ALTER TABLE WaitLists ALTER COLUMN ScheduleId bigint Not NULL

Go

ALTER TABLE [dbo].[WaitLists]  WITH CHECK ADD  CONSTRAINT [FK_dbo.WaitLists_dbo.ProgramSchedules_ScheduleId] FOREIGN KEY([ScheduleId])
REFERENCES [dbo].[ProgramSchedules] ([Id])