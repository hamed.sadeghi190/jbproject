﻿using Jumbula.Common.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;

namespace SportsClub.Models
{
    public class CalendarBodyViewModel : BaseViewModel<Program, long>, IProgramBodyViewModel
    {
        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
        public bool EnablePriceHidden { get; set; }

        public bool DisableCapacityRestriction { get; set; }

        public PostalAddress Address { get; set; }

        public string LocationName { get; set; }

        public string ClubLogo { get; set; }

        public string ClubDomain { get; set; }

        public string SeasonDomain { get; set; }

        public string Domain { get; set; }

        public string Name { get; set; }

        public Jumbula.Common.Enums.TimeZone  ClubTimeZone { get; set; }

        public List<ProgramBodySchedule> Schedules { get; set; }

        public string AgeRestrictionMessage
        {
            get
            {
                var firstSchedule = Schedules.FirstOrDefault();

                return Ioc.ProgramBusiness.AgeRestrictionMessage(firstSchedule.Restrict.MinAge,
                    firstSchedule.Restrict.MaxAge, firstSchedule.Restrict.ApplyAtProgramStart);
            }
        }


    }
}