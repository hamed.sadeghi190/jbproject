﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('renewalPTAReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });
        $scope.baseUrl = window.location.origin + "/Dashboard/PortalReport/";

        $scope.Model = {};

        $http.get($scope.baseUrl + '/GetAllSchool')
         .success(function (data, status, headers, config) {
             $scope.AllSchools = data;
         });

        $http.get($scope.baseUrl + '/GetRenewalPTARepotInfo').success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.runReport = function () {
            if ($scope.Model.Year == 0 || $scope.Model.SchoolId == 0 || is.undefined($scope.Model.SchoolId)) {
                if ($scope.Model.SchoolId == 0 || is.undefined($scope.Model.SchoolId)) {
                    jbToast.error("Please select school");
                }
                if ($scope.Model.Year == 0) {
                    jbToast.error("Please select year");
                }
            }
            else {
                var a = document.createElement("a");
                document.body.appendChild(a);
                a.href = '/Dashboard/PortalReport/GenerateRenewalPTAReportPdf?schoolId=' + $scope.Model.SchoolId + '&year=' + $scope.Model.Year + '';
                a.download = "SchoolsEnrollment1.Pdf";
                a.click();
            }
           
        };

      

    }]);
})();