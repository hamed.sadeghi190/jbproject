﻿using Jb.Framework.Common;
using Jumbula.Core.Model.Catalog;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ICatalogSettingBusiness
    {
        CatalogSetting Get(int id);

        IQueryable<CatalogItemPriceOption> GetCatalogPriceOptions(int id);

        OperationStatus Create(CatalogSetting entity);

        OperationStatus Create(int clubId);

        IQueryable<CatalogItemOption> GetCatalogOptions(int id);

        IQueryable<CatalogItemPriceOption> GetDefaultCatalogPriceOptions(int id);

        OperationStatus InsertCatalogPriceOptions(int id, List<CatalogItemPriceOption> catalogItempriceOptions);

        OperationStatus InsertCatalogOptions(int id, List<CatalogItemOption> catalogItemOptions);

        IQueryable<CatalogItemOption> GetDefaultCatalogOptions(int id);

        OperationStatus Update(CatalogSetting entity);

        List<SpaceRequirementsViewModel> GetDefaultSpaceRequirements(int clubId);
    }
}
