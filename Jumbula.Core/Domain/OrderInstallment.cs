﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;

namespace Jumbula.Core.Domain
{
    public class OrderInstallment : BaseEntity<long>
    {
        public OrderInstallment()
        {
            TransactionActivities = new HashSet<TransactionActivity>();
        }

        public string Token { get; set; } // Guid
        public DateTime InstallmentDate { get; set; }
        public DateTime? PaidDate { get; set; }

        public DateTime? SuspendDate { get; set; }
        public DateTime? UnSuspendDate { get; set; }
        public int AutoChargeAttemps { get; set; }

        public decimal? PaidAmount { get; set; }
        public decimal Amount { get; set; }
        public OrderStatusCategories Status { get; set; }
        public bool NoticeSent { get; set; }
        public bool EnableReminder { get; set; }

        public DeleteReason? DeleteReason { get; set; }
        public InstallmentType Type { get; set; }
        public long OrderItemId { get; set; }
        public bool IsDeleted { get; set; }
        public virtual OrderItem OrderItem { get; set; }
        public virtual ICollection<TransactionActivity> TransactionActivities { get; set; }
        public virtual ICollection<PendingPreapprovalTransaction> PendingPreapprovalTransactions { get; set; }

        public int? ProgramSchedulePartId { get; set; }

        [ForeignKey("ProgramSchedulePartId")]
        public virtual ProgramSchedulePart ProgramSchedulePart { get; set; }

        [NotMapped]
        public decimal Balance => Amount - (PaidAmount ?? 0);

        [NotMapped]
        public ChargeDiscountCategory? ChargeCategory
        {
            get
            {
                if (Type != InstallmentType.AddOn) return null;
                var charges = OrderItem.OrderChargeDiscounts;

                return charges.Any(c => c.InstallmentId == Id) ?
                    charges.Single(c => c.InstallmentId == Id).Category : (ChargeDiscountCategory?)null;
            }
        }
    }

    public enum InstallmentType : byte
    {
        [Description("Normal")]
        Noraml,
        [Description("Add-on charges")]
        AddOn
    }
}
