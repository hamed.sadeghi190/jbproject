﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Jumbula.Common.Helper
{
    public static class ReflectionHelper
    {
        public static T GetPropertyAttribute<T>(PropertyInfo propertyInfo) where T : Attribute
        {
            object[] attrs = propertyInfo.GetCustomAttributes(true);

            foreach (object attr in attrs)
            {
                if (attr is T propertyAttribute)
                {
                    return propertyAttribute;
                }
            }

            return (T)Activator.CreateInstance(typeof(T));
        }

        public static List<string> GetClassProperties(object obj)
        {
            return obj.GetType().GetProperties().Select(s => s.Name).ToList();
        }
    }
}
