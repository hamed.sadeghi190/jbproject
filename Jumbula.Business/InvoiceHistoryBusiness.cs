﻿

using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class InvoiceHistoryBusiness : IInvoiceHistoryBusiness
    {
        private readonly IRepository<InvoiceHistory> _invoiceHistoryRepository;

        public InvoiceHistoryBusiness(IRepository<InvoiceHistory> invoiceHistoryRepository)
        {
            _invoiceHistoryRepository = invoiceHistoryRepository;
        }

        public OperationStatus Create(InvoiceHistory invoiceHistory)
        {
            return _invoiceHistoryRepository.Create(invoiceHistory);

        }
        public IQueryable<InvoiceHistory> GetList()
        {
            return _invoiceHistoryRepository.GetList();
        }
    }

}
