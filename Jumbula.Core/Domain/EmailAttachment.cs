﻿using Jumbula.Common.Base;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class EmailAttachment : BaseEntity
    {
        public int EmailId { get; set; }

        [ForeignKey("EmailId")]
        public virtual Email Email { get; set; }

        [StringLength(255)]
        public string FileName { get; set; }

        [StringLength(1000)]
        public string FileUrl { get; set; }

        public double FileSize { get; set; }
    }

}
