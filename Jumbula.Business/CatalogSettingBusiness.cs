﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Model.Catalog;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class CatalogSettingBusiness : ICatalogSettingBusiness
    {
        private readonly IRepository<CatalogSetting> _catalogSettingRepository;

        public CatalogSettingBusiness(IRepository<CatalogSetting> catalogSettingRepository)
        {
            _catalogSettingRepository = catalogSettingRepository;
        }

        public  CatalogSetting Get(int id)
        {
            var result = _catalogSettingRepository.GetList().SingleOrDefault(p => p.Id == id);

            if(result == null)
            {
                Create(id);

                result = _catalogSettingRepository.GetList().SingleOrDefault(p => p.Id == id);
            }

            return result;
        }

        public IQueryable<CatalogItemPriceOption> GetCatalogPriceOptions(int id)
        {
            return this.Get(id).CatalogPriceOptions.AsQueryable();
        }

        public OperationStatus Create(int clubId)
        {
            var catalogSetting = new CatalogSetting { Id = clubId, CatalogSearchTags = new CatalogSearchTags() };

            var result = _catalogSettingRepository.Create(catalogSetting);

            return result;
        }

        public  OperationStatus Create(CatalogSetting entity)
        {
            return _catalogSettingRepository.Create(entity);
        }

        public IQueryable<CatalogItemOption> GetCatalogOptions(int id)
        {
            return this.Get(id).CatalogOptions.AsQueryable();
        }

        public IQueryable<CatalogItemPriceOption> GetDefaultCatalogPriceOptions(int id)
        {
            var result = Enumerable.Empty<CatalogItemPriceOption>().AsQueryable();

            var clubCatalogPriceOptions = this.GetCatalogPriceOptions(id);

            if (clubCatalogPriceOptions == null || (clubCatalogPriceOptions != null && !clubCatalogPriceOptions.Any()))
            {
                InsertDefaultCatalogPriceOptions(id);

                result = this.GetCatalogPriceOptions(id);
            }
            else
            {
                result = clubCatalogPriceOptions;
            }

            return result;
        }

        public IQueryable<CatalogItemOption> GetDefaultCatalogOptions(int id)
        {
            var result = Enumerable.Empty<CatalogItemOption>().AsQueryable();

            var clubCatalogOptions = this.GetCatalogOptions(id);

            if (clubCatalogOptions == null || (clubCatalogOptions != null && !clubCatalogOptions.Any()))
            {
                InsertDefaultCatalogOptions(id);

                result = this.GetCatalogOptions(id);
            }
            else
            {
                result = clubCatalogOptions;
            }

            return result;
        }

        public OperationStatus InsertCatalogPriceOptions(int id, List<CatalogItemPriceOption> catalogItempriceOptions)
        {
            var catalogSettings = this.Get(id);

            foreach (var item in catalogItempriceOptions)
            {
                catalogSettings.CatalogPriceOptions.Add(item);
            }

            var result = this.Update(catalogSettings);

            return result;
        }

        public OperationStatus InsertCatalogOptions(int id, List<CatalogItemOption> catalogItemOptions)
        {
            var catalogSettings = this.Get(id);

            foreach (var item in catalogItemOptions)
            {
                catalogSettings.CatalogOptions.Add(item);
            }

            var result = this.Update(catalogSettings);

            return result;
        }

        private OperationStatus InsertDefaultCatalogPriceOptions(int id)
        {
            var priceOptionGroups = new List<string>()
            {
                "45 minutes",
                "50 minutes",
                "60 minutes",
                "75 minutes",
                "90 minutes",
                "120 minutes",
            };

            var priceOptions = new List<CatalogItemPriceOption>()
            {
                  new CatalogItemPriceOption { Amount = 0,  Title = "6 - week" },
                  new CatalogItemPriceOption { Amount = 0,  Title = "7 - week" },
                  new CatalogItemPriceOption { Amount = 0,  Title = "8 - week" },
                  new CatalogItemPriceOption { Amount = 0,  Title = "9 - week" },
                  new CatalogItemPriceOption { Amount = 0,  Title = "10 - week" }
            };

            var catalogSettings = this.Get(id);

            catalogSettings.CatalogPriceOptions = new List<CatalogItemPriceOption>();

            foreach (var priceGroup in priceOptionGroups)
            {
                foreach (var priceOption in priceOptions)
                {
                    catalogSettings.CatalogPriceOptions.Add(new CatalogItemPriceOption { Title = priceOption.Title, Amount = 0, DurationTitle = priceGroup });
                }
            }

            return this.Update(catalogSettings);
        }

        private OperationStatus InsertDefaultCatalogOptions(int id)
        {

            var catalogOptions = new List<CatalogItemOption>()
            {
               new CatalogItemOption{  Title = "K - 2", Type = CatalogOptionType.GradeGrouping},
               new CatalogItemOption{  Title = "3 - 5", Type = CatalogOptionType.GradeGrouping},
               new CatalogItemOption{  Title = "6 - 8", Type = CatalogOptionType.GradeGrouping},
               new CatalogItemOption{  Title = "K - 6", Type = CatalogOptionType.GradeGrouping},

               new CatalogItemOption{  Title = "Classroom (carpeted)", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Classroom (non-carpeted)", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Music room", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Art room", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Story room(tiered floors)", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Stage", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Library or media center", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Teachers lounge", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Cafeteria or multi-purpose room", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Gym or auxiliary gym", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Hallway", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},
               new CatalogItemOption{  Title = "Atrium", Type = CatalogOptionType.SpaceRequrement, Group="Indoor"},

               new CatalogItemOption{  Title = "Field", Type = CatalogOptionType.SpaceRequrement, Group="Outdoor"},
               new CatalogItemOption{  Title = "Black top", Type = CatalogOptionType.SpaceRequrement, Group="Outdoor"},
               new CatalogItemOption{  Title = "Track", Type = CatalogOptionType.SpaceRequrement, Group="Outdoor"},
               new CatalogItemOption{  Title = "Tennis court", Type = CatalogOptionType.SpaceRequrement, Group="Outdoor"},
               new CatalogItemOption{  Title = "Baseball diamond", Type = CatalogOptionType.SpaceRequrement, Group="Outdoor"},
               new CatalogItemOption{  Title = "Court yard", Type = CatalogOptionType.SpaceRequrement, Group="Outdoor"},
               new CatalogItemOption{  Title = "Parking lot", Type = CatalogOptionType.SpaceRequrement, Group="Outdoor"},
            };

            var catalogSettings = this.Get(id);

            catalogSettings.CatalogOptions = new List<CatalogItemOption>();

            foreach (var option in catalogOptions)
            {
                catalogSettings.CatalogOptions.Add(option);
            }

            var result = this.Update(catalogSettings);

            return result;
        }

        public OperationStatus Update(CatalogSetting entity)
        {
            return _catalogSettingRepository.Update(entity);
        }

        public List<SpaceRequirementsViewModel> GetDefaultSpaceRequirements(int clubId)
        {
            return GetDefaultCatalogOptions(clubId).Where(o => o.Type == CatalogOptionType.SpaceRequrement).Select(c =>
                    new SpaceRequirementsViewModel
                    {
                        Id = c.Id,
                        IsDefault = true,
                        Title = c.Title,
                        Type = c.Group.Equals("Indoor") ? SpaceRequirementType.Indoor : SpaceRequirementType.Outdoor
                    })
                .ToList();
        }
    }
}
