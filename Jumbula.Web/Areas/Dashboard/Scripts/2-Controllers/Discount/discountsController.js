﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('discountsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'discounts-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });
        $scope.MC.stateParams = $stateParams;

        $scope.baseUrl = window.location.origin + "/Dashboard/Discount";
        $scope.mainDiscountGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + '/GetAllDiscounts?seasonDomain=' + $scope.MC.stateParams.seasonDomain,
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: false,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.mainGridOptions = {
            dataSource: $scope.mainDiscountGridDataSource,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            columns: [{
                field: "Name",
                title: "Name",
                template: "<strong class='grid-row-title'>#= Name #</strong>"
            }, {
                field: "CategoryDescription",
                title: "Type",
                width: "200px"
            }, {
                field: "Description",
                width: "400px"
            }, {
                title: "Actions",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                        <li> ... \
                            <ul>\
                                #if(Category == 'MultiSchedule'){#\
                                    <li ui-sref='MultiScheduleDiscountManage({seasonDomain:MC.stateParams.seasonDomain, discountCategory:\"MultiSchedule\",discountId: #=Id#})'>\
                                          <span class='k-link jbi-pencil'>Edit</span>\
                                    </li>\
                                  #}\
                                   else if(Category == 'MultiProgram' || Category == 'MultiPerson' || Category == 'CustomDiscount'){#\
                                    <li ui-sref='DiscountManage({seasonDomain:MC.stateParams.seasonDomain, discountCategory:\"MultiProgram\",discountId: #=Id#})'>\
                                          <span class='k-link jbi-pencil'>Edit</span>\
                                    </li>\
                                  #}\
                                   else{#\
                                    <li ui-sref='GeneralDiscountManage({seasonDomain:MC.stateParams.seasonDomain,discountId: #=Id#})'>\
                                          <span class='k-link jbi-pencil'>Edit</span>\
                                    </li>\
                                       #}#\
                                    <li ng-click='DeleteDiscount( #=Id# )'>\
                                          <span class='k-link jbi-remove'>Delete</span>\
                                    </li>\
                            </ul>\
                        </li>\
                    </ul>"
            }],
        };

        $scope.DeleteDiscount = function (id) {
            jbConfirmation
                .yes(function () {
                    $http.post($scope.baseUrl + '/DeleteDiscount', { discountId: id }).success(function (data, status, headers, config) {
                        if (data.Status == true) {

                            $scope.mainGridOptions.dataSource.read();

                        }
                    });
                })
                .confirm();
        };

        $scope.discountSearch = function () {
            $scope.mainGridOptions.dataSource.read();
        }
    }]);
})();