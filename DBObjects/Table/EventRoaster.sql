--Create time 2014/09/03 --- By MN----


If Object_ID('[dbo].[EventRoasters]') Is Null
BEGIN
CREATE TABLE [dbo].[EventRoasters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](1000) NULL,	
	[Events] [nvarchar](max) NOT NULL,
	[Club_Id] [int] NULL,
	[LastUpdateDate] [datetime] NOT NULL,
	[ClubDomain] [nvarchar](128) NOT NULL,
	[SeasonId] [int] NULL,
	[JbForm_Id] [int] NULL)

END
GO

--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.EventRoasters')
BEGIN
	ALTER TABLE [dbo].[EventRoasters] ADD CONSTRAINT [PK_dbo.EventRoasters] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

--<< ADD CLOLUMNS >>--
IF NOT EXISTS(select 1 from sys.columns where OBJECT_ID=OBJECT_ID('[dbo].[EventRoasters]') AND name='SeasonId')
BEGIN	
	ALTER TABLE [dbo].[EventRoasters]
	ADD [SeasonId] bigint null
END

--<< FOREIGNKEYS DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'FK_dbo.EventRoasters_dbo.Clubs_Club_Id')
BEGIN
	ALTER TABLE [dbo].[EventRoasters]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EventRoasters_dbo.Clubs_Club_Id] FOREIGN KEY([Club_Id])
	REFERENCES [dbo].[Clubs] ([Id])
END
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name='FK_dbo.EventRoasters_dbo.Seasons_SeasonId')
BEGIN
	ALTER TABLE [dbo].[EventRoasters]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EventRoasters_dbo.Seasons_SeasonId] FOREIGN KEY([SeasonId])
	REFERENCES [dbo].[Seasons] ([Id])
END
GO

--<< DROP COLUMNS >>--
--Delete Fields Column by MN----
IF  EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE OBJECT_ID=OBJECT_ID('dbo.EventRoasters') AND
				[name] = 'Fields')
BEGIN
	ALTER TABLE dbo.EventRoasters DROP COLUMN [Fields]
END
GO
IF  EXISTS (SELECT 1 FROM SYS.COLUMNS WHERE OBJECT_ID=OBJECT_ID('dbo.EventRoasters') AND
				[name] = 'ReportTemplate')
BEGIN
	ALTER TABLE dbo.EventRoasters DROP COLUMN [ReportTemplate]
END
GO


---<< FOREIGN KEYS JBFROM>>--
declare @TableName nvarchar(50)='EventRoasters' --without dbo.
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'FK_'+@TableName+'_JbForms')
BEGIN
	declare @command nvarchar(500)='ALTER TABLE [dbo].['+@TableName+'] Add JbForm_Id int null';
	exec(@command);
	print 'JbForm_Id column created successfully ';

	set @command='ALTER TABLE [dbo].['+@TableName+']  WITH CHECK ADD  CONSTRAINT [FK_'+@TableName+'_JbForms] FOREIGN KEY([JbForm_Id])
	REFERENCES [dbo].[JbForms] ([Id])
	ALTER TABLE [dbo].['+@TableName+'] CHECK CONSTRAINT [FK_'+@TableName+'_JbForms]';
	exec(@command);
	print 'FK created successfully ';
	END
	ELSE
	BEGIN
		print 'Fk has already existed';
END


GO