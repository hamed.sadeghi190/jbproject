﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class MyJumbulaClubSettingViewModel
    {
       public LocalPasswordModel ChangePassword{get;set;}
       public string ClubName { get; set; }
       public string Domain { get; set; }
       public string LastSignedIn { get; set; }


       public MyJumbulaClubSettingViewModel()
       {
           LastSignedIn = "07/09/2013 at 09:02";
           ChangePassword = new LocalPasswordModel();
       }

       public MyJumbulaClubSettingViewModel(Club club)
       {
           ChangePassword = new LocalPasswordModel();
           ClubName = club.Name;
           Domain = club.Domain;
           LastSignedIn = "07/09/2013 at 09:02";
       }
    }
}