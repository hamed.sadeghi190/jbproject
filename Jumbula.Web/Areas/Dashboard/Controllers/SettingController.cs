﻿using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Core.Model.ClubSetting;
using Jumbula.Core.Model.Email;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Areas.Dashboard.Models.Settings;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class SettingController : DashboardBaseController
    {

        #region Fields
        IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly IProgramBusiness _programBusiness;
        private readonly IClubSettingBusiness _clubSettingBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly IEmailBusiness _emailBusiness;
        #endregion

        #region Constractors
        public SettingController(IProgramBusiness programBusiness, IClubBusiness clubBusiness,
            IApplicationUserManager<JbUser, int> applicationUserManager,
            IApplicationSignInManager applicationSignInManager,
            IClubSettingBusiness clubSettingBusiness, IEmailBusiness emailBusiness)
        {
            _programBusiness = programBusiness;
            _applicationUserManager = applicationUserManager;
            _clubSettingBusiness = clubSettingBusiness;
            _emailBusiness = emailBusiness;
            _clubBusiness = clubBusiness;
        }
        #endregion



        // GET: Dashboard/Setting
        public virtual ActionResult Organization()
        {
            return View();
        }
        public virtual ActionResult EmailsSettings()
        {
            return View();
        }

        public virtual ActionResult SchoolProgramInfo()
        {
            return View();
        }
        public virtual ActionResult SchoolInfo()
        {
            return View();
        }
        public virtual ActionResult SchoolRegistrationInfo()
        {
            return View();
        }
        public virtual ActionResult CheckOut()
        {
            return View();
        }
        public virtual ActionResult SchoolOnSiteInfo()
        {
            return View();
        }

        public virtual ActionResult ToursJbHomeSite()
        {
            return View();
        }
        public virtual ActionResult TourJbHomeSite()
        {
            return View();
        }

        public virtual ActionResult Payment()
        {
            return View();
        }
        public virtual ActionResult PartnerSetting()
        {
            return View();
        }
        public virtual ActionResult Appearance()
        {
            return View();
        }
        public virtual ActionResult Holidays()
        {
            return View();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Settings_Appearance_View)]
        public virtual JsonNetResult EditClubAppearanceSettings()
        {
            var model = new AppearanceSettingViewModel();

            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            model.IsClubPartner = club.ClubType.EnumType == ClubTypesEnum.Partner ? true : false;


            if (club.Setting != null && club.Setting != null)
            {
                var appearanceSettings = club.Setting.AppearanceSetting;

                if (appearanceSettings == null)
                {
                    appearanceSettings = new ClubAppearanceSetting();
                }

                model.HideProgramDates = appearanceSettings.HideProgramDates;
                model.HidePhoneNumber = appearanceSettings.HidePhoneNumber;
                model.ContactBoxBackColor = !string.IsNullOrEmpty(appearanceSettings.ContactBoxBackColor) ? appearanceSettings.ContactBoxBackColor : Constants.DefaultContactBoxBackColor;
                model.TestimonialsBoxBackColor = !string.IsNullOrEmpty(appearanceSettings.TestimonialsBoxBackColor) ? appearanceSettings.TestimonialsBoxBackColor : Constants.DefaultTestimonialsBoxBackColor;
                model.TestimonialsBoxColor = !string.IsNullOrEmpty(appearanceSettings.TestimonialsBoxColor) ? appearanceSettings.TestimonialsBoxColor : Constants.DefaultTestimonialsBoxColor;
                model.TestimonialsTitle = !string.IsNullOrEmpty(appearanceSettings.TestimonialsTitle) ? appearanceSettings.TestimonialsTitle : Constants.DefaultTestimonialsTitle;

                model.ERButtonBackColor = !string.IsNullOrEmpty(appearanceSettings.ERButtonBackColor) ? appearanceSettings.ERButtonBackColor : Constants.DefaultERButtonBackColor;
                model.ERButtonTextColor = !string.IsNullOrEmpty(appearanceSettings.ERButtonTextColor) ? appearanceSettings.ERButtonTextColor : Constants.DefaultERButtonTextColor;
                model.ERButtonTextLabel = !string.IsNullOrEmpty(appearanceSettings.ERButtonTextLabel) ? appearanceSettings.ERButtonTextLabel : Constants.DefaultERButtonTextLabel;
                model.PriceBackColor = !string.IsNullOrEmpty(appearanceSettings.PriceBackColor) ? appearanceSettings.PriceBackColor : Constants.DefaultPriceBackColor;
                model.PriceTextColor = !string.IsNullOrEmpty(appearanceSettings.PriceTextColor) ? appearanceSettings.PriceTextColor : Constants.DefualtPriceTextColor;
                model.TuitionLabelTextColor = !string.IsNullOrEmpty(appearanceSettings.TuitionLabelTextColor) ? appearanceSettings.TuitionLabelTextColor : Constants.DefaultTuitionLabeltextColor;
                model.HideProgramDatesInCart = appearanceSettings.HideProgramDatesInCart;
                model.ShowProviderNameInClassPage = appearanceSettings.ShowProviderNameInClassPage;
                model.HideGoogleMapInClassPage = appearanceSettings.HideGoogleMapInClassPage;
            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Settings_Appearance_Save)]
        public virtual ActionResult EditClubAppearanceSettings(AppearanceSettingViewModel model)
        {
            if (ModelState.IsValid)
            {
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                if (club.Setting == null)
                {
                    club.Setting = new ClubSetting();
                }

                var appearanceSettings = club.Setting.AppearanceSetting;

                if (appearanceSettings == null)
                {
                    appearanceSettings = new ClubAppearanceSetting();
                }

                appearanceSettings.HideProgramDates = model.HideProgramDates;
                appearanceSettings.HidePhoneNumber = model.HidePhoneNumber;
                appearanceSettings.ContactBoxBackColor = model.ContactBoxBackColor;
                appearanceSettings.TestimonialsBoxBackColor = model.TestimonialsBoxBackColor;
                appearanceSettings.TestimonialsBoxColor = model.TestimonialsBoxColor;
                appearanceSettings.TestimonialsTitle = model.TestimonialsTitle;
                appearanceSettings.ERButtonBackColor = model.ERButtonBackColor;
                appearanceSettings.ERButtonTextColor = model.ERButtonTextColor;
                appearanceSettings.ERButtonTextLabel = model.ERButtonTextLabel;
                appearanceSettings.PriceBackColor = model.PriceBackColor;
                appearanceSettings.PriceTextColor = model.PriceTextColor;
                appearanceSettings.TuitionLabelTextColor = model.TuitionLabelTextColor;
                appearanceSettings.HideProgramDatesInCart = model.HideProgramDatesInCart;
                appearanceSettings.ShowProviderNameInClassPage = model.ShowProviderNameInClassPage;
                appearanceSettings.HideGoogleMapInClassPage = model.HideGoogleMapInClassPage;

                club.Setting.AppearanceSetting = appearanceSettings;

                club.IsSettingChanged = true;

                var res = Ioc.ClubBusiness.Update(club);

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.SchoolSetting_RegistrationInfo_View)]
        public virtual JsonNetResult EditSchoolSettingsRegistrationInfo()
        {
            var model = new SchoolSettingRegistrationInfoViewModel();

            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            if (club.Setting != null && club.Setting != null)
            {
                var registrationInfo = ((SchoolSetting)club.Setting).RegistrationInfo;
                if (registrationInfo != null)
                {
                    model.Colors = registrationInfo.Colors;
                    model.PTARegFee = registrationInfo.PTARegFee;
                    model.RegistrationEndTime = registrationInfo.RegistrationEndTime;
                    model.RegistrationStartTime = registrationInfo.RegistrationStartTime;
                    model.ScholarshipCodes = registrationInfo.ScholarshipCodes;
                    model.ScholarshipRequirement = registrationInfo.ScholarshipRequirement;
                    model.SelectedLateRegistrationOption = registrationInfo.LateRegistrationOption;
                    model.SelectedReconciliationDetail = registrationInfo.ReconciliationDetail;
                    model.SpecialRosterRequirements = registrationInfo.SpecialRosterRequirements;
                    model.Note = registrationInfo.Note;
                }
            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.SchoolSetting_RegistrationInfo_Save)]
        public virtual ActionResult EditSchoolSettingsRegistrationInfo(SchoolSettingRegistrationInfoViewModel model)
        {

            if (ModelState.IsValid)
            {

                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                if (club.Setting == null)
                {
                    club.Setting = new SchoolSetting();
                }
                var registrationInfo = ((SchoolSetting)club.Setting).RegistrationInfo;
                if (registrationInfo == null)
                {
                    registrationInfo = new SchoolSettingRegistrationInfo();
                }
                registrationInfo.Colors = model.Colors;
                registrationInfo.PTARegFee = model.PTARegFee;
                registrationInfo.RegistrationEndTime = model.RegistrationEndTime;
                registrationInfo.RegistrationStartTime = model.RegistrationStartTime;
                registrationInfo.ScholarshipCodes = model.ScholarshipCodes;
                registrationInfo.ScholarshipRequirement = model.ScholarshipRequirement;
                registrationInfo.LateRegistrationOption = model.SelectedLateRegistrationOption;
                registrationInfo.ReconciliationDetail = model.SelectedReconciliationDetail;
                registrationInfo.SpecialRosterRequirements = model.SpecialRosterRequirements;
                registrationInfo.Note = model.Note;

                ((SchoolSetting)club.Setting).RegistrationInfo = registrationInfo;

                club.IsSettingChanged = true;
                var res = Ioc.ClubBusiness.Update(club);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.SchoolSetting_OnSiteInfo_View)]
        public virtual JsonNetResult EditSchoolSettingsOnSiteInfo()
        {
            var model = new SchoolSettingOnSiteInfoViewModel();
            // model.ListOfOnSiteCancellationContact = Utilities.GetListOfClubStaffs(ActiveClub.Domain);
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            if (club.Setting != null && club.Setting != null)
            {
                var onSiteInfo = club.Setting.OnSiteInfo;
                if (onSiteInfo != null)
                {
                    model.OnSiteCancellationContact = onSiteInfo.OnSiteCancellationContactInfo;
                    model.InstructorArrivalTimeAM = onSiteInfo.InstructorArrivalTimeAM;
                    model.InstructorArrivalTimePM = onSiteInfo.InstructorArrivalTimePM;
                    model.LatePickUpPolicy = onSiteInfo.LatePickUpPolicy;
                    model.ParkingForProviders = onSiteInfo.ParkingForProviders;
                    model.PermitResponsibility = onSiteInfo.PermitResponsibility;
                    model.SchoolDismissalTime = onSiteInfo.SchoolDismissalTime;
                    model.SchoolStartTime = onSiteInfo.SchoolStartTime;
                    model.SelectedDismissalOption = onSiteInfo.DismissalOption;
                    model.SelectedEarlyReleaseDay = onSiteInfo.EarlyReleaseDay;
                    model.SelectedEnrichmentDismissalOption = onSiteInfo.EnrichmentDismissalOption;
                    model.SpaceRequirements = Ioc.CatalogSettingBusiness.GetDefaultSpaceRequirements(ActiveClub.Id);
                    model.TransitionInstructions = onSiteInfo.TransitionInstructions;
                    model.AttendanceProcedure = onSiteInfo.AttendanceProcedure;
                    model.CheckInProceduresForInstructors = onSiteInfo.CheckInProceduresForInstructors;
                    model.EarlyReleaseTime = onSiteInfo.EarlyReleaseTime;
                    model.EnrichmentDismissalInstructions = onSiteInfo.EnrichmentDismissalInstructions;
                    model.EnrichmentStartTimeAM = onSiteInfo.EnrichmentStartTimeAM;
                    model.EnrichmentStartTimePM = onSiteInfo.EnrichmentStartTimePM;
                    model.Note = onSiteInfo.Note;
                    model.Title = onSiteInfo.Title;
                    if (onSiteInfo.AvailableSpaces != null && onSiteInfo.AvailableSpaces.Any())
                    {
                        foreach (var item in onSiteInfo.AvailableSpaces)
                        {
                            if (model.SpaceRequirements.Any(c => c.Id == item.Id))
                            {
                                model.SpaceRequirements.FirstOrDefault(c => c.Id == item.Id).IsChecked = true;
                            }
                        }
                    }
                }

            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.SchoolSetting_OnSiteInfo_Save)]
        public virtual ActionResult EditSchoolSettingsOnSiteInfo(SchoolSettingOnSiteInfoViewModel model)
        {

            if (ModelState.IsValid)
            {

                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                if (club.Setting == null)
                {
                    club.Setting = new ClubSetting();
                }
                var onSiteInfo = club.Setting.OnSiteInfo;
                if (onSiteInfo == null)
                {
                    onSiteInfo = new SchoolSettingOnSiteInfo();
                }
                onSiteInfo.InstructorArrivalTimeAM = model.InstructorArrivalTimeAM;
                onSiteInfo.InstructorArrivalTimePM = model.InstructorArrivalTimePM;
                onSiteInfo.LatePickUpPolicy = model.LatePickUpPolicy;
                onSiteInfo.ParkingForProviders = model.ParkingForProviders;
                onSiteInfo.PermitResponsibility = model.PermitResponsibility;
                onSiteInfo.SchoolDismissalTime = model.SchoolDismissalTime;
                onSiteInfo.SchoolStartTime = model.SchoolStartTime;
                onSiteInfo.DismissalOption = model.SelectedDismissalOption;
                onSiteInfo.EarlyReleaseDay = model.SelectedEarlyReleaseDay;
                onSiteInfo.EnrichmentDismissalOption = model.SelectedEnrichmentDismissalOption;
                onSiteInfo.OnSiteCancellationContactInfo = model.OnSiteCancellationContact;
                onSiteInfo.TransitionInstructions = model.TransitionInstructions;
                onSiteInfo.AttendanceProcedure = model.AttendanceProcedure;
                onSiteInfo.CheckInProceduresForInstructors = model.CheckInProceduresForInstructors;
                onSiteInfo.EarlyReleaseTime = model.EarlyReleaseTime;
                onSiteInfo.EnrichmentDismissalInstructions = model.EnrichmentDismissalInstructions;
                onSiteInfo.EnrichmentStartTimeAM = model.EnrichmentStartTimeAM;
                onSiteInfo.EnrichmentStartTimePM = model.EnrichmentStartTimePM;
                onSiteInfo.Note = model.Note;
                onSiteInfo.Title = model.Title;

                club.Setting.OnSiteInfo = onSiteInfo;
                if (onSiteInfo.AvailableSpaces != null)
                {
                    onSiteInfo.AvailableSpaces.Clear();
                }
                if (model.SpaceRequirements.Any(c => c.IsChecked))
                {
                    onSiteInfo.AvailableSpaces = new List<SpaceRequirement>();
                    foreach (var item in model.SpaceRequirements.Where(c => c.IsChecked))
                    {
                        onSiteInfo.AvailableSpaces.Add(new SpaceRequirement()
                        {
                            Id = item.Id,
                            Title = item.Title
                        });
                    }
                }
                club.IsSettingChanged = true;
                var res = Ioc.ClubBusiness.Update(club);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.SchoolSetting_ProgramInfo_View)]
        public virtual JsonNetResult EditSchoolSettingsProgramInfo()
        {
            var model = new SchoolSettingProgramInfoViewModel();
            model.ListOfCoordinator = Ioc.ClubBusiness.GetListOfClubStaffs(ActiveClub.Domain);
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            model.ContactEmail = club.ContactPersons != null ? club.ContactPersons.First().Email : string.Empty;

            var programInfo = ((SchoolSetting)club.Setting)?.ProgramInfo;
            if (programInfo != null)
            {
                model.SelectedCoordinator = programInfo.SelectedCoordinator;
                model.NumberOfSeasons = programInfo.NumberOfSeasons;
                model.NumberOfClassesPerSeason = programInfo.NumberOfClassesPerSeason;
                model.MaxClassesPerDay = programInfo.MaxClassesPerDay;
                model.DaysClassesMeet = programInfo.DaysClassesMeet;
                model.BeforeSchool = programInfo.BeforeSchool;
                model.SelectedSchoolSize = programInfo.NumberOfStudents;
                model.SelectedRegistrationSize = programInfo.NumberOfRegistrationsPerSeason;
                model.OpenToNonStudents = programInfo.OpenToNonStudents;
                model.TranslationNeeded = programInfo.TranslationNeeded;
                model.FolderDay = programInfo.FolderDay;
                model.Note = programInfo.Note;
            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.SchoolSetting_ProgramInfo_Save)]
        public virtual ActionResult EditSchoolSettingsProgramInfo(SchoolSettingProgramInfoViewModel model)
        {
            if (ModelState.IsValid)
            {

                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                if (club.Setting == null)
                {
                    club.Setting = new SchoolSetting();
                }
                var programInfo = ((SchoolSetting)club.Setting).ProgramInfo;
                if (programInfo == null)
                {
                    programInfo = new SchoolSettingProgramInfo();
                }
                programInfo.SelectedCoordinator = model.SelectedCoordinator;
                programInfo.NumberOfSeasons = model.NumberOfSeasons;
                programInfo.NumberOfClassesPerSeason = model.NumberOfClassesPerSeason;
                programInfo.MaxClassesPerDay = model.MaxClassesPerDay;
                programInfo.DaysClassesMeet = model.DaysClassesMeet;
                programInfo.BeforeSchool = model.BeforeSchool;
                programInfo.NumberOfStudents = model.SelectedSchoolSize;
                programInfo.NumberOfRegistrationsPerSeason = model.SelectedRegistrationSize;
                programInfo.OpenToNonStudents = model.OpenToNonStudents;
                programInfo.TranslationNeeded = model.TranslationNeeded;
                programInfo.FolderDay = model.FolderDay;
                programInfo.Note = model.Note;
                ((SchoolSetting)club.Setting).ProgramInfo = programInfo;

                club.IsSettingChanged = true;
                var res = Ioc.ClubBusiness.Update(club);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.SchoolSetting_Info_View)]
        public virtual JsonNetResult EditSchoolSettingsInfoSetting()
        {
            var model = new SchoolSettingInfoViewModel();

            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            model.SchoolName = club.Name;
            model.Domain = club.Domain;
            model.Phone = club.ContactPersons != null ? club.ContactPersons.First().Phone : string.Empty;
            model.WebSite = club.Site;
            model.Address = club.ClubLocations != null ? club.ClubLocations.First().PostalAddress.Address : string.Empty;
            model.County = club.ClubLocations != null ? club.ClubLocations.First().PostalAddress.County : string.Empty;
            model.SelectedSubType = club.TypeId;

            if (club.Setting != null && club.Setting != null)
            {
                var schoolInfo = ((SchoolSetting)club.Setting).SchoolInfo;
                if (schoolInfo != null)
                {
                    model.PTASite = schoolInfo.PTASite;
                    model.SelectedMinGrade = schoolInfo.MinGrade;
                    model.SelectedMaxGrade = schoolInfo.MaxGrade;
                    model.Note = schoolInfo.Note;
                }

            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.SchoolSetting_Info_Save)]
        public virtual ActionResult EditSchoolSettingsInfoSetting(SchoolSettingInfoViewModel model)
        {

            if (ModelState.IsValid)
            {

                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
                club.TypeId = model.SelectedSubType;
                if (club.Setting == null)
                {
                    club.Setting = new SchoolSetting();
                }
                var schoolInfo = ((SchoolSetting)club.Setting).SchoolInfo;
                if (schoolInfo == null)
                {
                    schoolInfo = new SchoolSettingSchoolInfo();
                }
                schoolInfo.PTASite = model.PTASite;
                schoolInfo.Note = model.Note;
                var selectedgrade = SchoolGradeType.S1;
                if (Enum.TryParse<SchoolGradeType>(model.SelectedMinGrade, out selectedgrade))
                {
                    schoolInfo.MinGrade = model.SelectedMinGrade;
                }
                if (Enum.TryParse<SchoolGradeType>(model.SelectedMaxGrade, out selectedgrade))
                {
                    schoolInfo.MaxGrade = model.SelectedMaxGrade;
                }

            ((SchoolSetting)club.Setting).SchoolInfo = schoolInfo;

                club.IsSettingChanged = true;
                var res = Ioc.ClubBusiness.Update(club);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }



        [HttpGet]
        [JbAuthorize(JbAction.Settings_Checkout_View)]
        public virtual JsonNetResult EditCheckOutSetting()
        {
            var model = new CheckoutViewModel();

            var surchargeType = ChargeDiscountCategory.Surcharge;

            if (this.GetCurrentUserRole() == RoleCategory.Partner)
            {
                surchargeType = ChargeDiscountCategory.PartnerSurcharge;
            }

            var registrationFeeType = ChargeDiscountCategory.ApplicationFee;

            if (this.GetCurrentUserRole() == RoleCategory.Partner)
            {
                registrationFeeType = ChargeDiscountCategory.PartnerApplicationFee;
            }

            var confirmationUrlTypes = new List<SelectKeyValue<string>>();

            foreach (var item in model.ConfirmationUrlTypes)
            {
                if (item.Value != "0")
                {
                    if (item.Value == "1")
                    {
                        item.Text = "Jumbula standard page";
                    }

                    confirmationUrlTypes.Add(item);
                }
            }

            model.ConfirmationUrlTypes = confirmationUrlTypes;

            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            model.SiteUrl = club.Site;

            model.IsClubPartner = club.ClubType.EnumType == ClubTypesEnum.Partner ? true : false;

            model.ProgramsList = _programBusiness.GetClubProgramItems(club.Id);

            if (club.Setting != null)
            {
                model.HasNotPartner = !club.IsProvider && !club.IsSchool;
                model.IsDiscountApplyOnOriginPrice = !(club.Setting.IsDiscountApplyOnOriginPrice);
                model.BrowseMoreUrlSelectedType = club.Setting.BrowseMoreRedirectUrl != null ? ((int)club.Setting.BrowseMoreRedirectUrl.RedirectUrlType).ToString() : "0";
                model.BrowseMoreUrl = club.Setting.BrowseMoreRedirectUrl != null ? club.Setting.BrowseMoreRedirectUrl.RedirectUrl : string.Empty;
                model.BrowseMoreText = club.Setting.BrowseMoreText;
                model.AnotherRegisterButtonText = club.Setting.AnotherRegisterButtonText;

                model.HiddenPricePrograms = club.Setting.ClubProgram.HiddenPricePrograms;
                model.EnablePriceHidden = club.Setting.ClubProgram.EnablePriceHidden;
                model.HideAllProgramsPrice = club.Setting.ClubProgram.HiddenPricePrograms?.Count > 0 ? false : true;

                model.LogOutUrlSelectedType = club.Setting.LogOutRedirectUrl != null ? ((int)club.Setting.LogOutRedirectUrl.RedirectUrlType).ToString() : "0";
                model.LogOutUrl = club.Setting.LogOutRedirectUrl != null ? club.Setting.LogOutRedirectUrl.RedirectUrl : string.Empty;

                model.ConfirmationUrl = club.Setting.ConfirmationRedirectUrl != null ? club.Setting.ConfirmationRedirectUrl.RedirectUrl : string.Empty;
                model.ConfirmationUrlSelectedType = club.Setting.ConfirmationRedirectUrl != null ? ((int)club.Setting.ConfirmationRedirectUrl.RedirectUrlType).ToString() : "1";

                model.RegistrationLogoUrlSelectedType = club.Setting.RegistrationPageLogoRedirectUrl != null ? ((int)club.Setting.RegistrationPageLogoRedirectUrl.RedirectUrlType).ToString() : "0";
                model.RegistrationLogoUrl = club.Setting.RegistrationPageLogoRedirectUrl != null ? club.Setting.RegistrationPageLogoRedirectUrl.RedirectUrl : string.Empty;
                model.IsNewFamily = club.Setting.IsNewFamily;
                model.NewFamilyNotification = club.Setting.NewFamilyNotification;
                model.ShowCombinedPriceOnly = club.Setting.ShowCombinedPriceOnly;
                model.HideCouponInCart = club.Setting.HideCouponInCart;
                model.HideEnrollmentCapacityInClassPage = club.Setting.HideEnrollmentCapacityInClassPage;
                model.HideProgramDatesInCart = club.Setting.AppearanceSetting.HideProgramDatesInCart;
            }

            model.EnableDonation = club.Client.EnableDonation;
            model.EnableCartDonation = club.Client.EnableCartDonation;
            model.DonationMessage = !string.IsNullOrEmpty(club.Client.DonationMessage) ? club.Client.DonationMessage : Constants.W_DonationDescription;
            model.DonationDescription = club.Client.DonationDescription;
            model.PlayerCanEditOrder = club.Client.CanPlayerEditOrder;
            model.HasChessTourney = club.Client.HasChessTourney;

            if (string.IsNullOrEmpty(model.BrowseMoreText))
            {
                model.BrowseMoreText = "select classes for a new participant";
            }

            if (string.IsNullOrEmpty(model.AnotherRegisterButtonText))
            {
                model.AnotherRegisterButtonText = "Register for same program";
            }

            if (club.Client.ConvenienceFee > 0)
            {
                model.EnableConvenienceFee = true;
                model.ConvenienceFee = club.Client.ConvenienceFee;
                model.ConvenienceFeeType = (int)club.Client.ConvenienceFeeType;
                model.ConvenienceFeeMessage = club.Client.ConvenienceFeeMessage;
                model.ConvenienceFeeLabel = club.Client.ConvenienceFeeLabel;
            }
            model.HasChessTourney = club.Client.HasChessTourney;
            model.PlayerCanEditOrder = club.Client.CanPlayerEditOrder;

            model.SurcahrgeAmountType = (int)ChargeDiscountType.Fixed;
            if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == surchargeType))
            {
                var surcharge = club.Charges.First(c => !c.IsDeleted && c.Category == surchargeType);
                model.EnableSurCharge = true;
                model.SurchargeAmount = surcharge.Amount;
                model.DisableForZeroAmount = surcharge.DisableForZeroAmount;
                model.SurchargeLabel = surcharge.Name;
                model.SurchargeMessage = surcharge.Description;
                model.SurcahrgeAmountType = (int)surcharge.AmountType;
                model.ApplyType = surcharge.ApplyType;
            }

            if (club.Charges != null && club.Charges.Any(c => !c.IsDeleted && c.Category == registrationFeeType))
            {
                var registrationFeeCharge = club.Charges.First(c => !c.IsDeleted && c.Category == registrationFeeType);
                model.EnableRegisterationFee = true;
                model.RegisterationFee = registrationFeeCharge.Amount;
                model.RegistrationFeeApplyType = registrationFeeCharge.ApplyType;
                model.RegisterationFeeLabel = registrationFeeCharge.Name;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Settings_Checkout_Save)]
        public virtual ActionResult EditCheckOutSetting(CheckoutViewModel model, CheckOutSettingsSection settingsSection)
        {

            CheckoutSettingsValidation(ModelState, model, settingsSection);

            var surchargeType = ChargeDiscountCategory.Surcharge;

            if (this.GetCurrentUserRole() == RoleCategory.Partner)
            {
                surchargeType = ChargeDiscountCategory.PartnerSurcharge;
            }

            var registrationFeeType = ChargeDiscountCategory.ApplicationFee;

            if (this.GetCurrentUserRole() == RoleCategory.Partner)
            {
                registrationFeeType = ChargeDiscountCategory.PartnerApplicationFee;
            }

            if (ModelState.IsValid)
            {
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                if (settingsSection == CheckOutSettingsSection.Redirect)
                {
                    club.Setting.BrowseMoreRedirectUrl = new ClubRedirectUrl(model.BrowseMoreUrlSelectedType, model.BrowseMoreUrl);
                    club.Setting.BrowseMoreText = model.BrowseMoreText;
                    club.Setting.AnotherRegisterButtonText = model.AnotherRegisterButtonText;

                    club.Setting.RegistrationPageLogoRedirectUrl = new ClubRedirectUrl(model.RegistrationLogoUrlSelectedType, model.RegistrationLogoUrl);

                    club.Setting.LogOutRedirectUrl = new ClubRedirectUrl(model.LogOutUrlSelectedType, model.LogOutUrl);
                    club.Setting.ConfirmationRedirectUrl = new ClubRedirectUrl(model.ConfirmationUrlSelectedType, model.ConfirmationUrl);

                    club.IsSettingChanged = true;
                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }
                else if (settingsSection == CheckOutSettingsSection.Donation)
                {
                    club.Client.EnableDonation = model.EnableDonation;
                    club.Client.EnableCartDonation = model.EnableCartDonation;
                    club.Client.DonationMessage = model.DonationMessage;
                    club.Client.DonationDescription = model.DonationDescription;

                    if (!club.Client.DonationFormId.HasValue)
                    {
                        club.Client.DonationFormId = Ioc.ClubBusiness.GenerateDonationForm();
                    }

                    club.IsSettingChanged = true;
                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });

                }
                else if (settingsSection == CheckOutSettingsSection.DiscountCoupon)
                {
                    club.Setting.IsDiscountApplyOnOriginPrice = !(model.IsDiscountApplyOnOriginPrice);
                    club.Setting.HideCouponInCart = model.HideCouponInCart;

                    club.IsSettingChanged = true;
                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }
                else if (settingsSection == CheckOutSettingsSection.Charges)
                {
                    if (model.EnableConvenienceFee)
                    {
                        club.Client.ConvenienceFee = model.ConvenienceFee;
                        club.Client.ConvenienceFeeMessage = model.ConvenienceFeeMessage;
                        club.Client.ConvenienceFeeLabel = model.ConvenienceFeeLabel;
                        club.Client.ConvenienceFeeType = (ChargeDiscountType)model.ConvenienceFeeType;
                    }
                    else
                    {
                        club.Client.ConvenienceFee = 0;
                        club.Client.ConvenienceFeeMessage = "";
                        club.Client.ConvenienceFeeLabel = "";
                        club.Client.ConvenienceFeeType = ChargeDiscountType.Fixed;
                    }

                    if (model.EnableSurCharge)
                    {
                        if (club.Charges != null && club.Charges.Any(c => c.IsDeleted == false && c.Category == surchargeType))
                        {
                            var surcharges = club.Charges.First(c => c.IsDeleted == false && c.Category == surchargeType);
                            club.Charges.Remove(surcharges);
                        }
                        club.Charges = new List<Charge>();
                        if ((ChargeDiscountType)model.SurcahrgeAmountType == ChargeDiscountType.Percent)
                        {
                            model.ApplyType = 0;
                        }
                        club.Charges.Add(new Charge()
                        {
                            Amount = model.SurchargeAmount,
                            DisableForZeroAmount = model.DisableForZeroAmount,
                            AmountType = (ChargeDiscountType)model.SurcahrgeAmountType,
                            ApplyType = model.ApplyType,
                            ClubId = club.Id,
                            IsDeleted = false,
                            MetaData = new MetaData() { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                            Description = model.SurchargeMessage,
                            Name = model.SurchargeLabel,
                            Category = surchargeType,
                            Attributes = new ChargeAttribute()

                        });
                    }
                    else
                    {
                        if (club.Charges != null && club.Charges.Any(c => c.IsDeleted == false && c.Category == surchargeType))
                        {
                            var surcharges = club.Charges.First(c => c.IsDeleted == false && c.Category == surchargeType);
                            club.Charges.Remove(surcharges);
                        }
                    }

                    club.Setting.ShowCombinedPriceOnly = model.ShowCombinedPriceOnly;

                    club.IsSettingChanged = true;
                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }
                else if (settingsSection == CheckOutSettingsSection.ClassPage)
                {
                    club.Setting.HideEnrollmentCapacityInClassPage = model.HideEnrollmentCapacityInClassPage;

                    club.Setting.AppearanceSetting.HideProgramDates = model.HideProgramDates;


                    club.IsSettingChanged = true;
                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }
                else if (settingsSection == CheckOutSettingsSection.CartPage)
                {

                    club.Setting.AppearanceSetting.HideProgramDatesInCart = model.HideProgramDatesInCart;

                    club.IsSettingChanged = true;
                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }
                else if (settingsSection == CheckOutSettingsSection.LoginPage)
                {
                    club.Setting.IsNewFamily = model.IsNewFamily;
                    club.Setting.NewFamilyNotification = model.NewFamilyNotification;
                    club.IsSettingChanged = true;
                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }
                else
                {

                    club.Client.HasChessTourney = model.HasChessTourney;
                    club.Client.CanPlayerEditOrder = model.PlayerCanEditOrder;

                    club.Setting.ClubProgram.HiddenPricePrograms = !model.HideAllProgramsPrice ? model.HiddenPricePrograms : null;
                    club.Setting.ClubProgram.EnablePriceHidden = model.EnablePriceHidden;
                    club.Setting.ClubProgram.HideAllProgramsPrice = model.HideAllProgramsPrice;

                    //Add registration fee
                    if (model.EnableRegisterationFee)
                    {
                        if (club.Charges != null && club.Charges.Any(c => c.IsDeleted == false && c.Category == registrationFeeType))
                        {
                            var registrationfeeCharge = club.Charges.First(c => c.IsDeleted == false && c.Category == registrationFeeType);
                            club.Charges.Remove(registrationfeeCharge);
                        }

                        if (club.Charges == null)
                        {
                            club.Charges = new List<Charge>();
                        }

                        club.Charges.Add(new Charge()
                        {
                            Amount = model.RegisterationFee,
                            AmountType = ChargeDiscountType.Fixed,
                            ApplyType = model.RegistrationFeeApplyType,
                            ClubId = club.Id,
                            IsDeleted = false,
                            MetaData = new MetaData() { DateCreated = DateTime.UtcNow, DateUpdated = DateTime.UtcNow },
                            Name = model.RegisterationFeeLabel,
                            Category = registrationFeeType,
                            Attributes = new ChargeAttribute()
                        });
                    }
                    else
                    {
                        if (club.Charges != null && club.Charges.Any(c => c.IsDeleted == false && c.Category == registrationFeeType))
                        {
                            var registrationfeeCharge = club.Charges.First(c => c.IsDeleted == false && c.Category == registrationFeeType);
                            club.Charges.Remove(registrationfeeCharge);
                        }
                    }

                    club.IsSettingChanged = true;
                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }
            }
            return base.JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Settings_General_View)]
        public virtual JsonNetResult EditGeneralSetting()
        {
            var model = new OrganizationSettingsViewModel();

            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            model.AllStaffTitles.Add(new SelectKeyValue<string>() { Text = "Select", Value = null });

            var staffTitlesSorted = Ioc.ClubBusiness.GetStaffTitles(DropdownHelpers.ToSelectList<StaffTitle>());
            model.AllStaffTitles.AddRange(staffTitlesSorted);

            model.Username = this.GetCurrentUserName();
            model.Name = club.Name;
            model.Site = club.Site;
            model.Domain = string.Format("https://{0}.jumbula.com", club.Domain);
            model.Description = club.Description;



            model.Address = club.ClubLocations.First().PostalAddress.Address;

            model.SelectedTimeZone = ((int)club.TimeZone).ToString();

            model.Logo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
            var contact = club.ContactPersons.Where(c => c.IsPrimary).FirstOrDefault();

            if (contact != null)
            {
                model.FirstName = contact.FirstName;
                model.LastName = contact.LastName;
                model.Email = contact.Email;
                model.Phone = contact.Phone;
                model.Title = contact.Title;
                model.ExtensionPhone = contact.PhoneExtension;
            }

            model.ActiveStaffs = club.ClubStaffs.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen && !s.IsDeleted).Select(s =>
                              new SelectKeyValue<int>
                              {
                                  Text = s.JbUserRole.User.UserName,
                                  Value = s.Id
                              })
                          .ToList();

            if (club.Setting != null)
            {
                model.SelectedBusinessType = ((int)(club.Setting.BusinessType)).ToString();
                model.LegalName = club.Setting.LegalName;
                model.TaxIDFEIN = club.Setting.TaxIDFEIN;
                model.TaxIDSSN = club.Setting.TaxIDSSN;
                model.SelectedStaffResponder = club.Setting.StaffResponder;

                model.OnSiteCoordinatorName = club.Setting.OnSiteCoordinatorName;
                model.OnSiteCoordinatorEmail = club.Setting.OnSiteCoordinatorEmail;
            }

            if (club.Setting != null && club.Setting.NotificationEmails != null && club.Setting.NotificationEmails.Any())
            {

                model.AllCCEmails = club.Setting.NotificationEmails.Select(c =>
                        new SelectKeyValue<string>
                        {
                            Text = c,
                            Value = c
                        })
                        .ToList();

                model.SelectedCCEmails = model.AllCCEmails.Select(c => c.Value).ToList();

                model.IsNotificationEmailsSet = true;
            }
            else
            {
                model.AllCCEmails = new List<SelectKeyValue<string>>();
                model.SelectedCCEmails = new List<string>();
            }
            return JsonNet(model);
        }

        [HttpGet]
        [JbAuthorize(JbAction.Settings_PaymentSettings_View)]
        public virtual JsonNetResult PaymentSetting(bool isCallback = false)
        {
            var model = new PaymentSettingsViewModel();

            if (this.IsUserAdminForClub(ActiveClub.Domain))
            {
                var client = Ioc.ClientBusiness.Get(ActiveClub.Id);
                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                model.ClientId = client.Id;
                model.Currency = club.Currency;

                if (client.PaymentMethods != null)
                {

                    model.EnableManualPayment = client.PaymentMethods.EnableManualPayment;
                    model.EnableCreditCard = (client.PaymentMethods.EnableStripePayment || client.PaymentMethods.EnableAuthorizePayment) ? true : false;
                    model.EnableStripePayment = (client.PaymentMethods.EnableStripePayment || client.PaymentMethods.EnableAchPayment);
                    model.EnableAuthorizePayment = client.PaymentMethods.EnableAuthorizePayment;
                    model.IsCreditCardTestMode = client.PaymentMethods.IsCreditCardTestMode;
                    model.LoginId = client.PaymentMethods.AuthorizeLoginId;
                    model.TransactionKey = client.PaymentMethods.AuthorizeTransactionKey;
                    model.EnablePayByCash = client.PaymentMethods.EnableCashPayment;
                    model.EnablePaypalPayment = client.PaymentMethods.EnablePaypalPayment;
                    model.EnableBankTransfer = client.PaymentMethods.EnableBankTransferPayment;
                    //  model.IsCreditCardTestMode = client.PaymentMethods.EnableStripePayment;
                    model.ManualPaymentInstruction = client.PaymentMethods.ManualPaymentInstruction;
                    model.ManualPaymentLabel = client.PaymentMethods.ManualPaymentName;
                    model.ManualPaymentMessage = client.PaymentMethods.ManualPaymentMessage;
                    model.SelectedPaymentMethod = ((int)client.PaymentMethods.ManualPaymentMethod).ToString();
                    model.PayByCashInstruction = client.PaymentMethods.CashPaymentInstruction;
                    model.PayByCashMessage = client.PaymentMethods.CashPaymentMessage;
                    model.BankTransferInstruction = client.PaymentMethods.BankTransferPaymentInstruction;
                    model.BankTransferMessage = client.PaymentMethods.BankTransferPaymentMessage;
                    model.PaypalEmail = client.PaymentMethods.PaypalEmail;
                    model.StripeCustomerId = client.PaymentMethods.StripeCustomerId;
                    model.StripePaymentLabel = client.PaymentMethods.CreditCardCheckoutLabel;
                    model.PaypalPaymentLabel = client.PaymentMethods.PaypalCheckoutLabel;
                    model.CashPaymentLabel = client.PaymentMethods.CashCheckoutLabel;
                    model.BankTransferPaymentLabel = client.PaymentMethods.BankTransferCheckoutLabel;
                    model.EnableAchPayment = client.PaymentMethods.EnableAchPayment;
                    model.AchPaymentLabel = client.PaymentMethods.AchPaymentCheckoutLabel;
                    model.AchPaymentMessage = client.PaymentMethods.AchPaymentMessage;
                    model.AchPaymentInstruction = client.PaymentMethods.AchPaymentInstruction;
                }
                var urlFirstPart = Request.Url.GetLeftPart(UriPartial.Authority);
                if (urlFirstPart.ToLower().Contains("jumbula"))
                {
                    model.ReturnUrl = "https://jumbula.com/Stripe/StripeConnectReturnUrl";
                }
                else if (urlFirstPart.ToLower().Contains("greenteaplanet"))
                {
                    model.ReturnUrl = "https://greenteaplanet.com/Stripe/StripeConnectReturnUrl";
                }
                else if (urlFirstPart.ToLower().Contains("aftersearch"))
                {
                    model.ReturnUrl = "https://aftersearch.com/Stripe/StripeConnectReturnUrl";
                }
                else
                {
                    model.ReturnUrl = string.Format("{0}/Stripe/StripeConnectReturnUrl", Request.Url.GetLeftPart(UriPartial.Authority));
                }

                model.UserId = this.GetCurrentUserId();
                if (!isCallback)
                {
                    model.Token = $"{ActiveClub.Id}";
                }

            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Settings_PaymentSettings_Save)]
        public virtual ActionResult PaymentSetting(PaymentSettingsViewModel model, PaymentSettingsSection settingsSection)
        {
            var status = false;

            PaymentSettingsValidation(ModelState, model, settingsSection);

            if (ModelState.IsValid)
            {
                var clientDb = Ioc.ClientBusiness;
                var client = clientDb.GetById(model.ClientId);
                var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

                if (client.PaymentMethods == null)
                {
                    client.PaymentMethods = new ClientPaymentMethod();
                }
                if (settingsSection == PaymentSettingsSection.Stripe && model.EnableStripePayment)
                {

                    if (string.IsNullOrEmpty(client.PaymentMethods.StripeCustomerId))
                    {
                        return Json(new JResult { Status = false, Message = "Please connect to stripe before save your changes.", RecordsAffected = 0 });
                    }
                }
                switch (settingsSection)
                {
                    case PaymentSettingsSection.Stripe:
                        {
                            if (client.PaymentMethods.EnableAuthorizePayment && model.EnableStripePayment)
                            {
                                status = true;
                                break;

                            }
                            if (model.EnableStripePayment)
                            {
                                client.PaymentMethods.EnableStripePayment = model.EnableCreditCard;
                                client.PaymentMethods.EnableAchPayment = model.EnableAchPayment;
                                client.PaymentMethods.DefaultPaymentGateway = PaymentGateway.Stripe;
                            }
                            else
                            {
                                client.PaymentMethods.EnableStripePayment = false;
                                client.PaymentMethods.EnableAchPayment = false;
                            }
                            client.PaymentMethods.CreditCardCheckoutLabel = string.IsNullOrEmpty(model.StripePaymentLabel) ? Constants.StripeCheckoutLabel : model.StripePaymentLabel;
                            client.PaymentMethods.AchPaymentCheckoutLabel = string.IsNullOrEmpty(model.AchPaymentLabel) ? Constants.AchPaymentCheckoutLabel : model.AchPaymentLabel;
                            client.PaymentMethods.AchPaymentMessage = model.AchPaymentMessage;
                            client.PaymentMethods.AchPaymentInstruction = model.AchPaymentInstruction;
                        }
                        break;
                    case PaymentSettingsSection.AuthorizeNet:
                        {
                            if ((client.PaymentMethods.EnableStripePayment || client.PaymentMethods.EnableAchPayment) && model.EnableAuthorizePayment)
                            {
                                status = true;
                                break;

                            }
                            if (model.EnableAuthorizePayment)
                            {
                                client.PaymentMethods.EnableAuthorizePayment = model.EnableAuthorizePayment;
                                client.PaymentMethods.DefaultPaymentGateway = PaymentGateway.AuthorizeNet;
                            }
                            else
                            {
                                client.PaymentMethods.EnableAuthorizePayment = false;
                                client.PaymentMethods.EnableAchPayment = false;
                                client.PaymentMethods.EnableStripePayment = false;
                            }
                            client.PaymentMethods.CreditCardCheckoutLabel = string.IsNullOrEmpty(model.StripePaymentLabel) ? Constants.StripeCheckoutLabel : model.StripePaymentLabel;

                            client.PaymentMethods.AuthorizeLoginId = model.LoginId;
                            client.PaymentMethods.AuthorizeTransactionKey = model.TransactionKey;
                        }
                        break;
                    case PaymentSettingsSection.Paypal:
                        {
                            client.PaymentMethods.EnablePaypalPayment = model.EnablePaypalPayment;
                            client.PaymentMethods.PaypalEmail = model.PaypalEmail;
                            client.PaymentMethods.PaypalCheckoutLabel = string.IsNullOrEmpty(model.PaypalPaymentLabel) ? Constants.PaypalCheckoutLabel : model.PaypalPaymentLabel;
                        }
                        break;
                    case PaymentSettingsSection.Cash:
                        {
                            client.PaymentMethods.EnableCashPayment = model.EnablePayByCash;
                            client.PaymentMethods.CashPaymentMessage = model.PayByCashMessage;
                            client.PaymentMethods.CashPaymentInstruction = model.PayByCashInstruction;
                            client.PaymentMethods.CashCheckoutLabel = string.IsNullOrEmpty(model.CashPaymentLabel) ? Constants.CashOrCheckCheckoutLabel : model.CashPaymentLabel;
                        }
                        break;
                    case PaymentSettingsSection.BankTransfer:
                        {
                            client.PaymentMethods.EnableBankTransferPayment = model.EnableBankTransfer;
                            client.PaymentMethods.BankTransferPaymentMessage = model.BankTransferMessage;
                            client.PaymentMethods.BankTransferPaymentInstruction = model.BankTransferInstruction;
                            client.PaymentMethods.BankTransferCheckoutLabel = string.IsNullOrEmpty(model.BankTransferPaymentLabel) ? Constants.BankDepositCheckoutLabel : model.BankTransferPaymentLabel;
                        }
                        break;
                    case PaymentSettingsSection.Manual:
                        {
                            client.PaymentMethods.EnableManualPayment = model.EnableManualPayment;
                            client.PaymentMethods.ManualPaymentName = model.ManualPaymentLabel;
                            client.PaymentMethods.ManualPaymentMessage = model.ManualPaymentMessage;
                            client.PaymentMethods.ManualPaymentInstruction = model.ManualPaymentInstruction;
                            client.PaymentMethods.ManualPaymentMethod = (PaymentMethod)(int.Parse(model.SelectedPaymentMethod));
                        }
                        break;
                    case PaymentSettingsSection.Currency:
                        {
                            club.Currency = model.Currency;
                        }
                        break;
                }
                OperationStatus res = new OperationStatus();
                if (status)
                {
                    res.Status = false;
                    res.Message = "You cannot activate Stripe and Authorize.Net at the same time.";
                }
                else
                {
                    res = clientDb.Update(client);
                    if (string.IsNullOrEmpty(res.Message))
                    {
                        res.Status = true;
                    }
                }


                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [HttpGet]
        [JbAuthorize(JbAction.DonationForm_View)]
        public virtual JsonResult EditDonationForm()
        {
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            if (!club.Client.DonationFormId.HasValue)
            {
                club.Client.DonationFormId = Ioc.ClubBusiness.GenerateDonationForm();
                Ioc.ClubBusiness.Update(club);
            }

            return Json(new JResult { Status = true, Data = club.Client.DonationFormId.ToString() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Settings_General_Save)]
        public virtual ActionResult EditGeneralSetting(OrganizationSettingsViewModel model, OrganizationSettingsSection settingsSection)
        {

            OrganizationSettingsValidation(ModelState, model, settingsSection);
            var address = new PostalAddress();

            if (settingsSection == OrganizationSettingsSection.Basic)
            {

                if (!string.IsNullOrEmpty(model.Address))
                {
                    try
                    {
                        GoogleMap.GetGeo(model.Address, ref address);
                    }
                    catch
                    {
                        ModelState.AddModelError("Address", "Address is not valid.");
                    }
                }
            }

            if (ModelState.IsValid)
            {
                if (OrganizationSettingsSection.ChangePass == settingsSection)
                {
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = _applicationUserManager.ChangePasswordAsync(this.GetCurrentUserId(), model.OldPassword, model.NewPassword).Result == IdentityResult.Success;
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    return Json(new JResult { Status = changePasswordSucceeded, Message = (changePasswordSucceeded ? Constants.PlayerProfile_ChangePassword_Succeed : Constants.PlayerProfile_ChangePassword_Failed), RecordsAffected = (changePasswordSucceeded ? 1 : 0) });
                }
                var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

                if (club.Client == null)
                {
                    club.Client = new Client();
                    var priceplan = Ioc.PricePlanBusiness.GetFreeTrialPlan();
                    club.Client.PricePlan = priceplan;
                    club.Client.PricePlanId = priceplan.Id;
                }
                var message = "{0} updated successfully";

                switch (settingsSection)
                {
                    case OrganizationSettingsSection.Basic:
                        {
                            club.Name = model.Name;
                            club.Site = model.Site;
                            club.Description = model.Description;
                            club.Address = address;
                            club.ClubLocations.First().PostalAddress = address;
                            club.TimeZone = (Jumbula.Common.Enums.TimeZone)Convert.ToInt16(model.SelectedTimeZone);
                            message = string.Format(message, "Organization information");
                        }
                        break;
                    case OrganizationSettingsSection.Logo:
                        {
                            var fileName = string.Empty;
                            if (model.Logo != "")
                            {
                                model.Logo = model.Logo.Replace("data:image/jpeg;base64,", string.Empty);

                                model.Logo = model.Logo.Replace("data:image/png;base64,", string.Empty);

                                var fileData = Convert.FromBase64String(model.Logo);
                                fileName = "Logo_Large.jpg";

                                var sm = new StorageManager(WebConfigHelper.AzureStorageConnection, Constants.Path_ClubFilesContainer);

                                var fileSize = fileData.Count() / 1024;

                                var contentType = "image/jpeg";

                                Stream fileStream = new MemoryStream(fileData);

                                sm.UploadBlob(club.Domain, fileName, fileStream, contentType);

                                var url =
                                    StorageManager.GetUri(Constants.Path_ClubFilesContainer, club.Domain, fileName).AbsoluteUri;

                                fileStream.Close();
                            }
                            club.Logo = fileName;
                            message = string.Format(message, "Organization logo");
                        }
                        break;
                    case OrganizationSettingsSection.Contact:
                        {
                            var contact = club.ContactPersons.FirstOrDefault();
                            contact.FirstName = model.FirstName;
                            contact.Email = string.IsNullOrEmpty(model.Email) ? club.ClubStaffs.Single(c => c.JbUserRole.Role.Name == RoleCategory.Owner.ToString()).JbUserRole.User.UserName : model.Email;
                            contact.LastName = model.LastName;
                            contact.Phone = model.Phone;
                            contact.Title = model.Title;
                            contact.PhoneExtension = model.ExtensionPhone;

                            message = string.Format(message, "Contact information");
                        }
                        break;
                    case OrganizationSettingsSection.Formation:
                        {

                            var businessType = ((BusinessType)(Convert.ToInt32(model.SelectedBusinessType)));
                            if (businessType == BusinessType.Individual)
                            {
                                //club.Setting.LegalName = string.Empty;
                                //club.Setting.TaxIDFEIN = string.Empty;
                                club.Setting.TaxIDSSN = model.TaxIDSSN;
                            }
                            else
                            {
                                club.Setting.LegalName = model.LegalName;
                                club.Setting.TaxIDFEIN = model.TaxIDFEIN;
                                // club.Setting.TaxIDSSN = string.Empty;
                            }
                            club.Setting.BusinessType = businessType;

                            message = string.Format(message, "Organization formation");
                        }
                        break;
                    case OrganizationSettingsSection.NotificationEmail:
                        {

                            if (club.Setting.NotificationEmails == null)
                            {
                                club.Setting.NotificationEmails = new List<string>();
                            }
                            else
                            {
                                club.Setting.NotificationEmails.Clear();
                            }
                            if (model.IsNotificationEmailsSet)
                            {
                                club.Setting.NotificationEmails.AddRange(model.SelectedCCEmails);
                            }


                            message = string.Format(message, "Notification emails");
                        }
                        break;
                    case OrganizationSettingsSection.StaffResponder:
                        {
                            var staff = club.ClubStaffs.SingleOrDefault(c => c.Id == model.SelectedStaffResponder);

                            if (model.SelectedStaffResponder >= 0)
                            {
                                club.Setting.StaffResponder = model.SelectedStaffResponder;
                                message = string.Format(message, "Staff email");
                            }
                            else
                            {
                                return Json(new JResult { Status = false });
                            }

                        }
                        break;
                    case OrganizationSettingsSection.OnSiteCoordinator:
                        {
                            club.Setting.OnSiteCoordinatorName = model.OnSiteCoordinatorName;
                            club.Setting.OnSiteCoordinatorEmail = model.OnSiteCoordinatorEmail;

                            message = string.Format(message, "Onsite coordinator");
                        }
                        break;
                    default:
                        break;
                }

                club.IsSettingChanged = true;
                var res = Ioc.ClubBusiness.Update(club);

                return Json(new JResult { Status = res.Status, Message = res.Status ? message : res.Message, RecordsAffected = res.RecordsAffected });
            }

            return base.JsonFormResponse();
        }

        [JbAuthorize(JbAction.Settings_Partner_View)]
        [HttpGet]
        public virtual ActionResult GetPartnerSetting()
        {
            //return Json(new JResult { Status = changePasswordSucceeded, Message = (changePasswordSucceeded ? Constants.PlayerProfile_ChangePassword_Succeed : Constants.PlayerProfile_ChangePassword_Failed), RecordsAffected = (changePasswordSucceeded ? 1 : 0) });
            var model = new PartnerSettingViewModel();

            if (this.GetCurrentUserRole() == RoleCategory.Partner)
            {
                var clubId = ActiveClub.Id;
                var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(clubId);

                model.ReadStripeSettingsFromMember = partnerSetting.PortalParameters.ReadStripeSettingsFromMember;
                model.ShowWelcomeToJumbula = partnerSetting.PortalParameters.ShowWelocomeToJumbula;
                model.ShowSchoolsInHome = partnerSetting.PortalParameters.ShowSchoolsInHome;
                model.ReadInvoiceSettingsfromPartner = partnerSetting.PortalParameters.ReadInvoiceSettingsfromMember;
                model.SendEmailProvider = partnerSetting.PortalParameters.EmailProviderAfterInvitinganInstructor;
                model.EmailContent = partnerSetting.PortalParameters.EmailContent;
                model.ShowGenderForCatalog = partnerSetting.PortalParameters.ShowGenderForCatalog.Value;
                model.PriceOptionFeeInputBoxLabel = partnerSetting.PortalParameters.PriceOptionFeeInputBoxLabel;
                model.PriceOptionNote = partnerSetting.PortalParameters.PriceOptionNote;
                model.HelpInformationFromPartner = partnerSetting.PortalParameters.HelpInformationFromPartner;
                model.NotificationEmailFromPartner = partnerSetting.PortalParameters.NotificationEmailFromPartner;
                model.ReadRegistrationFeeFromMember = partnerSetting.PortalParameters.ReadRegistrationFeeFromMember;
                model.ReadLoginTabFromPartner = partnerSetting.PortalParameters.ReadLoginTabFromPartner;
                model.ReadMessageNewFamilyFromPartner = partnerSetting.PortalParameters.ReadMessageNewFamilyFromPartner;
                model.ReadAutoChargePolicyFromMember = partnerSetting.PortalParameters.ReadAutoChargePolicyFromMember;
                model.ReadProgramsPolicyFromMember = partnerSetting.PortalParameters.ReadProgramsPolicyFromMember;
                model.ReadDependentCareReportInfoFromMember = partnerSetting.PortalParameters.ReadDependentCareReportInfoFromMember;
                model.ReadGASettingFromPartner = partnerSetting.PortalParameters.ReadGASettingFromPartner;
                model.ReadDelinquentPolicyFromMember = partnerSetting.PortalParameters.ReadDelinquentPolicyFromMember;
                model.ReadDuplicateEnrollmentsFromMember = partnerSetting.PortalParameters.ReadDuplicateEnrollmentsFromMember;
            }

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.Settings_Partner_Save)]
        [HttpPost]
        public virtual ActionResult SavePartnerSetting(PartnerSettingViewModel model)
        {

            if (!model.SendEmailProvider)
            {
                ModelState.Remove("model.EmailContent");
            }

            if (ModelState.IsValid)
            {
                var message = string.Empty;
                var clubId = ActiveClub.Id;
                var club = Ioc.ClubBusiness.Get(clubId);
                var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(clubId);

                if (partnerSetting == null)
                {
                    partnerSetting = new PartnerSetting();
                }

                partnerSetting.PortalParameters.ReadStripeSettingsFromMember = model.ReadStripeSettingsFromMember;
                partnerSetting.PortalParameters.ShowWelocomeToJumbula = model.ShowWelcomeToJumbula;
                partnerSetting.PortalParameters.ShowSchoolsInHome = model.ShowSchoolsInHome;
                partnerSetting.PortalParameters.ReadInvoiceSettingsfromMember = model.ReadInvoiceSettingsfromPartner;
                partnerSetting.PortalParameters.EmailProviderAfterInvitinganInstructor = model.SendEmailProvider;
                partnerSetting.PortalParameters.EmailContent = model.EmailContent;
                partnerSetting.PortalParameters.ShowGenderForCatalog = model.ShowGenderForCatalog;
                partnerSetting.PortalParameters.PriceOptionFeeInputBoxLabel = model.PriceOptionFeeInputBoxLabel;
                partnerSetting.PortalParameters.PriceOptionNote = model.PriceOptionNote;
                partnerSetting.PortalParameters.HelpInformationFromPartner = model.HelpInformationFromPartner;
                partnerSetting.PortalParameters.NotificationEmailFromPartner = model.NotificationEmailFromPartner;
                partnerSetting.PortalParameters.ReadRegistrationFeeFromMember = model.ReadRegistrationFeeFromMember;
                partnerSetting.PortalParameters.ReadLoginTabFromPartner = model.ReadLoginTabFromPartner;
                partnerSetting.PortalParameters.ReadMessageNewFamilyFromPartner = model.ReadMessageNewFamilyFromPartner;
                partnerSetting.PortalParameters.ReadAutoChargePolicyFromMember = model.ReadAutoChargePolicyFromMember;
                partnerSetting.PortalParameters.ReadProgramsPolicyFromMember = model.ReadProgramsPolicyFromMember;

                partnerSetting.PortalParameters.ReadDelinquentPolicyFromMember = model.ReadDelinquentPolicyFromMember;
                partnerSetting.PortalParameters.ReadDependentCareReportInfoFromMember = model.ReadDependentCareReportInfoFromMember;
                partnerSetting.PortalParameters.ReadGASettingFromPartner = model.ReadGASettingFromPartner;
                partnerSetting.PortalParameters.ReadDuplicateEnrollmentsFromMember = model.ReadDuplicateEnrollmentsFromMember;

                var serializedSettings = JsonConvert.SerializeObject(partnerSetting);

                //club.IsSettingChanged = true;
                club.SettingSerialized = serializedSettings;

                var res = Ioc.ClubBusiness.Update(club);

                return Json(new JResult { Status = res.Status, Message = res.Status ? message : res.Message, RecordsAffected = res.RecordsAffected });
            }
            return base.JsonFormResponse();

        }
        [HttpGet]
        public virtual JsonNetResult GetHolidays()
        {
            var model = new HolidaysDateViewModel();

            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            if (club.Setting != null)
            {
                if (club.Setting.ListOfHolidayDates != null && club.Setting.ListOfHolidayDates.Any())
                {
                    model.HolidayDates = club.Setting.ListOfHolidayDates;
                    model.AllHolidayDates = club.Setting.ListOfHolidayDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                }
                if (club.Setting.ListOfHolidayAMDates != null && club.Setting.ListOfHolidayAMDates.Any())
                {
                    model.HolidayAMDates = club.Setting.ListOfHolidayAMDates;
                    model.AllHolidayAMDates = club.Setting.ListOfHolidayAMDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                }
                if (club.Setting.ListOfHolidayPMDates != null && club.Setting.ListOfHolidayPMDates.Any())
                {
                    model.HolidayPMDates = club.Setting.ListOfHolidayPMDates;
                    model.AllHolidayPMDates = club.Setting.ListOfHolidayPMDates.Select(c => new SelectKeyValue<string>() { Text = c, Value = c }).ToList();
                }
            }

            return JsonNet(model);
        }
        [HttpPost]
        public virtual ActionResult SaveHolidays(HolidaysDateViewModel model)
        {
            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);
            if (club.Setting == null)
            {
                club.Setting = new ClubSetting();
            }

            var holidayInfo = (club.Setting).ListOfHolidayDates;
            var amHolidayInfo = (club.Setting).ListOfHolidayAMDates;
            var pmHolidayInfo = (club.Setting).ListOfHolidayPMDates;

            holidayInfo = (model.HolidayDates.Any()) ? model.HolidayDates : null;
            club.Setting.ListOfHolidayDates = holidayInfo;

            amHolidayInfo = (model.HolidayAMDates.Any()) ? model.HolidayAMDates : null;
            club.Setting.ListOfHolidayAMDates = amHolidayInfo;

            pmHolidayInfo = (model.HolidayPMDates.Any()) ? model.HolidayPMDates : null;
            club.Setting.ListOfHolidayPMDates = pmHolidayInfo;

            club.IsSettingChanged = true;

            var res = Ioc.ClubBusiness.Update(club);

            return Json(new JResult { Status = res.Status, Message = res.Message });

        }

        [JbAuthorize(JbAction.SchoolSetting_AutoChargePolicy_View)]
        [HttpGet]
        public JsonNetResult GetAutoChargePolicy()
        {
            var model = _clubSettingBusiness.GetAutoChargePolicy(ActiveClub.Id);

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.SchoolSetting_AutoChargePolicy_Save)]
        [HttpPost]
        public JsonNetResult SaveAutoChargePolicy(AutoChargePolicySettingViewModel model)
        {
            AutoChargePolicyValidation(model);

            if (!ModelState.IsValid) return JsonNet(false);

            var result = _clubSettingBusiness.SaveAutoChargePolicy(model, ActiveClub.Id);

            return JsonNet(result);
        }

        [HttpPost]
        public JsonNetResult SuccessEmailPreview([System.Web.Http.FromBody] string message, [System.Web.Http.FromBody] bool isAdmin)
        {
            var successEmailParameter = new AutoChargeSuccessEmailParameterModel
            {
                ClubId = this.GetCurrentClubId(),
                SuccessMessage = message

            };

            var body = _emailBusiness.GetPreview(EmailCategory.AutoChargeSuccess, successEmailParameter, isAdmin ? "Admin" : "Parent");

            return JsonNet(body);
        }

        [HttpPost]
        public JsonNetResult FailEmailPreview(string message, int attemptNumber, bool isAdmin, int? nextAttemptIntervalDays)
        {
            var failEmailParameter = new AutoChargeFailEmailParameterModel()
            {
                ClubId = this.GetCurrentClubId(),
                FailMessage = message,
                AttemptNumber = attemptNumber,
                NextAttemptIntervalDays = nextAttemptIntervalDays
            };

            var body = _emailBusiness.GetPreview(EmailCategory.AutoChargeFail, failEmailParameter, isAdmin ? "Admin" : "Parent");
            return JsonNet(body);
        }

        [JbAuthorize(JbAction.SchoolSetting_DelinquentPolicy_View)]
        [HttpGet]
        public JsonNetResult GetDelinquentPolicy()
        {
            var model = _clubSettingBusiness.GetDelinquentPolicy(ActiveClub.Id);

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.SchoolSetting_DelinquentPolicy_Save)]
        [HttpPost]
        public JsonNetResult SaveDelinquentPolicy(DelinquentPolicySettingViewModel model)
        {
            DelinquentPolicyValidation(model);

            if (!ModelState.IsValid) return JsonNet(false);

            var result = _clubSettingBusiness.SaveDelinquentPolicy(model, ActiveClub.Id);

            return JsonNet(result);
        }

        private void PaymentSettingsValidation(ModelStateDictionary modelState, PaymentSettingsViewModel model, PaymentSettingsSection settingsSection)
        {
            switch (settingsSection)
            {
                case PaymentSettingsSection.Currency:
                    {
                        modelState.Remove("model.PaypalEmail");
                        modelState.Remove("model.PaypalPaymentLabel");
                        modelState.Remove("model.CashPaymentLabel");
                        modelState.Remove("model.BankTransferPaymentLabel");
                        modelState.Remove("model.ManualPaymentLabel");
                        modelState.Remove("model.TransactionKey");
                        modelState.Remove("model.LoginId");

                        modelState.Remove("model.StripePaymentLabel");
                        modelState.Remove("model.PaypalEmail");
                        modelState.Remove("model.PaypalPaymentLabel");
                        modelState.Remove("model.CashPaymentLabel");
                        modelState.Remove("model.BankTransferPaymentLabel");
                        modelState.Remove("model.ManualPaymentLabel");


                    }
                    break;
                case PaymentSettingsSection.Stripe:
                    modelState.Remove("model.PaypalEmail");
                    modelState.Remove("model.PaypalPaymentLabel");
                    modelState.Remove("model.CashPaymentLabel");
                    modelState.Remove("model.BankTransferPaymentLabel");
                    modelState.Remove("model.ManualPaymentLabel");
                    modelState.Remove("model.TransactionKey");
                    modelState.Remove("model.LoginId");
                    break;
                case PaymentSettingsSection.AuthorizeNet:
                    modelState.Remove("model.StripePaymentLabel");
                    modelState.Remove("model.PaypalEmail");
                    modelState.Remove("model.PaypalPaymentLabel");
                    modelState.Remove("model.CashPaymentLabel");
                    modelState.Remove("model.BankTransferPaymentLabel");
                    modelState.Remove("model.ManualPaymentLabel");

                    if (!model.EnableAuthorizePayment)
                    {
                        modelState.Remove("model.TransactionKey");
                        modelState.Remove("model.LoginId");
                    }

                    break;
                case PaymentSettingsSection.Paypal:
                    {
                        modelState.Remove("model.StripePaymentLabel");
                        modelState.Remove("model.CashPaymentLabel");
                        modelState.Remove("model.BankTransferPaymentLabel");
                        modelState.Remove("model.ManualPaymentLabel");
                        modelState.Remove("model.TransactionKey");
                        modelState.Remove("model.LoginId");
                        if (model.EnablePaypalPayment)
                        {
                            if (string.IsNullOrEmpty(model.PaypalEmail))
                            {
                                modelState.AddModelError("model.PaypalEmail", "Paypal email is required.");

                            }
                        }
                        else
                        {
                            modelState.Remove("model.PaypalEmail");
                        }
                    }
                    break;
                case PaymentSettingsSection.Cash:
                    modelState.Remove("model.StripePaymentLabel");
                    modelState.Remove("model.PaypalPaymentLabel");
                    modelState.Remove("model.BankTransferPaymentLabel");
                    modelState.Remove("model.ManualPaymentLabel");
                    modelState.Remove("model.PaypalEmail");
                    modelState.Remove("model.TransactionKey");
                    modelState.Remove("model.LoginId");
                    break;
                case PaymentSettingsSection.BankTransfer:
                    modelState.Remove("model.StripePaymentLabel");
                    modelState.Remove("model.CashPaymentLabel");
                    modelState.Remove("model.PaypalPaymentLabel");
                    modelState.Remove("model.ManualPaymentLabel");
                    modelState.Remove("model.PaypalEmail");
                    modelState.Remove("model.TransactionKey");
                    modelState.Remove("model.LoginId");

                    break;
                case PaymentSettingsSection.Manual:

                    modelState.Remove("model.StripePaymentLabel");
                    modelState.Remove("model.CashPaymentLabel");
                    modelState.Remove("model.PaypalPaymentLabel");
                    modelState.Remove("model.BankTransferPaymentLabel");
                    modelState.Remove("model.PaypalEmail");
                    modelState.Remove("model.TransactionKey");
                    modelState.Remove("model.LoginId");

                    if (model.EnableManualPayment && string.IsNullOrEmpty(model.ManualPaymentLabel))
                    {
                        modelState.AddModelError("model.ManualPaymentLabel", "Manual payment name is required.");
                    }

                    var paymentmethod = ((PaymentMethod)int.Parse(model.SelectedPaymentMethod));

                    if (paymentmethod != PaymentMethod.Other && paymentmethod != PaymentMethod.Scholarship && paymentmethod != PaymentMethod.FinancialAid)
                    {
                        modelState.AddModelError("model.SelectedPaymentMethod", "Method is required.");
                    }

                    if (!model.EnableManualPayment)
                    {
                        modelState.Remove("model.ManualPaymentLabel");
                    }


                    break;

            }
        }
        private void CheckoutSettingsValidation(ModelStateDictionary modelState, CheckoutViewModel model, CheckOutSettingsSection settingsSection)
        {

            if (settingsSection == CheckOutSettingsSection.Redirect)
            {
                modelState.Remove("model.RegisterationFee");
                modelState.Remove("model.RegisterationFeeLabel");
                modelState.Remove("model.ConvenienceFee");
                modelState.Remove("model.SurchargeAmount");
                modelState.Remove("model.SurchargeLabel");
                modelState.Remove("model.DonationMessage");

                if ((RedirectUrlTypes)int.Parse(model.RegistrationLogoUrlSelectedType) != RedirectUrlTypes.Other)
                {
                    modelState.Remove("model.RegistrationLogoUrl");
                }
                if (string.IsNullOrEmpty(model.SiteUrl) && (RedirectUrlTypes)int.Parse(model.RegistrationLogoUrlSelectedType) == RedirectUrlTypes.OrganizationWebsite)
                {
                    modelState.AddModelError("model.RegistrationSiteUrl", "The organization web site is not specified. Please navigate to General setting to set the organization site.");
                }
                if ((RedirectUrlTypes)int.Parse(model.BrowseMoreUrlSelectedType) != RedirectUrlTypes.Other)
                {
                    modelState.Remove("model.BrowseMoreUrl");
                }
                if (string.IsNullOrEmpty(model.SiteUrl) && (RedirectUrlTypes)int.Parse(model.BrowseMoreUrlSelectedType) == RedirectUrlTypes.OrganizationWebsite)
                {
                    modelState.AddModelError("model.BrowseSiteUrl", "The organization web site is not specified. Please navigate to General setting to set the organization site.");
                }
                if ((RedirectUrlTypes)int.Parse(model.LogOutUrlSelectedType) != RedirectUrlTypes.Other)
                {
                    modelState.Remove("model.LogOutUrl");
                }
                if (string.IsNullOrEmpty(model.SiteUrl) && (RedirectUrlTypes)int.Parse(model.LogOutUrlSelectedType) == RedirectUrlTypes.OrganizationWebsite)
                {
                    modelState.AddModelError("model.LogOutSiteUrl", "The organization web site is not specified. Please navigate to General setting to set the organization site.");
                }
                if ((RedirectUrlTypes)int.Parse(model.ConfirmationUrlSelectedType) != RedirectUrlTypes.Other)
                {
                    modelState.Remove("model.ConfirmationUrl");
                }
            }
            else
            {
                modelState.Remove("model.RegistrationLogoUrl");

                modelState.Remove("model.BrowseMoreUrl");
                modelState.Remove("model.BrowseMoreText");
                modelState.Remove("model.AnotherRegisterButtonText");
                modelState.Remove("model.LogOutUrl");

                if ((RedirectUrlTypes)int.Parse(model.ConfirmationUrlSelectedType) != RedirectUrlTypes.Other)
                {
                    modelState.Remove("model.ConfirmationUrl");
                }

                if (settingsSection == CheckOutSettingsSection.Charges)
                {

                    modelState.Remove("model.DonationMessage");

                    if (model.EnableConvenienceFee)
                    {
                        if (model.ConvenienceFee <= 0)
                        {
                            modelState.AddModelError("model.ConvenienceFee", "Amount is required.");
                        }
                        else
                        {
                            if (model.ConvenienceFeeType == ((int)ChargeDiscountType.Percent) && model.ConvenienceFee > 100)
                            {
                                modelState.AddModelError("model.ConvenienceFee", "Amount must be between 0 to 100.");
                            }
                        }
                    }
                    else
                    {
                        modelState.Remove("model.ConvenienceFee");
                    }

                    if (model.EnableSurCharge)
                    {
                        if (string.IsNullOrEmpty(model.SurchargeLabel))
                        {
                            modelState.AddModelError("model.SurchargeLabel", "Name is required.");
                        }
                        if (model.SurchargeAmount <= 0)
                        {
                            modelState.AddModelError("model.SurchargeAmount", "Amount is required.");
                        }
                        else
                        {
                            if (model.SurcahrgeAmountType == ((int)ChargeDiscountType.Percent) && model.SurchargeAmount > 100)
                            {
                                modelState.AddModelError("model.SurchargeAmount", "Amount must be between 0 to 100.");
                            }
                        }
                    }
                    else
                    {
                        modelState.Remove("model.SurchargeAmount");
                    }
                }
                else if (settingsSection == CheckOutSettingsSection.Donation)
                {
                    modelState.Remove("model.RegisterationFee");
                    modelState.Remove("model.RegisterationFeeLabel");
                    modelState.Remove("model.ConvenienceFee");
                    modelState.Remove("model.SurchargeAmount");
                    modelState.Remove("model.SurchargeLabel");

                    if (!model.EnableCartDonation)
                    {
                        modelState.Remove("model.DonationMessage");
                    }
                }
                else if (settingsSection == CheckOutSettingsSection.Miscellaneous)
                {
                    modelState.Remove("model.DonationMessage");
                    modelState.Remove("model.ConvenienceFee");
                    modelState.Remove("model.SurchargeAmount");
                    modelState.Remove("model.SurchargeLabel");

                    if (model.EnablePriceHidden)
                    {
                        if ((!model.HideAllProgramsPrice && model.HiddenPricePrograms == null) ||
                            (!model.HideAllProgramsPrice && model.HiddenPricePrograms.Count == 0))
                        {
                            ModelState.AddModelError("model.HiddenPricePrograms", "Please select one or more of programs.");
                        }
                    }

                    if (model.EnableRegisterationFee)
                    {
                        if (model.RegisterationFee <= 0)
                        {
                            modelState.AddModelError("model.RegisterationFee", "Amount is required.");
                        }
                        if (string.IsNullOrEmpty(model.RegisterationFeeLabel))
                        {
                            modelState.AddModelError("model.RegisterationFeeLabel", "Name is required.");
                        }
                    }
                    else
                    {
                        modelState.Remove("model.RegisterationFee");
                    }
                }
                else
                {
                    modelState.Remove("model.RegisterationFee");
                    modelState.Remove("model.RegisterationFeeLabel");
                    modelState.Remove("model.ConvenienceFee");
                    modelState.Remove("model.SurchargeAmount");
                    modelState.Remove("model.SurchargeLabel");
                    modelState.Remove("model.DonationMessage");
                }
            }
        }

        private void OrganizationSettingsValidation(ModelStateDictionary modelState, OrganizationSettingsViewModel model, OrganizationSettingsSection settingsSection)
        {
            switch (settingsSection)
            {
                case OrganizationSettingsSection.Basic:
                    {
                        modelState.Remove("model.OldPassword");
                        modelState.Remove("model.NewPassword");
                        modelState.Remove("model.ConfirmPassword");
                        modelState.Remove("model.FirstName");
                        modelState.Remove("model.LastName");
                        modelState.Remove("model.Email");
                        modelState.Remove("model.Phone");
                        modelState.Remove("model.OnSiteCoordinatorEmail");

                    }
                    break;
                case OrganizationSettingsSection.Contact:
                    {
                        modelState.Remove("model.OldPassword");
                        modelState.Remove("model.NewPassword");
                        modelState.Remove("model.ConfirmPassword");
                        modelState.Remove("model.Name");
                        modelState.Remove("model.Site");
                        modelState.Remove("model.Address");
                        modelState.Remove("model.OnSiteCoordinatorEmail");

                    }
                    break;
                case OrganizationSettingsSection.Logo:
                    {
                        modelState.Remove("model.FirstName");
                        modelState.Remove("model.LastName");
                        modelState.Remove("model.Email");
                        modelState.Remove("model.Phone");
                        modelState.Remove("model.Name");
                        modelState.Remove("model.Site");
                        modelState.Remove("model.Address");
                        modelState.Remove("model.OldPassword");
                        modelState.Remove("model.NewPassword");
                        modelState.Remove("model.ConfirmPassword");
                        modelState.Remove("model.OnSiteCoordinatorEmail");

                        break;
                    }
                case OrganizationSettingsSection.Formation:
                    {
                        modelState.Remove("model.FirstName");
                        modelState.Remove("model.LastName");
                        modelState.Remove("model.Email");
                        modelState.Remove("model.Phone");
                        modelState.Remove("model.Name");
                        modelState.Remove("model.Site");
                        modelState.Remove("model.Address");
                        modelState.Remove("model.OldPassword");
                        modelState.Remove("model.NewPassword");
                        modelState.Remove("model.ConfirmPassword");
                        modelState.Remove("model.OnSiteCoordinatorEmail");

                        if (model.SelectedBusinessType == "0")
                        {
                            modelState.AddModelError("model.SelectedBusinessType", "Business type is required.");
                        }
                        if (string.IsNullOrEmpty(model.LegalName) && int.Parse(model.SelectedBusinessType) != (int)BusinessType.Individual)
                        {
                            modelState.AddModelError("model.LegalName", "Business legal name is required.");
                        }
                    }
                    break;
                case OrganizationSettingsSection.ChangePass:
                    {

                        modelState.Remove("model.FirstName");
                        modelState.Remove("model.LastName");
                        modelState.Remove("model.Email");
                        modelState.Remove("model.Phone");
                        modelState.Remove("model.Name");
                        modelState.Remove("model.Site");
                        modelState.Remove("model.Address");
                        modelState.Remove("model.OnSiteCoordinatorEmail");

                        if (!string.IsNullOrEmpty(model.OldPassword) && !_applicationUserManager.CheckPassword(Ioc.UserProfileBusiness.Get(this.GetCurrentUserId()), model.OldPassword))
                        {
                            ModelState.AddModelError("model.OldPassword", "Current password is not correct.");
                        }
                        else if (!string.IsNullOrEmpty(model.NewPassword) && model.OldPassword == model.NewPassword)
                        {
                            ModelState.AddModelError("model.NewPassword", "New password is same as current password.");
                        }
                    }
                    break;
                case OrganizationSettingsSection.NotificationEmail:
                    {
                        modelState.Remove("model.OldPassword");
                        modelState.Remove("model.NewPassword");
                        modelState.Remove("model.ConfirmPassword");
                        modelState.Remove("model.FirstName");
                        modelState.Remove("model.LastName");
                        modelState.Remove("model.Email");
                        modelState.Remove("model.Phone");
                        modelState.Remove("model.Name");
                        modelState.Remove("model.Site");
                        modelState.Remove("model.Address");
                        modelState.Remove("model.OnSiteCoordinatorEmail");

                        if (model.IsNotificationEmailsSet && !model.SelectedCCEmails.Any())
                        {
                            modelState.AddModelError("model.SelectedCCEmails", "Notification email is required");
                        }
                    }
                    break;
                case OrganizationSettingsSection.StaffResponder:
                    {
                        modelState.Remove("model.OldPassword");
                        modelState.Remove("model.NewPassword");
                        modelState.Remove("model.ConfirmPassword");
                        modelState.Remove("model.FirstName");
                        modelState.Remove("model.LastName");
                        modelState.Remove("model.Email");
                        modelState.Remove("model.Phone");
                        modelState.Remove("model.Name");
                        modelState.Remove("model.Site");
                        modelState.Remove("model.Address");
                        modelState.Remove("model.OnSiteCoordinatorEmail");

                        if (!model.ActiveStaffs.Any())
                        {
                            modelState.AddModelError("model.SelectedStaffResponder", "Staff email is required");
                        }
                        if (model.SelectedStaffResponder == 0)
                        {
                            modelState.Remove("model.SelectedStaffResponder");
                        }
                    }
                    break;
                case OrganizationSettingsSection.OnSiteCoordinator:
                    {
                        modelState.Remove("model.OldPassword");
                        modelState.Remove("model.NewPassword");
                        modelState.Remove("model.ConfirmPassword");
                        modelState.Remove("model.FirstName");
                        modelState.Remove("model.LastName");
                        modelState.Remove("model.Email");
                        modelState.Remove("model.Phone");
                        modelState.Remove("model.Name");
                        modelState.Remove("model.Site");
                        modelState.Remove("model.Address");
                    }
                    break;
            }
        }

        [HttpGet]
        public virtual JsonNetResult GetEmailContents()
        {
            var model = _clubSettingBusiness.GetEmailsContents(ActiveClub.Id);
            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveEmailContents(EmailsSettingViewModel model)
        {
            var result = _clubSettingBusiness.SaveEmailsContents(model, ActiveClub.Id);
            return Json(new JResult { Status = result.Status, Message = result.Message });
        }
        [HttpGet]
        public virtual JsonNetResult GetReportsContents()
        {
            var model = _clubSettingBusiness.GetReportsContents(ActiveClub.Id);
            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveReportsContents(ReportsSettingViewModel model)
        {
            var result = _clubSettingBusiness.SaveReportsContents(model, ActiveClub.Id);
            return Json(new JResult { Status = result.Status, Message = result.Message });
        }

        [HttpGet]
        public virtual JsonNetResult GetCatalogContents()
        {
           var model = _clubSettingBusiness.GetCatalogContents(ActiveClub.Id);
            return JsonNet(model);
        }

        [HttpPost]
        public virtual ActionResult SaveCatalogContents(CatalogsSettingViewModel model)
        {
            var result = _clubSettingBusiness.SaveCatalogContents(model, ActiveClub.Id);
            return Json(new JResult { Status = result.Status, Message = result.Message });
        }

        [JbAuthorize(JbAction.SchoolSetting_ProgramPolicy_View)]
        [HttpGet]
        public JsonNetResult GetProgramsPolicy()
        {
            var model = _clubSettingBusiness.GetProgramsPolicy(ActiveClub.Id);

            return JsonNet(model);
        }

        [HttpPost]
        public JsonNetResult SaveSubscriptionProgramPolicy(ProgramsPolicyViewModel model)
        {
            var result = _clubSettingBusiness.SaveSubscriptionProgramPolicy(model, ActiveClub.Id);

            return JsonNet(result);
        }

        [HttpPost]
        public JsonNetResult SaveGeneralProgramsPolicy(ProgramsPolicyViewModel model)
        {
            var result = _clubSettingBusiness.SaveGeneralProgramsPolicy(model, ActiveClub.Id);

            return JsonNet(result);
        }


        [JbAuthorize(JbAction.SchoolSetting_DuplicateEnrollment_View)]
        [HttpGet]
        public JsonNetResult GetDuplicateEnrollmentPolicy()
        {
            var model = _clubSettingBusiness.GetDuplicateEnrollmentPolicy(ActiveClub.Id);

            return JsonNet(model);
        }

        [JbAuthorize(JbAction.SchoolSetting_DuplicateEnrollment_Save)]
        [HttpPost]
        public JsonNetResult SaveDuplicateEnrollmentPolicy(DuplicateEnrollmentPolicySettingViewModel model)
        {
            var result = _clubSettingBusiness.SaveDuplicateEnrollmentPolicy(model, ActiveClub.Id);

            return JsonNet(result);
        }

        #region Validations
        private void AutoChargePolicyValidation(AutoChargePolicySettingViewModel model)
        {
            for (var i = 0; i < model.Attempts.Count; i++)
            {
                if (model.Attempts[i].FailedEmailToAdmin && string.IsNullOrWhiteSpace(model.Attempts[i].FailedAdminEmailTemplate))
                {
                    ModelState.AddModelError($"model.TotalAttempts[{i}].FailedAdminEmailTemplate", "Email template cannot be empty");
                }
                if (model.Attempts[i].FailedEmailToParent && string.IsNullOrWhiteSpace(model.Attempts[i].FailedParentEmailTemplate))
                {
                    ModelState.AddModelError($"model.TotalAttempts[{i}].FailedParentEmailTemplate", "Email template cannot be empty");
                }
            }

            if (model.SuccessEmailToAdmin && string.IsNullOrWhiteSpace(model.SuccessAdminEmailTemplate))
            {
                ModelState.AddModelError(nameof(model.SuccessAdminEmailTemplate), "Email template cannot be empty");
            }

            if (!model.HasReplyTo)
            {
                ModelState.Remove($"model.{nameof(model.ReplyToEmailAddress)}");
            }

        }

        private void DelinquentPolicyValidation(DelinquentPolicySettingViewModel model)
        {
            if (model.AutoChargeFailAttemptNumber == null || int.Parse(model.AutoChargeFailAttemptNumber) > model.AutoChargeOptions.Max(x => int.Parse(x.Value)))
            {
                ModelState.AddModelError(nameof(model.AutoChargeFailAttemptNumber), "This selection should be selected");
            }

            if (model.CalculateFamilyBalance && model.FamilyBalanceMinPrice < 0)
            {
                ModelState.AddModelError(nameof(model.FamilyBalanceMinPrice), "Minimum unpaid balance cannot be less than zero");
            }

            if (model.CalculateFamilyBalance && model.FamilyBalanceNoLimitDate == null)
            {
                ModelState.AddModelError(nameof(model.FamilyBalanceNoLimitDate), "Cut-off date cannot be empty");
            }

            if (model.CalculateFamilyBalance && model.FamilyBalanceNoLimitDate >= DateTime.UtcNow.AddDays(1).Date)
            {
                ModelState.AddModelError(nameof(model.FamilyBalanceNoLimitDate), "Cut-off date cannot be greater than current date");
            }

            if (!model.AllowEnrollOnDelinquent)
            {
                var doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(model.NotAllowEnrollTemplate ?? string.Empty);
                if (string.IsNullOrWhiteSpace(doc.DocumentNode.InnerText))
                    ModelState.AddModelError(nameof(model.NotAllowEnrollTemplate), "Value is required");
            }
        }
        #endregion
    }
}