﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Infrastructure;
using Jumbula.Core.Model.CommunicationHistory;
using System;
using System.Linq;

namespace Jumbula.Business
{
    public class CommunicationHistoryBusiness : ICommunicationHistoryBusiness
    {
        #region Constants

        private const string RemoveEmailMessage = "You cannot delete email history!";
        #endregion

        #region Fields
        private readonly IRepository<CommunicationHistory> _communicationHistoryRepository;
        private readonly IContactBusiness _contactBusiness;
        private readonly IJbDateTime _jbDateTime;
        private readonly IClubBusiness _clubBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        #endregion Fields

        #region Constructors
        public CommunicationHistoryBusiness(IRepository<CommunicationHistory> communicationHistoryRepository, IContactBusiness contactBusiness, IJbDateTime jbDateTime, IClubBusiness clubBusiness, IOrderItemBusiness orderItemBusiness, IOrderInstallmentBusiness orderInstallmentBusiness)
        {
            _communicationHistoryRepository = communicationHistoryRepository;
            _contactBusiness = contactBusiness;
            _jbDateTime = jbDateTime;
            _clubBusiness = clubBusiness;
            _orderItemBusiness = orderItemBusiness;
            _orderInstallmentBusiness = orderInstallmentBusiness;
        }
        #endregion Constructors

        #region Public methods

        public CommunicationHistoryViewModel Get(int id, int clubId)
        {
            return MapCommunicationHistoryViewModel(_communicationHistoryRepository.Get(id), clubId);
        }

        public IQueryable<CommunicationHistory> GetListOfContact(int contactId)
        {
            return _communicationHistoryRepository.GetList().Where(x => x.ContactId == contactId);
        }

        public IQueryable<CommunicationHistory> GetListOfEmail(int emailId)
        {
            return _communicationHistoryRepository.GetList().Where(x => x.EmailId == emailId);
        }
        public OperationStatus Delete(int id)
        {
            var communicationHistory = _communicationHistoryRepository.Get(id);

            if (communicationHistory.EmailId.HasValue)
                throw new Exception(RemoveEmailMessage);

            var contact = communicationHistory.Contact;

            var operationStatus = _communicationHistoryRepository.Delete(communicationHistory);

            if (contact.Histories == null || contact.Histories.Count == 0) _contactBusiness.Delete(contact);

            return operationStatus;
        }

        public OperationStatus Save(CommunicationHistoryViewModel communicationHistoryViewModel)
        {
            communicationHistoryViewModel.Date = _jbDateTime.UtcNow.ToString(Constants.DefaultDateTimeFormat);

            if (communicationHistoryViewModel.Id != 0)
                return Update(MapCommunicationHistory(communicationHistoryViewModel));

            return _communicationHistoryRepository.Create(MapCommunicationHistory(communicationHistoryViewModel));
        }

        public OperationStatus Update(CommunicationHistory entity)
        {
            var communicationHistory = _communicationHistoryRepository.Get(entity.Id);

            communicationHistory.Id = entity.Id;
            communicationHistory.ContactId = entity.ContactId;
            communicationHistory.EmailId = entity.EmailId;
            communicationHistory.RelatedEntity = entity.RelatedEntity;
            communicationHistory.RelatedEntityId = entity.RelatedEntityId;
            communicationHistory.Contact.Category = entity.Contact.Category;
            communicationHistory.Contact.CreatedDate = entity.Contact.CreatedDate;
            communicationHistory.Contact.Note = entity.Contact.Note;
            communicationHistory.Contact.SenderId = entity.Contact.SenderId;

            return _communicationHistoryRepository.Update(communicationHistory);
        }

        //todo: we should be add one sort method for this
        public PaginatedList<CommunicationHistoryViewModel> GetViewHistoryList(PaginationModel paginationModel, CommunicationHistoryFilterViewModel filters, int clubId)
        {
            var communicationHistory = _communicationHistoryRepository.GetList().Where(x => x.RelatedEntity == filters.RelatedEntity && filters.RelatedEntityId == x.RelatedEntityId);

            communicationHistory = communicationHistory.OrderBy(x => x.Id);

            var result = communicationHistory.ToList().Where(x => x.EmailId == null || x.Email.Recipients.Any(r =>
                                                               r.EmailAddress.Contains(GetEmailAddressOfEntity(x.RelatedEntity,
                                                                   x.RelatedEntityId)))).ToList();

            var communicationHistoryPaginated = result.Paginate(paginationModel.Page, paginationModel.PageSize);

            return communicationHistoryPaginated.Select(x => MapCommunicationHistoryViewModel(x, clubId)).ToPaginatedList(paginationModel.Page, paginationModel.PageSize, result.Count);
        }

        #endregion Public methods

        #region Private methods

        private CommunicationHistoryViewModel MapCommunicationHistoryViewModel(CommunicationHistory communicationHistory, int clubId)
        {
            return new CommunicationHistoryViewModel
            {
                Id = communicationHistory.Id,
                Date = GetClubDateTime(clubId, communicationHistory.Email?.DateCreated ?? communicationHistory.Contact.CreatedDate),
                Category = communicationHistory.Email != null ? ContactCategory.Email : communicationHistory.Contact.Category,
                SenderId = communicationHistory.Email != null ? communicationHistory.Email.UserId : communicationHistory.Contact.SenderId,
                Sender = communicationHistory.Email == null ? communicationHistory.Contact.Sender.UserName :
                    (communicationHistory.Email.User != null ? communicationHistory.Email.User.UserName : communicationHistory.Email.CallerType.ToString()),
                DeliverDateTime = communicationHistory.Email != null ? GetClubDateTime(clubId, communicationHistory.Email?.Recipients.FirstOrDefault(x => x.EmailAddress == GetEmailAddressOfEntity(communicationHistory.RelatedEntity, communicationHistory.RelatedEntityId))?.DeliverDate) : string.Empty,
                OpenDateTime = communicationHistory.Email != null ? GetClubDateTime(clubId, communicationHistory.Email?.Recipients.FirstOrDefault(x => x.EmailAddress == GetEmailAddressOfEntity(communicationHistory.RelatedEntity, communicationHistory.RelatedEntityId))?.OpenDate) : string.Empty,
                Note = communicationHistory.Email != null ? string.Empty : communicationHistory.Contact.Note,
                ContactId = communicationHistory.ContactId,
                EmailId = communicationHistory.EmailId,
                RelatedEntity = communicationHistory.RelatedEntity,
                RelatedEntityId = communicationHistory.RelatedEntityId
            };
        }

        private static CommunicationHistory MapCommunicationHistory(CommunicationHistoryViewModel communicationHistoryViewModel)
        {
            return new CommunicationHistory
            {
                Id = communicationHistoryViewModel.Id,
                ContactId = communicationHistoryViewModel.ContactId,
                EmailId = communicationHistoryViewModel.EmailId,
                RelatedEntity = communicationHistoryViewModel.RelatedEntity,
                RelatedEntityId = communicationHistoryViewModel.RelatedEntityId,
                Contact = communicationHistoryViewModel.Category != ContactCategory.Email && communicationHistoryViewModel.SenderId != null ? new Contact
                {
                    Category = communicationHistoryViewModel.Category,
                    CreatedDate = DateTime.Parse(communicationHistoryViewModel.Date),
                    SenderId = communicationHistoryViewModel.SenderId.Value,
                    Note = communicationHistoryViewModel.Note,
                    Id = communicationHistoryViewModel.ContactId ?? 0
                } : null
            };
        }

        private string GetEmailAddressOfEntity(string entityName, long entityId)
        {
            var email = string.Empty;

            switch (entityName)
            {
                case nameof(OrderItem):
                    email = _orderItemBusiness.GetItem(entityId).Order.User.UserName;
                    break;
                case nameof(OrderInstallment):
                    email = _orderInstallmentBusiness.Get(entityId).OrderItem.Order.User.UserName;
                    break;
            }

            return email;
        }

        private string GetClubDateTime(int clubId, DateTime? dateTime)
        {
            var clubDateTime = string.Empty;

            if (dateTime != null) clubDateTime = _clubBusiness.GetClubDateTime(clubId, dateTime.Value).ToString(Constants.DefaultDateTimeFormatWithAMPMWithoutComma);

            return clubDateTime;
        }

        #endregion Private methods
    }
}
