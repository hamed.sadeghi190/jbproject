﻿using Jb.Framework.Common;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Model
{
    public class JbResult
    {
        public JbResult(string message, bool status, object data)
        {
            Message = message;
            Status = status;
            Data = data;
        }

        public JbResult(OperationStatus operationStatus)
        {
            Message = operationStatus.Message;
            Status = operationStatus.Status;
            Data = operationStatus.Data;

            if (!Status)
            {
                if (operationStatus.Exception == null && string.IsNullOrEmpty(operationStatus.Message) && operationStatus.RecordsAffected == 0)
                {
                    Status = true;
                }
            }
        }

        public string Message { get; set; }

        public bool Status { get; set; }

        public object Data { get; set; }
    }
}
