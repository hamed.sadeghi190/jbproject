﻿using System.Data.Entity;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class ClientPaymentMethodBusiness : IClientPaymentMethodBusiness
    {
        private readonly IClubBusiness _clubBusiness;
        private readonly IRepository<ClientPaymentMethod, long> _clientPaymentMethodRepository;
        private readonly IEntitiesContext _dataContext;

        public ClientPaymentMethodBusiness(IClubBusiness clubBusiness, IRepository<ClientPaymentMethod, long> clientPaymentMethodRepository, IEntitiesContext dataContext)
        {
            _clubBusiness = clubBusiness;
            _clientPaymentMethodRepository = clientPaymentMethodRepository;
            _dataContext = dataContext;
        }


        public ClientPaymentMethod Get(long? clientId)
        {
            var result = new ClientPaymentMethod();
            if (!clientId.HasValue)
            {
                result.EnableAchPayment = false;
                result.EnableBankTransferPayment = false;
                result.EnableCashPayment = false;
                result.EnablePaypalPayment = false;
                result.EnableStripePayment = false;

                return result;
            }

            var club = _dataContext.Set<Club>().AsNoTracking().SingleOrDefault(c => c.ClientId == clientId);

            var hasPartner = club.PartnerId.HasValue;
            var partner = new Club();
            var partnerSettings = new PartnerSetting();
            ClientPaymentMethod partnerPaymentMethod = null;

            result = club.Client != null ? club.Client.PaymentMethods : null;

            if (hasPartner && club.IsSchool)
            {
                partner = club.PartnerClub;
                partnerSettings = _clubBusiness.GetPartnerSetting(partner.Id);
                partnerPaymentMethod = partner.Client.PaymentMethods;

                if (partnerPaymentMethod != null)
                {
                    if (partnerSettings == null || partnerSettings.PortalParameters == null || (partnerSettings != null && partnerSettings.PortalParameters != null && !partnerSettings.PortalParameters.ReadStripeSettingsFromMember))
                    {
                        if (result == null)
                        {
                            result = new ClientPaymentMethod();
                        }

                        result.AchPaymentCheckoutLabel = partnerPaymentMethod.AchPaymentCheckoutLabel;
                        result.AchPaymentInstruction = partnerPaymentMethod.AchPaymentInstruction;
                        result.AchPaymentMessage = partnerPaymentMethod.AchPaymentMessage;
                        result.EnableAchPayment = partnerPaymentMethod.EnableAchPayment;
                        result.EnableStripePayment = partnerPaymentMethod.EnableStripePayment;
                        result.EnableAuthorizePayment = partnerPaymentMethod.EnableAuthorizePayment;
                        result.AuthorizeLoginId = partnerPaymentMethod.AuthorizeLoginId;
                        result.AuthorizeTransactionKey = partnerPaymentMethod.AuthorizeTransactionKey;
                        result.IsCreditCardTestMode = partnerPaymentMethod.IsCreditCardTestMode;
                        result.StripeAccessToken = partnerPaymentMethod.StripeAccessToken;
                        result.CreditCardCheckoutLabel = partnerPaymentMethod.CreditCardCheckoutLabel;
                        result.StripeCustomerId = partnerPaymentMethod.StripeCustomerId;
                        result.CreditCardPreferredLabel = partnerPaymentMethod.CreditCardPreferredLabel;
                        result.StripeResponse = partnerPaymentMethod.StripeResponse;
                        result.Id = partnerPaymentMethod.Id;
                        result.DefaultPaymentGateway = partnerPaymentMethod.DefaultPaymentGateway;

                    }

                    result.BankTransferCheckoutLabel = partnerPaymentMethod.BankTransferCheckoutLabel;
                    result.BankTransferPaymentInstruction = partnerPaymentMethod.BankTransferPaymentInstruction;
                    result.BankTransferPaymentMessage = partnerPaymentMethod.BankTransferPaymentMessage;
                    result.CashCheckoutLabel = partnerPaymentMethod.CashCheckoutLabel;
                    result.CashPaymentInstruction = partnerPaymentMethod.CashPaymentInstruction;
                    result.CashPaymentMessage = partnerPaymentMethod.CashPaymentMessage;
                    result.EnableBankTransferPayment = partnerPaymentMethod.EnableBankTransferPayment;
                    result.EnableCashPayment = partnerPaymentMethod.EnableCashPayment;
                    result.EnableManualPayment = partnerPaymentMethod.EnableManualPayment;
                    result.EnablePaypalPayment = partnerPaymentMethod.EnablePaypalPayment;
                    result.IsPaypalTestMode = partnerPaymentMethod.IsPaypalTestMode;
                    result.ManualPaymentInstruction = partnerPaymentMethod.ManualPaymentInstruction;
                    result.ManualPaymentMessage = partnerPaymentMethod.ManualPaymentMessage;
                    result.ManualPaymentMethod = partnerPaymentMethod.ManualPaymentMethod;
                    result.ManualPaymentName = partnerPaymentMethod.ManualPaymentName;
                    result.PaypalCheckoutLabel = partnerPaymentMethod.PaypalCheckoutLabel;
                    result.PaypalEmail = partnerPaymentMethod.PaypalEmail;

                }
            }

            return result;
        }

        public ClientPaymentMethod GetByClub(int clubId)
        {
            var club = _clubBusiness.Get(clubId);

            return Get(club.Client != null ? club.Client.Id : (long?)null);
        }
    }
}
