﻿using Jumbula.Core.Domain;

namespace SportsClub.Models
{

    /// </remarks>
    public class PostalAddressAutoCompleteViewModel : Jb.Framework.Common.Forms.Model.PostalAddressAutoCompleteViewModel
    {
        private bool _showMap;       

        public PostalAddressAutoCompleteViewModel()
        {

        }

        public PostalAddressAutoCompleteViewModel(bool showMap)
        {
            _showMap = showMap;
            Lat = 37.708333;
            Lng = -122.280278;
        }

        public PostalAddressAutoCompleteViewModel(PostalAddress model, bool showMap)
        {
            _showMap = showMap;
            Lat = model.Lat;
            Lng = model.Lng;
            Street_number = model.StreetNumber;
            Route = model.Route;
            Locality = model.City;
            Administrative_area_level_1 = model.State;
            Country = model.Country;
            Postal_code = model.Zip;
            AutoCompletedAddress = model.AutoCompletedAddress;
        }

      
        public PostalAddress ToPostalAddress()
        {
            var add = new PostalAddress
            {
                Lat = Lat,
                Lng = Lng,
                StreetNumber = Street_number,
                Route = Route,
                City = Locality,
                State = Administrative_area_level_1,
                Country = Country,
                Zip = Postal_code,
                AutoCompletedAddress = AutoCompletedAddress,
                Address = GetCompleteAddress()
            };

            return add;
        }
    }
}