﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programRespondToInvitationController', [ '$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function($scope, $http, $log, $stateParams, $state,  jbToast) {
        $scope.baseUrl = window.location.origin + "/Dashboard/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

      

        $scope.seasonName = $stateParams.seasonDomain;

        $http.get($scope.baseUrl + '/RespondToInvitationProgram', { params: { programId: $stateParams.programId } }).success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function() {
            
        });

        $scope.save = function () {

            $http.post($scope.baseUrl + '/RespondToInvitationProgram', { model: $scope.Model })
               .success(function (data, status, headers, config) {
                   if (data.Status == true) {
                       jbToast.success("Response is successfully submitted.", "");
                       $state.go('Programs', { seasonDomain: $stateParams.seasonDomain });
                   }
                   else {
                       $scope.MC.handleErrors($scope, data);
                   }
               });
        };


    }]);
})();