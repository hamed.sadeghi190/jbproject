﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('reportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbConfirmation) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.reportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetAllReports?seasonDomain=' + $scope.MC.stateParams.seasonDomain,
                        type: "POST",
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            //serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.reportListGridOptions = {
            dataSource: $scope.reportsDataSource,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            sortable: true,
            columns: [
                {
                    field: "Title",
                    title: "Report name",
                    template: "<a ui-sref='ReportEdit({reportId: #=Value#, seasonDomain:\"" + $scope.MC.stateParams.seasonDomain + "\"})'  class='block'> #= Title # </a>",
                }, {
                    field: "",
                    title: "",
                    template: "<a ui-sref='CustomReportSelectPrograms({seasonDomain: \"" + $scope.MC.stateParams.seasonDomain + "\", reportId: #= Value #, reportType:\"Custom\", reportName:\"#= Title #\"})' class='block'><i class='icon-play on-left'></i>Run</a>",
                }, {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				                    <ul><li ui-sref='ReportEdit({reportId: #=Value#, seasonDomain:\"" + $scope.MC.stateParams.seasonDomain + "\"})' class='k-item k-state-default k-first' role='menuitem'>\
						                <span class='k-link jbi-pencil'>Edit</span>\
				                        </li>" +
                                        "<li ng-click='deleteReport(#=Value#)' class='k-item k-state-default' role='menuitem'>\
						                <span class='k-link jbi-remove'>Delete</span>\
				                        </li>" +
                                    "</ul>\
			                        </li>\
		                      </ul>",
                    attributes: {
                        class: "grid-actions"
                    }
                }
            ],
        };

        $scope.deleteReport = function (reportId) {
            jbConfirmation.yes(function () {
                $http.post(baseUrl + '/DeleteReport', { reportId: reportId }).success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.reportListGridOptions.dataSource.read();
                    }
                });
            }).confirm();
        }

    }]);
})();