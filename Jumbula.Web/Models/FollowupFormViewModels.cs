﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class FollowUpFormUploadViewModel
    {
        public long OrderItemId { get; set; }
        public int FormId { get; set; }
        public string FileNames { get; set; }
        public string UId { get; set; }
        public string FileUrls { get; set; }

        public string[] FileNamesList
        {
            get
            {
                if (string.IsNullOrEmpty(FileNames))
                {
                    return null;
                }
                return FileNames.Split(';');
            }
        }
        public string[] FileUrlsList
        {
            get
            {
                if (string.IsNullOrEmpty(FileUrls))
                {
                    return null;
                }
                return FileUrls.Split(';');
            }
        }
    }
    public class FollowUpFormDownlodViewModel
    {
        public long OrderItemId { get; set; }
        public int FormId { get; set; }
        public string UId { get; set; }
        public List<FollowUpDownlodFiles> FollowUpFiles { get; set; }

    }
    public class FollowUpDownlodFiles
    {
        public int FormId { get; set; }
        public string ElementId { get; set; }
        public string filename { get; set; }
        public string fileUrl { get; set; }
    }
}