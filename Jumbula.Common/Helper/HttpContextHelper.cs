﻿using System.Web;

namespace Jumbula.Common.Helper
{
    public static class HttpContextHelper
    {
        public static bool IsFromAngular(this HttpRequestBase httpRequest)
        {
            if (httpRequest.Headers["From-Angular"] != null && bool.Parse(httpRequest.Headers["From-Angular"]))
            {
                return true;
            }

            return false;
        }
    }
}
