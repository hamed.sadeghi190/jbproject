﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models.SendGrid_Webhook_Model
{
    public class Webhook
    {
        public string email { get; set; }
        public int timestamp { get; set; }
        public int uid { get; set; }
        public int id { get; set; }
        public string sendgrid_event_id { get; set; }
        [JsonProperty("smtp-id")] 
        public string smtp_id { get; set; }
        public string sg_message_id { get; set; }
        [JsonProperty("event")] 
        public string sendgrid_event { get; set; }
        public string type { get; set; }
        public IList<string> category { get; set; }
        public string reason { get; set; }
        public string status { get; set; }
        public string url { get; set; }
        public string useragent { get; set; }
        public string ip { get; set; }
        
        // Add your custom fields here
        public int CampaignId { get; set; }

        public int CampaignRecipientsId { get; set; }

        public DateTime TimeStampToDateTime(double timeStamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime = dateTime.AddSeconds(timeStamp).ToLocalTime();
            return dateTime;
        }
    }
}