Alter table Charges Add [SeasonId] [bigint] NULL

GO

ALTER TABLE [dbo].[Charges]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Charges_dbo.Seasons_SeasonId] FOREIGN KEY([SeasonId])
REFERENCES [dbo].[Seasons] ([Id])