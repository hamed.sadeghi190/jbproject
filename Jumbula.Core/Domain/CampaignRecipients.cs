﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class CampaignRecipients : BaseEntity
    {
        //public int Id { get; set; }

        [StringLength(128)]
        public string RecipientName { get; set; }

        [StringLength(128)]
        public string RecipientLastName { get; set; }

        [NotMapped]
        public string FullName
        {
            get { return string.Format("{0}{1}", RecipientName, RecipientLastName); }
        }

        [StringLength(128)]
        public string EmailAddress { get; set; }

        public bool IsOpened { get; set; }

        public bool IsClicked { get; set; }

        public DateTime? OpenDate { get; set; }

        public DateTime? ClickDate { get; set; }

        public string Reason { get; set; }

        public EmailStatus EmailStatus { get; set; }

        [ForeignKey("CampaignId")]
        public virtual Campaign Campaign { get; set; }

        public int CampaignId { get; set; }

        public RecipeientType RecipeintType { get; set; }

        public string AttributesSerialized { get; set; }

        [NotMapped]
        public CampaignRecipeientAttribute Attribute
        {
            get
            {
                switch (Campaign.CampaignType)
                {
                    case CampaignReciepientsMode.AllUsers:
                    case CampaignReciepientsMode.SomePeople:
                    case CampaignReciepientsMode.UserList:
                        return null;
                    case CampaignReciepientsMode.ClassConfirmation:
                       return JsonConvert.DeserializeObject<ClassConfirmationAttribute>(AttributesSerialized);
                    default:
                        break;
                }

                return null;
            }
        }
    }

    public abstract class CampaignRecipeientAttribute
    {

    }

    public class ClassConfirmationAttribute : CampaignRecipeientAttribute
    {
        public string ParticipantName { get; set; }

        public string ClassName { get; set; }

        public string ParentName { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string OnSiteCoordinatorName { get; set; }

        public string OnSiteCoordinatorEmail { get; set; }

        public string ClassDates { get; set; }

        public string UpcommingClassDates { get; set; }

        public string Room { get; set; }

        public string SchoolWebsite { get; set; }
        public string InformationWebsite { get; set; }
        public string FaqWebsite { get; set; }
        public string ContactUsWebsite { get; set; }

        public string DayAndDate { get; set; }
    }

    public enum RecipeientType : byte
    {
        To = 0,
        CC = 1
    }
}
