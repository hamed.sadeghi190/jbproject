﻿
using Jb.Framework.Common;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Model.CreditCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Constants = Jumbula.Common.Constants.Constants;
using Jumbula.Core.Model.Generic;

namespace Jumbula.Business
{
    public class UserCreditCardBusiness : IUserCreditCardBusiness
    {
        #region Fields
        private readonly IRepository<UserCreditCard> _userCreditCardRepository;
        private readonly IClientBusiness _clientBusiness;
        #endregion

        public UserCreditCardBusiness(IRepository<UserCreditCard> userCreditCardRepository, IClientBusiness clientBusiness)
        {
            _userCreditCardRepository = userCreditCardRepository;
            _clientBusiness = clientBusiness;
        }

        private IQueryable<UserCreditCard> GetActiveList()
        {
            return _userCreditCardRepository.GetList(c => c.IsDeleted == false);
        }
        private IQueryable<UserCreditCard> GetAllList()
        {
            return _userCreditCardRepository.GetList();
        }
        public IQueryable<UserCreditCard> GetListwithCustomerId()
        {
            return GetActiveList().Where(u => !string.IsNullOrEmpty(u.CustomerId));
        }
        public IQueryable<UserCreditCard> GetAllList(int userId)
        {
            return _userCreditCardRepository.GetList().Where(u => u.UserId == userId && u.IsDeleted == false);
        }
        public IQueryable<UserCreditCard> GetAllList(int userId, PaymentGateway typeGateway)
        {
            return _userCreditCardRepository.GetList().Where(u => u.UserId == userId && u.IsDeleted == false && u.TypePaymentGateway == typeGateway);
        }
        public UserCreditCard Get(int cradId)
        {
            return GetActiveList().SingleOrDefault(u => u.Id == cradId);
        }

        public UserCreditCard Get(ParentBillingMethodsViewModel source, int userId, PaymentMethod method, int creditId, PaymentGateway typePayment, PostalAddress postalAddress = null)
        {
            if (userId > 0)
            {
                if (postalAddress == null)
                {
                    postalAddress = source.PostalAddress;
                }

                if (method != PaymentMethod.Express)
                {
                    var cards = GetAllList(userId, typePayment);
                    var name = source.CardHolderName;
                    name = name.Replace(" ", "");

                    if (!cards.Any()) return null;
                    var userCard = cards.FirstOrDefault(c => c.Address.Address == postalAddress.Address && c.LastDigits == source.Last4Digit && c.Name.Replace(" ", "").Trim() == name.Trim() && c.ExpiryMonth == source.Month && c.ExpiryYear == source.Year);
                    if (userCard != null)
                    {
                        return userCard;
                    }
                }
                else
                {
                    var cards = Get(creditId);
                    if (cards != null)
                    {
                        return cards;
                    }
                }

            }

            return null;
        }

        public UserCreditCard GetWithCardId(string creditCardId)
        {
            return GetActiveList().SingleOrDefault(u => u.CardId == creditCardId);
        }

        public string GetLastDigit(string cardNumber)
        {
            if (!string.IsNullOrWhiteSpace(cardNumber))
            {
                return cardNumber.Substring((cardNumber.Length) - 4, 4);
            }

            return string.Empty;
        }
        public bool IsExist(string cardNumber, int userId, PaymentGateway typePayment)
        {
            if (string.IsNullOrEmpty(cardNumber))
            {
                return false;
            }

            var lastDigit = cardNumber.Substring((cardNumber.Length) - 4, 4);
            return GetUserCreditCards(userId, typePayment).Any(u => u.LastDigits == lastDigit);
        }
        public UserCreditCard GetExist(int id, int userId, PaymentGateway typePayment)
        {
            if (id <= 0)
            {
                return null;
            }

            //var lastDigit = cardNumber.Substring((cardNumber.Length) - 4, 4);
            return GetUserCreditCards(userId, typePayment).SingleOrDefault(u => u.Id == id);
        }
        public bool IsExistAll(string cardNumber, int userId, PaymentGateway typeGateway)
        {
            if (string.IsNullOrEmpty(cardNumber))
            {
                return false;
            }

            var lastDigit = cardNumber.Substring((cardNumber.Length) - 4, 4);
            return GetAllList(userId, typeGateway).Any(u => u.LastDigits == lastDigit);
        }
        public IQueryable<UserCreditCard> GetCreditCards(string cardNumber, int userId, PaymentGateway typePayment)
        {
            if (string.IsNullOrEmpty(cardNumber))
            {
                return null;
            }

            var lastDigit = cardNumber.Substring((cardNumber.Length) - 4, 4);

            return GetAllList(userId, typePayment).Where(u => u.LastDigits == lastDigit);
        }
        public UserCreditCard Get(string stripeCustomerId)
        {
            return GetActiveList().SingleOrDefault(u => u.CustomerId.ToLower() == stripeCustomerId.ToLower());
        }

        public IQueryable<UserCreditCard> GetParentCreditCards(int userId)
        {
            return GetActiveList().Where(u => u.UserId == userId && !string.IsNullOrEmpty(u.CustomerId));
        }
        public IQueryable<UserCreditCard> GetUserCreditCards(int userId, PaymentGateway typeGateway)
        {
            return GetActiveList().Where(u => u.UserId == userId && !string.IsNullOrEmpty(u.CustomerId) && u.TypePaymentGateway == typeGateway);
        }

        public IQueryable<UserCreditCard> GetUserCreditCards(int userId, int clubId)
        {
            var paymentGateway = _clientBusiness.GetPaymentGatewayClub(clubId);
            return GetUserCreditCards(userId, paymentGateway);
        }

        public IQueryable<UserCreditCard> GetUserCreditCards(int userId)
        {

            return GetActiveList().Where(u => !string.IsNullOrEmpty(u.CustomerId) && u.UserId == userId);
        }

        public IQueryable<UserCreditCard> GetUserCreditCards(List<int> userIds)
        {

            return GetActiveList().Where(u => !string.IsNullOrEmpty(u.CustomerId) && userIds.Contains(u.UserId));
        }

        public UserCreditCard GetActiveUserCreditCards(int userId, PaymentGateway typeGateway)
        {
            return GetActiveList().FirstOrDefault(u => u.UserId == userId && !string.IsNullOrEmpty(u.CustomerId) && u.IsDefault == true && u.TypePaymentGateway == typeGateway);
        }
        public UserCreditCard GetLastUserCreditCards(int userId)
        {
            return GetActiveList().Last(u => u.UserId == userId && !string.IsNullOrEmpty(u.CustomerId));
        }
        public UserCreditCard GetListCard(int userId)
        {
            return GetAllList().FirstOrDefault(u => u.UserId == userId && !string.IsNullOrEmpty(u.CustomerId));
        }

        public OperationStatus Delete(int userCreditCardId)
        {
            OperationStatus res = new OperationStatus() { Status = false, Message = Constants.PlayerProfile_Ajax_Request_Failed };
            var creditCard = _userCreditCardRepository.GetList().First(c => c.Id == userCreditCardId);
            var userCreditCards = _userCreditCardRepository.GetList().Where(c => c.UserId == creditCard.UserId);

            var selectedCreditCard = Get(userCreditCardId);
            var anotherCreditCard = userCreditCards.SingleOrDefault(c => c.LastDigits == selectedCreditCard.LastDigits && c.TypePaymentGateway != selectedCreditCard.TypePaymentGateway && c.IsDeleted == false);

            if (userCreditCards.Any(c => c.Id == userCreditCardId))
            {
                selectedCreditCard.IsDeleted = true;
                selectedCreditCard.DeletedDate = DateTime.UtcNow;
                if (anotherCreditCard != null)
                {
                    anotherCreditCard.IsDeleted = true;
                    anotherCreditCard.DeletedDate = DateTime.UtcNow;
                }

            }

            return _userCreditCardRepository.Save();

        }
        public OperationStatus ActivateUserCreditCard(int userId, int cardId)
        {
            var creditCard = _userCreditCardRepository.GetList().First(c => c.Id == cardId);
            var userCreditCards = _userCreditCardRepository.GetList().Where(c => c.UserId == userId);

            OperationStatus res = new OperationStatus() { Status = false, Message = Constants.PlayerProfile_Ajax_Request_Failed };
            var selectedCreditCard = Get(cardId);
            var anotherCreditCard = userCreditCards.SingleOrDefault(c => c.LastDigits == selectedCreditCard.LastDigits && c.TypePaymentGateway != selectedCreditCard.TypePaymentGateway && c.IsDeleted == false);

            if (userCreditCards != null && userCreditCards.Any())
            {
                if (userCreditCards.Any(c => c.IsDefault))
                {
                    foreach (var item in userCreditCards.Where(c => c.IsDefault && c.Id != cardId))
                    {
                        item.IsDefault = false;
                    }
                }
                if (userCreditCards.Any(c => c.Id == cardId))
                {
                    selectedCreditCard.IsDefault = true;
                    selectedCreditCard.DefaultDate = DateTime.UtcNow;

                    if (anotherCreditCard != null) //if card Authorize was in db 
                    {
                        anotherCreditCard.IsDefault = true;
                        anotherCreditCard.DefaultDate = DateTime.UtcNow;
                    }

                    res = _userCreditCardRepository.Save();
                }
            }

            return res;
        }

        public int CountWords(string strInput)
        {
            int intCount = 0;
            for (int i = 1; i < strInput.Length; i++)
            {
                if (char.IsWhiteSpace(strInput[i - 1]) == true)
                {
                    if (char.IsLetterOrDigit(strInput[i]) == true ||
                        char.IsPunctuation(strInput[i]))
                    {
                        intCount++;
                    }
                }
            }
            if (strInput.Length > 2)
            {
                intCount++;
            }
            return intCount;
        }

        public OperationStatus Update(UserCreditCard userCreditCard)
        {
            return _userCreditCardRepository.Update(userCreditCard);
        }

        public OperationStatus Create(UserCreditCard userCreditCard)
        {
            return _userCreditCardRepository.Create(userCreditCard);
        }

        public List<SelectListItem> GetKeyValueUserCreditCards(int userId, int clubId)
        {
            var creditCards = GetUserCreditCards(userId, clubId);

            if (creditCards.Any())
            {
                return creditCards.OrderByDescending(c => c.IsDefault).ToList().Select(c =>
                          new SelectListItem
                          {
                              Text = $"{c.Brand} {" - ending with " + c.LastDigits}",
                              Value = c.Id.ToString()
                          })
                      .ToList();
            }

            return new List<SelectListItem>();

        }

    }
}
