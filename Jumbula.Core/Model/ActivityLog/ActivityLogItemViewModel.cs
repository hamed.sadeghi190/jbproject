﻿namespace Jumbula.Core.Model.ActivityLog
{
    public class ActivityLogItemViewModel
    {
        public string Id { get; set; }

        public string Action { get; set; }

        public string Description { get; set; }

        public string DateTime { get; set; }

        public string UserName { get; set; }

        public string Status { get; set; }
    }
}
