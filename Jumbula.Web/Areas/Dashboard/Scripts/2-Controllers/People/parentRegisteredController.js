﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('parentRegisteredController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'parent-registered-view';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/People';

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });

        $scope.searchTerm = "";
        $scope.SearchTypes = [{ Text: 'Name', Value: 'Name' }, { Text: 'Family email', Value: 'Email' }, { Text: 'Confirmation ID', Value: 'ConfirmationId' }, { Text: 'Phone', Value: 'Phone' }];
        $scope.SearchType = 'Name';

        $scope.Model = {};

        $http.get(window.location.origin + "/Dashboard/OverView/GetAccountInfo")
            .success(function (data, status, headers, config) {
                $scope.Model.HasPartner = data.HasPartner;

                if (data.HasPartner) {
                    $scope.SeasonNames = [{ Value: "Fall", Title: "Fall" }, { Value: "Winter", Title: "Winter" }, { Value: "Spring", Title: "Spring" }, { Value: "Summer", Title: "Summer" }];
                    $scope.SeasonName = "Fall";
                    $scope.Year = 0;
                    getYears();

                    $scope.SearchTypes.push({ Text: 'Season', Value: 'Season' });
                    getAllPartnerSchools();
                }

                prepareGrid();
            });

        var prepareGrid = function () {

            $scope.parentGridDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                {
                    read: {
                        url: baseUrl + '/GetParentRegistered',
                        type: "POST",
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                searchType: $scope.SearchType,
                                term: $scope.searchTerm,
                                firstName: $scope.firstName,
                                lastName: $scope.lastName,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort,
                                year: $scope.Year,
                                seasonName: $scope.SeasonName,
                                selectedSchools: $scope.Model._SelectedSchools
                            }
                        }
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverFiltering: true,
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
            });

            $scope.parentGridOptions = {
                excel: {
                    allPages: true,
                    fileName: "Parents.xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                dataSource: $scope.parentGridDataSource,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100
                    ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    var grid = $("#parentGrid").data("kendoGrid");

                    grid.hideColumn("ClubName");

                    if ($scope.Model.HasPartner) {
                        grid.showColumn("ClubName");
                    }
                },
                sortable: true,
                columns: [
                    {
                        field: "FullName",
                        title: "Full name",
                    },
                    {
                        field: "PhoneNumber",
                        title: "Phone",
                    },
                    {
                        field: "Email",
                        title: "Email",
                    },
                    {
                        field: "ClubName",
                        title: "School",
                        width: 130
                    },
                    {
                        title: "Actions",
                        width: "70px",
                        headerTemplate: "<span class='actions-title'>Actions</span>",
                        template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				                <ul><li ui_sref='ParentDetail({userId: #=UserId# , playerId: #= PlayerId #})' class='k-item k-state-default k-first' role='menuitem'>\
						                    <span class='k-link jbi-pencil'>View details</span>\
				                        </li></ul>\
			                        </li>\
		                        </ul>",
                        attributes: {
                            class: "grid-actions"
                        }
                    }
                ],
            };
        }

        $scope.parentRegisteredSearch = function () {
            $scope.parentGridOptions.dataSource.read();
        }

        $scope.search = function () {
            $scope.parentGridOptions.dataSource.page(1);
            $("#parentGrid").data("kendoGrid").dataSource.read();
        };

        $scope.clearFilters = function () {
            $scope.SearchType = 'Name';
            $scope.searchTerm = "";
            $scope.firstName = "";
            $scope.lastName = "";
            $scope.SeasonName = "Fall";
            $scope.Year = new Date().getFullYear().toString();
            $scope.Model._SelectedSchools = [];
            $scope.parentGridOptions.dataSource.page(1);
            $("#parentGrid").data("kendoGrid").dataSource.read();
        };

        function getYears() {
            $http.get(baseUrl + '/GetCurrentYearsFrom2015').success(function (data, status, headers, config) {

                $scope.Years = data.Years;
                $scope.Year = new Date().getFullYear().toString();
            }).error(function (data, status, headers, config) {
                jbToast.error("error " + data);
            });
        }

        function getAllPartnerSchools() {
            $http.get(baseUrl + '/GetAllSchools')
                .success(function (data, status, headers, config) {
                    $scope.AllSchools = data.AllSchools;
                    $scope.Model._SelectedSchools = [''];
                });
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#parentGrid").getKendoGrid().saveAsExcel();
            cfpLoadingBar.complete();
        }

    }]);
})();