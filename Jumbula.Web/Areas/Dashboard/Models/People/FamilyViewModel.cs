﻿using SportsClub.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FamilyViewModel 
    {
       
        public  int UserId{get;set;}

        [EmailAddress(ErrorMessage = "Email format is invalid.")]
        public string UserName{get;set;}
        public bool HasParent { get; set; }
        public bool HasParticipant { get; set; }
        public int TotalOrders { get; set; }
        public decimal TotalPayments { get; set; }
        public decimal Balance { get; set; }
        public decimal Credit { get; set; }
        public string TagName { get; set; }
        public List<JbTitleValue> StatusReason { get; set; }
        public List<JbTitleValue> Participants { get; set; }
        public int ClubId { get; set; }
    }

}