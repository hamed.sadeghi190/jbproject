﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class SeasonSettings
    {
        public SeasonProgramInfoSetting SeasonProgramInfoSetting { get; set; }
        public SeasonFormsSetting SeasonFormsSetting { get; set; }
        public SeasonAdditionalSetting SeasonAdditionalSetting { get; set; }
    }
    public enum SeasonStage
    {

        [Description("Planning summary")]
        PlanningSummary = 1,
        [Description("Invitations issued")]
        InvitationsIssued = 2,
        [Description("Tentative schedule")]
        TentativeSchedule = 3,
        [Description("Confirmed schedule")]
        ConfirmedSchedule = 4,
        [Description("Canceled")]
        Canceled = 5
    }

    public class SeasonSchoolSetting : SeasonSettings
    {
        public SchoolSeasonGeneralSetting SchoolSeasonGeneralSetting { get; set; }
        public SchoolSeasonDetailSetting SchoolSeasonDetailSetting { get; set; }
        public SchoolSeasonscheduleSetting SchoolSeasonscheduleSetting { get; set; }

    }
    public class SchoolSeasonGeneralSetting : BaseEntity
    {

        public DateTime? MakeUpOpeningDate { get; set; }
        public DateTime? MakeUpClosingDate { get; set; }
        public bool AMClassesPossible { get; set; }
        public DateTime? AMClassesStartTime { get; set; }
        public DateTime? PMClassesStartTime { get; set; }
        public DateTime? PMClassesStartTimeEarlyDismissal { get; set; }
        public SeasonStage? Stage { get; set; }
    }

    public class SchoolSeasonDetailSetting : BaseEntity
    {
        public List<string> ListOfBackpackFlierDates { get; set; }
        public List<string> ListOfEmailBlastDates { get; set; }

        public DateTime? InstructorAssignmentDeadline { get; set; }
        public DateTime? InstructorListDelivery { get; set; }
        public DateTime? RostersDue { get; set; }
        public DateTime? PaymentsIssued { get; set; }
        public DateTime? SurveyOpens { get; set; }
        public DateTime? SurveyCloses { get; set; }
        public DateTime? SurveyResultsDue { get; set; }
    }

    public class SchoolSeasonscheduleSetting : BaseEntity
    {
        public bool Monday { get; set; }
        public int MondayNumberOfClasses { get; set; }
        public List<string> ListOfMondayDates { get; set; }

        public bool Tuesday { get; set; }
        public int TuesdayNumberOfClasses { get; set; }
        public List<string> ListOfTuesdayDates { get; set; }

        public bool Wednesday { get; set; }
        public int WednesdayNumberOfClasses { get; set; }
        public List<string> ListOfWednesdayDates { get; set; }

        public bool Thursday { get; set; }
        public int ThursdayNumberOfClasses { get; set; }
        public List<string> ListOfThursdayDates { get; set; }

        public bool Friday { get; set; }
        public int FridayNumberOfClasses { get; set; }
        public List<string> ListOfFridayDates { get; set; }

        public List<string> ListOfNoClassDates { get; set; }
    }

    public class SeasonProgramInfoSetting : BaseEntity
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public DateTime? EarlyRegistrationOpens { get; set; }
        public DateTime? EarlyRegistrationCloses { get; set; }
        public DateTime? GeneralRegistrationOpens { get; set; }
        public DateTime? GeneralRegistrationCloses { get; set; }
        public DateTime? LateRegistrationOpens { get; set; }
        public DateTime? LateRegistrationCloses { get; set; }
        public decimal LateRegistrationFee { get; set; }
        public string LateRegistrationFeeMessage { get; set; }
        public int DefaultProgramCategoryId { get; set; }

        public bool HasEarlyRegisterPIN { get; set; }
        [MaxLength(64)]
        public string EarlyRegisterPIN { get; set; }
        [MaxLength(1024)]
        public string EarlyRegisterPINMessage { get; set; }

        public bool HasGeneralRegisterPIN { get; set; }
        [MaxLength(64)]
        public string GeneralRegisterPIN { get; set; }
        [MaxLength(1024)]
        public string GeneralRegisterPINMessage { get; set; }

        public bool HasLateRegisterPIN { get; set; }
        [MaxLength(64)]
        public string LateRegisterPIN { get; set; }
        [MaxLength(1024)]
        public string LateRegisterPINMessage { get; set; }

        public bool HasEarlyRegistration { get; set; }
        public bool HasLateRegistration { get; set; }

        public bool HasPreRegistration { get; set; }
        public DateTime? PreRegistrationOpens { get; set; }
        public DateTime? PreRegistrationCloses { get; set; }

        public bool HasLottery { get; set; }
        public DateTime? LotteryOpens { get; set; }
        public DateTime? LotteryCloses { get; set; }

        public DateTime? MiscellaneousInformationRostersDue { get; set; }
        public DateTime? MiscellaneousInformationPaymentsIssued { get; set; }


    }

    public class SeasonFormsSetting : BaseEntity
    {
        public int? Form { get; set; }

        public List<int> Waivers { get; set; }
    }

    public class SeasonAdditionalSetting : BaseEntity
    {
        public bool IsWaitListAvailable { get; set; }

        public string WaitlistPolicy { get; set; }

        public bool ShowRemainingSpots { get; set; }

        public int CapacityLeftNumbers { get; set; }
    }
}
