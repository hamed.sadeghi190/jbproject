﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class OrderItemFollowupForm : BaseEntity
    {
        //[Required]
        
        [Required]
        [MaxLength(128)]
        public string FormName { get; set; }

        [Required]
        public string Guid { get; set; }

        [Required]
        public FollowupStatus Status { get; set; }

        [Required]
        public long ISTS { get; set; }

        public FormType FormType { get; set; }

        public virtual ICollection<OrderItem> OrderItem { get; set; }

        public int JbFormId { get; set; }

        [ForeignKey("JbFormId")]
        public virtual JbForm JbForm { get; set; }

        public FollowUpFormMode FollowUpFormMode { get; set; }

    }

    public enum FollowupStatus : byte
    {
        [Display(Name = "Not filled")]
        [Description("Not filled")]
        NotFill = 0,
        [Display(Name = "Completed")]
        [Description("Completed")]
        Completed = 1
    }
}
