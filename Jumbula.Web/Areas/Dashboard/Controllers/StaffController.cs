﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.UserService;
using Microsoft.AspNet.Identity.Owin;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using SportsClub.Models.Email;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class StaffController : DashboardBaseController
    {

        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly IUserProfileBusiness _userProfileBusiness;

        public StaffController(IClubBusiness clubBusiness, IApplicationUserManager<JbUser, int> applicationUserManager, IUserProfileBusiness userProfileBusiness)
        {
            _applicationUserManager = applicationUserManager;

            _applicationUserManager.UserTokenProvider = new DataProtectorTokenProvider<JbUser, int>(
                new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Sample").Create(
                    "EmailConfirmation"))
            {
                TokenLifespan = TimeSpan.FromDays(15.0)
            };

            _userProfileBusiness = userProfileBusiness;
        }


        public virtual ActionResult ManageStaffs()
        {
            return View();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Staff_Edit)]
        public virtual ActionResult AddEditStaff()
        {
            return View();
        }

        [HttpGet]
        //[JbAuthorize(JbAction.Staff_Add_From_Partner)]
        public virtual ActionResult AddStaffFromPartner()
        {
            return View();
        }

        [HttpGet]
        [JbAuthorize(JbAction.Staff_View)]
        public virtual ActionResult GetStaff(int? id)
        {
            PageMode pageMode = id.HasValue ? PageMode.Edit : PageMode.Create;

            var model = new CreateEditStaffViewModel();

            model.AllRoles = DropdownHelpers.ToSelectList<RoleCategory>();

            model.AllStaffTitles.Add(new SelectKeyValue<string>() { Text = "Select", Value = null });

            var staffTitlesSorted = Ioc.ClubBusiness.GetStaffTitles(DropdownHelpers.ToSelectList<StaffTitle>());

            model.AllStaffTitles.AddRange(staffTitlesSorted);

            var currentUserRole = this.GetCurrentUserRole();

            var club = this.GetCurrentClubBaseInfo();

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(currentUserRole, club.ClubType);

            model.AllRoles = model.AllRoles.Where(r => managableRoles.Select(m => m.ToString()).Contains(r.Value)).ToList();

            switch (pageMode)
            {
                case PageMode.Create:
                    {
                        model.PageMode = PageMode.Create;
                        model.RoleCategory = currentUserRole;

                        break;
                    }
                case PageMode.Edit:
                    {
                        model.PageMode = PageMode.Edit;

                        var staff = Ioc.ClubBusiness.GetStaff(id.Value, ActiveClub.Id);
                        CheckResourceForAccess(staff, true);

                        if (!managableRoles.Select(r => r.ToString()).Contains(staff.JbUserRole.Role.Name))
                        {
                            throw new Exception("Illegal staff change.");
                        }

                        model.Id = staff.UserRoleId;

                        if (staff.Contact != null)
                        {
                            model.FirstName = staff.Contact.FirstName;
                            model.LastName = staff.Contact.LastName;
                            model.PhoneNumber = staff.Contact.Phone;
                            model.Title = staff.Contact.Title;
                            model.ExtensionPhone = staff.Contact.PhoneExtension;
                        }

                        model.DeteOfBackgrandCheck = staff.DateOfBackgroundCheck;

                        model.RoleCategory = EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name);

                        break;
                    }
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_Add)]
        public virtual ActionResult InviteStaff(CreateEditStaffViewModel model)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(this.GetCurrentUserRole(), club.ClubType);

            if (!managableRoles.Contains(model.RoleCategory))
            {
                throw new Exception("Illegal role assignment.");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var clubStaffs = club.ClubStaffs;
                    

                    var isUserExists = _applicationUserManager.IsUserExist(model.EmailAddress.ToLower());

                    if (!(club.ClubStaffs.Where(s => !s.IsDeleted).Any(s => s.ClubId == club.Id && s.JbUserRole.User.UserName.Equals(model.EmailAddress))))
                    {
                        if (!isUserExists)
                        {
                            var jbUser = new JbUser
                            {
                                UserName = model.EmailAddress.ToLower(),
                                Email = model.EmailAddress
                            };

                            var res = _applicationUserManager.Create(jbUser);
                        }

                        var userId = _userProfileBusiness.Get(model.EmailAddress.ToLower()).Id;

                        var roleId = _userProfileBusiness.GetRole(model.RoleCategory).Id;

                        var staff = new ClubStaff()
                        {
                            ClubId = ActiveClub.Id,
                            DateOfBackgroundCheck = model.DeteOfBackgrandCheck,
                            JbUserRole = new JbUserRole()
                            {
                                RoleId = roleId,
                                UserId = userId
                            },

                            Contact = new ContactPerson()
                            {
                                FirstName = model.FirstName,
                                LastName = model.LastName,
                                Phone = model.PhoneNumber,
                                Title = model.Title,
                                PhoneExtension = model.ExtensionPhone
                            },
                        };

                        if (model.RoleCategory == RoleCategory.Partner)
                        {
                            var adminRoleId = _userProfileBusiness.GetRole(RoleCategory.Admin).Id;
                            var newRoleForUser = new JbUserRole()
                            {
                                RoleId = adminRoleId,
                                UserId = userId
                            };

                            var res = Ioc.ClubBusiness.CreateUserRole(newRoleForUser);
                        }

                        Ioc.ClubBusiness.CreateStaff(staff);

                        if (club.ClubType.EnumType == ClubTypesEnum.Partner && _applicationUserManager.IsEmailConfirmed(userId) && staff.RoleOnClub != RoleCategory.Partner)
                        {
                            ConfirmStaffWithAlreadyConfirmedEmailForPartner(staff);
                        }
                        else
                        {
                            string token = _applicationUserManager.GenerateEmailConfirmationToken(userId);

                            var user = staff.JbUserRole.User;
                            user.Token = token;
                            _userProfileBusiness.Update(user);

                            SendConfirmationEmail(club, model.EmailAddress, string.Format("{0} {1}", model.FirstName, model.LastName), model.RoleCategory, token);

                            if (club.PartnerId.HasValue)
                            {
                                var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(club.PartnerId.Value);

                                if (partnerSetting.PortalParameters.EmailProviderAfterInvitinganInstructor != false && partnerSetting.PortalParameters.EmailContent != null && model.RoleCategory == RoleCategory.Instructor)
                                {
                                    SendEmailInviteStaffForProvider(model.EmailAddress, club.ContactPersons.FirstOrDefault().Email, string.Format("{0} {1}", model.FirstName, model.LastName), model.RoleCategory, token, partnerSetting.PortalParameters.EmailContent);
                                }
                            }
                        }

                        return Json(new JResult { Status = true, Message = "token", RecordsAffected = 1 });
                    }
                    else
                    {
                        ModelState.AddModelError("EmailAddress", string.Format("Sorry, it looks like {0} belongs to an existing staff.", model.EmailAddress));
                    }
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", "Error");
                }
            }

            return base.JsonFormResponse();
        }

        private void ConfirmStaffWithAlreadyConfirmedEmailForPartner(ClubStaff staff)
        {
            var result = Ioc.ClubBusiness.SetStaffActive(staff);

            if (result.Status)
            {
                SendConfirmationEmail(staff.Club, staff.JbUserRole.User.UserName, string.Format("{0} {1}", staff.Contact.FirstName, staff.Contact.LastName), staff.RoleOnClub, "", true, true);
            }
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_Edit)]
        public virtual ActionResult EditStaff(CreateEditStaffViewModel model)
        {
            var clubBusiness = Ioc.ClubBusiness;

            var club = clubBusiness.Get(ActiveClub.Id);

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(this.GetCurrentUserRole(), club.ClubType);

            if (!managableRoles.Contains(model.RoleCategory))
            {
                throw new Exception("Illegal role assignment.");
            }

            ModelState.Remove("model.EmailAddress");
            ModelState.Remove("model.ConfirmEmailAddress");

            if (ModelState.IsValid)
            {
                var staff = club.ClubStaffs.Single(s => s.JbUserRole.Id == model.Id);
                CheckResourceForAccess(staff);

                if (staff.Contact == null)
                {
                    staff.Contact = new ContactPerson();
                }

                if (!clubBusiness.IsStaffCanChange(staff))
                {
                    if (EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name) != model.RoleCategory)
                    {
                        var message = string.Empty;
                        if (EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name) == RoleCategory.Partner)
                        {
                            message = "You cannot edit, freeze or delete the last partner.";
                        }
                        else
                        {
                            message = "You cannot edit, freeze or delete the last administrator.";
                        }

                        return Json(new JResult { Status = false, Message = message, RecordsAffected = 0 });
                    }
                }

                staff.Contact.FirstName = model.FirstName;
                staff.Contact.LastName = model.LastName;
                staff.Contact.Phone = model.PhoneNumber;
                staff.Contact.Title = model.Title;
                staff.DateOfBackgroundCheck = model.DeteOfBackgrandCheck;
                staff.Contact.PhoneExtension = model.ExtensionPhone;

                var roleId = _userProfileBusiness.GetRole(model.RoleCategory).Id;

                if (model.RoleCategory == RoleCategory.Partner && staff.JbUserRole.RoleId != roleId)
                {
                    var adminRoleId = _userProfileBusiness.GetRole(RoleCategory.Admin).Id;
                    var newRoleForUser = new JbUserRole()
                    {
                        RoleId = adminRoleId,
                        UserId = staff.JbUserRole.UserId
                    };

                    var res = Ioc.ClubBusiness.CreateUserRole(newRoleForUser);
                }

                staff.JbUserRole.RoleId = roleId;

                var result = clubBusiness.Update(club);

                return Json(new JResult { Status = true, Message = "", RecordsAffected = 1 });
            }

            return base.JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_View)]
        public virtual JsonNetResult GetStaffs(PaginationModel paginationModel, List<GridSort> sort = null)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var isSchoolClub = Ioc.ClubBusiness.IsClubSchool(club.ClubType);

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(this.GetCurrentUserRole(), club.ClubType).Select(r => r.ToString());

            var model = Ioc.ClubBusiness.GetStaffs(ActiveClub.Id).Where(c => !c.IsDeleted && (c.JbUserRole.Role.Name != RoleCategory.SchoolContributor.ToString() || (c.JbUserRole.Role.Name == RoleCategory.SchoolContributor.ToString() && isSchoolClub)))
                .Select(p =>
                    new RoleItemViewModel
                    {
                        Id = p.UserRoleId,
                        FirstName = p.ContactId.HasValue ? p.Contact.FirstName : string.Empty,
                        LastName = p.ContactId.HasValue ? p.Contact.LastName : string.Empty,
                        Email = p.JbUserRole.User.UserName,
                        Role = (EnumHelper.ParseEnum<RoleCategory>(p.JbUserRole.Role.Name)).ToDescription(),
                        Status =  p.IsFrozen ? "Frozen" : p.Status.ToDescription() ,
                        Title = p.ContactId.HasValue && p.Contact.Title.HasValue ? (EnumHelper.ParseEnum<StaffTitle>(p.Contact.Title.ToString())).ToDescription() : string.Empty,
                        Manageable = managableRoles.Contains(p.JbUserRole.Role.Name),
                        IsFrozen = p.IsFrozen
                    })
                    .ToList();

            if (sort != null)
            {
                foreach (var s in sort)
                {
                    switch (s.field)
                    {
                        case "FullName":
                            {
                                model = (s.dir == "asc") ? model.OrderBy(o => o.FullName).ToList() : model.OrderByDescending(o => o.FullName).ToList();
                                break;
                            }
                        case "Title":
                            {
                                model = (s.dir == "asc") ? model.OrderBy(o => o.Title).ToList() : model.OrderByDescending(o => o.Title).ToList();
                                break;
                            }
                        case "Email":
                            {
                                model = (s.dir == "asc") ? model.OrderBy(o => o.Email).ToList() : model.OrderByDescending(o => o.Email).ToList();
                                break;
                            }
                        case "Role":
                            {
                                model = (s.dir == "asc") ? model.OrderBy(o => o.Role).ToList() : model.OrderByDescending(o => o.Role).ToList();
                                break;
                            }
                        case "Status":
                            {
                                model = (s.dir == "asc") ? model.OrderBy(o => o.Status).ToList() : model.OrderByDescending(o => o.Status).ToList();
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
            else
            {
                model = model.OrderBy(m => m.FullName).ToList();
            }

            var dataSource = model.Page(paginationModel).ToList();

            return JsonNet(new { DataSource = dataSource, TotalCount = model.Count() });
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_Delete)]
        public virtual ActionResult Delete(int id)
        {
            OperationStatus result = null;

            var clubBusiness = Ioc.ClubBusiness;

            var club = clubBusiness.Get(ActiveClub.Id);

            var staff = club.ClubStaffs.Single(s => s.JbUserRole.Id == id);

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(this.GetCurrentUserRole(), club.ClubType).Select(r => r.ToString());

            if (!managableRoles.Contains(staff.JbUserRole.Role.Name))
            {
                throw new Exception("Illegal staff change.");
            }

            if (!clubBusiness.IsStaffCanChange(staff))
            {
                var message = string.Empty;
                if (EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name) == RoleCategory.Partner)
                {
                    message = "You cannot edit, freeze or delete the last partner.";
                }
                else
                {
                    message = "You cannot edit, freeze or delete the last administrator.";
                }

                return Json(new JResult { Status = false, Message = message, RecordsAffected = 0 });
            }

            if (club.Setting.StaffResponder == staff.Id)
            {
                return Json(new JResult { Status = false, Message = "You cannot delete the staff member because this person is designated to respond to the communications massages.", RecordsAffected = 0 });
            }

            if (staff.JbUserRole.Role.Name == RoleCategory.Instructor.ToString() && clubBusiness.IsInstructorIsInUse(staff.Id))
            {
                return Json(new JResult { Status = false, Message = "This staff is in used as an instructor.", RecordsAffected = 0 });
            }
            else
            {
                result = clubBusiness.DeleteStaff(staff.Id);
            }

            if (result.Status)
            {
                return Json(new JResult { Status = true, Message = "Staff deleted successfully.", RecordsAffected = 1 });
            }

            return base.JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_Active)]
        public virtual ActionResult ManualActivation(int id)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var staff = club.ClubStaffs.Single(s => s.JbUserRole.Id == id);

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(this.GetCurrentUserRole(), club.ClubType).Select(r => r.ToString());

            if (!managableRoles.Contains(staff.JbUserRole.Role.Name))
            {
                throw new Exception(Common.Constants.ExceptionMessage.IllegalStaffChange);
            }

            if (ModelState.IsValid)
            {
                var pass = StringCipher.Encrypt(club.Domain + staff.JbUserRole.User.UserName, DateTime.UtcNow.ToShortDateString().Replace("/", ""));

                staff.Status = StaffStatus.Active;
                staff.JbUserRole.User.EmailConfirmed = true;
                staff.JbUserRole.User.PasswordHash = pass;

                var result = Ioc.ClubBusiness.Update(club);

                if (result.Status)
                {
                    return Json(new JResult { Status = true, Message = Common.Constants.Validation.ActivateStaff, RecordsAffected = 1 });
                }
            }

            return base.JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_Freeze)]
        public virtual ActionResult Freeze(int id)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var staff = club.ClubStaffs.Single(s => s.JbUserRole.Id == id);

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(this.GetCurrentUserRole(), club.ClubType).Select(r => r.ToString());

            if (!managableRoles.Contains(staff.JbUserRole.Role.Name))
            {
                throw new Exception("Illegal staff change.");
            }

            if (!Ioc.ClubBusiness.IsStaffCanChange(staff))
            {
                var message = string.Empty;
                if (EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name) == RoleCategory.Partner)
                {
                    message = "You cannot edit, freeze or delete the last partner.";
                }
                else
                {
                    message = "You cannot edit, freeze or delete the last administrator.";
                }

                return Json(new JResult { Status = false, Message = message, RecordsAffected = 0 });
            }

            if (club.Setting.StaffResponder == staff.Id)
            {
                return Json(new JResult { Status = false, Message = "You cannot freeze the staff member because this person is designated to respond to the communications massages.", RecordsAffected = 1 });
            }

            if (ModelState.IsValid)
            {
                staff.IsFrozen = true;

                var result = Ioc.ClubBusiness.Update(club);

                if (result.Status)
                {
                    return Json(new JResult { Status = true, Message = "Staff freezed successfully.", RecordsAffected = 1 });
                }
            }

            return base.JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_Freeze)]
        public virtual ActionResult Unfreeze(int id)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var staff = club.ClubStaffs.Single(s => s.JbUserRole.Id == id);

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(this.GetCurrentUserRole(), club.ClubType).Select(r => r.ToString());

            if (!managableRoles.Contains(staff.JbUserRole.Role.Name))
            {
                throw new Exception("Illegal staff change.");
            }

            staff.IsFrozen = false;

            var result = Ioc.ClubBusiness.Update(club);

            if (result.Status)
            {
                return Json(new JResult { Status = true, Message = "Staff unfreezed successfully.", RecordsAffected = 1 });
            }

            return base.JsonFormResponse();
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_ResendInvitation)]
        public virtual ActionResult ResendInvitation(int id)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            var staff = club.ClubStaffs.Single(s => s.JbUserRole.Id == id);

            var managableRoles = Ioc.SecurityBusiness.GetManageableDasboardRolesWithRole(this.GetCurrentUserRole(), club.ClubType).Select(r => r.ToString());

            if (!managableRoles.Contains(staff.JbUserRole.Role.Name))
            {
                throw new Exception("Illegal staff change.");
            }

            var email = staff.JbUserRole.User.UserName;
            var userId = staff.JbUserRole.UserId;
            string fullName = string.Empty;
            var user = staff.JbUserRole.User;

            if (staff.ContactId.HasValue)
            {
                fullName = staff.Contact.FullName;
            }

            string token = _applicationUserManager.GenerateEmailConfirmationToken(userId);
            user.Token = token;
            
            _userProfileBusiness.Update(user);

            SendConfirmationEmail(club, email, fullName, EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name), token);

            return Json(new JResult { Status = true, Message = "Invitation email sent successfully.", RecordsAffected = 1 });

        }

        [HttpGet]
        [JbAuthorize(JbAction.Staff_Add_From_Partner)]
        public ActionResult GetPartnerStaffs()
        {
            var model = new AddStaffFromMemberViewModel();

            model.AllStaffs = Ioc.ClubBusiness.GetAddablePartnerStaffsForMember(ActiveClub.Id).Select(s =>
                                                    new SelectKeyValue<int>
                                                    {
                                                        Text = s.JbUserRole.User.UserName,
                                                        Value = s.Id
                                                    })
                                                    .ToList();


            return JsonNet(model);
        }

        public ActionResult GetSettings()
        {
            dynamic model = new ExpandoObject();
            model.CanAddFromPartner = false;

            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);
            if (Ioc.ClubBusiness.AdminIsFromPartner(this.GetCurrentUserName(), this.GetCurrentClubId()) && allowedActions.Contains(JbAction.Staff_Add_From_Partner))
            {
                model.CanAddFromPartner = true;
            }

            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.Staff_Add_From_Partner)]
        public ActionResult AddPartnerStaff(AddStaffFromMemberViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = Ioc.ClubBusiness.AddPartnerStaffToMember(ActiveClub.Id, model.SelectedStaff.Value);

                if (model.SendNotificationMessage)
                {
                    var staff = Ioc.ClubBusiness.GetStaff(model.SelectedStaff.Value);

                    var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                    SendConfirmationEmail(club, staff.JbUserRole.User.UserName, staff.Contact.FullName, EnumHelper.ParseEnum<RoleCategory>(staff.JbUserRole.Role.Name), string.Empty, true);
                }

                return JsonNet(new JResult(result));
            }

            return JsonFormResponse();
        }

        private void SendConfirmationEmail(Club club, string email, string fullName, RoleCategory role, string token, bool addFromPartner = false, bool alredyActived = false)
        {
            var emailModel = new ConfirmationEmail();

            string confirmUrl = string.Empty;

            string hostUrl = Request.Url.Host;

            string basixUrl = string.Format("{0}://{1}", Request.Url.Scheme, hostUrl);

            string segments = string.Format("{0}/{1}?userName={2}&token={3}", "Account", "ConfirmStaffEmail", HttpUtility.UrlEncode(email), HttpUtility.UrlEncode(token));

            if (!addFromPartner)
            {
                confirmUrl = string.Format("{0}/{1}", basixUrl, segments);
            }
            else
            {
                confirmUrl = basixUrl;
                emailModel.InvaiteFromPartner = true;
            }

            #region Build email body

            string subject = string.Empty;
            MailAddress from = null;

            emailModel.HomePageUrl = ActiveClub.Site;// externalClub.WebSiteUrl;
            emailModel.HomeTitle = ActiveClub.Name;
            emailModel.LogoUrl = ActiveClub.Logo;

            subject = string.Format("Confirm your {0} account, {1}!", emailModel.HomeTitle, fullName);
            from = new MailAddress(Constants.W_NoReplyEmailAddress, ActiveClub.Name);

            emailModel.RequestType = this.GetCurrentRequestType();
            emailModel.UserFullName = !string.IsNullOrEmpty(fullName) ? fullName : email;
            emailModel.VerificationLink = confirmUrl;
            emailModel.ClubName = ActiveClub.Name;
            emailModel.Role = role;
            emailModel.IsEmailAlreadyActived = alredyActived;
            emailModel.ExpirationDate = DateTime.UtcNow.Date.AddDays(15).AddMinutes(-1);

            if (role == RoleCategory.Instructor || role == RoleCategory.OnsiteCoordinator || role == RoleCategory.OnsiteSupportManager)
            {
                emailModel.ShowPlayStoreIconForStaff = true;
            }

            #endregion

            string body = this.RenderViewToString(@"~/Views/Shared/Email/InviteStaffConfirmationEmail.cshtml", emailModel);

            Ioc.OldEmailBusiness.SendConfirmationEmail(from, new MailAddress(email), subject, body);

            if (WebConfigHelper.IsProductionMode)
            {
                Ioc.OldEmailBusiness.SendEmail(from, new MailAddress(Constants.W_BCC_EmailNotification), subject, body, true);
            }
        }

        private void SendEmailInviteStaffForProvider(string emailStaff, string email, string fullName, RoleCategory role, string token, string note)
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            string confirmUrl = string.Empty;

            string hostUrl = Request.Url.Host;

            string basixUrl = string.Format("{0}://{1}", Request.Url.Scheme, hostUrl);

            string segments = string.Format("{0}/{1}?userName={2}&token={3}", "Account", "ConfirmStaffEmail", HttpUtility.UrlEncode(email), HttpUtility.UrlEncode(token));

            confirmUrl = string.Format("{0}/{1}", basixUrl, segments);

            #region Build email body
            var emailModel = new ConfirmationEmail();
            string subject = string.Empty;
            MailAddress from = null;
            emailModel.Message = note;
            emailModel.HomePageUrl = ActiveClub.Site;// externalClub.WebSiteUrl;
            emailModel.HomeTitle = ActiveClub.Name;
            emailModel.LogoUrl = ActiveClub.Logo;

            subject = string.Format("Instructor joining to your staff");
            from = new MailAddress(Constants.W_NoReplyEmailAddress, ActiveClub.Name);

            emailModel.RequestType = this.GetCurrentRequestType();
            emailModel.UserFullName = !string.IsNullOrEmpty(fullName) ? fullName : email;
            emailModel.VerificationLink = confirmUrl;
            emailModel.ClubName = ActiveClub.Name;
            emailModel.ClubNamePartner = club.PartnerClub.Name;
            emailModel.Role = role;
            emailModel.EmailStaff = emailStaff;
            emailModel.ExpirationDate = DateTime.UtcNow.Date.AddDays(15).AddMinutes(-1);

            if (role == RoleCategory.Instructor || role == RoleCategory.OnsiteCoordinator || role == RoleCategory.OnsiteSupportManager)
            {
                emailModel.ShowPlayStoreIconForStaff = true;
            }
            #endregion

            string body = this.RenderViewToString(@"~/Views/Shared/Email/InviteStaffProviderEmail.cshtml", emailModel);

            Ioc.OldEmailBusiness.SendConfirmationEmail(from, new MailAddress(email), subject, body);
            if (WebConfigHelper.IsProductionMode)
            {
                Ioc.OldEmailBusiness.SendEmail(from, new MailAddress(Constants.W_BCC_EmailNotification), subject, body, true);
            }
        }

    }
}