﻿using System.Collections.Generic;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Model.Discount
{
    public class MultiScheduleDiscount
    {
        public int PlayerId { get; set; }
        public long ProgramId { get; set; }
        public IEnumerable<OrderItem> OrderItems { get; set; }
    }
}
