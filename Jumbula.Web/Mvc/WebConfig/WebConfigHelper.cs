﻿using Jumbula.Common.Enums;
using System.Collections.Specialized;
using System.Net.Configuration;
using System.Web.Configuration;

namespace Jumbula.Web.Mvc.WebConfig
{
    public static class WebConfigHelper
    {
        public static string SecureHttpProtocol { get; private set; }
        public static SmtpSection SmtpMailSettings { get; private set; }
        public static bool RecaptchaModeEnabled { get; private set; }
        public static string AzureDbConnection { get; private set; }
        public static string AzureStorageUrl { get; private set; }
        public static string AzureStorageConnection { get; private set; }


        public static string Paypal_ApiEndPoint_Sig_Live { get; set; }
        public static string Paypal_CmdApiEndPoint_Sig_Live { get; set; }
        public static string Paypal_ApiUsername_Live { get; private set; }
        public static string Paypal_ApiPassword_Live { get; private set; }
        public static string Paypal_ApiSignature_Live { get; private set; }
        public static string Paypal_AppId_Live { get; private set; }
        public static string Paypal_ApiEndPoint_Sig_Test { get; set; }
        public static string Paypal_CmdApiEndPoint_Sig_Test { get; set; }
        public static string Paypal_ApiUsername_Test { get; private set; }
        public static string Paypal_ApiPassword_Test { get; private set; }
        public static string Paypal_ApiSignature_Test { get; private set; }
        public static string Paypal_AppId_Test { get; private set; }
        public static string Paypal_ApiVersion { get; private set; }
        public static string Paypal_JumbulaEmail { get; private set; }
        public static string Paypal_Test_JumbulaEmail { get; private set; }
        public static string Stripe_PublishableKey_Live { get; private set; }
        public static string Stripe_SecretKey_Live { get; private set; }
        public static string Stripe_ClientId_Live { get; private set; }
        public static string Stripe_SecretKey { get; private set; }
        public static string Stripe_PublishableKey_Test { get; private set; }
        public static string Stripe_SecretKey_Test { get; private set; }
        public static string Stripe_ClientId_Test { get; private set; }

        public static string Stripe_TestPayment_ConnectedId { get; private set; }
        public static string Stripe_TestPayment_AccessToken { get; private set; }

        public static string EmailCampaign_UserName { get; set; }
        public static string EmailCampaign_Password { get; set; }

        public static string LuceneBlobName { get; private set; }

        public static string MailChimpAPIKey { get; private set; }
        public static string MailChimpContactListID { get; private set; }
        public static string MailChimpNewsletterListID { get; private set; }
        public static string MailChimpTrialListID { get; private set; }

        public static DeploymentEnvironment DeploymentEnvironment { get; private set; }

        public static bool IsProductionMode { get => DeploymentEnvironment == DeploymentEnvironment.Production; }

        // Facebook Variables
        public static string FBAppID { get; private set; }
        public static string FBAppSecret { get; private set; }
        public static string FBRedirectUri { get; private set; }
        public static string FBScope { get; private set; }
        public static bool NoFlowNoIndex { get; private set; }
        public static string Plaid_PublicKey { get; internal set; }
        public static string Plaid_SecretKey { get; internal set; }
        public static string Plaid_ClientId { get; internal set; }
        public static string Plaid_LocalEndPoint { get; internal set; }
        public static string Plaid_ProdEndPoint { get; internal set; }
        public static string Plaid_DevEndPoint { get; internal set; }
        public static string Plaid_ExchangeApi { get; internal set; }
        public static string Plaid_BankAccountTokenApi { get; internal set; }
        public static string AuthorizeNet_LoginId_Test { get; internal set; }
        public static string AuthorizeNet_SecretKey_Test { get; internal set; }
        public static string AuthorizeNet_Transaction_Test { get; internal set; }
        public static string InsightlyAPIKey { get; private set; }
        public static string Sendgrid_ApiKey { get; set; }
        public static string GoogleMapAPIKey { get; private set; }
        public static string GoogleRecaptchaAPIKey { get; private set; }
        public static string SeqApiKey { get; private set; }
        public static string SeqServerUrl { get; private set; }

        public static string GATrackingId { get; set; }
        static WebConfigHelper()
        {
            bool AzureStorageEnabledRes = false;
            bool RecaptchaModeEnabledRes = false;

            SmtpMailSettings = WebConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;
            var jumbulaSettings = WebConfigurationManager.GetSection("jumbula") as NameValueCollection;
            var jumbulaSecSettings = WebConfigurationManager.GetSection("jumbulaSec") as NameValueCollection;

            // Parse all settings from the jumbula section

            SecureHttpProtocol = jumbulaSettings["SecureHttpProtocol"];
            bool ignoreResultCaptcha = bool.TryParse(jumbulaSettings["RecaptchaModeEnabled"], out RecaptchaModeEnabledRes);
            RecaptchaModeEnabled = RecaptchaModeEnabledRes;
            DeploymentEnvironment =
                Common.Helper.EnumHelper.ParseEnum<DeploymentEnvironment>(jumbulaSettings["DeploymentEnvironment"]);

            // Note: AzureDbConnection is used in SportsClubDb.cs, any changes to its name must be reflected there
            AzureDbConnection = jumbulaSettings["AzureDbConnection"];
            bool ignoreResult = bool.TryParse(jumbulaSettings["AzureStorageEnabled"], out AzureStorageEnabledRes);
            AzureStorageUrl = jumbulaSettings["AzureStorageUrl"];
            AzureStorageConnection = jumbulaSettings["AzureStorageConnection"];

            Paypal_ApiEndPoint_Sig_Live = jumbulaSettings["Paypal_ApiEndPoint_Sig_Live"];
            Paypal_ApiEndPoint_Sig_Test = jumbulaSettings["Paypal_ApiEndPoint_Sig_Test"];
            Paypal_ApiPassword_Live = jumbulaSettings["Paypal_ApiPassword_Prod"];
            Paypal_ApiPassword_Test = jumbulaSettings["Paypal_ApiPassword_Test"];
            Paypal_ApiSignature_Live = jumbulaSettings["Paypal_ApiSignature_Prod"];
            Paypal_ApiSignature_Test = jumbulaSettings["Paypal_ApiSignature_Test"];
            Paypal_ApiUsername_Live = jumbulaSettings["Paypal_ApiUsername_Prod"];
            Paypal_ApiUsername_Test = jumbulaSettings["Paypal_ApiUsername_Test"];
            Paypal_AppId_Live = jumbulaSettings["Paypal_AppID_Prod"];
            Paypal_AppId_Test = jumbulaSettings["Paypal_AppID_Test"];
            Paypal_CmdApiEndPoint_Sig_Live = jumbulaSettings["Paypal_CmdApiEndPoint_Sig_Live"];
            Paypal_CmdApiEndPoint_Sig_Test = jumbulaSecSettings["Paypal_CmdApiEndPoint_Sig_Test"];

            Stripe_PublishableKey_Live = jumbulaSettings["Stripe_Publishable_Key_Prod"];
            Stripe_SecretKey_Live = jumbulaSettings["Stripe_Secret_Key_Prod"];
            Stripe_ClientId_Live = jumbulaSettings["Stripe_Client_Id_Prod"];
            Stripe_PublishableKey_Test = jumbulaSettings["Stripe_Publishable_Key_Test"];
            Stripe_SecretKey_Test = jumbulaSettings["Stripe_Secret_Key_Test"];
            Stripe_ClientId_Test = jumbulaSettings["Stripe_Client_Id_Test"];
            Stripe_SecretKey = jumbulaSettings["Stripe_Secret_Key"];
            Stripe_SecretKey = jumbulaSettings["Stripe_Secret_Key"];

            AuthorizeNet_SecretKey_Test = jumbulaSettings["AuthorizeNet_Secret_Key_Test"];
            AuthorizeNet_LoginId_Test = jumbulaSettings["AuthorizeNet_Login_Id_Test"];
            AuthorizeNet_Transaction_Test = jumbulaSettings["AuthorizeNet_Transaction_Key_Test"];

            Paypal_ApiVersion = jumbulaSettings["Paypal_ApiVersion"];
            Paypal_JumbulaEmail = jumbulaSettings["Paypal_JumbulaEmail"];
            Paypal_Test_JumbulaEmail = jumbulaSettings["Paypal_Test_JumbulaEmail"];
            Stripe_TestPayment_AccessToken = jumbulaSettings["Stripe_TestPayment_AccessToken"];
            Stripe_TestPayment_ConnectedId = jumbulaSettings["Stripe_TestPayment_ConnectedId"];


            // mailchimp
            MailChimpAPIKey = jumbulaSettings["MailChimpAPIKey"];
            MailChimpTrialListID = jumbulaSettings["MailChimpTrialListID"];
            MailChimpNewsletterListID = jumbulaSettings["MailChimpNewsletterListID"];
            MailChimpContactListID = jumbulaSettings["MailChimpContactListID"];

            // Insightly
            InsightlyAPIKey = jumbulaSettings["InsightlyAPIKey"];

            // EmailCampaign_Sendgrid

            EmailCampaign_UserName = jumbulaSecSettings["EmailCampaign_UserName"];
            EmailCampaign_Password = jumbulaSecSettings["EmailCampaign_Password"];
            Sendgrid_ApiKey = jumbulaSettings["Sendgrid_ApiKey"];
            //Plaid
            Plaid_PublicKey = jumbulaSettings["Plaid_PublicKey"];
            Plaid_SecretKey = jumbulaSettings["Plaid_SecretKey"];
            Plaid_ClientId = jumbulaSettings["Plaid_ClientId"];
            Plaid_LocalEndPoint = jumbulaSettings["Plaid_LocalEndPoint"];
            Plaid_ProdEndPoint = jumbulaSettings["Plaid_ProdEndPoint"];
            Plaid_DevEndPoint = jumbulaSettings["Plaid_DevEndPoint"];
            Plaid_ExchangeApi = jumbulaSettings["Plaid_ExchangeApi"];
            Plaid_BankAccountTokenApi = jumbulaSettings["Plaid_BankAccountTokenApi"];

            bool noFlowNoIndex;
            bool.TryParse(jumbulaSettings["NoFlowNoIndex"], out noFlowNoIndex);
            NoFlowNoIndex = noFlowNoIndex;

            GoogleMapAPIKey = jumbulaSettings["Google_Map_API_Key"];
            GoogleRecaptchaAPIKey = jumbulaSettings["Google_Recaptcha_API_Key"];

            SeqApiKey = jumbulaSettings["Seq_API_Key"];
            SeqServerUrl = jumbulaSettings["Seq_Server_Url"];

            GATrackingId = jumbulaSettings["GA_Tracking_Id"];
        }
    }
}