﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class State : BaseEntity
    {

        [StringLength(64)]
        public string Name { get; set; }

        [StringLength(16)]
        public string Code { get; set; }

        public virtual ICollection<County> Counties { get; set; }

        [ForeignKey("Country_Id")]
        public virtual Country Country { get; set; }

        public int Country_Id { get; set; }
    }
}
