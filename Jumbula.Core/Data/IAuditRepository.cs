﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Data
{
    public interface IAuditRepository : IRepository<JbAudit, Guid>
    {
        PaginatedList<JbAudit> GetAdminAudits(string clubDomain, Expression<Func<JbAudit, bool>> predicate,
            List<Sort> sorts, int pageIndex, int pageSize);
    }
}
