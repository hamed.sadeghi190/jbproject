﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ClubTypesEnum
    {
        [Description("SportClub")]
        SportClub = 1,
        [Description("School")]
        School = 2,
        [Description("Provider")]
        Provider = 3,
        [Description("Individual")]
        Individual_3 = 4,
        [Description("Corp")]
        Corp_3 = 5,
        [Description("Public")]
        Public_2 = 6,
        [Description("Private")]
        Private_2 = 7,
        [Description("Title 1")]
        Title1_2 = 8,
        [Description("Charter")]
        Charter_2 = 9,
        [Description("Partner")]
        Partner = 10,
        [Description("SchoolDistrict")]
        SchoolDistrict = 11

    }
}
