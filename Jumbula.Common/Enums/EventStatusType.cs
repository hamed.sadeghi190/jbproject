﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jumbula.Common.Enums
{
    public enum EventStatusType
    {
        None,
        Open,
        [Display(Name = "Open, in progress")]
        [Description("Open, in progress")]
        OpenInProgress,
        [Display(Name = "Closed")]
        [Description("Closed")]
        Closed,

        //[Description("Coming soon")]
        //ComingSoon,
    }
}
