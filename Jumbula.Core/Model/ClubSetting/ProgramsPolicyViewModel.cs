﻿
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Jumbula.Core.Model.ClubSetting
{
    public class ProgramsPolicyViewModel
    {
       public SubcriptionPolicyModel SubcriptionPolicy { get; set; }
        public GeneralPolicyModel GeneralPolicy { get; set; }

    }

    public class SubcriptionPolicyModel
    {
        public bool EnabledDesiredStartDateProrate { get; set; }

        public bool EnabledCancellationProrate { get; set; }

        public bool EnabledDowngradeProrate { get; set; }

        public bool EnabledUpgradeProrate { get; set; }
    }

    public class GeneralPolicyModel
    {
        public bool EnabledRefundAutomaticallyInCancellationOrder { get; set; }
        public bool EnabledCancellationFee { get; set; }

        public bool EnabledTransferFee { get; set; }

        public ProrationMethod SelectedProrationMethod { get; set; }

        public List<SelectListItem> ProrationMethods { get; set; }
    }
}
