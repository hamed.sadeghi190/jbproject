﻿using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FollowupReportPortal
    {
        public List<long> _SelectedPrograms { get; set; }
        public List<long> _SelectedClubs { get; set; }
        public SeasonNames SeasonName { get; set; }
        public int Year { get; set; }
        public string ClubType { get; set; }
        public string IsAllClubs { get; set; }
        public DateTime DateProgram { get; set; }
        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public string SeasonDomain { get; set; }

        public bool IsAllPrograms { get; set; }

        public string Term { get; set; }
        public string AutomaticTerm { get; set; }

        public IncludeForms IncludeForms { get; set; }

        public ReportFilters Filters { get; set; }
    }

}