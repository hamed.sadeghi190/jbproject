﻿using System.Web;
using Autofac;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Data.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace Jumbula.Bootstrapper.Modules
{
    public class IdentityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType(typeof(ApplicationUserManager)).As(typeof(IApplicationUserManager<JbUser, int>)).InstancePerRequest();
            builder.RegisterType(typeof(ApplicationUserManager)).As(typeof(ApplicationUserManager)).InstancePerRequest();
            builder.RegisterType(typeof(ApplicationSignInManager)).As(typeof(IApplicationSignInManager)).InstancePerRequest();
            builder.RegisterType(typeof(ApplicationRoleManager)).As(typeof(IApplicationRoleManager<JbRole>)).InstancePerRequest();
            builder.RegisterType(typeof(JbUser)).As(typeof(IUser<int>)).InstancePerRequest();
            builder.RegisterType(typeof(JbUserStore)).As(typeof(IUserStore<JbUser, int>)).InstancePerRequest();
            builder.RegisterType(typeof(AuthenticationAdditionalHelpers)).As(typeof(IAuthenticationAdditionalHelpers)).InstancePerRequest();
            builder.Register(b => HttpContext.Current.GetOwinContext().Authentication).As(typeof(IAuthenticationManager)).InstancePerRequest();
        }
    }
}
