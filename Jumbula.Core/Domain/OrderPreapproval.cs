﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class OrderPreapproval : BaseEntity<long>
    {
        public long OrderId { get; set; }
         [StringLength(128)]
        public string PaypalPreapprovalID { get; set; }

        public DateTime? PaypalPreapprovalStartDate { get; set; }
        public DateTime? PaypalPreapprovalEndDate { get; set; }
        public virtual Order Order { get; set; }
     
    }
}
