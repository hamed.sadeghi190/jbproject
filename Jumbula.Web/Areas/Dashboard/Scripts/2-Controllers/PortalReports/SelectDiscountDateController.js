﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SelectDiscountDateController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, $timeout) {
            window.app.globalObjects.pageClass = 'select-county-view';
            window.app.globalObjects.initializeView();

            $scope.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            $scope.updateChosenScope = function () {
                $("select[chosen]").trigger('chosen:updated');
            };

            $scope.Model = {};
            $scope.reportType = $stateParams.reportType;
            if ($scope.reportType == "ScholarshipDiscount") {
                $scope.reportType = "Scholarship/discount";
            }

            $scope.runReport = function () {
                customReportService.setModelInStorage($scope.Model);
                var type = $stateParams.reportType;
                switch (type) {
                    case "ScholarshipDiscount":
                        {
                            $state.go('AllDiscountReports');
                            break;
                        }
                    case "Discount":
                        {
                            $state.go('DiscountReport');
                            break;
                        }
                    case "Scholarship":
                        {
                            $state.go('ScholarshipReport');
                            break;
                        }
                }
            }
        }
    ]);

})();