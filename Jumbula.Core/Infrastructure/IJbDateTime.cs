﻿using System;

namespace Jumbula.Core.Infrastructure
{
    public interface IJbDateTime
    {
        DateTime Now { get; }

        DateTime UtcNow { get; }
    }
}
