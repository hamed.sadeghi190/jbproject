﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class ClientConfig : EntityTypeConfiguration<Client>
    {
        public ClientConfig()
        {
            HasRequired(c => c.PricePlan).WithMany(p => p.Clients).HasForeignKey(c => c.PricePlanId);
            HasOptional(c => c.DonationForm).WithMany();
            HasMany(c => c.BillingNotes).WithMany(i => i.Clients).Map(ci =>
            {
                ci.MapLeftKey("BillingNoteId");
                ci.MapRightKey("ClientId");
                ci.ToTable("ClientBillingNotes");
            });

        }
    }
}
