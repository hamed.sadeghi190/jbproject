﻿(function () {
    "use strict";

    angular.module("dashboardApp").controller("classConfirmationController", ['$scope', '$http', '$stateParams', '$state', 'classConfirmationService', 'jbToast', function ($scope, $http, $stateParams, $state, classConfirmationService, jbToast) {
        window.app.globalObjects.pageClass = 'jb-home-setting-view';
        window.app.globalObjects.initializeView();

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $http.get(baseUrl + '/GetClassConfirmationStep1')
            .success(function (data) {
                $scope.Model = data;
            });

        $scope.continue = function () {
            $http.post(baseUrl + '/SaveClassConfirmationStep1', { model: $scope.Model })
                .success(function (data) {

                    if (data.Status) {

                        classConfirmationService.set($scope.Model);
                        $state.go('SaveClassConfirmationStep2');
                    } else {
                        $scope.MC.handleErrors($scope, data);
                    }

                });
        }

        $scope.back = function () {
            window.history.back();
        }

    }]);


    angular.module('dashboardApp').controller('classConfirmationStep2Controller', ['$scope', '$http', '$stateParams', '$state', 'classConfirmationService', 'jbToast', function ($scope, $http, $stateParams, $state, classConfirmationService,jbToast) {
        window.app.globalObjects.pageClass = 'jb-home-setting-view';
        window.app.globalObjects.initializeView();

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        var model = classConfirmationService.get();

        $("#grid").kendoGrid({
            dataSource: {
                dataType: "json",
                serverPaging: true,
                pageSize: 5,
                transport:
                {
                    read:
                        {
                            contentType: "application/json; charset=utf-8", 
                            type: "POST", 
                            url: baseUrl + "/GetClassConfirmationItems"
                        },
                    parameterMap: function (data, operation) {
                        if (operation == "read") {
                            data = $.extend(
                                {
                                    sort: null,
                                    filter: null,
                                    model: model 
                                }, data);

                            return JSON.stringify(data);
                        }
                    }
                },

                schema: { data: "DataSource", total: "TotalCount" }
            },
            pageable: true,
            columns: [
                { field: "SchoolName", title: "School" },
                { field: "ProgramName", title: "Program" },
                { field: "RegistrationCount", title: "Registrations" },
            ],
            detailTemplate: kendo.template($("#template").html()),
            detailInit: detailInit,
        });


        function detailInit(e) {
            var detailRow = e.detailRow;
            detailRow.find(".recipients").kendoGrid({
                dataSource: {
                    type: "json",
                    serverPaging: true,
                    pageSize: 5,
                    transport: {
                        read:
                            {
                                url: baseUrl + "/GetClassConfirmationDetails",
                                type: "POST",
                                data: function () {
                                    return {
                                        programId: e.data.ProgramId,
                                    };
                                },
                            }
                    },
                    schema: { data: "DataSource", total: "TotalCount" }
                },
                columns: [
                    {
                        field: "ParticipantName", title: "Participant"
                    },
                    {
                        field: "Email", title: "Email"
                    },
                ],
            });
        }


        $scope.continue = function () {
            $http.post(baseUrl + "/MakeCampaignForClassConfiramtion", { model: model })
                .success(function (data) {
                    if (data.Status) {
                        $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'ClassConfirmation' });
                    } else {
                        jbToast.error(data.Message)
                    }
                });
        }

    }]);
})();