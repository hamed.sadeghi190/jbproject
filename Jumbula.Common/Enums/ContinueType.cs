﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ContinueType
    {
        [Description("and ends on")]
        Until = 0,
        [Description("and lasts for")]
        For = 1
    }
}
