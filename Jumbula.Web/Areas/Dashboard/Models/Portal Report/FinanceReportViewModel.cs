﻿using System;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;

namespace SportsClub.Areas.Dashboard.Models
{
    public class FinanceReportItemViewModel
    {
        public string ClassName { get; set; }

        public string ProviderName { get; set; }
        public decimal ClassFee { get; set; }
        public string ClassFeeString
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(ClassFee, Currency);
            }

        }
        public int? EMRate { get; set; }
        public string EMRatestr
        {
            get
            {

                return EMRate.HasValue && EMRate != 0 ? CurrencyHelper.FormatPercentWithoutPenny(EMRate) : "-";


            }
        }


        public int TotalRegistrations { get; set; }


        public decimal TotalGrossAmtRegistrations
        {
            get
            {
                return TotalRegistrations * ClassFee;
            }
        }

        public string TotalGrossAmtRegistrationsstr
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(TotalGrossAmtRegistrations, Currency);
            }
        }
    
        public decimal TotalPTAPTO { get; set; }
        public string TotalPTAPTOstr
        {
            get

            { return CurrencyHelper.FormatCurrencyWithPenny(TotalPTAPTO, Currency); }
        }
        public decimal CashPaymentsPTAPTO { get; set; }

        public string CashPaymentsPTAPTOStr { get { return CurrencyHelper.FormatCurrencyWithPenny(CashPaymentsPTAPTO, Currency); } }

        public decimal TotalAmtProviderFundedScholarships { get; set; }

        public string TotalAmtProviderFundedScholarshipsStr
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(TotalAmtProviderFundedScholarships, Currency);
            }
        }
        public CurrencyCodes Currency { get; set; }
        public decimal? TotalClassFeeslessProviderFundedScholarships
        {
            get
            {
                return TotalGrossAmtRegistrations - TotalAmtProviderFundedScholarships;
            }
        }
        public string TotalClassFeeslessProviderFundedScholarshipsstr
        {
            get
            {
                 return CurrencyHelper.FormatCurrencyWithPenny(TotalClassFeeslessProviderFundedScholarships, Currency);
            }
        }
        public decimal? TotalAmtofClassFeesCollected { get; set; }
      
        public string TotalAmtofClassFeesCollectedstr
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(TotalAmtofClassFeesCollected, Currency);
            }
        }
        public decimal? AmtofEMFeetoDeduct
        {
            get
            {
                return TotalClassFeeslessProviderFundedScholarships * EMRate / 100;
            }
        }
        public string AmtofEMFeetoDeductStr
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(AmtofEMFeetoDeduct, Currency);
            }
        }
     
        public decimal ProviderPenaltiesandLatePenaltyFines { get; set; }
        public decimal ProviderPenaltiesandNoShowFines { get; set; }
        public decimal ProviderPenaltiesandFines
        {
            get
            {
                return ProviderPenaltiesandLatePenaltyFines + ProviderPenaltiesandNoShowFines;
            }
        }
        public string ProviderPenaltiesandFinesStr { get { return CurrencyHelper.FormatCurrencyWithPenny(ProviderPenaltiesandFines, Currency); } }
        public decimal? TotalNetPaymenttoProvider
        {
            get
            {
                return (TotalClassFeeslessProviderFundedScholarships - AmtofEMFeetoDeduct - ProviderPenaltiesandFines);
            }
        }
        public string TotalNetPaymenttoProviderstr
        {
            get
            {
                return CurrencyHelper.FormatCurrencyWithPenny(TotalNetPaymenttoProvider, Currency);
            }
        }
    }

}