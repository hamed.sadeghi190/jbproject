﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('schoolSettingsOnSiteInfoController', ['$scope', '$http', 'jbToast', function ($scope, $http, jbToast) {
        window.app.globalObjects.pageClass = 'school-siteinfo-settings-view';
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/SchoolSettings", order: 2, menu: "SETTINGS" });


        $http.get('Dashboard/Setting/EditSchoolSettingsOnSiteInfo').success(function (data, status, headers, config) {
            $scope.Model = data;            

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

     
        $scope.save = function () {

         
            $http.post('Dashboard/Setting/EditSchoolSettingsOnSiteInfo', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {
                     $scope.errors = null;
                     jbToast.success("On-Site info updated successfully.", "");
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }
  
    }])

})();