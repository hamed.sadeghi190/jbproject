﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemRefundTransactionController', ['$scope', '$http', '$log', '$stateParams', '$state', 'cancelOrderService', function ($scope, $http, $log, $stateParams, $state, cancelOrderService) {
        window.app.globalObjects.pageClass = 'seasons-view order';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + "/Dashboard/System";


        var returndeByBack = $state.returnByBack();
        $scope.Model = {};
        if (returndeByBack) {
            $scope.Model = cancelOrderService.get();
            $scope.Model.clubId = $stateParams.clubId;
        }

        $http.get($scope.baseUrl + '/GetClubBillingHistories?billingId=' + $stateParams.billingId).success(function (data, status, headers, config) {
            $scope.Model = data;

            $scope.Model.ClubId = $stateParams.clubId;

        });

        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("SystemClubBillingHistory", { clubId: $stateParams.clubId });
                }
            }
            else {
                $state.go("SystemClubBillingHistory", { clubId: $stateParams.clubId });
            }
        }

       



        $scope.confirmCancellation = function () { 

            $http.post($scope.baseUrl + '/ClubBillingHistories', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        cancelOrderService.set($scope.Model);
                        $state.forward('SystemConfirmRefund', { clubId: $stateParams.clubId }, 'SystemClubRefund', { clubId: $stateParams.clubId });

                    }
                    else {

                        
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

    }]);
})();