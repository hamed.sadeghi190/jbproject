﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
    class GovermentSubsidyTest: BaseTest<GovermentSubsidyElements>
    {
        public GovermentSubsidyTest(IWebDriver webDriver)
            :base(webDriver)
        {
        }

        public void View()
        {
            Assert.AreEqual("Government Subsidies", Elements.PageTitle.Text);

            #region AssertRow1
            Assert.AreEqual("Subsidy5", Elements.Row1Name.Text);
            Assert.AreEqual("District of Columbia", Elements.Row1State.Text);
            Assert.AreEqual("Subsidy5@jumbula.com", Elements.Row1Email.Text);
            Assert.AreEqual("897-543-8766", Elements.Row1Phone.Text);
            Assert.AreEqual("www.subsidy5.com", Elements.Row1Website.Text);
            #endregion AssertRow1
            #region AssertRow2
            Assert.AreEqual("Subsidy4", Elements.Row2Name.Text);
            Assert.AreEqual("New Jersey", Elements.Row2State.Text);
            Assert.AreEqual("Subsidy4@jumbula.com", Elements.Row2Email.Text);
            Assert.AreEqual("358-258-3584", Elements.Row2Phone.Text);
            Assert.AreEqual("www.subsidy4.com", Elements.Row2Website.Text);
            #endregion AssertRow2
            #region AssertRow3
            Assert.AreEqual("Subsidy3", Elements.Row3Name.Text);
            Assert.AreEqual("Pennsylvania", Elements.Row3State.Text);
            Assert.AreEqual("Subsidy3@jumbula.com", Elements.Row3Email.Text);
            Assert.AreEqual("247-413-6812", Elements.Row3Phone.Text);
            Assert.AreEqual("www.subsidy3.com", Elements.Row3Website.Text);
            #endregion AssertRow3
            #region AssertRow4
            Assert.AreEqual("Subsidy2", Elements.Row4Name.Text);
            Assert.AreEqual("Florida", Elements.Row4State.Text);
            Assert.AreEqual("Subsidy2@jumbula.com", Elements.Row4Email.Text);
            Assert.AreEqual("846-210-6532", Elements.Row4Phone.Text);
            Assert.AreEqual("www.subsidy2.com", Elements.Row4Website.Text);
            #endregion AssertRow4
            #region AssertRow5
            Assert.AreEqual("Subsidy1", Elements.Row5Name.Text);
            Assert.AreEqual("Delaware", Elements.Row5State.Text);
            Assert.AreEqual("Subsidy1@jumbula.com", Elements.Row5Email.Text);
            Assert.AreEqual("246-719-8432", Elements.Row5Phone.Text);
            Assert.AreEqual("www.subsidy1.com", Elements.Row5Website.Text);
            #endregion AssertRow5
        }
        public void Filter()
        {
            Elements.SearchTerm.SendKeys("Subsidy3");
            Elements.SearchButton.Click();
            ExtraAPI.Wait(10000);

            Assert.AreEqual("Subsidy3", Elements.Row1Name.Text);
            Assert.IsFalse(Elements.Row2Exist);

            // Clear filter
            Elements.ClearFilterLink.Click();
            ExtraAPI.Wait(10000);

            Assert.IsEmpty(Elements.SearchTerm.Text);
            Assert.AreEqual("Subsidy5", Elements.Row1Name.Text);
            Assert.AreEqual("Subsidy4", Elements.Row2Name.Text);
            Assert.AreEqual("Subsidy3", Elements.Row3Name.Text);
            Assert.AreEqual("Subsidy2", Elements.Row4Name.Text);
            Assert.AreEqual("Subsidy1", Elements.Row5Name.Text);
        }
        public void Edit()
        {
            Elements.Row3Actions.Click();
            ExtraAPI.Wait(2000);
            Elements.Row3ViewEditAction.Click();
            ExtraAPI.Wait(10000);

            // Check the old data
            Assert.AreEqual("Subsidy3", Elements.Name.GetAttribute("value"));
            Assert.AreEqual("number:39", Elements.State.GetAttribute("value"));
            Assert.AreEqual("www.subsidy3.com", Elements.Website.GetAttribute("value"));
            Assert.AreEqual("Goverment", Elements.FirstName.GetAttribute("value"));
            Assert.AreEqual("Subsidy3", Elements.LastName.GetAttribute("value"));
            Assert.AreEqual("Subsidy3@jumbula.com", Elements.Email.GetAttribute("value"));
            Assert.AreEqual("2474136812", Elements.Phone.GetAttribute("value"));
            Assert.AreEqual("Subsidy3 Instructions", Elements.Instructions.GetAttribute("value"));

            // Update the data
            Elements.Name.Clear();
            Elements.Name.SendKeys("Subsidy3 changed");
            Elements.Website.Clear();
            Elements.Website.SendKeys("www.subsidy3changed.com");
            Elements.FirstName.Clear();
            Elements.FirstName.SendKeys("Goverment changed");
            Elements.LastName.Clear();
            Elements.LastName.SendKeys("Subsidy3 changed");
            //Elements.Email.Clear();
            //Elements.Email.SendKeys("Subsidy3changed@jumbula.com");
            Elements.Phone.Clear();
            Elements.Phone.SendKeys("0987654321");
            Elements.Instructions.Clear();
            Elements.Instructions.SendKeys("Subsidy3 Instructions changed");
            Elements.State.Click();
            ExtraAPI.Wait(2000);
            Elements.NewYorkState.Click();
            ExtraAPI.Wait(2000);
            ((IJavaScriptExecutor)WebDriver).ExecuteScript("document.getElementById('bodyPanel').scrollTo(0,30000)");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);

            // Check the new data in grid
            #region AssertRow3
            Assert.AreEqual("Subsidy3 changed", Elements.Row3Name.Text);
            Assert.AreEqual("New York", Elements.Row3State.Text);
            Assert.AreEqual("Subsidy3@jumbula.com", Elements.Row3Email.Text);
            Assert.AreEqual("098-765-4321", Elements.Row3Phone.Text);
            Assert.AreEqual("www.subsidy3changed.com", Elements.Row3Website.Text);
            #endregion AssertRow3

            // Check the new data in edit mode
            Elements.Row3Actions.Click();
            ExtraAPI.Wait(20000);
            Elements.Row3ViewEditAction.Click();
            ExtraAPI.Wait(50000);
            Assert.AreEqual("Subsidy3 changed", Elements.Name.GetAttribute("value"));
            Assert.AreEqual("www.subsidy3changed.com", Elements.Website.GetAttribute("value"));
            Assert.AreEqual("Goverment changed", Elements.FirstName.GetAttribute("value"));
            Assert.AreEqual("Subsidy3 changed", Elements.LastName.GetAttribute("value"));
            Assert.AreEqual("Subsidy3@jumbula.com", Elements.Email.GetAttribute("value"));
            Assert.AreEqual("0987654321", Elements.Phone.GetAttribute("value"));
            Assert.AreEqual("Subsidy3 Instructions changed", Elements.Instructions.GetAttribute("value"));
            Assert.AreEqual("number:33", Elements.State.GetAttribute("value"));
        }
        public void Add()
        {
            #region Step1 
            
            // Validations
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Name is required.", Elements.Name.GetAttribute("data-original-title"));
            Assert.AreEqual("State is required.", Elements.State.GetAttribute("data-original-title"));
            Elements.Name.Clear();
            Elements.Name.SendKeys("Subsidy1");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("This name exists.", Elements.Name.GetAttribute("data-original-title"));
           
            // Valid data
            Elements.Name.Clear();
            Elements.Name.SendKeys("New Subsidy");
            Elements.State.Click();
            ExtraAPI.Wait(2000);
            Elements.NewYorkState.Click();
            ExtraAPI.Wait(2000);
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            #endregion Step1 

            #region Step2

            // Check data from Step1
            Assert.AreEqual("New Subsidy", Elements.Name.GetAttribute("value"));
            Assert.AreEqual("number:33", Elements.State.GetAttribute("value"));

            // Validation
            Elements.Name.Clear();
            Elements.Name.SendKeys("Subsidy1");
            Elements.Website.SendKeys("newsubsidy.com");
            Elements.Phone.SendKeys("12345");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Website is not a valid fully-qualified www.", Elements.Website.GetAttribute("data-original-title"));
            Assert.AreEqual("Enter a complete phone number including area code.", Elements.Phone.GetAttribute("data-original-title"));
            Assert.AreEqual("This name exists.", Elements.Name.GetAttribute("data-original-title"));
            Elements.Phone.Clear();
            Elements.Phone.SendKeys("subsidy");
            Elements.Email.SendKeys("newsubsidy");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Enter a complete phone number including area code.<br>Please enter the phone number including area code (e.g. 5551239876).", Elements.Phone.GetAttribute("data-original-title"));
            Assert.AreEqual("Email is not a valid email address.", Elements.Email.GetAttribute("data-original-title"));
            Elements.Phone.Clear();
            Elements.Phone.SendKeys("1234567890123450");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Phone number is too long.", Elements.Phone.GetAttribute("data-original-title"));

            // Valid data
            Elements.Name.Clear();
            Elements.Phone.Clear();
            Elements.Email.Clear();
            Elements.Website.Clear();
            Elements.Name.SendKeys("New Subsidy");
            Elements.Website.SendKeys("www.newsubsidy.com");
            Elements.FirstName.SendKeys("New");
            Elements.LastName.SendKeys("Subsidy");
            Elements.Email.SendKeys("newsubsidy@jumbula.com");
            Elements.Phone.SendKeys("1234567890");
            Elements.Instructions.SendKeys("new subsidy instructions ");
            Elements.Submit.Click();
            ExtraAPI.Wait(5000);
            #endregion Step2

            #region Check new subsidy 

            // Check the grid
            #region AssertRow1
            Assert.AreEqual("New Subsidy", Elements.Row1Name.Text);
            Assert.AreEqual("New York", Elements.Row1State.Text);
            Assert.AreEqual("newsubsidy@jumbula.com", Elements.Row1Email.Text);
            Assert.AreEqual("123-456-7890", Elements.Row1Phone.Text);
            Assert.AreEqual("www.newsubsidy.com", Elements.Row1Website.Text);
            #endregion AssertRow1

            // Check in edit mode
            Elements.Row1Actions.Click();
            ExtraAPI.Wait(20000);
            Elements.Row1ViewEditAction.Click();
            ExtraAPI.Wait(50000);
            Assert.AreEqual("New Subsidy", Elements.Name.GetAttribute("value"));
            Assert.AreEqual("www.newsubsidy.com", Elements.Website.GetAttribute("value"));
            Assert.AreEqual("New", Elements.FirstName.GetAttribute("value"));
            Assert.AreEqual("Subsidy", Elements.LastName.GetAttribute("value"));
            Assert.AreEqual("newsubsidy@jumbula.com", Elements.Email.GetAttribute("value"));
            Assert.AreEqual("1234567890", Elements.Phone.GetAttribute("value"));
            Assert.AreEqual("new subsidy instructions", Elements.Instructions.GetAttribute("value"));
            Assert.AreEqual("number:33", Elements.State.GetAttribute("value"));
            #endregion Check new subsidy

        }

    }
}
