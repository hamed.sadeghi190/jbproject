﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class EmailRecipient : BaseEntity
    {
        [StringLength(255)]
        public string DisplayName { get; set; }
        [StringLength(255)]
        public string EmailAddress { get; set; }
        public EmailRecipientType Type { get; set; }
        public bool Delivered { get; set; }
        public DateTime? DeliverDate { get; set; }
        public bool Clicked { get; set; }
        public DateTime? ClickDate { get; set; }
        public bool Opened { get; set; }
        public DateTime? OpenDate { get; set; }

        public bool Dropped { get; set; }

        public DateTime? DropDate { get; set; }

        public bool Bounced { get; set; }

        public DateTime? BounceDate { get; set; }

        public bool Blocked { get; set; }

        public bool Deferred { get; set; }

        public DateTime? DefereDate { get; set; }

        public bool SpamReport { get; set; }

        public DateTime? SpamReportDate { get; set; }

        public bool Unsubscribe { get; set; }

        public DateTime? UnsubscribeDate { get; set; }

        [ForeignKey("EmailId")]
        public virtual Email Email { get; set; }

        public int EmailId { get; set; }
    }
}
