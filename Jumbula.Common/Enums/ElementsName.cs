﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum ElementsName
    {
        [Description("First name")]
        FirstName,

        [Description("Last name")]
        LastName,

        [Description("Gender")]
        Gender,

        [Description("Date of birth")]
        DoB,

        [Description("Grade")]
        Grade,

        [Description("Age")]
        Age,

        [Description("Email")]
        Email,

        [Description("Primary phone")]
        Phone,

        [Description("Alternate phone")]
        AlternatePhone,

        [Description("Alternate phone")]
        Cell,

        [Description("Occupation")]
        Occupation,

        [Description("Employer")]
        Employer,

        [Description("Home phone")]
        HomePhone,

        [Description("Work phone")]
        WorkPhone,

        [Description("Do you have medical conditions and special needs?")]
        HasMedicalConditions,

        [Description("Medical conditions and special needs")]
        MedicalConditions,

        [Description("Do you have allergies and dietary restrictions?")]
        HasAllergiesInfo,

        [Description("Allergies and dietary restrictions")]
        AllergiesInfo,

        [Description("Teacher name")]
        TeacherName,

        [Description("Teacher not listed")]
        TeacherNotListed,

        [Description("Address")]
        Address,

        [Description("Relationship")]
        Relationship,

        [Description("Authorize pickup description")]
        AuthorizePickupDescription,

        [Description("Doctor name")]
        DoctorName,

        [Description("Doctor phone")]
        DoctorPhone,

        [Description("Doctor location")]
        DoctorLocation,

        [Description("School name")]
        SchoolName,

        [Description("Standard dismissal")]
        StandardDismissal,

        [Description("Select your child's bus number")]
        ChildBusNumber,

        [Description("Please type your child's bus number or name")]
        ChildBusNumberNotListed,

        [Description("Dismissal from enrichment")]
        DismissalFromEnrichment,

        [Description("Name")]
        Name,

        [Description("Date of birth")]
        DateOfBirth,

        [Description("Email address")]
        EmailAddress,

        [Description("Homeroom")]
        Homeroom,

        [Description("Photography release")]
        PermissionPhotography,

        [Description("Signature")]
        Signature,

        [Description("Is adult")]
        IsAdult,

        [Description("Hear about us")]
        HearAboutUs,

        [Description("Parent relationship")]
        ParentRelationship,

        [Description("This person is also an emergency contact")]
        IsEmergencyContact,

        [Description("Self administered medication")]
        SelfAdministerMedication,

        [Description("Self administered medication description")]
        SelfAdministerMedicationDescription,

        [Description("Nut allergy")]
        NutAllergy,

        [Description("Nut allergy description")]
        NutAllergyDescription,


        [Description("Company name")]
        CompanyName,

        [Description("Policy number")]
        PolicyNumber,

        [Description("Company phone")]
        CompanyPhone,

        [Description("Address line 1")]
        AddressLine1,

        [Description("Address line 2")]
        AddressLine2,

        [Description("Country")]
        Country,

        [Description("City")]
        City,

        [Description("State")]
        State,

        [Description("ZIP")]
        Zip,

        [Description("Program")]
        Program,

        [Description("Program ID")]
        ProgramId,

        [Description("Participant ID")]
        ParticipantId,

        [Description("Parent ID")]
        ParentId
    }
}
