ALTER TABLE [dbo].[Programs] add [InstructorId] [int] NULL
Go

ALTER TABLE [dbo].[Programs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Programs_dbo.JbUsers_InstructorId] FOREIGN KEY([InstructorId])
REFERENCES [dbo].[JbUsers] ([Id])
GO

ALTER TABLE [dbo].[Programs] CHECK CONSTRAINT [FK_dbo.Programs_dbo.JbUsers_InstructorId]
GO