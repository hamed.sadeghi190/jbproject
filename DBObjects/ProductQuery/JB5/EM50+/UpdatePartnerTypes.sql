

INSERT INTO [dbo].[ClubTypes]
           ([Name]
           ,[ParentId]
           ,[EnumType])
     VALUES
           ('Partner',
            null,
            10)
GO

update clubs set TypeId = (select Id from ClubTypes where Name = 'Partner') where domain = 'kwaddle'
update clubs set TypeId = (select Id from ClubTypes where Name = 'Partner') where domain = 'EM'
update clubs set TypeId = (select Id from ClubTypes where Name = 'Partner') where domain = 'baroody'
update clubs set TypeId = (select Id from ClubTypes where Name = 'Partner') where domain = 'generationinfocus'
update clubs set TypeId = (select Id from ClubTypes where Name = 'Partner') where domain = 'flexacademies'





