﻿using Jumbula.Common.Helper;
using System.Collections.Generic;
using System.Linq;


namespace Jb.Framework.Common.Forms
{
    public static class FormExtension
    {

        public static void RightCopy(this JbForm sourceForm, JbForm destForm, bool replaceElements = false)
        {
            foreach (var rootElement in sourceForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    var section = destForm.Elements.SingleOrDefault(e => e != null && e.ElementId == rootElement.ElementId);
                    if (section != null)
                        foreach (var selemnt in (rootElement as JbSection).Elements)
                        {
                            var element = destForm.Elements.SingleOrDefault(e => e != null && e.ElementId == selemnt.ElementId);
                            if (element != null)
                            {
                                try
                                {
                                    if (replaceElements)
                                    {
                                        (section as JbSection).Elements.Remove(element);
                                        (section as JbSection).Elements.Add(selemnt);
                                    }
                                    else
                                    {
                                        element = selemnt;
                                    }
                                }
                                catch
                                {

                                }

                            }
                        }
                }
            }
        }

        //public static bool TryGet<T>(this IEnumerable<JbBaseElement> sourceElements, JbBaseElement searchElement, out T newElement) where T : JbBaseElement, new()
        //{
        //    newElement = new T();
        //    //Func<JbBaseElement, bool> filter = e => e.Equals(searchElement);
        //    if (sourceElements.Any(e => e.Equals(searchElement)))
        //    {
        //        var tempElement = sourceElements.FirstOrDefault(e => e.Title.Equals(searchElement.Title));
        //        if (tempElement == null)
        //            tempElement = sourceElements.FirstOrDefault(e => e.Name.Equals(searchElement.Name));
        //        if (tempElement == null)
        //            tempElement = sourceElements.FirstOrDefault(e => e.ElementId.Equals(searchElement.ElementId));
        //        if (tempElement != null)
        //        {
        //            newElement = tempElement as T;
        //            return true;
        //        }
        //    }
        //    return false;
        //}
        public static bool TryGet<T>(this IEnumerable<IJbBaseElement> sourceElements, IJbBaseElement searchElement, out T newElement) where T : JbBaseElement, new()
        {
            newElement = new T();
            //Func<JbBaseElement, bool> filter = e => e.Equals(searchElement);
            if (sourceElements.Any(e => e.Equals(searchElement)))
            {
                var tempElement = sourceElements.FirstOrDefault(e => e.Title.EqualsIgnoreSpaces(searchElement.Title));
                if (tempElement == null)
                    tempElement = sourceElements.FirstOrDefault(e => e.Name.Equals(searchElement.Name));
                if (tempElement == null)
                    tempElement = sourceElements.FirstOrDefault(e => e.ElementId.Equals(searchElement.ElementId));
                if (tempElement != null)
                {
                    newElement = tempElement as T;
                    return true;
                }
            }
            return false;
        }
        public static JbForm RightCopyNewByAny(this JbForm sourceForm, JbForm destForm, bool replaceElements = false)
        {
            var newForm = new JbForm();
            foreach (var rootElement in sourceForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    JbSection section;
                    if (destForm.Elements.TryGet(rootElement, out section))
                    {
                        if (!newForm.Elements.Any(s => s.ElementId == section.ElementId))
                            newForm.Elements.Add(new JbSection()
                            {
                                ElementId = section.ElementId,
                                Name = section.Name,
                                Title = section.Title
                            });
                        foreach (var sourceElement in (rootElement as JbSection).Elements)
                        {
                            JbBaseElement element;
                            if ((section as JbSection).Elements.TryGet(sourceElement, out element))
                            {
                                if (replaceElements)
                                    (newForm.Elements.Single(e => e.ElementId == section.ElementId) as JbSection).Elements.Add(sourceElement);
                                else
                                    (newForm.Elements.Single(e => e.ElementId == section.ElementId) as JbSection).Elements.Add(element);
                            }
                        }
                    }
                }
            }
            return newForm;
        }
        public static void RightReplace(this JbForm sourceForm, JbForm destForm)
        {
            foreach (var rootElement in sourceForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    var section = destForm.Elements.SingleOrDefault(e => e != null && e.ElementId == rootElement.ElementId);
                    if (section != null)
                        foreach (var selemnt in (rootElement as JbSection).Elements)
                        {
                            var element = destForm.Elements.SingleOrDefault(e => e != null && e.ElementId == selemnt.ElementId);
                            if (element != null)
                            {
                                try
                                {
                                    (section as JbSection).Elements.Remove(element);
                                    (section as JbSection).Elements.Add(selemnt);
                                }
                                catch
                                {

                                }
                            }
                        }
                }
            }
        }
        public static void CopyByTitle(this JbForm sourceForm, JbForm destForm)
        {
            foreach (var rootElement in sourceForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    var section = destForm.Elements.SingleOrDefault(e => e != null &&
                        ((string.IsNullOrEmpty(e.Title) &&
                        (e.ElementId == rootElement.ElementId || e.Name == rootElement.Name)) ||
                        e.Title.EqualsIgnoreSpaces(rootElement.Title)));

                    if (section != null)
                        foreach (var selemnt in (rootElement as JbSection).Elements)
                        {
                            var element = (section as JbSection).Elements.FirstOrDefault(e => e != null &&
                                ((string.IsNullOrEmpty(e.Title) && (e.ElementId == selemnt.ElementId || e.Name == selemnt.Name)) ||
                                e.Title.EqualsIgnoreSpaces(selemnt.Title)));
                            if (element == null)
                                (section as JbSection).Elements.Add(selemnt);
                            else
                                element = selemnt;
                        }
                    else
                        destForm.Elements.Add(rootElement);
                }
            }
        }
        public static void CopyAny(this JbForm sourceForm, JbForm destForm)
        {
            foreach (var rootElement in sourceForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    var section = destForm.Elements.SingleOrDefault(e => e != null && e.Equals(rootElement));

                    if (section != null)
                        foreach (var selemnt in (rootElement as JbSection).Elements)
                        {
                            var element = (section as JbSection).Elements.SingleOrDefault(e => e != null && e.Equals(rootElement));
                            if (element == null)
                                (section as JbSection).Elements.Add(selemnt);
                            else
                                element = selemnt;
                        }
                    else
                        destForm.Elements.Add(rootElement);
                }
            }
        }
        public static void BindingByTitle(this JbForm destForm, JbForm sourceForm)
        {
            foreach (var rootElement in sourceForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    var section = destForm.Elements.SingleOrDefault(e => e != null &&
                        ((string.IsNullOrEmpty(e.Title) &&
                        (e.ElementId == rootElement.ElementId || (e.Name != null && e.Name == rootElement.Name))) ||
                        e.Title.EqualsIgnoreSpaces(rootElement.Title)));

                    if (section != null)
                        foreach (var selemnt in (rootElement as JbSection).Elements)
                        {
                            var element = (section as JbSection).Elements.SingleOrDefault(e => e != null &&
                                ((string.IsNullOrEmpty(e.Title) && (e.ElementId == selemnt.ElementId || (e.Name != null && e.Name == selemnt.Name))) ||
                                e.Title.EqualsIgnoreSpaces(selemnt.Title)));
                            if (element != null)
                            {
                                if (element.GetType() != selemnt.GetType())
                                    continue;
                                if (element is JbDropDown)
                                    if (!(element as JbDropDown).EqualsItems(selemnt as JbDropDown))
                                        continue;

                                element.SetValue(selemnt.GetValue());
                            }

                        }
                }
            }
        }
        public static void BindingAny(this JbForm destForm, JbForm sourceForm)
        {
            foreach (var rootElement in sourceForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    if (!destForm.Elements.Any(e => e != null &&
                        (e.ElementId == rootElement.ElementId || (e.Name != null && e.Name == rootElement.Name) ||
                        e.Title.EqualsIgnoreSpaces(rootElement.Title))))
                        continue;

                    var section = destForm.Elements.FirstOrDefault(e => e != null &&
                        (e.ElementId == rootElement.ElementId || (e.Name != null && e.Name == rootElement.Name) ||
                        e.Title.EqualsIgnoreSpaces(rootElement.Title)));

                    if (section != null)
                        foreach (var selemnt in (rootElement as JbSection).Elements)
                        {
                            if (!(section as JbSection).Elements.Any(e => e != null &&
                                (e.ElementId == selemnt.ElementId || e.Title.EqualsIgnoreSpaces(selemnt.Title) || (e.Name != null && e.Name == selemnt.Name))))
                                continue;
                            if (selemnt is ICustomElement)
                                continue;

                            var element = (section as JbSection).Elements.FirstOrDefault(e => e != null &&
                                (e.ElementId == selemnt.ElementId || e.Title.EqualsIgnoreSpaces(selemnt.Title) || (e.Name != null && e.Name == selemnt.Name)));
                            if (element != null)
                            {
                                if (element.GetType() != selemnt.GetType())
                                    continue;
                                if (element is JbDropDown)
                                    if (!(element as JbDropDown).EqualsItems(selemnt as JbDropDown))
                                        continue;
                                if (element is JbAddress)
                                {
                                    (element as JbAddress).AddressLine1 = (selemnt as JbAddress).AddressLine1;
                                    (element as JbAddress).AddressLine2 = (selemnt as JbAddress).AddressLine2;
                                    (element as JbAddress).City = (selemnt as JbAddress).City;
                                    (element as JbAddress).State = (selemnt as JbAddress).State;
                                    (element as JbAddress).Zip = (selemnt as JbAddress).Zip;
                                }

                                if (element is JbDropDown)
                                {
                                    element.SetValue((selemnt as JbDropDown).GetKey());
                                }
                                else
                                {
                                    element.SetValue(selemnt.GetValue());
                                }
                            }

                        }
                }
            }
        }

        //public static void LeftCopy(this JbForm sourceForm, JbForm destForm)
        //{
        //    foreach (var rootElement in sourceForm.Elements)
        //    {
        //        if (rootElement is JbSection)
        //        {
        //            var section = destForm.Elements.SingleOrDefault(e => e != null && e.ElementId == rootElement.ElementId);
        //            if (section != null)
        //                destForm.Elements.Add(section);
        //            foreach (var selemnt in (rootElement as JbSection).Elements)
        //            {
        //                var element = (section as JbSection).Elements.SingleOrDefault(e => e != null && e.ElementId == selemnt.ElementId);
        //                if (element != null)
        //                {
        //                    (section as JbSection).Elements.Add(element);
        //                }
        //                else
        //                {
        //                    try
        //                    {
        //                        element = selemnt;
        //                    }
        //                    catch
        //                    {

        //                    }

        //                }
        //            }
        //        }
        //        else
        //        {

        //        }
        //    }
        //}
        //public static void LeftCopyByName(this JbForm sourceForm, JbForm destForm)
        //{
        //    foreach (var rootElement in sourceForm.Elements)
        //    {
        //        if (rootElement is JbSection)
        //        {
        //            var section = destForm.Elements.SingleOrDefault(e => e != null && e.Name == rootElement.Name);
        //            if (section != null)
        //                foreach (var selemnt in (rootElement as JbSection).Elements)
        //                {
        //                    var element = (section as JbSection).Elements.SingleOrDefault(e => e != null && e.Name == selemnt.Name);
        //                    if (element == null)
        //                        (section as JbSection).Elements.Add(selemnt);
        //                    else
        //                        element = selemnt;
        //                }
        //            else
        //                destForm.Elements.Add(rootElement);
        //        }
        //    }
        //}
        //public static void LeftCopyByAny(this JbForm sourceForm, JbForm destForm)
        //{
        //    foreach (var rootElement in sourceForm.Elements)
        //    {
        //        if (rootElement is JbSection)
        //        {
        //            var section = destForm.Elements.SingleOrDefault(e => e != null && (e.ElementId == rootElement.ElementId || e.Name == rootElement.Name || e.Title == rootElement.Title));
        //            if (section != null)
        //                foreach (var selemnt in (rootElement as JbSection).Elements)
        //                {
        //                    var element = (section as JbSection).Elements.SingleOrDefault(e => e != null && (e.ElementId == selemnt.ElementId || e.Name == selemnt.Name || e.Title == selemnt.Title));
        //                    if (element == null)
        //                        (section as JbSection).Elements.Add(selemnt);
        //                    else
        //                        element = selemnt;
        //                }
        //            else
        //                destForm.Elements.Add(rootElement);
        //        }
        //    }
        //}

        public static JbForm DifferenceByValue(this JbForm firstForm, JbForm secondForm)
        {
            var newForm = new JbForm();
            foreach (var rootElement in firstForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    var section = secondForm.Elements.SingleOrDefault(e => e != null && (e.ElementId == rootElement.ElementId ||
                        e.Name == rootElement.Name ||
                        e.Title.EqualsIgnoreSpaces(rootElement.Title)));
                    if (section != null)
                    {
                        foreach (var selemnt in (rootElement as JbSection).Elements)
                        {
                            var element =
                                (section as JbSection).Elements.SingleOrDefault(
                                    e =>
                                    e != null &&
                                    (e.ElementId == selemnt.ElementId || (e.Name != null && e.Name == selemnt.Name) ||
                                     e.Title.EqualsIgnoreSpaces(selemnt.Title)));
                            if (element != null && element.GetValue() != selemnt.GetValue())
                            {
                                newForm.Elements.Add(element);
                            }

                        }
                    }
                }
            }
            return newForm;
        }

        public static IEnumerable<string> DifferenceByValue(this JbForm firstForm, JbForm secondForm, string message, string emptyNewValueMessage, string emptyOldValueMessage)
        {
            string result = string.Empty;
            foreach (var rootElement in firstForm.Elements)
            {
                if (rootElement is JbSection)
                {
                    var section = secondForm.Elements.SingleOrDefault(e => e != null
                        && ((string.IsNullOrEmpty(e.Title) && (e.ElementId == rootElement.ElementId || (e.Name != null && e.Name == rootElement.Name)))
                        || e.Title.EqualsIgnoreSpaces(rootElement.Title)));
                    if (section != null)
                    {
                        foreach (var oldElement in (rootElement as JbSection).Elements)
                        {
                            var newElement =
                                (section as JbSection).Elements.SingleOrDefault(
                                    e =>
                                    e != null &&
                                    ((string.IsNullOrEmpty(e.Title) && (e.ElementId == oldElement.ElementId || (e.Name != null && e.Name == oldElement.Name))) ||
                                     e.Title.EqualsIgnoreSpaces(oldElement.Title)));
                            if (newElement.GetValue() == null && oldElement.GetValue() == null)
                                continue;
                            if (newElement.GetValue() != null && oldElement.GetValue() == null)
                                yield return string.Format(emptyOldValueMessage, oldElement.Title, section.Title, newElement.GetValue());

                            if (newElement.GetValue() == null && oldElement.GetValue() != null)
                                yield return string.Format(emptyNewValueMessage, oldElement.Title, section.Title, oldElement.GetValue());

                            if (!newElement.GetValue().Equals(oldElement.GetValue()))
                                yield return string.Format(message, oldElement.Title, section.Title, oldElement.GetValue(), newElement.GetValue());
                        }
                    }
                }
            }
        }

        public static void DataBinding(this JbForm form, JbForm secondform)
        {
            form.BindingAny(secondform);
        }
    }
}
