﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemAddDebitForClubController', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'DebitforClubService', function ($scope, $http, jbToast, $stateParams, $state, DebitforClubService) {
        window.app.globalObjects.pageClass = 'account-settings-view';
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.Model = {};
        $scope.Model.clubId = $stateParams.clubId; 

        $scope.back = function () {
            $state.go('SystemClubBillingHistory', { clubId: $stateParams.clubId });
        }

        $http.get($scope.baseUrl + '/GetInfoForDebit?clubId=' + $stateParams.clubId).success(function (data, status, headers, config) {
            $scope.Model = data;

        });


        $scope.confirmDebit = function () {

            $http.post($scope.baseUrl + '/ConfirmInfoDebit', { model: $scope.Model })

                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                       DebitforClubService.set($scope.Model);

                        $state.go('ConfirmAddDebitForClub', { clubId: $stateParams.clubId }, 'SystemChargeClub', { clubId: $stateParams.clubId });

                    }
                    else {


                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

    }])

})();