﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace SportsClub.Models
{
    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public LocalPasswordModel()
        {

        }
    }

    public class LoginModel
    {
        public LoginModel()
        {

        }

        [Required(ErrorMessage = "The Username field is required.")]
        [EmailAddress]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "The Username must be a valid e-mail address.")]
        [Display(Name = "Email address")]
        public string UserNameLogin { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string PasswordLogin { get; set; }

        [Display(Name = "Stay signed in")]
        public bool RememberMe { get; set; }
    }

    public class GuestUserModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email address")]
        public string UserName { get; set; }

    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        public string LogoUrl { get; set; }

    }

    public class ResetPasswordViewModel
    {
        public string LogoUrl { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string UserFullName { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        // the url that after user reset password redirct to it
        public string ReturnUrl { get; set; }
    }
    public class TransferClubViewModel
    {
        [Required]
        public string Token { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }



    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        [EmailAddress]
        public string UserName { get; set; }

        [Display(Name = "Confirm user name")]
        [Compare("UserName", ErrorMessage = "The user name and confirmation user name do not match.")]
        public string ConfirmUserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public string NewFamilyNotification { get; set; }
    }

    public class LoginRegister
    {
        public LoginRegister()
        {
            Login = new LoginModel();
            Register = new RegisterModel();
        }
        
        public LoginModel Login { get; set; }
        
        public RegisterModel Register { get; set; }

        public string LogoUrl { get; set; }

        public bool LoginOnly { get; set; }

        public bool RegisterMode { get; set; }
    }
}
