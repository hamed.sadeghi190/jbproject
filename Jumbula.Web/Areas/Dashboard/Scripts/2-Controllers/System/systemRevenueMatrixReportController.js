﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemRevenueMatrixReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', 'jbToast', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view order'
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/System";
       
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.Model = [];

        
        $scope.revenueReportsDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url:window.location.origin + '/Dashboard/System/GetRevenueMatrix',
                        type: "POST",
                        parameterMap: function (data, type) {
                            if (type === "read") {
                                data = $.extend(
                                   {
                                       sort: null,
                                       filter: null,

                                   }, data);
                                return JSON.stringify(data);
                            }
                        }
                    },
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverPaging: true,
            pageSize: 50,
            batch: true,
        });

        var columns = [];
        $scope.oldColumns = [];

        $scope.mainGridOptions = {
           
            dataSource: $scope.revenueReportsDataSource,
            
            sortable: true,
            scrollable: true,
            resizable: true,
            columnMenu: true,

            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
            dataBound: function (e) {
              
                $scope.oldColumns = $("#mainGrid").data("kendoGrid").columns;
            },
        }

        $(document).bind("ajaxComplete.changeColumns", function () {


            if ($scope.oldColumns && $scope.oldColumns.length > 0) {
                for (var i = 0; i < $scope.oldColumns.length; i++) {
                    var column = [];

                    
                    column = {
                        field: $scope.oldColumns[i].field,
                        title: $scope.oldColumns[i].field.replace(/_/g, " "),
                        width: '200px',

                    }
                    columns.push(column);
                }
                $("#mainGrid").data("kendoGrid").setOptions({
                    columns: columns
                });
            }
            $(document).unbind("ajaxComplete.changeColumns");
        });
       
    }]);
})();