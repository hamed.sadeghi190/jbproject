﻿using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class ImageGalleryItem : BaseEntity
    {
        public string Title { get; set; }
        public string Description { get; set; }

        [Required]
        public MetaData MetaData { get; set; }

        [Required]
        public string Path { get; set; }

        [Required]
        public int Order { get; set; }

        [Required]
        public EventStatusCategories Status { get; set; }

        public virtual ImageGallery ImageGallery { get; set; }
    }
}
