﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SignOutSheetPdfSchoolViewModel
    {
        public List<SchoolProgramsSignOutReportViewModel> Programs { get; set; }

        public string SchoolLogo { get; set; }
        public string ClubLogo { get; set; }
    }
}