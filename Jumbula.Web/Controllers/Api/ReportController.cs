﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Newtonsoft.Json;
using SportsClub.Models.Api;

namespace Jumbula.Web.Controllers.Api
{
    [Authorize]//(Roles = "Instructor, Admin, Manager")]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    public class ReportController : ApiController
    {
        [HttpGet]
        [Route("api/Report/Roster")]
        public HttpResponseMessage Roster(long programId)
        {
            HttpResponseMessage response = null;

            try
            {
                var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId).Where(o => o.Order.IsLive && o.ItemStatus == OrderItemStatusCategories.completed ||
                                                                        (o.ItemStatus == OrderItemStatusCategories.changed &&
                                                                        (o.ItemStatusReason == OrderItemStatusReasons.regular ||
                                                                        o.ItemStatusReason == OrderItemStatusReasons.transferIn ||
                                                                        o.ItemStatusReason == OrderItemStatusReasons.refund)));
                var formBusiness = Ioc.FormBusiness;

                if (orderItems == null || !orderItems.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Data = "", Message = "" });
                    return response;
                }

                var data = orderItems.ToList()
                    .Select(i => new
                    {
                        ParticipantId = i.PlayerId,
                        ParticipantFirstName = string.Empty,
                        ParticipantLastName = string.Format("{0}, {1}", formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName), formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName)),
                        ParticipantGender = formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.Gender),

                        SchoolGrade = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.Grade),
                        SchoolTeacherName = formBusiness.GetTeacher(i.JbForm),

                        SchoolDismissalFromEnrichment = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }),

                        ParentFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName),
                        ParentLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName),
                        ParentEmail = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Email),
                        ParentPrimaryPhone = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }),

                        ParentAlternatePhone = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.AlternatePhone, new List<string> { "Alternate phone", "Alternative phone" }),
                        ParentAuthorizedAdult = formBusiness.GetAuthorizedAdult(i.JbForm, null),
                        ParentAuthorizedAdultPhone = formBusiness.GetAuthorizedAdultPhone(i.JbForm, null),

                        EmergencyContactFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName),
                        EmergencyContactLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.LastName),
                        EmergencyContactPhone = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.HomePhone, new List<string> { "Emergency contact phone number", "Emergency contact phone", "Phone" }),


                        Medical_AllergiesMedicalInfo_SpecialNeeds = formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, ElementsName.MedicalConditions),
                        Medical_AllergiesAllergies = formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, "Allergies")
                    }
                    ).OrderBy(a => a.ParticipantLastName)
                    .ToList();


                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = data, Message = "" });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Report/Roster_V2_2_1")]
        public HttpResponseMessage Roster_V2_2_1(long programId, int page, int pageSize)
        {
            HttpResponseMessage response = null;

            try
            {
                var query = Ioc.OrderItemBusiness.GetProgramOrderItems(programId);

                var totalCount = query.Count();

                var orderItems = query.OrderBy(i => i.Player.Contact.LastName).ThenBy(i => i.Player.Contact.FirstName)
                    .Skip((page - 1) * pageSize).Take(pageSize)
                    .ToList();

                var formBusiness = Ioc.FormBusiness;

                if (orderItems == null || !orderItems.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Data = "", Message = "" });
                    return response;
                }

                var data = orderItems.ToList()
                    .Select(i => new
                    {
                        ParticipantId = i.PlayerId,
                        ParticipantFirstName = string.Empty,
                        ParticipantLastName = string.Format("{0}, {1}", formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName), formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName)),
                        ParticipantGender = formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.Gender),

                        SchoolGrade = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.Grade),
                        SchoolTeacherName = formBusiness.GetTeacher(i.JbForm),

                        SchoolDismissalFromEnrichment = formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }),

                        ParentFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName),
                        ParentLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName),
                        ParentEmail = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Email),
                        ParentPrimaryPhone = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }),

                        ParentAlternatePhone = formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.AlternatePhone, new List<string> { "Alternate phone", "Alternative phone" }),
                        ParentAuthorizedAdult = formBusiness.GetAuthorizedAdult(i.JbForm, null),
                        ParentAuthorizedAdultPhone = formBusiness.GetAuthorizedAdultPhone(i.JbForm, null),

                        EmergencyContactFirstName = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName),
                        EmergencyContactLastName = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.LastName),
                        EmergencyContactPhone = formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.HomePhone, new List<string> { "Emergency contact phone number", "Emergency contact phone", "Phone" }),


                        Medical_AllergiesMedicalInfo_SpecialNeeds = formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, ElementsName.MedicalConditions),
                        Medical_AllergiesAllergies = formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, "Allergies")
                    })
                    .OrderBy(a => a.ParticipantLastName)
                    .ToList();

                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = data, TotalCount = totalCount, Message = "" });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }


        [HttpGet]
        [Route("api/Report/RosterSubscription_V2_2_1")]
        public HttpResponseMessage RosterSubscription_V2_2_1(long programId, DateTime dateTime, int page, int pageSize)
        {
            HttpResponseMessage response = null;

            try
            {
                var startDate = dateTime;
                var endDate = dateTime.AddHours(24 - dateTime.Hour);

                var query = Ioc.OrderSessionBusiness.GetList().Where(o => o.OrderItem.ProgramSchedule.ProgramId == programId && (o.ProgramSession.StartDateTime >= startDate && o.ProgramSession.StartDateTime < endDate) &&
                                  o.OrderItem.ItemStatus == OrderItemStatusCategories.completed &&
                                 (o.OrderItem.Order.IsLive == (o.OrderItem.Season.Status == SeasonStatus.Live) ||
                                 (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed &&
                                 (o.OrderItem.ItemStatusReason == OrderItemStatusReasons.regular || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.transferIn || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.refund))))
                                 .GroupBy(g => g.OrderItemId)
                                 .Select(g => g.FirstOrDefault());

                var totalCount = query.Count();

                var orderSessions = query.OrderBy(i => i.OrderItem.Player.Contact.LastName).ThenBy(i => i.OrderItem.Player.Contact.FirstName)
                    .Skip((page - 1) * pageSize).Take(pageSize);

                var formBusiness = Ioc.FormBusiness;

                if (orderSessions == null || !orderSessions.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Data = "", Message = "" });
                    return response;
                }

                var data = orderSessions.ToList()
                    .Select(i => new
                    {
                        ParticipantId = i.OrderItem.PlayerId,
                        ParticipantFirstName = string.Empty,
                        ParticipantLastName = string.Format("{0}, {1}", formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.ParticipantSection, ElementsName.LastName), formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName)),
                        ParticipantGender = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.ParticipantSection, ElementsName.Gender),

                        SchoolGrade = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.SchoolSection, ElementsName.Grade),
                        SchoolTeacherName = formBusiness.GetTeacher(i.OrderItem.JbForm),

                        SchoolDismissalFromEnrichment = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }),

                        ParentFirstName = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName),
                        ParentLastName = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName),
                        ParentEmail = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.Email),
                        ParentPrimaryPhone = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }),

                        ParentAlternatePhone = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.AlternatePhone, new List<string> { "Alternate phone", "Alternative phone" }),
                        ParentAuthorizedAdult = formBusiness.GetAuthorizedAdult(i.OrderItem.JbForm, null),
                        ParentAuthorizedAdultPhone = formBusiness.GetAuthorizedAdultPhone(i.OrderItem.JbForm, null),

                        EmergencyContactFirstName = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName),
                        EmergencyContactLastName = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.EmergencyContactSection, ElementsName.LastName),
                        EmergencyContactPhone = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.EmergencyContactSection, ElementsName.HomePhone, new List<string> { "Emergency contact phone number", "Emergency contact phone", "Phone" }),


                        Medical_AllergiesMedicalInfo_SpecialNeeds = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.HealthSection, ElementsName.MedicalConditions),
                        Medical_AllergiesAllergies = formBusiness.GetFormValue(i.OrderItem.JbForm, SectionsName.HealthSection, "Allergies")
                    })
                    .OrderBy(a => a.ParticipantLastName)
                    .ToList();

                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = data, TotalCount = totalCount, Message = "" });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Report/GetAllSessionsPickup")]
        public HttpResponseMessage GetAllSessionsPickup(long programId)
        {
            HttpResponseMessage response = null;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

                var programSesssions = Ioc.ProgramBusiness.GetSessions(program)
                    .Where(s => s.End.Date <= clubDateTime.Date)
                    .OrderBy(o => o.Start)
                    .Select(s =>
                        new
                        {
                            Id = s.Id,
                            Date = s.Start.ToString("ddd, MMM dd, yyyy"),
                        })
                        .ToList();

                var currentSessionPickups = new object();
                var currentSessionId = 0;

                if (programSesssions.Any() && programSesssions != null)
                {
                    currentSessionId = programSesssions.Last().Id;

                    currentSessionPickups = new List<SessionPickupModel>();
                    switch (program.TypeCategory)
                    {
                        case ProgramTypeCategory.Class:
                            currentSessionPickups = GetSessionPickup(programId, currentSessionId);
                            break;
                        case ProgramTypeCategory.Camp:
                            break;
                        case ProgramTypeCategory.Subscription:
                            currentSessionPickups = GetSessionSubscriptionPickups(programId, currentSessionId);
                            break;
                        default:
                            break;
                    }


                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllSessions = programSesssions, CurrentSessionPickups = currentSessionPickups });
                    return response;
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllSessions = (ScheduleSession)null, Message = "The sessions for this class have not started yet." });
                    return response;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }

        }

        [HttpGet]
        [Route("api/Report/GetAllSessionsPickup_V2_2_1")]
        public HttpResponseMessage GetAllSessionsPickup_V2_2_1(long programId, int page, int pageSize)
        {
            HttpResponseMessage response = null;
            int totalCount = 0;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);

                var clubDateTime = Ioc.ClubBusiness.GetClubDateTime(program.Club.Id, DateTime.UtcNow);

                var programSesssions = Ioc.ProgramBusiness.GetSessions(program)
                    .Where(s => s.End.Date <= clubDateTime.Date)
                    .OrderBy(o => o.Start)
                    .Select(s =>
                        new
                        {
                            Id = s.Id,
                            Date = s.Start.ToString("ddd, MMM dd, yyyy"),
                            DateTime = s.Start.Date
                        })
                        .ToList();

                var currentSessionPickups = new object();
                var currentSessionId = 0;

                if (programSesssions.Any() && programSesssions != null)
                {
                    currentSessionId = programSesssions.Last().Id;

                    currentSessionPickups = new List<SessionPickupModel>();
                    switch (program.TypeCategory)
                    {
                        case ProgramTypeCategory.Class:
                            currentSessionPickups = GetSessionPickup_V2_2_1(programId, currentSessionId, page, pageSize, ref totalCount);
                            break;

                        case ProgramTypeCategory.Camp:
                        case ProgramTypeCategory.Subscription:
                            currentSessionPickups = GetSessionSubscriptionPickups_V2_2_1(programId, currentSessionId, page, pageSize, ref totalCount);
                            break;

                        case ProgramTypeCategory.BeforeAfterCare:
                            currentSessionPickups = GetSessionBeforeAfterPickups(programId, programSesssions.Last().DateTime, page, pageSize, ref totalCount);
                            break;

                    }


                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllSessions = programSesssions, CurrentSessionPickups = currentSessionPickups, TotalCount = totalCount });
                    return response;
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { AllSessions = (ScheduleSession)null, Message = "The sessions for this class have not started yet." });
                    return response;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }

        }


        [HttpGet]
        [Route("api/Report/GetSessionPickups")]
        public HttpResponseMessage GetSessionPickups(long programId, int sessionId)
        {
            HttpResponseMessage response = null;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);

                var sessionPickup = new List<SessionPickupModel>();

                switch (program.TypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        sessionPickup = GetSessionPickup(programId, sessionId);
                        break;
                    case ProgramTypeCategory.Camp:
                        break;
                    case ProgramTypeCategory.Subscription:
                        sessionPickup = GetSessionSubscriptionPickups(programId, sessionId);
                        break;
                    default:
                        break;
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { SessionPickups = sessionPickup });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Report/GetSessionPickups_V2_2_1")]
        public HttpResponseMessage GetSessionPickups_V2_2_1(long programId, int? sessionId, DateTime? dateTime, int page, int pageSize)
        {
            HttpResponseMessage response = null;
            int totalCount = 0;

            try
            {
                var program = Ioc.ProgramBusiness.Get(programId);

                var schedule = program.ProgramSchedules.First();

                var sessionPickup = new List<SessionPickupModel>();

                switch (program.TypeCategory)
                {
                    case ProgramTypeCategory.Class:
                        if (sessionId.Value != 0)
                        {
                            sessionPickup = GetSessionPickup_V2_2_1(programId, sessionId.Value, page, pageSize, ref totalCount);
                        }
                        break;

                    case ProgramTypeCategory.Camp:
                        var startDate = dateTime.Value;

                        var programSessionBusiness = Ioc.ProgramSessionBusiness;

                        var programSessions = programSessionBusiness.GetList()
                            .Where(p => p.Charge.ProgramSchedule.ProgramId == programId
                                        && !p.Charge.ProgramSchedule.IsDeleted)
                            .ToList();

                        if (programSessions != null && programSessions.Any())
                        {
                            sessionId = programSessions.Any(p => p.StartDateTime.Date == startDate.Date)
                                ? programSessions.FirstOrDefault(p => p.StartDateTime.Date == startDate.Date).Id
                                : 0;
                        }
                        else
                        {
                            sessionId = 0;
                        }

                        if (sessionId.Value != 0)
                        {
                            sessionPickup = GetSessionSubscriptionPickups_V2_2_1(programId, sessionId.Value, page, pageSize, ref totalCount);

                        }
                        break;

                    case ProgramTypeCategory.Subscription:
                        var sessions = Ioc.ProgramSessionBusiness.GetList(schedule);
                        sessionId = sessions != null && sessions.Any(p => p.StartDateTime.Date == dateTime.Value.Date) ? sessions.SingleOrDefault(p => p.StartDateTime.Date == dateTime.Value.Date).Id : 0;

                        if (sessionId.Value != 0)
                        {
                            sessionPickup = GetSessionSubscriptionPickups_V2_2_1(programId, sessionId.Value, page, pageSize, ref totalCount);
                        }
                        break;

                    case ProgramTypeCategory.BeforeAfterCare:
                        {
                            sessionPickup = GetSessionBeforeAfterPickups(programId, dateTime.Value, page, pageSize, ref totalCount);
                        }
                        break;
                }

                response = Request.CreateResponse(HttpStatusCode.OK, new { SessionPickups = sessionPickup, TotalCount = totalCount });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Report/UpdateSessionPickups")]
        public HttpResponseMessage UpdateSessionPickups([FromBody]PickupsStatusModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var statusClassUpdates = new OperationStatus() { Status = true };
                var statusSubscriptionUpdates = new OperationStatus() { Status = true };

                var programBusiness = Ioc.ProgramBusiness;

                var modelClasses = model.PickupsStatus.Where(s => s.Id.Split('-').Count() == 4);

                var modelSubscriptions = model.PickupsStatus.Where(s => s.Id.Split('-').Count() == 5);

                if (modelClasses != null && modelClasses.Any())
                {
                    var clubBusiness = Ioc.ClubBusiness;

                    var scheduleId = int.Parse(modelClasses.First().Id.Split('-').ElementAt(2));

                    var program = Ioc.ProgramBusiness.GetByScheduleId(scheduleId);

                    var clubId = program.OutSourceSeason != null ?
                                        program.OutSourceSeason.Club.Id :
                                        program.Club.Id;

                    var pickups = modelClasses.Select(s =>
                        new ScheduleAttendance
                        {
                            Id = int.Parse(s.Id.Split('-').ElementAt(0)),
                            ProfileId = int.Parse(s.Id.Split('-').ElementAt(1)),
                            ScheduleId = int.Parse(s.Id.Split('-').ElementAt(2)),
                            SessionId = int.Parse(s.Id.Split('-').ElementAt(3)),
                            ClubStaffId = model.ClubStaffId.Value,
                            IsPickedUp = s.IsPickedUp,
                            PickupSerialized = s.IsPickedUp ? JsonConvert.SerializeObject(new
                            {
                                FullName = s.FullName,
                                LocalDateTime = s.LocalDateTime,
                                ClubDateTime = clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow),
                                DismissalFromEnrichment = s.DismissalFromEnrichment,
                                Phone = s.Phone
                            }) : null
                        })
                        .ToList();

                    statusClassUpdates = Ioc.ProgramBusiness.UpdateAttendancesPickup(pickups);
                }


                if (modelSubscriptions != null && modelSubscriptions.Any())
                {
                    var pickups = modelSubscriptions.Select(s =>
                        new StudentAttendance
                        {
                            Id = int.Parse(s.Id.Split('-').ElementAt(0)),
                            OrderSessionId = int.Parse(s.Id.Split('-').ElementAt(4)),
                            IsDismissed = s.IsPickedUp,
                            DismissalStaffId = model.ClubStaffId.Value,
                            DismissalInfoSerialized = s.IsPickedUp ? JsonConvert.SerializeObject(new
                            {
                                FullName = s.FullName,
                                LocalDateTime = s.LocalDateTime,
                                DismissalFromEnrichment = s.DismissalFromEnrichment,
                                Phone = s.Phone
                            }) : null
                        })
                        .ToList();

                    statusSubscriptionUpdates = Ioc.StudentAttendanceBusiness.UpdateDismissal(pickups);
                }

                var result = statusClassUpdates.Status && statusSubscriptionUpdates.Status;

                response = Request.CreateResponse(HttpStatusCode.OK, new { Updated = result });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Report/UpdateSessionPickups_V2_2_1")]
        public HttpResponseMessage UpdateSessionPickups_V2_2_1([FromBody]PickupsStatusModel_V2_2_1 model)
        {
            HttpResponseMessage response = null;

            try
            {
                var statusClassUpdates = new OperationStatus() { Status = true };
                var statusSubscriptionUpdates = new OperationStatus() { Status = true };

                var programBusiness = Ioc.ProgramBusiness;

                var modelClasses = model.PickupsStatus.Where(s => s.Id.Split('-').Count() == 4);

                var modelSubscriptions = model.PickupsStatus.Where(s => s.Id.Split('-').Count() == 5);

                if (modelClasses != null && modelClasses.Any())
                {
                    var clubBusiness = Ioc.ClubBusiness;

                    var scheduleId = int.Parse(modelClasses.First().Id.Split('-').ElementAt(2));

                    var program = Ioc.ProgramBusiness.GetByScheduleId(scheduleId);

                    var clubId = program.OutSourceSeason != null ?
                                        program.OutSourceSeason.Club.Id :
                                        program.Club.Id;

                    var pickups = modelClasses.Select(s =>
                        new ScheduleAttendance
                        {
                            Id = int.Parse(s.Id.Split('-').ElementAt(0)),
                            ProfileId = int.Parse(s.Id.Split('-').ElementAt(1)),
                            ScheduleId = int.Parse(s.Id.Split('-').ElementAt(2)),
                            SessionId = int.Parse(s.Id.Split('-').ElementAt(3)),
                            ClubStaffId = s.ClubStaffId,
                            IsPickedUp = s.IsPickedUp,
                            PickupSerialized = s.IsPickedUp ? JsonConvert.SerializeObject(new
                            {
                                FullName = s.FullName,
                                LocalDateTime = s.LocalDateTime,
                                ClubDateTime = clubBusiness.GetClubDateTime(clubId, DateTime.UtcNow),
                                DismissalFromEnrichment = s.DismissalFromEnrichment,
                                Phone = s.Phone
                            }) : null
                        })
                        .ToList();

                    statusClassUpdates = Ioc.ProgramBusiness.UpdateAttendancesPickup(pickups);
                }


                if (modelSubscriptions != null && modelSubscriptions.Any())
                {
                    var pickups = modelSubscriptions.Select(s =>
                        new StudentAttendance
                        {
                            Id = int.Parse(s.Id.Split('-').ElementAt(0)),
                            OrderSessionId = int.Parse(s.Id.Split('-').ElementAt(4)),
                            IsDismissed = s.IsPickedUp,
                            DismissalStaffId = s.ClubStaffId.Value,
                            DismissalInfoSerialized = s.IsPickedUp ? JsonConvert.SerializeObject(new
                            {
                                FullName = s.FullName,
                                LocalDateTime = s.LocalDateTime,
                                DismissalFromEnrichment = s.DismissalFromEnrichment,
                                Phone = s.Phone
                            }) : null
                        })
                        .ToList();

                    statusSubscriptionUpdates = Ioc.StudentAttendanceBusiness.UpdateDismissal(pickups);
                }

                var result = statusClassUpdates.Status && statusSubscriptionUpdates.Status;

                response = Request.CreateResponse(HttpStatusCode.OK, new { Updated = result });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpGet]
        [Route("api/Report/GetProviderContactInfo")]
        public HttpResponseMessage GetProviderContactInfo(long programId)
        {
            HttpResponseMessage response = null;

            try
            {
                var programBusiness = Ioc.ProgramBusiness;
                var currentProgram = programBusiness.Get(programId);
                var seasonPrograms = programBusiness.GetSeasonPrograms(currentProgram.SeasonId).Where(p => p.Status != ProgramStatus.Frozen);
                var models = new List<dynamic>();

                foreach (var program in seasonPrograms)
                {
                    var providerName = (program.OutSourceSeasonId.HasValue)
                    ? program.OutSourceSeason.Club.Name
                    : string.Empty;

                    var fullName = (program.OutSourceSeasonId.HasValue)
                        ? program.OutSourceSeason.Club.ContactPersons.First().FirstName + " " + program.OutSourceSeason.Club.ContactPersons.First().LastName
                        : program.Club.ContactPersons.First().FirstName + " " + program.Club.ContactPersons.First().LastName;

                    var contactEmail = (program.OutSourceSeasonId.HasValue)
                        ? program.OutSourceSeason.Club.ContactPersons.First().Email
                        : program.Club.ContactPersons.First().Email;

                    var phone = (program.OutSourceSeasonId.HasValue)
                        ? program.OutSourceSeason.Club.ContactPersons.First().Phone
                        : program.Club.ContactPersons.First().Phone;

                    ClubStaff instructor1St = null;
                    ClubStaff instructor2nd = null;
                    if (program.Instructors != null && program.Instructors.Any())
                    {
                        instructor1St = program.Instructors.First();
                        if (program.Instructors.Count > 1)
                        {
                            instructor2nd = program.Instructors.Last();
                        }
                    }


                    models.Add(new
                    {
                        ClassName = program.Name,
                        ProviderName = providerName,
                        FullName = fullName,
                        ContactEmail = contactEmail,
                        Phone = PhoneNumberHelper.FormatPhoneNumber(phone),
                        Instructor1St = (instructor1St != null && instructor1St.Contact != null) ? instructor1St.Contact.FullName : "",
                        Phone1StInstructor = (instructor1St != null && instructor1St.Contact != null) ? PhoneNumberHelper.FormatPhoneNumber(instructor1St.Contact.Phone) : "",
                        Instructor2nd = (instructor2nd != null && instructor2nd.Contact != null) ? instructor2nd.Contact.FullName : "",
                        Phone2ndInstructor = (instructor2nd != null && instructor2nd.Contact != null) ? PhoneNumberHelper.FormatPhoneNumber(instructor2nd.Contact.Phone) : "",
                    });

                }
                models = models.OrderBy(p => p.ClassName).ToList();

                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = models, Message = "" });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Report/GetSchoolRosters")]
        public HttpResponseMessage GetSchoolRosters([FromBody]SchoolStudentsModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var programs = Ioc.ProgramBusiness.GetList().Where(p => p.ClubId == model.ClubId && p.SeasonId == model.SeasonId && p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                   && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Season.Status == SeasonStatus.Live && (p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare));

                var programIds = programs.Where(p => p.TypeCategory != ProgramTypeCategory.BeforeAfterCare).Select(p => p.Id).ToList();

                var allOrderItemsSeason = Ioc.OrderItemBusiness.GetReportOrderItems(programIds);

                var orderItemsClass = allOrderItemsSeason.Where(o => o.ProgramTypeCategory == ProgramTypeCategory.Class);

                var orderItemsClass1 = orderItemsClass
                    .ToList()
                    .Where(o => o.ProgramSchedule.Sessions.ToList().Select(s => s.Start).Any(s => s.Date == model.LocalDateTime.Value.Date))
                    .GroupBy(g => g.PlayerId.Value)
                    .Select(g => g.FirstOrDefault());



                var startDate = model.LocalDateTime.Value.Date;
                var endDate = model.LocalDateTime.Value.Date.AddDays(1);

                var orderItemSubscriptions = allOrderItemsSeason
                    .Where(o => o.ProgramTypeCategory == ProgramTypeCategory.Subscription || o.ProgramTypeCategory == ProgramTypeCategory.Camp);

                orderItemSubscriptions = orderItemSubscriptions
                    .Where(o => o.OrderSessions.Any(s => s.ProgramSession.StartDateTime >= startDate && s.ProgramSession.StartDateTime < endDate))
                    .GroupBy(g => g.PlayerId.Value)
                    .Select(g => g.FirstOrDefault());


                var orderSessionsBeforeAfter = new List<OrderSession>();

                var schedulesBeforeAfter = programs.Where(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).SelectMany(p => p.ProgramSchedules).Where(p => p.ScheduleMode != TimeOfClassFormation.Both);

                var orderSessionBusiness = Ioc.OrderSessionBusiness;

                foreach (var schedule in schedulesBeforeAfter)
                {
                    orderSessionsBeforeAfter.AddRange(orderSessionBusiness.GetReportOrderSessionsWithEffectiveDate(schedule.Id, model.LocalDateTime.Value.Date));
                }

                var orderItemsBeforeAfter = orderSessionsBeforeAfter.Select(o => o.OrderItem).Distinct().AsQueryable();

                orderItemsClass1 = orderItemsClass1.Union(orderItemSubscriptions).Union(orderItemsBeforeAfter);

                var totalCount = orderItemsClass1.Count();

                var orderItems = orderItemsClass1
                    .OrderBy(o => o.Player.Contact.LastName).ThenBy(o => o.Player.Contact.FirstName)
                    .Skip((model.Page - 1) * model.PageSize).Take(model.PageSize)
                    .Select(o => new
                    {
                        PlayerId = o.PlayerId,
                        JbForm = o.JbForm,
                        ProgramSchedule = o.ProgramSchedule
                    })
                    .ToList();

                var formBusiness = Ioc.FormBusiness;

                if (orderItems == null || !orderItems.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Data = "", Message = "" });
                    return response;
                }

                var data = orderItems
                        .Select(i => new
                        {
                            ParticipantId = i.PlayerId,
                            ParticipantFirstName = string.Empty,
                            ParticipantLastName = i.JbForm != null ? string.Format("{0}, {1}", formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.LastName), formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.FirstName)) : string.Empty,
                            ParticipantGender = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.ParticipantSection, ElementsName.Gender) : string.Empty,

                            SchoolGrade = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.Grade) : string.Empty,
                            SchoolTeacherName = i.JbForm != null ? formBusiness.GetTeacher(i.JbForm) : string.Empty,

                            SchoolDismissalFromEnrichment = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,

                            ParentFirstName = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName) : string.Empty,
                            ParentLastName = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName) : string.Empty,
                            ParentEmail = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Email) : string.Empty,
                            ParentPrimaryPhone = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                            ParentAlternatePhone = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.ParentGuardianSection, ElementsName.AlternatePhone, new List<string> { "Alternate phone", "Alternative phone" }) : string.Empty,
                            ParentAuthorizedAdult = i.JbForm != null ? formBusiness.GetAuthorizedAdult(i.JbForm, null) : string.Empty,
                            ParentAuthorizedAdultPhone = i.JbForm != null ? formBusiness.GetAuthorizedAdultPhone(i.JbForm, null) : string.Empty,

                            EmergencyContactFirstName = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.FirstName) : string.Empty,
                            EmergencyContactLastName = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.LastName) : string.Empty,
                            EmergencyContactPhone = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.EmergencyContactSection, ElementsName.HomePhone, new List<string> { "Emergency contact phone number", "Emergency contact phone", "Phone" }) : string.Empty,


                            Medical_AllergiesMedicalInfo_SpecialNeeds = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, ElementsName.MedicalConditions) : string.Empty,
                            Medical_AllergiesAllergies = i.JbForm != null ? formBusiness.GetFormValue(i.JbForm, SectionsName.HealthSection, "Allergies") : string.Empty,

                            ProgramId = i.ProgramSchedule.ProgramId,
                            ProgramName = i.ProgramSchedule.Program.Name
                        }).OrderBy(a => a.ParticipantLastName).ToList();


                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = data, TotalCount = totalCount, Message = "" });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Report/GetSchoolPickups")]
        public HttpResponseMessage GetSchoolPickups([FromBody]SchoolStudentsModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var programs = Ioc.ProgramBusiness.GetList().Where(p => p.ClubId == model.ClubId && p.SeasonId == model.SeasonId && p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                   && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Season.Status == SeasonStatus.Live && (p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare));

                var programIds = programs.Where(p => p.TypeCategory != ProgramTypeCategory.BeforeAfterCare).Select(p => p.Id).ToList();

                var allOrderItemsSeason = Ioc.OrderItemBusiness.GetReportOrderItems(programIds);

                var orderItemsClass = allOrderItemsSeason.Where(o => o.ProgramTypeCategory == ProgramTypeCategory.Class);

                var orderItemsClass1 = orderItemsClass
                    .ToList()
                    .Where(o => o.ProgramSchedule.Sessions.ToList().Select(s => s.Start).Any(s => s.Date == model.LocalDateTime.Value.Date))
                    .GroupBy(g => g.PlayerId.Value)
                    .Select(g => g.FirstOrDefault());



                var startDate = model.LocalDateTime.Value.Date;
                var endDate = model.LocalDateTime.Value.Date.AddDays(1);

                var orderItemSubscriptions = allOrderItemsSeason
                    .Where(o => o.ProgramTypeCategory == ProgramTypeCategory.Subscription || o.ProgramTypeCategory == ProgramTypeCategory.Camp);

                orderItemSubscriptions = orderItemSubscriptions
                    .Where(o => o.OrderSessions.Any(s => s.ProgramSession.StartDateTime >= startDate && s.ProgramSession.StartDateTime < endDate))
                    .GroupBy(g => g.PlayerId.Value)
                    .Select(g => g.FirstOrDefault());


                var orderSessionsBeforeAfter = new List<OrderSession>();

                var schedulesBeforeAfter = programs.Where(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).SelectMany(p => p.ProgramSchedules).Where(p => p.ScheduleMode != TimeOfClassFormation.Both);

                var orderSessionBusiness = Ioc.OrderSessionBusiness;

                foreach (var schedule in schedulesBeforeAfter)
                {
                    orderSessionsBeforeAfter.AddRange(orderSessionBusiness.GetReportOrderSessionsWithEffectiveDate(schedule.Id, model.LocalDateTime.Value.Date));
                }

                var orderItemsBeforeAfter = orderSessionsBeforeAfter.Select(o => o.OrderItem).Distinct().AsQueryable();

                var allOrderItems = orderItemsClass1.Union(orderItemSubscriptions).Union(orderItemsBeforeAfter);

                var totalCount = allOrderItems.Count();

                var orderItems = allOrderItems
                    .OrderBy(o => o.Player.Contact.LastName).ThenBy(o => o.Player.Contact.FirstName)
                    .Skip((model.Page - 1) * model.PageSize).Take(model.PageSize)
                    .ToList()
                    .Select(o => new ScheduleAttendance
                    {
                        PlayerProfile = o.Player,
                        ProfileId = o.PlayerId.Value,
                        Schedule = o.ProgramSchedule,
                        ScheduleId = o.ProgramScheduleId.Value,
                        SessionId = o.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Class ? o.ProgramSchedule.Sessions.SingleOrDefault(s => s.Start.Date == model.LocalDateTime.Value.Date).Id
                            : o.OrderSessions.FirstOrDefault(s => s.ProgramSession.StartDateTime >= startDate && s.ProgramSession.StartDateTime < endDate).ProgramSessionId.Value
                    })
                    .ToList();

                var formBusiness = Ioc.FormBusiness;

                if (orderItems == null || !orderItems.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Data = "", Message = "" });
                    return response;
                }

                var profileIds = orderItems.Select(s => s.ProfileId).ToList();
                var scheduleIds = orderItems.Select(s => s.ScheduleId).ToList();
                var sessionIds = orderItems.Select(s => s.SessionId).ToList();

                var orderSessions = Ioc.OrderSessionBusiness.GetList()
                    .Where(a => profileIds.Contains(a.OrderItem.PlayerId.Value)
                                && sessionIds.Contains(a.ProgramSessionId.Value)
                                && scheduleIds.Contains(a.OrderItem.ProgramScheduleId.Value))
                    .ToList();

                var data = Ioc.ProgramBusiness.UpdateScheduleAttendance(orderItems)
                        .Select(s => new SessionPickupModel
                        {
                            Id = s.Schedule.Program.TypeCategory == ProgramTypeCategory.Class ? string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId) :
                                string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId, "-", orderSessions.FirstOrDefault(a => s.ProfileId == a.OrderItem.PlayerId.Value && s.SessionId == a.ProgramSessionId.Value && s.ScheduleId == a.OrderItem.ProgramScheduleId.Value).Id),

                            StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                            DismissalFromEnrichment = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault().JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,
                            AuthorizeAdultPickupName = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetAuthorizedAdultNames(s.PlayerProfile.OrderItems.LastOrDefault().JbForm) : new List<string>(),
                            AuthorizeAdultPickupPhone = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetAuthorizedAdultPhones(s.PlayerProfile.OrderItems.LastOrDefault().JbForm) : new List<string>(),

                            Parent1FirstName = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName) : string.Empty,
                            Parent1LastName = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName) : string.Empty,
                            Parent1PrimaryPhone = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                            Parent2FirstName = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName) : string.Empty,
                            Parent2LastName = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName) : string.Empty,
                            Parent2PrimaryPhone = s.PlayerProfile.OrderItems.LastOrDefault() != null && s.PlayerProfile.OrderItems.LastOrDefault().JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                            IsPickedUp = s.IsPickedUp,
                            PickupTime = s.Pickup != null ? s.Pickup.LocalDateTime.ToString("hh:mm tt") : string.Empty,
                            PickupName = s.Pickup != null ? s.Pickup.FullName : string.Empty,
                            AdditionalInfo = new
                            {
                                IsAbsent = s.Status.HasValue ? (s.Status == AttendanceStatus.Absent ? true : false) : false
                            },
                            ProgramId = s.Schedule.Program.Id,
                            ProgramName = s.Schedule.Program.Name
                        }).OrderBy(a => a.StudentName).ToList();


                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = data, TotalCount = totalCount, Message = "" });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [HttpPost]
        [Route("api/Report/GetSchoolAbsentee")]
        public HttpResponseMessage GetSchoolAbsentee([FromBody]SchoolStudentsModel model)
        {
            HttpResponseMessage response = null;

            try
            {
                var studentAttendanceBusiness = Ioc.StudentAttendanceBusiness;
                var programBusiness = Ioc.ProgramBusiness;

                var programs = Ioc.ProgramBusiness.GetList().Where(p => p.ClubId == model.ClubId && p.SeasonId == model.SeasonId && p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                   && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Season.Status == SeasonStatus.Live && (p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare));

                var programIds = programs.Select(p => p.Id).ToList();

                var allOrderItems = Ioc.OrderItemBusiness.GetReportOrderItems(programIds)
                    .Include(o => o.ProgramSchedule);

                var orderItemsClass = allOrderItems.Where(o => o.ProgramTypeCategory == ProgramTypeCategory.Class);

                var orderItemsClass1 = new List<ScheduleAttendance>();
                if (orderItemsClass != null && orderItemsClass.Any())
                {
                    orderItemsClass1 = orderItemsClass.ToList()
                        .Where(o => o.ProgramSchedule.Sessions.ToList().Select(s => s.Start).Any(s => s.Date == model.LocalDateTime.Value.Date))
                        .GroupBy(g => g.PlayerId.Value)
                        .Select(g => g.FirstOrDefault())
                        .Select(o => new ScheduleAttendance()
                        {
                            PlayerProfile = o.Player,
                            ProfileId = o.PlayerId.Value,
                            Schedule = o.ProgramSchedule,
                            ScheduleId = o.ProgramScheduleId.Value,
                            SessionId = o.ProgramSchedule.Sessions.ToList().SingleOrDefault(s => s.Start.Date == model.LocalDateTime.Value.Date).Id,
                            Status = programBusiness.GetStudentAttendanceStatus(o.PlayerId.Value, o.ProgramScheduleId.Value, o.ProgramSchedule.Sessions.ToList().SingleOrDefault(s => s.Start.Date == model.LocalDateTime.Value.Date).Id)
                        })
                        .ToList();
                }



                var startDate = model.LocalDateTime.Value.Date;
                var endDate = model.LocalDateTime.Value.Date.AddDays(1);

                var subscriptionIds = programs.Where(p => p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).Select(p => p.Id).ToList();

                var orderItemsSubscription1 = Ioc.OrderSessionBusiness.GetList()
                             .Include(o => o.OrderItem.Player)
                             .Include(o => o.ProgramSession)
                             .Include(o => o.OrderItem.JbForm)
                             .Include(o => o.OrderItem.ProgramSchedule)
                             .Where(o => subscriptionIds.Contains(o.OrderItem.ProgramSchedule.ProgramId) && (o.ProgramSession.StartDateTime >= startDate && o.ProgramSession.StartDateTime < endDate) &&
                                     o.OrderItem.ItemStatus == OrderItemStatusCategories.completed &&
                                    (o.OrderItem.Order.IsLive == (o.OrderItem.Season.Status == SeasonStatus.Live) ||
                                    (o.OrderItem.ItemStatus == OrderItemStatusCategories.changed &&
                                    (o.OrderItem.ItemStatusReason == OrderItemStatusReasons.regular || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.transferIn || o.OrderItem.ItemStatusReason == OrderItemStatusReasons.refund))))
                             .GroupBy(g => g.OrderItemId)
                             .Select(g => g.FirstOrDefault())
                             .OrderBy(o => o.OrderItem.Player.Contact.LastName).ThenBy(o => o.OrderItem.Player.Contact.FirstName)
                             .ToList()
                             .Select(o => new ScheduleAttendance()
                             {
                                 PlayerProfile = o.OrderItem.Player,
                                 ProfileId = o.OrderItem.PlayerId.Value,
                                 Schedule = o.OrderItem.ProgramSchedule,
                                 ScheduleId = o.OrderItem.ProgramScheduleId.Value,
                                 SessionId = o.ProgramSession.Id,
                                 Status = studentAttendanceBusiness.GetStudentStatus(o.OrderItem.PlayerId.Value, o.OrderItem.ProgramScheduleId.Value, o.ProgramSessionId.Value)
                             })
                             .ToList();


                orderItemsClass1.AddRange(orderItemsSubscription1);

                var totalCount = orderItemsClass1.Where(o => o.Status == AttendanceStatus.Absent).Count();

                var orderItems = orderItemsClass1.Where(o => o.Status == AttendanceStatus.Absent)
                    .OrderBy(o => o.PlayerProfile.Contact.LastName).ThenBy(o => o.PlayerProfile.Contact.FirstName)
                    .Skip((model.Page - 1) * model.PageSize).Take(model.PageSize)
                    .Select(o => new ScheduleAttendance
                    {
                        PlayerProfile = o.PlayerProfile,
                        ProfileId = o.ProfileId,
                        Schedule = o.Schedule,
                        ScheduleId = o.ScheduleId,
                        SessionId = o.SessionId
                    })
                    .ToList();

                var formBusiness = Ioc.FormBusiness;

                if (orderItems == null || !orderItems.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Data = "", Message = "" });
                    return response;
                }

                var data = Ioc.ProgramBusiness.UpdateScheduleAttendance(orderItems)
                    .Select(s => new SessionAbsentee
                    {
                        Id = string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId),
                        StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                        ParentFullName = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? string.Format("{0} {1}", formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName), formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName)) : string.Empty,
                        ParentPhone = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,
                        ProgramId = s.Schedule.Program.Id,
                        ProgramName = s.Schedule.Program.Name
                    }).OrderBy(s => s.StudentName).ToList();


                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = data, TotalCount = totalCount, Message = "" });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        #region

        private List<SessionPickupModel> GetSessionPickup(long programId, int sessionId)
        {
            var formBusiness = Ioc.FormBusiness;
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId).ToList();

            var result = Ioc.ProgramBusiness.GetSessionAttendance(programId, sessionId).ToList().Select(s =>
                  new SessionPickupModel
                  {
                      Id = string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId),
                      StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                      DismissalFromEnrichment = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,
                      AuthorizeAdultPickupName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetAuthorizedAdultNames(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm) : new List<string>(),
                      AuthorizeAdultPickupPhone = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetAuthorizedAdultPhones(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm) : new List<string>(),

                      Parent1FirstName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName) : string.Empty,
                      Parent1LastName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName) : string.Empty,
                      Parent1PrimaryPhone = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                      Parent2FirstName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName) : string.Empty,
                      Parent2LastName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName) : string.Empty,
                      Parent2PrimaryPhone = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                      IsPickedUp = s.IsPickedUp,
                      PickupTime = s.Pickup != null ? s.Pickup.LocalDateTime.ToString("hh:mm tt") : string.Empty,
                      PickupName = s.Pickup != null ? s.Pickup.FullName : string.Empty,
                      AdditionalInfo = new
                      {
                          IsAbsent = s.Status.HasValue ? (s.Status == AttendanceStatus.Absent ? true : false) : false
                      },
                      ProgramId = s.Schedule.Program.Id,
                      ProgramName = s.Schedule.Program.Name
                  }).OrderBy(s => s.StudentName).ToList();

            return result;
        }

        private List<SessionPickupModel> GetSessionPickup_V2_2_1(long programId, int sessionId, int page, int pageSize, ref int totalCount)
        {
            var formBusiness = Ioc.FormBusiness;

            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId).ToList();

            var result = Ioc.ProgramBusiness.GetSessionAttendance_V2_2_1(programId, sessionId, page, pageSize, ref totalCount).Select(s =>
                  new SessionPickupModel
                  {
                      Id = string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId),
                      StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                      DismissalFromEnrichment = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,
                      AuthorizeAdultPickupName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetAuthorizedAdultNames(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm) : new List<string>(),
                      AuthorizeAdultPickupPhone = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetAuthorizedAdultPhones(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm) : new List<string>(),

                      Parent1FirstName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName) : string.Empty,
                      Parent1LastName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName) : string.Empty,
                      Parent1PrimaryPhone = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                      Parent2FirstName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName) : string.Empty,
                      Parent2LastName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName) : string.Empty,
                      Parent2PrimaryPhone = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                      IsPickedUp = s.IsPickedUp,
                      PickupTime = s.Pickup != null ? s.Pickup.LocalDateTime.ToString("hh:mm tt") : string.Empty,
                      PickupName = s.Pickup != null ? s.Pickup.FullName : string.Empty,
                      AdditionalInfo = new
                      {
                          IsAbsent = s.Status.HasValue ? (s.Status == AttendanceStatus.Absent ? true : false) : false
                      },
                      ProgramId = s.Schedule.Program.Id,
                      ProgramName = s.Schedule.Program.Name
                  }).OrderBy(s => s.StudentName).ToList();

            return result;
        }

        private List<SessionPickupModel> GetSessionSubscriptionPickups(long programId, int sessionId)
        {
            var formBusiness = Ioc.FormBusiness;
            var result = new List<SessionPickupModel>();

            result = Ioc.ProgramBusiness.GetSessionSubscriptionAttendance(programId, sessionId).Select(s =>
                 new SessionPickupModel
                 {
                     Id = string.Concat(s.Id, "-", s.OrderSession.OrderItem.PlayerId, "-", s.OrderSession.OrderItem.ProgramScheduleId, "-", s.OrderSession.ProgramSessionId, "-", s.OrderSession.Id),
                     StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.FirstName)),
                     DismissalFromEnrichment = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,
                     AuthorizeAdultPickupName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetAuthorizedAdultNames(s.OrderSession.OrderItem.JbForm) : new List<string>(),
                     AuthorizeAdultPickupPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetAuthorizedAdultPhones(s.OrderSession.OrderItem.JbForm) : new List<string>(),

                     Parent1FirstName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName) : string.Empty,
                     Parent1LastName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName) : string.Empty,
                     Parent1PrimaryPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                     Parent2FirstName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName) : string.Empty,
                     Parent2LastName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName) : string.Empty,
                     Parent2PrimaryPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                     IsPickedUp = s.IsDismissed,
                     PickupTime = s.DismissalInfo != null ? s.DismissalInfo.LocalDateTime.ToString("hh:mm tt") : string.Empty,
                     PickupName = s.DismissalInfo != null ? s.DismissalInfo.FullName : string.Empty,
                     AdditionalInfo = new
                     {
                         IsAbsent = s.AttendanceStatus.HasValue ? (s.AttendanceStatus == AttendanceStatus.Absent ? true : false) : false
                     },
                     ProgramId = s.OrderSession.OrderItem.ProgramSchedule.Program.Id,
                     ProgramName = s.OrderSession.OrderItem.ProgramSchedule.Program.Name
                 }).OrderBy(s => s.StudentName).ToList();

            return result;
        }

        private List<SessionPickupModel> GetSessionSubscriptionPickups_V2_2_1(long programId, int sessionId, int page, int pageSize, ref int totalCount)
        {
            var formBusiness = Ioc.FormBusiness;
            var result = new List<SessionPickupModel>();

            result = Ioc.ProgramBusiness.GetSessionSubscriptionAttendance_V2_2_1(programId, sessionId, page, pageSize, ref totalCount).Select(s =>
                 new SessionPickupModel
                 {
                     Id = string.Concat(s.Id, "-", s.OrderSession.OrderItem.PlayerId, "-", s.OrderSession.OrderItem.ProgramScheduleId, "-", s.OrderSession.ProgramSessionId, "-", s.OrderSession.Id),
                     StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.FirstName)),
                     DismissalFromEnrichment = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,
                     AuthorizeAdultPickupName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetAuthorizedAdultNames(s.OrderSession.OrderItem.JbForm) : new List<string>(),
                     AuthorizeAdultPickupPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetAuthorizedAdultPhones(s.OrderSession.OrderItem.JbForm) : new List<string>(),

                     Parent1FirstName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName) : string.Empty,
                     Parent1LastName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName) : string.Empty,
                     Parent1PrimaryPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                     Parent2FirstName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName) : string.Empty,
                     Parent2LastName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName) : string.Empty,
                     Parent2PrimaryPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                     IsPickedUp = s.IsDismissed,
                     PickupTime = s.DismissalInfo != null ? s.DismissalInfo.LocalDateTime.ToString("hh:mm tt") : string.Empty,
                     PickupName = s.DismissalInfo != null ? s.DismissalInfo.FullName : string.Empty,
                     AdditionalInfo = new
                     {
                         IsAbsent = s.AttendanceStatus.HasValue ? (s.AttendanceStatus == AttendanceStatus.Absent ? true : false) : false
                     },
                     ProgramId = s.OrderSession.OrderItem.ProgramSchedule.Program.Id,
                     ProgramName = s.OrderSession.OrderItem.ProgramSchedule.Program.Name
                 }).OrderBy(s => s.StudentName).ToList();

            return result;
        }

        private List<SessionPickupModel> GetSessionBeforeAfterPickups(long programId, DateTime date, int page, int pageSize, ref int totalCount)
        {
            var formBusiness = Ioc.FormBusiness;
            var result = new List<SessionPickupModel>();

            result = Ioc.ProgramBusiness.GetBeforeAfterAttendance(programId, date, page, pageSize, ref totalCount).Select(s =>
                 new SessionPickupModel
                 {
                     Id = string.Concat(s.Id, "-", s.OrderSession.OrderItem.PlayerId, "-", s.OrderSession.OrderItem.ProgramScheduleId, "-", s.OrderSession.ProgramSessionId, "-", s.OrderSession.Id),
                     StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.FirstName)),
                     DismissalFromEnrichment = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,
                     AuthorizeAdultPickupName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetAuthorizedAdultNames(s.OrderSession.OrderItem.JbForm) : new List<string>(),
                     AuthorizeAdultPickupPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetAuthorizedAdultPhones(s.OrderSession.OrderItem.JbForm) : new List<string>(),

                     Parent1FirstName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName) : string.Empty,
                     Parent1LastName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName) : string.Empty,
                     Parent1PrimaryPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                     Parent2FirstName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName) : string.Empty,
                     Parent2LastName = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName) : string.Empty,
                     Parent2PrimaryPhone = s.OrderSession.OrderItem.JbForm != null ? formBusiness.GetFormValue(s.OrderSession.OrderItem.JbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                     IsPickedUp = s.IsDismissed,
                     PickupTime = s.DismissalInfo != null ? s.DismissalInfo.LocalDateTime.ToString("hh:mm tt") : string.Empty,
                     PickupName = s.DismissalInfo != null ? s.DismissalInfo.FullName : string.Empty,
                     AdditionalInfo = new
                     {
                         IsAbsent = s.AttendanceStatus.HasValue ? (s.AttendanceStatus == AttendanceStatus.Absent ? true : false) : false
                     },
                     ProgramId = s.OrderSession.OrderItem.ProgramSchedule.Program.Id,
                     ProgramName = s.OrderSession.OrderItem.ProgramSchedule.Program.Name
                 }).OrderBy(s => s.StudentName).ToList();

            return result;
        }


        private List<SessionAbsentee> GetSessionAbsentee(long programId, int sessionId)
        {
            var formBusiness = Ioc.FormBusiness;
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId).ToList();
            var program = Ioc.ProgramBusiness.Get(programId);

            var result = Ioc.ProgramBusiness.GetSessionAttendance(programId, sessionId).Where(s => s.Status == AttendanceStatus.Absent).ToList().Select(s =>
                  new SessionAbsentee
                  {
                      Id = string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId),
                      StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                      ParentFullName = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? string.Format("{0} {1}", formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName), formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName)) : string.Empty,
                      ParentPhone = orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.PlayerProfile.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,
                      ProgramId = program.Id,
                      ProgramName = program.Name
                  }).OrderBy(s => s.StudentName).ToList();

            return result;
        }

        private List<SessionAbsentee> GetSessionSubscriptionAbsentee(long programId, int sessionId)
        {
            var formBusiness = Ioc.FormBusiness;
            var orderItems = Ioc.OrderItemBusiness.GetProgramOrderItems(programId).ToList();
            var program = Ioc.ProgramBusiness.Get(programId);

            var result = Ioc.ProgramBusiness.GetSessionSubscriptionAttendance(programId, sessionId).Where(s => s.AttendanceStatus == AttendanceStatus.Absent).ToList().Select(s =>
                  new SessionAbsentee
                  {
                      Id = string.Concat(s.Id, "-", s.OrderSession.OrderItem.PlayerId, "-", s.OrderSession.OrderItem.ProgramScheduleId, "-", s.OrderSession.ProgramSessionId, "-", s.OrderSession.Id),
                      StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.OrderSession.OrderItem.Player.Contact.FirstName)),
                      ParentFullName = orderItems.Where(o => o.PlayerId == s.OrderSession.OrderItem.Player.Id).LastOrDefault() != null ? string.Format("{0} {1}", formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.OrderSession.OrderItem.Player.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName), formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.OrderSession.OrderItem.Player.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName)) : string.Empty,
                      ParentPhone = orderItems.Where(o => o.PlayerId == s.OrderSession.OrderItem.Player.Id).LastOrDefault() != null ? formBusiness.GetFormValue(orderItems.Where(o => o.PlayerId == s.OrderSession.OrderItem.Player.Id).LastOrDefault().JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,
                      ProgramId = program.Id,
                      ProgramName = program.Name
                  }).OrderBy(s => s.StudentName).ToList();

            return result;
        }

        public class PickupsStatusModel_V2_2_1
        {
            public List<PickupStatusModel_V2_2_1> PickupsStatus { get; set; }
        }

        public class PickupsStatusModel
        {
            public List<PickupStatusModel> PickupsStatus { get; set; }
            public int programId { get; set; }
            public int sessionId { get; set; }
            public int? ClubStaffId { get; set; }
        }

        #endregion

    }
}
