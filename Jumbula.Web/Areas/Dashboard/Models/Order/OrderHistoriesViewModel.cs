﻿
namespace SportsClub.Areas.Dashboard.Models
{
    public class OrderHistoriesViewModel
    {
        public string Email { get; set; }

        public string ActionDate { get; set; }

        public string Action { get; set; }

        public string Description { get; set; }

        public int Id { get; set; }
    }
}