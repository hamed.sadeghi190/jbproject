﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using SportsClub.Models.Api;
using Constants=Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers.Api
{
    [AllowAnonymous]
    [EnableCors(origins: "*", headers: "*", methods: "*", exposedHeaders: "X-Custom-Header")]
    public class ClubsController : ApiController
    {
        public object GetRelatedClubs(string domain)
        {
            object result = null;

            try
            {
                result = Ioc.ClubBusiness.GetRelatedClubs(domain).Where(r => r.ClubType.ParentType.Name == "Provider" && !(r.CatalogSetting != null && r.CatalogSetting.CatalogSearchTags.NoGlobalSearch)).Select(s =>
                       new KeyValuePair<string, string>(s.Domain, s.Name)).OrderBy(o => o.Value).ToList();
            }
            catch (Exception ex)
            {
                return new { Status = "Error", Message = "An exception has occurred." };
            }

            return result;
        }

        public object GetProvider(string providerDomain)
        {
            if (string.IsNullOrEmpty(providerDomain))
            {
                return new { Status = "Error", Message = "Provider domain can not be empty." };
            }

            var club = Ioc.ClubBusiness.Get(providerDomain);

            object result = null;

            if (club == null)
            {
                return new { Status = "Error", Message = "Provider is not exists." };
            }

            try
            {
                var catalogSetting = club.CatalogSetting;

                if (catalogSetting == null)
                {
                    Ioc.CatalogSettingBusiness.Create(club.Id);

                    catalogSetting = Ioc.CatalogSettingBusiness.Get(club.Id);
                }

                var catalogItems = new List<CatalogItem>();

                if (catalogSetting.CatalogSearchTags.NoGlobalSearch)
                {
                    catalogItems = new List<CatalogItem>();
                }
                else
                {
                    catalogItems = Ioc.CatalogBusiness.GetList(club.Id).Where(c => c.Status == CatalogStatus.Active).ToList();
                }

                var clubLogoUrl = Ioc.ClubBusiness.GetClubLogoURL(club.Domain, club.Logo);

                string basixUrl = string.Format("{0}://{1}", HttpContext.Current.Request.Url.Scheme, HttpContext.Current.Request.Url.Host);

                if (!clubLogoUrl.StartsWith("http"))
                {
                    clubLogoUrl = string.Concat(basixUrl, "/", clubLogoUrl);
                }

                result = new
                {
                    BannerImage = GetCatalogBannerImageURL(club.Domain),
                    Logo = clubLogoUrl,
                    Name = club.Name,
                    Description = club.Description,
                    Locations = catalogSetting.CatalogSettingCounties.Where(c => !c.Maybe).ToList().Select(l => new { CountyName = l.County.Name }),
                    MaybeLocations = catalogSetting.CatalogSettingCounties.Where(c => c.Maybe).ToList().Select(l => new { CountyName = l.County.Name }),
                    ProgramTypes = (catalogSetting.CatalogSearchTags.AfterSchool && catalogSetting.CatalogSearchTags.BeforeSchool) ? " Before & After School" : (catalogSetting.CatalogSearchTags.AfterSchool ? "After School" : (catalogSetting.CatalogSearchTags.BeforeSchool ? "Before School" : string.Empty)),
                    //SchoolTypes = new List<object> { "Preschools", "Elementary schools", "Middle schools", "High schools" }.ForEach(a => a) //new List<object> { "Preschools", "Elementary schools", "Middle schools", "High schools" }.Where(s => (s.Equals("Preschools") && catalogSetting.CatalogSearchTags.PreSchools)).Where(s => s.Equals("Elementary schools") && catalogSetting.CatalogSearchTags.ElementarySchools).ToList(),
                    About = new { FairFaxCounty = catalogSetting.FairFaxCounty, VirtusCertified = catalogSetting.VirtusCertified, JumbulaMobileApp = catalogSetting.JumbulaMobileApp , WeeklyEmail = catalogSetting.WeeklyEmail },
                    MemberSince = club.Setting.MemberSince.HasValue ? club.Setting.MemberSince.Value.ToString("MMM dd, yyyy") : string.Empty,
                    Catalogs = catalogItems.ToList().Select(c => new
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Description = c.Description,
                        ImageUrl = GetCatalogImageURL(club.Domain, c.ImageFileName),
                        Categories = c.Categories.ToList().Select(r => new { Name = r.Name }),
                        PriceGroups = c.PriceOptions.GroupBy(g => g.DurationTitle).Select(b =>
                        new
                        {
                            Title = b.Key,
                            Prices = b.Select(p =>
                            new
                            {
                                Amount = p.Amount,
                                SessionLength = p.Title,
                            })
                            .ToList()
                        })
                        .ToList(),
                        RestrictionTypes = GetRestrictionTypes(c.AttendeeRestriction),
                        RecommendedGradeGroupings = string.Join(", ", c.CatalogItemOptions.Where(i => i.Type == CatalogOptionType.GradeGrouping).Select(a => a.Title)),
                        MinimumMaximum = (c.MaximumEnrollment.HasValue && c.MinimumEnrollment.HasValue ? string.Format("Minimum: {0}/Maximum: {1}", c.MinimumEnrollment, c.MaximumEnrollment) : (c.MinimumEnrollment.HasValue ? string.Concat("Minimum enrollment:", c.MinimumEnrollment.Value) : c.MinimumEnrollment.HasValue ? string.Concat("Maximum enrollment:", c.MaximumEnrollment.Value) : string.Empty)),
                        InstructorStudentRatio = c.ISRatio,
                        Genders = c.AttendeeRestriction.Gender == Genders.NoRestriction ? "Boys and girls" : (c.AttendeeRestriction.Gender == Genders.Female ? "Girls only" : "Boys only"),
                        Indoor = SpaceRequirments(c, "indoor"),
                        Outdoor = SpaceRequirments(c, "outdoor"),
                        Detail = c.Detail,
                        MaterialsNeeded = c.MaterialsNeeded
                    }
                    )
                    .ToList(),
                };
            }
            catch
            {
                return new { Status = "Error", Message = "An exception has occurred." };
            }

            return result;
        }

        [Authorize]//(Roles = "Instructor, Admin, Manager")]
        [Route("api/Clubs/GetSchoolInfo")]
        public HttpResponseMessage GetSchoolInfo(int clubId, int programId)
        {
            HttpResponseMessage response = null;

            try
            {
                var club = Ioc.ClubBusiness.Get(clubId);
                var program = Ioc.ProgramBusiness.Get(programId);

                if (club.IsSchool)
                {
                    var info = new
                    {
                        OnSiteCancellationContact = StringHelper.ReturnEmptyIfNull(((SchoolSetting)club.Setting).OnSiteInfo.OnSiteCancellationContactInfo),
                        InstructorArrivalTimeAM = ((SchoolSetting)club.Setting).OnSiteInfo.InstructorArrivalTimeAM.HasValue ? ((SchoolSetting)club.Setting).OnSiteInfo.InstructorArrivalTimeAM.Value.ToString("hh:mm tt") : string.Empty,
                        InstructorArrivalTimePM = ((SchoolSetting)club.Setting).OnSiteInfo.InstructorArrivalTimePM.HasValue ? ((SchoolSetting)club.Setting).OnSiteInfo.InstructorArrivalTimePM.Value.ToString("hh:mm tt") : string.Empty,
                        ParkingForProviders = StringHelper.ReturnEmptyIfNull(((SchoolSetting)club.Setting).OnSiteInfo.ParkingForProviders),
                        TransitionInstructions = StringHelper.ReturnEmptyIfNull(((SchoolSetting)club.Setting).OnSiteInfo.TransitionInstructions),
                        AttendanceProcedure = StringHelper.ReturnEmptyIfNull(((SchoolSetting)club.Setting).OnSiteInfo.AttendanceProcedure),
                        CheckInProceduresForInstructors =
                            StringHelper.ReturnEmptyIfNull(((SchoolSetting)club.Setting).OnSiteInfo.CheckInProceduresForInstructors),
                        EnrichmentDismissalInstructions =
                            StringHelper.ReturnEmptyIfNull(((SchoolSetting)club.Setting).OnSiteInfo.EnrichmentDismissalInstructions),
                        LatePickUpPolicy = StringHelper.ReturnEmptyIfNull(((SchoolSetting)club.Setting).OnSiteInfo.LatePickUpPolicy),
                        SchoolStartTimeAM = ((SchoolSetting)club.Setting).OnSiteInfo.EnrichmentStartTimeAM.HasValue ? ((SchoolSetting)club.Setting).OnSiteInfo.EnrichmentStartTimeAM.Value.ToString("hh:mm tt") : string.Empty,
                        SchoolStartTimePM = ((SchoolSetting)club.Setting).OnSiteInfo.EnrichmentStartTimePM.HasValue ? ((SchoolSetting)club.Setting).OnSiteInfo.EnrichmentStartTimePM.Value.ToString("hh:mm tt") : string.Empty,
                        SchoolAddress = club.Address != null ? club.Address.Address : string.Empty,
                        Instructors = program.Instructors.Where(s => s.Status == StaffStatus.Active && !s.IsFrozen).Select(i => new
                        {
                            Name = i.Contact.FirstName != null ? i.Contact.FullName : string.Empty,
                            Phone = i.Contact.Phone != null ? PhoneNumberHelper.FormatPhoneNumber(i.Contact.Phone) : string.Empty
                        }),
                        OnsiteCoordinators = club.ClubStaffs.Where(c => c.JbUserRole.Role.Name == RoleCategory.OnsiteCoordinator.ToString() && c.Status == StaffStatus.Active)
                        .Select(o => new
                        {
                            Name = o.Contact.FirstName != null ? o.Contact.FullName : string.Empty,
                            Phone = o.Contact.Phone != null ? PhoneNumberHelper.FormatPhoneNumber(o.Contact.Phone) : string.Empty
                        }),
                        OnsiteSupportManagers = club.ClubStaffs.Where(c => c.JbUserRole.Role.Name == RoleCategory.OnsiteSupportManager.ToString() && c.Status == StaffStatus.Active)
                        .Select(o => new
                        {
                            Name = o.Contact.FirstName != null ? o.Contact.FullName : string.Empty,
                            Phone = o.Contact.Phone != null ? PhoneNumberHelper.FormatPhoneNumber(o.Contact.Phone) : string.Empty
                        }),
                    };

                    response = Request.CreateResponse(HttpStatusCode.OK, info);
                    return response;
                }
                else
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Status = "NotSchoolId", Message = "Id is not belongs to school." });
                    return response;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }

        [Authorize]//(Roles = "Instructor, Admin, Manager")]
        [HttpPost]
        [Route("api/Clubs/GetSchoolStudents")]
        public HttpResponseMessage GetSchoolStudents([FromBody]SchoolStudentsModel model)
        {
            HttpResponseMessage response = null;
            var formBusiness = Ioc.FormBusiness;

            try
            {
                var programs = Ioc.ProgramBusiness.GetList().Where(p => p.ClubId == model.ClubId && p.SeasonId == model.SeasonId && p.Status != ProgramStatus.Deleted && p.Status != ProgramStatus.Frozen
                   && p.LastCreatedPage >= ProgramPageStep.Step4 && p.Season.Status == SeasonStatus.Live && (p.TypeCategory == ProgramTypeCategory.Class || p.TypeCategory == ProgramTypeCategory.Subscription || p.TypeCategory == ProgramTypeCategory.Camp || p.TypeCategory == ProgramTypeCategory.BeforeAfterCare));

                var programIds = programs.Where(p => p.TypeCategory != ProgramTypeCategory.BeforeAfterCare).Select(p => p.Id).ToList();

                var allOrderItemsSeason = Ioc.OrderItemBusiness.GetReportOrderItems(programIds);

                var orderItemsClass = allOrderItemsSeason.Where(o => o.ProgramTypeCategory == ProgramTypeCategory.Class);

                var orderItemsClass1 = orderItemsClass
                    .ToList()
                    .Where(o => o.ProgramSchedule.Sessions.ToList().Select(s => s.Start).Any(s => s.Date == model.LocalDateTime.Value.Date))
                    .GroupBy(g => g.PlayerId.Value)
                    .Select(g => g.FirstOrDefault());



                var startDate = model.LocalDateTime.Value.Date;
                var endDate = model.LocalDateTime.Value.Date.AddDays(1);

                var orderItemSubscriptions = allOrderItemsSeason
                    .Where(o => o.ProgramTypeCategory == ProgramTypeCategory.Subscription || o.ProgramTypeCategory == ProgramTypeCategory.Camp);

                orderItemSubscriptions = orderItemSubscriptions
                    .Where(o => o.OrderSessions.Any(s => s.ProgramSession.StartDateTime >= startDate && s.ProgramSession.StartDateTime < endDate))
                    .GroupBy(g => g.PlayerId.Value)
                    .Select(g => g.FirstOrDefault());


                var orderSessionsBeforeAfter = new List<OrderSession>();

                var schedulesBeforeAfter = programs.Where(p => p.TypeCategory == ProgramTypeCategory.BeforeAfterCare).SelectMany(p => p.ProgramSchedules).Where(p => p.ScheduleMode != TimeOfClassFormation.Both);

                var orderSessionBusiness = Ioc.OrderSessionBusiness;

                foreach (var schedule in schedulesBeforeAfter)
                {
                    orderSessionsBeforeAfter.AddRange(orderSessionBusiness.GetReportOrderSessionsWithEffectiveDate(schedule.Id, model.LocalDateTime.Value.Date));
                }

                var orderItemsBeforeAfter = orderSessionsBeforeAfter.Select(o => o.OrderItem).Distinct().AsQueryable();

                var allOrderItems = orderItemsClass1.Union(orderItemSubscriptions).Union(orderItemsBeforeAfter);

                var totalCount = allOrderItems.Count();

                var orderItems = allOrderItems
                    .OrderBy(o => o.Player.Contact.LastName).ThenBy(o => o.Player.Contact.FirstName)
                    .Skip((model.Page - 1) * model.PageSize).Take(model.PageSize)
                    .ToList()
                    .Select(o => new ScheduleAttendance
                    {
                        PlayerProfile = o.Player,
                        ProfileId = o.PlayerId.Value,
                        Schedule = o.ProgramSchedule,
                        ScheduleId = o.ProgramScheduleId.Value,
                        SessionId = o.ProgramSchedule.Program.TypeCategory == ProgramTypeCategory.Class ? o.ProgramSchedule.Sessions.SingleOrDefault(s => s.Start.Date == model.LocalDateTime.Value.Date).Id
                            : o.OrderSessions.FirstOrDefault(s => s.ProgramSession.StartDateTime >= startDate && s.ProgramSession.StartDateTime < endDate).ProgramSessionId.Value
                    })
                    .ToList();


                if (orderItems == null || !orderItems.Any())
                {
                    response = Request.CreateResponse(HttpStatusCode.OK, new { Data = new List<SchoolStudentsViewModel>(), TotalCount = 0, Message = "" });
                    return response;
                }

                var profileIds = orderItems.Select(s => s.ProfileId).ToList();
                var scheduleIds = orderItems.Select(s => s.ScheduleId).ToList();
                var sessionIds = orderItems.Select(s => s.SessionId).ToList();

                var orderSessions = Ioc.OrderSessionBusiness.GetList()
                    .Where(a => profileIds.Contains(a.OrderItem.PlayerId.Value)
                                && sessionIds.Contains(a.ProgramSessionId.Value)
                                && scheduleIds.Contains(a.OrderItem.ProgramScheduleId.Value))
                    .ToList();

                var data = Ioc.ProgramBusiness.UpdateScheduleAttendance(orderItems)
                        .Select(s => new SchoolStudentsViewModel()
                        {
                            Id = s.Schedule.Program.TypeCategory == ProgramTypeCategory.Class ? string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId) :
                                string.Concat(s.Id, "-", s.ProfileId, "-", s.ScheduleId, "-", s.SessionId, "-", orderSessions.FirstOrDefault(a => s.ProfileId == a.OrderItem.PlayerId.Value && s.SessionId == a.ProgramSessionId.Value && s.ScheduleId == a.OrderItem.ProgramScheduleId.Value).Id),

                            StudentName = string.Format("{0}, {1}", StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.LastName), StringHelper.ReturnEmptyIfNull(s.PlayerProfile.Contact.FirstName)),
                            Gender = s.PlayerProfile.Gender,
                            ImageUrl = GetStudentImageUrl(s.PlayerProfile.Gender),
                            Status = s.Status,
                            DismissalFromEnrichment = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.SchoolSection, ElementsName.DismissalFromEnrichment, new List<string> { "Dismissal After Enrichment", "Afternoon Dismissal From Enrichment", "Dismissal From Enrichment" }) : string.Empty,
                            AuthorizeAdultPickupName = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetAuthorizedAdultNames(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm) : new List<string>(),
                            AuthorizeAdultPickupPhone = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetAuthorizedAdultPhones(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm) : new List<string>(),

                            Parent1FirstName = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName) : string.Empty,
                            Parent1LastName = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.ParentGuardianSection, ElementsName.LastName) : string.Empty,
                            Parent1PrimaryPhone = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.ParentGuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                            Parent2FirstName = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName) : string.Empty,
                            Parent2LastName = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.Parent2GuardianSection, ElementsName.LastName) : string.Empty,
                            Parent2PrimaryPhone = s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm != null ? formBusiness.GetFormValue(s.PlayerProfile.OrderItems.LastOrDefault(p => p.ProgramScheduleId.Value == s.ScheduleId).JbForm, SectionsName.Parent2GuardianSection, ElementsName.Phone, new List<string> { "Cell Phone", "Primary Phone" }) : string.Empty,

                            IsPickedUp = s.IsPickedUp,
                            PickupTime = s.Pickup != null ? s.Pickup.LocalDateTime.ToString("hh:mm tt") : string.Empty,
                            PickupName = s.Pickup != null ? s.Pickup.FullName : string.Empty,

                            ProgramId = s.Schedule.Program.Id,
                            ProgramName = s.Schedule.Program.Name
                        }).OrderBy(a => a.StudentName).ToList();

                response = Request.CreateResponse(HttpStatusCode.OK, new { Data = data, TotalCount = totalCount, Message = "" });
                return response;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                response = Request.CreateResponse(HttpStatusCode.InternalServerError, new { Message = "Something went wrong.", Exception = ex.Message });
                return response;
            }
        }
        private string GetStudentImageUrl(GenderCategories gender = GenderCategories.None)
        {
            var baseUrl = string.Format("{0}://{1}", Url.Request.RequestUri.Scheme, Url.Request.RequestUri.Host);
            var imageFolderUrl = string.Concat(baseUrl, "/Images/Mobile");
            string imageFileName = string.Empty;

            switch (gender)
            {
                case GenderCategories.None:
                    {
                        imageFileName = "allgenders.png";
                    }
                    break;
                case GenderCategories.Male:
                    {
                        imageFileName = "male.png";
                    }
                    break;
                case GenderCategories.Female:
                    {
                        imageFileName = "female.png";
                    }
                    break;
                case GenderCategories.All:
                    {
                        imageFileName = "allgenders.png";
                    }
                    break;
                default:
                    break;
            }

            return string.Concat(imageFolderUrl, "/", imageFileName);
        }

        private string SpaceRequirments(CatalogItem catalogItem, string type)
        {
            var result = string.Join(", ", catalogItem.CatalogItemOptions.Where(i => i.Type == CatalogOptionType.SpaceRequrement && (!string.IsNullOrEmpty(i.Group) && i.Group.Equals(type, StringComparison.OrdinalIgnoreCase))).Select(s => s.Title));

            return result;
        }

        private string GetRestrictionTypes(AttendeeRestriction attendeeRestriction)
        {
            var result = string.Empty;

            string minGrade = null;
            string maxGrade = null;

            if (attendeeRestriction.MinGrade.HasValue)
            {
                if (attendeeRestriction.MinGrade.Value.ToString().StartsWith("s", StringComparison.OrdinalIgnoreCase))
                {
                    minGrade = string.Concat("Grade ", attendeeRestriction.MinGrade.ToDescription());
                }
                else
                {
                    minGrade = attendeeRestriction.MinGrade.ToDescription();
                }
            }

            if (attendeeRestriction.MaxGrade.HasValue)
            {
                if (attendeeRestriction.MaxGrade.Value.ToString().StartsWith("s", StringComparison.OrdinalIgnoreCase))
                {
                    maxGrade = string.Concat("Grade ", attendeeRestriction.MaxGrade.ToDescription());
                }
                else
                {
                    maxGrade = attendeeRestriction.MaxGrade.ToDescription();
                }
            }

            if (attendeeRestriction.RestrictionType == RestrictionType.Grade)
            {
                if (!string.IsNullOrEmpty(minGrade) && !string.IsNullOrEmpty(maxGrade))
                {
                    result = string.Concat(minGrade, " to ", maxGrade);
                }
                else
                {
                    if (attendeeRestriction.MinGrade.HasValue)
                    {
                        result = string.Concat(minGrade, " to ", SchoolGradeType.College.ToDescription());
                    }
                    else if (attendeeRestriction.MaxGrade.HasValue)
                    {
                        result = string.Concat(SchoolGradeType.NotInSchool.ToDescription(), " to ", maxGrade);
                    }
                }
            }

            return result;
        }

        private string JoinStrings(string[] strings)
        {
            var result = string.Empty;

            foreach (var item in strings)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    var seperator = item != strings.First() ? ", " : string.Empty;

                    result = string.Concat(result, seperator, item);
                }

            }

            return result;
        }

        private string GetCatalogImageURL(string clubDomain, string fileName)
        {
            string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, string.Format(@"{0}\{1}", clubDomain, Constants.Path_CatalogItems_Folder), fileName).AbsoluteUri;

            if (string.IsNullOrEmpty(fileName) || !StorageManager.IsExist(imageUri))
            {
                imageUri = string.Empty;
            }

            return imageUri;
        }

        private string GetCatalogBannerImageURL(string clubDomain)
        {
            string fileName = string.Format("{0}.jpg", "Catalog_PageBanner_Image");

            string imageUri = StorageManager.GetUri(Constants.Path_ClubFilesContainer, clubDomain, fileName).AbsoluteUri;

            if (string.IsNullOrEmpty(fileName) || !StorageManager.IsExist(imageUri))
            {
                string basixUrl = "https://jumbula.com/";
                var defaultImageFile = "provider-catalog-default-banner.jpg";

                imageUri = string.Concat(basixUrl, "/Images/", defaultImageFile);
            }

            return imageUri;
        }
    }
}
