﻿namespace Jumbula.Web.Infrastructure.DataMigration
{
    public static class DataManipulationHelpers
    {
        public static string FixBadCharacters(this string term)
        {
            if (string.IsNullOrEmpty(term))
            {
                return term;
            }

            string result = term;

            result = result.Replace("'", "########");

            return result;
        }
    }
}