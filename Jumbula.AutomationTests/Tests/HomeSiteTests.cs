﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Jumbula.AutomationTests.Tests
{
    class HomeSiteTests : BaseTest<HomeSiteElements>
    {
        public HomeSiteTests(IWebDriver webDriver)
            :base(webDriver)
        {

        }
        public void SeasonGrid()
        {
            Actions action = new Actions(WebDriver);
            
            //ac.DragAndDropToOffset(Elements.SeasonGridTool, 500, 500);
            action.ClickAndHold(Elements.SeasonGridTool).MoveToElement(Elements.Tab1Body).Release().Build().Perform();

            var c = Elements.SeasonGridTitle;
            Elements.SeasonGridTitle.Click();
            ExtraAPI.Wait(1000);
            Elements.SeasonGridEditIcon.Click();
        }
    }
}
