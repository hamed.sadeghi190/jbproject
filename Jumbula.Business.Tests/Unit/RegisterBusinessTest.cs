﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.ClubSetting;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class RegisterBusinessTest
    {
        #region Fields
        private Mock<IOrderBusiness> _mockOrderBusiness;
        private Mock<IFollowupFormBusiness> _mockFollowupFormBusiness;
        private Mock<IOrderItemBusiness> _mockOrderItemBusiness;
        private Mock<IProgramBusiness> _mockProgramBusiness;
        private Mock<IClubSettingBusiness> _mockClubSettingBusiness;
        private Mock<IOrderSessionBusiness> _mockOrderSessionBusiness;
        private Mock<IClubBusiness> _mockClubBusiness;
        #endregion

        #region Constants
        private const int ClubId = 10;
        #endregion

        #region Public Methods

        [TestMethod]
        public void CheckDuplicateEnrollment_regularOrderItemsIsEmpty_NoCallGetDuplicateEnrollmentPolicy()
        {
            // Arrange
            var service = CreateService();

            // Act
            service.CheckDuplicateEnrollment(new List<OrderItem>());

            // Assert
            _mockClubSettingBusiness.Verify(x => x.GetDuplicateEnrollmentPolicy(ClubId), Times.Never);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_AllPolicyIsInActive_IsDuplicateEnrollmentShouldFalse()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItem = FactoryGirl.Build<OrderItem>(o =>
            {
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
            });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel());
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem.PlayerId, regularOrderItem.SeasonId)).Returns<OrderItem>(null);
            
            // Act
            service.CheckDuplicateEnrollment(new List<OrderItem>());

            // Assert
            regularOrderItem.IsDuplicateEnrollment.Should().BeFalse();
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForSameClassTwiceWithRegularOrderItems_HasNotAnyDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
            });

            regularOrderItems.Add(orderItem1);

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel {PreventSameClass = true});
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(orderItem1.PlayerId, orderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeFalse();
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForSameClassTwiceWithRegularOrderItemsAndSeminarOrderItem_HasNotAnyDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            var orderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.SeminarTour;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            regularOrderItems.AddRange(new List<OrderItem> {orderItem1, orderItem2});

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventSameClass = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(orderItem1.PlayerId, orderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeFalse();
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForSameClassTwiceWithRegularOrderItemsAndCampOrderItem_HasNotAnyDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            var orderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            regularOrderItems.AddRange(new List<OrderItem> { orderItem1, orderItem2 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventSameClass = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(orderItem1.PlayerId, orderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeFalse();
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForSameClassTwiceWithRegularOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            var orderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            regularOrderItems.AddRange(new List<OrderItem> {orderItem1, orderItem2});

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventSameClass = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(orderItem1.PlayerId, orderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.DuplicateRegistrationInCart);
            regularOrderItems.ElementAt(1).ValidationMessages.First().Should().Be(CartConstants.DuplicateRegistrationInCart);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForSameClassTwiceWithRegisteredOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            var registeredOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventSameClass = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem> { registeredOrderItem1 }.AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.AlreadyRegisteredInSeason);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForMultiClassWithConflictingHoursWithRegularOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var date = DateTime.UtcNow;

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 12,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 2, ScheduleId = 12, Start = date.AddHours(2), End = date.AddHours(3)
                        }
                    }
                };
            });

            var regularOrderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 13,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 3, ScheduleId = 13, Start = date.AddHours(2), End = date.AddHours(3)
                        }
                    }
                };
            });

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1, regularOrderItem2 });

            var allSessions = regularOrderItem2.ProgramSchedule.Sessions;
            allSessions.AddRange(regularOrderItem1.ProgramSchedule.Sessions);

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventOtherClasses = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem2.ProgramSchedule, regularOrderItem1.ProgramSchedule })).Returns(allSessions);
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem1.ProgramSchedule, regularOrderItem2.ProgramSchedule })).Returns(allSessions);

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.ConflictClassScheduleWithPreviousRegistration);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForMultiClassWithConflictingHoursWithRegularOrderItemsAndTime7_745_HasNoDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var startDate1 = DateTime.Parse("2019-1-1 8:00:00 AM");
            var endDate1 = DateTime.Parse("2019-1-1 9:00:00 AM");

            var startDate2 = DateTime.Parse("2019-1-1 7:00:00 AM");
            var endDate2 = DateTime.Parse("2019-1-1 7:45:00 AM");

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 12,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 2, ScheduleId = 12, Start = startDate1, End = endDate1
                        }
                    }
                };
            });

            var regularOrderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 13,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 3, ScheduleId = 13, Start = startDate2, End = endDate2
                        }
                    }
                };
            });

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1, regularOrderItem2 });

            var allSessions = regularOrderItem2.ProgramSchedule.Sessions;
            allSessions.AddRange(regularOrderItem1.ProgramSchedule.Sessions);

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventOtherClasses = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem2.ProgramSchedule, regularOrderItem1.ProgramSchedule })).Returns(allSessions);
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem1.ProgramSchedule, regularOrderItem2.ProgramSchedule })).Returns(allSessions);

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeFalse();
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForMultiClassWithConflictingHoursWithRegularOrderItemsAndTime7_8_HasNoDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var startDate1 = DateTime.Parse("2019-1-1 8:00:00 AM");
            var endDate1 = DateTime.Parse("2019-1-1 9:00:00 AM");

            var startDate2 = DateTime.Parse("2019-1-1 7:00:00 AM");
            var endDate2 = DateTime.Parse("2019-1-1 8:0:00 AM");

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 12,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 2, ScheduleId = 12, Start = startDate1, End = endDate1
                        }
                    }
                };
            });

            var regularOrderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 13,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 3, ScheduleId = 13, Start = startDate2, End = endDate2
                        }
                    }
                };
            });

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1, regularOrderItem2 });

            var allSessions = regularOrderItem2.ProgramSchedule.Sessions;
            allSessions.AddRange(regularOrderItem1.ProgramSchedule.Sessions);

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventOtherClasses = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem2.ProgramSchedule, regularOrderItem1.ProgramSchedule })).Returns(allSessions);
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem1.ProgramSchedule, regularOrderItem2.ProgramSchedule })).Returns(allSessions);

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeFalse();
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForMultiClassWithConflictingHoursWithRegularOrderItemsAndTime730_830_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var startDate1 = DateTime.Parse("2019-1-1 8:00:00 AM");
            var endDate1 = DateTime.Parse("2019-1-1 9:00:00 AM");

            var startDate2 = DateTime.Parse("2019-1-1 7:30:00 AM");
            var endDate2 = DateTime.Parse("2019-1-1 8:30:00 AM");

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 12,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 2, ScheduleId = 12, Start = startDate1, End = endDate1
                        }
                    }
                };
            });

            var regularOrderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 13,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 3, ScheduleId = 13, Start = startDate2, End = endDate2
                        }
                    }
                };
            });

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1, regularOrderItem2 });

            var allSessions = regularOrderItem2.ProgramSchedule.Sessions;
            allSessions.AddRange(regularOrderItem1.ProgramSchedule.Sessions);

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventOtherClasses = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem2.ProgramSchedule, regularOrderItem1.ProgramSchedule })).Returns(allSessions);
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem1.ProgramSchedule, regularOrderItem2.ProgramSchedule })).Returns(allSessions);

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForMultiClassWithConflictingHoursWithRegularOrderItemsAndSeminar_HasNoDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();
            
            var startDate1 = DateTime.Parse("2019-1-1 8:00:00 AM");
            var endDate1 = DateTime.Parse("2019-1-1 9:00:00 AM");

            var startDate2 = DateTime.Parse("2019-1-1 7:00:00 AM");
            var endDate2 = DateTime.Parse("2019-1-1 8:00:00 AM");

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 12,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 2, ScheduleId = 12, Start = startDate1, End = endDate1
                        }
                    }
                };
            });

            var regularOrderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.SeminarTour;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 13,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 3, ScheduleId = 13, Start = startDate2, End = endDate2
                        }
                    }
                };
            });

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1, regularOrderItem2 });

            var allSessions = regularOrderItem2.ProgramSchedule.Sessions;
            allSessions.AddRange(regularOrderItem1.ProgramSchedule.Sessions);

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventOtherClasses = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem2.ProgramSchedule, regularOrderItem1.ProgramSchedule })).Returns(allSessions);
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { regularOrderItem1.ProgramSchedule, regularOrderItem2.ProgramSchedule })).Returns(allSessions);

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeFalse();
        }


        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterClassForMultiClassWithConflictingHoursWithRegisteredOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var date = DateTime.UtcNow;

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 12,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 2, ScheduleId = 12, Start = date.AddHours(2), End = date.AddHours(3)
                        }
                    }
                };
            });

            var registeredOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule
                {
                    Id = 13,
                    Sessions = new List<ScheduleSession>
                    {
                        new ScheduleSession
                        {
                            Id = 3, ScheduleId = 13, Start = date.AddHours(2), End = date.AddHours(3)
                        }
                    }
                };
            });

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1 });

            var allSessions = registeredOrderItem1.ProgramSchedule.Sessions;
            allSessions.AddRange(regularOrderItem1.ProgramSchedule.Sessions);

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventOtherClasses = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem> { registeredOrderItem1 }.AsQueryable());
            _mockProgramBusiness.Setup(x => x.GetSessions(new List<ProgramSchedule> { registeredOrderItem1.ProgramSchedule, regularOrderItem1.ProgramSchedule })).Returns(allSessions);

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.ConflictClassSchedule);
        }


        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterCampForSameScheduleOfTheSameCampTwiceWithRegularOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            var orderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            regularOrderItems.AddRange(new List<OrderItem> { orderItem1, orderItem2 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventSameCampSchedule = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(orderItem1.PlayerId, orderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.DuplicateRegistrationInCart);
            regularOrderItems.ElementAt(1).ValidationMessages.First().Should().Be(CartConstants.DuplicateRegistrationInCart);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterCampForSameScheduleOfTheSameCampTwiceWithRegisteredOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            var registeredOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.ProgramScheduleId = 12;
            });

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventSameCampSchedule = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem> { registeredOrderItem1 }.AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.AlreadyRegisteredInSeason);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterCampForMultipleDropInOnTheSameDayWithConflictingHoursWithRegularOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var date = DateTime.UtcNow;

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.DropIn;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule { Id = 12};
            });

            var regularOrderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.DropIn;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule { Id = 13 };
            });

            var orderSessions1 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 1,
                    OrderItem = regularOrderItem1,
                    OrderItemId = regularOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = date.AddHours(1), EndDateTime = date.AddHours(4)}
                }
            };

            var orderSessions2 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 2,
                    OrderItem = regularOrderItem2,
                    OrderItemId = regularOrderItem2.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = date.AddHours(2), EndDateTime = date.AddHours(4)}
                }
            };

            var allOrderSessions = orderSessions1;
            allOrderSessions.AddRange(orderSessions2);

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1, regularOrderItem2 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventMultipleCampDropIn = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());

            _mockOrderSessionBusiness.Setup(x => x.GetList(It.IsAny<Expression<Func<OrderSession, bool>>>())).Returns(allOrderSessions.AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.ConflictDropInInCart);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterCampForMultipleDropInOnTheSameDayWithConflictingHoursWithRegisteredOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var date = DateTime.UtcNow;

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.DropIn;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule { Id = 12 };
            });

            var registeredOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.DropIn;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule { Id = 13 };
            });

            var orderSessions1 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 1,
                    OrderItem = regularOrderItem1,
                    OrderItemId = regularOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = date.AddHours(1), EndDateTime = date.AddHours(4)}
                }
            };

            var orderSessions2 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 2,
                    OrderItem = registeredOrderItem1,
                    OrderItemId = registeredOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = date.AddHours(2), EndDateTime = date.AddHours(4)}
                }
            };

            var allOrderSessions = orderSessions1;
            allOrderSessions.AddRange(orderSessions2);

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventMultipleCampDropIn = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem> { registeredOrderItem1 }.AsQueryable());

            _mockOrderSessionBusiness.Setup(x => x.GetList(It.IsAny<Expression<Func<OrderSession, bool>>>())).Returns(allOrderSessions.AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.ConflictDropInWithPreviousDropIn);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterCampForMultipleDropInOnTheSameDayWithConflictingHoursWithRegisteredOrderItemsAndTime7_745_HasNoDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var startDate1 = DateTime.Parse("2019-1-1 8:00:00 AM");
            var endDate1 = DateTime.Parse("2019-1-1 9:00:00 AM");

            var startDate2 = DateTime.Parse("2019-1-1 7:00:00 AM");
            var endDate2 = DateTime.Parse("2019-1-1 7:45:00 AM");

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.DropIn;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule { Id = 12 };
            });

            var registeredOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.DropIn;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule { Id = 13 };
            });

            var orderSessions1 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 1,
                    OrderItem = regularOrderItem1,
                    OrderItemId = regularOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = startDate1, EndDateTime = endDate1, ProgramScheduleId = 12}
                    
                }
            };

            var orderSessions2 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 2,
                    OrderItem = registeredOrderItem1,
                    OrderItemId = registeredOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 2, StartDateTime = startDate2, EndDateTime = endDate2, ProgramScheduleId = 13}
                }
            };

            var allOrderSessions = orderSessions1;
            allOrderSessions.AddRange(orderSessions2);

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventMultipleCampDropIn = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem> { registeredOrderItem1 }.AsQueryable());

            _mockOrderSessionBusiness.Setup(x => x.GetList(It.IsAny<Expression<Func<OrderSession, bool>>>())).Returns(allOrderSessions.AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeFalse();
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterCampForMultipleDropInOnTheSameDayWithConflictingHoursWithRegisteredOrderItemsAndTime730_830_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var startDate1 = DateTime.Parse("2019-1-1 8:00:00 AM");
            var endDate1 = DateTime.Parse("2019-1-1 9:00:00 AM");

            var startDate2 = DateTime.Parse("2019-1-1 7:30:00 AM");
            var endDate2 = DateTime.Parse("2019-1-1 8:30:00 AM");

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.DropIn;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule { Id = 12 };
            });

            var registeredOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.Camp;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.DropIn;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule { Id = 13 };
            });

            var orderSessions1 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 1,
                    OrderItem = regularOrderItem1,
                    OrderItemId = regularOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = startDate1, EndDateTime = endDate1, ProgramScheduleId = 12}

                }
            };

            var orderSessions2 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 2,
                    OrderItem = registeredOrderItem1,
                    OrderItemId = registeredOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 2, StartDateTime = startDate2, EndDateTime = endDate2, ProgramScheduleId = 13}
                }
            };

            var allOrderSessions = orderSessions1;
            allOrderSessions.AddRange(orderSessions2);

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventMultipleCampDropIn = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem> { registeredOrderItem1 }.AsQueryable());

            _mockOrderSessionBusiness.Setup(x => x.GetList(It.IsAny<Expression<Func<OrderSession, bool>>>())).Returns(allOrderSessions.AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
        }


        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterBeforeAfterForProgramWithConflictingDayAndHourWithRegularOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var date = DateTime.UtcNow;

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.BeforeAfterCare;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.Noraml;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule { Id = 12 };
            });

            var regularOrderItem2 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.BeforeAfterCare;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.Noraml;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule { Id = 13 };
            });

            var orderSessions1 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 1,
                    OrderItem = regularOrderItem1,
                    OrderItemId = regularOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = date.AddHours(1), EndDateTime = date.AddHours(4)}
                }
            };

            var orderSessions2 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 2,
                    OrderItem = regularOrderItem2,
                    OrderItemId = regularOrderItem2.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = date.AddHours(2), EndDateTime = date.AddHours(4)}
                }
            };

            var allOrderSessions = orderSessions1;
            allOrderSessions.AddRange(orderSessions2);

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1, regularOrderItem2 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventSameProgramBeforeAfterWeekDay = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem>().AsQueryable());

            _mockOrderSessionBusiness.Setup(x => x.GetList(It.IsAny<Expression<Func<OrderSession, bool>>>())).Returns(allOrderSessions.AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.ConflictProgramsInCart);
        }

        [TestMethod]
        public void CheckDuplicateEnrollment_RegisterBeforeAfterForProgramWithConflictingDayAndHourWithRegisteredOrderItems_HasDuplicateEnrollments()
        {
            // Arrange
            var service = CreateService();

            var regularOrderItems = new List<OrderItem>();

            var date = DateTime.UtcNow;

            var regularOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.BeforeAfterCare;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.Noraml;
                o.ProgramScheduleId = 12;
                o.ProgramSchedule = new ProgramSchedule { Id = 12 };
            });

            var registeredOrderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 2;
                o.PlayerId = 2;
                o.ProgramTypeCategory = ProgramTypeCategory.BeforeAfterCare;
                o.Order.ClubId = 1;
                o.Mode = OrderItemMode.Noraml;
                o.ProgramScheduleId = 13;
                o.ProgramSchedule = new ProgramSchedule { Id = 13 };
            });

            var orderSessions1 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 1,
                    OrderItem = regularOrderItem1,
                    OrderItemId = regularOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = date.AddHours(1), EndDateTime = date.AddHours(4)}
                }
            };

            var orderSessions2 = new List<OrderSession>
            {
                new OrderSession()
                {
                    Id = 2,
                    OrderItem = registeredOrderItem1,
                    OrderItemId = registeredOrderItem1.Id,
                    ProgramSession = new ProgramSession() {Id = 1, StartDateTime = date.AddHours(2), EndDateTime = date.AddHours(4)}
                }
            };

            var allOrderSessions = orderSessions1;
            allOrderSessions.AddRange(orderSessions2);

            regularOrderItems.AddRange(new List<OrderItem> { regularOrderItem1 });

            _mockClubSettingBusiness.Setup(x => x.GetDuplicateEnrollmentPolicy(1)).Returns(new DuplicateEnrollmentPolicySettingViewModel { PreventSameProgramBeforeAfterWeekDay = true });
            _mockOrderItemBusiness.Setup(x => x.GetRegisteredProfileBySeason(regularOrderItem1.PlayerId, regularOrderItem1.SeasonId)).Returns(new List<OrderItem> { registeredOrderItem1 }.AsQueryable());

            _mockOrderSessionBusiness.Setup(x => x.GetList(It.IsAny<Expression<Func<OrderSession, bool>>>())).Returns(allOrderSessions.AsQueryable());

            // Act
            service.CheckDuplicateEnrollment(regularOrderItems);

            // Assert
            regularOrderItems.Any(o => o.IsDuplicateEnrollment).Should().BeTrue();
            regularOrderItems.ElementAt(0).ValidationMessages.First().Should().Be(CartConstants.ProgramConflictWithPreviousRegistration);
        }

        #endregion

        #region Private Methods
        private RegisterBusiness CreateService()
        {
            _mockOrderBusiness = new Mock<IOrderBusiness>();
            _mockFollowupFormBusiness = new Mock<IFollowupFormBusiness>();
            _mockOrderItemBusiness = new Mock<IOrderItemBusiness>();
            _mockProgramBusiness = new Mock<IProgramBusiness>();
            _mockClubSettingBusiness = new Mock<IClubSettingBusiness>();
            _mockOrderSessionBusiness = new Mock<IOrderSessionBusiness>();
            _mockClubBusiness = new Mock<IClubBusiness>();

            return new RegisterBusiness(_mockOrderBusiness.Object, _mockFollowupFormBusiness.Object, _mockOrderItemBusiness.Object, _mockProgramBusiness.Object, _mockClubSettingBusiness.Object, _mockOrderSessionBusiness.Object, _mockClubBusiness.Object);
        }
        #endregion


    }
}
