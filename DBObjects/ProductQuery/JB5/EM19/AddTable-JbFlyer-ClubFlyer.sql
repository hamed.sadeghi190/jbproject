
GO

/****** Object:  Table [dbo].[JbFlyers]    Script Date: 7/30/2017 11:22:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JbFlyers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NULL,
	[FlyerContent] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.JbFlyers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


/****** Object:  Table [dbo].[ClubFlyers]    Script Date: 7/30/2017 11:23:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ClubFlyers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClubId] [int] NOT NULL,
	[SeasonId] [bigint] NOT NULL,
	[FlyerId] [int] NOT NULL,
 CONSTRAINT [PK_dbo.ClubFlyers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ClubFlyers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubFlyers_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClubFlyers] CHECK CONSTRAINT [FK_dbo.ClubFlyers_dbo.Clubs_ClubId]
GO

ALTER TABLE [dbo].[ClubFlyers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubFlyers_dbo.JbFlyers_FlyerId] FOREIGN KEY([FlyerId])
REFERENCES [dbo].[JbFlyers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClubFlyers] CHECK CONSTRAINT [FK_dbo.ClubFlyers_dbo.JbFlyers_FlyerId]
GO

ALTER TABLE [dbo].[ClubFlyers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ClubFlyers_dbo.Seasons_SeasonId] FOREIGN KEY([SeasonId])
REFERENCES [dbo].[Seasons] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ClubFlyers] CHECK CONSTRAINT [FK_dbo.ClubFlyers_dbo.Seasons_SeasonId]
GO
