﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('addEditeESignatureController', ['$scope', '$http', '$stateParams', '$state', 'jbToast', function ($scope, $http, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'invoice_View';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + '/Dashboard/ESignature';


        $scope.Model = {};

        $http.get($scope.baseUrl + '/CreatEditEsignature?documentSignId=' + $stateParams.documentSignId).success(function (data, status, headers, config) {

            $scope.Model = data;
        });


        $scope.pageMode;

        if ($state.current.name == 'InvoiceDetail') {

            $scope.pageMode = 'AdminInvoiceView'
        }
        else {
            $scope.pageMode = 'ParentInvoiceView'
        }

        $scope.OnUploaderSuccess = function (e) {

                if (e.response.Status == true && e.response.Data == 'Add') {

                    $scope.uploadedList = true;

                    $scope.Model.FileName = e.response.FileName;
                    $scope.Model.File = e.response.uploadedList;

                }
                else if (e.response.Status == true && e.response.Data == 'Remove') {
                    $scope.Model.File = null;
                    jbToast.success("Remove uploaded file completed.");
                }

            }
        
        $scope.onUpload = function (e) {

            $scope.Model.File = e.files[0].rawFile;
            var files = e.files;
        }

        $scope.SaveDocument = function () {
            if ($scope.Model.File == null) {
                jbToast.error("You should upload your file.");
            } else {

                $http.post($scope.baseUrl + '/CreatEditEsignature', { model: $scope.Model })
               .success(function (data, status, headers, config) {
                   if (data.Status == true) {
                       jbToast.success("Your document sent successfuly. ");
                       $state.go('ESignature');
                   }
                   else {
                       $scope.MC.handleErrors($scope, data);
                   }
               });
            }

        }

        $scope.cancelAction = function () {
            if ("comesFromOrderItem" in window) {
                if (window.comesFromOrderItem) {
                    window.comesFromOrderItem = false;
                    $state.go("ESignature", null);
                }
            }
            else {
                $state.go("ESignature", null);
            }
        }
    }])

})();