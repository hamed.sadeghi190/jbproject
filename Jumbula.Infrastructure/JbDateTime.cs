﻿using Jumbula.Core.Infrastructure;
using System;

namespace Jumbula.Infrastructure
{
    public class JbDateTime : IJbDateTime
    {
        public DateTime Now => DateTime.Now;
        public DateTime UtcNow => DateTime.UtcNow;
    }
}
