﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignAddStep2controller', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'templateEditorService', 'CampaignService', 'jbConfirmation', 'imageUploadService', function ($scope, $http, jbToast, $stateParams, $state, templateEditorService, CampaignService, jbConfirmation, imageUploadService) {
        window.app.globalObjects.pageClass = 'campaigns-add-step2';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $scope.campaignId = $scope.MC.stateParams.campaignId;
        $scope.isMultiSeason = $scope.MC.stateParams.isMultiSeason;
        $scope.listId = $scope.MC.stateParams.listId;

        if (is.undefined($scope.Model)) {
            $scope.Model = {};
            $scope.Model.MailTemplate = {};
            $scope.Model.Attachments = [];
        }


        //Image validate and crop start
        $scope.validateImage = function (elem, pos) {
            if (pos == 'header') {
                $scope.positionBanner = 'header';
            }
            else if (pos = 'footer') {
                $scope.positionBanner = 'footer';
            }
            imageUploadService.validateImage(this, elem)

        }
        $scope.typeBannerHeader = function (typeb) {
            $scope.typeBannerHeaderValue = typeb;
        }
        $scope.typeBannerFooter = function (typeb) {
            $scope.typeBannerFooterValue = typeb;
        }
        $scope.showCropper1 = true;
        $scope.showCropper2 = false;
        $scope.hideCropper1 = function () {
            $scope.showCropper1 = false;
            $scope.showCropper2 = true;
            $scope.cropper = imageUploadService.clearCropper(this);
        }
        $scope.hideCropper2 = function () {
            $scope.showCropper1 = true;
            $scope.showCropper2 = false;
            $scope.cropper = imageUploadService.clearCropper(this);
        }
        $scope.clearCropper = function () {
            $scope.cropper = imageUploadService.clearCropper(this);

        }

        var decodeHtml = function (text) {
            var txt = document.createElement("textarea");
            txt.innerHTML = text;
            return txt.value;
        }

        $scope.fixTextIssueInEditro = function () {
            $scope.Model.Body = decodeHtml($scope.Model.Body);
        }

        // use service.
        $scope.mailDesignerInit = function () {
            return templateEditorService.mailDesignerInit(this);
        }

        $scope.existingImages = templateEditorService.existingImages;

        $scope.setHeaderBackground = function (image) {
            return templateEditorService.setHeaderBackground(this, image);
        }

        $scope.getHeaderBackground = function () {
            return templateEditorService.getHeaderBackground(this);
        }

        $scope.setFooterBackground = function (image) {
            return templateEditorService.setFooterBackground(this, image);
        }

        $scope.getFooterBackground = function () {
            return templateEditorService.getFooterBackground(this);
        }

        $scope.getClubLogo = function () {
            return templateEditorService.getClubLogo(this);
        }

        $scope.enterEditing = function () {
            return templateEditorService.enterEditing(this);
        }

        $scope.saveEditing = function (posbanner, typeBannerHV, typeBannerFV) {
            $scope.Model.MailTemplate.ChangeTemplate = true;
            $scope.positionBanner = null;
            return templateEditorService.saveEditing(this, posbanner, typeBannerHV, typeBannerFV);
        }

        $scope.cancelEditing = function () {
            $scope.positionBanner = null;
            return templateEditorService.cancelEditing(this);
        }
        //Add for icon information
        jQuery(function ($) {
            $(".icon").click(function () {
                $("~ .icon-msg-box", this).slideToggle(500);
            });
        });

        $http.get(baseUrl + '/CampaignAddEditStep2', { params: { campaignId: $scope.campaignId } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
                $scope.ClubLogo = $scope.Model.MailTemplate.ClubLogo;
                var oldModel = CampaignService.getModelFromStrorage("campaignModel");

                /* must change this code in future */
                if (is.not.undefined(oldModel) && oldModel.Id == $scope.Model.Id) {
                   
                    $scope.Model.Body = oldModel.Body;
                    $scope.Model.Subject = oldModel.Subject;
                    $scope.Model.EditMode = oldModel.EditMode;
                    $scope.Model.EmailStyle = oldModel.EmailStyle;
                    $scope.Model.SelectedTemplateId = oldModel.SelectedTemplateId;
                    $scope.Model.MailTemplate = oldModel.MailTemplate;
                    $scope.Model.MailTemplate.ClubLogo = $scope.ClubLogo;
                    $scope.Model.IsReplyToEmailSet = oldModel.IsReplyToEmailSet;
                    $scope.Model.IsCcListEmailSet = oldModel.IsCcListEmailSet;
                    $scope.Model.ReplyTo = oldModel.ReplyTo;
                    $scope.Model.Ccs = oldModel.Ccs;
                }
            });


        $scope.OnUploaderSuccess = function (e) {
            if (e.response.data && e.response.action == 'Add') {
                if ($scope.Model.Attachments == null) {
                    $scope.Model.Attachments = [];
                }
                $scope.Model.Attachments.push(e.response.data);
                jbToast.success("Attachment(s) Uploaded.");
            } else if (e.response.data && e.response.action == 'Remove') {
                $scope.Model.Attachments.removeByPropertyValue("FileUrl", e.response.data);
                jbToast.success("Attachment(s) Removed.");
            }
        }

        $scope.removeOldAttachments = function (fileName) {
            $http.post(baseUrl + '/DeleteUploadedFile', { fileNames: fileName })
                .success(function (data, status, headers, config) {
                    if (status == 200) {
                        $scope.Model.Attachments.removeByPropertyValue("FileUrl", data.data);
                        jbToast.success("Attachment(s) Removed.");
                    }
                });
        }

        $scope.ChangeTemplate = function (idx) {
            $http.get(baseUrl + '/GetTemplate', { params: { id: idx } }).success(function (data, stats, headers, config) {
                $scope.Model.MailTemplate = data;
                if ($scope.Model.MailTemplate.Name == "") {
                    $scope.Model.MailTemplate.ChangeTemplate = true;
                } else {
                    $scope.Model.MailTemplate.ChangeTemplate = false;
                }
            });
        };

        if ($scope.MC.stateParams.campaignType == "UserList") {
            $scope.Model.ListId = $scope.MC.stateParams.listId;
        }

        $scope.SaveStep2 = function () {
            $http.post(baseUrl + '/CampaignAddEditStep2', { model: $scope.Model })
              .success(function (data, status, headers, config) {
                  if (data.Status == true) {
                      CampaignService.setModelInStorage($scope.Model, "campaignModel");
                      $state.go('EmailCampaignAddStep3', { campaignId: data.Data, campaignType: $scope.MC.stateParams.campaignType });
                  }
                  else {
                      $scope.MC.handleErrors($scope, data);
                  }
              });
        }

        $scope.disableBackStepInEditMode = function () {
            var oldModel = CampaignService.get();
            if ((is.not.undefined($scope.MC.stateParams.campaignId) && is.undefined(oldModel)) || (is.under($scope.Model.LastCreatedPage, 3) && is.undefined(oldModel))) {
                jbToast.warning("Your criteria has been saved, If you try to change them please create a new campaign.");
            } else {
                $state.go('EmailCampaignAddSomePeopleStep1', { campaignId: $scope.MC.stateParams.campaignId });
            }
        }

        $scope.editRecipients = function () {
            if (is.not.undefined($scope.Model.Body)) {
                $scope.Model.Body = decodeHtml($scope.Model.Body);
            }
            CampaignService.setModelInStorage($scope.Model, "campaignModel");
            $state.go('EmailCampaignDisplayAllRecipients', { campaignId: $scope.MC.stateParams.campaignId, campaignType: $scope.MC.stateParams.campaignType });
        }

        $scope.GoStep1 = function () {
            jbConfirmation
               .yes(function () {
                   $state.go('EmailCampaignAddSomePeopleStep1', { campaignId: $scope.MC.stateParams.campaignId, campaignType: $scope.MC.stateParams.campaignType });
               })
              .title('Warrning')
              .message('Are you sure you want to go back? You will lose any changes you might have done to the list of recipients.')
              .confirm();
        }
    }]);
})();