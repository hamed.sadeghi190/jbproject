﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
    class HomePageTest : BaseTest<HomePageElements>
    {
        public HomePageTest(IWebDriver webDriver)
            :base(webDriver)
        {
            
        }

        public void ClickLoginLink()
        {
            Elements.LoginLink.Click();
            ExtraAPI.Wait(5000);
        }
    }
}
