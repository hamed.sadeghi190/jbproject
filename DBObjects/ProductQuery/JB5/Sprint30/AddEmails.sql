
GO

/****** Object:  Table [dbo].[Emails]    Script Date: 12/11/2018 10:37:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Emails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](255) NULL,
	[DateCreated] [datetime] NOT NULL,
	[ReplyTo] [nvarchar](255) NULL,
	[FromEmail] [nvarchar](255) NULL,
	[ScheduleDate] [datetime] NULL,
	[Parameters] [nvarchar](max) NULL,
	[Priority] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[CallerType] [tinyint] NOT NULL,
	[Category] [tinyint] NOT NULL,
	[SubCategory] [nvarchar](max) NULL,
	[SentDate] [datetime] NULL,
	[FromDisplayName] [nvarchar](255) NULL,
	[Response] [nvarchar](max) NULL,
	[AttemptNumber] [int] NOT NULL,
	[LastAttemptTime] [datetime] NULL,
 CONSTRAINT [PK_dbo.Emails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Emails] ADD  DEFAULT ('1900-01-01T00:00:00.000') FOR [DateCreated]
GO

ALTER TABLE [dbo].[Emails] ADD  DEFAULT ((0)) FOR [Priority]
GO

ALTER TABLE [dbo].[Emails] ADD  DEFAULT ((0)) FOR [Status]
GO

ALTER TABLE [dbo].[Emails] ADD  DEFAULT ((0)) FOR [CallerType]
GO

ALTER TABLE [dbo].[Emails] ADD  DEFAULT ((0)) FOR [Category]
GO

ALTER TABLE [dbo].[Emails] ADD  DEFAULT ((0)) FOR [AttemptNumber]
GO


GO

/****** Object:  Table [dbo].[EmailRecipients]    Script Date: 12/11/2018 10:39:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailRecipients](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailAddress] [nvarchar](255) NULL,
	[Delivered] [bit] NOT NULL,
	[DeliverDate] [datetime] NULL,
	[Clicked] [bit] NOT NULL,
	[ClickDate] [datetime] NULL,
	[Type] [tinyint] NOT NULL,
	[EmailId] [int] NOT NULL,
	[Opened] [bit] NOT NULL,
	[OpenDate] [datetime] NULL,
	[DisplayName] [nvarchar](255) NULL,
	[Dropped] [bit] NOT NULL,
	[DropDate] [datetime] NULL,
	[Bounced] [bit] NOT NULL,
	[Blocked] [bit] NOT NULL,
	[Deferred] [bit] NOT NULL,
	[SpamReport] [bit] NOT NULL,
	[Unsubscribe] [bit] NOT NULL,
	[BounceDate] [datetime] NULL,
	[DefereDate] [datetime] NULL,
	[SpamReportDate] [datetime] NULL,
	[UnsubscribeDate] [datetime] NULL,
 CONSTRAINT [PK_dbo.EmailRecipients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[EmailRecipients] ADD  DEFAULT ((0)) FOR [Opened]
GO

ALTER TABLE [dbo].[EmailRecipients] ADD  DEFAULT ((0)) FOR [Dropped]
GO

ALTER TABLE [dbo].[EmailRecipients] ADD  DEFAULT ((0)) FOR [Bounced]
GO

ALTER TABLE [dbo].[EmailRecipients] ADD  DEFAULT ((0)) FOR [Blocked]
GO

ALTER TABLE [dbo].[EmailRecipients] ADD  DEFAULT ((0)) FOR [Deferred]
GO

ALTER TABLE [dbo].[EmailRecipients] ADD  DEFAULT ((0)) FOR [SpamReport]
GO

ALTER TABLE [dbo].[EmailRecipients] ADD  DEFAULT ((0)) FOR [Unsubscribe]
GO

ALTER TABLE [dbo].[EmailRecipients]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EmailRecipients_dbo.Emails_EmailId] FOREIGN KEY([EmailId])
REFERENCES [dbo].[Emails] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EmailRecipients] CHECK CONSTRAINT [FK_dbo.EmailRecipients_dbo.Emails_EmailId]
GO



