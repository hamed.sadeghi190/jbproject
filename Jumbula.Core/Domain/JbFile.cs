﻿using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class JbFile : BaseEntity
    {
        public string Name { get; set; }
        public byte[] Content { get; set; }
    }
}
