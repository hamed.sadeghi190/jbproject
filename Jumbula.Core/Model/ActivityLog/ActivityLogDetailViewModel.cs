﻿namespace Jumbula.Core.Model.ActivityLog
{
    public class ActivityLogDetailViewModel
    {
        public string Activity { get; set; }

        public string Description { get; set; }

        public string DateTime { get; set; }

        public string UserName { get; set; }

        public string IpAdress { get; set; }

        public string Device { get; set; }

        public string Browser { get; set; }

        public string StatusCode { get; set; }

        public string Status { get; set; }
    }
}
