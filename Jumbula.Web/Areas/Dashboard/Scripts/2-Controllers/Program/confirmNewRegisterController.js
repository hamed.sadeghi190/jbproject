﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('confirmNewRegisterController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'seasons-view program';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
   
        $scope.MC.stateParams = $stateParams;


  $scope.actionType = {
      value: "cart"
  };
  $scope.username = '';
  $http.get(window.location.origin + "/Dashboard/Order/GetUserNameForLastOfflineOrder").success(function (data, status, headers, config) {
      $scope.username = data;
  });

      

  $scope.cancelAction = function () {
      $state.go("ProgramsReview", { seasonDomain: $stateParams.seasonDomain });
  }
          
  $scope.offlineRegister = function ()
  {
      if ($scope.actionType.value == "newPlayer")
      {
          window.open(window.location.origin + "/dashboard/program/register/?programId=" + $stateParams.programId);
          $state.go("ProgramsReview", { seasonDomain: $stateParams.seasonDomain });
      }
      else if ($scope.actionType.value == "cart")
      {
          window.open(window.location.origin + "/dashboard/order/cart/");
          $state.go("ProgramsReview", { seasonDomain: $stateParams.seasonDomain });
      }
      else
      {
         
              $state.go("Register", { seasonDomain: $stateParams.seasonDomain, programId: $stateParams.programId,Type:"register" });
      }
  }

    }]);
})();