﻿using iTextSharp.text;
using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Model;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.ModelState;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Jumbula.Web.Mvc.Controllers
{
    public class JbBaseController : Controller
    {
        #region Properties
        protected string DefaultDateFormat => string.Empty;

        protected string ClubSubDomain { get; set; }

        public string ActivityDescription { get; set; }

        public bool Status { get; set; }

        public JbZone? Zone { get; set; }

        public string OwnerKey { get; set; }
        #endregion

        #region Methods
        protected Exception PageNotFound()
        {
            throw new HttpException(404, "HTTP/1.1 404 Not Found");
        }

        public virtual void CheckResourceForAccess(IProtectedResource protectedResource, bool notAccessibleIfNull = false)
        {
            PageNotFound();
        }

        protected ActionResult JsonFormResponse(JsonRequestBehavior jsonRequestBehaviour = JsonRequestBehavior.DenyGet)
        {
            var result = ModelState.GetErrorList();

            return Json(result, jsonRequestBehaviour);
        }

        protected override IAsyncResult BeginExecute(RequestContext requestContext, AsyncCallback callback, object state)
        {
            ClubSubDomain = this.GetCurrentClubDomain();

            return base.BeginExecute(requestContext, callback, state);
        }

        protected void SetActivityDescription(string activityDescription)
        {
            ActivityDescription = activityDescription;
        }

        protected void SetActivityDescription(string format, params object[] args)
        {
            ActivityDescription = string.Format(format, args);
        }

        protected void SetZone(JbZone zone)
        {
            Zone = zone;
        }

        protected void SetOwnerKey(string ownerKey)
        {
            OwnerKey = ownerKey;
        }

        #region GeneratePdf
        protected ActionResult Pdf()
        {
            return Pdf(null, null, null);
        }

        protected ActionResult Pdf(string fileDownloadName)
        {
            return Pdf(fileDownloadName, null, null);
        }

        protected ActionResult Pdf(string fileDownloadName, string viewName)
        {
            return Pdf(fileDownloadName, viewName, null);
        }

        protected ActionResult Pdf(object model)
        {
            return Pdf(null, null, model);
        }

        protected ActionResult Pdf(string fileDownloadName, object model)
        {
            return Pdf(fileDownloadName, null, model);
        }
        protected ActionResult Pdf(string fileDownloadName, string viewName, object model)
        {
            return Pdf(fileDownloadName, viewName, model, string.Empty);
        }

        protected ActionResult Pdf(string fileDownloadName, string viewName, object model, Rectangle pageSize)
        {
            return Pdf(fileDownloadName, viewName, model, string.Empty, pageSize);
        }

        protected ActionResult Pdf(string fileDownloadName, string viewName, object model, string headerTitle, Rectangle pageSize = null)
        {
            // Based on View() code in Controller base class from MVC
            if (model != null)
            {
                ViewData.Model = model;
            }

            var pdf = new PdfResult
            {
                FileDownloadName = fileDownloadName,
                HeaderTitle = headerTitle,
                ViewName = viewName,
                ViewData = ViewData,
                TempData = TempData,
                ViewEngineCollection = ViewEngineCollection,
                PageSize = pageSize
            };

            return pdf;
        }

        #endregion

        protected JsonNetResult JsonNet(bool data)
        {
            object finalData;

            if (data)
            {
                finalData = new OperationStatus { Status = true };
            }
            else
            {
                finalData = ModelState.GetErrorList();
            }

            return JsonNet(finalData);
        }

        protected JsonNetResult JsonNet(object data)
        {
            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.Converters.Add(new StringEnumConverter { CamelCaseText = false });

            return JsonNet(data, serializerSettings, DefaultDateFormat, string.Empty);
        }

        protected JsonNetResult JsonNet(object data, string message)
        {
            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.Converters.Add(new StringEnumConverter { CamelCaseText = false });

            return JsonNet(data, serializerSettings, DefaultDateFormat, message);
        }

        protected JsonNetResult JsonNet(object data, JsonSerializerSettings serializerSettings)
        {
            return JsonNet(data, serializerSettings, DefaultDateFormat, string.Empty);
        }

        protected JsonNetResult JsonNet(object data, JsonSerializerSettings serializerSettings, string dateFormat, string message = null)
        {
            object finalData = null;

            if (data != null)
            {

                if (data.GetType().Name == typeof(PaginatedList<>).Name)
                {
                    if (data is IPaginatedList paginatedResult)
                        finalData = new
                        {
                            DataSource = data,
                            paginatedResult.TotalCount
                        };
                }
                else if (data.GetType() == typeof(OperationStatus))
                {
                    var operationResult = data as OperationStatus;
                    finalData = new JbResult(operationResult);

                    if (operationResult != null) Status = operationResult.Status;
                }
                else
                {
                    finalData = data;
                }
            }

            return new JsonNetResult
            {
                Data = finalData,
                ContentEncoding = Encoding.Unicode,
                DateFormat = dateFormat,
                SerializerSettings = serializerSettings
            };

        }

        #endregion
    }
}