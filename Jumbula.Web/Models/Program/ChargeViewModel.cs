﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models
{
    public class ChargeViewModel
    {
        public int Id { get; set; }

        [MaxLength(256, ErrorMessage = "{0} cannot be longer than 256 characters.")]
        public string Name { get; set; }
        public Charge Charge { get; set; }
        public string Description { get; set; }

        public decimal Amount { get; set; }        

        public string StrAmount { get; set; }
        public int Capacity { get; set; }
        public int NumberOfRegistration { get; set; }
        public int CapacityLeft
        {
            get
            {
                if(Capacity>0)
                {
                    return (Capacity - NumberOfRegistration) > 0 ? (Capacity - NumberOfRegistration) : 0;
                }
                return -1;
            }
        }
        public ChargeDiscountCategory Category { get; set; }
        
        public int ChargeNumber { get; set; }

        public long ChargeId { get; set; }
        public bool HasEarlyBird { get; set; }

        public List<EarlyBird> EarlyBirds { get; set; }

        public string DropInLable { get; set;}

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }

        public bool IsMandatory { get; set; }

        public bool IsProrate { get; set; }

        public decimal ProratePrice { get; set; }

        public int LeftSessions { get; set; }

        public int TotalSessions { get; set; }

        public bool TutionIsFull { get; set; }

        public string DropInName { get; set; }

        public decimal DropInAmount { get; set; }
        public string PunchcardName { get; set; }

        public decimal PunchcardAmount { get; set; }
        public bool HasNoSession
        {
            get
            {
                return IsProrate && LeftSessions < 1;
            }
        }
    }
}