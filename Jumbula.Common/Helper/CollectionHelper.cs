﻿using System;
using Jumbula.Common.Base;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Jumbula.Common.Helper
{
    public  static class CollectionHelper
    {
        public static void AddEntities(this ICollection<IBaseEntity> entities, ICollection<IBaseEntity> newEntities)
        {
            List<IBaseEntity> listEntities = entities.ToList();
            listEntities.AddRange(newEntities.Where(entity => newEntities.All(e => e.Id != entity.Id)));
        }

        public static void AddEntities<T>(this ICollection<T> entities, List<T> newEntities) where T : IBaseEntity
        {
            foreach (var entity in newEntities)
                if (entities.All(e => e.Id != entity.Id))
                    entities.Add(entity);
        }

        public static IEnumerable<TEntity> AddRange<TEntity>(this IDbSet<TEntity> dbset, IEnumerable<TEntity> entitiesToAdd) where TEntity : class
        {
            return ((DbSet<TEntity>)dbset).AddRange(entitiesToAdd);
        }

        public static IEnumerable<TEntity> RemoveRange<TEntity>(this IDbSet<TEntity> dbset, IEnumerable<TEntity> entitiesToDelete) where TEntity : class
        {
            return ((DbSet<TEntity>)dbset).RemoveRange(entitiesToDelete);
        }

        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property) { return items.GroupBy(property).Select(x => x.First()); }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string memberPath, string methodName)
        {
            IOrderedQueryable<T> result;

            if (memberPath.Contains("|"))
            {
                var members = memberPath.Split('|').ToList();

                result = source.OrderByUsing(members.First(), methodName);

                for (var i = 1; i < members.Count; i++)
                {
                    result = result.OrderByUsing(members.ElementAt(i), Constants.ReportConstants.ThenBy);
                }
            }
            else
            {
                result = source.OrderByUsing(memberPath, methodName);
            }
            
            return result;
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string memberPath)
        {
            return source.OrderByUsing(memberPath, "OrderBy");
        }

        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string memberPath)
        {
            return source.OrderByUsing(memberPath, "OrderByDescending");
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string memberPath)
        {
            return source.OrderByUsing(memberPath, "ThenBy");
        }

        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source,
            string memberPath)
        {
            return source.OrderByUsing(memberPath, "ThenByDescending");
        }

        private static IOrderedQueryable<T> OrderByUsing<T>(this IQueryable<T> source, string memberPath, string methodName)
        {
            string[] props = memberPath.Split('.');
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (string prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.Property(expr, pi ?? throw new InvalidOperationException());
                type = pi.PropertyType;
            }
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            var lambda = Expression.Lambda(delegateType, expr, arg);

            object result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                              && method.IsGenericMethodDefinition
                              && method.GetGenericArguments().Length == 2
                              && method.GetParameters().Length == 2)
                .MakeGenericMethod(typeof(T), type)
                .Invoke(null, new object[] { source, lambda });
            return (IOrderedQueryable<T>)result;
        }
    }
}
