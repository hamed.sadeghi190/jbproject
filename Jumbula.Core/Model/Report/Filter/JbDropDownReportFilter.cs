﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Web.Mvc;
using Jumbula.Common.Constants;

namespace Jumbula.Core.Model.Report.Filter
{
    public class JbDropDownReportFilter : IJbReportFilterElement
    {
        public JbDropDownReportFilter()
        {
            Width = ReportConstants.OneFourth;
        }
        public int Id { get; set; }
        public ReportFilterElementName Name { get; set; }
        public string SectionName { get; set; }
        public string Title { get; set; }
        public ReportFilterType Type { get; set; }
        public List<SelectListItem> Data { get; set; }
        public ReportFilterAction? ActionFilter { get; set; }
        public ReportFilterElementName? TargetFilter { get; set; }
        public int ShowByFilterId { get; set; }
        public string ShowByValue { get; set; }
        public bool IsRequired { get; set; }
        public string ParameterName { get; set; }
        public object ParameterValue { get; set; }
        public bool IsBr { get; set; }
        public bool IsHr { get; set; }
        public string Width { get; set; }
        public string FontWeight { get; set; }
    }
}
