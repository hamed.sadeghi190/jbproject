﻿using System.ComponentModel.DataAnnotations;


namespace SportsClub.Areas.Dashboard.Models
{
    public class NewRecipient
    {
        [EmailAddress]
        [Required(ErrorMessage = "{0} is required.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}