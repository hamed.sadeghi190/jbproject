﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('confirmFamilyInvoiceController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'familyOrderItemsService', 'familyOrdersService', 'informationInvoiceService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, familyOrderItemsService, familyOrdersService, informationInvoiceService) {
        window.app.globalObjects.pageClass = 'seasons-view programs';
        window.app.globalObjects.initializeView();



        $scope.MC.stateParams = $stateParams;
        $scope.stateParams = $stateParams;
        $scope.UserName = "";
        $scope.Model = familyOrdersService.get();
        
        $scope.OrderitemModel = familyOrderItemsService.get();

        $scope.cancelAction = function () {

            familyOrdersService.set($scope.Model)
            familyOrderItemsService.set($scope.OrderitemModel);
            $state.back();
            
        }

        var returnedByBack = $state.returnByBack();
        $scope.InfoModel = {};
        if (returnedByBack) {

            $scope.InfoModel = informationInvoiceService.get();

        }
        if (!returnedByBack) {

            $http.get(window.location.origin + '/Dashboard/People/GetInfoForInvoice?userId=' + $stateParams.userId + '&orderItemId=' + $stateParams.orderItemId).success(function (data, status, headers, config) {
                $scope.InfoModel = data;

            });

        }
        $scope.totalAmount = 0;
        for (var i = 0; i < $scope.Model.length; i++) {

            if ($scope.Model[i].Checked) {

                $scope.Model[i].orginalAmount = $scope.Model[i].InvoiceAmount;
                $scope.totalAmount += $scope.Model[i].orginalAmount;
            }

        }
        
       


        $scope.ConfirmInvoice = function () {
            
            $('#ConfirmInvoice').prop("disabled", false);
            $http.post(window.location.origin + '/Dashboard/People/ConfirmInvoice', { InfoModel: $scope.InfoModel })
                
                .success(function (data, status, headers, config) {
                    
                    if (data.Status == true) {
                        
                        familyOrdersService.set($scope.Model);

                        familyOrderItemsService.set($scope.OrderitemModel);
                        informationInvoiceService.set($scope.InfoModel);
                        $state.forward("DisplayInvoiceMessage", { userId: $stateParams.userId, orderItemId: $stateParams.orderItemId }, 'ConfirmFamilyInvoice', { userId: $stateParams.userId, clubId: $stateParams.clubId, orderItemId: $stateParams.orderItemId }, function () { });

                        }
                    else {

                        $scope.MC.handleErrors($scope, data);
                    }

                });
           
        };

        if (returnedByBack) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();