﻿
(function () {
    'use strict';

    angular.module('dashboardApp').controller('addClubDistrictStep2Controller', ['$scope', '$http', '$stateParams', '$state', 'jbToast', 'jbConfirmation', function ($scope, $http, $stateParams, $state, jbToast, jbConfirmation) {
        window.app.globalObjects.pageClass = 'Subsidies_View';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;
        $scope.baseUrl = window.location.origin + '/Dashboard/Club';
        $scope.clubId = $scope.MC.stateParams.clubId;

        $scope.Model = {};

        $http.get($scope.baseUrl + '/CreateDistrictStep2', { params: { clubId: $scope.clubId } }).success(function (data, status, headers, config) {

            $scope.Model = data;

            if ($scope.Model.SelectedSchools.length > 0) {

                $scope.SaveSchoolsToTable();
            }
            if ($scope.Model.SelectedSubsidies.length > 0) {

                $scope.SaveSubsidyTogrid();

                $scope.Model.ListSelectedSubsidiesForSave = $scope.Model.SelectedSubsidies;
            }
        });

        //Add asign school to district
        $scope.ShowSchoolBox = false;
        $scope.RemoveBtn_addSchool = false;

        $scope.ShowSchoolListBox = function (mode) {
            if (mode) {
                $scope.RemoveBtn_addSchool = true;
                $scope.ShowSchoolBox = true;
            } else {
                $scope.ShowSchoolBox = false;
            }

        }
        $scope.ShowSchoolGrid = false;

        $scope.cancelGetSchool = function () {

            $scope.ShowSchoolBox = false;
            $scope.RemoveBtn_addSchool = false;
        }

        $scope.SaveSchoolsToTable = function () {

            $http.get($scope.baseUrl + '/FillSchoolGridForDistrict', { params: { schoolIds: $scope.Model.SelectedSchools } }).success(function (data, status, headers, config) {

                $scope.Model.ListSelectedSchool = data;

                if ($scope.Model.SelectedSchools.length > 0) {

                    $scope.ShowSchoolGrid = true;
                    $scope.ShowBox = false;
                    $scope.ShowSchoolBox = false;
                    $scope.RemoveBtn_addSchool = false;
                } else {

                    $scope.ShowSchoolBox = false;
                    $scope.RemoveBtn_addSchool = false;
                }
            });

        };

        $scope.editSchoolItem = function (school) {
         
            if ($scope.Model.SelectedSubsidies.length == 0) {

                jbToast.error('You should select subsidy.');
            } else {
                school.EditMode = true;

                school._firstProviderId = school.FirstProviderId;
                school._secondProviderId = school.SecondProviderId;
            }
        }

        $scope.saveSchool = function (school) {

            var firstProviderId = school.FirstProviderId;
            var secondProviderId = school.SecondProviderId;
            var ncesId = school.NCESId;
            school.EditMode = false;
            $scope.ShowSchoolBox = false;
            $scope.RemoveBtn_addSchool = false;
        }

        $scope.cancelSchoolItem = function (school) {

            school.EditMode = false;

            school.FirstProviderId = school._firstProviderId;
            school.SecondProviderId = school._secondProviderId;
            school.NCESId = school._ncesId;
        }

        $scope.DeleteSchoolFromDistrict = function (school) {


            for (var i = 0; i < $scope.Model.SelectedSchools.length; i++) {

                if ($scope.Model.SelectedSchools[i] == school.Id) {

                    $scope.Model.SelectedSchools[i] = 0;
                }
            }

            for (var i = 0; i < $scope.Model.ListSelectedSchool.length; i++) {

                if (school.Id == $scope.Model.ListSelectedSchool[i].Id) {

                    $scope.Model.ListSelectedSchool.splice(i, 1);
                }
            }
        }

        //Add asign subsidy to district
        $scope.ShowSubsidyBox = false;
        $scope.RemoveBtn_addSubsidy = false;
        $scope.ShowSubsidyListBox = function (mode) {
            if (mode) {
                $scope.ShowSubsidyBox = true;
                $scope.RemoveBtn_addSubsidy = true;
            } else {
                $scope.ShowSubsidyBox = false;
            }

        }

        $scope.cancelGetSubsidy = function () {
            $scope.ShowSubsidyBox = false;
            $scope.RemoveBtn_addSubsidy = false;

        }

        $scope.ShowSubsidyGrid = false;

        $scope.SaveSubsidyTogrid = function () {

            if ($scope.Model.SelectedSubsidies.length > 0) {

                $scope.Model.ListSelectedSubsidiesForSave = $scope.Model.SelectedSubsidies;
                $scope.SubsidyGridOptions.dataSource.read();
                $scope.ShowSubsidyGrid = true;
                $scope.ShowSubsidyBox = false;
                $scope.RemoveBtn_addSubsidy = false;

            } else {
                $scope.SubsidyGridOptions.dataSource.read();
                $scope.Model.ListSelectedSubsidiesForSave = $scope.Model.SelectedSubsidies;
                $scope.RemoveBtn_addSubsidy = false;
                $scope.ShowSubsidyBox = false;
            }
        }

        $scope.mainGridDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: $scope.baseUrl + '/FillSubsidyGridForDistrict/',
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        data = $.extend(
                           {
                               sort: null,
                               filter: null,
                               districtId: $scope.clubId,
                               subsidyIds: $scope.Model.SelectedSubsidies,
                           }, data);
                        return JSON.stringify(data);
                    }
                },

            },
            pageSize: 5,
            batch: true,
            serverPaging: true,

            schema: {
                data: "DataSource",
                total: "TotalCount"
            },
            sortable: false,
        });
        $scope.SubsidyGridOptions = {
            dataSource: $scope.mainGridDataSource,
            serverPaging: true,
            columns: [
            {
                field: "Name",
                title: "Name",
                template: '<div style="color:black;"> #=Name # </div>',
                labels: {
                    visible: true,
                },
            },
            {
                field: "StateName",
                title: "State",
                labels: {
                    visible: true,
                },
            },
            {
                field: "WebSite",
                title: "Website",
                labels: {
                    visible: true,
                },
            },
            {
                title: "Action",
                width: "70px",
                headerTemplate: "<span class='actions-title'>Actions</span>",
                template: '<ul kendo-menu style="display: inline-block;overflow: visible" k-direction="\'left\'" k-orientation="\'horizontal\'" k-rebind="\'horizontal\'" k-on-select="">\
                             <li > ... \
                                      <ul>\
                                      <li ng-click="DeleteSubsidiesFromDistrict(#:Id#)">\
                                          <span class="k-link">Delete</span>\
                                      </li>\
                                    </ul>\
                                </li>\
                            </ul>'
            }
            ],

        };

        $scope.DeletedSubsidies = [];
        $scope.DeleteSubsidiesFromDistrict = function (id) {

            for (var i = 0; i < $scope.Model.SelectedSubsidies.length; i++) {

                if ($scope.Model.SelectedSubsidies[i] == id) {

                    $scope.DeletedSubsidies.push($scope.Model.SelectedSubsidies[i]);
                    $scope.Model.SelectedSubsidies[i] = 0;
                }
            }
            $scope.Model.ListSelectedSubsidiesForSave = $scope.Model.SelectedSubsidies;
            $scope.Model.DeletedSubsidiesId = $scope.DeletedSubsidies;
            $scope.SubsidyGridOptions.dataSource.read();
        }


        $scope.save = function () {
            $http.post('Dashboard/Club/SaveDistrictStep2', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status == true) {

                     $state.go('SchoolDistrict');
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

        $scope.homePageReplicate = function (memberId) {

            var message = 'Are you sure you want to replicate the home page to all the member schools?';

            if (memberId  != null && memberId != 0) {
                message = 'Are you sure you want to replicate the home page to this member?'
            }

            jbConfirmation
               .yes(function () {

                   $http.post($scope.baseUrl + '/SaveHomePageReplicate', { districtId: $scope.Model.DistrictId, memberId: memberId }).success(function (data, status, headers, config) {

                       if (data.Status) {
                           jbToast.success("Home page replicated successfully.");
                       }
                       else {
                           $scope.MC.handleErrors($scope, data);
                       }
                   });

               })
                .title('Replicate home page')
                .message(message)
                .confirm();
        }

    }])

})();