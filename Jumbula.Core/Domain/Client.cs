﻿using Jb.Framework.Common.Forms;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{
    public class Client : BaseEntity<long>
    {
        public Client()
        {
            EnableDonation = false;
            CanPlayerEditOrder = false;

            ConvenienceFee = 0;

            HasChessTourney = false;

            CreditCards = new HashSet<ClientCreditCard>();
        }
        public int PricePlanId { get; set; }

        public decimal ConvenienceFee { get; set; }
        public ChargeDiscountType ConvenienceFeeType { get; set; }
        public string ConvenienceFeeMessage { get; set; }
        [MaxLength(256)]
        public string ConvenienceFeeLabel { get; set; }
        public string DonationMessage { get; set; }
        [StringLength(2000)]
        public string DonationDescription { get; set; }
        public bool EnableDonation { get; set; }
        public bool EnableCartDonation { get; set; }
        public int? DonationFormId { get; set; }
        [ForeignKey("DonationFormId")]
        public virtual JbForm DonationForm { get; set; }

        public virtual PricePlan PricePlan { get; set; }
        public bool CanPlayerEditOrder { get; set; }
        public bool IsDeleted { get; set; }
        public DiscountMode DiscountMode { get; set; }

        public bool HasChessTourney { get; set; }

        public int BillingPeriodDay { get; set; }
        public int LastMonthBillingPaid { get; set; }

        [StringLength(128)]
        public string GATrackingId { get; set; }

        public virtual ICollection<ClientCreditCard> CreditCards { get; set; }


        public virtual ClientPaymentMethod PaymentMethods { get; set; }
        

        public virtual ICollection<BillingNote> BillingNotes { get; set; }

        public virtual ICollection<ClientBillingHistory> BillingHistories { get; set; }
    }

    public enum DiscountMode : byte
    {
        OrdersInSeason,
        OrdersInCart
    }

   
}
