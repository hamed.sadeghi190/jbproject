﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using HtmlAgilityPack;

namespace Jumbula.Web.WebLogic.Register
{
    public class Ustt
    {

        public List<UsTTMemSearch> UsaTTMemSearch(string term)
        {
            try
            {
                string url;

                WebClient client = new WebClient();

                url = "http://register.usatt.org/tournaments/lookup.php?lname={0}&submit=Submit";

                var webGet = new HtmlWeb(); //use using to dispose?

                var document = webGet.Load(string.Format(url, term), "GET");

                HtmlNodeCollection trs = document.DocumentNode.SelectNodes("//table/tr");

                var records = new List<string>();
                var recs = new List<UsTTMemSearch>();

                // Nothing to do

                if (trs == null || trs.Count == 0)
                {

                }

                // More than one row
                // Always good records

                else
                {
                    for (int i = 1; i < trs.Count; i++)
                    {
                        ProcessUsTTName(recs, trs[i].SelectNodes("td"));
                    }

                }


                return recs;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public UsTTMemSearch UscTTIdSearch(string term)
        {
            try
            {
                string url = "http://register.usatt.org/tournaments/register.php";

                HtmlWeb.PreRequestHandler handler = delegate(HttpWebRequest request)
                {
                    AddPostData(request, term);
                    return true;
                };

                var webGet = new HtmlWeb(); //use using to dispose?

                webGet.PreRequest += handler;

                var document = webGet.Load(url, "POST");

                // HtmlNodeCollection trs = document.DocumentNode.SelectNodes("//table[1]/tr");
                HtmlNodeCollection trs = document.DocumentNode.SelectNodes("/html/body/div[4]/div//table/tr[2]");
                var recs = new UsTTMemSearch();

                // Nothing to do

                if (trs == null || trs.Count == 0)
                {
                    recs = null;
                }

                // More than one row
                // Always good records

                else
                {
                    ProcessUsTTId(ref recs, trs[0].SelectNodes("td"));
                }


                return recs;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        private void ProcessUsTTId(ref UsTTMemSearch record, HtmlNodeCollection tds)
        {
            if (tds.Count == 7) // Id, first name last name, expiration rating
            {
                string id = tds[3].InnerHtml;
                string[] fullName = tds[1].InnerHtml.Split(',');
                string family = fullName[0];
                string name = fullName[1];
                string rating = tds[4].InnerHtml;

                char[] mask = new char[] { '\n', ' ', '\t' };
                id.Trim(mask);
                name.Trim(mask);
                family.Trim(mask);

                record = new UsTTMemSearch { Id = id, FirstName = name, LastName = family, Rating = rating };
            }
        }

        private void ProcessUsTTName(List<UsTTMemSearch> records, HtmlNodeCollection tds)
        {
            if (tds.Count == 6) // Id, first name last name, expiration rating
            {
                string id = tds[1].InnerHtml;
                string family = tds[2].InnerHtml;
                string name = tds[3].InnerHtml;
                string rating = tds[5].InnerHtml;

                char[] mask = new char[] { '\n', ' ', '\t' };
                id.Trim(mask);
                name.Trim(mask);
                family.Trim(mask);

                records.Add(new UsTTMemSearch { Id = id, FirstName = name, LastName = family, Rating = rating });
            }
        }

        private void AddPostData(HttpWebRequest request, string term)
        {
            StringBuilder sb = new StringBuilder(1024); // 1024 good size?
            AppendArg(sb, "usatt", term);
            AppendArg(sb, "remember", "Add this member");

            byte[] buff = Encoding.UTF8.GetBytes(sb.ToString());
            request.ContentLength = buff.Length;
            request.ContentType = "application/x-www-form-urlencoded";

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(buff, 0, buff.Length);
            }
        }

        private void AppendArg(StringBuilder sb, string argLabel, string arg)
        {
            const char AmperSign = '&';  // move and merge with other files
            const char EqualSign = '=';


            if (sb.Length != 0)
            {
                sb.Append(AmperSign);
            }

            sb.Append(argLabel).Append(EqualSign).Append(HttpUtility.UrlEncode(arg));
        }
    }

    public class UsTTIdSearch
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }

    public class UsTTMemSearch
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Rating { get; set; }
    }
}