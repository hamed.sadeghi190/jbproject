﻿
using System.ComponentModel;


namespace Jumbula.Common.Enums
{
    public enum RestrictionType : byte
    {
        [Description("No restrictions")]
        None,
        [Description("Age")]
        Age,
        [Description("Grade")]
        Grade
    }

}
