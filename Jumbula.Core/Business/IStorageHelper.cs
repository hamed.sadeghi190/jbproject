﻿using Microsoft.WindowsAzure.Storage;
using System;
using System.IO;

namespace Jumbula.Core.Business
{
    public interface IStorageBusiness
    {
        bool IsExist(string path);
        CloudStorageAccount GetAccountInstance();
        void CreateContainer(string connectionString, string containerName, bool publicAccess);
        bool DoesContainerExist(string connectionString, string containerName);
        Uri GetUri(string containerName, string directoryName, string blobName);
        Uri GetUri(string containerName, string parentDirName, string childDirName, string blobName);
        string PathCombine(string directoryName, string blobName);
        string PathCombine(string parentDirName, string childDirName, string blobName);
        void UploadBlob(string blobName, Stream stream, string contentType, string containerName);
        void UploadBlob(string directoryName, string blobName, Stream stream, string contentType, string containerName);
        void UploadBlob(string parentDirName, string childDirName, string blobName, Stream stream, string contentType, string containerName);
        void DeleteBlob(string parentDirName, string childDirName, string blobName, string containerName);
        void DeleteBlob(string parentDirName, string blobName, string containerName);
        void DeleteBlob(string blobPath, string containerName);
        void UploadDirectory(string sourceDirectory, string folerName, string containerName);
        Uri[] ListDirectoryBlobFiles(string parentDirName, string childDirName, string containerName);
        Uri[] ListDirectoryBlobFiles(string parentDirName, string childDirName, string grandChildDirName, string containerName);
        Uri[] ListDirectoryBlobFiles(string path, string containerName);
        void ReadFile(string sourcePath, string targetPath, string containerName);
        Stream DownloadBlobAsStream(string blobUri, string containerName);
        void UploadBlob(string directoryName, string blobName, Stream stream, string contentType);
    }
}
