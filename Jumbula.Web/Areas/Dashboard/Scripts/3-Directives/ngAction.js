﻿(function () {
	'use strict';

	angular.module("dashboardApp").directive('ngaction', function ($compile) {
		return {
			restrict: 'A',
			replace: false,
			terminal: true,
			link: function (scope, element, attrs) {

				var isActionExist = false;
				var currentAction = attrs.ngaction
				var allowedActions = null;

					if (scope.$parent.MC) {
						allowedActions = scope.$parent.MC.allowedActions;
					}
					else if (scope.$parent.$parent.MC) {
						allowedActions = scope.$parent.$parent.MC.allowedActions;
					}
					else if (scope.$parent.$parent.$parent.MC) {
						allowedActions = scope.$parent.$parent.$parent.MC.allowedActions;
					}

				if (currentAction == 'true' || currentAction == 'false') {

					isActionExist = (currentAction === 'true');
				}
				else {

					if (allowedActions) {
						for (var i = 0; i < allowedActions.length; i++) {

							if (allowedActions[i] == currentAction) {
								isActionExist = true;
								break;
							}
						}
					}
				}

				if (isActionExist) {
					element.removeClass('no-access');
				}
				else {

				    if (element.hasClass('display-link')) {
				        element.removeAttr('href');
				    }
				    else {
				        element.addClass('no-access');
				    }

					angular.element(element).off("click.disabled").on("click.disabled", function (e) {
						e.preventDefault();
						e.stopImmediatePropagation();
					})
				}

				$compile(element.contents())(scope);
			}
		};
	});
})();
