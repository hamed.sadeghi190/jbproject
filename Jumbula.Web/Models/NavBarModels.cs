﻿using Jumbula.Common.Enums;
using Jumbula.Core.Model.Toolbox;
using System.Collections.Generic;

namespace SportsClub.Models
{

    public class NavBarViewModel
    {
        public NavBarViewModel()
        {
            GoogleAnalytics = new ToolboxGoogleAnalyticsViewModel();
        }

        public string Name { get; set; }
        public bool IsClassPage { get; set; }
        public int CountOrder { get; set; }
        public string ImageUrl { get; set; }
        public string LogoRedirecturl { get; set; }

        public string ClubWebsiteURL { get; set; }

        public string ClubDomain { get; set; }

        public bool ShowLogo { get; set; }

        public string LogoUrl { get; set; }

        public string HeaderTitle { get; set; }

        public bool IsSchool { get; set; }

        public bool ShowParentLogo
        {
            get
            {
                return HasPartner && IsSchool;
            }
        }

        public bool HasPartner { get; set; }

        public string PartnerLogoUrl { get; set; }

        public string PartnerLogoRedirectUrl { get; set; }

        public bool HasPreregisteredOrder { get; set; }

        public bool HasAddedPreregisteredOrder { get; set; }

        public bool HasLotteryOrder { get; set; }
        public bool HasWonLotteryOrder{ get; set; }

        public string CartToolTip
        {
            get
            {
                var result = "Cart";

                if (this.HasPreregisteredOrder)
                {
                    result = "You have pre-registration items.";
                }

                if (this.HasAddedPreregisteredOrder)
                {
                    result = "You can pay for your pre-registration items now.";
                }

                if (this.HasLotteryOrder)
                {
                    result = "You have lottery items.";
                }

                if (this.HasWonLotteryOrder)
                {
                    result = "You can pay for your won lottery items now.";
                }

                return result;
            }
        }

        public List<MemberClubsModel> Clubs { get; set; }

        public bool IsAdminSwitchedAsParent { get; set; }

        public string SwitchedParentUserName { get; set; }

        public ToolboxGoogleAnalyticsViewModel GoogleAnalytics { get; set; }
    }

    public class MemberClubsModel
    {
        string _role;
        public string Name { get; set; }

        public string Domain { get; set; }

        public string Role
        {
            get
            {
                return !_role.Equals(RoleCategory.Parent.ToString(), System.StringComparison.OrdinalIgnoreCase) ? _role : "Family";
            }
            set
            {
                _role = value;
            }
        }
    }
}