﻿using Jumbula.Common.Enums;
using Jumbula.Common.Helper;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeEmailBodyItemModel
    {
        public AutoChargeEmailBodyItemModel()
        {

        }
        public AutoChargeEmailBodyItemModel(CurrencyCodes currency)
        {
            Currency = currency;
        }
        public string ConfirmationId { get; set; }
        public string PlayerFullName { get; set; }
        public CurrencyCodes Currency { get; set; }
        public decimal Price { get; set; }
        public string Amount => CurrencyHelper.FormatCurrencyWithPenny(Price, Currency);
        public string EventName { get; set; }
        
    }
}
