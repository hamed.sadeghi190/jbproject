

ALTER table clubs Add [DistrictId] [int] NULL
Go

ALTER TABLE [dbo].[clubs]  WITH CHECK ADD  CONSTRAINT [FK_dbo.clubs_dbo.Clubs_DistrictId] FOREIGN KEY([DistrictId])
REFERENCES [dbo].[Clubs] ([Id])
