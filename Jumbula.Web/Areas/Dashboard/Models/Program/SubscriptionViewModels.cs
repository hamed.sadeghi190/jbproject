﻿using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Newtonsoft.Json;
using SportsClub.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SubscriptionCreateEditModelStep2 : BaseViewModel<Program, long>, IProgramWizardModel
    {
        public SubscriptionCreateEditModelStep2()
        {
            RegistrationPeriod = new RegistrationPeriodViewModel();
            Subscriptions = new List<SubscriptionViewModel>();
        }

        public string ClubDomain { get; set; }

        public string Domain { get; set; }

        public ProgramPageStep LastCreatedPage { get; set; }

        public string Name { get; set; }

        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }

        public ProgramPageStep PageStep { get; set; }

        public ProgramRegStatus RegStatus { get; set; }

        public ProgramRunningStatus RunningStatus { get; set; }

        public SaveType SaveType { get; set; }

        public string SeasonDomain { get; set; }

        public ProgramStatus Status { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        [Display(Name = "Start date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? EndDate { get; set; }

        public List<SubscriptionViewModel> Subscriptions { get; set; }
        public ProgramTypeCategory TypeCategory { get; set; }
        public List<SelectKeyValue<string>> ContinueTypes { get; set; }
        public ContinueType SelectedContinueType { get; set; }
        public List<SelectKeyValue<string>> GenderItems { get; set; }
        public Genders SelectedGender { get; set; }
        public List<SelectKeyValue<string>> RestrictionTypes { get; set; }
        public RestrictionType SelectedRestrictionType { get; set; }
        public List<SelectKeyValue<string>> Ages { get; set; }
        public List<SelectKeyValue<string>> Grades { get; set; }

        public List<SelectKeyValue<string>> DaysBeforeBillingItems { get; set; }
        public string SelectedMinAge { get; set; }
        public string SelectedMaxAge { get; set; }
        public SchoolGradeType? SelectedMinGrade { get; set; }
        public SchoolGradeType? SelectedMaxGrade { get; set; }
        public List<SelectKeyValue<string>> Sessions { get; set; }
        public string SelectedSession { get; set; }

        public bool IsProrate { get; set; }

        [Display(Name = "Prorate time")]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime ProrateTime { get; set; }

        [Display(Name = "Registration fee")]
        [Required(ErrorMessage = "{0} is required")]
        [Range(minimum: 0.01, maximum: double.MaxValue, ErrorMessage = "{0} is incorrect.")]
        public decimal? RegistrationFee { get; set; }

        public long ProgramScheduleId { get; set; }

        [Display(Name = "Due date")]
        [Required(ErrorMessage = "{0} is required.")]
        public int? DueDate { get; set; }

        public List<EarlyBird> EarlyBirds { get; set; }

        public bool HasEarlyBird { get; set; }

        public bool HasPayInFullOption { get; set; }

        public decimal FullPayDiscount { get; set; }

        public bool EnableDropIn { get; set; }

        [Display(Name = "Drop-in session label")]
        [Required(ErrorMessage = "{0} is required.")]
        public string DropInSessionLabel { get; set; }

        [Display(Name = "Drop-in session price")]
        [Required(ErrorMessage = "{0} is required.")]
        public decimal DropInSessionPrice { get; set; } 


        [Display(Name = "Message to users")]
        [MaxLength(50, ErrorMessage = "Maximum of {0} id {1} maximum.")]
        public string FullPayDiscountMessage { get; set; }

        public List<ProgramScheduleDayViewModel> Days { get; set; }

        public List<SelectKeyValue<string>> Capacities { get; set; }

        public string Capacity { get; set; }

        public bool AppliesRestrictionAtTheProgramStart { get; set; }
    }

    public class SubscriptionViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Price for schedule")]
        [Required(ErrorMessage = "{0} is required.")]
        [Range(minimum: 0.01, maximum: double.MaxValue, ErrorMessage = "{0} is incorrect.")]
        public decimal Amount { get; set; }
        public RepeatType RepeatType { get; set; }
        public string Occurances { get; set; }
        public string DaysBeforeBilling { get; set; }

        [Display(Name = "Tuition label")]
        [Required(ErrorMessage = "{0} is required.")]
        public string PriceLabel { get; set; }

        [Display(Name = "Week days")]
        [Required(ErrorMessage = "{0} is required.")]
        public int? WeekDays { get; set; }
    }

    public class SubscriptionCreateEditModelStep2Point5
    {
        public List<SuscriptionItemViewModel> SubscriptionItems { get; set; }

    }

    public class SuscriptionItemViewModel
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public decimal Amount { get; set; }

        public DateTime PaymentDueDate { get; set; }
    }

    public class SubscriptionScheduleViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Start date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End date")]
        [DataType(DataType.DateTime)]
        [Required(ErrorMessage = "{0} is required.")]
        [JsonConverter(typeof(CustomDateConverter))]
        public DateTime? EndDate { get; set; }

        public RegistrationPeriodViewModel RegistrationPeriod { get; set; }

        public bool IsProrate { get; set; }

        public List<SelectKeyValue<string>> GenderItems { get; set; }

        public List<SuscriptionItemViewModel> SubscriptionItems { get; set; }

        public Genders SelectedGender { get; set; }

        public List<SelectKeyValue<string>> Capacities { get; set; }

        public List<SelectKeyValue<int>> MinimumEnrollments { get; set; }

        [Display(Name = "Minimum enrollment")]
        public int MinimumEnrollment { get; set; }

        public string Capacity { get; set; }

        public int SelectedCapacity { get; set; }
        [Display(Name = "show capacity left number")]
        public bool ShowCapacityLeft { get; set; }
        [Display(Name = "capacity left number")]
        public int ShowCapacityLeftNumber { get; set; }
        public List<SelectKeyValue<int>> CapacityLeftNumbers { get; set; }

        public List<SelectKeyValue<string>> ContinueTypes { get; set; }

        public ContinueType SelectedContinueType { get; set; }

        public List<SelectKeyValue<string>> RestrictionTypes { get; set; }

        public RestrictionType SelectedRestrictionType { get; set; }

        public List<SelectKeyValue<string>> Ages { get; set; }

        public List<SelectKeyValue<string>> Grades { get; set; }

        public string SelectedMinAge { get; set; }

        public string SelectedMaxAge { get; set; }

        public SchoolGradeType? SelectedMinGrade { get; set; }

        public SchoolGradeType? SelectedMaxGrade { get; set; }

        public bool IsScheduleChanged { get; set; }
    }
}