﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SchoolMemberListController', [
        '$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
            window.app.globalObjects.pageClass = 'school-member-view';
            window.app.globalObjects.initializeView();

            $scope.stateParams = $stateParams;
            $scope.seasonName = $scope.stateParams.seasonDomain;

            $scope.MC.stateParams = $stateParams;

            angular.forEach($scope.MC.seasons, function (value, key) {
                if (value.Domain == $scope.stateParams.seasonDomain)
                    $scope.MC.currentSeason = value;
            });

            var baseUrl = window.location.origin + '/Dashboard/PortalReport';

            $scope.Model = {};
            $scope.Model.checkedIds = {};

            $scope.SchoolMemberListReportDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport:
                {
                    read: {
                        url: baseUrl + '/GetAllSchoolMemberList',
                        type: "POST",
                    },

                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                batch: true,
                resizable: true,
            });

            $scope.SchoolMemberListReportOption = {
                dataSource: $scope.SchoolMemberListReportDataSource,
                excel: {
                    allPages: true,
                    fileName: "School Member List.xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();
                },
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    var view = this.dataSource.view();
                    for (var i = 0; i < view.length; i++) {
                        if ($scope.Model.checkedIds[view[i].ClubId]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                        }
                    }
                },
                sortable: { mode: 'single' },
                columns: [
                    {
                        template: "<input type='checkbox' class='checkbox' />",
                        width: "30px",
                        headerTemplate: '<input type="checkbox" class="checkAll" />'
                    },
                    {
                        field: "SchoolName",
                        title: "School name",
                        width: "140px",
                    },
                    {
                        field: "ContactPerson",
                        title: "Contact name",
                        width: "140px",
                    },
                    {
                        field: "ContactEmail",
                        title: "Contact email",
                        width: "200px",
                    }
                ],
            }

            $(document.body).on('click', '#SchoolMemberListReportGrid table .checkbox', selectRow);
            $(document.body).on('click', '#SchoolMemberListReportGrid table .checkAll', checkAll);

            //on click of the checkbox:
            function selectRow() {
                var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#SchoolMemberListReportGrid").data("kendoGrid"),
                dataItem = grid.dataItem(row);
                $scope.Model.checkedIds[dataItem.ClubId] = checked;
                if (checked) {
                    //-select the row
                    row.addClass("k-state-selected");
                } else {
                    //-remove selection
                    row.removeClass("k-state-selected");
                }
            }

            $scope.MakeCampaign = function () {
                var checked = [];
                for (var i in $scope.Model.checkedIds) {
                    if ($scope.Model.checkedIds[i]) {
                        checked.push(i);
                    }
                }
                // Redirect to the special action
                if (checked.length > 0) {
                    $http.post(baseUrl + "/SendEmailToMembers", { membersIds: checked, reportName: "SchoolMemberList" })
                        .success(function (data, status, headers, config) {
                            if (data.Status == true) {
                                $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'UserList' });
                            } else {
                                jbToast.error(data.Message);
                            }
                        }).error(function (data, status, headers, config) {
                        });
                } else {
                    jbToast.error("Please select at lest a member to send mail.");
                }
            };

            function checkAll() {
                var grid = $("#SchoolMemberListReportGrid").data("kendoGrid");
                var state = $("#SchoolMemberListReportGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
                var ckbox;
                grid.tbody.find("tr").each(function () {
                    var $this = $(this);
                    ckbox = $this.find("td .checkbox");
                    ckbox.prop("checked", state);
                    var dataItem = grid.dataItem($this);
                    $scope.Model.checkedIds[dataItem.ClubId] = state;
                    if (state) {
                        //-select the row
                        $this.addClass("k-state-selected");
                    } else {
                        //-remove selection
                        $this.removeClass("k-state-selected");
                    }
                });
            }

            $scope.saveToExcel = function () {
                cfpLoadingBar.start();
                $("#SchoolMemberListReportGrid").getKendoGrid().saveAsExcel();
            }
        }
    ]);

})();