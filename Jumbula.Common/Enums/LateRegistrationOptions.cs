﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum LateRegistrationOptions
    {
        [Description("No late registration")]
        NoLateRegistration,
        [Description("EM facilitated")]
        EMFacilitated,
        [Description("Coordinator facilitated")]
        CoordinatorFacilitated,
        [Description("Provider facilitated")]
        ProviderFacilitated
    }

}
