﻿using Jumbula.Common.Enums;
using Jumbula.Common.Utilities;
using Jumbula.Core.Model.Catalog;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Core.Model.Generic;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.UIHelpers;

namespace SportsClub.Areas.Dashboard.Models
{


    public class SchoolSettingInfoViewModel
    {
        public SchoolSettingInfoViewModel()
        {
            Grades = DropdownHelpers.ToOrderedSelectList<SchoolGradeType>("Select grade", null);
            SchoolSubTypes = Ioc.ClubBusiness.GetClubTypes(ClubTypesEnum.School.ToString());
            SchoolSubTypes.Add(new SelectKeyValue<int>() { Text = "Select school type", Value = 0 });
        }
        public string Domain { get; set; }
        public string SchoolName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string County { get; set; }
        public string WebSite { get; set; }
        public int SelectedSubType { get; set; }
        public List<SelectKeyValue<int>> SchoolSubTypes { get; set; }
        public List<SelectKeyValue<string>> Grades { get; set; }
        public string SelectedMinGrade { get; set; }

        public string SelectedMaxGrade { get; set; }
        [Url]
        [DataType(DataType.Url)]
        [Display(Name = "PTA site")]
        public string PTASite { get; set; }
        public string Note { get; set; }

    }

    public class SchoolSettingProgramInfoViewModel
    {

        public SchoolSettingProgramInfoViewModel()
        {
            FolderDaysOfWeek = DropdownHelpers.ToSelectList<DayOfWeek>("Select folder day");
            DaysOfWeek = DropdownHelpers.ToSelectList<DayOfWeek>();
            ListOfRegistrationSizes = DropdownHelpers.ToSelectList<RegistrationSize>("Select number of registration per season");
            ListOfSchoolSizes= DropdownHelpers.ToSelectList<SchoolSize>("Select number of students");
        }
        public string ContactEmail { get; set; }
        [DisplayName("number of seasons")]
        [Range(1, 3, ErrorMessage = "Number of seasons must be between 1 and 3")]
        public Int16? NumberOfSeasons { get; set; }
        [DisplayName("number of classes per season")]
        [Range(1, 50, ErrorMessage = "Number of classes per season must be between 1 and 50")]
        public Int16? NumberOfClassesPerSeason { get; set; }
        [DisplayName("max classes per day")]
        [Range(1, 10,ErrorMessage = "Max classes per day must be between 1 and 10")]
        public Int16? MaxClassesPerDay { get; set; }
        public bool BeforeSchool { get; set; }
        public bool OpenToNonStudents { get; set; }
        public string TranslationNeeded { get; set; }

        public List<string> DaysClassesMeet { get; set; }

        public int SelectedCoordinator { get; set; }
        public List<SelectKeyValue<int>> ListOfCoordinator { get; set; }

        public string SelectedSchoolSize { get; set; }
        public List<SelectKeyValue<string>> ListOfSchoolSizes { get; set; }

        public string SelectedRegistrationSize { get; set; }
        public List<SelectKeyValue<string>> ListOfRegistrationSizes { get; set; }

        public List<SelectKeyValue<string>> DaysOfWeek { get; set; }
        public List<SelectKeyValue<string>> FolderDaysOfWeek { get; set; }
        public string FolderDay { get; set; }
        public string Note { get; set; }


    }

    public class SchoolSettingOnSiteInfoViewModel
    {

        public SchoolSettingOnSiteInfoViewModel()
        {
            DaysOfWeek = DropdownHelpers.ToSelectList<DayOfWeek>("Select early release day");
            ListOfDismissalOption = DropdownHelpers.ToSelectList<DismissalOptions>();
        }
        public string OnSiteCancellationContact { get; set; }
    
        public List<SelectKeyValue<string>> DaysOfWeek { get; set; }

        public string SelectedEarlyReleaseDay { get; set; }

        [Display(Name = "school start time")]
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? SchoolStartTime { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? SchoolDismissalTime { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? EnrichmentStartTimeAM { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? EnrichmentStartTimePM { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? InstructorArrivalTimeAM { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? InstructorArrivalTimePM { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? EarlyReleaseTime { get; set; }

        public List<SpaceRequirementsViewModel> SpaceRequirements { get; set; }

        public string ParkingForProviders { get; set; }
        public string TransitionInstructions { get; set; }
        public string AttendanceProcedure { get; set; }
        
        public string CheckInProceduresForInstructors { get; set; }
        public string EnrichmentDismissalInstructions { get; set; }
        public string LatePickUpPolicy { get; set; }
        public string PermitResponsibility { get; set; }

        public List<string> SelectedDismissalOption { get; set; }
        public List<SelectKeyValue<string>> ListOfDismissalOption { get; set; }

        public List<string> SelectedEnrichmentDismissalOption { get; set; }
        public string Note { get; set; }

        public string Title { get; set; }
    }

    public class SchoolSettingRegistrationInfoViewModel
    {
        public SchoolSettingRegistrationInfoViewModel()
        {
            ListOfLateRegistrationOption = DropdownHelpers.ToSelectList<LateRegistrationOptions>();
            ListOfReconciliationDetail = DropdownHelpers.ToSelectList<ReconciliationDetails>();
        }
        public List<SelectKeyValue<string>> ListOfLateRegistrationOption { get; set; }
        public List<SelectKeyValue<string>> ListOfReconciliationDetail { get; set; }
        public string Colors { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? RegistrationStartTime { get; set; }
        [JsonConverter(typeof(CustomTimeConverter))]
        public DateTime? RegistrationEndTime { get; set; }

        public List<string> SelectedLateRegistrationOption { get; set; }
        public decimal PTARegFee { get; set; }

        public string ScholarshipRequirement { get; set; }
        public string ScholarshipCodes { get; set; }
        public string SpecialRosterRequirements { get; set; }
        public List<string> SelectedReconciliationDetail { get; set; }
        public string Note { get; set; }

    }


}