﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('MemberListReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};
        var isBegin = false;
            
        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.activeStatus = [{ Value: -1, Title: "All" }, { Value: 1, Title: "Active" }, { Value: 2, Title: "Inactive" }];
        $scope.filter.activeStatus = 1;

        $scope.filter.printMode = false;

        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.ProviderNames = data.ProviderNames;
            dataInfo = data;
            //manipulateMemberListGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $http.get('/Dashboard/Club/ClubPage').success(function (data, status, headers, config) {
            $scope.Model.ClubType = data;
        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });
        $scope.filter.typeId = -1;

        var model = $scope.Model;        

        var manipulateMemberListGrid = function () {

            isBegin = true;

            $scope.MemberListDataSource = new kendo.data.DataSource({
                
                transport: {
                    read: {
                        url: baseUrl + "/GetMemberListReportInfo",
                        dataType: "json",
                        //type: "POST",
                        data: function () {
                            return {
                                startDate: $scope.filter.startDate,
                                endDate: $scope.filter.endDate,
                                typeId: $scope.filter.typeId,
                                activeStatus: $scope.filter.activeStatus,
                                printMode: $scope.filter.printMode
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.MemberListGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Member list).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Member list - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.MemberListDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#MemberListGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "Name",
                        title: "Name",
                        width: "190px"
                    }, {
                        field: "ContactName",
                        title: "Contact name",
                        width: "120px"
                    }, {
                        field: "EmailAddress",
                        title: "Email",
                        width: "120px"
                    }, {
                        field: "PhoneNumber",
                        title: "Phone",
                        width: "120px"
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $scope.filter.printMode = false;
            $("#MemberListGrid").getKendoGrid().saveAsExcel();
            
        }
        $scope.saveToPDF = function () {
            $scope.filter.printMode = true;
            $("#MemberListGrid").data("kendoGrid").dataSource.read();

        }
        $scope.print = function () {
            $scope.filter.printMode = true;
            $("#MemberListGrid").data("kendoGrid").dataSource.read();
        }

        $scope.search = function () {
            if (!isBegin) {
                manipulateMemberListGrid();
            } else {
                $scope.filter.printMode = false;
                $("#MemberListGrid").data("kendoGrid").dataSource.read();
            }
            
        };

        $scope.clearFilters = function () {
            $scope.filter.endDate = "";
            $scope.filter.startDate = "";
            $scope.filter.typeId = -1;
            $scope.filter.activeStatus = -1;
            $scope.filter.printMode = false;
            $("#MemberListGrid").data("kendoGrid").dataSource.read();
        }
    }
    ]);

})();