﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('messagingController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService) {
        window.app.globalObjects.pageClass = 'catalog-view';//seasons-view programs
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Messaging", order: 0, menu: "MESSAGING" });
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        $scope.chatId = $stateParams.chatId;

        $scope.MC.getChat($scope.chatId);

        $scope.Send = function () {

            if ($scope.SendText == null || $scope.SendText == '' || $scope.SendText == 'undefined') {
                return;
            }

            $http.post('/Message/Send', { model: { Text: $scope.SendText, ChatId: $scope.chatId } })
             .success(function (data, status, headers, config) {

                 var dateTimeTicks = $scope.getDateTimeAsTick();
                 $scope.MC.CurrentChat.Messages.push({ Text: $scope.SendText, IsFromMe: true, DateTimeTicks: dateTimeTicks });


                 for (var m = 0; m < $scope.MC.Chats.length; m++) {
                     if ($scope.MC.CurrentChat.ChatId == $scope.MC.Chats[m].ChatId) {
                         $scope.MC.Chats[m].IsUnread = true;
                         $scope.MC.Chats[m].DateTimeTicks = dateTimeTicks;
                         $scope.MC.Chats[m].DateTime = new Date().format('mmm d, h:MM TT', false);
                         $scope.MC.Chats[m].Unread = 0;
                     }
                 }

                 $scope.SendText = '';

                 setTimeout(function () {
                     var message_box = angular.element(document.querySelector('#messageBox'));
                     message_box[0].scrollTop = message_box[0].scrollHeight;
                 }, 100);
             });

        }

        $scope.closeDropdown = function () {

            $scope.HideMenu = true;
        }

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.init = function () {
            $('[data-role=dropdown]').dropdown();
        }

        $scope.accept = function () {

            $scope.showConfirmation('Accept');
        }

        $scope.decline = function () {
            $scope.showConfirmation('Decline');
        }

        $scope.modify = function () {

            if ($scope.MC.CurrentChat.ClubType == 'Partner') {
                $scope.showConfirmation('Modify');
            }
            else {
                $scope.showConfirmation('ModifyRequest');
            }
        }

        $scope.cancelConfirmation = function () {

            $scope.ShowActionConfirmation = false;
        }

        $scope.showConfirmation = function (actionMode) {
            $http.get('/Message/GetMessageAction?programId=' + $scope.MC.CurrentChat.ProgramId).success(function (data, status, headers, config) {

                $scope.Model = {};

                $scope.Model.ActionModel = data;
                $scope.Model.ActionModel.RespondType = actionMode;
                $scope.Model.ActionModel.ProgramId = $scope.MC.CurrentChat.ProgramId;
                $scope.ConfirmationMode = actionMode;
                $scope.ShowActionConfirmation = true;

            }).error(function () {

            });
        }

        $scope.save = function () {

            $http.post('/Message/SaveMessageAction', { model: $scope.Model.ActionModel, chatId: $stateParams.chatId })
               .success(function (data, status, headers, config) {
                   if (data.Status) {
                       jbToast.success(data.Message, "");
                       $scope.ShowActionConfirmation = false;

                       $scope.MC.getChat($scope.chatId);

                       var dateTimeTicks = $scope.getDateTimeAsTick();

                       for (var m = 0; m < $scope.MC.Chats.length; m++) {
                           if ($scope.MC.CurrentChat.ChatId == $scope.MC.Chats[m].ChatId) {
                               $scope.MC.Chats[m].IsUnread = true;
                               $scope.MC.Chats[m].DateTimeTicks = dateTimeTicks;
                               $scope.MC.Chats[m].DateTime = new Date().format('mmm d, h:MM TT', false);
                               $scope.MC.Chats[m].Unread = 0;
                           }
                       }
                   }
                   else {
                       $scope.MC.handleErrors($scope, data);
                   }
               });
        };


        $scope.formatTime = function (ticks) {

            //ticks are in nanotime; convert to microtime
            var ticksToMicrotime = ticks / 10000;

            //ticks are recorded from 1/1/1; get microtime difference from 1/1/1/ to 1/1/1970
            var epochMicrotimeDiff = 2208988800000;

            //new date is ticks, converted to microtime, minus difference from epoch microtime
            var dateTime = new Date(ticksToMicrotime - epochMicrotimeDiff);

            var result = dateTime.format('mmm d, h:MM TT', false);

            return result;
        }

        $scope.getDateTimeAsTick = function () {
            var dateTicks = ((new Date().getTime() * 10000) + 621355968000000000);

            return dateTicks;
        }

    }]);
})();

