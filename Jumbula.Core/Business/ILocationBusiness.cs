﻿using Jb.Framework.Common;
using Jumbula.Core.Domain;
using System.Linq;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ILocationBusiness
    {
        ClubLocation GetClubLocation(int id);

        bool IsLocationAssignToAnotherProgram(int clubId, long? programId, long locationId);

        OperationStatus DeleteLocation(int id);

        OperationStatus CreateLocation(ClubLocation clubLocation);

        IQueryable<ClubLocation> GetClubLocationsByClubId(int clubId);

        OperationStatus UpdateLocation(ClubLocation clubLocation);
    }
}
