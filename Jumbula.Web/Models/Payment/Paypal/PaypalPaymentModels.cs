﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Policy;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Web.Mvc.WebConfig;

namespace SportsClub.Models
{
    [Serializable]
    public class PaypalAPIProfile
    {

        public string APIUsername { get; private set; }

        public string APIPassword { get; private set; }

        public string APISignature { get; private set; }
        public string DeviceIPAddress { get; set; }

        public string RequestDataformat { get; set; }

        public string ResponseDataformat { get; set; }

        public string ServiceVersion { get; private set; }

        public string ApplicationID { get; private set; }

        public string APIEndPoint { get; private set; }

        public PaypalAPIOperations ApiOperation { get; set; }

        public string APIEndPointUrl
        {
            get
            {
                return APIEndPoint + ApiOperation.ToString();
            }
        }
       
        public PaypalAPIProfile(bool isSandBox)
        {
            if (isSandBox)
            {
                APIUsername = WebConfigHelper.Paypal_ApiUsername_Test;
                APIPassword = WebConfigHelper.Paypal_ApiPassword_Test;
                APISignature = WebConfigHelper.Paypal_ApiSignature_Test;
                ApplicationID = WebConfigHelper.Paypal_AppId_Test;
                ServiceVersion = WebConfigHelper.Paypal_ApiVersion;
                APIEndPoint = WebConfigHelper.Paypal_ApiEndPoint_Sig_Test;
            }
            else
            {
                APIUsername = WebConfigHelper.Paypal_ApiUsername_Live;
                APIPassword = WebConfigHelper.Paypal_ApiPassword_Live;
                APISignature = WebConfigHelper.Paypal_ApiSignature_Live;
                ApplicationID = WebConfigHelper.Paypal_AppId_Live;
                ServiceVersion = WebConfigHelper.Paypal_ApiVersion;
                APIEndPoint = WebConfigHelper.Paypal_ApiEndPoint_Sig_Live;
            }


        }
    }

    public class PayRequest
    {
        private string _currencyCode;

        public RequestEnvelope requestEnvelope { get; set; }

        public string actionType { get; set; }

        public string cancelUrl { get; set; }

        public string currencyCode 
        { 
            get
            {
                return _currencyCode;
            }

            set
            { 
                if(value == CurrencyCodes.CNY.ToString())
                {
                    _currencyCode =CurrencyCodes.USD.ToString();
                }
                else
                {
                    _currencyCode = value;
                }
            }

        }
    
        public string feesPayer { get; set; }

        public string memo { get; set; }

        public ReceiverList receiverList { get; set; }

        public string ipnNotificationUrl { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string preapprovalKey { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string trackingId { get; set; }
        public string returnUrl { get; set; }



        public PayRequest()
        {
        }



    }
    public class PayResponse
    {
        public ResponseEnvelope responseEnvelope { get; set; }
        public string payKey { get; set; }

        public string paymentExecStatus { get; set; }


        //public PayErrorList payErrorList {get;set;}

        //public PaymentInfoList paymentInfoList {get;set;}

        //public WarningDataList warningDataList {get;set;}


        public List<ErrorData> error { get; set; }

        public PayResponse()
        {
        }


    }

    /// <summary>
    /// The request to set the options of a payment request. 
    /// </summary>

    public class ExecutePaymentRequest
    {
        public ExecutePaymentRequest() {}
        public string actionType { get; set; }
        public string payKey { get; set; }
        public RequestEnvelope requestEnvelope { get; set; }

    }
    public class GetPaymentOptionsRequest
    {
        public GetPaymentOptionsRequest() { }      
        public string payKey { get; set; }
        public RequestEnvelope requestEnvelope { get; set; }

    }
    public class GetPaymentOptionsResponse
    {
        public GetPaymentOptionsResponse() { }

        public List<ErrorData> error { get; set; }
        public List<ReceiverOptions> receiverOptions { get; set; }
        public ResponseEnvelope responseEnvelope { get; set; }
        public SenderOptions senderOptions { get; set; }

    }
    public partial class SetPaymentOptionsRequest
    {

      
        public RequestEnvelope requestEnvelope{get;set;}
       
        public string payKey {get;set;}
        public List<ReceiverOptions> receiverOptions { get; set; }
        public SenderOptions senderOptions{get;set;}

        public SetPaymentOptionsRequest()
        {
            this.requestEnvelope = new RequestEnvelope();
            this.payKey = payKey;
        }

    }
    public partial class SetPaymentOptionsResponse
    {

        public ResponseEnvelope responseEnvelope { get; set; }
        public List<ErrorData> error { get; set; }

        public SetPaymentOptionsResponse()
        {
        }

    }
    public class PaymentDetailRequest
    {
        public RequestEnvelope requestEnvelope { get; set; }
         [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string payKey { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string trackingId { get; set; }
        public PaymentDetailRequest()
        {
        }
        public PaymentDetailRequest(string paykey)
        {
            requestEnvelope = new RequestEnvelope();
            payKey = paykey;
        }
    }
    public class PaymentDetailsResponse
    {
        public string actionType { get; set; }
        public string cancelUrl { get; set; }
        public string currencyCode { get; set; }
        public string feesPayer { get; set; }
        public FundingConstraint fundingConstraint { get; set; }
        public ResponseEnvelope responseEnvelope { get; set; }



        //public string ipnNotificationUrl { get; set; }

        public string memo { get; set; }
        public string payKey { get; set; }

        public PaymentInfoList paymentInfoList { get; set; }

        public string returnUrl { get; set; }

        public ShippingAddressInfo shippingAddress { get; set; }
        public string senderEmail { get; set; }
        public string status { get; set; }

        public string trackingId { get; set; }

        public string transactionId { get; set; }




        public bool? reverseAllParallelPaymentsOnError { get; set; }




        public SenderIdentifier sender { get; set; }

        public string payKeyExpirationDate { get; set; }

        public List<ErrorData> error { get; set; }

        public PaymentDetailsResponse()
        {
        }
    }
    public class PreapprovalResponse
    {
        public ResponseEnvelope responseEnvelope { get; set; }
        public string preapprovalKey { get; set; }

    }

    public partial class PreapprovalRequest
    {

        private string _currencyCode;
        public RequestEnvelope requestEnvelope { get; set; }

        //public ClientDetailsType clientDetails { get; set; }

        public string cancelUrl { get; set; }

        public string currencyCode
        {
            get
            {
                return _currencyCode;
            }

            set
            {
                if (value == CurrencyCodes.CNY.ToString())
                {
                    _currencyCode = CurrencyCodes.USD.ToString();
                }
                else
                {
                    _currencyCode = value;
                }
            }

        }
        public string returnUrl { get; set; }
        public System.DateTime startingDate { get; set; }

        //public int dateOfMonth { get; set; }

        //public bool dateOfMonthSpecified { get; set; }
        //public DayOfWeek dayOfWeek { get; set; }

        //public bool dayOfWeekSpecified { get; set; }

        public DateTime endingDate { get; set; }
        //public bool endingDateSpecified { get; set; }

        //public decimal maxAmountPerPayment { get; set; }

        //public bool maxAmountPerPaymentSpecified { get; set; }

        //public int maxNumberOfPayments { get; set; }

        //public bool maxNumberOfPaymentsSpecified { get; set; }

        //public int maxNumberOfPaymentsPerPeriod { get; set; }

        //public bool maxNumberOfPaymentsPerPeriodSpecified { get; set; }

        public decimal maxTotalAmountOfAllPayments { get; set; }

        //public bool maxTotalAmountOfAllPaymentsSpecified { get; set; }

        //public string paymentPeriod { get; set; }

       

        //public string memo { get; set; }

        public string ipnNotificationUrl { get; set; }

      //  public string senderEmail { get; set; }

        

        //public string pinType { get; set; }

        //public string feesPayer { get; set; }

        //public bool displayMaxTotalAmount { get; set; }

        //public bool displayMaxTotalAmountSpecified { get; set; }


    }
    public class PreapprovalDetailsRequest
    {
        public PreapprovalDetailsRequest()
        {
        }
        public PreapprovalDetailsRequest(string preapprovalkey)
        {
            requestEnvelope = new RequestEnvelope();
            preapprovalKey = preapprovalkey;
        }
        public string preapprovalKey { get; set; }
        public RequestEnvelope requestEnvelope { get; set; }

    }

    public class PreapprovalDetailsResponse
    {
      
        public bool? approved { get; set; }
        public string cancelUrl { get; set; }
        //The current number of payments by the sender for this preapproval.
        public long? curPayments { get; set; }
        //The current total of payments by the sender for this preapproval.
        public decimal? curPaymentsAmount { get; set; }
      
        public string endingDate { get; set; }
        public List<ErrorData> error { get; set; }
        public decimal? maxTotalAmountOfAllPayments { get; set; }
        public ResponseEnvelope responseEnvelope { get; set; }
        public string returnUrl { get; set; }
        public SenderIdentifier sender { get; set; }
        public string senderEmail { get; set; }
        public string startingDate { get; set; }
        public string status { get; set; }
       
        #region 
        //public string feesPayer { get; set; }
        //public string ipnNotificationUrl { get; set; }
        //public decimal? maxAmountPerPayment { get; set; }
        //public int? maxNumberOfPayments { get; set; }
        //public int? maxNumberOfPaymentsPerPeriod { get; set; }
        // public long? curPeriodAttempts { get; set; }
        // public string curPeriodEndingDate { get; set; }
        //public string currencyCode { get; set; }
        //public int? dateOfMonth { get; set; }
        //public DayOfWeek? dayOfWeek { get; set; }
        //public bool? displayMaxTotalAmount { get; set; }
        //public string memo { get; set; }
        //public string paymentPeriod { get; set; }
        //public string pinType { get; set; }
        #endregion

    }
    public class RefundRequest
    {
        private string _currencyCode;
        public RequestEnvelope requestEnvelope { get; set; }

        public string currencyCode
        {
            get
            {
                return _currencyCode;
            }

            set
            {
                if (value == CurrencyCodes.CNY.ToString())
                {
                    _currencyCode = CurrencyCodes.USD.ToString();
                }
                else
                {
                    _currencyCode = value;
                }
            }

        }
       
        public string payKey { get; set; }

        public ReceiverList receiverList { get; set; }
        public RefundRequest(string paymentKey)
        {
            this.payKey = paymentKey;
        }
        public RefundRequest() { }
    }

    public class RefundResponse
    {


        public ResponseEnvelope responseEnvelope { get; set; }

        public string currencyCode { get; set; }

        public RefundInfoList refundInfoList { get; set; }
      
        public RefundResponse() { }


    }
    public class PPFaultMessage
    {
        public ResponseEnvelope responseEnvelope { get; set; }
        public List<ErrorData> error { get; set; }
    }
    public class ClientDetailsType
    {
        public string applicationId { get; set; }
        public string customerId { get; set; }
        public string customerType { get; set; }
        public string deviceId { get; set; }
        public string geoLocation { get; set; }
        public string ipAddress { get; set; }
        public string model { get; set; }
        public string partnerName { get; set; }

    }
    public class RefundInfoList
    {

        public List<RefundInfo> refundInfo { get; set; }

        public RefundInfoList()
        {
        }
    }
    public class RefundInfo
    {


        public Receiver receiver { get; set; }

        public string refundStatus { get; set; }

        public decimal refundNetAmount { get; set; }

        public bool refundNetAmountSpecified { get; set; }

        public decimal refundFeeAmount { get; set; }

        public bool refundFeeAmountSpecified { get; set; }

        public decimal refundGrossAmount { get; set; }

        public bool refundGrossAmountSpecified { get; set; }

        public decimal totalOfAllRefunds { get; set; }

        public bool totalOfAllRefundsSpecified { get; set; }

        public bool refundHasBecomeFull { get; set; }

        public bool refundHasBecomeFullSpecified { get; set; }

        public string encryptedRefundTransactionId { get; set; }

        public string refundTransactionStatus { get; set; }

        public ErrorData[] errorList { get; set; }

    }
    public partial class SenderOptions
    {
        public string referrerCode  { get; set; }

        public SenderOptions()
        {
        }

    }


    public class ReceiverOptions
    {

        //public string customId { get; set; }
        //public string description { get; set; }
        public InvoiceData invoiceData { get; set; }
        public ReceiverIdentifier receiver { get; set; }
       // public string referrerCode { get; set; }
        public ReceiverOptions(InvoiceData invoiceData, string email)
        {
            this.invoiceData = invoiceData;
            receiver = new ReceiverIdentifier();
            receiver.email = email;
        }

        public ReceiverOptions()
        {
        }

    }

    public class InvoiceData
    {
        public List<InvoiceItem> item { get; set; }
       
        //public decimal? totalTax { get; set; }
        
        //public decimal? totalShipping { get; set; }
      
        public InvoiceData()
        {
        }
        public InvoiceData(List<InvoiceItem> invoiceItem)
        {
            item = invoiceItem;
        }
       
    }
    public partial class InvoiceItem
    {

        public string name { get; set; }


        public string identifier { get; set; }
  
      
        public decimal? price { get; set; }

        //public decimal? itemPrice { get; set; }

        //public int? itemCount { get; set; }
       
        public InvoiceItem()
        {
        }

       
    }
    public partial class ReceiverIdentifier : AccountIdentifier
    {

        public ReceiverIdentifier()
        {
        }

    
    }
    public class ShippingAddressInfo
    {

        public string addresseeName { get; set; }

        public string street1 { get; set; }

        public string street2 { get; set; }

        public string city { get; set; }

        public string state { get; set; }

        public string zip { get; set; }

        public string country { get; set; }

        public List<PhoneNumber> phone { get; set; }

        public ShippingAddressInfo()
        {
        }

    }

    public class PhoneNumber : PhoneNumberType
    {

        public PhoneType? type { get; set; }


        public PhoneNumber(PhoneType? type)
        {
            this.type = type;
        }


        public PhoneNumber()
        {
        }
    }
    public class SenderIdentifier : AccountIdentifier
    {


        public TaxIdDetails taxIdDetails { get; set; }

        public SenderIdentifier()
        {
        }
    }
    public class TaxIdDetails
    {

        public string taxId { get; set; }

        public string taxIdType { get; set; }

        public TaxIdDetails()
        {
        }
    }
    public class AccountIdentifier
    {

        public string email { get; set; }
          [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PhoneNumberType phone { get; set; }
          [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string accountId { get; set; }

        public AccountIdentifier()
        {
        }
    }

    public class PhoneNumberType
    {
        public string countryCode { get; set; }

        public string phoneNumber { get; set; }

        public string extension { get; set; }

        public PhoneNumberType(string countryCode, string phoneNumber)
        {
            this.countryCode = countryCode;
            this.phoneNumber = phoneNumber;
        }
        public PhoneNumberType()
        {
        }



    }
    public class PaymentInfoList
    {

        public List<PaymentInfo> paymentInfo { get; set; }

        public PaymentInfoList()
        {
        }
    }
    public class FundingConstraint
    {
        public FundingTypeList allowedFundingType { get; set; }

        public FundingConstraint()
        {
        }
    }

    public class FundingTypeList
    {

        public List<FundingTypeInfo> fundingTypeInfo { get; set; }

        public FundingTypeList(List<FundingTypeInfo> fundingTypeInfo)
        {
            this.fundingTypeInfo = fundingTypeInfo;
        }
        public FundingTypeList()
        {
        }
    }
    public class FundingTypeInfo
    {


        public string fundingType { get; set; }


        public FundingTypeInfo(string fundingType)
        {
            this.fundingType = fundingType;
        }
        public FundingTypeInfo()
        {
        }
    }
    public class PaymentInfo
    {
        public string transactionId { get; set; }

        public string transactionStatus { get; set; }


        public Receiver receiver { get; set; }

        public decimal? refundedAmount { get; set; }

        public bool? pendingRefund { get; set; }

        public string senderTransactionId { get; set; }

        public string senderTransactionStatus { get; set; }


        public string pendingReason { get; set; }

        public PaymentInfo()
        {
        }



    }
    public class WarningDataList
    {


        public List<WarningData> warningData { get; set; }

        public WarningDataList()
        {
        }
    }

    public class WarningData
    {

        public long? warningId { get; set; }

        public string message { get; set; }

        public WarningData()
        {
        }
    }
    public class PayErrorList
    {

        public List<PayError> payError { get; set; }

        public PayErrorList()
        {
        }


    }

    public class PayError
    {
        public Receiver receiver { get; set; }

        public ErrorData error { get; set; }

        public PayError()
        {
        }



    }

    public class ErrorData
    {

        public long? errorId { get; set; }

        public string domain { get; set; }

        public string subdomain { get; set; }


        public ErrorSeverity? severity { get; set; }

        public ErrorCategory? category { get; set; }

        public string message { get; set; }


        public string[] parameter { get; set; }

        public ErrorData()
        {
        }

    }


    public class ErrorParameter
    {

        public string name { get; set; }

        public string value { get; set; }

        public ErrorParameter()
        {
        }

    }

    public class ResponseEnvelope
    {

        public System.DateTime timestamp { get; set; }


        public AckCode? ack { get; set; }



        public string correlationId { get; set; }

        public string build { get; set; }
    }
    public class Receiver
    {
        public decimal? amount { get; set; }
        public string email { get; set; }
       // public string invoiceId { get; set; }
        public bool primary { get; set; }

    }

    public class RequestEnvelope
    {

        public string detailLevel
        {
            get
            {
                return "ReturnAll";
            }
        }

        public string errorLanguage
        {
            get
            {
                return "en_US";
            }
        }
    }

    public class ReceiverList
    {

        private List<Receiver> receiverField = new List<Receiver>();
        public List<Receiver> receiver
        {
            get
            {
                return this.receiverField;
            }
            set
            {
                this.receiverField = value;
            }
        }

        public ReceiverList(List<Receiver> receiver)
        {
            this.receiverField = receiver;
        }

        public ReceiverList()
        {
        }
    }


    public enum PaypalAPIOperations
    {
        Pay,
        PaymentDetails,
        ExecutePayment,
        GetPaymentOptions,
        SetPaymentOptions,
        GetPrePaymentDisclosure,
        Preapproval,
        PreapprovalDetails,
        CancelPreapproval,
        Refund,
        ConvertCurrency,
        GetFundingPlans,
        GetShippingAddresses

    }

    public enum PaypalCmd
    {
        [Description("_ap-payment")]
        _ap_payment,
        [Description("_notify-validate")]
        _notify_validate,
        [Description("_ap-preapproval")]
        _ap_preapproval
    }

    public enum PaypalFeeReceiver
    {
        EACHRECEIVER,
        SENDER,
        PRIMARYRECEIVER,
        SECONDARYONLY
    }


    public enum AckCode
    {
        [Description("Success")]
        SUCCESS,
        [Description("Failure")]
        FAILURE,
        [Description("Warning")]
        WARNING,
        [Description("SuccessWithWarning")]
        SUCCESSWITHWARNING,
        [Description("FailureWithWarning")]
        FAILUREWITHWARNING
    }

    public enum ErrorCategory
    {
        [Description("System")]
        SYSTEM,
        [Description("Application")]
        APPLICATION,
        [Description("Request")]
        REQUEST
    }

    public enum ErrorSeverity
    {
        [Description("Error")]
        ERROR,
        [Description("Warning")]
        WARNING
    }

    public enum PhoneType
    {
        [Description("NONE")]
        NONE,
        [Description("HOME")]
        HOME,
        [Description("WORK")]
        WORK,
        [Description("BUSINESS")]
        BUSINESS,
        [Description("MOBILE")]
        MOBILE,
        [Description("FAX_HOME")]
        FAXHOME,
        [Description("FAX_BUSINESS")]
        FAXBUSINESS
    }
    public enum RefundType
    {
        Full,
        Partial
    }
    public enum RefundStatus
    {
        [Description("Refund successfully completed.")]
        REFUNDED,
        [Description("Refund awaiting transfer of funds; for example, a refund paid by eCheck.")]
        REFUNDED_PENDING,
        [Description("Payment was never made; therefore, it cannot be refunded.")]
        NOT_PAID,
        [Description("Request rejected because the refund was already made, or the payment was reversed prior to this request.")]
        ALREADY_REVERSED_OR_REFUNDED,
        [Description("Request cannot be completed because you do not give us paypal access to make the refund.")]
        NO_API_ACCESS_TO_RECEIVER,
        [Description("Refund is not allowed. Please call Paypal for more information.")]
        REFUND_NOT_ALLOWED,
        [Description("Request rejected because the receiver from which the refund is to be paid does not have sufficient funds or the funding source cannot be used to make a refund")]
        INSUFFICIENT_BALANCE,
        [Description("Request rejected because you attempted to refund more than the remaining amount of the payment.")]
        AMOUNT_EXCEEDS_REFUNDABLE,
        [Description("Request rejected because a refund is currently pending for this part of the payment.")]
        PREVIOUS_REFUND_PENDING,
        [Description("Request rejected because it cannot be processed at this time. Please call Paypal for more information.")]
        NOT_PROCESSED,
        [Description("Request rejected because of an internal error. Please call Paypal for more information.")]
        REFUND_ERROR,
        [Description("Request rejected because another part of this refund caused an internal error. Please call Paypal for more information.")]
        PREVIOUS_REFUND_ERROR
    }
}