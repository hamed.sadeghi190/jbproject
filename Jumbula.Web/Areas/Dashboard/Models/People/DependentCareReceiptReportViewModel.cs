﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class DependentCareReceiptReportViewModel
    {
        public DependentCareReceiptReportViewModel()
        {

        }

        public int? Year { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public DependentCareReceiptReportFilterType FilterType { get; set; }
    }

    public enum DependentCareReceiptReportFilterType : byte
    {
        Year,
        DateRange
    }
}