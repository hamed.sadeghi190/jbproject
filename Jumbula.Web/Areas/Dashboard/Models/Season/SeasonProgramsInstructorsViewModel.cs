﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SeasonProgramsInstructorsViewModel
    {
        public List<SeasonProgramsInstructorsItemViewModel> ProgramInstructors { get; set; }

        public List<SelectKeyValue<int>> AllInstructors { get; set; }
    }

    public class SeasonProgramsInstructorsItemViewModel
    {
        public long ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string SchoolName { get; set; }
        public List<int> SelectedInstructors { get; set; }
        public List<SelectKeyValue<int>> SelectedInstructorsStatus { get; set; }
    }
}