﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class ReportHeaderViewModel
    {
        public string ClubName { get; set; }

        public string Date { get; set; }

        public string SelectedClubName { get; set; }

        public string SeasonName { get; set; }

        public List<SelectKeyValue<string>> InvoiceTypes { get; set; }
    }
}