﻿; (function () {
    'use strict';
    var isHome = $("body").hasClass("home");
    $("#portfolio").click(function (e) { 
        var windowHeight = $(window).height();
        var windowWidth = $(window).width();
        if (windowHeight >= 550 && windowWidth > 570) {
            e.preventDefault();
            var marketsCount = $("#backMenu a").length;
            $("#backMenu .img-content.same-height, #backMenu .text-canton.same-height").outerHeight($(window).height() / marketsCount);
            $("#portfolioBackmenu").fadeIn();
            $("#backMenu").fadeIn().removeClass("transform-close").addClass("transform-open");
        } 
    });

    $("#portfolioBackmenu, .close-back-menu").click(function (e) {
        $("#backMenu").removeClass("transform-open").addClass("transform-close").fadeOut();
        $("#portfolioBackmenu").fadeOut();
    });

    if (!isHome) {
        $("#solutionCover .jump-down").click(function (e) {
            e.preventDefault();
            $("html, body").animate({ scrollTop: $("#solutionCover").offset().top + $("#solutionCover").outerHeight() }, 300);
        });

        var scrollize = function () {
            var scroll = $(this).scrollTop(),
                coverHeight = $("#solutionCover").outerHeight();

            if (scroll >= coverHeight) {
                $("#mainHeader .navbar-default").addClass("menu-enter");
            } else {
                $("#mainHeader .navbar-default").removeClass("menu-enter");
            }
        }
        $(window).scroll(scrollize);
        scrollize();
    }
})(window);