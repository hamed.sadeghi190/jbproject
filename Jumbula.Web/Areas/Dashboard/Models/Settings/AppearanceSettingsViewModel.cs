﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class AppearanceSettingViewModel
    {
        public bool HideProgramDates { get; set; }
        public bool HideProgramDatesInCart { get; set; }
        public bool HidePhoneNumber { get; set; }
        public string ContactBoxBackColor { get; set; }
        public string TestimonialsBoxBackColor { get; set; }
        public string TestimonialsBoxColor { get; set; }
        public string TestimonialsTitle { get; set; }
        public string TuitionLabelTextColor { get; set; }
        public string PriceTextColor { get; set; }
        public string PriceBackColor { get; set; }
        public string ERButtonTextColor { get; set; }
        public string ERButtonBackColor { get; set; }
        public string ERButtonTextLabel { get; set; }

        public  bool IsClubPartner { get; set; }

        public bool ShowProviderNameInClassPage { get; set; }

        public bool HideGoogleMapInClassPage { get; set; }
    }
}