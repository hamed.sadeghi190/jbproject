﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class Order : BaseEntity<long>
    {

        public Order()
        {
          OrderItems = new HashSet<OrderItem>();
          IsDraft = true;
          IsLive = true;
        }
        public Order(bool isOrderLive)
        {
            OrderItems = new HashSet<OrderItem>();
            IsDraft = true;
            IsLive = isOrderLive;
        }
        public bool IsLive { get; set; }
        public int ClubId { get; set; }
        public virtual Club Club { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }
        public decimal OrderAmount { get; set; }

        [DefaultValue(true)]
        public bool IsDraft { get; set; }

        [DefaultValue(false)]
        public bool IsAutoCharge { get; set; }

        [StringLength(64)]
        public string ConfirmationId { get; set; }

        public DateTime CompleteDate { get; set; }

        public DateTime? SubmitDate { get; set; }

        public OrderMode OrderMode { get; set; }
        public PaymentMethod? PaymentMethod { get; set; }

        public OrderStatusCategories OrderStatus { get; set; }
        public PaymentPlanType PaymentPlanType { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public virtual ICollection<OrderHistory> OrderHistories { get; set; }

        public virtual ICollection<TransactionActivity> TransactionActivities { get; set; }

        public virtual ICollection<OrderPreapproval> OrderPreapprovals { get; set; }
        public virtual ICollection<PendingPreapprovalTransaction> PendingPreapprovalTransactions { get; set; }

        [NotMapped]
        public IEnumerable<OrderItem> PreregisterItems
        {
            get
            {
                return OrderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.PreRegistrationStatus == PreRegistrationStatus.Initiated);
            }
        }

        [NotMapped]
        public IEnumerable<OrderItem> LotteryItems
        {
            get
            {
                return OrderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted && (o.LotteryStatus == LotteryStatus.Initiated || o.LotteryStatus == LotteryStatus.Condidate || o.LotteryStatus == LotteryStatus.Lose));
            }
        }

        [NotMapped]
        public IEnumerable<OrderItem> RegularOrderItems
        {
            get
            {
                return OrderItems.Where(o => o.ItemStatus != OrderItemStatusCategories.deleted && o.ItemStatus != OrderItemStatusCategories.notInitiated);
            }
        }

        public decimal CalculateOrderAmount
        {
            get
            {
                return RegularOrderItems.Where(c => c.ItemStatusReason != OrderItemStatusReasons.transferOut && c.PreRegistrationStatus != PreRegistrationStatus.Initiated).Sum(item => item.TotalAmount);
            }
        }
        [NotMapped]
        public decimal PaidAmount
        {
            get
            {
                return RegularOrderItems.Sum(item => item.PaidAmount);
            }
        }
        [NotMapped]
        public bool IsEditedOrder { get; set; }
        [NotMapped]
        public virtual ICollection<OrderInstallment> OrderInstallments { get; set; }

    }
}