﻿(function () {
    'use strict';

    angular.module('jbTools').service("EmailService", [function () {

        var model;

        function setModel(data) {
            model = data;
        }

        function getModel() {
            return model;
        }

        return {
            set: setModel,
            get: getModel
        }
    }]);
})();