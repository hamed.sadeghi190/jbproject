﻿(function () {
    'use strict';

    angular.module('jbTools').service("ProgramSchedulePartsService", [function () {

        var _isScheduleChanged;
        var isScheduleChangedInStorage;


       
        function setAttribute(Data) {
            _isScheduleChanged = Data;
            localStorage.setItem(isScheduleChangedInStorage, Data);
        }

        function getAttribute() {

            if (_isScheduleChanged != 'undefined') {

                return _isScheduleChanged;
            } else {
                var data = localStorage.isScheduleChangedInStorage;
                return data;
            }
            
        }

        return {
            set: setAttribute,
            get: getAttribute
        }
    }]);
})();