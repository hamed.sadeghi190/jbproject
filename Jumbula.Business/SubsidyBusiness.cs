﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class SubsidyBusiness :  ISubsidyBusiness
    {
        private readonly IRepository<Subsidy> _subsidyRepository;

        public SubsidyBusiness(IRepository<Subsidy> subsidyRepository)
        {
            _subsidyRepository = subsidyRepository;
        }

        public OperationStatus Create(Subsidy subsidy)
        {
            return _subsidyRepository.Create(subsidy);

        }
        public IQueryable<Subsidy> GetList()
        {
            return _subsidyRepository.GetList();
        }
        public Subsidy Get(int subsidyId)
        {
            return _subsidyRepository.Get(subsidyId);
        }
        public OperationStatus Update(Subsidy subsidy)
        {
            return _subsidyRepository.Update(subsidy);
        }
        public IQueryable<Subsidy> GetAllSubsidiesClub(int clubId)
        {
            return _subsidyRepository.GetList().Where(s => s.PartnerId == clubId);
        }
    }
}
