﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('subscriptionSingOutReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;


        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.Model = customReportService.getModelFromStrorage();

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};
        $http.get(baseUrl + '/GetSubscriptionSignOutReportDetail', { params: { programId: $scope.Model._SelectedPrograms, date: $scope.Model.DateProgram } }).success(function (data, status, headers, config) {

            dataInfo = data;
            $scope.ProgramName = data.ProgramName;
            $scope.ClubName = data.ClubName;
            $scope.ClassDate = data.StrClassDate;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        var model = $scope.Model;
        $scope.SingOutReportDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/RunSignOutReport',
                        type: "POST",
                        data: function () {
                            return {
                                programId: model._SelectedPrograms,
                                date: $scope.Model.DateProgram,
                            };
                        }
                       
                    },

                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 20,
            resizable: true,
        });
        $scope.SingOutReportGridOptions = {
            excel: {
                allPages: true,
                fileName: "SingOutList_Report.xlsx",
                proxyURL: "/Dashboard/CustomReport/Save"
            },
            excelExport: function (e) {
                var sheet = e.workbook.sheets[0];
                cfpLoadingBar.complete();
            },
            serverPaging: true,
            dataSource: $scope.SingOutReportDataSource,
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5,
            },
            dataBound: function () {
                var grid = $("#SingOutReportGrid").data("kendoGrid");
                customReportService.initializeDoubleScroll(grid);

                var rows = this.items();
                $(rows).each(function () {
                    var index = $(this).index() + 1
                        + ($("#SingOutReportGrid").data("kendoGrid").dataSource.pageSize() * ($("#SingOutReportGrid").data("kendoGrid").dataSource.page() - 1));;
                    var rowLabel = $(this).find(".row-number");
                    $(rowLabel).html(index);
                });

            },
            scrollable: true,
            sortable: { mode: 'single', allowUnsort: false },
            columns: [
                 {
                     field: "Number",
                     template: "<span class='row-number'></span>",
                     title: "#",
                     width: "50px",
                     sortable:false
                 },
                {
                    field: "ParticipantName",
                    title: "Participant",
                    width: "200px",
                },
                 {
                     field: "Grade",
                     title: "Grade",
                     width: "50px"
                 },
                 {
                     field: "TuitionName",
                     title: "Tuition label",
                     width: "100px"
                 },
                {
                    field: "Present",
                    title: "Present",
                    width: "100px",
                },
                {
                    field: "Signature",
                    title: "Signature",
                    width: "200px",
                },
                {
                    field: "Time",
                    title: "Time",
                },
            ]
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#SingOutReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.saveToPDF = function () {

            $http({
                url: baseUrl + '/GenerateSignReportPdf',
                method: "POST",
                data:
                   {
                       programId: $scope.Model._SelectedPrograms,
                       startDate: $scope.Model.DateProgram,

                   },
                headers: {
                    'Content-type': 'application/json'
                },
                responseType: 'arraybuffer'
            }).success(function (data, status, headers, config) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.target = '_self';

                a.download = "Sign report.pdf";
                a.click();
            });
        }

    }
    ]);

})();