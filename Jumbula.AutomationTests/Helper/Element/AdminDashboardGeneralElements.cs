﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class AdminDashboardGeneralElements : BaseElements
    {
        public AdminDashboardGeneralElements(IWebDriver driver)
            :base(driver)
        {

        }
        public IWebElement ClubName
        {
            get
            {
                return Driver.FindElement(By.ClassName("account-menu-clubnames"));
            }
        }
        public IWebElement Username
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainBody']/nav/nav/div[2]/ul/li[1]"));
            }
        }
        public IWebElement Role
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='mainBody']/nav/nav/div[2]/ul/li[2]"));
            }
        }
        public IWebElement DomainLink
        {
            get
            {
                return Driver.FindElement(By.XPath("//a[contains(text(),'Domains')]"));
            }
        }
        public List <IWebElement> Domains
        {
            get
            {
                return Driver.FindElements(By.XPath("//*[@id='mainBody']/nav/nav/div[2]/ul/li[4]/ul/li")).ToList();
            }
        }
        public IWebElement GetDomain(string domain)
        {
            var domains = Domains.ToList();
            return domains.First(d => d.Text == domain);
        }
    }
}
