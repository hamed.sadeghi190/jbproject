﻿using Jumbula.Common.Enums;
using System;
using System.Globalization;

namespace Jumbula.Common.Helper
{
    public static class CurrencyHelper
    {
        public static decimal RoundCurrencyWithPenny(decimal money)
        {
            return Math.Round(money, 2);
        }

        public static decimal RoundCurrencyWithOutPenny(decimal money)
        {
            return Math.Round(money);
        }

        public static decimal RoundCurrencyCeilingWithoutPenny(decimal money)
        {
            return Math.Ceiling(money);
        }

        public static string FormatCurrencyWithPenny(decimal? money, bool withNegativeSign = false)
        {
            return FormatCurrency(money, null, true, withNegativeSign);
        }

        public static string FormatCurrencyWithPenny(decimal? money, CurrencyCodes currency, bool withNegativeSign = false)
        {
            return FormatCurrency(money, currency, true, withNegativeSign);
        }

        public static string FormatCurrencyWithoutPenny(decimal? money, bool withNegativeSign = false)
        {
            return FormatCurrency(money, null, false, withNegativeSign);
        }

        public static string FormatCurrencyWithoutPenny(decimal? money, CurrencyCodes currency, bool withNegativeSign = false)
        {
            return FormatCurrency(money, currency, false, withNegativeSign);
        }

        public static string FormatPercentWithoutPenny(decimal? money)
        {
            return money.HasValue ? $"{money.Value:#.##} %" : string.Empty;
        }

        private static string FormatCurrency(decimal? money, CurrencyCodes? currency, bool hasPenny, bool withNegativeSign)
        {
            if (!money.HasValue)
                return string.Empty;

            if (!withNegativeSign)
                money = Math.Abs(money.Value);

            var decimalsFormat = "{0:C0}";

            if (hasPenny)
                decimalsFormat = "{0:C2}";

            var culture = currency.HasValue ? CultureInfo.CreateSpecificCulture(currency.ToDescription()) : CultureInfo.InvariantCulture;

            var clonedNumbers = (NumberFormatInfo)culture.NumberFormat.Clone();
            clonedNumbers.CurrencyNegativePattern = 1;

            var result = string.Format(clonedNumbers, decimalsFormat, money);

            if (currency == CurrencyCodes.MYR)
                result = result.Replace("RM", CurrencyCodes.MYR.ToString());

            return result;
        }
    }
}

