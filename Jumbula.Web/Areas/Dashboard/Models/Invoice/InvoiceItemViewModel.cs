﻿using SportsClub.Areas.Dashboard.Models.Invoice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class InvoiceItemViewModel
    {
        public List<InvoiceOrderViewModel> Orders { get; set; }
        public List<InvoiceOrderItemViewModel> OrderItems { get; set; }
        public List<InvoiceInstallmentViewModel> Instalments { get; set; }
    }
}