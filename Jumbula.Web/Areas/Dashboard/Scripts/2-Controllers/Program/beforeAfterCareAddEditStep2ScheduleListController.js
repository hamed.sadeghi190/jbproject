﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('beforeAfterCareAddEditStep2ScheduleListController', ['$scope', '$http', '$log', '$stateParams', '$state', 'programService', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, programService, jbToast, jbConfirmation) {

        $scope.baseUrl = window.location.origin + "/Program";

        window.app.globalObjects.pageClass = 'seasons-view programs-add ' + $stateParams.programType;
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.MC.stateParams = $stateParams;

        $scope.seasonName = $stateParams.seasonDomain;
        $scope.programName = $stateParams.programDomain;


        $http.get('Dashboard/Program/BeforeAfterCareCreateEditStep2ScheduleList', { params: { programId: $stateParams.programId } })
             .success(function (data, status, headers, config) {

                 $scope.Model = data;
             });

        $scope.LastCreatedPage = "Step2.75";

        $scope.save = function () {
            $http.post('Dashboard/Program/BeforeAfterCareCreateEditStep2ScheduleList', { model: $scope.Model, programId: $stateParams.programId })
                .success(function (data, status, headers, config) {
                    if (data.Status) {
                        $state.go('ProgramAddEditStep3', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $scope.MC.stateParams.programId });
                    } else {

                        $scope.MC.handleErrors($scope, data);
                    }
                })
        }

        $scope.editSchedulePart = function (schedulePart) {

            schedulePart._DueDate = schedulePart.DueDate;
            schedulePart._StartDate = schedulePart.StartDate;
            schedulePart._EndDate = schedulePart.EndDate;

            for (var i = 0; i < schedulePart.schedulePartCharges.length; i++) {

                schedulePart.schedulePartCharges[i]._Amount = schedulePart.schedulePartCharges[i].Amount;
            }

            schedulePart.EditMode = true;
        }

        $scope.cancelSchedulePartEdit = function (schedulePart) {

            schedulePart.DueDate = schedulePart._DueDate;
            schedulePart.StartDate = schedulePart._StartDate;
            schedulePart.EndDate = schedulePart._EndDate;

            for (var i = 0; i < schedulePart.schedulePartCharges.length; i++) {

                schedulePart.schedulePartCharges[i].Amount = schedulePart.schedulePartCharges[i]._Amount;
            }

            schedulePart.EditMode = false;
        }

        $scope.saveSchedulePartEdit = function (schedulePart) {

            if (schedulePart.DueDate == 0 || schedulePart.DueDate == "" || schedulePart.DueDate == null || schedulePart.DueDate == 'undefined') {

                jbToast.error('The format of date is invalid.');

            } else if (schedulePart.StartDate == 0 || schedulePart.StartDate == "" || schedulePart.StartDate == null || schedulePart.StartDate == 'undefined') {

                jbToast.error('The format of date is invalid.');

            } else if (schedulePart.EndDate == 0 || schedulePart.EndDate == "" || schedulePart.EndDate == null || schedulePart.EndDate == 'undefined') {

                jbToast.error('The format of date is invalid.');
            }
            else {

                var hasError = false;
                var endDate = new Date(schedulePart.EndDate);

                var startDate = new Date(schedulePart.StartDate);
                
                var dueDate = new Date(schedulePart.DueDate);

                if (endDate <= startDate) {

                    jbToast.error('Start date should be before the end date.');
                    hasError = true;
                    
                }

                if (dueDate >= endDate) {
                    jbToast.error('Due date should be before the end date.');
                    hasError = true;
                }

                for (var i = 0; i < $scope.Model.ScheduleParts.length; i++) {

                    var dueDate2 = new Date($scope.Model.ScheduleParts[i].DueDate);

                    var endDate2 = new Date($scope.Model.ScheduleParts[i].EndDate);

                    var startdate2 = new Date($scope.Model.ScheduleParts[i].StartDate);
                    

                    if ($scope.Model.ScheduleParts[i].Id == schedulePart.Id) {

                        var tenBeforeStardate = new Date();
                        tenBeforeStardate = new Date(startdate2.getTime() - 10 * 24 * 60 * 60 * 1000);

                        if (dueDate < tenBeforeStardate) {
                            jbToast.error('Payment due date cannot be more than 10 days before from the start date.');
                            hasError = true;
                            break;
                        }
                       
                    }

                    if ($scope.Model.ScheduleParts[i].Id < schedulePart.Id) {

                        if (dueDate2 >= dueDate) {

                            jbToast.error('Payment due date should be after the before payment due date.');
                            hasError = true;
                            break;
                        }

                        if (endDate2 > startDate) {

                            jbToast.error('start date should be after the before end date schedule.');
                            hasError = true;
                            break;
                        }
                    }
                    else if ($scope.Model.ScheduleParts[i].Id > schedulePart.Id) {

                        if (endDate >= startdate2) {

                            jbToast.error('End date should be before the start date of the next schedule.');
                            hasError = true;
                            break;
                        }
                        if (dueDate >= dueDate2) {
                            jbToast.error('Payment due date should be before the next payment due date.');
                            hasError = true;
                            break;
                        }
                    }
                }
                if (!hasError) {

                    schedulePart._DueDate = schedulePart.DueDate;
                    schedulePart._StartDate = schedulePart.StartDate;
                    schedulePart._EndDate = schedulePart.EndDate;

                    for (var i = 0; i < schedulePart.schedulePartCharges.length; i++) {

                        schedulePart.schedulePartCharges[i]._Amount = schedulePart.schedulePartCharges[i].Amount;
                    }

                    schedulePart.EditMode = false;
                }      
            }
        }

        $scope.formatDate = function (date) {

            if (date != '' && date != null && date != 'undefined') {
                return new Date(date).format('mm/dd/yyyy');
            }
        }

        $scope.back = function () {
            $state.go('BeforeAfterCareAddEditStep2Fees', { seasonDomain: $scope.MC.stateParams.seasonDomain, programType: $scope.MC.stateParams.programType, programId: $stateParams.programId });
        }

        if ($state.returnByBack()) {
            $scope.MC.backQueueItems.pop();
        }
    }]);
})();

