﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Helper;
using System;
using Jumbula.Common.Enums;
using Jumbula.Core.Model.Generic;
using Jumbula.Core.Model.Order;

namespace SportsClub.Areas.Dashboard.Models
{
    public class MakePaymentViewModel
    {
        public MakePaymentViewModel()
        {
            SendEmail = false;
            PaymentMethods = SeedMakePaymentMethods();
            SelectedPaymentMethod = "0";
            SelectedChargeMethod = "";
            Installments = new List<InstallmentViewModel>();
            isPayInstallment = false;
            IsEdit = false;
        }
     
        public List<InstallmentViewModel> Installments { get; set; }
        public bool isPayInstallment { get; set; }
        public string Username { get; set; }
        public bool HasInstallment => Installments != null && Installments.Count > 0;
        public bool IsStripeEnabled { get; set; }

        public bool HasProgramScheduleId { get; set; }
        public bool IsPaypalEnabled { get; set; }
        public string SeasonDomain { get; set; }
        public long? SeasonId { get; set; }
        public long ProgramId { get; set; }
        public long OrderItemId { get; set; }
        [MaxLength(128, ErrorMessage = "{0} cannot be longer than 128 characters.")]
        [Display(Name = "Transaction/Check ID")]
        public string CheckId { get; set; }
        public decimal Amount { get; set; }
        public DateTime? PaymentDate { get; set; }
        public bool SendEmail { set; get; }
        [MaxLength(1000, ErrorMessage = "{0} cannot be longer than 1000 characters.")]
        public string Note { get; set; }
        public decimal Balance
        {
            get
            {
                if (OrderItem != null)
                {
                    return OrderItem.TotalAmount - OrderItem.PaidAmount;
                }
                return 0;
            }
        }
        public decimal NewBalance { get; set; }
        public decimal OldBalance
        {
            get

            { return Balance + Amount; }
        }
        public OrderItemsViewModel OrderItem { get; set; }
        public List<SelectKeyValue<string>> ChargeMethodList
        {
            get
            {
                return SeedMakeChargeMethods(IsStripeEnabled, IsPaypalEnabled);
            }

        }
        public string SelectedChargeMethod { get; set; }
        public string SelectedPaymentMethod { get; set; }
        public List<SelectKeyValue<string>> PaymentMethods { get; set; }

        public decimal AvailableCredit { get; set; }
        public bool IsEdit { get; set; }

        public long PaymentDetailId { get; set; }

        List<SelectKeyValue<string>> SeedMakePaymentMethods()
        {
            var result = new List<SelectKeyValue<string>>();
            result.Add(new SelectKeyValue<string>() { Text = "Select a payment method", Value = "0" });
            foreach (PaymentMethod payMethod in Enum.GetValues(typeof(PaymentMethod)))
            {
                if (payMethod != PaymentMethod.None && payMethod!=PaymentMethod.Credit && payMethod != PaymentMethod.Express)
                {
                    result.Add(new SelectKeyValue<string>() { Text = payMethod.ToDescription(), Value = ((int)payMethod).ToString() });
                }
            }

            return result;
        }

        List<SelectKeyValue<string>> SeedMakeChargeMethods(bool isStripeEnabled, bool isPaypalEnabled)
        {
            var result = new List<SelectKeyValue<string>>();

            foreach (ChargeMethod chargeMethod in Enum.GetValues(typeof(ChargeMethod)))
            {
              
                if (chargeMethod == ChargeMethod.PayNowWithCreditCard && (isStripeEnabled || IsPaypalEnabled))
                {
                    if (!IsPaypalEnabled && isStripeEnabled)
                    {
                        result.Add(new SelectKeyValue<string>()
                        {

                            Value = ((int)chargeMethod).ToString(),
                            Text = "I have the payer credit card and want to charge now"
                        });
                    }
                    else
                    {
                        result.Add(new SelectKeyValue<string>()
                        {

                            Value = ((int)chargeMethod).ToString(),
                            Text = chargeMethod.ToDescription()
                        });
                    }
                 
                }
                if (chargeMethod != ChargeMethod.PayNowWithCreditCard /*&& chargeMethod != ChargeMethod.PayNowWithPaypal*/)
                {
                    result.Add(new SelectKeyValue<string>()
                        {

                            Value = ((int)chargeMethod).ToString(),
                            Text = chargeMethod.ToDescription()
                        });
                }

            }

            return result;
        }

    }

    public enum ChargeMethod
    {
        [Description("I have already received the payment")]
        ReceivedPayment = 0,
        [Description("I want to pay with the family's available credit")]
        PayWithCredit = 1,
        [Description("I have the payer paypal or credit card and want to charge now")]
        PayNowWithCreditCard = 2,

      

    }



}