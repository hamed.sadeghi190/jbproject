﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('programDeleteController', ['$scope', '$http', '$log','$stateParams','$state', function ($scope, $http, $log,$stateParams,$state) {
        window.app.globalObjects.pageClass = 'seasons-view delete-program';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/SeasonSetup", order: 2, menu: "SEASONS" });

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $scope.programName = $stateParams.programDomain;
    }]);
})();