﻿(function () {
    'use strict';

    function publishFreezeSeason($scope, $http, $stateParams, $state) {
        $scope.baseUrl = window.location.origin + "/Dashboard/Season";
        $scope.cancelPublish = function () {
            $state.go('SeasonSetup', { seasonDomain: $scope.MC.stateParams.seasonDomain });
        }
        $scope.publishSeason = function () {
            $http.post($scope.baseUrl + '/PublishSeason', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
                        $scope.MC.getSeasons();
                        $state.go('Seasons');
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };
    }
    function addEditSeason($scope, $http, $stateParams, $state, oldModel) {
        $scope.Model = {};
        $scope.Model.Domain = "";
        if ($stateParams.seasonDomain)
            $scope.Model.Domain = $stateParams.seasonDomain;

        $http.get(window.location.origin + "/Dashboard/season/get?seasonDomain=" + $scope.Model.Domain).success(function (data, status, headers, config) {
            $scope.Model = data;
            $scope.Model.IsCopy = "false";
            if (is.not.undefined(oldModel.IsCopy)) {
                $scope.Model = oldModel;
            }

        });

        $scope.saveSeason = function () {
            $http.post(window.location.origin + "/Dashboard/season/CreateEdit", { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (data.Status == true) {
        
                        $scope.MC.getSeasons();
                        $scope.MC.getClubbaseInfo();
                        $scope.MC.currentSeason.Title = data.Data;
                        $scope.MC.stateParams.seasonDomain = data.Message;
                        $state.go('Season', { seasonDomain: data.Message  });
                    }
                    else {
                        $scope.MC.handleErrors($scope, data);
                    }
                });
        };

        $scope.cancelSeason = function () {
            $scope.MC.getSeasons();
            $state.go('Seasons');
        }
    }

    angular.module('dashboardApp').controller('seasonsController', ['$scope', '$http', function ($scope, $http) {
        window.app.globalObjects.pageClass = 'seasons-view step1';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/Seasons", order: 0, menu: "SEASONS" });
        

    }]).controller('seasonAddController', ['$scope', '$http', '$stateParams', '$state', 'seasonService', function ($scope, $http, $stateParams, $state, seasonService) {
        window.app.globalObjects.pageClass = 'seasons-view add-season';
        window.app.globalObjects.initializeView();

        var oldModel = seasonService.getModelFromStrorage();

        addEditSeason($scope, $http, $stateParams, $state, oldModel);
        $scope.pageTitle = "Add Season";

        var baseUrl = window.location.origin + '/Dashboard/Season';

        if (is.undefined($scope.Model)) {
            $scope.Model = {};
        }

        $scope.FillSeasonList = function () {
            $http.get(baseUrl + '/GetAllSeasonList')
                .success(function (data, status, headers, config) {
                    $scope.Model.SeasonList = data.seasons;
                    $scope.Model.Months = data.months;
                });
        }

        
        $scope.copySeasonStep2 = function () {
            seasonService.setModelInStorage($scope.Model, "seasonModel");
            $http.post(baseUrl + '/CopySeasonStep1', { model: $scope.Model }).success(function (data, status, headers, config) {
                if (data.Status) {
                    $scope.Model.Title = data.Data.Title;
                    $scope.Model.Description = data.Data.Description;
                    seasonService.setModelInStorage($scope.Model, "seasonModel");
                    $state.go('CopySeasonStep2');
                } else {
                    $scope.MC.handleErrors($scope, data);
                }
            }).error(function (data, status, headers, config) {

            });
        }

    }]).controller('seasonEditController', ['$scope', '$http', '$stateParams', '$state', function ($scope, $http, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'seasons-view add-season';
        window.app.globalObjects.initializeView();

        addEditSeason($scope, $http, $stateParams, $state);


        $scope.pageTitle = "Edit Season";
    }]).controller('seasonPublishController', ['$scope', '$http', '$stateParams', '$state', function ($scope, $http, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'seasons-view';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        publishFreezeSeason($scope, $http, $stateParams, $state);

        $scope.pageTitle = "Publish Season";
        $scope.pageAction = "Publish";
    }]).controller('seasonFreezeController', ['$scope', '$http', '$stateParams', '$state', function ($scope, $http, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'seasons-view';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;
        publishFreezeSeason($scope, $http, $stateParams, $state);

        $scope.pageTitle = "Freeze Season";
        $scope.pageAction = "Freeze";
    }]).controller('seasonController', ['$scope', '$http', '$stateParams', 'jbToast', '$state', function ($scope, $http, $stateParams, jbToast, $state) {
        window.app.globalObjects.pageClass = 'seasons-view step2';
        window.app.globalObjects.initializeView();
        $scope.MC.stateParams = $stateParams;

        $scope.MC.openBlade({ url: "Dashboard/Blade/Season", order: 1, menu: "SEASONS" });

        var baseUrl = window.location.origin + "/Dashboard/Season";

        $scope.Model = {};

        //var colors = ["#c2e4ff", "#8fcdff", "#5cb7ff", "#29a0ff", "#0088f5", "#006cc2", "#00508f", "#00335c"];
        var colors = ["#5cb7ff", "#8fcdff", "#0088f5", "#006cc2", "#00508f", "#00335c"];

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        
        var getCurrentSeason = function (currentSeasonDomain) {

            if ($scope.MC.seasons == null || $scope.MC.seasons == 'undefined' || $scope.MC.seasons.length < 1) {
                $http.get('/dashboard/season/GetList')
                    .success(function (data, status, headers, config) {
                        $scope.MC.seasons = data;

                        for (var i = 0; i < $scope.MC.seasons.length; i++) {
                            if ($scope.MC.seasons[i].Domain == currentSeasonDomain) {

                                var currentSeason = $scope.MC.seasons[i];

                                $scope.CurrentSeason = currentSeason;

                                $scope.CurrentSeason.CheckStatus = $scope.CurrentSeason.Status;

                                return currentSeason;
                            }
                        }
                    });
            }
            else {

                for (var i = 0; i < $scope.MC.seasons.length; i++) {
                    if ($scope.MC.seasons[i].Domain == currentSeasonDomain) {

                        var currentSeason = $scope.MC.seasons[i];

                        $scope.CurrentSeason = currentSeason;

                        $scope.CurrentSeason.CheckStatus = $scope.CurrentSeason.Status;

                        return currentSeason;
                    }
                }
            }
        }

        var seasonDomain = $stateParams.seasonDomain;

        getCurrentSeason(seasonDomain);

        $scope.cancelChangeMode = function () {
            $scope.CurrentSeason.CheckStatus = $scope.CurrentSeason.Status;
            $scope.CurrentSeason.ShowConfirmation = false;
        }

        $scope.switchSeasonMode = function () {
            $scope.CurrentSeason.ShowConfirmation = true;
        }

        $scope.switchLiveTestMode = function () {

            $scope.CurrentSeason.ShowConfirmation = false;

            $scope.CurrentSeason.EnteredTitle = '';

            if ($scope.CurrentSeason.Status == 'Live') {
                $scope.switchTestMode();
            }
            else {
                $scope.switchLiveMode();
            }
        }

        $scope.switchLiveMode = function () {
            $http.post(baseUrl + '/SwitchLiveMode', { seasonDomain: $scope.MC.stateParams.seasonDomain })
            .success(function (data, status, headers, config) {
                if (data.Status) {
                    $scope.CurrentSeason.Status = 'Live';
                    $scope.MC.getSeasons();
                    $state.go('Seasons');
                    jbToast.success('Season sets live mode successfully.');
                }
                else {
                    $scope.MC.handleErrors($scope, data);
                }

                $scope.CurrentSeason.CheckStatus = $scope.CurrentSeason.Status;
            });
        }

        $scope.switchTestMode = function () {
            $http.post(baseUrl + '/SwitchTestMode', { seasonDomain: $scope.MC.stateParams.seasonDomain })
            .success(function (data, status, headers, config) {
                if (data.Status) {
                    $scope.CurrentSeason.Status = 'Test';
                    $scope.MC.getSeasons();
                     $state.go('Seasons');
                    jbToast.success('Season sets test mode successfully.');
                }
                else {
                    $scope.MC.handleErrors($scope, data);
                }

                $scope.CurrentSeason.CheckStatus = $scope.CurrentSeason.Status;
            });
        }

        /* season overview */

        $http.get(baseUrl + "/GetTotalSales", { params: { seasonDomain: $scope.MC.stateParams.seasonDomain } })
                .success(function (data, status, headers, config) {
                    $scope.Model.Totals = data;

                    var totalSaleOptions = {
                        useEasing: true,
                        useGrouping: true,
                        separator: ',',
                        decimal: '.',
                        prefix: '$',
                        suffix: ''
                    };
                    var totalSaleCountUp = new countUp("totalSale", 0, $scope.Model.Totals.TotalSales, 2, 2.5, totalSaleOptions);
                    totalSaleCountUp.start();

                    var totalAttendeesOptions = {
                        useEasing: true,
                        useGrouping: true,
                        separator: ',',
                        decimal: '.',
                        prefix: '',
                        suffix: ''
                    };
                    var totalAttendeesCountUp = new countUp("totalAttendees", 0, $scope.Model.Totals.TotalAttendees, 0, 2.5, totalAttendeesOptions);
                    totalAttendeesCountUp.start();
                });

        $scope.SearchTypes = [{ Text: 'Name', Value: 'Name' }, { Text: 'Email', Value: 'Email' }, { Text: 'Confirmation ID', Value: 'ConfirmationId' }];
        $scope.SearchType = 'Name';

        var seasonRecentRegistrationDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetSeasonRecentRegistration?seasonDomain=' + $scope.MC.stateParams.seasonDomain,
                        cache: false
                    },
                    parameterMap: function (data, type) {
                        if (type === "read") {
                            return {
                                term: $scope.searchTerm,
                                searchType: $scope.SearchType,
                                firstName: $scope.firstName,
                                lastName: $scope.lastName,
                                skip: data.skip,
                                take: data.take,
                                page: data.page,
                                pageSize: data.pageSize,
                                sort: data.sort
                            }
                        }
                    }
                },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverFiltering: true,
            serverSorting: true,
            serverPaging: true,
            pageSize: 10,
        });

        $scope.seasonRecentRegistration = {
            dataSource: seasonRecentRegistrationDataSource,
            height: 306,
            scrollable: {
                virtual: true
            },
            sortable: true,
            columns: [
                {
                    field: "AttendeeName",
                    title: "Participant",
                    width: "120px",
                    template: "#= AttendeeName # #if(ItemStatusReason == 1) {# <i class='grid-label label info'>Transferred in</i> #}else if(ItemStatusReason == 2) {# <i class='grid-label label bg-red  fg-white'>Transferred out</i> #}else if(ItemStatusReason == 5) {# <i class='grid-label label bg-red fg-white'>Refund</i> #} else if (ItemStatusReason == 4){# <i class='grid-label label warning'>Canceled</i> #}#",
                    sortable: false
                }, {
                    field: "ProgramName",
                    title: "Program",
                    width: "120px",
                    sortable: false
                },
                {
                    field: "Schedule",
                    title: "Schedule",
                    width: "124px",
                    sortable: false
                },
                 {
                     field: "TuitionName",
                     title: "Tuition label",
                     width: "120px",
                     sortable: false
                 },
                {
                    field: "DateTimeLable",
                    title: "Date",
                    template: "#= (kendo.toString( kendo.parseDate(DateTimeLable) , 'MMM/dd/yyyy')) == null ? DateTimeLable : kendo.toString( kendo.parseDate(DateTimeLable) , 'MMM/dd/yyyy') #",
                    width: "120px",
                    sortable: false
                }, {
                    field: "ConfirmationId",
                    title: "Confirmation",
                    width: "130px",
                    sortable: false
                },
                {
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\<li> ... \
				            <ul>" +
                            "<li ngaction='OrderDetail_View' ui_sref='OrderItem({ seasonDomain:\"#= SeasonDomain #\",orderItemId:#= OrderItemId # })' class='k-item k-state-default k-first' role='menuitem'>\
						                <span class='k-link jbi-angle-right'>View detail</span>\
				                    </li></ul>\
			                    </li>\
		                    </ul>",
                    attributes: {
                        class: "grid-actions"
                    },
                    sortable: false
                }
            ]
        };

        /* Capacity bar chart */
        $scope.dataSourceBarChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetCapacityBarChartReport",
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            seasonDomain: $scope.MC.stateParams.seasonDomain,
                        }
                    }
                }
            },
            sort: {
                field: "Date",
                dir: "asc"
            }
        });

        $scope.dataSourcePieChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetSalesPieChartReport",
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            seasonDomain: $scope.MC.stateParams.seasonDomain,
                        }
                    }
                }
            },
            sort: {
                field: "Price",
                dir: "asc"
            }
        });

        $scope.dataSourceLinearChart = new kendo.data.DataSource({
            transport: {
                read: {
                    url: baseUrl + "/GetSaleOverTimeChartReport",
                    cache: false,
                    dataType: "json"
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            seasonDomain: $scope.MC.stateParams.seasonDomain,
                        }
                    }
                }
            },
            sort: {
                field: "SortDate",
                dir: "asc"
            }
        });

        /* ============== */
        $scope.search = function () {
            $("#grid").data("kendoGrid").dataSource.read();
        };

        $scope.barChartOption = {
            dataSource: $scope.dataSourceBarChart,
            legend: {
                visible: false
            },
            chartArea: {
                background: "",
                width: 260,
                height: 210
            },
            seriesDefaults: {
                //type: "bar",
                stack: true
            },
            seriesColors: colors,
            series: [{
                field: "FillSpot",
                name: "filled spots",
                overlay: {
                    gradient: "none"
                },
                color: function (point) {
                    return colors[0];
                }
            }, {
                field: "OpenSpot",
                name: "open spots",
                overlay: {
                    gradient: "none"
                }
                //opacity: 0.3
            }],
            categoryAxis: {
                majorGridLines: {
                    visible: false
                },
                majorTicks: {
                    visible: false
                },
                crosshair: {
                    visible: false
                },
                justified: true
            },
            valueAxis: {
                majorUnit: 10,
                line: {
                    visible: false
                },
                visible: false,
            },
            tooltip: {
                visible: true,
                template: function (point) {
                    if (point.dataItem.IsFull) {
                        return point.dataItem.ProgramFullName + "(" + point.series.name + ":" + point.value + ") " + "program is full.";
                    } else {
                        return point.dataItem.ProgramFullName + " (" + point.series.name + ":" + point.value + ")";
                    }
                },
            }
        };

        $scope.pieChartOption = {
            dataSource: $scope.dataSourcePieChart,
            legend: {
                visible: false
            },
            chartArea: {
                background: "",
                width: 200,
                height: 200
            },
            seriesDefaults: {
                type: "pie",
                overlay: {
                    gradient: "none"
                },
            },
            seriesColors: colors,
            series: [{
                field: "Price",
                categoryField: "ProgramName",
                padding: 0
            }],
            tooltip: {
                visible: true,
                format: "N0",
                template: "#= category # (#= kendo.format('{0:P}', percentage)#)"
            }
        };

        $scope.linearChartOption = {
            dataSource: $scope.dataSourceLinearChart,
            legend: {
                visible: false
            },
            chartArea: {
                background: "",
                width: 260,
                height: 240
            },
            seriesColors: colors,
            series: [{
                //type: "line",
                type: "line",
                style: "smooth",
                field: "Price",
                markers: {
                    visible: true
                },
                overlay: {
                    gradient: "none"
                },
                color: "#29506D"
            },
            {
                //type: "line",
                type: "area",
                style: "smooth",
                //style: "step",
                field: "Revenue",
                markers: {
                    visible: true
                },
                overlay: {
                    gradient: "none"
                },
                color: "#73c100"
            }
            ],
            categoryAxis: {
                majorGridLines: {
                    visible: false
                },
                majorTicks: {
                    visible: false
                },
                crosshair: {
                    visible: false
                },
                justified: true
            },
            tooltip: {
                visible: true,
                template: "#= kendo.format('{0:C}',value)# On #=dataItem.Date#"
            },
            valueAxis: {
                majorGridLines: {
                    visible: false
                },
                visible: false,
                labels: {
                    format: "$" + ("{0}")
                },
                majorUnit: 100
            }
        };

    }]);
})();