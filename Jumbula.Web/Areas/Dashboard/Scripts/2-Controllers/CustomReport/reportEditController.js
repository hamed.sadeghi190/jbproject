﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('reportEditController', ['$scope', '$http', '$log', '$stateParams', '$state', function ($scope, $http, $log, $stateParams, $state) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $http.get(baseUrl + '/CreateEditReport', { params: { id: $scope.MC.stateParams.reportId, seasonDomain: $scope.MC.stateParams.seasonDomain } })
            .success(function (data, status, headers, config) {
                $scope.Model = data;
            });

        $scope.saveReport = function () {
            $http.post(baseUrl + '/CreateEditReport', { model: $scope.Model })
                .success(function (data, status, headers, config) {
                    if (status == 200) {
                        $state.go('CustomReports', { seasonDomain: $scope.MC.stateParams.seasonDomain })
                    }
                });
        }

        $scope.checkAll = function (sectionCheckAllModel, sectionCheckModels) {
            angular.forEach(sectionCheckModels, function (item) {
                if (item.Name[0] != '%') {
                    item.Value = sectionCheckAllModel;
                }
            });
        };

    }]);
})();