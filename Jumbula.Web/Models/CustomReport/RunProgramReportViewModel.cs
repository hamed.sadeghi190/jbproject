﻿using Jb.Framework.Common.Forms;
using SportsClub.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jb.Framework.Web.Model;
using SportsClub.Domain.Generic;
using SportsClub.Domain.Report;
using SportsClub.Infrastructure;

namespace SportsClub.Models
{
    public class RunProgramReportViewModel : BaseViewModel<EventRoaster>
    {
        public string EventName { get; set; }

        public string EventTitle { get; set; }

        public List<JbForm> DataTable { get; set; }

        public JbForm JbForm { get; set; }

        public RunProgramReportViewModel()
        {
            DataTable = new List<JbForm>();
        }
        public RunProgramReportViewModel(string eventName, string eventTitle)
            : this()
        {
            EventName = eventName;
            EventTitle = eventTitle;
        }
        public RunProgramReportViewModel(string eventName, string eventTitle, List<JbForm> dataTable)
            : this(eventName, eventTitle)
        {
            DataTable = dataTable;
        }
        public RunProgramReportViewModel(string eventName, string eventTitle, EventRoaster eventRoaster, IEnumerable<ICollection<OrderItem>> orderItems, bool isFilterable, KendoCustomFilter filters)
            : this(eventName, eventTitle)
        {
            JbForm = eventRoaster.JbForm;

            var isInFilter = false;

            foreach (var section in JbForm.Elements)
                if (section is JbSection)
                    (section as JbSection).Elements.RemoveAll(e => e is JbCheckBox && !(e as JbCheckBox).Value);

            JbForm.Elements.RemoveAll(e => e is JbSection && !(e as JbSection).Elements.Any());

            var filteredOrderItems = new List<OrderItem>();

            if (isFilterable)
            {
                var filterData = orderItems.ToList();

                if (filters.RegistrationStart.HasValue && filters.RegistrationEnd.HasValue)
                {
                    filteredOrderItems =
                            filterData.SelectMany(order => order).Where(item => item.DateCreated.Date >= filters.RegistrationStart.Value.Date && item.DateCreated <= filters.RegistrationEnd.Value.Date)
                                .Select(m => m)
                                .ToList();
                    isInFilter = true;
                }
                if (filters.Tuition.HasValue && filteredOrderItems.Count > 0)
                {
                    filteredOrderItems =
                        filteredOrderItems.Select(m => m)
                            .Where(
                                item =>
                                    item.OrderChargeDiscounts.Single(m => m.Category == ChargeDiscountCategory.EntryFee)
                                        .ChargeId == filters.Tuition)
                            .ToList();
                }
                else if (filters.Tuition.HasValue && !isInFilter)
                {
                    filteredOrderItems =
                        filterData.SelectMany(item => item)
                            .Where(
                                n =>
                                    n.OrderChargeDiscounts.Single(m => m.Category == ChargeDiscountCategory.EntryFee)
                                        .ChargeId == filters.Tuition)
                            .Select(p => p)
                            .ToList();
                    isInFilter = true;
                }
                if (filters.Status.HasValue && filteredOrderItems.Count > 0)
                {
                    filteredOrderItems =
                        filteredOrderItems.Select(m => m)
                            .Where(
                                item =>
                                    item.PaymentStatus == filters.Status)
                            .ToList();
                }
                else if (filters.Status.HasValue && !isInFilter)
                {
                    filteredOrderItems =
                        filterData.SelectMany(item => item)
                            .Where(
                                n => n.PaymentStatus == filters.Status)
                            .Select(p => p)
                            .ToList();
                    isInFilter = true;
                }
            }
            else
            {
                filteredOrderItems = orderItems.SelectMany(m => m).ToList();
            }



            //foreach (var order in orderItems)
            //{
            //var player = DependencyResolverHelper.IPlayerProfileDbService.GetFromForm(order.JbForm);
            //var basicInfo = OrderMetaDataHelper.GetEventBaseInfo(order.Id);

            //{
            foreach (var item in filteredOrderItems)
            {
                if (item.JbForm != null)
                {
                    #region Just4Cantabile

                    //if (forOldEvents)
                    //{
                    //    try
                    //    {
                    //        var flag = false;
                    //        foreach (var element in order.JbForm.Elements.Where(r => r is JbSection))
                    //        {
                    //            if (element.Title == "Other parent information" || element.Title == "Parent 2" ||
                    //                element.Title == "Other parent info" || element.Title == "Other parent")
                    //            {
                    //                element.Name = "Parent2GuardianSection2";
                    //                flag = true;
                    //            }
                    //        }
                    //        if (flag)
                    //        {
                    //            order.JbForm.Elements.RemoveAll(s => s.Name == "Parent2GuardianSection");
                    //            order.JbForm.Elements.FirstOrDefault(e => e.Name == "Parent2GuardianSection2").Name =
                    //                "Parent2GuardianSection";
                    //        }
                    //        if (order.JbForm.Elements.Any(e => e.Name == "Parent job information"))
                    //        {
                    //            var parentJob =
                    //                order.JbForm.Elements.FirstOrDefault(e => e.Name == "Parent job information") as
                    //                JbSection;
                    //            if (parentJob.Elements.Any(e => e.Name == "Occupation"))
                    //                (order.JbForm.Elements.SingleOrDefault(e => e.Name == "ParentGuardianSection") as
                    //                 JbSection)
                    //                    .Elements.SingleOrDefault(e => e.Name == "Occupation")
                    //                    .SetValue(
                    //                        parentJob.Elements.FirstOrDefault(e => e.Name == "Occupation").GetValue());

                    //            if (parentJob.Elements.Any(e => e.Name == "Employer"))
                    //                (order.JbForm.Elements.SingleOrDefault(e => e.Name == "ParentGuardianSection") as
                    //                 JbSection)
                    //                    .Elements.SingleOrDefault(e => e.Name == "Employer")
                    //                    .SetValue(
                    //                        parentJob.Elements.FirstOrDefault(e => e.Name == "Employer").GetValue());
                    //        }
                    //    }
                    //    catch
                    //    {
                    //    }
                    //}

                    #endregion

                    var newForm = item.JbForm.RightCopyNewByAny(JbForm, true);
                    //var newForm = RightCopyNewByAny(item.JbForm, JbForm, true);

                    #region MoreInfo Codes

                    if (JbForm.Elements.Any(e => e.Name == "MoreInfoSection"))
                    {
                        var moreSection = new JbSection()
                        {
                            Title =
                                (JbForm.Elements.Single(s => s.Name == "MoreInfoSection") as JbSection).Title
                        };
                        foreach (
                            var element in
                                (JbForm.Elements.Single(s => s.Name == "MoreInfoSection") as JbSection).Elements
                            )
                        {
                            switch (element.Name)
                            {
                                case "Tuition":
                                    {
                                        var tuition = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = Utilities.FormatCurrencyWithPenny(item.EntryFee)
                                        };
                                        moreSection.Elements.Add(tuition);
                                        break;
                                    }
                                case "DiscountTotal":
                                    {
                                        var DiscountTotal = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = Utilities.FormatCurrencyWithPenny(item.OrderChargeDiscounts.Where(m => m.OrderItemId == item.Id).Sum(m => m.Amount))
                                        };
                                        moreSection.Elements.Add(DiscountTotal);
                                        break;
                                    }
                                //case "PaidAmount":
                                //    {
                                //        var tuition = new JbTextBox { Title = element.Title, Name = element.Name, Value = "100.00" };
                                //        moreSection.Elements.Add(tuition);
                                //        break;
                                //    }
                                case "OrderDate":
                                    {
                                        var OrderDate = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.Order.OrderDate.ToShortDateString()
                                        };
                                        moreSection.Elements.Add(OrderDate);
                                        break;
                                    }
                                case "ProgramStart":
                                    {
                                        var ProgramStart = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.StartDate.ToShortDateString()
                                        };
                                        moreSection.Elements.Add(ProgramStart);
                                        break;
                                    }
                                case "ProgramEnd":
                                    {
                                        var ProgramEnd = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.EndDate.ToShortDateString()
                                        };
                                        moreSection.Elements.Add(ProgramEnd);
                                        break;
                                    }
                                case "ProgramLocation":
                                    {
                                        var ProgramLocation = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.ProgramSchedule.Program.ClubLocation.PostalAddress.Address
                                        };
                                        moreSection.Elements.Add(ProgramLocation);
                                        break;
                                    }
                                case "ProgramType":
                                    {
                                        var ProgramType = new JbTextBox
                                        {
                                            Title = element.Title,
                                            Name = element.Name,
                                            Value = item.ProgramTypeCategory.ToDescription()
                                        };
                                        moreSection.Elements.Add(ProgramType);
                                        break;
                                    }
                                default:
                                    break;
                            }

                        }
                        newForm.Elements.Add(moreSection);
                    }
                    newForm.CurrentMode = AccessMode.ReadOnly;
                    DataTable.Add(newForm);

                    #endregion
                }
            }
            //}
            //}

        }
    }
}