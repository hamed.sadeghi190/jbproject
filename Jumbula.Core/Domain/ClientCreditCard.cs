﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{

    public class ClientCreditCard : BaseEntity<long>
    {
        public ClientCreditCard()
        {
            IsDefault = false;
            IsDeleted = false;
        }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual JbUser User { get; set; }
        [StringLength(128)]
        public string StripeCustomerId { get; set; }
        [StringLength(4)]
        public string LastDigits { get; set; }
        public long ClientId { get; set; }
        public string CardHolderName { get; set; }
        
        [StringLength(4)]
        public string ExpiryYear { get; set; }
        [StringLength(2)]
        public string ExpiryMonth { get; set; }
        [StringLength(20)]
        public string Brand { get; set; }
        public string StripeCustomerResponse { get; set; }
        public virtual PostalAddress Address { get; set; }

        public bool IsDefault { get; set; }

        public bool IsDeleted { get; set; }
        [ForeignKey("ClientId")]
        public virtual Client Client { get; set; }

      public virtual ICollection<BillingTransactionActivity> TransactionActivities { get; set; }

    }

   
}
