﻿namespace Jumbula.Core.Model.People
{
    public class DelinquentListViewModel
    {
        public long RelatedEntityId { get; set; }
        public string RelatedEntity { get; set; }
        public string Email { get; set; }
        public string Participant { get; set; }
        public string Program { get; set; }
        public string Season { get; set; }
        public string Confirmation { get; set; }
        public string TotalAmount { get; set; }
        public string PaidAmount { get; set; }
        public string Balance { get; set; }
        public string Policy { get; set; }
        public string Attempts { get; set; }
        public string Status { get; set; }
    }
}
