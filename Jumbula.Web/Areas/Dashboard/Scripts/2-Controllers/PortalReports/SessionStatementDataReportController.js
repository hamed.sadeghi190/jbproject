﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('SessionStatementDataReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};
        $scope.filter = {};
        var isBegin = false;

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.SeasonNames = [{ Value: "Fall", Title: "Fall" }, { Value: "Winter", Title: "Winter" }, { Value: "Spring", Title: "Spring" }, { Value: "Summer", Title: "Summer" }];
        $scope.filter.SeasonName = "Fall";
        $scope.filter.Year = 0;

        $http.get(baseUrl + '/GetSessionStatementDataReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.Model.Years = data.Years;
            $scope.filter.Year = data.Years[0].Text;
            dataInfo = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });



        var manipulateSessionStatementData = function () {

            isBegin = true;

            $scope.SessionStatementData_DataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetSessionStatementDataReportInfo",
                        dataType: "json",
                        type: "POST",
                        data: function () {
                            return {
                                seasonName: $scope.filter.SeasonName,
                                year: $scope.filter.Year,
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.SessionStatementDataGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Session statement data).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: $scope.filter.SeasonName + " " + $scope.filter.Year
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Session statement data - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.SessionStatementData_DataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#SessionStatementDataGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "SeasonName",
                        title: "Season name",
                        width: "180px"
                    }, {
                        field: "SeasonRosterDueDate",
                        title: "Roster due date",
                        width: "170px",
                    }, {
                        field: "SeasonPaymentsIssuedDate",
                        title: "Payments issued date",
                        width: "180px"
                    }, {
                        field: "ProgramName",
                        title: "Program name",
                        width: "160px",
                    }, {
                        field: "ProviderName",
                        title: "Provider name",
                        width: "170px",
                    }, {
                        field: "Cost",
                        title: "Cost",
                        width: "100px",
                        format: "{0:c2}",
                    }, {
                        field: "TotalOfRegistrations",
                        title: "Total # of registrations",
                        width: "260px",
                    }, {
                        field: "TotalOfProviderFunded",
                        title: "Total # of provider funded",
                        width: "260px",
                    }, {
                        field: "ProviderAddress",
                        title: "Provider address",
                        width: "240px",
                        template: '<span style="white-space: pre-line" > #=ProviderAddress# </span>'
                    }, {
                        field: "ProviderContactEmail",
                        title: "Provider contact email",
                        width: "180px",
                    }, {
                        field: "ProviderContactName",
                        title: "Provider contact name",
                        width: "180px",
                    }, {
                        field: "EMRate",
                        title: "EM rate",
                        width: "80px",
                    }
                ]
            }
        }


        $scope.saveToExcel = function () {
            if ($("#SessionStatementDataGrid").data("kendoGrid") != null) {
                cfpLoadingBar.start();
                $("#SessionStatementDataGrid").getKendoGrid().saveAsExcel();
            }
        }

        $scope.search = function () {
            if (!isBegin) {
                manipulateSessionStatementData();
            } else {
                $("#SessionStatementDataGrid").data("kendoGrid").dataSource.read();
            }
        };

        $scope.clearFilters = function () {
            $scope.filter.SeasonName = "Fall";
            $scope.filter.Year = $scope.Model.Years[0].Text;
            $("#SessionStatementDataGrid").data("kendoGrid").dataSource.read();
        }

    }
    ]);

})();