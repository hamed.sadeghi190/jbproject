﻿

using System.ComponentModel;

namespace Jumbula.Common.Enums
{
    public enum Genders
    {
        [Description("No restriction")]
        NoRestriction,

        [Description("Male")]
        Male,

        [Description("Female")]
        Female,
    }
}
