﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.ActionResults;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.UserService;
using Newtonsoft.Json;
using SportsClub.Areas.Dashboard.Models;
using SportsClub.Models;
using SportsClub.Models.Home;
using Stripe;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class AccountSettingController : DashboardBaseController
    {

        public virtual ActionResult AccountOverview()
        {
            return View();
        }
        public virtual ActionResult BillingReceipt()
        {
            return View();
        }
        public virtual ActionResult AccountInsurance()
        {
            return View();
        }
        public virtual ActionResult AccountPlan()
        {
            return View();
        }
        public virtual ActionResult Subscription()
        {
            return View();
        }
        public virtual ActionResult AccountBilling()
        {
            return View();
        }
        public virtual ActionResult AccountBillingList()
        {
            return View();
        }
        public virtual ActionResult AccountBillingHistory()
        {
            return View();
        }

        [JbAuthorize(JbAction.AccountSetting_Billing_View)]
        public virtual JsonNetResult GetAllBillingInfo(int skip, int take, int page, int pageSize)
        {
            var model = new List<AccountBillingViewModel>();
            var total = 0;
            try
            {
                var client = Ioc.ClientBusiness.Get(ActiveClub.Id);


                model = client.CreditCards.Where(c => c.IsDeleted == false).Select(c => new AccountBillingViewModel()
                {
                    CardHolderName = c.CardHolderName,
                    Last4Digit = c.LastDigits,
                    Id = c.Id,
                    Year = c.ExpiryYear,
                    Month = c.ExpiryMonth.Length == 1 ? ("0" + c.ExpiryMonth) : c.ExpiryMonth,
                    Brand = c.Brand,
                    IsDefault = c.IsDefault,
                    Address = c.Address.Address,
                    ClientId = c.ClientId
                }).ToList();

                total = model.Count;
                model = model.Skip(skip).Take(take).ToList();
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return JsonNet(new { DataSource = model, TotalCount = total });
        }

        [HttpPost]
        [JbAuthorize(JbAction.AccountSetting_Billing_History_View)]
        public virtual ActionResult GetAllBillingHistories(int skip, int take, int page, int pageSize, bool printMode)
        {
            var model = new List<AccountBillingHistoryViewModel>();
            var total = 0;
            try
            {
                var client = Ioc.ClientBusiness.Get(ActiveClub.Id);

                decimal balance = 0;
                foreach (var item in client.BillingHistories)
                {
                    balance = balance + item.Debit - item.Credit;
                    model.Add(new AccountBillingHistoryViewModel()
                    {
                        BillingDate = (item.TransactionActivities != null && item.TransactionActivities.Any() && item.Credit > 0) ? item.TransactionActivities.Last().TransactionDate : item.BillingDate,
                        BillingId = item.BillingId,
                        Credit = item.Credit,
                        Debit = item.Debit,
                        Balance = balance,
                        Description = item.Description,
                        Id = item.Id

                    });
                }

                total = model.Count;

                if (printMode)
                {
                    var clubId = ActiveClub.Id;
                    dynamic models = new System.Dynamic.ExpandoObject();
                    models.Date = Ioc.ClubBusiness.GetClubDateTime(clubId, DateTime.UtcNow).ToString("MMM dd, yyyy hh:mm tt");//Sep 21, 2016 HH:MM
                    models.Data = model;
                    var ClubName = ActiveClub.Name;
                    models.ClubName = ClubName;

                    return Pdf("Billing History", "_AccountBillingHistoryReportPdfExportView", models);
                }

            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return JsonNet(new { DataSource = model.Skip(skip).Take(take).ToList(), TotalCount = total });
        }

        public virtual JsonNetResult GetBillingHistoryDetail(long billingId)
        {
            var billing = Ioc.ClientBusiness.GetClientBillingHistory(billingId);

            var model = new
            {
                Amount = CurrencyHelper.FormatCurrencyWithPenny(billing.Credit, CurrencyCodes.USD),
                Description = billing.Description,
                BillingPeriod = billing.BillingDate.ToString(Constants.DateTime_Comma) + " - " + billing.BillingDate.AddMonths(1).ToString(Constants.DateTime_Comma),
                TransactionDate = billing.TransactionActivities.First().TransactionDate.ToString(Constants.DefaultDateFormat),
                CreditCardNumber = billing.TransactionActivities.First().CreditCard.Brand + "  ***" + billing.TransactionActivities.First().CreditCard.LastDigits,
                BillingId = billing.BillingId

            };
            return JsonNet(model, new JsonSerializerSettings());
        }

        [HttpGet]
        [JbAuthorize(JbAction.Account_Overview_View)]
        public virtual JsonNetResult GetAccountOverview()
        {
            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
            var model = new
            {
                MemberSince = club.MetaData.DateCreated.ToString(Constants.DefaultDateFormat),
                CurrentPlan = club.Client.PricePlan.Type.ToDescription(),
                AccountStatus = "Active",
                MonthlyCharge = CurrencyHelper.FormatCurrencyWithPenny(club.Client.PricePlan.MonthlyCharges, club.Currency),
                TransactionRate = club.Client.PricePlan.TransactionFee + "% ",
                IsFreeTrialAccount = club.Client.PricePlan.Type == PricePlanType.FreeTrial,
                PlanMessage = Ioc.ClubBusiness.GetAccountOverviewMessage(club.Client.PricePlan, club.MetaData.DateCreated, club.Currency)
            };
            return JsonNet(model);
        }

        [HttpGet]
        [JbAuthorize(JbAction.Account_Subscription_View)]
        public virtual JsonNetResult GetAllSubscriptions(int skip, int take, int page, int pageSize)
        {
            var termValue = Request.Params["term"] ?? string.Empty;
            var model = new List<ClubRevenue>();
            var total = 0;
            try
            {
                var query = Ioc.PricePlanBusiness.GetAllRevenues();
                total = query.Count();
                var term = 0;
                if (termValue != null && int.TryParse(termValue, out term))
                {
                    query = query.Where(c => c.Minimum <= term && c.Maximum >= term);
                }
                model = query.ToList().Skip(skip).Take(take).ToList();
            }

            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return JsonNet(new { DataSource = model, TotalCount = total });
        }
        [HttpPost]
        [JbAuthorize(JbAction.AccountSetting_Billing_Delete)]
        public virtual JsonResult DeleteCard(long cardId)
        {
            try
            {
                OperationStatus res = new OperationStatus() { Status = false, Message = "Please select the credit card" };
                var card = Ioc.ClientCreditCardBusiness.Get(cardId);
                if (card != null)
                {
                    if (card.IsDefault)
                    {
                        return Json(new JResult { Status = false, Message = " You cannot remove an active credit card", RecordsAffected = res.RecordsAffected });
                    }
                    if (string.IsNullOrEmpty(card.StripeCustomerId))
                    {
                        var stripeService = new JbStripeService(ActiveClub.Domain, false);
                        stripeService.DeleteCustomer(card.StripeCustomerId);
                    }
                    res = Ioc.ClientCreditCardBusiness.Delete(cardId);
                }
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        [HttpPost]
        [JbAuthorize(JbAction.AccountSetting_Billing_Activate)]
        public virtual JsonResult ActivateCard(long cardId, long clientId)
        {
            try
            {
                OperationStatus res = new OperationStatus() { Status = false, Message = "Please select a credit card" };
                if (cardId > 0)
                {
                    res = Ioc.ClientBusiness.ActivateClientCreditCard(clientId, cardId);
                }

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        [HttpGet]
        [JbAuthorize(JbAction.AccountSetting_Insurance_View)]
        public virtual JsonNetResult EditAccountSetting()
        {
            var model = new AccountInsuranceViewModel();

            var club = Ioc.ClubBusiness.Get(base.ActiveClub.Id);

            if (club.Setting != null && club.Setting.CertificateOfInsurances != null)
            {
                model.PolicyExpirationDate = club.Setting.CertificateOfInsurances.PolicyExpirationDate;
                if (club.Setting.CertificateOfInsurances.CertificateList != null && club.Setting.CertificateOfInsurances.CertificateList.Any())
                {
                    foreach (var certificate in club.Setting.CertificateOfInsurances.CertificateList)
                    {


                        var modelCerti = new InsuranceCertificateViewModel()
                        {
                            SelectedCertificate = ((int)certificate.CertificateOfInsuranceType),
                            CertificateFileNames = certificate.CertificateFileName,
                        };
                        if (modelCerti.CertificateFileNames != null && modelCerti.CertificateFileNames.Any())
                        {
                            modelCerti.CertificateFileURLs = new List<string>();
                            foreach (var filename in modelCerti.CertificateFileNames)
                            {

                                modelCerti.CertificateFileURLs.Add(StorageManager.GetUriByPath(Constants.Path_ClubFilesContainer + "/" + ActiveClub.Domain + Constants.Path_InsuranceForm + "//" + filename).AbsoluteUri);
                            }
                        }
                        model.Certificates.Add(modelCerti);

                    }
                }


            }
            return JsonNet(model);
        }

        [HttpPost]
        [JbAuthorize(JbAction.AccountSetting_Insurance_Save)]
        public virtual ActionResult EditAccountSetting(AccountInsuranceViewModel model)
        {
            try
            {
                if (model.Certificates.Any() && !model.Certificates.Any(c => c.SelectedCertificate > 0))
                {
                    return Json(new JResult { Status = false, Message = "Insurance type is required.", RecordsAffected = 0 });
                }
                AccountSettingValidation(ModelState, model);
                if (ModelState.IsValid)
                {
                    var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

                    if (model.Certificates != null && model.Certificates.Any(c => c.SelectedCertificate > 0))
                    {
                        if (club.Setting.CertificateOfInsurances == null)
                        {
                            club.Setting.CertificateOfInsurances = new CertificateOfInsurances();
                        }
                        else if (club.Setting.CertificateOfInsurances.CertificateList != null && club.Setting.CertificateOfInsurances.CertificateList.Any())
                        {
                            club.Setting.CertificateOfInsurances.CertificateList.Clear();
                        }
                        club.Setting.CertificateOfInsurances.PolicyExpirationDate = model.PolicyExpirationDate;

                        foreach (var item in model.Certificates.Where(c => c.SelectedCertificate > 0))
                        {
                            var newCerti = new Certificate();

                            newCerti.CertificateOfInsuranceType = (CertificateOfInsuranceTypes)(item.SelectedCertificate);

                            newCerti.CertificateFileName.AddRange(item.CertificateFileNames);

                            club.Setting.CertificateOfInsurances.CertificateList.Add(newCerti);
                        }
                    }
                    else
                    {
                        if (club.Setting.CertificateOfInsurances != null)
                        {
                            club.Setting.CertificateOfInsurances = null;
                        }
                    }
                    club.IsSettingChanged = true;

                    var res = Ioc.ClubBusiness.Update(club);
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });

                }

                return base.JsonFormResponse();
            }
            catch (Exception ex)
            {
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }



        [HttpPost]
        [JbAuthorize(JbAction.AccountSetting_ChangePlan_Save)]
        public virtual JsonResult SendEnterpriseEmail()
        {


            var clubDb = Ioc.ClubBusiness;
            var club = clubDb.Get(ActiveClub.Id);
            var model = new EnterpriseViewModel()
            {
                BizName = club.Name,
                BizWeb = club.Domain,
                Address = club.ClubLocations.First().PostalAddress.Address,
                FullName = club.ContactPersons.First().FullName,
                Message = "Enterprise plan",
                Phone = club.ContactPersons.First().Phone,
                Email = string.IsNullOrEmpty(club.ContactPersons.First().Email) ? JbUserService.GetCurrentUserName() : club.ContactPersons.First().Email,
                Revenue = club.ClubRevenue.Description,
                TimeZone = club.TimeZone
            };
            EmailService.Get(this.ControllerContext).SendEnterpriseEmailToSupport(model);
            if (club.Client != null && club.Client.CreditCards != null && club.Client.CreditCards.Any())
            {
                return Json(new JResult { Status = true, Message = "", RecordsAffected = 0 });
            }
            else
            {
                return Json(new JResult { Status = true, Message = "NeedCredit", RecordsAffected = 0 });
            }
        }

        [HttpPost]
        [JbAuthorize(JbAction.AccountSetting_ChangePlan_Save)]
        public virtual JsonResult ChangePlan()
        {

            var priceplan = Ioc.PricePlanBusiness.GetPayAsYouGoPlan();

            if (priceplan == null)
            {
                return Json(new JResult { Status = false, Message = "Selected plan is invalid", RecordsAffected = 0 });
            }
            var clubDb = Ioc.ClubBusiness;
            var club = clubDb.Get(ActiveClub.Id);
            if (club.Client != null && club.Client.CreditCards != null && club.Client.CreditCards.Any())
            {

                club.Client.PricePlanId = priceplan.Id;
                club.Client.PricePlan = priceplan;
                var res = clubDb.Update(club);
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = 0 });
            }
            else
            {
                return Json(new JResult { Status = true, Message = "NeedCredit", RecordsAffected = 0 });
            }
        }
        [HttpGet]
        [JbAuthorize(JbAction.AccountSetting_Billing_Add)]
        public virtual JsonNetResult CreateEditClientCard(string planType, long id = 0)
        {
            var model = new AccountBillingViewModel();

            if (id > 0)
            {
                var clientCard = Ioc.ClientCreditCardBusiness.Get(id);
                if (clientCard != null)
                {
                    model = clientCard.ToViewModel<AccountBillingViewModel>();
                    model.Year = clientCard.ExpiryYear;
                    model.Year2Char = int.Parse(clientCard.ExpiryYear) - 2000;
                    model.Month = clientCard.ExpiryMonth.Length == 1 ? "0" + clientCard.ExpiryMonth : clientCard.ExpiryMonth;
                    model.Address = clientCard.Address.Address;
                    model.IsDefault = clientCard.IsDefault;
                    model.Last4Digit = clientCard.LastDigits;
                }
            }
            PricePlanType outPlan = PricePlanType.PayAsYouGo;
            if (!Enum.TryParse<PricePlanType>(planType, out outPlan))
            {
                model.PlanType = planType;
            }
            else
            {
                model.PlanType = outPlan.ToString();
            }
            return JsonNet(model, new JsonSerializerSettings());
        }

        [HttpPost]
        [JbAuthorize(JbAction.AccountSetting_Billing_Save)]
        public virtual ActionResult CreateEditClientCard(AccountBillingViewModel model)
        {
            try
            {
                var address = new PostalAddress();
                CheckClientCardValidation(ModelState, model, ref address);
                if (ModelState.IsValid)
                {
                    model.Year = (2000 + model.Year2Char).ToString();
                    var isDefault = model.IsDefault;
                    var clientDb = Ioc.ClientBusiness;
                    var client = clientDb.Get(ActiveClub.Id);
                    if (!(client.CreditCards != null && client.CreditCards.Any()))
                    {
                        isDefault = true;
                    }

                    Stripe.StripeCustomer stripeCustomer = null;
                    var stripeService = new JbStripeService(ActiveClub.Domain, false);
                    ClientCreditCard clientCard = null;

                    if (model.Id > 0)
                    {
                        clientCard = client.CreditCards.Single(c => c.Id == model.Id);
                        if (clientCard != null)
                        {
                            stripeCustomer = stripeService.UpdateStripeCustomerForClient(model, address, clientCard.StripeCustomerId);
                        }
                    }
                    else
                    {

                        stripeCustomer = stripeService.CreateStripeCustomerForClient(model, address);
                        clientCard = new ClientCreditCard();
                    }


                    var res = new OperationStatus() { Status = false };


                    var cardService = new StripeCardService(WebConfigHelper.Stripe_SecretKey);
                    var card = cardService.Get(stripeCustomer.Id, stripeCustomer.DefaultSourceId);

                    clientCard.LastDigits = card.Last4;
                    clientCard.ExpiryYear = card.ExpirationYear.ToString(); ;
                    clientCard.ExpiryMonth = card.ExpirationMonth.ToString().Length == 1 ? ("0" + card.ExpirationMonth.ToString()) : card.ExpirationMonth.ToString();
                    clientCard.Address = address;
                    clientCard.Brand = card.Brand;
                    clientCard.CardHolderName = card.Name;
                    clientCard.StripeCustomerId = stripeCustomer.Id;
                    clientCard.StripeCustomerResponse = JsonConvert.SerializeObject(res);
                    clientCard.Id = model.Id;
                    clientCard.ClientId = client.Id;
                    clientCard.UserId = JbUserService.GetCurrentUserId();

                    if (isDefault && client.CreditCards != null && client.CreditCards.Any())
                    {
                        foreach (var item in client.CreditCards.Where(c => c.IsDefault))
                        {
                            item.IsDefault = false;
                        }
                    }
                    clientCard.IsDefault = isDefault;
                    if (model.Id == 0)
                    {
                        client.CreditCards.Add(clientCard);
                    }

                    res = clientDb.Update(client);
                    if (!model.PlanType.ToLower().Equals("edit") && res.Status && !model.PlanType.ToLower().Equals("add"))
                    {
                        PricePlanType outPlan = PricePlanType.PayAsYouGo;
                        if (Enum.TryParse<PricePlanType>(model.PlanType, out outPlan))
                        {
                            PricePlan pricePlan = null;
                            if (outPlan == PricePlanType.PayAsYouGo)
                            {
                                pricePlan = Ioc.PricePlanBusiness.GetPayAsYouGoPlan();


                            }
                            else if (outPlan == PricePlanType.Subscription)
                            {
                                var club = Ioc.ClubBusiness.Get(ActiveClub.Id);
                                if (club.ClubRevenueId.HasValue)
                                {
                                    pricePlan = Ioc.PricePlanBusiness.GetSubscriptionPlan(club.ClubRevenueId.Value);
                                }

                            }
                            if (pricePlan != null)
                            {
                                client.PricePlanId = pricePlan.Id;
                                client.PricePlan = pricePlan;
                                res = Ioc.ClientBusiness.Update(client);
                            }

                        }
                    }
                    return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
                }

                return JsonFormResponse();
            }
            catch (Exception ex)
            {
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        [HttpPost]
        [JbAuthorize(JbAction.Account_Subscription_Save)]
        public virtual ActionResult SaveClubSubscription(int revenueId, bool isNonProfit = false)
        {
            var res = new OperationStatus() { Status = false, Message = "Please select your annual revenue" };
            if (revenueId > 0)
            {
                var revenue = Ioc.PricePlanBusiness.GetAllRevenues().FirstOrDefault(c => c.Id == revenueId);
                if (revenue != null)
                {
                    var clubDb = Ioc.ClubBusiness;
                    var club = clubDb.Get(ActiveClub.Id);
                    club.ClubRevenueId = revenueId;
                    club.ClubRevenue = revenue;
                    club.IsNonProfit = isNonProfit;

                    if (club.Client != null && club.Client.CreditCards != null && club.Client.CreditCards.Any())
                    {
                        var pricePlan = Ioc.PricePlanBusiness.GetSubscriptionPlan(revenueId);
                        club.Client.PricePlanId = pricePlan.Id;
                        club.Client.PricePlan = pricePlan;
                        res = clubDb.Update(club);
                        if (res.Status)
                        {
                            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = 0 });
                        }

                    }
                    else
                    {
                        res = clubDb.Update(club);
                        if (res.Status)
                        {
                            return Json(new JResult { Status = true, Message = "NeedCredit", RecordsAffected = 0 });
                        }
                    }

                }


            }

            return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
        }

        private void CheckClientCardValidation(ModelStateDictionary modelState, AccountBillingViewModel model, ref PostalAddress address)
        {

            if (!string.IsNullOrEmpty(model.Address))
            {
                try
                {
                    GoogleMap.GetGeo(model.Address, ref address);
                }
                catch
                {
                    ModelState.AddModelError("Address", "Address is not valid.");
                }
            }

            if (string.IsNullOrEmpty(model.CVV))
            {

                ModelState.AddModelError("CVV", "CVC is required.");
            }
            else if (model.CVV.Length < 3 || model.CVV.Length > 4)
            {
                ModelState.AddModelError("CVV", "CVC is invalid.");
            }

            if (model.Year2Char <= 0)
            {
                ModelState.AddModelError("Month", "Expiration date is required.");
            }
            else
            {
                var month = 0;
                if (int.TryParse(model.Month, out month))
                {

                    if ((model.Year2Char + 2000) == DateTime.UtcNow.Year && month < DateTime.UtcNow.Month)
                    {
                        ModelState.AddModelError("Month", "Expiration date is invalid.");
                    }
                }
            }
            if (model.Id > 0)
            {

                ModelState.Remove("model.CardNumber");
            }
        }

        private void AccountSettingValidation(ModelStateDictionary modelState, AccountInsuranceViewModel model)
        {
            if (model.Certificates != null && model.Certificates.Any() && model.Certificates.Where(c => c.SelectedCertificate > 0).Any())
            {
                for (int i = 0; i < model.Certificates.Count; i++)
                {
                    if (model.Certificates[i].SelectedCertificate <= 0)
                    {
                        if (!(model.Certificates[i].CertificateFileNames == null || model.Certificates[i].CertificateFileNames.Count == 0))
                        {
                            modelState.AddModelError("model.Certificates[" + i.ToString() + "].SelectedCertificate", "Insurance type is required.");
                        }
                    }
                    else if (model.Certificates[i].CertificateFileNames == null || model.Certificates[i].CertificateFileNames.Count == 0)
                    {
                        modelState.AddModelError("model.Certificates[" + i.ToString() + "].SelectedFiles", "Upload your certificate of insurance.");

                    }


                }

                if (model.Certificates != null && model.CertificateTypes.Any())
                {
                    if (model.PolicyExpirationDate == null)
                    {
                        modelState.AddModelError("model.PolicyExpirationDate", "Policy expiration date is required.");
                    }
                }
            }
        }


    }
}