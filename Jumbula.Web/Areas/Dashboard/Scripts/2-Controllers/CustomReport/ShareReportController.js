﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ShareReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'report-add';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        $scope.reportName = $scope.MC.stateParams.reportName;

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.previousModel = customReportService.getModelFromStrorage();

        $http.get(baseUrl + '/FillShareModel?reportName='+ $scope.reportName)
            .success(function (data, status, headers, config) {
                $scope.Model = data;
            });

        $scope.sendReport = function () {
            if ($scope.MC.stateParams.reportName == 'Finance' || $scope.MC.stateParams.reportName == 'Coupon') {
                $http.post(baseUrl + '/ShareProgramLessReports', { type: $scope.MC.stateParams.reportName, model: $scope.Model, seasonDomain: $scope.MC.stateParams.seasonDomain, filters: $scope.previousModel.Filters, term: $scope.previousModel.Filters.Term })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            jbToast.success("Report sent successfully.");
                            $scope.back();
                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            } else if ($scope.MC.stateParams.reportName == 'RoomAssigment') {
                $http.post('/Dashboard/portalReport/GetRoomAssignments', { model: $scope.Model, seasonId: $scope.MC.stateParams.seasonId, isShare: true })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            jbToast.success("Report sent successfully.");
                            $scope.back();
                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            } else if ($scope.MC.stateParams.reportName == 'ProviderContactInfo') {
                $http.post('/Dashboard/portalReport/GetProviderContactInfo', { model: $scope.Model, seasonId: $scope.MC.stateParams.seasonId, isShare: true })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            jbToast.success("Report sent successfully.");
                            $scope.back();
                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            } else if ($scope.MC.stateParams.reportName == 'ProviderContactInformation') {
                $http.post('/Dashboard/CustomReport/GetProviderContactInformationReport', { model: $scope.Model, seasonId: $scope.MC.stateParams.seasonId, isShare: true })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            jbToast.success("Report sent successfully.");
                            $scope.back();
                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            } else if ($scope.MC.stateParams.reportName == 'CustomParentPortal') {
                $http.post('/Dashboard/PortalReport/ShareReport', { type: $scope.MC.stateParams.reportName, model: $scope.Model, selectedModel: $scope.previousModel, term: "", automaticType: "" })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            jbToast.success("Report sent successfully.");
                            $scope.back();
                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            } else if ($scope.MC.stateParams.reportName == 'CustomParent') {
                $http.post('/Dashboard/CustomReport/ShareReport', { type: $scope.MC.stateParams.reportName, model: $scope.Model, selectedModel: $scope.previousModel, term: "", automaticType: "" })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            jbToast.success("Report sent successfully.");
                            $scope.back();
                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            } else if ($scope.MC.stateParams.reportName == 'TransactionSubscriptionFinance') {

                $http.post('/Dashboard/CustomReport/CheckIfShareModelIsValid', { model: $scope.Model })
                    .success(function (data, status, headers, config) {

                        if (data.Status) {
                            $http.post('/Dashboard/PortalReport/GeneralReportSubscriptionRun',
                                {
                                    seasonDomain: '',
                                    filters: $scope.previousModel.Filters,
                                    memberId: $scope.previousModel.MemberId,
                                    isGroupBy: $scope.previousModel.Filters.TransactionFilters.IsGroupBy,
                                    isExcelExport: true,
                                    isShare: true,
                                    shareModel: $scope.Model
                                });

                            jbToast.info("We'll send an email to " + $scope.Model.SentTo + " as soon as your export is ready to download.");
                            setTimeout(function () {
                                cfpLoadingBar.complete();
                                $state.go('FinanceReports');
                            }, 3000);

                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
               

            } else {
                $http.post(baseUrl + '/ShareReport', { type: $scope.MC.stateParams.reportName, model: $scope.Model, selectedModel: $scope.previousModel, term: $scope.previousModel.Filters.Term, automaticType: $scope.previousModel.Filters.AutomaticTerm })
                    .success(function (data, status, headers, config) {
                        if (data.Status == true) {
                            jbToast.success("Report sent successfully.");
                            $scope.back();
                        } else {
                            $scope.MC.handleErrors($scope, data);
                        }
                    });
            }
        }

        $scope.back = function () {
            var reportName = $scope.MC.stateParams.reportName;
            switch (reportName) {
                case "Capacity":
                    {
                        $state.go('CapacityReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }

                case "Installment":
                    {
                        $state.go('InstallmentReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }

                case "FollowupForm":
                    {
                        $state.go('FollowupReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "Balance":
                    {
                        $state.go('BalanceReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "ChargeDiscount":
                    {
                        $state.go('ChargesReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "Finance":
                    {
                        $state.go('FinanceReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                case "Coupon":
                    {
                        $state.go('CouponReportRun', { seasonDomain: $scope.MC.stateParams.seasonDomain });
                        break;
                    }
                default:
                    {
                        window.history.back();
                        break;
                    }
            }
        }

    }]);
})();