

Alter table ContactPersons add [IsPrimary] [bit] NOT NULL DEFAULT ((0))
go

Update ContactPersons set IsPrimary = 1  where  Club_Id is not null
go
