﻿using Jb.Framework.Common;
using System.Linq;
using Jumbula.Core.Domain;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface IEmailCampaignTemplateBusiness
    {
        IQueryable<EmailTemplate> GetAllTemplates(int clubId);
        OperationStatus UpdateCampaignTemplate(EmailTemplate newTemplate);
        OperationStatus SaveCampaignTemplate(EmailTemplate template);
        OperationStatus DeleteCampaignTemplate(int templateId);
        EmailTemplate GetTemplateById(int templateId);
        EmailTemplate GetDefaultClubTemplate();
        string UploadImageTemplate(string image, string clubDomain, string folderName);
    }
}
