﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Jb.Framework.Common.Forms.Binding;
using Jumbula.Common.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Jumbula.Core.Domain.Generic
{
    public class JbPageConverter : JsonConverter
    {

        public override bool CanConvert(Type objectType)
        {
            return typeof(JbPage).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var form = new JbPage();
            JObject jsonObject = JObject.Load(reader);
            var properties = jsonObject.Properties().ToList();
            foreach (var property in properties)
            {
                if (property.Name.Equals("Type"))
                    continue;
                var proInfo = form.GetType().GetProperty(property.Name);

                if (proInfo.PropertyType == form.Elements.GetType())
                {
                    var jbc = new JbPElementConverter();
                    var props = property.Value.ToObject<List<object>>();
                    List<JbPBaseElement> elements = props.Select(prop => jbc.GetJbElement(prop as JObject)).ToList();

                    proInfo.SetValue(form, elements);
                }
                else if (proInfo.PropertyType == form.deletedImages.GetType())
                {
                    var Images = new List<JbPImage>();
                    var jbc = new JbPElementConverter();
                    var props = property.Value.ToObject<List<JbPImage>>();
                  
                    proInfo.SetValue(form, props);
                }
                else if (proInfo.PropertyType == typeof(JbPHeader))
                {
                    var header = new JbPHeader();
                    header = JsonHelper.JsonDeserialize<JbPHeader>(property.Value.ToString());
                    form.Header = header;
                }
                else if (proInfo.PropertyType == typeof(JbPFooter))
                {
                    var footer = new JbPFooter();
                    footer = JsonHelper.JsonDeserialize<JbPFooter>(property.Value.ToString());
                    form.Footer = footer;
                }
                else if (proInfo.PropertyType == typeof(JbPSetting))
                {
                    var setting = new JbPSetting();
                    setting = JsonHelper.JsonDeserialize<JbPSetting>(property.Value.ToString());
                    form.Setting = setting;
                }
                else if (proInfo.PropertyType == typeof(PostalAddress))
                {
                    var postalAddress = new PostalAddress();
                    postalAddress = JsonHelper.JsonDeserialize<PostalAddress>(property.Value.ToString());
                    form.Address = postalAddress;
                }
                else
                {
                    if (proInfo.PropertyType != typeof(BindingConfig))
                        proInfo.SetValue(form, Convert.ChangeType(property.Value, proInfo.PropertyType));
                }

            }
            return form;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var form = value as JbPage;
            writer.WriteStartObject();
            var properties = form.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public)
                .Where(p => !Attribute.IsDefined(p, typeof(JsonIgnoreAttribute)));

            foreach (var property in properties)
            {
                if (Attribute.IsDefined(property, typeof(JsonIgnoreAttribute)))
                    continue;
                writer.WritePropertyName(property.Name);
                serializer.Serialize(writer, property.GetValue(form));
            }

            writer.WriteEndObject();
        }
    }
}