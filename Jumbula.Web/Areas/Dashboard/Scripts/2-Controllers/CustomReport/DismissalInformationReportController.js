﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('DismissalInformationReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        var model = {};
        var dataInfo = {};

        $scope.Model = customReportService.getModelFromStrorage();

        var programId = $scope.Model._SelectedPrograms;

        if (programId == 'undefined' || programId == null) {
            programId = 0;
        }

        $http.get(baseUrl + '/GetDismissalInformationReportDetail', { params: { seasonDomain: $scope.seasonDomain, programId: programId } }).success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.SeasonName = data.SeasonName;
            $scope.ClassName = data.ClassName;
            $scope.Days = data.Days;
            dataInfo = data;
            prepareGrid();

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        var prepareGrid = function () {
            $scope.DismissalDataSource = new kendo.data.DataSource({

                transport: {
                    read: {
                        url: baseUrl + "/GetDismissalInformationReport",
                        dataType: "json",
                        type: "GET",
                        data: function () {
                            return {
                                programId: programId,
                            };
                        },
                    }
                },
                schema: {
                    total: "TotalCount",
                    data: "DataSource"
                },
                serverSorting: false,
                serverPaging: true,
                pageSize: 50,
                resizable: true,
                batch: true,
            });

            $scope.DismissalGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Dismissal).xlsx",
                    proxyURL: "/Dashboard/CustomReport/Save"
                },
                excelExport: function (e) {

                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.ClassName + " - " + dataInfo.Days
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.SeasonName
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Dismissal information - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.DismissalDataSource,
                resizable: true,
                dataBound: function () {
                    var grid = $("#DismissalGrid").data("kendoGrid");
                    customReportService.initializeDoubleScroll(grid);
                },
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5,
                },
                sortable: true,
                columns: [

                    {
                        field: "ParticipantFullName",
                        title: "Full name",
                        width: "230px"
                    }, {
                        field: "ParentFirstName",
                        title: "Parent first name",
                        width: "230px"
                    }, {
                        field: "ParentLastName",
                        title: "Last name",
                        width: "230px"
                    }, {
                        field: "ParentPrimaryPhone",
                        title: "Primary phone",
                        width: "230px"
                    }, {
                        field: "ParentAlternatePhone",
                        title: "Alternate phone",
                        width: "230px"
                    }, {
                        field: "ParentAuthorizedAdult1",
                        title: "Authorized adult 1",
                        width: "230px"
                    }, {
                        field: "ParentAuthorizedAdultPhone1",
                        title: "Authorized adult phone 1",
                        width: "230px"
                    }, {
                        field: "ParentAuthorizedAdult2",
                        title: "Authorized adult 2",
                        width: "230px"
                    }, {
                        field: "ParentAuthorizedAdultPhone2",
                        title: "Authorized adult phone 2",
                        width: "230px"
                    }, {
                        field: "ParentAuthorizedAdult3",
                        title: "Authorized adult 3",
                        width: "230px"
                    }, {
                        field: "ParentAuthorizedAdultPhone3",
                        title: "Authorized adult phone 3",
                        width: "230px"
                    }, {
                        field: "ParentAuthorizedAdult4",
                        title: "Authorized adult 4",
                        width: "230px"
                    }, {
                        field: "ParentAuthorizedAdultPhone4",
                        title: "Authorized adult phone 4",
                        width: "230px"
                    }, {
                        field: "EmergencyContactFirstName",
                        title: "Emergency contact first name",
                        width: "230px"
                    }, {
                        field: "EmergencyContactLastName",
                        title: "Last name",
                        width: "230px"
                    }, {
                        field: "EmergencyContactPhone",
                        title: "Phone",
                        width: "230px"
                    }, {
                        field: "SchoolDismissalFromEnrichment",
                        title: "Dismissal from enrichment",
                        width: "230px"
                    },

                ]
            }
        }


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#DismissalGrid").getKendoGrid().saveAsExcel();
        }


    }
    ]);

})();