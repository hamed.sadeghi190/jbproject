ALTER TABLE [dbo].[CampaignRecipients]
ALTER COLUMN [EmailStatus] int not null
--

Delete [dbo].[Campaigns]  where Club_Id Is NULL

ALTER TABLE [dbo].[Campaigns] ALTER COLUMN [Club_Id] int not null

--
ALTER TABLE [dbo].[Campaigns]
DROP CONSTRAINT [FK_dbo.Campaigns_dbo.MailLists_MailList_Id]

ALTER TABLE [dbo].[Campaigns] Drop COLUMN [MailList_Id] 
--

CREATE TABLE [dbo].[ClubWaivers]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Text] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Club_Id] [int] NULL
)
GO
-- Constraints and Indexes

ALTER TABLE [dbo].[ClubWaivers] ADD CONSTRAINT [PK_dbo.ClubWaivers] PRIMARY KEY CLUSTERED  ([Id])
GO
CREATE NONCLUSTERED INDEX [IX_Club_Id] ON [dbo].[ClubWaivers] ([Club_Id])
GO
-- Foreign Keys

ALTER TABLE [dbo].[ClubWaivers] ADD CONSTRAINT [FK_dbo.ClubWaivers_dbo.Clubs_Club_Id] FOREIGN KEY ([Club_Id]) REFERENCES [dbo].[Clubs] ([Id])
GO
--

-- Columns

CREATE TABLE [dbo].[CouponEvents]
(
[Coupon_Id] [int] NOT NULL,
[Event_Id] [int] NOT NULL
)
GO
-- Constraints and Indexes

--
ALTER TABLE [dbo].[ClubWaiverPrograms] ADD CONSTRAINT [FK_dbo.ClubWaiverPrograms_dbo.ClubWaivers_ClubWaiver_Id] FOREIGN KEY ([ClubWaiver_Id]) REFERENCES [dbo].[ClubWaivers] ([Id]) ON DELETE CASCADE

ALTER TABLE [dbo].[ClubWaiverPrograms] ADD CONSTRAINT [FK_dbo.ClubWaiverPrograms_dbo.Programs_Program_Id] FOREIGN KEY ([Program_Id]) REFERENCES [dbo].[Programs] ([Id]) ON DELETE CASCADE
--

ALTER TABLE [dbo].[Coupons] Drop column [Events]


ALTER TABLE [dbo].[Coupons] Add [ApplyToAllEvents] [bit] NOT NULL CONSTRAINT [DF__Coupons__ApplyTo__7C3A67EB] DEFAULT ((0))
--
 
ALTER TABLE [dbo].[Coupons] Add [Program_Id] [bigint] NULL

ALTER TABLE [dbo].[Coupons] ADD CONSTRAINT [FK_dbo.Coupons_dbo.Programs_Program_Id] FOREIGN KEY ([Program_Id]) REFERENCES [dbo].[Programs] ([Id])
--

Alter Table [EmailTemplates] Drop column [Template]

--

Alter Table [EventForms] Add [Program_Id] [bigint] NULL

ALTER TABLE [dbo].[EventForms] ADD CONSTRAINT [FK_dbo.EventForms_dbo.Programs_Program_Id] FOREIGN KEY ([Program_Id]) REFERENCES [dbo].[Programs] ([Id])

--

ALTER TABLE [dbo].[EventRoasters] Drop CONSTRAINT [FK_dbo.EventRoasters_dbo.Seasons_SeasonId] 
ALTER TABLE [dbo].[EventRoasters] Add [Type] [int] NOT NULL CONSTRAINT [DF__EventRoast__Type__59E54FE7] DEFAULT ((0))

--

ALTER TABLE [JbForms] Add [OrderItem_Id] [int] NULL
ALTER TABLE [dbo].[JbForms] ADD CONSTRAINT [FK_dbo.JbForms_dbo.OrderItems_OrderItem_Id] FOREIGN KEY ([OrderItem_Id]) REFERENCES [dbo].[OrderItems] ([Id])

--
Alter Table [MailListContacts] Alter COLUMN [MailList_Id] [int] NOT NULL

ALTER TABLE [dbo].[MailListContacts] Drop [FK_dbo.MailListContacts_dbo.MailLists_MailList_Id] 
ALTER TABLE [dbo].[MailListContacts] ADD CONSTRAINT [FK_dbo.MailListContacts_dbo.MailLists_MailList_Id] FOREIGN KEY ([MailList_Id]) REFERENCES [dbo].[MailLists] ([Id]) ON DELETE CASCADE

--
Alter TABLE [dbo].[OrderCharges] Add [Charge_Id] [bigint] NULL

ALTER TABLE [dbo].[OrderCharges] Drop [FK_dbo.OrderCharges_dbo.Orders_OrderId] 
ALTER TABLE [dbo].[OrderCharges] ADD CONSTRAINT [FK_dbo.OrderCharges_dbo.Charges_Charge_Id] FOREIGN KEY ([Charge_Id]) REFERENCES [dbo].[Charges] ([Id])

--

Alter Table [OrderItems] Add [scid] [int] NOT NULL CONSTRAINT [DF__OrderItems__scid__53584DE9] DEFAULT ((0))
Alter Table [OrderItems] Add [Start] [datetime] NULL
Alter Table [OrderItems] Add [End] [datetime] NULL
Alter Table [OrderItems] Add [RegType] [int] NOT NULL CONSTRAINT [DF__OrderItem__RegTy__544C7222] DEFAULT ((0))
Alter Table [OrderItems] Add [Skill_Rating] [int] NULL
Alter Table [OrderItems] Add [Skill_Level] [int] NOT NULL CONSTRAINT [DF__OrderItem__Skill__5540965B] DEFAULT ((0))
Alter Table [OrderItems] Add [ProgramScheduleId] [bigint] NULL
Alter Table [OrderItems] Add [Section] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table [OrderItems] Add [Schedule] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table [OrderItems] Add [Byes] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table [OrderItems] Add [Options] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table [OrderItems] Add [JbFormId] [int] NULL
Alter Table [OrderItems] Add [Category_Id] [int] NULL

ALTER TABLE [dbo].[OrderItems] ADD CONSTRAINT [FK_dbo.OrderItems_dbo.Categories_Category_Id] FOREIGN KEY ([Category_Id]) REFERENCES [dbo].[Categories] ([Id])
ALTER TABLE [dbo].[OrderItems] ADD CONSTRAINT [FK_dbo.OrderItems_dbo.JbForms_JbFormId] FOREIGN KEY ([JbFormId]) REFERENCES [dbo].[JbForms] ([Id])


Alter Table [OrderItems] Drop Column [Domain] 
Alter Table [OrderItems] Drop Column [SubDomain]
Alter Table [OrderItems] Drop Column [ItemId]
Alter Table [OrderItems] Drop Column [OrderId]

Alter Table [OrderItems] Drop Column [CategoryId]
Alter Table [OrderItems] Drop Column [SubDomainCategory] 

Alter Table [OrderItems] Add [OrderId] [int] NULL
-- 

Alter Table [Orders] Drop Column [CardSecurityCode] 
Alter Table [Orders] Drop Column [Currency]
Alter Table [Orders] Drop Column [CardFirstName]
Alter Table [Orders] Drop Column [CardLastName]
Alter Table [Orders] Drop Column [CardEmail]
Alter Table [Orders] Drop Column [CardNumber]
Alter Table [Orders] Drop Column [CardExpireDate]
Alter Table [Orders] Drop Column [Name] 
Alter Table [Orders] Drop Column [FirstName] 
Alter Table [Orders] Drop Column [LastName] 
Alter Table [Orders] Drop Column [Custom1] 
Alter Table [Orders] Drop Column [Custom2] 
Alter Table [Orders] Drop Column [Custom3] 
Alter Table [Orders] Drop Column [Custom4] 
Alter Table [Orders] Drop Column [Custom5] 
Alter Table [Orders] Drop Column [PrimaryOrderEmail] 
Alter Table [Orders] Drop Column [SecondaryOrderEmail]
Alter Table [Orders] Drop Column [CustomFieldsMetaData] 
Alter Table [Orders] Drop Column [EventBaseInfoMetaData] 
Alter Table [Orders] Drop Column [ClubOwnerCustomFieldMetaData] 


Alter Table [Orders] Drop [FK_dbo.Orders_dbo.PostalAddresses_BillingAddress_Id]
Alter Table [Orders] Drop [FK_dbo.Orders_dbo.Categories_CategoryId] 
Alter Table [Orders] Drop [FK_Orders_JbForms]
Alter Table [Orders] Drop [FK_dbo.Orders_dbo.PlayerProfiles_PlayerId]



DROP INDEX IX_BillingAddress_Id ON [Orders]
DROP INDEX [IX_CategoryId] ON [dbo].[Orders] 
DROP INDEX [IX_Orders_Domain] ON [dbo].[Orders] 
DROP INDEX [IX_Orders_JbForm_Id] ON [dbo].[Orders] 
DROP INDEX [IX_Orders_PaymentStatus] ON [dbo].[Orders] 
DROP INDEX [IX_PlayerId] ON [dbo].[Orders] 
DROP INDEX [IX_Orders_SubDomain] ON [dbo].[Orders] 
DROP INDEX [IX_Orders_UserId] ON [dbo].[Orders] 


Alter Table [Orders] Drop DF__Orders__Domain__670A40DB
Alter Table [Orders] Drop DF__Orders__SubDomai__67FE6514
Alter Table [Orders] Drop DF__Orders__SubDomai__69E6AD86
Alter Table [Orders] Drop DF__Orders__Sport__6ADAD1BF
Alter Table [Orders] Drop DF__Orders__PlayerId__68F2894D

Alter Table [Orders] Drop Column [BillingAddress_Id]
Alter Table [Orders] Drop Column [Domain]
Alter Table [Orders] Drop Column [SubDomain]
Alter Table [Orders] Drop Column [SubDomainCategory]
Alter Table [Orders] Drop Column [CategoryId]
Alter Table [Orders] Drop Column [PlayerId] 

--

Alter Table [dbo].[PaymentPlans] Add [Program_Id] [bigint] NULL

ALTER TABLE [dbo].[PaymentPlans] ADD CONSTRAINT [FK_dbo.PaymentPlans_dbo.Programs_Program_Id] FOREIGN KEY ([Program_Id]) REFERENCES [dbo].[Programs] ([Id])

--

ALTER TABLE [dbo].[PlayerForms] Drop [FK_dbo.PlayerForms_dbo.Orders_OrderID] 

--
CREATE TABLE [dbo].[Discounts]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[Name] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Category] [smallint] NOT NULL,
[Amount] [decimal] (18, 2) NOT NULL,
[AmounType] [int] NOT NULL,
[NOP] [int] NOT NULL,
[BenefitType] [tinyint] NOT NULL,
[ApplyType] [tinyint] NOT NULL,
[IsAllProgram] [bit] NOT NULL,
[SeasonId] [bigint] NOT NULL,
[MetaData_DateCreated] [datetime] NOT NULL,
[MetaData_DateUpdated] [datetime] NOT NULL,
[IsDeleted] [bit] NOT NULL
)

ALTER TABLE [dbo].[Discounts] ADD CONSTRAINT [PK_dbo.Discounts] PRIMARY KEY CLUSTERED  ([Id])

CREATE NONCLUSTERED INDEX [IX_SeasonId] ON [dbo].[Discounts] ([SeasonId])

ALTER TABLE [dbo].[Discounts] ADD CONSTRAINT [FK_dbo.Discounts_dbo.Seasons_SeasonId] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Seasons] ([Id])

ALTER TABLE [dbo].[ProgramDiscounts] ADD CONSTRAINT [FK_dbo.ProgramDiscounts_dbo.Discounts_DiscountId] FOREIGN KEY ([DiscountId]) REFERENCES [dbo].[Discounts] ([Id]) ON DELETE CASCADE
--

Alter Table [ProgramSchedules] Drop DF__ProgramSc__Price__49E3F248

Alter Table [ProgramSchedules] Drop Column [Title]
Alter Table [ProgramSchedules] Drop Column [Price_Lable]
Alter Table [ProgramSchedules] Drop Column [Capacity]
Alter Table [ProgramSchedules] Drop Column [IsProrate]
Alter Table [ProgramSchedules] Drop Column [IsDropIn]
Alter Table [ProgramSchedules] Drop Column [Price_Amount]
Alter Table [dbo].[ProgramSchedules] Alter Column [Attributes] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NULL


--
ALTER TABLE [dbo].[Campaigns] Drop  [FK_dbo.Campaigns_dbo.Clubs_Club_Id]
ALTER TABLE [dbo].[Campaigns] ADD CONSTRAINT [FK_dbo.Campaigns_dbo.Clubs_Club_Id] FOREIGN KEY ([Club_Id]) REFERENCES [dbo].[Clubs] ([Id]) ON DELETE CASCADE
--

Alter Table Coupons Drop Column [Users] 
Alter Table Coupons Alter Column [ClubDomain] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
Alter Table Coupons Drop [FK_dbo.Coupons_dbo.Seasons_SeasonId]

--

Alter Table Orders Drop Column OrderId
Alter Table Orders Drop Column Domain
Alter Table Orders Drop Column [JbForm_Id]
Alter Table Orders Drop Column [SeasonDomain]

Alter Table Orders Add [ClubId] [int] NULL
Alter Table Orders Add [SeasonId] [bigint] NULL

ALTER TABLE [dbo].[Orders] ADD CONSTRAINT [FK_dbo.Orders_dbo.Clubs_ClubId] FOREIGN KEY ([ClubId]) REFERENCES [dbo].[Clubs] ([Id])
ALTER TABLE [dbo].[Orders] ADD CONSTRAINT [FK_dbo.Orders_dbo.Seasons_SeasonId] FOREIGN KEY ([SeasonId]) REFERENCES [dbo].[Seasons] ([Id])

--

CREATE TABLE [dbo].[CouponEvents]
(
[Coupon_Id] [int] NOT NULL,
[Event_Id] [int] NOT NULL
)
GO

ALTER TABLE [dbo].[CouponEvents] ADD CONSTRAINT [PK_dbo.CouponEvents] PRIMARY KEY CLUSTERED  ([Coupon_Id], [Event_Id])
GO
CREATE NONCLUSTERED INDEX [IX_Coupon_Id] ON [dbo].[CouponEvents] ([Coupon_Id])
GO
CREATE NONCLUSTERED INDEX [IX_Event_Id] ON [dbo].[CouponEvents] ([Event_Id])
GO

ALTER TABLE [dbo].[CouponEvents] ADD CONSTRAINT [FK_dbo.CouponEvents_dbo.Coupons_Coupon_Id] FOREIGN KEY ([Coupon_Id]) REFERENCES [dbo].[Coupons] ([Id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CouponEvents] ADD CONSTRAINT [FK_dbo.CouponEvents_dbo.Events_Event_Id] FOREIGN KEY ([Event_Id]) REFERENCES [dbo].[Events] ([Id]) ON DELETE CASCADE
GO

--

CREATE TABLE [dbo].[CouponUserProfiles]
(
[Coupon_Id] [int] NOT NULL,
[UserProfile_Id] [int] NOT NULL
)
GO

ALTER TABLE [dbo].[CouponUserProfiles] ADD CONSTRAINT [PK_dbo.CouponUserProfiles] PRIMARY KEY CLUSTERED  ([Coupon_Id], [UserProfile_Id])
GO
CREATE NONCLUSTERED INDEX [IX_Coupon_Id] ON [dbo].[CouponUserProfiles] ([Coupon_Id])
GO
CREATE NONCLUSTERED INDEX [IX_UserProfile_UserId] ON [dbo].[CouponUserProfiles] ([UserProfile_Id])
GO

ALTER TABLE [dbo].[CouponUserProfiles] ADD CONSTRAINT [FK_dbo.CouponUserProfiles_dbo.Coupons_Coupon_Id] FOREIGN KEY ([Coupon_Id]) REFERENCES [dbo].[Coupons] ([Id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CouponUserProfiles] ADD CONSTRAINT [FK_dbo.CouponUserProfiles_dbo.UserProfile_UserProfile_UserId] FOREIGN KEY ([UserProfile_Id]) REFERENCES [dbo].[UserProfile] ([UserId]) ON DELETE CASCADE
GO
---

CREATE TABLE [dbo].[OrderItemSessions]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[OrderItemId] [int] NOT NULL
)
GO


ALTER TABLE [dbo].[OrderItemSessions] ADD CONSTRAINT [PK_dbo.OrderItemSessions] PRIMARY KEY CLUSTERED  ([Id])
GO
CREATE NONCLUSTERED INDEX [IX_OrderItemId] ON [dbo].[OrderItemSessions] ([OrderItemId])
GO


ALTER TABLE [dbo].[OrderItemSessions] ADD CONSTRAINT [FK_dbo.OrderItemSessions_dbo.OrderItems_OrderItemId] FOREIGN KEY ([OrderItemId]) REFERENCES [dbo].[OrderItems] ([Id]) ON DELETE CASCADE
GO
--

Alter Table [dbo].[Discounts] Drop Column [AmounType]
Alter Table [dbo].[Discounts] Add [AmountType] [int] NOT NULL CONSTRAINT [DF__Discounts__Amoun__0D84EF7E] DEFAULT ((0))
Alter Table [dbo].[Discounts] Add [Description] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

--
Delete [dbo].[EmailTemplates] where club_Id Is Null
Alter Table [dbo].[EmailTemplates] Alter Column [Club_Id] [int] NOT NULL
Alter Table [dbo].[EmailTemplates] Add [Template] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

ALTER TABLE [dbo].[EmailTemplates] Drop [FK_dbo.EmailTemplates_dbo.Clubs_Club_Id] 
ALTER TABLE [dbo].[EmailTemplates] ADD CONSTRAINT [FK_dbo.EmailTemplates_dbo.Clubs_Club_Id] FOREIGN KEY ([Club_Id]) REFERENCES [dbo].[Clubs] ([Id]) ON DELETE CASCADE
--

Alter Table [Events] Add [SeasonDomain] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Events] Drop [FK_dbo.Events_dbo.Seasons_SeasonId] 
---
ALTER TABLE [dbo].[OrderChargeDiscounts] Add [Name] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
----
ALTER TABLE [dbo].[Seasons]  Add [Template_Id] [int] NULL
ALTER TABLE [dbo].[Seasons] ADD CONSTRAINT [FK_dbo.Seasons_dbo.EmailTemplates_Template_Id] FOREIGN KEY ([Template_Id]) REFERENCES [dbo].[EmailTemplates] ([Id])

ALTER TABLE [dbo].[Seasons] Alter Column [Title] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Seasons] Alter Column [Domain] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
--
ALTER TABLE [dbo].[TourneyCoverages] Alter Column [Data] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL

---
Alter TABLE [dbo].[Coupons] Add [IsForAllUsers] [bit] NOT NULL CONSTRAINT [DF__Coupons__IsForAl__797DF6D1] DEFAULT ((0))


---
ALTER TABLE [dbo].[OrderItems] Drop CONSTRAINT [FK_dbo.OrderItems_dbo.Orders_Order_Id]

Drop Index IX_Order_Id On [dbo].[OrderItems] 
 
ALTER TABLE [dbo].[OrderItems] Drop Column [Order_Id]

ALTER TABLE [dbo].[OrderItems] ADD CONSTRAINT [FK_dbo.OrderItems_dbo.Orders_Order_Id] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([Id])
---

Drop Table [CampOrderItems]
Drop Table [CourseOrderItems]
Drop Table [dbo].[EntityForms]
Drop Table [EventOrderItems]
Drop Table [dbo].[TourneyOrderItems]
Drop Table [dbo].[TTTourneyOrderItems]
Drop Table [dbo].[Webhooks]
Drop Table [dbo].[ChargeDiscounts]
Drop Table [dbo].[OrderCharges]


