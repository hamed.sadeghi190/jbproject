﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('ProviderContactInfoController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        $scope.Model = {};
        $scope.Model.checkedIds = {};
        $scope.reportName = "";
        var nameModel = customReportService.getModelFromStrorage();
        if (is.not.undefined(nameModel) && nameModel != null) {
            var seasonId = nameModel.seasonId;
            var schoolId = nameModel.schoolId;
            $http.get('Dashboard/PortalReport/GetReoprtNameWithSeason', { params: { schoolId: schoolId, seasonId: seasonId } }).success(function (data, status, headers, config) {
                $scope.reportName = data;
                SetTheGrid();
            }).error(function (data, status, headers, config) {
                SetTheGrid();
                jbToast.error(data.Message);
            });
        }

        $scope.ProvidersContactInfoDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetProviderContactInfo?SeasonId=' + $scope.MC.stateParams.seasonId,
                    },

                },
            sort: { field: 'ClassName', dir: 'asc' },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
        });
        function SetTheGrid() {
            $scope.ProvidersContactInfoReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.reportName + " (Provider contact info).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {
                    cfpLoadingBar.complete();
                },
                dataSource: $scope.ProvidersContactInfoDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5
                },

                sortable: { mode: 'single' },
                columns: [

                    {
                        field: "ClassName",
                        title: "Class name",
                        width: "180px",
                    },
                    {
                        field: "ProviderName",
                        title: "Provider name",
                        width: "200px",
                    },
                    {
                        field: "FullName",
                        title: "Full name",
                        width: "200px",
                    },
                    {
                        field: "ContactEmail",
                        title: "Email (Contact)",
                        width: "200px",
                    },
                    {
                        field: "Phone",
                        title: "Phone",
                        width: "200px",
                    },
                    {
                        field: "Instructor1St",
                        title: "1st Instuctor",
                        width: "200px",
                    },
                    {
                        field: "Phone1StInstructor",
                        title: "Phone",
                        width: "200px",
                    },
                    {
                        field: "Instructor2nd",
                        title: "2nd Instuctor",
                        width: "200px",
                    },
                    {
                        field: "Phone2ndInstructor",
                        title: "Phone",
                        width: "200px",
                    }
                ],
            }
        }
        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ProvidersContactInfoReportGrid").getKendoGrid().saveAsExcel();
        }

        $scope.shareReport = function () {
           
            $state.go('SharePortalReportAsAttchment', { seasonId: nameModel.seasonId, reportName: "ProviderContactInfo" });
        }
    }
    ]);

})();