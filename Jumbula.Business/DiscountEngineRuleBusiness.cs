﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{

    public class DiscountEngineRuleBusiness : IDiscountEngineRuleBusiness
    {
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IRepository<Discount, long> _discountRepository;

        public DiscountEngineRuleBusiness(IOrderItemBusiness orderItemBusiness, IRepository<Discount, long> discountRepository)
        {
            _orderItemBusiness = orderItemBusiness;
            _discountRepository = discountRepository;
        }

        public void CalculateDiscount(Order order, DiscountMode discountMode)
        {

            TotalDiscountAmount = 0;
            var isTestMode = !order.IsLive;

            var orderItemsGroup = order.RegularOrderItems.Where(i => i.ProgramScheduleId.HasValue).GroupBy(c => c.SeasonId);
            foreach (var item in order.RegularOrderItems)
            {
                if (item.GetOrderChargeDiscounts() != null && item.GetOrderChargeDiscounts().ToList().Any())
                {
                    //Select all discounts blong to one of the rules
                    var items = item.OrderChargeDiscounts.Where(o => !o.IsDeleted && (int)o.Category <= ((int)ChargeDiscountCategory.AllProgramAllParticipant)).ToList();
                    foreach (var ch in items)
                    {
                        item.OrderChargeDiscounts.Remove(ch);
                    }
                }
            }

            if (order.Club.Setting != null)
            {
                isApplyOnOrigin = order.Club.Setting.IsDiscountApplyOnOriginPrice;
            }
            IOrderItemBusiness orderBusiness = null;
            if (orderBusiness == null)
            {
                orderBusiness = _orderItemBusiness;
            }
            foreach (var seasonOrderItems in orderItemsGroup)
            {
                long currentSeason = seasonOrderItems.Key.Value;
                var allOrders = seasonOrderItems.ToList();
                var userId = allOrders.FirstOrDefault().Order.UserId;

                if (discountMode == DiscountMode.OrdersInSeason)
                {

                    if (orderBusiness.GetOrderItems(userId: userId, seasonId: currentSeason, isTestMode: isTestMode).Any())
                        allOrders.AddRange(orderBusiness.GetOrderItems(userId: userId, seasonId: currentSeason, isTestMode: isTestMode));
                }

                int totalNumberOfItems = allOrders.Count();

                var DiscountList = GetAllProgramAllParticipantList(currentSeason, totalNumberOfItems).ToList();
                if (DiscountList.Any())
                {
                    Discount discount = null;
                    List<long> eligibleOrderItem = new List<long>();

                    discount = EligibleAllProgramAllParticipantRule(DiscountList, allOrders, order.Id, out eligibleOrderItem);
                    if (discount != null)
                    {
                        ApplyDiscountOnOrder(discount, allOrders.Where(c => eligibleOrderItem.Contains(c.Id)).ToList(), order.OrderItems.Where(c => eligibleOrderItem.Contains(c.Id) && c.Order_Id == order.Id).ToList(), discountMode, isTestMode);

                    }
                }
            }
            foreach (var item in order.RegularOrderItems)
            {

                if (item.GetOrderChargeDiscounts() != null && item.GetOrderChargeDiscounts().ToList().Any())
                {
                    item.CalculateTotalAmount();
                }
            }
            order.OrderAmount -= TotalDiscountAmount;
        }

        public Discount Get(long discountId)
        {
            return _discountRepository.Get(d => d.Id == discountId && d.IsDeleted != true);
        }

        private IQueryable<Discount> GetList(long seasonId)
        {
            return _discountRepository.GetList().Where(d => d.SeasonId == seasonId && d.IsDeleted != true);
        }

        private int GetTotalNumberofItems(Order order)
        {
            return order.OrderItems.Count();
        }

        private Discount EligibleAllProgramAllParticipantRule(List<Discount> discountRules, List<OrderItem> orderItems, long orderId, out List<long> eligibleOrderItem)
        {
            Discount eligibleDiscount = null;
            IEnumerable<OrderItem> eligibleOrderItems = null;
            IEnumerable<long> programListId = null;
            eligibleOrderItem = new List<long>();
            foreach (var discount in discountRules.OrderByDescending(o => o.NOP))
            {
                if (discount.IsAllProgram)
                {
                    programListId = orderItems.Select(p => p.ProgramSchedule.ProgramId).Distinct();
                    eligibleOrderItems = orderItems;
                }
                else
                {
                    programListId = discount.Programs.Select(p => p.Id).Distinct();
                    eligibleOrderItems = orderItems.Where(c => (programListId.Contains(c.ProgramSchedule.ProgramId)) == true);
                }



                if (discountRules.Any(d => d.NOP <= eligibleOrderItems.Count()))
                {
                    eligibleOrderItem = eligibleOrderItems.Select(c => c.Id).ToList();
                    return (discountRules.Where(d => d.NOP <= eligibleOrderItems.Count()).OrderByDescending(c => c.NOP).FirstOrDefault());
                }
            }
            return eligibleDiscount;
        }

        private void ApplyDiscountOnOrder(Discount discount, List<OrderItem> allOrderItems, List<OrderItem> eligibleOrdersForDiscount, DiscountMode discountMode, bool isTestMode)
        {
            var appliedDiscounts = allOrderItems.SelectMany(c => c.OrderChargeDiscounts).Where(d => !d.IsDeleted && d.Category == discount.Category).ToList();
            decimal appliedDiscountAmount = appliedDiscounts != null ? (appliedDiscounts.Sum(c => c.Amount) * -1) : 0;

            decimal discountAmount = 0;
            switch (discount.ApplyType)
            {
                case DiscountApplyType.Every:
                    {
                        decimal wholeDiscount = getdiscountAmount(allOrderItems, discount);

                        discountAmount = wholeDiscount - appliedDiscountAmount;

                        break;
                    }
                case DiscountApplyType.SelectedNumOnly:
                    {
                        if (discount.BenefitType == DiscountBenefitType.Organization)
                        {

                            decimal wholeDiscount = getdiscountAmount(new List<OrderItem>() { allOrderItems.OrderBy(c => c.EntryFee).FirstOrDefault() }, discount);

                            discountAmount = wholeDiscount - appliedDiscountAmount;

                        }
                        else
                        {
                            decimal wholeDiscount = getdiscountAmount(new List<OrderItem>() { allOrderItems.OrderByDescending(c => c.EntryFee).FirstOrDefault() }, discount);

                            discountAmount = wholeDiscount - appliedDiscountAmount;

                        }
                        break;
                    }
                case DiscountApplyType.SelectedNumAndAddition:
                    {
                        var takeitem = allOrderItems.Count() - discount.NOP + 1;

                        if (discount.BenefitType == DiscountBenefitType.Organization)
                        {
                            decimal wholeDiscount = getdiscountAmount(allOrderItems.OrderBy(c => c.EntryFee).Take(takeitem), discount);

                            discountAmount = wholeDiscount - appliedDiscountAmount;

                        }
                        else
                        {
                            decimal wholeDiscount = getdiscountAmount(allOrderItems.OrderByDescending(c => c.EntryFee).Take(takeitem), discount);

                            discountAmount = wholeDiscount - appliedDiscountAmount;

                        }
                        break;
                    }
            }

            if (discountAmount > 0)
            {
                AddDiscountToItems(discount, eligibleOrdersForDiscount, discountAmount);
            }
        }

        private void AddDiscountToItems(Discount discount, List<OrderItem> orderItem, decimal appliedDiscountAmount)
        {
            decimal applicableAmount = 0;
            decimal appliedAmount = 0;
            foreach (var item in orderItem)
            {


                if (isApplyOnOrigin)
                {
                    applicableAmount = (discount.AmountType == ChargeDiscountType.Fixed) ? discount.Amount : ((item.EntryFee * discount.Amount) / 100);
                }
                else
                {
                    applicableAmount = (discount.AmountType == ChargeDiscountType.Fixed) ? discount.Amount : (((item.EntryFee - (-1 * item.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Subcategory == ChargeDiscountSubcategory.Discount).Sum(c => c.Amount))) * discount.Amount) / 100);
                }

                applicableAmount = Common.Helper.CurrencyHelper.RoundCurrencyWithPenny(applicableAmount);
                
                var orderDiscount = new OrderChargeDiscount()
                {
                    Name = discount.Name,
                    Amount = ((-1) * (applicableAmount)),
                    Category = discount.Category,
                    Description = string.Format("${0} off for {1}", appliedDiscountAmount, discount.Name),
                    DiscountId = discount.Id,
                    Discount = discount,
                    OrderItemId = item.Id,
                    Subcategory = ChargeDiscountSubcategory.Discount
                };
                appliedAmount += applicableAmount;

                item.OrderChargeDiscounts.Add(orderDiscount);
            }

            var discountAmounts = appliedDiscountAmount - appliedAmount;
            discountAmounts = Common.Helper.CurrencyHelper.RoundCurrencyWithPenny(discountAmounts);

            if (discountAmounts > 0)
            {
                orderItem.First().OrderChargeDiscounts.FirstOrDefault(c => !c.IsDeleted && c.Category == discount.Category).Amount += (-1 * (discountAmounts));
            }

        }

        private Discount EligibleAllProgramDiffParticipant(IQueryable<Discount> discountRules, List<OrderItem> orderItems, DiscountMode discountMode, int userId, bool isTestMode)
        {
            Discount eligibleDiscount = null;
            IEnumerable<OrderItem> eligibleOrderItems = null;
            IEnumerable<long> programListId = null;

            foreach (var discount in discountRules.OrderByDescending(o => o.NOP))
            {
                if (discount.IsAllProgram)
                {
                    programListId = orderItems.Select(p => p.ProgramSchedule.ProgramId).Distinct();
                    eligibleOrderItems = orderItems;
                }
                else
                {
                    programListId = discount.Programs.Select(p => p.Id).Distinct();
                    eligibleOrderItems = orderItems.Where(c => (programListId.Contains(c.ProgramSchedule.ProgramId)) == true);
                }

                var numberOfDiffParticipant = eligibleOrderItems.Where(o => o.PlayerId.HasValue).Select(c => c.PlayerId).Distinct().Count();
                IEnumerable<Discount> discountSortedRules = discountRules.Where(d => d.NOP <= numberOfDiffParticipant).OrderByDescending(o => o.NOP);

                if (discountSortedRules != null && discountSortedRules.Any())
                {
                    return (discountSortedRules.FirstOrDefault());
                }
            }
            return eligibleDiscount;
        }
        private decimal getdiscountAmount(IEnumerable<OrderItem> prevDiscountOrderItems, Discount discount)
        {
            if (isApplyOnOrigin)
            {
                return prevDiscountOrderItems.Sum(o => (discount.AmountType == ChargeDiscountType.Fixed) ? discount.Amount : ((o.EntryFee * discount.Amount) / 100));
            }
            else
            {
                return prevDiscountOrderItems.Sum(o => (discount.AmountType == ChargeDiscountType.Fixed) ? discount.Amount : (((o.EntryFee - (-1 * o.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Subcategory == ChargeDiscountSubcategory.Discount).Sum(c => c.Amount))) * discount.Amount) / 100));
            }
        }

        private IQueryable<Discount> GetAllProgramAllParticipantList(long seasonId, int numberOfItems)
        {
            return GetList(seasonId).Where(d => d.Category == ChargeDiscountCategory.AllProgramAllParticipant && d.NOP <= numberOfItems);
        }

        private IQueryable<Discount> GetAllProgramDiffParticipant()
        {
            return _discountRepository.GetList().Where(d => d.Category == ChargeDiscountCategory.AllProgramDiffParticipant);
        }

        private IQueryable<Discount> GetAllProgramSameParticipant()
        {
            return _discountRepository.GetList().Where(d => d.Category == ChargeDiscountCategory.AllProgramSameParticipant);
        }

        private IQueryable<Discount> GetDiffProgramDiffParticipant(long seasonId)
        {
            return _discountRepository.GetList().Where(d => d.Category == ChargeDiscountCategory.DiffProgramDiffParticipant);
        }

        private IQueryable<Discount> GetDiffProgramSameParticipant(long seasonId)
        {
            return _discountRepository.GetList().Where(d => d.Category == ChargeDiscountCategory.DiffProgramSameParticipant);
        }

        private IQueryable<Discount> GetDiffProgramAllParticipant(long seasonId)
        {
            return _discountRepository.GetList().Where(d => d.Category == ChargeDiscountCategory.DiffProgramAllParticipant);
        }

        private IQueryable<Discount> GetSameProgramAllParticipant(long seasonId)
        {
            return _discountRepository.GetList().Where(d => d.Category == ChargeDiscountCategory.SameProgramAllParticipant);
        }

        private IQueryable<Discount> GetSameProgramDiffParticipant(long seasonId)
        {
            return _discountRepository.GetList().Where(d => d.Category == ChargeDiscountCategory.SameProgramDiffParticipant);
        }

        private IQueryable<Discount> GetSameProgramSameParticipant(long seasonId)
        {
            return _discountRepository.GetList().Where(d => d.Category == ChargeDiscountCategory.SameProgramSameParticipant);
        }

        private decimal TotalDiscountAmount
        {
            get;
            set;
        }
        private bool isApplyOnOrigin = false;
    }
}
