﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('CatalogDetailedActivitiesReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        $scope.filter = {};


        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

            $scope.Date = data.Date;
            $scope.ClubName = data.ClubName;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.Model = customReportService.getModelFromStrorage();

        $scope.Model.activeStatus = [{ Value: -1, Title: "All" }, { Value: 1, Title: "Active" }, { Value: 2, Title: "Inactive" }];
        $scope.filter.activeStatus = 1;

        $scope.CatalogListDataSource = new kendo.data.DataSource({

            transport: {
                read: {
                    url: baseUrl + "/GetCatalogDetailedActivitiesReportInfo",
                    dataType: "json",
                    type: "POST",
                    data: function () {
                        return {
                            model: $scope.Model,
                            startDate: $scope.filter.startDate,
                            endDate: $scope.filter.endDate,
                            activeStatus: $scope.filter.activeStatus,
                        };
                    },
                }
            },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
            batch: true,
        });

        $scope.CatalogListGridOptions = {
            excel: {
                allPages: true,
                fileName: "Catalog detailed activites.xlsx",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {

                var rows = e.workbook.sheets[0].rows;

                rows.unshift({
                    cells: [{
                        value: ""
                    }]
                });
                rows.unshift({
                    cells: [{
                        value: $scope.Date
                    }]
                });
                rows.unshift({
                    cells: [{
                        value: "Catalog detailed activites - " + $scope.ClubName
                    }]
                });

                cfpLoadingBar.complete();
            },
            dataSource: $scope.CatalogListDataSource,
            resizable: true,
            dataBound: function () {
                var grid = $("#CatalogListGrid").data("kendoGrid");
                customReportService.initializeDoubleScroll(grid);

                var dataSource = $("#CatalogListGrid").data("kendoGrid");
                var columns = dataSource.columns;
                columns[2].title = "Duration\n (minute)";
                columns[5].title = "Grade\n minimum";
                columns[6].title = "Grade\n maximum";
                columns[7].title = "Enrollment\n minimum";
                columns[8].title = "Enrollment\n maximum";


            },
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5,
            },
            sortable: true,
            columns: [

                {
                    field: "ProviderName",
                    title: "Provider",
                    sortable: true,
                    width: "190px"
                }, {
                    field: "CatalogName",
                    title: "Activity",
                    sortable: true,
                    width: "170px"
                }, {
                    field: "Duration",
                    title: "Duration<br>(minute)",
                    sortable: true,
                    width: "120px"
                }, {
                    field: "Week",
                    title: "Weeks",
                    width: "120px"
                }, {
                    field: "Price",
                    title: "Price",
                    sortable: true,
                    width: "120px"
                }, {
                    field: "MinGrade",
                    title: "Grade<br>minimum",
                    sortable: true,
                    width: "120px"
                }, {
                    field: "MaxGrade",
                    title: "Grade<br>maximum",
                    sortable: true,
                    width: "120px"
                }, {
                    field: "MinEnrollment",
                    title: "Enrollment<br>minimum",
                    sortable: true,
                    width: "130px"
                }, {
                    field: "MaxEnrollment",
                    title: "Enrollment<br>maximum",
                    sortable: true,
                    width: "130px"
                }, {
                    field: "UpdateDate",
                    title: "Last updated",
                    sortable: true,
                    width: "120px"
                }, {
                    field: "CountiesDistricts",
                    title: "Counties/Districts",
                    sortable: true,
                    width: "200px",
                    attributes: {
                        //"class": "table-cell",
                        style: "overflow: initial;text-overflow: initial;white-space: initial;"
                    }
                }, {
                    field: "StrActivityCategories",
                    title: "Categories",
                    sortable: true,
                    width: "200px",
                    attributes: {
                        style: "overflow: initial;text-overflow: initial;white-space: initial;"
                    }
                }, {
                    field: "ActivityDescription",
                    title: "Description",
                    sortable: true,
                    width: "900px",
                    attributes: {
                        style: "overflow: initial;text-overflow: initial;white-space: initial;"
                    }
                }

            ]
        }     


        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#CatalogListGrid").getKendoGrid().saveAsExcel();
        }


        $scope.search = function () {
            $("#CatalogListGrid").data("kendoGrid").dataSource.read();
        };

        $scope.clearFilters = function () {
            $scope.filter.endDate = "";
            $scope.filter.startDate = "";
            $scope.filter.activeStatus = 1;
            $scope.Model._SelectedProviders = [];
            $scope.Model.IsAllProviders = true;
            $("#CatalogListGrid").data("kendoGrid").dataSource.read();
        }
    }
    ]);

})();