﻿namespace Jumbula.AutomationTests.Helper
{
    class ExtraAPI
    {
        public static void Wait(int timeDuration)
        {
            System.Threading.Thread.Sleep(timeDuration);
        }
    }
}
