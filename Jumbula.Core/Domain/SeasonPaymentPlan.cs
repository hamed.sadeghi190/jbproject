﻿using System.ComponentModel.DataAnnotations.Schema;
using Jumbula.Common.Base;

namespace Jumbula.Core.Domain
{
    public class SeasonPaymentPlan : BaseEntity<long>
    {
        public long SeasonId { get; set; }
        [ForeignKey("SeasonId")]
        public virtual Season Season { get; set; }

        public long PaymentPlanId { get; set; }
        [ForeignKey("PaymentPlanId")]
        public virtual PaymentPlan PaymentPlan { get; set; }

        public bool IsDeleted { get; set; }
    }
}
