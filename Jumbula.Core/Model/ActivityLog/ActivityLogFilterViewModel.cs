﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Jumbula.Core.Model.ActivityLog
{
    public class ActivityLogFilterViewModel
    {
        public string UserName { get; set; }

        public string Activity { get; set; }

        public List<SelectListItem> Activities { get; set; }
    }
}
