﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using Jumbula.Common.Base;
using Jumbula.Common.Enums;

namespace Jumbula.Core.Domain
{
    public class PaypalTracking : BaseEntity<long>
    {
        [StringLength(127)]
        public string TrackingId { get; set; }
        public JumbulaSubSystem PaypalCaller { get; set; }
        public long CallerId { get; set; }
        public bool IsProceed { get; set; }
        public bool IsActionTaken { get; set; }
        public string Token { get; set; }
    }

}
