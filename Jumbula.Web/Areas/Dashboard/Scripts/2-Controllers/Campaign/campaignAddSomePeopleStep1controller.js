﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('campaignAddSomePeopleStep1controller', ['$scope', '$http', 'jbToast', '$stateParams', '$state', 'CampaignService', function ($scope, $http, jbToast, $stateParams, $state, CampaignService) {
        window.app.globalObjects.pageClass = 'campaigns-add-step1';
        window.app.globalObjects.initializeView();

        $scope.MC.stateParams = $stateParams;

        var baseUrl = window.location.origin + "/Dashboard/Campaign";

        $scope.campaignId = $scope.MC.stateParams.campaignId;
        $scope.pageMode = null;

        $scope.Model = {};

        $scope.isMultiSeason = false;

        if ($scope.campaignId == 'undefined' || $scope.campaignId == '') {
            $scope.pageMode = 'Create';
        } else {
            $scope.pageMode = 'Edit';
        }

        var loadData = function () {
            $http.get(baseUrl + '/GetDataForSomePeople').success(function (data, status, headers, config) {
                if (is.undefined($scope.Model.SomePeople)) {
                    $scope.Model.SomePeople = {};
                }
                $scope.Model.SomePeople = data;
                if ($scope.pageMode == 'Edit') {
                    var oldModel = CampaignService.get();

                    $scope.Model.SomePeople.Seasons = oldModel.Seasons;
                    $scope.Model.SomePeople.Programs = oldModel.Programs;
                    $scope.Model.SomePeople.DateOption = oldModel.DateOption;
                    $scope.Model.SomePeople.CustomDateStart = oldModel.CustomDateStart;
                    $scope.Model.SomePeople.CustomDateEnd = oldModel.CustomDateEnd;
                    $scope.Model.SomePeople.MinAge = oldModel.MinAge;
                    $scope.Model.SomePeople.MaxAge = oldModel.MaxAge;
                    $scope.Model.SomePeople.Gender = oldModel.Gender;
                    $scope.Model.SomePeople.IsAllSeasons = oldModel.IsAllSeasons;
                    $scope.Model.SomePeople.IsAllPrograms = oldModel.IsAllPrograms;
                    $scope.Model.SomePeople.ListId = $scope.Model.ListId;
                }
            }).error(function (data, status, headers, config) {
                jbToast.error(data);
            });
        }

        if ($scope.pageMode == 'Create') {
            $http.get(baseUrl + '/CreateCampaign', { params: { campaignType: 'SomePeople' } }).success(function (data, status, headers, config) {
                $scope.Model = data;
                loadData();
                $scope.multiSeason(1)
            });
        } else {
            $http.get(baseUrl + '/EditCampaign', { params: { id: $scope.campaignId, campaignType: 'SomePeople' } }).success(function (data, status, headers, config) {
                $scope.Model = data;
                loadData();
            });
        }

        $scope.multiSeason = function (isAllSeason) {
            var seasonsLen = ($scope.Model.SomePeople.Seasons != null) ? $scope.Model.SomePeople.Seasons.length : 0;
            $scope.Model.SomePeople.IsAllPrograms = true;

            if (seasonsLen == 1 && isAllSeason == 0) {
                $http.get(baseUrl + '/GetSeasonPrograms', { params: { seasonId: $scope.Model.SomePeople.Seasons[0] } })
                    .success(function (data, status, headers, config) {
                        $scope.isMultiSeason = false;
                        $scope.Model.SomePeople.ProgramsList = data;
                    });
            }
            else if (seasonsLen > 1 && isAllSeason != null && isAllSeason == 0) {
                $scope.Model.SomePeople.IsAllPrograms = true;
                $scope.isMultiSeason = true;
            }
            else if (isAllSeason == 1) {
                $scope.isMultiSeason = false;
                $http.get(baseUrl + '/GetSeasonPrograms', { params: { seasonId: 0, isAllPrograms: true } })
                    .success(function (data, status, headers, config) {
                        $scope.isMultiSeason = false;
                        $scope.Model.SomePeople.ProgramsList = [];
                        $scope.Model.SomePeople.ProgramsList = data;
                    });
            }
            $scope.updateChosenScope();
        }

        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };


        $scope.getProgramsList = function () {
            if ($scope.Model && $scope.Model.SomePeople && $scope.Model.SomePeople.ProgramsList) {
                return $scope.Model.SomePeople.ProgramsList;
            } else {
                return [];
            }
        }

        $scope.SaveStep1 = function () {
            CampaignService.set($scope.Model.SomePeople);
            $http.post(baseUrl + '/CampaignAddEditStep1', { model: $scope.Model }).success(function (data, status, headers, config) {
                if (data.Status == true) {
                    $state.go('EmailCampaignAddStep2', { campaignId: data.Data, campaignType: 'SomePeople' });
                }
                else {
                    $scope.MC.handleErrors($scope, data);
                    if (data.FormErrors[0].Key == "model.NoUser") {
                        jbToast.error($scope.MC.joinStringsWithBreak(data.FormErrors[0].Messages));
                    }
                }
            });
        }
    }]);
})();