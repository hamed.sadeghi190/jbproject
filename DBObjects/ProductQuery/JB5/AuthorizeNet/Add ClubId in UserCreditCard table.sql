ALTER TABLE [dbo].[UserCreditCards] DROP CONSTRAINT [FK_dbo.UserCreditCards_dbo.Clubs_ClubId]
GO


alter table usercreditcards drop column clubid

Go
 
ALTER table UserCreditCards Add ClubId int null
Go

ALTER TABLE [dbo].[UserCreditCards]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserCreditCards_dbo.Clubs_ClubId] FOREIGN KEY([ClubId])
REFERENCES [dbo].[Clubs] ([Id])