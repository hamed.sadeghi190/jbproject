﻿using SportsClub.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class SelectedReportProgramsViewModel
    {
        public List<SelectKeyValue> AllPrograms { get; set; }

        public List<string> SelectedPrograms { get; set; }

        public int ReportId { get; set; }

        public string ReportName { get; set; }

        public string SeasonDomain { get; set; }

        public bool IsAllPrograms { get; set; }
    }
}