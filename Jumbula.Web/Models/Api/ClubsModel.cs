﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Models.Api
{
    public class SchoolStudentsModel
    {
        public int ClubId { get; set; }
        public long SeasonId { get; set; }
        public DateTime? LocalDateTime { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class SchoolStudentsViewModel
    {
        public string Id { get; set; }
        public string StudentName { get; set; }
        public GenderCategories Gender { get; set; }
        public string ImageUrl { get; set; }
        public AttendanceStatus? Status { get; set; }
        public string DismissalFromEnrichment { get; set; }
        public List<string> AuthorizeAdultPickupName { get; set; }
        public List<string> AuthorizeAdultPickupPhone { get; set; }
        public string Parent1FirstName { get; set; }
        public string Parent1LastName { get; set; }
        public string Parent1PrimaryPhone { get; set; }
        public string Parent2FirstName { get; set; }
        public string Parent2LastName { get; set; }
        public string Parent2PrimaryPhone { get; set; }
        public bool IsPickedUp{ get; set; }
        public string PickupTime { get; set; }
        public string PickupName { get; set; }
        public long ProgramId { get; set; }
        public string ProgramName { get; set; }
    }
}