﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.WebLogic.UserService;
using SportsClub.Areas.Dashboard.Models;

namespace Jumbula.Web.Areas.Dashboard.Controllers
{
    public class BladeController : DashboardBaseController
    {

        public virtual ActionResult Index(string name)
        {
            
            var model = new BladeViewModel();

            var parent =
                DashboardMenuItemsModel.MenuItems.Single(m => m.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            var menuItems = DashboardMenuItemsModel.MenuItems.Where(d => d.ParentId == parent.Id);

            model.MenuItems = menuItems.ToList();

            model.Title = parent.Title;

            return View(model);
        }

        [JbAuthorize(JbAction.Campaign_View)]
        public virtual ActionResult Campaigns()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);
            var model = new List<DashboardMenuItemViewModel>();

            if (ActiveClub.Domain.Equals("flexacademies", StringComparison.OrdinalIgnoreCase) || ActiveClub.Domain.Equals("baroodycamps", StringComparison.OrdinalIgnoreCase) || ActiveClub.Domain.Equals("em", StringComparison.OrdinalIgnoreCase))
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "SystemCampaigns",
                    Title = "System campaigns",
                    Link = "SystemCampaigns",
                    Icon = "jbi-gear",
                    Action = JbAction.CampaignClassConfirmation_View
                });
            }

            model.AddRange(new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "EmailCampaignTemplates",
                    Title = "Templates",
                    Link = "EmailCampaignTemplates",
                    Icon = "jbi-email-template",
                    Action = JbAction.Campaign_EmailTemplates_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "AddNewTemplate",
                    Title = "Add new template",
                    Link = "EmailCampaignTemplateAddEdit",
                    Icon = "icon-plus-2",
                    Action = JbAction.Campaign_Template_Add,
                    ParentId = 2
                },
                new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "EmailCampaigns",
                    Title = "Email campaigns",
                    Link = "EmailCampaigns",
                    Icon = "jbi-email",
                    Action = JbAction.Campaign_Email_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "SendNewCampaign",
                    Title = "Send new campaign",
                    Link = "EmailCampaignAddBlade",
                    Icon = "icon-plus-2",
                    Action = JbAction.Campaign_Email_Send,
                    ParentId = 4
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList());


            return View(model);
        }

        [JbAuthorize(JbAction.People_View)]
        public virtual ActionResult People()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "Families",
                    Title = "Families",
                    Link = "Families",
                    Icon = "jbi-families",
                    Action = JbAction.Families_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "RegistrantPeople",
                    Title = "Participants",
                    Link = "RegistrantPeople",
                    Icon = "jbi-boy",
                    Action = JbAction.PeopleRegistrations_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "ParentRegistration",
                    Title = "Parents",
                    Link = "ParentRegistration",
                    Icon = "jbi-parents",
                    Action = JbAction.Parents_View
            },
                new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "Delinquents",
                    Title = "Delinquents",
                    Link = "Delinquents",
                    Icon = "jbi-money-2",
                    Action = JbAction.People_Delinquents_View
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.Catalogs_View)]
        public virtual ActionResult Catalog()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "CatalogItems",
                    Title = "Catalog items",
                    Link = "CatalogList",
                    Icon = "jbi-catalog-item",
                    Action = JbAction.Catalogs_ViewItem
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "CatalogSettings",
                    Title = "Catalog settings",
                    Link = "CatalogSettings",
                    Icon = "jbi-gear",
                    Action = JbAction.CatalogSettings_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "AddCatalogItem",
                    Title = "Add catalog item",
                    Link = "AddCatalog",
                    Icon = "icon-plus-2",
                    Action = JbAction.CatalogItem_Add,
                    ParentId = 1
                },
                new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "ViewCatalogs",
                    Title = "View catalogs",
                    Link = "ViewCatalogs",
                    Icon = "jbi-user-Invoice",
                    Action = JbAction.Catalog_ViewAll,
                },
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            if (club.IsSchool || club.IsPartner)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "Searchcatalog",
                    Title = "Search provider catalogs",
                    Link = "SearchCatalogs",
                    Icon = "jbi-search",
                    Action = JbAction.Catalog_Search
                });
            }

            return View(model);
        }

        [JbAuthorize(JbAction.WeeklyAgenda_View)]
        public virtual ActionResult WeeklyAgenda()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                 new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "WeeklyAgenda_View",
                    Title = "Manage agendas",
                    Link = "ManageWeeklyAgenda",
                    Icon = "jbi-weekly-email",
                    Action = JbAction.WeeklyAgenda_Settings_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "WeeklyAgendaSettings",
                    Title = "Settings",
                    Link = "WeeklyAgendaSettings",
                    Icon = "jbi-gear",
                    Action = JbAction.WeeklyAgenda_Settings_View
                },
            }
            .Where(a => !a.Action.HasValue || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.Settings_View)]
        public virtual ActionResult Settings()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel{ Id = 1, Name = "OrganizationSettings", Title = "General", Link = "OrganizationSettings",  Icon = "jbi-gear", Action = JbAction.Settings_General_View},
                new DashboardMenuItemViewModel{ Id = 2, Name = "PaymentSettings", Title = "Payment methods", Link = "PaymentSettings", Icon="jbi-credit-card1", Action = JbAction.Settings_PaymentSettings_View},
                new DashboardMenuItemViewModel{ Id = 3, Name = "CheckOutSetting", Title = "Checkout", Link = "CheckOutSetting", Icon="jbi-checkout", Action = JbAction.Settings_Checkout_View},
                new DashboardMenuItemViewModel{ Id = 4, Name = "AppearanceSetting", Title = "Appearance", Link = "AppearanceSetting", Icon="jbi-gear", Action = JbAction.Settings_Appearance_View},
                new DashboardMenuItemViewModel{ Id = 5, Name = "AccountSettings", Title = "Account", Link = "AccountSettings", Icon="jbi-account", Action = JbAction.Settings_Account_View},
                new DashboardMenuItemViewModel{ Id = 6, Name = "PartnerSettings", Title = "Partner", Link = "PartnerSettings", Icon="jbi-account", Action = JbAction.Settings_Partner_View},
                new DashboardMenuItemViewModel{ Id = 9, Name = "Holidays", Title = "Holiday calendar", Link = "HolidaysSetting", Icon = "jbi-schedule", Action = JbAction.Settings_Holidays_View },
                new DashboardMenuItemViewModel{ Id = 7, Name = "jbHomeSiteSettings", Title = "Jumbula home site", Link = "JbHomeSiteSettings()", Icon="jbi-logo", Action = JbAction.Settings_JbHomeSite},
                new DashboardMenuItemViewModel{ Id = 8, Name = "AdvanceSetting", Title = "Advanced", Link = "AdvanceSetting", Icon = "jbi-gear", Action = JbAction.Settings_Advance_View },
                new DashboardMenuItemViewModel{ Id = 9, Name = "MobileAppSetting", Title = "Mobile app", Link = "MobileAppSetting", Icon = "jbi-mobile", Action = JbAction.Settings_MobileApp_View },
                new DashboardMenuItemViewModel{ Id = 10, Name = "PolicySetting", Title = "Policies", Link = "PolicySetting", Icon = "jbi-money-4", Action = JbAction.Settings_MobileApp_View },
                new DashboardMenuItemViewModel{ Id = 9, Name = "JBContentsSettings", Title = "Notification texts", Link = "JBContentsSettings", Icon = "jbi-catalog", Action = JbAction.Settings_Notifications_View },

            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            if (ActiveClub.IsSchool)
            {

                var clubBaseInfo = this.GetCurrentClubBaseInfo();

                if (allowedActions.Contains(JbAction.Season_SchoolAdvancedSettings_View) && clubBaseInfo.HasPartner)
                {
                    var partnerId = clubBaseInfo.PartnerId;

                    var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(partnerId.Value);

                    if (partnerSetting.PortalParameters.ShowSchoolAdvancedSetting)
                    {
                        model.Add(new DashboardMenuItemViewModel
                        {
                            Id = 8,
                            Name = "SchoolSetting",
                            Title = "EM - School",
                            Link = "SchoolSetting",
                            Icon = "jbi-school",
                            Action = JbAction.Settings_School_View
                        });
                    }
                }
            }

            if (!ActiveClub.HasPartner)
            {
                model.RemoveAll(item => item.Name == "AdvanceSetting");
            }

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            if (club.IsPartner)
            {
                model.RemoveAll(item => item.Name == "MobileAppSetting");
            }

            return View(model);
        }

        //[JbAuthorize(JbAction.ViewSettings)]
        public virtual ActionResult JbHomeSiteSettings()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>()
            {
                new DashboardMenuItemViewModel { Id = 1, Name = "ViewJbHomeSite", Title = "View site", Link = "/view",  Icon = "jbi-show", Action = JbAction.Settings_JbHomeSite, Type = MenuLinkType.ExternalLink},
                new DashboardMenuItemViewModel{ Id= 2, Name = "EditJbHomeSite", Title = "Edit site", Link = "/edit", Icon="jbi-pencil", Action = JbAction.Settings_JbHomeSite, Type = MenuLinkType.ExternalLink},
                //new DashboardMenuItemViewModel{ Id= 3, Name = "TourJbHomeSite", Title = "Tour", Link = "ToursJbHomeSite", Icon="jbi-tour", Action = JbAction.Settings_JbHomeSite},
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult MobileAppSettings()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>()
                {
                    new DashboardMenuItemViewModel
                    {
                        Id = 1,
                        Name = "OnSiteInfo",
                        Title = "Onsite information",
                        Link = "MobileAppSettingOnSiteInfo",
                        Icon = "jbi-info",
                        Action = JbAction.SchoolSetting_OnSiteInfo_View
                    },
                }
                .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult JBContentsSettings()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>()
            {
                  new DashboardMenuItemViewModel
                    {
                        Id = 1,
                        Name = "Emails",
                        Title = "Emails",
                        Link = "EmailsContent",
                        Icon = "jbi-email",
                        Action = JbAction.SchoolSetting_ProgramPolicy_View
                    },

                new DashboardMenuItemViewModel{
                    Id = 2,
                    Name = "Reports",
                    Title = "Reports",
                    Link = "ReportsContent",
                    Icon ="jbi-reports",
                    Action = JbAction.SchoolSetting_AutoChargePolicy_View
                },

            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            var club = Ioc.ClubBusiness.Get(ActiveClub.Id);

            if (club.IsPartner)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "Catalogs",
                    Title = "Catalog",
                    Link = "CatalogsContent",
                    Icon = "jbi-catalog",
                    Action = JbAction.SchoolSetting_AutoChargePolicy_View
                });
            }

            return View(model);
        }

        public virtual ActionResult PolicySettings()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>()
            {
                  new DashboardMenuItemViewModel
                    {
                        Id = 2,
                        Name = "ProgramsPolicy",
                        Title = "Programs",
                        Link = "ProgramsPolicy",
                        Icon = "jbi-gear",
                        Action = JbAction.SchoolSetting_ProgramPolicy_View
                    },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "DuplicateEnrollment",
                    Title = "Duplicate enrollments",
                    Link = "DuplicateEnrollmentPolicy",
                    Icon = "jbi-gear",
                    Action = JbAction.SchoolSetting_DuplicateEnrollment_View
                },
                new DashboardMenuItemViewModel{
                    Id = 1,
                    Name = "AutoChargePolicy",
                    Title = "Installment auto charge",
                    Link = "AutoChargePolicy",
                    Icon ="jbi-money-1",
                    Action = JbAction.SchoolSetting_AutoChargePolicy_View
                },
                //new DashboardMenuItemViewModel
                //{
                //    Id = 2,
                //    Name = "DeinquentPolicy",
                //    Title = "Delinquent accounts",
                //    Link = "DelinquentPolicy",
                //    Icon = "jbi-money-2",
                //    Action = JbAction.SchoolSetting_DelinquentPolicy_View
                //}
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            if (!ActiveClub.HasPartner)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "DeinquentPolicy",
                    Title = "Delinquent accounts",
                    Link = "DelinquentPolicy",
                    Icon = "jbi-money-2",
                    Action = JbAction.SchoolSetting_DelinquentPolicy_View
                });
            }

            return View(model);
        }

        [JbAuthorize(JbAction.Settings_Account_View)]
        public virtual ActionResult AccountSettings()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "Overview",
                    Title = "Overview",
                    Link = "AccountSettings",
                    Icon = "jbi-show",
                    Action = JbAction.AccountSetting_Overview_View
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "ChangePlan",
                    Title = "Select plan",
                    Link = "AccountSettingChangePlan",
                    Icon = "jbi-change",
                    Action = JbAction.AccountSetting_ChangePlan_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "Billing",
                    Title = "Billing",
                    Link = "AccountSettingBilling",
                    Icon = "jbi-billing",
                    Action = JbAction.AccountSetting_Billing_View
                },
                    new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "ManageCreditCard",
                    Title = "Add billing method",
                    Link = "AccountSettingManageBilling({type:'Add'})",
                    Icon = "icon-plus-2",
                    Action = JbAction.AccountSetting_Billing_Add,
                    ParentId = 3
                },
                new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "BillingHistory",
                    Title = "Billing history",
                    Link = "AccountSettingBillingHistory",
                    Icon = "icon-history",
                    Action = JbAction.AccountSetting_Billing_History_View
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }
        [JbAuthorize(JbAction.Settings_School_View)]
        public virtual ActionResult SchoolSettings()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "SchoolInfo",
                    Title = "School information",
                    Link = "SchoolSettingInfo",
                    Icon = "jbi-info",
                    Action = JbAction.SchoolSetting_Info_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "ProgramInfo",
                    Title = "Program information",
                    Link = "SchoolSettingProgramInfo",
                    Icon = "jbi-info",
                    Action = JbAction.SchoolSetting_ProgramInfo_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "OnSiteInfo",
                    Title = "On-Site information",
                    Link = "SchoolSettingOnSiteInfo",
                    Icon = "jbi-info",
                    Action = JbAction.SchoolSetting_OnSiteInfo_View
                },
                new DashboardMenuItemViewModel
                    {
                        Id = 4,
                        Name = "RegistrationInfo",
                        Title = "Registration information",
                        Link = "SchoolSettingRegistrationInfo",
                        Icon = "jbi-info",
                        Action = JbAction.SchoolSetting_RegistrationInfo_View
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.Settings_Advance_View)]
        public virtual ActionResult AdvanceSettings()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "Insurance",
                    Title = "Insurance",
                    Link = "AccountSettingInsurance",
                    Icon = "jbi-insurance",
                    Action = JbAction.AccountSetting_Insurance_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "BackgroundCheck",
                    Title = "Background Check Certification Letter",
                    Link = "AdvanceSettingBackgroundCheck",
                    Icon = "jbi-info",
                    Action = JbAction.AdvanceSetting_BackgroundCheck_View
                },
                   new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "LogoSetting",
                    Title = "Logo display",
                    Link = "LogoSetting",
                    Icon = "jbi-info",
                    Action = JbAction.LogoSetting_View
                },

            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult SeasonSchoolSetting()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>();

            if (ActiveClub.IsSchool)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "GeneralInfo",
                    Title = "General information",
                    Link = "SeasonSchoolSettingGeneralInfo({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-info",
                    Action = JbAction.Season_SchoolSetting_GeneralInfo_View
                });
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "DetailInfo",
                    Title = "Season details",
                    Link = "SeasonSchoolSettingDetailInfo({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-details",
                    Action = JbAction.Season_SchoolSetting_DetailInfo_View
                });
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "ScheduleInfo",
                    Title = "Schedule information",
                    Link = "SeasonSchoolSettingScheduleInfo({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-clock1",
                    Action = JbAction.Season_SchoolSetting_ScheduleInfo_View
                });
            }
            return View(model);
        }

        public virtual ActionResult SeasonSchoolSettingsRegistration()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "ProgramInfo",
                    Title = "Program information",
                    Link = "SeasonSchoolSettingProgramInfo({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-info",
                    Action = JbAction.Season_SchoolSetting_GeneralInfo_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "SeasonSchoolSettingForms",
                    Title = "Form and waivers",
                    Link = "SeasonSchoolSettingForms({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-details",
                    Action = JbAction.Season_SchoolSetting_DetailInfo_View
                },
                   new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "MultiPaymentPlan",
                    Title = "Payment plans",
                    Link = "SeasonSchoolPaymentPlan({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-cash",
                    Action = JbAction.Season_SchoolSetting_PaymentPlan
                },
                new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "AdditionalOptions",
                    Title = "Additional options",
                    Link = "SeasonSchoolSettingAdditionalOptions({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-info",
                    Action = JbAction.Season_SchoolSetting_ScheduleInfo_View
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.Toolbox_view)]
        public virtual ActionResult Toolbox()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
               new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "Forms",
                    Title = "Forms",
                    Link = "ClubForms()",
                    Icon = "jbi-details",
                    Action = JbAction.Forms_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "GA",
                    Title = "GA",
                    Link = "GoogleAnalytics()",
                    Icon = "jbi-gear",
                    Action = JbAction.GaTrackingId_View
                },
            }
          .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }
        public virtual ActionResult ClubForms()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);
            var model = new List<DashboardMenuItemViewModel>
            {
               new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "FollowUpForm",
                    Title = "Follow-up forms",
                    Link = "ClubFollowUpForms",
                    Icon = "jbi-details",
                    Action = JbAction.FollowUpForms_View
                },
            }
           .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);

        }

        [JbAuthorize(JbAction.Staff_View)]
        public virtual ActionResult Staff()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "ManageStaffs",
                    Title = "Manage staff",
                    Link = "ManageStaffs",
                    Icon = "jbi-user-setting",
                    Action = JbAction.Staff_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "AddStaff",
                    Title = "Add staff",
                    Link = "InviteStaff",
                    Icon = "icon-plus-2",
                    Action = JbAction.Staff_Add,
                    ParentId = 1
                },

            };

            if (Ioc.ClubBusiness.AdminIsFromPartner(this.GetCurrentUserName(), this.GetCurrentClubId()))
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "AddStaffFromPartner",
                    Title = "Add staff from partner",
                    Link = "AddStaffFromPartner",
                    Icon = "icon-plus-2",
                    Action = JbAction.Staff_Add_From_Partner,
                    ParentId = 1
                });
            }

            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.invoice_View)]
        public virtual ActionResult Invoices()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);
            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "ManageInvoices",
                    Title = "Manage invoices",
                    Link = "Invoices",
                    Icon = "jbi-user-Invoice",
                    Action = JbAction.invoice_View
                },

                new DashboardMenuItemViewModel
                {
                    Id=2,
                    Name="AddInvoice",
                    Title="Add invoice",
                    Link="Families",
                    Icon="icon-plus-2",
                    Action=JbAction.invoice_View,
                    ParentId=1,

                }
            }
             .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);

        }
        [JbAuthorize(JbAction.Member_View)]
        public virtual ActionResult Clubs()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                  new DashboardMenuItemViewModel
               {
                    Id = 1,
                    Name = "Schools",
                    Title = "Schools",
                    Link = "Clubs({type:'Schools'})",
                    Icon = "",
                    Action = JbAction.Member_Add
                },
                     new DashboardMenuItemViewModel
               {
                    Id = 2,
                    Name = "AddSchools",
                    Title = "Add school",
                    Link = "ClubManage({clubId:0,type:'School'})",
                    Icon = "icon-plus-2",
                    Action = JbAction.Member_Add,
                    ParentId = 1
                },
                     new DashboardMenuItemViewModel
               {
                    Id = 3,
                    Name = "Providers",
                    Title = "Providers",
                    Link = "Clubs({type:'Providers'})",
                    Icon = "",
                    Action = JbAction.Member_Add
                },
                    new DashboardMenuItemViewModel
               {
                    Id = 4,
                    Name = "AddProvider",
                    Title = "Add provider",
                    Link = "ClubManage({clubId:0,type:'Provider'})",
                    Icon = "icon-plus-2",
                    Action = JbAction.Member_Add,
                    ParentId = 3
                },
                  new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "SchoolDistrict",
                    Title = "School districts",
                    Link = "SchoolDistrict",
                    Icon = "",
                    Action = JbAction.Districts_View
                },
                  new DashboardMenuItemViewModel
                 {
                    Id = 6,
                    Name = "AddSchoolDistrict",
                    Title = "Add district",
                    Link = "AddClubDistrict",
                    Icon = "icon-plus-2",
                    Action = JbAction.District_Add,
                    ParentId = 5
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "Subsidies",
                    Title = "Government subsidies",
                    Link = "Subsidies",
                    Icon = "",
                    Action = JbAction.Subsidies_View
                },
                  new DashboardMenuItemViewModel
                 {
                    Id = 8,
                    Name = "AddSubsidy",
                    Title = "Add subsidy",
                    Link = "AddSubsidy",
                    Icon = "icon-plus-2",
                    Action = JbAction.Subsidy_Add,
                    ParentId = 7
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            var currentPartner = this.GetCurrentClubBaseInfo();

            if (currentPartner.Domain.Equals(Constants.YoungRembrandstDomain))
            {
                model.Where(i => i.Name.Equals("Providers")).ToList().ForEach(i => i.Title = "Franchise/Branches");
                model.Where(i => i.Name.Equals("AddProvider")).ToList().ForEach(i => i.Title = "Add Franchise/Branch");
            }

            return View(model);
        }

        [JbAuthorize(JbAction.PortalReport_View)]
        public virtual ActionResult PortalReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);
            var clubBaseInfo = this.GetCurrentClubBaseInfo();

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
               {
                    Id = 1,
                    Name = "Catalog",
                    Title = "Catalog",
                    Link = "CatalogReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Catalog
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "School",
                    Title = "School",
                    Link = "SchoolReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_SchoolMemberList
                },

                new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "Planning-Reports",
                    Title = "Planning",
                    Link = "PlanningReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Planning
                },
                new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "Finance-Reports",
                    Title = "Finance-EM",
                    Link = "FinanceReportsBlade",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Finance
                },

                 new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "Finance",
                    Title = "Finance",
                    Link = "SystemFinanceReport",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_SystemFinance
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 8,
                    Name = "ParentMailingList",
                    Title = "Parent mailing list",
                    Link = "ParentMailingListReport",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_ParentMailingList
                },
                   new DashboardMenuItemViewModel
                {
                    Id = 11,
                    Name = "Graduate",
                    Title = "Participants by grade",
                    Link = "GraduateSelectGradeReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Graduate
                },
                    new DashboardMenuItemViewModel
                {
                    Id = 12,
                    Name = "Installments",
                    Title = "Installments",
                    Link = "ReportsSystemFinanceInstallments",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_SystemFinance_Installments
                },
                         new DashboardMenuItemViewModel
                {
                    Id = 13,
                    Name = "AutoCharge",
                    Title = "Auto charge",
                    Link = "SystemAutoChargeReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_System_AutoCharge
                },
                new DashboardMenuItemViewModel
                {
                    Id = 14,
                    Name = "CustomReportClubReport",
                    Title = "Custom",
                    Link = "CustomReportClubReport",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_CustomReportClub
                },
                new DashboardMenuItemViewModel
                {
                    Id = 15,
                    Name = "Form-Reports",
                    Title = "Forms",
                    Link = "FormReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Form
                },
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            if (this.GetCurrentUserRole() == RoleCategory.Partner)
            {
                if (clubBaseInfo.Domain != "em")
                {
                    model.RemoveAll(m => m.Title == "Finance-EM");
                }
            }
            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

            model.Add(
              new DashboardMenuItemViewModel
              {
                  Id = 12,
                  Name = "Finance-Reports",
                  Title = "Finance",
                  Link = "FinanceReports",
                  Icon = "icon-stats-2",
                  Action = JbAction.PortalReport_Enrollments
              });

            if (club.ClubType.EnumType == ClubTypesEnum.Partner)
            {
                model.Add(
                  new DashboardMenuItemViewModel
                  {
                      Id = 13,
                      Name = "Rosters-Reports",
                      Title = "Rosters",
                      Link = "RostersReportsBlade",
                      Icon = "icon-stats-2",
                      Action = JbAction.PortalReport_Enrollments
                  });

                model.Add(
                    new DashboardMenuItemViewModel
                    {
                        Id = 14,
                        Name = "CustomReportPortal",
                        Title = "Custom",
                        Link = "CustomReportPortal",
                        Icon = "icon-stats-2",
                        Action = JbAction.PortalReport_CustomReport
                    });

            }

            //if (club.IsSchool)
            //{
            //    model.Add(
            //      new DashboardMenuItemViewModel
            //      {
            //          Id = 14,
            //          Name = "OnsitePersonCheckin",
            //          Title = "Onsite person check in",
            //          Link = "OnsitePersonCheckinReports",
            //          Icon = "icon-stats-2",
            //          Action = JbAction.PortalReport_OnsitePersonCheckin
            //      });
            //}


            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();
            return View(model);
        }

        [JbAuthorize(JbAction.PortalReport_View)]
        public virtual ActionResult CatalogReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "CatalogOverviewActivity",
                    Title = "Catalog overview",
                    Link = "CatalogOverviewActivity",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_CatalogOverviewActivity
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "CatalogUpdateDate",
                    Title = "Catalog activities",
                    Link = "CatalogUpdateDate",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_UpdateReport
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "CatalogDetailedActivitiesReport",
                    Title = "Catalog detailed activities",
                    Link = "DeafultReportSelectProviders({reportType:'CatalogDetailedActivities'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_CatalogList
                },
                new DashboardMenuItemViewModel
            {
                    Id = 4,
                    Name = "Title1Discount",
                    Title = "Title 1 discount",
                    Link = "Title1Discount",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Title1Discount
                },
                //new DashboardMenuItemViewModel { Id = 3, Name = "FairfaxProviders", Title = "Fairfax providers", Link = "FairfaxProviders", Icon = "icon-stats-2", Action = JbAction.PortalReport_FiarFaxCounty},
                new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "SelectCounty",
                    Title = "County providers",
                    Link = "SelectCounty",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_FiarFaxCounty
                }
            }
                .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.PortalReport_View)]
        public virtual ActionResult SchoolReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "SchoolMemberList",
                    Title = "School member list",
                    Link = "SchoolMemberList",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_UpdateReport
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.PortalReport_View)]
        public virtual ActionResult DiscountReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "DiscountsList",
                    Title = "Scholarship/discount",
                    Link = "SelectDiscountDate({reportType:'ScholarshipDiscount'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Discount
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "JustDiscount",
                    Title = "Discount",
                    Link = "SelectDiscountDate({reportType:'Discount'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Discount
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "JustScholarship",
                    Title = "Scholarship",
                    Link = "SelectDiscountDate({reportType:'Scholarship'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Discount
                }

            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult FinanceReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "ProviderSeason",
                    Title = "Provider Seasons Reconciliation",
                    Link = "SchoolSeasonsFinanceReports({reportType:'ProviderSeason'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Finance
                },


                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "SchoolSeasonReconciliation",
                    Title = "School Season Reconciliation",
                    Link = "SchoolSeasonReconciliationReports({reportType:'SchoolSeason'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Finance
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "EMSeasonReconciliation",
                    Title = "EM Season Reconciliation",
                    Link = "EMSeasonReconciliationReports({reportType:'EMSeason', reportName: 'EMSeasonReconciliationReport'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Finance
                },
                new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "SchoolReconciliationInfo",
                    Title = "School Reconciliation Info",
                    Link = "SchoolReconciliationInfoReports({reportType:'EMSeason'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Finance
                },
                new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "ReconciliationPrep",
                    Title = "Reconciliation Prep",
                    Link = "EMSeasonReconciliationReports({reportType:'EMSeason', reportName: 'ReconciliationPrepReport'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Finance
                },
                new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "MemberTaxIdStatus",
                    Title = "Member tax ID status",
                    Link = "MemberTaxIdStatusReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Finance
                },
                new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "SessionStatementData",
                    Title = "Session statement data",
                    Link = "SessionStatementDataReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Finance
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }


        public virtual ActionResult EnrollmentReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);
            var model = new List<DashboardMenuItemViewModel>();

            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

            model = new List<DashboardMenuItemViewModel>
            {
                   new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "Schoolarship-Discount",
                    Title = "Discount",
                    Link = "DiscountsReportsBlade",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Discount
                },
                   new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "Donation-Reports",
                    Title = "Donation",
                    Link = "DonationsReport",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Donations
                },
                   new DashboardMenuItemViewModel
                {
                    Id = 9,
                    Name = "FamilyCreditCard",
                    Title = "Family payment method",
                    Link = "FamilyCreditCardReport",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_FamilyCreditCard
                },
                   new DashboardMenuItemViewModel
                {
                    Id = 10,
                    Name = "BulkDependentCareReceipt",
                    Title = "Bulk dependent care receipt",
                    Link = "BulkDependentCareReceiptReport",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_BulkDependentCareReceipt
                },
            }.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();


            if (club.ClubType.EnumType == ClubTypesEnum.Partner)
            {
                model.Add(
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "Master-enrollments",
                    Title = "Master enrollments",
                    Link = "EnrollmentReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Enrollments
                });
                model.Add(
              new DashboardMenuItemViewModel
              {
                  Id = 2,
                  Name = "school-enrollments",
                  Title = "Enrollments by school",
                  Link = "SchoolEnrollmentsReports",
                  Icon = "icon-stats-2",
                  Action = JbAction.PortalReport_Enrollments
              });
                model.Add(
               new DashboardMenuItemViewModel
               {
                   Id = 3,
                   Name = "school-renewal",
                   Title = "PTA annual renewal",
                   Link = "RenewalPTAReports",
                   Icon = "icon-stats-2",
                   Action = JbAction.PortalReport_Enrollments
               });
                model.Add(
                new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "ClassProfits",
                    Title = "Class profits",
                    Link = "ClassProfitsReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.PortalReport_Enrollments
                });
                model.Add(
                 new DashboardMenuItemViewModel
                 {
                     Id = 5,
                     Name = "Invoices",
                     Title = "Invoices",
                     Link = "DeafultReportSelectSchools",
                     Icon = "icon-stats-2",
                     Action = JbAction.PortalReport_SchoolInvoices
                 });
                //model.Add(
                //    new DashboardMenuItemViewModel
                //    {
                //        Id = 6,
                //        Name = "General",
                //        Title = "Transactions",
                //        Link = "GeneralTransactionsReport",
                //        Icon = "icon-stats-2",
                //        Action = JbAction.PortalReport_SchoolInvoices
                //    });
                model.Add(
                    new DashboardMenuItemViewModel
                    {
                        Id = 7,
                        Name = "General",
                        Title = "Transactions",
                        Link = "GeneralTransactionsSubscriptionReport",
                        Icon = "icon-stats-2",
                        Action = JbAction.PortalReport_SchoolInvoices
                    });
                model.Add(
                    new DashboardMenuItemViewModel
                    {
                        Id = 8,
                        Name = "Installment",
                        Title = "Installments",
                        Link = "InstallmentsReport",
                        Icon = "icon-stats-2",
                        Action = JbAction.PortalReport_Installments
                    });
                model.Add(
                    new DashboardMenuItemViewModel
                    {
                        Id = 9,
                        Name = "ClientCreditCardsReport",
                        Title = "User credit cards",
                        Link = "ClientCreditCardsReport",
                        Icon = "icon-stats-2",
                        Action = JbAction.PortalReport_ClientCreditCards
                    });
                model.Add(
                    new DashboardMenuItemViewModel
                    {
                        Id = 10,
                        Name = "Donation-Reports",
                        Title = "Donation",
                        Link = "DonationsReport",
                        Icon = "icon-stats-2",
                        Action = JbAction.PortalReport_Donations
                    });
            }

            if (club.ClubType.EnumType != ClubTypesEnum.Partner)
            {
                model.Add(
                        new DashboardMenuItemViewModel
                        {
                            Id = 7,
                            Name = "Installment",
                            Title = "Installments",
                            Link = "InstallmentsReport",
                            Icon = "icon-stats-2",
                            Action = JbAction.PortalReport_Installments
                        });
                model.Add(
                        new DashboardMenuItemViewModel
                        {
                            Id = 7,
                            Name = "ClientCreditCardsReport",
                            Title = "User credit cards",
                            Link = "ClientCreditCardsReport",
                            Icon = "icon-stats-2",
                            Action = JbAction.PortalReport_ClientCreditCards
                        });
            }

            model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.PortalReport_Rosters)]
        public virtual ActionResult RostersReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
              new DashboardMenuItemViewModel
               {
                Id = 1,
                Name = "Teacher-master-list",
                Title = "Teacher master list",
                Link = "TeachersReport",
                Icon = "icon-stats-2",
                Action = JbAction.PortalReport_Rosters
               },
              new DashboardMenuItemViewModel
               {
                Id = 2,
                Name = "SchoolProgramsList-Check-In/Out",
                Title = "Sign-out sheet - advanced",
                Link = "SchoolProgramsListCheckInOut",
                Icon = "icon-stats-2",
                Action = JbAction.PortalReport_Rosters
               },
              new DashboardMenuItemViewModel
               {
                Id = 3,
                Name = "InternalRoster",
                Title = "Internal roster",
                Link = "InternalRosterReports",
                Icon = "icon-stats-2",
                Action = JbAction.PortalReport_Rosters
               },
              new DashboardMenuItemViewModel
               {
                Id = 4,
                Name = "CampRoster",
                Title = "Camp roster",
                Link = "CampRosterReportsPortal",
                Icon = "icon-stats-2",
                Action = JbAction.PortalReport_Rosters
               }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult Reports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "RegistrationDataReports",
                    Title = "Registration data",
                    Link = "RegistrationDataReports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RegistrationData
                },
                new DashboardMenuItemViewModel
               {
                    Id = 2,
                    Name = "FinanceReport",
                    Title = "Finance",
                    Link = "FinanceReport({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance
                },
                 new DashboardMenuItemViewModel
               {
                    Id = 3,
                    Name = "RosterReports",
                    Title = "Rosters",
                    Link = "RosterReports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Rosters
                },
               new DashboardMenuItemViewModel
               {
                    Id = 4,
                    Name = "CapacityReports",
                    Title = "Capacities and waitlists",
                    Link = "CapacityReports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Capacities
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "SubscriptionsReports",
                    Title = "Subscriptions",
                    Link = "SubscriptionsReports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Subscription
                },
                  new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "MedicalReports",
                    Title = "Medical",
                    Link = "MedicalReports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Medical
                },
                  new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "OperationsReports",
                    Title = "Operations",
                    Link = "OperationsReports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Operations
                },
                new DashboardMenuItemViewModel
                {
                    Id = 8,
                    Name = "CustomReports",
                    Title = "Custom",
                    Link = "CustomReports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Custom
                },
                new DashboardMenuItemViewModel
                {
                    Id = 9,
                    Name = "MobileAppReports",
                    Title = "Mobile app",
                    Link = "MobileAppReports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_MobileApp
                },

            };

            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult RegistrationDataReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "RegistrationForms",
                    Title = "Registration forms",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'RegistrationForm',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RegistrationData
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "Follow-upForms",
                    Title = "Follow-up forms",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'FollowupForm',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RegistrationData
                },
               }
               .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult FinanceReport()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                  new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "GeneralFinanceReport",
                    Title = "General",
                    Link = "FinanceReportRun({seasonDomain: MC.stateParams.seasonDomain,reportName:'finance',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance_School_Provider
                },
                    new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "GeneralTransactionFinanceReport",
                    Title = "General transactions",
                    Link = "GeneralTransactionFinanceReportRun({seasonDomain: MC.stateParams.seasonDomain,reportName:'financeTransaction',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance_School_Provider
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "BalanceReport",
                    Title = "Balance",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'Balance',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance_School_Provider
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "ChargeDiscountReport",
                    Title = "Charge discount",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'ChargeDiscount',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance_School_Provider
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "InstallmentReport",
                    Title = "Installment report",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'Installment',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance_School_Provider
                },
                new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "CouponReport",
                    Title = "Coupon report",
                    Link = "CouponReportRun({seasonDomain: MC.stateParams.seasonDomain,reportName:'Coupon',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance_Coupon_Coordinator
                },

               };


            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

            if (club.IsProvider)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "ProviderFinancialReport",
                    Title = "Provider financial",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'Provider Financial',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance_Provider
                });
            }

            if (club.IsSchool)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "CoordinatorFinancialReport",
                    Title = "Coordinator financial",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'Coordinator Financial',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Finance_Coupon_Coordinator
                });
            }

            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult RosterReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "AdminRosterReport",
                    Title = "Class roster",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'Roster',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Rosters_Admin
                },

                 new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "AdvanceRosterReport",
                    Title = "Class roster - advanced",
                    Link = "AdvanceClassRosterReportRun({seasonDomain: MC.stateParams.seasonDomain,reportName:'AdvanceRoster',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Rosters_Instructor
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "CampRosterReport",
                    Title = "Camp roster",
                    Link = "CampRosterReport({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_CampRoster
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "StudentAttendance",
                    Title = "Student attendance",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'StudentAttendance',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_StudentAttendance
                },
                  new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "StudentSummaryAttendance",
                    Title = "Student attendance - summary",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'StudentSummaryAttendance',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_StudentSummaryAttendance
                },
                new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "CustomAttendanceReports",
                    Title = "Attendance sheet",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'CustomAttendance',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_CustomAttendance
                },
                new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "AttendanceSheetAdvanceReports",
                    Title = "Attendance sheet - advanced",
                    Link = "AttendanceSheetAdvanced({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_AttendanceSheetAdvanced
                },
                new DashboardMenuItemViewModel
                {
                    Id = 8,
                    Name = "CheckinReports",
                    Title = "Instructor check-in",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'InstructorCheckin',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Checkin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 9,
                    Name = "SignOutSheetReports",
                    Title = "Sign-out sheet",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'SignOutSheet',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_SignOutSheet
                },
                new DashboardMenuItemViewModel
                {
                    Id = 10,
                    Name = "DismissalInformationReports",
                    Title = "Dismissal information",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'DismissalInformation',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_DismissalInformation
                },
                new DashboardMenuItemViewModel
                {
                    Id = 11,
                    Name = "DailyDismissalReports",
                    Title = "Daily dismissal",
                    Link = "DailyDismissalReportRun({seasonDomain: MC.stateParams.seasonDomain,reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_DailyDismissal
                },
                new DashboardMenuItemViewModel
                {
                    Id = 12,
                    Name = "SignOutSheetAdvanced",
                    Title = "Sign-out sheet - advanced",
                    Link = "SignOutSheetAdvanced({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 13,
                    Name = "SignOutSheetAdvanced",
                    Title = "Sign-out sheet - advanced v2",
                    Link = "SignOutSheetAdvancedV2({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 14,
                    Name = "SignOutSheetAdvanced",
                    Title = "Sign-out sheet - advanced v3",
                    Link = "SignOutSheetAdvancedV3({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 15,
                    Name = "SignOutSheetAdvanced",
                    Title = "Sign-out sheet - advanced v4",
                    Link = "SignOutSheetAdvancedV4({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 16,
                    Name = "InternalRoster",
                    Title = "Internal roster",
                    Link = "InternalRosterSchoolReport({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 17,
                    Name = "InternalRoster",
                    Title = "Internal roster - profile",
                    Link = "InternalRosterSchoolReport_v2({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 18,
                    Name = "InternalRosterV3",
                    Title = "Internal roster v2",
                    Link = "InternalRosterSchoolReport_v3({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                }
        };

            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

            if (club.IsProvider)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "InstructorListReport",
                    Title = "Instructor list",
                    //Link = "DeafultReportSelectProviders({seasonDomain: MC.stateParams.seasonDomain,reportName:'InstructorList',reportType:'Default'})",
                    Link = "InstructorListReport({seasonDomain: MC.stateParams.seasonDomain,reportName:'InstructorList',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_InstructorList
                });
            }

            if (club.IsSchool)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "ProviderContactInformationReport",
                    Title = "Provider contact information",
                    Link = "ProviderContactInformationReport({seasonDomain: MC.stateParams.seasonDomain,reportName:'ProviderContactInformation',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_ProviderContactInformation
                });
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 9,
                    Name = "ParentEmailsListReport",
                    Title = "Parent emails list",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'ParentEmailsList',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_ParentEmailsList
                });
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 10,
                    Name = "RoomAssignmentsAdminReport",
                    Title = "Room assignments",
                    Link = "RoomAssignmentsAdminReport({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                });
            }
            else
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 8,
                    Name = "InstructorAssignmentsReport",
                    Title = "Instructor assignments",
                    Link = "InstructorAssignmentsReport({seasonDomain: MC.stateParams.seasonDomain,reportName:'InstructorAssignments',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_InstructorAssignments
                });
            }


            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult CapacityReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "GeneralCapacityReport",
                    Title = "General",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'Capacity',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Capacities
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "CapacityGraphReport",
                    Title = "Graph",
                    Link = "FullcapacityChart({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_CapacitiesGraph
                }
            };
            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
            if (club.IsSchool || club.ClubType.EnumType == ClubTypesEnum.SportClub)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "PerRegistrationReports",
                    Title = "Pre registration",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'PreRegistration',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_PerRegistration
                });
            }

            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        //[JbAuthorize(JbAction.ProtalReport_Planning)]
        public virtual ActionResult PlanningReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "Provider-Contact-Information",
                    Title = "Provider contact information",
                    Link = "SelectSchoolSeason({reportType:'ProviderConatact'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Planning
                },new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "Room-Assignment",
                    Title = "Room assignment",
                    Link = "SelectSchoolSeason({reportType:'RoomAssignment'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Planning
                },new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "Season-Processing-Dates",
                    Title = "Season processing dates",
                    Link = "SelectSeason({reportType:'SeasonProcessing'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Planning
                },new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "ADM24-Data",
                    Title = "ADM24 Data",
                    Link = "SelectSchoolSeason({reportType:'ADM24Data'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Planning
                },new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "Insurance-Report",
                    Title = "Insurance",
                    Link = "InsuranceReport",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Planning
                },
                new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "ProviderAddressListReport",
                    Title = "Provider address list",
                    Link = "ProviderAddressListReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_ProviderAddressList
                },
                new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "SchoolAddressListReport",
                    Title = "School address list",
                    Link = "SchoolAddressListReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_SchoolAddressList
                },
                new DashboardMenuItemViewModel
                {
                    Id = 8,
                    Name = "MemberListReport",
                    Title = "Member list",
                    Link = "MemberListReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_MemberList
                },
                new DashboardMenuItemViewModel
                {
                    Id = 9,
                    Name = "InstructorListReport",
                    Title = "Instructor list",
                    Link = "DeafultReportSelectProviders({reportType:'InstructorListReport'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_InstructorList

                },
                new DashboardMenuItemViewModel
                {
                    Id = 10,
                    Name = "InsuranceCertificateCheckReport",
                    Title = "Insurance certificate check",
                    Link = "InsuranceCertificateCheckReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_InsuranceCertificateCheck
                },
                new DashboardMenuItemViewModel
                {
                    Id = 11,
                    Name = "SeasonSurveyDatesReport",
                    Title = "Season survey dates",
                    Link = "SeasonSurveyDatesReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_SeasonSurveyDates
                },
                new DashboardMenuItemViewModel
                {
                    Id = 12,
                    Name = "InstructorAssignmentsReport",
                    Title = "Instructor assignments",
                    Link = "SelectProviderSeason({reportType:'InstructorAssignmentsPortal'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_InstructorAssignments
                },
                new DashboardMenuItemViewModel
                {
                    Id = 13,
                    Name = "FullSeasonClassListReport",
                    Title = "Full season class list",
                    Link = "FullSeasonClassListReports",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_FullSeasonClassList
                },
                new DashboardMenuItemViewModel
                    {
                        Id = 14,
                        Name = "PricingReport",
                        Title = "Migration class list",
                        Link = "PricingReport",
                        Icon = "icon-stats-2",
                        Action = JbAction.PortalReport_Pricing
                    },
                 new DashboardMenuItemViewModel
                    {
                        Id = 15,
                        Name = "FullClassListReport",
                        Title = "Full class list - before & after",
                        Link = "FullClassListReport",
                        Icon = "icon-stats-2",
                        Action = JbAction.PortalReport_FullClassList
                    }
                //new DashboardMenuItemViewModel
                //{
                //    Id = 14,
                //    Name = "InstructorCheckinReport",
                //    Title = "Instructor check-in",
                //    Link = "SelectProviderSeason({reportType:'InstructorCheckinPortal'})",
                //    Icon = "icon-stats-2",
                //    Action = JbAction.ProtalReport_FullSeasonClassList
                //},
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            if (ActiveClub.Domain != "em")
            {
                model.RemoveAll(m => m.Action == JbAction.ProtalReport_SeasonSurveyDates);
            }

            return View(model);
        }

        public virtual ActionResult FormReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "FollowupReportPortal",
                    Title = "Follow-up forms",
                    Link = "FollowupReportPortal",
                    Icon = "icon-stats-2",
                    Action = JbAction.ProtalReport_Followup
                },
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult SubscriptionsReports()
        {

            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                        Id = 1,
                        Name = "SingOut",
                        Title = "Sign out",
                        Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'SignOut',reportType:'Default'})",
                        Icon = "icon-stats-2",
                        Action = JbAction.Reports_SignOut
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "SubscriptionRosterReport",
                    Title = "Roster",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'SubscriptionRoster',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Rosters_Admin
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "SubscriptionInternalReport",
                    Title = "Internal roster",
                    Link = "DeafultReportSelectPrograms({seasonDomain: MC.stateParams.seasonDomain,reportName:'SubscriptionInternal',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_InternalUse
                },
                   new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "SubscriptionAttendanceReport",
                    Title = "Attendance",
                    Link = "SubscriptionAttendanceReportRun({seasonDomain: MC.stateParams.seasonDomain,reportName:'SubscriptionAttendance',reportType:'Default'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_Attendance,
                },
                }
                .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);


        }

        public virtual ActionResult MedicalReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "AllergyMedicalConditionReport",
                    Title = "Medical information",
                    Link = "AllergyMedicalConditionReport({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult OperationsReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "InternalRoster",
                    Title = "Internal roster - profile",
                    Link = "InternalRosterSchoolReport_v2({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "SignOutSheetAdvanced",
                    Title = "Sign-out sheet v2",
                    Link = "SignOutSheetAdvancedV2({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "AllergyMedicalConditionReport",
                    Title = "Medical information",
                    Link = "AllergyMedicalConditionReport({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_RoomAssignmentsAdmin
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult MobileAppReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
                {
                    new DashboardMenuItemViewModel
                    {
                        Id = 1,
                        Name = "ParticipantNotesReport",
                        Title = "Participant notes",
                        Link = "ParticipantNoteReport({seasonDomain: MC.stateParams.seasonDomain})",
                        Icon = "icon-stats-2",
                        Action = JbAction.Reports_MobileApp
                    }
                }
                .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.Season_View)]
        public virtual ActionResult Seasons()
        {
            return View();
        }

        [JbAuthorize(JbAction.Season_View)]
        public virtual ActionResult Season()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);
            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "SeasonOverview",
                    Title = "Overview",
                    Link = "Season({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-stats-2",
                    Action = JbAction.SeasonOverview_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "ProgramsAndOrders",
                    Title = "Programs and orders",
                    Link = "ProgramsReview({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-list",
                    Action = JbAction.ProgramsAndOrders_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "Reports",
                    Title = "Reports",
                    Link = "Reports({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-printer",
                    Action = JbAction.Report_View
                },
            };
            if (club.IsSchool)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "RoomAssignements",
                    Title = "Room assignments",
                    Link = "SeasonAssignRoomForPrograms({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-list",
                    Action = JbAction.Season_AssignRoom,
                    ParentId = 1
                });
            }
            model.Add(new DashboardMenuItemViewModel
            {
                Id = 4,
                Name = "Setup",
                Title = "Setup",
                Link = "SeasonSetup({seasonDomain: MC.stateParams.seasonDomain})",
                Icon = "icon-plus-2",
                Action = JbAction.Setup_View
            });

            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult SeasonSetup()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "Programs",
                    Title = "Programs",
                    Link = "Programs({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-list",
                    Action = JbAction.Program_List_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "AddProgram",
                    Title = "Add program",
                    Link = "ProgramAddBlade({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-plus-2",
                    Action = JbAction.Program_Add,
                    ParentId = 1
                },
                new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "Coupons",
                    Title = "Coupons",
                    Link = "Coupons({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-coupon",
                    Action = JbAction.Coupon_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 4,
                    Name = "AddCoupon",
                    Title = "Add coupon",
                    Link = "CouponAdd({seasonDomain:MC.stateParams.seasonDomain})",
                    Icon = "icon-plus-2",
                    Action = JbAction.Coupon_Add,
                    ParentId = 3
                }
                ,new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "UploadCoupons",
                    Title = "Upload coupons",
                    Link = "CouponsUpload({seasonDomain:MC.stateParams.seasonDomain})",
                    Icon = "icon-plus-2",
                    Action = JbAction.Coupon_Upload,
                    ParentId = 3
                },
                new DashboardMenuItemViewModel
                {
                    Id = 6,
                    Name = "Discounts",
                    Title = "Discounts",
                    Link = "Discounts({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-percent",
                    Action = JbAction.Discount_View
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 14,
                    Name = "Charges",
                    Title = "Charges",
                    Link = "Charges({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-coins",
                    Action = JbAction.Charges_View
                },
                   new DashboardMenuItemViewModel
                {
                    Id = 5,
                    Name = "AddCharge",
                    Title = "Add charge",
                    Link = "ChargesAdd({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "icon-plus-2",
                    Action = JbAction.Coupon_Upload,
                    ParentId = 14
                },
                new DashboardMenuItemViewModel
                {
                    Id = 7,
                    Name = "AddDiscount",
                    Title = "Add discount",
                    Link = "DiscountsAdd({seasonDomain:MC.stateParams.seasonDomain})",
                    Icon = "icon-plus-2",
                    Action = JbAction.Discount_Add,
                    ParentId = 5
                },
                new DashboardMenuItemViewModel
                {
                    Id = 8,
                    Name = "PaymentPlans",
                    Title = "Payment plans",
                    Link = "PaymentPlans({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-cash",
                    Action = JbAction.PaymentPlan_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 9,
                    Name = "AddPaymentPlan",
                    Title = "Add payment plan",
                    Link = "PaymentPlanManage({seasonDomain:MC.stateParams.seasonDomain})",
                    Icon = "icon-plus-2",
                    Action = JbAction.PaymentPlan_Add,
                    ParentId = 7
                },
                //new DashboardMenuItemViewModel
                //{
                //    Id = 15,
                //    Name = "SeasonForms",
                //    Title = "Forms",
                //    Link = "SeasonForms({seasonDomain:MC.stateParams.seasonDomain})",
                //    Icon = "jbi-details",
                //    Action = JbAction.Forms_View
                //},
                new DashboardMenuItemViewModel
                {
                    Id = 10,
                    Name = "RegistrationEmailTemplate",
                    Title = "Registration email template",
                    Link = "RegistrationEmailTemplateDesigner({seasonDomain:MC.stateParams.seasonDomain})",
                    Icon = "jbi-email-template",
                    Action = JbAction.ReigstrationEmailTemplate_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 11,
                    Name = "RegistrationInfo",
                    Title = "Registration information",
                    Link = "SeasonSchoolSettingRegistrationInfo({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-info",
                    Action = JbAction.Season_SchoolSetting_ScheduleInfo_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 12,
                    Name = "ManageSeason",
                    Title = "Manage season",
                    Icon = "jbi-list",
                    Action = JbAction.Season_Edit
                },
                 new DashboardMenuItemViewModel
                {
                    Id = 13,
                    Name = "EditSeason",
                    Title = "Edit",
                    Link = "SeasonEdit({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-edit-document",
                    Action = JbAction.Season_Edit,
                    ParentId =12
                },
                  new DashboardMenuItemViewModel
                {
                    Id = 14,
                    Name = "DeleteSeason",
                    Title = "Delete",
                    Icon = "jbi-delete-trash-1",
                    OnClick = "deleteSeason()",
                    Action = JbAction.Season_Delete,
                    ParentId =12
                },
                new DashboardMenuItemViewModel
                {
                    Id = 15,
                    Name = "ArchiveSeason",
                    Title = "Archive",
                    Icon = "jbi-ban",
                    OnClick = "archiveSeason()",
                    Action = JbAction.Season_Archive,
                    ParentId =12
                }
            };

            if (ActiveClub.IsSchool)
            {

                var clubBaseInfo = this.GetCurrentClubBaseInfo();

                if (allowedActions.Contains(JbAction.Season_SchoolAdvancedSettings_View) && clubBaseInfo.HasPartner)
                {
                    var partnerId = clubBaseInfo.PartnerId;

                    var partnerSetting = Ioc.ClubBusiness.GetPartnerSetting(partnerId.Value);

                    if (partnerSetting.PortalParameters.ShowSchoolSeasonAdvancedSetting)
                    {
                        model.Add(new DashboardMenuItemViewModel
                        {
                            Id = 13,
                            Name = "SeasonSchoolSetting",
                            Title = "EM - Advanced",
                            Link = "SeasonSchoolSetting({seasonDomain: MC.stateParams.seasonDomain})",
                            Icon = "jbi-gear",
                            Action = JbAction.Season_SchoolAdvancedSettings_View
                        });
                    }
                }
            }

            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
            if (club.IsProvider)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 14,
                    Name = "InstructorAssignForPrograms",
                    Title = "Instructor assignments",
                    Link = "InstructorAssignForPrograms({seasonDomain: MC.stateParams.seasonDomain})",
                    Icon = "jbi-list",
                    Action = JbAction.Season_AssignInstructor,
                    ParentId = 1
                });
            }

            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.Clubs_View)]
        public virtual ActionResult SystemClubs()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
            {
                    Id = 1,
                    Name = "AddClub",
                    Title = "Add Club",
                    Link = "SystemClubManage({clubId:0})",
                    Icon = "icon-plus-2",
                    Action = JbAction.Clubs_Add
                }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }
        //[JbAuthorize(JbAction.Clubs_View)]
        public virtual ActionResult SystemFinanceReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
            {
                    Id = 1,
                    Name = "Transaction",
                    Title = "Transaction",
                    Link = "SystemTransaction({reportType:'Transaction'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_SystemFinance_Transaction
                },
                   new DashboardMenuItemViewModel
            {
                    Id = 2,
                    Name = "Revenue",
                    Title = "Revenue",
                    Link = "SystemRevenue({reportType:'Revenue'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_SystemFinance_Revenue
                },
                   new DashboardMenuItemViewModel
                   {
                    Id = 3,
                    Name = "RevenueMatrix",
                    Title = "Revenue matrix",
                    Link = "SystemRevenueMatrix({reportType:'RevenueMatrix'})",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_SystemFinance_RevenuMatrix
                   }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult SystemAutoChargeReports()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
            {
                    Id = 1,
                    Name = "ClientCreditCards",
                    Title = "User credit cards",
                    Link = "SystemClientCreditCards",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_System_ClientCreditCards
             },
                 new DashboardMenuItemViewModel
            {
                    Id = 2,
                    Name = "AllInstallments",
                    Title = "Installments",
                    Link = "AllInstallments",
                    Icon = "icon-stats-2",
                    Action = JbAction.Reports_System_Installments
             }
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }


        [JbAuthorize(JbAction.ESignature_View)]
        public virtual ActionResult ESignature()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel> {

                  new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "ManageDocuments",
                    Title = "Manage documents",
                    Link = "ESignature",
                    Icon = "jbi-signing-a-contract",
                    Action = JbAction.ESignature_View
            },
            new DashboardMenuItemViewModel
            {
                Id = 2,
                Name = "AddESignature",
                Title = "Add",
                Link = "AddESignature",
                Icon = "icon-plus-2",
                Action = JbAction.ESignature_Add,
                ParentId = 1,
            },

        };
            if (ActiveClub.ClubType.EnumType != ClubTypesEnum.Partner)
            {
                model.Add(new DashboardMenuItemViewModel
                {
                    Id = 3,
                    Name = "Setting",
                    Title = "Setting",
                    Link = "SettingDocumentSign",
                    Icon = "jbi-gear",
                    Action = JbAction.ESignature_Setting
                });
            }
            model = model.Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();
            return View(model);
        }

        [JbAuthorize(JbAction.ESignature_View)]
        public virtual ActionResult Flyers()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>();

            model.Add(new DashboardMenuItemViewModel
            {
                Id = 1,
                Name = "Flyers",
                Title = "Flyers",
                Link = "ClubFlyers",
                Icon = "jbi-megaphone",
                Action = JbAction.Flyer_View
            });

            model.Add(new DashboardMenuItemViewModel
            {
                Id = 2,
                Name = "FlyerTemplates",
                Title = "Templates",
                Link = "FlyerTemplates",
                Icon = "jbi-email-template",
                Action = JbAction.Flyer_View
            });
            model.Add(new DashboardMenuItemViewModel
            {
                Id = 2,
                Name = "FlyersSetting",
                Title = "Settings",
                Link = "FlyersSetting",
                Icon = "jbi-gears",
                Action = JbAction.Flyer_View
            });
            return View(model);
        }

        public virtual ActionResult ProgramAdd()
        {
            var client = Ioc.ClubBusiness.Get(ActiveClub.Id).Client;

            if (client == null)
            {
                client = new Client
                {
                    HasChessTourney = false
                };
            }

            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model =
                new List<DashboardMenuItemViewModel>().Where(
                    a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            if (client.HasChessTourney)
            {
                //model.Add(new DashboardMenuItemViewModel { Id = 4, Name = "PaymentPlans", Title = "Payment plans", Link = "PaymentPlans({seasonDomain: MC.stateParams.seasonDomain})", Icon = "jbi-cash", Action = JbAction.ViewPaymentPlans});
            }

            return View(model);
        }

        public virtual ActionResult EmailCampaignAddChooseType()
        {
            return View();
        }

        //[JbAuthorize(JbAction.Messaging_View)]
        public virtual ActionResult Messaging()
        {
            return View();
        }

        [JbAuthorize(JbAction.Contact_View)]
        public virtual ActionResult Contact()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "Contacts",
                    Title = "All contacts",
                    Link = "Contacts",
                    Icon = "jbi-catalog-item",
                    Action = JbAction.Contact_View
                },
                new DashboardMenuItemViewModel
                {
                    Id = 2,
                    Name = "ContactSettings",
                    Title = "Settings",
                    Link = "ContactSettings",
                    Icon = "jbi-gear",
                    Action = JbAction.ContactSettings_View
                },
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        [JbAuthorize(JbAction.Charges_View)]
        public virtual ActionResult Charges()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>
            {
                new DashboardMenuItemViewModel
                {
                    Id = 1,
                    Name = "ApplicationFee",
                    Title = "Application fee",
                    Link = "ApplicationFee({seasonDomain: MC.stateParams.seasonDomain,type:'Add'})",
                    Icon = "jbi-coin",
                    Action = JbAction.ApplicationFee_Add
                },
            }
            .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }

        public virtual ActionResult SystemCampaigns()
        {
            var allowedActions = Ioc.SecurityBusiness.GetActionRoles(this.GetCurrentUserRole()).Select(a => a.Action);

            var model = new List<DashboardMenuItemViewModel>()
                {
                    new DashboardMenuItemViewModel
                    {
                        Id = 1,
                        Name = "ClassConfirmation",
                        Title = "Class confirmation",
                        Link = "ClassConfirmation",
                        Icon = "jbi-gear",
                        //Action = JbAction.Contact_View
                    },
                }
                .Where(a => a.Action == null || allowedActions.Contains(a.Action.Value)).ToList();

            return View(model);
        }
    }
}