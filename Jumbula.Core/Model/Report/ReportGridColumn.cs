﻿namespace Jumbula.Core.Model.Report
{
    public class ReportGridColumn
    {
        public ReportGridColumn()
        {
            Template = "";
            HeaderTemplate = null;
        }

        public string Field { get; set; }
        public string Title { get; set; }
        public string Width { get; set; }
        public string Template { get; set; }
        public string HeaderTemplate { get; set; }
        public bool Locked { get; set; }

        public string SortPath { get; set; }
    }
}
