﻿using Jb.Framework.Common;
using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Discount;
using Microsoft.Ajax.Utilities;

namespace Jumbula.Business
{
    public class DiscountBusiness : IDiscountBusiness
    {
        private readonly IProgramBusiness _programBusiness;
        private readonly ISubscriptionBusiness _subscripitonBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IClubBusiness _clubBusiness;
        private readonly ICouponBusiness _couponBusiness;
        private readonly ISeasonBusiness _seasonBusiness;
        private readonly IRepository<Discount, long> _discountRepository;

        public DiscountBusiness(IProgramBusiness programBusiness,
                                               ISubscriptionBusiness subscripitonBusiness,
                                               IOrderItemBusiness orderItemBusiness,
                                               IClubBusiness clubBusiness,
                                               ICouponBusiness couponBusiness,
                                               ISeasonBusiness seasonBusiness,
                                               IRepository<Discount, long> discountRepository)
        {
            _programBusiness = programBusiness;
            _subscripitonBusiness = subscripitonBusiness;
            _orderItemBusiness = orderItemBusiness;
            _clubBusiness = clubBusiness;
            _couponBusiness = couponBusiness;
            _discountRepository = discountRepository;
            _seasonBusiness = seasonBusiness;
        }

        public Discount Get(long discountId)
        {
            return _discountRepository.Get(d => d.Id == discountId && d.IsDeleted != true);
        }

        public IQueryable<Discount> GetList(long seasonId, bool isDeleted = false)
        {
            return !isDeleted ? _discountRepository.GetList().Where(d => d.SeasonId == seasonId && d.IsDeleted == false) : _discountRepository.GetList().Where(d => d.SeasonId == seasonId);
        }

        public IQueryable<Discount> GetList(List<long> seasonIds, bool isDeleted = false)
        {
            return !isDeleted ? _discountRepository.GetList().Where(d => seasonIds.Contains(d.SeasonId) && d.IsDeleted != true) : _discountRepository.GetList().Where(d => seasonIds.Contains(d.SeasonId));
        }

        public OperationStatus Delete(long discountId)
        {
            var discount = _discountRepository.Get(d => d.Id == discountId);
            discount.IsDeleted = true;
            return _discountRepository.Update(discount);
        }

        public void CalculateDiscount(Order order, DiscountMode discountMode)
        {

            TotalDiscountAmount = 0;
            var isTestMode = !order.IsLive;

            var orderItemsGroup = order.RegularOrderItems.Where(i => i.ProgramScheduleId.HasValue).GroupBy(c => c.SeasonId);
            foreach (var item in order.RegularOrderItems)
            {
                if (item.GetOrderChargeDiscounts().ToList().Any())
                {
                    var items = item.GetOrderChargeDiscounts().Where(o => o.Category == ChargeDiscountCategory.CustomDiscount || o.Category == ChargeDiscountCategory.MultiPerson || o.Category == ChargeDiscountCategory.MultiProgram || o.Category == ChargeDiscountCategory.MultiSchedule).ToList();
                    foreach (var ch in items)
                    {
                        item.OrderChargeDiscounts.Remove(ch);
                    }
                }

            }

            if (order.Club.Setting != null)
            {
                isApplyOnOrigin = order.Club.Setting.IsDiscountApplyOnOriginPrice;
            }
            foreach (var seasonOrderItems in orderItemsGroup)
            {
                IEnumerable<Discount> programDiscount = GetProgramsDiscounts(seasonOrderItems.Select(c => c.ProgramSchedule.Program));
             
                CalculateMultiPersonDiscountOfOrder(programDiscount.Where(d => d.Category == ChargeDiscountCategory.MultiPerson), seasonOrderItems.ToList(), discountMode, order.UserId, isTestMode);

                CalculateMultiProgramDiscountOfOrder(programDiscount.Where(d => d.Category == ChargeDiscountCategory.MultiProgram), seasonOrderItems.ToList(), discountMode, order.UserId, isTestMode);

                CalculateMultiScheduleDiscountOfOrder(
                    programDiscount.Where(d => d.Category == ChargeDiscountCategory.MultiSchedule),
                    seasonOrderItems.ToList().Where(o => o.ProgramTypeCategory == ProgramTypeCategory.Camp).ToList(),
                    discountMode, order.UserId, isTestMode);
            }
            foreach (var item in order.RegularOrderItems)
            {

                if (item.GetOrderChargeDiscounts().ToList().Any())
                {
                    item.CalculateTotalAmount();
                }
            }
            order.OrderAmount -= TotalDiscountAmount;
        }

        private void CalculateMultiPersonDiscountOfOrder(IEnumerable<Discount> MultiPersonDiscount, List<OrderItem> orderItems, DiscountMode discountMode, int userId, bool isTestMode)
        {
            if (MultiPersonDiscount == null || MultiPersonDiscount.Count() == 0)
            {
                return;
            }
            foreach (var discount in MultiPersonDiscount)
            {
                if (discount.EligibleMode == EligibleMode.DifferentProgram)
                {
                    MultiPersonForDifferentProgram(discount, orderItems, discountMode, userId, isTestMode: isTestMode);
                }
                else
                {
                    MultiPersonForSameProgram(discount, orderItems, discountMode, userId, isTestMode);
                }

            }
        }
        private void MultiPersonForDifferentProgram(Discount discount, List<OrderItem> orderItems, DiscountMode discountMode, int userId, bool isTestMode)
        {
            List<OrderItem> discountOrderItems = null;
            IEnumerable<long> programListId = null;
            IOrderItemBusiness orderBusiness = null;
            IQueryable<OrderItem> completedOrders = null;
            if (discount.IsAllProgram)
            {
                programListId = orderItems.Select(p => p.ProgramSchedule.ProgramId).Distinct();
                discountOrderItems = orderItems;
                if (discountMode == DiscountMode.OrdersInSeason)
                {
                    if (orderBusiness == null)
                    {
                        orderBusiness = _orderItemBusiness;
                    }

                    if (orderItems.First().Order.ClubId == 10 || orderItems.First().Order.ClubId == 11963)
                    {
                        completedOrders = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(o => o.ItemStatus == OrderItemStatusCategories.completed && DbFunctions.TruncateTime(o.DateCreated) >= DbFunctions.TruncateTime(discount.MetaData.DateCreated));
                    }
                    else
                    {
                        completedOrders = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(o => o.ItemStatus == OrderItemStatusCategories.completed);
                    }
                }

            }
            else
            {
                programListId = discount.Programs.Select(p => p.Id).Distinct();
                discountOrderItems = orderItems.Where(c => (programListId.Contains(c.ProgramSchedule.ProgramId)) == true).ToList();
                if (discountMode == DiscountMode.OrdersInSeason)
                {
                    if (orderBusiness == null)
                    {
                        orderBusiness = _orderItemBusiness;
                    }
                    if (orderItems.First().Order.ClubId == 10 || orderItems.First().Order.ClubId == 11963)
                    {
                        completedOrders = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(o => (programListId.Contains(o.ProgramSchedule.ProgramId)) == true && o.ItemStatus == OrderItemStatusCategories.completed && DbFunctions.TruncateTime(o.DateCreated) >= DbFunctions.TruncateTime(discount.MetaData.DateCreated));
                    }
                    else
                    {
                        completedOrders = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(o => (programListId.Contains(o.ProgramSchedule.ProgramId)) == true && o.ItemStatus == OrderItemStatusCategories.completed);

                    }
                }
            }

            var NewPlayerIds = discountOrderItems.Where(o => o.PlayerId.HasValue).Select(c => c.PlayerId).Distinct();
            var numberOfPerson = NewPlayerIds.Count();

            if (discountMode == DiscountMode.OrdersInSeason)
            {
                var PreviousPlayerIds = completedOrders.Select(p => p.PlayerId).Distinct();

                numberOfPerson += PreviousPlayerIds.Except(NewPlayerIds).Count();
            }
            if (numberOfPerson < discount.NOP)
            {
                return;
            }

            switch (discount.ApplyType)
            {
                case DiscountApplyType.Every:
                    {
                        CalculateMultiPersonDiscountEvery(discountOrderItems, discount, completedOrders, discountMode);
                        break;
                    }
                case DiscountApplyType.SelectedNumOnly:
                    {
                        CalculateMultiPersonDiscountSelectedNumOnly(discountOrderItems, discount, completedOrders, discountMode);
                        break;
                    }
                case DiscountApplyType.SelectedNumAndAddition:
                    {
                        CalculateMultiPersonDiscountSelectedNumAndAddition(discountOrderItems, discount, completedOrders, discountMode);
                        break;
                    }
            }

        }

        private void MultiPersonForSameProgram(Discount discount, List<OrderItem> orderItems, DiscountMode discountMode, int userId, bool isTestMode)
        {
            List<OrderItem> discountOrderItems = null;
            IEnumerable<long> programListId = null;
            if (discount.IsAllProgram)
            {
                programListId = orderItems.Select(p => p.ProgramSchedule.ProgramId).Distinct();
                discountOrderItems = orderItems;

            }
            else
            {
                programListId = discount.Programs.Select(p => p.Id).Distinct();
                discountOrderItems = orderItems.Where(c => (programListId.Contains(c.ProgramSchedule.ProgramId)) == true).ToList();
            }

            foreach (var programId in programListId)
            {
                var ordersOfProgram = discountOrderItems.Where(c => c.ProgramSchedule.ProgramId == programId);
                if (ordersOfProgram == null && ordersOfProgram.Count() == 0)
                {
                    continue;
                }
                var NewPlayerIds = ordersOfProgram.Where(o => o.PlayerId.HasValue).Select(c => c.PlayerId).Distinct();
                var numberOfPerson = NewPlayerIds.Count();
                IOrderItemBusiness orderBusiness = null;
                IQueryable<OrderItem> completedOrders = null;
                if (discountMode == DiscountMode.OrdersInSeason)
                {
                    if (orderBusiness == null)
                    {
                        orderBusiness = _orderItemBusiness;
                    }
                    if (orderItems.First().Order.ClubId == 10 || orderItems.First().Order.ClubId == 11963)
                    {
                        completedOrders = orderBusiness.GetOrderItems(programId: programId, userId: userId, isTestMode: isTestMode).Where(o => o.ItemStatus == OrderItemStatusCategories.completed && DbFunctions.TruncateTime(o.DateCreated) >= DbFunctions.TruncateTime(discount.MetaData.DateCreated));
                    }
                    else
                    {
                        completedOrders = orderBusiness.GetOrderItems(programId: programId, userId: userId, isTestMode: isTestMode).Where(o => o.ItemStatus == OrderItemStatusCategories.completed);
                    }
                    var PreviousPlayerIds = completedOrders.Select(p => p.PlayerId).Distinct();

                    numberOfPerson += PreviousPlayerIds.Except(NewPlayerIds).Count();
                }
                if (numberOfPerson < discount.NOP)
                {
                    continue;
                }

                switch (discount.ApplyType)
                {
                    case DiscountApplyType.Every:
                        {
                            CalculateMultiPersonDiscountEvery(ordersOfProgram, discount, completedOrders, discountMode);
                            break;
                        }
                    case DiscountApplyType.SelectedNumOnly:
                        {
                            CalculateMultiPersonDiscountSelectedNumOnly(ordersOfProgram, discount, completedOrders, discountMode);
                            break;
                        }
                    case DiscountApplyType.SelectedNumAndAddition:
                        {
                            CalculateMultiPersonDiscountSelectedNumAndAddition(ordersOfProgram, discount, completedOrders, discountMode);
                            break;
                        }
                }

            }
        }

        private void CalculateMultiPersonDiscountEvery(IEnumerable<OrderItem> ordersOfProgram, Discount discount, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            if (discountMode == DiscountMode.OrdersInCart || !completedOrders.Any())
            {
                AddOrderItemsDiscounts(ordersOfProgram, discount, ChargeDiscountCategory.MultiPerson);
            }
            else
            {
                decimal extraCharges = 0;
                if (completedOrders != null && completedOrders.Any())
                {
                    var appliedDiscounts = completedOrders.SelectMany(c => c.OrderChargeDiscounts).Where(d => !d.IsDeleted && d.DiscountId == discount.Id).ToList();
                    decimal appliedDiscountAmount = appliedDiscounts != null ? (appliedDiscounts.Sum(c => c.Amount) * -1) : 0;
                    decimal wholeDiscount = getdiscountAmount(completedOrders.ToList(), discount);
                    extraCharges = wholeDiscount - appliedDiscountAmount;

                    if (extraCharges < 0)
                    {
                        extraCharges = 0;
                    }
                }
                AddOrderItemsDiscounts(ordersOfProgram, discount, ChargeDiscountCategory.MultiPerson, extraCharges);
            }
        }

        private void CalculateMultiPersonDiscountSelectedNumAndAddition(IEnumerable<OrderItem> ordersOfProgram, Discount discount, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            var ordersList = ordersOfProgram.ToList();
            if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
            {
                ordersList.AddRange(completedOrders);
            }
            var PlayerIdList = ordersList.Where(o => o.PlayerId.HasValue).GroupBy(o => o.PlayerId);
            var priceList = PlayerIdList.Select(o => new { price = o.Sum(c => c.EntryFee), playerId = o.FirstOrDefault().PlayerId });

            List<int> selectedPersons = new List<int>();
            int totalRow = priceList.Count() - discount.NOP + 1;
            if (discount.BenefitType == DiscountBenefitType.Organization)
            {
                selectedPersons = priceList.OrderBy(p => p.price).Take(totalRow).Select(p => p.playerId.Value).ToList();
            }
            else
            {
                selectedPersons = priceList.OrderByDescending(p => p.price).Take(totalRow).Select(p => p.playerId.Value).ToList();
            }
            List<OrderItem> discountOrderItems = new List<OrderItem>();
            List<OrderItem> prevDiscountOrderItems = null;
            foreach (var person in selectedPersons)
            {
                discountOrderItems.AddRange(ordersOfProgram.Where(o => o.PlayerId == person));
                if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
                {
                    if (prevDiscountOrderItems == null)
                    {
                        prevDiscountOrderItems = new List<OrderItem>();

                    }
                    prevDiscountOrderItems.AddRange(ordersList.Where(o => o.PlayerId == person));
                }
            }
            decimal extraCharges = 0;
            if (completedOrders != null && completedOrders.Any())
            {
                var appliedDiscounts = completedOrders.SelectMany(c => c.OrderChargeDiscounts).Where(d =>!d.IsDeleted && d.DiscountId == discount.Id).ToList();

                decimal appliedDiscountAmount = appliedDiscounts != null ? (appliedDiscounts.Sum(c => c.Amount) * -1) : 0;

                decimal newDiscountAmount = getdiscountAmount(prevDiscountOrderItems, discount);
                extraCharges = newDiscountAmount - appliedDiscountAmount;
                if (extraCharges <= 0)
                {
                    return;
                }
                if (extraCharges > 0)
                {
                    extraCharges -= getdiscountAmount(discountOrderItems, discount);
                }
            }
            bool onlyExtraAmount = false;
            if (extraCharges > 0 && (discountOrderItems == null || discountOrderItems.Count == 0))
            {

                discountOrderItems.Add(ordersOfProgram.FirstOrDefault(o => o.EntryFee >= extraCharges));
                onlyExtraAmount = true;
            }
            AddOrderItemsDiscounts(discountOrderItems, discount, ChargeDiscountCategory.MultiPerson, extraCharges, onlyExtraAmount);

        }

        private void CalculateMultiPersonDiscountSelectedNumOnly(IEnumerable<OrderItem> ordersOfProgram, Discount discount, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            var ordersList = ordersOfProgram.ToList();
            if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
            {
                ordersList.AddRange(completedOrders);
            }
            var PlayerIdList = ordersList.Where(o => o.PlayerId.HasValue).GroupBy(o => o.PlayerId);
            // var priceList = PlayerIdList.Select(o => new { price = o.Sum(c => c.EntryFee), playerId = o.FirstOrDefault().PlayerId });
            var itemIds = new List<long>();

            if (discount.BenefitType == DiscountBenefitType.Organization)
            {
                itemIds = ordersList.GroupBy(p => p.PlayerId).OrderBy(g => g.Sum(s => s.EntryFee)).First().Select(o => o.Id).ToList();
            }
            else
            {
                itemIds = ordersList.GroupBy(p => p.PlayerId).OrderByDescending(g => g.Sum(s => s.EntryFee)).First().Select(o => o.Id).ToList();
            }
            decimal extraCharges = 0;
            if (completedOrders != null && completedOrders.Any())
            {
                var appliedDiscounts = completedOrders.SelectMany(c => c.OrderChargeDiscounts).Where(d => !d.IsDeleted && d.DiscountId == discount.Id).ToList();
                decimal appliedDiscountAmount = appliedDiscounts != null ? (appliedDiscounts.Sum(c => c.Amount) * -1) : 0;
                decimal appliedDiscountWithAllOrders = getdiscountAmount(ordersList.Where(o => itemIds.Contains(o.Id)).ToList(), discount);


                extraCharges = appliedDiscountWithAllOrders - appliedDiscountAmount;
                if (extraCharges <= 0)
                {
                    return;
                }
                if (extraCharges > 0 && ordersOfProgram.Any(c => itemIds.Contains(c.Id)))
                {
                    extraCharges -= getdiscountAmount(ordersOfProgram.Where(o => itemIds.Contains(o.Id)).ToList(), discount);
                }

            }
            var discountOrderItems = new List<OrderItem>();
            if (ordersOfProgram.Any(o => itemIds.Contains(o.Id)))
            {
                discountOrderItems.AddRange(ordersOfProgram.Where(o => itemIds.Contains(o.Id)).ToList());
            }

            bool onlyExtraAmount = false;
            if (extraCharges > 0 && (discountOrderItems == null || discountOrderItems.Count == 0))
            {
                if (discountOrderItems == null)
                {
                    discountOrderItems = new List<OrderItem>();
                }
                discountOrderItems.Add(ordersOfProgram.FirstOrDefault(o => o.EntryFee >= extraCharges));
                onlyExtraAmount = true;
            }
            AddOrderItemsDiscounts(discountOrderItems, discount, ChargeDiscountCategory.MultiPerson, extraCharges, onlyExtraAmount);

        }

        private void CalculateMultiProgramDiscountOfOrder(IEnumerable<Discount> multiProgramDiscount, List<OrderItem> orderItems, DiscountMode discountMode, int userId, bool isTestMode)
        {
            if (multiProgramDiscount == null || multiProgramDiscount.Count() == 0)
            {
                return;
            }
            foreach (var discount in multiProgramDiscount)
            {
                IEnumerable<IGrouping<int?, OrderItem>> discountOrderItems = null;
                IEnumerable<long> programListId = null;
                if (discount.IsAllProgram)
                {
                    programListId = orderItems.Select(p => p.ProgramSchedule.ProgramId).Distinct();
                    discountOrderItems = orderItems.GroupBy(o => o.PlayerId);

                }
                else
                {
                    programListId = discount.Programs.Select(p => p.Id).Distinct();
                    discountOrderItems = orderItems.Where(c => (programListId.Contains(c.ProgramSchedule.ProgramId)) == true).GroupBy(o => o.PlayerId);
                }
                IOrderItemBusiness orderBusiness = null;
                foreach (var itemgroup in discountOrderItems)
                {

                    IQueryable<OrderItem> completedOrderItems = null;
                    var uniqueProgramIDs = itemgroup.Select(o => o.ProgramSchedule.ProgramId).Distinct();
                    var uniqueProgramNum = uniqueProgramIDs.Count();
                    if (discountMode == DiscountMode.OrdersInSeason)
                    {
                        if (orderBusiness == null)
                        {
                            orderBusiness = _orderItemBusiness;
                        }
                        if (discount.IsAllProgram)
                        {
                            if (orderItems.First().Order.ClubId == 10 || orderItems.First().Order.ClubId == 11963)
                            {
                                completedOrderItems = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(o => o.PlayerId == itemgroup.Key && o.ItemStatus == OrderItemStatusCategories.completed && DbFunctions.TruncateTime(o.DateCreated) >= DbFunctions.TruncateTime(discount.MetaData.DateCreated));
                            }
                            else
                            {
                                completedOrderItems = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(c => c.PlayerId == itemgroup.Key);

                            }
                        }
                        else
                        {
                            if (orderItems.First().Order.ClubId == 10 || orderItems.First().Order.ClubId == 11963)
                            {
                                completedOrderItems = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(o => o.PlayerId == itemgroup.Key && (programListId.Contains(o.ProgramSchedule.ProgramId)) == true && o.ItemStatus == OrderItemStatusCategories.completed && DbFunctions.TruncateTime(o.DateCreated) >= DbFunctions.TruncateTime(discount.MetaData.DateCreated));
                            }
                            else
                            {
                                completedOrderItems = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(c => c.PlayerId == itemgroup.Key && (programListId.Contains(c.ProgramSchedule.ProgramId)) == true);

                            }
                        }
                        uniqueProgramNum += completedOrderItems.Select(o => o.ProgramSchedule.ProgramId).Distinct().Except(uniqueProgramIDs).Count();
                    }

                    if (uniqueProgramNum < discount.NOP)
                    {
                        continue;
                    }

                    switch (discount.ApplyType)
                    {
                        case DiscountApplyType.Every:
                            {
                                CalculateMultiProgramEvery(itemgroup.ToList(), discount, completedOrderItems, discountMode);
                                break;
                            }
                        case DiscountApplyType.SelectedNumOnly:
                            {
                                CalculateMultiProgramDiscountNumOnly(itemgroup.ToList(), discount, uniqueProgramIDs, completedOrderItems, discountMode);

                                break;
                            }
                        case DiscountApplyType.SelectedNumAndAddition:
                            {
                                CalculateMultiProgramDiscountNumAndAddition(itemgroup.ToList(), discount, completedOrderItems, discountMode);

                                break;
                            }
                    }
                }
            }
        }

        private void CalculateMultiProgramEvery(List<OrderItem> orderItems, Discount discount, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            if (discountMode == DiscountMode.OrdersInCart || !completedOrders.Any())
            {
                AddOrderItemsDiscounts(orderItems, discount, ChargeDiscountCategory.MultiProgram);
            }
            else
            {
                decimal extraCharges = 0;

                if (completedOrders != null && completedOrders.Any())
                {
                    var appliedDiscounts = completedOrders.SelectMany(c => c.OrderChargeDiscounts).Where(d =>!d.IsDeleted && d.DiscountId == discount.Id).ToList();
                    decimal appliedDiscountAmount = appliedDiscounts != null ? (appliedDiscounts.Sum(c => c.Amount) * -1) : 0;

                    decimal wholeDiscount = getdiscountAmount(completedOrders.ToList(), discount);

                    extraCharges = wholeDiscount - appliedDiscountAmount;

                    if (extraCharges < 0)
                    {
                        extraCharges = 0;
                    }
                }



                AddOrderItemsDiscounts(orderItems, discount, ChargeDiscountCategory.MultiPerson, extraCharges);
            }
        }

        private void CalculateMultiProgramDiscountNumAndAddition(List<OrderItem> orderItems, Discount discount, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            var allOrderItems = orderItems.ToList();
            if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
            {
                allOrderItems.AddRange(completedOrders);
            }
            var itemGroupByProgram = allOrderItems.GroupBy(o => o.ProgramSchedule.ProgramId).Select(o => new { price = o.Sum(p => p.EntryFee), programId = o.FirstOrDefault().ProgramSchedule.ProgramId });
            List<long> selectedprogram = new List<long>();

            var numOfRow = itemGroupByProgram.Count() - discount.NOP + 1;
            if (discount.BenefitType == DiscountBenefitType.Organization)
            {
                selectedprogram.AddRange(itemGroupByProgram.OrderBy(o => o.price).Take(numOfRow).Select(o => o.programId));
            }
            else
            {
                selectedprogram.AddRange(itemGroupByProgram.OrderByDescending(o => o.price).Take(numOfRow).Select(o => o.programId));
            }
            List<OrderItem> discountOrderItems = new List<OrderItem>();
            List<OrderItem> prevDiscountOrderItems = null;
            foreach (var program in selectedprogram)
            {
                discountOrderItems.AddRange(orderItems.Where(o => o.ProgramSchedule.ProgramId == program));
                if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
                {
                    if (prevDiscountOrderItems == null)
                    {
                        prevDiscountOrderItems = new List<OrderItem>();

                    }
                    prevDiscountOrderItems.AddRange(allOrderItems.Where(o => o.ProgramSchedule.ProgramId == program));
                }
            }
            decimal extraCharges = 0;
            if (completedOrders != null && completedOrders.Any())
            {
                var appliedDiscounts = completedOrders.SelectMany(c => c.OrderChargeDiscounts).Where(d => !d.IsDeleted && d.DiscountId == discount.Id).ToList();

                decimal appliedDiscountAmount = appliedDiscounts != null ? (appliedDiscounts.Sum(c => c.Amount) * -1) : 0;

                decimal newDiscountAmount = getdiscountAmount(prevDiscountOrderItems, discount);
                extraCharges = newDiscountAmount - appliedDiscountAmount;
                if (extraCharges <= 0)
                {
                    return;
                }
                if (extraCharges > 0)
                {
                    extraCharges -= getdiscountAmount(discountOrderItems, discount);
                }
            }
            bool onlyExtraAmount = false;
            if (extraCharges > 0 && (discountOrderItems == null || discountOrderItems.Count == 0))
            {
                if (discountOrderItems == null)
                {
                    discountOrderItems = new List<OrderItem>();
                }
                discountOrderItems.Add(orderItems.FirstOrDefault(o => o.EntryFee >= extraCharges));
                onlyExtraAmount = true;
            }
            AddOrderItemsDiscounts(discountOrderItems, discount, ChargeDiscountCategory.MultiProgram, extraCharges, onlyExtraAmount);
        }

        private void CalculateMultiProgramDiscountNumOnly(List<OrderItem> orderItems, Discount discount, IEnumerable<long> programIds, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            var allOrderItems = orderItems.ToList();
            if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
            {
                allOrderItems.AddRange(completedOrders);
            }
            //var itemGroupByProgram = allOrderItems.GroupBy(o => o.ProgramSchedule.ProgramId).Select(o => new { price = o.Sum(p => p.EntryFee), programId = o.FirstOrDefault().ProgramSchedule.ProgramId });
            long selectedprogram = 0;
            if (discount.BenefitType == DiscountBenefitType.Organization)
            {
                selectedprogram = allOrderItems.OrderBy(c => c.EntryFee).First().ProgramSchedule.ProgramId;
            }
            else
            {
                selectedprogram = allOrderItems.OrderByDescending(c => c.EntryFee).First().ProgramSchedule.ProgramId;
            }
            decimal extraCharge = 0;
            if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
            {
                var prevDiscount = completedOrders.SelectMany(o => o.OrderChargeDiscounts).Where(c => !c.IsDeleted && c.DiscountId == discount.Id).ToList();
                decimal prevDiscountAmount = prevDiscount != null ? (prevDiscount.Sum(c => c.Amount) * -1) : 0;
                var discountAmountWillAllOrders = getdiscountAmount(new List<OrderItem>() { allOrderItems.FirstOrDefault(o => o.ProgramSchedule.ProgramId == selectedprogram) }, discount);


                extraCharge = discountAmountWillAllOrders - prevDiscountAmount;
                if (extraCharge <= 0)
                {
                    return;
                }

                if (extraCharge > 0 && orderItems.Any(o => o.ProgramSchedule.ProgramId == selectedprogram))
                {
                    extraCharge -= getdiscountAmount(new List<OrderItem>() { orderItems.FirstOrDefault(o => o.ProgramSchedule.ProgramId == selectedprogram) }, discount);
                }
            }
            var discountOrderItems = new List<OrderItem>();
            if (orderItems.Any(o => o.ProgramSchedule.ProgramId == selectedprogram))
            {
                discountOrderItems.Add(orderItems.FirstOrDefault(o => o.ProgramSchedule.ProgramId == selectedprogram));
            }
            bool onlyExtraAmount = false;
            if (extraCharge > 0 && (discountOrderItems == null || discountOrderItems.Count == 0))
            {
                if (discountOrderItems == null)
                {
                    discountOrderItems = new List<OrderItem>();
                }
                discountOrderItems.Add(orderItems.FirstOrDefault(o => o.EntryFee >= extraCharge));
                onlyExtraAmount = true;
            }
            AddOrderItemsDiscounts(discountOrderItems, discount, ChargeDiscountCategory.MultiProgram, extraCharge, onlyExtraAmount);
        }

        private void CalculateMultiScheduleDiscountOfOrder(IEnumerable<Discount> multiScheduleDiscount,
                  List<OrderItem> orderItems, DiscountMode discountMode, int userId, bool isTestMode)
        {
            if (multiScheduleDiscount == null || !multiScheduleDiscount.Any())
            {
                return;
            }
            foreach (var discount in multiScheduleDiscount)
            {
                IEnumerable<MultiScheduleDiscount> discountOrderItems = null;
                IEnumerable<long> programListId = null;
                if (discount.IsAllProgram)
                {
                    programListId = orderItems.Select(p => p.ProgramSchedule.ProgramId).Distinct();
                    discountOrderItems = orderItems.GroupBy(x => new { x.PlayerId, x.ProgramSchedule.ProgramId })
                        .Select(y => new MultiScheduleDiscount()
                        {
                            PlayerId = y.Key.PlayerId.Value,
                            ProgramId = y.Key.ProgramId,
                            OrderItems = y.ToList()
                        }
                        );

                }
                else
                {
                    programListId = discount.Programs.Select(p => p.Id).Distinct();
                    discountOrderItems =
                        orderItems.Where(c => (programListId.Contains(c.ProgramSchedule.ProgramId)))
                            .GroupBy(x => new { x.PlayerId, x.ProgramSchedule.ProgramId })
                            .Select(y => new MultiScheduleDiscount()
                            {
                                PlayerId = y.Key.PlayerId.Value,
                                ProgramId = y.Key.ProgramId,
                                OrderItems = y.ToList()
                            }
                            );
                }
                IOrderItemBusiness orderBusiness = null;

                foreach (var itemgroup in discountOrderItems)
                {
                    var programId = itemgroup.ProgramId;
                    IQueryable<OrderItem> completedOrderItems = null;
                    var uniqueScheduleIDs = itemgroup.OrderItems.Select(o => o.ProgramScheduleId.Value).Distinct();
                    var uniqueScheduleNum = uniqueScheduleIDs.Count();
                    if (discountMode == DiscountMode.OrdersInSeason)
                    {
                        if (orderBusiness == null)
                        {
                            orderBusiness = _orderItemBusiness;
                        }
                        if (orderItems.First().Order.ClubId == 10 || orderItems.First().Order.ClubId == 11963)
                        {
                            completedOrderItems = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(o => o.ProgramSchedule.ProgramId == programId && o.PlayerId == itemgroup.PlayerId && o.ItemStatus == OrderItemStatusCategories.completed && DbFunctions.TruncateTime(o.DateCreated) >= DbFunctions.TruncateTime(discount.MetaData.DateCreated));
                        }
                        else
                        {
                            completedOrderItems = orderBusiness.GetOrderItems(userId: userId, seasonId: orderItems.First().SeasonId.Value, isTestMode: isTestMode).Where(c => c.ProgramSchedule.ProgramId == programId && c.PlayerId == itemgroup.PlayerId);

                        }
                        uniqueScheduleNum += completedOrderItems.Select(o => o.ProgramScheduleId.Value).Distinct().Except(uniqueScheduleIDs).Count();
                    }

                    if (uniqueScheduleNum < discount.NOP)
                    {
                        continue;
                    }

                    switch (discount.ApplyType)
                    {
                        case DiscountApplyType.Every:
                            {
                                CalculateMultiScheduleEvery(itemgroup.OrderItems.ToList(), discount, completedOrderItems, discountMode);
                                break;
                            }
                        case DiscountApplyType.SelectedNumOnly:
                            {
                                CalculateMultiScheduleDiscountNumOnly(itemgroup.OrderItems.ToList(), discount, uniqueScheduleIDs,
                                    completedOrderItems, discountMode);
                                break;
                            }
                        case DiscountApplyType.SelectedNumAndAddition:
                            {
                                CalculateMultiScheduleDiscountNumAndAddition(itemgroup.OrderItems.ToList(), discount,
                                    completedOrderItems, discountMode);
                                break;
                            }
                    }
                }
            }
        }
        private void CalculateMultiScheduleEvery(List<OrderItem> orderItems, Discount discount, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            if (discountMode == DiscountMode.OrdersInCart || !completedOrders.Any())
            {
                AddOrderItemsDiscounts(orderItems, discount, ChargeDiscountCategory.MultiSchedule);
            }
            else
            {
                decimal extraCharges = 0;

                if (completedOrders != null && completedOrders.Any())
                {
                    var appliedDiscounts = completedOrders.SelectMany(c => c.OrderChargeDiscounts).Where(d => !d.IsDeleted && d.DiscountId == discount.Id).ToList();
                    decimal appliedDiscountAmount = appliedDiscounts != null ? (appliedDiscounts.Sum(c => c.Amount) * -1) : 0;

                    decimal wholeDiscount = getdiscountAmount(completedOrders.ToList(), discount);
                    extraCharges = wholeDiscount - appliedDiscountAmount;
                    if (extraCharges < 0)
                    {
                        extraCharges = 0;
                    }
                }
                AddOrderItemsDiscounts(orderItems, discount, ChargeDiscountCategory.MultiSchedule, extraCharges);
            }
        }
        private void CalculateMultiScheduleDiscountNumOnly(List<OrderItem> orderItems, Discount discount, IEnumerable<long> scheduleIds, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            var allOrderItems = orderItems.ToList();
            if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
            {
                allOrderItems.AddRange(completedOrders);
            }
            var itemGroupByProgram = allOrderItems.GroupBy(o => o.ProgramSchedule.Id).Select(o => new { price = o.Sum(p => p.EntryFee), scheduleId = o.FirstOrDefault().ProgramSchedule.Id });
            long selectedschedule = 0;
            if (discount.BenefitType == DiscountBenefitType.Organization)
            {
                selectedschedule = allOrderItems.OrderBy(c => c.EntryFee).First().ProgramSchedule.Id;
            }
            else
            {
                selectedschedule = allOrderItems.OrderByDescending(c => c.EntryFee).First().ProgramSchedule.Id;
            }
            decimal extraCharge = 0;
            if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
            {
                var prevDiscount = completedOrders.SelectMany(o => o.OrderChargeDiscounts).Where(c =>!c.IsDeleted && c.DiscountId == discount.Id).ToList();
                decimal prevDiscountAmount = prevDiscount != null ? (prevDiscount.Sum(c => c.Amount) * -1) : 0;
                decimal newDiscountAmount = getdiscountAmount(new List<OrderItem>() { allOrderItems.FirstOrDefault(o => o.ProgramSchedule.Id == selectedschedule) }, discount);
                extraCharge = newDiscountAmount - prevDiscountAmount;
                if (extraCharge <= 0)
                {
                    return;
                }
                if (extraCharge > 0 && orderItems.Any(o => o.ProgramSchedule.Id == selectedschedule))
                {
                    extraCharge -= getdiscountAmount(new List<OrderItem>() { orderItems.FirstOrDefault(o => o.ProgramSchedule.Id == selectedschedule) }, discount);
                }
            }

            var discountOrderItems = new List<OrderItem>();
            if (orderItems.Any(o => o.ProgramSchedule.Id == selectedschedule))
            {
                discountOrderItems.Add(orderItems.FirstOrDefault(o => o.ProgramSchedule.Id == selectedschedule));
            }

            bool onlyExtraAmount = false;
            if (extraCharge > 0 && (discountOrderItems == null || discountOrderItems.Count == 0))
            {
                if (discountOrderItems == null)
                {
                    discountOrderItems = new List<OrderItem>();
                }
                discountOrderItems.Add(orderItems.FirstOrDefault(o => o.EntryFee >= extraCharge));
                onlyExtraAmount = true;
            }
            AddOrderItemsDiscounts(discountOrderItems, discount, ChargeDiscountCategory.MultiSchedule, extraCharge, onlyExtraAmount);
        }

        private void CalculateMultiScheduleDiscountNumAndAddition(List<OrderItem> orderItems, Discount discount, IQueryable<OrderItem> completedOrders, DiscountMode discountMode)
        {
            var allOrderItems = new List<OrderItem>();
            allOrderItems.AddRange(orderItems);
            if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
            {
                allOrderItems.AddRange(completedOrders);
            }
            var itemGroupByProgram = allOrderItems.GroupBy(o => o.ProgramSchedule.Id).Select(o => new { price = o.ToList().Sum(p => p.EntryFee), scheduleId = o.FirstOrDefault().ProgramSchedule.Id });
            List<long> selectedschedule = new List<long>();

            var numOfRow = itemGroupByProgram.Count() - discount.NOP + 1;
            if (discount.BenefitType == DiscountBenefitType.Organization)
            {
                selectedschedule.AddRange(itemGroupByProgram.OrderBy(o => o.price).Take(numOfRow).Select(o => o.scheduleId));
            }
            else
            {
                selectedschedule.AddRange(itemGroupByProgram.OrderByDescending(o => o.price).Take(numOfRow).Select(o => o.scheduleId));
            }
            List<OrderItem> discountOrderItems = new List<OrderItem>();
            List<OrderItem> prevDiscountOrderItems = null;
            foreach (var schedule in selectedschedule)
            {
                discountOrderItems.AddRange(orderItems.Where(o => o.ProgramSchedule.Id == schedule));
                if (discountMode == DiscountMode.OrdersInSeason && completedOrders != null && completedOrders.Any())
                {
                    if (prevDiscountOrderItems == null)
                    {
                        prevDiscountOrderItems = new List<OrderItem>();

                    }
                    prevDiscountOrderItems.AddRange(allOrderItems.Where(o => o.ProgramSchedule.Id == schedule));
                }
            }
            decimal extraCharges = 0;
            if (completedOrders != null && completedOrders.Any())
            {
                var appliedDiscounts = completedOrders.SelectMany(c => c.OrderChargeDiscounts).Where(d =>!d.IsDeleted && d.DiscountId == discount.Id).ToList();

                decimal appliedDiscountAmount = appliedDiscounts != null ? (appliedDiscounts.Sum(c => c.Amount) * -1) : 0;

                decimal newDiscountAmount = getdiscountAmount(prevDiscountOrderItems, discount);

                extraCharges = newDiscountAmount - appliedDiscountAmount;
                if (extraCharges <= 0)
                {
                    return;
                }
                if (extraCharges > 0)
                {
                    extraCharges -= getdiscountAmount(discountOrderItems, discount);
                }
            }
            bool onlyExtraAmount = false;
            if (extraCharges > 0 && (discountOrderItems == null || discountOrderItems.Count == 0))
            {
                if (discountOrderItems == null)
                {
                    discountOrderItems = new List<OrderItem>();
                }
                discountOrderItems.Add(orderItems.FirstOrDefault(o => o.EntryFee >= extraCharges));
                onlyExtraAmount = true;
            }
            AddOrderItemsDiscounts(discountOrderItems, discount, ChargeDiscountCategory.MultiSchedule, extraCharges, onlyExtraAmount);
        }

        private decimal getdiscountAmount(List<OrderItem> prevDiscountOrderItems, Discount discount)
        {
            if (isApplyOnOrigin)
            {
                return prevDiscountOrderItems.Sum(o => (discount.AmountType == ChargeDiscountType.Fixed) ? discount.Amount : ((o.EntryFee * discount.Amount) / 100));
            }
            else
            {
                return prevDiscountOrderItems.Sum(o => (discount.AmountType == ChargeDiscountType.Fixed) ? discount.Amount : (((o.EntryFee - (-1 * o.OrderChargeDiscounts.Where(c => !c.IsDeleted && c.Subcategory == ChargeDiscountSubcategory.Discount).Sum(c => c.Amount))) * discount.Amount) / 100));
            }
        }

        private void CalculateCustomDiscountOfOrder(IEnumerable<Discount> CustomDiscounts, List<OrderItem> orderItems)
        {
            if (CustomDiscounts != null && CustomDiscounts.Count() > 0)
            {
                foreach (var discount in CustomDiscounts)
                {
                    List<OrderItem> discountOrderItems = null;
                    if (discount.Programs != null && discount.Programs.Count > 0)
                    {
                        var programList = discount.Programs.Select(p => p.Id);
                        discountOrderItems = orderItems.Where(c => (programList.Contains(c.ProgramSchedule.ProgramId)) == true).ToList();
                    }
                    else
                    {
                        discountOrderItems = orderItems;
                    }

                    AddOrderItemsDiscounts(discountOrderItems, discount, ChargeDiscountCategory.CustomDiscount);
                }

            }

        }

        private void AddOrderItemsDiscounts(IEnumerable<OrderItem> orderItems, Discount discount, ChargeDiscountCategory chargeDiscountCategory, decimal extraDiscountAmount = 0, bool onlyExtraAmount = false)
        {
            foreach (OrderItem item in orderItems)
            {

                if (item == null)
                {
                    return;
                }

                decimal discountAmount = 0;
                if (isApplyOnOrigin)
                {
                    discountAmount = (discount.AmountType == ChargeDiscountType.Fixed) ? discount.Amount : ((item.EntryFee * discount.Amount) / 100);

                    if ((item.ProgramTypeCategory == ProgramTypeCategory.Subscription || item.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && item.PaymentPlanType == PaymentPlanType.FullPaid)
                    {
                        extraDiscountAmount = 0;
                        onlyExtraAmount = false;

                        if (discount.AmountType == ChargeDiscountType.Fixed)
                        {
                            discountAmount = _subscripitonBusiness.GetAppliedSubscriptionItemsCount(item) * discount.Amount;
                        }
                        else
                        {
                            discountAmount = (item.EntryFee * discount.Amount) / 100;
                        }
                    }
                }
                else
                {
                    decimal itemTotalDiscount = 0;

                    if (item.GetOrderChargeDiscounts().ToList().Any(c => c.Subcategory == ChargeDiscountSubcategory.Discount && c.Category != ChargeDiscountCategory.Coupon))
                    {
                        itemTotalDiscount = (item.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount && c.Category != ChargeDiscountCategory.Coupon).Sum(c => c.Amount) * -1);
                    }

                    discountAmount = (discount.AmountType == ChargeDiscountType.Fixed) ? discount.Amount : (((item.EntryFee - itemTotalDiscount) * discount.Amount) / 100);

                }

                discountAmount = onlyExtraAmount ? extraDiscountAmount : discountAmount + extraDiscountAmount;

                extraDiscountAmount = 0;
                discountAmount = Common.Helper.CurrencyHelper.RoundCurrencyWithPenny(discountAmount);

                TotalDiscountAmount += discountAmount;
                var orderDiscount = new OrderChargeDiscount()
                {
                    Name = discount.Name,
                    Amount = ((-1) * discountAmount),
                    Category = chargeDiscountCategory,
                    Description = string.Format("${0} off for {1}", discountAmount, discount.Name),
                    DiscountId = discount.Id,
                    Discount = discount,
                    OrderItemId = item.Id,
                    Subcategory = ChargeDiscountSubcategory.Discount
                };

                item.OrderChargeDiscounts.Add(orderDiscount);
            }
        }

        public IEnumerable<Discount> GetProgramsDiscounts(Program program)
        {
            var seasonId = program.Season.Id;
            List<Discount> programDiscounts = new List<Discount>();
            programDiscounts.AddRange(program.Discounts.Where(d => d.IsDeleted == false && d.SeasonId == seasonId));
            programDiscounts.AddRange(_discountRepository.GetList().Where(d => d.SeasonId == seasonId && d.IsDeleted == false && d.IsAllProgram == true));
            return programDiscounts.Distinct();
        }

        public IEnumerable<Discount> GetProgramsDiscounts(IEnumerable<Program> selectedProgram)
        {
            var seasonId = selectedProgram.FirstOrDefault().SeasonId;

            List<Discount> programDiscounts = new List<Discount>();
            programDiscounts.AddRange(selectedProgram.Where(c => c.Discounts != null).SelectMany(c => c.Discounts).Where(d => d.IsDeleted == false && d.SeasonId == seasonId));
            programDiscounts.AddRange(_discountRepository.GetList().Where(d => d.SeasonId == seasonId && d.IsDeleted == false && d.IsAllProgram == true));
            return programDiscounts.Distinct();
        }

        private List<string> GetSeasonDiscountNames(List<long> seasonIds, bool isDeleted)
        {
            return GetList(seasonIds, isDeleted)
                .Select(c => new { c.IsDeleted, c.Name })
                .AsEnumerable()
                .DistinctBy(c => c.Name)
                .Select(c => c.IsDeleted ? $"{c.Name} (deleted)" : c.Name)
                .ToList();
        }

        private List<string> GetSeasonCouponNames(List<long> seasonIds, bool isDeleted)
        {
            return _couponBusiness.GetList(seasonIds, isDeleted)
                .Select(c => new { c.Deleted, c.Name })
                .AsEnumerable()
                .DistinctBy(c => c.Name)
                .Select(c => c.Deleted ? $"{c.Name} (deleted)" : c.Name)
                .ToList();
        }

        public List<string> GetAllSeasonDiscountCoupon(long seasonId)
        {
            var allDiscountsCoupons = new List<string>();

            allDiscountsCoupons.AddRange(GetSeasonDiscountNames(new List<long> {seasonId}, true));

            allDiscountsCoupons.AddRange(GetSeasonCouponNames(new List<long> { seasonId }, true));

            return allDiscountsCoupons;
        }

        public List<string> GetAllSeasonPartnerDiscountCoupon(int partnerId)
        {
            var allDiscountsCoupons = new List<string>();

            var seasonIds = _clubBusiness.GetList().Where(c => c.PartnerId == partnerId).SelectMany(c => c.Seasons).Where(c => c.Status != SeasonStatus.Deleted).Select(c => c.Id).ToList();

            allDiscountsCoupons.AddRange(GetSeasonDiscountNames(seasonIds, true));

            allDiscountsCoupons.AddRange(GetSeasonCouponNames(seasonIds, true));

            return allDiscountsCoupons;
        }

        public List<string> GetAllSeasonClubDiscountCoupon(int clubId)
        {
            var allDiscountsCoupons = new List<string>();

            var seasonIds = _seasonBusiness.GetList(clubId).Where(c => c.Status != SeasonStatus.Deleted).Select(c => c.Id).ToList();

            allDiscountsCoupons.AddRange(GetSeasonDiscountNames(seasonIds, true));

            allDiscountsCoupons.AddRange(GetSeasonCouponNames(seasonIds, true));

            return allDiscountsCoupons;
        }

        private decimal TotalDiscountAmount
        {
            get;
            set;
        }
        private bool isApplyOnOrigin = false;
        public OperationStatus CopyInSeason(long discountId, long seasonId, int clubId, string newSeasonDomain)
        {
            var dbDiscount = _discountRepository.GetList().AsNoTracking().SingleOrDefault(d => d.Id == discountId);

            dbDiscount.SeasonId = seasonId;

            dbDiscount.Season = null;

            if (!dbDiscount.IsAllProgram)
            {
                var programs = dbDiscount.Programs.Select(p => p.Domain).ToList();
                dbDiscount.Programs.Clear();

                foreach (var newProgram in programs.Select(program => _programBusiness.Get(clubId, newSeasonDomain, program)).Where(newProgram => newProgram != null))
                {
                    dbDiscount.Programs.Add(newProgram);
                }
            }

            return _discountRepository.Create(dbDiscount);
        }

        public OperationStatus Create(Discount discount)
        {
            return _discountRepository.Create(discount);
        }

        public OperationStatus Update(Discount discount)
        {
            return _discountRepository.Update(discount);
        }
    }
}
