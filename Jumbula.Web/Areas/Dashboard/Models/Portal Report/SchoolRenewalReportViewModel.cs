﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SchoolRenewalReportViewModel
    {
        public SchoolRenewalReportViewModel()
        {
            //Programs = new List<SchoolsProgramsEnrollmentReportViewModel>();
        }

        public string SchoolName { get; set; }
        public string SchoolLogo { get; set; }
        public SeasonNames SeasonName { get; set; }
        public int Year { get; set; }
        public string StrYear { get; set; }
        public int TotalEnrollment { get; set; }
        public List<SchoolProgramsRenewalReportViewModel> Programs { get; set; }
    }
}