﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('contactsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', 'seasonService', 'customReportService', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation, seasonService, customReportService) {
        window.app.globalObjects.pageClass = 'catalog-view';//seasons-view programs
        window.app.globalObjects.initializeView();
        $scope.MC.openBlade({ url: "Dashboard/Blade/Contact", order: 0, menu: "CONTACT" });
        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value) {
            if (value.Domain === $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        $http.get(window.location.origin + "/Dashboard/Contact/GetFilters").success(function (data) {
            $scope.Filters = data;

        }).error(function (data) {
            $scope.MC.handleErrors($scope, data);
        });

        $http.get(window.location.origin + '/Dashboard/Contact/GetListSettings').success(function (data) {
            $scope.Model = data;

            $scope.loadGrid();

        }).error(function (data) {
            $scope.MC.handleErrors($scope, data);
        });

        $scope.loadGrid = function () {
            $scope.contactDataSource = new kendo.data.DataSource({
                dataType: "json",
                transport: {
                    read:
                        {
                            url: window.location.origin + '/Dashboard/Contact/GetList/',
                            contentType: "application/json; charset=utf-8",
                            type: "POST"
                        },
                    parameterMap: function (data, operation) {
                        if (operation === "read") {
                            data = $.extend(
                                {
                                    sort: null,
                                    filters: $scope.Filters 
                                },
                                data);

                            return JSON.stringify(data);
                        }
                    }
                },
                schema:
                    {
                        total: "TotalCount",
                        data: "DataSource"
                    },
                sort:
                    [{
                        field: "FullName",
                        dir: "asc"
                    }],
                pageSize: 50,
                batch: true,
                serverFiltering: true,
                serverPaging: true,
                serverSorting: true,
                requestStart: function () {
                    kendo.ui.progress($('[kendo-grid]'), true);
                },
                requestEnd: function () {
                    kendo.ui.progress($('[kendo-grid]'), false);
                }
            });

            $scope.contactGridOptions = {
                dataSource: $scope.contactDataSource,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                        [
                            5, 10, 20, 30, 50, 100
                        ],
                    buttonCount: 5
                },
                dataBound: function () {
                    customReportService.initializeDoubleScroll();
                },
                columns: [{
                    field: "",
                    title: "",
                    template: "<span> #if(IsStaff){#\ #if(StaffStatus == 'Active'){# <i class='jbi-user-male staff-contact' title='Active staff' style='color:rgb(0, 194, 124)'></i> #}# #if(StaffStatus == 'Pending'){# <i class='jbi-user-male staff-contact' title='Pending staff' style='color:rgb(251, 204, 24)'></i> #}# #if(StaffStatus == 'Frozen'){# <i class='jbi-user-male staff-contact' style='color:rgb(208, 46, 32)' title='Frozen staff'></i> #}#  #}else{# #if(IsPrimary){# <i class='jbi-user-male staff-contact' title='Primary contact' style='color:rgb(0,0,0)'></i> #}else{#<i class='jbi-user-male staff-contact' title='Contact' style='color:rgb(171, 171, 171)'></i> #}}# </span>",
                    width: "26px",
                    sortable: false
                },
                {
                    field: "FullName",
                    title: "Name",
                    width: "210px"
                },
                {
                    field: "OrganizationName",
                    title: "Organization",
                    width: "180px"
                },
                {
                    field: "Email",
                    title: "Email",
                    width: "250px",
                    hidden: !$scope.Model.ShowEmail
                },
                {
                    field: "Phone",
                    title: "Phone",
                    sortable: false,
                    hidden: !$scope.Model.ShowPhone,
                    width: "170px"
                },
                {
                    field: "CellPhone",
                    title: "Cell phone",
                    //width: "160px",
                    sortable: false,
                    hidden: !$scope.Model.ShowCellPhone,
                    width: "130px"
                },
                {
                    field: "WorkPhone",
                    title: "Work phone",
                    sortable: false,
                    hidden: !$scope.Model.ShowWorkPhone,
                    width: "130px"
                },
                {
                    field: "Nickname",
                    title: "Nickname",
                    sortable: true,
                    hidden: !$scope.Model.ShowNickname,
                    width: "130px"
                },
                {
                    field: "Title",
                    title: "Title / Role",
                    sortable: false,
                    hidden: !$scope.Model.ShowTitle,
                    width: "250px"
                },
                {
                    field: "DoB",
                    title: "Date of birth",
                    sortable: false,
                    hidden: !$scope.Model.ShowDoB,
                    width: "130px"
                },
                {
                    field: "",
                    title: "Actions",
                    width: "70px",
                    headerTemplate: "<span class='actions-title'>Actions</span>",
                    template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
                                <li> ... \
                                    <ul>\
									   <li ui_sref='EditContact({contactId: #: Id#})'>\
                                                <span class='k-link jbi-pencil'>Edit</span>\
                                        </li>\
                                        <li ng-hide='#= IsLimitedForChange == true #' ng-click='deleteContact(#= Id #)'>\
                                                <span class='k-link jbi-remove'>Delete</span>\
                                        </li>\
                                    </ul>\
                                </li>\
                              </ul>"
                }
                ]
            };

        }

        $scope.deleteContact = function (contactId) {

            jbConfirmation
                .yes(function () {
                    $http.post('Dashboard/Contact/Delete', { id: contactId })
                        .success(function (data) {

                            if (data.Status) {

                                jbToast.success("Contact deleted successfully.", "");

                                $scope.contactGridOptions.dataSource.read();
                            }
                            else {
                                $scope.MC.handleErrors($scope, data);
                            }

                        });
                })
                .confirm();
        }

        $scope.search = function () {
            $scope.contactGridOptions.dataSource.read();
        }

        $scope.clearFilters = function () {
            $scope.Filters.Term = '';
            $scope.Filters.SearchType = 'Name';
            $scope.Filters.Title = null;
            $scope.Filters.MemberId = null;

            $scope.contactGridOptions.dataSource.read();
        }

        $scope.init = function () {
            $('[data-role=dropdown]').dropdown();
        }
    }]);
})();

