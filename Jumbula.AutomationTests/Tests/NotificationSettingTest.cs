﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Helper.Element;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Tests
{
    class NotificationSettingTest : BaseTest<NotificationSettingElements>
    {
        public NotificationSettingTest(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public void SaveConfirmationEmail()
        {
            Assert.AreEqual("Class confirmation email", Elements.ConfirmationEmailBody.GetAttribute("value"));
            Elements.ConfirmationEmailBody.Clear();
            Elements.ConfirmationEmailBody.SendKeys("The class has been confirmed.");
            Elements.SaveConfirmationBtn.Click();
            ExtraAPI.Wait(5000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Notifications updated successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();
        }

        public void SaveCancellationEmail()
        {
            Assert.AreEqual("Class cancellation email", Elements.CancellationEmailBody.GetAttribute("value"));
            Elements.CancellationEmailBody.Clear();
            Elements.CancellationEmailBody.SendKeys("The class has been cancelled.");
            Elements.SaveConfirmationBtn.Click();
            ExtraAPI.Wait(1000);
            Assert.AreEqual("Success", Elements.MessageboxTitle.Text);
            Assert.AreEqual("Notifications updated successfully.", Elements.MessageboxBody.Text);
            Elements.MessageboxTitle.Click();
        }
    }
}
