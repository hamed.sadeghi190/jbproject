﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('graduateSelectGradeReportsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', function ($scope, $http, $log, $stateParams, $state, jbToast) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;
        $scope.seasonName = $scope.stateParams.seasonDomain;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });
        $scope.baseUrl = window.location.origin + "/Dashboard/PortalReport/";

        $scope.Model = {};

        $http.get($scope.baseUrl + '/GetGradeInfo').success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        $scope.runReport = function (grade) {
            if (grade == 0) {

                jbToast.error("You should select grade.")
            } else {
                $state.go('GraduateReports', { grade: grade });
            }
            
        }

    }]);
})();