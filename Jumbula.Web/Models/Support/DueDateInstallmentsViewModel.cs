﻿using System;

namespace Jumbula.Web.Models
{
    public class DueDateInstallmentsViewModel
    {
        public long Id { get; set; }
        public DateTime InstallmentDate { get; set; }
        public decimal? PaidAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance => Amount - (PaidAmount ?? 0);
        public string FirstName;
        public string LastName;
    }
}