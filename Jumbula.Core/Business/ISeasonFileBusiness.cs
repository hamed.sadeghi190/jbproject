﻿using Jb.Framework.Common;
using OperationStatus = Jumbula.Core.Data.OperationStatus;

namespace Jumbula.Core.Business
{
    public interface ISeasonFileBusiness
    {
        OperationStatus Create(long seasonId, int FileId);

    }
   
}
