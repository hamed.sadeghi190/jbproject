﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('RoomAssignmentsAdminReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();

        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/CustomReport';

        $scope.Model = {};
        var dataInfo = {};
        $scope.filter = {};
        $scope.filter.printMode = false;
        
        var nameModel = customReportService.getModelFromStrorage();
        
        $http.get(baseUrl + '/GetRoomAssignmentsReportHeader', { params: { seasonDomain: $scope.stateParams.seasonDomain } }).success(function (data, status, headers, config) {
            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            $scope.SeasonName = data.SeasonName;
            dataInfo = data;
            SetTheGrid();
        }).error(function (data, status, headers, config) {
            SetTheGrid();
            jbToast.error(data.Message);
        });


        $scope.RoomAssignmentDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: baseUrl + '/GetRoomAssignments',
                        type: "POST",
                        data: function () {
                            return {
                                seasonDomain: $scope.stateParams.seasonDomain,
                                printMode: $scope.filter.printMode
                            };
                        },
                    },

                },
            sort: { field: 'ClassName', dir: 'asc' },
            schema: {
                total: "TotalCount",
                data: "DataSource"
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
            batch: true,
        });
        function SetTheGrid() {
            $scope.RoomAssignmentReportGridOptions = {
                excel: {
                    allPages: true,
                    fileName: $scope.ClubName + " (Room assignments).xlsx",
                    proxyURL: "/Dashboard/PortalReport/Save"
                },
                excelExport: function (e) {
                    var rows = e.workbook.sheets[0].rows;

                    rows.unshift({
                        cells: [{
                            value: ""
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.Date
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: dataInfo.SeasonName
                        }]
                    });
                    rows.unshift({
                        cells: [{
                            value: "Room assignments - " + dataInfo.ClubName
                        }]
                    });

                    cfpLoadingBar.complete();
                },
                dataSource: $scope.RoomAssignmentDataSource,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                    buttonCount: 5
                },
                dataBound: function (e) {
                    customReportService.initializeDoubleScroll();
                },
                sortable: { mode: 'single' },
                columns: [
                    
                    {
                        field: "ClassName",
                        title: "Class name",
                        width: "180px",
                    },
                    {
                        field: "SpaceReq",
                        title: "Space Requirements",
                        width: "200px",
                    },
                    {
                        field: "AdditionalComments",
                        title: "Additional comments",
                        width: "200px",
                    },
                    {
                        field: "Duration",
                        title: "Class Duration",
                        width: "200px",
                    },
                    //{
                    //    field: "Day",
                    //    title: "Day of the Week",
                    //    width: "200px",
                    //},
                    {
                        field: "Sessions",
                        title: "Meeting Dates",
                        width: "200px",
                    },
                    {
                        field: "Min",
                        title: "Minimum",
                        width: "200px",
                    },
                    {
                        field: "Max",
                        title: "Maximum",
                        width: "200px",
                    },
                    {
                        field: "Room",
                        title: "Room Assignment",
                        width: "200px",
                    }
                ],
            }
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $scope.filter.printMode = false;
            $("#RoomAssignmentReportGrid").getKendoGrid().saveAsExcel();
        }

        
        $scope.saveToPDF = function () {
            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetRoomAssignments', {
                seasonDomain: $scope.stateParams.seasonDomain,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (Room assignments).pdf";
                a.click();
            });
            $scope.filter.printMode = false;

        }

        $scope.print = function () {
            $scope.filter.printMode = true;
            $http.post(baseUrl + '/GetRoomAssignments', {
                seasonDomain: $scope.stateParams.seasonDomain,
                printMode: $scope.filter.printMode
            }, {
                headers: { 'Content-Type': 'application/json' }, responseType: 'arraybuffer'
            }).success(function (data) {
                var a = document.createElement("a");
                document.body.appendChild(a);
                var blob = new Blob([data], { type: "application/pdf" });

                var fileURL = URL.createObjectURL(blob);
                a.href = fileURL;
                a.download = $scope.ClubName + " (Room assignments).pdf";
                a.click();
            });
            $scope.filter.printMode = false;
        }
    }
    ]);

})();