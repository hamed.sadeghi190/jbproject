﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Models
{
    public class ClubServiceModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public ClubLocation Location { get; set; }
        public string Tel { get; set; }
        public string Domain { get; set; }
        public string Site { get; set; }
        public string SiteLink { get; set; }
        public string Email { get; set; }
        public string LogoUri { get; set; }
        public List<ClubGalleryImage> Gallery { get; set; }
        public List<ClubEvent> Events { get; set; }
        public List<ClubCoach> Coaches { get; set; }
        public class ClubLocation
        {
            public double Lat { get; set; }
            public double Lng { get; set; }
        }
        public class ClubGalleryImage
        {
            public string Title { get; set; }
            public string Description { get; set; }
            public string ImageUri { get; set; }
            public string ThumbnamilUri { get; set; }

        }
        public class ClubEvent
        {
            public string Title { get; set; }
            public string Address { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }
        public class ClubCoach
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public string ImageUri { get; set; }
        }
    }
}