﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Order;

namespace SportsClub.Areas.Dashboard.Models
{
    public class CancelSubscriptionOrderViewModel
    {
        public CancelSubscriptionOrderViewModel()
        {
            UpdatedChargeDiscounts = new List<ChargeDiscountItemViewModel>();
            PastItemSchedules = new List<InstallmentandScheduleItemViewModel>();
            DeletedItemSchedules = new List<InstallmentandScheduleItemViewModel>();
            UpdatedDiscounts = new List<ChargeDiscountItemViewModel>();
            UpdatedCharges = new List<ChargeDiscountItemViewModel>();
        }
        public long OrderItemId { get; set; }
        public DateTime? DesiredStartDate { get; set; }
        public DateTime TodayDate { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string StrEffectiveDate
        {
            get
            {
                return EffectiveDate.HasValue ? EffectiveDate.Value.ToString("MMM dd, yyyy") : null;
            }
        }
        public string StrDesiredStartDate
        {
            get
            {
                return DesiredStartDate.HasValue ? DesiredStartDate.Value.ToString("MMM dd, yyyy") : null;
            }
        }
        public ProgramTypeCategory ProgramType { get; set; }

        public PaymentPlanType PaymentPlan { get; set; }
        public List<InstallmentandScheduleItemViewModel> ItemSchedules { get; set; }
        public List<InstallmentandScheduleItemViewModel> PastItemSchedules { get; set; }
        public List<InstallmentandScheduleItemViewModel> DeletedItemSchedules { get; set; }
        public decimal Balance { get; set; }
        public decimal PaidAmount { get; set; }
        public bool SendEmail { get; set; }
        public decimal NewBalance { get; set; }
        public decimal NewTotalAmount { get; set; }
        public decimal RefundFee { get; set; }
        public decimal NewPaidAmount { get; set; }
        public string FullName { get; set; }
        public string ProgramName { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal SourceEntryFee { get; set; }
        public decimal EntryFee { get; set; }
        public bool IsEffectiveDateBeforeDesired { get; set; }
        public string Name { get; set; }
        public object Gender { get; set; }
        public int Age { get; set; }
        public bool HasProgramSchedule { get; set; }
        public decimal CancellationFee { get; set; }
        public string CancelMemo { get; set; }
        public string ConfirmationId { get; set; }
        public DateTime? LastSessionDate { get; set; }
        public OrderItem OrderItem { get; set; }
        public decimal PaidInstallmentAmount { get; set; }
        public decimal PaidAndPastInstallmentAmount { get; set; }
        public decimal UnPaidAndPastInstallmentAmount { get; set; }
        public List<ChargeDiscountItemViewModel> SourceCharges { get; set; }
        public List<ChargeDiscountItemViewModel> SourceDiscounts { get; set; }
        public List<ChargeDiscountItemViewModel> UpdatedChargeDiscounts { get; set; }
        public List<ChargeDiscountItemViewModel> UpdatedDiscounts { get; set; }
        public List<ChargeDiscountItemViewModel> UpdatedCharges { get; set; }
        public bool ShowCancellationFeeBox { get; set; }

    }
    public class InstallmentandScheduleItemViewModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal? PaidAmount { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaymentDueDate { get; set; }
        public long InstallmentId { get; set; }
        public long? ProgramPartId { get; set; }
        public decimal PartAmount { get; set; }
    }
    public class CancelSubscriptionInstallmentViewModel
    {
        public string Token { get; set; } 
        public DateTime InstallmentDate { get; set; }
        public DateTime? PaidDate { get; set; }
        public decimal? PaidAmount { get; set; }
        public decimal Amount { get; set; }
        public OrderStatusCategories Status { get; set; }
        public bool NoticeSent { get; set; }
        public bool EnableReminder { get; set; }
        public long Id { get; set; }
        public InstallmentType Type { get; set; }
        public long OrderItemId { get; set; }
        public bool IsDeleted { get; set; }
    }
}