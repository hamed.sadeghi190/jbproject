
alter table CatalogItems add [MinimumEnrollment] [int] NULL
alter table CatalogItems add [MaximumEnrollment] [int] NULL
alter table CatalogItems add [ISRatio] [int] NULL
alter table CatalogItems add [Status] [tinyint] NOT NULL
alter table CatalogItems add [Detail] [nvarchar](500) NULL

alter table CatalogItems drop column IsDeleted
