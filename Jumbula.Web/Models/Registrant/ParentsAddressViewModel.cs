﻿using Jb.Framework.Common.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsClub.Models
{
    public class ParentsAddressViewModel
    {
  
        [Required(ErrorMessage = "You must enter address line1.")]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "You must enter zip.")]
        public string Zip { get; set; }

        [Required(ErrorMessage = "You must enter country.")]
        public string Country { get; set; }

        [Required(ErrorMessage = "You must enter state.")]
        public string State { get; set; }

        [Required(ErrorMessage = "You must enter city.")]
        public string City { get; set; }
        public int FamilyId { get; set; }

    }
    
}