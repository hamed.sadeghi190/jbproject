﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('holidaysSettingController', ['$scope', '$http', '$state', 'jbToast', function ($scope, $http, $state, jbToast) {
        window.app.globalObjects.pageClass = 'general-settings-view';
        window.app.globalObjects.initializeView();

        $scope.Model = {};
        $http.get('Dashboard/Setting/GetHolidays').success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });


        $scope.classdates = '';
        $scope.classAMdates = '';
        $scope.classPMdates = '';
        $scope.datetimePickerChanged = function (e, dayOfWeek) {

            if (dayOfWeek == 'Dates') {
                $scope.Model.AllHolidayDates.push({ Text: $scope.classdates, Value: $scope.classdates });
                $scope.Model.HolidayDates.push($scope.classdates);
            }
            if (dayOfWeek == 'AMdates') {
                $scope.Model.AllHolidayAMDates.push({ Text: $scope.classAMdates, Value: $scope.classAMdates });
                $scope.Model.HolidayAMDates.push($scope.classAMdates);
            }
            if (dayOfWeek == 'PMdates') {
                $scope.Model.AllHolidayPMDates.push({ Text: $scope.classPMdates, Value: $scope.classPMdates });
                $scope.Model.HolidayPMDates.push($scope.classPMdates);
            }
        }

        $scope.save = function () {

            $http.post('Dashboard/Setting/SaveHolidays', { model: $scope.Model })
             .success(function (data, status, headers, config) {

                 if (data.Status) {

                     $scope.errors = null;

                     jbToast.success('Holiday calendar updated successfully.');
                 }
                 else {
                     $scope.MC.handleErrors($scope, data);
                 }
             })
        }

    }])
})();