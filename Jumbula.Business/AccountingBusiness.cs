﻿using Jumbula.Common.Enums;
using Jumbula.Core.Business;
using Jumbula.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Business
{
    public class AccountingBusiness : IAccountingBusiness
    {
        public decimal OrderItemBalance(OrderItem orderItem)
        {

            if (orderItem.ItemStatus == OrderItemStatusCategories.completed)
            {
                return orderItem.TotalAmount - orderItem.PaidAmount;
            }
            if (orderItem.ItemStatus == OrderItemStatusCategories.changed && orderItem.ItemStatusReason == OrderItemStatusReasons.canceled)
            {
                decimal cancellationfee = 0;
                decimal prorateFee = 0;

                if ((orderItem.ProgramTypeCategory != ProgramTypeCategory.Subscription && orderItem.ProgramTypeCategory != ProgramTypeCategory.BeforeAfterCare && orderItem.PaymentPlanType == PaymentPlanType.FullPaid) 
                    || orderItem.Mode == OrderItemMode.DropIn || orderItem.Mode == OrderItemMode.PunchCard)
                {
                    if (orderItem.OrderChargeDiscounts.Any(o => !o.IsDeleted) && orderItem.GetOrderChargeDiscounts() != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CancellationFee))
                    {
                        cancellationfee = orderItem.OrderChargeDiscounts.Single(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.CancellationFee).Amount;
                    }

                    if (orderItem.OrderChargeDiscounts.Any(o => !o.IsDeleted) && orderItem.GetOrderChargeDiscounts() != null && orderItem.OrderChargeDiscounts.Any(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ProrateFee))
                    {
                        prorateFee = orderItem.OrderChargeDiscounts.Single(c => !c.IsDeleted && c.Category == ChargeDiscountCategory.ProrateFee).Amount;
                    }

                    return -1 * (orderItem.PaidAmount - cancellationfee - prorateFee);
                }
                else
                {
                    return orderItem.TotalAmount - orderItem.PaidAmount;
                }

            }
            if ((orderItem.ProgramTypeCategory == ProgramTypeCategory.Subscription || orderItem.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare) && orderItem.ItemStatusReason == OrderItemStatusReasons.transferOut)
            {
                return orderItem.TotalAmount - orderItem.PaidAmount;
            }
            return 0;
        }
        public decimal OrderItemsBalance(List<OrderItem> orderItems)
        {
            decimal balance = 0;
            foreach (var item in orderItems)
            {
                balance += OrderItemBalance(item);
            }
            return balance;
        }
        public decimal OrderItemsBalance(IQueryable<OrderItem> orderItems)
        {
            return OrderItemsBalance(orderItems.ToList());
        }
        public decimal GetClientBalance(Client client)
        {
            decimal balance = 0;
            if (client.BillingHistories != null && client.BillingHistories.Any())
            {
                balance = client.BillingHistories.Sum(c => (c.Debit - c.Credit));
            }
            return balance;
        }
        public decimal OrderBalance(Order order)
        {

            decimal balance = 0;
            foreach (var orderItem in order.RegularOrderItems)
            {
                balance += OrderItemBalance(orderItem);
            }

            return balance;
        }
        public decimal OrderItemDueAmount(OrderItem orderItem, DateTime? date = null)
        {
            if (!date.HasValue)
            {
                date = DateTime.UtcNow;
            }
            if (orderItem.ItemStatus == OrderItemStatusCategories.completed || orderItem.ItemStatusReason == OrderItemStatusReasons.transferIn)
            {
                var paidAmount = orderItem.PaidAmount;// Ioc.TransactionActivityBusiness.GetPaidAmountForOrderItem(orderItem.Id);
                var totalAmount = orderItem.TotalAmount;
                if (orderItem.PaymentPlanType == PaymentPlanType.Installment && orderItem.Installments != null && orderItem.Installments.Count > 0)
                {
                    totalAmount = orderItem.Installments.Where(i => i.InstallmentDate <= date.Value).Sum(i => i.Amount);
                }
                var dueAmount = totalAmount - paidAmount;
                return dueAmount > 0 ? dueAmount : 0;
            }
            else
            {
                return 0;
            }
        }
        public decimal OrderDueAmount(Order order, DateTime? date = null)
        {
            if (!date.HasValue)
            {
                date = DateTime.UtcNow;
            }
            decimal dueAmount = 0;
            foreach (var orderItem in order.RegularOrderItems)
            {
                dueAmount += OrderItemDueAmount(orderItem, date);
            }

            return dueAmount;
        }
    }
}
