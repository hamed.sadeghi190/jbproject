﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;

namespace SportsClub.Models
{
    public class DeleteDialogViewModel
    {
        public string Title { get; set; }

        public string Question { get; set; }

        public string Note { get; set; }

        public string ActionName { get; set; }

        public string ControllerName { get; set; }

        public object RouteValues { get; set; }
    }

    public class MessageBoxViewModel
    {
        public string Title { get; set; }

        public string Message { get; set; }

        public string OKButtonText { get; set; }

        //these parameters help to go to ~/playerprofile/ChooseProfileMember

        public string Domain { get; set; }

        public string ClubDomain { get; set; }

        public SubDomainCategories SubDomainCategory { get; set; }

        public int CategoryId { get; set; }
    }
}