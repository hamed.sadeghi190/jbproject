﻿using Jumbula.Common.Base;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SysAdminRefundConfirmationViewModel : BaseViewModel<ClientBillingHistory, long>
    {
        public int ClubId { get; set; }

        public decimal RefundAmount { get; set; }

        public string RefundReason { get; set; }
        public string ClubName { get; set; }
        public bool RefundAutomatically { get; set; }
        public string BillingId { get; set; }
        public string Description { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string Balance { get; set; }
        public string BalanceView { get; set; }
        public bool IsSelected { get; set; }
        public CurrencyCodes Currency { get; set; }
        public bool Editable { get; set; }
        public string BillingDate { get; set; }


    }
}