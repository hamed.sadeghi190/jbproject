﻿
using Jb.Framework.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;

namespace Jumbula.Business
{
    public class PendingPreapprovalTransactionBusiness :  IPendingPreapprovalTransactionBusiness
    {
        private readonly IRepository<PendingPreapprovalTransaction, long> _pendingPreapprovalTransactionRepository;

        public PendingPreapprovalTransactionBusiness(IRepository<PendingPreapprovalTransaction, long> pendingPreapprovalTransactionRepository)
        {
            _pendingPreapprovalTransactionRepository = pendingPreapprovalTransactionRepository;
        }

        public PendingPreapprovalTransaction Get(long PendingPreapprovalId)
        {
            return _pendingPreapprovalTransactionRepository.Get(d => d.Id ==PendingPreapprovalId);
        }
        public IQueryable<PendingPreapprovalTransaction> GetListByOrderId(long orderId)
        {
            return _pendingPreapprovalTransactionRepository.GetList().Where(o => o.OrderId == orderId);
        }
        public bool OrderHasPendingTransaction(long orderId)
        {
            var result = GetListByOrderId(orderId);
            if(result==null && result.Count()==0)
            {
                return false;
            }
            return true;
        }
        public IQueryable<PendingPreapprovalTransaction> GetListByDueDate(DateTime dueDate)
        {
            return _pendingPreapprovalTransactionRepository.GetList().Where(o => o.DueDate <= dueDate && ( o.Status == PendingPreapprovalTransactionStatus.Pending || o.Status==PendingPreapprovalTransactionStatus.PartiallyCompleted));
        }
        public IQueryable<PendingPreapprovalTransaction> GetList()
        {
            return _pendingPreapprovalTransactionRepository.GetList();
        }
        public OperationStatus CreateList(List<PendingPreapprovalTransaction> pendingTransaction)
        {
            try
            {
                foreach (var item in pendingTransaction)
                {
                    _pendingPreapprovalTransactionRepository.Create(item);
                }
                return new OperationStatus() { Status = true, Message = string.Empty };
            }
            catch (Exception ex)
            {
                return new OperationStatus() { Status = false, Message = ex.Message };
            }
        }

        public OperationStatus DeleteWhere(Expression<Func<PendingPreapprovalTransaction, bool>> filter = null, Func<IQueryable<PendingPreapprovalTransaction>, IOrderedQueryable<PendingPreapprovalTransaction>> orderBy = null, string includeProperties = "")
        {
            return _pendingPreapprovalTransactionRepository.DeleteWhere(filter, orderBy, includeProperties);
        }

        public OperationStatus Update(PendingPreapprovalTransaction pendingPreapproval)
        {
            return _pendingPreapprovalTransactionRepository.Update(pendingPreapproval);
        }

        public OperationStatus Create(PendingPreapprovalTransaction pendingPreapproval)
        {
            return _pendingPreapprovalTransactionRepository.Create(pendingPreapproval);
        }
    }
    
}
