﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('policySettingsController', ["$scope", '$http', function ($scope, $http) {
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/PolicySettings", order: 2, menu: "SETTINGS" });
    }]);
})();