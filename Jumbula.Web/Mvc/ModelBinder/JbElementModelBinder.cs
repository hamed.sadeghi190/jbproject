﻿using System;
using System.Web.Mvc;
using Jumbula.Common.Constants;

namespace Jumbula.Web.Mvc.ModelBinder
{
    public class JbElementModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            string typename;
            Type type;
            var typeValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName + ".ModelType");
            if (typeValue == null)
                typename = "Jb.Framework.Common.Forms.Jb" + bindingContext.ValueProvider.GetValue(bindingContext.ModelName + ".TypeTitle").ConvertTo(typeof(string));
            else
                typename = (string)typeValue.ConvertTo(typeof(string));
            
            if (typename.Contains("Jb.Framework.Common"))
                type = Type.GetType(typename + ",Jb.Framework.Common", true);
            else if (typename.Contains("USCFInfo"))
                type = Type.GetType("Jb.Framework.Common.Forms.USCFInfo" + ",Jb.Framework.Common", true);
            else
                type = Type.GetType(typename + "," + typename.Split('.')[0], true);


            var model = Activator.CreateInstance(type);
            bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => model, type);
            return model;
        }


    }

    public class CustomElementModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            var typeValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName + ".ModelType");
            var type = Type.GetType(
                (string)typeValue.ConvertTo(typeof(string)),
                true
            );
            var model = Activator.CreateInstance(type);
            bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => model, type);
            return model;
        }
    }
}