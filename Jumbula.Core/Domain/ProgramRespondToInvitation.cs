﻿using Jumbula.Common.Base;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jumbula.Core.Domain
{

    public class ProgramRespondToInvitation : BaseEntity
    {
        private BaseRespond _respond;
        public ProgramRespondType RespondType { get; set; }
        [ForeignKey("Program")]
        public long ProgramId { get; set; }

        public virtual Program Program { get; set; }

        [Column("Respond")]
        public string RespondSerialized { get; set; }
        [NotMapped]
        public BaseRespond Respond
        {
            get
            {
                if (_respond == null)
                {

                    if (!string.IsNullOrEmpty(RespondSerialized))
                    {
                        switch (RespondType)
                        {
                            case ProgramRespondType.Accept:
                                _respond = JsonConvert.DeserializeObject<AcceptRespond>(RespondSerialized);
                                break;
                            case ProgramRespondType.Decline:
                                _respond = JsonConvert.DeserializeObject<DeclineRespond>(RespondSerialized);
                                break;
                            default:
                                _respond = JsonConvert.DeserializeObject<BaseRespond>(RespondSerialized);
                                break;
                        }

                    }
                    else
                    {
                        switch (RespondType)
                        {
                            case ProgramRespondType.Accept:
                                _respond = new AcceptRespond();
                                break;
                            case ProgramRespondType.Decline:
                                _respond = new DeclineRespond();
                                break;
                            default:
                                _respond = new BaseRespond();
                                break;
                        }
                    }
                }

                return _respond;
            }

            set
            {
                RespondSerialized = JsonConvert.SerializeObject(value);

            }
        }
    }

   

    public class BaseRespond
    {
        public string Note { get; set; }
    }
    public class AcceptRespond : BaseRespond
    {
        public bool AvailableAnyDay { get; set; }
        public DayOfWeek FirstDayPreference { get; set; }
        public DayOfWeek SecondDayPreference { get; set; }
    }

    public class DeclineRespond : BaseRespond
    {
        public PorgramRespondDeclineResaon DeclineResaon { get; set; }
    }

    public enum ProgramRespondType
    {
        Accept = 1,
        Modify = 2,
        Decline = 3, 
        //ModifyRequest = 4
    }
    public enum PorgramRespondDeclineResaon
    {
        [Description("No available staff/no more capacity")]
        NoAvailableStaff = 1,
        [Description("Location is too far")]
        LocationIsTooFar = 2,
        [Description("Not available on days requested")]
        NotAvailableOnDaysRequested = 3,
        [Description("Other")]
        Other = 4

    }
}
