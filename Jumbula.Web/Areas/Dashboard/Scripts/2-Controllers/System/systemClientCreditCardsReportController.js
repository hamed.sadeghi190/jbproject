﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('systemClientCreditCardsReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'cfpLoadingBar', 'jbToast', '$timeout', function ($scope, $http, $log, $stateParams, $state, customReportService, cfpLoadingBar, jbToast, $timeout) {
        window.app.globalObjects.pageClass = 'seasons-view order'
        window.app.globalObjects.initializeView();

        $scope.baseUrl = window.location.origin + "/Dashboard/System";

        $scope.stateParams = $stateParams;
        var timer = false;
        $scope.MC.stateParams = $stateParams;
        $scope.Model = {};

        $scope.Model.SeasonId = 0;

        $http.get($scope.baseUrl + '/GetAllPartners')
            .success(function (data, status, headers, config) {

                $scope.AllPartners = data;
            });
        $http.get($scope.baseUrl + '/GetAllSchoolsAndProvider')
            .success(function (data, status, headers, config) {

                $scope.AllSchools = data;
            });
        $scope.updateChosenScope = function () {
            $("select[chosen]").trigger('chosen:updated');
        };

        $scope.$watch('AllPartners', function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.$watch('AllSchools', function () {
            if (timer) {
                $timeout.cancel(timer);
            }
            timer = $timeout(function () {
                $scope.updateChosenScope();
            }, 1500);
        });

        $scope.Model.Mode = 'NotPreferred';
        $scope.mainGridDataSource = new kendo.data.DataSource({
            dataType: "json",
            transport:
                {
                    read: {
                        url: $scope.baseUrl + '/GetClientCreditCards',
                        type: "POST",
                        data: function () {
                            return {
                                partnerId: $scope.Model.PartnerId,
                                clubId: $scope.Model.SchoolId,
                                gateway: $scope.Model.Gateway,
                                isDefault: $scope.Model.Default,
                                userName: $scope.Model.UserName,
                                mode: $scope.Model.Mode,
                                lastOrderMode: $scope.Model.LastOrderMode,
                                orderDate: $scope.Model.OrderDate,
                                installmentDate: $scope.Model.InstallmentDate,
                                installmentEndDate: $scope.Model.InstallmentEndDate,
                            };
                        },
                    },

                },
            schema: {
                total: "total",
                data: "data"
            },

            serverSorting: true,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
            requestStart: function (e) {
                cfpLoadingBar.start();
            },
            requestEnd: function (e) {
                cfpLoadingBar.complete();
            }
        });

        $scope.mainGridOptions = {
            excel: {
                allPages: true,
                fileName: "ClientCreditCards_Report.xlsx",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {
                cfpLoadingBar.complete();
            },
            dataSource: $scope.mainGridDataSource,
            resizable: true,
            pageable: {
                refresh: true,
                pageSizes:
                    [
                        5, 10, 20, 30, 50, 100, 200
                    ],
                buttonCount: 5,
            },
            dataBound: function () {
                var grid = $("#mainGrid").data("kendoGrid");
                customReportService.initializeDoubleScroll(grid);
            },
            //columnMenu: true,
            scrollable: true,
            sortable: true,
            columns: [
                {
                    field: "User",
                    title: "User",
                    width: "180px",
                    attributes: { 'style': "#if(HasError){#background-color: yellow;#}else if(HasNoCreditCardError){#background-color: red;color:White#} #" },
                },
                {
                    field: "ClubDomain",
                    title: "Domain",
                    width: "160px",
                    
                },
                {
                    field: "CustomerId",
                    title: "Customer ID",
                    width: "180px",
                },
                {
                    field: "IsDefault",
                    title: "Preferred",
                    width: "130px",
                },
                {
                    field: "userCardsCount",
                    title: "Number of cards",
                    width: "140px",
                },
                {
                    field: "LastOrderMode",
                    title: "Last order mode",
                    width: "190px",
                },
                {
                    field: "TypeCreditcard",
                    title: "Capture type",
                    width: "160px",
                }
                ,
                {
                    field: "CreateDate",
                    title: "Created",
                    width: "180px",
                }
                ,
                {
                    field: "UpdateDate",
                    title: "Updated",
                    width: "180px",
                }
                ,
                {
                    field: "DefaultDate",
                    title: "Defaulted",
                    width: "180px",
                },
                {
                    field: "Expiry",
                    title: "Expiration",
                    width: "130px",
                },
                {
                    field: "LastDigits",
                    title: "last 4 digits",
                    width: "120px",
                },

                {
                    field: "Name",
                    title: "Name on card",
                    width: "150px",
                },
                {
                    field: "Brand",
                    title: "Card brand",
                    width: "110px",
                },
                {
                    field: "CustomerId2",
                    title: "Customer ID 2",
                    width: "170px",
                },
                {
                    field: "IsDefault2",
                    title: "Preferred 2",
                    width: "130px",
                },
                {
                    field: "TypeCreditcard2",
                    title: "Capture type 2",
                    width: "170px",
                },
                {
                    field: "CreateDate2",
                    title: "Created 2",
                    width: "180px",
                }
                ,
                {
                    field: "UpdateDate2",
                    title: "Updated 2",
                    width: "180px",
                }
                ,
                {
                    field: "DefaultDate2",
                    title: "Defaulted 2",
                    width: "180px",
                },
                {
                    field: "Expiry2",
                    title: "Expiration 2",
                    width: "130px",
                },
                {
                    field: "LastDigits2",
                    title: "last 4 digits 2",
                    width: "130px",
                },
                {
                    field: "Name2",
                    title: "Name on card 2",
                    width: "150px",
                },
                {
                    field: "Brand2",
                    title: "Card brand 2",
                    width: "120px",
                },
                {
                    field: "CustomerId3",
                    title: "Customer ID 3",
                    width: "180px",
                },
                {
                    field: "IsDefault3",
                    title: "Preferred 3",
                    width: "130px",
                },
                {
                    field: "TypeCreditcard3",
                    title: "Capture type 3",
                    width: "170px",
                },
                {
                    field: "CreateDate3",
                    title: "Created 3",
                    width: "180px",
                }
                ,
                {
                    field: "UpdateDate3",
                    title: "Updated 3",
                    width: "180px",
                }
                ,
                {
                    field: "DefaultDate3",
                    title: "Defaulted 3",
                    width: "180px",
                },
                {
                    field: "Expiry3",
                    title: "Expiration 3",
                    width: "120px",
                },
                {
                    field: "LastDigits3",
                    title: "last 4 digits 3",
                    width: "130px",
                },

                {
                    field: "Name3",
                    title: "Name on card 3",
                    width: "150px",
                },
                {
                    field: "Brand3",
                    title: "Card brand 3",
                    width: "120px",
                }],
        }

        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#ClientCreditCards").getKendoGrid().saveAsExcel();
        }

        $scope.filterData = function () {

            $scope.mainGridOptions.dataSource.read();

        }

        $scope.clearFilters = function () {
                $scope.Model.PartnerId = "";
                $scope.Model.SchoolId = "";
                $scope.Model.Gateway = "";
                $scope.Model.Default = "";
                $scope.Model.UserName = "";
                $scope.Model.CountCard = "";
                $scope.Model.LastOrderMode = "";
                $scope.Model.OrderDate = "",
                $scope.Model.InstallmentDate = "",
                $scope.Model.InstallmentEndDate = "",

                $scope.mainGridOptions.dataSource.read();

        }
    }]);
})();