﻿using System;
using FluentAssertions;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Test.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Jumbula.Common.Enums;
using Jumbula.Core.Domain.Generic;
using Jumbula.Core.Model.Order;
using Jumbula.Core.Data.DbHelpers;

namespace Jumbula.Business.Tests.Unit
{
    [TestClass]
    public class OrderItemBusinessTest
    {
        #region Fields
        private Mock<IClubBusiness> _mockClubBusiness;
        private Mock<IPendingPreapprovalTransactionBusiness> _mockPendingPreapprovalTransactionBusiness;
        private Mock<IPlayerProfileBusiness> _mockPlayerProfileBusiness;
        private Mock<IProgramBusiness> _mockProgramBusiness;
        private Mock<ISubscriptionBusiness> _mockSubscriptionBusiness;
        private Mock<IUserProfileBusiness> _mockUserProfileBusiness;
        private Mock<ISeasonBusiness> _mockSeasonBusiness;
        private Mock<IOrderSessionBusiness> _mockOrderSessionBusiness;
        private Mock<IRepository<OrderItem, long>> _mockOrderItemRepository;
        private Mock<IEntitiesContext> _mockDataContext;
        private Mock<IOrderInstallmentBusiness> _mockOrderInstallmentBusiness;
        private Mock<IOrderBusiness> _mockOrderBusiness;
        private Mock<IProgramSessionBusiness> _mockProgramSessionBusiness;
        private Mock<IClubSettingBusiness> _mockClubSettingBusiness;
        private Mock<IFormBusiness> _mockFormBusiness;
        private Mock<IAccountingBusiness> _mockAccountingBusiness;
        private Mock<IJbDbFunctions> _mockJbDbFunctions;
        private Mock<ITransactionActivityBusiness> _mockTransactionActivityBusiness;
        private Mock<IPaymentBusiness> _mockPaymentBusiness;
        private Mock<IOrderHistoryBusiness> _mockOrderHistoryBusiness;
        #endregion

        #region Constants
        private const long Id = 10;
        private const long ProgramId = 1;
        private const long ProgramScheduleId = 1;
        private const int ClubId = 10;
        private readonly DateTime _equalToFamilyBalanceNoLimitDate = DateTime.Now.AddDays(-9);
        private readonly DateTime _lowerThanFamilyBalanceNoLimitDate = DateTime.Now.AddDays(-10);
        private readonly DateTime _greaterThanFamilyBalanceNoLimitDate = DateTime.Now.AddDays(-8);
        private readonly List<long> _ids = new List<long> { 10, 20, 30 };

        private const int UserId = 63;
        private const long OrderInstallmentId = 1;
        private const int AutoChargeFailAttemptNumber = 4;
        private readonly DateTime _installmentDayCurrentDate = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1);
        private readonly DateTime _installmentDayLowerThanNow = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1).AddDays(-1);
        private readonly DateTime _installmentDayGreaterThanNow = DateTime.UtcNow.Date.AddDays(1).AddSeconds(-1).AddDays(1);
        #endregion

        #region Test methods
        [TestMethod]
        public void GetReportOrderItems_CompleteOrderItems_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(CreateOrderItems().AsQueryable());

            // Act
            var result = service.GetProgramOrderItems(ProgramId);

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetReportOrderItems_CompleteOrderItems_ShouldReturnEmpty()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(CreateOrderItems().AsQueryable());

            // Act
            var result = service.GetProgramOrderItems(ProgramId + 100).ToList();

            // Assert
            result.Count.Should().Be(0);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GetReportOrderItems_TestSeason_ShouldReturnEmpty()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(CreateOrderItems().Where(c => c.ProgramSchedule != null && c.ProgramSchedule.Program.Season.Status == SeasonStatus.Test).AsQueryable());

            // Act
            var result = service.GetProgramOrderItems(ProgramId).ToList();

            // Assert
            result.Count.Should().Be(0);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GetReportOrderItems_TestOrder_ShouldReturnEmpty()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(CreateOrderItems().Where(c => !c.Order.IsLive).AsQueryable());

            // Act
            var result = service.GetProgramOrderItems(ProgramId).ToList();

            // Assert
            result.Count.Should().Be(0);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GetReportOrderItems_ScheduleIdIsNull_ShouldReturnEmpty()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(CreateOrderItems().Where(c => c.ProgramScheduleId == null).AsQueryable());

            // Act
            var result = service.GetProgramOrderItems(ProgramId).ToList();

            // Assert
            result.Count.Should().Be(0);
            result.Should().BeEmpty();
        }

        [TestMethod]
        public void GetReportOrderItems_ShowAllStatusCategory_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(CreateOrderItems().AsQueryable());

            // Act
            var result = service.GetProgramOrderItems(ProgramId, OrderItemStatusCategories.showAll);

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void GetUserPaymentItems_HasOrdersWithPaymentPlanInstallmentAndInstallmentsHasAnyItemButOrderItemHasNotAnyItem_ShouldReturnValidInstallmentModel()
        {
            //Arrange
            var service = CreateService();
            var installments = CreateOrderInstallments();
            var orderItems = new List<OrderItem>();

            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable);
            _mockOrderInstallmentBusiness.Setup(x => x.GetCalendarOrderItemInstallments(It.IsAny<IEnumerable<OrderItem>>())).Returns(installments.AsQueryable);
            _mockOrderInstallmentBusiness.Setup(x => x.CheckIsPayable(It.IsAny<OrderInstallment>())).Returns(true);

            _mockOrderInstallmentBusiness.Setup(x => x.Get(installments.First().Id)).Returns(installments.First());

            //Act
            var result = service.GetUserPaymentItems(UserId, ClubId).ToList();

            //Assert
            result.First().IsAutoCharge.Should().Be(!installments.First().EnableReminder);
            result.First().IsManually.Should().Be(installments.First().EnableReminder);
            result.First().Title.Should().Be(Common.Constants.Payment.ParentPaymentSchedulerTitle);
            result.First().PaymentType.Should().Be(PaymentPlanType.Installment.ToString());
            result.First().IsPayable.Should().BeTrue();
            result.First().IsPresentable.Should().BeTrue();
        }

        [TestMethod]
        public void GetUserPaymentItems_HasOrdersWithPaymentPlanFullPaidAndOrderItemsHasAnyItemButInstallmentHasNotAnyItem_ShouldReturnValidFullPaidModel()
        {
            //Arrange
            var service = CreateService();
            var FullPaidOrderItems = CreateFullPaidOrderItems();
            var orders = CreateOrders();
            var installments = new List<OrderInstallment>();
            var cancelInstallments = new List<OrderInstallment>();
            var classSession = new ClassSessionsDateTime
            {
                StartTime = DateTime.UtcNow.AddDays(2),
                EndTime = DateTime.UtcNow.AddDays(2),
                SessionDate = DateTime.UtcNow.AddDays(2),
            };

            _mockProgramSessionBusiness.Setup(x => x.GetFirstSession(It.IsAny<OrderItem>(), true)).Returns(classSession);
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FullPaidOrderItems.AsQueryable);
            _mockOrderInstallmentBusiness.Setup(x => x.GetCalendarOrderItemInstallments(FullPaidOrderItems.AsQueryable())).Returns(installments.AsQueryable);


            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FullPaidOrderItems.AsQueryable());

            //Act
            var result = service.GetUserPaymentItems(UserId, ClubId).ToList();

            //Assert
            result.First().Start.Should().Be(classSession.StartTime);
            result.First().IsAutoCharge.Should().BeFalse();
            result.First().IsManually.Should().BeFalse();
            result.First().Title.Should().BeSameAs(Common.Constants.Payment.ParentPaymentSchedulerTitle);
            result.First().PaymentType.Should().Be(PaymentPlanType.FullPaid.ToString());
            result.First().IsPresentable.Should().BeTrue();
            result.First().IsPayable.Should().BeTrue();
        }

        [TestMethod]
        public void AddItemToCart_PaymentTypeIsInstallmentAndInstallmentIsNotNullAndAmountPayTypeIsFullPay_ShouldbeTrueResultUpdate()
        {
            //Arrange
            var service = CreateService();
            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.OrderItem = FactoryGirl.Build<OrderItem>();
                p.OrderItem.Order = FactoryGirl.Build<Order>();
                p.OrderItem.Order.User = FactoryGirl.Build<JbUser>();
                p.OrderItem.Order.Club = FactoryGirl.Build<Club>();
                p.OrderItem.Order.User.UserName = "maryam@jumbula.com";
                p.OrderItem.Order.Club.Currency = CurrencyCodes.USD;
                p.TransactionActivities = null;
            });

            var model = new ConfirmPaymentAmountViewModel()
            {
                PaymentType = PaymentPlanType.Installment,
                Id = 1,
                Balance = 200,
                Token = "",
                PayableAmount = 100,
                AmountPayType = AmountPayType.FullPay

            };

            var updateResult = new OperationStatus()
            {
                Status = true
            };

            _mockOrderInstallmentBusiness.Setup(x => x.Get(model.Id)).Returns(installment);
            _mockOrderInstallmentBusiness.Setup(x => x.Update(installment)).Returns(updateResult);

            _mockOrderBusiness.Setup(x => x.SetPaymentTransaction(null, It.IsAny<PaymentDetail>(), TransactionStatus.Draft, installment, null, It.IsAny<decimal>(), null, installment.Id, string.Empty, "test", string.Empty, TransactionCategory.TakePayment, HandleMode.Online));
            //Act

            var result = service.AddItemToCart(model);

            //Assert
            result.Should().BeTrue();
            _mockOrderInstallmentBusiness.Verify(h => h.Update(installment), Times.Once);
        }


        [TestMethod]
        public void AddItemToCart_PaymentTypeIsInstallmentAndInstallmentIsNull_ShouldReturnFalse()
        {
            //Arrange
            var service = CreateService();

            var model = new ConfirmPaymentAmountViewModel()
            {
                PaymentType = PaymentPlanType.Installment,
                Id = 1,
                Balance = 200,
                Token = "",
                PayableAmount = 100,
                AmountPayType = AmountPayType.FullPay
            };

            _mockOrderInstallmentBusiness.Setup(x => x.Get(model.Id)).Returns<OrderInstallment>(null);

            //Act
            var result = service.AddItemToCart(model);

            //Assert
            result.Should().BeFalse();

        }

        [TestMethod]
        public void AddItemToCart_PaymentTypeIsFullPaidAndOrderItemIsNotNullAndAmountPayTypeIsFullPay_ShouldbeTrueResultUpdate()
        {
            //Arrange
            var service = CreateService();
            var orderItems = new List<OrderItem>();
            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 1;
                p.Order = FactoryGirl.Build<Order>();
                p.Order.User = FactoryGirl.Build<JbUser>();
                p.Order.Club = FactoryGirl.Build<Club>();
                p.Order.User.UserName = "maryam@jumbula.com";
                p.Order.Club.Currency = CurrencyCodes.USD;
                p.TransactionActivities = null;
            });

            var model = new ConfirmPaymentAmountViewModel()
            {
                PaymentType = PaymentPlanType.FullPaid,
                Id = 1,
                Balance = 200,
                Token = "",
                PayableAmount = 100,
                AmountPayType = AmountPayType.FullPay
            };

            var updateResult = new OperationStatus()
            {
                Status = true
            };

            orderItems.Add(orderItem);

            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable());
            _mockOrderItemRepository.Setup(x => x.Update(orderItem)).Returns(updateResult);
            _mockOrderBusiness.Setup(x => x.SetPaymentTransaction(null, It.IsAny<PaymentDetail>(), TransactionStatus.Draft, null, null, It.IsAny<decimal>(), orderItem, null, string.Empty, "test", string.Empty, TransactionCategory.TakePayment, HandleMode.Online));

            //Act
            var result = service.AddItemToCart(model);

            //Assert
            result.Should().BeTrue();
            _mockOrderItemRepository.Verify(h => h.Update(orderItem), Times.Once);
        }

        [TestMethod]
        public void AddItemToCart_PaymentTypeIsFullPaidAndOrderItemIsNull_ShouldReturnFalse()
        {
            //Arrange
            var service = CreateService();

            var model = new ConfirmPaymentAmountViewModel()
            {
                PaymentType = PaymentPlanType.FullPaid,
                Id = 1,
                Balance = 200,
                Token = "",
                PayableAmount = 100,
                AmountPayType = AmountPayType.FullPay
            };

            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(new List<OrderItem>().AsQueryable());

            //Act
            var result = service.AddItemToCart(model);

            //Assert
            result.Should().BeFalse();

        }

        [TestMethod]
        public void CheckPayableAmountValidation_PayableAmountMoreThanTotalAmount_ShouldReturnFalseWithCorrectErrorMessage()
        {
            //Arrange
            var service = CreateService();
            var payableAmount = 500;
            var totalAmount = 200;

            //Act
            var result = service.CheckPayableAmountValidation(payableAmount, totalAmount);

            //Assert
            result.Item1.Should().BeFalse();
            result.Item2.Should().BeSameAs(Common.Constants.Payment.PaymentAmountValidationMoreThanBalance);
        }

        [TestMethod]
        public void CheckPayableAmountValidation_PayableAmountEqualZero_ShouldReturnFalseWithCorrectErrorMessage()
        {
            //Arrange
            var service = CreateService();
            var payableAmount = 0;
            var totalAmount = 200;

            //Act
            var result = service.CheckPayableAmountValidation(payableAmount, totalAmount);

            //Assert
            result.Item1.Should().BeFalse();
            result.Item2.Should().BeSameAs(Common.Constants.Payment.PaymentAmountValidationLessThanZero);
        }

        [TestMethod]
        public void CheckPayableAmountValidation_PayableAmountLessThanZero_ShouldReturnFalseWithCorrectErrorMessage()
        {
            //Arrange
            var service = CreateService();
            var payableAmount = -100;
            var totalAmount = 200;

            //Act
            var result = service.CheckPayableAmountValidation(payableAmount, totalAmount);

            //Assert
            result.Item1.Should().BeFalse();
            result.Item2.Should().BeSameAs(Common.Constants.Payment.PaymentAmountValidationLessThanZero);
        }

        [TestMethod]
        public void ConfirmPaymentAmount_ItemIsInstallmentAndProgramTypeIsElseBeforeAfter_ShouldReturnValidModelData()
        {
            //Arrange
            var service = CreateService();
            var id = 1;
            var paymentType = PaymentPlanType.Installment;
            var returnUrl = "/Registrant/Payment";

            var installment = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = 1;
                p.OrderItem = FactoryGirl.Build<OrderItem>();
                p.OrderItem.Order = FactoryGirl.Build<Order>();
                p.OrderItem.Player = FactoryGirl.Build<PlayerProfile>();
                p.OrderItem.Player.Contact = FactoryGirl.Build<ContactPerson>();
                p.OrderItem.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.OrderItem.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.InstallmentDate = DateTime.Now;
                p.EnableReminder = false;
                p.OrderItem.ProgramTypeCategory = ProgramTypeCategory.Subscription;
                p.OrderItem.Player.Contact.FirstName = "Maryam";
                p.OrderItem.Player.Contact.LastName = "Saeedi";
                p.OrderItem.EntryFeeName = "Name Fee";
                p.OrderItem.ProgramSchedule.Program.Name = "Art Program";
                p.OrderItem.TransactionActivities = null;
                p.Amount = 200;
                p.OrderItem.Start = DateTime.Now;
                p.OrderItem.End = DateTime.Now;
                p.PaidAmount = 100;
            });

            _mockOrderInstallmentBusiness.Setup(x => x.Get(id)).Returns(installment);
            _mockProgramBusiness.Setup(x => x.GetProgramName(installment.OrderItem.ProgramSchedule.Program.Name, installment.OrderItem.Start.Value, installment.OrderItem.End.Value, installment.OrderItem.EntryFeeName)).Returns("Return Name");

            //Act
            var result = service.ConfirmPaymentAmount(id, paymentType, returnUrl);

            //Assert
            result.Id.Should().Be(installment.Id);
            result.PayableAmount.Should().Be(installment.Balance);
            result.Balance.Should().Be(installment.Balance);
            result.DueDate.Should().Be(installment.InstallmentDate);
            result.FullName.Should().Be($"{installment.OrderItem.Player.Contact.FirstName} {installment.OrderItem.Player.Contact.LastName}");
            result.ProgramName.Should().NotBeNullOrEmpty();
            result.AmountPayType.Should().Be(AmountPayType.FullPay);
            result.PaymentType.Should().Be(paymentType);
            result.IsAutoChargeNotPastDue.Should().BeTrue();
            result.ReturnUrl.Should().Be(returnUrl);
        }

        [TestMethod]
        public void ConfirmPaymentAmount_ItemIsFullPaidAndProgramTypeIsBeforeAfter_ShouldReturnValidModelData()
        {
            //Arrange
            var service = CreateService();
            var id = 1;
            var paymentType = PaymentPlanType.FullPaid;
            var returnUrl = "/Registrant/Payment";
            var date = new ClassSessionsDateTime() { StartTime = DateTime.UtcNow, EndTime = DateTime.UtcNow };
            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 1;
                p.Player = FactoryGirl.Build<PlayerProfile>();
                p.Player.Contact = FactoryGirl.Build<ContactPerson>();
                p.ProgramSchedule = FactoryGirl.Build<ProgramSchedule>();
                p.ProgramSchedule.Program = FactoryGirl.Build<Program>();
                p.ProgramTypeCategory = ProgramTypeCategory.BeforeAfterCare;
                p.Player.Contact.FirstName = "Maryam";
                p.Player.Contact.LastName = "Saeedi";
                p.EntryFeeName = "Name Fee";
                p.ProgramSchedule.Program.Name = "Art Program";
                p.TransactionActivities = null;
                p.TotalAmount = 200;
                p.Start = DateTime.Now;
                p.End = DateTime.Now;
                p.PaidAmount = 100;
            });

            var orderItems = new List<OrderItem> { orderItem };

            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable);
            _mockProgramSessionBusiness.Setup(x => x.GetFirstSession(orderItem, true)).Returns(date);
            _mockProgramBusiness.Setup(x => x.GetProgramNameWithScheduleTitleAndDate(orderItem, orderItem.EntryFeeName, "")).Returns("Return Name");


            //Act
            var result = service.ConfirmPaymentAmount(id, paymentType, returnUrl);

            //Assert
            result.Id.Should().Be(orderItem.Id);
            result.PayableAmount.Should().Be(100);
            result.Balance.Should().Be(100);
            result.DueDate.Should().Be(date.StartTime);
            result.FullName.Should().Be($"{orderItem.Player.Contact.FirstName} {orderItem.Player.Contact.LastName}");
            result.ProgramName.Should().NotBeNullOrEmpty();
            result.AmountPayType.Should().Be(AmountPayType.FullPay);
            result.PaymentType.Should().Be(paymentType);
            result.IsAutoChargeNotPastDue.Should().BeFalse();
            result.ReturnUrl.Should().Be(returnUrl);
            result.CancellationFee.Should().Be(0);
        }

        [TestMethod]
        public void ConfirmPaymentAmount_ItemInstallmentIsNull_ShouldReturnEmptyModelData()
        {
            //Arrange
            var service = CreateService();
            var id = 1;
            var paymentType = PaymentPlanType.Installment;
            var returnUrl = "/Registrant/Payment";

            _mockOrderInstallmentBusiness.Setup(x => x.Get(id)).Returns<OrderInstallment>(null);

            //Act
            var result = service.ConfirmPaymentAmount(id, paymentType, returnUrl);

            //Assert
            result.Id.Should().Be(0);
            result.PayableAmount.Should().Be(0);
            result.Balance.Should().Be(0);
            result.FullName.Should().BeNullOrEmpty();
            result.ProgramName.Should().BeNullOrEmpty();
            result.AmountPayType.Should().Be(AmountPayType.FullPay);
            result.PaymentType.Should().Be(PaymentPlanType.FullPaid);
            result.IsAutoChargeNotPastDue.Should().BeFalse();
            result.ReturnUrl.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void ConfirmPaymentAmount_ItemOrderItemIsNull_ShouldReturnEmptyModelData()
        {
            //Arrange
            var service = CreateService();
            var id = 1;
            var paymentType = PaymentPlanType.FullPaid;
            var returnUrl = "/Registrant/Payment";
            var orderItem = FactoryGirl.Build<OrderItem>(p => { p.Id = 3; });

            var orderItems = new List<OrderItem> { orderItem };

            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable);

            //Act
            var result = service.ConfirmPaymentAmount(id, paymentType, returnUrl);

            //Assert
            result.Id.Should().Be(0);
            result.PayableAmount.Should().Be(0);
            result.Balance.Should().Be(0);
            result.FullName.Should().BeNullOrEmpty();
            result.ProgramName.Should().BeNullOrEmpty();
            result.AmountPayType.Should().Be(AmountPayType.FullPay);
            result.PaymentType.Should().Be(PaymentPlanType.FullPaid);
            result.IsAutoChargeNotPastDue.Should().BeFalse();
            result.ReturnUrl.Should().BeNullOrEmpty();
        }

        [TestMethod]
        public void GetParentPayments_IdHasValueAndPaymentTypeHasValueAndPaymentTypeIsInstallment_ReturnInstallmentDateForShowDefaultCalendarDate()
        {
            //Arrange
            var service = CreateService();
            var id = 1;
            var paymentType = PaymentPlanType.Installment;
            var userId = 63290;
            var installment = FactoryGirl.Build<OrderInstallment>(p => { p.Id = 1; p.InstallmentDate = DateTime.UtcNow.AddDays(1); });

            _mockOrderInstallmentBusiness.Setup(x => x.Get(id)).Returns(installment);

            //Act
            var result = service.GetParentPayments(id, paymentType, null, userId);

            //Assert
            result.Start.Should().Be(installment.InstallmentDate);
            result.UserId.Should().Be(userId);
        }

        [TestMethod]
        public void GetParentPayments_IdHasNotValue_ReturnDateTimeUtcNow()
        {
            //Arrange
            var service = CreateService();
            var paymentType = PaymentPlanType.Installment;
            var userId = 63290;

            //Act
            var result = service.GetParentPayments(null, paymentType, null, userId);

            //Assert
            result.Start.Should().Be(DateTime.UtcNow);
            result.UserId.Should().Be(userId);
        }

        [TestMethod]
        public void GetParentPayments_IdHasNotValuePaymentTypeHasNotValue_ReturnDateTimeUtcNow()
        {
            //Arrange
            var service = CreateService();
            var userId = 63290;

            //Act
            var result = service.GetParentPayments(null, null, null, userId);

            //Assert
            result.UserId.Should().Be(userId);
        }

        [TestMethod]
        public void GetParentPayments_IdHasValueAndPaymentTypeHasValueAndPaymentTypeIsInstallmentButInstallmentIsNull_ReturnDateTimeUtcNow()
        {
            //Arrange
            var service = CreateService();
            var id = 1;
            var paymentType = PaymentPlanType.Installment;
            var userId = 63290;

            _mockOrderInstallmentBusiness.Setup(x => x.Get(id)).Returns<OrderInstallment>(null);

            //Act
            var result = service.GetParentPayments(id, paymentType, null, userId);

            //Assert
            result.Start.Should().Be(DateTime.UtcNow);
            result.UserId.Should().Be(userId);
        }

        [TestMethod]
        public void GetParentPayments_IdHasValueAndPaymentTypeHasValueAndPaymentTypeIsFullPaid_ReturnOrderItemDateForShowDefaultCalendarDate()
        {
            //Arrange
            var service = CreateService();
            var id = 1;
            var paymentType = PaymentPlanType.Installment;
            var userId = 63290;
            var firstSessionDate = new ClassSessionsDateTime() { StartTime = DateTime.UtcNow, EndTime = DateTime.UtcNow };
            var orderItem = FactoryGirl.Build<OrderItem>(p => { p.Id = 1; });
            var orderItems = new List<OrderItem> { orderItem };

            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable);
            _mockProgramSessionBusiness.Setup(x => x.GetFirstSession(orderItem, true)).Returns(firstSessionDate);

            //Act
            var result = service.GetParentPayments(id, paymentType, null, userId);

            //Assert
            result.UserId.Should().Be(userId);
        }

        [TestMethod]
        public void GetParentPayments_IdHasValueAndPaymentTypeHasValueAndPaymentTypeIsFullPaidButOrderItemIsNull_ReturnDateTimeUtcNow()
        {
            //Arrange
            var service = CreateService();
            var id = 1;
            var paymentType = PaymentPlanType.Installment;
            var userId = 63290;
            var orderItem = FactoryGirl.Build<OrderItem>(p => { p.Id = 3; });
            var orderItems = new List<OrderItem> { orderItem };

            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable);

            //Act
            var result = service.GetParentPayments(id, paymentType, null, userId);

            //Assert
            result.Start.Should().Be(DateTime.UtcNow);
            result.UserId.Should().Be(userId);
        }


        [TestMethod]
        public void GetPayableBalance_OrderItemIsComplateTotalAmountMinusPaidAmountIsMoreThanZero_ShouldReturnBalanceWithTotalAmountMinusPaidAmount()
        {
            //Arrange
            var service = CreateService();

            var orderItem = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatus = OrderItemStatusCategories.completed;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.TotalAmount = 250;
                o.PaidAmount = 100;
            });

            //Act
            var result = service.GetPayableBalance(orderItem);

            //Assert
            result.Should().Be(150);
        }

        [TestMethod]
        public void GetPayableBalance_OrderItemIsComplateTotalAmountMinusPaidAmountIsLessThanZero_ShouldReturnZero()
        {
            //Arrange
            var service = CreateService();

            var orderItem = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatus = OrderItemStatusCategories.completed;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.TotalAmount = 250;
                o.PaidAmount = 500;
            });

            //Act
            var result = service.GetPayableBalance(orderItem);

            //Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GetPayableBalance_OrderItemIsCancelAndNotSubscriptionAndBeforeAfterWithCancelationFeeAndPaidAmountIsMoreThanCancellationFee_ShouldReturnZero()
        {
            //Arrange
            var service = CreateService();

            var orderItem = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatusReason = OrderItemStatusReasons.canceled;
                o.ItemStatus = OrderItemStatusCategories.changed;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.TotalAmount = 250;
                o.PaidAmount = 250;
                o.OrderChargeDiscounts.Add(FactoryGirl.Build<OrderChargeDiscount>(c => { c.Subcategory = ChargeDiscountSubcategory.Charge; c.Category = ChargeDiscountCategory.CancellationFee; c.Name = "Cancellation fee"; c.Amount = 50; }));
            });

            //Act
            var result = service.GetPayableBalance(orderItem);

            //Assert
            result.Should().Be(0);

        }

        [TestMethod]
        public void GetPayableBalance_OrderItemIsCancelAndIsSubscriptionOrBeforeAfter_ShouldReturnBalanceWithTotalAmountMinusPaidAmount()
        {
            //Arrange
            var service = CreateService();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatusReason = OrderItemStatusReasons.canceled;
                o.ItemStatus = OrderItemStatusCategories.changed;
                o.ProgramTypeCategory = ProgramTypeCategory.BeforeAfterCare;
                o.TotalAmount = 250;
                o.PaidAmount = 100;
            });

            //Act
            var result = service.GetPayableBalance(orderItem1);

            //Assert
            result.Should().Be(150);
        }

        [TestMethod]
        public void GetPayableBalance_OrderItemIsCancelAndSubscriptionOrBeforeAfterTotalAmountMinusPaidAmountIsLessThanZero_ShouldReturnBalanceZero()
        {
            //Arrange
            var service = CreateService();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatusReason = OrderItemStatusReasons.canceled;
                o.ItemStatus = OrderItemStatusCategories.changed;
                o.ProgramTypeCategory = ProgramTypeCategory.Subscription;
                o.TotalAmount = 250;
                o.PaidAmount = 300;
            });

            //Act
            var result = service.GetPayableBalance(orderItem1);

            //Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GetPayableBalance_OrderItemIsCancelAndNotSubscriptionAndBeforeAfterAndPaidAmountIsLessThanCancellationFee_ShouldReturnBalanceWithCancellationFeeMinusPaidAmount()
        {
            //Arrange
            var service = CreateService();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatusReason = OrderItemStatusReasons.canceled;
                o.ItemStatus = OrderItemStatusCategories.changed;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.TotalAmount = 250;
                o.PaidAmount = 20;
                o.OrderChargeDiscounts.Add(FactoryGirl.Build<OrderChargeDiscount>(c => { c.Subcategory = ChargeDiscountSubcategory.Charge; c.Category = ChargeDiscountCategory.CancellationFee; c.Name = "Cancellation fee"; c.Amount = 50; }));
            });

            //Act
            var result = service.GetPayableBalance(orderItem1);

            //Assert

            // exp: 50-20 = 230
            result.Should().Be(230);
        }

        [TestMethod]
        public void GetPayableBalance_OrderItemIsCancelAndSubscriptionOrBeforeAfterTotalAmountMinusPaidAmountIsEqualZero_ShouldReturnBalanceZero()
        {
            //Arrange
            var service = CreateService();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatusReason = OrderItemStatusReasons.canceled;
                o.ItemStatus = OrderItemStatusCategories.changed;
                o.ProgramTypeCategory = ProgramTypeCategory.Subscription;
                o.TotalAmount = 300;
                o.PaidAmount = 300;
            });

            //Act
            var result = service.GetPayableBalance(orderItem1);

            //Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GetLastCancellationCharge_OrderItemHasOneCancellationCharge_ShouldReturnCancellationCharge()
        {
            //Arrange
            var service = CreateService();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatusReason = OrderItemStatusReasons.canceled;
                o.ItemStatus = OrderItemStatusCategories.changed;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.TotalAmount = 250;
                o.PaidAmount = 20;
                o.OrderChargeDiscounts.Add(FactoryGirl.Build<OrderChargeDiscount>(c => { c.Subcategory = ChargeDiscountSubcategory.Charge; c.Category = ChargeDiscountCategory.CancellationFee; c.Name = "Cancellation fee"; c.Amount = 50; }));
            });

            //Act
            var result = service.GetLastCancellationCharge(orderItem1);

            //Assert
            result.Amount.Should().Be(50);
        }

        [TestMethod]
        public void GetLastCancellationCharge_OrderItemHasTwoCancellationCharge_ShouldReturnLastCancellationCharge()
        {
            //Arrange
            var service = CreateService();

            var orderItem1 = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatusReason = OrderItemStatusReasons.canceled;
                o.ItemStatus = OrderItemStatusCategories.changed;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.TotalAmount = 250;
                o.PaidAmount = 20;
                o.OrderChargeDiscounts.Add(FactoryGirl.Build<OrderChargeDiscount>(c => { c.Subcategory = ChargeDiscountSubcategory.Charge; c.Category = ChargeDiscountCategory.CancellationFee; c.Name = "Cancellation fee"; c.Amount = 50; }));
                o.OrderChargeDiscounts.Add(FactoryGirl.Build<OrderChargeDiscount>(c => { c.Subcategory = ChargeDiscountSubcategory.Charge; c.Category = ChargeDiscountCategory.CancellationFee; c.Name = "Cancellation fee"; c.Amount = 20; }));
            });

            //Act
            var result = service.GetLastCancellationCharge(orderItem1);

            //Assert
            result.Amount.Should().Be(20);
        }

        [TestMethod]
        public void GetLastCancellationCharge_OrderItemNotAnyCancellationCharge_ShouldReturnNull()
        {
            //Arrange
            var service = CreateService();

            var orderItem = FactoryGirl.Build<OrderItem>(o =>
            {
                o.Id = 1;
                o.ItemStatusReason = OrderItemStatusReasons.canceled;
                o.ItemStatus = OrderItemStatusCategories.changed;
                o.ProgramTypeCategory = ProgramTypeCategory.Class;
                o.TotalAmount = 250;
                o.PaidAmount = 20;
            });

            //Act
            var result = service.GetLastCancellationCharge(orderItem);

            //Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_PartnerIdIsNull_ShouldBeHaveOneResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemWithoutPartner().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_PartnerIdEqualSpeciallyClubId_ShouldBeHaveTwoResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemWithPartner().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_PaymentPlanTypeNotFullPaid_ShouldCountBeZero()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemPaymentPlanTypeNotFullPaid().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_PaymentPlanTypeIsFullPaid_ShouldBeHaveOneResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemPaymentPlanTypeIsFullPaid().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(1);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_CompleteDateEqualToFamilyBalanceNoLimitDate_ShouldBeHaveTwoResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemCompleteDateEqualToFamilyBalanceNoLimitDate().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_CompleteDateGreaterThanFamilyBalanceNoLimitDate_ShouldCountBeZero()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemCompleteDateGreaterThanFamilyBalanceNoLimitDate().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_CompleteDateLowerThanFamilyBalanceNoLimitDate_ShouldBeHaveTwoResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemCompleteDateLowerThanFamilyBalanceNoLimitDate().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_TotalAmountEqualToPaidAmount_ShouldCountBeZero()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemTotalAmountEqualToPaidAmount().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicyWithZeroFamilyBalanceMinPrice());

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_TotalAmountGreaterThanPaidAmount_ShouldBeHaveTwoResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemTotalAmountGreaterThanPaidAmount().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicyWithZeroFamilyBalanceMinPrice());

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_TotalAmountLowerThanPaidAmount_ShouldCountBeZero()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemTotalAmountLowerThanPaidAmount().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicyWithZeroFamilyBalanceMinPrice());

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_DifferenceAmountEqualToFamilyBalanceMinPrice_ShouldBeHaveTwoResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemDifferenceAmountEqualToFamilyBalanceMinPrice().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_ProgramScheduleIdIsNull_ShouldBeHaveZeroResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemProgramScheduleIdIsNull().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_IsLiveFalse_ShouldBeHaveZeroResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemIsLiveFalse().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_DifferenceAmountGreaterThanFamilyBalanceMinPrice_ShouldBeHaveTwoResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemDifferenceAmountGreaterThanFamilyBalanceMinPrice().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(2);
        }

        [TestMethod]
        public void GetDelinquentFullPaidItems_DifferenceAmountLowerThanFamilyBalanceMinPrice_ShouldCountBeZero()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemDifferenceAmountLowerThanFamilyBalanceMinPrice().AsQueryable());

            // Act
            var result = service.GetDelinquentFullPaidItems(ClubId, FillDelinquentPolicy());

            // Assert
            result.Count().Should().Be(0);
        }

        [TestMethod]
        public void GetList_DoNotShowDeleted_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItemWithDifferencesItemStatus().AsQueryable());

            // Act
            var result = service.GetList();

            // Assert
            result.Count(x => x.ItemStatus == OrderItemStatusCategories.deleted).Should().Be(0);
        }

        [TestMethod]
        public void GetOrderItems_GetAllIds_ShouldReturnValidResult()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillOrderItems().AsQueryable());

            // Act
            var result = service.GetOrderItems(_ids);

            // Assert
            result.Count().Should().Be(_ids.Count);
        }

        [TestMethod]
        public void GetDelinquentOrderInstallmentItems_AutoChargeFailAttemptNumberIsZero_ShouldReturnNull()
        {
            // Arrange
            var service = CreateService();
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(FillInstallmentOrderItemWithoutPartner().AsQueryable());

            // Act
            var result = service.GetDelinquentOrderInstallmentItems(ClubId, new DelinquentPolicy { AutoChargeFailAttemptNumber = 0 });

            // Assert
            result.Should().BeNull();
        }

        [TestMethod]
        public void GetDelinquentOrderInstallmentItems_AutoChargeFailAttemptNumberIsNotZero_ShouldReturnValid()
        {
            // Arrange
            var service = CreateService();
            var delinquentPolicy = FillDelinquentPolicy();

            _mockOrderInstallmentBusiness.Setup(x => x.GetDelinquentInstallmentItemsAsNoTracking(ClubId, delinquentPolicy.AutoChargeFailAttemptNumber)).Returns(FillOrderItemsInstallmentExists().AsQueryable());

            // Act
            var result = service.GetDelinquentOrderInstallmentItems(ClubId, delinquentPolicy);

            // Assert
            result.Count().Should().Be(3);
        }

        [TestMethod]
        public void Refund_RefundAmountMoreThanZeroFullRefundByAddToCredit_AddRefundAmountToLiveFamilyCreditSaveTransactionSuccess()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 0m;
            var updateResult = new OperationStatus {Status = true};

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = true;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var orderItems = new List<OrderItem> { orderItem };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 20,
                Mode = RefundMode.AddToCredit,
                IsSelected = true,
                Title = "Transfer the balance to the family credit"
            };

            var modes = new List<RefundInformation> { mode1 };

            var mode = new OrderItemsViewModel
            {
                Id = 3,
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            var credit = orderItem.Order.Club.Users.SingleOrDefault(p => p.UserId == orderItem.Order.UserId).Credit;
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable());
            _mockOrderItemRepository.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            //_mockOrderItemRepository.Setup(x => x.SetRefundTransaction(installment.OrderItem, null, It.IsAny<PaymentDetail>(), model.TransactionStatus, model.RefundAmount, It.IsAny<DateTime>(), model.RefundNote, HandleMode.Online)).Returns(transaction);

            //Act
            var result = service.Refund(mode, ref refundedAmount);

            //Assert
            result.Status.Should().Be(true);

            orderItem.Order.Club.Users.SingleOrDefault(p => p.UserId == orderItem.Order.UserId).Credit.Should().Be(credit + refundedAmount);
            orderItem.PaidAmount.Should().Be(0);

            orderItem.TransactionActivities.Count(t => t.Amount == refundedAmount
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Credit
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == mode.CurrentUserName).Should().Be(1);
        }

        [TestMethod]
        public void Refund_RefundAmountMoreThanZeroFullRefundByAddToTestCredit_AddRefundAmountToTestFamilyCreditSaveTransactionSuccess()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 0m;
            var updateResult = new OperationStatus();
            updateResult.Status = true;

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;

            });
            var orderItems = new List<OrderItem> { orderItem };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 20,
                Mode = RefundMode.AddToCredit,
                IsSelected = true,
                Title = "Transfer the balance to the family credit"
            };

            var modes = new List<RefundInformation> { mode1 };

            var mode = new OrderItemsViewModel()
            {
                Id = 3,
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            _mockOrderItemRepository.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);

            var testCredit = orderItem.Order.Club.Users.SingleOrDefault(p => p.UserId == orderItem.Order.UserId).TestCredit;
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable());

            //Act
            var result = service.Refund(mode, ref refundedAmount);

            //Assert
            result.Status.Should().Be(true);

            orderItem.Order.Club.Users.SingleOrDefault(p => p.UserId == orderItem.Order.UserId).TestCredit.Should().Be(testCredit + refundedAmount);
            orderItem.PaidAmount.Should().Be(0);

            orderItem.TransactionActivities.Count(t => t.Amount == refundedAmount
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Credit
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == mode.CurrentUserName).Should().Be(1);
        }

        [TestMethod]
        public void Refund_RefundAmountMoreThanZeroPartiallyRefundByAddToFamilyCredit_SaveTransactionSuccess()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 0m;
            var updateResult = new OperationStatus();
            updateResult.Status = true;

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = true;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });
            var orderItems = new List<OrderItem> { orderItem };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.AddToCredit,
                IsSelected = true,
                Title = "Transfer the balance to the family credit"
            };

            var modes = new List<RefundInformation> { mode1 };

            var mode = new OrderItemsViewModel()
            {
                Id = 3,
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            var orderItemPaidAmount = orderItem.PaidAmount;

            _mockOrderItemRepository.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable());

            //Act
            var result = service.Refund(mode, ref refundedAmount);

            //Assert
            result.Status.Should().Be(true);

            orderItem.PaidAmount.Should().Be(orderItemPaidAmount - refundedAmount);

            orderItem.TransactionActivities.Count(t => t.Amount == refundedAmount
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Credit
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == mode.CurrentUserName).Should().Be(1);
        }

        [TestMethod]
        public void Refund_RefundAmountMoreThanZeroFullRefundByManually_SaveTransactionSuccessAndPaymentMethodCash()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 0m;
            var updateResult = new OperationStatus();
            updateResult.Status = true;

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = true;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });
            var orderItems = new List<OrderItem> { orderItem };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 20,
                Mode = RefundMode.Manually,
                IsSelected = true,
                Title = "My organization will issue the refund manually"
            };

            var mode2 = new RefundInformation()
            {
                Priority = 1,
                Amount = 20,
                Mode = RefundMode.AddToCredit,
                IsSelected = false,
                Title = "Transfer the balance to the family credit"
            };

            var modes = new List<RefundInformation> { mode1, mode2 };

            var mode = new OrderItemsViewModel()
            {
                Id = 3,
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            var orderItemPaidAmount = orderItem.PaidAmount;

            _mockOrderItemRepository.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable());

            //Act
            var result = service.Refund(mode, ref refundedAmount);

            //Assert
            result.Status.Should().Be(true);

            orderItem.PaidAmount.Should().Be(orderItemPaidAmount - refundedAmount);

            orderItem.TransactionActivities.Count(t => t.Amount == refundedAmount
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Cash
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == mode.CurrentUserName).Should().Be(1);
        }

        [TestMethod]
        public void Refund_RefundAmountMoreThanZeroPartiallyRefundByManually_SaveTransactionSuccess()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 0m;
            var updateResult = new OperationStatus();
            updateResult.Status = true;

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = true;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });
            var orderItems = new List<OrderItem> { orderItem };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.Manually,
                IsSelected = true,
                Title = "My organization will issue the refund manually"
            };

            var mode2 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.AddToCredit,
                IsSelected = false,
                Title = "Transfer the balance to the family credit"
            };

            var modes = new List<RefundInformation> { mode1, mode2 };

            var mode = new OrderItemsViewModel()
            {
                Id = 3,
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            var orderItemPaidAmount = orderItem.PaidAmount;

            _mockOrderItemRepository.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable());

            //Act
            var result = service.Refund(mode, ref refundedAmount);

            //Assert
            result.Status.Should().Be(true);

            orderItem.PaidAmount.Should().Be(orderItemPaidAmount - refundedAmount);

            orderItem.TransactionActivities.Count(t => t.Amount == refundedAmount
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Cash
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == mode.CurrentUserName).Should().Be(1);
        }

        [TestMethod]
        public void Refund_RefundAmountMoreThanZeroPartiallyRefundByAutomaticallyByStripe_SaveTransactionSuccessAndPaymentDetailCategoryIsCard()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 0m;
            var updateResult = new OperationStatus();
            updateResult.Status = true;
            var refunded = new List<TransactionActivity>();
            var isPaypalTransaction = false;
            var isTestMode = true;

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";
            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;
            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });
            var orderItems = new List<OrderItem> { orderItem };

            var paymentdetails = new PaymentDetailModel()
            {
                Amount = 20,
                AmountRefunded = 0,
                Status = true,
            };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.Automatically,
                IsSelected = true,
                Title = "Issue the refund automatically"
            };

            var mode2 = new RefundInformation
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.AddToCredit,
                IsSelected = false,
                Title = "Transfer the balance to the family credit"
            };

            var model = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 10,
                RefundNote = null,
                Status = true,
            };
            var club = user1.Club;

            var refundAmount = model.RefundAmount;

            var modes = new List<RefundInformation> { mode1, mode2 };

            var mode = new OrderItemsViewModel()
            {
                Id = 3,
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            var orderItemPaidAmount = orderItem.PaidAmount;

            _mockOrderItemRepository.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable());
            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(orderItem)).Returns(CreateOrderItemTransactions);
            _mockTransactionActivityBusiness.Setup(x => x.GetRefundAutomaticallyTransactions(orderItem)).Returns(new List<TransactionActivity>());
            _mockPaymentBusiness.Setup(x => x.GetPaymentDetails(orderItem.Order.Club.Domain, transaction.PaymentDetail.PayKey, refunded, isPaypalTransaction, isTestMode)).Returns(paymentdetails);
            _mockPaymentBusiness.Setup(x => x.Refund(It.IsAny<TransactionActivity>(), null, It.IsAny<OrderItem>(), club.Domain, refundAmount, !orderItem.Order.IsLive)).Returns(model);

            //Act
            var result = service.Refund(mode, ref refundedAmount);

            //Assert
            result.Status.Should().Be(true);

            orderItem.PaidAmount.Should().Be(orderItemPaidAmount - refundedAmount);

            orderItem.TransactionActivities.Count(t => t.Amount == refundedAmount
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Card
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == mode.CurrentUserName).Should().Be(1);
        }

        [TestMethod]
        public void Refund_RefundAmountMoreThanZeroFullRefundByAutomaticallyByStripe_StatusIsFalseAndPaymentDetailCategoryIsCardTransactionstatusFailur()
        {
            //Arrange
            var service = CreateService();
            var refundedAmount = 0m;
            var updateResult = new OperationStatus { Status = true };
            var refunded = new List<TransactionActivity>();
            var isPaypalTransaction = false;
            var isTestMode = true;

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.ERROR;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";
            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Failure;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;
            });

            var user1 = new ClubUser()
            {
                User = FactoryGirl.Build<JbUser>(),
                UserId = FactoryGirl.Build<JbUser>().Id,
                Club = FactoryGirl.Build<OrderItem>().Order.Club,
            };

            var users = new List<ClubUser>() { user1 };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.Order.Club.Users = users;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });
            var orderItems = new List<OrderItem> { orderItem };

            var paymentdetails = new PaymentDetailModel()
            {
                Amount = 20,
                AmountRefunded = 0,
                Status = true,
            };

            var mode1 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.Automatically,
                IsSelected = true,
                Title = "Issue the refund automatically"
            };

            var mode2 = new RefundInformation()
            {
                Priority = 1,
                Amount = 10,
                Mode = RefundMode.AddToCredit,
                IsSelected = false,
                Title = "Transfer the balance to the family credit"
            };

            var model = new RefundResultModel
            {
                TransactionStatus = TransactionStatus.Success,
                RefundAmount = 10,
                RefundNote = null,
                Status = false,
            };
            var club = user1.Club;

            var refundAmount = model.RefundAmount;

            var modes = new List<RefundInformation> { mode1, mode2 };

            var mode = new OrderItemsViewModel()
            {
                Id = 3,
                AddOnInstallments = null,
                RefundModes = modes,
                CurrentUserName = "Support@jumbula.com",
                CurrentRoleType = RoleCategoryType.Dashboard,
            };

            var orderItemPaidAmount = orderItem.PaidAmount;

            _mockOrderItemRepository.Setup(x => x.Update(It.IsAny<OrderItem>())).Returns(updateResult);
            _mockOrderItemRepository.Setup(x => x.GetList()).Returns(orderItems.AsQueryable());
            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(orderItem)).Returns(CreateOrderItemTransactions);
            _mockTransactionActivityBusiness.Setup(x => x.GetRefundAutomaticallyTransactions(orderItem)).Returns(new List<TransactionActivity>());
            _mockPaymentBusiness.Setup(x => x.GetPaymentDetails(orderItem.Order.Club.Domain, transaction.PaymentDetail.PayKey, refunded, isPaypalTransaction, isTestMode)).Returns(paymentdetails);
            _mockPaymentBusiness.Setup(x => x.Refund(It.IsAny<TransactionActivity>(), null, It.IsAny<OrderItem>(), club.Domain, refundAmount, !orderItem.Order.IsLive)).Returns(model);

            //Act
            var result = service.Refund(mode, ref refundedAmount);

            //Assert
            result.Status.Should().Be(false);

            orderItem.PaidAmount.Should().Be(orderItemPaidAmount);

            orderItem.TransactionActivities.Count(t => t.Amount == refundAmount
            && t.TransactionCategory == TransactionCategory.Refund
            && t.PaymentDetail.PaymentMethod == PaymentMethod.Card
            && t.PaymentDetail.PayerType == RoleCategoryType.Dashboard
            && t.PaymentDetail.PayerEmail == mode.CurrentUserName
            && t.TransactionStatus == TransactionStatus.Failure
            && t.PaymentDetail.Status == PaymentDetailStatus.ERROR).Should().Be(1);

        }

        [TestMethod]
        public void GetAmountAutomaticallyTransactions_OrderItemNotRefundTransaction_ShouldReturnPaymentTransaction()
        {
            //Arrange
            var service = CreateService();

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";
            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.TakePayment;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Payment;
                p.PaymentDetail = paymentDetail;
                p.Amount = 20;
            });

            var transactions = new List<TransactionActivity> { transaction };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
                p.TransactionActivities = transactions;
            });

            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(orderItem)).Returns(transactions);
            _mockTransactionActivityBusiness.Setup(x => x.GetRefundAutomaticallyTransactions(orderItem)).Returns(new List<TransactionActivity>());

            //Act
            var result = service.GetAmountAutomaticallyTransactions(orderItem);

            //Assert

            result.Should().Be(20);
        }

        [TestMethod]
        public void GetAmountAutomaticallyTransactions_OrderItemHasRefundTransaction_ShouldReturnPaymentAmountTransactionNegativeRefundAmountTransaction()
        {
            //Arrange
            var service = CreateService();

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";
            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.TakePayment;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Payment;
                p.PaymentDetail = paymentDetail;
                p.Amount = 20;
            });

            var refundTransaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 5;
            });

            var refundTransactions = new List<TransactionActivity> { refundTransaction };
            var transactions = new List<TransactionActivity> { transaction };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
                p.TransactionActivities = transactions;
            });

            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(orderItem)).Returns(transactions);
            _mockTransactionActivityBusiness.Setup(x => x.GetRefundAutomaticallyTransactions(orderItem)).Returns(refundTransactions);

            //Act
            var result = service.GetAmountAutomaticallyTransactions(orderItem);

            //Assert

            result.Should().Be(15);
        }

        [TestMethod]
        public void GetAmountAutomaticallyTransactions_OrderItemDoseNotTransaction_ShouldReturnZero()
        {
            //Arrange
            var service = CreateService();

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(orderItem)).Returns(new List<TransactionActivity>());
            _mockTransactionActivityBusiness.Setup(x => x.GetRefundAutomaticallyTransactions(orderItem)).Returns(new List<TransactionActivity>());

            //Act
            var result = service.GetAmountAutomaticallyTransactions(orderItem);

            //Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void UpdateRefund_RefundedAmountHasValue_ShouldSetOrderItemPaidAmountNegativeRefundedAmount()
        {
            //Arrange
            var service = CreateService();
            var status = true;
            var refundedAmount = 20;
            var historyDescription = "Test";
            var model = new OrderItemsViewModel
            {
                CurrentUserId = 2,
            };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var updateResult = new OperationStatus()
            {
                Status = true
            };

            _mockTransactionActivityBusiness.Setup(x => x.GetPaymentTransactions(orderItem)).Returns(new List<TransactionActivity>());
            _mockTransactionActivityBusiness.Setup(x => x.GetRefundAutomaticallyTransactions(orderItem)).Returns(new List<TransactionActivity>());
            _mockOrderItemRepository.Setup(x => x.Update(orderItem)).Returns(updateResult);

            //Act
            var result = service.UpdateRefund(status, orderItem, refundedAmount, model, historyDescription);

            //Assert
            orderItem.PaidAmount.Should().Be(0);
            result.Status.Should().BeTrue();
            _mockOrderItemRepository.Verify(h => h.Update(orderItem), Times.Once);
        }

        [TestMethod]
        public void GetRefundAmount_AmountAndRefundAmountISEqual_ShouldReturnZeroForRefund()
        {
            //Arrange
            var service = CreateService();

            var refundedAmount = 20;
            var isPaypalTransaction = false;
            var isTestMode = false;

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";
                p.PayKey = "test";
            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Failure;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;
            });

            var refundTransaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;
            });

            var refunded = new List<TransactionActivity> { refundTransaction };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.Order.Club.Domain = "Qaclubsand";
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var paymentdetails = new PaymentDetailModel()
            {
                Amount = 20,
                AmountRefunded = 20,
                Status = true,
            };

            _mockPaymentBusiness.Setup(x => x.GetPaymentDetails(orderItem.Order.Club.Domain, transaction.PaymentDetail.PayKey, refunded, isPaypalTransaction, isTestMode)).Returns(paymentdetails);

            //Act
            var result = service.GetRefundAmount(transaction, orderItem, refundedAmount, false, refunded, false);

            //Assert
            result.Should().Be(0);
        }

        [TestMethod]
        public void GetRefundAmount_RefundedAmountIsLessThanAmountAndCanRefundIsLessThanRefundAmountAndTransactionAmount_ShouldReturnAmountNegativeRefundedAmount()
        {
            //Arrange
            var service = CreateService();

            var refundedAmount = 20;
            var isPaypalTransaction = false;
            var isTestMode = false;

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";
                p.PayKey = "test";
            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Failure;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 30;
            });

            var refundTransaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;
            });

            var refunded = new List<TransactionActivity> { refundTransaction };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.Order.Club.Domain = "Qaclubsand";
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var paymentdetails = new PaymentDetailModel()
            {
                Amount = 20,
                AmountRefunded = 10,
                Status = true,
            };

            _mockPaymentBusiness.Setup(x => x.GetPaymentDetails(orderItem.Order.Club.Domain, transaction.PaymentDetail.PayKey, refunded, isPaypalTransaction, isTestMode)).Returns(paymentdetails);

            //Act
            var result = service.GetRefundAmount(transaction, orderItem, refundedAmount, false, refunded, false);

            //Assert
            result.Should().Be(paymentdetails.Amount - paymentdetails.AmountRefunded);
        }

        [TestMethod]
        public void GetRefundAmount_RefundedAmountIsLessThanAmountAndCanRefundIsLessThanRefundAmountAndMorethanTransactionAmount_ShouldReturnTransactionAmount()
        {
            //Arrange
            var service = CreateService();

            var refundedAmount = 20;
            var isPaypalTransaction = false;
            var isTestMode = false;

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";
                p.PayKey = "test";
            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Failure;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 5;
            });

            var refundTransaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 10;
            });

            var refunded = new List<TransactionActivity> { refundTransaction };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.Order.Club.Domain = "Qaclubsand";
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var paymentdetails = new PaymentDetailModel()
            {
                Amount = 20,
                AmountRefunded = 10,
                Status = true,
            };

            _mockPaymentBusiness.Setup(x => x.GetPaymentDetails(orderItem.Order.Club.Domain, transaction.PaymentDetail.PayKey, refunded, isPaypalTransaction, isTestMode)).Returns(paymentdetails);

            //Act
            var result = service.GetRefundAmount(transaction, orderItem, refundedAmount, false, refunded, false);

            //Assert
            result.Should().Be(transaction.Amount);
        }

        [TestMethod]
        public void GetRefundAmount_RefundedAmountIsLessThanAmountAndCanRefundIsMOreThanRefundAmountAndLessThanTransactionAmount_ShouldReturnRefundAmount()
        {
            //Arrange
            var service = CreateService();

            var refundedAmount = 20;
            var isPaypalTransaction = false;
            var isTestMode = false;

            var paymentDetail = FactoryGirl.Build<PaymentDetail>(p =>
            {
                p.PaymentMethod = PaymentMethod.Card;
                p.Status = PaymentDetailStatus.COMPLETED;
                p.TransactionMessage = "";
                p.PayerType = RoleCategoryType.Dashboard;
                p.PayerEmail = "Support@jumbula.com";
                p.PayKey = "test";
            });

            var transaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Failure;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 30;
            });

            var refundTransaction = FactoryGirl.Build<TransactionActivity>(p =>
            {
                p.TransactionCategory = TransactionCategory.Refund;
                p.TransactionStatus = TransactionStatus.Success;
                p.TransactionType = TransactionType.Refund;
                p.PaymentDetail = paymentDetail;
                p.Amount = 30;
            });

            var refunded = new List<TransactionActivity> { refundTransaction };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.Order.Club.Domain = "Qaclubsand";
                p.TotalAmount = 20;
                p.PaidAmount = 20;
            });

            var paymentdetails = new PaymentDetailModel()
            {
                Amount = 60,
                AmountRefunded = 10,
                Status = true,
            };

            _mockPaymentBusiness.Setup(x => x.GetPaymentDetails(orderItem.Order.Club.Domain, transaction.PaymentDetail.PayKey, refunded, isPaypalTransaction, isTestMode)).Returns(paymentdetails);

            //Act
            var result = service.GetRefundAmount(transaction, orderItem, refundedAmount, false, refunded, false);

            //Assert
            result.Should().Be(refundedAmount);
        }

        [TestMethod]
        public void SetToUserCredit_OrderItemIsTestModeIsFalse_ShouldReturnUserCreditSumWithRefundAmount()
        {
            //Arrange
            var service = CreateService();

            var refundedAmount = 20;

            var user = FactoryGirl.Build<ClubUser>(p =>
            {
                p.TestCredit = 10;
                p.Credit = 20;
                p.UserId = 3;
            });
            var users = new List<ClubUser>() { user };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = true;
                p.Order.Club.Domain = "Qaclubsand";
                p.TotalAmount = 20;
                p.PaidAmount = 20;
                p.Order.Club.Users = users;
                p.Order.UserId = 3;
            });

            //Act
            service.SetToUserCredit(orderItem, refundedAmount);

            //Assert
            user.TestCredit.Should().Be(user.TestCredit);
            user.Credit.Should().Be(40);
        }

        [TestMethod]
        public void SetToUserCredit_OrderItemIsTestModeIsTrue_ShouldReturnUserTestCreditSumWithRefundAmount()
        {
            //Arrange
            var service = CreateService();

            var refundedAmount = 20;

            var user = FactoryGirl.Build<ClubUser>(p =>
            {
                p.TestCredit = 10;
                p.Credit = 20;
                p.UserId = 3;
            });
            var users = new List<ClubUser>() { user };

            var orderItem = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = 3;
                p.Order.IsLive = false;
                p.Order.Club.Domain = "Qaclubsand";
                p.TotalAmount = 20;
                p.PaidAmount = 20;
                p.Order.Club.Users = users;
                p.Order.UserId = 3;
            });

            //Act
            service.SetToUserCredit(orderItem, refundedAmount);

            //Assert
            user.TestCredit.Should().Be(30);
            user.Credit.Should().Be(user.Credit);
        }

        #endregion

        #region Private methods
        private OrderItemBusiness CreateService()
        {
            _mockClubBusiness = new Mock<IClubBusiness>();
            _mockPendingPreapprovalTransactionBusiness = new Mock<IPendingPreapprovalTransactionBusiness>();
            _mockPlayerProfileBusiness = new Mock<IPlayerProfileBusiness>();
            _mockProgramBusiness = new Mock<IProgramBusiness>();
            _mockSubscriptionBusiness = new Mock<ISubscriptionBusiness>();
            _mockUserProfileBusiness = new Mock<IUserProfileBusiness>();
            _mockOrderInstallmentBusiness = new Mock<IOrderInstallmentBusiness>();
            _mockSeasonBusiness = new Mock<ISeasonBusiness>();
            _mockOrderSessionBusiness = new Mock<IOrderSessionBusiness>();
            _mockOrderItemRepository = new Mock<IRepository<OrderItem, long>>();
            _mockDataContext = new Mock<IEntitiesContext>();
            _mockProgramSessionBusiness = new Mock<IProgramSessionBusiness>();
            _mockOrderBusiness = new Mock<IOrderBusiness>();
            _mockJbDbFunctions = new Mock<IJbDbFunctions>();
            _mockClubSettingBusiness = new Mock<IClubSettingBusiness>();
            _mockFormBusiness = new Mock<IFormBusiness>();
            _mockAccountingBusiness = new Mock<IAccountingBusiness>();
            _mockPaymentBusiness = new Mock<IPaymentBusiness>();
            _mockOrderHistoryBusiness = new Mock<IOrderHistoryBusiness>();
            _mockTransactionActivityBusiness = new Mock<ITransactionActivityBusiness>();

            var result = new OrderItemBusiness(_mockClubBusiness.Object,
                                               _mockPendingPreapprovalTransactionBusiness.Object,
                                               _mockUserProfileBusiness.Object,
                                               _mockDataContext.Object,
                                               _mockOrderItemRepository.Object, _mockClubSettingBusiness.Object, _mockFormBusiness.Object,
                                               _mockAccountingBusiness.Object, _mockJbDbFunctions.Object,
                                               _mockTransactionActivityBusiness.Object,
                                               _mockPaymentBusiness.Object, _mockOrderHistoryBusiness.Object);

            result.ProgramBusiness = _mockProgramBusiness.Object;
            result.SubscriptionBusiness = _mockSubscriptionBusiness.Object;
            result.OrderSessionBusiness = _mockOrderSessionBusiness.Object;
            result.PlayerProfileBusiness = _mockPlayerProfileBusiness.Object;
            result.SeasonBusiness = _mockSeasonBusiness.Object;
            result.OrderInstallmentBusiness = _mockOrderInstallmentBusiness.Object;
            result.ProgramSessionBusiness = _mockProgramSessionBusiness.Object;
            result.OrderBusiness = _mockOrderBusiness.Object;

            return result;
        }

        private List<Order> CreateOrders()
        {
            var order1 = FactoryGirl.Build<Order>(p => { p.Id = Id; p.IsLive = false; });
            var order2 = FactoryGirl.Build<Order>(p => { p.Id = Id + 1; p.IsLive = false; });
            var order3 = FactoryGirl.Build<Order>(p =>
            {
                p.Id = Id + 2;
                p.Club = FactoryGirl.Build<Club>();
                p.PaymentPlanType = PaymentPlanType.Installment;
                p.UserId = UserId;
                p.Club.Id = ClubId;
                p.IsLive = false;

            });
            var order4 = FactoryGirl.Build<Order>(p =>
            {
                p.Id = Id + 3;
                p.Club = FactoryGirl.Build<Club>();
                p.PaymentPlanType = PaymentPlanType.FullPaid;
                p.UserId = UserId;
                p.Club.Id = ClubId;
                p.IsLive = false;
            });

            var order5 = FactoryGirl.Build<Order>(p =>
            {
                p.Id = Id + 4;
                p.Club = FactoryGirl.Build<Club>();
                p.PaymentPlanType = PaymentPlanType.Installment;
                p.UserId = UserId;
                p.Club.Id = ClubId;
                p.IsLive = false;
            });

            var order6 = FactoryGirl.Build<Order>(p =>
            {
                p.Id = Id + 4;
                p.IsDraft = false;
                p.Club = FactoryGirl.Build<Club>();
                p.UserId = UserId;
                p.Club.Id = ClubId;
                p.OrderStatus = OrderStatusCategories.completed;
                p.IsLive = true;
                p.OrderItems = CreateFullPaidOrderItems();
                p.Club.TimeZone = Common.Enums.TimeZone.UTC;
            });

            var order7 = FactoryGirl.Build<Order>(p =>
            {
                p.Id = Id + 4;
                p.IsDraft = false;
                p.Club = FactoryGirl.Build<Club>();
                p.UserId = UserId;
                p.Club.Id = ClubId;
                p.OrderStatus = OrderStatusCategories.completed;
                p.IsLive = true;
                p.OrderItems = CreateInstallmentOrderItems();
                p.Club.TimeZone = Common.Enums.TimeZone.UTC;
            });

            return new List<Order> { order1, order2, order3, order4, order5, order6, order7 };
        }

        private List<Order> CreateLiveOrders()
        {
            var order1 = FactoryGirl.Build<Order>(p =>
            {
                p.Id = Id + 4;
                p.IsDraft = false;
                p.Club = FactoryGirl.Build<Club>();
                p.PaymentPlanType = PaymentPlanType.Installment;
                p.UserId = 63;
                p.Club.Id = 10;
                p.OrderStatus = OrderStatusCategories.completed;
                p.IsLive = true;
            });

            var order2 = FactoryGirl.Build<Order>(p =>
            {
                p.Id = Id + 4;
                p.IsDraft = false;
                p.Club = FactoryGirl.Build<Club>();
                p.PaymentPlanType = PaymentPlanType.Installment;
                p.UserId = 13;
                p.Club.Id = 10;
                p.OrderStatus = OrderStatusCategories.completed;
                p.IsLive = true;
            });

            return new List<Order> { order1, order2 };
        }

        private List<OrderItem> CreateFullPaidOrderItems()
        {
            var orderItem1 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 4;
                p.PaymentPlanType = PaymentPlanType.FullPaid;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.Order.Club = FactoryGirl.Build<Club>();
                p.Order.Club.Id = ClubId;
                p.Order.UserId = UserId;
                p.Order.IsLive = true;
                p.TotalAmount = 200;
                p.PaidAmount = 100;
                p.Order.Club.TimeZone = Common.Enums.TimeZone.UTC;
                p.Player = FactoryGirl.Build<PlayerProfile>();
                p.Player.Contact = FactoryGirl.Build<ContactPerson>();
            });

            var orderItem2 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 5;
                p.PaymentPlanType = PaymentPlanType.FullPaid;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.Order.Club = FactoryGirl.Build<Club>();
                p.Order.Club.Id = ClubId;
                p.Order.UserId = UserId;
                p.Order.IsLive = true;
                p.PaidAmount = 200;
                p.Order.Club.TimeZone = Common.Enums.TimeZone.UTC;
                p.Player = FactoryGirl.Build<PlayerProfile>();
                p.Player.Contact = FactoryGirl.Build<ContactPerson>();
            });

            return new List<OrderItem> { orderItem1, orderItem2 };
        }

        private List<OrderItem> CreateInstallmentOrderItems()
        {
            var orderItem1 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 4;
                p.PaymentPlanType = PaymentPlanType.Installment;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.Order.Club = FactoryGirl.Build<Club>();
                p.Order.Club.Id = ClubId;
                p.Order.UserId = UserId;
                p.Order.IsLive = true;
                p.Installments = CreateOrderInstallments();
                p.Order.Club.TimeZone = Common.Enums.TimeZone.UTC;
                p.Player = FactoryGirl.Build<PlayerProfile>();
                p.Player.Contact = FactoryGirl.Build<ContactPerson>();
            });

            var orderItem2 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 4;
                p.PaymentPlanType = PaymentPlanType.Installment;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.Order.Club = FactoryGirl.Build<Club>();
                p.Order.Club.Id = ClubId;
                p.Order.UserId = UserId;
                p.Order.IsLive = true;
                p.Installments = CreateOrderInstallments();
                p.Order.Club.TimeZone = Common.Enums.TimeZone.UTC;
                p.Player = FactoryGirl.Build<PlayerProfile>();
                p.Player.Contact = FactoryGirl.Build<ContactPerson>();
            });

            return new List<OrderItem> { orderItem1, orderItem2 };
        }

        private List<OrderInstallment> CreateOrderInstallments()
        {
            var orderItem = FactoryGirl.Build<OrderItem>(p => p.Id = 1);

            var installment1 = FactoryGirl.Build<OrderInstallment>(p =>
            {
                p.Id = OrderInstallmentId;
                p.OrderItem = orderItem;
                p.OrderItem.Order = FactoryGirl.Build<Order>();
                p.OrderItem.Order.Club = FactoryGirl.Build<Club>();
                p.OrderItem.Order.Club.Currency = CurrencyCodes.USD;
                p.OrderItem.Player = FactoryGirl.Build<PlayerProfile>();
                p.OrderItem.Player.Contact = FactoryGirl.Build<ContactPerson>();
                p.InstallmentDate = DateTime.UtcNow;
                p.EnableReminder = false;
                p.Amount = 200;
                p.PaidAmount = 0;
                p.OrderItem.Player.Contact.FirstName = "Maryam";
                p.OrderItem.Player.Contact.LastName = "Saeedi";
            });

            return new List<OrderInstallment> { installment1 };
        }

        private List<OrderItem> CreateOrderItems()
        {
            var orderItem1 = FactoryGirl.Build<OrderItem>(p => p.Id = Id);
            var orderItem2 = FactoryGirl.Build<OrderItem>(p => p.Id = Id + 1);
            var orderItem3 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 2;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.ProgramScheduleId = ProgramScheduleId;
            });
            var orderItem4 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 3;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.completed;
                p.ProgramSchedule = null;
                p.ProgramScheduleId = null;
            });
            var orderItem5 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 4;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Test;
                p.ItemStatus = OrderItemStatusCategories.completed;

            });
            var orderItem6 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 5;
                p.Order.IsLive = false;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.changed;
            });
            var orderItem7 = FactoryGirl.Build<OrderItem>(p =>
            {
                p.Id = Id + 6;
                p.Order.IsLive = true;
                p.ProgramSchedule.Program.Season.Status = SeasonStatus.Live;
                p.ItemStatus = OrderItemStatusCategories.changed;
            });

            return new List<OrderItem> { orderItem1, orderItem2, orderItem3, orderItem4, orderItem5, orderItem6, orderItem7 };
        }

        private DelinquentPolicy FillDelinquentPolicy()
        {
            return new DelinquentPolicy { FamilyBalanceNoLimitDate = _equalToFamilyBalanceNoLimitDate, FamilyBalanceMinPrice = 50, AutoChargeFailAttemptNumber = 4 };
        }

        private DelinquentPolicy FillDelinquentPolicyWithZeroFamilyBalanceMinPrice()
        {
            return new DelinquentPolicy { FamilyBalanceNoLimitDate = _equalToFamilyBalanceNoLimitDate, FamilyBalanceMinPrice = 0, AutoChargeFailAttemptNumber = 4 };
        }

        private OrderItem FillOrderItem(long id, int clubId, int? partnerId, PaymentPlanType paymentPlanType, bool isLive, DateTime completeDate, OrderItemStatusCategories itemStatus, OrderItemStatusReasons itemStatusReason, decimal totalAmount, decimal paidAmount)
        {
            return new OrderItem
            {
                Id = id,
                PaymentPlanType = paymentPlanType,
                Order = new Order
                {
                    ClubId = clubId,
                    IsLive = isLive,
                    Club = new Club { Id = clubId, PartnerId = partnerId },
                    CompleteDate = completeDate
                },
                ItemStatus = itemStatus,
                ItemStatusReason = itemStatusReason,
                TotalAmount = totalAmount,
                PaidAmount = paidAmount,
                ProgramScheduleId = ProgramScheduleId
            };
        }

        private OrderItem FillOrderItem(int clubId, int? partnerId, PaymentPlanType paymentPlanType, bool isLive, DateTime completeDate, OrderItemStatusCategories itemStatus, OrderItemStatusReasons itemStatusReason, decimal totalAmount, decimal paidAmount)
        {
            return FillOrderItem(0, clubId, partnerId, paymentPlanType, isLive, completeDate, itemStatus, itemStatusReason, totalAmount, paidAmount);
        }

        private OrderItem FillInstallmentOrderItem(long id, int clubId, int? partnerId, bool isLive, bool isAutoCharge, List<OrderInstallment> orderInstallment)
        {
            return new OrderItem
            {
                Id = id,
                Order = new Order { ClubId = clubId, Club = new Club { Id = clubId, PartnerId = partnerId }, IsLive = isLive, IsAutoCharge = isAutoCharge },
                Installments = orderInstallment,
                PaymentPlanType = PaymentPlanType.Installment,
                ItemStatus = OrderItemStatusCategories.completed
            };
        }

        private OrderInstallment FillOrderInstallment(int id, long orderItemId, bool enableReminder, bool isDeleted, DateTime installmentDate, decimal amount, decimal paidAmount, int autoChargeAttempts)
        {
            return new OrderInstallment
            {
                Id = id,
                OrderItemId = orderItemId,
                EnableReminder = enableReminder,
                IsDeleted = isDeleted,
                InstallmentDate = installmentDate,
                Amount = amount,
                PaidAmount = paidAmount,
                AutoChargeAttemps = autoChargeAttempts
            };
        }

        private IEnumerable<OrderItem> FillOrderItemWithoutPartner()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 50)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemWithPartner()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 50),
                FillOrderItem(new Random().Next(), ClubId, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 50)
            };
        }

        private IEnumerable<OrderItem> FillInstallmentOrderItemWithoutPartner()
        {
            return new List<OrderItem>
            {
                FillInstallmentOrderItem(0,ClubId, null,true,true, new List<OrderInstallment>{FillOrderInstallment(0,0,false,false,DateTime.UtcNow.Date.AddDays(-1),200,0,AutoChargeFailAttemptNumber+1)})
            };
        }

        //private IEnumerable<OrderItem> FillInstallmentOrderItemWithPartner()
        //{
        //    return new List<OrderItem>
        //    {
        //        FillInstallmentOrderItem(0,ClubId, null,true,true,  new List<OrderInstallment>{FillOrderInstallment(0,0,false,false,DateTime.UtcNow.Date.AddDays(-1),200,0,AutoChargeFailAttemptNumber+1)}),
        //        FillInstallmentOrderItem(0,new Random().Next(), ClubId, true,true, new List<OrderInstallment>{FillOrderInstallment(0,0,false,false,DateTime.UtcNow.Date.AddDays(-1),200,0,AutoChargeFailAttemptNumber+1)})
        //    };
        //}

        private IEnumerable<OrderItem> FillOrderItemPaymentPlanTypeNotFullPaid()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.Installment, true, _lowerThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 50),
            };
        }

        private IEnumerable<OrderItem> FillOrderItemPaymentPlanTypeIsFullPaid()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 50)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemCompleteDateEqualToFamilyBalanceNoLimitDate()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _equalToFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 50),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _equalToFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn, 120, 50)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemCompleteDateGreaterThanFamilyBalanceNoLimitDate()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _greaterThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 50),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _greaterThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn, 120, 50)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemCompleteDateLowerThanFamilyBalanceNoLimitDate()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _equalToFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 50),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.transferIn, 120, 50)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemTotalAmountEqualToPaidAmount()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.regular, 120, 120),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.transferIn, 120, 120)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemTotalAmountGreaterThanPaidAmount()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.regular, 120, 110),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.transferIn, 120, 110)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemTotalAmountLowerThanPaidAmount()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.regular, 120, 130),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.transferIn, 120, 130)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemProgramScheduleIdIsNull()
        {
            var orderItem = FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, false, _lowerThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 120, 70);

            orderItem.ProgramScheduleId = null;

            return new List<OrderItem>
            {
                orderItem
            };
        }

        private IEnumerable<OrderItem> FillOrderItemIsLiveFalse()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, false, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.regular, 120, 70)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemDifferenceAmountEqualToFamilyBalanceMinPrice()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.regular, 120, 70),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.transferIn, 120, 70)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemDifferenceAmountGreaterThanFamilyBalanceMinPrice()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.regular, 120, 50),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.transferIn, 120, 50)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemDifferenceAmountLowerThanFamilyBalanceMinPrice()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.regular, 120, 100),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.transferIn, 120, 100)
            };
        }

        private IEnumerable<OrderInstallment> FillOrderItemsInstallmentExists()
        {
            return new List<OrderInstallment>
            {
                FillOrderInstallment(0, 0, false, false, _installmentDayLowerThanNow, 150, 100,
                    AutoChargeFailAttemptNumber),
                FillOrderInstallment(0, 0, false, false, _installmentDayLowerThanNow, 150, 100,
                    AutoChargeFailAttemptNumber + 1),
                FillOrderInstallment(0, 0, false, false, _installmentDayLowerThanNow, 200, 100,
                    AutoChargeFailAttemptNumber)
            };
        }

        private IEnumerable<OrderItem> FillOrderItemWithDifferencesItemStatus()
        {
            return new List<OrderItem>
            {
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.notInitiated,OrderItemStatusReasons.regular, 0,0),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.initiated,OrderItemStatusReasons.regular, 0,0),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.completed,OrderItemStatusReasons.regular, 0,0),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.changed,OrderItemStatusReasons.canceled, 0,0),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.deleted,OrderItemStatusReasons.regular, 0,0),
                FillOrderItem(ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate,OrderItemStatusCategories.showAll,OrderItemStatusReasons.showAll, 0,0)
            };
        }

        private IEnumerable<OrderItem> FillOrderItems()
        {
            var orderItems = new List<OrderItem>();

            foreach (var itemId in _ids)
            {
                orderItems.Add(FillOrderItem(itemId, ClubId, null, PaymentPlanType.FullPaid, true, _lowerThanFamilyBalanceNoLimitDate, OrderItemStatusCategories.completed, OrderItemStatusReasons.regular, 0, 0));
            }

            return orderItems;
        }

        private List<TransactionActivity> CreateOrderItemTransactions()
        {
            var orderItem1 = FactoryGirl.Build<OrderItem>(p => p.Id = 1);
            var orderItem2 = FactoryGirl.Build<OrderItem>(p => p.Id = 2);
            var orderItem3 = FactoryGirl.Build<OrderItem>(p => p.Id = 3);
            var orderItem4 = FactoryGirl.Build<OrderItem>(p => p.Id = 4);

            var transaction1 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 1;
                t.OrderItem = orderItem1;
                t.OrderItemId = 1;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PaymentMethod = PaymentMethod.Card;
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Failure;
                t.Amount = 10;
            });

            var transaction2 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 2;
                t.OrderItem = orderItem2;
                t.OrderItemId = 2;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test payKey";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Pending;
                t.Amount = 10;
            });

            var transaction3 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 3;
                t.OrderItem = orderItem3;
                t.OrderItemId = 3;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test payKey";
                t.TransactionType = TransactionType.Refund;
                t.TransactionStatus = TransactionStatus.Success;
                t.Amount = 10;
            });

            var transaction4 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 4;
                t.OrderItem = orderItem4;
                t.OrderItemId = 4;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test payKey";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Success;
                t.Amount = 10;
            });

            var transaction5 = FactoryGirl.Build<TransactionActivity>(t =>
            {
                t.Id = 4;
                t.OrderItem = orderItem4;
                t.OrderItemId = 4;
                t.PaymentDetail = FactoryGirl.Build<PaymentDetail>();
                t.PaymentDetail.PayKey = "Test payKey";
                t.TransactionType = TransactionType.Payment;
                t.TransactionStatus = TransactionStatus.Success;
                t.Amount = 10;
            });


            return new List<TransactionActivity> { transaction1, transaction2, transaction3, transaction4, transaction5 };
        }

        #endregion
    }

}
