﻿using Jb.Framework.Common.Forms;
using SportsClub.Models.FormModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Core.Domain;
using Jumbula.Web.Infrastructure;

namespace SportsClub.Models
{
    public enum ChessTourneyEntriesSortType
    {
        None,
        Section,
        Schedule
    }
    public class ChessTourneyEntriesViewModel
    {
        /// <summary>
        /// Tourney name ( used in dialog header )
        /// </summary>
        public string Name { get; set; }
        public Club Club { get; set; }
        public string ClubDomain { get; set; }
        public int ClubId { get; set; }
        public long SeasonId { get; set; }
        public long ProgramId { get; set; }
        public string Domain { get; set; }

        [DisplayName("Total users")]
        public int TotalUsers { get; set; }

        public ChessTourneyEntriesSortType Sort { get; set; }
        /// <summary>
        /// list of tournament entries
        /// </summary>
        public TourneyRewardType TourneyType { get; set; }

        public ChessTourneyEntriesViewModel()
        {
        }

        public ChessTourneyEntriesViewModel(long programId, long seasonId, int clubId)
            : this()
        {
            this.ClubId = clubId;
            this.SeasonId = seasonId;
            this.ProgramId = programId;
            var items=Ioc.OrderItemBusiness.ChessTourneyEntries(programId,seasonId,clubId);
            TotalUsers=items.Count();
            Club = Ioc.ClubBusiness.Get(clubId);
        }
        public List<ChessTourneyEntryViewModel> GetChessTourneyEntries()
        {
            var entries = Ioc.OrderItemBusiness.ChessTourneyEntries(this.ProgramId, this.SeasonId, this.ClubId).ToList();
            var result = new List<ChessTourneyEntryViewModel>();
            foreach (var item in entries)
            {
                var entry = new ChessTourneyEntryViewModel(item, this.ClubId, Club.TimeZone);
                entry.OrderItemId = item.OrderItemId;
                try
                {
                    entry.UscfId = item.UscfId;
                    entry.UscfExpirationDate = item.UscfExpiration;
                    entry.UscfRating = item.UscfRatingReg;
                    if (!string.IsNullOrEmpty(item.UscfRatingQuick))
                        entry.RatingNumber = int.Parse(item.UscfRatingQuick);
                }
                catch { }
                if (TourneyType == TourneyRewardType.Quad && entry.UscfRating != null)
                {
                    // calculate rating number for quad tournaments
                    try
                    {
                        int index = entry.UscfRating.IndexOf("*");
                        var rating = int.Parse(entry.UscfRating.Substring(0, index));
                        entry.RatingNumber = rating;

                        result.Add(entry);

                        continue;
                    }
                    catch
                    {
                    }

                    try
                    {
                        int index = entry.UscfRating.IndexOf("/");
                        var rating = int.Parse(entry.UscfRating.Substring(0, index));
                        entry.RatingNumber = rating;

                        result.Add(entry);

                        continue;
                    }
                    catch
                    {
                    }

                    try
                    {

                        int rating;
                        if (int.TryParse(entry.UscfRating.Trim(), out rating))
                        {
                            entry.RatingNumber = rating;
                        }
                        else
                        {
                            entry.RatingNumber = null;
                        }


                        result.Add(entry);

                        continue;
                    }
                    catch
                    {
                    }

                } // end of if
                else // other type of tourneys
                {
                    result.Add(entry);
                }
            } // end of foreach
            return result;
        }

    }

    public class ChessTourneyEntryViewModel
    {
     
        public long OrderItemId { get; set; }
        public DateTime Date { get; set; }

        public string FullName { get; set; }

        public string Gender { get; set; }

        public string UscfExpirationDate { get; set; }

        public string UscfId { get; set; }

        public int? RatingNumber { get; set; }

        public string Section { get; set; }

        public string SectionOrder
        {
            get
            {
                switch (Section)
                {
                    case "Open":
                        {
                            return string.Format("A#{0}", Section);
                        }
                    case "U2200":
                        {
                            return string.Format("B#{0}", Section);
                        }
                    case "U2000":
                        {
                            return string.Format("C#{0}", Section);
                        }
                    case "U1800":
                        {
                            return string.Format("D#{0}", Section);
                        }
                    case "U1600":
                        {
                            return string.Format("E#{0}", Section);
                        }
                    case "Unrated":
                        {
                            return string.Format("F#{0}", Section);
                        }
                }

                return string.Format("Z#{0}", Section);
            }
        }

        public string Schedule { get; set; }

        public string Byes { get; set; }

        public int? ProfileId { get; set; }

        public string Options { get; set; }

        public string FideId { get; set; }

        private string _uscfRating;
        public string UscfRating
        {
            get
            {
                return _uscfRating;
            }
            set
            {
                _uscfRating = value != null ? value : string.Empty;

                while (_uscfRating.Length < 7)
                {
                    _uscfRating = "0" + _uscfRating;
                }
            }
        }

        string _fideRating;
        public string FideRating
        {
            get
            {
                return _fideRating;
            }
            set
            {
                _fideRating = value != null ? value : string.Empty;

                while (_fideRating.Length < 7)
                {
                    _fideRating = "0" + _fideRating;
                }
            }
        }

        string _mcRating;
        public string McRating
        {
            get
            {
                return _mcRating;
            }
            set
            {
                _mcRating = value != null ? value : string.Empty;

                while (_mcRating.Length < 7)
                {
                    _mcRating = "0" + _mcRating;
                }
            }
        }

        public string Country { get; set; }

        public JbForm JbForm { get; set; }

        public ChessTourneyEntryViewModel() { }

        // public SportInUserProfile SportProfile { get; set; }

        public ChessTourneyEntryViewModel(ChessTourneyEntryItem item, int clubId, Jumbula.Common.Enums.TimeZone  timeZone)
            : this()
        {
            Date = DateTimeHelper.ConvertUtcDateTimeToLocal(item.Date, timeZone);

            FullName = string.Format("{0}, {1}", item.LastName, item.FirstName);

            Section = item.Section;

            Schedule = item.Schedule;

            Byes = item.Byes;

            Options = item.Options;

            ProfileId = item.ProfileId;

         
        }
    }
}