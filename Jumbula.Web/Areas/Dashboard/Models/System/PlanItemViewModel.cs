﻿using Jumbula.Common.Enums;



namespace SportsClub.Areas.Dashboard.Models
{
    public class PlanItemViewModel// : BaseViewModel<Order, long>
    {
        public int Id { get; set; }
        public string PlanName { get; set; }
        public string MonthlyCharge { get; set; }
        public string TransactionRate { get; set; }
        public string ClubName { get; set; }
        public string CardHolderName { get; set; }
        public string CardNumber { get; set; }

        public string ExpiryYear { get; set; }
        public CurrencyCodes Currency { get; set; }
        public decimal OrderAmount { get; set; }

        public string ExpiryMonth { get; set; }

        public string Brand { get; set; }
        public string Last4Digit { get; set; }
        public string Method => string.Format("{0} ***{1}", Brand, Last4Digit);

        public string ExpiryDate => string.Format("{0}/{1}", ExpiryMonth, ExpiryYear);
        public bool IsDefault { get; set; }
        public string Address { get; set; }
        public long ClientId { get; set; }
    }
}