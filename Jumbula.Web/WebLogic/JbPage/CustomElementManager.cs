﻿using Jb.Framework.Common.Forms;
using System.Collections.Generic;

namespace Jumbula.Web.WebLogic.JbPage
{
    public class CustomElementManager : ICustomElementManager
    {
        private static List<ICustomElement> _elements;

        public CustomElementManager()
        {
            _elements = new List<ICustomElement>();
            AddCustomeElement(new USCFInfo());
        }

        public List<ICustomElement> GetAllCustomeElement()
        {
            return _elements;
        }

        public void AddCustomeElement(ICustomElement customeElement)
        {
            if (_elements.IndexOf(customeElement) < 0)
                _elements.Add(customeElement);
        }
    }
}
