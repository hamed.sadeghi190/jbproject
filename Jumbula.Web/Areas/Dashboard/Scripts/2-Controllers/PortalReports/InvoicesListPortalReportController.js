﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('InvoicesListPortalReportController', ['$scope', '$http', '$log', '$stateParams', '$state', 'customReportService', 'jbToast', 'cfpLoadingBar', 'reminderInvoicesService', function ($scope, $http, $log, $stateParams, $state, customReportService, jbToast, cfpLoadingBar, reminderInvoicesService) {
        window.app.globalObjects.pageClass = 'county-providers-view';
        window.app.globalObjects.initializeView();


        $scope.stateParams = $stateParams;

        $scope.MC.stateParams = $stateParams;
        $scope.seasonDomain = $scope.stateParams.seasonDomain;

        angular.forEach($scope.MC.seasons, function (value, key) {
            if (value.Domain == $scope.stateParams.seasonDomain)
                $scope.MC.currentSeason = value;
        });

        var baseUrl = window.location.origin + '/Dashboard/PortalReport';

        var model = {};
        var dataInfo = {};


        $scope.Model = customReportService.getModelFromStrorage();

        $scope.filter = {};
        $scope.filter.printMode = false;
        $scope.filter.InvoiceType = "0";

        $http.get(baseUrl + '/GetReportHeader').success(function (data, status, headers, config) {

            $scope.ClubName = data.ClubName;
            $scope.Date = data.Date;
            dataInfo = data;
            $scope.InvoiceTypes = data.InvoiceTypes;
            $scope.filter.InvoiceType = 'All';

            console.log($scope.filter.InvoiceType)

        }).error(function (data, status, headers, config) {
            jbToast.error("error " + data);
        });

        var model = $scope.Model;

      
        $scope.saveToExcel = function () {
            cfpLoadingBar.start();
            $("#InvoicesListGrid").getKendoGrid().saveAsExcel();
            cfpLoadingBar.complete();
        }

        $scope.Model.checkedIds = {};
        $scope.InvoicesListDataSource = new kendo.data.DataSource({
            transport:
                {
                    read: {
                        url: baseUrl + '/GetInvoiceListReportInfo',
                        type: "Post",
                        data: function () {
                            return {
                                model: model,
                                InvoiceType: $scope.filter.InvoiceType,
                            };
                        },
                    },

                },

            schema: {
                total: "TotalCount",
                data: "DataSource",
            },
            serverSorting: false,
            serverPaging: true,
            pageSize: 50,
            resizable: true,
        });

        $scope.InvoicesListGridOptions = {
            excel: {
                allPages: true,
                fileName: "InvoiceList.xlsx",
                proxyURL: "/Dashboard/PortalReport/Save"
            },
            excelExport: function (e) {

                var rows = e.workbook.sheets[0].rows;
                rows.unshift({
                    cells: [{
                        value: ""
                    }]
                });
                rows.unshift({
                    cells: [{
                        value: dataInfo.Date
                    }]
                });
                rows.unshift({
                    cells: [{
                        value: "Invoice list - " + dataInfo.ClubName
                    }]
                });

                cfpLoadingBar.complete();
            },
            dataSource: $scope.InvoicesListDataSource,
            resizable: true,
            serverPaging: true,
            scrollable: true,
            pageable: {
                refresh: true,
                pageSizes:
                [
                    5, 10, 20, 30, 50, 100, 200
                ],
                buttonCount: 5,
            },
            dataBound: function (e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if ($scope.Model.checkedIds[view[i].Id]) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected")
                        .find(".checkbox")
                        .attr("checked", "checked");
                    }
                }
            },
            sortable: { mode: 'single' },
            columns: [

                {
                    template: "<input type='checkbox' class='checkbox' />",
                    width: "30px",
                    headerTemplate: '<input type="checkbox" class="checkAll" />'
                },
                 {
                     field: "ClubName",
                     title: "School",
                     width: "190px"
                 }, {
                     field: "InvoiceNumber",
                     title: "Invoice #",
                     width: "120px"
                 }, {
                     field: "Recipient",
                     title: "Recipient",
                     width: "160px"
                 },
                 {
                     field: "Status",
                     title: "Status",
                     template: '<div style="color:black;"> #=Status # </div><div> #if(data.Status=="Unpaid"){# Due #= data.StrDueDate# #}# </div>',
                     labels: {
                         visible: true,
                     },
                     width: "150px",
                 },
                {
                    field: "StrAmount",
                    title: "Amount",
                    width: "120px"
                },
            ]
        }


        $(document.body).on('click', '#InvoicesListGrid table .checkbox', selectRow);
        $(document.body).on('click', '#InvoicesListGrid table .checkAll', checkAll);

        function selectRow() {
            var checked = this.checked,
            row = $(this).closest("tr"),
            grid = $("#InvoicesListGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);
            $scope.Model.checkedIds[dataItem.Id] = checked;

            if (checked) {
                //-select the row
                row.addClass("k-state-selected");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
            }
        }

        function checkAll() {
            var grid = $("#InvoicesListGrid").data("kendoGrid");
            var state = $("#InvoicesListGrid table > thead > tr > th:nth-child(1) > input").prop("checked");
            var ckbox;
            grid.tbody.find("tr").each(function () {
                var $this = $(this);
                ckbox = $this.find("td .checkbox");
                ckbox.prop("checked", state);
                var dataItem = grid.dataItem($this);
                $scope.Model.checkedIds[dataItem.Id] = state;
                if (state) {
                    //-select the row
                    $this.addClass("k-state-selected");
                } else {
                    //-remove selection
                    $this.removeClass("k-state-selected");
                }
            });
        }


        $scope.search = function () {
            $("#InvoicesListGrid").data("kendoGrid").dataSource.read();
        };
        $scope.clearFilters = function () {

            $scope.filter.InvoiceType = 'All';

            $("#InvoicesListGrid").data("kendoGrid").dataSource.read();
        };

        $scope.SendReminder = function () {
            var checkedInvoice = [];
            for (var i in $scope.Model.checkedIds) {
                if ($scope.Model.checkedIds[i]) {
                    checkedInvoice.push(i);
                }
            }
            if (checkedInvoice.length > 0) {

                reminderInvoicesService.set($scope.Model);
              
                $state.go('ReminderInvoicesFromReports', 'InvoicesPortalReports');
            } else {
                jbToast.error("Please select at least one invoice.");
            }
        };
    }
    ]);

})();