﻿using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using System.Collections.Generic;
using System.Linq;

namespace Jumbula.Core.Model.Email
{
    public class AutoChargeSuccessEmailBodyModel
    {
        public AutoChargeSuccessEmailBodyModel()
        {
            var url = System.Web.HttpContext.Current.Request.Url;
            RootSiteUrl = string.Format(Constants.F_HttpsSchemeAuthority, url.Authority);
        }

        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public string ClubImageUrl { get; set; }
        public string ClubName { get; set; }
        public string TransactionId { get; set; }
        public string ClubEmail { get; set; }
        public string ClubPhone { get; set; }
        public CurrencyCodes ClubCurrency { get; set; }
        public string InstallmentDate { get; set; }
        public string RootSiteUrl { get; set; }
        public string BodyMailTitle { get; set; }

        public string PlayerFullName { get; set; }

        public string TotalAmount => CurrencyHelper.FormatCurrencyWithPenny(ReservationItems?.Sum(c => c.Price) ?? 0, ClubCurrency);

        public List<AutoChargeEmailBodyItemModel> ReservationItems { get; set; }

        public string LastDigits { get; set; }

        public bool IsAdminMode { get; set; }

        public bool IsPreviewMode { get; set; }
    }
}
