
IF OBJECT_ID('[dbo].[EventSchedules]') IS NULL
BEGIN
	CREATE TABLE [dbo].[EventSchedules](
	[Id] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[EventScheduleAttributes] [nvarchar](max) NULL)
END
GO

--<< PRIMARYKEY DEFINITION >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_dbo.EventSchedules')
BEGIN
	ALTER TABLE [dbo].[EventSchedules] ADD CONSTRAINT [PK_dbo.EventSchedules] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	) 
END
GO

--<< ADD FOREIGN KEYS >>--
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE name='FK_dbo.EventSchedules_dbo.Events_Id')
BEGIN
	ALTER TABLE [dbo].[EventSchedules]  WITH CHECK ADD  CONSTRAINT [FK_dbo.EventSchedules_dbo.Events_Id] FOREIGN KEY([Id])
	REFERENCES [dbo].[Events] ([Id])
END
GO



