﻿using System.Collections.Generic;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class OrderItemDetailsElements : BaseElements
    {
        public OrderItemDetailsElements(IWebDriver driver)
            :base(driver)
        {

        }
        #region Actions
        public IWebElement EditCancelIcon
        {
            get
            {
                return Driver.FindElement(By.Id("EditCancelDropIn"));
            }
        }
        #endregion Actions
        #region Hearder
        #endregion Hearder
        #region Main Info
        public IWebElement Balance
        {
            get
            {
                return Driver.FindElement(By.XPath("//b[contains(text(),'Balance')]/following-sibling::i"));
            }
        }
        #endregion Main Info
        #region Charge/discounts
        public IWebElement Chargediscount(int row, string column)
        {
            switch(column)
            {
                case "Item":
                    return Driver.FindElement(By.XPath(string.Format("//*[@id='registrationDetails']//tbody/tr[{0}]/td[1]", row)));
                case "Amount":
                    return Driver.FindElement(By.XPath(string.Format("//*[@id='registrationDetails']//tbody/tr[{0}]/td[2]", row)));
                default:
                    return Driver.FindElement(By.XPath(string.Format("//*[@id='registrationDetails']//tbody/tr[{0}]/td[1]", row)));
            }
        }
        public IWebElement Total
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='registrationDetails']//tfoot/tr[1]/td[2]"));
            }
        }
        public IWebElement PaidAmount
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='registrationDetails']//tfoot/tr[2]/td[2]"));
            }
        }
        #endregion Charge/discounts
        #region Drop in sessions
        public IReadOnlyCollection<IWebElement> Sessions
        {
            get
            {
                return Driver.FindElements(By.XPath("//div[@id='SessionsItem']//label"));
                
            }
        }
        
        #endregion Drop in sessions
    }
}
