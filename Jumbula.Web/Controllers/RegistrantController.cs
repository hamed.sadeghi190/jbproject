﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuthorizeNet.Api.Contracts.V1;
using Jb.Framework.Common.Forms;
using Jumbula.Common.Constants;
using Jumbula.Common.Enums;
using Jumbula.Common.Helper;
using Jumbula.Common.Utilities;
using Jumbula.Core.Business;
using Jumbula.Core.Data;
using Jumbula.Core.Domain;
using Jumbula.Core.Identity;
using Jumbula.Core.Model.CreditCard;
using Jumbula.Core.Model.Order;
using Jumbula.Web.Infrastructure;
using Jumbula.Web.Mvc.Attributes;
using Jumbula.Web.Mvc.Controllers;
using Jumbula.Web.Mvc.ModelState;
using Jumbula.Web.Mvc.UIHelpers;
using Jumbula.Web.Mvc.WebConfig;
using Jumbula.Web.WebLogic.ClubsService;
using Jumbula.Web.WebLogic.Email;
using Jumbula.Web.WebLogic.Payment;
using Jumbula.Web.WebLogic.Register;
using Jumbula.Web.WebLogic.UserService;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SportsClub.Models;
using SportsClub.Models.Register;
using SportsClub.Models.Registrant;
using Stripe;
using Constants = Jumbula.Common.Constants.Constants;

namespace Jumbula.Web.Controllers
{


    public partial class RegistrantController : ParentBaseController
    {

        private readonly IProgramBusiness _programBusiness;
        private readonly IPlayerProfileBusiness _playerProfileBusiness;
        private readonly ICartBusiness _cartBusiness;
        private readonly IAuditBusiness _auditBusiness;
        private readonly IOrderInstallmentBusiness _orderInstallmentBusiness;
        private readonly IOrderItemBusiness _orderItemBusiness;
        private readonly IApplicationUserManager<JbUser, int> _applicationUserManager;
        private readonly ITransactionActivityBusiness _transactionActivityBusiness;

        public RegistrantController(IApplicationUserManager<JbUser, int> applicationUserManager, IAuditBusiness auditBusiness
            , ICartBusiness cartBusiness, IPlayerProfileBusiness playerProfileBusiness,
            IOrderInstallmentBusiness orderInstallmentBusiness, IOrderItemBusiness orderItemBusiness,
            ITransactionActivityBusiness transactionActivityBusiness,
           IProgramBusiness programBusiness)
        {
            _applicationUserManager = applicationUserManager;
            _auditBusiness = auditBusiness;
            _playerProfileBusiness = playerProfileBusiness;
            _cartBusiness = cartBusiness;
            _orderItemBusiness = orderItemBusiness;
            _orderInstallmentBusiness = orderInstallmentBusiness;
            _transactionActivityBusiness = transactionActivityBusiness;
            _programBusiness = programBusiness;
        }

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        public virtual ActionResult Dashboard()
        {
            return View();
        }

        public virtual ActionResult Invoices()
        {
            return View();
        }

        public virtual ActionResult WaitList()
        {
            return View();
        }

        public virtual ActionResult Lottery()
        {
            return View();
        }

        public virtual ActionResult Waiver(int id)
        {
            var orderItemWaiver = Ioc.OrderItemWaiverBusiness.Get(id);
            CheckResourceForAccess(orderItemWaiver.OrderItem);

            var model = new ParentWaiverViewModel();

            model = new ParentWaiverViewModel()
            {
                Text = orderItemWaiver.Text,
                Name = orderItemWaiver.Name,
                WaiverId = orderItemWaiver.Id,
                Date = DateTimeHelper.ConvertUtcDateTimeToLocal(orderItemWaiver.OrderItem.Order.CompleteDate, orderItemWaiver.OrderItem.ProgramSchedule.Program.Club.TimeZone),
                ProgramName = orderItemWaiver.OrderItem.ProgramSchedule.Program.Name,
                Signiture = orderItemWaiver.Signature,
                Agreed = orderItemWaiver.Agreed,
                WaiverConfirmationType = orderItemWaiver.WaiverConfirmationType
            };
            return View(model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public virtual JsonResult GetOrders()
        {
            var userId = CurrentUserId;
            var userOrders = Ioc.OrderBusiness.GetUserOrders(userId);

            var itemList = new List<object>();
            foreach (var item in userOrders.OrderBy(c => c.Id))
            {
                itemList.Add(new
                {
                    Id = item.Id,
                    PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(item.PaidAmount, item.Club.Currency),
                    OrderAmount = CurrencyHelper.FormatCurrencyWithPenny(item.OrderAmount, item.Club.Currency),
                    UserId = item.UserId,
                    Date = item.CompleteDate.ToString("MMM dd, yyyy"),
                    DateTime = item.CompleteDate,
                    Confirmation = item.ConfirmationId
                });
            }

            return Json(new { DataSource = itemList }, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public virtual JsonResult GetInvoices()
        {
            var userId = CurrentUserId;
            var paymentDetails = Ioc.PaymentDetailBusiness;
            var transactionActivity = Ioc.TransactionActivityBusiness;
            var userInvoices = Ioc.InvoiceBusiness.GetInvoicesUser(userId);

            var itemList = new List<object>();

            var invoicePaymentDetails = paymentDetails.GetByInvoiceIds(userInvoices.Select(s => s.Id).ToList());

            foreach (var invoice in userInvoices.OrderBy(c => c.Id))
            {
                var paymentDetailId = invoicePaymentDetails.Single(s => s.InvoiceId == invoice.Id).Id;
                var token = transactionActivity.GetTransactions(paymentDetailId).Token;

                var dueDateValue = invoice.DueDate;
                var invoiceDate = invoice.InvoicingDate;
                DateTime duedate;
                if (dueDateValue > 0)
                {
                    duedate = invoiceDate.AddDays(dueDateValue.Value);
                }
                else
                {
                    duedate = invoiceDate;
                }

                itemList.Add(new
                {
                    Id = invoice.Id,
                    PaidAmount = invoice.PaidAmount,
                    InvoiceNumber = invoice.InvoiceNumber,
                    UserId = invoice.UserId,
                    InvoicingDate = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), invoice.InvoicingDate).ToString("MMM dd, yyyy, h:mm tt"),
                    DateTime = invoice.InvoicingDate,
                    DueDateValue = invoice.DueDate,
                    DueDate = duedate.ToString("MMM dd, yyyy"),
                    Amount = invoice.Amount,
                    Token = token,
                    Status = invoice.Status.ToDescription(),
                    ActionColler = JumbulaSubSystem.Invoice,
                });
            }

            return Json(new { DataSource = itemList }, JsonRequestBehavior.AllowGet);
        }

        [JbAudit(JbAction.Family_Add_InvoiceToCart, "Invoice was added to cart")]
        public ActionResult AddInvoiceToCart(int invoiceId)
        {
            Status = true;
            return RedirectToAction("Index", "Cart", new { invoiceId = invoiceId, actionCaller = JumbulaSubSystem.Invoice });
        }

        public virtual ActionResult Payments(long? id, PaymentPlanType? paymentType, bool? status)
        {
            //Get Calendar data
            var result = _orderItemBusiness.GetParentPayments(id, paymentType, status, CurrentUserId);

            return View(result);
        }

        [HttpGet]
        public ActionResult ConfirmPaymentAmount(long id, PaymentPlanType paymentType, string returnUrl)
        {
            var result = _orderItemBusiness.ConfirmPaymentAmount(id, paymentType, returnUrl);

            return View(result);
        }

        [JbAudit(JbAction.FamilyAddItemToCart, "Item was added to cart by Parent")]
        public ActionResult AddItemToCart(ConfirmPaymentAmountViewModel model)
        {
            CheckPaymentAmountValidation(model);

            if (!ModelState.IsValid) return PartialView("ConfirmPaymentAmount", model);

            var result = _orderItemBusiness.AddItemToCart(model);

            return JsonNet(new { Status = result, model.Token, Amount = model.PayableAmount, ItemType = model.PaymentType.ToString(), ReturnUrl = model.EncodedReturnUrl });
        }

        private void CheckPaymentAmountValidation(ConfirmPaymentAmountViewModel model)
        {
            switch (model.AmountPayType)
            {
                case AmountPayType.FullPay:
                    ModelState.Remove(nameof(model.PayableAmount));
                    break;
                case AmountPayType.PartiallyPay:

                    var errors = _orderItemBusiness.CheckPayableAmountValidation(model.PayableAmount, model.Balance);

                    if (!errors.Item1)
                    {
                        ModelState.AddModelError(nameof(model.PayableAmount), errors.Item2);
                    }

                    break;
            }
        }

        [Authorize(Roles = "Admin, Parent, Manager")]
        public virtual ActionResult GetCalendarItems([DataSourceRequest]DataSourceRequest request, long userId)
        {
            var result = _orderItemBusiness.GetUserPaymentItems(CurrentUserId, this.GetCurrentClubId());

            return JsonNet(result.ToDataSourceResult(request));
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public virtual JsonResult GetWaitList()
        {
            var userId = CurrentUserId;

            var userWaitList = Ioc.WaitListBusiness.GetListByUser(userId);

            var itemList = new List<object>();

            foreach (var waitlist in userWaitList.OrderBy(c => c.Id))
            {

                itemList.Add(new
                {
                    Id = waitlist.Id,

                    Participant = string.Concat(waitlist.FirstName, " ", waitlist.LastName),
                    UserId = waitlist.UserId,
                    Grade = waitlist.Grade.ToDescription(),
                    Date = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), waitlist.Date).ToString("MMM dd, yyyy, h:mm tt"),
                    ProgramName = waitlist.Program.Name,
                    EventDomain = Url.EventDomain(waitlist.Program.Season.Domain, waitlist.Program.ClubDomain, waitlist.Program.Domain, true),
                });
            }

            return Json(new { DataSource = itemList }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Delete_Waitlist)]
        public virtual ActionResult DeleteWaitlist(int id)
        {
            var waitlist = Ioc.WaitListBusiness.GetById(id);

            SetActivityDescription("{0} {1} {2}", waitlist.FirstName, waitlist.LastName, "was deleted from waitlist");

            var program = Ioc.ProgramBusiness.Get(waitlist.ProgramId);

            EmailService emailService = EmailService.Get(this.ControllerContext);
            var club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());
            var result = Ioc.WaitListBusiness.Delete(id);

            Status = result.Status;


            var contactEmail = string.Empty;
            var clubDomain = string.Empty;

            if (club.PartnerId.HasValue)
            {
                contactEmail = club.PartnerClub.ContactPersons.First().Email;
                clubDomain = club.PartnerClub.Domain;
            }
            else
            {
                clubDomain = club.Domain;
                contactEmail = club.ContactPersons.First().Email;
            }

            if (result.Status)
            {
                var datetime = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), waitlist.Date).ToString("MMM dd, yyyy, h:mm tt");
                emailService.DeleteWaitlistForAdmin(datetime, waitlist.FirstName + " " + waitlist.LastName, waitlist.Grade.ToDescription(), this.GetCurrentUserName(), program.Name, club, club.Site, contactEmail, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo);
                emailService.DeleteWaitlistForParent(datetime, waitlist.FirstName + " " + waitlist.LastName, waitlist.Grade.ToDescription(), this.GetCurrentUserName(), program.Name, club, club.Site, contactEmail, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo);

            }
            return JsonNet(new { Status = result.Status, Message = result.Message, RecordsAffected = result.RecordsAffected });
        }

        public virtual ActionResult InvoiceDetail(int invoiceId, bool? status)
        {
            var orderItems = Ioc.OrderItemBusiness;
            var allIstallments = Ioc.InstallmentBusiness;
            var invoice = Ioc.InvoiceBusiness.Get(invoiceId);
            var club = invoice.Club;

            var clubLogo = "";
            var partner = club.PartnerClub;
            bool hasPartner = partner != null ? true : false;
            var clubInfo = Ioc.ClubBusiness.GetInvoiceSetting(club);
            if (clubInfo.logo)
            {
                clubLogo = UrlHelpers.GetClubLogoUrl(partner.Domain, partner.Logo);
            }
            else
            {
                clubLogo = UrlHelpers.GetClubLogoUrl(club.Domain, club.Logo);
            }


            var paymantDetailId = Ioc.PaymentDetailBusiness.GetByInvoiceId(invoiceId).Id;
            var transactions = Ioc.TransactionActivityBusiness.GetList().Where(p => p.PaymentDetailId == paymantDetailId);
            var totalAmount = transactions.Sum(o => (decimal?)o.Amount) ?? 0;

            var userId = invoice.UserId;
            var userName = Ioc.UserProfileBusiness.Get(userId).UserName;
            var dueDateValue = invoice.DueDate;
            var invoceDate = invoice.InvoicingDate;
            DateTime duedate;

            if (dueDateValue > 0)
            {
                duedate = invoceDate.AddDays(dueDateValue.Value);
            }
            else
            {
                duedate = invoceDate;
            }
            var infoModel = new ParentInvoiceInfoViewModel()
            {
                InvoiceNumber = invoice.InvoiceNumber,
                invoiceId = invoice.Id,
                Refrense = invoice.Reference,
                EmailCc = invoice.EmailToCc,
                DueDate = duedate,
                UserEmail = userName,
                ClubSite = clubInfo.Site,
                ContactClubEmail = clubInfo.Email,
                ClubAddress = clubInfo.Address,
                ClubUrlLogo = clubLogo,
                ClubName = clubInfo.Name,
                NoteToRecipient = invoice.NotToRecipient,
                StatusInvoice = invoice.Status.ToDescription(),
                TotalAmount = invoice.Amount,
                AmountPaid = invoice.PaidAmount > 0 ? -(invoice.PaidAmount) : invoice.PaidAmount,
                AmountDue = invoice.Amount - invoice.PaidAmount,
                MemberName = club.Name,
                HasPartner = hasPartner
            };
            decimal orderbalance;
            decimal orderItemInstBalanse;
            decimal orderInvoiceAmount = 0;

            var invoiceItemModel = new ParentInvoiceItemViewModel();
            invoiceItemModel.Orders = new List<ParentInvoiceOrderViewModel>();
            invoiceItemModel.OrderItems = new List<ParentInvoiceOrderItemViewModel>();
            invoiceItemModel.Instalments = new List<ParentInvoiceInstallmentViewModel>();

            var ordersGroupBy = transactions.Select(o => o.Order).GroupBy(i => i.Id).ToList();


            var participantName = Ioc.PlayerProfileBusiness.GetList().Where(p => p.UserId == userId).First().Contact.FullName;


            foreach (var orderGroup in ordersGroupBy)
            {
                var orderFirst = orderGroup.First();
                var orderItemsForGourp = orderItems.GetCompletetCancelOrderItems(orderFirst.Id);

                var orderItemsGroupBy = transactions.Select(o => o.OrderItem).Where(or => or.Order_Id == orderFirst.Id).GroupBy(oi => oi.Id).ToList();

                foreach (var orderItemByGroup in orderItemsGroupBy)
                {

                    var orderItemGroupFirst = orderItemByGroup.First();
                    var allOrderItemInstallments = allIstallments.GetListByOrderItemId(orderItemGroupFirst.Id).Where(i => !i.IsDeleted);
                    var orderItemInstallments = transactions.Select(inst => inst.Installment).Where(i => i.OrderItemId == orderItemGroupFirst.Id);


                    if (orderItemGroupFirst.ProgramScheduleId.HasValue)
                    {

                        if (orderItemInstallments != null && orderItemInstallments.Any())
                        {
                            var orderItemInstpaidAmounts = allOrderItemInstallments.Sum(p => (decimal?)p.PaidAmount) ?? 0;

                            var orderItemInstTotalAmounts = allOrderItemInstallments.Sum(a => (decimal?)a.Amount) ?? 0;

                            orderItemInstBalanse = (orderItemInstTotalAmounts - orderItemInstpaidAmounts);

                            decimal orderItemInvoiceAmount = 0;
                            orderItemInvoiceAmount = orderItemInstallments.Sum(t => t.Amount);

                            foreach (var installment in orderItemInstallments)
                            {
                                var paidAmount = installment.PaidAmount != null ? installment.PaidAmount : 0;
                                invoiceItemModel.Instalments.Add(new ParentInvoiceInstallmentViewModel
                                {

                                    DueDate = installment.InstallmentDate,
                                    Amount = installment.Amount,
                                    PaidAmount = paidAmount,
                                    id = installment.Id,
                                    LastDueDate = installment.PaidDate,
                                    InvoiceAmount = (installment.Amount - paidAmount).Value,
                                    OrderItemId = installment.OrderItemId,

                                });

                            }

                            invoiceItemModel.OrderItems.Add(new ParentInvoiceOrderItemViewModel
                            {
                                Attendee = orderItemGroupFirst.FirstName + " " + orderItemGroupFirst.LastName,
                                EntryFee = orderItemGroupFirst.TotalAmount,
                                PaidAmount = orderItemGroupFirst.PaidAmount,
                                Balance = orderItemInstBalanse > 0 ? orderItemInstBalanse : orderItemInstBalanse <= 0 ? orderItemInstBalanse * (-1) : 0,
                                Id = orderItemGroupFirst.Id,
                                EntryFeeName = orderItemGroupFirst.EntryFeeName,
                                PaymentplanType = orderItemGroupFirst.PaymentPlanType,
                                InvoiceAmount = orderItemInvoiceAmount,
                                ProgramName = orderItemGroupFirst.ProgramTypeCategory != ProgramTypeCategory.Camp ? _programBusiness.GetProgramNameOnlyTuition(orderItemGroupFirst.ProgramSchedule, orderItemGroupFirst.EntryFeeName) : _programBusiness.GetProgramName(orderItemGroupFirst.ProgramSchedule, orderItemGroupFirst.EntryFeeName),
                                ProgramScheduleId = orderItemGroupFirst.ProgramScheduleId.Value,
                                OrderId = orderItemGroupFirst.Order_Id,
                            });

                            orderInvoiceAmount += orderItemInvoiceAmount;

                        }
                        else
                        {
                            var orderItemBalance = orderItemGroupFirst.TotalAmount - orderItemGroupFirst.PaidAmount;
                            invoiceItemModel.OrderItems.Add(new ParentInvoiceOrderItemViewModel
                            {
                                Attendee = orderItemGroupFirst.FirstName + " " + orderItemGroupFirst.LastName,
                                EntryFee = orderItemGroupFirst.TotalAmount,
                                PaidAmount = orderItemGroupFirst.PaidAmount,
                                Balance = orderItemBalance > 0 ? orderItemBalance : orderItemBalance <= 0 ? orderItemBalance * (-1) : 0,
                                Id = orderItemGroupFirst.Id,
                                EntryFeeName = orderItemGroupFirst.EntryFeeName,
                                PaymentplanType = orderItemGroupFirst.PaymentPlanType,
                                InvoiceAmount = orderItemBalance > 0 ? orderItemBalance : orderItemBalance <= 0 ? orderItemBalance * (-1) : 0,
                                ProgramName = orderItemGroupFirst.ProgramTypeCategory != ProgramTypeCategory.Camp ? _programBusiness.GetProgramNameOnlyTuition(orderItemGroupFirst.ProgramSchedule, orderItemGroupFirst.EntryFeeName) : _programBusiness.GetProgramName(orderItemGroupFirst.ProgramSchedule, orderItemGroupFirst.EntryFeeName),
                                ProgramScheduleId = orderItemGroupFirst.ProgramScheduleId.Value,
                                OrderId = orderItemGroupFirst.Order_Id,

                            });

                            orderInvoiceAmount += orderItemGroupFirst.TotalAmount - orderItemGroupFirst.PaidAmount;
                        }

                    }
                    else
                    {
                        var orderItemBalance = orderItemGroupFirst.TotalAmount - orderItemGroupFirst.PaidAmount;
                        invoiceItemModel.OrderItems.Add(new ParentInvoiceOrderItemViewModel
                        {
                            Attendee = participantName,
                            EntryFee = orderItemGroupFirst.TotalAmount,
                            PaidAmount = orderItemGroupFirst.PaidAmount,
                            Balance = orderItemBalance > 0 ? orderItemBalance : orderItemBalance <= 0 ? orderItemBalance * (-1) : 0,
                            Id = orderItemGroupFirst.Id,
                            EntryFeeName = orderItemGroupFirst.EntryFeeName,
                            PaymentplanType = orderItemGroupFirst.PaymentPlanType,
                            InvoiceAmount = orderItemBalance > 0 ? orderItemBalance : orderItemBalance <= 0 ? orderItemBalance * (-1) : 0,
                            ProgramName = orderItemGroupFirst.Name,
                            OrderId = orderItemGroupFirst.Order_Id,

                        });
                        orderInvoiceAmount += orderItemGroupFirst.TotalAmount;
                    }

                }
                var orderPaidAmounts = orderItemsForGourp.Where(o => o.ItemStatusReason != OrderItemStatusReasons.canceled || ((o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || o.ProgramTypeCategory == ProgramTypeCategory.Subscription) && o.ItemStatusReason == OrderItemStatusReasons.canceled)).Sum(p => (decimal?)p.PaidAmount) ?? 0;
                var orderTotalAmounts = orderItemsForGourp.Where(o => o.ItemStatusReason != OrderItemStatusReasons.canceled || ((o.ProgramTypeCategory == ProgramTypeCategory.BeforeAfterCare || o.ProgramTypeCategory == ProgramTypeCategory.Subscription) && o.ItemStatusReason == OrderItemStatusReasons.canceled)).Sum(a => (decimal?)a.TotalAmount) ?? 0;

                //-----------Get amount for cancelation orderItem-------------
                var cancelOrderItems = orderItemsForGourp.Where(o => o.ItemStatusReason == OrderItemStatusReasons.canceled && o.ProgramTypeCategory != ProgramTypeCategory.Subscription);
                decimal cancelOrderItemTotalAmount = 0;
                decimal cancelOrderItemPaidAmount = 0;
                if (cancelOrderItems != null && cancelOrderItems.Any())
                {
                    cancelOrderItemTotalAmount = 0;
                    cancelOrderItemPaidAmount = cancelOrderItems.Where(o => o.ItemStatusReason == OrderItemStatusReasons.canceled).Sum(p => (decimal?)p.PaidAmount) ?? 0;
                    foreach (var cancelItem in cancelOrderItems)
                    {
                        var cancelOredrItem = cancelItem.GetOrderChargeDiscounts().Where(c => c.Category == ChargeDiscountCategory.CancellationFee);
                        decimal cncelOredrItemAmount = 0;

                        if (cancelOredrItem.Any())
                        {
                            cncelOredrItemAmount = cancelOredrItem.OrderBy(c => c.Id).ToList().LastOrDefault().Amount;
                        }

                        cancelOrderItemTotalAmount = cancelOrderItemTotalAmount + cncelOredrItemAmount;
                    }
                }
                var cancelItemBalance = cancelOrderItemTotalAmount - cancelOrderItemPaidAmount;

                //end

                orderbalance = (orderTotalAmounts - orderPaidAmounts) + cancelItemBalance;

                invoiceItemModel.Orders.Add(new ParentInvoiceOrderViewModel()
                {
                    OrderDate = Ioc.ClubBusiness.GetClubDateTime(this.GetCurrentClubId(), orderFirst.CompleteDate),
                    ConfirmationId = orderFirst.ConfirmationId,
                    OrderAmount = orderTotalAmounts,
                    Balance = orderbalance > 0 ? orderbalance : orderbalance <= 0 ? orderbalance * (-1) : 0,
                    PaidAmount = orderPaidAmounts,
                    Id = orderFirst.Id,
                    InvoiceAmount = orderInvoiceAmount > 0 ? orderInvoiceAmount : orderInvoiceAmount < 0 ? orderInvoiceAmount * (-1) : 0,

                });
                orderInvoiceAmount = 0;
            }

            var model = new ParentInvoiceViewModel()
            {
                InfoModel = infoModel,
                InvoiceItemModel = invoiceItemModel,
                Status = status
            };

            return View(model);
        }

        [HttpGet]
        public virtual ActionResult InvoiceExportPdf(int invoiceId)
        {
            dynamic model = new System.Dynamic.ExpandoObject();
            model = InvoiceDetail(invoiceId, false);

            return Pdf("ParentInvoiceDetailReport", "_InvoiceDetailReportPdfExportView", model.Model);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public virtual ActionResult GetOrderItems(long orderId)
        {
            var query = Ioc.OrderItemBusiness.GetOrderItems(orderId: orderId, userId: CurrentUserId, isTestMode: false).ToList();
            var model = new List<RegistrantOrderItemViewModel>();

            foreach (var item in query)
            {
                model.Add(new RegistrantOrderItemViewModel()
                {
                    Id = item.Id,
                    AttendeeName = item.FullName,
                    OrderAmount = CurrencyHelper.FormatCurrencyWithPenny(item.TotalAmount, item.Order.Club.Currency),
                    PaidAmount = CurrencyHelper.FormatCurrencyWithPenny(item.PaidAmount, item.Order.Club.Currency),
                    ItemStatusReasons = item.ItemStatusReason,
                    ProegramName = item.ProgramScheduleId.HasValue ? item.ProgramSchedule.Program.Name : item.EntryFeeName,
                    EventDomain = item.ProgramScheduleId.HasValue ? Url.EventDomain(item.Season.Domain, item.ProgramSchedule.Program.ClubDomain, item.ProgramSchedule.Program.Domain, true) : null,
                    IsDonation = item.ProgramScheduleId.HasValue ? false : true
                });

            }

            return JsonNet(new { DataSource = model });
        }

        [HttpGet]
        public virtual ActionResult EditOrderDetail(int orderItemId)
        {
            var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            orderItem.JbForm.CurrentMode = AccessMode.Edit;
            foreach (var section in orderItem.JbForm.Elements)
            {
                section.CurrentMode = AccessMode.Edit;
                foreach (var element in (section as JbSection).Elements)
                {
                    element.CurrentMode = AccessMode.Edit;
                }
            }

            DateTime date;

            string dobStr = string.Empty;

            if (orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(m => m.Name.ToLower() == ElementsName.DoB.ToString()))
            {
                dobStr = orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Single(m => m.Name.ToLower() == ElementsName.DoB.ToString().ToLower()).GetValue().ToString();


                if (DateTime.TryParse(dobStr, out date))
                {
                    dobStr = date.ToShortDateString();
                    orderItem.JbForm.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Single(m => m.Name.ToLower() == ElementsName.DoB.ToString().ToLower()).SetValue(dobStr);
                }
            }
            return View(orderItem.JbForm);

        }

        [HttpPost]
        [ValidateInput(false)]
        [JbAudit(JbAction.Family_Change_RegistrationForm, "Registratin form was updated")]
        public virtual ActionResult EditOrderDetail(JbForm model, int orderItemId)
        {
            var registerService = RegisterService.Get();

            var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            if (ModelState.IsValid)
            {
                string dobStr = string.Empty;
                if (model.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Any(m => m.Name.ToLower() == ElementsName.DoB.ToString().ToLower()))
                {
                    dobStr = model.GetElement<JbSection>(SectionsName.ParticipantSection.ToString()).Elements.Single(m => m.Name.ToLower() == ElementsName.DoB.ToString().ToLower()).GetValue().ToString();
                }

                if (!DateTimeHelper.IsValidDateFormat(dobStr))
                {
                    ViewBag.ErrorMessage = "Date of birth invalid";
                    return View(model);
                }

                var res = Ioc.JbFormBusiness.CreateEdit(model);

                Status = res.Status;

                var profileId = orderItem.PlayerId;

                var playerProfile = orderItem.Player;

                var errorParent = registerService.SetFormToParentDashboard(model, profileId, orderItem.Order.UserId, ref playerProfile);

                if (!errorParent.Status)
                {
                    ManipulateParentDashboardValidations(model, errorParent.Message);
                }

                if (ModelState.IsValid)
                {
                    if (res.Status)
                    {
                        ViewBag.OkMessage = "Your information has been updated successfully.";

                        Ioc.OrderHistoryBusiness.AddOrderHistory(OrderAction.EditRegistrationForm, "Registration form is edited by parent.", orderItem.Order_Id, User.Identity.GetCurrentUserId(), orderItem.Id);
                    }
                    else
                    {
                        ViewBag.ErrorMessage = res.ExceptionMessage + " " + res.ExceptionInnerMessage;
                    }
                }
                else
                {
                    var allErrors = ModelState.Values.SelectMany(v => v.Errors).ToArray().ToList();

                    if (allErrors != null && allErrors.Any())
                    {
                        foreach (var error in allErrors)
                            ViewBag.ErrorMessage = error.ErrorMessage;

                        return View(model);
                    }


                    return View(model);
                }

            }

            return View(model);
        }

        private void ManipulateParentDashboardValidations(JbForm jbForm, string message)
        {
            if (message == "Participant")
            {
                ModelState.AddModelError(jbForm, SectionsName.ParticipantSection, ElementsName.FirstName, "The participant already exists.");
            }

            if (message == "Parent1")
            {
                ModelState.AddModelError(jbForm, SectionsName.ParentGuardianSection, ElementsName.FirstName, "The parent you have entered already exists in the form.");
            }

            if (message == "Parent2")
            {
                ModelState.AddModelError(jbForm, SectionsName.Parent2GuardianSection, ElementsName.FirstName, "The parent you have entered already exists in the form.");
            }

            if (message == "authorizedPickup1")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup1, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }

            if (message == "authorizedPickup2")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup2, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }

            if (message == "authorizedPickup3")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup3, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }

            if (message == "authorizedPickup4")
            {
                ModelState.AddModelError(jbForm, SectionsName.AuthorizedPickup4, ElementsName.FirstName, "The authorized Pickup you have entered already exists in the form.");
            }
        }

        public virtual ActionResult OrderItemDetail(long orderItemId)
        {
            var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId);
            var showCombinedPriceOnlyForClub = false;
            CheckResourceForAccess(orderItem);

            if (orderItem.ProgramSchedule.Program.Club.Setting != null)
            {
                showCombinedPriceOnlyForClub = orderItem.ProgramSchedule.Program.Club.Setting.ShowCombinedPriceOnly;
            }

            var entryFee = orderItem.GetOrderChargeDiscounts().FirstOrDefault(c => c.Category == ChargeDiscountCategory.EntryFee);

            var model = new OrderItemDetailViewModel
            {
                Currency = orderItem.Order.Club.Currency,
                OrderItemId = orderItem.Id,
                PaidAmount = orderItem.PaidAmount,
                TotalAmount = orderItem.TotalAmount,
                ItemStatusReasons = orderItem.ItemStatusReason,
                Confirmation = orderItem.Order.ConfirmationId,
                Editable = orderItem.ProgramSchedule.Program.Club.Client.CanPlayerEditOrder,
                ProgramName = orderItem.ProgramSchedule.Program.Name,
                ProgramType = orderItem.ProgramSchedule.Program.TypeCategory,
                TuitionName = entryFee != null ? entryFee.Name : string.Empty,
                TuitionPrice = orderItem.ProgramTypeCategory == ProgramTypeCategory.Class ? _cartBusiness.CalculateOrderItemTuitionPrice(orderItem, 0) : entryFee != null ? entryFee.Amount : 0,
                Price = entryFee != null ? entryFee.Amount : 0,
                ScheduleDate =
                    $"{orderItem.ProgramSchedule.StartDate.ToString("MMM dd, yyyy")} - {orderItem.ProgramSchedule.EndDate.ToString("MMM dd, yyyy")}",
                Charges = showCombinedPriceOnlyForClub && orderItem.ProgramTypeCategory == ProgramTypeCategory.Class ? orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge && c.Category != ChargeDiscountCategory.EntryFee && c.Category != ChargeDiscountCategory.Surcharge && c.Category != ChargeDiscountCategory.PartnerSurcharge).Select(s => new ChargeDiscountItem { Name = s.Name, Price = s.Amount }).ToList()
                : orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Charge && c.Category != ChargeDiscountCategory.EntryFee).Select(s => new ChargeDiscountItem { Name = s.Name, Price = s.Amount }).ToList(),
                Discounts = orderItem.GetOrderChargeDiscounts().Where(c => c.Subcategory == ChargeDiscountSubcategory.Discount).Select(s => new ChargeDiscountItem { Name = s.Name, Price = s.Amount }).ToList(),
                OrderDate = DateTimeHelper.ConvertUtcDateTimeToLocal(orderItem.Order.CompleteDate, orderItem.ProgramSchedule.Program.Club.TimeZone),
                AttendeeName = orderItem.FullName,
                Installments = orderItem.Installments.Where(i => i.Type == InstallmentType.Noraml).Select(s => new InstallmentItemDetailViewModel(s)).ToList(),
                FollowUpForms = GetParentFollowupForms(orderItem),
                ClubWaivers = orderItem.OrderItemWaivers.ToList(),
            };

            return View(model);
        }

        public virtual ActionResult GetOrderItemInstallments(long orderItemId, PaginationModel paginationModel)
        {
            var result = _orderInstallmentBusiness.GetOrderItemInstallments(orderItemId, paginationModel.Sort, 0, int.MaxValue, this.GetCurrentUserRoleType(), null);

            return JsonNet(result);
        }

        public virtual ActionResult GetParentOrderItemTransactions(long orderItemId, long? installmentId = null)
        {
            var clubDomain = this.GetCurrentClubDomain();
            var club = Ioc.ClubBusiness.Get(clubDomain);

            var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var transactions =
                Ioc.TransactionActivityBusiness.GetAllTransactionByOrderItemId(orderItemId)
                    .Where(
                        t =>
                            t.TransactionStatus != TransactionStatus.Draft &&
                            t.TransactionStatus != TransactionStatus.Deleted &&
                            t.TransactionStatus != TransactionStatus.Failure);

            var model = new List<ParentTransactionActivitiesViewModel>();
            if (transactions.Any())
            {
                TimeZoneInfo localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(club.TimeZone.ToDescription());

                foreach (var trans in transactions.OrderBy(c => c.TransactionDate).ToList().GroupBy(c => c.PaymentDetailId))
                {
                    var item = trans.First().ToViewModel<ParentTransactionActivitiesViewModel>();
                    item.Currency = club.Currency;
                    item.ProgramId = trans.First().OrderItem.ProgramSchedule.ProgramId;
                    item.Amount = trans.Sum(c => c.Amount);
                    item.TransactionDate = TimeZoneInfo.ConvertTimeFromUtc(trans.First().TransactionDate, localTimeZone);
                    model.Add(item);
                }
                return JsonNet(new { DataSource = model, total = model.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
            }

            return JsonNet(new { DataSource = model, total = model.Count() }, new Newtonsoft.Json.JsonSerializerSettings(), "");
        }

        public virtual ActionResult OrderItemRegistrationForm(long orderItemId)
        {
            var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var model = new OrderItemRegistrationFormViewModel
            {
                OrderItemId = orderItem.Id,
                Form = orderItem.JbForm,
                Editable = orderItem.ProgramSchedule.Program.Club.Client.CanPlayerEditOrder,
            };

            model.Form.CurrentMode = AccessMode.ReadOnly;

            return View(model);
        }

        public virtual ActionResult OrderItemFollowUpForm(long orderItemId)
        {
            var orderItem = Ioc.OrderItemBusiness.GetItem(orderItemId);
            CheckResourceForAccess(orderItem);

            var model = new OrderItemFollowUpFormViewModel
            {
                OrderItemId = orderItem.Id,
                Form = orderItem.JbForm,
            };

            model.FollowUpForms = GetParentFollowupForms(orderItem);

            model.Form.Elements = model.Form.Elements.Where(f => f.Name.Equals(SectionsName.ParticipantSection.ToDescription())).ToList();//.Add(jbForm.Elements.First());
            model.Form.CurrentMode = Jb.Framework.Common.Forms.AccessMode.ReadOnly;

            return View(model);
        }

        public List<UserFollowupFormDetails> GetParentFollowupForms(OrderItem orderItem)
        {
            List<UserFollowupFormDetails> followupforms = new List<UserFollowupFormDetails>();

            foreach (var form in orderItem.FollowupForms.ToList())
            {
                var followupformdetails = new UserFollowupFormDetails();

                followupformdetails.Title = form.FormName;
                followupformdetails.Type = form.FormType;
                followupformdetails.Status = form.Status;
                followupformdetails.Guid = form.Guid;
                followupformdetails.Id = form.Id;
                if (form.FormType == FormType.UploadForm)
                {
                    followupformdetails.Link = Url.Action("DownloadForm", "FollowupForm", new { fId = form.Id, uId = form.Guid, orderItemId = orderItem.Id });
                }
                else if (form.FormType == FormType.FollowUp)
                {
                    followupformdetails.Link = Url.Action("CompleteForm", "FollowupForm", new { fId = form.Id, uId = form.Guid, orderItemId = orderItem.Id });
                }

                followupforms.Add(followupformdetails);
            }

            return followupforms;
        }

        [AllowAnonymous]
        public virtual ActionResult ViewChessEntries(long programId, long seasonId, int clubId)
        {
            var model = new ChessTourneyEntriesViewModel(programId, seasonId, clubId);
            return View("_ViewEntries", model);
        }

        [AllowAnonymous]
        public virtual JsonResult Entries_Read([DataSourceRequest] DataSourceRequest request, long programId, long seasonId, int clubId)
        {
            var model = new ChessTourneyEntriesViewModel(programId, seasonId, clubId).GetChessTourneyEntries();
            return Json(model.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult FSAReport(FSAReportViewModel model)
        {
            var clubBusiness = Ioc.ClubBusiness;

            var years = new List<int>();

            for (int i = DateTime.Now.Year; i >= DateTime.Now.AddYears(-2).Year; i--)
            {
                years.Add(i);
            }

            ViewBag.Years = years.Select(y =>
            new SelectListItem
            {
                Text = y.ToString(),
                Value = y.ToString()
            })
            .ToList();

            var orderItemBusiness = Ioc.OrderItemBusiness;

            DateTime? startDate = !string.IsNullOrEmpty(Request["StartDate"]) ? DateTime.Parse(Request["StartDate"].ToString()) : new DateTime(DateTime.Now.Year, 1, 1);
            DateTime? endDate = !string.IsNullOrEmpty(Request["EndDate"]) ? DateTime.Parse(Request["EndDate"]) : DateTime.Now;
            int? year = Convert.ToInt32(Request["Year"]);

            FSAReportFilterType Type = model.FilterType;

            var orderItems = orderItemBusiness.GetFSAReport(model.FilterType.ToString(), CurrentUserId, ref year, ref startDate, ref endDate);

            var result = new List<FSAReportHeaderViewModel>();

            var clubs = orderItems != null && orderItems.Any() ? orderItems.Select(o => o.Order.Club).DistinctBy(o => o.Id).ToList() : new List<Club>();

            foreach (var item in clubs)
            {
                var club = item.PartnerId.HasValue ? (((PartnerSetting)item.PartnerClub.Setting)?.PortalParameters != null && ((PartnerSetting)item.PartnerClub.Setting).PortalParameters.ReadDependentCareReportInfoFromMember ? item : item.PartnerClub) : item;
                result.Add(new FSAReportHeaderViewModel()
                {
                    Id = club.Id,
                    Name = !string.IsNullOrEmpty(club.Name) ? club.Name : "",
                    Address = club.Address != null && !string.IsNullOrEmpty(club.Address.Address) ? club.Address.Address : "",
                    Phone = club.ContactPersons != null && !string.IsNullOrEmpty(club.ContactPersons.FirstOrDefault().Phone) ? PhoneNumberHelper.FormatPhoneNumber(club.ContactPersons.FirstOrDefault().Phone) : "",
                    Site = !string.IsNullOrEmpty(club.Site) ? club.Site.EndsWith("/") ? club.Site.Remove(club.Site.Count() - 1, 1) : club.Site : "",
                    Email = club.ContactPersons != null && !string.IsNullOrEmpty(club.ContactPersons.FirstOrDefault().Email) ? club.ContactPersons.FirstOrDefault().Email : "",
                    Description = club.Setting?.Notifications.Report.DependentCareDescription
                });
            }

            result = result.DistinctBy(r => r.Id).ToList();

            if (Type == FSAReportFilterType.Year)
            {
                ViewBag.Date = year.Value;
            }
            else
            {
                ViewBag.Date = string.Format("{0} - {1}", startDate.Value.Date.ToString("M/d/yyyy"), endDate.Value.Date.ToString("M/d/yyyy"));
            }
            ViewBag.Header = result;
            ViewBag.Count = result.Count;

            return View(model);
        }

        [HttpGet]
        public virtual JsonResult GetFSAReport(string filterType, int? year, DateTime? startDate, DateTime? endDate)
        {
            var orderItemBusiness = Ioc.OrderItemBusiness;

            FSAReportFilterType Type = (FSAReportFilterType)Enum.Parse(typeof(FSAReportFilterType), filterType);

            var orderItems = orderItemBusiness.GetFSAReport(filterType, CurrentUserId, ref year, ref startDate, ref endDate);

            var programBusiness = Ioc.ProgramBusiness;
            var transactionBusiness = Ioc.TransactionActivityBusiness;

            var result = new List<FSAReportItemViewModel>();

            foreach (var item in orderItems)
            {

                var transactions = Type == FSAReportFilterType.Year ? transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate.Year == year.Value).Any() ?
                                                                      transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate.Year == year.Value) : null :
                                   Type == FSAReportFilterType.DateRange ? transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate >= startDate.Value && t.TransactionDate <= endDate.Value).Any() ?
                                                                           transactionBusiness.GetAllsucceedTransactionByOrderItemId(item.Id).Where(t => (t.TransactionType == TransactionType.Payment || t.TransactionType == TransactionType.Refund) && t.TransactionDate >= startDate.Value && t.TransactionDate <= endDate.Value) : null : null;



                if (transactions != null)
                {
                    foreach (var transaction in transactions)
                    {
                        var programScheduleMode = _programBusiness.GetClassScheduleTime(item.ProgramSchedule.Program);

                        if (transaction.Amount != 0)
                        {
                            result.Add(new FSAReportItemViewModel()
                            {
                                StudentName = string.Format("{0}, {1}", item.LastName, item.FirstName),
                                ClassName = item.ProgramTypeCategory != ProgramTypeCategory.Camp ?
                                    transaction.TransactionType == TransactionType.Payment ? _programBusiness.GetProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName) : $"Refund - {_programBusiness.GetProgramNameOnlyTuition(item.ProgramSchedule, item.EntryFeeName)}" :
                                    transaction.TransactionType == TransactionType.Payment ? _programBusiness.GetProgramName(item.ProgramSchedule, item.EntryFeeName) : $"Refund - {_programBusiness.GetProgramName(item.ProgramSchedule, item.EntryFeeName)}",
                                AmountPaid = transaction.TransactionType == TransactionType.Payment ? (decimal?)transaction.Amount ?? 0 : (decimal?)-transaction.Amount ?? 0,
                                ClassMeetingDates = programBusiness.GetClassDates(item.ProgramSchedule, programScheduleMode) != null ? DateTimeHelper.GetDaysSeperatedByComma(programBusiness.GetClassDates(item.ProgramSchedule, programScheduleMode).Select(c => c.SessionDate).ToList()) : "-",
                                MethodOfPayment = !string.IsNullOrEmpty(transaction.PaymentDetail.PaymentMethod.ToDescription()) ? transaction.PaymentDetail.PaymentMethod.ToDescription() : "-",
                                ProviderName = item.ProgramSchedule.Program.OutSourceSeasonId.HasValue ? item.ProgramSchedule.Program.OutSourceSeason.Club.Name : item.ProgramSchedule.Program.Club.Name,
                                ProviderTaxId = item.ProgramSchedule.Program.OutSourceSeasonId.HasValue ?
                                                                       (item.ProgramSchedule.Program.OutSourceSeason.Club.Setting.TaxIDFEIN) :
                                                                       (item.ProgramSchedule.Program.Club.Setting.TaxIDFEIN),
                                Date = transaction.TransactionDate.ToString(Constants.DefaultDateFormat),
                                ScheduleDate = transaction.InstallmentId.HasValue && transaction.Installment.ProgramSchedulePartId.HasValue ? $"{transaction.Installment.ProgramSchedulePart.StartDate.ToString(Constants.DateTime_Comma)} - {transaction.Installment.ProgramSchedulePart.EndDate.ToString(Constants.DateTime_Comma)}" : $"{item.ProgramSchedule.StartDate.ToString(Constants.DateTime_Comma)} - {item.ProgramSchedule.EndDate.ToString(Constants.DateTime_Comma)}"
                            });
                        }
                    }
                }

            }

            return Json(new { DataSource = result, TotalCount = result.Count() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual ActionResult Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);

            return File(fileContents, contentType, fileName);
        }

        public virtual ActionResult Preferences()
        {
            var user = Ioc.UserProfileBusiness.Get(CurrentUserId);

            var model = new PreferencesViewModel()
            {
                CurrentUserName = user.UserName,
                ChangePassword = new ParentChangePasswordViewModel(),
                ChangeEmail = new ParentChangeEmailViewModel()
            };

            return View(model);
        }

        public virtual ActionResult FamiliesAndChildren()
        {
            return View();
        }
        [HttpPost]
        [JbAudit(JbAction.Family_Change_Password, "Password changed")]
        public virtual ActionResult ChangePassword(ParentChangePasswordViewModel model)
        {
            var clubDomain = this.GetCurrentClubDomain();
            var club = Ioc.ClubBusiness.Get(clubDomain);
            if (!string.IsNullOrEmpty(model.CurrentPassword) && !_applicationUserManager.CheckPassword(Ioc.UserProfileBusiness.Get(CurrentUserId), model.CurrentPassword))
            {
                ModelState.AddModelError("CurrentPassword", "Current password is not correct.");
            }
            else if (!string.IsNullOrEmpty(model.NewPassword) && model.CurrentPassword == model.NewPassword)
            {
                ModelState.AddModelError("NewPassword", "New password is same as current password.");
            }

            if (ModelState.IsValid)
            {
                var status = _applicationUserManager.ChangePassword(CurrentUserId, model.CurrentPassword, model.NewPassword);

                Status = status.Succeeded;

                EmailService emailService = EmailService.Get(this.ControllerContext);

                var user = Ioc.UserProfileBusiness.Get(CurrentUserId);

                emailService.ParentNotificationChangePassword(user.UserName, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo, clubDomain);


                return Json(new { statusPassword = true });
            }

            return PartialView("_ChangePassword", model);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Change_Email)]
        public virtual ActionResult ChangeEmail(ParentChangeEmailViewModel model)
        {

            try
            {
                CheckChangeEmailValidation(ModelState, model);

                if (ModelState.IsValid)
                {
                    SetActivityDescription(Constants.AuditingConstants.AuditingChangeFormat, model.Email);

                    var clubDomain = this.GetCurrentClubDomain();
                    var club = Ioc.ClubBusiness.Get(clubDomain);
                    var CreditCard = Ioc.UserCreditCardBusiness;
                    var clubBaseInfo = ClubService.Get().GetClubBaseInfo(club);
                    var email = model.Email.ToLower();
                    var userprofile = Ioc.UserProfileBusiness.Get(email);

                    CurrentUser userBaseInfo = new CurrentUser()
                    {
                        Id = CurrentUserId,
                        UserName = email,
                        Role = RoleCategory.Parent,
                    };

                    if (!_applicationUserManager.IsUserExist(email))
                    {

                        userprofile = Ioc.UserProfileBusiness.Get(CurrentUserId);
                        userprofile.UserName = email;
                        userprofile.Email = email;

                        var res = Ioc.UserProfileBusiness.Update(userprofile);

                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        var logingUrl = Url.Action("Login", "Account");

                        EmailService emailService = EmailService.Get(this.ControllerContext);
                        emailService.ParentNotificationChangeEmail(userprofile.UserName, email, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo, clubDomain);

                        ////////////////////////////////////////////////////change email in stipe
                        if (CreditCard != null)
                        {

                            var customer = CreditCard.GetListCard(CurrentUserId);
                            var myCustomer = new StripeCustomerUpdateOptions();
                            myCustomer.Email = email;
                            myCustomer.Description = string.Format("{0} ({1})", email, CurrentUserId);
                            var customerService = new StripeCustomerService(WebConfigHelper.Stripe_SecretKey);
                            if (customer != null)
                            {
                                StripeCustomer stripeCustomer = customerService.Update(customer.CustomerId, myCustomer);
                            }

                        }

                        Status = res.Status;

                        return Json(new { status = true, loginUrl = logingUrl });
                    }
                    else if (userprofile.Id == CurrentUserId)
                    {
                        var res = Ioc.UserProfileBusiness.Update(userprofile);
                        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                        var logingUrl = Url.Action("Login", "Account");

                        EmailService emailService = EmailService.Get(this.ControllerContext);
                        emailService.ParentNotificationChangeEmail(userprofile.UserName, email, club.PartnerId.HasValue ? club.PartnerClub.Logo : club.Logo, clubDomain);

                        ////////////////////////////////////////////////////change email in stipe
                        if (CreditCard != null)
                        {
                            var customer = CreditCard.GetListCard(CurrentUserId);
                            var myCustomer = new StripeCustomerUpdateOptions();
                            myCustomer.Email = email;
                            myCustomer.Description = string.Format("{0} ({1})", email, CurrentUserId);
                            var customerService = new StripeCustomerService(WebConfigHelper.Stripe_SecretKey);
                            if (customer != null)
                            {
                                StripeCustomer stripeCustomer = customerService.Update(customer.CustomerId, myCustomer);
                            }
                        }

                        Status = res.Status;

                        ViewBag.MyResult = true;

                        return Json(new { status = true, loginUrl = logingUrl });
                    }

                }
                return PartialView("_ChangeEmail", model);
            }
            catch (Exception ex)
            {
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }

        }

        [HttpPost]
        public virtual ActionResult ChangeEmailValidation(ParentChangeEmailViewModel model)
        {
            CheckChangeEmailValidation(ModelState, model);

            if (ModelState.IsValid)
            {
                return Json(new { status = true });
            }

            return PartialView("_ChangeEmail", model);
        }

        public virtual ActionResult BillingMethods()
        {
            Club club = Ioc.ClubBusiness.Get(this.GetCurrentClubId());

            var paymentMethod = Ioc.ClientPaymentMethodBusiness.GetByClub(club.Id);

            ViewBag.HasInstallment = false;

            var allOrderItems = Ioc.OrderItemBusiness.GetUserOrderItems(CurrentUserId, false).Where(s => s.PaymentPlanType == PaymentPlanType.Installment && s.Order.IsAutoCharge && s.ItemStatus == OrderItemStatusCategories.completed);

            var countInstallment = Ioc.InstallmentBusiness.CountInstallmentParentNotPaid(allOrderItems);
            if (allOrderItems.Any(o => o.ProgramSchedule.EndDate > DateTime.Now))
            {
                countInstallment++;
            }
            if (countInstallment > 0)
            {
                ViewBag.HasInstallment = true;
            }

            ViewBag.MonthList = DateTimeHelper.GetMonthForDropdownlist();
            ViewBag.Year2CharList = DateTimeHelper.GetYearForDropdownlist();

            ViewBag.cardId = 0;
            var model = new BillingMethodsViewModel()
            {
                BillingMethodsItem = new List<ParentBillingMethodsItemViewModel>(),
                BillingMethods = new ParentBillingMethodsViewModel()
            };

            return View(model);

        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [HttpGet]
        public virtual ActionResult BillingMethodsItems()
        {

            Club club = Ioc.ClubBusiness.Get(this.GetCurrentClubDomain());
            ClientPaymentMethod clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
            var clubId = Ioc.ClubBusiness.GetClubByClientId(clientPaymentMethod.Client.Id).Id;

            var _paymentGateway = Ioc.ClientBusiness.GetPaymentGatewayClub(clubId);

            bool HasInstallment = false;

            var allOrderItems = Ioc.OrderItemBusiness.GetUserOrderItems(CurrentUserId, false).Where(s => s.PaymentPlanType == PaymentPlanType.Installment && s.Order.IsAutoCharge && s.ItemStatus == OrderItemStatusCategories.completed);
            var countInstallment = Ioc.InstallmentBusiness.CountInstallmentParentNotPaid(allOrderItems);

            if (allOrderItems.Any(o => o.ProgramSchedule.EndDate > DateTime.Now))
            {
                countInstallment++;
            }
            if (countInstallment > 0)
            {
                HasInstallment = true;
            }

            var year = DateTime.Now.AddYears(-2000).Year;


            var model = new List<ParentBillingMethodsItemViewModel>();

            try
            {

                var userCreditCard = Ioc.UserCreditCardBusiness.GetUserCreditCards(CurrentUserId, _paymentGateway);
                var IsLast = userCreditCard.Count() == 1 ? true : false;
                foreach (var item in userCreditCard)
                {

                    model.Add(new ParentBillingMethodsItemViewModel()
                    {
                        CardHolderName = item.Name,
                        Last4Digit = item.LastDigits,
                        Id = item.Id,
                        Year = item.ExpiryYear,
                        Month = item.ExpiryMonth.Length == 1 ? ("0" + item.ExpiryMonth) : item.ExpiryMonth,
                        Brand = item.Brand,
                        CardId = item.CardId,
                        IsDefault = item.IsDefault,
                        Address = item.Address.Address,
                        UserId = item.UserId,
                        HasInstallment = HasInstallment,
                        IsLast = IsLast,
                    });


                }



            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
            }
            return Json(new { DataSource = model, TotalCount = model.Count() }, JsonRequestBehavior.AllowGet);
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        [HttpGet]
        public virtual ActionResult CreateEditUserCard(int? cardId)
        {
            ViewBag.MonthList = DateTimeHelper.GetMonthForDropdownlist();
            ViewBag.Year2CharList = DateTimeHelper.GetYearForDropdownlist();

            var pageMode = PageMode.Create;

            if (cardId.HasValue)
            {
                pageMode = PageMode.Edit;
            }

            var model = new ParentBillingMethodsViewModel();

            switch (pageMode)
            {
                case PageMode.Create:
                    {
                        model.PageMode = pageMode;
                    }
                    break;
                case PageMode.Edit:
                    {
                        var card = Ioc.UserCreditCardBusiness.Get(cardId.Value);
                        if (card != null)
                        {
                            model = card.ToViewModel<ParentBillingMethodsViewModel>();
                            model.Year = card.ExpiryYear;
                            model.CardHolderName = card.Name;
                            model.Year2Char = int.Parse(card.ExpiryYear) - 2000;
                            model.Month = card.ExpiryMonth.Length == 1 ? "0" + card.ExpiryMonth : card.ExpiryMonth;
                            model.Address = card.Address.Address;
                            model.IsDefault = card.IsDefault;

                            model.CustomerResponse = card.CustomerResponse;
                            model.Last4Digit = card.LastDigits;
                            model.Id = card.Id;

                            model.PageMode = pageMode;
                        }
                    }
                    break;
                default:
                    break;

            }
            return PartialView("_CreateEditUserCard", model);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Change_Creditcard)]
        public virtual ActionResult CreateEditUserCard(ParentBillingMethodsViewModel model)
        {

            var creditCards = Ioc.UserCreditCardBusiness;
            int clubId;
            var secretKey = WebConfigHelper.Stripe_SecretKey;
            string LastCardCustomerIdAuthorize = "";
            string LastCardCustomerIdStripe = "";
            ViewBag.MonthList = DateTimeHelper.GetMonthForDropdownlist();
            ViewBag.Year2CharList = DateTimeHelper.GetYearForDropdownlist();
            Dictionary<string, string> LastCardCustomerId = new Dictionary<string, string>();

            UserCreditCard userCard = null;
            var modelCard = new PaymentModels();
            createCustomerProfileResponse responseAuthorizeNet = null;
            createCustomerPaymentProfileResponse responsePaymentAuthorizeNet = null;
            updateCustomerPaymentProfileResponse updateResponseAuthorizeNet = null;

            var userCustomerResponse = "";
            var userCustomerResponseStripe = "";

            try
            {
                Club club = Ioc.ClubBusiness.Get(this.GetCurrentClubDomain());
                ClientPaymentMethod clientPaymentMethod = Ioc.ClientPaymentMethodBusiness.Get(club.ClientId.Value);
                clubId = Ioc.ClubBusiness.GetClubByClientId(clientPaymentMethod.Id).Id;

                var address = new PostalAddress();
                CheckUserCardValidation(ModelState, model, ref address);
                var _paymentGateway = Ioc.ClientBusiness.GetPaymentGatewayClub(clubId);
                UserCreditCard selectedCard = null;
                UserCreditCard anotherCreditCard = null;
                var userCreditCard = creditCards.GetParentCreditCards(CurrentUserId).ToList();

                var userCreditCardGroup = userCreditCard.OrderByDescending(u => u.TypePaymentGateway).GroupBy(u => u.TypePaymentGateway);

                List<UserCreditCard> ListLastCard = new List<UserCreditCard>();
                bool isDefault = false;
                var defaultCard = userCreditCard.Where(c => c.IsDefault);

                if (!defaultCard.Any())
                {
                    isDefault = true;
                    model.IsDefault = true;
                }
                foreach (var creditCard in userCreditCardGroup) //get customerId from All Credit Card User - Stripe Or Authorize 
                {
                    var firstCard = creditCard.ToList().FirstOrDefault();
                    var lastCard = creditCard.ToList().LastOrDefault();

                    LastCardCustomerId.Add(creditCard.Key.ToString(), lastCard.CustomerId);
                }
                var parentCreditCard = creditCards.GetParentCreditCards(CurrentUserId).ToList().OrderByDescending(u => u.TypePaymentGateway);
                var userCrditCardDefaultGateway = parentCreditCard.Where(c => c.TypePaymentGateway == _paymentGateway);

                if (model.PageMode == PageMode.Create)
                {
                    if (model.CardNumber != null)
                    {
                        var lastDigit = model.CardNumber.Substring((model.CardNumber.Length) - 4, 4);

                        var isCreditCardExist = userCrditCardDefaultGateway.Any(u => u.LastDigits == lastDigit); //check the card in db All cards User with default gateway club

                        if (isCreditCardExist)
                        {
                            ModelState.AddModelError("CardNumber", "The card number already exists.");
                        }

                    }

                }
                if (model.PageMode == PageMode.Edit)
                {
                    selectedCard = creditCards.Get(model.Id);
                    model.Last4Digit = selectedCard.LastDigits;

                    if (model.CardNumber != null)
                    {
                        var lastDigit = Ioc.UserCreditCardBusiness.GetLastDigit(model.CardNumber);

                        var isCardNumberChanged = lastDigit != selectedCard.LastDigits;

                        if (isCardNumberChanged) // if cardnumber selected not similar with cardnumber inserted
                        {
                            ModelState.AddModelError("CardNumber", string.Format("You must enter the same credit card number ending in {0}.", selectedCard.LastDigits));
                        }

                    }

                }

                if (ModelState.IsValid)
                {
                    model.Year = (2000 + model.Year2Char).ToString();
                    isDefault = model.IsDefault;

                    var userDb = Ioc.UserProfileBusiness;

                    StripeCard stripeCard = null;
                    var stripeService = new JbStripeService(this.GetCurrentClubDomain(), false);
                    var authorizeNetService = new JbAuthorizeNetService(this.GetCurrentClubDomain(), false);



                    bool isAuthorizeNet = false;

                    var LoginId = "";
                    var TransactionKey = "";

                    if (!string.IsNullOrEmpty(clientPaymentMethod.AuthorizeLoginId) && !string.IsNullOrEmpty(clientPaymentMethod.AuthorizeTransactionKey))
                    {
                        isAuthorizeNet = true;

                        if (clientPaymentMethod.IsCreditCardTestMode)
                        {
                            LoginId = WebConfigHelper.AuthorizeNet_LoginId_Test;
                            TransactionKey = WebConfigHelper.AuthorizeNet_Transaction_Test;
                        }
                        else
                        {
                            LoginId = clientPaymentMethod.AuthorizeLoginId;
                            TransactionKey = clientPaymentMethod.AuthorizeTransactionKey;
                        }

                    }
                    else
                    {
                        isAuthorizeNet = false;
                    }


                    #region separate CustomerId stripe and AuthorizeNet


                    #endregion

                    List<UserCreditCard> listCreditsForUpdate = new List<UserCreditCard>();
                    int stripeCreditId = 0;
                    int authoizeCreditId = 0;
                    var lastDigit = Ioc.UserCreditCardBusiness.GetLastDigit(model.CardNumber);

                    switch (model.PageMode)
                    {

                        case PageMode.Create:
                            {
                                SetActivityDescription("Credit card {0} was added", lastDigit);

                                if (LastCardCustomerId.Any())
                                {
                                    foreach (var item in LastCardCustomerId) //
                                    {
                                        if (item.Key == "Stripe")
                                        {
                                            LastCardCustomerIdStripe = LastCardCustomerId["Stripe"];
                                        }
                                        if (item.Key == "AuthorizeNet")
                                        {
                                            LastCardCustomerIdAuthorize = LastCardCustomerId["AuthorizeNet"];
                                        }
                                    }
                                }

                                modelCard = new PaymentModels()
                                {
                                    CardNumber = model.CardNumber,
                                    Month = model.Month,
                                    Year = (model.Year2Char + 2000).ToString(),
                                    CardHolderName = model.CardHolderName,
                                    StrAddress = model.Address,
                                    CVV = model.CVV,
                                };


                                #region Create Customer or card in Stripe always

                                if (LastCardCustomerIdStripe.HasValue())
                                {// create Card with customerid in stripe
                                    stripeCard = stripeService.CreateStripeCustomerForUser(model, address, LastCardCustomerIdStripe, secretKey);
                                    userCustomerResponseStripe = JsonConvert.SerializeObject(stripeCard);
                                }
                                else if (!LastCardCustomerIdStripe.HasValue())
                                { // create new customer in stripe
                                    stripeCard = stripeService.CreateStripeCustomerForUser(model, address, null, secretKey);
                                    userCustomerResponseStripe = JsonConvert.SerializeObject(stripeCard);
                                }

                                #endregion

                                #region Create Customer in Authorize

                                if (isAuthorizeNet)
                                {
                                    if (LastCardCustomerIdAuthorize.HasValue()) // create Card with CustomerID in authorizeNet
                                    {
                                        responsePaymentAuthorizeNet = authorizeNetService.CreateAuthorizeNetPaymentProfile(modelCard, address, LastCardCustomerIdAuthorize);

                                        if (responsePaymentAuthorizeNet == null)
                                        {
                                            throw new Exception("Issue response null");
                                        }

                                        if (responsePaymentAuthorizeNet.messages.resultCode == messageTypeEnum.Ok)
                                        {
                                            userCustomerResponse = JsonConvert.SerializeObject(responsePaymentAuthorizeNet);
                                            modelCard.CardId = responsePaymentAuthorizeNet.customerPaymentProfileId;
                                        }
                                        else if (responsePaymentAuthorizeNet.messages.resultCode == messageTypeEnum.Error && responsePaymentAuthorizeNet.messages.message[0].code == "E00003")
                                        {
                                            throw new Exception("Issue response null");
                                        }
                                        else if (responsePaymentAuthorizeNet.messages.resultCode == messageTypeEnum.Error && responsePaymentAuthorizeNet.messages.message[0].code == "E00042")
                                        {
                                            ModelState.AddModelError("CardNumber", "You cannot add more than 10 Credit Card.");
                                            return PartialView("_CreateEditUserCard", model);
                                        }
                                        else if (responsePaymentAuthorizeNet.messages.resultCode == messageTypeEnum.Error)
                                        {
                                            ModelState.AddModelError("", responsePaymentAuthorizeNet.messages.message[0].text);
                                            return PartialView("_CreateEditUserCard", model);
                                        }
                                    }
                                    else
                                    { // create new customer in authorizeNet
                                        responseAuthorizeNet = authorizeNetService.CreateAuthorizeNetCustomerId(modelCard, address, null);

                                        if (responseAuthorizeNet == null)
                                        {
                                            throw new Exception("Issue response null");
                                        }
                                        else
                                        {
                                            if (responseAuthorizeNet.messages.resultCode == messageTypeEnum.Error && responseAuthorizeNet.messages.message[0].code == "E00039")
                                            { // when Available customer for user just in authorize dashboard
                                                var result = responseAuthorizeNet.messages.message[0].text;
                                                LastCardCustomerIdAuthorize = result.Substring(27, 10); // get customer from response

                                                if (clientPaymentMethod.DefaultPaymentGateway == PaymentGateway.AuthorizeNet && LastCardCustomerIdAuthorize.HasValue())
                                                {
                                                    responsePaymentAuthorizeNet = authorizeNetService.CreateAuthorizeNetPaymentProfile(modelCard, address, LastCardCustomerIdAuthorize);
                                                    userCustomerResponse = JsonConvert.SerializeObject(responsePaymentAuthorizeNet);
                                                    modelCard.CardId = responsePaymentAuthorizeNet.customerPaymentProfileId;
                                                }
                                            }
                                            else if (responseAuthorizeNet.messages.resultCode == messageTypeEnum.Error && responseAuthorizeNet.messages.message[0].code == "E00003")
                                            {
                                                throw new Exception("Issue response null");
                                            }
                                            else if (responseAuthorizeNet.messages.resultCode == messageTypeEnum.Error && responseAuthorizeNet.messages.message[0].code == "E00042")
                                            {
                                                ModelState.AddModelError("CardNumber", "You cannot add more than 10 Credit Card.");
                                                return PartialView("_CreateEditUserCard", model);
                                            }
                                            else if (responseAuthorizeNet.messages.resultCode == messageTypeEnum.Error)
                                            {
                                                ModelState.AddModelError("", responseAuthorizeNet.messages.message[0].text);
                                                return PartialView("_CreateEditUserCard", model);
                                            }
                                            else if (responseAuthorizeNet.messages.resultCode == messageTypeEnum.Ok)
                                            { userCustomerResponse = JsonConvert.SerializeObject(responseAuthorizeNet); }
                                        }
                                    }
                                }


                                #endregion

                                userCard = new UserCreditCard();
                            }

                            break;
                        case PageMode.Edit:
                            {
                                SetActivityDescription("Credit card {0} was edited", lastDigit);
                                var userCreditCards = Ioc.UserCreditCardBusiness.GetAllList(CurrentUserId);
                                anotherCreditCard = userCreditCards.SingleOrDefault(c => c.LastDigits == selectedCard.LastDigits && c.TypePaymentGateway != selectedCard.TypePaymentGateway && c.IsDeleted == false);

                                listCreditsForUpdate.Add(selectedCard);
                                if (anotherCreditCard != null)
                                {
                                    listCreditsForUpdate.Add(anotherCreditCard);
                                }

                                foreach (var Card in listCreditsForUpdate)
                                {
                                    userCard = userCreditCard.SingleOrDefault(c => c.Id == Card.Id);

                                    if (Card.TypePaymentGateway == PaymentGateway.AuthorizeNet)
                                    {
                                        //LastCardCustomerIdAuthorize = Card.CustomerId;

                                        if (isAuthorizeNet)
                                        {
                                            authoizeCreditId = Card.Id;
                                            //update in Authorize

                                            updateResponseAuthorizeNet = authorizeNetService.UpdateAuthorizeNetCustomerForUser(model, address, Card.CustomerId, Card.CardId);

                                            if (updateResponseAuthorizeNet == null)
                                            {
                                                throw new Exception("Issue response null");
                                            }

                                            userCustomerResponse = JsonConvert.SerializeObject(updateResponseAuthorizeNet);
                                        }
                                    }
                                    else
                                    {
                                        LastCardCustomerIdStripe = Card.CustomerId;
                                        stripeCreditId = Card.Id;
                                        //update in stripe

                                        stripeCard = stripeService.UpdateStripeCustomerForUser(model, address, Card, Card.CustomerId, secretKey);

                                        if (stripeCard == null)
                                            throw new Exception("Delete failed");

                                        userCustomerResponseStripe = JsonConvert.SerializeObject(stripeCard);
                                    }
                                }
                            }
                            break;
                        default:
                            break;
                    }

                    var res = new OperationStatus() { Status = false };

                    if (userCreditCard.Any(u => u.IsDefault) && isDefault) // Remove default Previous credit card
                    {
                        foreach (var Card in userCreditCard)
                        {
                            if (Card.IsDefault)
                            {
                                Card.IsDefault = false;
                                creditCards.Update(Card);
                            }
                        }
                    }


                    if (model.PageMode == PageMode.Create)
                    {
                        userCard.LastDigits = modelCard.CardNumber.Substring((modelCard.CardNumber.Length) - 4, 4);
                        userCard.ExpiryYear = modelCard.Year;
                        userCard.ExpiryMonth = modelCard.Month.Length == 1 ? ("0" + modelCard.Month) : modelCard.Month;
                        userCard.Name = modelCard.CardHolderName;
                        userCard.Address = address;
                    }


                    userCard.TypeCreditCard = UserCreditCardType.ManuallyAdded;
                    userCard.UserId = CurrentUserId;

                    userCard.IsDefault = isDefault;
                    modelCard.IsDefault = isDefault;
                    DateTime? dateDefault = null;

                    switch (model.PageMode)
                    {
                        case PageMode.Create:
                            {
                                var brand = JbBasePaymentGateway.GetBrandCard(model.CardNumber);
                                // create row for stripe
                                if (stripeCard != null && !string.IsNullOrEmpty(stripeCard.CustomerId))
                                {
                                    if (isDefault)
                                    {
                                        dateDefault = DateTime.UtcNow;
                                    }
                                    userCard.ClubId = clubId;
                                    userCard.CardId = stripeCard.Id;
                                    userCard.CustomerId = stripeCard.CustomerId;
                                    userCard.CreatedDate = DateTime.UtcNow;
                                    userCard.UpdatedDate = null;
                                    userCard.DefaultDate = dateDefault;
                                    userCard.DeletedDate = null;
                                    userCard.Brand = stripeCard.Brand;
                                    userCard.CustomerResponse = userCustomerResponseStripe;
                                    userCard.TypePaymentGateway = PaymentGateway.Stripe;
                                    brand = stripeCard.Brand;
                                    userCard.IsDefault = isDefault;
                                    res = creditCards.Create(userCard);
                                }

                                if (isAuthorizeNet && ((responseAuthorizeNet != null && responseAuthorizeNet.messages.resultCode == messageTypeEnum.Ok) || responsePaymentAuthorizeNet != null && responsePaymentAuthorizeNet.messages.resultCode == messageTypeEnum.Ok))
                                { //create row for Authorize

                                    if (isDefault)
                                    {
                                        dateDefault = DateTime.UtcNow;
                                    }
                                    if (responsePaymentAuthorizeNet != null)
                                    {
                                        userCard.CardId = responsePaymentAuthorizeNet.customerPaymentProfileId;
                                        userCard.CustomerId = responsePaymentAuthorizeNet.customerProfileId;
                                        userCard.Brand = brand;
                                    }
                                    else
                                    {
                                        userCard.CardId = responseAuthorizeNet.customerPaymentProfileIdList[0];
                                        userCard.CustomerId = responseAuthorizeNet.customerProfileId;
                                        userCard.Brand = brand;
                                    }
                                    userCard.ClubId = clubId;
                                    userCard.CustomerResponse = userCustomerResponse;
                                    userCard.TypePaymentGateway = PaymentGateway.AuthorizeNet;
                                    userCard.CreatedDate = DateTime.UtcNow;
                                    userCard.UpdatedDate = null;
                                    userCard.IsDefault = isDefault;
                                    userCard.DefaultDate = dateDefault;
                                    userCard.DeletedDate = null;
                                    res = creditCards.Create(userCard);

                                    Status = res.Status;
                                }
                            }
                            break;
                        case PageMode.Edit:
                            {
                                if (stripeCard != null && !string.IsNullOrEmpty(stripeCard.CustomerId))
                                {

                                    userCard = creditCards.Get(stripeCreditId);
                                    userCard.ExpiryYear = model.Year;
                                    userCard.ExpiryMonth = model.Month.Length == 1 ? ("0" + model.Month) : model.Month;
                                    userCard.Name = model.CardHolderName;
                                    userCard.Address.City = address.City;
                                    userCard.Address.Street = address.Street;
                                    userCard.Address.StreetNumber = address.StreetNumber;
                                    userCard.Address.Country = address.Country;
                                    userCard.Address.Address = address.Address;
                                    userCard.Address.AutoCompletedAddress = address.AutoCompletedAddress;
                                    userCard.Address.State = address.State;
                                    userCard.Address.Route = address.Route;
                                    userCard.Address.Lng = address.Lng;
                                    userCard.Address.Lat = address.Lat;
                                    userCard.Address.Zip = address.Zip;
                                    userCard.Address.TimeZone = address.TimeZone;
                                    userCard.CardId = stripeCard.Id;
                                    userCard.UpdatedDate = DateTime.UtcNow;
                                    userCard.CustomerId = stripeCard.CustomerId;
                                    userCard.CustomerResponse = userCustomerResponseStripe;
                                    userCard.IsDefault = model.IsDefault;
                                    userCard.TypePaymentGateway = PaymentGateway.Stripe;

                                    res = creditCards.Update(userCard);
                                }
                                if (isAuthorizeNet && (updateResponseAuthorizeNet != null && updateResponseAuthorizeNet.messages.resultCode == messageTypeEnum.Ok))
                                {
                                    userCard = creditCards.Get(authoizeCreditId);
                                    userCard.ExpiryYear = model.Year;
                                    userCard.ExpiryMonth = model.Month.Length == 1 ? ("0" + model.Month) : model.Month;
                                    userCard.Name = model.CardHolderName;
                                    userCard.Address.City = address.City;
                                    userCard.Address.Street = address.Street;
                                    userCard.Address.StreetNumber = address.StreetNumber;
                                    userCard.Address.Country = address.Country;
                                    userCard.Address.Address = address.Address;
                                    userCard.Address.AutoCompletedAddress = address.AutoCompletedAddress;
                                    userCard.Address.State = address.State;
                                    userCard.Address.Route = address.Route;
                                    userCard.Address.Lng = address.Lng;
                                    userCard.Address.Lat = address.Lat;
                                    userCard.Address.Zip = address.Zip;
                                    userCard.Address.TimeZone = address.TimeZone;
                                    userCard.CustomerResponse = userCustomerResponse;
                                    userCard.UpdatedDate = DateTime.UtcNow;
                                    userCard.TypePaymentGateway = PaymentGateway.AuthorizeNet;
                                    userCard.IsDefault = model.IsDefault;
                                    res = creditCards.Update(userCard);

                                    Status = res.Status;
                                }
                            }
                            break;
                        default:
                            {
                                throw new NotSupportedException("the Page Mode not supported");
                            }
                    }

                    return Json(new { status = res.Status, PageMode = model.PageMode });
                }
            }
            catch (Exception ex)
            {
                if (ex.Message == "startIndex cannot be larger than length of string.\r\nParameter name: startIndex" || ex.Message == "Your card number is incorrect.")
                {
                    ModelState.AddModelError("CardNumber", "The card number is not a valid credit card number.");
                }
                else if (ex.Message == "Your card was declined. Your request was in test mode, but used a non test card. For a list of valid test cards, visit: https://stripe.com/docs/testing.")
                {
                    ModelState.AddModelError("", "Your card was declined. Your request was in test mode but used a non test card. For a list of valid test cards, visit: https://stripe.com/docs/testing.");
                }
                else if (ex.Message == "Your card was declined. Your request was in live mode, but used a known test card.")
                {
                    ModelState.AddModelError("", "Your card was declined. Your request was in live mode but you used a known test card.");
                }
                else if (ex.Message == "Your card's security code is incorrect.")
                {
                    ModelState.AddModelError("CVV", ex.Message);
                }
                else if (ex.Message == "StartIndex cannot be less than zero.\r\nParameter name: startIndex")
                {
                    ModelState.AddModelError("", "Please enter a valid card number.");
                }
                else if (ex.Message == "Delete failed")
                {
                    ModelState.AddModelError("", "The update invalid.");
                }
                else if (ex.Message == "Issue response null")
                {
                    ModelState.AddModelError("Address", "There is an issue with the card you are trying to use. Make sure all data is correct or use a different card.");
                }
                else
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return PartialView("_CreateEditUserCard", model);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Delete_Creditcard)]
        public virtual JsonResult DeleteCard(int id)
        {
            var secretKey = WebConfigHelper.Stripe_SecretKey;
            try
            {
                OperationStatus res = new OperationStatus() { Status = false, Message = "Please select the credit card" };

                var userCreditCards = Ioc.UserCreditCardBusiness.GetAllList(CurrentUserId);
                var selectedCreditCard = Ioc.UserCreditCardBusiness.Get(id);
                var anotherCreditCard = userCreditCards.SingleOrDefault(c => c.LastDigits == selectedCreditCard.LastDigits && c.TypePaymentGateway != selectedCreditCard.TypePaymentGateway && c.IsDeleted == false);

                var cardId = selectedCreditCard.CardId;
                var stripeService = new JbStripeService(this.GetCurrentClubDomain(), false);
                if (selectedCreditCard != null)
                {

                    if (cardId == null)
                    {
                        if (selectedCreditCard.TypePaymentGateway == PaymentGateway.Stripe)
                        {
                            var json = selectedCreditCard.CustomerResponse;
                            dynamic data = JObject.Parse(json);
                            cardId = data.DefaultSourceId;
                        }
                        else
                        {
                            var json = selectedCreditCard.CustomerResponse;
                            dynamic data = JObject.Parse(json);
                            cardId = data.customerProfileId;
                        }

                    }

                    //stripeService.DeleteCard(card.StripeCustomerId, cardId);
                    res = Ioc.UserCreditCardBusiness.Delete(id);
                    Status = res.Status;
                    SetActivityDescription("Credit card {0} was deleted", selectedCreditCard.LastDigits);

                    if (res.Status)
                    {

                        switch (selectedCreditCard.TypePaymentGateway)
                        {
                            case PaymentGateway.Stripe:
                                {
                                    var cardService = new StripeCardService(secretKey); //search stripe if not card in customer also delete customer
                                    IEnumerable<StripeCard> response = cardService.List(selectedCreditCard.CustomerId);
                                    if (!response.Any())
                                    {
                                        stripeService.DeleteCustomer(selectedCreditCard.CustomerId);
                                    }


                                    if (anotherCreditCard != null) //delete from Authorize
                                    {
                                        var authorizeNetService = new JbAuthorizeNetService(this.GetCurrentClubDomain(), false);
                                        authorizeNetService.DeleteCustomerPaymentProfile(anotherCreditCard.CustomerId, anotherCreditCard.CardId);
                                    }

                                }
                                break;
                            case PaymentGateway.AuthorizeNet:
                                {
                                    //delete from Authorize
                                    var authorizeNetService = new JbAuthorizeNetService(this.GetCurrentClubDomain(), false);
                                    authorizeNetService.DeleteCustomerPaymentProfile(selectedCreditCard.CustomerId, selectedCreditCard.CardId);


                                    if (anotherCreditCard != null)  //search stripe if not card in customer also delete customer
                                    {
                                        var cardService = new StripeCardService(secretKey);
                                        IEnumerable<StripeCard> response = cardService.List(anotherCreditCard.CustomerId);
                                        if (!response.Any())
                                        {
                                            stripeService.DeleteCustomer(anotherCreditCard.CustomerId);
                                        }
                                    }

                                }
                                break;
                            default:
                                break;
                        }

                    }

                }
                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }

            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        [HttpPost]
        public virtual JsonResult ActivateCard(int cardId, int userId)
        {
            try
            {
                OperationStatus res = new OperationStatus() { Status = false, Message = "Please select a credit card" };
                if (cardId > 0)
                {
                    res = Ioc.UserCreditCardBusiness.ActivateUserCreditCard(userId, cardId);
                }

                return Json(new JResult { Status = res.Status, Message = res.Message, RecordsAffected = res.RecordsAffected });
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        private void CheckUserCardValidation(ModelStateDictionary modelState, ParentBillingMethodsViewModel model, ref PostalAddress address)
        {

            if (!string.IsNullOrEmpty(model.Address))
            {
                try
                {
                    GoogleMap.GetGeo(model.Address, ref address);
                }
                catch
                {
                    ModelState.AddModelError("Address", "Address is not valid.");
                }
            }



            if (model.Year2Char <= 0)
            {
                ModelState.AddModelError("Month", "Expiration date is required.");
            }
            else
            {
                var month = 0;
                if (int.TryParse(model.Month, out month))
                {

                    if ((model.Year2Char + 2000) == DateTime.UtcNow.Year && month < DateTime.UtcNow.Month)
                    {
                        ModelState.AddModelError("Month", "Expiration date is invalid.");
                    }
                }
            }

            if (string.IsNullOrEmpty(model.CVV))
            {

                ModelState.AddModelError("CVV", "CVC is required.");
            }
            else if (model.CVV.Length < 3 || model.CVV.Length > 4)
            {
                ModelState.AddModelError("CVV", "CVC is invalid.");
            }

            if (!string.IsNullOrEmpty(model.CardHolderName))
            {
                var count = Ioc.UserCreditCardBusiness.CountWords(model.CardHolderName);

                if (count < 2)
                {
                    ModelState.AddModelError("CardHolderName", " Please enter first name and last name.");
                }
            }
        }

        private void CheckChangeEmailValidation(ModelStateDictionary modelState, ParentChangeEmailViewModel model)
        {
            if (!string.IsNullOrEmpty(model.CurrentPassword) && !_applicationUserManager.CheckPassword(Ioc.UserProfileBusiness.Get(CurrentUserId), model.CurrentPassword))
            {
                ModelState.AddModelError("CurrentPassword", "Current password is not correct.");
            }
            if (string.IsNullOrEmpty(model.Email))
            {
                ModelState.AddModelError("Email", "The email address is required");
            }
            else if (_applicationUserManager.IsUserExist(model.Email))
            {
                ModelState.AddModelError("Email", "The email, you trying to add, already exists in the system");
            }
        }

        public virtual ActionResult GetFamiliesProfile()
        {
            var model = new FamiliesProfileViewModel();
            var userId = CurrentUserId;


            var familyProfiles = Ioc.PlayerProfileBusiness.GetFamilyProfiles(userId);

            var family = Ioc.FamilyBusiness.GetFamily(userId);
            CheckResourceForAccess(family);

            foreach (var item in familyProfiles.Parents)
            {
                model.Parents.Add(new ParentsInformationViewModel
                {
                    Id = item.Id,
                    FullName = item.Contact.FullName,
                    Email = item.Contact.Email,
                    ContactId = item.Contact.Id,
                    GenderList = item.Gender,
                    ParentRelationship = item.Contact.Relationship,
                });
            }

            foreach (var item in familyProfiles.Participants)
            {
                model.Participants.Add(new ParticipantViewModel
                {
                    Id = item.Id,
                    FullName = item.Contact.FullName,
                    Email = item.Contact.Email,
                    ContactId = item.Contact.Id,
                    Gender = item.Gender,
                    Age = item.Contact.DoB != null ? DateTimeHelper.CalculateAge(item.Contact.DoB, DateTime.UtcNow) : 0,

                    Info = new PlayerInfoViewModel()
                    {
                        SchoolName = item.Info != null ? item.Info.SchoolName : string.Empty,
                        StrGrade = item.Info != null ? item.Info.Grade != 0 ? ((SchoolGradeType?)item.Info.Grade).ToDescription() : string.Empty : string.Empty,
                    },
                });
            }

            if (familyProfiles.EmergencyContacts != null)
            {
                foreach (var item in familyProfiles.EmergencyContacts)
                {
                    model.EmergencyContact.Add(new EmergencyContactViewModel
                    {
                        Id = item.Id,
                        FullName = item.Contact.FullName,
                        Email = item.Contact.Email,
                        ContactId = item.ContactId,
                        Relationship = item.Relationship,
                    });
                }
            }
            if (familyProfiles.AuthorizedPickups != null)
            {
                foreach (var item in familyProfiles.AuthorizedPickups)
                {
                    model.AuthorizedPickup.Add(new AuthorizepickupViewModel
                    {
                        Id = item.Id,
                        FullName = item.Contact.FullName,
                        Email = item.Contact.Email,
                        ContactId = item.ContactId,
                        Relationship = item.Relationship,
                        IsEmergencyContact = item.IsEmergencyContact
                    });
                }
            }

            model.InsuranceInformation.Add(new InsuranceInformationViewModel
            {
                Id = family.Id,
                InsuranceCompanyName = family.InsuranceCompanyName,
                InsurancePolicyNumber = family.InsurancePolicyNumber,
                InsurancePhone = family.InsurancePhone,

            });

            return View("FamiliesAndChildren", model);
        }

        [HttpGet]
        public virtual ActionResult FamilyInsurance()
        {
            var model = new InsuranceInformationViewModel();
            var userId = CurrentUserId;

            var family = Ioc.FamilyBusiness.GetFamily(userId);
            CheckResourceForAccess(family);

            if (family == null)
            {
                family = Ioc.FamilyBusiness.AddUserFamily(userId);
            }

            model = new InsuranceInformationViewModel
            {
                Id = family.Id,
                InsuranceCompanyName = family.InsuranceCompanyName,
                InsurancePolicyNumber = family.InsurancePolicyNumber,
                InsurancePhone = family.InsurancePhone,
            };


            return View(model);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Change_Insurance)]
        public virtual ActionResult FamilyInsurance(InsuranceInformationViewModel model)
        {
            var userId = CurrentUserId;
            var family = Ioc.FamilyBusiness.GetFamily(userId);
            CheckResourceForAccess(family);

            if (ModelState.IsValid)
            {
                family.InsuranceCompanyName = model.InsuranceCompanyName;
                family.InsurancePhone = model.InsurancePhone?.Replace("-", "");
                family.InsurancePolicyNumber = model.InsurancePolicyNumber;
                var dbResult = Ioc.FamilyBusiness.Update(family);
                SetActivityDescription(Constants.AuditingConstants.AuditingChangeFormat, family.InsuranceCompanyName);

                var result = new JResult(dbResult);
                Status = result.Status;

                return Json(new { Status = result.Status }, JsonRequestBehavior.AllowGet);
            }

            return PartialView("_FamilyInsurance", model);
        }

        [HttpGet]
        public virtual ActionResult ParentInformation(int? Id, PageStep pageStep)
        {
            var model = new ParentsInformationViewModel();
            var pageMode = PageMode.Create;

            if (Id.HasValue)
            {
                Ioc.UserProfileBusiness.EnsureUserIsProfileOwner(CurrentUserId, this.GetCurrentUserName(), Id.Value);
                pageMode = PageMode.Edit;
            }
            else
            {
                model.PageStep = PageStep.Step1;
            }

            if (pageMode == PageMode.Edit)
            {
                PlayerProfile player = Ioc.PlayerProfileBusiness.Get(Id.Value);
                CheckResourceForAccess(player);

                model = new ParentsInformationViewModel
                {
                    PlayerId = player.Id,
                    Email = player.Contact.Email,
                    FirstName = player.Contact.FirstName,
                    LastName = player.Contact.LastName,
                    FullName = player.Contact.FullName,
                    DOB = player.Contact.DoB != null ? player.Contact.DoB.Value.ToShortDateString() : null,
                    PrimaryPhone = player.Contact.Phone,
                    CellPhone = player.Contact.Cell,
                    UserId = player.UserId,
                    ContactId = player.Contact.Id,
                    PlayerType = player.Relationship,
                    GenderList = player.Gender,
                    Employer = player.Contact.Employer,
                    Occupation = player.Contact.Occupation,
                    ParentRelationship = player.Contact.Relationship,
                    AddressLine1 = player.Contact.Address != null ? player.Contact.Address.Street : "",
                    AddressLine2 = player.Contact.Address != null ? player.Contact.Address.Street2 : "",
                    City = player.Contact.Address != null ? player.Contact.Address.City : "",
                    State = player.Contact.Address != null ? player.Contact.Address.State : "",
                    TxtState = player.Contact.Address != null ? player.Contact.Address.Country != "UnitedStates" && player.Contact.Address.Country != "Canada" ? player.Contact.Address.State : "" : "",
                    Country = player.Contact.Address != null ? player.Contact.Address.Country : "UnitedStates",
                    Zip = player.Contact.Address != null ? player.Contact.Address.Zip : "",
                    PageMode = pageMode,
                    PageStep = pageStep,
                };
            }

            return View(model);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Change_Parent)]
        public virtual ActionResult ParentInformation(ParentsInformationViewModel model)
        {
            var playerContact = new ContactPerson();
            var player = new PlayerProfile();

            var state = string.Empty;
            var dbResult = new OperationStatus();

            state = !string.IsNullOrEmpty(model.State) ? model.State : model.TxtState;

            if (model.PageStep == PageStep.Step2 || model.PageStep == PageStep.Step3)
            {
                ModelState.Remove("FirstName");
                ModelState.Remove("LastName");
            }

            if (model.PageStep == PageStep.Step1)
            {
                var listContactFamily = Ioc.PlayerProfileBusiness.GetParents(CurrentUserId).Where(p => p.Id != model.PlayerId).Select(p => p.Contact).ToList();

                if (Ioc.PlayerProfileBusiness.IsExistPlayer(listContactFamily, model.FirstName, model.LastName))
                {
                    ModelState.AddModelError("FirstName", "The name you entered is an existing parent.");
                }
                if (!DateTimeHelper.IsValidDateFormat(model.DOB))
                {
                    ModelState.AddModelError("DOB", "Date of birth is invalid.");
                }
            }

            if (ModelState.IsValid)
            {
                var address = new PostalAddress();

                if (model.PageMode == PageMode.Edit)
                {
                    playerContact = Ioc.ContactPersonBusiness.Get(model.ContactId);
                    player = Ioc.PlayerProfileBusiness.Get(model.PlayerId);
                    CheckResourceForAccess(player);

                    address = playerContact.Address;
                }

                if (address == null)
                {
                    address = new PostalAddress();
                }

                if (model.PageStep == PageStep.Step1)
                {
                    playerContact.FirstName = model.FirstName;
                    playerContact.LastName = model.LastName;
                    playerContact.Email = model.Email;
                    playerContact.Phone = model.PrimaryPhone != null ? model.PrimaryPhone.Replace("-", "") : null;
                    playerContact.Cell = model.CellPhone != null ? model.CellPhone.Replace("-", "") : null;
                    playerContact.Relationship = model.ParentRelationship;
                    player.Gender = model.GenderList;
                    player.Relationship = RelationshipType.Parent;
                    playerContact.DoB = model.DOB != null ? DateTime.Parse(model.DOB) : (DateTime?)null;
                }

                if (model.PageStep == PageStep.Step2)
                {
                    playerContact.Employer = model.Employer;
                    playerContact.Occupation = model.Occupation;
                }

                if (model.PageStep == PageStep.Step3)
                {
                    address.Street = model.AddressLine1;
                    address.Street2 = model.AddressLine2;
                    address.Address = model.AddressLine1 + " " + model.AddressLine2 + ", " + model.City + ", " + state + " " + model.Zip + ", " + model.Country;
                    address.AutoCompletedAddress = model.AddressLine1 + " " + model.AddressLine2 + ", " + model.City + ", " + state + " " + model.Zip + ", " + model.Country;
                    address.State = state;
                    address.City = model.City;
                    address.Zip = model.Zip;
                    address.Country = model.Country;
                    playerContact.Address = address;
                    playerContact.AddressId = address.Id;
                }

                player.Relationship = RelationshipType.Parent;
                player.UserId = CurrentUserId;
                player.Contact = playerContact;

                if (model.PageMode == PageMode.Edit)
                {
                    //playerContact
                    dbResult = Ioc.ContactPersonBusiness.Update(playerContact);
                    SetActivityDescription(Constants.AuditingConstants.AuditingChangeFormat, player.Contact.FullName);
                }
                else
                {
                    dbResult = Ioc.PlayerProfileBusiness.Create(player);
                    SetActivityDescription(Constants.AuditingConstants.AuditingAddFormat, playerContact.FullName);
                }

                var result = new JResult(dbResult);
                Status = result.Status;

                return Json(new { Status = result.Status }, JsonRequestBehavior.AllowGet);

            }
            return PartialView("_ParentInformation", model);
        }

        public ActionResult FillState(string country)
        {
            var states = new List<SelectListItem>();
            if (country.ToLower() == "canada")
            {
                states = Enum.GetValues(typeof(CanadaStates)).Cast<CanadaStates>().Select(d => new SelectListItem { Text = d.ToDescription(), Value = d.ToString() }).ToList();
            }
            else if (country.ToLower() == "us" || country.ToLower() == "unitedstates")
            {
                states = Enum.GetValues(typeof(USStates)).Cast<USStates>().Select(d => new SelectListItem { Text = d.ToDescription(), Value = d.ToString() }).ToList();
            }

            return Json(states, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult ParticipantInformation(int? Id, PageStep pageStep)
        {
            var pageMode = PageMode.Create;
            var model = new ParticipantViewModel();

            model.Info = new PlayerInfoViewModel();

            model.Info.GradeList = _playerProfileBusiness.GetSchoolGrades();
            model.Info.GenderList = _playerProfileBusiness.GetGenderCategories();

            if (Id.HasValue)
            {
                // Ioc.UserProfileBusiness.EnsureUserIsProfileOwner(CurrentUserId, this.GetCurrentUserName(), Id.Value);
                pageMode = PageMode.Edit;
            }
            else
            {
                model.PageStep = PageStep.Step1;
            }

            var enumData = from GenderCategories e in Enum.GetValues(typeof(GenderCategories))
                           select new
                           {
                               Id = (int)e,
                               Name = e.ToDescription()
                           };

            ViewBag.EnumList = new SelectList(enumData.Where(e => e.Id != 3), "ID", "Name"); //remove All in dropdown

            if (pageMode == PageMode.Edit)
            {
                var player = Ioc.PlayerProfileBusiness.Get(Id.Value);
                CheckResourceForAccess(player);

                if (player.Info == null)
                {
                    player.Info = new PlayerInfo();
                }

                model = new ParticipantViewModel
                {

                    PlayerId = player.Id,
                    Email = player.Contact.Email,
                    FirstName = player.Contact.FirstName,
                    LastName = player.Contact.LastName,
                    FullName = player.Contact.FullName,
                    DOB = player.Contact.DoB != null ? player.Contact.DoB.Value.ToShortDateString() : null,
                    PageStep = pageStep,

                    Info = new PlayerInfoViewModel()
                    {
                        Id = player.Info.Id,
                        Grade = player.Info.Grade,
                        HasPermissionPhotography = player.Info.HasPermissionPhotography.HasValue ? player.Info.HasPermissionPhotography.Value : (bool?)null,
                        GradeList = _playerProfileBusiness.GetSchoolGrades(),
                        GenderList = _playerProfileBusiness.GetGenderCategories(),
                        Allergies = player.Info.Allergies,
                        SpecialNeeds = player.Info.SpecialNeeds,
                        HasSpecialNeeds = player.Info.HasSpecialNeeds,
                        SchoolName = player.Info.SchoolName,
                        HomeRoom = player.Info.HomeRoom,
                        DoctorLocation = player.Info.DoctorLocation,
                        DoctorName = player.Info.DoctorName,
                        DoctorPhone = player.Info.DoctorPhone,
                        SelfAdministerMedicationDescription = player.Info.SelfAdministerMedicationDescription,
                        SelfAdministerMedication = player.Info.SelfAdministerMedication,
                        NutAllergy = player.Info.NutAllergy,
                        HasAllergies = player.Info.HasAllergies,
                        NutAllergyDescription = player.Info.NutAllergyDescription,
                    },

                    UserId = player.UserId,
                    ContactId = player.Contact.Id,
                    PlayerType = player.Relationship,
                    Gender = player.Gender,
                    PageMode = pageMode,
                };
            }

            return View(model);
        }


        [HttpPost]
        [JbAudit(JbAction.Family_Change_Participant)]
        public virtual ActionResult ParticipantInformation(ParticipantViewModel model)
        {
            var currentUserName = this.GetCurrentUserName();

            var playerContact = new ContactPerson();
            var player = new PlayerProfile();
            var playerInfo = new PlayerInfo();
            var result = new OperationStatus();

            if (model.PageStep == PageStep.Step1 || model.PageStep == PageStep.Step2)
            {
                ModelState.Remove("Info.SpecialNeeds");
                ModelState.Remove("Info.Allergies");
                ModelState.Remove("Info.SelfAdministerMedicationDescription");
                ModelState.Remove("Info.NutAllergyDescription");
            }

            if (model.PageStep == PageStep.Step2 || model.PageStep == PageStep.Step3)
            {
                ModelState.Remove("FirstName");
                ModelState.Remove("LastName");
            }

            if (model.PageStep == PageStep.Step1)
            {
                var listContactFamily = Ioc.PlayerProfileBusiness.GetParticipants(CurrentUserId).Where(p => p.Id != model.PlayerId).Select(p => p.Contact).ToList();

                if (Ioc.PlayerProfileBusiness.IsExistPlayer(listContactFamily, model.FirstName, model.LastName))
                {
                    ModelState.AddModelError("FirstName", "The name you entered is an existing participant.");

                }

                if (!DateTimeHelper.IsValidDateFormat(model.DOB))
                {
                    ModelState.AddModelError("DOB", "Date of birth is invalid.");
                }

            }

            if (model.PageStep == PageStep.Step3)
            {
                if (model.Info.HasSpecialNeeds == null || model.Info.HasSpecialNeeds.Value == false)
                {
                    ModelState.Remove("Info.SpecialNeeds");
                }

                if (model.Info.HasAllergies == null || model.Info.HasAllergies.Value == false)
                {
                    ModelState.Remove("Info.Allergies");
                }

                if (model.Info.SelfAdministerMedication == null || model.Info.SelfAdministerMedication.Value == false)
                {
                    ModelState.Remove("Info.SelfAdministerMedicationDescription");
                }

                if (model.Info.NutAllergy == null || model.Info.NutAllergy.Value == false)
                {
                    ModelState.Remove("Info.NutAllergyDescription");
                }

            }

            if (ModelState.IsValid)
            {
                if (model.PageMode == PageMode.Edit)
                {
                    playerContact = Ioc.ContactPersonBusiness.Get(model.ContactId);
                    player = Ioc.PlayerProfileBusiness.Get(model.PlayerId);
                    CheckResourceForAccess(player);

                    playerInfo = player.Info;
                }

                if (player.Info == null)
                {
                    playerInfo = new PlayerInfo();
                    player.Info = new PlayerInfo();
                }

                if (model.PageStep == PageStep.Step1)
                {
                    playerContact.FirstName = model.FirstName;
                    playerContact.LastName = model.LastName;
                    playerContact.DoB = model.DOB != null ? DateTime.Parse(model.DOB) : (DateTime?)null; ;
                    playerContact.Email = model.Email;
                    playerInfo.Grade = model.Info.Grade != null ? model.Info.Grade.Value : 0;
                    playerInfo.HasPermissionPhotography = model.Info.HasPermissionPhotography;
                    player.Gender = model.Gender;
                    player.Contact = playerContact;
                    player.Relationship = RelationshipType.Registrant;
                    player.UserId = CurrentUserId;

                }
                else if (model.PageStep == PageStep.Step2) //school information
                {
                    playerInfo.SchoolName = model.Info.SchoolName;
                    playerInfo.HomeRoom = model.Info.HomeRoom;

                }
                else if (model.PageStep == PageStep.Step3) //Medical information
                {
                    playerInfo.Allergies = model.Info.HasAllergies == true ? model.Info.Allergies : "";
                    playerInfo.SpecialNeeds = model.Info.HasSpecialNeeds == true ? model.Info.SpecialNeeds : "";
                    playerInfo.DoctorLocation = model.Info.DoctorLocation;
                    playerInfo.DoctorName = model.Info.DoctorName;
                    playerInfo.DoctorPhone = model.Info.DoctorPhone != null ? model.Info.DoctorPhone.Replace("-", "") : null;
                    playerInfo.SelfAdministerMedication = model.Info.SelfAdministerMedication;
                    playerInfo.SelfAdministerMedicationDescription = model.Info.SelfAdministerMedication == true ? model.Info.SelfAdministerMedicationDescription : "";
                    playerInfo.NutAllergy = model.Info.NutAllergy;
                    playerInfo.HasAllergies = model.Info.HasAllergies;
                    playerInfo.HasSpecialNeeds = model.Info.HasSpecialNeeds;
                    playerInfo.NutAllergyDescription = model.Info.NutAllergy == true ? model.Info.NutAllergyDescription : "";
                }

                player.Info = playerInfo;

                if (model.PageMode == PageMode.Edit)
                {
                    SetActivityDescription(Constants.AuditingConstants.AuditingChangeFormat, playerContact.FullName);
                    //playerContact
                    result = Ioc.PlayerProfileBusiness.Update(player);

                }
                else
                {
                    SetActivityDescription(Constants.AuditingConstants.AuditingAddFormat, playerContact.FullName);

                    result = Ioc.PlayerProfileBusiness.Create(player);
                }

                var res = new JResult(result);
                Status = res.Status;

                return Json(new { Status = Status, Message = result.Message }, JsonRequestBehavior.AllowGet);
            }

            if (model.PageStep == PageStep.Step3)
            {
                return PartialView("_MedicalInformation", model);
            }

            model.Info.GradeList = _playerProfileBusiness.GetSchoolGrades();
            model.Info.GenderList = _playerProfileBusiness.GetGenderCategories();

            return PartialView("_ParticipantInformation", model);
        }

        [HttpGet]
        public virtual ActionResult EmergencyContact(int? Id)
        {
            var model = new EmergencyContactViewModel();
            var pageMode = PageMode.Create;

            if (Id.HasValue)
            {
                pageMode = PageMode.Edit;
            }

            if (pageMode == PageMode.Edit)
            {
                var userId = CurrentUserId;

                var emergencyContacts = Ioc.PlayerProfileBusiness.GetFamilyProfiles(userId).EmergencyContacts;
                var emergencyContact = ((IEnumerable)emergencyContacts).Cast<dynamic>().SingleOrDefault(c => c.Id == Id);

                model = new EmergencyContactViewModel
                {
                    Email = emergencyContact.Contact.Email,
                    FirstName = emergencyContact.Contact.FirstName,
                    LastName = emergencyContact.Contact.LastName,
                    FullName = emergencyContact.Contact.FullName,
                    DOB = emergencyContact.Contact.DoB != null ? emergencyContact.Contact.DoB.Value.Date.ToShortDateString() : null,
                    PrimaryPhone = emergencyContact.Contact.Phone,
                    CellPhone = emergencyContact.Contact.Cell,
                    Relationship = emergencyContact.Relationship,
                    ContactId = emergencyContact.ContactId,
                    Id = emergencyContact.Id,
                    PageMode = pageMode,
                };
            }

            return View(model);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Change_EmergencyContact)]
        public virtual ActionResult EmergencyContact(EmergencyContactViewModel model)
        {
            var userId = CurrentUserId;
            var playerContact = new ContactPerson();
            var dbResult = new OperationStatus();
            var contactsFamily = Ioc.FamilyContactBusiness.GetEmergencyContacts(userId).Where(e => e.Id != model.Id).Select(e => e.Contact).ToList();
            var family = Ioc.FamilyBusiness.GetFamily(userId);
            CheckResourceForAccess(family);

            var fullName = string.Format("{0} {1}", model.FirstName, model.LastName);

            if (Ioc.PlayerProfileBusiness.IsExistPlayer(contactsFamily, model.FirstName, model.LastName))
            {
                ModelState.AddModelError("FirstName", "The name you entered is an existing emergency contact.");
            }

            if (ModelState.IsValid)
            {
                if (model.PageMode == PageMode.Edit)
                {
                    playerContact = Ioc.ContactPersonBusiness.Get(model.ContactId);
                }
                playerContact.FirstName = model.FirstName;
                playerContact.LastName = model.LastName;
                playerContact.Email = model.Email;
                playerContact.Phone = model.PrimaryPhone != null ? model.PrimaryPhone.Replace("-", "") : null;
                playerContact.Cell = model.CellPhone != null ? model.CellPhone.Replace("-", "") : null;

                //playerContact
                if (model.PageMode == PageMode.Edit)
                {
                    family.Contacts.SingleOrDefault(f => f.ContactId == model.ContactId).Relationship = model.Relationship;
                    dbResult = Ioc.ContactPersonBusiness.Update(playerContact);
                    SetActivityDescription(Constants.AuditingConstants.AuditingChangeFormat, playerContact.FullName);
                }
                else
                {
                    var familyContact = new FamilyContact();
                    familyContact.Contact = playerContact;
                    familyContact.Type = FamilyContactType.Emergency;
                    familyContact.Relationship = model.Relationship;
                    familyContact.Family = family;

                    dbResult = Ioc.FamilyContactBusiness.Create(familyContact);
                    SetActivityDescription(Constants.AuditingConstants.AuditingAddFormat, playerContact.FullName);
                }

                var result = new JResult(dbResult);
                Status = result.Status;

                return Json(new { Status = result.Status }, JsonRequestBehavior.AllowGet);
            }
            return PartialView("_EmergencyContact", model);
        }

        [HttpGet]
        public virtual ActionResult AuthorizePickup(int? Id)
        {
            var pageMode = PageMode.Create;

            if (Id.HasValue)
            {
                pageMode = PageMode.Edit;
            }

            var model = new AuthorizepickupViewModel();

            if (pageMode == PageMode.Edit)
            {
                var userId = CurrentUserId;

                var authorizedPickups = Ioc.PlayerProfileBusiness.GetFamilyProfiles(userId).AuthorizedPickups;
                var authorizedPickup = ((IEnumerable)authorizedPickups).Cast<dynamic>().SingleOrDefault(c => c.Id == Id);

                model = new AuthorizepickupViewModel
                {
                    FirstName = authorizedPickup.Contact.FirstName,
                    LastName = authorizedPickup.Contact.LastName,
                    FullName = authorizedPickup.Contact.FullName,
                    PrimaryPhone = authorizedPickup.Contact.Phone,
                    CellPhone = authorizedPickup.Contact.Cell,
                    Relationship = authorizedPickup.Relationship,
                    Email = authorizedPickup.Contact.Email,
                    ContactId = authorizedPickup.ContactId,
                    IsEmergencyContact = authorizedPickup.IsEmergencyContact,
                    Id = authorizedPickup.Id,
                    PageMode = pageMode,
                };
            }

            return View(model);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Change_AuthorizedPickup)]
        public virtual ActionResult AuthorizePickup(AuthorizepickupViewModel model)
        {
            var userId = CurrentUserId;
            var user = Ioc.UserProfileBusiness.Get(userId);
            var playerContact = new ContactPerson();
            var contactsFamily = Ioc.FamilyContactBusiness.GetAuthorizedPickups(userId).Where(a => a.Id != model.Id).Select(e => e.Contact).ToList();
            var family = Ioc.FamilyBusiness.GetFamily(userId);
            CheckResourceForAccess(family);

            var dbResult = new OperationStatus();

            if (Ioc.PlayerProfileBusiness.IsExistPlayer(contactsFamily, model.FirstName, model.LastName))
            {
                ModelState.AddModelError("FirstName", "The name you entered is an existing authorize pickup contact.");

            }

            if (ModelState.IsValid)
            {
                if (model.PageMode == PageMode.Edit)
                {
                    family.Contacts.SingleOrDefault(f => f.ContactId == model.ContactId).Relationship = model.Relationship;
                    family.Contacts.SingleOrDefault(f => f.ContactId == model.ContactId).IsEmergencyContact = model.IsEmergencyContact;
                    playerContact = Ioc.ContactPersonBusiness.Get(model.ContactId);
                }
                playerContact.FirstName = model.FirstName;
                playerContact.LastName = model.LastName;
                playerContact.Phone = model.PrimaryPhone != null ? model.PrimaryPhone.Replace("-", "") : null;
                playerContact.Cell = model.CellPhone != null ? model.CellPhone.Replace("-", "") : null;
                playerContact.Email = model.Email;

                //playerContact
                if (model.PageMode == PageMode.Edit)
                {
                    dbResult = Ioc.ContactPersonBusiness.Update(playerContact);
                    SetActivityDescription(Constants.AuditingConstants.AuditingChangeFormat, playerContact.FullName);
                }
                else
                {
                    if (family == null)
                    {
                        family = Ioc.FamilyBusiness.AddUserFamily(userId);
                    }

                    var familyContact = new FamilyContact();
                    familyContact.Contact = playerContact;

                    familyContact.Type = FamilyContactType.AuthorizedPickup;
                    familyContact.Relationship = model.Relationship;
                    familyContact.Family = family;
                    familyContact.IsEmergencyContact = model.IsEmergencyContact;
                    dbResult = Ioc.FamilyContactBusiness.Create(familyContact);
                    SetActivityDescription(Constants.AuditingConstants.AuditingAddFormat, playerContact.FullName);
                }

                var result = new JResult(dbResult);
                Status = result.Status;

                return Json(new { Status = result.Status }, JsonRequestBehavior.AllowGet);
            }

            return PartialView("_AuthorizePickup", model);
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Delete_Player)]
        public virtual ActionResult DeletePlayer(int id)
        {
            try
            {
                var player = Ioc.PlayerProfileBusiness.Get(id);
                CheckResourceForAccess(player);

                var result = Ioc.PlayerProfileBusiness.DeletePlayerProfile(id);

                SetActivityDescription(Constants.AuditingConstants.AuditingDeleteFormat, player.Contact.FullName);
                Status = result.Status;

                return Json(new JResult { Status = result.Status, Message = result.Message, RecordsAffected = 0 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [JbAudit(JbAction.Family_Delete_Contact)]
        public virtual ActionResult DeleteFamilyContact(int id)
        {
            try
            {
                var familyContact = Ioc.FamilyContactBusiness.GetFamilyContact(id);
                CheckResourceForAccess(familyContact);

                var result = Ioc.FamilyContactBusiness.DeleteFamilyContact(id);

                Status = result.Status;

                SetActivityDescription(Constants.AuditingConstants.AuditingDeleteFormat, familyContact.Contact.FirstName);

                return Json(new JResult { Status = result.Status, Message = result.Message, RecordsAffected = 0 });
            }
            catch (Exception ex)
            {
                LogHelper.LogException(ex);
                return Json(new JResult { Status = false, Message = ex.Message, RecordsAffected = 0 });
            }
        }

        [HttpGet]
        public ActionResult ActivityLog(ParentActivityLogFilter model)
        {

            var activities = Enum.GetValues(typeof(JbAction))
               .Cast<JbAction>()
               .Where(a => JbZoneAttribute.GetZone(a) != null && JbZoneAttribute.GetZone(a) == JbZone.ParentDashboard)
               .Select(d => new SelectListItem { Text = Jumbula.Common.Helper.EnumHelper.GetEnumDescription(d), Value = d.ToString() })
               .OrderBy(a => a.Text)
               .ToList();

            activities.Insert(0, new SelectListItem { Text = "All", Value = "null" });

            ViewBag.Activities = activities;

            return View(model);
        }


        [HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult GetActivityLog(PaginationModel paginationModel, ParentActivityLogFilter filters)
        {
            var auditService = _auditBusiness;

            var club = this.GetCurrentClubBaseInfo();
            var clubTimeZone = club.TimeZone;

            var user = Ioc.UserProfileBusiness.Get(CurrentUserId);

            string activity = filters.Activity.HasValue ? filters.Activity.ToDescription() : null;
            string userName = !string.IsNullOrEmpty(filters.UserName) ? filters.UserName : null;

            var audits = auditService.GetParentAudits(this.GetCurrentClubDomain(), CurrentUserId, activity, userName);

            var totalItems = audits.Count();

            var pagedResult = audits.Pagination(paginationModel);

            var result = pagedResult.Select(a =>
                            new ParentActivityLogItemViewModel(a, clubTimeZone)).ToList();

            return Json(new { DataSource = result, TotalCount = totalItems }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ActivityLogDetail(Guid id)
        {
            var auditService = _auditBusiness;

            var audit = auditService.Get(id);
            var clubBaseInfo = this.GetCurrentClubBaseInfo();

            var model = new ParentActivityLogDetailViewModel(audit, clubBaseInfo.TimeZone);

            return View(model);
        }


    }
}