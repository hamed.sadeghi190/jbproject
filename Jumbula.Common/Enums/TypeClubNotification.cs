﻿

namespace Jumbula.Common.Enums
{
    public enum TypeClubNotification : byte
    {
        Confirmation = 1,
        Cancellation = 2,
        Absentee = 3,
        Reports = 4
    }
}
