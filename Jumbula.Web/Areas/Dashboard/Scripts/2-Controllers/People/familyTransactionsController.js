﻿(function () {
    'use strict';

    angular.module('dashboardApp').controller('familyTransactionsController', ['$scope', '$http', '$log', '$stateParams', '$state', 'jbToast', 'jbConfirmation', function ($scope, $http, $log, $stateParams, $state, jbToast, jbConfirmation) {
        window.app.globalObjects.pageClass = 'family-transactions-view';
        window.app.globalObjects.initializeView();

        $scope.MC.openBlade({ url: "Dashboard/Blade/People", order: 0, menu: "PEOPLE" });

        $scope.MC.stateParams = $stateParams;

        $scope.filter = {};
        $scope.filter.searchTerm = "";
        $scope.filter.participantId = -1;
        $scope.filter.statusReasonId = -1;
        $scope.filter.startDate = "";
        $scope.filter.endDate = "";
        $scope.transactionDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: {
                    url: window.location.origin + '/Dashboard/Order/GetUserTransactions/?userId=' + $scope.MC.stateParams.userId + '&clubId=' + $scope.MC.stateParams.clubId,
                    cache: false
                },
                parameterMap: function (data, type) {
                    if (type === "read") {
                        return {
                            searchTerm: $scope.filter.searchTerm,
                            participantId: $scope.filter.participantId,
                            startDate:$scope.filter.startDate ,
                            endDate: $scope.filter.endDate,
                            skip: data.skip,
                            take: data.take,
                            page: data.page,
                            pageSize: data.pageSize,
                            sort: data.sort
                        }
                    }
                }
            },
            pageSize: 10,
            batch: true,
            serverPaging: true,
            serverSorting: false,
            schema: {
                data: "data",
                total: "total"
            },
        });

    
        $http.get(window.location.origin + '/dashboard/people/GetUserListDetail/?userId=' + $scope.MC.stateParams.userId + '&clubId=' + $scope.MC.stateParams.clubId).success(function (data, status, headers, config) {
            $scope.Model = data;

        }).error(function (data, status, headers, config) {
            jbToast.error(data);
        });

        $scope.transactionGridOptions = {
            dataSource: $scope.transactionDataSource,
            sortable: true,
            serverPaging: true,
            columns: [
                   {
                       field: "Participant",
                       title: "Participant",
                       width: "70px"
                   },
                  {
                      field: "ConfirmationId",
                      title: "Confirmation",
                      width: "70px"
                  },
                 {
                     field: "StrTransactionDate",
                     title: "Date",
                     width: "70px"
                 },
                 {
                     field: "StrAmount",
                     title: "Amount",
                     width: "50px"
                 },
                 {
                     field: "StrPaymentMethod",
                     title: "Payment method",
                     width: "90px"
                 },
                 {
                     field: "TransactionId",
                     title: "Transaction Id",
                     width: "100px"
                 },
                  {
                      field: "Note",
                      title: "Memo",
                      width: "100px"
                }
      //          {
      //                title: "Actions",
      //                width: "70px",
      //                template: "<ul kendo-menu style='display: inline-block;' k-direction='\"left\"' k-orientation='\"horizontal\"' k-rebind='\"horizontal\"' k-on-select=''>\
      //                  #if(EnableUndoRefund){#\
						//    <li> ...\
      //                          <ul>\
      //                             <li class='k-item k-state-default k-first' role='menuitem'>\
      //                                  <span ng-click=\"UndoRefundFromFamilyCreditToOrder('#=Id#','#=OrderItemId#')\" class=\"k-link jbi-angle-right\">Undo to order</span>\
      //                              </li>\
      //                          </ul>\
      //                      </li>\
						//#}#\
      //              </ul>"
      //            }
            ],
            pageable: {
                refresh: true,
                pageSizes:
                    [
                       5, 10, 20, 30, 50, 100
                    ],
                buttonCount: 5
            },
        };

        $scope.search = function () {
            $("#transactionGrid").data("kendoGrid").dataSource.read();
        };

        $scope.clearFilters = function () {
            $scope.filter.searchTerm = "";
            $scope.filter.participantId = -1;
            $scope.filter.startDate = "";
            $scope.filter.endDate = "";
            $("#transactionGrid").data("kendoGrid").dataSource.read();
        }

     
        $scope.editPayment = function (programid, transactionId, orderItemId, seasonDomain) {
            window.goToFamilyOrder = $scope.MC.stateParams.userId;
            $state.go("MakePayment", { seasonDomain: seasonDomain, programId: programid, orderItemId: orderItemId, transactionId: transactionId });
        }
    }])
})();