﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Jumbula.Common.Utilities;
using Jumbula.Core.Domain;

namespace Jumbula.Core.Data
{
    public interface IContactRepository : IRepository<ContactPerson>
    {
        PaginatedList<ContactPerson> GetClubContacts(List<int> clubIds, Expression<Func<ContactPerson, bool>> predicate, string emailAddress, List<Sort> sorts, int pageIndex, int pageSize);
    }
}
