﻿using Jb.Framework.Common.Forms.JsonFormatter;
using Jb.Framework.Common.Forms.Validation;
using Newtonsoft.Json;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Jb.Framework.Common.Forms
{
    [FluentValidation.Attributes.Validator(typeof(PhoneValidator))]
    [JsonConverter(typeof(JbElementConverter))]
    public class JbPhone : JbTextBox
    {

        [DisplayName("Phone type")]
        public PhoneType PhoneType { get; set; }

        //[DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone number")]
       
        public override string Value { get; set; }
    }

    public enum PhoneType : byte
    {
        [Description("US/Canada phone number")]
        US = 0,
        [Description("International phone number")]
        INTERNATIONAL = 1
    }
}
