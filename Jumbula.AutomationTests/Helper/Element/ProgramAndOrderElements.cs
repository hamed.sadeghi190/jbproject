﻿using System.Linq;
using OpenQA.Selenium;

namespace Jumbula.AutomationTests.Helper.Element
{
    class ProgramAndOrderElements : BaseElements
    {
        public ProgramAndOrderElements(IWebDriver driver)
            : base(driver)
        {

        }

        #region Filter
        public IWebElement SearchTerm
        {
            get
            {
                return Driver.FindElement(By.Id("searchPrgramName"));
            }
        }
        public IWebElement SearchBtn
        {
            get
            {
                var btns = Driver.FindElements(By.TagName("button"));
                return btns.First(btn => btn.Text == "Search");
            }
        }
        #endregion Filter
        #region GridRow1
        public IWebElement Row1Actions
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='programGrid']/div[2]/table/tbody/tr[1]/td[14]/ul/li/span"));
            }
        }
        public IWebElement Row1ClassSchedulingEmailAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='programGrid']/div[2]/table/tbody/tr/td[14]/ul/li/div/ul/li[8]/span/span[1]"));
            }
        }
        public IWebElement Row1OrderslAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='programGrid']//tr[1]//span[text()='Orders']"));
            }
        }
        public IWebElement Row1ConfirmationEmailAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='programGrid']/div[2]/table/tbody/tr/td[14]/ul/li/div/ul/li[8]/div/ul/li[1]/span"));
            }
        }
        public IWebElement Row1CancelletionEmailAction
        {
            get
            {
                return Driver.FindElement(By.XPath("//*[@id='programGrid']/div[2]/table/tbody/tr/td[14]/ul/li/div/ul/li[8]/div/ul/li[2]/span"));
            }
        }
        #endregion GridRow1
    }
}