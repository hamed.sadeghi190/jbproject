﻿namespace Jumbula.Common.Constants
{
    public static class Lottery
    {
        public static string MaximumCapacityRequiredMessage = "You must set the capacity on the class enrollment maximum before yon can draw the lottery.";
        public static string SuccessfullyCandidateMessage = "Successfully marked as candidate";
        public static string NoSpaceForLotteryMessage = "No capacity left to draw for";
        public static string DrawLottery = "Draw lottery";
        public static string FinalizeLottery = "Finalize lottery";
        public static string DrawBeforeFinalizeMessage = "You must draw the lottery before you can finalize";
        public static string LotteryIsNotEnabled = "Lottery is not enabled";
    }
}
