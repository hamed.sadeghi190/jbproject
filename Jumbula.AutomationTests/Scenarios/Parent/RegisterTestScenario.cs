﻿using Jumbula.AutomationTests.Helper;
using Jumbula.AutomationTests.Tests;
using NUnit.Framework;

namespace Jumbula.AutomationTests.Scenarios.Parent
{
    public class RegisterTestScenario : BaseTestScenario
    {
        [Test]
        public void RegisterNewClass()
        {
            //BaseUrl = Helper.DataDriver.NewClassURL;
            base.Browser = Browser.Chrome;

            // Navigate
            base.WebDriver.Navigate().GoToUrl(BaseUrl);
            WebDriver.Manage().Window.Maximize();
            ExtraAPI.Wait(2000);

            // Register
            RegisterTest registerTest = new RegisterTest(WebDriver);
            registerTest.RegisterProgramStep1();

            // Login parent
            LoginTest loginTest = new LoginTest(WebDriver);
            //loginTest.LoginParent();

            // Regisetr
            registerTest.RegisterNewClass();
        }
    }
}
