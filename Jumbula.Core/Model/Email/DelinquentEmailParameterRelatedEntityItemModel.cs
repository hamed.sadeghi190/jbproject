﻿using System.Collections.Generic;

namespace Jumbula.Core.Model.Email
{
    public class DelinquentEmailParameterRelatedEntityItemModel
    {
        public string RelatedEntityName { get; set; }
        public long RelatedEntityId { get; set; }
    }
}
