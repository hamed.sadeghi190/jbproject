﻿namespace Jumbula.Core.Model.ClubSetting
{
    public class DuplicateEnrollmentPolicySettingViewModel
    {
        public bool PreventSameClass { get; set; }
        public bool PreventOtherClasses { get; set; }
        public bool PreventSameCampSchedule { get; set; }
        public bool PreventMultipleCampDropIn { get; set; }
        public bool PreventSameProgramBeforeAfterWeekDay { get; set; }
    }
}
