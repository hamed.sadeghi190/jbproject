﻿using Elmah;
using System;

namespace Jumbula.Common.Helper
{
    public static class LogHelper
    {
        public static void LogException(Exception ex)
        {
            ErrorSignal.FromCurrentContext().Raise(ex);
        }

        public static void LogException(Exception ex, string messsage)
        {
            ErrorSignal.FromCurrentContext().Raise(new Exception(messsage, ex));
        }
    }
}
