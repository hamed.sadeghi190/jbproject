﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SportsClub.Areas.Dashboard.Models
{
    public class SessionStatementDataReportViewModel
    {
        public string SeasonName { get; set; }

        public string SeasonRosterDueDate { get; set; }

        public string SeasonPaymentsIssuedDate { get; set; }

        public string ProgramName { get; set; }

        public string ProviderName { get; set; }

        public decimal Cost { get; set; }

        public int TotalOfRegistrations { get; set; }

        public decimal TotalOfProviderFunded { get; set; }

        public string ProviderAddress { get; set; }

        public string ProviderContactEmail { get; set; }

        public string ProviderContactName { get; set; }

        public string EMRate { get; set; }
    }
}