﻿using System;
using System.Web.Mvc;
using Jumbula.Web.Mvc.Controllers;
using SportsClub.Models;

namespace Jumbula.Web.Controllers
{
    public class FeedBackController : JbBaseController
    {

        public virtual ActionResult Index()
        {
            return PartialView("_FeedBack");
        }

        [RequireHttps]
        [HttpPost]
        [AllowAnonymous]
        public virtual JsonResult Submit(GeneralFeedBackModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Email.SendFeedbackToSupport(model.DescribeIdea,model.ShortSummary,model.Name,model.Email);
                    //NotificationHelper.Add(Constants.F_ReceivedFeedbackNotificationTitle,
                    //    Constants.F_ReceivedFeedbackNotificationDesc);
                    return Json(new { Status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { Message = e.Message });
                }
            }

            // If we got this far, something failed, redisplay form
            return Json(new { Message = "erorr..." }, JsonRequestBehavior.AllowGet);
        }
    }
}
