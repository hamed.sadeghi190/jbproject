﻿using Jumbula.Common.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Jumbula.Common.Base;
using Jumbula.Core.Domain;
using Jumbula.Core.Model.Generic;

namespace SportsClub.Areas.Dashboard.Models
{
    public class TournamentCreateEditModelStep2 : BaseViewModel<Program, long>, IProgramWizardModel
    {
        public string ClubDomain { get; set; }
        public string SeasonDomain { get; set; }
        public PageMode PageMode
        {
            get
            {

                if (this.PageStep <= this.LastCreatedPage)
                {
                    return PageMode.Edit;
                }

                return PageMode.Create;
            }
        }
        public ProgramPageStep PageStep { get; set; }
        public ProgramPageStep LastCreatedPage { get; set; }
        public SaveType SaveType { get; set; }
        public string Domain { get; set; }
        public string Name { get; set; }
        public ProgramStatus Status { get; set; }
        public ProgramRegStatus RegStatus { get; set; }
        public ProgramRunningStatus RunningStatus { get; set; }
        public ProgramTypeCategory TypeCategory { get; set; }
        public List<ProgramScheduleViewModel> Schedules { get; set; }

        [Display(Name = "Prize fund")]
        public decimal? PrizeFund { get; set; }

        [Display(Name = "Number of full entries")]
        public int? FullEntries { get; set; }

        [Display(Name = "Prize fund guarantee")]
        public string PrizeFundGuarantee { get; set; }

        public List<TournamentSection> TourneySections { get; set; }

        public List<TournamentSchedule> TourneySchedules { get; set; }

        public TournamentType TournamentType { get; set; }

        public List<SelectKeyValue<string>> TournamentTypes { get; set; }

        [Display(Name = "Max of byes")]
        public int? MaxOfByes { get; set; }

        [Display(Name = "Last round bye")]
        public int? LastRoundBye { get; set; }

        [Display(Name = "Number of rounds")]
        public int? NumberOfRounds { get; set; }

        [Display(Name = "Bye note")]
        public string ByeNote { get; set; }

        [Display(Name = "Trophies")]
        public string Trophies { get; set; }

        public bool HideViewEntries { get; set; }

        public List<SelectKeyValue<string>> Capacities { get; set; }

        public List<SelectKeyValue<int>> MinimumEnrollments { get; set; }

        public List<SelectKeyValue<string>> Sessions { get; set; }

        public List<SelectKeyValue<string>> Ages { get; set; }
    }
}