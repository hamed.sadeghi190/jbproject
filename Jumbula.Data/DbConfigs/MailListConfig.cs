﻿using Jumbula.Core.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Jumbula.Data.DbConfigs
{
    public class MailListConfig : EntityTypeConfiguration<MailList>
    {
        public MailListConfig()
        {
            HasMany(c => c.ListContact).WithRequired().WillCascadeOnDelete(true);
        }
    }
}
